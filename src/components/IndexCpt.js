import '../styles/index.scss'

import React from "react"
import { Link } from "gatsby"
import { Row, Col } from 'antd'
import { allProductLabels } from '../utils/menu_config'
import { FormattedMessage } from 'react-intl'

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = ({ pageContext }) => {
  const { lang } = pageContext

  const menuKeys = [
    ['mogdb-stack', 'manager', 'parameter'],
    ['mogeaver', 'ptk', 'mogha'],
    ['mtk', 'mdb', 'sca', 'mvd'],
    ['uqbar']
  ]

  const getMenus = (arr, l = 1) => {
    return <>
      {arr.map((k, i) => {
        const productData = allProductLabels[k]
        if (productData.isExternalLink) {
          return (
            <a href={`${productData.link}&lang=${lang}`} rel="noreferrer" target="_blank" className={`product-box ${k} ${productData.disabled ? 'disabled' : ''} ${i >= arr.length - l ? 'no-bottom' : ''}`} key={i}>
              <div className="desc">{productData['alias' + lang] || productData[lang]}</div>
              <div className="title-box">
                <div className={`${k} ${productData.disabled ? 'disabled' : ''} w-product-icon-colors `}></div>
                <div className="title">{productData.label || productData[lang]}</div>
              </div>
            </a>
          )
        } else {
          return (
            <Link to={`/${lang}/${productData.link || k}/`} className={`product-box ${k} ${productData.disabled ? 'disabled' : ''} ${i >= arr.length - l ? 'no-bottom' : ''}`} key={i}>
              <div className="desc">{productData['alias' + lang] || productData[lang]}</div>
              <div className="title-box">
                <div className={`${k} ${productData.disabled ? 'disabled' : ''} w-product-icon-colors `}></div>
                <div className="title">{productData.label || productData[lang]}</div>
              </div>
            </Link>
          )
        }
      })}
    </>
  }

  return (
    <Layout pageContext={pageContext}>
      <Seo
        pageContext={pageContext}
        title="MogDB Docs"
        description="{frontmatter.summary}" />
      <article className="index-container">

        <div className="menu-container">
          <Row gutter={24} >
            <Col span={24} md={10}>
              <div>
                <Link to={`/${lang}/mogdb`} className='product-box mogdb'>
                  <div className="title">{allProductLabels.mogdb.label || allProductLabels.mogdb[lang]}</div>
                  <div className="desc">{allProductLabels.mogdb['alias' + lang] || allProductLabels.mogdb[lang]}</div>
                </Link>
                {getMenus(menuKeys[3])}
              </div>
            </Col>
            <Col span={24} sm={12} md={8}>{getMenus(menuKeys[0])}</Col>
            <Col span={24} sm={12} md={6}>{getMenus(menuKeys[1])}</Col>
          </Row>
        </div>

        <h2 className="box-title"><FormattedMessage id="index.migrateTitle" /></h2>
        <div className="migrate-container">
          <Row gutter={24}>
            <Col span={24} md={16}>
              <div className="left-box">{getMenus(menuKeys[2], 2)}</div>
            </Col>
            <Col span={24} md={8}><div className="right-box"></div></Col>
          </Row>
        </div>
      </article>
    </Layout>
  )
}
export default IndexPage

