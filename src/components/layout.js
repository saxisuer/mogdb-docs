import React from 'react'
import { IntlProvider } from 'react-intl'
import flat from 'flat'
import langMap from '../intl'

import Header from './header'
import Footer from './footer'

const Layout = ({ pageContext, children }) => {
  const lang = (pageContext && pageContext.lang) || 'zh'
  return (
    <IntlProvider locale={lang} messages={flat(langMap[lang])}>
      <Header pageContext={pageContext} />
      <main className="main-container">
        {children}
        <Footer pageContext={pageContext} />
      </main>
    </IntlProvider>
  )
}

export default Layout
