/* eslint-disable */
import '../styles/components/header.scss'

import React, { useState, Fragment } from "react"
import { Link, navigate } from "gatsby"
import { useLocation } from '@reach/router'

import { FormattedMessage } from 'react-intl'
import { Popover } from 'antd'

import SearchInput from "./seachInput"
import { allToolsMenu } from '../utils/menu_config'

const Header = ({ pageContext }) => {
  const location = useLocation()
  const { lang, docType } = pageContext || {}
  // 配置产品列表
  const goToolsDocs = (type, link) => {
    const url = link ? `${link}&lang=${lang}` : `/${lang}/${type}`
    setMediaMenuVisible(false)
    navigate(url)
  }

  // 获取数据库菜单
  const dbtools = allToolsMenu.find(item => item.key === 'database')
  
  // 获取文档工具菜单
  const doctools = allToolsMenu.find(item => item.key === 'doc_tools')
  const [doctoolsVisible, setDoctoolsVisible] = useState(false)
  const handleVisibleChange = (newVisible) => {
    setDoctoolsVisible(newVisible)
  }
  const doctoolsContent = (
    <div className="tools-down-menu">
      {doctools.menu.map((item, idx) => (
        <div className={`box ${item.disabled ? 'disabled' : ''}`} key={idx}>
          <div className={`${item.value} w-product-icon-menu`}></div>
          <a className="title" href={`${item.link}&lang=${lang}`} target="_blank" onClick={() => setDoctoolsVisible(false)}>{item[lang]}</a>
        </div>
      ))}
    </div>
  )
  // 获取生态工具菜单
  const ecologicalTools = allToolsMenu.filter(item => item.isEcologicalTools)
  const toolsContent = (
    <div className="tools-down-menu">
      {ecologicalTools.map((item, idx) => (
        <Fragment key={idx}>
          <div className="type-title">{item[lang]}</div>
          <div className="tools-box">
            {item.menu.map((m, i) => (
              <div className={`box ${docType === m.value ? 'is-active' : ''} ${m.disabled ? 'disabled' : ''}`} key={i} onClick={() => goToolsDocs(m.value)}>
                <div className={`${m.value} w-product-icon-menu`}></div>
                <div className="info">
                  <div className="title">{m.label}</div>
                  <div className="desc">{m[lang]}</div>
                </div>
              </div>
            ))}
          </div>
        </Fragment>
      ))}
    </div>
  )

  // 语言切换
  const switchLang = () => {
    const toLang = lang === 'zh' ? 'en' : 'zh'
    if (location.pathname === '/') {
      navigate('/en')
    } else {
      const url = location.pathname.replace(/(zh|en)/, toLang) + location.search + location.hash
      navigate(url)
    }
  }

  // 移动端产品菜单
  const [mediaMenuVisible, setMediaMenuVisible] = useState(false)
  const [activeMediaMenuTab, setMediaMenuActiveTab] = useState('database')
  const allMediaMenu = allToolsMenu.filter(item => !item.mediaHeaderDisbled)

  // 处理遮罩层点击
  function handelMaskClick() {
    setMediaMenuVisible(false)
    document.body.style.overflow = 'auto'
  }
  function handelBoxClick(e) {
    e.stopPropagation();
  }

  return (
    <header className="header-container">
      <a href={`https://www.mogdb.io${lang === 'en' ? '/en' : ''}`} target="_blank" className="logo-box" alt="LOGO"></a>
      <Link to={`${lang === 'en' ? '/en' : '/'}`} className={`docs-link menu-link index-link ${!docType ? 'is-active' : ''}`}><FormattedMessage id="header.index" /></Link>
      {docType && <SearchInput pageContext={pageContext} />}
      <div className="right-box">
        <Popover overlayClassName="doc-tools-dropdown" placement="bottomRight" content={doctoolsContent} visible={doctoolsVisible} onVisibleChange={handleVisibleChange} trigger="click">
          <div className="docs-link menu-link"><FormattedMessage id="header.doctools" /></div>
        </Popover>
        {dbtools.menu.map((item, idx) => (
          !item.disabled && <Link key={idx} className={`docs-link menu-link ${docType === item.value ? 'is-active' : ''}`} to={`/${lang}/${item.value}/`}>{item.label}</Link>
        ))}
        <Popover overlayClassName="tools-dropdown" placement="bottomRight" content={toolsContent} trigger="click">
          <div className={`tools-dropdown-trigger ${(docType && !['mogdb', 'mogdb-stack', 'uqbar'].includes(docType)) ? 'is-active' : ''}`}><FormattedMessage id="header.ecologicaltools" /></div>
        </Popover>
        <div onClick={() => switchLang()} className="menu-link"><FormattedMessage id="header.lang" /></div>
        <Link to={`${lang === 'en' ? '/en' : '/'}`} className="media-index-btn"></Link>
        <div className="media-tools-btn" onClick={() => setMediaMenuVisible(true)}></div>
        {mediaMenuVisible &&
          <div className="media-tools-box-mask" onClick={e => handelMaskClick(e)}>
            <div className="close-btn"></div>
            <div className="media-tools-box" onClick={e => handelBoxClick(e)}>
              <div className="tab-box">
                {allMediaMenu.map((item, idx) => (
                  <div key={idx} className={`${activeMediaMenuTab === item.key ? 'is-active' : ''} tab-item`} onClick={() => setMediaMenuActiveTab(item.key)}>{item[lang].replace('Management ', '')}</div>
                ))}
              </div>
              <div className="tab-content-box">
                {allMediaMenu.map((item, idx) => (
                  <Fragment key={idx}>
                    {activeMediaMenuTab === item.key && item.menu.map((m, i) => (
                      <div className={`box ${item.key} ${m.disabled ? 'disabled' : ''}`} key={i} onClick={() => goToolsDocs(m.value, m.link)}>
                        <div className={`${m.value} w-product-icon-menu`}></div>
                        {m.label ?
                          <div className="box-info">
                            <div className="title">{m.label}</div>
                            <div className="desc">{m[lang]}</div>
                          </div>
                          :
                          <div className="box-info">
                            <div className="title">{m[lang]}</div>
                          </div>
                        }
                      </div>
                    ))}
                  </Fragment>
                ))}
              </div>
            </div>
          </div>
        }
      </div>
    </header>
  )
}

export default Header