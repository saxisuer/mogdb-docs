/* eslint-disable */ 
import '../styles/components/footer.scss'

import React from "react"

const Footer = () => {
  return (
    <footer className="footer-container">
      Copyright © 2009-2022 www.enmotech.com All rights reserved.
    </footer>
  )
}

export default Footer