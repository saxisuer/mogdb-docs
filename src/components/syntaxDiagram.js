import '../styles/components/syntaxDiagram.scss'
import PropTypes from 'prop-types'
import React from 'react'
import langMap from '../intl'
// import Prism from 'prismjs'
// import 'prismjs/components/prism-ebnf'

const SyntaxDiagram = ({ lang, diagramTree, code }) => {
  const [value, setValue] = React.useState(0)
  // const codeHtml = Prism.highlightAll(code, Prism.languages.ebnf, 'ebnf')

  return (
    <div className="syntax-diagram-root">
      <div className="syntax-diagram-toolbar buttons are-small are-light has-addons is-right">
        <button className={`button is-light ${value === 0 ? 'is-active' : ''}`} onClick={() => setValue(0)}>
          <span>{langMap[lang].doc.diagram}</span>
        </button>
        <button className={`button is-light ${value === 1 ? 'is-active' : ''}`} onClick={() => setValue(1)}>
          <span>{langMap[lang].doc.sourceCode}</span>
        </button>
      </div>
      <div
        hidden={value !== 0}
        className="syntax-diagram-container"
        dangerouslySetInnerHTML={{__html: diagramTree}}
      >
      </div>
      <div hidden={value !== 1}>
        <pre className="language-ebnf">
          {code }
          {/* <code className="language-ebnf" dangerouslySetInnerHTML={{__html: codeHtml}}></code> */}
        </pre>
      </div>
    </div>
  )
}

SyntaxDiagram.propTypes = {
  diagramTree: PropTypes.string.isRequired,
  code: PropTypes.string.isRequired,
  lang: PropTypes.string.isRequired
}

export default SyntaxDiagram
