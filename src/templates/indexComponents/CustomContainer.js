import * as React from "react"

const CustomContainer = ({ title, subTitle, children }) => {
  return (
    <>
      <div className="title-box">
        <h1>{title}</h1>
        <p>{subTitle}</p>
      </div>
      <div className="index-box">{ children }</div>
    </>
  )
}
export default CustomContainer
