import React from "react"
import IndexCpt from "../components/IndexCpt"

const IndexPage = () => {
  return <IndexCpt pageContext={{ lang: 'en' }}/>
}
export default IndexPage

