---
title: Release Note
summary: Release Note
author: Wang Dong
date: 2022-06-30
---

# Release Note

## v1.0.0

Release mgo client v1.0.0 and MogDB operator v1.0.0

[MogDB operator](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar)

mgo client

+ [mgo_linux_x86_64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_linux_x86_64)
+ [mgo_linux_arm64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_linux_arm64)
+ [mgo_darwin_x86_64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_darwin_x86_64)
+ [mgo_darwin_arm64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_darwin_arm64)