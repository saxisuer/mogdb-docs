---
title: kustomize
summary: kustomize
author: Ji Yabin
date: 2022-06-16
---

## Prerequisites

- Install Kustomize v3+

  ```shell
  go get sigs.k8s.io/kustomize/kustomize/v3@v3.8.7
  ~/go/bin/kustomize version
  ```

- Download MogDB Operator sample

  The MogDB Operator Kustomize installer is included.

  <https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar>

  ```shell
  wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar
  tar xf mogdb-operator-examples.tar
  cd mogdb-operator-examples
  ```

  The MogDB Operator installation item is in the **kustomize/install** directory.

- Installed components
  - mogdb-operator-controller-manager
  
  - mogdb-apiserver
  
  - mgo-client
  
    Where mgo-client runs under the physical machine, the rest of the components run under k8s
  
- Generated k8s objects
  - mogdb-operator-manager-config
  - mogdb-operator-mgo-config
  - mogdb-operator-huawei-registry

## Configuration

The default Kustomize will work in most Kubernetes environments, or can be customized to meet your specific needs.

For example, the custom image path for MogDB Operator, which is in the kustomize/install/default/kustomization.yaml file, can be modified to:

```yaml
images:
- name: controller
  newName: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-operator
  newTag: v1.0.0
```

If you need to change the namespace name, you need to change the following configuration in the kustomize/install/default/kustomization.yaml file.

```yaml
namespace: custom-namespace
```

## Installation

### Install MogDB Operator

```shell
kustomize build ./kustomize/install/default | kubectl apply -f -
```

Expected output:

```text
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-apiserver created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
configmap/mogdb-operator-mgo-config created
secret/mgorole-admin created
secret/mgouser-admin created
secret/mogdb-operator-huawei-registry created
service/mogdb-apiserver created
deployment.apps/mogdb-apiserver created
deployment.apps/mogdb-operator-controller-manager created
```

### Install the mgo client

```shell
wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/client-setup.sh
chmod +x client-setup.sh
./client-setup.sh
```

This will download the mgo client and prompt you to add some environment variables for you to set in your session, which you can do with the following command.

```shell
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
```

If you wish to permanently add these variables to your environment, you can run the following command.

```shell
cat <<EOF >> ~/.bashrc
export PATH="${HOME?}/.mgo:$PATH"
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
EOF

source ~/.bashrc
```

> **Note**: For macOS users, you use the file **\~/.bash_profile** instead of **\~/.bashrc**.

## Uninstall

```shell
kustomize build config/default | kubectl delete -f -
```

Expected output:

```shell
namespace "mogdb-operator-system" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbbackups.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbclusters.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbrecoveries.mogdb.enmotech.io" deleted
serviceaccount "mogdb-operator-controller-manager" deleted
role.rbac.authorization.k8s.io "mogdb-operator-leader-election-role" deleted
clusterrole.rbac.authorization.k8s.io "mogdb-operator-manager-role" deleted
rolebinding.rbac.authorization.k8s.io "mogdb-operator-leader-election-rolebinding" deleted
clusterrolebinding.rbac.authorization.k8s.io "mogdb-operator-manager-rolebinding" deleted
configmap "mogdb-operator-manager-config" deleted
secret "mogdb-operator-huawei-registry" deleted
deployment.apps "mogdb-operator-controller-manager" deleted
```

> **Note**:
>
> Before uninstalling, please make sure all clusters on the system have been deleted completely, otherwise it will not be uninstalled.
