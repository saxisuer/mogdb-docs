<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# Documentation

## MogDB Stack Documentation

+ [About MogDB Stack](/overview.md)
+ [Quick Start](/quick-start.md)
+ Installation
    + [Kustomize](installation/kustomize.md)
    + [Kubectl](installation/kubectl.md)
+ Tutorial
    + [Start](tutorial/getting-started.md)
    + [Create MogDB Cluster](tutorial/create-a-mogdb-cluster.md)
    + [Connect to MogDB Cluster](tutorial/conntect-to-mogdb-cluster.md)
    + [Resize MogDB Cluster](tutorial/resize-a-mogdb-cluster.md)
    + [Customize MogDB Cluster](tutorial/customize-a-mogdb-cluster.md)
+ Architecture
    + [High Availability](architecture/high-availability.md)
    + [Monitor](architecture/monitor.md)
    + [Backup Management](architecture/backup-management.md)
    + [Recovery Management](architecture/recovery-management.md)
    + [Client](architecture/client.md)
+ References
    + Client
        + [mgo](references/client/mgo.md)
        + [mgo create](references/client/mgo-create.md)
        + [mgo create mgorole](references/client/mgo-create-mgorole.md)
        + [mgo create mgouser](references/client/mgo-create-mgouser.md)
        + [mgo create cluster](references/client/mgo-create-cluster.md)
        + [mgo show](references/client/mgo-show.md)
        + [mgo show mgorole](references/client/mgo-show-mgorole.md)
        + [mgo show mgouser](references/client/mgo-show-mgouser.md)
        + [mgo show cluster](references/client/mgo-show-cluster.md)
        + [mgo delete](references/client/mgo-delete.md)
        + [mgo delete mgorole](references/client/mgo-delete-mgorole.md)
        + [mgo delete mgouser](references/client/mgo-delete-mgouser.md)
        + [mgo delete cluster](references/client/mgo-delete-cluster.md)
        + [mgo scale](references/client/mgo-scale.md)
        + [mgo scaledown](references/client/mgo-scaledown.md)
        + [mgo switch](references/client/mgo-switch.md)
        + [mgo update](references/client/mgo-update.md)
        + [mgo update mgorole](references/client/mgo-update-mgorole.md)
        + [mgo update mgouser](references/client/mgo-update-mgouser.md)
        + [mgo update cluster](references/client/mgo-update-cluster.md)
        + [mgo version](references/client/mgo-version.md)
    + Server
        + [CRD Reference](references/server/crd-references.md)
        + [MogDB Cluster Reference](references/server/mogdbcluster-sample.md)
        + [MogDB Backup Reference](references/server/mogdbbackup-sample.md)
        + [MogDB Recovery Reference](references/server/mogdbrecovery-sample.md)
+ [FAQ](faq.md)
+ [Release Note](release.md)
