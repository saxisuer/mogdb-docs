---
title: Quick Start
summary: Quick Start
author: Ji Yabin
date: 2022-06-16
---

# Quick Start

This article describes how to create a simple Kubernetes cluster, deploy MogDB Operator, and deploy a MogDB cluster using Mogdb Operator.

> The deployment instructions in this article are for testing purposes only and should not be used directly in a production environment. To deploy in a production environment, please refer to **Deploying MogDB Cluster** section.

Basic steps are as follows.

[1. Creating Kubernetes Test Cluster](#creating-kubernetes-test-cluster)

[2. Deploying MogDB Operator](#deploying-mogdb-operator)

[3. Deploying MogDB Cluster](#deploying-mogdb-cluster)

[4. Connecting to MogDB Cluster](#connecting-to-mogdb-cluster)

[5. Destroying MogDB Cluster](#destroying-mogdb-cluster)

<br/>

## Creating a Kubernetes Test Cluster

This section describes how to deploy a Kubernetes cluster using minikube.

minikube creates a Kubernetes cluster in a virtual machine that can run on macOS, Linux, and Windows.

Before deploying, make sure the following requirements are met.

- minikube: version 1.18.1+

- minikube requires a compatible hypervisor to be installed, refer to the official installation tutorial for details.

- kubectl: version >= 1.18.1

After installing minikube, you can start a Kubernetes cluster by executing the following command.

```shell
minikube start
```

If everything is running correctly, you will see output similar to the following, with slight variations depending on the operating system and the hypervisor used.

```shell
😄  minikube v1.10.1 on Darwin 10.15.4
✨  Automatically selected the hyperkit driver. Other choices: docker, vmwarefusion
💾  Downloading driver docker-machine-driver-hyperkit:
    > docker-machine-driver-hyperkit.sha256: 65 B / 65 B [---] 100.00% ? p/s 0s
    > docker-machine-driver-hyperkit: 10.90 MiB / 10.90 MiB  100.00% 1.76 MiB p
🔑  The 'hyperkit' driver requires elevated permissions. The following commands will be executed:

    $ sudo chown root:wheel /Users/user/.minikube/bin/docker-machine-driver-hyperkit
    $ sudo chmod u+s /Users/user/.minikube/bin/docker-machine-driver-hyperkit


💿  Downloading VM boot image ...
    > minikube-v1.10.0.iso.sha256: 65 B / 65 B [-------------] 100.00% ? p/s 0s
    > minikube-v1.10.0.iso: 174.99 MiB / 174.99 MiB [] 100.00% 6.63 MiB p/s 27s
👍  Starting control plane node minikube in cluster minikube
💾  Downloading Kubernetes v1.18.2 preload ...
    > preloaded-images-k8s-v3-v1.18.2-docker-overlay2-amd64.tar.lz4: 525.43 MiB
🔥  Creating hyperkit VM (CPUs=2, Memory=4000MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.18.2 on Docker 19.03.8 ...
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
```

For mainland China users, you can use the **gcr.io mirror** repository, such as **registry.cn-hangzhou.aliyuncs.com/google_containers**.

```shell
minikube start --image-repository registry.cn-hangzhou.aliyuncs.com/google_containers
```

Or configure an HTTP/HTTPS proxy for Docker.

Replace **127.0.0.1:1086** with your own HTTP/HTTPS proxy address in the following command.

```shell
minikube start --docker-env https_proxy=http://127.0.0.1:1086 \
      --docker-env http_proxy=http://127.0.0.1:1086
```

> **Note**: 
>
> Since minikube (by default) runs through a virtual machine with 127.0.0.1 pointing to the virtual machine itself, you may need to change the proxy to the actual IP of your host in some cases.

Please refer to minikube setup to see more options for configuring virtual machines and Kubernetes clusters.

You can use minikube's subcommand **kubectl** for clustering operations. To make the **kubectl** command work, you need to add the following alias setup command to your shell configuration file, or execute the following alias setup command after opening a new shell.

```shell
alias kubectl='minikube kubectl --'
```

Run the following command to check the cluster status and ensure that the cluster can be accessed via kubectl:

```shell
kubectl cluster-info
```

The desired output is as follows.

```shell
Kubernetes master is running at https://192.168.64.2:8443
KubeDNS is running at https://192.168.64.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Now you can start deploying MogDB Operator!

After testing is complete, run the following command to destroy the cluster.

```shell
minikube delete
```

<br/>

## Deploying MogDB Operator

Before starting, ensure that the following requirements are met.

- Kubernetes clusters that can be accessed using kubectl
- Kustomize v3+ already installed

**Step 1: Download the sample**

```shell
wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar
tar xf mogdb-operator-examples.tar
cd mogdb-operator-examples
```

**Step 2: Install MogDB Operator**

```shell
kustomize build ./kustomize/install/default | kubectl apply -f -
```

The desired output is as follows.

```shell
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
secret/mogdb-operator-huawei-registry created
deployment.apps/mogdb-operator-controller-manager created
```

<br/>

## Deploying MogDB Cluster

```shell
kubectl apply -f https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-cluster.yaml
```

The desired output is as follows.

```text
mogdbcluster.mogdb.enmotech.io/cluster1 created
```

### Check pod status

```shell
kubectl get pods -n mogdb-operator-system
```

The desired output is as follows.

```shell
NAME                          READY   STATUS            RESTARTS   AGE
cluster1-jl2kn                3/3     Running           0          4m18s
cluster1-zuveg                0/3     PodInitializing   0          108s
```

Wait for all pods to be in running state, then proceed to the next step and connect to the MogDB cluster.

<br/>

## Connecting to MogDB Cluster

### Install gsql command line tool

To connect to the MogDB cluster, you need to install the MogDB client tool [gsql](../../docs-mogdb/v3.0/quick-start/mogdb-access/use-cli-to-access-mogdb/gsql.md#installing-the-gsql-client) on the host machine using **kubectl** to install the command line tool that matches the physical machine.

<br/>

### Check the MogDB service port

First, forward the port from the local host to the MogDB Service in Kubernetes. We start by getting a list of services in the mogdb-operator-system namespace.

```shell
kubectl get svc -n mogdb-operator-system
```

The desired output is as follows.

```text
NAME                       TYPE       CLUSTER-IP     EXTERNAL-IP        PORT(S)          AGE
cluster1-svc-master        NodePort   10.1.115.245   <none>       5432:30013/TCP   38m
cluster1-svc-replicas      NodePort   10.1.41.187    <none>       5432:30012/TCP   38m
```

The output shows that the actual MogDB cluster is accessed through port 5432 inside the k8s cluster and through port 30013 outside the cluster.

<br/>

### Connect to the cluster

Before connecting, you need to change the password of the default user. You can change the password by connecting to the container with the **kubectl** command.

```shell
kubectl exec -it cluster1-xxx -c ha-sidecar -- /bin/bash
gsql -dpostgres
```

```sql
ALTER USER mogdb WITH PASSWORD 'new_password';
```

```shell
gsql -h 10.1.115.245 -p 5432 -U mogdb -W
```

The desired output is as follows.

```shell
gsql ((MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

The following are some of the commands that can be used to verify cluster functionality.

- Check out the MogDB version

```sql
MogDB=#select version();
                                                                      version
---------------------------------------------------------------------------------------------------------------------------------------------------
 (MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr   on aarch64-unknown-linux-gnu, compiled by g++ (GCC) 7.3.0, 64-bit
(1 row)
```

- Create table t1

```sql
MogDB=#create table t1(id int);
CREATE TABLE
```

- Insert data

```sql
MogDB=#insert into t1(id) values(1),(2),(3);
INSERT 0 3
```

- View the data

```sql
MogDB=#select * from t1;
 id
----
  1
  2
  3
(3 rows)
```

<br/>

## Destroying MogDB Cluster

After completing the test, you may wish to destroy the MogDB cluster.

### Delete MogDB Cluster

```shell
kubectl delete mogdbcluster cluster1
```

The desired output is as follows.

```text
mogdbcluster.mogdb.enmotech.io "cluster1" deleted
```

<br/>

## Uninstall MogDB Operator

```shell
kustomize build ./kustomize/install/default | kubectl delete -f -
```

The desired output is as follows.

```shell
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-apiserver created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
configmap/mogdb-operator-mgo-config created
secret/mgorole-admin created
secret/mgouser-admin created
secret/mogdb-operator-huawei-registry created
service/mogdb-apiserver created
deployment.apps/mogdb-apiserver created
deployment.apps/mogdb-operator-controller-manager created
```
