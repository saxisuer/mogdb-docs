---
title: Client
summary: Client
author: Wang Dong
date: 2022-06-30
---

# mgo Client

MogDB Operator Client, also known as mgo, is the easiest way to interact with the MogDB Operator. mgo provides many convenient ways to create, manage and delete MogDB clusters through a series of simple commands.
MogDB Operator provides an interface to connect to the mgo client and manage authentication through RBAC and TLS.

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/single-idc-operator-arch.png)

The mgo client is available for Linux, macOS, and you can select your desired mgo client from [Release Note](../release.md#v100) to select the mgo client you need.

<br/>

## General Considerations for Using mgo

If you use the [Quick Start](../quick-start.md) guide to install MogDB Operator, you will install MogDB Operator under the namespace named **mogdb-operator-system**.

For convenience, we recommend setting **mogdb-operator-system** to the value of the environment variable **MGO_NAMESPACE**. In the shell where you will execute the mgo command, run the following command.

```shell
export MGO_NAMESPACE=mogdb-operator-system
```

If you do not want to set this environment variable, or are in an environment where environment variables are not available, you must use the **--namespace(or -n)** flag for most commands, for example

```shell
mgo version -n mogdb-operator-system
```

<br/>

## Syntax

While the syntax of mgo is as simple as if you were using **kubectl**, one of the goals of the MogDB Operator project is to allow seamless management of MogDB clusters in Kubernetes-enabled environments, and to make the learning curve easier by following a command pattern familiar to users.

To find out what is available at the top level of the mgo command, execute the following command.

```shell
mgo
```

The syntax of the mgo command usually follows the following format.

```shell
pgo [command] ([TYPE] [NAME]) [flags]
```

Where **command** is a verb, e.g.

- create
- show
- delete

**TYPE** is a resource type, e.g.

- cluster
- mgorole
- mgouser

**NAME** is the name of the resource type, e.g.

- cluster1
- admin

Some global flags apply to each mgo command, and some special flags apply to specific mgo commands. To get a list of all options and flags available for a command, you can use the **--help** flag. For example, to see all options available for the mgo create cluster command, you can run the following command.

```shell
mgo create cluster --help
```

<br/>

## Commands Overview

The following table summarizes the commands provided by the mgo client.

| Command  | Syntax                           | Description                                      |
|-------------|--------------------------------------|-----------------------------------------------------|
| create      | mgo create cluster cluster1          | Create Operator resource types (e.g. cluster, mgouser, mgorole) |
| delete      | mgo delete cluster cluster1          | Delete Operator resource types (e.g. cluster, mgouser, mgorole) |
| update      | mgo update cluster cluster1 --cpu=1  | Update MogDB Cluster, mgouser, mgorole |
| show        | mgo show cluster cluster1            | Show Operator resource types (e.g. cluster, mgouser, mgorole) |
| scale       | mgo scale cluster1                   | Create a copy of MogDB for a given MogDB cluster |
| scaledown   | mgo scaledown cluster1 --query       | Delete a copy from the MogDB cluster |
| version     | mgo version                          | Show Operator version information |

<br/>

## Global flag

Global flags that apply to all mgo commands.

**Note:** Flags takes precedence over environment variables.

| flag         | Description                                                                           |
|---------------------------|-----------------------------------------------------------------------------|
| --apiserver-url string    | The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/" |
| --debug                   | Enable debug output for debugging                       |
| --disable-tls             | Disable TLS authentication for MogDB Operator |
| --exclude-os-trust        | Exclude CA certificates from the OS default trust store |
| -h, --help                | Help information for mgo                                     |
| --mgo-ca-cert string      | Connection to MogDB Operator apiserver CA certificate file path. |
| --mgo-client-cert string  | Path to the client certificate file used to authenticate to the MogDB Operator apiserver |
| --mgo-client-key string   | Path to the client key file used to authenticate to the MogDB Operator apiserver |
| -n, --namespace string    | Namespace for mgo requests                                |