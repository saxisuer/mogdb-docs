---
title: Recovery Management
summary: Recovery Management
author: Ji Yabin
date: 2022-06-16
---

# Recovery Management

MogDB Operator restores the database to the specified point in time by finding the appropriate backup data on the s3 storage.

You can restore the specified database by setting the following expectation values:

```yaml
kind: MogDBRecovery
metadata:
  name: sample1-recovery
spec:
  # Add fields here
  clusterName: sample1
  recoveryTimePoint: 2022-02-28 07:15:59
```

<br/>

## Architecture

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/recovery-management.png)
