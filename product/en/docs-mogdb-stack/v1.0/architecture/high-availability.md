---
title: High Availability
summary: High Availability
author: Ji Yabin
date: 2022-06-16
---

# High Availability

High availability is a functional component in MogDB Stack system, running in sidecar mode in the pod where MogDB is located, monitoring the operation status of MogDB in real time, and triggering the switchover logic when the primary becomes unavailable.

The scenarios of switchover are as follows:

- Database disk failure or some hardware failure
- The network of primary node is unreachable
- Database failure and downtime
- Rack power down

<br/>

## Features

- Ensure the high availability of HA itself through dcs
- HA has leader and follower roles, and the leader has decision making power
- Repair the replication status of MogDB cluster in real time
- Maintain the roles of MogDB

<br/>

## Architecture

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/ha.png)
