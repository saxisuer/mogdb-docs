---
title: Backup Management
summary: Backup Management
author: Ji Yabin
date: 2022-06-16
---

# Backup Management

MogDB Operator uses MogDB's native **dump** command to perform backups, so the backup content is in the form of a fungible sql, which has the advantage that it can be restored across versions.

<br/>

## Backup CR General Fields Introduction

To back up a MogDB cluster on Kubernetes, users can create a custom backup Custom Resource (CR) object to describe a backup, and the following describes the specific meaning of each field of the Backup CR.

**For the exact meaning, please refer to [CRD References](../references/server/crd-references.md) section.**

<br/>

## Backup Configuration

When you use MogDB Operator, you can complete the configuration function of backup by the following expectation settings.

```yaml
backupSpec:
  backupSchedule: "0 */1 * * * ?"
```

Another thing to note is to modify the relevant rclone configuration in the MogDB Operator installation package.

Specific path:

```text
./kustomize/install/manager/configmap.yaml
```

Modified content:

```text
[backup]
type = s3
provider = Minio
env_auth = false
access_key_id = root   // Connecting user
secret_access_key = root@123   // Connection password
endpoint = http://10.105.68.26:9000  // Service address of s3
```

<br/>

## Architecture

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/backup-management.png)
