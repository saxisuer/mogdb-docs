---
title: mgo create cluster
summary: mgo创建集群
author: Ji Yabin
date: 2021-07-09
---

# Create MogDB Cluster

## Overview

Create a MogDB cluster consisting of a master node and several slave nodes in the background, for example:

```shell
mgo create cluster mycluster
```

```shell
mgo create cluster [flags]
```

<br/>

## Options

```text
      --access-modes int             Set the access method for the data volume. 1: ReadWriteOnce, 2: ReadOnlyMany, 3: ReadWriteMany. Default value is ReadWriteOnce
      --backup-storage-size string   Set the backup data volume size. Default value is 128Mi
      --cluster-role string          The role in clusters, There are only three legal roles: primary, standby, cascade
      --cpu string                   Set the number of CPU cores to request, e.g. "100m" or "0.1"
      --cpu-limit string             Set the number of cores to be limited for the CPU, e.g. "100m" or "0.1"
      --data-storage-size string     Set the database data volume size. The default value is 128Mi
      --exporter-image string        The image that will be used for the MogDB exporter sidecar container. If specified, this will replace the default value
      --ha-image string              The image that will be used for the MogDB ha sidecar container. If specified, this will replace the default value
  -h, --help                         help for cluster
      --image string                 The image that will be used for the MogDB server container. If specified, this will replace the default value
      --init-image string            The image that will be used to initialize the container for MogDB. If specified, this will replace the default value
      --log-storage-size string      Set the log data volume size. Default value is 128Mi
      --memory string                Set the amount of memory to request, e.g. 1GiB
      --memory-limit string          Set the amount of memory to be limited, e.g. 1GiB
      --read-port int                Set the read port of the service
      --replicas int                 Set the number of slave nodes to create the cluster, the default value is 2
      --sidecar-image string         The image that will be used for the MogDB sidecar container. If specified, will replace the default value
      --storage-class string         Set the name of the required StorageClass to be declared. The default value is local-path
      --write-port int               Set the service write port
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo create](mgo-create.md)  - Create MogDB Operator Resources