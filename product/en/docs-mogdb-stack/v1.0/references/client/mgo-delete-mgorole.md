---
title: mgo delete mgorole
desription: mgo 删除角色
author: Ji Yabin
date: 2022-06-30
---

# Delete mgo Permission Roles

## Overview

Delete a specified role or all roles, e.g.

```shell
mgo delete mgorole somerole
```

```shell
mgo delete mgorole [flags]
```

<br/>

## Options

```text
      --all         Delete all mgo permission roles
  -h, --help        Help information for delete mgorole
      --no-prompt   No command line confirmation prompt before deleting
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo delete](mgo-delete.md)  - Delete MogDB Operator Resources