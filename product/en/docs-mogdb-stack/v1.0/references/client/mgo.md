---
title: mgo 
desription: mgo 命令行工具参考
author: Wang Dong
date: 2022-06-30
---

# mgo Commands

## Overview

The mgo command line tool helps you create and manage MogDB clusters.

<br/>

## Options

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo version](mgo-version.md) - Print MogDB Operator version information
* [mgo show](mgo-show.md) - Show MogDB Operator resources
* [mgo show cluster](mgo-show-cluster.md) - Show MogDB Operator cluster
* [mgo show mgorole](mgo-show-mgorole.md) - Show MogDB Operator roles
* [mgo show mgouser](mgo-show-mgouser.md) - Show MogDB Operator users
* [mgo create](mgo-create.md) - Create MogDB Operator Resources
* [mgo create cluster](mgo-create-cluster.md) - Create MogDB Operator cluster
* [mgo create mgorole](mgo-create-mgorole.md) - Create MogDB Operator roles
* [mgo create mgouser](mgo-create-mgouser.md) - Create MogDB Operator users
* [mgo update](mgo-update.md) - Update MogDB Operator resources
* [mgo update cluster](mgo-update-cluster.md) - Update MogDB Operator clusters
* [mgo update mgorole](mgo-update-mgorole.md) - Update MogDB Operator roles
* [mgo update mgouser](mgo-update-mgouser.md) - Update MogDB Operator users
* [mgo delete](mgo-delete.md) - Delete MogDB Operator resources
* [mgo delete cluster](mgo-delete-cluster.md) - Delete MogDB Operator cluster
* [mgo delete mgorole](mgo-delete-mgorole.md) - Delete MogDB Operator roles
* [mgo delete mgouser](mgo-create-mgouser.md) - Delete MogDB Operator users
* [mgo scale](mgo-scale.md) - Scale Up MogDB Clusters
* [mgo scaledown](mgo-scaledown.md) - Scale down MogDB clusters
* [mgo switch](mgo-switch.md) - Switch MogDB Operator users