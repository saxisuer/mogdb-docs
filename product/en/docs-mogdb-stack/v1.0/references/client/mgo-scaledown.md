---
title: mgo-scaledown
desription: mgo 缩容集群命令
author: Wang Dong
date: 2022-06-30
---

# Scale Down MogDB Clusters

## Overview

The scaledown command allows you to shrink the replica configuration of a cluster. For example:

List the target sub-nodes.

 ```shell
 mgo scaledown mycluster --query
 ```

Scale down specific sub-nodes.

 ```shell
 mgo scaledown mycluster --target=mycluster-replica-xxxx
 ```

```shell
mgo scaledown [flags]
```

<br/>

## Options

```text
  -h, --help            Help information for scaledown
      --no-prompt       No command line confirmation prompt
      --query           Print the target replica candidate list
      --target string   Scale down comma-separated list of sub-nodes
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```