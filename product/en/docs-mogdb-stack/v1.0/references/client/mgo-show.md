---
title: mgo-show
desription: mgo 展示命令
author: Wang Dong
date: 2022-06-30
---

# Show MogDB Operator resources

## Overview

The Show command allows you to display details about a cluster, mgorole, or mgouser. For example:

```shell
mgo show cluster mycluster
mgo show mgouser someuser
mgo show mgorole somerole
```

```shell
mgo show [flags]
```

<br/>

## Options

```text
  -h, --help   Help information for show
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo](mgo.md) - mgo command line tool
* [mgo show cluster](mgo-show-cluster.md) - Show MogDB Operator cluster
* [mgo show mgorole](mgo-show-mgorole.md) - Show MogDB Operator roles
* [mgo show mgouser](mgo-show-mgouser.md) - Show MogDB Operator users