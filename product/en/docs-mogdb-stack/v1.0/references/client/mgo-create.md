---
title: mgo-create
desription: mgo 创建命令
author: Wang Dong
date: 2022-06-30
---

## Create MogDB Operator resource

### Overview

The CREATE command allows you to create a new Operator resource. For example:

```shell
mgo create cluster
mgo create mgouser
mgo create mgorole
```

```shell
mgo create [flags]
```

<br/>

### Options

```text
  -h, --help   Helpful information about create
```

<br/>

### Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

### Helpful Links

* [mgo](mgo.md)  - mgo command line tool
* [mgo create cluster](mgo-create-cluster.md)  - Create a MogDB Cluster
* [mgo create mgorole](mgo-create-mgorole.md)  - Create mgo permission roles
* [mgo create mgouser](mgo-create-mgouser.md)  - Create mgo user