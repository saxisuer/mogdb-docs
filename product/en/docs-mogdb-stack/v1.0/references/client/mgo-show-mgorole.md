---
title: mgo show mgorole
summary: mgo查看角色信息
author: Ji Yabin
date: 2021-07-09
---

# Show MogDB Operator roles

## Overview

Show information about the specified mgo role or all roles. For example:

```shell
mgo show mgorole somerole
```

```shell
mgo show mgorole [flags]
```

<br/>

## Options

```text
      --all    Show all mgo role information
  -h, --help   Help information for show mgorole
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo show](mgo-show.md) - Show MogDB Operator resources