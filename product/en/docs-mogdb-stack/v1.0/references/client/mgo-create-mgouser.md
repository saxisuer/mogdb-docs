---
title: mgo create mgouser
summary: mgo创建用户
author: Ji Yabin
date: 2021-07-09
---

# Create mgo User

## Overview

Create an mgo user. For example:

```shell
mgo create mgouser someuser
```

```shell
mgo create mgouser [flags]
```

<br/>

## Options

```text
      --all-namespaces               Specify that this user will have access to all namespaces
  -h, --help                         help for mgouser
      --mgouser-namespaces string   Specify a comma-separated list of namespaces for mgouser
      --mgouser-password string     Specify the password for mgouser
      --mgouser-roles string        Specify a comma-separated list of roles for mgouser
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo create](mgo-create.md)  - Create MogDB Operator Resources