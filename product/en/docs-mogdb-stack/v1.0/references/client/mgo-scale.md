---
title: mgo-scale
desription: mgo 扩容集群命令
author: Wang Dong
date: 2022-06-30
---

# Scale Up MogDB Clusters

## Overview

The scale command allows you to add a replica configuration of the cluster. For example:

```shell
mgo scale mycluster --replica-count=1
```

```shell
mgo scale [flags]
```

<br/>

## Options

```text
  -h, --help                Help information for scale
      --no-prompt           No command line confirmation prompt
      --replica-count int   The count of replicas to add to the cluster. (default value is 1)
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```