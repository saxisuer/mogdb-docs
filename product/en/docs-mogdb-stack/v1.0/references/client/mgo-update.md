---
title: mgo-update
desription: mgo 修改命令
author: Wang Dong
date: 2022-06-30
---

# Update MogDB Operator resources

## Overview

The update command allows you to update mgouser, mgorole or cluster. e.g.

```shell
mgo update cluster --selector=name=mycluster --disable-autofail
mgo update cluster --all --enable-autofail
mgo update mgorole somerole --mgorole-permission="version"
mgo update mgouser someuser --mgouser-password=somenewpassword
mgo update mgouser someuser --mgouser-roles="role1,role2"
mgo update mgouser someuser --mgouser-namespaces="mgouser2"
```

```shell
mgo update [flags]
```

<br/>

## Options

```text
  -h, --help   Helpful information about update
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo](mgo.md) - mgo command line tool
* [mgo update cluster](mgo-update-cluster.md) - Update MogDB Operator clusters
* [mgo update mgorole](mgo-update-mgorole.md) - Update MogDB Operator roles
* [mgo update mgouser](mgo-update-mgouser.md) - Update MogDB Operator users