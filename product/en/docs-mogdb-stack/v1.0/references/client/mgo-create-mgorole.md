---
title: mgo create mgorole
summary: mgo创建角色
author: Ji Yabin
date: 2021-07-09
---

# Create mgo Permission Roles

## Overview

Create the mgo permission role. For example:

```shell
mgo create mgorole somerole --permissions="Cat,Ls"
```

```shell
mgo create mgorole [flags]
```

<br/>

## Options

```text
  -h, --help                 help for mgorole
      --permissions string   List of permissions for the specified mgo permission role, separated by commas
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo create](mgo-create.md)  - Create MogDB Operator Resources