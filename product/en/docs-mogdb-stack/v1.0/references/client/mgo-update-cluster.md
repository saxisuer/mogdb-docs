---
title: mgo update cluster
desription: mgo 修改集群
author: Ji Yabin
date: 2022-06-30
---

# Update MogDB Operator Clusters

## Overview

Update MogDB clusters. For example:

```shell
mgo update cluster mycluster --image=xxx
mgo update cluster mycluster myothercluster --image=xxx
mgo update cluster --all --image=xxx
```

```shell
mgo update cluster [flags]
```

<br/>

## Options

```text
      --access-modes int             Sets the access method for data volumes. 1: ReadWriteOnce, 2: ReadOnlyMany, 3: ReadWriteMany The default value is ReadWriteOnce
      --all                          Update all MogDB clusters
      --backup-storage-size string   Sets the backup data volume size. Default value 128Mi
      --cpu string                   Set the number of CPU cores to request. e.g. "100m" or "0.1"
      --cpu-limit string             Set the number of cores to be limited for the CPU. e.g. "100m" or "0.1"
      --data-storage-size string     Sets the database data volume size. Default value 128Mi
      --exporter-image string        The image that will be used for the MogDB exporter sidecar container. If specified, this will replace the default value
      --ha-image string              The image that will be used for the MogDB HA sidecar container. If specified, this will replace the default value
  -h, --help                         Help information for update cluster
      --image string                 The image that will be used for the MogDB server container. If specified, this will replace the default value
      --init-image string            The image that will be used to initialize the container for MogDB. If specified, this will replace the default value
      --log-storage-size string      Sets the log data volume size. Default value 128Mi
      --memory string                Set the amount of memory to request. e.g. 1GiB
      --memory-limit string          Set the amount of memory to be restricted. e.g. 1GiB
      --read-port int                Set the read port of the service
      --sidecar-image string         The image that will be used for the MogDB sidecar container. If specified, will replace the default value
      --storage-class string         Sets the name of the StorageClass required for the declaration. The default value is local-path
      --write-port int               Set the service's write port
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo update](mgo-update.md) - Update MogDB Operator resources