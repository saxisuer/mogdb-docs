---
title: mgo-delete
desription: mgo 删除命令
author: Wang Dong
date: 2022-06-30
---

# Delete MogDB Operator Resource

## Overview

The delete command allows you to delete a MogDB Operator resource. For example:

```shell
mgo delete cluster mycluster
mgo delete mgouser someuser
mgo delete mgorole somerole
mgo delete user --username=testuser --selector=name=mycluster
```

```shell
mgo delete [flags]
```

<br/>

## Options

```text
  -h, --help   help information for delete
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo](mgo.md)  - mgo command line tool
* [mgo delete cluster](mgo-delete-cluster.md)  - Delete MogDB clusters
* [mgo delete mgorole](mgo-delete-mgorole.md)  - Delete mgo permission roles
* [mgo delete mgouser](mgo-delete-mgouser.md)  - Delete mgo users