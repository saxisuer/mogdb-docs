---
title: mgo delete mgouser
desription: mgo 删除用户
author: Ji Yabin
date: 2022-06-30
---

# Delete mgo Users

## Overview

Delete the specified mgo user or all users. For example:

```shell
mgo delete mgouser someuser
```

```shell
mgo delete mgouser [flags]
```

<br/>

## Options

```text
      --all         Delete all mgo users
  -h, --help        Help information for delete mgouser
      --no-prompt   No command line confirmation prompt before deleting
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo delete](mgo-delete.md)  - Delete MogDB Operator Resource