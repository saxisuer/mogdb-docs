---
title: mgo update mgorole
desription: mgo 修改角色
author: Ji Yabin
date: 2022-06-30
---

# Update MogDB Operator roles

## Overview

The update mgorole command allows to update the mgo roles. For example:

```shell
mgo update mgorole somerole  --permissions="version,show,create"
```

```shell
mgo update mgorole [flags]
```

<br/>

## Options

```text
  -h, --help                 Help information for update mgorole
      --no-prompt            No command line confirmation prompt
      --permissions string   Specify a comma-separated list of permissions for mgorole
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```

<br/>

## Helpful Links

* [mgo update](mgo-update.md) - Update MogDB Operator resources