---
title: mgo-version
desription: mgo 版本命令
author: Wang Dong
date: 2022-06-30
---

# Print MogDB Operator Version Information

## Overview

The version command allows you to print the version information of the MogDB Operator. For example:

```shell
mgo version
```

```shell
mgo version [flags]
```

<br/>

## Options

```text
  -h, --help   Helpful information about version
```

<br/>

## Options Inherited From The Parent Command

```text
      --apiserver-url string      The URL of the mogDB Operator apiserver that will handle requests from the mgo client. please note that the URL should not end in "/"
      --debug                     Enable debug output for debugging
      --disable-tls               Disable TLS authentication for MogDB Operator
      --exclude-os-trust          Exclude CA certificates from the OS default trust store
      --mgo-ca-cert string       Connection to MogDB Operator apiserver CA certificate file path.
      --mgo-client-cert string   Path to the client certificate file used to authenticate to the MogDB Operator apiserver
      --mgo-client-key string    Path to the client key file used to authenticate to the MogDB Operator apiserver
  -n, --namespace string          Namespace for mgo requests
```