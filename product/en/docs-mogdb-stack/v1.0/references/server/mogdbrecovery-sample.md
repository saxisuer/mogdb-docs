---
title: MogDB Recovery CR Sample
summary: MogDB Recovery CR Sample
author: Ji Yabin
date: 2022-06-16
---

# MogDB Recovery CR Sample

If you wish to manually create a MogDB Recovery CR via the kubectl command to complete a backup, you can do so by writing a simple yaml file and submitting it to the k8s system, such as :

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbRecovery
metadata:
  name: sample1-003
spec:
  # Add fields here
  clusterName: cluster1
  recoveryTimePoint: "2022-07-06 12:47:59"
```

The above configuration specifies the cluster to be recovered and the point-in-time for recovery.