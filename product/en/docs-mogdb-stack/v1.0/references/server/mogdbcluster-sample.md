---
title: MogDB Cluster CR Sample
summary: MogDB Cluster CR Sample
author: Ji Yabin
date: 2022-06-16
---

# MogDB Cluster CR Sample

If you wish to create a MogDB Cluster CR via the native kubectl command, you can do so by writing a simple yaml file and submitting it to the k8s system, such as :

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbCluster
metadata:
  name: cluster1
  namespace: mogdb-operator-system
spec:
  # Add fields here
  podSpec:
    volumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    logVolumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    backupVolumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    initImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-init:v3.0.0
    sidecarImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-sidecar:v3.0.0
    exporterImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-exporter:v3.0.0
    sidecarHaImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-ha:v3.0.0
  readPort: 30012
  writePort: 30013
  replicas: 2
  backupSpec:
    backupSchedule: "0 */1 * * * ?"
    jobSpec:
      image: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-sidecar:v3.0.0
```

The above configuration, which specifies the data volume, log volume, backup volume and basic image configuration, as well as the number of replicas, will create a MogDB cluster when submitted to the system.