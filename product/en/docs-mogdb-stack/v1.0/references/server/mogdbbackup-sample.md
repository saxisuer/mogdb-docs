---
title: MogDB Backup CR Sample
summary: MogDB Backup CR Sample
author: Ji Yabin
date: 2022-06-16
---

# MogDB Backup CR Sample

If you wish to create a MogDB Backup CR via the native kubectl command, you can do so by writing a simple yaml file and submitting it to the k8s system, such as :

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbBackup
metadata:
  name: sample1-002
  labels:
    cluster: sample1
spec:
  # Add fields here
  clusterName: cluster1
  dbName: db1
```

The above configuration, specifies the clusters to be backed up, and the specific database.