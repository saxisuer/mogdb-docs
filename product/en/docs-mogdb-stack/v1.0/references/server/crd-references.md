---
title: CRD References
summary: CRD References
author: Ji Yabin
date: 2022-06-16
---

# CRD References

## Resource Types

+ MogdbCluster
+ MogdbBackup
+ MogdbRecovery

<br/>

## MogdbCluster

MogdbCluster is a simple summary of the MogdbCluster API

| Name      | Type | Description                | Required option |
|------------|--------|------------------------------|----------|
| apiVersion | string | mogdb.enmotech.io/v1         |  true    |
| kind       | string | MogdbCluster                 |  true    |
| metadata   | object | Related fields refer to the Kubernetes API documentation |  true |
| spec       | object | The desired state of MogdbCluster |  false   |
| status     | object | The state observed by MogdbCluster |  false   |

<br/>

### MogdbCluster.spec

MogdbClusterSpec defines the desired state of MogdbCluster

| Name         | Type    | Description                                   | Required option |
|----------------|---------|------------------------------|----------|
| replicas       | integer | The number of replicas expected by MogdbCluster |  true  |
| readPort       | integer | Read-only service port |  true  |
| writePort      | integer | Read and write service port |  true  |
| postgresConf   | object  | MogDB configuration file configuration items |  false |
| podSpec        | object  | Desired state of the cluster pod |  false |
| backupSpec     | object  | Desired backup state | false  |

<br/>

### MogdbCluster.spec.podSpec

| Name               | Type              | Description                                 | Required option |
| ------------------ | ----------------- | ------------------------------------------- | --------------- |
| mogdbVersion       | string            | MogDB database version                      | false           |
| image              | string            | MogDB image                                 | false           |
| sidecarImage       | string            | Sidecar image                               | false           |
| initImage          | string            | Init image                                  | false           |
| volumeSpec         | object            | Volume type configuration                   | false           |
| logVolumeSpec      | object            | Log volume type configuration               | false           |
| backupVolumeSpec   | object            | Backup volume type configuration            | false           |
| imagePullPolicy    | string            | Image pulling strategy                      | false           |
| imagePullSecrets   | []string          | Image pull secrets                          | false           |
| labels             | map[string]string | Custom labels                               | false           |
| annotations        | map[string]string | Custom Comments                             | false           |
| resources          | object            | Resource Limits                             | false           |
| affinity           | object            | Affinity setting                            | false           |
| nodeSelector       | map[string]string | Node selector                               | false           |
| priorityClassName  | string            | Priority class name setting                 | false           |
| tolerations        | []object          | Tolerance setting                           | false           |
| serviceAccountName | string            | Service account name                        | false           |
| volumes            | []object          | Additional volume settings                  | false           |
| volumeMounts       | []object          | volume mount setting                        | false           |
| initContainers     | []object          | Additional initialization container setting | false           |
| containers         | []object          | Additional container setting                | false           |
| sidecarResources   | object            | sidecar container resource limits           | false           |

<br/>

### MogdbCluster.spec.podSpec.volumeSpec

| Name                  | Type   | Description             | Required option |
| --------------------- | ------ | ----------------------- | --------------- |
| emptyDir              | object | Volume of type emptyDir | false           |
| hostPath              | object | Volume of type hostPath | false           |
| persistentVolumeClaim | object | pvc volume statement    | false           |

<br/>

### MogdbCluster.spec.backupSpec

| Name                           | Type     | Description                                | Required option |
| ------------------------------ | -------- | ------------------------------------------ | --------------- |
| backupSchedule                 | string   | Backup schedule cron setting               | false           |
| backupScheduleJobsHistoryLimit | integer  | Number of days of backup history retention | false           |
| backupURL                      | string   | Backup remote storage path                 | false           |
| backupSecretName               | string   | Remote storage of secrets                  | false           |
| backupCompressCommand          | []string | Backup compression command                 | false           |
| backupDecompressCommand        | []string | Backup decompress command                  | false           |
| rcloneExtraArgs                | []string | Extra arguments for the rclone command     | false           |
| gsAllDumpExtraArgs             | []string | Extra arguments for the gs_alldump command | false           |
| gsqlExtraArgs                  | []string | Extra arguments for the gsql command       | false           |
| jobSpec                        | object   | Job Desired State                          | false           |

<br/>

### MogdbCluster.spec.backupSpec.jobSpec

| Name               | Type              | Description          | Required option |
| ------------------ | ----------------- | -------------------- | --------------- |
| image              | string            | Job image            | false           |
| imagePullPolicy    | string            | Image pull policy    | false           |
| imagePullSecrets   | []string          | Image pull secrets   | false           |
| serviceAccountName | string            | Service Account Name | false           |
| affinity           | object            | Affinity             | false           |
| nodeSelector       | map[string]string | Node Selector        | false           |
| priorityClassName  | string            | Priority class name  | false           |
| tolerations        | []object          | Tolerance setting    | false           |
| labels             | map[string]string | Custom labels        | false           |

<br/>

### MogdbCluster.status

| Name       | Type     | Description                     | Required option |
| ---------- | -------- | ------------------------------- | --------------- |
| readyNodes | integer  | Number of nodes in ready status | true            |
| conditions | []object | Cluster condition               | true            |
| nodes      | []object | Node condition                  | true            |

<br/>

### MogdbCluster.status.conditions

| Name               | Type   | Description                  | Required option |
| ------------------ | ------ | ---------------------------- | --------------- |
| type               | string | Condition type               | true            |
| status             | string | Status of the condition      | true            |
| lastTransitionTime | time   | When the condition occurred  | true            |
| reason             | string | The reason for the condition | true            |
| message            | string | Condition message            | true            |

<br/>

### MogdbCluster.status.nodes

| Name       | Type     | Description                    | Required option |
| ---------- | -------- | ------------------------------ | --------------- |
| name       | string   | Name of the cluster node state | true            |
| conditions | []object | Node condition collection      | true            |

<br/>

### MogdbCluster.status.nodes.conditions

| Name               | Type   | Description                 | Required option |
| ------------------ | ------ | --------------------------- | --------------- |
| type               | string | Condition type              | true            |
| status             | string | Status of the condition     | true            |
| lastTransitionTime | time   | When the condition occurred | true            |

<br/>

## MogdbBackup

MogdbBackup is a simple summary of the MogdbBackup API

| Name       | Type   | Description                                              | Required option |
| ---------- | ------ | -------------------------------------------------------- | --------------- |
| apiVersion | string | mogdb.enmotech.io/v1                                     | true            |
| kind       | string | MogdbBackup                                              | true            |
| metadata   | object | Related fields refer to the Kubernetes API documentation | true            |
| spec       | object | Desired state of MogdbBackup                             | false           |
| status     | object | The state observed by MogdbBackup                        | false           |

<br/>

### MogdbBackup.spec

| Name                           | Type     | Description                               | Required option |
| ------------------------------ | -------- | ----------------------------------------- | --------------- |
| clusterName                    | string   | MogDB cluster name                        | true            |
| backupScheduleJobsHistoryLimit | integer  | Days of backup history retention          | false           |
| backupURL                      | string   | Path for backup to save in remote storage | false           |
| backupSecretName               | string   | Remotely stored secrets                   | false           |
| remoteDeletePolicy             | string   | Backup history deletion policy            | false           |
| dbName                         | string   | Backup database name                      | false           |
| tbNames                        | []string | List of backed up table names             | false           |

<br/>

### MogdbBackup.status

| Name               | Type     | Description                    | Required option |
| ------------------ | -------- | ------------------------------ | --------------- |
| completed          | bool     | Whether the backup is complete | true            |
| completedTimestamp | time     | Backup completion time         | true            |
| conditions         | []object | Backup Status List             | true            |

<br/>

### MogdbBackup.status.conditions

| Name               | Type   | Description                 | Required option |
| ------------------ | ------ | --------------------------- | --------------- |
| type               | string | Condition type              | true            |
| status             | string | Status of the condition     | true            |
| lastTransitionTime | time   | When the condition occurred | true            |
| reason             | string | Reasons for the condition   | true            |
| message            | string | Condition details           | true            |

<br/>

## MogdbRecovery

MogdbRecovery is a simple summary of the MogdbRecovery API

| Name       | Type   | Description                                              | Required option |
| ---------- | ------ | -------------------------------------------------------- | --------------- |
| apiVersion | string | mogdb.enmotech.io/v1                                     | true            |
| kind       | string | MogdbRecovery                                            | true            |
| metadata   | object | Related fields refer to the Kubernetes API documentation | true            |
| spec       | object | The desired state of MogdbRecovery                       | false           |
| status     | object | The state observed by MogdbRecovery                      | false           |

<br/>

### MogdbRecovery.spec

| Name              | Type   | Description                    | Required option |
| ----------------- | ------ | ------------------------------ | --------------- |
| clusterName       | string | MogDB cluster name             | true            |
| recoveryTimePoint | string | Time point for backup recovery | true            |
| backupSecretName  | string | Remote storage of secrets      | true            |

<br/>

### MogdbRecovery.status

| Name       | Type     | Description                      | Required option |
| ---------- | -------- | -------------------------------- | --------------- |
| completed  | bool     | Whether the recovery is complete | true            |
| conditions | []object | Recovery condition list          | true            |

<br/>

### MogdbRecovery.status.conditions

| Name               | Type   | Description                 | Required option |
| ------------------ | ------ | --------------------------- | --------------- |
| type               | string | Condition type              | true            |
| status             | string | Status of the condition     | true            |
| lastTransitionTime | time   | When the condition occurred | true            |
| reason             | string | Reasons for the condition   | true            |
| message            | string | Condition details           | true            |