---
title: Overview of MogDB Stack
summary: Overview of MogDB Stack
author: Ji Yabin
date: 2021-06-16
---

# Overview of MogDB Stack

MogDB Stack is an automatic operation and maintenance system for MogDB cluster on Kubernetes, providing full lifecycle management of MogDB including deployment, high availability, scaling, monitoring, backup recovery. With MogDB Stack, MogDB can run seamlessly on public cloud or private deployment of Kubernetes cluster.

<br/>

## MogDB Stack Features

- **Easy and convenient automatic deployment/recovery**

  With Kubernetes as the native container orchestration system, you can easily and conveniently create a full set of MogDB clusters by scaling resources, and expand and contract at will according to user expectations.

- **Stable and reliable backup recovery capability**

  Support SQL-based backup method and stored in remote distributed storage, multiple replicas to ensure the reliability of backup, specify point-in-time recovery method to restore to the specified point in time.

- **Enterprise-level high availability**

  Automatic fault detection, switching, and automated pulling of data snapshots to restore failed nodes or rescheduling of new nodes to achieve self-healing capability of the system and ensure the number of replicas expected by users.

- **Complete and reliable monitoring capability**

  Prometheus-based unified multi-dimensional monitoring, including a complete set of monitoring metrics for the system, container and database layers.

- **Fine-grained resource control**

  For CPU, memory, storage and other resource limits to ensure the independence of the container layer, will not interfere with each other, support the affinity and anti-affinity scheduling of containers.

<br/>

## System Architecture

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/overview-arch.png)

<br/>

## OS & Platform Support

MogDB Stack currently supports running on the following operating systems and platform architectures.

1. Linux x86_64
2. Linux arm64