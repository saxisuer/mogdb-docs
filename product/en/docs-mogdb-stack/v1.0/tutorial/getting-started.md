---
title: Getting Started
summary: Getting Started
author: Wang Dong
date: 2022-06-30
---

# Getting Started

## Install

If you have not installed MogDB operator and mgo, please refer to [Quick Start](../quick-start.md) to install MogDB operator and [install mgo-client](../installation/kustomize.md#install-the-mgo-client).

<br/>

## Set up mgo client

The MogDB Operator and mgo client are designed to work in a multiple namespace deployment environment, and many mgo commands require that namespace flags ( -n) be passed to them. You can use the MGO_NAMESPACE environment variable to set the namespaces that pgo commands can use. For example.

```shell
export MGO_NAMESPACE=mogdb-operator-system
mgo show cluster --all
```

This will show all MogDB clusters deployed to the mogdb-operator-system namespace. This is equivalent to:

```shell
mgo show cluster -n mogdb-operator-system --all
```

(Note: **-n** takes precedence over **MGO_NAMESPACE**.)

<br/>

## Next Step

[mgo version](../references/client/mgo-version.md) command is a good way to check the connectivity of MogDB Operator because it is a very simple and safe operation. Try it:

```shell
mgo version
```

If it works properly, you should see a result similar to the following.

```shell
mgo client version v1.0.0
mgo-apiserver version v1.0.0
```

Please note that the version of the mgo client **must** match the version of the MogDB Operator.
