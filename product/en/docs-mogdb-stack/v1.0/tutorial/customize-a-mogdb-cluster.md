---
title: Customize a MogDB Cluster
summary: Customize a MogDB Cluster
author: Wang Dong
date: 2022-06-30
---

# Customize a MogDB Cluster

## Custom MogDB cluster

+ Resource allocation (e.g. memory, CPU, PVC size)
+ High availability (e.g. adding replicas)
+ Specify specific MogDB images
+ Custom MogDB Configuration

There are many different configurations for customizing MogDB clusters. You can refer to [mgo create cluster](. /references/client/mgo-create-cluster.md) to learn about all these options.

<br/>

### Custom PVC Size

Databases come in a variety of sizes, and those sizes are sure to change over time. Therefore, it is helpful to be able to specify the size of the PVC in which the MogDB data is to be stored.

<br/>

#### Customizing the PVC Size for MogDB Database

The MogDB Operator allows you to select the size of the "MogDB data directory" using the --data-storage-size flag. the PVC size should be selected using the standard Kubernetes resource unit, e.g. 128Mi.

For example, to create a MogDB cluster with a 128Mi sized data directory.

```shell
mgo create cluster cluster1 --data-storage-size=20Gi
```

<br/>

#### Customize the PVC Size of the Backup

You can also use --backup-storage-size. backup is used to store all your backups, so you want to resize it to meet your backup retention policy.

For example, to create a backup repository with a PVC size of 1Gi:

```shell
mgo create cluster cluster1 --backup-storage-size=1Gi
```

<br/>

#### Customize the PVC Size of the Database Log

You can also set the log data volume size using --log-storage-size. so you want to adjust its size to meet your log retention policy.

For example, for storing log data volumes, which have a PVC size of 128Mi:

```shell
mgo create cluster cluster1 --log-storage-size=1Gi
```

<br/>

### Custom CPU / Memory

Databases have different CPU and memory requirements, often depending on the amount of data (i.e., actively accessed data) in the working set. kubernetes provides several ways for Pods to manage CPU and memory resources.

+ CPU Requests and Memory Requests
+ CPU Limits and Memory Limits

CPU Requests and Memory Requests tell Kubernetes to ensure that there are at least enough resources available on the node to schedule Pods.

CPU Limits tell Kubernetes not to let a Pod use more CPUs than that amount, and only allow a Pod to use the maximum number of CPUs. similarly, Memory Limits tell Kubernetes not to let a Pod exceed a certain amount of memory. In this case, if Kubernetes detects that a Pod has exceeded its memory limit, it will attempt to terminate any processes that cause the limit to be exceeded. Memory limits can affect the availability of MogDB, and we recommend using them with caution.

Here's how to customize the CPU and memory resources available to the core deployment Pods of your MogDB cluster. Customizing CPU and memory does add more resources to your MogDB cluster, but to take full advantage of the additional resources, you will need to customize the MogDB configuration and tune parameters such as shared_buffers.

#### Custom CPU / Memory for MogDB

The MogDB Operator provides several flags [mgo create cluster](../references/client/mgo-create-cluster.md) to help manage the resources of the MogDB instance.

+ --cpu: Specify the CPU requests for the MogDB instance
+ --cpu-limit: Specify the CPU limit for the MogDB instance
+ --memory: Specify the memory request for the MogDB instance
+ --memory-limit: Specify the memory limit for the MogDB instance

For example, to create a MogDB cluster with a CPU request of 1.0, a CPU limit of 2.0, a memory request of 1Gi, and a memory limit of 2Gi.

```shell
mgo create cluster cluster1 \
--cpu=1.0 --cpu-limit=2.0 \
--memory=1Gi --memory-limit=2Gi
```

<br/>

## Creating a High Availability MogDB Cluster

High Availability allows you to deploy MogDB clusters with redundancy so that your applications can access them even if your primary instance experiences a downtime event. MogDB clusters use the distributed consensus storage system that comes with Kubernetes, so availability is tied to the availability of the Kubernetes cluster.

To create a High Availability MogDB cluster with a slave node, you can run the following command.

```shell
mgo create cluster cluster1 --replicas=1
```

You can use [mgo scale](../references/client/mgo-scale.md) and [mgo scaledown](../references/client/mgo-scaledown.md) command to scaling up and down MogDB clusters.