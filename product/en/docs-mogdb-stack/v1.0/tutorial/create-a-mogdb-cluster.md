---
title: Create a MogDB Cluster
summary: Create a MogDB Cluster
author: Wang Dong
date: 2022-06-30
---

# Create a MogDB Cluster

## Create a Cluster Using the mgo Command

Refer to [mgo create cluster](../references/client/mgo-create-cluster.md) and create a cluster, for example:

```shell
mgo create cluster cluster1
```

Expected output:

```text
created cluster: cluster1
```

This will create a new MogDB cluster named cluster1 , which may take some time to complete.

You can use the [mgo show cluster](../references/client/mgo-show-cluster.md) command to check the status of cluster creation. This command is used to check the details of the MogDB cluster.

For example, when **cluster1** is ready, run the following command:

```shell
mgo show cluster cluster1
```

Expected output:

```text
cluster : cluster1
 pod : cluster1-ib7zq (Running) on mogdb-k8s-001 (3/3) (primary)
 pod : cluster1-rtwdz (Running) on mogdb-k8s-002 (3/3) (standby)
 service : cluster1-svc-master - ClusterIP (10.1.149.4) - Ports (5432:30013/TCP)
 service : cluster1-svc-replicas - ClusterIP (10.1.175.46) - Ports (5432:30012/TCP)
```

<br/>

## Cluster Creation Process

First, the mgo client creates an entry in the custom resource mogdbcluster that contains the properties needed to create the cluster. In the example above, cluster1 utilizes many of the defaults in the MogDB Operator configuration. For example, the default two slave nodes (replicas:2).

We will cover more information about the MogDB Operator configuration later in this tutorial.