---
title: Connecting to MogDB Cluster
summary: Connecting to MogDB Cluster
author: Wang Dong
date: 2022-06-30
---

# Connecting to MogDB Cluster

Before connecting, you need to change the password of the default user. You can change the password by connecting to the container with the **kubectl** command.

```shell
kubectl exec -it cluster1-xxx -c ha-sidecar -- /bin/bash
gsql -dpostgres
```

```sql
ALTER USER mogdb WITH PASSWORD 'new_password';
```

## Connection method gsql

Let's see how to connect to cluster1 using gsql's command line tools. Make sure you have the gsql client installed.

MogDB Operator creates two services with cluster name prefix, one service for the master node and one service for the slave node. get a list of all available services in the mogdb-operator-system namespace.

Execute the following command:

```shell
kubectl get svc -n mogdb-operator-system
```

Expected output:

```text
NAME                    TYPE       CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
cluster1-svc-master     NodePort   10.1.149.4     <none>         5432:30013/TCP   57m
cluster1-svc-replicas   NodePort   10.1.175.46    <none>         5432:30012/TCP   57m
mogdb-apiserver         NodePort   10.1.254.248   <none>         8444:32444/TCP   25h
```

You can connect to the database using the following command.

```shell
gsql -h 127.0.0.1 -p 30013 -U mogdb -W
```

You should then see the MogDB prompt.

```text
gsql ((MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

<br/>

## Outside of Kubernetes Cluster

MogDB Operator creates two services with cluster name prefix, and the type of service is set to NodePort, which can map service port and host port.

You only need to connect "Host:Port".