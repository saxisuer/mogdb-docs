---
title: Resize a MogDB Cluster
summary: Resize a MogDB Cluster
author: Wang Dong
date: 2022-06-30
---

# Resize a MogDB Cluster

## Creating a Highly Available MogDB Cluster

High availability is enabled by default in MogDB Operator as long as you have multiple replicas. To create a High Availability MogDB cluster, you can execute the following command.

```shell
mgo create cluster cluster1 --replica=1
```

<br/>

## Scaling Up MogDB Cluster

You can use the following command [mgo scale](../references/client/mgo-scale.md) to scale an existing MogDB cluster to add slave nodes to it.

```shell
mgo scale cluster1
```

<br/>

## Scaling Down MogDB Cluster

To scaled down a MogDB cluster, you must provide the target of the instances to be scaled down. You can perform this operation using the following [mgo scaledown](../references/client/mgo-scaledown.md) command to perform this operation.

```shell
mgo scaledown cluster1 --query
```

Expected output:

```text
PODNAME              STATUS     NODE
cluster1-rtwdz       Running    mogdb-k8s-002
```

After determining the instances to be scaled down, you can run the following command:

```shell
mgo scaledown cluster1 --target=cluster1-rtwdz
```
