<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# Uqbar Documentation 1.1.0

## Documentation List

+ About Uqbar
  + [What Is Time-Series Data](/about-uqbar/what-is-time-series-data.md)
  + [Uqbar Overview](/overview.md)
  + [Release Note](/about-uqbar/release-note.md)
+ [Uqbar Installation](/ptk-based-installation.md)
+ [Uqbar Management](/uqbar-management.md)
+ [Time-Series Data Retention Policy](./retention-policy.md)
+ [Time-Series Table Management](/time-series-table-management.md)
+ Time-Series Data Write
  + [Overview](/data-write/data-write-introduction.md)
  + Time-Series Data Write Method
    + [Using SQL Statements to Write Data](/data-write/data-writing-method/SQL-supported-for-importing-data-to-table.md)
    + [Using Drivers to Write Data](/data-write/data-writing-method/driver-supported-for-importing-data-to-table.md)
    + [Using Kafka to Write Data](/data-write/data-writing-method/Kafka-supported-for-importing-data-to-table.md)
  + Time-Series Data Write Model
    + [Single-Value and Multi-Value Model](/data-write/data-writing-model/single-field-and-multiple-field-model.md)
    + [Out-of-Order Data Write](/data-write/data-writing-model/data-imported-out-of-order.md)
    + [Wring Data Without the Time Field](/data-write/data-writing-model/data-imported-without-the-time-field.md)
  + [Time-Series Data Write Performance Indicator](/data-write/data-write-performance-indicator.md)
+ [Time-Series Data Compression](/data-compression.md)
+ [Continuous Aggregation](/continuous-aggregation.md)
+ [Deletion of Expired Time-Series Data](/expired-deletion.md)
+ [Data Query](/query-data.md)
+ [Cluster Management](/cluster-management.md)
+ [Backup and Restoration](/backup-restore.md)
+ [Security](/security.md)
+ SQL Syntax
  + [ALTER TIMESERIES TABLE](/uqbar-sql-syntax/ALTER-TIMESERIES-TABLE.md)
  + [COMMENT](/uqbar-sql-syntax/COMMENT.md)
  + [CONNECT BY](/uqbar-sql-syntax/CONNECT-BY.md)
  + [COPY](/uqbar-sql-syntax/COPY.md)
  + [CREATE MATERIALIZED VIEW](/uqbar-sql-syntax/CREATE-MATERIALIZED-VIEW.md)
  + [CREATE PROCEDURE](/uqbar-sql-syntax/CREATE-PROCEDURE.md)
  + [CREATE RULE](/uqbar-sql-syntax/CREATE-RULE.md)
  + [CREATE SEQUENCE](/uqbar-sql-syntax/CREATE-SEQUENCE.md)
  + [CREATE SYNONYM](/uqbar-sql-syntax/CREATE-SYNONYM.md)
  + [CREATE TABLE AS](/uqbar-sql-syntax/CREATE-TABLE-AS.md)
  + [CREATE TIMESERIES TABLE](/uqbar-sql-syntax/CREATE-TIMESERIES-TABLE.md)
  + [CREATE VIEW](/uqbar-sql-syntax/CREATE-VIEW.md)
  + [CURSOR](/uqbar-sql-syntax/CURSOR.md)
  + [DROP TIMESERIES TABLE](/uqbar-sql-syntax/DROP-TIMESERIES-TABLE.md)
  + [EXPLAIN](/uqbar-sql-syntax/EXPLAIN.md)
  + [GRANT](/uqbar-sql-syntax/GRANT.md)
  + [INSERT](/uqbar-sql-syntax/INSERT.md)
  + [LOCK](/uqbar-sql-syntax/LOCK.md)
  + [SELECT](/uqbar-sql-syntax/SELECT.md)
  + [SELECT-INTO](/uqbar-sql-syntax/SELECT-INTO.md)
  + [VACUUM](/uqbar-sql-syntax/VACUUM.md)
+ [Glossary](/glossary.md)