---
title: Release Notes
summary: Release Notes
author: Yao Qian
date: 2021-09-14
---

# Release Notes

## mogha-2.4.2 (2022.12.02)

- openEuler-arm64: [mogha-2.4.2-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.2-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.2-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-CentOS-x86_64.tar.gz)

Change Log:

- Fixed determine role of instance which not started

## mogha-2.4.1 (2022.11.28)

- openEuler-arm64: [mogha-2.4.1-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.1-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.1-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-CentOS-x86_64.tar.gz)

Change Log:

- Support to detect Starting/Building/Catchup state of db
- Fixed repeatedly start db during db in `Starting` state

## mogha-2.4.0 (2022.11.24)

- openEuler-arm64: [mogha-2.4.0-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.0-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.0-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-CentOS-x86_64.tar.gz)

Change Log:

- Add `vip_bind_nic`, `vip_netmask` fields to Zone config in node.conf
- Communication between HA nodes prefer to use `ip`, and fallback to use redundant ips `heartbeat_ips` when the request failed

## mogha-2.3.8 (2022.11.2)

- openEuler-arm64: [mogha-2.3.8-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.8-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.8-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-CentOS-x86_64.tar.gz)

Change Log:

- Fixed not failover when local zone has no sync standby in FULL mode

## mogha-2.3.6 (2022.8.14)

- openEuler-arm64: [mogha-2.3.6-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.6-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.6-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-CentOS-x86_64.tar.gz)

Change Log:

- Added HeartbeatError type, output the known error with log text instead of throwing exception

## mogha-2.3.5 (2022.5.10)

- openEuler-arm64: [mogha-2.3.5-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.5-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.5-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-CentOS-x86_64.tar.gz)

Change Log:

- Add virtual IP conflict detection during failover

## mogha-2.3.4 (2022.4.24)

- openEuler-arm64: [mogha-2.3.4-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.4-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.4-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-CentOS-x86_64.tar.gz)

Change Log:

- Optimize heartbeat requests, use persistent connections (keep-alive) to reduce socket usage and communication latency
- Optimize heartbeat logic, shorten communication latency by concurrent requests
- Optimizes the handling of log disk write failures to avoid process blocking
- Change the ip priority of heartbeat, will use heartbeat ip first

## mogha-2.3.2 (2022.3.15)

- openEuler-arm64: [mogha-2.3.2-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.2-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.2-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-CentOS-x86_64.tar.gz)

Change Log:

- Fix: Failed to flush the route cache after switching
- Feat: Use the immediate mode while stopping database.
- Fix(full mode): The configuration of cluster `replconninfo` is incorrect after switching

## MogHA-2.3.0 (2021.11.30)

- openEuler-arm64: [mogha-2.3.0-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.0-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.0-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-CentOS-x86_64.tar.gz)

Change Log:

- Adjust the system architecture and remove Python dependency.
- Support concurrent deployment of an HA service on each node.
- Enable the maximum availability mode before failover under the lite mode.
- Dynamically obtain the subnet mask of the service NIC as that of the virtual IP address.
- Automatically try incremental rebuilding when the status of the standby database is "Need Repair".
- Define the user-friendly process cmdline for easy distinction.
- Update the mogha.service.tmpl template.
- Optimize code and fix partial bugs.

## MogHA-2.2.2  (2021.10.14)

- arm: [mogha-2.2.2-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-aarch64.tar.gz)
- x86: [mogha-2.2.2-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-x86_64.tar.gz)

Change Log:

- Add the check of the maximum available space before the standby database is switched.
- Add the check of the primary database role after the standby database is switched.
- Optimize the switchover process.
- Optimize the restart process of the standby database.

## MogHA-2.2.1  (2021.9.10)

- arm: [mogha-2.2.1-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-aarch64.tar.gz)
- x86: [mogha-2.2.1-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-x86_64.tar.gz)

Change Log:

- Add the limit of metadatabase connection timeout duration of 3s.
- Add the limit of the HTTP interface request timeout duration of 3s.
- Rectify the fault that **allow_ips** does not include a virtual IP address during HTTP interface authentication.
- Optimize the code structure.

## MogHA-2.2.0  (2021.9.7)

- arm : [mogha-2.2.0-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-aarch64.tar.gz)
- x86：[mogha-2.2.0-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-x86_64.tar.gz)

Change Log:

- Modify the version management mode.
- Pack objects by platform.
- Modify the table structure of the metadatabase.
- Remove the db_host_zone table.
- Add the switchover time field to the db_cluster table.
- Add the **zone** field to the db_instance table.
- Change the **ctime** and **utime** fields to the **create_time** and **update_time** fields for all tables, respectively.
- Support automatic discovery of database ports and writing into **node.conf** during installation.

## MogHA-2.1.1 (2021.7.14)

- [mogha-2.1.1.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.1.1/mogha-2.1.1.tar.gz) (support for x86_64 and aarch64)

Change Log:

- Output a Chinese localization script.
- Add a MogHA uninstallation script.
- Add UCE fault detection.
- Add disk fault detection.
