---
title: Overview
summary: Overview
author: Zhang Cuiping
date: 2021-09-14
---

# About MogHA

MogHA is an ENMOTECH-developed enterprise-ready software system in virtue of the synchronous and asynchronous streaming replication technology. It can guarantee HA of the primary and standby databases.

(It applies to the MogDB and openGauss databases.)

MogHA can automatically detect faults and trigger failover and automatic assignment of virtual IP addresses so that the fault time of a database is reduced from minutes to seconds (RPO=0 and RTO<30s). Therefore, the HA of the database cluster can be ensured.

## Why Is MogHA Still Needed Even If a Database Supports Primary/Standby?

We need to understand what is high availability. High availability can make a database provide continuous services to ensure stable running of upper-layer services. That a database supports primary/standby deployment aims to prevent single point of failures, but it does not offer fault detection and automatic primary/standby switchover. Therefore, MogHA is needed to ensure continuity of database services.

## Functions and Features

- Automatically identify the role of a database instance.
- Automatically trigger failover.
- Detect the network fault.
- Detect the disk fault.
- Support automatic assignment of a virtual IP address to a virtual machine.
- Handle dual-primary brain-split and automatic selection of a primary database.
- Support binding of database processes and CPUs.
- Support process-level HA.
- Support concurrent deployment of an HA service on each node.
- Support x86_64 and aarch64.

## MogHA System Architecture

![image](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-deploy-arch.png)

## Modes Supported by MogHA

### Lite Mode (Recommended)

As the name suggests, the lite mode is the light mode. It enables a MogHA service on only a primary database server and a synchronous standby database server. In this way can MogHA service guarantee HA of database instances on the two machines. If the primary database server encounters an irreparable fault or network isolation, MogHA can achieve database failover and automatic assignment of a virtual IP address to a virtual machine.
![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-lite-mote.png)

### Full Mode

The full mode requires the MogHA service run on each database machine and all database instances automatically managed by the MogHA service. If the primary database is faulty, MogHA preferentially chooses the synchronous standby database in the local equipment room as the new primary database. If the synchronous standby database is also faulty, MogHA chooses the synchronous standby database in the standby equipment room in the local city as the new primary database. To make RPO reach 0 and avoid data loss, MogHA does not choose the asynchronous standby database as the primary database. During primary/standby switchover, the replication connection of databases and configuration of synchronous standby database list will be automatically modified in this mode.

For example: two-city-and-three-centers (one primary database and six standby databases)
![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-full-mode.png)
