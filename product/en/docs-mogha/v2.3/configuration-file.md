---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# Configuration File

MogHA configuration file **node.conf** automatically generates during execution of the **install.sh** script according to the **node.conf.tmpl** template. If it already exists in the installation directory, automatic generation will be skipped. The configuration file is compiled in the INI format.

An INI file is composed of section, key, and value.

  **Section**: [section]

  **Parameter** (key=value): name=value

  **Note**: A note starts with ; or (#).

## Key and Value of a Parameter

For all parameters, their keys are defined in snake case. The type of each parameter can be boolean, string, or integer according to its function.

- Boolean: The value can be `on`, `off`, `true`, `false`, `yes`, `no`, `1`, or `0` (They are case-insensitive.)
- String: The value is a string literal without quotation marks.
- Integer: The value is a number without quotation marks.

## Basic Configuration Items

- **db_port** (integer)

  Specifies the database port for MogHA to connect to a database.

- **db_user** (string)

  Specifies the database user for MogHA to connect to, operate with, and manage a database. The system user specified in the **install.sh** script during installation is used by default.

- **db_datadir** (string)

  Specifies the data directory of a database.

- **primary_info** (string)

  Specifies the storage path of primary and standby MogHA metadata. The default storage path is the **primary_info** file in the installation directory. The metadata supports storage in the JSON and binary format, which is configured through `meta_file_type`.

- **standby_info** (string)

  Specifies the storage path of standby MogHA metadata. The default storage path is the **standby_info** file in the installation directory. The metadata supports storage in the JSON and binary format, which is configured through `meta_file_type`.

- **meta_file_type** (string)

  Specifies the metadata storage format. The value can be **json** and **bin**. The default value is **json**.

- **lite_mode** (boolean)

  Specifies whether the mode is the lite mode. The default value is `True`. MogHA uses this parameter to distinguish the lite mode from the non-lite mode.

  - **lite mode**: As the name suggests, the lite mode indicates the light scheduling mode. The lite mode requires startup of the MogHA service only on servers where the primary database and the synchronous standby database are located. It applies to the scenario where there are one primary database and one standby database or there are one primary database and multiple standby databases.

  - **Non-lite mode**: This mode requires O&M personnel configure all related server information in the **node.conf** configuration file and runs the MogHA service on all servers with instances. During failover in this mode, the system will choose the most optimal standby database as the primary database from zone1/zone2 (the local city) and automatically modify the **repliconninfo** configuration of the primary and standby instances.

    The policies of choosing a standby database during failover:

    - If there is a synchronous standby database in the primary equipment room (zone1), this database will be preferentially chosen as the new primary database.
    - If a synchronous standby database is not in the same equipment room as the faulty primary database is but in zone2 (the equipment room of the local city), this database will be preferentially chosen as the new primary database.

- **agent_port** (integer)

  Specifies the communication port between MogHA nodes. If there is a firewall, ensure that the communication port can be connected to the firewall. The default value is **8081**.

- **http_req_timeout** (integer)

  Specifies the communication request timeout (unit: second) between MogHA nodes. The default value is **3**.

- **heartbeat_interval** (integer)

  Specifies the MogHA heartbeat interval (unit: second). The default value is **3**.

- **primary_lost_timeout** (integer)

  Specifies the timeout (unit: second) for the standby database to detect that the primary database is lost. The default value is **10**.

  When the standby database cannot ping the primary database but can ping the gateway or other nodes, the standby database thinks that the primary database is lost (offline). When the time for detecting that the primary database is lost exceeds the timeout, the standby database triggers failover and takes over services of the primary database.

- **primary_lonely_timeout** (integer)

  Specifies the timeout (unit: second) for MogHA on the primary database server to judge whether the current server is lonely or isolated. When MogHA cannot ping any server, it thinks that the network of the current server is faulty. When the fault time exceeds the timeout, MogHA will disable the virtual IP address (if exists) and the current instance.

- **double_primary_timeout** (integer)

  Specifies the timeout for MogHA on the primary database server to monitor whether another primary database instance exists in the current cluster. If there is another primary database instance, double-primary exception occurs. If the exception time exceeds the timeout, a new primary database of the two is independently chosen.

- **taskset** (boolean)

  Specifies whether to configure CPU affinity for the database instance process. The default value is **False**.

  When this field is set to **True**, the following rules will be followed:

  Assume that the number of CPU cores of the current server is *N*.
  > When *N* is greater than or equal to 16, the number of available CPUs for setting database processes is *N*-4.

    When *N* is greater than or equal to 8 and less than 16, the number of available CPUs for setting database processes is *N*-2.

    When *N* is greater than 1 and less than 8, the number of available CPUs for setting database processes is *N*-1.

    When N is equal to 1, there is no constraint.

- **logger_format** (string)

  Specifies the log format. The default value is `%(asctime)s %(levelname)s [%(filename)s:%(lineno)d]: %(message)s`.

  For more fields, see [python3 logging logrecord-attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes).

- **log_dir** (string)

  Specifies the log directory. The default value is the installation directory.

- **log_max_size** (string)

  Specifies the maximum size of a log file. If the size approaches the upper value, the log scrolls. The default value is **512MB**. The supported unit can be KB, MB, and GB (the unit is case-insensitive).

- **log_backup_count** (integer)

  Specifies the number of log backup files. The default value is **10**.

- **allow_ips** (string)

  Specifies the IP address list that can be requested through MogHA cluster HTTP API. The IP addresses are separated with commas.

- **handle_down_primary** (boolean)

  Specifies whether MogHA needs to be restarted or triggers switchover when detecting that the primary database process crashes. The default value is **True**.

  The `primary_down_handle_method` parameter takes effect only when this parameter is set to **True**.

- **handle_down_standby** (boolean)

  Specifies whether MogHA needs to pull up the standby database process once detecting that it crashes.

- **primary_down_handle_method** (string)

  Specifies the MogHA handling method when the primary database process is not started. The value can be **restart** and **failover**. The default value is **restart**.

  When this parameter is set to **restart**, the `restart_strategy` parameter needs to be configured.

- **restart_strategy** (string)

  Specifies the maximum number of restart times and maximum timeout. The format is Maximum number/timeout (minutes). The default value is **10/3**.

  Assume that the parameter value is **10/3**. **10** indicates the maximum number of restart times. **3** indicates the restart timeout (minutes). Any condition is met, the restart operation will stop and failover is triggered.

- **uce_error_detection** (boolean)

  Specifies the UCE (uncorrected error) fault detection function. The default value is **True**.

  *UCE: When a database kernel process receives certain sigbus error involved in the system kernel, such as damaged memory blocks, it will stop gracefully and write the error code into a database log and exit. The damaged memory block will be marked by the system kernel and not be assigned to an application program. Therefore, when this error occurs, you can restart the database process to restore the service.*

- **uce_detect_max_lines** (integer)

  Specifies the number of lines of a database log that can be read from back to front during UCE check. The default value is **200**.

- **debug_mode** (boolean)

  Specifies the debug mode. When this function is enabled, the MogHA print log level is degraded to DEBUG and more run logs will be printed. The default value is **False**.

## (Optional) Metadatabase

No configuration is needed by default.

MogHA retains the metadatabase storage function, facilitating the third-party monitoring system to read the database cluster status. However, MogHA does not depend on the metadatabase for logical judgment. If this part has parameters configured, MogHA will output metadata information to the metadatabase during running.

### Configuration Items

- **ha_name** (string)

  Specifies the MogHA cluster name, which is globally unique. It is prohibited that two HA clusters share the same name.

- **host** (string)

  Specifies the IP address of the host where the metadatabase is located.

- **port** (integer)

  Specifies the port for connecting to the metadatabase.

- **db** (string)

  Specifies the name of the metadatabase.

- **user** (string)

  Specifies the username for connecting to the metadatabase.

- **password** (string)

  Specifies the password for connecting to the metadatabase.

- **connect_timeout** (integer)

  Specifies the timeout (unit: second) for connecting to the metadatabase. The default value is **3**.

## Host Configuration

host1 to host9 indicate hosts where different database instances are located.

The following uses host1 as an example to introduce parameters. The parameter configuration is similar to that of any other host.

```ini
[host1]
ip=
heartbeat_ips=
```

- **ip** (string)

  (Required) Communication IP address between HA nodes, please ensure it equals ip used in db WAL log

- **heartbeat_ips** (string)

  (Optional) Redundant ip address for communication between HA nodes. This field is an optional field and is used as the standby ip address if request failed by `ip`.

  If there are multiple heartbeat IP addresses. separate them with commas.

## Equipment Room Configuration

zone1 to zone3 indicate the primary equipment room, the standby equipment room of the local city, and the equipment room of the remote city.

The following uses zone1 as an example to introduce parameters. The parameter configuration is similar to that of any other equipment room.

```ini
[zone1]
hosts=
ping_list=
vip=
cascades=
arping=
vip_bind_nic=
vip_netmask=
```

- **vip** (string)

  (Optional) Specifies the virtual IP address of the current equipment room. If there exists a virtual IP address, MogHA will automatically attach this IP address to the server where the primary database is located. Otherwise, this parameter is left blank.

- **hosts** (string)

  (Required) Specifies the host list in the current equipment room. The value is the section name of the corresponding host. If there are multiple hosts, separate them with commas.

  For example, host1 and host2 are located in zone1. This parameter can be set to `hosts=host1,host2`.

- **ping_list** (string)

  (Required) Specifies the arbitration node that determines whether the host network is normal in the current equipment room. The value can be the gateway address of the equipment room. You are allowed to configure multiple arbitration IP addresses separated with commas.

- **cascades** (string)

  (Optional) Specifies the host list of the cascaded standby databases are located in the current equipment room. The value is the same as that of `hosts`. If there is no cascaded standby database, this parameter is left blank.

- **arping** (string)

  (Optional) Specifies the broadcast address for broadcasting when a virtual IP address is attached. If there is no broadcast address, this parameter is left blank.

- **vip_bind_nic** (string)

  [2.4.0 added] (Optional)，Specifies the name of NIC which the vip to be bound. By default, the NIC will use current host ip's NIC

- **vip_netmask** (string)

  [2.4.o added] (Optional)，，Specifies the netmask of vip, default will use current host ip's netmask if not given
