---
title: FAQs
summary: FAQs
author: Yao Qian
date: 2021-09-14
---

# FAQs

- **Why can we find four processes (including two web processes and two heartbeat processes) by running the `ps -ef|grep mogha`  command after the MogHA service is enabled?**

  MogHA is developed using Python. Running the Python code depends on a Python interpreter and a third-party dependency package. To avoid too many requirements for the system during deployment or avoid too much dependency on the network, MogHA is packed using the Pyinstaller tool. During file execution after MogHA packing, programs will be decompressed to a temporary directory for running. When the programs start running, the tool will enable two processes, including one actual work process and a cleaner process used for cleaning the temporary directory once the work process breaks down. However, when you run `ps` to query the process, you will find that two processes with the same instruction are started.

  Because MogHA runs a web process and a heartbeat process, there are totally four processes.

- **Do I have to configure metadatabase?**

  No.

- **Why does the primary heartbeat log reports the error that the standby instance cannot be found when the MogHA service on the primary and standby instances has been enabled?**

  Please check whether the primary and standby instances have the firewall enabled and whether the **agent_port** port can be accessed.

- **What is the MogHA fault detection mechanism?**

  For the primary database instance, the check includes instance status check, network check, and dual-primary check. Instance status check is performed through the database management tool. It supports configuration of automatically starting an instance that has crashed and configuration of number of start times and start time. Network check is to check the connection with other instances and gateways to check whether the network is isolated. You can configure the check timeout. If the network is verified faulty, it will be killed automatically. Dual-primary is to find the real primary instance through checking most of the instances in a cluster. If there are only two instances, find the real primary instance according to metadata time.

  For the standby database instance, the fault detection mechanism is to check connection with other instances and gateways to check whether the primary database is faulty. You can configure the check timeout.

- **How does MogHA achieve primary/standby switchover?**

  When the primary database finds itself faulty, it will send a request to the candidate standby database for failover through the HTTP interface according to the defined failover sequence.
  When a standby database detects that the primary database is faulty, it will automatically perform failover according to the defined failover sequence.
  The switchover action includes failover and attachment of virtual IP addresses.

- **What does the primary database do to ensure data consistency if the standby database cannot be connected in the synchronous replication environment?**

  In the synchronous replication environment, if a standby database fails connection, the transactions of the primary database will be hung. If the maximum availability mode is used, the system modifies the synchronous mode to the asynchronous mode by default.

  For both synchronous mode and asynchronous mode, if all standby databases become faulty, data consistency makes no sense during the downtime. However, once the standby database recovers, it will notify the primary database of the required start LSN position to obtain all differentiated WAL logs and ensure data consistency.
