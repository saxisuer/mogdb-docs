---
title: Installation and Deployment
summary: Installation and Deployment
author: Yao Qian
date: 2021-09-14
---

# Installation and Deployment

<br/>

## Downloading an Installation Package

Visit [Release Notes](release-notes.md) to download an installation package and obtain a package similar to `mogha-x.x.x.tar.gz` (`x` indicates the version.)

<br/>

## Description of Files in an Installation Package

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/installation-dir-tree.png)

<br/>

## System Environment Requirements

### Ensuring that python 3.6+ or Later Is Used

Because the supervisord component of MogHA runs depending on python3.6 or later, ensure that the python3 commands can be run on the current server and check the python3 version before installation.
Check methods are as follows:

```bash
command -v python3
# The python3 directory may be different from /usr/bin/python3. Therefore, ensure that python3 exists.
python3 -V
# Python 3.6 or later will be OK for installation.
```

### Verifying that gsql and gs_ctl Are Run Without the Password Requirement

Ensure that a password is not required for running **gsql** or **gs_ctl** when you log in to a server as the database installation user. You can find the **pg_hba.conf** authentication file in the data directory to verify that the verification mode of local connection is trust.

Verification method: switch to user **omm** and run the following command to check the cluster status. (If you can query the cluster status without entering a password, the verification is successful.)

```bash
gs_ctl -D [PGDATA] query
```

### Granting the Sudo Permission to the Database Installation User

Because a virtual IP needs to be automatically attached to MogHA, the `ifconfig` command is used for NIC-related operations. MogHA is enabled by the database installation user. To operate with a NIC, the database installation user needs to have the sudo permission. During installation, the `install.sh` script will try to automatically add the `omm` user to `/etc/sudoers` and grant the user the `ifconfig` and `systemctl` permission but it cannot ensure that the operation is successful. Therefore, it is recommended to verify whether the `omm` user is granted the sudo permission by checking `/etc/sudoers`.
The configuration method is as follows:

```bash
Configure a user's sudo permission.
chmod +w /etc/sudoers

which systemctl
# /usr/bin/systemctl
which ifconfig
# /usr/sbin/ifconfig

vi /etc/sudoers
# Add the following content to the end of the file.
omm     ALL=(ALL)       NOPASSWD: /usr/sbin/ifconfig
omm     ALL=(ALL)       NOPASSWD: /usr/sbin/systemctl
# Save the file and exit.
chmod -w /etc/sudoers
```

### Enabling the Communication Port

MogHA requires a fixed port (default: **8081** which can be modified through the **agent_port** parameter) for MogHA communication between nodes. Therefore, you need to ensure that the port can be directly accessed between different nodes.

### Correcting the Time

Use **ntp** or **chronyd** to correct the time of the primary and standby instances.

<br/>

## Installation Procedure

> ！Note: Please perform installation as the `root` user because the database installation user needs to be granted the sudo permission and the system service needs to be registered.

Copy the installation package to a server where MogHA is to be deployed. It is recommended to use the **/home** directory as the directory for storing the installation package, such as `/home/omm/`. The following procedure will use this directory to introduce how to perform the installation.

### Decompress the Installation Package

```bash
cd /home/omm
tar -zxf mogha-2.x.x.tar.gz
```

After the installation package is decompressed, a `mogha` folder will be obtained in the current directory.

### Installing the MogHA Service

Open the `mogha` folder and execute the `./install.sh USER PGDATA` installation script (You need to configure related parameters in the actual installation).

- `USER` indicates the database installation user, such as `omm` in this example.
- `PGDATA` indicates the data directory of the database, such as `/opt/mogdb/data` in this example.

The following example shows the installation process:

```bash
# Open the mogha folder.
cd /home/omm/mogha
# Execute the installation script.
./install.sh omm /opt/mogdb/data

# During the installation, information similar to the following is returned:
[2021-07-01 10:43:24]: MogHA installation directory: /home/omm/mogha
[2021-07-01 10:43:24]: Database installation user: omm; user group: dbgrp
[2021-07-01 10:43:24]: Data directory of the database: /opt/mogdb/data
[2021-07-01 10:43:24]: LD_LIBRARY_PATH=/opt/mogdb/app/lib:/opt/mogdb/tool/lib:
[2021-07-01 10:43:24]: GAUSSHOME=/opt/mogdb/app
[2021-07-01 10:43:24]: Current system architecture: aarch64
[2021-07-01 10:43:24]: Modify the installation directory and its subdirectories or the user and user group of the file...
[2021-07-01 10:43:24]: Link python3 interpreter to the virtual environment...
[2021-07-01 10:43:24]: Directory of Python3: /usr/bin/python3
[2021-07-01 10:43:24]: Python3 version: 3.7
[2021-07-01 10:43:24]: python interpreter is successfully linked in the virtual environment
[2021-07-01 10:43:24]: Generate the mogha.service file...
[2021-07-01 10:43:24]: Copy the mogha.service file to /usr/lib/systemd/system/
[2021-07-01 10:43:24]: Reload the configuration file
[2021-07-01 10:43:24]: mogha service registered successfully
[2021-07-01 10:43:24]: User omm already exists in /etc/sudoers file. Please check the permission manually!
[2021-07-01 10:43:24]: Generate supervisord.conf configuration file...
[2021-07-01 10:43:24]: supervisord.conf generated successfully
[2021-07-01 10:43:24]: node.conf already exists and will not automatically generate.
[2021-07-01 10:43:24]: MogHA installation succeed!

Start the service after modifying the /home/omm/mogha/node.conf configuration file!!!

Start MogHA:
    systemctl start mogha
```

The installation script output shows the installation process. A successful installation must contains "MogHA installation succeed!". For other situations, you need to check which installation procedure fails to perform.

If the installation script is successfully executed, there are three new files generated.

- node.conf: HA configuration file (if this file exists, it will not be generated.) requires completion of instance and equipment room information.
- supervisord.conf: supervisord configuration file (supervisord is a process HA component)
- mogha.service: description file of the service for registering systemd, which has been registered with the systemd service. The MogHA service can be managed through the systemctl tool.

#### Completing node.conf Configuration

After the installation script is executed, you are not advised to start the MogHA service immediately. In view of the fact that MogHA runs depending on the configuration of `node.conf`  in the installation directory. You need to correctly configure the file and then start the MogHA service. During the first installation, `node.conf` does not exist. The script will generate a `node.conf` configuration file according to the `node.conf.tmpl` template file. You can find the caution prompted at the end of the installation process "Please modify `node.conf` configuration based on your cluster information first and then start the service". The next section will introduce how to modify the `node.conf` configuration file.

### Starting the MogHA Service

After `node.conf` is successfully configured, start the MogHA service.

```bash
sudo systemctl start mogha
```

After starting MogHA, check the running status of MogHA.

```bash
sudo systemctl status mogha
● mogha.service - MogDB/openGuass HA manager service
   Loaded: loaded (/usr/lib/systemd/system/mogha.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-06-22 10:17:19 CST; 2s ago
 Main PID: 832531 (supervisord)
    Tasks: 16
   Memory: 144.1M
   CGroup: /system.slice/mogha.service
           ├─832531 /home/omm/mogha/prod_venv/bin/python /home/omm/mogha/prod_venv/bin/supervisord -c /home/omm/mogha/supervisord.conf
           ├─832548 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --heartbeat
           ├─832549 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --web
           ├─832550 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --heartbeat
           └─832551 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --web

Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: mogha.service: Succeeded.
Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: Stopped MogDB/openGuass HA manager service.
Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: Started MogDB/openGuass HA manager service.
Jun 22 10:17:19 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:19,967 INFO supervisord started with pid 832531
Jun 22 10:17:20 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:20,969 INFO spawned: 'heartbeat' with pid 832548
Jun 22 10:17:20 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:20,971 INFO spawned: 'web' with pid 832549
```

### Setting MogHA that Is Enabled Upon Server Startup

```shell
sudo systemctl enable mogha
```

### Restarting the MogHA Service

```bash
sudo systemctl restart mogha
```

### Stoping the MogHA Service

```bash
sudo systemctl stop mogha
```

<br/>

## Uninstalling MogHA

Open the installation directory and execute the uninstallation script.

```bash
cd /home/omm/mogha./uninstall.sh
# The output is as follows:
[2021-07-01 11:09:07]: Try to stop mogha (systemctl stop mogha)
[2021-07-01 11:09:08]: Delete the registration file[2021-07-01 11:09:08]: Uninstallation success
```

<br/>

## Log Files

MogHA will generate two log files.

- mogha_heartbeat.log
This log file is stored in the installation directory, such as `/home/omm/mogha` in this example. It records HA heartbeat logs and is a main log for fault analysis.
- mogha_web.log
This log file stores the same directory as the heartbeat log. It records web request logs.
