---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# Configuration File

## node.conf Configuration

### Fields that Must Be Confirmed by the User

- Database connection port

    ```ini
    # Database port used for connecting a database
    db_port=0
    ```

- HA communication port

    ```ini
    # Port used for communication between HA nodes. If a firewall is deployed, the HA communication port needs to be enabled.
    agent_port=8081
    ```

- Host list (here only host1 is used as an example. The configuration of other hosts is the same as that of host1.)

    The user needs to fill the service IP addresses of the servers where all primary and standby instances in the database cluster are located in the `ip` field.

    If there is no heartbeat IP address, leave it blank. If there is, fill it. If there are multiple heartbeat IP addresses, separate them with commas.

    ```ini
    # host1-8 indicates 1 to 8 servers.
    [host1]
    # IP address of the service network
    ip=192.168.1.111
    # Multiple heartbeat networks can be configured with their IP addresses separated with commas.
    heartbeat_ips=192.168.1.112,192.168.1.113
    ```

- Zone equipment room list (here only zone1 is used as an example. The configuration of other zones is the same as that of zone1.)

    Zone here indicates an equipment room.

  - `vip`: indicates a virtual IP address owned by an equipment room, which is used for service access and decoupled from the private IP address of a server, thereby facilitating primary/standby switchover.
  - `arping`:  can be left blank. The system will use the broadcast address.
  - `ping_list`: used for checking the IP address of the intermediate node on the network, such as a gateway address which is usually used.
  - `hosts`: indicates the host list in an equipment room. It needs to be specially pointed out that `hosts` here does not require you to enter the host name or IP address but such section name defined in the configuration file as `host1`, `host2`.... For example, `host1` and `host2` belong to the `zone1` equipment room. Here you can configure `hosts` as `hosts=host1,host2`.
  - `cascade`: indicates the cascaded standby instance list. The configuration method is similar to that of  `hosts`.

    ```ini
    # This part is used for distinguishing equipment rooms. Different equipment rooms have their IP addresses.
    [zone1]
    # Virtual IP address of the local equipment room
    vip=
    # arping address of the local equipment room. If it is unknown, leave it blank. The system will use the arp broadcast mode.
    arping=
    # Check the intermediate node on the network, such as a gateway address that can be pinged and is usually used.
    ping_list=
    # Host list in the local equipment room, such as host1, host2, and host3
    hosts=
    # Cascaded standby instance list in the equipment room
    cascade=
    ```

### Configuration of Other Options

```ini
[config]
# Heartbeat interval or primary process check interval, which is usually set to 3s to 5s
heartbeat_interval=3

# Time for detecting a lost primary instance. When a primary instance crashes and cannot be pinged for the time long, the standby instance can be switched as a new primary instance.
primary_lost_timeout=10

# Time taken for that a primary instance fails to connect and ping other machines. In this case, the network of the primary instance is thought faulty.
# When the primary instance fails to ping other machines for the time long, the primary instance needs to be shut down. (it is set to readonly in MogDB.)
primary_lonely_timeout=10

# When the system detects that there are two primary instances for a specific time, the judgment of checking whether there are two primary instances needs to be performed.
# During maintenance operation, the system may detect that there are two primary instances for a very short time (At that moment, database actually does not allow write operations.)
double_primary_timeout=10

# Operating system user of a database, such as omm or postgres
db_user=__USER__

# Data directory of a database, which is used for local communication, such as signal transmission
db_datadir=__PGDATA__

# File type of local metadata:
#   - "json": json format
#   - "bin": binary format (If this parameter is not configured, the binary format is used by default.)
meta_file_type=json

# Location for storing local metadata, which is automatically maintained. When metadatabase becomes unavailable, this data can be used for primary instance judgment.
primary_info=__INSTALLDIR__/primary_info

# Location for storing local metadata, which is automatically maintained. When the primary instance is lost, the standby instance depends on this data to check its synchronization status.
standby_info=__INSTALLDIR__/standby_info

# Whether to set the CPU limit for database instances. If the value is true, at least one CPU will be reserved and will not be allocated to a database for use.
taskset=True

# Whether to use the lite mode. The value can be True and False.
# In the lite mode, even if there are one primary instance and multiple standby instances, the MogHA service is enabled on only the primary instance and a synchronous standby instance.
# If the value is False, the full mode applies, indicating that the MogHA service needs to be enabled on all nodes.
# Distinction between the lite and full modes: In the lite mode, the HA service does not modify the related configuration of the primary and standby database instances. However, in the full mode, the configuration will be automatically modified.
lite_mode=False

# Whether to use the metadatabase
# If the value is True, configure the metadatabase connection parameters in the [meta] section.
# If the value is False, the [meta] section can be left blank.
use_meta=False

# Set the log output format.
logger_format=%(asctime)s - %(name)s - %(levelname)s [%(filename)s:%(lineno)d]: %(message)s

# Apart from the primary and standby machines, you need to set the IP address list that can be used for accessing the web interface. If there are multiple IP addresses, separate them with commas.
allow_ips=

# [Newly added in v2.1] What should I do if the primary database instance process is not started?
# There are two methods.
#   restart: You can try to restart the process. You can set the number of restart times by setting the restart_instance_limit parameter.
#   failover: After the standby instance is switched as a new primary instance, the primary instance tries to rebuild a database and the database is started as a new standby instance.
primary_down_handle_method=failover

# [Newly added in v2.1] Maximum number of times or maximum time taken for restarting an instance: times/minutes
# For example, 10/3 indicates that you can restart an instance a maximum of 10 times or 3 minutes. If any one condition is met, you cannot try again.
restart_strategy=10/3

# This section contains metadatabase connection parameters (openGauss database).
# Weak dependency. The configuration error will be reported in the log but does not impact the HA scheduling.
[meta]
ha_name=    # Names of primary and standby HA instances, which must be globally unique. It is prohibited that two sets of primary and standby instances use the same name.
host=       # Database server
port=       # Database port
db=         # database
user=       # User name
password=   # Password
schema=     # schema
```
