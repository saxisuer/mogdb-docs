---
title: Release Notes
summary: Release Notes
author: Yao Qian
date: 2021-09-14
---

# Release Notes

## MogHA-2.2.2  (2021.10.14)

- arm: [mogha-2.2.2-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-aarch64.tar.gz)
- x86: [mogha-2.2.2-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-x86_64.tar.gz)

Change Log:

- Add the check of the maximum available space before the standby database is switched.
- Add the check of the primary database role after the standby database is switched.
- Optimize the switchover process.
- Optimize the restart process of the standby database.

## MogHA-2.2.1  (2021.9.10)

- arm: [mogha-2.2.1-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-aarch64.tar.gz)
- x86: [mogha-2.2.1-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-x86_64.tar.gz)

Change Log:

- Add the limit of metadatabase connection timeout duration of 3s.
- Add the limit of the HTTP interface request timeout duration of 3s.
- Rectify the fault that **allow_ips** does not include a virtual IP address during HTTP interface authentication.
- Optimize the code structure.

## MogHA-2.2.0  (2021.9.7)

- arm : [mogha-2.2.0-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-aarch64.tar.gz)
- x86：[mogha-2.2.0-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-x86_64.tar.gz)

Change Log:

- Modify the version management mode.
- Pack objects by platform.
- Modify the table structure of the metadatabase.
- Remove the db_host_zone table.
- Add the switchover time field to the db_cluster table.
- Add the **zone** field to the db_instance table.
- Change the **ctime** and **utime** fields to the **create_time** and **update_time** fields for all tables, respectively.
- Support automatic discovery of database ports and writing into **node.conf** during installation.

## MogHA-2.1.1 (2021.7.14)

- [mogha-2.1.1.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.1.1/mogha-2.1.1.tar.gz) (support for x86_64 and aarch64)

Change Log:

- Output a Chinese localization script.
- Add a MogHA uninstallation script.
- Add UCE fault detection.
- Add disk fault detection.
