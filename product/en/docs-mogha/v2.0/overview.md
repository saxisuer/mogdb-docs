---
title: Overview
summary: Overview
author: Zhang Cuiping
date: 2021-09-14
---

# Overview

## About MogHA

MogHA is an ENMOTECH-developed enterprise-ready high availability software system in virtue of the synchronous and asynchronous streaming replication technology. It satisfies the high availability requirement for MogDB and openGauss databases. MogHA aims to realize automatic primary/standby switchover and automatic assignment of a virtual IP address to a virtual machine in case of server or instance breakdown so that the database downtime can be shortened from minutes to seconds, thereby ensuring continuous running of the service system.

## Why Is MogHA Still Needed Even If a Database Supports Primary/Standby?

We need to understand what is high availability. High availability can make a database provide uninterrupted services to ensure stable running of upper-layer services. That a database supports primary/standby deployment aims to prevent single point of failures, but it does not offer fault detection and automatic primary/standby switchover. Therefore, MogHA is needed. It provides a system for monitoring the instance status and the primary/standby status and triggering primary/standby failover once it detects a failure.

## Functions and Features

- Automatically identify the role of a database instance.
- Monitor the health status of a database instance process in real time.
- Handle the database instance failure or failover.
- Detect the network fault.
- Detect the disk fault.
- Support automatic assignment of a virtual IP address to a virtual machine.
- Handle dual-primary brain-split and automatic selection of a primary database.
- Support affinity binding of database processes and CPUs.
- Support process-level high availability.
- Support x86_64 and aarch64.

## MogHA Deployment Architecture

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-mogha-arch.png)

MogHA realizes one-click deployment, greatly simplifying the deployment process. It automatically hosts MogHA web and heartbeat processes in supervisord that is integrated with an installation package, achieving high availability of MogHA itself. Additionally, it automatically registers the MogHA service with systemd so that maintenance personnel can quickly manage the MogHA service by running the systemctl command.

## MogHA Communication Model

MogHA communicates using the HTTP protocol, in which there are a set of interaction interfaces used for MogHA between different nodes. The user can customize the mutual trust mechanism between nodes through the configuration file. With the mutual trust mechanism, requests outside a cluster will be rejected to ensure the MogHA cluster security.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-communicate-strategy.png)

## Modes Supported by MogHA

- Lite mode

As the name suggests, the lite mode is the light mode. It enables a MogHA service on only a primary database server and a synchronous standby database server. In this way can MogHA service guarantee continuity of applications. If the primary database server encounters an irreparable fault or network isolation, MogHA can achieve database failover and automatic assignment of a virtual IP address to a virtual machine.
（This mode applies to the scenarios where there are only two database servers.)
![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-lite-mode.png)

- Full mode (Beta)

The full mode requires configuring of all database instances in a configuration file before the MogHA service is enabled and then running of the MogHA service on all database instances (a maximum of nine instances, including one primary instance and eight standby instances). This mode will modify the database replication connection configuration (replconninfo) after the primary/standby switchover.

For example: two-city-and-three-centers (one primary instance and six standby instances)
![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-full-mode.png)

## Distinction Between the Lite Mode and Full Mode

- **Database configuration repliconninfo**

The lite mode does not modify the replication connection configuration but the full mode does after the primary/standby switchover.
Modification mode: For a primary instance, configure the replication connection relationship between the primary instance and all standby instances. For a standby instance, configure the replication connection relationship only between the primary instance and the standby instance.

- **MogHA configuration ping_list**

The lite mode must has the **ping_list** field of the zone where the machine is located configured in the MogHA configuration file but the full mode does not have this requirement. In the lite mode where MogHA is enabled on only two machines, when one of the machines suffers a network fault or network isolation, single machine cannot judge whether its network is normal. Therefore, the user needs to configure one or more additional IP address (**ping_list**) for judging the network status. You can set **ping_list** to the common gateway address of the two machines or the IP address of a third machine that can be pinged by the two machines.
