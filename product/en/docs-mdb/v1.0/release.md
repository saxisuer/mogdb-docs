---
title: Release Note
summary: Release Note
author: tianzijian
date: 2022-06-30
---

# Release Note

## v1.0.5(2022.11.03)

### Bug Fixes

- Fix datetime data type synchronization error.

## v1.0.4 (2022.10.30)

### Features

- Support object synchronization from PostgreSQL to MogDB (schema,table).
- Support object synchronization from PostgreSQL to MogDB for customized name mapping
- Support full data synchronization and incremental data synchronization from PostgreSQL to MogDB.
- Support incremental data synchronization pausing and continuing from PostgreSQL to MogDB.
- Channel Data synchronization and incremental tracking are combined, and channel status is adjusted.
- Added the status of the data Capture and Integrate services and the abnormal data synchronization status.
- Supports synchronous recovery of abnormal data.
- Optimized the way the command line tool checks the survival status of subservices.

### Bug Fixes

- Resolve the issue the scenario where the same table name in different schemas lead to objects synchronization fails.
- Resolve the issue that the Connector connect is not exist lead to a channel pause failure.

## v1.0.3 (2022.9.23)

### Features

- Support Oracle character set: ZHS16GBK
- Add dump command, start and stop child service command.

### Bug Fixes

- Resolve the issue the scenario where the schema name or database name contains '#' lead to data synchronization fails
- Resolve the issue that the heartbeat table is not filtered when the heartbeat table is enabled.

## v1.0.2(2022.9.15)

### Features

- Support object synchronization from MySQL to MogDB (schema,table).
- Support object synchronization from MySQL to MogDB for customized name mapping
- Support full data synchronization and incremental data synchronization from MySQL to MogDB.
- Support incremental data synchronization pausing and continuing from MySQL to MogDB.
- Support Oracle CDB-PDB mode.
- Add details about the channel synchronization task.
- Changing the login URL.

## v1.0.1 (2022.8.31)

### Features

- add [Upgrade](./upgrade.md)

### Bug Fixes

- Resolve the issue that the snapshot data synchronization is completed, and the data synchronization task status is still "In progress"

## v1.0.0 (2022.6.30)

### Features

- Support object synchronization from Oracle to MogDB (schema,table).
- Support object synchronization from Oracle to MogDB for customized name mapping
- Support full data synchronization and incremental data synchronization from Oracle to MogDB.
- Support incremental data synchronization pausing and continuing from Oracle to MogDB.
- Support MDB access from a browser for graphical operations.
