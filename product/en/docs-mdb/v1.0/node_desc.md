---
title: MDB Node Function
summary: MDB Node Function
author: tianzijian
date: 2022-06-30
---

# MDB Node Function

In the MDB system, a node is an endpoint in the replication process and corresponds to a database system.

A node can be a local (located in the server where the MDB system is deployed) or remote (located in a remote server) node.

A node can be used as a source database (for capturing data) or a target database (for synchronizing data).

## Node List

Fuzzy search is supported by the node name, username, IP address, and service name. Currently, only MogDB and Oracle are supported.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_3.png?x-oss-process=image/resize,w_1000)

## Node Addition

- Add an Oracle node

    For the Oracle node, you can choose connection type `ServiceName` or `SID`, and click `Test Connection`. After the test is successful, click `OK`. If a channel uses an Oracle node as a source node, see [Oracle](./source_oracle.md) for database configuration and user permission description. If an Oracle node is used as a target node, see [Target Database Requirement](./target_db.md).

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_1.png?x-oss-process=image/resize,w_1000)

- Add a MogDB node

    After a MogDB node is added, click `Test Connection`. After the test is successful, click `OK`. If a channel uses a MogDB node as a source node, see [MogDB](./source_mogdb.md) for database configuration and user permission description. If a MogDB node is used as a target node, see [Target Database Requirement](./target_db.md).

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_2.png?x-oss-process=image/resize,w_1000)

- Add a MySQL node

    After a MySQL node is added, click `Test Connection`. After the test is successful, click `OK`. If a channel uses a MySQL node as a sourcenode, see [MySQL](./source_mysql.md) for database configuration and user permission description. If a MySQL node is used as a target node,see [Target Database Requirement](./target_db.md).

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_25.png?x-oss-process=image/resize,w_1000)

- Add a PostgreSQL node

    After a PostgreSQL node is added, click `Test Connection`. After the test is successful, click `OK`. If a channel uses a PostgreSQL node as a sourcenode, see [PostgreSQL](./source_postgresql.md) for database configuration and user permission description. If a PostgreSQL node is used as a target node,see [Target Database Requirement](./target_db.md).

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_28.png?x-oss-process=image/resize,w_1000)

## Node Updating

If a node is not used by a channel, the connection test passes and is saved after node updating. If a node is used by a channel, pause data synchronization, and re-execute related tasks after node updating.

## Connection Test

This is a node connection test. If the connection fails, abnormal connection information is returned.

## Node Deletion

If a node is not used by a channel, delete it directly. If a node is used by a channel, delete the channel first and then delete the node.
