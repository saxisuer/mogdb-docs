---
title: MogDB as a Source Database
summary: MogDB as a Source Database
author: tianzijian
date: 2022-06-30
---

# MogDB as a Source Database

## Supported Version

Database version: 2.0 or later version
Dependency plug-in: wal2json

## Database Configuration

### Install wal2json

For details, see [wal2json](https://gitee.com/enmotech/wal2json).

### Modify the WAL Parameter of the Source Database

Modify the parameter configuration file `postgresql.conf` of the source database. The parameter file is located in the `data/` directory of the database.

Use gsql to log in to the MogDB database. Run the following command to query the data directory:

```shell
gsql -d postgres -U $USER -c 'show data_directory'
```

Modify the `postgresql.conf` file in the `data/` directory.

```shell
# Relace <data_directory> with the actual data directory
vi <data_directory>/postgresql.conf
```

Find the following parameters in the `postgresql.conf` file and set them to specified values.

1. wal_level=logical   #Use logical decoding to read pre-write logs.
2. wal_sender_timeout = 0s    #Customize the timeout.

### Modify the HBA Configuration of the Source Database

In the data directory, find the `pg_hba.conf` configuration file, add the following content to add the replication permission.

**Note**: mdb_user indicates the user for using the replication function. It needs to be modified based on actual requirement.

```shell
# Replace <data_directory> with the actual data directory. 
cat >> <data_directory>/pg_hba.conf <"EOF"

# Add for MDB replication
host  replication  mdb_user   0.0.0.0/0  sha256
EOF
```

### Set User Permission

Create the mdb_user user for logging in to and replicate source MogDB data. The username and password need to be modified based on the actual requirement.

- The login permission is used for logging in to the database.
- The replication permission is used for synchronizing incremental data.
- The sysadmin permission is used for initializing objects and synchronizing full data.

```sql
create user mdb_user with login replication sysadmin PASSWORD 'Enmo@123';
```

### Enable the REPLICA IDENTITY Mode of the Table

For all tables to be synchronized, enable the REPLICA IDENTITY mode.

**Note**: The following is the command example.

```sql
ALTER TABLE public.customers REPLICA IDENTITY FULL;
```

### Enable Heartbeat Configuration

Sending a heartbeat message enables the connector to send the latest retrieved LSN to the database slot, allowing the database to reclaim disk space used by WAL files that are no longer needed.

To enable the heartbeat configuration, see [Channel-related Advanced Parameters](./channel_desc.md#Channel-Advanced Parameter). You need to set `heartbeatIntervalMs` to a positive integer, and run the following command on the source node.

**Note**: The following is only an example. The table name `heartbeat` needs to be consistent with the `heartbeatTableName` value.

```sql
CREATE TABLE heartbeat (ts TIMESTAMP WITH TIME ZONE);
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## Data Type Mapping

| Source Database | Target Database | Source Database Type | Target Database Type |
|--------|-----------|----------|-----------|
| MogDB|Oracle|timestamptz|TIMESTAMP WITH TIME ZONE|
| MogDB|Oracle|raw|RAW|
| MogDB|Oracle|bit|RAW|
| MogDB|Oracle|varbit|RAW|
| MogDB|Oracle|blob|XML|
| MogDB|Oracle|bytea|BLOB|
| MogDB|Oracle|byteawithoutordercol|BLOB|
| MogDB|Oracle|byteawithoutorderwithequalcol|BLOB|
| MogDB|Oracle|uuid|BLOB|
| MogDB|Oracle|json|JSON|
| MogDB|Oracle|jsonb|JSON|
| MogDB|Oracle|xml|XMLTYPE|
| MogDB|Oracle|float4|FLOAT|
| MogDB|Oracle|float8|DOUBLE|
| MogDB|Oracle|oid|NUMBER|
| MogDB|Oracle|int1|NUMBER|
| MogDB|Oracle|int2|NUMBER|
| MogDB|Oracle|int4|NUMBER|
| MogDB|Oracle|int8|NUMBER|
| MogDB|Oracle|int16|NUMBER|
| MogDB|Oracle|money|NUMBER|
| MogDB|Oracle|numeric|NUMBER|
| MogDB|Oracle|bpchar|CHAR|
| MogDB|Oracle|char|CHAR|
| MogDB|Oracle|name|CHAR|
| MogDB|Oracle|varchar|VARCHAR2|
| MogDB|Oracle|nvarchar2|NVARCHAR2|
| MogDB|Oracle|bool|VARCHAR2|
| MogDB|Oracle|clob|CLOB|
| MogDB|Oracle|text|CLOB|
| MogDB|Oracle|abstime|TIMESTAMP WITH TIME ZONE|
| MogDB|Oracle|date|DATE|
| MogDB|Oracle|interval|INTERVAL|
| MogDB|Oracle|reltime|TEXT|
| MogDB|Oracle|smalldatetime|DATE|
| MogDB|Oracle|time|DATE|
| MogDB|Oracle|timetz|DATE|
| MogDB|Oracle|timestamp|TIMESTAMP WITHOUT TIME ZONE|

The character set mapping supports the following.

## Character Set Mapping

| Source Database | Target Database | Character Set of the Source Database | Character Set of the Target Database |
|--------|----------------|----------|-----------|
| MogDB|Oracle| BIG5           |ZHT16BIG5|
| MogDB|Oracle| EUC_JP         |JA16EUC|
| MogDB|Oracle| EUC_JIS_2004   |JA16EUCTILDE|
| MogDB|Oracle| EUC_TW         |ZHT32EUC|
| MogDB|Oracle| GB18030        |ZHS32GB18030|
| MogDB|Oracle| GBK            |ZHS16GBK|
| MogDB|Oracle| ISO_8859_5     |CL8ISO8859P5|
| MogDB|Oracle| ISO_8859_6     |AR8ISO8859P6|
| MogDB|Oracle| ISO_8859_7     |EL8ISO8859P7|
| MogDB|Oracle| ISO_8859_8     |IW8ISO8859P8|
| MogDB|Oracle| JOHAB          |KO16KSCCS|
| MogDB|Oracle| KOI8R          |CL8KOI8R|
| MogDB|Oracle| KOI8U          |CL8KOI8U|
| MogDB|Oracle| LATIN1         |WE8ISO8859P1|
| MogDB|Oracle| LATIN2         |EE8ISO8859P2|
| MogDB|Oracle| LATIN3         |SE8ISO8859P3|
| MogDB|Oracle| LATIN4         |NEE8ISO8859P4|
| MogDB|Oracle| LATIN5         |WE8ISO8859P9|
| MogDB|Oracle| LATIN6         |NE8ISO8859P10|
| MogDB|Oracle| LATIN7         |BLT8ISO8859P13|
| MogDB|Oracle| LATIN8         |CEL8ISO8859P14|
| MogDB|Oracle| LATIN9         |WE8ISO8859P15|
| MogDB|Oracle| SJIS           |JA16SJIS|
| MogDB|Oracle| SHIFT_JIS_2004 |JA16SJISTILDE|
| MogDB|Oracle| SQL_ASCII      |US7ASCII|
| MogDB|Oracle| UHC            |KO16MSWIN949|
| MogDB|Oracle| UTF8           |AL32UTF8|
| MogDB|Oracle| WIN866         |RU8PC866|
| MogDB|Oracle| WIN1250        |EE8MSWIN1250|
| MogDB|Oracle| WIN1251        |CL8MSWIN1251|
| MogDB|Oracle| WIN1252        |WE8MSWIN1252|
| MogDB|Oracle| WIN1253        |EL8MSWIN1253|
| MogDB|Oracle| WIN1254        |TR8MSWIN1254|
| MogDB|Oracle| WIN1255        |IW8MSWIN1255|
| MogDB|Oracle| WIN1256        |AR8MSWIN1256|
| MogDB|Oracle| WIN1257        |BLT8MSWIN1257|
| MogDB|Oracle| WIN1258        |VN8MSWIN1258|
