---
title: Upgrade
summary: MDB Upgrade
author: tianzijian
date: 2022-08-31
---

# MDB Upgrade

## Upgrade Procedure

### Download Software

Download the MDB software package of the latest version. To obtain the software package, contact the sales personnel.

### Upload and Decompress the Software Package

Upload the MDB software package to the server and decompress it.

```shell
tar -zxvf mdb_v1.0.1.RELEASE_linux_x86_64.tar.gz
```

### Preparing for the upgrade

Example: Old version MDB installation directory: /u01/mdb_v1.0.0.RELEASE_linux_x86_64

Switch to the old version MDB software package directory and stop MDB service.

```shell
cd /u01/mdb_v1.0.0.RELEASE_linux_x86_64

./mdb stop
```

Switch to the decompressed MDB software package directory and copy the file from Old version MDB.

```shell
cd mdb_v1.0.1.RELEASE_linux_x86_64

# copy config.json、license.json to mdb_v1.0.1.RELEASE_linux_x86_64 directory
cp -f /u01/mdb_v1.0.0.RELEASE_linux_x86_64/config.json ./  
cp -f /u01/mdb_v1.0.0.RELEASE_linux_x86_64/license.json ./
```

### Upgrade MDB

Check and make sure that you have switched to the MDB software package directory.

```shell
pwd         # Make sure that you have switched to the mdb_v1.0.1.RELEASE_linux_x86_64 directory.
```

Upgrade MDB.

```shell
./mdb upgrade
```

The output is as follows:

```text
   2022-08-30 14:36:26  ======BEGIN TO UPGRADE MDB======
   2022-08-30 14:36:26    1.CHECK MDB FILE
   2022-08-30 14:36:26    2.CHECK MDB ENVIRONMENT
   2022-08-30 14:36:26     JAVA_VERSION: 11.0
   2022-08-30 14:36:26     EXIST RESIDUAL DIRS ! PATH: /tmp/kafka-logs ,/tmp/zookeeper
   2022-08-30 14:36:26    3.UNZIP KAFKA
   2022-08-30 14:36:27    4.INSTALL MDB SERVER
   2022-08-30 14:36:28    5.INSTALL MDB DATA WORKER
   2022-08-30 14:36:28  

   ======END UPGRADE MDB RESULT======
   +------------+-----------+-------------+---------------+
   | File_Check | Env_Check | MDB_Install | KAFKA_INSTALL |
   +------------+-----------+-------------+---------------+
   |  Success   |  Success  |   Success   |    Success    |
   +------------+-----------+-------------+---------------+

```

### Start MDB

Start MDB.

```shell
 ./mdb start
```

The output is as follows:

```text
   2022-06-23 11:33:10  start zookeeper success
   2022-06-23 11:33:21  start kafka success
   2022-06-23 11:33:32  start connector success
   2022-06-23 11:33:42  start mdb-server success
   2022-06-23 11:33:42  After MDB is started, the the ports occupied by programs are as follows: 
   +-------+-----------+-----------+------------+
   | KAFKA | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +-------+-----------+-----------+------------+
   |  9092 |    2181   |    8083   |    55437    |
   +-------+-----------+-----------+------------+
   2022-06-23 11:33:42  MDB program service status
   +--------+-----------+-----------+------------+
   | KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +--------+-----------+-----------+------------+
   | online |   online  |   online  |   online   |
   +--------+-----------+-----------+------------+
```

### Login to MDB

After MDB is started, access the client browser (Google Chrome is recommended) to check whether the original user can login normally.

<http://localhost:55437/mdb/index.html>

After a successful login, restart the suspended service in the channel list.
