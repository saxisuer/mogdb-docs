---
title: Overview of User Interface
summary: Overview of User Interface
author: tianzijian
date: 2022-06-30
---

# Overview of User Interface

This document introduces MDB web user interface to help you use the MDB system.

## Login

You need to log in to the MDB web UI. The user register portal is unavailable now, therefore you need to log in to the UI using the initial username and password.  

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_18.png?x-oss-process=image/resize,w_1000)

## Initial Password Resetting

You are forcibly required to reset the password upon first login.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_22.png?x-oss-process=image/resize,w_1000)

## Home Page

After login, the MDB web UI is displayed. The navigation tree on the left displays the node and channel. You need to add nodes first.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_17.png?x-oss-process=image/resize,w_1000)

## Personal Center

The personal center shows the user account, nickname, mobile number, and password. The password can be changed.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_24.png?x-oss-process=image/resize,w_1000)
