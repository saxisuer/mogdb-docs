---
title: MySQL as a Source Database
summary: MySQL as a Source Database
author: tianzijian
date: 2022-09-15
---

# MySQL as a Source Database

## Supported Version

5.7, 8.0.x (not support MariaDB)

## Database Configuration

### Binlog Configuration

```shell
vi /etc/my.cnf

#  add the following content to the end of the file
server_id=2
log_bin = mysql-bin
binlog_format = ROW

# Restarting the mysql Service
systemctl restart mysqld
```

### Binlog Check

```sql
SHOW VARIABLES WHERE VARIABLE_NAME IN ('log_bin','binlog_format');

-- 期望结果:log_bin: ON, binlog_format: ROW 
```

### Set User Permission

Create the mdb_user user，Grant the required permissions to the user：

```sql
CREATE USER 'mdb_user'@'localhost' IDENTIFIED BY 'Enmo@123';
GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'mdb_user' IDENTIFIED BY 'Enmo@123';
FLUSH PRIVILEGES;
```

### Enable Heartbeat Configuration

Heartbeat messages are useful for monitoring whether the connector is receiving change events from the database. Heartbeat messages might help decrease the number of change events that need to be re-sent when a connector restarts.

To enable the heartbeat configuration, see [Channel-related Advanced Parameters](./channel_desc.md#Channel-Advanced Parameter). You need to set `heartbeatIntervalMs` to a positive integer, and run the following command on the source node.

**Note**: The following is only an example. The table name `heartbeat` needs to be consistent with the `heartbeatTableName` value.

```sql
CREATE TABLE heartbeat (`ts` timestamp ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## Data Type Mapping

| Source Database      | Target Database | Source Database Type | Target Database Type    |
|--------|-----------|----------|-----------|
|MySQL|MogDB|tinytext|text|
|MySQL|MogDB|text|text|
|MySQL|MogDB|mediumtext|text|
|MySQL|MogDB|longtext|text|
|MySQL|MogDB|tinyblob|bytea|
|MySQL|MogDB|blob|bytea|
|MySQL|MogDB|mediumblob|bytea|
|MySQL|MogDB|longblob|bytea|
|MySQL|MogDB|json|json|
|MySQL|MogDB|bit|bytea|
|MySQL|MogDB|binary|bytea|
|MySQL|MogDB|char byte|bytea|
|MySQL|MogDB|varbinary|bytea|
|MySQL|MogDB|tinyint|smallint|
|MySQL|MogDB|smallint|smallint|
|MySQL|MogDB|mediumint|integer|
|MySQL|MogDB|integer|integer|
|MySQL|MogDB|int|integer|
|MySQL|MogDB|bigint|bigint|
|MySQL|MogDB|serial|bigserial|
|MySQL|MogDB|float|float|
|MySQL|MogDB|double|double precision|
|MySQL|MogDB|double precision|double precision|
|MySQL|MogDB|real|double precision|
|MySQL|MogDB|decimal|numeric|
|MySQL|MogDB|numeric|numeric|
|MySQL|MogDB|dec|numeric|
|MySQL|MogDB|fixed|numeric|
|MySQL|MogDB|char|character|
|MySQL|MogDB|character|character|
|MySQL|MogDB|nchar|character|
|MySQL|MogDB|national char|character|
|MySQL|MogDB|varchar|character varying|
|MySQL|MogDB|character varying|character varying|
|MySQL|MogDB|national varchar|character varying|
|MySQL|MogDB|nvarchar|character varying|
|MySQL|MogDB|date|date|
|MySQL|MogDB|timestamp|timestamp|
|MySQL|MogDB|year|int|
|MySQL|MogDB|boolean|boolean|
|MySQL|MogDB|bool|boolean|
|MySQL|MogDB|enum|text|
|MySQL|MogDB|time|time|
|MySQL|MogDB|datetime|timestamp|
|MySQL|MogDB|set|text|
|MySQL|MogDB|geometry|text|
|MySQL|MogDB|point|text|
|MySQL|MogDB|linestring|text|
|MySQL|MogDB|polygon|text|
|MySQL|MogDB|multipoint|text|
|MySQL|MogDB|multilinestring|text|
|MySQL|MogDB|multipolygon|text|
|MySQL|MogDB|geometrycollection|text|
|MySQL|MogDB|geomcollection|text|
|MySQL|MogDB|auto_increase|serial|

The character set mapping supports the following.

## Character Set Mapping

| Source Database    |  Target Database | Character Set of the Source Database         | Character Set of the Target Database   |
 |--------|----------------|----------|-----------|
| MySQL|MogDB| utf8mb4           |UTF8|
