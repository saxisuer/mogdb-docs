---
title: MDB Channel Function
summary: MDB Channel Function
author: tianzijian
date: 2022-06-30
---

# MDB Channel Function

In the MDB system, channel is a logical unit and used for connecting a source node and a target node through a series of operations.
Data replication, scheduling, error processing, performance adjustment, and other operations generate in the channel.
MDB synchronizes data in the channel from a source data to a target data.

Currently, a channel supports only a source node and a target node, that is, one-to-one data synchronization and  replication.

## Channel List

Fuzzy search is supported by channel name and node name.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_5.png?x-oss-process=image/resize,w_1000)

## Channel Addition

Add a data synchronization channel, and synchronize data from Oracle to MogDB.  Adding a channel does not allow the source node type to be the same as the target node type.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_4.png?x-oss-process=image/resize,w_1000)

## Channel Deletion

If a channel is not in progress, delete it directly. If the channel is in progress, stop the task in the channel and then delete the channel.

## Channel Synchronization Pausing

A channel can be paused only when data synchronization is in progress. After the channel is paused, you can also start it.

## Channel Details

Channel details show the channel name, status, number of synchronization objects and tables, and node name.

## Channel-Configuration

The channel name can be modified.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_6.png?x-oss-process=image/resize,w_1000)

## Channel-Object

### Add Object

Select the object to be synchronized to the target node from the source node. If the mapping rule is configured, rule conversion is performed. The case of the target object name is converted based on the specification of the target node by default.
![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_35.png?x-oss-process=image/resize,w_1000)

### Delete Object 

Delete the added synchronization object.

### Manually Adding Object

Enter the object name of the source node (format: schema.table) to add the object to be synchronized to the target node.

### Field Mapping Viewing

This part shows the source and target field mapping information of the source and target nodes. Only tables can be viewed.

### Field Mapping Configuration

This part supports modification of the schema, table name, and field name on  the target node. After the modification is saved, perform pre-check, object synchronization, and data synchronization again.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_36.png?x-oss-process=image/resize,w_1000)

### Mapping Rule

### Schema Mapping Configuration

- Multiple mapping rules can be configured and performed based on the sequence listed.
- The configuration takes effect immediately after being saved. Object and data synchronization needs to be performed again.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_9.png?x-oss-process=image/resize,w_1000)

### Object Mapping Configuration

- Character replacement: Replace the specified string on the target node with that in the object name of the source node.
- Regular replacement: Replace the specified string on the target node with that conforming to the regular expression in the object name of the source node.
- Multiple mapping rules can be configured and performed based on the sequence listed.
- The configuration takes effect immediately after being saved. Object and data synchronization needs to be performed again.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_10.png?x-oss-process=image/resize,w_1000)

## Channel-Advanced Parameter

In MDB, advanced parameters control the logic of the channel synchronization process. For example, skipping snapshots, synchronizing only incremental data, and overriding the existing objects on the target node.

| Parameter     | Default | Description                                                                                                                                                  |
|:-----------------|:---:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| compatibleModel  | ON | Migration compatibility mode: <br />ON indicates that the compatibility is enabled.<br />OFF indicates that the compatibility is disabled.                   |
| overrideMode     | NONE | Override mode<br />NONE_FORCE indicates data is not overridden.<br />NONE is default.<br />FORCE indicates that data is overridden.                          |
| tableSpaceSwitch | OFF | Whether to ignore the tablespace<br />ON indicates that the tablespace is not ignored.<br />OFF indicates that the tablespace is ignored.                    |
| skipSnapShot     | ON | Whether to skip full data<br />ON indicates that full data is skipped.<br />OFF indicates that full data is not skipped.                                     |
| heartbeatIntervalMs     | 0 | Heartbeat interval (s)<br />0 indicates that the heartbeat interval is disabled.<br />If the value is a positive integer, the heartbeat interval is enabled. |
| heartbeatTableName     | heartbeat | Heartbeat table: Create a heartbeat table on the source node.                                                                                                |
| skipMigrationObject     | OFF | Whether to skip object synchronization                                                                                                                       |
| postgresqlPluginName     | decoderbufs | PostgreSQL Plug-in:decoderbufs，wal2json                                                                                                                             |

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_11.png?x-oss-process=image/resize,w_1000)

## Channel-Log Synchronization

Run log describes the data capturing and target node writing process after data synchronization.

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_19.png?x-oss-process=image/resize,w_1000)

## Channel-Operation

### Recent Task List

The synchronization progress, execution time of each phase, and result are displayed in the channel.

### Task Synchronization

- Perform one-key synchronization or perform pre-check, object synchronization, and data synchronization in sequence.

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_34.png?x-oss-process=image/resize,w_1000)

- Pause data synchronization: Pause data capturing and integrating.
- Start data synchronization: Restore data synchronization and data capturing and integrating.
- Data synchronization reset: Reset Data synchronization, the data synchronization progress and records for all tables in the channel are deleted.
  
### Task Synchronization Details

- View the data synchronization result
  
  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_29.png?x-oss-process=image/resize,w_1000)

### Task Failure Viewing

- View pre-check abnormity

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_31.png?x-oss-process=image/resize,w_1000)

- View the synchronization object abnormity

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_32.png?x-oss-process=image/resize,w_1000)

  The SQL on the target node supports editing, formatting, and replicating. After a SQL is edited and saved, perform it again. After successful execution, the abnormity is removed from the list.

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_20.png?x-oss-process=image/resize,w_1000)

### Refreshing

After a task is performed, refresh the list to show the latest channel status and task status.
