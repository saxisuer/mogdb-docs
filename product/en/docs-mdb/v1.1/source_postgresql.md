---
title: PostgreSQL as a Source Database
summary: PostgreSQL as a Source Database
author: tianzijian
date: 2022-10-24
---

# PostgreSQL as a Source Database

## Supported Version

Database version: 9.6, 10, 11, 12, 13, 14
Dependency plug-in: decoderbufs, wal2json

## Database Configuration

### Install plug-in

For details, see [decoderbufs](https://github.com/debezium/postgres-decoderbufs),
[wal2json](https://github.com/eulerto/wal2json)

### Modify the WAL Parameter of the Source Database

Modify the parameter configuration file `postgresql.conf` of the source database. The parameter file is located in the `data/` directory of the database.

Use psql to log in to the PostgreSQL database. Run the following command to query the data directory:

```shell
psql -d postgres -U $USER -c 'show data_directory'
```

Modify the `postgresql.conf` file in the `data/` directory.

```shell
# Relace <data_directory> with the actual data directory
vi <data_directory>/postgresql.conf
```

Find the following parameters in the `postgresql.conf` file and set them to specified values.

1. wal_level=logical               # use logical (change requires restart)
2. max_replication_slots = 4       # max number of replication slots (change requires restart)
3. wal_sender_timeout = 0s         # in milliseconds; 0 disables
4. max_wal_senders = 8             # max number of walsender processes (change requires restart)
5. shared_preload_libraries = 'decoderbufs' （If the plug-in is wal2json, ignore it）

### Modify the HBA Configuration of the Source Database

In the data directory, find the `pg_hba.conf` configuration file, add the following content to add the replication permission.

**Note**: mdb_user indicates the user for using the replication function. It needs to be modified based on actual requirement.

```shell
# Replace <data_directory> with the actual data directory. 
cat >> <data_directory>/pg_hba.conf <"EOF"

# Add for MDB replication
host  replication  mdb_user   0.0.0.0/0  sha256
EOF
```

### Set User Permission

Create the mdb_user user for logging in to and replicate source PostgreSQL data. The username and password need to be modified based on the actual requirement.

- The login permission is used for logging in to the database.
- The replication permission is used for synchronizing incremental data.
- The superuser permission is used for initializing objects and synchronizing full data.

```sql
create user mdb_user with superuser login replication PASSWORD 'Enmo@123';
```

### Enable the REPLICA IDENTITY Mode of the Table

For all tables to be synchronized, enable the REPLICA IDENTITY mode.

**Note**: The following is the command example.

```sql
ALTER TABLE public.customers REPLICA IDENTITY FULL;
```

### Enable Heartbeat Configuration

Sending a heartbeat message enables the connector to send the latest retrieved LSN to the database slot, allowing the database to reclaim disk space used by WAL files that are no longer needed.

To enable the heartbeat configuration, see [Channel-related Advanced Parameters](./channel_desc.md#Channel-Advanced Parameter). You need to set `heartbeatIntervalMs` to a positive integer, and run the following command on the source node.

**Note**: The following is only an example. The table name `heartbeat` needs to be consistent with the `heartbeatTableName` value.

```sql
CREATE TABLE heartbeat (ts TIMESTAMP WITH TIME ZONE);
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## Data Type Mapping

| Source Database | Target Database | Source Database Type | Target Database Type |
|--------|-----------|----------|-----------|
|PostgreSQL| MogDB|inet|inet|
|PostgreSQL| MogDB|macaddr|macaddr|
|PostgreSQL| MogDB|tsquery|bytea|
|PostgreSQL| MogDB|tsvector|bytea|
|PostgreSQL| MogDB|aclitem|bytea|
|PostgreSQL| MogDB|cid|bytea|
|PostgreSQL| MogDB|gtsvector|bytea|
|PostgreSQL| MogDB|int2vector|bytea|
|PostgreSQL| MogDB|oidvector|bytea|
|PostgreSQL| MogDB|oidvector_extend|bytea|
|PostgreSQL| MogDB|refcursor|bytea|
|PostgreSQL| MogDB|regclass|bytea|
|PostgreSQL| MogDB|oid|numeric|
|PostgreSQL| MogDB|bit|bytea|
|PostgreSQL| MogDB|varbit|bytea|
|PostgreSQL| MogDB|bytea|bytea|
|PostgreSQL| MogDB|uuid|uuid|
|PostgreSQL| MogDB|json|text|
|PostgreSQL| MogDB|jsonb|jsonb|
|PostgreSQL| MogDB|xml|text|
|PostgreSQL| MogDB|float4|float4|
|PostgreSQL| MogDB|float8|float8|
|PostgreSQL| MogDB|int2|int2|
|PostgreSQL| MogDB|int4|int4|
|PostgreSQL| MogDB|int8|int8|
|PostgreSQL| MogDB|money|numeric|
|PostgreSQL| MogDB|numeric|numeric|
|PostgreSQL| MogDB|bpchar|bpchar|
|PostgreSQL| MogDB|char|char|
|PostgreSQL| MogDB|name|name|
|PostgreSQL| MogDB|varchar|varchar|
|PostgreSQL| MogDB|bool|bytea|
|PostgreSQL| MogDB|text|text|
|PostgreSQL| MogDB|date|date|
|PostgreSQL| MogDB|interval|interval|
|PostgreSQL| MogDB|time|time|
|PostgreSQL| MogDB|timetz|timetz|
|PostgreSQL| MogDB|timestamp|timestamp|
|PostgreSQL| MogDB|timestamptz|timestamptz|
|PostgreSQL| MogDB|point|text|
|PostgreSQL| MogDB|lseg|bytea|
|PostgreSQL| MogDB|box|bytea|
|PostgreSQL| MogDB|path|bytea|
|PostgreSQL| MogDB|polygon|bytea|
|PostgreSQL| MogDB|circle|bytea|
|PostgreSQL| MogDB|cidr|cidr|
|PostgreSQL| MogDB|regconfig|bytea|
|PostgreSQL| MogDB|regdictionary|bytea|
|PostgreSQL| MogDB|regoper|bytea|
|PostgreSQL| MogDB|regoperator|bytea|
|PostgreSQL| MogDB|regproc|bytea|
|PostgreSQL| MogDB|regprocedure|bytea|
|PostgreSQL| MogDB|regtype|bytea|
|PostgreSQL| MogDB|tid|bytea|
|PostgreSQL| MogDB|txid_snapshot|bytea|
|PostgreSQL| MogDB|xid|bytea|
|PostgreSQL| MogDB|smallserial|serial|
|PostgreSQL| MogDB|int4range|int4range|
|PostgreSQL| MogDB|int8range|int8range|
|PostgreSQL| MogDB|numrange|numrange|
|PostgreSQL| MogDB|tsrange|tsrange|
|PostgreSQL| MogDB|daterange|daterange|
|PostgreSQL| MogDB|ltree|text|
|PostgreSQL| MogDB|citext|text|
|PostgreSQL| MogDB|macaddr8|text|
|PostgreSQL| MogDB|geometry|text|
|PostgreSQL| MogDB|geography|text|
|PostgreSQL| MogDB|hstore|text|
|PostgreSQL| MogDB|enum|text|

The character set mapping supports the following.

## Character Set Mapping

| Source Database | Target Database | Character Set of the Source Database | Character Set of the Target Database |
|--------|----------------|----------|-----------|
|PostgreSQL|MogDB|BIG5|BIG5|
|PostgreSQL|MogDB|EUC_CN|EUC_CN|
|PostgreSQL|MogDB|EUC_JP|EUC_JP|
|PostgreSQL|MogDB|EUC_JIS_2004|EUC_JIS_2004|
|PostgreSQL|MogDB|EUC_KR|EUC_KR|
|PostgreSQL|MogDB|EUC_TW|EUC_TW|
|PostgreSQL|MogDB|GB18030|GB18030|
|PostgreSQL|MogDB|GBK|GBK|
|PostgreSQL|MogDB|Windows936|Windows936|
|PostgreSQL|MogDB|ISO_8859_5|ISO_8859_5|
|PostgreSQL|MogDB|ISO_8859_6|ISO_8859_6|
|PostgreSQL|MogDB|ISO_8859_7|ISO_8859_7|
|PostgreSQL|MogDB|ISO_8859_8|ISO_8859_8|
|PostgreSQL|MogDB|JOHAB|JOHAB|
|PostgreSQL|MogDB|KOI8R|KOI8R|
|PostgreSQL|MogDB|KOI8U|KOI8U|
|PostgreSQL|MogDB|LATIN1|LATIN1|
|PostgreSQL|MogDB|LATIN2|LATIN2|
|PostgreSQL|MogDB|LATIN3|LATIN3|
|PostgreSQL|MogDB|LATIN4|LATIN4|
|PostgreSQL|MogDB|LATIN5|LATIN5|
|PostgreSQL|MogDB|LATIN6|LATIN6|
|PostgreSQL|MogDB|LATIN7|LATIN7|
|PostgreSQL|MogDB|LATIN8|LATIN8|
|PostgreSQL|MogDB|LATIN9|LATIN9|
|PostgreSQL|MogDB|LATIN10|LATIN10|
|PostgreSQL|MogDB|MULE_INTERNAL|MULE_INTERNAL|
|PostgreSQL|MogDB|SJIS|SJIS|
|PostgreSQL|MogDB|SHIFT_JIS_2004|SHIFT_JIS_2004|
|PostgreSQL|MogDB|SQL_ASCII|SQL_ASCII|
|PostgreSQL|MogDB|UHC|UHC|
|PostgreSQL|MogDB|UTF8|UTF8|
|PostgreSQL|MogDB|WIN866|WIN866|
|PostgreSQL|MogDB|WIN874|WIN874|
|PostgreSQL|MogDB|WIN1250|WIN1250|
|PostgreSQL|MogDB|WIN1251|WIN1251|
|PostgreSQL|MogDB|WIN1252|WIN1252|
|PostgreSQL|MogDB|WIN1253|WIN1253|
|PostgreSQL|MogDB|WIN1254|WIN1254|
|PostgreSQL|MogDB|WIN1255|WIN1255|
|PostgreSQL|MogDB|WIN1256|WIN1256|
|PostgreSQL|MogDB|WIN1257|WIN1257|
|PostgreSQL|MogDB|WIN1258|WIN1258|
