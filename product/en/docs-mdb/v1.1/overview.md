---
title: MDB Introduction
summary: MDB Introduction
author: tianzijian
date: 2022-06-30
---

# MDB Introduction

MDB (MogDB Data Bridge) is a heterogeneous database migration and synchronization tool.

It is used for database migration and synchronization between MogDB/openGauss or openGauss-based databases and other databases such as Oracle, DB2, MySQL,and PostgreSQL.

## Architecture

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/overview-en.png)

## Supported Database Types

| Source Database              | Target database | Supported or Not | Supported Version Estimated |
|:-----------------------------|:-----------|:----|:----------------------------|
| [Oracle](./source_oracle.md) | [MogDB](./target_db.md#mogdb)| Supported ||
| [MogDB](./source_mogdb.md)                         | [Oracle](./target_db.md#oracle) | Supported (Only data synchronization is supported)|                       |
| [MySQL](./source_mysql.md)   | [MogDB](./target_db.md#mogdb) | Supported |                             |
| [MogDB](./source_mogdb.md)   | [MySQL](./target_db.md#mysql)| Supported (Only data synchronization is supported)     |                       |
| [PostgreSQL](./source_postgresql.md)   | [MogDB](./target_db.md#mogdb) | Supported ||
| [MogDB](./source_mogdb.md)   | [PostgreSQL](./target_db.md#postgresql) | Supported (Only data synchronization is supported)|                       |
| [SQL Server](./source_sqlserver.md)   | [MogDB](./target_db.md#mogdb) | Supported ||
| [MogDB](./source_mogdb.md)   | [SQL Server](./target_db.md#sqlserver) | Supported (Only data synchronization is supported)|  |

## Supported Database Objects

| Object Name | Supported or Not | Description | Supported Version Estimated |
|---------------|--------|-----|-----------------------------|
| Schema        | Supported |     ||
| Table         | Supported |     ||
| Constraint   | Supported | Foreign key unsupported ||
| Index         | Supported |     ||
| TableData   | Supported |Table data     ||
| User   | Unsupported |    | v2.2                        |
| Sequence   | Unsupported |    | v2.2                        |
| Synonyms   | Unsupported |    | v2.2                        |

## Unsupported Database Objects

| Object Name | Supported or Not | Description |
|---------------|--------|-----|
| Trigger        |  Unsupported  |     |
| Procedure |  Unsupported  |     |
| Function   | Unsupported |  |
| Package        |  Unsupported  |     |
| View        |  Unsupported  |     |
