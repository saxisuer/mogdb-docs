---
title: Oracle as a Source Database
summary: Oracle as a Source Database
author: tianzijian
date: 2022-06-30
---

# Oracle as a Source Database

## Supported Version

11g, 12c, 19c

## Database Configuration

### Create an MDB User

#### 11g

Log in to the Oracle source database as an administrator to create a user for logging in to MDB.

Log in as an administrator:

```shell
sqlplus / as sysdba
```

Create a user:

```sql
-- Optional: Create a tablespace of an MDB user (The tablespace name and path need to be modified based on the actual requirement)
CREATE TABLESPACE mdb_tbs DATAFILE '/u01/app/oracle/oradata/ORCL/mdb_tbs.dbf' SIZE 1024M AUTOEXTEND OFF;

-- Mandatory: Create an MDB user (The username and password need to be modified based on the actual requirement)
CREATE USER mdb_user IDENTIFIED BY "Enmo@123" DEFAULT TABLESPACE mdb_tbs QUOTA UNLIMITED ON mdb_tbs;
```

Grant the user permission:

```sql
GRANT CREATE SESSION TO mdb_user;
GRANT FLASHBACK ANY TABLE TO mdb_user;
GRANT SELECT ANY TABLE TO mdb_user;
GRANT SELECT_CATALOG_ROLE TO mdb_user;
GRANT EXECUTE_CATALOG_ROLE TO mdb_user;
GRANT SELECT ANY TRANSACTION TO mdb_user;

GRANT CREATE TABLE TO mdb_user;
GRANT LOCK ANY TABLE TO mdb_user;
GRANT CREATE SEQUENCE TO mdb_user;

GRANT EXECUTE ON DBMS_LOGMNR TO mdb_user;
GRANT EXECUTE ON DBMS_LOGMNR_D TO mdb_user;

GRANT SELECT ON V_$DATABASE to mdb_user;
GRANT SELECT ON V_$LOG TO mdb_user;
GRANT SELECT ON V_$LOG_HISTORY TO mdb_user;
GRANT SELECT ON V_$LOGMNR_LOGS TO mdb_user;
GRANT SELECT ON V_$LOGMNR_CONTENTS TO mdb_user;
GRANT SELECT ON V_$LOGMNR_PARAMETERS TO mdb_user;
GRANT SELECT ON V_$LOGFILE TO mdb_user;
GRANT SELECT ON V_$ARCHIVED_LOG TO mdb_user;
GRANT SELECT ON V_$ARCHIVE_DEST_STATUS TO mdb_user;
GRANT SELECT ON V_$TRANSACTION TO mdb_user;
```

#### 12c 19c

Log in to the Oracle source database as an administrator to create a user for logging in to MDB.

Log in as an administrator:

```shell
sqlplus / as sysdba
```

Create a user:

```sql
-- Optional: Create a tablespace of an MDB user (The tablespace name and path need to be modified based on the actual requirement)
-- Connect to the CBD container by default
CREATE TABLESPACE mdb_tbs DATAFILE '/opt/oracle/oradata/ORCLCDB/mdb_tbs.dbf' SIZE 1024M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;

-- Switch the connection to the PDB container, pdb_name:ORCLPDB1
alter session set container=ORCLPDB1; 
CREATE TABLESPACE mdb_tbs DATAFILE '/opt/oracle/oradata/ORCLCDB/ORCLPDB1/mdb_tbs.dbf' SIZE 1024M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;

-- Mandatory: Create an MDB user (The username and password need to be modified based on the actual requirement)
conn / as sysdba
CREATE USER c##mdbuser IDENTIFIED BY "Enmo@123" DEFAULT TABLESPACE mdb_tbs QUOTA UNLIMITED ON mdb_tbs CONTAINER=ALL;
alter user c##mdbuser set container_data=all container=current;
```

Grant the user permission:

```sql
GRANT CREATE SESSION TO c##mdbuser CONTAINER=ALL;
GRANT SET CONTAINER TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$DATABASE to c##mdbuser CONTAINER=ALL;
GRANT FLASHBACK ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT_CATALOG_ROLE TO c##mdbuser CONTAINER=ALL;
GRANT EXECUTE_CATALOG_ROLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ANY TRANSACTION TO c##mdbuser CONTAINER=ALL;
GRANT LOGMINING TO c##mdbuser CONTAINER=ALL;

GRANT CREATE TABLE TO c##mdbuser CONTAINER=ALL;
GRANT LOCK ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT CREATE SEQUENCE TO c##mdbuser CONTAINER=ALL;

GRANT EXECUTE ON DBMS_LOGMNR TO c##mdbuser CONTAINER=ALL;
GRANT EXECUTE ON DBMS_LOGMNR_D TO c##mdbuser CONTAINER=ALL;

GRANT SELECT ON V_$LOG TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOG_HISTORY TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_LOGS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_CONTENTS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_PARAMETERS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGFILE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$ARCHIVED_LOG TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$ARCHIVE_DEST_STATUS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$TRANSACTION TO c##mdbuser CONTAINER=ALL;
```

### Archive Setting

#### Check the Archive Path

Use sysdba to connect the database. Run the following commands to check the database archive mode:

```sql
COL DEST_NAME FOR A33
COL DESTINATION FOR A55
COL ERROR FOR A55
SET LINES 188 PAGES 111
SELECT DEST_NAME, DESTINATION, ERROR
  FROM V$ARCHIVE_DEST
 WHERE STATUS = 'VALID';
```

The output is as follows (the archive path is displayed, the destination value may be different, and no error information is displayed)

```text
DEST_NAME           DESTINATION                ERROR
------------------- -------------------------- ------------------
LOG_ARCHIVE_DEST_1  USE_DB_RECOVERY_FILE_DEST
```

#### Set the Archive Information

If the source database does not have the archive mode set (the archive path is empty),    you need to manually enable the archive mode of the source database.

Use sqlplus to log in to the database as an administrator.

```shell
sqlplus / as sysdba
```

Set the archive path (choose one of the following two):

```sql
-- Use FRA (automatic management)
alter system set db_recovery_file_dest_size=100G;
alter system set db_recovery_file_dest='/opt/oracle/oradata/recovery_area' scope=spfile;

-- Use a manually specified archive path
alter system set LOG_ARCHIVE_DEST_1='LOCATION=/u01/app/oracle/archivelog' scope=spfile;
alter system set LOG_ARCHIVE_DEST_STATE_1='enable' scope=spfile;
```

Enable the archive mode (this needs to restart the Oracle database on the source node).

```sql
-- Restart the database to the mount mode.
shutdown immediate
startup mount

-- Enable the archive mode.
alter database archivelog;

-- Open the database.
alter database open;

-- Check the archive mode.
archive log list

-- The output is as follows: 
-- Database log mode              Archive Mode  -- The archive mode has been enabled. 
-- Automatic archival             Enabled
-- Archive destination            USE_DB_RECOVERY_FILE_DEST
-- Oldest online log sequence     49609
-- Next log sequence to archive   49613
-- Current log sequence           49613
```

### Enable the Supplementary Log

Enable the database-level minimum supplementary log.

```sql
ALTER DATABASE ADD SUPPLEMENTAL LOG DATA;
```

For the tables to be synchronized, enable the more comprehensive supplementary log.

```sql
ALTER TABLE inventory.customers ADD SUPPLEMENTAL LOG DATA (ALL) COLUMNS;
```

### CDB and PDB

12C and later, If CDB is enabled, the system prompts you to enter the PDB name when adding a node(Note: All PDB must be enabled to ensure successful data synchronization).

### Enable the Heartbeat Configuration

Sending a heartbeat message enables the connector to send the latest retrieved LSN to offset to avoid offset timeout and keep offset synchronization.

To enable the heartbeat configuration, see [Channel-related Advanced Parameters](./channel_desc.md#Channel-Advanced Parameter). You need to set `heartbeatIntervalMs` to a positive integer.

## Data Type Mapping

| Source Database | Target Database | Source Database Type | Target Database Type |
|--------|-----------|----------|-----------|
| Oracle | MogDB| NCLOB| text                     |
| Oracle | MogDB| BLOB| bytea                    |
| Oracle | MogDB| FLOAT| real                     |
| Oracle | MogDB| NUMBER| numeric                  |
| Oracle | MogDB| CLOB| text                     |
| Oracle | MogDB| RAW| bytea                    |
| Oracle | MogDB| CHAR| character                |
| Oracle | MogDB| JSON| json                     |
| Oracle | MogDB| VARCHAR2| character varying        |
| Oracle | MogDB| NCHAR| character                |
| Oracle | MogDB| INTERVAL DAY TO SECOND| interval day to second   |
| Oracle | MogDB| NVARCHAR2| character varying        |
| Oracle | MogDB| DATE| timestamp                |
| Oracle | MogDB| TIMESTAMP WITH LOCAL TIME ZONE| timestamp with time zone |
| Oracle | MogDB| XMLTYPE| xml                      |
| Oracle | MogDB| LONG RAW| bytea                    |
| Oracle | MogDB| TIMESTAMP WITH TIME ZONE| timestamp with time zone |
| Oracle | MogDB| INTERVAL YEAR TO MONTH| interval year to month   |
| Oracle | MogDB| BINARY_FLOAT| real                     |
| Oracle | MogDB| TIMESTAMP| timestamp                |
| Oracle | MogDB| BFILE| bytea                    |
| Oracle | MogDB| BINARY_DOUBLE| double precision         |
| Oracle | MogDB| LONG| text                     |

The character set mapping supports the following.

## Character Set Mapping

| Source Database | Target Database | Character Set of the Source Database | Character Set of the Target Database |
|--------|----------------|----------|-----------|
|Oracle| MogDB| ZHT16BIG5      |BIG5|
|Oracle| MogDB| JA16EUC        |EUC_JP|
|Oracle| MogDB| JA16EUCTILDE   |EUC_JIS_2004|
|Oracle| MogDB| ZHT32EUC       |EUC_TW|
|Oracle| MogDB| ZHS32GB18030   |GB18030|
|Oracle| MogDB| ZHS16GBK       |GBK|
|Oracle| MogDB| CL8ISO8859P5   |ISO_8859_5|
|Oracle| MogDB| AR8ISO8859P6   |ISO_8859_6|
|Oracle| MogDB| EL8ISO8859P7   |ISO_8859_7|
|Oracle| MogDB| IW8ISO8859P8   |ISO_8859_8|
|Oracle| MogDB| KO16KSCCS      |JOHAB|
|Oracle| MogDB| CL8KOI8R       |KOI8R|
|Oracle| MogDB| CL8KOI8U       |KOI8U|
|Oracle| MogDB| WE8ISO8859P1   |LATIN1|
|Oracle| MogDB| EE8ISO8859P2   |LATIN2|
|Oracle| MogDB| SE8ISO8859P3   |LATIN3|
|Oracle| MogDB| NEE8ISO8859P4  |LATIN4|
|Oracle| MogDB| WE8ISO8859P9   |LATIN5|
|Oracle| MogDB| NE8ISO8859P10  |LATIN6|
|Oracle| MogDB| BLT8ISO8859P13 |LATIN7|
|Oracle| MogDB| CEL8ISO8859P14 |LATIN8|
|Oracle| MogDB| WE8ISO8859P15  |LATIN9|
|Oracle| MogDB| JA16SJIS       |SJIS|
|Oracle| MogDB| JA16SJISTILDE  |SHIFT_JIS_2004|
|Oracle| MogDB| US7ASCII       |SQL_ASCII|
|Oracle| MogDB| KO16MSWIN949   |UHC|
|Oracle| MogDB| AL32UTF8       |UTF8|
|Oracle| MogDB| RU8PC866       |WIN866|
|Oracle| MogDB| EE8MSWIN1250   |WIN1250|
|Oracle| MogDB| CL8MSWIN1251   |WIN1251|
|Oracle| MogDB| WE8MSWIN1252   |WIN1252|
|Oracle| MogDB| EL8MSWIN1253   |WIN1253|
|Oracle| MogDB| TR8MSWIN1254   |WIN1254|
|Oracle| MogDB| IW8MSWIN1255   |WIN1255|
|Oracle| MogDB| AR8MSWIN1256   |WIN1256|
|Oracle| MogDB| BLT8MSWIN1257  |WIN1257|
|Oracle| MogDB| VN8MSWIN1258   |WIN1258|
