---
title: Target Database Description
summary: Target Database Description
author: tianzijian
date: 2022-06-30
---

# Target Database Description

## User Permission

Assume that mdb_user is the login user of the target MDB database. You need to change the user name in the following commands based on the actual environment.

### Oracle

Oracle is a target database and the DBA role is granted to the MDB user.

```sql
grant dba to mdb_user;
```

### MySQL

MySQL is a target database and grant all permissions to the MDB user.

```sql
GRANT ALL PRIVILEGES ON *.* TO "mdb_user"@"%" IDENTIFIED BY "Enmo@123" WITH GRANT OPTION; 
```

### PostgreSQL

PostgreSQL is a target database and the superuser permission is granted to the MDB user.

```sql
alter role mdb_user with superuser;
```

### SQL Server

SQL Server is a target database and the DBA role is granted to the MDB user.

```sql
exec sp_addrolemember 'db_owner','mdb_user';
```

### MogDB

MogDB is a target database and the sysadmin permission is granted to the MDB user.

```sql
alter user mdb_user with sysadmin;
```

When creating a MogDB database, you must specify the compatible database type. By default, it is compatible with A.

```sql
create database db1 DBCOMPATIBILITY='A';
```

| Source Database | Target Database  | Compatible Value|
 |--------|-----|----------|
| Oracle|MogDB| A   |
| MySQL|MogDB| B   |
| PostgreSQL|MogDB| PG  |
