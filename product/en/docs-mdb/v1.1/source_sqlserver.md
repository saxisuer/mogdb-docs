---
title: SQL Server as a Source Database
summary: SQL Server as a Source Database
author: tianzijian
date: 2022-11-23
---

# SQL Server as a Source Database

## Supported Version

SQL Server 2016 Service Pack 1 (SP1) and later

## Database Configuration

### Set User Permission

User Permission for enable CDC on the SQL Server database：

1、You are a member of the sysadmin fixed server role for the SQL Server.</br>
2、You are a db_owner of the database.

### Enabling CDC on the SQL Server database

1、From the View menu in SQL Server Management Studio, click Template Explorer.</br>
2、In the Template Browser, expand SQL Server Templates.</br>
3、Expand Change Data Capture > Configuration and then click Enable Database for CDC.</br>
4、In the template, replace the database name in the USE statement with the name of the database that you want to enable for CDC.</br>
5、Run the stored procedure sys.sp_cdc_enable_db to enable the database for CDC.

The following example shows how to enable CDC for the database MyDB:

```sql
    USE MyDB -- MyDB: database name
    GO
    EXEC sys.sp_cdc_enable_db
    GO
```

### Enabling CDC on a SQL Server table

1、From the View menu in SQL Server Management Studio, click Template Explorer.</br>
2、In the Template Browser, expand SQL Server Templates.</br>
3、Expand Change Data Capture > Configuration, and then click Enable Table Specifying Filegroup Option.</br>
4、In the template, replace the table name in the USE statement with the name of the table that you want to capture.</br>
5、Run the stored procedure sys.sp_cdc_enable_table.

The following example shows how to enable CDC for the table MyTable:

```sql
    USE MyDB -- MyDB: Enable CDC for database name
    GO

    EXEC sys.sp_cdc_enable_table
    @source_schema = N'mySchema', -- mySchema: schema name
    @source_name   = N'myTable',  -- myTable: table name
    @role_name     = NULL,  
    @supports_net_changes = 0
    GO
```

### Online schema updates

An update was committed to the schema of a SQL Server table that has CDC enabled.

Modify the schema of the mySchema.myTable source table by running the following query to add the phone_number field:

```sql
ALTER TABLE myTable ADD phone_number VARCHAR(32);
```

1、Create the new capture instance by running the sys.sp_cdc_enable_table stored procedure.

```sql
    EXEC sys.sp_cdc_enable_table 
    @source_schema = 'mySchema', -- schema
    @source_name = 'myTable',  -- table
    @role_name = NULL, 
    @supports_net_changes = 0, 
    @capture_instance = 'mySchema_myTable_v2'; -- new capture_instance not be repeated with the old，default:mySchema_myTable
```

2、Drop the old capture instance by running the sys.sp_cdc_disable_table stored procedure.

```sql
    EXEC sys.sp_cdc_disable_table 
    @source_schema = 'mySchema', -- schema
    @source_name = 'myTable',  -- table
    @capture_instance = 'mySchema_myTable'; -- default:mySchema_myTable
    GO
```

## Data Type Mapping

| Source Database | Target Database | Source Database Type | Target Database Type |
|--------|-----------|----------|-----------|
|SQL Server|MogDB|bigint|bigint|
|SQL Server|MogDB|numeric|numeric|
|SQL Server|MogDB|bit|bytea|
|SQL Server|MogDB|smallint|smallint|
|SQL Server|MogDB|decimal|numeric|
|SQL Server|MogDB|smallmoney|numeric|
|SQL Server|MogDB|tinyint|tinyint|
|SQL Server|MogDB|money|numeric|
|SQL Server|MogDB|float|float|
|SQL Server|MogDB|real|real|
|SQL Server|MogDB|date|date|
|SQL Server|MogDB|datetime|timestamp|
|SQL Server|MogDB|datetime2|timestamp|
|SQL Server|MogDB|datetimeoffset|timestamptz|
|SQL Server|MogDB|smalldatetime|smalldatetime|
|SQL Server|MogDB|time|time|
|SQL Server|MogDB|char|character|
|SQL Server|MogDB|varchar|character varying|
|SQL Server|MogDB|text|text|
|SQL Server|MogDB|nchar|char|
|SQL Server|MogDB|nvarchar|character varying|
|SQL Server|MogDB|ntext|text|
|SQL Server|MogDB|binary|bytea|
|SQL Server|MogDB|varbinary|bytea|
|SQL Server|MogDB|image|bytea|
|SQL Server|MogDB|xml|text|
|SQL Server|MogDB|geography|bytea|
|SQL Server|MogDB|geometry|bytea|
|SQL Server|MogDB|rowversion|bytea|
|SQL Server|MogDB|hierarchyid|bytea|
|SQL Server|MogDB|uniqueidentifier|text|
|SQL Server|MogDB|sql_variant|text|
|SQL Server|MogDB|sysname|character varying|
|SQL Server|MogDB|int|integer|

The character set mapping supports the following.

## Character Set Mapping

| Source Database | Target Database | Character Set of the Source Database | Character Set of the Target Database |
 |--------|----------------|----------|-----------|
| SQL Server|MogDB|            |UTF8|
