---
title: MDB Installation
summary: MDB Installation
author: tianzijian
date: 2022-06-30
---

# MDB Installation

## Requirements

The system in which MDB is to be deployed must meet the following requirement.

- Server: Linux x86_64
- Repository: MogDB
- Disk space: 500 G

## Supported Version

| Software | Version                                    |
| :----- |:-------------------------------------------|
| Java   | 11+                                        |
| MogDB  | Database: 2.0+  Plug-ins: wal2json         |
| Oracle | 11g, 12c, 19c                    |
| MySQL  | 5.7, 8.0.x (not support MariaDB) |
|PostgreSQL|Database: 9.6, 10, 11, 12, 13, 14 Plug-ins: decoderbufs, wal2json|
|SQL Server|22016 Service Pack 1 (SP1) and later|

## Installation Procedure

### Download Software

Download the MDB software package of the latest version. To obtain the software package, contact the sales personnel.

### Upload and Decompress the Software Package

Upload the MDB software package to the server and decompress it.

```shell
tar -zxvf mdb_v1.0.0.RELEASE_linux_x86_64.tar.gz
```

### Prepare a Repository

The MDB repository must use a MogDB (openGauss) database, and the version cannot be earlier than 2.0.

For details about how to install MogDB, see [MogDB Single Node Installation](https://docs.mogdb.io/en/mogdb/v2.1/installation-on-a-single-node).

After MogDB is installed, use gsql to log in to the database as a super administrator.

```shell
gsql -r
```

In MogDB, create an MDB repository and an MDB repository user.

```sql
-- Create a repository user with the password changed based on the actual requirement.
create user mdb_repo with password 'MDB@password' login;

-- Create a repository with the repository owner that must be set to mdb_repo.
create database mdb_repo_db with owner mdb_repo;
```

### Check the Java Version

MDB depends on  Java 11. Therefore, make sure that the Java version is not earlier than 11 in the current server.

```shell
java -version
```

If the Java version is earlier, you need to visit the following Oracle official website to download the JDK/JRE software package corresponding to Java 11.

<https://www.oracle.com/java/technologies/downloads/#java11>

Download the  jdk-11.0.15.1_linux-x64_bin.tar.gz  software package, and upload it to the server and decompress it.

```shell
tar -zxvf jdk-11.0.15.1_linux-x64_bin.tar.gz

# Obtain the current path: /usr/local
pwd 

# Configure environment variables. 
vi ~/.bash_profile

# Replace the JAVA_HOME path and then add the following content to the end of the file. 
export JAVA_HOME=/usr/local/jdk-11.0.15
export CLASSPATH=$:CLASSPATH:$JAVA_HOME/lib/
export PATH=$PATH:$JAVA_HOME/bin

# Save the modification and run the following command to update the configuration file. 
source ~/.bash_profile
```

Confirm the Java version and make sure that Java 11 is used.

```shell
java -version

# The following shows the command output: 
# --------------
# java version "11.0.15" 2022-04-19 LTS
# Java(TM) SE Runtime Environment 18.9 (build 11.0.15+8-LTS-149)
# Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.15+8-LTS-149, mixed mode)
```

### Check the Web Port

MDB Server requires a web listening port (default: 55437). During installation, make sure that the port is available.

```shell
netstat -anp | grep LISTEN | grep 55437    # Make sure that no process is in the listening status.
```

### Check the Residual File

For ZooKeeper  and kafka installation, make sure that the `<tempFilePath>/zookeeper` and `<tempFilePath>/kafka-logs` directories do not exist. If they are residual files, run the following commands to delete them.

```shell
rm -rf <tempFilePath>/zookeeper # <tempFilePath>: MDB configuration item, default: /tmp
rm -rf <tempFilePath>/kafka-logs
```

### Modify Installation Settings

Switch to the decompressed MDB software package directory and check the file integrity.

```shell
cd mdb_v1.0.0.RELEASE_linux_x86_64

ls -ltr
```

The following describes the software package files.

| File | Description |
|-|-|
| kafka.zip | Kafaka installation package used for synchronizing middle data queue |
| mdb | MDB command execution file |
| mdb-data-worker.zip | Software package related to data collection and delivery tasks |
| mdb-server.zip | MDB main software package |
| config.json | Installation and configuration file |

Modify the `config.json` configuration file.

```json
{
  "mdbServerPort": 55437,
  "dbHost": "127.0.0.1",
  "dbPort": 26000,
  "dbUser": "mdb_repo",
  "dbPassword": "MDB@password",
  "dbName": "mdb_repo_db",
  "tempFilePath": "/tmp",
  "tempFileMaxSize": 0,
  "tempFileRetentionTime": 168
 }
```

The following describes the configuration items.

| Configuration Item | Default Value | Description |
|-|-|-|
| mdbServerPort | 55437 | MDB server port <br />Used for access the web page. Make sure that the network between the MDB server and the web client is normal. |
| dbHost | 127.0.0.1 | Listening IP address of the repository |
| dbPort | 26000 | Listening server port of the repository |
| dbUser | mdb_repo | MDB repository user |
| dbPassword | MDB@password | MDB repository password |
| dbName | mdb_repo_db | MDB repository database name |
| tempFilePath | /tmp | MDB temporary file saving path |
| tempFileMaxSize | 0 | MDB temporary file size (unit: GB). Default value 0: Unlimited size |
| tempFileRetentionTime | 168 | Retention time of MDB temporary files (unit: hour) |

### Install MDB

Check and make sure that you have switched to the MDB software package directory.

```shell
pwd         # Make sure that you have switched to the mdb_v1.0.0.RELEASE_linux_x86_64 directory.
```

Install MDB.

```shell
./mdb install
```

The output is as follows:

```text
   2022-06-23 14:08:24  ======BEGIN TO INSTALL MDB======
   2022-06-23 14:08:24    1.CHECK MDB FILE
   2022-06-23 14:08:24    2.CHECK MDB ENVIRONMENT
   2022-06-23 14:08:25     JAVA_VERSION: 11.0
   2022-06-23 14:08:25    3.UNZIP KAFKA
   2022-06-23 14:08:26    4.INSTALL MDB SERVER
   2022-06-23 14:08:26    5.INSTALL MDB DATA WORKER
   2022-06-23 14:08:26

   ======END INSTALL MDB RESULT======
   +------------+-----------+-------------+---------------+
   | File_Check | Env_Check | MDB_Install | KAFKA_INSTALL |
   +------------+-----------+-------------+---------------+
   |  Success   |  Success  |   Success   |    Success    |
   +------------+-----------+-------------+---------------+

```

### Apply for a LICENSE

Apply for a license.

```shell
 ./mdb generate-licence test@enmotech.com      #test@enmotech.com is a email address for receiving the license file which needs to be uploaded to the server. 
```

The output is as follows:

```text
   2022-07-07 14:06:22  The email has been successfully sent to test@enmotech.com. Please check your email. 
```

### Apply the LICENSE to the Server

Apply the license to the server.

```shell
 ./mdb apply-license /u01/mdb/license.json      #/u01/mdb/license.json is the path of the license file on the server.
```

The output is as follows:

```text
   2022-07-04 11:26:01  apply license success
```

### Start MDB

Start MDB.

```shell
 ./mdb start
```

The output is as follows:

```text
   2022-06-23 11:33:10  start zookeeper success
   2022-06-23 11:33:21  start kafka success
   2022-06-23 11:33:32  start connector success
   2022-06-23 11:33:42  start mdb-server success
   2022-06-23 11:33:42  After MDB is started, the the ports occupied by programs are as follows: 
   +-------+-----------+-----------+------------+
   | KAFKA | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +-------+-----------+-----------+------------+
   |  9092 |    2181   |    8083   |    55437    |
   +-------+-----------+-----------+------------+
   2022-06-23 11:33:42  MDB program service status
   +--------+-----------+-----------+------------+
   | KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +--------+-----------+-----------+------------+
   | online |   online  |   online  |   online   |
   +--------+-----------+-----------+------------+
```

Start child service command:

```shell
 ./mdb start --help #  --server_name TEXT  Optional service name: zookeeper、kafka、connector、mdb_server
 ./mdb start --server_name zookeeper 
 ./mdb start --server_name kafka 
 ./mdb start --server_name connector 
 ./mdb start --server_name mdb_server 
```

### Check the MDB Server Status

Check the MDB server status.

```shell
 ./mdb status
```

The output is as follows:

```text
   2022-06-23 14:06:39  MDB program service status
   +---------+-----------+-----------+------------+
   |  KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +---------+-----------+-----------+------------+
   | offline |  offline  |  offline  |  offline   |
   +---------+-----------+-----------+------------+
```

### Stop MDB

Stop MDB.

```shell
 ./mdb stop
```

The output is as follows:

```text
   2022-06-23 14:02:52  stop connector success
   2022-06-23 14:03:02  stop kafka success
   2022-06-23 14:03:12  stop zookeeper success
   2022-06-23 14:03:22  stop mdb-server success
   2022-06-23 14:03:22  stop mdb-data-worker success
   2022-06-23 14:03:22  MDB program service status
   +---------+-----------+-----------+------------+
   |  KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +---------+-----------+-----------+------------+
   | offline |  offline  |  offline  |  offline   |
   +---------+-----------+-----------+------------+
```

Stop child service command:

```shell
 ./mdb stop --help #  --server_name TEXT  Optional service name: zookeeper、kafka、connector、mdb_server
 ./mdb stop --server_name zookeeper 
 ./mdb stop --server_name kafka 
 ./mdb stop --server_name connector 
 ./mdb stop --server_name mdb_server 
```

### Apply Configuration

When the config.json configuration file is modified, run the following command to update configuration files of all services. Before update, make sure that the service is offline (You can perform the stop command to make the service offline).

Apply the configuration.

```shell
 ./mdb apply-config  
```

The output is as follows:

```text
   2022-08-08 14:54:34  apply config success!
```

### Log collection

Generate a dump.zip file in the current directory.

Log collection.

```shell
 ./mdb dump
```

The output is as follows:

```text
   2022-09-23 10:33:46  dump log success
```

### Login to MDB

After MDB is started, access the client browser (Google  Chrome is recommended) to check whether the MDB interface is normal.

<http://localhost:55437/mdb/index.html>

- Initial username: test@enmotech.com
- Initial password: Enmo@123

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/install-en.png)
