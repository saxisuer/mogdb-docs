---
title: MTK Limit
summary: MTK Limit
author: mtk
date: 2022-09-26 11:15:35
---

# MTK Limit

## Limit

Limit concurrency configuration

| Field | Type | Description |
|-------|------|-------------|
| [parallel](#parallel)|int | Degree of parallelism. |
| [fetchSize](#fetchsize) |int |Specifies the number of rows to fetch at a time. |
| [batchSize](#batchsize) |int |Specify the size of batch insert or copy and Commit. |
| [bufferSize](#buffersize) |int |Specifies the buffer size (in MB) when querying or inserting in batches. |
| [cpBufferSize](#cpbuffersize) |int |Defines the buffer size (in MB) used in the Copy command. |
| [oracleSelectParallel](#oracleselectparallel) |int |Add hint `/* +parallel(t,n) */` |
| [limit](#limit) |int64 |Defines how many rows to migrate per table. |

Appears in:

- <code><a href="#config">Config</a>.limit</code>

**Example**:

Limit example

```json
{
  "parallel": 2,
  "fetchSize": 1000,
  "batchSize": 1000,
  "bufferSize": 8,
  "cpBufferSize": 8,
  "oracleSelectParallel": 2,
  "channelCacheNum": 10000,
  "limit": 0
}
```

### parallel

**Type**: int

**Desc**: Indicates the degree of parallelism.

Create tables, constraints, etc. in parallel..

**Default**: 1

### fetchSize

**Type**: int

**Desc**: Specifies the row size for one row fetch.

Valid values ​​are 1-50000

Oracle support.

**Default**: 1000

### batchSize

**Type**: int

**Desc**: Specify the size of batch insert or Copy and make Commit.

Versions after 2.3.4 began to support batch Commit. The previous version only interacted with data in batches without committing

If the submission fails the program will exit and log the error data to the error file.

If you configure [igErrorData](./mtk-parameter.md#igerrordata) the program will not exit and will log the error data to the error file and continue processing the data.

Valid values ​​are 1-50000

Support Oracle, PostgreSQL, openGauss, MySQL.

Batch commit supports PostgreSQL/MogDB,MySQL

**Default**: 1000

### bufferSize

**Type**: int

**Desc**: Specifies the cache size (in MB) when querying or bulk inserting.

Valid values ​​are 1-1024

Support PostgreSQL, openGauss, MySQL.

**Default**: 8 MB

### cpBufferSize

**Type**: int

**Desc**: Defines the cache size (in MB) used in the Copy command.

Valid values ​​are 1-1024

Support PostgreSQL, openGauss, MySQL

**Default**: 8 MB

### oracleSelectParallel

**Type**: int

**Desc**: Add hint `/* +parallel(t,n) */` to Oracle query statement

### limit

**Type**: int64

**Desc**: Define how many rows to migrate per table.
Defining this parameter no longer does table concurrent migrations.
