---
title: MTK Configuration File Description
summary: Configuration File Description
author: mtk
date: 2022-06-19 18:23:32
---

# MTK Configuration File Description

## Config

Config MTK迁移配置文件.

| Field | Type | Description |
|-------|------|-------------|
| [source](#option)  |Option |Source Database configuration information. |
| [target](#option)  |Option |Target Database configuration information. |
| [limit](./mtk-limit.md#limit)</a> |Limit |Concurrency configuration. |
| [object](./mtk-object.md#object) |Object |Object definition. |
| [dataOnly](#dataonly) |bool |Whether only data is migrated. |
| [schemaOnly](#schemaonly) |bool |Whether only the data structure is migrated. |
| [disableTableDataComp](#disabletabledatacomp) |bool |Disabling table data comparison. |
| [disableCollStatistics](#disablecollstatistics)  |bool |Disabling collection statistics. |
| [reportFile](#reportfile) |string |Migration report directory. |
| [debug](#debug)  |bool |debug. |
| [preRun](#prerun)  |bool |preRun. |
| [test](#test)  |bool |test. |
| [disableIgnoreCase](#disableignorecase) |bool |Disables ignoring case queries. |
| [disableSelectPart](#disableselectpart) |bool |Disable the select by partition. |
| [disableFKCons](#disablefkcons) |bool |Disable table foreign key sync. |
| [disableSyncIdxAfterData](#disablesyncidxafterdata) |bool | disable the synchronization of table data and create this table index immediately|
| [disablePrintMigDataProgress](#disableprintmigdataprogress) |bool | disable printing migration data progress |

**Example**:

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "127.0.0.1",
      "user": "system",
      "port": 1521,
      "password": "******",
      "dbName": "orcl"
    }
  },
  "target": {
    "type": "opengauss",
    "connect": {
      "version": "3.0.0",
      "host": "127.0.0.1",
      "user": "gaussdb",
      "port": 26000,
      "password": "******",
      "dbName": "postgres"
    },
    "parameter": {
      "parallelInsert": 1,
      "dropExistingObject": false,
      "truncTable": false,
      "colKeyWords": {},
      "objKeyWords": {},
      "caseSensitive": 0,
      "quoteMark": false,
      "path": "./data",
      "schemaPath": "",
      "dataPath": "",
      "errDataPath": "",
      "fileType": "",
      "fileSize": "",
      "csvHeader": false,
      "csvNullValue": "",
      "csvFieldDelimiter": ",",
      "csvOptionallyEnclosed": "\"",
      "excludeSysTable": [],
      "remapSchema": {},
      "remapTable": {},
      "remapTablespace": {},
      "enableSyncTabTbsPro": false,
      "enableSyncCompTabPro": false,
      "timeFormat": "HH:MI:SS",
      "dateFormat": "YYYY-MM-DD",
      "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
      "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
      "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
      "noSupportPartTabToNormalTab": false,
      "ignoreDB2PartInclusive": false,
      "igNotSupportIntervalPart": false,
      "igErrorData": false,
      "enableBatchCommit": false,
      "ignoreTabPartition": false,
      "autoAddMaxvaluePart": false,
      "autoAddMySQLAutoIncr": false,
      "autoAddMySQLAutoIncrTabList": [],
      "ignoreNotSupportDefault": false,
      "replaceZeroDate": "",
      "virtualColToNormalCol": false,
      "virtualColConv": {},
      "mySQLSkipErrorDateTimeData": false,
      "ignoreTableDDLCompErr": false,
      "convertPackageMethod": "",
      "convertOracleIntegerToNumeric": false,
      "enableOgBlobClob": false,
      "enableConvertSrid": false,
      "defaultSrid": "4326",
      "seqLastNumAddNum": 0,
      "skipColumnType": {},
      "skipColumnName": {},
      "templateSeqName": "",
      "charAppendEmptyString": false,
      "tableOptions": {},
      "indexOptions": {}
    }
  },
  "limit": {
    "parallel": 2,
    "fetchSize": 1000,
    "batchSize": 1000,
    "bufferSize": 8,
    "cpBufferSize": 8,
    "oracleSelectParallel": 2,
    "channelCacheNum": 100000,
    "limit": 0
  },
  "object": {
    "tables": [
      "MTK.TAB1",
      "MTK.TAB111%",
      "MTK1.*"
    ],
    "schemas": [
      "SCHEMA1",
      "SCHEMA2"
    ],
    "excludeTable": {
      "SCHEMA1": [
        "TABLE_SKIP1",
        "TABLE_SKIP1",
        "TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG.*$"
      ],
      "SCHEMA2": [
        "TABLE_SKIP1",
        "TABLE_SKIP1"
      ]
    },
    "tableSplit": {
      "SCHEMA1": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      },
      "SCHEMA2": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      }
    }
  },
  "dataOnly": false,
  "schemaOnly": false,
  "disableTableDataComp": false,
  "disableCollStatistics": false,
  "reportFile": "./report/",
  "debug": false,
  "preRun": false,
  "test": false,
  "disableIgnoreCase": false,
  "disableSelectPart": false,
  "disableFKCons": false,
  "disableSyncIdxAfterData": false,
  "disablePrintMigDataProgress": false
}
```

### source

**Type**: [`Option`](#option)

**Desc**: Source Database configuration information.

**Example**:

Source Database Config Example

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "127.0.0.1",
      "user": "system",
      "port": 1521,
      "password": "******",
      "dbName": "orcl"
    }
  }
}
```

### target

**Type**: [`Option`](#option)

**Desc**: Target Database configuration information.

**Example**:

Target Database Config Example

```json
{
  "target": {
    "type": "opengauss",
    "connect": {
      "version": "3.0.0",
      "host": "127.0.0.1",
      "user": "gaussdb",
      "port": 26000,
      "password": "******",
      "dbName": "postgres"
    },
    "parameter": {
      "parallelInsert": 1,
      "dropExistingObject": false,
      "truncTable": false,
      "colKeyWords": {},
      "objKeyWords": {},
      "caseSensitive": 0,
      "quoteMark": false,
      "path": "./data",
      "schemaPath": "",
      "dataPath": "",
      "errDataPath": "",
      "fileType": "",
      "fileSize": "",
      "csvHeader": false,
      "csvNullValue": "",
      "csvFieldDelimiter": ",",
      "csvOptionallyEnclosed": "\"",
      "excludeSysTable": [],
      "remapSchema": {},
      "remapTable": {},
      "remapTablespace": {},
      "enableSyncTabTbsPro": false,
      "enableSyncCompTabPro": false,
      "timeFormat": "HH:MI:SS",
      "dateFormat": "YYYY-MM-DD",
      "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
      "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
      "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
      "noSupportPartTabToNormalTab": false,
      "ignoreDB2PartInclusive": false,
      "igNotSupportIntervalPart": false,
      "igErrorData": false,
      "enableBatchCommit": false,
      "ignoreTabPartition": false,
      "autoAddMaxvaluePart": false,
      "autoAddMySQLAutoIncr": false,
      "autoAddMySQLAutoIncrTabList": [],
      "ignoreNotSupportDefault": false,
      "replaceZeroDate": "",
      "virtualColToNormalCol": false,
      "virtualColConv": {},
      "mySQLSkipErrorDateTimeData": false,
      "ignoreTableDDLCompErr": false,
      "convertPackageMethod": "",
      "convertOracleIntegerToNumeric": false,
      "enableOgBlobClob": false,
      "enableConvertSrid": false,
      "defaultSrid": "4326",
      "seqLastNumAddNum": 0,
      "skipColumnType": {},
      "skipColumnName": {},
      "templateSeqName": "",
      "charAppendEmptyString": false,
      "tableOptions": {},
      "indexOptions": {}
    }
  }
}
```

### limit

**Type**: [`Limit`](./mtk-limit.md#limit)

**Desc**: Concurrency configuration.

**Example**:

limit example

```json
{
  "limit": {
    "parallel": 2,
    "fetchSize": 1000,
    "batchSize": 1000,
    "bufferSize": 8,
    "cpBufferSize": 8,
    "oracleSelectParallel": 2,
    "channelCacheNum": 10000,
    "limit": 0
  }
}
```

### object

**Type**: [`Object`](./mtk-object.md#object)

**Desc**: Object definition.

**Example**:

Object Example

```json
{
  "object": {
    "tables": [
      "MTK.TAB1",
      "MTK.TAB111%",
      "MTK1.*"
    ],
    "schemas": [
      "SCHEMA1",
      "SCHEMA2"
    ],
    "excludeTable": {
      "SCHEMA1": [
        "TABLE_SKIP1",
        "TABLE_SKIP1",
        "TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG.*$"
      ],
      "SCHEMA2": [
        "TABLE_SKIP1",
        "TABLE_SKIP1"
      ]
    },
    "tableSplit": {
      "SCHEMA1": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      },
      "SCHEMA2": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      }
    }
  }
}
```

### dataOnly

**Type**: bool

**Desc**: Indicates whether to migrate only data.

**Default**: `false`

**Option**:

- true
- false

### schemaOnly

**Type**: bool

**Desc**: Indicates whether to migration only the data structure.

**Default**: `false`

**Option**:

- true
- false

### disableTableDataComp

**Type**: bool

**Desc**: Disables the table data comparison function.
After data migration is complete, MTK will count and compare the number of rows in the source and target databases.

**Default**: `false`

**Option**:

- true
- false

### disableCollStatistics

**Type**: bool

**Desc**: Disables the collection statistics function.
After data migration is complete, MTK will collect statistics information from the target database.

**Default**: `false`

**Option**:

- true
- false

### reportFile

**Type**: string

**Desc**: Indicates the migration report directory.
For MTK of a version earlier than v0.0.18, the migration report is shown in HTML.
For MTK v0.0.18 or later, the migration report is stored in a directory.

### debug

**Type**: bool

**Desc**: Indicates whether to enable the log debug mode.

**Default**: `false`

**Option**:

- true
- false

### preRun

**Type**: bool

**Desc**: pre Run.

**Default**: `false`

**Option**:

- true
- false

### test

**Type**: bool

**Desc**: Try migration.
Migration parameter `limit.limit` row data. In this mode, the data can be submitted without any data, and the data will be automatically rolled back.
Just to test if the data is inserted normally

**Default**: `false`

**Option**:

- true
- false

### disableIgnoreCase

**Type**: bool

**Desc**: Disables ignoring case queries.

**Default**: `false`

**Option**:

- true
- false

### disableSelectPart

**Type**: bool

**Desc**: Disable the partition table migration data by partition feature.

**Default**: `false`

**Option**:

- true
- false

### disableFKCons

**Type**: bool

**Desc**: Disables synchronization of table foreign key constraints.

**Default**: `false`

**Option**:

- true
- false

### disableSyncIdxAfterData

**Type**: bool

**Desc**: disable the synchronization of table data and create this table index immediately

**Default**: `false`

**Option**:

- true
- false

### disablePrintMigDataProgress

**Type**: bool

**Desc**: disable printing migration data progress

**Default**: `false`

**Deprecated**: Changed the printing progress method, not asynchronous printing progress

**Option**:

- true
- false

## Connect

Connect database connection information

| Field | Type | Description |
|-------|------|-------------|
| [version](#version) |string |Database version. |
| [vendor](#vendor) |string |Database issuer. |
| [host](#host) |string |Database host |
| [user](#user) |string |Database user |
| [port](#port)|int |Database port |
| [password](#password)  |Password |Database User Password |
| [dbName](#dbname) |string |Database name |
| [dsn](#dsn) |string |User-specified connection string. |
| [timeout](#timeout) |Duration |Connection timeout. |
| [charset](#charset)|string |Database character set. |
| [datCompatibility](#datcompatibility) |string |For openGauss database compatibility mode. |
| [sqlMode](#sqlmode)  |string |For MySQL database `sql_mode`. |
| [clientCharset](#clientcharset) |string |It is used for database encoding conversion scenarios, and generally does not need to be set. |

Appears in:

- [`Option`](#option).connect

### version

**Type**: string

**Desc**: Database version.

No need to specify. Automatic query when connecting to database

Migrating into a file requires manually specifying the version

**Example**:

Version Example

```json
{
  "version": "2.1.0"
}
```

### vendor

**Type**: string

**Desc**: Database publisher.

No need to specify. Automatic query when connecting to database

**Example**:

Vendor Example

```json
{
  "vendor": "MySQL"
}
```

### host

**Type**: string

**Desc**: database host

**Option**:

- ip
- 域名

**Example**:

Host Example

```json
{
  "host": "127.0.0.1"
}
```

### user

**Type**: string

**Desc**: 数据库用户

**Example**:

User Example

```json
{
  "user": "system"
}
```

### port

**Type**: int

**Desc**: database port

**Example**:

Port Example

```json
{
  "port": 1521
}
```

### password

**Type**: Password

**Desc**: database user password

### dbName

**Type**: string

**Desc**: Name database

**Example**:

DBName Example

```json
{
  "dbName": "orcl"
}
```

### dsn

**Type**: string

**Desc**: User-specified connection string.

No need to specify by default. Reserve functions for special scenarios

### timeout

**Type**: Duration

**Desc**: connection timeout.

No configuration required

**Default**: 30s

### charset

**Type**: string

**Desc**: database character set.

No configuration by default, connect to database query

**Option**:

- gbk
- utf8

**Example**:

Charset Example

```json
{
  "charset": "gbk"
}
```

### datCompatibility

**Type**: string

**Desc**: Compatibility mode for the openGauss database.

By default, no configuration is required, and the database query is connected.

Migrating into a file requires configuration

**Option**:

- A
- B
- PG

**Example**:

DatCompatibility Example

```json
{
  "datCompatibility": "A"
}
```

### sqlMode

**Type**: string

**Desc**: `sql_mode` for MySQL databases.

By default, no configuration is required, and the database query is connected.

Migrating into a file requires configuration

### clientCharset

**Type**: string

**Desc**: It is used for database encoding conversion scenarios, and generally does not need to be set.

Such as Oracle ZHS16GBK encoding migrated to openGauss UTF8. Encountered `ORA-29275: partial multibyte character`.

## Option

Option database configuration

| Field | Type | Description |
|-------|------|-------------|
| [type](#type)  |string |Database type, case insensitive |
| [connect](#connect)|Connect |Database connection information |
| [parameter](./mtk-parameter.md#parameter) |Parameter |Parameter configuration |

Appears in:

- [`Config`](#config).source
- [`Config`](#config).target

**Example**:

Source Database Config Example

```json
{
  "type": "oracle",
  "connect": {
    "version": "",
    "host": "127.0.0.1",
    "user": "system",
    "port": 1521,
    "password": "******",
    "dbName": "orcl"
  }
}
```

Target Database Config Example

```json
{
  "type": "opengauss",
  "connect": {
    "version": "3.0.0",
    "host": "127.0.0.1",
    "user": "gaussdb",
    "port": 26000,
    "password": "******",
    "dbName": "postgres"
  },
  "parameter": {
    "parallelInsert": 1,
    "dropExistingObject": false,
    "truncTable": false,
    "colKeyWords": {},
    "objKeyWords": {},
    "caseSensitive": 0,
    "quoteMark": false,
    "path": "./data",
    "schemaPath": "",
    "dataPath": "",
    "errDataPath": "",
    "fileType": "",
    "fileSize": "",
    "csvHeader": false,
    "csvNullValue": "",
    "csvFieldDelimiter": ",",
    "csvOptionallyEnclosed": "\"",
    "excludeSysTable": [],
    "remapSchema": {},
    "remapTable": {},
    "remapTablespace": {},
    "enableSyncTabTbsPro": false,
    "enableSyncCompTabPro": false,
    "timeFormat": "HH:MI:SS",
    "dateFormat": "YYYY-MM-DD",
    "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
    "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
    "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
    "noSupportPartTabToNormalTab": false,
    "ignoreDB2PartInclusive": false,
    "igNotSupportIntervalPart": false,
    "igErrorData": false,
    "enableBatchCommit": false,
    "ignoreTabPartition": false,
    "autoAddMaxvaluePart": false,
    "autoAddMySQLAutoIncr": false,
    "autoAddMySQLAutoIncrTabList": [],
    "ignoreNotSupportDefault": false,
    "replaceZeroDate": "",
    "virtualColToNormalCol": false,
    "virtualColConv": {},
    "mySQLSkipErrorDateTimeData": false,
    "ignoreTableDDLCompErr": false,
    "convertPackageMethod": "",
    "convertOracleIntegerToNumeric": false,
    "enableOgBlobClob": false,
    "enableConvertSrid": false,
    "defaultSrid": "4326",
    "seqLastNumAddNum": 0,
    "skipColumnType": {},
    "skipColumnName": {},
    "templateSeqName": "",
    "charAppendEmptyString": false,
    "tableOptions": {},
    "indexOptions": {}
  }
}
```

### type

**Type**: string

**Desc**: database type, case insensitive

**Option**:

- MySQL
- Oracle
- Postgres/PostgreSQL
- openGauss
- MogDB
- DB2
- sqlServer
- file
- informix

**Example**:

Type Example

```json
{
  "type": "Oracle"
}
```

### connect

**Type**: [`Connect`](#connect)

**Desc**: Database connection information

### parameter

**Type**: [`Parameter`](./mtk-parameter.md#parameter)

**Desc**: parameter configuration
