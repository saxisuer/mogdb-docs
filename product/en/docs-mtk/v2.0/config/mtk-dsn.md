---
title: Introduction to DSN
summary: Introduction to DSN
author: Liu Xu
date: 2021-03-04
---

# Introduction to DSN

## Oracle Cloud Autonomous Database

Normally, there is no need to configure the dsn key value in an MTK configuration file, but directly configure the host, port, user, and password key values.

However, if you need to migrate data from Oracle Cloud Autonomous Database, i.e. the source database is an Autonomous Database instance located in [Oracle Cloud](https://www.oracle.com/), then you need to configure dsn, while other parameters can be left blank.

For details about the operations, see the Oracle official documentations.
[Connecting to an Autonomous Database](https://docs.cloud.oracle.com/en-us/iaas/Content/Database/Tasks/adbconnecting.htm)

The following is the configuration example.

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "",
      "user": "",
      "port": 0,
      "password": "",
      "dbName": "",
      "dsn": "connectString=db202009151510_high user=ADMIN password=your_admin_password"
    }
  }
}
```

Or

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "",
      "user": "ADMIN",
      "port": 0,
      "password": "your_admin_password",
      "dbName": "",
      "dsn": "connectString=db202009151510_high"
    }
  }
}
```

**connectString** indicates the Oracle net service name in the **tnsnames.ora** file.

The **tnsnames.ora** file can be obtained from the Oracle Autonomous Database instance-related web page.

<br/>

Other reference documents:

[Go DRiver for Oracle User Guide](https://godror.github.io/godror/doc/connection.html)

[Oracle Database 19c Easy Connect Plus Configurable Database Connection Syntax](https://download.oracle.com/ocomdocs/global/Oracle-Net-19c-Easy-Connect-Plus.pdf)

```json
{
  "dsn": "user=system password=pwd connectString=\"(DESCRIPTION=(CONNECT_TIMEOUT = 5)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=mtk)))\" configDir= connectionClass=godror enableEvents=0 externalAuth=0 heterogeneousPool=0 libDir= newPassword= noTimezoneCheck=0 poolIncrement=0 poolMaxSessions=100 poolMinSessions=0 poolSessionMaxLifetime=0s poolSessionTimeout=0s poolWaitTimeout=0s prelim=0 standaloneConnection=1 sysasm=0 sysdba=0 sysoper=0 timezone="
}
```

## openGauss/MogDB/postgres

```json
{
  "dsn": "application_name=mtk_postgres connect_timeout=5 dbname=postgres host=127.0.0.1 min_read_buffer_size=8388608 password=pwd port=5434 sslmode=disable user=gaussdb"
}
```

## MySQL

```json
{
  "dsn": "root:pwd@tcp(127.0.0.1:3306)/mysql?loc=Local&multiStatements=true&timeout=30s&maxAllowedPacket=1073741824&charset=utf8mb4"
}
```

## DB2

```json
{
  "dsn": "ClientApplName=MTK;ConnectTimeout=5;DATABASE=testdb;HOSTNAME=127.0.0.1;PORT=50000;PWD=pwd;ProgramName=MTK;UID=db2inst1;XMLDeclaration=0"
}
```

## SQLServer

```json
{
  "dsn": "sqlserver://sa:pwd@127.0.0.1:1433?app+name=mtk&connection+timeout=0&database=testdb"
}
```
