<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# Documentation

## Data Migration

+ [Overview](./overview.md)
+ [Environment](./mtk-env.md)
+ [Quick Start](./mtk-usage.md)
+ Configuration
  + [Config Overview](./config/mtk-config.md)
  + [Object](./config/mtk-object.md)
  + [Limit](./config/mtk-limit.md)
  + [Parameter](./config/mtk-parameter.md)
  + [Data Source Name](./config/mtk-dsn.md)
+ Commands
  + [mtk](./commands/mtk.md)
  + [init-project](./commands/mtk_init-project.md)
  + config
    + [config-check](./commands/mtk_config-check.md)
    + [config-gen](./commands/mtk_config-gen.md)
  + [license](./commands/mtk_license.md)
    + [check](./commands/mtk_license_check.md)
    + [gen](./commands/mtk_license_gen.md)
  + mig
    + [mig-select](./commands/mtk_mig-select.md)
    + [mig-selects](./commands/mtk_mig-selects.md)
    + [mig-tab-pre](./commands/mtk_mig-tab-pre.md)
    + [mig-tab-data](./commands/mtk_mig-tab-data.md)
    + [mig-tab-post](./commands/mtk_mig-tab-post.md)
    + [mig-tab-other](./commands/mtk_mig-tab-other.md)
  + show
    + [show-schema](./commands/mtk_show-schema.md)
    + [show-db-info](./commands/mtk_show-db-info.md)
    + [show-type](./commands/mtk_show-type.md)
    + [show-table](./commands/mtk_show-table.md)
    + [show-table-split](./commands/mtk_show-table-split.md)
    + [show-support-db](./commands/mtk_show-support-db.md)
    + [show-table-data-estimate](./commands/mtk_show-table-data-estimate.md)
  + sync
    + [sync-schema](./commands/mtk_sync-schema.md)
    + [sync-sequence](./commands/mtk_sync-sequence.md)
    + [sync-object-type](./commands/mtk_sync-object-type.md)
    + [sync-domain](./commands/mtk_sync-domain.md)
    + [sync-wrapper](./commands/mtk_sync-wrapper.md)
    + [sync-server](./commands/mtk_sync-server.md)
    + [sync-user-mapping](./commands/mtk_sync-user-mapping.md)
    + [sync-queue](./commands/mtk_sync-queue.md)
    + [sync-table](./commands/mtk_sync-table.md)
    + [sync-nickname](./commands/mtk_sync-nickname.md)
    + [sync-rule](./commands/mtk_sync-rule.md)
    + [sync-table-data](./commands/mtk_sync-table-data.md)
    + [sync-table-data-estimate](./commands/mtk_sync-table-data-estimate.md)
    + [sync-index](./commands/mtk_sync-index.md)
    + [sync-constraint](./commands/mtk_sync-constraint.md)
    + [sync-db-link](./commands/mtk_sync-db-link.md)
    + [sync-view](./commands/mtk_sync-view.md)
    + [sync-mview](./commands/mtk_sync-mview.md)
    + [sync-function](./commands/mtk_sync-function.md)
    + [sync-procedure](./commands/mtk_sync-procedure.md)
    + [sync-package](./commands/mtk_sync-package.md)
    + [sync-trigger](./commands/mtk_sync-trigger.md)
    + [sync-synonym](./commands/mtk_sync-synonym.md)
    + [sync-table-data-com](./commands/mtk_sync-table-data-com.md)
    + [sync-alter-sequence](./commands/mtk_sync-alter-sequence.md)
    + [sync-coll-statistics](./commands/mtk_sync-coll-statistics.md)
  + [check-table-data](./commands/mtk_check-table-data.md)
  + [gen](./commands/mtk_gen.md)
  + [gen completion](./commands/mtk_gen_completion.md)
  + [encrypt](./commands/mtk_encrypt.md)
  + [convert-plsql](./commands/mtk_convert-plsql.md)
  + [report](./commands/mtk_report.md)
  + [self](./commands/mtk_self.md)
    + [upgrade](./commands/mtk_self_upgrade.md)
+ Faqs
  + [Frequently Asked Questions](./faq/faqs.md)
  + [Oracle To MogDB](./faq/mtk-oracle-to-openGauss.md)
  + [MySQL To MogDB](./faq/mtk-mysql-to-openGauss.md)
  + [DB2 To MogDB](./faq/mtk-db2-to-openGauss.md)
  + [Informix To MogDB](./faq/mtk-informix-to-openGauss.md)
  + [DB2 To MySQL](./faq/mtk-db2-to-mysql.md)
+ Release
  + [Release Notes](./releases/release-notes.md)
  + [2.7](./releases/release-2.7.md)
  + [2.6](./releases/release-2.6.md)
  + [2.5](./releases/release-2.5.md)
  + [2.4](./releases/release-2.4.md)
  + [2.3](./releases/release-2.3.md)
  + [2.2](./releases/release-2.2.md)
  + [2.1](./releases/release-2.1.md)
  + [2.0](./releases/release-2.0.md)
  + [History Release Notes](./releases/release-history-notes.md)
