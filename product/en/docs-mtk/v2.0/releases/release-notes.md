---
title: Release Notes
summary: Release Notes
author: Zhang Cuiping
date: 2021-09-13
---

# Release Notes

- mtk   command line tool
- mtkd mtk background service process. used for specific scenarios

## 2.7

- [2.7.2](/releases/release-2.7.md#272)
- [2.7.1](/releases/release-2.7.md#271)
- [2.7.0](/releases/release-2.7.md#270)

## 2.6

- [2.6.6](/releases/release-2.6.md#266)
- [2.6.5](/releases/release-2.6.md#265)
- [2.6.4](/releases/release-2.6.md#264)
- [2.6.3](/releases/release-2.6.md#263)
- [2.6.2](/releases/release-2.6.md#262)
- [2.6.1](/releases/release-2.6.md#261)
- [2.6.0](/releases/release-2.6.md#260)

## 2.5

- [2.5.2](/releases/release-2.5.md#252)
- [2.5.1](/releases/release-2.5.md#251)
- [2.5.0](/releases/release-2.5.md#250)
