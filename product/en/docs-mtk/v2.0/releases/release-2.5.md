---
title: Release 2.4
summary: Release 2.4
author: Bin.Liu
date: 2022-07-29
---


# Release 2.5 Notes

## v2.5.2

2022-08-30

### MTK

- [mtk_2.5.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_windows_amd64.tar.gz)
- [mtk_2.5.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_arm64.tar.gz)
- [mtk_2.5.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_amd64_db2.tar.gz)
- [mtk_2.5.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_amd64_db2.tar.gz)
- [mtk_2.5.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_windows_amd64_db2.tar.gz)
- [mtk_2.5.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_arm64.tar.gz)
- [mtk_2.5.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_amd64.tar.gz)
- [mtk_2.5.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_amd64.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_amd64_db2.tar.gz)
- [mtkd_2.5.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_arm64.tar.gz)
- [mtkd_2.5.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_amd64.tar.gz)

2022-08-30

### Bug Fixes

- **DB2:** `syscat.Columns` view with table name suffix and space problem
- **MySQL:** Remove connection parameter `explicit_defaults_for_timestamp` when MySQL is source database
- **MySQL:** Migration `id BIGINT(20) unsigned AUTO_INCREMENT` to sequence issue
- **MySQL:** Migration `id INT(10) unsigned AUTO_INCREMENT` to sequence issue
- **MySQL:** Query constraint SQL add `binary` keyword
- **MySQL:** Migration MySQL `Time` type column default value problem
- **openGauss:** Query partition syntax issue when partition name is numeric

### Code Refactoring

- **MTKD:** Update `supportOperate` API return data structure
- **MTKD:** Update `convertSQLSupportType` API

### Features

- **MTK:** Add parameter `charLengthChangeExclude`
- **MTK:** Query Oracle NLS related parameters and save to `otherParams`
- **MTK:** Add `mig-selects` subcommand
- **MTKD:** Add task `getSchemas` API
- **MTKD:** Add operate API
- **MTKD:** Add merge task running record function
- **SQLServer:** Supports partial function/procedure syntax conversion to openGauss (beta)
- **SQLServer:** Supports query functions/procedures
- **SQLServer:** Supports query triggers

## v2.5.1

2022-08-16

### MTK

- [mtk_2.5.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_amd64.tar.gz)
- [mtk_2.5.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_amd64_db2.tar.gz)
- [mtk_2.5.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_arm64.tar.gz)
- [mtk_2.5.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_amd64.tar.gz)
- [mtk_2.5.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_amd64_db2.tar.gz)
- [mtk_2.5.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_arm64.tar.gz)
- [mtk_2.5.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_windows_amd64.tar.gz)
- [mtk_2.5.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_amd64.tar.gz)
- [mtkd_2.5.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_amd64_db2.tar.gz)
- [mtkd_2.5.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_arm64.tar.gz)

2022-08-16

### Bug Fixes

- **DB2:** Rewrite `insert into table_name values` error problem
- **MTKD:** Typo SUCCESS -> Succeed
- **Oracle:** Query Oracle Package issues

### Code Refactoring

- **MTKD:** Optimize DB2 query table information statement

### Features

- **MTK:** Add `convertOracleIntegerToNumeric` parameter
- **MTK:** `excludeTable` adds regular expression support
- **MTK:** `mig-tab-data`/`sync-table-data` add `tableSplit` parameter
- **MTK:** Add `convert-plsql`/`report-to-sql` command
- **MTKD:** Add `errorDataFile` api

## v2.5.0

2022-08-14

### MTK

- [mtk_2.5.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_amd64.tar.gz)
- [mtk_2.5.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_amd64_db2.tar.gz)
- [mtk_2.5.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_arm64.tar.gz)
- [mtk_2.5.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_amd64.tar.gz)
- [mtk_2.5.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_amd64_db2.tar.gz)
- [mtk_2.5.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_arm64.tar.gz)
- [mtk_2.5.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_windows_amd64.tar.gz)
- [mtk_2.5.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_amd64.tar.gz)
- [mtkd_2.5.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_amd64_db2.tar.gz)
- [mtkd_2.5.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_arm64.tar.gz)

### Bug Fixes

- **DB2:** Migration DB2 view syntax conversion related issues
- **MTK:** fix ColumnType null pointer problem
- **MTK:** add invalid information prompt for invalid objects
- **MySQL:** `DEFAULT_GENERATED ON UPDATE CURRENT_TIMESTAMP` syntax issue
- **Oracle:** ignore `IOT_OVERFLOW` table
- **Oracle:** syntax conversion `AQ$_CBSD_CHREXX` problem
- **Oracle:** VARRAY Type and type varchar2(10 char)
- **Oracle:** changed to `PIPE ROW` to `return next ();`
- **Oracle:** changed to Oracle `SYS_EXTRACT_UTC` problem
- **openGauss:** left join alias problem and judging stored procedure error problem

### Code Refactoring

- **MTK:** add character set definition
- **Oracle:** `authid current_user` syntax adaptation
- **RDBMS:** SetConnMaxLifetime to SetConnMaxIdleTime

### Features

- **MTK:** Add Excel report
- **MTK:** add `convert-plsql` subcommand
- **MTK:** `report-to-sql` subcommand adds the function of converting HTML reports to Excel reports
- **MTKD:** Added report and log download, conversion SQL, system information API
- **Oracle:** query view/function/procedure object ID
- **Oracle:** Rewrite Oracle `commit work` to openGauss `commit`
- **openGauss:** migrates Oracle Package to openGauss 3.0 Package by default

### Performance Improvements

- **Oracle:** Optimize query view performance
