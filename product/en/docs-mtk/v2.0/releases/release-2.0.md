---
title: Release 2.2
summary: Release 2.2
author: Bin.Liu
date: 2022-07-29
---


# Release 2.0 Notes

## v2.0.0

2021-11-15

- [mtk_2.0.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_amd64.tar.gz)
- [mtk_2.0.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_amd64_db2.tar.gz)
- [mtk_2.0.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_arm64.tar.gz)
- [mtk_2.0.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_amd64.tar.gz)
- [mtk_2.0.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_amd64_db2.tar.gz)
- [mtk_2.0.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_arm64.tar.gz)
- [mtk_2.0.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_windows_amd64.tar.gz)
- [mtk_2.0.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_checksums.txt)

### Feat

- Support migration from DB2 to DB2.

### Fix

- Resolve the issue that the format is incorrect during migration of the default value of the `timestamp` column of DB2 to openGauss.
- Resolve the issue during migration of the function index of Oracle to openGauss.
- Resolve the issue that format conversion fails during migration of the `date_format` function of MySQL to openGauss.
