---
title: Release 2.2
summary: Release 2.2
author: Bin.Liu
date: 2022-07-29
---


# Release 2.1 Notes

## v2.1.6

2021-12-28

- [mtk_2.1.6_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_amd64.tar.gz)
- [mtk_2.1.6_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_amd64_db2.tar.gz)
- [mtk_2.1.6_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_arm64.tar.gz)
- [mtk_2.1.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_amd64.tar.gz)
- [mtk_2.1.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_amd64_db2.tar.gz)
- [mtk_2.1.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_arm64.tar.gz)
- [mtk_2.1.6_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_windows_amd64.tar.gz)
- [mtk_2.1.6_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_checksums.txt)

### Feat

- Add the parameter [disableSelectPart](../config/mtk-config.md#disableselectpart) to disable the partition table by partition parallel migration function
- Add the parameter [templateSeqName](../config/mtk-parameter.md#templateseqname)  to support MySQL to openGauss sequence naming templates
- Migrate MySQL to openGauss to add `show create table` queries
- Support for openGauss partitioned indexes
- Add the `show-drop-index` `show-drop-cons` `mig-select` subcommand

### Fix

- Disable query MySQL `INNODB_TABLESTATS` view
- Command line parallelism and configuration file parallelism conflict issues
- Migrate openGauss query partitioning and constraint issues
- Migrate DB2 to MySQL `alter_sequence` issues
- DB2 timestamp precision issue
- DB2 drive slice bounds out issue
- Migrate the openGauss index add opClass
- Subcommand `mig-tab-post` did not collect statistics for issues
- Migrate DB2 to openGauss syntax conversion issues

## v2.1.5

2021-12-11

- [mtk_2.1.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_amd64.tar.gz)
- [mtk_2.1.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_amd64_db2.tar.gz)
- [mtk_2.1.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_arm64.tar.gz)
- [mtk_2.1.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_amd64.tar.gz)
- [mtk_2.1.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_amd64_db2.tar.gz)
- [mtk_2.1.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_arm64.tar.gz)
- [mtk_2.1.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_windows_amd64.tar.gz)
- [mtk_2.1.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_checksums.txt)

### Fix

- Crash when preRun mode
- Migration DB2 index column order issues
- Migration openGaus constraint issues

## v2.1.4

2021-12-10

- [mtk_2.1.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_amd64.tar.gz)
- [mtk_2.1.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_amd64_db2.tar.gz)
- [mtk_2.1.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_arm64.tar.gz)
- [mtk_2.1.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_amd64.tar.gz)
- [mtk_2.1.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_amd64_db2.tar.gz)
- [mtk_2.1.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_arm64.tar.gz)
- [mtk_2.1.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_windows_amd64.tar.gz)
- [mtk_2.1.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_checksums.txt)

### Feat

- Supports skipping columns of a data type in a table
- Supports skipping columns of table column name
- The parameter `seqLastNumAddNum` is supported to set how much the value of the sequence is increased at the end

### Fix

- Resolve the problem of migrating MySQL autoincrement column size query
- Resolve migration of Oracle column defaults

## v2.1.3

2021-12-08

- [mtk_2.1.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_amd64.tar.gz)
- [mtk_2.1.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_amd64_db2.tar.gz)
- [mtk_2.1.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_arm64.tar.gz)
- [mtk_2.1.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_amd64.tar.gz)
- [mtk_2.1.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_amd64_db2.tar.gz)
- [mtk_2.1.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_arm64.tar.gz)
- [mtk_2.1.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_windows_amd64.tar.gz)
- [mtk_2.1.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_checksums.txt)

### Fix

- Migration mysql view crash issue

## v2.1.2

2021-12-06

- [mtk_2.1.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_amd64.tar.gz)
- [mtk_2.1.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_amd64_db2.tar.gz)
- [mtk_2.1.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_arm64.tar.gz)
- [mtk_2.1.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_amd64.tar.gz)
- [mtk_2.1.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_amd64_db2.tar.gz)
- [mtk_2.1.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_arm64.tar.gz)
- [mtk_2.1.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_windows_amd64.tar.gz)
- [mtk_2.1.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_checksums.txt)

### Feat

- Supports MySQL Limit syntax conversion
- Supports MySQL 5.1.73

### Fix

- Fix the migration **date/datetime(n)/timestamp(n)** to openGauss **timestamp(n)** precision issue
- Fix the migration MySQL partitions with the **UNIX_TIMESTAMP** function issue
- Fix the migration MySQL **timestampdiff/isnsull/str_to_date** function syntax conversion issue
- Fix the migration DB2 loss function issue
- Fix the issue of migrating MySQL columns with empty strings by default
- Fix the issue of migrating empty strings in the MySQL `text` field
- Fix the migration MySQL stored procedure hang issue

## v2.1.1

2021-11-29

- [mtk_2.1.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_amd64.tar.gz)
- [mtk_2.1.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_amd64_db2.tar.gz)
- [mtk_2.1.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_arm64.tar.gz)
- [mtk_2.1.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_amd64.tar.gz)
- [mtk_2.1.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_amd64_db2.tar.gz)
- [mtk_2.1.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_arm64.tar.gz)
- [mtk_2.1.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_windows_amd64.tar.gz)
- [mtk_2.1.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_checksums.txt)

### Feat

- Support the hidden columns of MySQL (`invisible column`).
- Support `identity column` of Oracle.
- Support migration from `XML` to MogDB `XML`.
- Support DB2 9.7.
- Add the `init-project` subcommand.
- Add the parameter [autoAddMySQLAutoIncr](../config/mtk-parameter.md#autoaddmysqlautoincr).

### Fix

- Resolve the problem of MTK run in Windows.
- Resolve partial syntax conversion problems during migration from DB2 stored procedures to MogDB.
- Resolve the problem of migration from openGauss `blob` to `openGauss` `blob`.
- Resolve partial problems of migrating PostgreSQL.
- Resolve the problem of migration from DB2 `vargraphic` to MySQL `varchar`.
- Resolve the syntax problem during migration of Oracle partitioned tables where the partition value contains `timestamp`.

## v2.1.0

2021-11-23

- [mtk_2.1.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_amd64.tar.gz)
- [mtk_2.1.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_amd64_db2.tar.gz)
- [mtk_2.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_arm64.tar.gz)
- [mtk_2.1.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_amd64.tar.gz)
- [mtk_2.1.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_amd64_db2.tar.gz)
- [mtk_2.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_arm64.tar.gz)
- [mtk_2.1.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_windows_amd64.tar.gz)
- [mtk_2.1.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_checksums.txt)

### Feat

- Support migration of Oracle Spatial to Postgis. For details, see [Spatial](../faq/mtk-oracle-to-openGauss.md#spatial).
- Reserve the keyword list of openGauss.
- Add parameter [enableOgBlobClob](../config/mtk-parameter.md#enableogblobclob) to control migration of Oracle/DB2/MySQL blob/clob data to openGauss blob/clob.
- Optimize the terminal report display.

### Fix

- Resolve the syntax of migrating DB2 function `STATIC DISPATCH`.
- Resolve the syntax of migrating DB2 views `with check option` and `with row movement`.
- Resolve the issue that the migration table and column remark includes "0x00".
- Resolve the issue of the statement for querying the Oracle package.
