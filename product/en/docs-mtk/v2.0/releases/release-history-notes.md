---
title: History Release Notes
summary: History Release Notes
author: Zhang Cuiping
date: 2021-09-13
---

# History Release Notes

- mtk   command line tool
- mtkd mtk background service process. used for specific scenarios

## v0.0.41

2021-11-06

### Fix

- Migration MySQL `datatime` type and there is a `0000-00-00 00:00:00` data report that does not prompt for an error issue

## v0.0.40

2021-11-05

### Feat

- Support query and migration of the `server` type of DB2.
- Support query and migration of the `wrapper` type of DB2.
- Support query and migration of Oracle materialized views.
- Support query and migration of objects inrrespective of the case.
- Support configuration of file parameters `remapSchema`, `remapTable`, and `remapTablespace` inrrespective of the case.
- Add the migration test function and check parameters `test` and `limit`.

### Fix

- Resolve the problem that materialized views are not ignored during migration of Oracle tables.
- Resolve the problem that there are no migration sequences during migration of specified tables of DB2.
- Resolve the problem that the program cannot end because the statement for migrating MySQL views includes the IF expression.
- Resolve the problem that the program crashes because an exception is  not processed during data import.

## v0.0.39

2021-10-25

### Feat

- Support estimation of real size of a DB2 compression table.
- Support reports in the text format.
- Support automatic conversion of `minvalue` to `maxvalue`.
- Support openGauss package migration.

### Fix

- Resolve the `varchar(10 char)` length problem during Oracle migration.
- Resolve the `with read only` syntax problem during Oracle view migration.
- Resolve the problem that it is slow to view the table size in DB2.
- Resolve the messy code problem of the DB2 character set, and add the `DB2CODEPAGE` check.
- Resolve the problem of concurrent creation of indexes and constraints.
- Resolve the `truncate` and `update set` syntax problem during DB2 stored procedure migration.
- Resolve some problems during openGauss stored procedure migration.

## v0.0.38

2021-10-08

### Feat

- Data is exported into a file and the file number is added.
- Data is exported into a file and MySQL statements are generated `LOAD DATA INFILE XX INTO TABLE`.
- DB2 storage procedures can be migrated to openGauss (beta version).
- DB2 functions can be migrated to openGauss (beta version).
- The authentication license mechanism is updated.
- openGauss virtual columns are supported.
- PostgreSQL/openGauss `rule` can be migrated to openGauss/PostgreSQL.
- PostgreSQL/openGauss `base type` can be migrated to openGauss/PostgreSQL.

### Fix

- The problem during migration of `interval expression` from DB2 to openGauss is resolved.
- The problem during migration of `values expression` from DB2 to openGauss is resolved. ignore insert into xx values xx
- The problem that the sequence is increased by 1 when auto-increment columns are migrated from MySQL to openGauss is resolved.
- The problem for migrating openGauss constraints is resolved.
- Part problems for migration from openGauss to openGauss are resolved.
- The problem that migrating the openGauss query sequences is slow is resolved.
- The CSV format problem during data export is resolved.
- The problem that `varchar`/`timestamp` range partitions are migrated from DB2 to MySQL is resolved.

## v0.0.37

2021-09-24

### Feat

- Materialized views can be migrated from PostgreSQL, DB2, and openGauss  to openGauss.
- Stored procedures, functions and stored syntax can be viewed in DB2.
- Migration of MySQL range partitions containing `to_days`/`year`/`unix_timestamp` to openGauss is supported.
- Migration of PostgreSQL/openGauss `type` to openGauss is supported.
- Migration of PostgreSQL/openGauss `domain` to openGauss is supported.

### Fix

- The problem that the database compatibility mode B is not judged during migration to openGauus is resolved.
- The problem that the automatic virtual column of DB2 is always 0 is resolved. It is rewritten as `default 0`.
- The problem that the minimum and maximum values of a sequence are equal is resolved.
- The problems related to partitions during migration from DB2 to openGauss are resolved.
- The problem of uppercase and lowercase letters in MySQL query statements is resolved.
- The problems related to the migration from PostgreSQL to openGauss are resolved.

### Perf

- That the query of `ADMINTABINFO` in DB2 is quite slow is optimized.

## v0.0.36

2021-09-17

### Feat

- Multilingual help instructions are supported.
- Migration of openGauss functions, views, stored procedures and triggers is supported.
- Error data logging is supported.
- Parameter `autoAddMaxvaluePart` is added to support adding `maxvalue` partitions automatically when partition tables are migrated to `openGauss`.
- A parameter is added to the command line for exporting data to a file.
- Migration to PostgreSQL is supported.

### Fix

- MySQL 8.0.23 query view issues are resolved.
- The problem of determining the parallel completion of a single table is resolved.
- The problem that constraints are lost during migration from DB2 to openGauss is resolved.
- The problem that default values of columns are lost during migration from DB2 to openGauss is resolved.

## v0.0.35

2021-09-13

### Fix

- Parameter `ignoreTabPartition` is added to support migration to the target database and ignoring of partition syntax. Currently, this parameter supports only migration to MySQL.
- The column type `timestamp` of DB2 and Oracle is changed to `datetime(6)` after migration to MySQL.

## v0.0.34

2021-09-03

### Feat

- The problem that the name of an index or constraint exceeds 64 characters is resolved.
- The migration report is optimized.
- MariaDB 5.5.62 can be matched.

### Fix

- Some syntax problems involved in migration of views from Oracle to openGauss are resolved.

- The problem that `remapSchema` becomes invalid is resolved.

## v0.0.33

2021-08-30

### Feat

- During table data migration, a check item is added and used for checking whether a table exists.
- The parameter `excludeSysTable` is added and can ignore a system table. The user can customize the parameter and does not configure using system configuration by default.
- Indexes and constraints can be created once data synchronization is complete.
- Collecting table statistics information is added.
- The column type `set` `enum` for MySQL can be migrated to openGauss.
- The parameters `EnableSyncTabTbsPro` is added. `RemapTbsSpace` supports conversion of tablespace names.
- DB2 table creation syntax has `compression` and `organize by` syntax added.
- DB2 connection string has `ClientApplName` and `ProgramName` added.
- Case is ignored when a database is queried in MySQL.

### Fix

- The problem of matching special characters in a table name and a column name is resolved.
- After migrated to openGauss, `dual` is replaced with `sys_dummy`.
- The Chinese character gash problem is resolved.
- The problem that the table creation statement in Oracle does not have double quotation marks is resolved.
- After migrated to openGauss, when a view, function, procedure, or trigger is created, that the `search_path` is incorrect is resolved.
- The problem of consistency for the table, constraint, and index name is resolved.
- The problem that no warning is reported indicating that a table exists is resolved.
- The `auto-increment` column of the `information_schema.TABLES` view in  MySQL 8.0 is inaccurate, which needs to be associated with the `information_schema.INNODB_TABLESTATS` view for query.

## v0.0.32

2021-08-25

### Fix

- The ESLint safety problem is resolved.

## v0.0.31

2021-08-25

### Feat

- The DB2 virtual autoincrement column can be migrated to the MySQL autoincrement column.
- Oracle supports automatic query of a character set and configuration of  environment variable **NLS_LANG**.
- For log files, parameter **logfile** is preferably used. If there is no parameter **logfile**, use parameter **reportFile**.

### Fix

- The time consumed for migrating table data can be estimated.
- The VarGraphic column type problem in DB2 is resolved.
- axios is upgrade.
- The memory leak problem is resolved.
- The problem of the HTML report style is resolved.
- The format problem of migrating DB2 files to openGauss is resolved.
- The safety problem is resolved.

## v0.0.30

2021-08-18

### Fix

- The problem of constraints with the same name is resolved.
- The format problem existing in the timestamp column of DB2 is resolved.  `2019-01-10-10.52.08.554035`
- Some problems existing in migrating data from Oracle to MySQL are resolved.
- The sorting problem involving index during data migration from Oracle to MySQL is resolved.

## v0.0.29

2021-08-13

### Feat

- Oracle packages can be migrated to openGauss. (alpha beta version)
- The  mig-tab-pre/data/post/other subcommands are supported.
- Oracle triggers can be migrated to openGauss.

### Fix

- Oracle trim is converted to openGauss trim (both xx).
- Oracle dbms_lock.sleep is converted to openGauss pg_sleep.
- The insert syntax problem in data migration from DB2 to MySQL is resolved.
- The problem of migrating partition tables from DB2 to MySQL is resolved.
- Constraint creation in MySQL does not allow usage of indexes, which is rectified to a normal syntax.
- The status problem of migrating sequences from DB2 to MySQL is resolved.
- The problem of generating reports slowly is resolved.
- Some problems of converting the storage procedure are resolved.

## v0.0.28

2021-08-09

### Feat

- Part of time functions in DB2 can be converted in openGauss.
- The gen auto complete subcommand is added.
- The connect by can be rewritten as the CTE syntax. (alpha beta version)
- Data can be migrated from DB2 to MySQL.
- batchSize is supported in the COPY command.

### Fix

- Split table add abs function because mod function will appear negative.
- The problem that the overloading parameter package is lost is resolved during function and storage procedure migration to openGauss.
- The problem that the schema of the sequence is not remapped after the autoincrement column of MySQL is migrated to the openGauss sequence is resolved.
- After the autoincrement column of MySQL is migrated to the openGauss sequence, the cache is changed to 1.
- When the MySQL setting parameter is put in the timestamp column, it automatically generate the on update syntax.
- The problem of the time zone in the MySQL connection string is resolved. The default time zone is that of the local host.

## v0.0.27

2021-08-02

### Feat

- The Oracle synonym and the DB2 alias to openGauss synonym are supported.
- The DB2 nickname view is automatically skipped.
- The MySQL cursor syntax  `xxx cursor for` can be rewritten into openGauss `cursor xxx is`.
- The MySQL `join` syntax can be rewritten.
- Oracle `NLSSORT` can be rewritten into openGauss `collate`.
- The Oracle `months_between` function can be rewritten.
- Oracle or MySQL functions can be migrated to openGauss.
- Oracle `rownum` can be rewritten into openGauss `limit`.
- Oracle outer join can be rewritten into a normal syntax.
- Oracle storage procedure can be migrated to openGauss. (alpha beta version)

### Fix

- The problem of rewritting the Oracle `add_years` function is resolved.
- The view can be created by skipping the DB2 function index.
- The `sql_mode` parameter is added in MySQL.
- The golang panic catching is added.
- The UTF8 string definition is added.
- Some problems of migrating Oracle functions to openGauss are resolved.
- The problem of querying the MySQL 8.0 views is resolved.
- The default value `000-00-00 00:00:00`of the MySQL column is changed to `1970-01-01` after migration.
- The prefix index syntax of the MySQL column is ignored. (custom_condition(100)
- MySQL JOIN with WHERE clause instead of ON is rewritten.
- The problem of inconsistency between the unique constraint, constraint, and index names of the primary key in DB2 is resolved.

## v0.0.26

2021-07-23

### Feat

- Oracle functions can be migrated from Oracle to openGauss. (alpha beta version)
- Oracle types can be migrated from Oracle to openGauss.
- The default value `"SYSIBM"."BLOB"('')` of the DB2 column is removed.
- The MySQL `bit` column type is supported.

### Fix

- The problem that the configuration file is incorrect but no warning is reported is resolved.
- The problem of the MySQL constraints with the same name is resolved.
- The problem that error "pq: invalid byte sequence for encoding "UTF8": 0x00" is reported when the MySQL text field is migrated is resolved.
- The problem that the **virtualColConv** parameter is case-insensitive is resolved.
- The MySQL bigint unsigned auto incr issue is resolved.
- The problem of querying the storage procedure of a function in MySQL 8.0 is resolved.
- The problem of a table name with space is resolved.
- The constraint problem of migrating data from DB2 to openGauss is resolved. A unique index is created first and a unique constraint is created second.
- The problem of the MySQL index column `custom_condition(1000)` to openGauss `substring(custom_condition,0,1000)` is resolved.
- The DB2 function index problem is resolved.
- The MySQL `int unsigned` to bigint problem is resolved.
- The virtual column problem in MySQL 8.0 is resolved.

## v0.0.25

2021-07-19

### Feat

- Removing and restoring comment code is added.
- Interval type regular matching is added.
- The `virtualColConv` parameter is added, which allows the user to customize expression conversion of the virtual columns.
- The sequence modification function is added, and the last value of a sequence is synchronized to the target database.
- The check constraint of the virtual column in DB2 is automatically skipped.
- The `gen-config` subcommand is added.
- The DB2 nickname table index is automatically skipped.
- The sorting of the start time and end time in a report is added.

### Fix

- The code smell problem is resolved.
- The problem that the copy syntax does not include DB2 autoincrement ID virtual column is resolved.
- The Oracle 11g deferred_segment_creation is resolved. Change the show table size to left join.

## v0.0.24

2021-07-16

### Feat

- The MySQL/DB2 table split function is supported.
- The openGauss virtual column syntax is supported.
- sqlServer views can be migrated to openGauss/MogDB.
- sqlServer indexes can be migrated to openGauss/MogDB.
- sqlServer constraints can be migrated to openGauss/MogDB.
- sqlServer schema  table data can be migrated to openGauss/MogDB.
- The syntax of creating secondary partitions in PostgreSQL is supported.
- The Oracle select hint parallel syntax is supported.
- The `show-schema` command can display the size of `schema`.
- The `check-config` command is added to check whether a configuration file is correct.
- A command is added to allow the user to control the migration type.
- The `showSchema`, `showTopTableSize`, and `showTopTableSplit` commands are added.

### Fix

- The problem that multiple columns are to be partitioned after migrated to PostgreSQL is resolved.
- The problem that a column name starts with numbers is resolved.

## v0.0.23

2021-07-07

### Feat

- The sqlserver identity column can be migrated.
- The sqlserver table query can be migrated.
- sqlserver sequences can be migrated to openGauss/MogDB.

### Fix

- The problem of querying the partition sorting in DB2 is resolved.

## v0.0.22

2021-07-02

### Fix

- The length problem of migrating columns from MySQL to openGauss is resolved.
- The problem that the procedure does not exit normally is resolved.
- The problem that the column length does not increase automatically when a table is exported to a file is resolved.
- The problem that a report cannot generate on the Windows platform is resolved.
- The problem that the select syntax does not have double quotation marks added is resolved.

## v0.0.21

2021-06-30

### Feat

- The trigger, function, storage procedure, and package creation syntax is added in Oracle.

### Fix

- The view filter condition is removed from Oracle.

### Perf

- Concurrent lock is optimized.
- The performance of table synchronization data is optimized.

## v0.0.20

2021-06-22

### Fix

- The problem that the DB2 `xml` column type includes the XML version is resolved. `XMLDeclaration=1` is added to a connection string.
- The problem that the DB2 `dbClob` column type is changed to the openGauss text column type after migration is resolved.

## v0.0.19

2021-06-18

### Fix

- The problem that the Oracle range partition includes multiple columns is resolved.
- The empty string and null problems in Oracle, DB2, and MySQL are resolved.

## v0.0.18

2021-06-16

### Feat

- The license function is added.
- That  the column length automatically increases after a non-UTF8 character set is changed to a UTF8 character set during migration to openGauss/MogDB is supported.
- The time consumed for migrating data from Oracle or MySQL to openGauss can be estimated.
- Data can be migrated from a Dameng database to openGauss.
- The `colKeyWords` and `objKeyWords` configuration parameters are added.
- The DB2 trigger, function, and procedure query statistics are added.
- The Oracle procedure, function, trigger, and type query statistics are added.
- The Oracle procedure, function, package, synonym, dblink, and queue type query statistics are added.
- Migration to openGauss 2.0.0 is supported.
- The database compatibility query function is added to openGauss.

### Fix

- DB2 GBK CHARSET Database char include Chinese error slice bounds out of range
- Oracle '' = null to migrate openGauss is not null issue
- MySQL time issue. MySQL year to openGauss int
- Oracle/MySQL 0x00 issue
- DB2/MySQL blob/clob issue
- Oracle clob blob data issue
- Modify query Oracle dblink view. replace all_db_links to dba_db_links
- add Oracle long raw column type
- openGauss add Col Key Word stream. convert to "STREAM"
- missing column length from query openGauss/PostgreSQL database.
- migrate Oracle missing table
- table ddl comp CHAR = CHARACTER/VARCHAR = CHARACTER VARYING
