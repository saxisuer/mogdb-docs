---
title: Release 2.2
summary: Release 2.2
author: Bin.Liu
date: 2022-07-29
---


# Release 2.2 Notes

## v2.2.5

2022-04-10

### MTK

- [mtk_2.2.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_amd64.tar.gz)
- [mtk_2.2.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_amd64_db2.tar.gz)
- [mtk_2.2.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_arm64.tar.gz)
- [mtk_2.2.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_amd64.tar.gz)
- [mtk_2.2.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_amd64_db2.tar.gz)
- [mtk_2.2.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_arm64.tar.gz)
- [mtk_2.2.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_windows_amd64.tar.gz)
- [mtk_2.2.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.2.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_amd64.tar.gz)
- [mtkd_2.2.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_amd64_db2.tar.gz)
- [mtkd_2.2.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_arm64.tar.gz)

### Bug Fixes

- **MariaDB:** Delete the parameter `explicit_defaults_for_timestamp` setting
- **MariaDB:** 10.3.9 Column Default issue
- **MariaDB:** Query indexing SQL statement issue
- **Oracle:** Migration transforms `to_clob` syntax issue
- **Oracle:** No column length conversion for Char(1).
- **Oracle:** Sequence name contains $ symbol issue
- **Postgres:** Migration GIS data issues
- **Postgres:** Query function SQL statement issue
- **Postgres:** Upgrade driver version

### Features

- **MTK:** Add `SetConnMaxLifetime` settings and upgrade to Go 1.17
- **MTKD:** Add `SetConnMaxLifetime` setting
- **MTKD:** Refactor the MTKD program
- **Oracle:** Add the parameter `convertPackageMethod` to support migrating Oracle packages to openGauss packages
- **Oracle:** Convert `binary_integer` to `integer`
- **Oracle:** Convert `pls_integer` to `integer`
- **openGauss:** Connection string add parameter `check_function_bodies=off`

### Performance Improvements

- **MTK:** Optimized memory usage

## v2.2.4

2022-03-25

- [mtk_2.2.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_amd64.tar.gz)
- [mtk_2.2.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_amd64_db2.tar.gz)
- [mtk_2.2.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_arm64.tar.gz)
- [mtk_2.2.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_amd64.tar.gz)
- [mtk_2.2.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_amd64_db2.tar.gz)
- [mtk_2.2.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_arm64.tar.gz)
- [mtk_2.2.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_windows_amd64.tar.gz)
- [mtk_2.2.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_checksums.txt)

### Bug Fixes

- **Oracle:** query table size sql statement is missing the condition
- **Oracle:** table statistics syntax issue
- **openGauss:** 2.1.0 COPY does not support sub transactions or exceptions

### Code Refactoring

- **Table:** create table error code add sql

### Features

- **openGauss:** datCompatibility add ora/mysql
- **DB2:** support TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
- **Oracle:** The package does not have a package body definition to return success issues
- **Oracle:** remove procedure `authid current_user` syntax
- **Postgres:** support mig postgis to openGauss

## v2.2.3

2022-03-15

- [mtk_2.2.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_amd64.tar.gz)
- [mtk_2.2.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_amd64_db2.tar.gz)
- [mtk_2.2.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_arm64.tar.gz)
- [mtk_2.2.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_amd64.tar.gz)
- [mtk_2.2.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_amd64_db2.tar.gz)
- [mtk_2.2.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_arm64.tar.gz)
- [mtk_2.2.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_windows_amd64.tar.gz)
- [mtk_2.2.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_checksums.txt)

### Feat

- The subcommand `show-table-split` adds a `ora_hash` method for oracle databases
- Added the parameter [charAppendEmptyString](../config/mtk-parameter.md#charappendemptystring) to resolve the ORA-29275 error
- Add the argument [clientCharset](../config/mtk-config.md#clientcharset)
- Add the parameter [IgErrorData](../config/mtk-parameter.md#igerrordata)

### Fix

- Fixed the issue of converting Oracle packages to openGauss syntax package names without deletion

## v2.2.2

2022-03-01

- [mtk_2.2.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_amd64.tar.gz)
- [mtk_2.2.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_amd64_db2.tar.gz)
- [mtk_2.2.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_arm64.tar.gz)
- [mtk_2.2.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_amd64.tar.gz)
- [mtk_2.2.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_amd64_db2.tar.gz)
- [mtk_2.2.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_arm64.tar.gz)
- [mtk_2.2.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_windows_amd64.tar.gz)
- [mtk_2.2.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_checksums.txt)

### Feat

- Added check if the `orafce` plugin is installed and skips some syntax conversions
- Disables `connect by` syntax conversion for MogDB 2.1.0
- Optimized the migration of Oracle to openGauss/MogDB constraint renaming logic
- Added Oracle table data character set check

### Fix

- Code smell and security issues
- Migration of Oracle to MySQL section issue
- Migrated Some Syntax Conversion Issues from Oracle to MogDB 2.1.0
- DB2 clob length issue

## v2.2.1

2022-01-20

- [mtk_2.2.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_amd64.tar.gz)
- [mtk_2.2.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_amd64_db2.tar.gz)
- [mtk_2.2.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_arm64.tar.gz)
- [mtk_2.2.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_amd64.tar.gz)
- [mtk_2.2.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_amd64_db2.tar.gz)
- [mtk_2.2.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_arm64.tar.gz)
- [mtk_2.2.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_windows_amd64.tar.gz)
- [mtk_2.2.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_checksums.txt)

### Feat

- Add command flags `disableFKCons` to support unsynchronized foreign key constraints
- Add the argument [autoAddMySQLAutoIncrTabList](../config/mtk-parameter.md#autoaddmysqlautoincrtablist)

### Fix

- MySQL `show create table` syntax issue
- MySQL connection string issue
- Migrating to Oracle `ORA-00911` error
- Migration to Oracle object name length greater than 30 issues
- Migration to Oracle sequence cache must be greater than 1 issue
- The Parameter  '`quoteMark` does not take effect issue
- Migrating DB2 to MySQL constraint issues
- Migrating DB2 to MySQL index names and constraint names with lengths greater than 64 issues
- Migrated DB2 `GENERATED IDENTITY` columns to MySQL `auto_incr` primary key column order issues
- Migration of SqlServer to MySQL column type conversion issue

## v2.2.0

2022-01-11

- [mtk_2.2.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_amd64.tar.gz)
- [mtk_2.2.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_amd64_db2.tar.gz)
- [mtk_2.2.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_arm64.tar.gz)
- [mtk_2.2.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_amd64.tar.gz)
- [mtk_2.2.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_amd64_db2.tar.gz)
- [mtk_2.2.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_arm64.tar.gz)
- [mtk_2.2.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_windows_amd64.tar.gz)
- [mtk_2.2.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_checksums.txt)

### Feat

- add table data check feat [check-table-data](../commands/mtk_check-table-data.md)
- support MogDB subpartition

### Fix

- mig openGauss to openGauss column default null issue
- mig DB2 to openGauss view gen create view syntax issue
- mig DB2 to openGauss converts procedure `end p1` issue
- mig DB2 to openGauss converts procedure and functions missing the `package` attribute
- mig MySQL to openGauss `information_schema.tables` fields null issue
