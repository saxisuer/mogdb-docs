---
title: Release 2.3
summary: Release 2.3
author: Bin.Liu
date: 2022-07-29
---


# Release 2.3 Notes

## v2.3.5

### MTK

- [mtk_2.3.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_amd64.tar.gz)
- [mtk_2.3.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_amd64_db2.tar.gz)
- [mtk_2.3.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_arm64.tar.gz)
- [mtk_2.3.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_amd64.tar.gz)
- [mtk_2.3.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_amd64_db2.tar.gz)
- [mtk_2.3.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_arm64.tar.gz)
- [mtk_2.3.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_windows_amd64.tar.gz)
- [mtk_2.3.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_amd64.tar.gz)
- [mtkd_2.3.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_amd64_db2.tar.gz)
- [mtkd_2.3.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_arm64.tar.gz)

### Bug Fixes

- **mtk:** Migration data hang

### Performance Improvements

- **mtk:** Optimize processing time data performance

## v2.3.4

2022-06-19

### Bug Fixes

- **MySQL:** Migrate MySQL to openGauss to rewrite `if then` syntax issue
- **MySQL:** Migrating MySQL to openGauss B compatibility mode, `NULL` and empty strings, field length issues
- **Oracle:** Migrate Oracle to openGauss to rewrite `mod` function issue
- **Oracle:** Migrate Oracle to openGauss to remove extra brackets
- **Oracle:** no query table partition information in `dataOnly` mode
- **Oracle:** object name with $ problem, no need to add double quotes by default
- **mtk:** to troubleshoot table problems
- **openGauss:** ignore create plugin errors

### Features

- **Oracle:** declaration does not support syntax conversion in the case of functions containing functions
- **mtk:** Add parameter [disablePrintMigDataProgress](../config/mtk-parameter.md#disableprintmigdataprogress) to disable printing migration data progress
- **mtk:** Add parameter [disableSyncIdxAfterData](../config/mtk-parameter.md#disablesyncidxafterdata) to disable the synchronization of table data and create this table index immediately
- **mtk:** report add migration data query SQL
- **openGauss:** supports batch Commit. Task retry requires user configuration [truncTable](../config/mtk-parameter.md#trunctable)

### Performance Improvements

- **mtk:** Optimize the performance of migrating data to openGauss. 2-3 times faster

## v2.3.3

2022-06-08

### MTK

- [mtk_2.3.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_amd64.tar.gz)
- [mtk_2.3.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_amd64_db2.tar.gz)
- [mtk_2.3.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_arm64.tar.gz)
- [mtk_2.3.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_amd64.tar.gz)
- [mtk_2.3.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_amd64_db2.tar.gz)
- [mtk_2.3.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_arm64.tar.gz)
- [mtk_2.3.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_windows_amd64.tar.gz)
- [mtk_2.3.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_amd64.tar.gz)
- [mtkd_2.3.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_amd64_db2.tar.gz)
- [mtkd_2.3.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_arm64.tar.gz)

### Features

- **Oracle:** rewrite Oracle function declaration `PARALLEL_ENABLE` to be empty
- **Oracle:** rewrite Oracle function declaration `deterministic` to empty openGauss `IMMUTABLE`
- **mtkd:** add connection pool parameter
- **openGauss:** add `timestamp` precision warning
- **openGauss:** support Oracle `ST_GEOMETRY` for openGauss postgis `geometry`
- **openGauss:** rewrite Oracle `wmsys.wm_concat` to `wm_concat`
- **openGauss:** rewrite Oracle `!=` to openGauss `!=`

### Bug Fixes

- **report:** optimize the display of results and solve the sql copy bug
- **MySQL:** MySQL ignores case query constraints where conditional issues
- **Oracle:** Migrating Oracle `raw` to openGauss `raw` issue
- **mtkd:** create/modify/run tasks add configuration checks

## v2.3.2

2022-05-16

### MTK

- [mtk_2.3.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_amd64.tar.gz)
- [mtk_2.3.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_amd64_db2.tar.gz)
- [mtk_2.3.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_arm64.tar.gz)
- [mtk_2.3.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_amd64.tar.gz)
- [mtk_2.3.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_amd64_db2.tar.gz)
- [mtk_2.3.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_arm64.tar.gz)
- [mtk_2.3.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_windows_amd64.tar.gz)
- [mtk_2.3.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_amd64.tar.gz)
- [mtkd_2.3.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_amd64_db2.tar.gz)
- [mtkd_2.3.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** There is no keyword handling problem in the primary key syntax section of the table-building statement
- **openGauss:** convert DB2 `current user` 为 openGauss `session_user`

### Features

- **openGauss:** add parameter[indexOptions](../config/mtk-parameter.md#indexoptions) support migration to openGauss add/modify/delete index creation options
- **openGauss:** add parameter[tableOptions](../config/mtk-parameter.md#tableoptions) support migration to openGauss add/modify/delete table creation options

## v2.3.1

2022-05-09

### Bug Fixes

- **DB2:** Migration to MySQL auto-incr columns is not a primary key first column issue
- **MTKD:** Log default level issue
- **MySQL:** Migration Oracle Number(*,0) loss of specific precision issues
- **MySQL:** Migration MySQL connections take up too many issues
- **openGauss:** Migration Oracle Number(*,0) loss of specific accuracy issues
- **openGauss:** Query partition SQL statement issues

### Features

- **MySQL:** Supports migration triggers to openGauss/MogDB
- **Postgre:** Supports `GENERATED ALWAYS AS IDENTITY`
- **openGauss:** Supports openGauss 3.0 large sequence

## v2.3.0

### MTK

- [mtk_2.3.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_amd64.tar.gz)
- [mtk_2.3.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_amd64_db2.tar.gz)
- [mtk_2.3.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_arm64.tar.gz)
- [mtk_2.3.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_amd64.tar.gz)
- [mtk_2.3.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_amd64_db2.tar.gz)
- [mtk_2.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_arm64.tar.gz)
- [mtk_2.3.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_windows_amd64.tar.gz)
- [mtk_2.3.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_amd64.tar.gz)
- [mtkd_2.3.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_amd64_db2.tar.gz)
- [mtkd_2.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_arm64.tar.gz)

### Bug Fixes

- **DB2:** Migration to MySQL object name contains spaces to the right, and the MTK automatically trim the space on the right
- **DB2:** Migration to MySQL BLOB, TEXT, GEOMETRY or JSON column cannot define default values
- **DB2:** Removes the DB2CODEPAGE environment variable force check
- **MTKD:** Api interface returned 400 questions
- **MTKD:** Modify `deleteUserId` to `deletedUserId`, `updateUserId` to 'updatedUserId`
- **MySQL:** update syntax error issue
- **MySQL:** Conversion `interval` syntax issue
- **MySQL:** Chinese conversion issues
- **MySQL:** There is no `begin/end` syntax issue with the function
- **MySQL:** Conversion `join` syntax issue
- **MySQL:** Conversion `convert` to `cast` syntax issues
- **Oracle:** Conversion package to og package comment missing issue
- **Oracle:** Migration to mySQL object name contains spaces to the right, and the MTK automatically trim the space on the right
- **Oracle:** Conversion insert statement contains the `return` keyword problem
- **Oracle:** There is no stored procedure name issue in package
- **Oracle:** Parameter `colKeyWords` priority issue
- **Oracle:** Stored procedure parameters with annotated issue

### Code Refactoring

- **MTK:** Survival object name method

### Features

- **DB2:** Support DB2 `date/time` column default `current date/time` to MySQL `current_date/time`
- **MTKD:** Encrypted storage of passwords
- **MySQL:** Supports converting comment `#` to `--`
- **Oracle:** Replace the query view `all_` with the `dba_` view
- **Oracle:** Supports converting Insert statements with aliases
- **Oracle:** Supports converting Chinese symbol `(),` to English
- **openGauss:** Remove parameter `check_function_bodies=off`
