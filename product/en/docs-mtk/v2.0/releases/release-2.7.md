---
title: 发布记录
summary: 发布记录
author: Zhang Cuiping
date: 2021-09-17
---

# Release 2.7 Notes

- mtk   command line tool
- mtkd mtk background service process. used for specific scenarios

## v2.7.2

2022-11-29

### MTK

- [mtk_2.7.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_windows_amd64.tar.gz)
- [mtk_2.7.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_arm64.tar.gz)
- [mtk_2.7.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_amd64.tar.gz)
- [mtk_2.7.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_amd64.tar.gz)
- [mtk_2.7.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_arm64.tar.gz)
- [mtk_2.7.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_amd64_db2.tar.gz)
- [mtk_2.7.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_amd64_db2.tar.gz)
- [mtk_2.7.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_amd64.tar.gz)
- [mtkd_2.7.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_amd64_db2.tar.gz)
- [mtkd_2.7.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** The table data comparison does not record SQL statements
- **openGauss:** Mig BLOB data to bytea data issue
- **openGauss:** Check version `3.0.3.6` issues when generating partition syntax

## v2.7.1

2022-11-28

### MTK

- [mtk_2.7.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_windows_amd64.tar.gz)
- [mtk_2.7.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_arm64.tar.gz)
- [mtk_2.7.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_amd64.tar.gz)
- [mtk_2.7.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_amd64.tar.gz)
- [mtk_2.7.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_arm64.tar.gz)
- [mtk_2.7.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_amd64_db2.tar.gz)
- [mtk_2.7.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_amd64_db2.tar.gz)
- [mtk_2.7.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_amd64.tar.gz)
- [mtkd_2.7.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_amd64_db2.tar.gz)
- [mtkd_2.7.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_arm64.tar.gz)

### Bug Fixes

- **Informix:** Migrate `interval` data issue
- **MTK:** Remove add alias function
- **MTK:** User configured character set does not ignore case when exporting files
- **MTK:** auto-set `clientCharset` issue
- **MTKD:** download configuration file does not specify a file name problem
- **MogDB:** Migrate Informix to MogDB column default value problem
- **MySQL:** Rewrite MySQL `GROUP_CONCAT` to openGauss syntax issue
- **Oracle:** Migrate Oracle `VARRAY Type Column` data problem
- **Oracle:** Rewrite Oracle Type `VARRAY OF PLS_INTEGER` problem
- **Oracle:** Rewrite Oracle Column Type `Char(10 Char)` issue
- **openGauss:** Rewrite Mariadb/MySQL `current_timestamp()` issue
- **openGauss:** Create trigger syntax missing `;`
- **openGauss:** Migrate Oracle Clob did not replace 0x00

### Code Refactoring

- **MTK:** replaces MySQL `0000-00-00`

### Features

- **MTK:** Support SQL Server 2012
- **MTK:** Oracle external tables are not supported sorry
- **MTK:** Update html report
- **MTKD:** Get task list interface adds search function
- **MTKD:** Add subcommand to generate Linux service configuration file
- **MTKD:** Modify task interface to add modification database type check
- **openGauss:** Add parameter [enableParallelIndex](../config/mtk-parameter.md#enableparallelindex)
- **openGauss:** rewrite Oracle VARRAY Type to openGauss domain
- **openGauss:** add parameter [enableCharTrimRightSpace](../config/mtk-parameter.md#enablechartrimrightspace)
- **openGauss:** upgrade driver support to return copy error data
- **openGauss:** rewrite Oracle `DBMS_UTILITY.format_error_backtrace` to openGauss `PG_EXCEPTION_CONTEXT`

## v2.7.0

2022-11-09

### MTK

- [mtk_2.7.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_windows_amd64.tar.gz)
- [mtk_2.7.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_arm64.tar.gz)
- [mtk_2.7.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_amd64.tar.gz)
- [mtk_2.7.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_amd64.tar.gz)
- [mtk_2.7.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_arm64.tar.gz)
- [mtk_2.7.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_amd64_db2.tar.gz)
- [mtk_2.7.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_amd64_db2.tar.gz)
- [mtk_2.7.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_amd64.tar.gz)
- [mtkd_2.7.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_amd64_db2.tar.gz)
- [mtkd_2.7.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_arm64.tar.gz)

### Bug Fixes

- Insert duplicate data issues in configuration `igErrorData`
- Self-upgrade related issues
- **DB2:** Generates nvarchar syntax issues
- **DB2:** Migration of MySQL blobs to DB2 issues
- **MTK:** Configure `remapSchema`, `xxx.xxx not existed` to indicate that the schema is incorrect
- **MTKD:** Returns a 500 error when resolving program exceptions
- **MySQL:** Migration Oracle Nvarchar to MySQL Nvarchar issues
- **MySQL:** Some issues with migrating openGauss to MySQL
- **Oracle:** Parallel statement problem generated by `ora_hash`
- **Oracle:** Migration Oracle Nvarchar to openGauss/MogDB issues
- **Oracle:** Migration of Oracle type to openGauss to does not remove the `authid current_user` syntax issue
- **SQLServer:** Migration Sequence issues
- **SQLServer:** Some issues migrating SQLServer To MogDB
- **openGauss:** Query column information loses timestamp precision issue
- **openGauss:** Remove data checks when exporting files

### Features

- **MTK:** Support GaussDB for openGauss Primary/Standby version
- **MTK:** Add parameter `dropSchema`. `dropExistingObject=true`does not support the deletion of schema functionality
- **MySQL:** Migrate MySQL Key Partition to MogDB Hash Partition
- **Oracle:** Automatically set environment variable `ORA_OCI_NO_OPTIMIZED_FETCH=1`
