---
title: Release Notes
summary: Release Notes
author: Zhang Cuiping
date: 2021-09-13
---

# Release 2.6 Notes

- mtk   command line tool
- mtkd mtk background service process. used for specific scenarios

## v2.6.6

2022-10-20

### MTK

- [mtk_2.6.6_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_windows_amd64.tar.gz)
- [mtk_2.6.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_arm64.tar.gz)
- [mtk_2.6.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_amd64.tar.gz)
- [mtk_2.6.6_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_amd64.tar.gz)
- [mtk_2.6.6_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_arm64.tar.gz)
- [mtk_2.6.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_amd64_db2.tar.gz)
- [mtk_2.6.6_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_amd64_db2.tar.gz)
- [mtk_2.6.6_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_amd64.tar.gz)
- [mtkd_2.6.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_amd64_db2.tar.gz)
- [mtkd_2.6.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** Disable the self-calculating lob field fetchSize
- **Oracle:** clob/blob use godror driver

## v2.6.5

2022-10-17

### MTK

- [mtk_2.6.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_windows_amd64.tar.gz)
- [mtk_2.6.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_arm64.tar.gz)
- [mtk_2.6.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_amd64.tar.gz)
- [mtk_2.6.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_amd64.tar.gz)
- [mtk_2.6.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_arm64.tar.gz)
- [mtk_2.6.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_amd64_db2.tar.gz)
- [mtk_2.6.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_amd64_db2.tar.gz)
- [mtk_2.6.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_amd64.tar.gz)
- [mtkd_2.6.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_amd64_db2.tar.gz)
- [mtkd_2.6.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** If the table comment statement fails, modify the table status to Warning
- **MTK:** The export file did not exit
- **MTKD:** Object Finish Time is a negative number problem
- **MTKD:** Updated database status is not timely and data loss issues
- **MTKD:** getSchemas error info
- **Oracle:** Add the package name when there is no definition of the package name in the package header end

### Features

- **MTK:** cmd add group
- **MTK:** Automatically recognizes the Oracle character set and sets the ClientCharset parameter
- **Oracle:** Support Public Synonym
- **openGauss:** disable auto create extension orafce

### Performance Improvements

- update go-ora driver. Improves string conversion performance
- Optimize Varchar data processing logic and reduce object allocation
- **openGauss:** Optimized Performance

## v2.6.4

2022-10-05

### MTK

- [mtk_2.6.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_windows_amd64.tar.gz)
- [mtk_2.6.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_arm64.tar.gz)
- [mtk_2.6.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_amd64.tar.gz)
- [mtk_2.6.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_amd64.tar.gz)
- [mtk_2.6.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_arm64.tar.gz)
- [mtk_2.6.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_amd64_db2.tar.gz)
- [mtk_2.6.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_amd64_db2.tar.gz)
- [mtk_2.6.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_amd64.tar.gz)
- [mtkd_2.6.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_amd64_db2.tar.gz)
- [mtkd_2.6.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_arm64.tar.gz)

### Bug Fixes

- **PostgreSQL:** bool type issue
- **PostgreSQL:** Mig Oracle `minus` to `EXCEPT`
- **PostgreSQL:** Mig Oracle function cursor issue

### Features

- **MTK:** Update the license version and be compatible with the old version
- **MTKD:** add tableOption `COLLATE=xxx_bin` for MySQL database
- **MTKD:** swagger add https schema
- **Oracle:** Support Oracle Flashback Queries.

## v2.6.3

2022-09-22

### MTK

- [mtk_2.6.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_windows_amd64.tar.gz)
- [mtk_2.6.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_arm64.tar.gz)
- [mtk_2.6.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_amd64.tar.gz)
- [mtk_2.6.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_amd64.tar.gz)
- [mtk_2.6.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_arm64.tar.gz)
- [mtk_2.6.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_amd64_db2.tar.gz)
- [mtk_2.6.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_amd64_db2.tar.gz)
- [mtk_2.6.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_amd64.tar.gz)
- [mtkd_2.6.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_amd64_db2.tar.gz)
- [mtkd_2.6.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** export to file error and not exit issue
- **MTK:** create foreign key constraint issue
- **Oracle:** Migration of Spatial data to file issue
- **Oracle:** Rewrite Oracle trigger `after statement` issue

### Performance Improvements

- **MTK:** Optimized migration to MySQL performance

## v2.6.2

2022-09-20

### MTK

- [mtk_2.6.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_windows_amd64.tar.gz)
- [mtk_2.6.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_arm64.tar.gz)
- [mtk_2.6.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_amd64.tar.gz)
- [mtk_2.6.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_amd64.tar.gz)
- [mtk_2.6.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_arm64.tar.gz)
- [mtk_2.6.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_amd64_db2.tar.gz)
- [mtk_2.6.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_amd64_db2.tar.gz)
- [mtk_2.6.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_amd64.tar.gz)
- [mtkd_2.6.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_amd64_db2.tar.gz)
- [mtkd_2.6.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** File size mismatch when exporting file specified file size
- **MySQL:** Mig MySQL varchar null issue
- **Oracle:** Rewrite Oracle trigger `IF:NEW.ID` to openGauss issue
- **Oracle:** Rewrite the last parameter of the function with comments issue
- **Oracle:** Spatial Rectangle issue
- **Oracle:** Mig Oracle `BINARY_DOUBLE` and `UDT` Data to openGauss issue

### Performance Improvements

- **MTK:** Optimized record structure

## 2.6.1

2022-09-19

### MTK

- [mtk_2.6.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_windows_amd64.tar.gz)
- [mtk_2.6.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_arm64.tar.gz)
- [mtk_2.6.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_amd64.tar.gz)
- [mtk_2.6.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_amd64.tar.gz)
- [mtk_2.6.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_arm64.tar.gz)
- [mtk_2.6.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_amd64_db2.tar.gz)
- [mtk_2.6.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_amd64_db2.tar.gz)
- [mtk_2.6.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_amd64.tar.gz)
- [mtkd_2.6.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_amd64_db2.tar.gz)
- [mtkd_2.6.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** Mig MySQL Blob Issue
- **Oracle:** Mig MySQL Blob Issue
- **MTK** `mig-select` create table failed exit issue

### Performance Improvements

- Optimize Performance

## 2.6.0

2022-09-13

### MTK

- [mtk_2.6.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_windows_amd64.tar.gz)
- [mtk_2.6.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_arm64.tar.gz)
- [mtk_2.6.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_amd64.tar.gz)
- [mtk_2.6.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_amd64.tar.gz)
- [mtk_2.6.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_arm64.tar.gz)
- [mtk_2.6.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_amd64_db2.tar.gz)
- [mtk_2.6.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_amd64_db2.tar.gz)
- [mtk_2.6.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_amd64.tar.gz)
- [mtkd_2.6.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_amd64_db2.tar.gz)
- [mtkd_2.6.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** solve some null pointer issue
- **MTK:** command line parameter priority issue
- **MTKD:** command line conversion SQL statement issue
- **MySQL:** MySQL query view definition issue
- **MySQL:** Migrate MySQL `set` type to PostgreSQL/openGauss/MogDB `Varchar` type
- **Oracle:** Migrating Oracle `Spatial` type To PostgreSQL/openGauss/MogDB issue
- **PostgreSQL:** Export Oracle to PostgreSQL/openGauss/MogDB CSV file issue

### Code Refactoring

- **MTK:** Optimize the openGauss/PostgreSQL driver
- **PostgreSQL:** Storage related logic

### Features

- **MTK:** `show-table` adds `detail` parameter
- **MTK:** added `self upgrade` command
- **MTK:** update example configuration file
- **MTK:** `convert-plsql` adds `output` parameter
- **MTKD:** added `self upgrade` command
- **MySQL:** supports MySQL Spatial Data Types
- **MySQL:** Add `srs_id` for MySQL 8.0 column information
- **Oracle:** Support Pure Golang Oracle Driver Read Data
- **Oracle:** Support Pure go oracle client `go-ora`
- **openGauss:** Support Local Socket Connect

### Performance Improvements

- **MTK:** Optimize Performance
- **MTK:** Optimize Export file performance
