---
title: Release 2.4
summary: Release 2.4
author: Bin.Liu
date: 2022-07-29
---


# Release 2.4 Notes

## v2.4.4

2022-08-01

### MTK

- [mtk_2.4.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_amd64.tar.gz)
- [mtk_2.4.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_amd64_db2.tar.gz)
- [mtk_2.4.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_arm64.tar.gz)
- [mtk_2.4.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_amd64.tar.gz)
- [mtk_2.4.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_amd64_db2.tar.gz)
- [mtk_2.4.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_arm64.tar.gz)
- [mtk_2.4.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_windows_amd64.tar.gz)
- [mtk_2.4.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_amd64.tar.gz)
- [mtkd_2.4.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_amd64_db2.tar.gz)
- [mtkd_2.4.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** DDL script generation issue
- **MySQL:** Migrating MySQL double to openGauss issue
- **config:** remove config file default to uppercase logic
- **openGauss:** create view without commit issue

### Code Refactoring

- **MySQL:** Optimize DB2 auto-increment column migration to MySQL auto-increment column logic
- **Oracle:** Optimize query custom type SQL statement

### Features

- **MySQL:** Support MySQL Bit type to migrate to MySQL bit type
- **MySQL:** Support generating MySQL foreign key pointing to index statement
- **Oracle:** Support Oracle `type body` rewriting
- **mtkd:** Added preCheck API interface
- **mtkd:** table `task_run` adds error reason field
- **openGauss:** Support serial column addition `ALTER SEQUENCE seq_name OWNED BY table_name.column_name` statement

## v2.4.3

2022-07-18

### MTK

- [mtk_2.4.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_amd64.tar.gz)
- [mtk_2.4.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_amd64_db2.tar.gz)
- [mtk_2.4.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_arm64.tar.gz)
- [mtk_2.4.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_amd64.tar.gz)
- [mtk_2.4.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_amd64_db2.tar.gz)
- [mtk_2.4.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_arm64.tar.gz)
- [mtk_2.4.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_windows_amd64.tar.gz)
- [mtk_2.4.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_amd64.tar.gz)
- [mtkd_2.4.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_amd64_db2.tar.gz)
- [mtkd_2.4.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** Rewrite the syntax error of the sequence `.nextval/currval` in Oracle Package
- **Oracle:** Rewrite Oracle type to openGauss `varchar2 (4000)` problem
- **Oracle:** Rewrite Oracle `decode` to include comment syntax errors
- **Oracle:** Rewrite Oracle trigger `into:new.id` to openGauss syntax error
- **Oracle:** Convert Oracle `number(*,2)` to openGauss `NUMERIC(2,2)` error problem
- **openGauss:** Create view does not set `search_path` problem
- **openGauss:** Migration Package automatically generates synonyms, ignoring the migration of such synonyms
- **openGauss:** table column duplication problem

### Code Refactoring

- **Oracle:** Rename postgres to postgresql
- **file:** generate SQL script
- **openGauss:** parsing database version
- **plsql:** conversion Type syntax problem

### Features

- **MySQL:** Support converting MySQL double-quote constants to openGauss single-quote constants
- **MySQL:** support converting MySQL `on update current_timestamp` to MogDB 3.0 B compatibility mode
- **Oracle:** openGauss/MogDB 3.0.0 no longer converts Oracle `decode` functions
- **Oracle:** Support the coexistence of Oracle function out and return rewriting
- **Oracle:** Support rewriting part of `new type` syntax in stored procedures/packages/functions
- **Oracle:** Rewrite Oracle `table(` to openGauss `UNNEST(` syntax
- **Oracle:** openGauss/MogDB 3.0.0 and later not in the conversion header `:=` to `default`
- **Oracle:** rewrite Oracle `extend` function as a comment
- **Oracle:** add migration user permission check
- **mtk:** Add source invalid object failure statistics
- **mtk:** Support encrypted ciphers and provides a subcommand [encrypt](../commands/mtk_encrypt.md) to generate encrypted ciphers
- **openGauss:** Modify the default version of openGauss to 3.0.0
- **openGauss:** Support migrating MogDB column properties `on update current_timestamp`
- **openGauss:** Support migration MogDB/openGauss `create type as table of`
- **openGauss:** add keyword `VERIFY`

## v2.4.2

2022-07-04

### MTK

- [mtk_2.4.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_amd64.tar.gz)
- [mtk_2.4.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_amd64_db2.tar.gz)
- [mtk_2.4.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_arm64.tar.gz)
- [mtk_2.4.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_amd64.tar.gz)
- [mtk_2.4.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_amd64_db2.tar.gz)
- [mtk_2.4.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_arm64.tar.gz)
- [mtk_2.4.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_windows_amd64.tar.gz)
- [mtk_2.4.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_amd64.tar.gz)
- [mtkd_2.4.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_amd64_db2.tar.gz)
- [mtkd_2.4.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** batch commit some issue
- **Oracle:** rewrite float(64) syntax problem
- **Oracle:** Rewrite Oracle Package to openGauss package Some problems
- **Oracle:** rewrite Oracle Type to openGauss Type Some problems
- **Oracle:** Rewrite Oracle Package without package body restrictions

### Features

- **MySQL:** add long_query_time default connect parameter. default 36000
- **Oracle:** support oracle column udt type
- **Oracle:** support table of type
- **Oracle:** rewrite Oracle `PIPELINED` to PostgreSQL `setof`

## v2.4.1

2022-06-24

### MTK

- [mtk_2.4.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_amd64.tar.gz)
- [mtk_2.4.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_amd64_db2.tar.gz)
- [mtk_2.4.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_arm64.tar.gz)
- [mtk_2.4.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_amd64.tar.gz)
- [mtk_2.4.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_amd64_db2.tar.gz)
- [mtk_2.4.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_arm64.tar.gz)
- [mtk_2.4.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_windows_amd64.tar.gz)
- [mtk_2.4.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_amd64.tar.gz)
- [mtkd_2.4.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_amd64_db2.tar.gz)
- [mtkd_2.4.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** query lost primary key column information
- **db2:** constraint repetition problem
- **mtkd:** task_result_details sequence issue

### Code Refactoring

- **db2:** query index method

### Features

- **MTK:** support Informix migration to openGauss/MogDB
- **db2:** Add object ID query
- **mtk:** supports MySQL/openGasuss/MogDB/PostgreSQL batch commit

### Performance Improvements

- **db2:** Optimize Convert float64 to string
- **db2:** Optimize Convert TimeDuration To OraInterval
