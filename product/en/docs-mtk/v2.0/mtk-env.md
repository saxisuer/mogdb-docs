---
title: MTK Environment
summary: MTK Environment
author: Liu Xu
date: 2021-03-04
---

# MTK Environment

Migrating different databases requires installation of necessary client software.

<br/>

## Oracle

1. Visit [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html) to choose the corresponding platform.
   - [Instant Client for Linux x86-64](https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html)
   - [Instant Client for Linux ARM (aarch64)](https://www.oracle.com/database/technologies/instant-client/linux-arm-aarch64-downloads.html)
   - [Instant Client for macOS (Intel x86)](https://www.oracle.com/database/technologies/instant-client/macos-intel-x86-downloads.html)

2. Choose the corresponding version, such as `Version 19.10.0.0.0`.

3. Download the`Basic Package (ZIP)` package and upload it to the server, and run the following command to decompress it:

   ```
   # This package applies to the arm64 platform.
   unzip instantclient-basic-linux.arm64-19.10.0.0.0dbru.zip
   ```

   After the package is decompressed, the directory like `instantclient_19_10` will be obtained.

4. Configure environment variables.

   ```bash
   Linux:
   export LD_LIBRARY_PATH=< target directory>/instantclient_19_10:$LD_LIBRARY_PATH
   Mac:
   export DYLD_LIBRARY_PATH=< target directory>/instantclient_19_10$DYLD_LIBRARY_PATH
   ```

<br/>

## DB2

1. Visit [DB2 ODBC CLI](http://public.dhe.ibm.com/ibmdl/export/pub/software/data/db2/drivers/odbc_cli/)

2. Choose the platform version. Download package and upload it to the server, and run the following command to decompress it:

   ```bash
   tar -zxvf linuxx64_odbc_cli.tar.gz -C /db2client
   ```

3. Configure environment variables.

   ```bash
   export DB2HOME=/db2client/clidriver
   export CGO_CFLAGS=-I$DB2HOME/include
   export CGO_LDFLAGS=-L$DB2HOME/lib
   export PATH=$DB2HOME/bin:$PATH
   Linux:
   export LD_LIBRARY_PATH=$DB2HOME/lib:$LD_LIBRARY_PATH
   Mac:
   export DYLD_LIBRARY_PATH=$DB2HOME/lib:$DYLD_LIBRARY_PATH
   ```

4. Test

    ```bash
    db2cli --version
    ```

If the DB2 ODBC CLI driver is not installed, the following error will occur.

```bash
mtk: error while loading shared libraries: libdb2.so.1: cannot open shared object file: No such file or directory
mtk: error while loading shared libraries: libdb2.so.1: cannot open shared object file: No such file or directory
```

<br/>

## MySQL

No client needs to be installed.

<br/>

## openGauss

No client needs to be installed.

## Informix

See [DB2](#db2)
