---
title: Overview of MTK
summary: Overview of MTK
author: Liu Xu
date: 2021-09-03
---

# Overview of MTK

MTK (Database Migration Toolkit) is a tool used for quickly importing the data structure and full data of Oracle/DB2/MySQL/openGauss/SqlServer/Informix into MogDB. MTK of the latest version also supports compatibility with MogDB by rewriting and importing stored procedures, functions, triggers and other program segments in Oracle databases.

## Support for Multiple Database Types

- Data migration between Oracle, DB2, openGauss, SQL Server, MySQL and Informix is supported. These databases act as the source or target database of each other.
- Database content can be exported as an executable SQL script.

## Migration Performance Adjustment

- During data migration, such parameters as batch query and batch insert size can be modified to adjust the data migration performance.
- During data migration, multiple concurrency, parallelism, and data fragmentation are supported.

## Structures Separated from Data

- Both the structure and data of the migration object can be migrated simultaneously. Additionally, migrating only the structure or only the data is also supported. Generally, data migration is later than structure migration.
- The migration range can be specified, such as table-level or schema-level migration. You can specify whether to migrate all objects or some objects under a schema.
- Schema remapping is supported during the migration. In other word, objects under the source schema can be migrated to a different schema in the target database.

## Procedure Migration from Oracle/MySQL/DB2 to openGauss

Storage procedures, functions, triggers, and packages can be migrated from Oracle/MySQL/DB2 to openGauss.

According to the openGauss syntax rules, Oracle/MySQL/DB2 procedures are automatically rewritten and then created in the target openGauss database.

## Migration Scenario

| Source Database | Target Database |
| :-------------- | :-------------- |
| Oracle          | openGauss       |
| Oracle          | MySQL           |
| Oracle          | PostgreSQL      |
| MySQL           | openGauss       |
| MySQL           | MySQL           |
| MySQL           | PostgreSQL      |
| DB2             | openGauss       |
| DB2             | MySQL           |
| DB2             | PostgreSQL      |
| DB2             | DB2             |
| SqlServer       | openGauss       |
| SqlServer       | MySQL           |
| SqlServer       | PostgreSQL      |
| openGauss       | openGauss       |
| openGauss       | PostgreSQL      |
| PostgreSQL      | openGauss       |
| Informix        | openGauss       |

<br/>

## Supported Database Objects

For details, run the [show-type](commands/mtk_show-type.md) command.

| Name           | Supported or Not | Description                                                                        |
|----------------|------------------|------------------------------------------------------------------------------------|
| Schema         | Supported        | Oracle Users <br />MySQL Database <br />DB2 Schema <br />SqlServer Database Schema |
| Sequence       | Supported        |                                                                                    |
| ObjectType     | Supported        |                                                                                    |
| Domain         | Not Supported    |                                                                                    |
| Wrapper        | Supported        | Only DB2                                                                           |
| Server         | Supported        | Only DB2                                                                           |
| User-mapping   | Supported        | Only DB2                                                                           |
| Queue          | Not Supported    |                                                                                    |
| Table          | Supported        | OBJECT TYPE TABLE is not                                                           |
| NickName       | Supported        | Only DB2                                                                           |
| Rule           | Supported        |                                                                                    |
| TableData      | Supported.       | Table data                                                                         |
| Index          | Supported        |                                                                                    |
| Constraint     | Supported        |                                                                                    |
| DBLink         | Supported        | Database connection  Support only the query                                        |
| View           | Supported        |                                                                                    |
| MView          | Supported        | Materialize View                                                                   |
| Function       | Supported        |                                                                                    |
| Procedure      | Supported        |                                                                                    |
| Package        | Supported        |                                                                                    |
| Trigger        | Supported        |                                                                                    |
| Synonym        | Supported        |                                                                                    |
| TableDataCom   | Supported        | Comparison of table column count                                                   |
| AlterSequence  | Supported        | Synchronizing the last value of a sequence                                         |
| CollStatistics | Supported        | Collection of statistics                                                           |

<br/>

## Procedure Conversion

MTK supports the following procedure conversion.

- Type conversion of input and output parameters

- Function overloading

- "FROM dual" is removed.

- Syntax conversion

  | Before Conversion                      | After Conversion       |
  | :------------------------------------- | :--------------------- |
  | connect by                             | CTE rewriting          |
  | EXECUTE IMMEDIATE                      | EXECUTE                |
  | Partial syntax for EXIT WHEN NOT FOUND |                        |
  | interval syntax                        |                        |
  | mod syntax: counter mod 1000 = 0       | mod(counter,4) == 0    |
  | null related syntax:  !=&#124;<> null  | is not null            |
  | PIPE ROW                               | RETURN NEXT            |
  | select 1,2 into                        | select 1,2 into STRICT |
  | select unique                          | SELECT DISTINCT        |
  | sequence nextval/currval               | nextval(xxx)           |
  | SQLCODE                                | SQLSTATE               |
  | Partial syntax for truncate table      |                        |

- Function conversion

  | Before conversion             | After conversion                           |
  | :---------------------------- | :----------------------------------------- |
  | ADD_YEARS                     |                                            |
  | DBMS_OUTPUT.PUT_LINE          | RAISE NOTICE                               |
  | DBMS_LOB.GETLENGTH            | octet_length                               |
  | DBMS_LOB.SUBSTR               | substr                                     |
  | DBMS_STANDARD.RAISE EXCEPTION |                                            |
  | decode                        | case when                                  |
  | empty_blob/empty_clob         | null                                       |
  | from_tz                       |                                            |
  | listagg                       | string_agg                                 |
  | months_between                |                                            |
  | NUMTODSINTERVAL               |                                            |
  | NUMTOYMINTERVAL               |                                            |
  | nvl2                          | case when                                  |
  | raise_application_error       | RAISE EXCEPTION                            |
  | SYS_CONTEXT                   |                                            |
  | SYSTIMESTAMP                  | CURRENT_TIMESTAMP                          |
  | TO_CHAR                       | ::varchar                                  |
  | to_char(l_curr_time, 'TZH')   | EXTRACT( timezone_hour from l_curr_time)   |
  | to_char(l_curr_time, 'TZM')   | EXTRACT( timezone_minute from l_curr_time) |
  | TO_CLOB                       | ""                                         |
  | TO_TIMESTAMP_TZ               |                                            |
  | trunc                         | date_trunc                                 |
  | UTL_ROW.CAST_TO_RAW           | encode                                     |
