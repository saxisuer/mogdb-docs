---
title: "mtk sync-trigger"
summary: mtk sync-trigger
author: mtk
date: 2022-09-21
---
## mtk sync-trigger

sync trigger.

### Synopsis

sync trigger.

```bash
mtk sync-trigger [flags]
```

### Options

```bash
  -h, --help   help for sync-trigger
```

### Options inherited from parent commands

```bash
      --batchSize int            Specify the batch size to be used for bulk/copy insert.
                                 Valid values are  1-50000, default batch size is 1000
                                 for MySQL,Oracle,openGauss,PostgreSQL.
      --bufferSize int           Specify the Buffer size in MB, to be used inserting or querying. 
                                 Valid value is from 1 to 1024, default Buffer size is 8 MB
                                 for postgres,openGauss,mysql.
      --caseSensitive int        Object case parameters in SQL statements.
                                 1 - lower case 
                                 2 - upper case 
                                 3 - Keep it the same as the source database.
      --channelCacheNum int      Specify channelCacheNum.
                                 Valid values are  1-50000, default batch size is 10000
  -c, --config string            Set mtk config file. Support json,yaml. [env MTK_CONFIG] (default "mtk.json")
      --cpBufferSize int         Specify the Copy Buffer size in MB, to be used in the Copy Command. 
                                 Valid value is from 1 to 1024, default Copy Buffer Size is 8 MB
                                 for PostgreSQL,openGauss.
  -d, --debug                    Set the debug mode.
                                 Not necessary for the normal usage. [env MTK_DEBUG]
      --disableCollStatistics    disable Collect table statistics.
      --disableFKCons            Disable table foreign key sync
      --disableIgnoreCase        Disable ignoring case queries
      --disableSelectPart        Disable the select by partition
      --disableTableDataComp     disable table select count compare.
      --enableSyncCompTabPro     Enable Synchronize table compressed properties
      --fetchSize int            Specify fetch size in terms of number of rows should be fetched in result set at a time.
                                 Valid values are  1-50000, default fetch size is 1000
                                 for Oracle. (default 1000)
      --file                     export to file
      --fileType string          Indicates the type of a file when data is to be exported to a file.
                                 support csv,sql
      --noTerminalReport         The terminal does not print an overview of the migration report
  -p, --parallel int             Specify the parallelism. 
                                 the degree of parallelism is now only useful when migrating table data and parallelism when creating an index. (default 1)
      --path string              Indicates the directory of a file to which data is to be exported. 
                                 Default value is config.target.parameter.path. If not configured, the system default value ./data
                                 Command Value > Config Value > MTK Default Value
      --preRun                   preRun check.
  -r, --reportFile string        Set mtk report file or dir. If it is a file, use the file directory. 
                                 Default value is ./report
                                 report directory format [./report/reportYYYYMMDDHHMISS] ./report/report20210101121314
      --schemas string           the Migrate schema,Separated by commas.(schema1,schema2,db1)
      --seqLastNumAddNum int     The last value of the sequence is increased
      --tableSkip stringArray    Define table split. Can be specified multiple times. 
                                 format schema.tableName
                                 --tableSkip MTK.TABLE01
                                 --tableSkip MTK.TABLE02
      --tableSplit stringArray   Define table split. Can be specified multiple times. 
                                 format schema.tableName:where:where:where
                                 --tableSplit 'MTK.TABLE01: "ID">100 AND ID<=200: ID>200 AND ID<=300:ID>300'
                                 --tableSplit "MTK.TABLE02: \"ID\">100 AND ID<=200: ID>200 AND ID<=300:ID>300 AND COL1='1'"
      --tables string            the Migrate tables,Separated by commas.(tab1,schema1.tab1)
```

### SEE ALSO

* [mtk](./mtk.md)     - Database Migration Toolkit
