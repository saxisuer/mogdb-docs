---
title: "mtk"
summary: mtk
author: mtk
date: 2022-09-21
---
## mtk

Database Migration Toolkit

### Synopsis

The MTK(Database Migration Toolkit) helps you migrate your database

```bash
mtk [flags]
```

### Examples

```
# init project
./mtk init-project -n ora2og

# Generate configuration file. 
# edit connection information and migration objects (tables/schemas). 
# the directory definition does not need to be modified
vi ora2og/config/mtk.json

# Run
./mtk -c ora2og/config/mtk.json

# Specify migration report
./mtk -c ora2og/config/mtk.json

# Specify debug mode
./mtk -c ora2og/config/mtk.json --debug

# Migrate to a file
./mtk -c ora2og/config/mtk.json --file

# Only Schema is migrated
./mtk -c ora2og/config/mtk.json --schemaOnly

# Only Data is migrated
./mtk -c ora2og/config/mtk.json --dataOnly

# Defining the migration table overrides the contents of the configuration file
./mtk -c ora2og/config/mtk.json --tables schema1.table1,schema2.table2

# Defining the migration schema overrides the contents of the configuration file
./mtk -c ora2og/config/mtk.json --schemas schema1,schema2

```

### Options

```bash
      --batchSize int            Specify the batch size to be used for bulk/copy insert.
                                 Valid values are  1-50000, default batch size is 1000
                                 for MySQL,Oracle,openGauss,PostgreSQL.
      --bufferSize int           Specify the Buffer size in MB, to be used inserting or querying. 
                                 Valid value is from 1 to 1024, default Buffer size is 8 MB
                                 for postgres,openGauss,mysql.
      --caseSensitive int        Object case parameters in SQL statements.
                                 1 - lower case 
                                 2 - upper case 
                                 3 - Keep it the same as the source database.
      --channelCacheNum int      Specify channelCacheNum.
                                 Valid values are  1-50000, default batch size is 10000
  -c, --config string            Set mtk config file. Support json,yaml. [env MTK_CONFIG] (default "mtk.json")
      --cpBufferSize int         Specify the Copy Buffer size in MB, to be used in the Copy Command. 
                                 Valid value is from 1 to 1024, default Copy Buffer Size is 8 MB
                                 for PostgreSQL,openGauss.
      --dataOnly                 the only Migrate table data
  -d, --debug                    Set the debug mode.
                                 Not necessary for the normal usage. [env MTK_DEBUG]
      --disableCollStatistics    disable Collect table statistics.
      --disableFKCons            Disable table foreign key sync
      --disableIgnoreCase        Disable ignoring case queries
      --disableSelectPart        Disable the select by partition
      --disableTableDataComp     disable table select count compare.
      --fetchSize int            Specify fetch size in terms of number of rows should be fetched in result set at a time.
                                 Valid values are  1-50000, default fetch size is 1000
                                 for Oracle. (default 1000)
      --file                     export to file
      --fileType string          Indicates the type of a file when data is to be exported to a file.
                                 support csv,sql
  -h, --help                     help for mtk
      --httpAddr string          Set mtk run Http Service Address [env MTK_HTTP_ADDR]
      --logDir string            Set mtk run Http Service log dir [env MTK_LOG_DIR]
      --logfile string           Set mtk log file. Default value is reportFile dir.
      --noTerminalReport         The terminal does not print an overview of the migration report
  -p, --parallel int             Specify the parallelism. 
                                 the degree of parallelism is now only useful when migrating table data and parallelism when creating an index. (default 1)
      --path string              Indicates the directory of a file to which data is to be exported. 
                                 Default value is config.target.parameter.path. If not configured, the system default value ./data
                                 Command Value > Config Value > MTK Default Value
      --preRun                   preRun check.
  -r, --reportFile string        Set mtk report file or dir. If it is a file, use the file directory. 
                                 Default value is ./report
                                 report directory format [./report/reportYYYYMMDDHHMISS] ./report/report20210101121314
      --reportServer string      reportServer [env MTK_REPORT_SERVER]
      --schemaOnly               the only Migrate schema ddl
      --schemas string           the Migrate schema,Separated by commas.(schema1,schema2,db1)
      --seqLastNumAddNum int     The last value of the sequence is increased
      --tableSkip stringArray    Define table split. Can be specified multiple times. 
                                 format schema.tableName
                                 --tableSkip MTK.TABLE01
                                 --tableSkip MTK.TABLE02
      --tableSplit stringArray   Define table split. Can be specified multiple times. 
                                 format schema.tableName:where:where:where
                                 --tableSplit 'MTK.TABLE01: "ID">100 AND ID<=200: ID>200 AND ID<=300:ID>300'
                                 --tableSplit "MTK.TABLE02: \"ID\">100 AND ID<=200: ID>200 AND ID<=300:ID>300 AND COL1='1'"
      --tables string            the Migrate tables,Separated by commas.(tab1,schema1.tab1)
  -v, --version                  MTK Version
```

### SEE ALSO

* [mtk init-project](./mtk_init-project.md)     - Initialise a typical mtk project tree. Top directory will be created under project base dir.
* [mtk config-check](./mtk_config-check.md)     - check configuration file has errors
* [mtk config-gen](./mtk_config-gen.md)     - Interactively generate configuration files
* [mtk gen](./mtk_gen.md)     - gen command docs, shell autocompletion script
* [mtk license](./mtk_license.md)     - manage license
* [mtk mig-tab-pre](./mtk_mig-tab-pre.md)     - mig database object Schema,Sequence,ObjectType,Domain,Queue,Wrapper,Server,UserMapping,Table,NickName,Rule
* [mtk mig-tab-data](./mtk_mig-tab-data.md)     - mig database object TableData,TableDataEstimate,CheckTableData
* [mtk mig-tab-post](./mtk_mig-tab-post.md)     - mig database object Index,Constraint,AlterSequence,TableDataCom,CollStatistics
* [mtk mig-tab-other](./mtk_mig-tab-other.md)     - mig database object DBLink,View,MaterializedView,Function,Procedure,Package,Trigger,Synonym
* [mtk mig-select](./mtk_mig-select.md)     - migrate custom query statements to csv or target table
* [mtk mig-selects](./mtk_mig-selects.md)     - migrate custom query statements to csv or target table
* [mtk show-schema](./mtk_show-schema.md)     - Show Source Database schema.
* [mtk show-db-info](./mtk_show-db-info.md)     - show database Information.
* [mtk show-type](./mtk_show-type.md)     - show support database type.
* [mtk show-table](./mtk_show-table.md)     - display size of the top n table to be migrated
* [mtk show-table-split](./mtk_show-table-split.md)     - split large tables in parallel according to user defined parallelism
* [mtk show-support-db](./mtk_show-support-db.md)     - show support database version.
* [mtk show-table-data-estimate](./mtk_show-table-data-estimate.md)     - estimate table data migration time.
* [mtk report](./mtk_report.md)     - Convert mtk report file to sql script.
* [mtk sync-schema](./mtk_sync-schema.md)     - sync schema.
* [mtk sync-object-type](./mtk_sync-object-type.md)     - sync object-type.
* [mtk sync-domain](./mtk_sync-domain.md)     - sync domain.
* [mtk sync-sequence](./mtk_sync-sequence.md)     - sync sequence.
* [mtk sync-queue](./mtk_sync-queue.md)     - sync queue.
* [mtk sync-wrapper](./mtk_sync-wrapper.md)     - sync db2 wrapper.
* [mtk sync-server](./mtk_sync-server.md)     - sync db2 server.
* [mtk sync-user-mapping](./mtk_sync-user-mapping.md)     - sync db2 user mapping.
* [mtk sync-table](./mtk_sync-table.md)     - sync table.
* [mtk sync-nickname](./mtk_sync-nickname.md)     - sync db2 nickname.
* [mtk sync-table-data](./mtk_sync-table-data.md)     - sync table data.
* [mtk sync-table-data-estimate](./mtk_sync-table-data-estimate.md)     - estimate table data migration time.
* [mtk sync-index](./mtk_sync-index.md)     - sync index.
* [mtk sync-constraint](./mtk_sync-constraint.md)     - sync constraint.
* [mtk sync-view](./mtk_sync-view.md)     - sync view.
* [mtk sync-mview](./mtk_sync-mview.md)     - sync materialized view.
* [mtk sync-trigger](./mtk_sync-trigger.md)     - sync trigger.
* [mtk sync-procedure](./mtk_sync-procedure.md)     - sync procedure.
* [mtk sync-function](./mtk_sync-function.md)     - sync function.
* [mtk sync-package](./mtk_sync-package.md)     - sync package.
* [mtk sync-synonym](./mtk_sync-synonym.md)     - sync synonym.
* [mtk sync-db-link](./mtk_sync-db-link.md)     - sync db link.
* [mtk sync-rule](./mtk_sync-rule.md)     - sync rule.
* [mtk sync-table-data-com](./mtk_sync-table-data-com.md)     - table rows count comparison.
* [mtk sync-alter-sequence](./mtk_sync-alter-sequence.md)     - modify sequence start value.
* [mtk sync-coll-statistics](./mtk_sync-coll-statistics.md)     - collect table statistics.
* [mtk check-table-data](./mtk_check-table-data.md)     - Check the table data for exception data
* [mtk show-drop-index](./mtk_show-drop-index.md)     - generate drop index sql
* [mtk show-drop-cons](./mtk_show-drop-cons.md)     - generate drop constraint sql
* [mtk version](./mtk_version.md)     - mtk version
* [mtk encrypt](./mtk_encrypt.md)     - Provides a convenient way to encrypt your text/password
* [mtk convert-plsql](./mtk_convert-plsql.md)     - Convert PLSQL Statements
* [mtk self](./mtk_self.md)     - Modify the mtk installation
