---
title: MTK Usage
summary: MTK Usage
author: Liu Xu
date: 2021-03-04
---

# MTK Usage

<div class="asciinema-player" data-cast="mtk"></div>

## Show Version

```bash
./mtk version
```

## Apply the license

```bash
./mtk license gen
```

## Migrate Oracle To MogDB

Migrating Oracle to MogDB FAQ reference [Oracle To MogDB](./faq/mtk-oracle-to-openGauss.md)

### Install [Oracle](./mtk-env.md#oracle) Client

### init project

```bash
./mtk init-project -s oracle -t mogdb -n ora2mg
# will build a migration directory
ora2mg
├── config
│   └── mtk.json
├── data
├── report
└── schema
```

### edit the configuration file

Edit the configuration file in advance. For details, see [Configuration File](./config/mtk-config.md)

```bash
vi ora2mg/config/mtk.json
```

1. Edit `source` node to define the source database connection information. Configuration reference [source](config/mtk-config.md#source)
2. Edit `target` node to define the target database connection information. Configuration reference [target](config/mtk-config.md#target)

    > Modify the connect and type information, and the parameter is adjusted according to the operation

3. Edit `object` node to define the migration object. Configuration reference [object](config/mtk-object.md#object)

After editing, run [config-check](commands/mtk_config-check.md)

```shell
./mtk config-check -c ora2mg/config/mtk.json
```

The configuration file will output type information normally

```shell
use config : ora2mg/config/mtk.json
There is no error in the configuration file
```

### run

```bash
./mtk -c ora2mg/config/mtk.json
# debug
./mtk -c ora2mg/config/mtk.json --debug
# only Schema is migrated
./mtk -c ora2mg/config/mtk.json --schemaOnly
# Only Data is migrated
./mtk -c ora2mg/config/mtk.json --dataOnly
```

### view Report

Reports and run logs are generated in the `ora2og/report` directory. as follows

```bash
[2022-01-20 11:29:25.800978]  INFO reportDir: ora2mg/report/report_20220120110125 function=GenReport line=412 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.426822]  INFO the text report : ora2mg/report/report_20220120110125.txt function=PrintReport line=270 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.429545]  INFO the warring report : ora2mg/report/report_20220120110125.warring function=PrintReport line=281 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.430118]  INFO the error report : ora2mg/report/report_20220120110125.err file=mtk/cmd/mtk/services/cmd.go function=PrintReport line=292
```

| file name                                   | description                                       |
|---------------------------------------------|---------------------------------------------------|
| ora2mg/report/report_20220120110125         | html report                                       |
| ora2mg/report/report_20220120110125.txt     | text report                                       |
| ora2mg/report/report_20220120110125.warring | A text report that contains only warning messages |
| ora2mg/report/report_20220120110125.err     | A text report that contains only error messages   |

### export as a file

```bash
./mtk -c ora2mg/config/mtk.json --file
# Only Schema is migrated
./mtk -c ora2mg/config/mtk.json --file --schemaOnly
# Only Data is migrated
./mtk -c ora2mg/config/mtk.json --file --dataOnly
```

### The command line specifies the migration object

```bash
# specify the table
./mtk -c ora2mg/config/mtk.json --tables schema1.table1,schema2.table2
# specify the schema
./mtk -c ora2mg/config/mtk.json --schemas schema1,schema2
```

## Migrate MySQL to MogDB

Migrating MySQL to MogDB FAQ reference [MySQL To MogDB](./faq/mtk-mysql-to-openGauss.md)

### init project

```shell
./mtk init-project -s mysql -t mogdb -n my2mg
```

### edit the configuration file

```shell
vi my2mg/config/mtk.json
```

1. Edit `source` node to define the source database connection information. Configuration reference [source](config/mtk-config.md#source)
2. Edit `target` node to define the target database connection information. Configuration reference [target](config/mtk-config.md#target)

    > Modify the connect and type information, and the parameter is adjusted according to the operation

3. Edit `object` node to define the migration object. Configuration reference [object](config/mtk-object.md#object)

After editing, run [config-check](commands/mtk_config-check.md)

```shell
./mtk config-check -c my2mg/config/mtk.json
```

The configuration file will output type information normally

```shell
use config : my2mg/config/mtk.json
There is no error in the configuration file
```

### run

```bash
./mtk -c my2mg/config/mtk.json
```

## Migrate DB2 to MogDB

Migrating DB2 to MogDB FAQ reference [DB2 To MogDB](./faq/mtk-db2-to-openGauss.md)

### Install [db2](./mtk-env.md#db2) Client

### init project

```shell
./mtk init-project -s db2 -t mogdb -n db22mg
```

### edit the configuration file

```shell
vi db22mg/config/mtk.json
```

1. Edit `source` node to define the source database connection information. Configuration reference [source](config/mtk-config.md#source)
2. Edit `target` node to define the target database connection information. Configuration reference [target](config/mtk-config.md#target)

    > Modify the connect and type information, and the parameter is adjusted according to the operation

3. Edit `object` node to define the migration object. Configuration reference [object](config/mtk-object.md#object)

After editing, run [config-check](commands/mtk_config-check.md)

```shell
./mtk config-check -c db22mg/config/mtk.json
```

The configuration file will output type information normally

```shell
use config : db22mg/config/mtk.json
There is no error in the configuration file
```

### run

```bash
./mtk -c db22mg/config/mtk.json
```

## Migrate Informix to MogDB

Migrating Informix to MogDB FAQ reference [Informix To MogDB](./faq/mtk-informix-to-openGauss.md)

### Install [db2](./mtk-env.md#db2) Client

### init project

```shell
./mtk init-project -s informix -t mogdb -n ifx2mg
```

### edit the configuration file

```shell
vi ifx2mg/config/mtk.json
```

1. Edit `source` node to define the source database connection information. Configuration reference [source](config/mtk-config.md#source)
2. Edit `target` node to define the target database connection information. Configuration reference [target](config/mtk-config.md#target)

    > Modify the connect and type information, and the parameter is adjusted according to the operation

3. Edit `object` node to define the migration object. Configuration reference [object](config/mtk-object.md#object)

After editing, run [config-check](commands/mtk_config-check.md)

```shell
./mtk config-check -c ifx2mg/config/mtk.json
```

The configuration file will output type information normally

```shell
use config : ifx2mg/config/mtk.json
There is no error in the configuration file
```

### run

```bash
./mtk -c ifx2mg/config/mtk.json
```

## Migration methods

MTK supports three migration methods.

### Migrate all at once

```bash
./mtk -c mtk.json
```

### Combined migration

| Step                            | Command                                        |
|---------------------------------|------------------------------------------------|
| ./mtk mig-tab-pre -c mtk.json   | [mig-tab-pre](commands/mtk_mig-tab-pre.md)     |
| ./mtk mig-tab-data -c mtk.json  | [mig-tab-data](commands/mtk_mig-tab-data.md)   |
| ./mtk mig-tab-post -c mtk.json  | [mig-tab-post](commands/mtk_mig-tab-post.md)   |
| ./mtk mig-tab-other -c mtk.json | [mig-tab-other](commands/mtk_mig-tab-other.md) |

### Step-by-step migration

The user controls the overall migration logic.

| Step                                       | Command                                                                  | Desc                                |
|--------------------------------------------|--------------------------------------------------------------------------|-------------------------------------|
| ./mtk sync-schema -c mtk.json              | [mtk sync-schema](commands/mtk_sync-schema.md)                           | Migrate schema                      |
| ./mtk sync-sequence -c mtk.json            | [mtk sync-sequence](commands/mtk_sync-sequence.md)                       | Migrate Sequence                    |
| ./mtk sync-object-type -c mtk.json         | [mtk sync-object-type](commands/mtk_sync-object-type.md)                 | Migrate Object Type                 |
| ./mtk sync-domain -c mtk.json              | [mtk sync-domain](commands/mtk_sync-domain.md)                           | Migrate Domain                      |
| ./mtk sync-wrapper -c mtk.json             | [mtk sync-wrapper](commands/mtk_sync-wrapper.md)                         | Migrate DB2 Wrapper                 |
| ./mtk sync-server -c mtk.json              | [mtk sync-server](commands/mtk_sync-server.md)                           | Migrate DB2 Server                  |
| ./mtk sync-user-mapping -c mtk.json        | [mtk sync-user-mapping](commands/mtk_sync-user-mapping.md)               | Migrate DB2 User Mapping            |
| ./mtk sync-queue -c mtk.json               | [mtk sync-queue](commands/mtk_sync-queue.md)                             | Migrate queue table                 |
| ./mtk sync-table -c mtk.json               | [mtk sync-table](commands/mtk_sync-table.md)                             | Migrate Table                       |
| ./mtk sync-nickname -c mtk.json            | [mtk sync-nickname](commands/mtk_sync-nickname.md)                       | Migrate DB2 Nickname                |
| ./mtk sync-rule -c mtk.json                | [mtk sync-rule](commands/mtk_sync-rule.md)                               | Migrate Rule                        |
| ./mtk sync-table-data -c mtk.json          | [mtk sync-table-data](commands/mtk_sync-table-data.md)                   | Migrate Table Data                  |
| ./mtk sync-table-data-estimate -c mtk.json | [mtk sync-table-data-estimate](commands/mtk_sync-table-data-estimate.md) | Estimated table data migration time |
| ./mtk sync-index -c mtk.json               | [mtk sync-index](commands/mtk_sync-index.md)                             | Migrate Index                       |
| ./mtk sync-constraint -c mtk.json          | [mtk sync-constraint](commands/mtk_sync-constraint.md)                   | Migrate Constraint                  |
| ./mtk sync-db-link -c mtk.json             | [mtk sync-db-link](commands/mtk_sync-db-link.md)                         | Migrate Database Link               |
| ./mtk sync-view -c mtk.json                | [mtk sync-view](commands/mtk_sync-view.md)                               | Migrate View                        |
| ./mtk sync-mview -c mtk.json               | [mtk sync-mview](commands/mtk_sync-mview.md)                             | Migrate Materialized View           |
| ./mtk sync-function -c mtk.json            | [mtk sync-function](commands/mtk_sync-function.md)                       | Migrate Function                    |
| ./mtk sync-procedure -c mtk.json           | [mtk sync-procedure](commands/mtk_sync-procedure.md)                     | Migrate Procedure                   |
| ./mtk sync-package -c mtk.json             | [mtk sync-package](commands/mtk_sync-package.md)                         | Migrate Package                     |
| ./mtk sync-trigger -c mtk.json             | [mtk sync-trigger](commands/mtk_sync-trigger.md)                         | Migrate Trigger                     |
| ./mtk sync-synonym -c mtk.json             | [mtk sync-synonym](commands/mtk_sync-synonym.md)                         | Migrate Synonym                     |
| ./mtk sync-table-data-com -c mtk.json      | [mtk sync-table-data-com](commands/mtk_sync-table-data-com.md)           | table row count comparison          |
| ./mtk sync-alter-sequence -c mtk.json      | [mtk sync-alter-sequence](commands/mtk_sync-alter-sequence.md)           | Modify sequence start value         |
| ./mtk sync-coll-statistics -c mtk.json     | [mtk sync-coll-statistics](commands/mtk_sync-coll-statistics.md)         | Gather table statistics             |
| ./mtk check-table-data -c mtk.json         | [mtk check-table-data](commands/mtk_check-table-data.md)                 | Check table data for abnormal data  |
