---
title: HA FAQs
summary: HA FAQs
author: Guo Huan
date: 2021-09-26
---

# HA FAQs

**Q1: What is the function of MogHA?**

MogHA is an independent HA suite. It can handle multiple failures such as server or instance breakdown, thereby lowering the downtime from minutes to seconds.

**Q2: What is the HA architecture of MogDB?**

MogHA supports one primary database and at least one standby database or a maximum of eight standby databases (which is kept pace with openGauss of the latest version).

The architecture of one primary and multiple standby databases is optimized based on openGauss, in which RPO can reach zero and RTO is less than 10s. In other word, when the primary instance fails, the standby instance can take over services from the faulty primary instance within 10s, ensuring zero data loss.

**Q3：What is the common deployment architecture of MogDB?**

MogDB supports three types of nodes, including primary, standby, and cascaded nodes. The maximum number of standby and cascaded nodes totals eight, one primary node, four standby nodes, and four cascaded nodes, or one primary node and eight standby nodes. The following figure shows the production deployment architecture from one customer, including one primary node, five standby nodes, and one cascaded node.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/faqs/high-availability-faqs-en-1.png)

**Q4: How is RPO/RTO of MogDB achieved? Can HA reach 99.999%?**

RPO = 0, RTO < 10s, and SLA = 99.999%