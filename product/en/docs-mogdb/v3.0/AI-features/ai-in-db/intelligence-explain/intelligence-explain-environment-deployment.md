---
title: Environment Deployment
summary: Environment Deployment
author: Guo Huan
date: 2022-05-06
---

# Environment Deployment

## Prerequisites

MogDB is in the normal state. The user has logged in to MogDB with an authenticated identity. The executed SQL syntax is correct and no error is reported. In the historical performance data window, the number of MogDB concurrent tasks is stable, the structure and number of tables remain unchanged, the data volume changes smoothly, and the GUC parameters related to query performance remain unchanged. During prediction, the model has been trained and converged. The running environment of the AIEngine is stable.

## Request Example

The AIEngine process communicates with the kernel process using HTTPS. An example request is as follows:

```bash
curl -X POST -d '{"modelName":"modelname"}' -H 'Content-Type: application/json' https://IP-address:port/request-API
```

**Table 1** AIEngine external APIs

| Request API    | Description                                 |
| :------------- | :------------------------------------------ |
| /check         | Checks whether a model is properly started. |
| /configure     | Sets model parameters.                      |
| /train         | Trains a model.                             |
| /track_process | Views model training logs.                  |
| /setup         | Loads historical models.                    |
| /predict       | Predicts a model.                           |

## Generating Certificates

Before using the prediction function, you need to use OpenSSL to generate certificates required for authentication between the communication parties, ensuring communication security.

1. Set up a certificate generation environment. The certificate file is stored in **$GAUSSHOME/CA**.

   Copy the certificate generation script and related files.

   ```bash
   cp path_to_predictor/install/ssl.sh $GAUSSHOME/
   cp path_to_predictor/install/ca_ext.txt $GAUSSHOME/
   ```

   Copy the configuration file **openssl.cnf** to **$GAUSSHOME**.

   ```bash
   cp $GAUSSHOME/share/om/openssl.cnf  $GAUSSHOME/
   ```

   Modify the configuration parameters in **openssl.conf**.

   ```bash
   dir  = $GAUSSHOME/CA/demoCA
   default_md      = sha256
   ```

   The certificate generation environment is ready.

2. Generate a certificate and private key.

   ```bash
   cd $GAUSSHOME
   sh ssl.sh
   ```

   Set the password as prompted, for example, **Test@123**.

   The password must contain at least eight characters of at least three different types.

   ```bash
   Please enter your password:
   ```

   Set the options as prompted.

   ```bash
   Certificate Details:
           Serial Number: 1 (0x1)
           Validity
               Not Before: May 15 08:32:44 2020 GMT
               Not After : May 15 08:32:44 2021 GMT
           Subject:
               countryName               = CN
               stateOrProvinceName       = SZ
               organizationName          = HW
               organizationalUnitName    = GS
               commonName                = CA
           X509v3 extensions:
               X509v3 Basic Constraints:
                   CA:TRUE
   Certificate is to be certified until May 15 08:32:44 2021 GMT (365 days)
   Sign the certificate? [y/n]:y
   1 out of 1 certificate requests certified, commit? [y/n]y
   ```

   Enter the IP address for starting the AIEngine, for example, **127.0.0.1**.

   ```bash
   Please enter your aiEngine IP: 127.0.0.1
   ```

   Set the options as prompted.

   ```bash
   Certificate Details:
           Serial Number: 2 (0x2)
           Validity
               Not Before: May 15 08:38:07 2020 GMT
               Not After : May 13 08:38:07 2030 GMT
           Subject:
               countryName               = CN
               stateOrProvinceName       = SZ
               organizationName          = HW
               organizationalUnitName    = GS
               commonName                = 127.0.0.1
           X509v3 extensions:
               X509v3 Basic Constraints:
                   CA:FALSE
   Certificate is to be certified until May 13 08:38:07 2030 GMT (3650 days)
   Sign the certificate? [y/n]:y
   1 out of 1 certificate requests certified, commit? [y/n]y
   ```

   Enter the IP address for starting MogDB, for example, **127.0.0.1**.

   ```bash
   Please enter your mogdb IP: 127.0.0.1
   ```

   Set the options as prompted.

   ```bash
   Certificate Details:
           Serial Number: 3 (0x3)
           Validity
               Not Before: May 15 08:41:46 2020 GMT
               Not After : May 13 08:41:46 2030 GMT
           Subject:
               countryName               = CN
               stateOrProvinceName       = SZ
               organizationName          = HW
               organizationalUnitName    = GS
               commonName                = 127.0.0.1
           X509v3 extensions:
               X509v3 Basic Constraints:
                   CA:FALSE
   Certificate is to be certified until May 13 08:41:46 2030 GMT (3650 days)
   Sign the certificate? [y/n]:y
   1 out of 1 certificate requests certified, commit? [y/n]y
   ```

   The related certificate and key have been generated. The content in **$GAUSSHOME/CA** is as follows:

   ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/intelligence-explain-1.png)

## Setting Up the Environment

1. Copy the tool code folder to the target environment.

   – Assume that the installation directory is **$INSTALL_FOLDER**.

   – Assume that the destination directory is **/home/ai_user**.

   ```bash
   scp -r $INSTALL_FOLDER/bin/dbmind/predictor  ai_user@127.0.0.1:path_to_Predictor
   ```

2. Copy the CA certificate folder to a directory in the AIEngine environment.

   ```bash
   cp -r $GAUSSHOME/CA  ai_user@127.0.0.1:path_to_CA
   ```

3. Install the **predictor/install/requirements(-gpu).txt** tool.

   ```bash
   With GPU: pip install -r requirements-gpu.txt
   Without GPU: pip install -r requirements.txt
   ```

## Starting the AIEngine

1. Switch to the AIEngine environment (that is, copy the target environment **ai_user** of the predictor).

   Set parameters in **predictor/python/settings.py**.

   ```bash
   DEFAULT_FLASK_SERVER_HOST = '127.0.0.1' (running IP address of the AIEngine)
   DEFAULT_FLASK_SERVER_PORT = '5000' (running port number of the AIEngine)
   PATH_SSL = "path_to_CA" (CA folder path)
   ```

2. Run the startup script of the AIEngine.

   ```bash
   python path_to_Predictor/python/run.py
   ```

   In this case, the AIEngine keeps enabled on the corresponding port and waits for the request of the time prediction function from the kernel.

   For details about how to initiate a time prediction command from the kernel, see[User Guide](intelligence-explain-user-guide.md)。
