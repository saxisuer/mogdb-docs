---
title: Best Practices
summary: Best Practices
author: Guo Huan
date: 2022-05-06
---

# Best Practices

For details about the parameters, see [GS_OPT_MODEL](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)。

**Table 1**

| Model Parameter  | Recommended Value                                            |
| :--------------- | :----------------------------------------------------------- |
| template_name    | ‘rlstm’                                                      |
| model_name       | The value can be customized, for example, **open_ai**. The value must meet the unique constraint. |
| datname          | Name of the database to be served, for example, **postgres**. |
| ip               | IP address of the AI Engine, for example, **127.0.0.1**.     |
| port             | AI Engine listening port number, for example, **5000**.      |
| max_epoch        | Iteration times. A large value is recommended to ensure the convergence effect, for example, **2000**. |
| learning_rate    | (0, 1] is a floating-point number. A large learning rate is recommended to accelerate convergence. |
| dim_red          | Number of feature values to be reduced. **-1**: Do not use PCA for dimension reduction. All features are supported. Floating point number in the range of (0, 1]: A smaller value indicates a smaller training dimension and a faster convergence speed, but affects the training accuracy. |
| hidden_units     | If the feature value dimension is high, you are advised to increase the value of this parameter to increase the model complexity. For example, set this parameter to **64**, **128**, and so on. |
| batch_size       | You are advised to increase the value of this parameter based on the amount of encoded data to accelerate model convergence. For example, set this parameter to **256**, **512**, and so on. |
| Other parameters | See [GS_OPT_MODEL](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md) |

Recommended parameter settings:

```
INSERT INTO gs_opt_model values('rlstm', 'open_ai', 'postgres', '127.0.0.1', 5000, 2000，1, -1, 64, 512, 0 , false, false, '{S, T}', '{0,0}', '{0,0}', 'Text');
```