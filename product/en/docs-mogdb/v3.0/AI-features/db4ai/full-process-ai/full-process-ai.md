---
title: Full-process AI
summary: Full-process AI
author: Guo Huan
date: 2022-05-06
---

# Full-process AI

A traditional AI task usually has multiple processes. For example, a data collection process includes data collection, data cleaning, and data storage, an algorithm training process includes data preprocessing, training, and model storage and management, and a model training process includes hyperparameter tuning. The entire lifecycle of such machine learning models can be integrated into the database. The model training, management, and optimization processes are performed closest to the data storage. The out-of-the-box AI lifecycle management function of SQL statements is provided on the database side, which is called full-process AI.

MogDB implements some full-process AI functions, which will be described in detail in this section.
