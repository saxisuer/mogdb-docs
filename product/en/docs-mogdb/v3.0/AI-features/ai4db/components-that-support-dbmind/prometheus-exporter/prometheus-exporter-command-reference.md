---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2022-05-06
---

# Command Reference

For details about how to use reprocessing-exporter, see the following help information:

```
gs_dbmind component reprocessing_exporter --help
usage:  [-h] [--disable-https] [--ssl-keyfile SSL_KEYFILE]
        [--ssl-certfile SSL_CERTFILE]
        [--web.listen-address WEB.LISTEN_ADDRESS]
        [--web.listen-port WEB.LISTEN_PORT]
        [--collector.config COLLECTOR.CONFIG] [--log.filepath LOG.FILEPATH]
        [--log.level {debug,info,warn,error,fatal}] [--version]
        prometheus_host prometheus_port

Reprocessing Exporter: A re-processing module for metrics stored in the
Prometheus server.

positional arguments:
  prometheus_host       from which host to pull data
  prometheus_port       the port to connect to the Prometheus host

optional arguments:
  -h, --help            show this help message and exit
  --disable-https       disable Https schema
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --collector.config COLLECTOR.CONFIG
                        according to the content of the yaml file for metric
                        collection
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above.
                        Valid levels: [debug, info, warn, error, fatal]
  --version             show program's version number and exit
```

**Table 1** reprocessing-exporter parameters

| Parameter           | Description                                                  | Value Range                     |
| :------------------ | :----------------------------------------------------------- | :------------------------------ |
| prometheus_host     | Prometheus-server IP address                                 | -                               |
| prometheus_port     | Listening port of Prometheus-server                          | 1024-65535                      |
| -h, –help           | Help option                                                  | -                               |
| –disable-https      | Disables HTTPS.                                              | -                               |
| –ssl-keyfile        | Path of the HTTPS certificate private key file               | -                               |
| –ssl-certfile       | Path of the HTTPS certificate file                           | -                               |
| –web.listen-address | IP address bound to the exporter service                     | -                               |
| –web.listen-port    | Listening port of the exporter service                       | 1024-65535                      |
| –collector.config   | Path of the configuration file that explicitly specifies the metrics to be collected | -                               |
| –log.filepath       | Path for storing log files. By default, log files are stored in the current directory. | -                               |
| –log.level          | Printing level of the log file. The default level is **INFO**. | debug, info, warn, error, fatal |
| –version            | Displays version information.                                | -                               |

For details about how to use openGauss-exporter, see the following help information:

```
gs_dbmind component opengauss_exporter --help
usage:  [-h] --url URL [--config CONFIG] [--constant-labels CONSTANT_LABELS]
        [--web.listen-address WEB.LISTEN_ADDRESS]
        [--web.listen-port WEB.LISTEN_PORT]
        [--web.telemetry-path WEB.TELEMETRY_PATH] [--disable-cache]
        [--disable-settings-metrics] [--disable-statement-history-metrics]
        [--disable-https] [--ssl-keyfile SSL_KEYFILE]
        [--ssl-certfile SSL_CERTFILE] [--parallel PARALLEL]
        [--log.filepath LOG.FILEPATH]
        [--log.level {debug,info,warn,error,fatal}] [--version]

openGauss Exporter (DBMind): Monitoring for MogDB.

optional arguments:
  -h, --help            show this help message and exit
  --url URL             MogDB database target url.
  --config CONFIG       path to config dir or file.
  --constant-labels CONSTANT_LABELS
                        a list of label=value separated by comma(,).
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --web.telemetry-path WEB.TELEMETRY_PATH
                        path under which to expose metrics.
  --disable-cache       force not using cache.
  --disable-settings-metrics
                        not collect pg_settings.yml metrics.
  --disable-statement-history-metrics
                        not collect statement-history metrics (including slow
                        queries).
  --disable-https       disable Https schema
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --parallel PARALLEL   not collect pg_settings.yml metrics.
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above.
                        Valid levels: [debug, info, warn, error, fatal]
  --version             show program's version number and exit
```

**Table 2** openGauss-exporter parameters

| Parameter                          | Description                                                  | Value Range                     |
| :--------------------------------- | :----------------------------------------------------------- | :------------------------------ |
| –url                               | URL of the MogDB server, for example, postgres://user:pwd@host:port/dbname. | -                               |
| –constant-labels                   | Constant labels, which are forcibly added to the collected metrics. | 1024-65535                      |
| -h, –help                          | Help option                                                  | -                               |
| –disable-https                     | Disables HTTPS.                                              | -                               |
| –ssl-keyfile                       | Path of the HTTPS certificate private key file               | -                               |
| –ssl-certfile                      | Path of the HTTPS certificate file                           | -                               |
| –web.listen-address                | IP address bound to the exporter service                     | -                               |
| –web.listen-port                   | Listening port of the exporter service                       | 1024-65535                      |
| –web.telemetry-path                | URI of the exporter for collecting metrics. The default value is **/metrics**. | -                               |
| –config                            | Path of the configuration file that explicitly specifies the metrics to be collected | -                               |
| –log.filepath                      | Path for storing log files. By default, log files are stored in the current directory. | -                               |
| –log.level                         | Printing level of the log file. The default level is **INFO**. | debug, info, warn, error, fatal |
| –version                           | Displays version information.                                | -                               |
| –disable-cache                     | Disables the cache.                                          | -                               |
| –disable-settings-metrics          | Disables the collection of metrics in the **pg_settings** table. | -                               |
| –disable-statement-history-metrics | Disables the collection of slow SQL statements in the **statement_history** table. | -                               |
| –parallel                          | Size of the database connection pool connected to MogDB.     | Positive integer                |