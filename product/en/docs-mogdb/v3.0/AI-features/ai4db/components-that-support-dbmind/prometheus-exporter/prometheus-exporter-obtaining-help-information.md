---
title: Obtaining Help Information
summary: Obtaining Help Information
author: Guo Huan
date: 2022-05-06
---

# Obtaining Help Information

You can run the **–help** command to obtain the help information. For example:

```
gs_dbmind component opengauss_exporter --help
gs_dbmind component reprocessing_exporter --help
```
