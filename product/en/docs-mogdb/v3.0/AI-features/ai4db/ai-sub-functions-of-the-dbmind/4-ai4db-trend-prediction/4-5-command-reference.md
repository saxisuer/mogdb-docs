---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2022-05-06
---

# Command Reference

**Table 1** gs_dbmind component forecast parameters

| Parameter       | Description                                                  | Value Range                                                  |
| :-------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -h, –help       | Help information                                             | -                                                            |
| action          | Action parameter                                             | - **show**: displays results.<br/>- **clean**: clears results. |
| -c, –conf       | Configuration directory                                      | -                                                            |
| –metric-name    | Specifies the metric name to be displayed, which is used for filtering. | -                                                            |
| –host           | Specifies the service IP address and port number, which is used for filtering. | -                                                            |
| –start-time     | Timestamp of the start time, in milliseconds.                | Non-negative real number                                     |
| –end-time       | Timestamp of the end time, in milliseconds.                  | Non-negative real number                                     |
| –retention-days | Number of days retaining results                             | Non-negative real number                                     |
