---
title: Overview
summary: Overview
author: Guo Huan
date: 2022-05-06
---

# Overview

Slow SQL statements have always been a pain point in data O&M. How to effectively diagnose the root causes of slow SQL statements is a big challenge. Based on the characteristics of MogDB and the slow SQL diagnosis experience of DBAs on the live network, this tool supports more than 15 root causes of slow SQL statements, outputs multiple root causes based on the possibility, and provides specific solutions.
