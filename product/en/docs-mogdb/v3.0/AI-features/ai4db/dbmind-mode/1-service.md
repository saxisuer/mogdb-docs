---
title: service
summary: service
author: Guo Huan
date: 2022-05-06
---

# service

This subcommand can be used to initialize the configuration directory and start and stop background tasks.

## Initializing the Configuration Directory

You can run the **gs_dbmind service setup** subcommand to initialize the configuration directory. This directory may contain the configuration files and logs of the DBMind. Some files in the directory are described as follows:

- **dbmind.conf**: DBMind parameter configuration file. You can modify it using the **gs_dbmind set** command or a text editor.
- **dynamic_config.db**: DBMind service metadata stored on the local node, including algorithm hyperparameters and monitoring thresholds. This file contains DBMind service metadata and cannot be configured by users.
- **metric_map.conf**: monitoring metric mapping table, which can be used to adapt to different collection platforms. For example, in DBMind, the monitored system CPU usage is named **os_cpu_usage**, but a user-defined collection tool may name the CPU usage **my_cpu_usage_rate**. In this case, if you want DBMind to represent the CPU usage **metric my_cpu_usage_rate**, you need to modify this configuration item. That is, add the **os_cpu_usage = my_cpu_usage_rate** configuration item for mapping. For common users, you are advised to use the collection component and solution of the DBMind. In this case, you do not need to modify the configuration file.
- **logs**: This directory stores logs generated by the DBMind service.

You can initialize the configuration directory in interactive or non-interactive mode. For example, if the name of the configuration directory to be initialized is **confpath**, perform the following operations:

**Interactive mode**

```
gs_dbmind service setup -c confpath --interactive
```

After running the preceding command, you can configure the configuration items in interactive mode through the CLI client.

**Non-interactive mode**

In non-interactive mode, the initialization consists of three steps: starting configuration, modifying configuration items, and initializing configuration. In the second step, you need to manually edit the configuration file by using the text editor. The procedure is as follows:

1. Run the following command to start the configuration:

   ```
   gs_dbmind service setup -c confpath
   ```

2. After the preceding command is executed, the **dbmind.conf** configuration file is generated in the **confpath** directory. You need to use the text editor to manually modify the file. The following parameters need to be modified: **[TSDB]**, **[METADATABASE]**, and **[SELF-MONITORING]**.

   ```
   [TSDB] # Information about the time series database, that is, information about the metric data source.
   name = prometheus # Time series database type. Currently, only prometheus is supported.
   host = # IP address of the time series database.
   port = # Port number of the time series database.
        
   [METADATABASE] # Metadata database information and prediction result storage mode.
   dbtype = # Metadata database type. The options are sqlite, MogDB, and postgresql. If the configuration item name is MogDB, pay attention to the Python driver. For details, see the following information about the Python driver.
   host = #IP address of the metadata database.
   port = # Port number of the metadata database.
   username = # Metadata database user information.
   password = # Metadata database password, which will be encrypted for storage.
   database = # Metadata database name. If the database type is SQLite, you only need to set this parameter. The DBMind generates the SQLite database file with the corresponding file name in the configuration directory according to the value of the database configuration item.
        
   [SELF-MONITORING] # Self-monitoring parameter.
   detection_interval = 600 # Interval for performing health check on MogDB (used only for slow SQL root cause diagnosis), in seconds.
   last_detection_time = 600 # Duration of data monitoring for health check (used only for slow SQL root cause diagnosis), in seconds.
   forecasting_future_time = 3600  # Duration of time series prediction (for time series prediction only) and scheduling period of the prediction function, in seconds.
   # KPIs to be predicted (for time series prediction only)
   golden_kpi = os_cpu_usage, os_mem_usage, gaussdb_qps_by_instance
        
   [LOG] # Log information
   maxbytes = 10485760 # Maximum size of a log file. The default value is 10 MB. If the value of maxbytes is 0, the file size increases infinitely. It is recommended that you retain the default value.
   backupcount = 1 # Maximum number of log files.
   level = INFO # Log level. It can be set to DEBUG, INFO, WARNING, or ERROR.
   ```

3. After manually modifying the preceding parameters, initialize the configuration items. In this phase, DBMind preliminarily checks the correctness of the configuration items, encrypts the plaintext passwords in the configuration items, and initializes the metadata database for storing the result data.

   ```
   gs_dbmind service setup --initialize -c confpath
   ```

4. Start the DBMind background service based on the configuration directory.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** 
>
> 1. The comments in the configuration file are used to prompt users in interactive mode. Do not manually modify or delete the comments. 
> 2. Make sure that the value of the configuration item and the comment are separated by a space. Otherwise, the system regards the comment as the value of the configuration item. 
> 3. If special characters in a configuration item need to be escaped, use the percent sign ( %) to escape the special characters. For example, if the password is **password %**, use the percent sign ( %) to escape the special characters, that is, **password %%**.

## Starting a Service

After the configuration directory is initialized, you can start the DBMind background service based on the configuration directory. For example, if the configuration directory is **confpath**, run the following command:

```
gs_dbmind service start -c confpath
```

After the preceding command is executed, the system displays a message indicating that the service has been started. If no additional parameter is specified, this command starts all background tasks by default. If you want to start only one background task, add the **–only-run** option. For example, if you only want to start the slow SQL root cause analysis service, run the following command:

```
gs_dbmind service start -c confpath --only-run slow_query_diagnosis
```

## Stopping a Service

Similar to starting a service, stopping a service has a simpler command line structure. You only need to specify the address of the configuration directory. For example, if the configuration directory is **confpath**, run the following command:

```
gs_dbmind service stop -c confpath
```

The DBMind service automatically exits after the running task is complete in the background.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** 
>
> - The metabase user in **[METADATABASE]** must have the permission to create tables and insert and update data in the database. Otherwise, an exception will occur during tool execution. 
> - Currently, multiple services cannot be started separately using the same configuration file. 
> - The tool provides the **requirement.txt** file. You can use this file to install required third-party dependencies.

## Command Reference

You can use the **–help** option to obtain the help information about this mode. For example:

```
gs_dbmind service --help
```

```
usage: service [-h] -c DIRECTORY [--only-run {slow_query_diagnosis,forecast}] [--interactive | --initialize] {setup,start,stop}

positional arguments:
  {setup,start,stop}
                        perform an action for service

optional arguments:
  -h, --help            show this help message and exit
  -c DIRECTORY, --conf DIRECTORY
                        set the directory of configuration files
  --only-run {slow_query_diagnosis,forecast}
                        explicitly set a certain task running in the backend
  --interactive         configure and initialize with interactive mode
  --initialize          initialize and check configurations after configuring.
```

**Table 1** Parameters of the gs_dbmind service subcommand

| Parameter     | Description                                | Value Range                                                  |
| :------------ | :----------------------------------------- | :----------------------------------------------------------- |
| action        | Action parameter                           | - setup: initializes configuration directory.<br/>- start: starts a service.<br/>- stop: stops a service. |
| -c，--conf    | Configuration file directory               | -                                                            |
| --initialize  | Initializes configuration parameters.      | -                                                            |
| --interactive | Configures parameters in interactive mode. | -                                                            |
| --only-run    | Selects the module to be run only.         | - forecast: prediction module.<br/>- slow_query_diagnosis: root cause analysis module for slow SQL statements. |
| -h, --help    | Help information                           | -                                                            |
