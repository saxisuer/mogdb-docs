---
title: set
summary: set
author: Guo Huan
date: 2022-05-06
---

# set

This command is used to change the parameter values in the **dbmind.conf** configuration file. You can also manually modify the **dbmind.conf** configuration file. The two methods have no difference. For example, to change the value of **host** in the **TSDB** configuration item of the **dbmind.conf** file in the **confpath** directory to **127.0.0.1**, run the following command:

```
gs_dbmind set TSDB host 127.0.0.1 -c confpath
```

You can choose either of the methods to modify common parameters. However, the plaintext password cannot be encrypted by manually modifying the configuration file. If you want to change the password, run the following command:

```
gs_dbmind set METADATABASE password xxxxxx -c confpath
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This command is case sensitive to character strings. If you enter an incorrect character string, an error may occur during the command execution.

## Command Reference

You can use the **–help** option to obtain the help information about this mode. For example:

```
gs_dbmind set --help
```

```
usage:  set [-h] -c DIRECTORY section option target

positional arguments:
  section               which section (case sensitive) to set
  option                which option to set
  target                the parameter target to set

optional arguments:
  -h, --help            show this help message and exit
  -c DIRECTORY, --conf DIRECTORY
                        set the directory of configuration files
```

**Table 1** Parameters of the set subcommand: python dbmind/ set xxx

| Parameter | Description                               | Value Range |
| :-------- | :---------------------------------------- | :---------- |
| -h, –help | Help information                          | -           |
| -c, –conf | Configuration file directory **confpath** | -           |
| section   | Setting area                              | -           |
| option    | Configuration item                        | -           |
| target    | Set value                                 | -           |
