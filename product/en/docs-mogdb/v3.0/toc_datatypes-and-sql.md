<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# MogDB Documentation 3.0

## Data Types and SQL

+ Supported Data Types
  + [Numeric Types](/reference-guide/supported-data-types/1-numeric-data-types.md)
  + [Monetary Types](/reference-guide/supported-data-types/2-monetary.md)
  + [Boolean Types](/reference-guide/supported-data-types/3-boolean-data-types.md)
  + [Enumerated Types](/reference-guide/supported-data-types/4.1-enumerated-types.md)
  + [Character Types](/reference-guide/supported-data-types/4-character-data-types.md)
  + [Binary Types](/reference-guide/supported-data-types/5-binary-data-types.md)
  + [Date/Time Types](/reference-guide/supported-data-types/6-date-time-types.md)
  + [Geometric](/reference-guide/supported-data-types/7-geometric.md)
  + [Network Address Types](/reference-guide/supported-data-types/8-network-address.md)
  + [Bit String Types](/reference-guide/supported-data-types/9-bit-string-types.md)
  + [Text Search Types](/reference-guide/supported-data-types/10-text-search-types.md)
  + [UUID](/reference-guide/supported-data-types/11-uuid-type.md)
  + [JSON/JSONB Types](/reference-guide/supported-data-types/12-json-types.md)
  + [HLL](/reference-guide/supported-data-types/13-HLL.md)
  + [Array Types](/reference-guide/supported-data-types/13.0-array-types.md)
  + [Range](/reference-guide/supported-data-types/13.1-range.md)
  + [OID Types](/reference-guide/supported-data-types/14-object-identifier-types.md)
  + [Pseudo-Types](/reference-guide/supported-data-types/15-pseudo-types.md)
  + [Data Types Supported by Column-store Tables](/reference-guide/supported-data-types/16-data-types-supported-by-column-store-tables.md)
  + [XML Types](/reference-guide/supported-data-types/17-xml-type.md)
  + [Data Type Used by the Ledger Database](/reference-guide/supported-data-types/18-data-type-used-by-the-ledger-database.md)
+ SQL Syntax
  + [ABORT](/reference-guide/sql-syntax/ABORT.md)
  + [ALTER AGGREGATE](/reference-guide/sql-syntax/ALTER-AGGREGATE.md)
  + [ALTER AUDIT POLICY](/reference-guide/sql-syntax/ALTER-AUDIT-POLICY.md)
  + [ALTER DATABASE](/reference-guide/sql-syntax/ALTER-DATABASE.md)
  + [ALTER DATA SOURCE](/reference-guide/sql-syntax/ALTER-DATA-SOURCE.md)
  + [ALTER DEFAULT PRIVILEGES](/reference-guide/sql-syntax/ALTER-DEFAULT-PRIVILEGES.md)
  + [ALTER DIRECTORY](/reference-guide/sql-syntax/ALTER-DIRECTORY.md)
  + [ALTER EXTENSION](/reference-guide/sql-syntax/ALTER-EXTENSION.md)
  + [ALTER FOREIGN TABLE](/reference-guide/sql-syntax/ALTER-FOREIGN-TABLE.md)
  + [ALTER FUNCTION](/reference-guide/sql-syntax/ALTER-FUNCTION.md)
  + [ALTER GLOBAL CONFIGURATION](/reference-guide/sql-syntax/ALTER-GLOBAL-CONFIGURATION.md)
  + [ALTER GROUP](/reference-guide/sql-syntax/ALTER-GROUP.md)
  + [ALTER INDEX](/reference-guide/sql-syntax/ALTER-INDEX.md)
  + [ALTER LANGUAGE](/reference-guide/sql-syntax/ALTER-LANGUAGE.md)
  + [ALTER LARGE OBJECT](/reference-guide/sql-syntax/ALTER-LARGE-OBJECT.md)
  + [ALTER MASKING POLICY](/reference-guide/sql-syntax/ALTER-MASKING-POLICY.md)
  + [ALTER MATERIALIZED VIEW](/reference-guide/sql-syntax/ALTER-MATERIALIZED-VIEW.md)
  + [ALTER PACKAGE](/reference-guide/sql-syntax/ALTER-PACKAGE.md)
  + [ALTER PROCEDURE](/reference-guide/sql-syntax/ALTER-PROCEDURE.md)
  + [ALTER PUBLICATION](/reference-guide/sql-syntax/ALTER-PUBLICATION.md)
  + [ALTER RESOURCE LABEL](/reference-guide/sql-syntax/ALTER-RESOURCE-LABEL.md)
  + [ALTER RESOURCE POOL](/reference-guide/sql-syntax/ALTER-RESOURCE-POOL.md)
  + [ALTER ROLE](/reference-guide/sql-syntax/ALTER-ROLE.md)
  + [ALTER ROW LEVEL SECURITY POLICY](/reference-guide/sql-syntax/ALTER-ROW-LEVEL-SECURITY-POLICY.md)
  + [ALTER RULE](/reference-guide/sql-syntax/ALTER-RULE.md)
  + [ALTER SCHEMA](/reference-guide/sql-syntax/ALTER-SCHEMA.md)
  + [ALTER SEQUENCE](/reference-guide/sql-syntax/ALTER-SEQUENCE.md)
  + [ALTER SERVER](/reference-guide/sql-syntax/ALTER-SERVER.md)
  + [ALTER SESSION](/reference-guide/sql-syntax/ALTER-SESSION.md)
  + [ALTER SUBSCRIPTION](/reference-guide/sql-syntax/ALTER-SUBSCRIPTION.md)
  + [ALTER SYNONYM](/reference-guide/sql-syntax/ALTER-SYNONYM.md)
  + [ALTER SYSTEM KILL SESSION](/reference-guide/sql-syntax/ALTER-SYSTEM-KILL-SESSION.md)
  + [ALTER SYSTEM SET](/reference-guide/sql-syntax/ALTER-SYSTEM-SET.md)
  + [ALTER TABLE](/reference-guide/sql-syntax/ALTER-TABLE.md)
  + [ALTER TABLE PARTITION](/reference-guide/sql-syntax/ALTER-TABLE-PARTITION.md)
  + [ALTER TABLE SUBPARTITION](/reference-guide/sql-syntax/ALTER-TABLE-SUBPARTITION.md)
  + [ALTER TABLESPACE](/reference-guide/sql-syntax/ALTER-TABLESPACE.md)
  + [ALTER TEXT SEARCH CONFIGURATION](/reference-guide/sql-syntax/ALTER-TEXT-SEARCH-CONFIGURATION.md)
  + [ALTER TEXT SEARCH DICTIONARY](/reference-guide/sql-syntax/ALTER-TEXT-SEARCH-DICTIONARY.md)
  + [ALTER TRIGGER](/reference-guide/sql-syntax/ALTER-TRIGGER.md)
  + [ALTER TYPE](/reference-guide/sql-syntax/ALTER-TYPE.md)
  + [ALTER USER](/reference-guide/sql-syntax/ALTER-USER.md)
  + [ALTER USER MAPPING](/reference-guide/sql-syntax/ALTER-USER-MAPPING.md)
  + [ALTER VIEW](/reference-guide/sql-syntax/ALTER-VIEW.md)
  + [ANALYZE | ANALYSE](/reference-guide/sql-syntax/ANALYZE-ANALYSE.md)
  + [BEGIN](/reference-guide/sql-syntax/BEGIN.md)
  + [CALL](/reference-guide/sql-syntax/CALL.md)
  + [CHECKPOINT](/reference-guide/sql-syntax/CHECKPOINT.md)
  + [CLEAN CONNECTION](/reference-guide/sql-syntax/CLEAN-CONNECTION.md)
  + [CLOSE](/reference-guide/sql-syntax/CLOSE.md)
  + [CLUSTER](/reference-guide/sql-syntax/CLUSTER.md)
  + [COMMENT](/reference-guide/sql-syntax/COMMENT.md)
  + [COMMIT | END](/reference-guide/sql-syntax/COMMIT-END.md)
  + [COMMIT PREPARED](/reference-guide/sql-syntax/COMMIT-PREPARED.md)
  + [CONNECT BY](/reference-guide/sql-syntax/CONNECT-BY.md)
  + [COPY](/reference-guide/sql-syntax/COPY.md)
  + [CREATE AGGREGATE](/reference-guide/sql-syntax/CREATE-AGGREGATE.md)
  + [CREATE AUDIT POLICY](/reference-guide/sql-syntax/CREATE-AUDIT-POLICY.md)
  + [CREATE CAST](/reference-guide/sql-syntax/CREATE-CAST.md)
  + [CREATE CLIENT MASTER KEY](/reference-guide/sql-syntax/CREATE-CLIENT-MASTER-KEY.md)
  + [CREATE COLUMN ENCRYPTION KEY](/reference-guide/sql-syntax/CREATE-COLUMN-ENCRYPTION-KEY.md)
  + [CREATE DATABASE](/reference-guide/sql-syntax/CREATE-DATABASE.md)
  + [CREATE DATA SOURCE](/reference-guide/sql-syntax/CREATE-DATA-SOURCE.md)
  + [CREATE DIRECTORY](/reference-guide/sql-syntax/CREATE-DIRECTORY.md)
  + [CREATE EXTENSION](/reference-guide/sql-syntax/CREATE-EXTENSION.md)
  + [CREATE FOREIGN TABLE](/reference-guide/sql-syntax/CREATE-FOREIGN-TABLE.md)
  + [CREATE FUNCTION](/reference-guide/sql-syntax/CREATE-FUNCTION.md)
  + [CREATE GROUP](/reference-guide/sql-syntax/CREATE-GROUP.md)
  + [CREATE INCREMENTAL MATERIALIZED VIEW](/reference-guide/sql-syntax/CREATE-INCREMENTAL-MATERIALIZED-VIEW.md)
  + [CREATE INDEX](/reference-guide/sql-syntax/CREATE-INDEX.md)
  + [CREATE LANGUAGE](/reference-guide/sql-syntax/CREATE-LANGUAGE.md)
  + [CREATE MASKING POLICY](/reference-guide/sql-syntax/CREATE-MASKING-POLICY.md)
  + [CREATE MATERIALIZED VIEW](/reference-guide/sql-syntax/CREATE-MATERIALIZED-VIEW.md)
  + [CREATE MODEL](/reference-guide/sql-syntax/CREATE-MODEL.md)
  + [CREATE OPERATOR](/reference-guide/sql-syntax/CREATE-OPERATOR.md)
  + [CREATE PACKAGE](/reference-guide/sql-syntax/CREATE-PACKAGE.md)
  + [CREATE PROCEDURE](/reference-guide/sql-syntax/CREATE-PROCEDURE.md)
  + [CREATE PUBLICATION](/reference-guide/sql-syntax/CREATE-PUBLICATION.md)
  + [CREATE RESOURCE LABEL](/reference-guide/sql-syntax/CREATE-RESOURCE-LABEL.md)
  + [CREATE RESOURCE POOL](/reference-guide/sql-syntax/CREATE-RESOURCE-POOL.md)
  + [CREATE ROLE](/reference-guide/sql-syntax/CREATE-ROLE.md)
  + [CREATE ROW LEVEL SECURITY POLICY](/reference-guide/sql-syntax/CREATE-ROW-LEVEL-SECURITY-POLICY.md)
  + [CREATE RULE](/reference-guide/sql-syntax/CREATE-RULE.md)
  + [CREATE SCHEMA](/reference-guide/sql-syntax/CREATE-SCHEMA.md)
  + [CREATE SEQUENCE](/reference-guide/sql-syntax/CREATE-SEQUENCE.md)
  + [CREATE SERVER](/reference-guide/sql-syntax/CREATE-SERVER.md)
  + [CREATE SUBSCRIPTION](/reference-guide/sql-syntax/CREATE-SUBSCRIPTION.md)
  + [CREATE SYNONYM](/reference-guide/sql-syntax/CREATE-SYNONYM.md)
  + [CREATE TABLE](/reference-guide/sql-syntax/CREATE-TABLE.md)
  + [CREATE TABLE AS](/reference-guide/sql-syntax/CREATE-TABLE-AS.md)
  + [CREATE TABLE PARTITION](/reference-guide/sql-syntax/CREATE-TABLE-PARTITION.md)
  + [CREATE TABLE SUBPARTITION](/reference-guide/sql-syntax/CREATE-TABLE-SUBPARTITION.md)
  + [CREATE TABLESPACE](/reference-guide/sql-syntax/CREATE-TABLESPACE.md)
  + [CREATE TEXT SEARCH CONFIGURATION](/reference-guide/sql-syntax/CREATE-TEXT-SEARCH-CONFIGURATION.md)
  + [CREATE TEXT SEARCH DICTIONARY](/reference-guide/sql-syntax/CREATE-TEXT-SEARCH-DICTIONARY.md)
  + [CREATE TRIGGER](/reference-guide/sql-syntax/CREATE-TRIGGER.md)
  + [CREATE TYPE](/reference-guide/sql-syntax/CREATE-TYPE.md)
  + [CREATE USER](/reference-guide/sql-syntax/CREATE-USER.md)
  + [CREATE USER MAPPING](/reference-guide/sql-syntax/CREATE-USER-MAPPING.md)
  + [CREATE VIEW](/reference-guide/sql-syntax/CREATE-VIEW.md)
  + [CREATE WEAK PASSWORD DICTIONARY](/reference-guide/sql-syntax/CREATE-WEAK-PASSWORD-DICTIONARY.md)
  + [CURSOR](/reference-guide/sql-syntax/CURSOR.md)
  + [DEALLOCATE](/reference-guide/sql-syntax/DEALLOCATE.md)
  + [DECLARE](/reference-guide/sql-syntax/DECLARE.md)
  + [DELETE](/reference-guide/sql-syntax/DELETE.md)
  + [DO](/reference-guide/sql-syntax/DO.md)
  + [DROP AGGREGATE](/reference-guide/sql-syntax/DROP-AGGREGATE.md)
  + [DROP AUDIT POLICY](/reference-guide/sql-syntax/DROP-AUDIT-POLICY.md)
  + [DROP CAST](/reference-guide/sql-syntax/DROP-CAST.md)
  + [DROP CLIENT MASTER KEY](/reference-guide/sql-syntax/DROP-CLIENT-MASTER-KEY.md)
  + [DROP COLUMN ENCRYPTION KEY](/reference-guide/sql-syntax/DROP-COLUMN-ENCRYPTION-KEY.md)
  + [DROP DATABASE](/reference-guide/sql-syntax/DROP-DATABASE.md)
  + [DROP DATA SOURCE](/reference-guide/sql-syntax/DROP-DATA-SOURCE.md)
  + [DROP DIRECTORY](/reference-guide/sql-syntax/DROP-DIRECTORY.md)
  + [DROP EXTENSION](/reference-guide/sql-syntax/DROP-EXTENSION.md)
  + [DROP FOREIGN TABLE](/reference-guide/sql-syntax/DROP-FOREIGN-TABLE.md)
  + [DROP FUNCTION](/reference-guide/sql-syntax/DROP-FUNCTION.md)
  + [DROP GLOBAL CONFIGURATION](/reference-guide/sql-syntax/DROP-GLOBAL-CONFIGURATION.md)
  + [DROP GROUP](/reference-guide/sql-syntax/DROP-GROUP.md)
  + [DROP INDEX](/reference-guide/sql-syntax/DROP-INDEX.md)
  + [DROP LANGUAGE](/reference-guide/sql-syntax/DROP-LANGUAGE.md)
  + [DROP MASKING POLICY](/reference-guide/sql-syntax/DROP-MASKING-POLICY.md)
  + [DROP MATERIALIZED VIEW](/reference-guide/sql-syntax/DROP-MATERIALIZED-VIEW.md)
  + [DROP MODEL](/reference-guide/sql-syntax/DROP-MODEL.md)
  + [DROP OPERATOR](/reference-guide/sql-syntax/DROP-OPERATOR.md)
  + [DROP OWNED](/reference-guide/sql-syntax/DROP-OWNED.md)
  + [DROP PACKAGE](/reference-guide/sql-syntax/DROP-PACKAGE.md)
  + [DROP PROCEDURE](/reference-guide/sql-syntax/DROP-PROCEDURE.md)
  + [DROP PUBLICATION](/reference-guide/sql-syntax/DROP-PUBLICATION.md)
  + [DROP RESOURCE LABEL](/reference-guide/sql-syntax/DROP-RESOURCE-LABEL.md)
  + [DROP RESOURCE POOL](/reference-guide/sql-syntax/DROP-RESOURCE-POOL.md)
  + [DROP ROLE](/reference-guide/sql-syntax/DROP-ROLE.md)
  + [DROP ROW LEVEL SECURITY POLICY](/reference-guide/sql-syntax/DROP-ROW-LEVEL-SECURITY-POLICY.md)
  + [DROP RULE](/reference-guide/sql-syntax/DROP-RULE.md)
  + [DROP SCHEMA](/reference-guide/sql-syntax/DROP-SCHEMA.md)
  + [DROP SEQUENCE](/reference-guide/sql-syntax/DROP-SEQUENCE.md)
  + [DROP SERVER](/reference-guide/sql-syntax/DROP-SERVER.md)
  + [DROP SUBSCRIPTION](/reference-guide/sql-syntax/DROP-SUBSCRIPTION.md)
  + [DROP SYNONYM](/reference-guide/sql-syntax/DROP-SYNONYM.md)
  + [DROP TABLE](/reference-guide/sql-syntax/DROP-TABLE.md)
  + [DROP TABLESPACE](/reference-guide/sql-syntax/DROP-TABLESPACE.md)
  + [DROP TEXT SEARCH CONFIGURATION](/reference-guide/sql-syntax/DROP-TEXT-SEARCH-CONFIGURATION.md)
  + [DROP TEXT SEARCH DICTIONARY](/reference-guide/sql-syntax/DROP-TEXT-SEARCH-DICTIONARY.md)
  + [DROP TRIGGER](/reference-guide/sql-syntax/DROP-TRIGGER.md)
  + [DROP TYPE](/reference-guide/sql-syntax/DROP-TYPE.md)
  + [DROP USER](/reference-guide/sql-syntax/DROP-USER.md)
  + [DROP USER MAPPING](/reference-guide/sql-syntax/DROP-USER-MAPPING.md)
  + [DROP VIEW](/reference-guide/sql-syntax/DROP-VIEW.md)
  + [DROP WEAK PASSWORD DICTIONARY](/reference-guide/sql-syntax/DROP-WEAK-PASSWORD-DICTIONARY.md)
  + [EXECUTE](/reference-guide/sql-syntax/EXECUTE.md)
  + [EXECUTE DIRECT](/reference-guide/sql-syntax/EXECUTE-DIRECT.md)
  + [EXPLAIN](/reference-guide/sql-syntax/EXPLAIN.md)
  + [EXPLAIN PLAN](/reference-guide/sql-syntax/EXPLAIN-PLAN.md)
  + [FETCH](/reference-guide/sql-syntax/FETCH.md)
  + [GRANT](/reference-guide/sql-syntax/GRANT.md)
  + [INSERT](/reference-guide/sql-syntax/INSERT.md)
  + [LOCK](/reference-guide/sql-syntax/LOCK.md)
  + [MERGE INTO](/reference-guide/sql-syntax/MERGE-INTO.md)
  + [MOVE](/reference-guide/sql-syntax/MOVE.md)
  + [PREDICT BY](/reference-guide/sql-syntax/PREDICT-BY.md)
  + [PREPARE](/reference-guide/sql-syntax/PREPARE.md)
  + [PREPARE TRANSACTION](/reference-guide/sql-syntax/PREPARE-TRANSACTION.md)
  + [PURGE](/reference-guide/sql-syntax/PURGE.md)
  + [REASSIGN OWNED](/reference-guide/sql-syntax/REASSIGN-OWNED.md)
  + [REFRESH INCREMENTAL MATERIALIZED VIEW](/reference-guide/sql-syntax/REFRESH-INCREMENTAL-MATERIALIZED-VIEW.md)
  + [REFRESH MATERIALIZED VIEW](/reference-guide/sql-syntax/REFRESH-MATERIALIZED-VIEW.md)
  + [REINDEX](/reference-guide/sql-syntax/REINDEX.md)
  + [RELEASE SAVEPOINT](/reference-guide/sql-syntax/RELEASE-SAVEPOINT.md)
  + [RESET](/reference-guide/sql-syntax/RESET.md)
  + [REVOKE](/reference-guide/sql-syntax/REVOKE.md)
  + [ROLLBACK](/reference-guide/sql-syntax/ROLLBACK.md)
  + [ROLLBACK PREPARED](/reference-guide/sql-syntax/ROLLBACK-PREPARED.md)
  + [ROLLBACK TO SAVEPOINT](/reference-guide/sql-syntax/ROLLBACK-TO-SAVEPOINT.md)
  + [SAVEPOINT](/reference-guide/sql-syntax/SAVEPOINT.md)
  + [SELECT](/reference-guide/sql-syntax/SELECT.md)
  + [SELECT INTO](/reference-guide/sql-syntax/SELECT-INTO.md)
  + [SET](/reference-guide/sql-syntax/SET.md)
  + [SET CONSTRAINTS](/reference-guide/sql-syntax/SET-CONSTRAINTS.md)
  + [SET ROLE](/reference-guide/sql-syntax/SET-ROLE.md)
  + [SET SESSION AUTHORIZATION](/reference-guide/sql-syntax/SET-SESSION-AUTHORIZATION.md)
  + [SET TRANSACTION](/reference-guide/sql-syntax/SET-TRANSACTION.md)
  + [SHOW](/reference-guide/sql-syntax/SHOW.md)
  + [SHUTDOWN](/reference-guide/sql-syntax/SHUTDOWN.md)
  + [SNAPSHOT](/reference-guide/sql-syntax/SNAPSHOT.md)
  + [START TRANSACTION](/reference-guide/sql-syntax/START-TRANSACTION.md)
  + [TIMECAPSULE TABLE](/reference-guide/sql-syntax/TIMECAPSULE-TABLE.md)
  + [TRUNCATE](/reference-guide/sql-syntax/TRUNCATE.md)
  + [UPDATE](/reference-guide/sql-syntax/UPDATE.md)
  + [VACUUM](/reference-guide/sql-syntax/VACUUM.md)
  + [VALUES](/reference-guide/sql-syntax/VALUES.md)
+ SQL Reference
  + [MogDB SQL](/reference-guide/sql-reference/1-mogdb-sql.md)
  + [Keywords](/reference-guide/sql-reference/2-keywords.md)
  + [Constant and Macro](/reference-guide/sql-reference/3-constant-and-macro.md)
  + Expressions
    + [Simple Expressions](/reference-guide/sql-reference/4-expressions/1-simple-expressions.md)
    + [Condition Expressions](/reference-guide/sql-reference/4-expressions/2-condition-expressions.md)
    + [Subquery Expressions](/reference-guide/sql-reference/4-expressions/3-subquery-expressions.md)
    + [Array Expressions](/reference-guide/sql-reference/4-expressions/4-array-expressions.md)
    + [Row Expressions](/reference-guide/sql-reference/4-expressions/5-row-expressions.md)
  + Type Conversion
    + [Overview](/reference-guide/sql-reference/5-type-conversion/1-type-conversion-overview.md)
    + [Operators](/reference-guide/sql-reference/5-type-conversion/2-operators.md)
    + [Functions](/reference-guide/sql-reference/5-type-conversion/3-functions.md)
    + [Value Storage](/reference-guide/sql-reference/5-type-conversion/4-value-storage.md)
    + [UNION, CASE, and Related Constructs](/reference-guide/sql-reference/5-type-conversion/5-union-case-and-related-constructs.md)
  + Full Text Search
    + Introduction
      + [Full-Text Retrieval](/reference-guide/sql-reference/6-full-text-search/1-introduction/1-full-text-retrieval.md)
      + [What Is a Document?](/reference-guide/sql-reference/6-full-text-search/1-introduction/2-what-is-a-document.md)
      + [Basic Text Matching](/reference-guide/sql-reference/6-full-text-search/1-introduction/3-basic-text-matching.md)
      + [Configurations](/reference-guide/sql-reference/6-full-text-search/1-introduction/4-configurations.md)
    + Tables and Indexes
      + [Searching a Table](/reference-guide/sql-reference/6-full-text-search/2-tables-and-indexes/1-searching-a-table.md)
      + [Creating an Index](/reference-guide/sql-reference/6-full-text-search/2-tables-and-indexes/2-creating-an-index.md)
      + [Constraints on Index Use](/reference-guide/sql-reference/6-full-text-search/2-tables-and-indexes/3-constraints-on-index-use.md)
    + Controlling Text Search
      + [Parsing Documents](/reference-guide/sql-reference/6-full-text-search/3-controlling-text-search/1-parsing-documents.md)
      + [Parsing Queries](/reference-guide/sql-reference/6-full-text-search/3-controlling-text-search/2-parsing-queries.md)
      + [Ranking Search Results](/reference-guide/sql-reference/6-full-text-search/3-controlling-text-search/3-ranking-search-results.md)
      + [Highlighting Results](/reference-guide/sql-reference/6-full-text-search/3-controlling-text-search/4-highlighting-results.md)
    + Additional Features
      + [Manipulating tsvector](/reference-guide/sql-reference/6-full-text-search/4-additional-features/1-manipulating-tsvector.md)
      + [Manipulating Queries](/reference-guide/sql-reference/6-full-text-search/4-additional-features/2-manipulating-queries.md)
      + [Rewriting Queries](/reference-guide/sql-reference/6-full-text-search/4-additional-features/3-rewriting-queries.md)
      + [Gathering Document Statistics](/reference-guide/sql-reference/6-full-text-search/4-additional-features/4-gathering-document-statistics.md)
    + [Parser](/reference-guide/sql-reference/6-full-text-search/5-parser.md)
    + Dictionaries
      + [Overview](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/1-dictionaries-overview.md)
      + [Stop Words](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/2-stop-words.md)
      + [Simple Dictionary](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/3-simple-dictionary.md)
      + [Synonym Dictionary](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/4-synonym-dictionary.md)
      + [Thesaurus Dictionary](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/5-thesaurus-dictionary.md)
      + [Ispell Dictionary](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/6-ispell-dictionary.md)
      + [Snowball Dictionary](/reference-guide/sql-reference/6-full-text-search/6-dictionaries/7-snowball-dictionary.md)
    + [Configuration Examples](/reference-guide/sql-reference/6-full-text-search/7-configuration-examples.md)
    + Testing and Debugging Text Search
      + [Testing a Configuration](/reference-guide/sql-reference/6-full-text-search/8-testing-and-debugging-text-search/1-testing-a-configuration.md)
      + [Testing a Parser](/reference-guide/sql-reference/6-full-text-search/8-testing-and-debugging-text-search/2-testing-a-parser.md)
      + [Testing a Dictionary](/reference-guide/sql-reference/6-full-text-search/8-testing-and-debugging-text-search/3-testing-a-dictionary.md)
    + [Limitations](/reference-guide/sql-reference/6-full-text-search/9-limitations.md)
  + [System Operation](/reference-guide/sql-reference/7-system-operation.md)
  + [Controlling Transactions](/reference-guide/sql-reference/8-controlling-transactions.md)
  + [DDL Syntax Overview](/reference-guide/sql-reference/9-ddl-syntax-overview.md)
  + [DML Syntax Overview](/reference-guide/sql-reference/10-dml-syntax-overview.md)
  + [DCL Syntax Overview](/reference-guide/sql-reference/11-dcl-syntax-overview.md)
  + Appendix
    + GIN Indexes
      + [Introduction](/reference-guide/sql-reference/13-appendix/1-gin-indexes/1-introduction.md)
      + [Scalability](/reference-guide/sql-reference/13-appendix/1-gin-indexes/2-scalability.md)
      + [Implementation](/reference-guide/sql-reference/13-appendix/1-gin-indexes/3-implementation.md)
      + [GIN Tips and Tricks](/reference-guide/sql-reference/13-appendix/1-gin-indexes/4-gin-tips-and-tricks.md)
    + [Extended Functions](/reference-guide/sql-reference/13-appendix/2-extended-functions.md)
    + [Extended Syntax](/reference-guide/sql-reference/13-appendix/3-extended-syntax.md)