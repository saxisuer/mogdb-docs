---
title: Glossary
summary: Glossary
author: Zhang Cuiping
date: 2021-05-18
---

# Glossary

| Acronym | Definition/Description                                       |
| :------ | :----------------------------------------------------------- |
| 2PL     | 2-Phase Locking                                              |
| ACID    | Atomicity, Consistency, Isolation, Durability                |
| AP      | Analytical Processing                                        |
| ARM     | Advanced RISC Machine, a hardware architecture alternative to x86 |
| CC      | Concurrency Control                                          |
| CPU     | Central Processing Unit                                      |
| DB      | Database                                                     |
| DBA     | Database Administrator                                       |
| DBMS    | Database Management System                                   |
| DDL     | Data Definition Language. Database Schema management language |
| DML     | Data Modification Language                                   |
| ETL     | Extract, Transform, Load or Encounter Time Locking           |
| FDW     | Foreign Data Wrapper                                         |
| GC      | Garbage Collector                                            |
| HA      | High Availability                                            |
| HTAP    | Hybrid Transactional-Analytical Processing                   |
| IoT     | Internet of Things                                           |
| IM      | In-Memory                                                    |
| IMDB    | In-Memory Database                                           |
| IR      | Intermediate Representation of a source code, used in compilation and optimization |
| JIT     | Just In Time                                                 |
| JSON    | JavaScript Object Notation                                   |
| KV      | Key Value                                                    |
| LLVM    | Low-Level Virtual Machine, refers to a compilation code or queries to IR |
| M2M     | Machine-to-Machine                                           |
| ML      | Machine Learning                                             |
| MM      | Main Memory                                                  |
| MO      | Memory Optimized                                             |
| MOT     | Memory Optimized Tables storage engine (SE), pronounced as /em/ /oh/ /tee/ |
| MVCC    | Multi-Version Concurrency Control                            |
| NUMA    | Non-Uniform Memory Access                                    |
| OCC     | Optimistic Concurrency Control                               |
| OLTP    | Online Transaction Processing                                |
| PG      | PostgreSQL                                                   |
| RAW     | Reads-After-Writes                                           |
| RC      | Return Code                                                  |
| RTO     | Recovery Time Objective                                      |
| SE      | Storage Engine                                               |
| SQL     | Structured Query Language                                    |
| TCO     | Total Cost of Ownership                                      |
| TP      | Transactional Processing                                     |
| TPC-C   | An On-Line Transaction Processing Benchmark                  |
| Tpm-C   | Transactions-per-minute-C. A performance metric for TPC-C benchmark that counts new-order transactions. |
| TVM     | Tiny Virtual Machine                                         |
| TSO     | Time Sharing Option                                          |
| UDT     | User-Defined Type                                            |
| WAL     | Write Ahead Log                                              |
| XLOG    | A PostgreSQL implementation of transaction logging (WAL - described above) |
