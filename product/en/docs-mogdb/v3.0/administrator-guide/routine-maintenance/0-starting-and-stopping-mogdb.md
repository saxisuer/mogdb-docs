---
title: Starting and Stopping MogDB
summary: Starting and Stopping MogDB
author: Guo Huan
date: 2021-06-24
---

# Starting and Stopping MogDB

## Operating By OM

### Starting MogDB

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to start MogDB:

   ```bash
   gs_om -t start
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** An HA cluster must be started in HA mode. If the cluster is started in standalone mode, you need to restore the HA relationship by running the **gs_ctl build** command. For details about how to use the **gs_ctl** tool, see [gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md).

### Stopping MogDB

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to stop MogDB:

   ```bash
   gs_om -t stop
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** For details about how to start and stop nodes and availability zones (AZs), see [gs_om](../../reference-guide/tool-reference/server-tools/8-gs_om.md).

### Examples

Start MogDB:

```bash
gs_om -t start
Starting cluster.
=========================================
=========================================
Successfully started.
```

Stop MogDB:

```bash
gs_om -t stop
Stopping cluster.
=========================================
Successfully stopped cluster.
=========================================
End stop cluster.
```

### Troubleshooting

If starting or stopping MogDB fails, troubleshoot the problem based on log information. For details, see [Log Reference](11-log-reference.md).

If the startup fails due to timeout, you can run the following command to set the startup timeout interval, which is 300s by default:

```bash
gs_om -t start --time-out=300
```

<br/>

## Operating By PTK

### Start the Cluster

> The following uses the `cluster_slirist` cluster as an example.

After the database cluster is installed, PTK will start the database cluster by default.

If the `--skip-launch-db` parameter is specified during installation, the database is in the stopped status.

You can run the `cluster start` command to start the database cluster. You need to specify the cluster name in the command.

Example:

```shell
# ptk cluster -n cluster_slirist start
INFO[2022-08-02T11:40:48.728] Operating: Starting.
INFO[2022-08-02T11:40:48.728] =========================================
INFO[2022-08-02T11:40:48.784] starting host 192.168.122.101
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.102
INFO[2022-08-02T11:40:56.329] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:40:56.613] waiting for check cluster state...
INFO[2022-08-02T11:41:01.861] =========================================
INFO[2022-08-02T11:41:01.861] Successfully started.
INFO[2022-08-02T11:41:01.861] Operation succeeded: Start.
```

PTK starts all instances in the cluster by default. It also supports the starting of a single instance.

```shell
# ptk cluster -n cluster_slirist start -H 192.168.122.101
INFO[2022-08-02T11:50:04.442] Operating: Starting.
INFO[2022-08-02T11:50:04.442] =========================================
INFO[2022-08-02T11:50:06.692] starting host 192.168.122.101 successfully
```

For more parameters, see the help information.

```shell
# ptk cluster start -h
Start a database instance or cluster.

Usage:
  ptk cluster start [flags]

Flags:
  -h, --help                   help for start
  -H, --host string            Specifies the IP address of an instance. 
  -n, --name string            Specifies the cluster name. 
      --security-mode string   Specifies whether to start a database in safe mode. 
                               The value can be on and off. 
      --time-out duration      Specifies the start timeout duration. The default value is 10 minutes.
```

### Stop the Cluster

> The following uses the `cluster_slirist` cluster as an example.

You can run the `cluster stop` command to stop a database cluster. PTK will stop all instances in a cluster by default.

```shell
# ptk cluster -n cluster_slirist stop
INFO[2022-08-02T11:49:40.685] Operating: Stopping.
INFO[2022-08-02T11:49:40.685] =========================================
INFO[2022-08-02T11:49:40.891] stopping host 192.168.122.102
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.101
INFO[2022-08-02T11:49:43.004] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:49:43.004] =========================================
INFO[2022-08-02T11:49:43.004] Successfully stoped.
INFO[2022-08-02T11:49:43.004] Operation succeeded: Stop.
```

You can use the `-H` parameter to specify a instance to stop it.

```shell
# ptk cluster -n cluster_slirist stop -H 192.168.122.101
INFO[2022-08-02T11:56:32.880] Operating: Stopping.
INFO[2022-08-02T11:56:32.881] =========================================
INFO[2022-08-02T11:56:34.154] stopping host 192.168.122.101 successfully
```

For more parameters, see the help information.

```shell
# ptk cluster stop -h
Stop a database instance or cluster. 

Usage:
  ptk cluster stop [flags]

Flags:
  -h, --help                help for stop
  -H, --host string         Specifies the IP address of an instance.
  -n, --name string         Specifies the cluster name. 
      --time-out duration   Specifies the stop timeout duration. The default value is 10 minutes. 
```

### Restart the Cluster

> The following uses the `cluster_slirist` cluster as an example.

Restarting a cluster is actually to stop the database first and then start the database.

You can run the `cluster restart` command to restart the cluster.

```shell
# ptk cluster -n cluster_slirist restart
INFO[2022-08-02T11:59:31.037] Operating: Stopping.
INFO[2022-08-02T11:59:31.037] =========================================
INFO[2022-08-02T11:59:31.217] stopping host 192.168.122.102
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.101
INFO[2022-08-02T11:59:33.309] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:59:33.309] =========================================
INFO[2022-08-02T11:59:33.309] Successfully stoped.
INFO[2022-08-02T11:59:33.309] Operation succeeded: Stop.

INFO[2022-08-02T11:59:33.310] Operating: Starting.
INFO[2022-08-02T11:59:33.310] =========================================
INFO[2022-08-02T11:59:33.376] starting host 192.168.122.101
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.102
INFO[2022-08-02T11:59:36.787] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:59:36.995] waiting for check cluster state...
INFO[2022-08-02T11:59:42.247] =========================================
INFO[2022-08-02T11:59:42.247] Successfully started.
INFO[2022-08-02T11:59:42.247] Operation succeeded: Start.
```

For more parameters, see the help information.

```shell
# ptk cluster restart -h
Restart a database instance or cluster. 

Usage:
  ptk cluster restart [flags]

Flags:
  -h, --help                   help for restart
  -H, --host string            Specifies the IP address of an instance.
  -n, --name string            Specifies the cluster name. 
      --security-mode string   Specifies whether to start a database in safe mode. 
                               The value can be on and off. 
      --time-out duration      Specifies the start timeout duration. The default value is 10 minutes.
```
