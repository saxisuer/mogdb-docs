---
title: Source Code Parsing
summary: Source Code Parsing
author: Guo Huan
date: 2022-02-07
---

# Source Code Parsing

This series of articles parses the source code of the openGauss database module by module. It can be used as a reference tutorial for kernel developers to understand the openGauss database and develop database based on openGauss.

You can click the links below to view this series of articles.

## Introduction to openGauss

[Introduction to openGauss (Part 1)](https://www.modb.pro/db/47895)

[Introduction to openGauss (Part 2)](https://www.modb.pro/db/48291)

## Quick Start to openGauss Development

[Quick Start to openGauss Development (Part 1)](https://www.modb.pro/db/48657)

[Quick Start to openGauss Development (Part 2)](https://www.modb.pro/db/48870)

[Quick Start to openGauss Development (Part 3)](https://www.modb.pro/db/49118)

## Parsing of Public Component Source Code

[Parsing of public component source code (Part 1)](https://www.modb.pro/db/49242)

[Parsing of public component source code (Part 2)](https://www.modb.pro/db/49711)

## Parsing of Storage Engine Source Code

[Parsing of storage engine source code (Part 1)](https://www.modb.pro/db/51541)

[Parsing of storage engine source code (Part 2)](https://www.modb.pro/db/53926)

[Parsing of storage engine source code (Part 3)](https://www.modb.pro/db/57991)

[Parsing of storage engine source code (Part 4)](https://www.modb.pro/db/64255)

[Parsing of storage engine source code (Part 5)](https://www.modb.pro/db/77848)

## Parsing of Transaction Mechanism Source Code

[Parsing of transaction mechanism source code (Part 1)](https://www.modb.pro/db/78553)

[Parsing of transaction mechanism source code (Part 2)](https://www.modb.pro/db/79534)

[Parsing of transaction mechanism source code (Part 3)](https://www.modb.pro/db/81991)

## Parsing of SQL Engine Source Code

[Parsing of SQL engine source code (Part 1)](https://www.modb.pro/db/86068)

[Parsing of SQL engine source code (Part 2)](https://www.modb.pro/db/100257)

## Executor Parsing

[Executor Parsing (Part 1)](https://www.modb.pro/db/103591)

[Executor Parsing (Part 2)](https://www.modb.pro/db/106997)

[Executor Parsing (Part 3)](https://www.modb.pro/db/111770)

## AI Technology

[AI Technology (Part 1): Self-tuning](https://www.modb.pro/db/162007)

[AI Technology (Part 2): Slow SQL discovery](https://www.modb.pro/db/171668)

[AI Technology (Part 3): Index Recommendation](https://www.modb.pro/db/210235)

[AI Technology (Part 4): Metrics Collection, Prediction, and Anomaly Detection](https://www.modb.pro/db/245341)

[AI Technology (Part 5): AI Query Time Prediction](https://www.modb.pro/db/251405)

[AI Technology (Part 6): DeepSQL](https://www.modb.pro/db/329691)

## Parsing of Security Management Source Code

[Parsing of Security Management Source Code (Part 1)](https://www.modb.pro/db/335331)

[Parsing of Security Management Source Code (Part 2)](https://www.modb.pro/db/337160)

[Parsing of Security Management Source Code (Part 3)](https://www.modb.pro/db/378249)