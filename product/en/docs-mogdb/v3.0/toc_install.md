<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 3.0

## Installation Guide

+ Installation Preparation
  + [Environment Requirement](/installation-guide/installation-preparation/environment-requirement.md)
  + [Operating System Configuration](/installation-guide/installation-preparation/os-configuration.md)
+ [Container Installation](/installation-guide/docker-installation/docker-installation.md)
+ [PTK-based Installation](/installation-guide/ptk-based-installation.md)
+ OM-based Installation
  + [Installation Overview](/installation-guide/om-installation/installation-overview.md)
  + [Obtaining the Installation Package](/installation-guide/om-installation/obtaining-installation-package.md)
  + [Simple Installation](/installation-guide/om-installation/simple-installation.md)
  + [Standard Installation](/installation-guide/om-installation/standard-installation.md)
  + [Verifying the Installation](/installation-guide/om-installation/verifying-installation.md)
  + [Uninstalling MogDB](/installation-guide/om-installation/uninstallation.md)
+ [Manual Installation](/installation-guide/manual-installation.md)
+ [Recommended Parameter Settings](/installation-guide/recommended-parameter-settings.md)
