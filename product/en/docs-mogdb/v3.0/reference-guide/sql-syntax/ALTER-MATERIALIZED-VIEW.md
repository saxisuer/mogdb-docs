---
title: ALTER MATERIALIZED VIEW
summary: ALTER MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER MATERIALIZED VIEW

## Function

**ALTER MATERIALIZED VIEW** changes multiple auxiliary attributes of an existing materialized view.

Statements and actions that can be used for ALTER MATERIALIZED VIEW are a subset of ALTER TABLE and have the same meaning when used for materialized views. For details, see **ALTER TABLE**.

## Precautions

- Only the owner of a materialized view or a system administrator has the **ALTER TMATERIALIZED VIEW** permission.
- The materialized view structure cannot be modified.

## Syntax

- Modify the definition of the materialized view.

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      OWNER TO new_owner;
  ```

- Modify the column of a materialized view.

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME [ COLUMN ] column_name TO new_column_name;
  ```

- Rename a materialized view.

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME TO new_name;
  ```

## Parameter Description

- **mv_name**

  Specifies the name of an existing materialized view, which can be schema-qualified.

  Value range: a string. It must comply with the naming convention.

- **column_name**

  Specifies the name of a new or existing column.

  Value range: a string. It must comply with the naming convention.

- **new_column_name**

  Specifies the new name of an existing column.

- **new_owner**

  Specifies the user name of the new owner of a materialized view.

- **new_name**

  Specifies the user name of the new owner of a materialized view.

## Example

```sql
-- Rename the materialized view foo to bar.
MogDB=# ALTER MATERIALIZED VIEW foo RENAME TO bar;
```

## Helpful Links

[CREATE MATERIALIZED VIEW](CREATE-MATERIALIZED-VIEW.md), [CREATE INCREMENTAL MATERIALIZED VIEW](CREATE-INCREMENTAL-MATERIALIZED-VIEW.md), [DROP MATERIALIZED VIEW](DROP-MATERIALIZED-VIEW.md), [REFRESH INCREMENTAL MATERIALIZED VIEW](REFRESH-INCREMENTAL-MATERIALIZED-VIEW.md) and  [REFRESH MATERIALIZED VIEW](REFRESH-MATERIALIZED-VIEW.md)
