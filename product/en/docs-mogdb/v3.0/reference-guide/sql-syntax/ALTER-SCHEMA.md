---
title: ALTER SCHEMA
summary: ALTER SCHEMA
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SCHEMA

## Function

**ALTER SCHEMA** alters the attributes of a schema.

## Precautions

- Only the owner of a schema or users granted with the  **ALTER**  permission on the schema can run the  **ALTER SCHEMA**  command. The system administrator has this permission by default. To change the owner of a schema, you must be the owner of the schema or system administrator and a member of the new owner role.
- Only the initial user is allowed to change the owner of the  **pg\_catalog**  system schema.

## Syntax

- Alter the tamper-proof attribute of a schema.

    ```ebnf+diagram
    AlterSchema ::= ALTER SCHEMA schema_name { WITH | WITHOUT } BLOCKCHAIN
    ```

- Rename a schema.

  ```ebnf+diagram
  AlterSchema ::= ALTER SCHEMA schema_name
      RENAME TO new_name;
  ```

- Change the owner of a schema.

  ```ebnf+diagram
  AlterSchema ::= ALTER SCHEMA schema_name
      OWNER TO new_owner;
  ```

## Parameter Description

- **schema_name**

  Specifies the name of an existing schema.

  Value range: an existing schema name.

- **RENAME TO new_name**

  Rename a schema. If a non-administrator user wants to change the schema name, the user must have the  **CREATE**  permission on the database.

  **new_name**: new name of the schema.

  Value range: a string. It must comply with the identifier naming convention.

- **OWNER TO new_owner**

  Change the owner of a schema. To do this as a non-administrator, you must be a direct or indirect member of the new owner role, and that role must have the  **CREATE**  permission on the database.

  **new_owner**: new owner of the schema.

  Value range: an existing username or role name.

- **{ WITH | WITHOUT } BLOCKCHAIN**

    Alters the tamper-proof attribute of a schema. Common row-store tables with the tamper-proof attribute are tamper-proof history tables, excluding foreign tables, temporary tables, and system catalogs. The tamper-proof attribute can be altered only when no table is contained in the schema. In addition, the temporary table mode is not supported. Alter the tamper-proof attribute in the  **toast table**  schema,  **dbe\_perf**  schema, and  **blockchain**  schema.

## Examples

```sql
-- Create the ds schema.
MogDB=# CREATE SCHEMA ds;

-- Rename the current schema ds to ds_new.
MogDB=# ALTER SCHEMA ds RENAME TO ds_new;

-- Create user jack.
MogDB=# CREATE USER jack PASSWORD 'xxxxxxxxx';

-- Change the owner of ds_new to jack.
MogDB=# ALTER SCHEMA ds_new OWNER TO jack;

-- Delete user jack and schema ds_new.
MogDB=# DROP SCHEMA ds_new;
MogDB=# DROP USER jack;
```

## Helpful Links

[CREATE SCHEMA](CREATE-SCHEMA.md)，[DROP SCHEMA](DROP-SCHEMA.md)
