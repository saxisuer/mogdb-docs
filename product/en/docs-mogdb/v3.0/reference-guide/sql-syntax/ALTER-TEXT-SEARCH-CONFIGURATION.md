---
title: ALTER TEXT SEARCH CONFIGURATION
summary: ALTER TEXT SEARCH CONFIGURATION
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER TEXT SEARCH CONFIGURATION

## Function

**ALTER TEXT SEARCH CONFIGURATION** modifies the definition of a text search configuration. You can modify its mappings from token types to dictionaries, change the configuration's name or owner, or modify the parameters.

The **ADD MAPPING FOR** form installs a list of dictionaries to be consulted for the specified token types; an error will be generated if there is already a mapping for any of the token types.

The **ALTER MAPPING FOR** form removes existing mapping for those token types and then adds specified mappings.

**ALTER MAPPING REPLACE …** **WITH …** and **ALTER MAPPING FOR…** **REPLACE …** **WITH …** options replace **old_dictionary** with **new_dictionary**. Note that only when **pg_ts_config_map** has tuples corresponding to **maptokentype** and **old_dictionary**, the update will succeed. If the update fails, no messages are returned.

The **DROP MAPPING FOR** form deletes all dictionaries for the specified token types in the text search configuration. If **IF EXISTS** is not specified and the string type mapping specified by **DROP MAPPING FOR** does not exist in text search configuration, an error will occur in the database.

## Precautions

- If a search configuration is referenced (to create indexes), users are not allowed to modify the text search configuration.
- To use **ALTER TEXT SEARCH CONFIGURATION**, you must be the owner of the configuration.

## Syntax

- Add text search configuration string mapping.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name
  ADD MAPPING FOR token_type [, ... ] WITH dictionary_name [, ... ];
  ```

- Modify the text search configuration dictionary syntax.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name
  ALTER MAPPING FOR token_type [, ... ] REPLACE old_dictionary WITH new_dictionary;
  ```

- Modify the text search configuration string.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name
  ALTER MAPPING FOR token_type [, ... ] WITH dictionary_name [, ... ];
  ```

- Change the text search configuration dictionary.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name
  ALTER MAPPING REPLACE old_dictionary WITH new_dictionary;
  ```

- Remove text search configuration string mapping.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name
  DROP MAPPING [ IF EXISTS ] FOR token_type [, ... ];
  ```

- Rename the owner of text search configuration.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name OWNER TO new_owner;
  ```

- Rename the text search configuration.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name RENAME TO new_name;
  ```

- Rename the namespace of text search configuration.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name SET SCHEMA new_schema;
  ```

- Modify the attributes of the text search configuration.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name SET ( { configuration_option = value } [, ...] );
  ```

- Reset the attributes of text search configuration.

  ```ebnf+diagram
  AlterTextSearchConfiguration ::= ALTER TEXT SEARCH CONFIGURATION name RESET ( {configuration_option} [, ...] );
  ```

## Parameter Description

- **name**

  Specifies the name (optionally schema-qualified) of an existing text search configuration.

- **token_type**

  Specifies the name of a token type that is emitted by the configuration's parser. For details, see **Parser**.

- **dictionary_name**

  Specifies the name of a text search dictionary. If multiple dictionaries are listed, they are searched in the specified order.

- **old_dictionary**

  Specifies the name of a text search dictionary to be replaced in the mapping.

- **new_dictionary**

  Specifies the name of a text search dictionary to be substituted for **old_dictionary**.

- **new_owner**

  Specifies the new owner of the text search configuration.

- **new_name**

  Specifies the new name of the text search configuration.

- **new_schema**

  Specifies the new schema for the text search configuration.

- **configuration_option**

  Specifies the text search configuration option. For details, see **CREATE TEXT SEARCH CONFIGURATION**.

- **value**

  Specifies the value of text search configuration option.

## Examples

```sql
-- Create a text search configuration.
MogDB=# CREATE TEXT SEARCH CONFIGURATION english_1 (parser=default);
CREATE TEXT SEARCH CONFIGURATION

-- Add text search configuration string mapping.
MogDB=# ALTER TEXT SEARCH CONFIGURATION english_1 ADD MAPPING FOR word WITH simple,english_stem;
ALTER TEXT SEARCH CONFIGURATION

-- Add text search configuration string mapping.
MogDB=# ALTER TEXT SEARCH CONFIGURATION english_1 ADD MAPPING FOR email WITH english_stem, french_stem;
ALTER TEXT SEARCH CONFIGURATION

-- Query information about the text search configuration.
MogDB=# SELECT b.cfgname,a.maptokentype,a.mapseqno,a.mapdict,c.dictname FROM pg_ts_config_map a,pg_ts_config b, pg_ts_dict c WHERE a.mapcfg=b.oid AND a.mapdict=c.oid AND b.cfgname='english_1' ORDER BY 1,2,3,4,5;
  cfgname  | maptokentype | mapseqno | mapdict |   dictname
-----------+--------------+----------+---------+--------------
 english_1 |            2 |        1 |    3765 | simple
 english_1 |            2 |        2 |   12960 | english_stem
 english_1 |            4 |        1 |   12960 | english_stem
 english_1 |            4 |        2 |   12964 | french_stem
(4 rows)

-- Add text search configuration string mapping.
MogDB=# ALTER TEXT SEARCH CONFIGURATION english_1 ALTER MAPPING REPLACE french_stem with german_stem;
ALTER TEXT SEARCH CONFIGURATION

-- Query information about the text search configuration.
MogDB=# SELECT b.cfgname,a.maptokentype,a.mapseqno,a.mapdict,c.dictname FROM pg_ts_config_map a,pg_ts_config b, pg_ts_dict c WHERE a.mapcfg=b.oid AND a.mapdict=c.oid AND b.cfgname='english_1' ORDER BY 1,2,3,4,5;
  cfgname  | maptokentype | mapseqno | mapdict |   dictname
-----------+--------------+----------+---------+--------------
 english_1 |            2 |        1 |    3765 | simple
 english_1 |            2 |        2 |   12960 | english_stem
 english_1 |            4 |        1 |   12960 | english_stem
 english_1 |            4 |        2 |   12966 | german_stem
(4 rows)
```

See **Examples** in [CREATE TEXT SEARCH CONFIGURATION](CREATE-TEXT-SEARCH-CONFIGURATION.md).

## Helpful Links

[CREATE TEXT SEARCH CONFIGURATION](CREATE-TEXT-SEARCH-CONFIGURATION.md), [DROP TEXT SEARCH CONFIGURATION](DROP-TEXT-SEARCH-CONFIGURATION.md)
