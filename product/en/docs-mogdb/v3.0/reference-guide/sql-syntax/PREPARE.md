---
title: PREPARE
summary: PREPARE
author: Zhang Cuiping
date: 2021-05-18
---

# PREPARE

## Function

**PREPARE** creates a prepared statement.

A prepared statement is a performance optimizing object on the server. When the **PREPARE** statement is executed, the specified query is parsed, analyzed, and rewritten. When **EXECUTE** is executed, the prepared statement is planned and executed. This avoids repetitive parsing and analysis. After the **PREPARE** statement is created, it exists throughout the database session. Once it is created (even if in a transaction block), it will not be deleted when a transaction is rolled back. It can only be deleted by explicitly invoking **DEALLOCATE** or automatically deleted when the session ends.

## Precautions

None

## Syntax

```ebnf+diagram
Prepare ::= PREPARE name [ ( data_type [, ...] ) ] AS statement;
```

## Parameter Description

- **name**

  Specifies the name of a prepared statement. It must be unique in the session.

- **data_type**

  Specifies the type of an argument.

- **statement**

  Specifies a **SELECT**, **INSERT**, **UPDATE**, **DELETE**, **MERGE INTO**, or **VALUES** statement.

## Examples

See **Examples** in [EXECUTE](EXECUTE.md).

## Helpful Links

[DEALLOCATE](DEALLOCATE.md)
