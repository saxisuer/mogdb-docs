---
title: CREATE SERVER
summary: CREATE SERVER
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SERVER

## Function

**CREATE SERVER** defines a new foreign server.

## Syntax

```ebnf+diagram
CreateServer ::= CREATE SERVER server_name
    FOREIGN DATA WRAPPER fdw_name
    OPTIONS ( { option_name ' value ' } [, ...] ) ;
```

## Parameter Description

- **server_name**

  Specifies the server name.

  Value range: a string containing no more than 63 characters

- **fdw_name**

  Specifies the name of the foreign data wrapper.

  Value range:  **oracle_fdw**,  **mysql_fdw**,  **postgres_fdw**,  **mot_fdw**, and  **file_fdw**

- **OPTIONS ( { option_name ' value ' } [, …] )**

    Specifies options for the server. These options typically define the connection details of the server, but the actual names and values depend on the foreign data wrapper of the server.

  - Options supported by  **oracle_fdw**  are as follows:

    - **dbserver**

        Connection string of the remote Oracle database.

    - **isolation_level** (default value: **serializable**)

        Oracle database transaction isolation level.

        Value range: serializable, read_committed, and read_only

  - Options supported by mysql_fdw are as follows:

    - **host** (default value: **127.0.0.1**)

        IP address of the MySQL server or MariaDB.

    - **port** (default value: **3306**)

        Listening port number of the MySQL server or MariaDB.

  - The options supported by postgres_fdw are the same as those supported by libpq. For details, see [Link Parameters](../../developer-guide/dev/4-development-based-on-libpq/link-parameters.md). Note that the following options cannot be set:

    - **user** and **password**

        The user name and password are specified when the user mapping is created.

    - **client_encoding**

        The encoding mode of the local server is automatically obtained and set.

    - **application_name**

        This option is always set to **postgres_fdw**.

  - Specifies the parameters for the foreign server. The detailed parameter description is as follows:

    - encrypt

        Specifies whether data is encrypted. This parameter is available only when **type** is **OBS**. The default value is **on**.

         Value range:

      - **on** indicates that data is encrypted and HTTPS is used for communication.
      - **off** indicates that data is not encrypted and HTTP is used for communication.

    - access_key

        Specifies the access key (AK) (obtained by users from the OBS console) used for the OBS access protocol. When you create a foreign table, the AK value is encrypted and saved to the metadata table of the database. This parameter is available only when **type** is set to **OBS**.

    - secret_access_key

        Specifies the secret key (SK) value (obtained by users from the OBS console) used for the OBS access protocol. When you create a foreign table, the SK value is encrypted and saved to the metadata table of the database. This parameter is available only when **type** is set to **OBS**.

In addition to the connection parameters supported by libpq, the following options are provided:

- **use_remote_estimate**

  Controls whether  **postgres_fdw**  issues the EXPLAIN command to obtain the estimated run time. The default value is  **false**.

- **fdw_startup_cost**

  Estimates the startup time required for a foreign table scan, including the time to establish a connection, analyzes the request at the remote server, and generates a plan. The default value is  **100**.

- **fdw_typle_cost**

  Specifies the additional consumption when each tuple is scanned on a remote server. The value specifies the extra consumption of data transmission between servers. The default value is **0.01**.

## Examples

Create a server.

```sql
MogDB=# create server my_server foreign data wrapper log_fdw;
CREATE SERVER
```

## Helpful Links

[ALTER SERVER](ALTER-SERVER.md)  and  [DROP SERVER](DROP-SERVER.md)
