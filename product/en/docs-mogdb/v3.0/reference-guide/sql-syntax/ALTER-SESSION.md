---
title: ALTER SESSION
summary: ALTER SESSION
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SESSION

## Function

**ALTER SESSION** defines or modifies the conditions or parameters that affect the current session. Modified session parameters are kept until the current session is disconnected.

## Precautions

- If the **START TRANSACTION** statement is not executed before the **SET TRANSACTION** statement, the transaction is ended instantly and the statement does not take effect.
- You can use the **transaction_mode(s)** method declared in the **START TRANSACTION** statement to avoid using the **SET TRANSACTION** statement.

## Syntax

- Set transaction parameters of a session.

  ```ebnf+diagram
  AlterSession ::= ALTER SESSION SET [ SESSION CHARACTERISTICS AS ] TRANSACTION
      { ISOLATION LEVEL { READ COMMITTED } | { READ ONLY  | READ WRITE } } [, ...] ;
  ```

- Set other running parameters of a session.

  ```ebnf+diagram
  AlterSession ::= ALTER SESSION SET
      {{config_parameter { { TO  | =  }  { value | DEFAULT }
        | FROM CURRENT }}
        | TIME ZONE time_zone
        | CURRENT_SCHEMA schema
        | NAMES encoding_name
        | ROLE role_name PASSWORD 'password'
        | SESSION AUTHORIZATION { role_name PASSWORD 'password' | DEFAULT }
        | XML OPTION { DOCUMENT | CONTENT }
      } ;
  ```

## Parameter Description

For details about the descriptions of parameters related to **ALTER SESSION**, see **Parameter Description** of the SET syntax.

## Examples

```sql
-- Create the ds schema.
MogDB=# CREATE SCHEMA ds;

-- Set the search path of the schema.
MogDB=# SET SEARCH_PATH TO ds, public;

-- Set the time/date type to the traditional postgres format (date before month).
MogDB=# SET DATESTYLE TO mogdb, dmy;

-- Set the character code of the current session to UTF8.
MogDB=# ALTER SESSION SET NAMES 'UTF8';

-- Set the time zone to Berkeley of California.
MogDB=# SET TIME ZONE 'PST8PDT';

-- Set the time zone to Italy.
MogDB=# SET TIME ZONE 'Europe/Rome';

-- Set the current schema.
MogDB=# ALTER SESSION SET CURRENT_SCHEMA TO tpcds;

-- Set XML OPTION to DOCUMENT.
MogDB=# ALTER SESSION SET XML OPTION DOCUMENT;

-- Create the role joe, and set the session role to joe.
MogDB=# CREATE ROLE joe WITH PASSWORD 'xxxxxxxxx';
MogDB=# ALTER SESSION SET SESSION AUTHORIZATION joe PASSWORD 'xxxxxxxxx';

-- Switch to the default user.
MogDB=> ALTER SESSION SET SESSION AUTHORIZATION default;

-- Delete the ds schema.
MogDB=# DROP SCHEMA ds;

-- Delete the role joe.
MogDB=# DROP ROLE joe;
```

## Helpful Links

[SET](SET.md)
