---
title: DROP OPERATOR
summary: DROP OPERATOR
author: Zhang Cuiping
date: 2021-06-07
---

# DROP OPERATOR

MogDB does not support the drop operator function.
