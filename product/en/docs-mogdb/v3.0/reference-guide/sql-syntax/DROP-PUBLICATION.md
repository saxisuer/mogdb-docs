---
title: DROP PUBLICATION
summary: DROP PUBLICATION
author: Guo Huan
date: 2022-05-16
---

# DROP PUBLICATION

## Function

**DROP PUBLICATION** deletes an existing publication from a database.

## Precautions

A publication can be deleted only by its owner or the system administrator.

## Syntax

```ebnf+diagram
DropPublication ::= DROP PUBLICATION [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Does not throw an error if a publication does not exist, but instead reports a notice.

- **name**

  Specifies the name of an existing publication.

- **CASCADE|RESTRICT**

  Currently, these keywords do not work because there is no dependency on publications.

## Examples

For details, see Examples in [CREATE PUBLICATION](CREATE-PUBLICATION.md).

## Helpful Links

[ALTER PUBLICATION](ALTER-PUBLICATION.md), [CREATE PUBLICATION](CREATE-PUBLICATION.md)
