---
title: CREATE INCREMENTAL MATERIALIZED VIEW
summary: CREATE INCREMENTAL MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE INCREMENTAL MATERIALIZED VIEW

## Function

**CREATE INCREMENTAL MATERIALIZED VIEW**  creates an fast-refresh materialized view, and you can refresh the data of the materialized view by using  **REFRESH MATERIALIZED VIEW**  (full refresh) and  **REFRESH INCREMENTAL MATERIALIZED VIEW**  (incremental refresh).

**CREATE INCREMENTAL MATERIALIZED VIEW**  is similar to  **CREATE TABLE AS**, but it remembers the query used to initialize the view, so it can refresh data later. A materialized view has many attributes that are the same as those of a table, but does not support temporary materialized views.

## Precautions

- fast-refresh materialized views cannot be created on temporary tables or global temporary tables.
- fast-refresh materialized views support only simple filter queries and UNION ALL queries of base tables.
- Distribution columns cannot be specified when an incremental MV is created.
- After an fast-refresh materialized view is created, most DDL operations in the base table are no longer supported.
- IUD operations cannot be performed on fast-refresh materialized views.
- After an fast-refresh materialized view is created, you need to run the  **REFRESH**  command to synchronize the materialized view with the base table when the base table data changes.

## Syntax

```ebnf+diagram
CreateIncrementalMaterializedView ::= CREATE INCREMENTAL MATERIALIZED VIEW mv_name
    [ (column_name [, ...] ) ]
    [ TABLESPACE tablespace_name ]
    AS query;
```

## Parameter Description

- **mv_name**

  Name (optionally schema-qualified) of the materialized view to be created.

  Value range: a string. It must comply with the naming convention.

- **column_name**

  Column name in the new materialized view. The materialized view supports specified columns. The number of specified columns must be the same as the number of columns in the result of the subsequent query statement. If no column name is provided, the column name is obtained from the output column name of the query.

  Value range: a string. It must comply with the naming convention.

- **TABLESPACE tablespace_name**

  Tablespace to which the new materialized view belongs. If not specified, the default tablespace is used.

- **AS query**

  **SELECT**, **TABLE**, or **VALUES** command This query will be run in a security-constrained operation.

## Examples

```sql
-- Create an ordinary table.
MogDB=# CREATE TABLE my_table (c1 int, c2 int);
-- Create an fast-refresh materialized view.
MogDB=# CREATE INCREMENTAL MATERIALIZED VIEW my_imv AS SELECT * FROM my_table;
-- Write data to the base table.
MogDB=# INSERT INTO my_table VALUES(1,1),(2,2);
-- Incrementally refresh the fast-refresh materialized view my_imv.
MogDB=# REFRESH INCREMENTAL MATERIALIZED VIEW my_imv;
```

## Helpful Links

[ALTER MATERIALIZED VIEW](ALTER-MATERIALIZED-VIEW.md), [CREATE MATERIALIZED VIEW](CREATE-MATERIALIZED-VIEW.md), [CREATE TABLE](CREATE-TABLE.md), [DROP MATERIALIZED VIEW](DROP-MATERIALIZED-VIEW.md), [REFRESH INCREMENTAL MATERIALIZED VIEW](REFRESH-INCREMENTAL-MATERIALIZED-VIEW.md), [REFRESH MATERIALIZED VIEW](REFRESH-MATERIALIZED-VIEW.md)
