---
title: DROP SUBSCRIPTION
summary: DROP SUBSCRIPTION
author: Guo Huan
date: 2022-05-16
---

# DROP SUBSCRIPTION

## Function

**DROP SUBSCRIPTION** deletes a subscription from a database instance.

## Precautions

- A subscription can be deleted only by the system administrator.
- If the subscription to be deleted is associated with a replication slot, **DROP SUBSCRIPTION** cannot be executed inside a transaction block.

## Syntax

```ebnf+diagram
DropSubscription ::= DROP SUBSCRIPTION [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **name**

  Specifies the name of the subscription to be deleted.

- **CASCADE|RESTRICT**

  Currently, these keywords do not work because there is no dependency on subscriptions.

## Examples

For details, see Examples in [CREATE SUBSCRIPTION](CREATE-SUBSCRIPTION.md).

## Helpful Links

[ALTER SUBSCRIPTION](ALTER-SUBSCRIPTION.md), [CREATE SUBSCRIPTION](CREATE-SUBSCRIPTION.md)
