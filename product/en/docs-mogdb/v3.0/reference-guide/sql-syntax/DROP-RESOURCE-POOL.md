---
title: DROP RESOURCE POOL
summary: DROP RESOURCE POOL
author: Zhang Cuiping
date: 2021-11-01
---

# DROP RESOURCE POOL

## Function

**DROP RESOURCE POOL** deletes a resource pool.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The resource pool cannot be deleted if it is associated with a role.

## Precautions

Only a user with the **DROP** permission on the current database can perform this operation.

## Syntax

```ebnf+diagram
DropResourcePool ::= DROP RESOURCE POOL [ IF EXISTS ] pool_name;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if a specified resource pool does not exist.

- **pool_name**

  Specifies the name of a created resource pool.

  Value range: a string. It must comply with the identifier naming convention.

## Examples

See **Examples** in **CREATE RESOURCE POOL**.

## Helpful Links

[ALTER RESOURCE POOL](ALTER-RESOURCE-POOL.md) and [CREATE RESOURCE POOL](CREATE-RESOURCE-POOL.md)