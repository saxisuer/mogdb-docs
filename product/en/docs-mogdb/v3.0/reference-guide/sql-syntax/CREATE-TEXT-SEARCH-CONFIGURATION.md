---
title: CREATE TEXT SEARCH CONFIGURATION
summary: CREATE TEXT SEARCH CONFIGURATION
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE TEXT SEARCH CONFIGURATION

## Function

**CREATE TEXT SEARCH CONFIGURATION** creates a text search configuration. A text search configuration specifies a text search parser that can divide a string into tokens, plus dictionaries that can be used to determine which tokens are of interest for searching.

## Precautions

- If only the parser is specified, the new text search configuration initially has no mapping from token types to dictionaries, and therefore will ignore all words. Subsequently, **ALTER TEXT SEARCH CONFIGURATION** must be used to create mapping to make the configuration useful. If **COPY** is specified, the parser, mapping and parameters of the text search configuration is copied automatically.
- If the schema name is given, the text search configuration will be created in the specified schema. Otherwise, the configuration will be created in the current schema.
- The user who defines a text search configuration becomes its owner.
- **PARSER** and **COPY** options are mutually exclusive, because when an existing configuration is copied, its parser selection is copied too.
- If only the parser is specified, the new text search configuration initially has no mapping from token types to dictionaries, and therefore will ignore all words.

## Syntax

```ebnf+diagram
CreateTextSearchConfiguration ::= CREATE TEXT SEARCH CONFIGURATION name
    ( PARSER = parser_name | COPY = source_config )
    [ WITH ( {configuration_option = value} [, ...] )];
```

## Parameter Description

- **name**

  Specifies the name of the text search configuration to be created. The name can be schema-qualified.

- **parser_name**

  Specifies the name of the text search parser to use for this configuration.

- **source_config**

  Specifies the name of an existing text search configuration to copy.

- **configuration_option**

  Specifies parameters for the text search configuration, particularly for the parser executed by **parser_name** or contained by **source_config**.

  Value range: The default and **ngram** parsers are supported. The parser of default type has no corresponding **configuration_option**. Table 1 lists **configuration_option** for **ngram** parsers.

  **Table 1** Configuration parameters for **ngram** parsers <a id="configurationparameter"> </a>

  | Parser             | Parameter                              | Description                                                  | Value Range                               |
  | :------------------------------------- | :------------------------------------- | :----------------------------------------------------------- | :---------------------------------------- |
  | ngram              | gram_size                              | Length of word segmentation                                  | Integer, 1 to 4<br />Default value: **2** |
  | punctuation_ignore | Whether to ignore punctuations         | - **true** (default value): Ignore punctuations.<br />- **false**: Do not ignore punctuations. |                                           |
  | grapsymbol_ignore  | Whether to ignore graphical characters | - **true**: Ignore graphical characters.<br />- **false** (default value): Do not ignore graphical characters. |                                           |

## Examples

```sql
-- Create a text search configuration.
MogDB=# CREATE TEXT SEARCH CONFIGURATION ngram2 (parser=ngram) WITH (gram_size = 2, grapsymbol_ignore = false);

-- Create a text search configuration.
MogDB=# CREATE TEXT SEARCH CONFIGURATION ngram3 (copy=ngram2) WITH (gram_size = 2, grapsymbol_ignore = false);

-- Add type mapping.
MogDB=# ALTER TEXT SEARCH CONFIGURATION ngram2 ADD MAPPING FOR multisymbol WITH simple;

-- Create user joe.
MogDB=# CREATE USER joe IDENTIFIED BY 'xxxxxxxxx';

-- Change the owner of the text search configuration.
MogDB=# ALTER TEXT SEARCH CONFIGURATION ngram2 OWNER TO joe;

-- Change the schema of the text search configuration.
MogDB=# ALTER TEXT SEARCH CONFIGURATION ngram2 SET SCHEMA joe;

-- Rename the text search configuration.
MogDB=# ALTER TEXT SEARCH CONFIGURATION joe.ngram2 RENAME TO ngram_2;

-- Delete the type mapping.
MogDB=# ALTER TEXT SEARCH CONFIGURATION joe.ngram_2 DROP MAPPING IF EXISTS FOR multisymbol;

-- Delete the text search configuration.
MogDB=# DROP TEXT SEARCH CONFIGURATION joe.ngram_2;
MogDB=# DROP TEXT SEARCH CONFIGURATION ngram3;

-- Delete the schema and user joe.
MogDB=# DROP SCHEMA IF EXISTS joe CASCADE;
MogDB=# DROP ROLE IF EXISTS joe;
```

## Helpful Links

[ALTER TEXT SEARCH CONFIGURATION](ALTER-TEXT-SEARCH-CONFIGURATION.md), [DROP TEXT SEARCH CONFIGURATION](DROP-TEXT-SEARCH-CONFIGURATION.md)
