---
title: CLEAN CONNECTION
summary: CLEAN CONNECTION
author: Zhang Cuiping
date: 2021-11-01
---

# CLEAN CONNECTION

## Function

**CLEAN CONNECTION** can be used to clean database connections. It is allowed to clean related connections of specified users with specified database.

## Precautions

1. MogDB does not support specifying nodes but only TO ALL.

2. In the non-force mode, this function does not clean connections. In the force mode, this function can clean normal connections in use.

## Syntax

```ebnf+diagram
CleanConnection ::= CLEAN CONNECTION
      TO { COORDINATOR ( nodename [, ... ] ) | NODE ( nodename [, ... ] )| ALL [ CHECK ] [ FORCE ] }
      [ FOR DATABASE dbname ]
      [ TO USER username ];
```

## Parameter Description

- **CHECK**

  Checks whether a database is accessed by other sessions before cleaning connections. This parameter can be set only when the node list is TO ALL. This parameter is used for connection check before a database is deleted. If detecting a connection, it will report an error and stop deleting a database.

- **FORCE**

  Specifies that all threads related to specified databases and users will receive SIGTERM signals and are forcibly disabled. This parameter can be set only when the node list is TO ALL.

- **COORDINATOR ( nodename [, … ] ) | NODE ( nodename [, … ] ) | ALL**

  Deletes connections with a specified node. There are three scenarios:

  - Deletes connection with a specified CN. MogDB does not support this scenario.
  - Deletes connection with a specified DN. MogDB does not support this scenario.
  - Deletes connections with all nodes (TO ALL). MogDB supports only this scenario.

- **dbname**

  Deletes connections with a specified database. If not specified, connections with all databases will be deleted.

  Value range: an existing database name

- **username**

  Deletes connections with a specified user. If not specified, connections with all users will be deleted.

  Value range: an existing user

## Examples

```sql
--Create user jack.
CREATE USER jack PASSWORD 'Bigdata123@';

--Delete all connections of user jack with the template1 database.
CLEAN CONNECTION TO ALL FOR DATABASE template1 TO USER jack;

--Delete all connections with user jack.
CLEAN CONNECTION TO ALL TO USER jack;

--Delete all connections with the gaussdb database.
CLEAN CONNECTION TO ALL FORCE FOR DATABASE gaussdb;

--Delete user jack.
DROP USER jack;
```
