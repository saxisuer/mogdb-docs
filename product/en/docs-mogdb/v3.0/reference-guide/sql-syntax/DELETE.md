---
title: DELETE
summary: DELETE
author: Zhang Cuiping
date: 2021-05-10
---

# DELETE

## Function

**DELETE** deletes rows that satisfy the **WHERE** clause from the specified table. If the **WHERE** clause is absent, the effect is to delete all rows in the table. The result is a valid, but an empty table.

## Precautions

- The owner of a table, users granted with the  **DELETE**  permission on the table, or users granted with the  **DELETE ANY TABLE**  permission can delete data from the table. The system administrator has the permission to delete data from the table by default, as well as the  **SELECT**  permission on any table in the  **USING**  clause or whose values are read in  **condition**.
- For column-store tables, the **RETURNING** clause is currently not supported.

## Syntax

```ebnf+diagram
Delete ::= [ WITH [ RECURSIVE ] with_query [, ...] ]
DELETE [/*+ plan_hint */] [FROM] [ ONLY ] table_name [partition_clause] [ * ] [ [ AS ] alias ]
    [ USING using_list ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING { * | { output_expr [ [ AS ] output_name ] } [, ...] } ];
```

## Parameter Description

- **WITH [ RECURSIVE ] with_query [, …]**

  Specifies one or more subqueries that can be referenced by name in the main query, which is equivalent to a temporary table.

  If **RECURSIVE** is specified, it allows a **SELECT** subquery to reference itself by name.

  Format of **with_query**:

  ```ebnf+diagram
  with_query_name ::= with_query_name [ ( column_name [, ...] ) ] AS [ [ NOT ] MATERIALIZED ] ( {select | values | insert | update | delete} )
  ```
  
  - **with_query_name** specifies the name of the result set generated by a subquery. Such names can be used to access the subquery result set.
  - **column_name** specifies the column name displayed in the subquery result set.
  - Each subquery can be a **SELECT**, **VALUES**, **INSERT**, **UPDATE** or **DELETE** statement.
  - You can use  **MATERIALIZED**  or  **NOT MATERIALIZED**  to modify the CTE.
  - If  **MATERIALIZED**  is specified, the WITH query will be materialized, and a copy of the subquery result set is generated. The copy is directly queried at the reference point. Therefore, the WITH subquery cannot be jointly optimized with the SELECT statement trunk \(for example, predicate pushdown and equivalence class transfer\). In this scenario, you can use  **NOT MATERIALIZED**  for modification. If the WITH query can be executed as a subquery inline, the preceding optimization can be performed.
  - If the user does not explicitly declare the materialized attribute, comply with the following rules: If the CTE is referenced only once in the trunk statement to which it belongs and semantically supports inline execution, it will be rewritten as subquery inline execution. Otherwise, the materialized execution will be performed in CTE Scan mode.
  
- **plan_hint** clause

  Follows the **DELETE** keyword in the **/\*+ \*/** format. It is used to optimize the plan of a **DELETE** statement block. For details, see [Hint-based Tuning](../../performance-tuning/2-sql/9-hint-based-tuning.md). In each statement, only the first **/\*+** _plan_hint _***/** comment block takes effect as a hint. Multiple hints can be written.

- **ONLY**

  If **ONLY** is specified before the table name, matching rows are deleted from the named table only. If **ONLY** is not specified, matching rows are also deleted from any tables inheriting from the named table.

- **table_name**

  Specifies the name (optionally schema-qualified) of the target table.

  Value range: an existing table name

- **partition\_clause**

  Deletes a specified partition.

  PARTITION \{ \( partition\_name \) | FOR \( partition\_value \[, ...\] \) \} |

  SUBPARTITION \{ \( subpartition\_name \) | FOR \( subpartition\_value \[, ...\] \) \}

  For details about the keywords, see  [SELECT](SELECT.md).

  For details, see  [CREATE TABLE SUBPARTITION](CREATE-TABLE-SUBPARTITION.md).

- **alias**

  Specifies a substitute name for the target table.

  Value range: a string. It must comply with the identifier naming convention rule.

- **using_list**

  Specifies the **USING** clause.

- **condition**

  Specifies an expression that returns a Boolean value. Only rows for which this expression returns **true** will be deleted. You are not advised to use numeric types such as int for  **condition**, because such types can be implicitly converted to bool values \(non-zero values are implicitly converted to  **true**  and  **0**  is implicitly converted to  **false**\), which may cause unexpected results.

- **WHERE CURRENT OF cursor_name**

  This parameter is reserved.

- **output_expr**

  Specifies an expression to be computed and returned by the **DELETE** statement after each row is deleted. The expression can use any column names of the table. Write ***** to return all columns.

- **output_name**

  Specifies a name to use for a returned column.

  Value range: a string. It must comply with the identifier naming convention.

## Examples

```sql
-- Create the tpcds.customer_address_bak table.
MogDB=# CREATE TABLE tpcds.customer_address_bak AS TABLE tpcds.customer_address;

-- Delete employees whose ca_address_sk is smaller than 14888 from the tpcds.customer_address_bak table.
MogDB=# DELETE FROM tpcds.customer_address_bak WHERE ca_address_sk < 14888;

-- Delete all data from the tpcds.customer_address_bak table.
MogDB=# DELETE FROM tpcds.customer_address_bak;

Delete the tpcds.customer_address_bak table.
MogDB=# DROP TABLE tpcds.customer_address_bak;
```

## Suggestions

- delete

  To delete all records in a table, use the **truncate** syntax.
