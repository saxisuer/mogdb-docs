---
title: DROP SYNONYM
summary: DROP SYNONYM
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SYNONYM

## Function

**DROP SYNONYM** deletes a synonym.

## Precautions

Only the owner of a synonym or a system administrator has the **DROP SYNONYM** permission.

## Syntax

```ebnf+diagram
DropSynonym ::= DROP SYNONYM [ IF EXISTS ] synonym_name [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified synonym does not exist.

- **synonym_name**

  Specifies the name (optionally schema-qualified) of the synonym to be deleted.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects (such as views) that depend on the synonym.
  - **RESTRICT**: refuses to delete the synonym if any objects depend on it. This is the default action.

## Examples

See **Example** in **CREATE SYNONYM**.

## Helpful Links

[ALTER SYNONYM](ALTER-SYNONYM.md) and [CREATE SYNONYM](CREATE-SYNONYM.md)
