---
title: DROP MODEL
summary: DROP MODEL
author: Zhang Cuiping
date: 2021-11-01
---

# DROP MODEL

## Function

**DROP MODEL** deletes a model object that has been trained and saved.

## Precautions

The deleted model can be viewed in the **gs_model_warehouse** system catalog.

## Syntax

```ebnf+diagram
DropModel ::= DROP MODEL model_name;
```

## Parameter Description

model_name

Specifies a model name.

Value range: a string. It must comply with the identifier naming convention.

## Helpful Links

[CREATE MODEL](CREATE-MODEL.md) and [PREDICT BY](PREDICT-BY.md)
