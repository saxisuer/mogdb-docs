---
title: DROP CLIENT MASTER KEY
summary: DROP CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP CLIENT MASTER KEY

## Function

**DROP CLIENT MASTER KEY** deletes a client master key (CMK).

## Precautions

- Only the CMK owner or a user who has been granted the DROP permission can run this command. By default, the system administrator has the permission.
- This command not only deletes the key object in the database, but also deletes the key file corresponding to the key object in the specified path on the client.

## Syntax

```ebnf+diagram
DropClientMasterKey ::= DROP CLIENT MASTER KEY [ IF EXISTS ] client_master_key_name [CASCADE];
```

## Parameter Description

- **IF EXISTS**

  If a specified CMK does not exist, a notice rather than an error is issued.

- **client_master_key_name**

  Name of a CMK to be deleted.

  Value range: a string. It is the name of an existing CMK object.

- **CASCADE**

  - **CASCADE**: automatically deletes objects that depend on the CMK.

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** During the life cycle of the syntax, the status of both client and server needs to be changed. When an exception occurs, the key information may have been deleted from the server, but the key file is not deleted from the client. In this case, the client does not check whether there are unexpected key files retain due to exceptions during the lifecycle of the next syntax. You need to periodically check the key folder and confirm and process the key files that are not used.

## **Examples**

```sql
-- Delete a CMK object.
MogDB=> DROP CLIENT MASTER KEY ImgCMK CASCADE;
NOTICE:  drop cascades to column setting: imgcek
DROP CLIENT MASTER KEY
```
