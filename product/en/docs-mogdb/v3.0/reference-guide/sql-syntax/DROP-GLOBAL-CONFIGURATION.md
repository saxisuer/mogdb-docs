---
title: DROP GLOBAL CONFIGURATION
summary: DROP GLOBAL CONFIGURATION
author: Guo Huan
date: 2022-05-16
---

# DROP GLOBAL CONFIGURATION

## Function

**DROP GLOBAL CONFIGURATION** deletes parameter values from the **gs_global_config** system catalog.

## Precautions

- Only the initial database user can run this command.
- The **weak_password** keyword cannot be deleted.

## Syntax

```ebnf+diagram
DropGlobalConfiguration ::= DROP GLOBAL CONFIGURATION argname [, ...];
```

## Parameter Description

The parameter is a parameter that already exists in the **gs_global_config** system catalog. If you delete a parameter that does not exist, an error will be reported.
