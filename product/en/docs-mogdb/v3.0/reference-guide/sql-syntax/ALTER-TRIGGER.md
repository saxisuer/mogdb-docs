---
title: ALTER TRIGGER
summary: ALTER TRIGGER
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER TRIGGER

## Function

**ALTER TRIGGER** modifies the name of a trigger.

> **NOTE:** Only the name modification is currently supported.

## Precautions

Only the owner of a table where the trigger is created and a system administrator can run the **ALTER TRIGGER** statement.

## Syntax

```ebnf+diagram
AlterTrigger ::= ALTER TRIGGER trigger_name ON table_name RENAME TO new_name;
```

## Parameter Description

- **trigger_name**

  Specifies the name of the trigger to be modified.

  Value range: an existing trigger

- **table_name**

  Specifies the name of the table where the trigger to be modified is located.

  Value range: an existing table having a trigger

- **new_name**

  Specifies the new name after modification.

  Value range: a string, which complies with the identifier naming convention. A value contains a maximum of 63 characters and cannot be the same as other triggers on the same table.

## Examples

See examples in [CREATE TRIGGER](CREATE-TRIGGER.md).

## Helpful Links

[CREATE TRIGGER](CREATE-TRIGGER.md), [DROP TRIGGER](DROP-TRIGGER.md), [ALTER TABLE](ALTER-TRIGGER.md)
