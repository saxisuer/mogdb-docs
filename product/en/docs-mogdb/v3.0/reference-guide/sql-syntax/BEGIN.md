---
title: BEGIN
summary: BEGIN
author: Zhang Cuiping
date: 2021-05-17
---

# BEGIN

## Function

**BEGIN** may be used to initiate an anonymous block or a single transaction. This section describes the syntax of **BEGIN** used to initiate an anonymous block. For details about the **BEGIN** syntax that initiates transactions, see **START TRANSACTION**.

An anonymous block is a structure that can dynamically create and execute stored procedure code instead of permanently storing code as a database object in the database.

## Precautions

None

## Syntax

- Enable an anonymous block.

  ```ebnf+diagram
  Begin ::= [DECLARE [declare_statements]]
  BEGIN
  execution_statements
  END;

  ```

- Start a transaction.

  ```ebnf+diagram
  Begin ::= BEGIN [ WORK | TRANSACTION ]
    [
      {
         ISOLATION LEVEL { READ COMMITTED | SERIALIZABLE | REPEATABLE READ }
         | { READ WRITE | READ ONLY }
        } [, ...]
    ];
  ```

## Parameter Description

- **declare_statements**

  Declares a variable, including its name and type, for example, **sales_cnt int**.

- **execution_statements**

  Specifies the statement to be executed in an anonymous block.

  Value range: DML operations (such as select, insert, delete, and update) or registered functions in the system catalog.

## Examples

None

## Helpful Links

[START TRANSACTION](START-TRANSACTION.md)
