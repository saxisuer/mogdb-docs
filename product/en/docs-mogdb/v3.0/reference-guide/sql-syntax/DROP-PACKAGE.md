---
title: DROP PACKAGE
summary: DROP PACKAGE
author: Zhang Cuiping
date: 2021-11-08
---

# DROP PACKAGE

## Function

**DROP PACKAGE** deletes the existing package or package body.

## Precautions

After the package body is deleted, the stored procedures and functions in the package become invalid at the same time.

## Syntax

```sql
DROP PACKAGE [ IF EXISTS ] package_name;
DROP PACKAGE BODY [ IF EXISTS ] package_name;
```