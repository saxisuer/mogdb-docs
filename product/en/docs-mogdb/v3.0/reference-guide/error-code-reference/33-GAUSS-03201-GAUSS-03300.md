---
title: GAUSS-03201 - GAUSS-03300
summary: GAUSS-03201 - GAUSS-03300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03201 - GAUSS-03300

<br/>

## GAUSS-03201 - GAUSS-03210

<br/>

GAUSS-03201: "ctid isn't of type TID"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03202: "currtid cannot handle views with no CTID"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03203: "the view has no rules"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03204: "only one select rule is allowed in views"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03205: "currtid cannot handle this view"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03206: "no operand in tsquery: '%s'"

SQLSTATE: 42601

Description: In the **tsquery** string, no variable follows the operator.

Solution: Check the string to ensure that each operator is followed by an object.

GAUSS-03207: "value is too big in tsquery: '%s'"

SQLSTATE: 54000

Description: In the **tsquery** string, the length of the string following an operator exceeds the limit (1<<20) - 1.

Solution: Ensure that the length of the string does not exceed the upper limit.

GAUSS-03208: "operand is too long in tsquery: '%s'"

SQLSTATE: 54000

Description: In the **tsquery** string, the length of a string exceeds the limit (1<<11) - 1.

Solution: Ensure that the length of the string does not exceed the upper limit.

GAUSS-03209: "word is too long in tsquery: '%s'"

SQLSTATE: 54000

Description: In the **tsquery** string, the length of a string exceeds the limit (1<<11) - 1.

Solution: Ensure that the length of the string does not exceed the upper limit.

GAUSS-03210: "tsquery stack too small"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03211 - GAUSS-03220

<br/>

GAUSS-03211: "malformed tsquery: operand not found"

SQLSTATE: XX000

Description: No operator is found in the string of the tsquery type.

Solution: Check the input and ensure that the variable of the tsquery type contains an operator.

GAUSS-03212: "malformed tsquery: extra nodes"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03213: "unrecognized operator type: %d"

SQLSTATE: XX000

Description: An incorrect operator is entered.

Solution: Use only !, |, or & as the input operator.

GAUSS-03214: "unrecognized tsquery node type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03215: "invalid size of tsquery"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03216: "invalid tsquery: invalid weight bitmap"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03218: "invalid tsquery: total operand length exceeded"

SQLSTATE: XX000

Description: The string of the tsquery type exceeds the upper limit (1<<20) - 1.

Solution: Ensure that the length of the string does not exceed the upper limit.

GAUSS-03219: "invalid tsquery: unrecognized operator type %d"

SQLSTATE: XX000

Description: An incorrect operator is entered.

Solution: Ensure that the operator uses one of the following characters: ! | &.

<br/>

## GAUSS-03221 - GAUSS-03230

<br/>

GAUSS-03221: "cannot accept a value of type any"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03222: "cannot display a value of type any"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03223: "cannot accept a value of type anyarray"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03224: "cannot accept a value of type anyenum"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03225: "cannot accept a value of type anyrange"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03226: "cannot accept a value of type trigger"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03227: "cannot display a value of type trigger"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03228: "cannot accept a value of type language_handler"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03229: "cannot display a value of type language_handler"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03230: "cannot accept a value of type fdw_handler"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03231 - GAUSS-03240

<br/>

GAUSS-03231: "cannot display a value of type fdw_handler"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03232: "cannot accept a value of type internal"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03233: "cannot display a value of type internal"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03234: "cannot accept a value of type opaque"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03235: "cannot display a value of type opaque"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03236: "cannot accept a value of type anyelement"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03237: "cannot display a value of type anyelement"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03238: "cannot accept a value of type anynonarray"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03239: "cannot display a value of type anynonarray"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03240: "cannot accept a value of a shell type"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03241 - GAUSS-03250

<br/>

GAUSS-03241: "cannot display a value of a shell type"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03242: "cannot accept a value of type pg_node_tree"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03243: "TIME(%d)%s precision must not be negative"

SQLSTATE: 22023

Description: The time format precision cannot be negative.

Solution: Set the time format precision to positive.

GAUSS-03244: "date/time value 'current' is no longer supported"

SQLSTATE: 0A000

Description: The **current** value is invalid.

Solution: Do not use the **current** value.

GAUSS-03245: "date out of range: '%s'"

SQLSTATE: 22008

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03246: "date out of range"

SQLSTATE: 22008

Description: The date exceeds the limit.

Solution: Change the date to a valid value.

GAUSS-03248: "cannot subtract infinite dates"

SQLSTATE: 22008

Description: The date does not have a start or end.

Solution: Change the date to a valid value.

GAUSS-03249: "date out of range for timestamp"

SQLSTATE: 22008

Description: The date exceeds the timestamp range.

Solution: Change the date to a valid value.

GAUSS-03250: "cannot convert reserved abstime value to date"

SQLSTATE: 0A000

Description: The absolute time cannot be converted to the required time format.

Solution: Change the date format to a valid value.

<br/>

## GAUSS-03251 - GAUSS-03260

<br/>

GAUSS-03251: "time out of range"

SQLSTATE: 22008

Description: Out of range.

Solution: Check the original data.

GAUSS-03252: "'time' units '%s' not recognized"

SQLSTATE: 22023

Description: The SQL parameter is incorrect.

Solution: Modify the SQL statement.

GAUSS-03253: "time zone displacement out of range"

SQLSTATE: 22009

Description: Out of range.

Solution: Check the original data.

GAUSS-03254: "'time with time zone' units '%s' not recognized"

SQLSTATE: 22023

Description: The SQL parameter is incorrect.

Solution: Modify the SQL statement.

GAUSS-03255: "'interval' time zone '%s' not valid"

SQLSTATE: 22023

Description: Data is invalid.

Solution: Check the original data.

GAUSS-03256: "compressed data is corrupt"

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03257: "value too long for type character(%d)"

SQLSTATE: 22001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03258: "value too long for type character varying(%d)"

SQLSTATE: 22001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03259: "value too long for type nvarchar2(%d)"

SQLSTATE: 22001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03260: "SPI_prepare failed for '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03261 - GAUSS-03270

<br/>

GAUSS-03261: "failed to get pg_rewrite tuple for rule %u"

SQLSTATE: SP005

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03262: "failed to get pg_rewrite tuple for view %u"

SQLSTATE: SP005

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03264: "tgargs is null for trigger %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03265: "unrecognized confmatchtype: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03266: "unrecognized confupdtype: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03267: "unrecognized confdeltype: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03268: "invalid constraint type '%c'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03269: "Deparse of this query at planning is not supported yet"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03270: "Deparse of this query at planning not supported yet"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03271 - GAUSS-03280

<br/>

GAUSS-03271: "rule '%s' has unsupported event type %d"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03272: "unrecognized query command type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03273: "too many subquery RTEs in INSERT"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03274: "too many values RTEs in INSERT"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03275: "both subquery and values RTEs in INSERT"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03278: "Invalid distribution type"

SQLSTATE: 42000

Description: The system does not support the specified distribution type.

Solution: Ensure that the distribution type in use is supported by the system.

GAUSS-03279: "unexpected utility statement type"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03280: "bogus varlevelsup: %d offset %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03281 - GAUSS-03290

<br/>

GAUSS-03281: "bogus varattno for OUTER_VAR var: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03282: "bogus varattno for INNER_VAR var: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03283: "bogus varattno for INDEX_VAR var: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03284: "bogus varno: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03285: "bogus varattno for subquery var: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03286: "bogus varattno for remotequery var: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03287: "cannot decompile join alias var in plan tree"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03288: "failed to find plan for subquery %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03289: "failed to find plan for CTE %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03290: "bogus oprkind: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03291 - GAUSS-03300

<br/>

GAUSS-03291: "could not find window clause for winref %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03292: "unrecognized sublink type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03293: "argument type %s of FieldStore is not a tuple type"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03294: "unrecognized oprkind: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03295: "date/time field value out of range: '%s'"

SQLSTATE: 22008

Description: The value of the **date/time** field is out of range. For example, the month value should range from 1 to 12.

Solution: Correct the input value.

GAUSS-03296: "interval field value out of range: '%s'"

SQLSTATE: 22015

Description: The value of the **interval** field is out of range.

Solution: Correct the input value.

GAUSS-03297: "time zone displacement out of range: '%s'"

SQLSTATE: 22009

Description: The value of the **time zone displacement** field is out of range.

Solution: Correct the input value.

GAUSS-03298: "invalid input syntax for type %s: '%s'"

SQLSTATE: 22007

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-03299: "only system admin can lock the cluster for backup"

SQLSTATE: 42501

Description: Only system administrators have the permission to lock the cluster for backup.

Solution: Perform this operation as a system administrator.

GAUSS-03300: "internal error while locking the cluster for backup"

SQLSTATE: 08000

Description: An error occurs when the cluster is locked for backup.

Solution:Contact technical support.
