---
title: GAUSS-01401 - GAUSS-01500
summary: GAUSS-01401 - GAUSS-01500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01401 - GAUSS-01500

<br/>

## GAUSS-01401 - GAUSS-01410

<br/>

GAUSS-01401: "permission denied: '%s' is a system trigger"

SQLSTATE: 42501

Description: A non-administrator user enables or disables system triggers.

Solution: Enable or disable system triggers as a system administrator.

GAUSS-01402: "tgargs is null in trigger for relation '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01403: "trigger function %u returned null value"

SQLSTATE: 39P01

Description: Internal system error. The trigger function returns null values.

Solution:Contact technical support.

GAUSS-01404: "BEFORE STATEMENT trigger cannot return a value"

SQLSTATE: 39P01

Description: The BEFORE STATEMENT trigger returns a value.

Solution: Ensure that the trigger function returns no value.

GAUSS-01405: "could not serialize access due to concurrent update"

SQLSTATE: 40001

Description: Transactions cannot be serialized due to concurrent update.

Solution: Retry the failed transaction.

GAUSS-01406: "unrecognized heap_lock_tuple status: %u"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01407: "could not find trigger %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01408: "failed to fetch tuple1 for AFTER trigger"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01409: "failed to fetch tuple2 for AFTER trigger"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01410: "relation %u has no triggers"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01411 - GAUSS-01420

<br/>

GAUSS-01411: "cross-database references are not implemented: '%s.%s.%s'"

SQLSTATE: 0A000

Description: The **SET CONSTRAINT** statement is executed in the current database to set a constraint for another database.

Solution: Do not set a constraint for any other database.

GAUSS-01412: "constraint '%s' is not deferrable"

SQLSTATE: 42809

Description: When the **SET CONSTRAINT** statement is executed, the constraint is deferrable.

Solution: Do not set a non-deferrable constraint to a deferrable one.

GAUSS-01413: "constraint '%s' does not exist"

SQLSTATE: 42704

Description: When the **SET CONSTRAINT** statement is executed, the constraint does not exist.

Solution: Specify the constraint in the **SET CONSTRAINT** statement.

GAUSS-01414: "no triggers found for constraint with OID %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01415: "AfterTriggerSaveEvent() called outside of transaction"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01416: "AfterTriggerSaveEvent() called outside of query"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01418: "XC: Could not find the required row position %d for AFTER ROW trigger"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01419: "XC: Could not find the required row position %d forAFTER ROW trigger"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01421 - GAUSS-01430

<br/>

GAUSS-01421: "could not obtain lock on relation '%s'"

SQLSTATE: 55P03

Description: The statement is **LOCK TABLE NOWAIT**. The table cannot obtain a lock.

Solution: Modify the statement to **LOCK TABLE**.

GAUSS-01422: "cache lookup failed for access method %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01423: "operator family '%s' does not exist for access method '%s'"

SQLSTATE: 42704

Description: During **ALTER OPERATOR FAMILY** execution, the access method specified for the operator family does not exist.

Solution: View the existing operator family and ensure that the access method and the specified operator family match.

GAUSS-01424: "operator family '%s' for access method '%s' already exists"

SQLSTATE: 42710

Description: During **CREATE OPERATOR FAMILY** execution, the operator family already exists.

Solution: Ensure that the operator family name is unique.

GAUSS-01425: "must be system admin to create an operator class"

SQLSTATE: 42501

Description: A non-administrator user executes the **CREATE OPERATOR FAMILY** statement.

Solution: Execute the statement as a system administrator.

GAUSS-01426: "invalid operator number %d, must be between 1 and %d"

SQLSTATE: 42P17

Description:**strategy_number** in the **OPERATOR** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement is less than 1 or greater than that of **index_method**.

Solution: Ensure that **strategy_number** ranges from 1 to the maximum **strategy_number** of **index_method**.

GAUSS-01427: "invalid procedure number %d, must be between 1 and %d"

SQLSTATE: 42P17

Description:**support_number** in the **FUNCTION** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement is less than 1 or greater than that of **index_method**.

Solution: Ensure that **support_number** ranges from 1 to the maximum **strategy_number** of **index_method**.

GAUSS-01428: "storage type specified more than once"

SQLSTATE: 42P17

Description: The **CREATE OPERATOR CLASS** statement contains multiple and same **STORAGE** clauses.

Solution: Ensure that **STORAGE** clauses contained in the **CREATE OPERATOR CLASS** are different.

GAUSS-01429: "unrecognized item type: %d"

SQLSTATE: XX000

Description: The system does not support customized operators.

Solution: Do not create customized operators using the **CREATE OPERATOR** syntax.

GAUSS-01430: "storage type cannot be different from data type for access method '%s'"

SQLSTATE: 42P17

Description:**column_type** differs from **storage_type** specified in the **STORAGE** clause of the **CREATE OPERATOR CLASS** statement.

Solution: Ensure that the data type of **storage_type** and that of **access_method** are the same.

<br/>

## GAUSS-01431 - GAUSS-01440

<br/>

GAUSS-01431: "operator class '%s' for access method '%s' already exists"

SQLSTATE: 42710

Description: During **CREATE OPERATOR CLASS** execution, the defined operator class already exists.

Solution: Ensure that the operator class name is unique.

GAUSS-01432: "could not make operator class '%s' be default for type %s"

SQLSTATE: 42710

Description: During **CREATE OPERATOR CLASS** execution, the default operator class has been created for a data type.

Solution: Ensure that no default operator class has been created for a data type.

GAUSS-01433: "must be system admin to create an operator family"

SQLSTATE: 42501

Description: A non-administrator user executes the **CREATE OPERATOR FAMILY** statement.

Solution: Execute the statement as a system administrator.

GAUSS-01434: "must be system admin to alter an operator family"

SQLSTATE: 42501

Description: A non-administrator user executes the **ADD** or **DROP** clause of the **ALTER OPERATOR FAMILY** statement.

Solution: Execute the clause as a system administrator.

GAUSS-01435: "operator argument types must be specified in ALTER OPERATOR FAMILY"

SQLSTATE: 42601

Description: In the **ADD OPERATOR** clause of the **ALTER OPERATOR FAMILY** statement, the operator parameter type is not specified.

Solution: Specify the operator parameter type.

GAUSS-01436: "STORAGE cannot be specified in ALTER OPERATOR FAMILY"

SQLSTATE: 42601

Description: In the **ADD** clause of the **ALTER OPERATOR FAMILY … ADD** statement, a **STORAGE** clause is specified.

Solution: Do not specify a **STORAGE** clause.

GAUSS-01437: "one or two argument types must be specified"

SQLSTATE: 42601

Description: In the **ADD** or **DROP** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, more than two operators or functions are specified.

Solution: Specify only one or two operators or functions.

GAUSS-01438: "index operators must be binary"

SQLSTATE: 42P17

Description: In the **ADD** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the specified operator is not a binary operator.

Solution: The specified operator must be a binary operator.

GAUSS-01439: "access method '%s' does not support ordering operators"

SQLSTATE: 42P17

Description: In the **ADD** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, a sort operator is specified, but **access_method** does not support sorting.

Solution: Ensure that **access_method** supports sorting so that operators can be ordered.

GAUSS-01440: "index search operators must return Boolean"

SQLSTATE: 42P17

Description: In the **ADD OPERATOR…FOR SEARCH** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the returned operator value is not a Boolean type.

Solution: Ensure that the returned operator value is a Boolean type.

<br/>

## GAUSS-01441 - GAUSS-01450

<br/>

GAUSS-01441: "btree comparison procedures must have two arguments"

SQLSTATE: 42P17

Description: In the **ADD FUNCTION** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the value of **access_method** is B-tree, and a B-tree comparison function is specified, but the number of parameters is not two.

Solution: If a B-tree comparison function is specified, ensure that the number of parameters must be two.

GAUSS-01442: "btree comparison procedures must return integer"

SQLSTATE: 42P17

Description: In the **ADD FUNCTION** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the value of **access_method** is B-tree, and a B-tree comparison function is specified, but the return value type is not int.

Solution: If a B-tree comparison function is specified, ensure that the return value must be of the int data type.

GAUSS-01443: "btree sort support procedures must accept type 'internal'"

SQLSTATE: 42P17

Description: In the **ADD FUNCTION** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the value of **access_method** is B-tree, and a B-tree sort support function is specified, but the return value is not internal.

Solution: If a B-tree sort support function is specified, ensure that the return value must be of the internal type.

GAUSS-01444: "btree sort support procedures must return void"

SQLSTATE: 42P17

Description: In the **ADD FUNCTION** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the value of **access_method** is B-tree, and a B-tree sort support function is specified, but the return value is not void.

Solution: If a B-tree sort support function is specified, ensure that the return value must be of the void type.

GAUSS-01446: "hash procedures must return integer"

SQLSTATE: 42P17

Description: In the ADD FUNCTION clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the value of **access_method** is **hash**, but the return value type is of the int type.

Solution: If **access_method** is a hash type, the return value type must be of the int type.

GAUSS-01447: "associated data types must be specified for index support procedure"

SQLSTATE: 42P17

Description: FOR TYPE data_type is not specified in the **CREATE OPERATOR CLASS** statement. Left and right parameter types are not specified in the **ADD OPERATOR** clause of the **ALTER OPERATOR FAMILY** statement.

Solution: FOR TYPE data_type must be specified. Left and right parameter types must be specified.

GAUSS-01448: "procedure number %d for (%s,%s) appears more than once"

SQLSTATE: 42P17

Description: In the **ADD** or **DROP** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the same function appears multiple times.

Solution: Ensure that a function does not repeatedly appear in an SQL statement.

GAUSS-01449: "operator number %d for (%s,%s) appears more than once"

SQLSTATE: 42P17

Description: In the **ADD** or **DROP** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the same operator appears multiple times.

Solution: Ensure that an operator does not repeatedly appear in an SQL statement.

GAUSS-01450: "operator %d(%s,%s) already exists in operator family '%s'"

SQLSTATE: 42710

Description: In the **ADD** or **DROP** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the operator to be added already exists.

Solution: Ensure that the operator to be added does not exist in the operator family.

<br/>

## GAUSS-01451 - GAUSS-01460

<br/>

GAUSS-01451: "function %d(%s,%s) already exists in operator family '%s'"

SQLSTATE: 42710

Description: In the **ADD** or **DROP** clause of the **CREATE OPERATOR CLASS** or **ALTER OPERATOR FAMILY** statement, the function to be added already exists.

Solution: Ensure that the function to be added does not exist in the operator family.

GAUSS-01452: "operator %d(%s,%s) does not exist in operator family '%s'"

SQLSTATE: 42704

Description: In the **DROP** clause of the **ALTER OPERATOR FAMILY** statement, the specified operator does not exist.

Solution: Ensure that the operator to be dropped exists in the operator family.

GAUSS-01453: "function %d(%s,%s) does not exist in operator family '%s'"

SQLSTATE: 42704

Description: In the **DROP** clause of the **ALTER OPERATOR FAMILY** statement, the specified function does not exist.

Solution: Ensure that the function to be dropped exists in the operator family.

GAUSS-01454: "could not find tuple for amop entry %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01455: "could not find tuple for amproc entry %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01456: "operator class '%s' for access method '%s' already exists in schema '%s'"

SQLSTATE: 42710

Description: A conflict occurs when changing the operator class name.

Solution: Rename the operator class name.

GAUSS-01457: "operator family '%s' for access method '%s' already exists in schema '%s'"

SQLSTATE: 42710

A conflict occurs when changing the operator family name.

Solution: Rename the operator family name.

GAUSS-01458: "SETOF type not allowed for operator argument"

SQLSTATE: 42P13

Description: In the **CREATE OPERATOR** statement, the type of the left or right parameter is **SETOF** (set).

Solution: Ensure that left and right parameters do not belong to the **SETOF** type (set).

GAUSS-01459: "operator procedure must be specified"

SQLSTATE: 42P13

Description: In **CREATE OPERATOR**, no procedure is specified.

Solution: Specify a procedure in the **CREATE OPERATOR** statement.

GAUSS-01460: "at least one of leftarg or rightarg must be specified"

SQLSTATE: 42P13

Description: In the **CREATE OPERATOR** statement, **LEFTARG** and **RIGHTARG** are not specified.

Solution: Specify either **LEFTARG** or **RIGHTARG** or both of them in the **CREATE OPERATOR** statement.

<br/>

## GAUSS-01461 - GAUSS-01470

<br/>

GAUSS-01461: "restriction estimator function %s must return type 'float8'"

SQLSTATE: 42P17

Description: During **CREATE OPERATOR** execution, the value returned by the specified restriction function is not of the float8 type.

Solution: Ensure that the return value is of the float8 type.

GAUSS-01462: "join estimator function %s must return type 'float8'"

SQLSTATE: 42P17

Description: During **CREATE OPERATOR** execution, the value returned by the specified join function is not of the float8 type.

Solution: Ensure that the return value is of the float8 type.

GAUSS-01464: "invalid cursor name: must not be empty"

SQLSTATE: 34000

Description: During **DECLARE**, **CLOSE**, **FETCH**, or **MOVE CURSOR** execution, the cursor name is not specified.

Solution: Specify the cursor name.

GAUSS-01465: "cursor '%s' does not exist"

SQLSTATE: 34000

Description: During **FETCH**, **MOVE**, **CLOSE** **CURSOR**, **CURRENT OF**, or **XML CURSOR** execution, the specified cursor name does not exist.

Solution: Ensure that the specified cursor name exists.

GAUSS-01466: "portal '%s' cannot be run"

SQLSTATE: 55000

Description: Internal error. The portal status is abnormal.

Solution:Contact technical support.

GAUSS-01467: "could not reposition held cursor"

SQLSTATE: 55000

Description: Internal error. The portal status is abnormal.

Solution:Contact technical support.

GAUSS-01468: "unexpected end of tuple stream"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01469: "unrecognized rename stmt type: %d"

SQLSTATE: XX000

Description: The alter object type in the **ALTER..RENAME TO..** syntax is incorrect.

Solution: Correct the alter object type by referring to the *SQL Reference*.

GAUSS-01470: "unrecognized AlterObjectSchemaStmt type: %d"

SQLSTATE: XX000

Description: The alter object type in the **ALTER..SET SCHEMA..** syntax is incorrect.

Solution: Correct the alter object type by referring to the *SQL Reference*.

<br/>

## GAUSS-01471 - GAUSS-01480

<br/>

GAUSS-01471: "cache lookup failed for object %u of catalog '%s'"

SQLSTATE: XX000

Description: During **ALTER..SET TABLESPACE..** execution, alter objects fail to be obtained from the cache.

Solution: The system cache is abnormal. Contact technical support.

GAUSS-01472: "must be system admin to set schema of %s"

SQLSTATE: 42501

Description: A non-administrator user modifies the object **namespace**.

Solution: Modify the object as a system administrator.

GAUSS-01473: "%s already exists in schema '%s'"

SQLSTATE: 42710

Description: The object already exists in the new schema when the user is to modify the object **namespace**.

Solution: Ensure that the object does not exist in the new schema if the user is to modify the object **namespace**.

GAUSS-01474: "unrecognized AlterOwnerStmt type: %d"

SQLSTATE: XX000

Description: The alter object type in the **ALTER..OWNER TO..** syntax is incorrect.

Solution: Correct the alter object type by referring to the *SQL Reference*.

GAUSS-01475: "Table %s does not support sampling operation for analyze."

SQLSTATE: 42703

Description: GDS foreign tables do not support sample analysis.

Solution: Do not perform sample analysis.

GAUSS-01476: "unsupported datatype"

SQLSTATE: XX000

Description: The analyzed column-store table contains unsupported data types.

Solution: Check whether the column-store table is defined properly. If not, create a column-store table again. Check whether this error persists. Contact technical support.

GAUSS-01477: "extension '%s' does not exist"

SQLSTATE: 42704

Description: During **CREATE EXTENSION** or **ALTER EXTENSION** execution, the extension does not exist.

Solution: Ensure that the specified extension exists.

GAUSS-01478: "invalid extension name: '%s'"

SQLSTATE: 22023

During **CREATE EXTENSION** execution, the extension name is invalid.

Solution: Ensure that the extension name cannot be empty, contain "-", start or end with "-", or contain separators, such as / and \\.

GAUSS-01479: "invalid extension version name: '%s'"

SQLSTATE: 22023

Description: During **CREATE EXTENSION** execution, the version name is invalid. During **ALTER EXTENSION** execution, the new version or default version is invalid.

Solution: Ensure that the version, new version, and default_version cannot be empty, contain "-", begin or end with "-", or contain separators, such as / and \\.

<br/>

## GAUSS-01481 - GAUSS-01490

<br/>

GAUSS-01481: "parameter '%s' cannot be set in a secondary extension control file"

SQLSTATE: 42601

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension-related functions, **default_version** or **directory** is specified in the control file.

Solution: Do not specify **default_version** or **directory** in the control file.

GAUSS-01482: "parameter '%s' requires a Boolean value"

SQLSTATE: 22023

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension related functions, **relocatable** or **sysadmin** lacks a Boolean value in the control file.

Solution: Assign a Boolean value to **relocatable** or **sysadmin** in the control file.

GAUSS-01483: "'%s' is not a valid encoding name"

SQLSTATE: 42704

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension related functions, **encoding** lacks a valid name in the control file.

Solution: Specify a valid name to **encoding** in the control file.

GAUSS-01484: "parameter '%s' must be a list of extension names"

SQLSTATE: 22023

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension-related functions, **requires** lacks a list of extension names in the control file.

Solution: Specify a list of extension names to **requires** in the control file.

GAUSS-01485: "unrecognized parameter '%s' in file '%s'"

SQLSTATE: 42601

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension -related functions, an invalid parameter is specified in the control file.

Solution: Specify only the following parameters: directory/default_version/module_pathname/comment/schema/relocatable/sysadmin/encoding/requires

GAUSS-01486: "parameter 'schema' cannot be specified when 'relocatable' is true"

SQLSTATE: 42601

Description: During the execution of **CREATE EXTENSION**, **ALTER EXTENSION**, or other extension-related functions, **relocatable** is set to true and **schema** is also specified in the control file.

Solution: Do not specify **schema** if **relocatable** is true in the control file.

GAUSS-01487: "transaction control statements are not allowed within an extension script"

SQLSTATE: 0A000

Description: During **CREATE EXTENSION** or **ALTER EXTENSION** execution, SQL script files contain transaction control SQL statements.

Solution: Do not use transaction control SQL statements in SQL script files.

GAUSS-01488: "permission denied to create extension '%s'"

SQLSTATE: 42501

Description: During **CREATE EXTENSION** execution, the extension control file is run with system administrator rights. However, the user does not run the file as a system administrator.

Solution: Ensure that the file is run by a system administrator.

GAUSS-01489: "permission denied to update extension '%s'"

SQLSTATE: 42501

Description: During **ALTER EXTENSION** execution, the extension control file is run with system administrator rights. However, the user does not run the file as a system administrator.

Solution: Ensure that the file is run by a system administrator.

GAUSS-01490: "extension '%s' has no update path from version '%s' to version '%s'"

SQLSTATE: 22023

Description: During **CREATE EXTENSION** or **ALTER EXTENSION** execution, the updated path does not exist.

Solution: Ensure that the name and path of the SQL files are correct if **FROM** *old_version* and **UPDATE TO** *new_version* are contained in the statement.

<br/>

## GAUSS-01491 - GAUSS-01500

<br/>

GAUSS-01492: "nested CREATE EXTENSION is not supported"

SQLSTATE: 0A000

Description: During **CREATE EXTENSION** execution, a nested extension exist in the SQL statement.

Solution: Do not create a nested extension in the SQL statement.

GAUSS-01493: "unrecognized option: %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01494: "version to install must be specified"

SQLSTATE: 22023

Description: During **CREATE/ALTER EXTENSION** execution, **new version** is not specified in the statement, and **default version** is not specified in the control file.

Solution: Specify **new version** in the statement or **default version** in the control file.

GAUSS-01495: "FROM version must be different from installation target version '%s'"

SQLSTATE: 22023

Description: During **CREATE EXTENSION** execution, the from version number and specified version number are the same.

Solution: Ensure that the from version number and specified version number are different.

GAUSS-01496: "extension '%s' must be installed in schema '%s'"

SQLSTATE: 0A000

Description: During **CREATE EXTENSION** execution, the schema specified in the **WITH SCHEMA** statement is different from that specified in the control file.

Solution: Ensure that the schema specified in the **WITH SCHEMA** clause is the same as that specified in the control file.

GAUSS-01497: "there is no default creation target"

SQLSTATE: XX000

Description:**SCHEMA** is not specified in the **CREATE EXTENSION** statement, and the default **SCHEMA** is used, which is empty.

Solution: The default **SCHEMA** is empty, the current system is abnormal, or the current directory of **SCHEMA** is deleted manually. Contact technical support.

GAUSS-01498: "required extension '%s' is not installed"

SQLSTATE: 42704

Description: During **CREATE EXTENSION** or **ALTER EXTENSION** execution, a required extension as specified in the control file is not installed.

Solution: Ensure that all extensions specified in the control file are installed and check them using **\dx**.

GAUSS-01499: "cannot drop extension '%s' because it is being modified"

SQLSTATE: 55000

Description: During **DROP EXTENSION** execution, an extension is being modified by another session.

Solution: Ensure that extensions cannot be modified by other sessions.

GAUSS-01500: "pg_extension_config_dump() can only be called from an SQL script executed by CREATE EXTENSION"

SQLSTATE: 0A000

Description: During **CREATE EXTENSION** execution, **pg_extension_config_dump** is not executed.

Solution: Execute **pg_extension_config_dump** in the **CREATE EXTENSION** statement.
