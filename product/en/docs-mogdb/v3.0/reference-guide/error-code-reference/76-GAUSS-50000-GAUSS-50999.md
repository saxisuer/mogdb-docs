---
title: GAUSS-50000 - GAUSS-50999
summary: GAUSS-50000 - GAUSS-50999
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-50000 - GAUSS-50999

<br/>

## GAUSS-50000 - GAUSS-50099

<br/>

GAUSS-50000: "Unrecognized parameter: %s."

SQLSTATE: None

Description: Failed to recognize the parameter.

Solution: Check the parameters involved in the error information. An error is reported if the parameters are not those specified in the script to be executed.

GAUSS-50001: "Incorrect parameter. Parameter '-%s' is required"

SQLSTATE: None

Description: The required parameters are missing.

Solution: Check the parameters involved in the error information. An error is reported if the user does not specify a parameter that must be specified in the script to be executed.

GAUSS-50002: "Incorrect parameter. Parameter '-%s' is not required"

SQLSTATE: None

Description: This parameter is unnecessary.

Solution: Check the parameters involved in the error information. An error is reported if the user specifies this parameter, which is not required in the script to be executed.

GAUSS-50003: "The parameter '-%s' type should be %s."

SQLSTATE: None

Description: The parameter type is incorrect.

Solution: Check whether the parameter type in the error information is the specified type.

GAUSS-50004: "The parameter '-%s' value is incorrect."

SQLSTATE: None

Description: The parameter value is incorrect.

Solution: Check whether the parameter value in the error information is correct.

GAUSS-50005: "The parameter '-%s' and '-%s' can not be used together."

SQLSTATE: None

Description: Parameter **A** and parameter **B** cannot be specified simultaneously.

Solution: Check the two parameters involved and the script scenarios to be performed in the error information. Specify only one parameter in the scenarios.

GAUSS-50006: "Too many command-line arguments (first is \"%s\")."

SQLSTATE: None

Description: The number of specified parameters exceeds the required quantity.

Solution: Check whether the number of parameters involved in the error information meets the requirements.

GAUSS-50007: "Failed to set %s parameter."

SQLSTATE: None

Description: GUC parameter settings fail.

Solution: Check whether the parameter settings in **gs_guc** meet the tool requirements.

GAUSS-50008: "Failed to reload parameter."

SQLSTATE: None

Description: Failed to load the parameter.

Solution: Check whether command for loading the parameter is correct.

GAUSS-50009: "Parameter format error."

SQLSTATE: None

Description: The parameter format is incorrect.

Solution: Check whether the parameter format in the command is correct.

GAUSS-50010: "Failed to check %s parameter."

SQLSTATE: None

Description: Failed to check the kernel parameter.

Solution: Check whether the value of the kernel parameter **shared_buffers** or **max_connections** is correct.

GAUSS-50011: "The parameter[%s] value[%s] is invalid."

SQLSTATE: None

Description: The parameter value is invalid.

Solution: Enter the correct parameter.

GAUSS-50012: "The parameter '%s' value can't be empty."

SQLSTATE: None

Description: The parameter cannot be empty.

Solution: Check whether the parameter is empty.

GAUSS-50013: "The parameter '%s' have not been initialized."

SQLSTATE: None

Description: The parameters have not been initialized.

Solution: Check the code logic and initialize the specified parameters.

GAUSS-50014: "Parameters of '%s' can not be empty."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50015: "The command line parser error: %s."

SQLSTATE: None

Description: An error occurs when the command line parser is executed.

Solution: Correct the command line input parameters based on the error information.

GAUSS-50016: "The re-entrant parameter '-%s' is not same with the previous command."

SQLSTATE: None

Description: The re-entrant parameters are different from the previous ones and the re-entry operation fails.

Solution: Enter the same parameters as those in the last failed operation, or perform rollback before performing a new operation.

GAUSS-50017: "Incorrect value '%s' specified by the parameter '-%s'."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50018: "The·nodename·%s·upgrade·failed,·please·warm-standby·at·first·for·this·node."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-50100 - GAUSS-50199

<br/>

GAUSS-50100: "The %s is not readable for %s."

SQLSTATE: None

Description: The user has no read permission on this file.

Solution: Check whether the user is correct or whether the user is granted a matching read permission.

GAUSS-50101: "The %s is not executable for %s."

SQLSTATE: None

Description: The user has no execution permission on this file.

Solution: Check whether the user is correct or whether the user is granted a matching execution permission.

GAUSS-50102: "The %s is not writable for %s."

SQLSTATE: None

Description: The user has no write permission on this file.

Solution: Check whether the user is correct or whether the user is granted a matching write permission.

GAUSS-50103: "The %s has unexpected rights."

SQLSTATE: None

Description: The permissions are unauthorized.

Solution: Grant the user with matching permissions.

GAUSS-50104: "Only a user with the root permission can run this script."

SQLSTATE: None

Description: Only the user **root** can execute this script.

Solution: Execute this script as a user **root**.

GAUSS-50105: "Cannot run this script as a user with the root permission."

SQLSTATE: None

Description: This script cannot be executed by the user **root**.

Solution: Execute this script as a non-**root** user.

GAUSS-50106: "Failed to change the owner of %s."

SQLSTATE: None

Description: Failed to change the owner of the file or directory.

Solution: Check whether the permissions on the file or directory of the user or the group are correctly granted.

GAUSS-50107: "Failed to change the permission of %s."

SQLSTATE: None

Description: Failed to change the permission of the file or directory.

Solution: Check whether the user who modifies the directory or file is the user **root**, or check whether the permission granted to the directory or file is correct.

GAUSS-50108: "Failed to change the owner and permission of %s."

SQLSTATE: None

Description: Failed to change the owner and permission of the file or directory.

Solution: Check whether the owner of the file or directory exists, and whether the user or permission value of the file or directory is correct.

GAUSS-50109: "Only a user with the root permission can check SSD information."

SQLSTATE: None

Description: Only the user **root** can check the SSD information.

Solution: Check whether the current user has the **root** permission.

GAUSS-50110: "Cannot execute this script on %s."

SQLSTATE: None

Description: The script cannot be executed on a node.

Solution: Do not execute the **gs_replace** script on the node where the warm backup or node replacement is performed.

<br/>

## GAUSS-50200 - GAUSS-50299

<br/>

GAUSS-50200: "The %s already exists."

SQLSTATE: None

Description: The file or directory exists.

Solution: Delete this file or directory.

GAUSS-50201: "The %s does not exist."

SQLSTATE: None

Description: The file or directory does not exist.

Solution: Check why the file or directory does not exist, create a file or directory, or execute the script that creates this file or directory.

GAUSS-50202: "The %s must be empty."

SQLSTATE: None

Description: The directory is not empty.

Solution: Check the directory and leave it empty after backing it up.

GAUSS-50203: "The %s cannot be empty."

SQLSTATE: None

Description: The directory or file cannot be empty.

Solution: Check why the directory or file is empty, create a directory or file, or execute the script that creates this directory.

GAUSS-50204: "Failed to read %s."

SQLSTATE: None

Description: Failed to read the file.

Solution: Check and modify the file and user permissions.

GAUSS-50205: "Failed to write %s."

SQLSTATE: None

Description: Failed to write the file.

Solution: Check and modify the file and user permissions.

GAUSS-50206: "Failed to create %s."

SQLSTATE: None

Description: Failed to create a file.

Solution: Check and modify the user permissions of this directory under this file path.

GAUSS-50207: "Failed to delete %s."

SQLSTATE: None

Description: Failed to delete the file.

Solution: Check and modify the user permissions of this directory under this file path.

GAUSS-50208: "Failed to create the %s directory."

SQLSTATE: None

Description: Failed to create the folder.

Solution: Check and modify the user permissions of this directory.

GAUSS-50209: "Failed to delete the %s directory."

SQLSTATE: None

Description: Failed to delete the folder.

Solution: Check and modify the user permissions of this directory.

GAUSS-50210: "The %s must be a file."

SQLSTATE: None

Description: The type is not a file type.

Solution: Ensure that the type is a file type.

GAUSS-50211: "The %s must be a directory."

SQLSTATE: None

Description: The type is not a directory type.

Solution: Ensure that the type is a directory type.

GAUSS-50212: "The suffix of the file '%s' should be '%s'."

SQLSTATE: None

Description: The file name extension is not a specified type.

Solution: Change the file name extension to a specified type.

GAUSS-50213: "The %s path must be an absolute path."

SQLSTATE: None

Description: The path is not an absolute path.

Solution: Change the path to an absolute path.

GAUSS-50214: "Failed to copy %s."

SQLSTATE: None

Description: Failed to copy the file.

Solution: Check whether the file to be copied exists, whether the target path exists, and whether the network is normal.

GAUSS-50215: "Failed to back up %s."

SQLSTATE: None

Description: Failed to back up the file or directory.

Solution: Check whether the file or directory to be backed up exists.

GAUSS-50216: "Failed to remote copy %s."

SQLSTATE: None

Description: Failed to copy the file or directory remotely.

Solution: Check whether the file or directory to be copied exists and whether the target path exists.

GAUSS-50217: "Failed to decompress %s."

SQLSTATE: None

Description: Failed to decompress the package.

Solution: Check whether the compressed package to be decompressed exists, whether the format is correct, and whether the package is broken.

GAUSS-50218: "Failed to rename %s."

SQLSTATE: None

Description: Failed to rename the file.

Solution: Check whether the file to be renamed exists and whether the name and extension of the renamed file are correct.

GAUSS-50219: "Failed to obtain %s."

SQLSTATE: None

Description: Failed to obtain the directory, file, or path.

Solution: Check whether the directory, file, or path exists.

GAUSS-50220: "Failed to restore %s."

SQLSTATE: None

Description: Failed to restore the directory, file, or path.

Solution: Check whether the directory, file, or path exists.

GAUSS-50221: "Failed to obtain file type."

SQLSTATE: None

Description: Failed to obtain the file type.

Solution: Check whether the command is correct. Check whether the specified file or path exists.

GAUSS-50222: "The content of file %s is not correct."

SQLSTATE: None

Description: The file content is incorrect.

Solution: Check whether the file content is correct.

GAUSS-50223: "Failed to update %s files."

SQLSTATE: None

Description: Failed to update (write) the file.

Solution: Check whether the file to be updated (written) exists and whether the content written into the file is correct.

GAUSS-50224: "The file name is incorrect."

SQLSTATE: None

Description: The file name is incorrect.

Solution: Check whether multiple extensions (".") exist in the file.

GAUSS-50225: "Failed to back up remotely."

SQLSTATE: None

Description: Failed to back up remotely.

Solution: Check whether the file or directory to be backed up exists and whether the remote target path exists.

GAUSS-50226: "Failed to restore remotely."

SQLSTATE: None

Description: Failed to restore remotely.

Solution: Check whether the running **restore** command is correct.

GAUSS-50227: "Failed to compress %s."

SQLSTATE: None

Description: Failed to compress the file or directory.

Solution: Check whether the file or directory to be compressed exists or whether the target path exists.

GAUSS-50228: "The %s does not exist or is empty."

SQLSTATE: None

Description: The file or directory does not exist or is empty.

Solution: Check whether the file or directory exists or is empty.

GAUSS-50229: "Cannot specify the file [%s] to the cluster path %s."

SQLSTATE: None

Description: A file cannot be specified to a path in a cluster.

Solution: Change the path for saving the file.

GAUSS-50230: "Failed to read/write %s."

SQLSTATE: None

Description: Failed to read or write the file.

Solution: Check whether the file exists and whether you have the read/write permission on the file.

GAUSS-50231: "Failed to generate %s file."

SQLSTATE: None

Description: Failed to generate the file.

Solution: Identify the cause of the file generation failure and try again.

GAUSS-50232: "The instance directory [%s] cannot set in app directory [%s].Please check the xml."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50233: "The directory name %s and %s cannot be same."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50234: "Cannot execute the script in the relevant path of the database."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-50300 - GAUSS-50399

<br/>

GAUSS-50300: "User %s does not exist."

SQLSTATE: None

Description: The user does not exist.

Solution: Check why the user does not exist, and then create a user.

GAUSS-50301: "The cluster user/group cannot be a root user/group."

SQLSTATE: None

Description: The user or group of the cluster user cannot be the user or group **root**.

Solution: Change the user or group of the cluster user.

GAUSS-50302: "The cluster user cannot be a user with the root permission."

SQLSTATE: None

Description: Cluster users cannot have the **root** permission.

Solution: Change the permission of a cluster user or switch to another cluster user who does not have the **root** permission.

GAUSS-50303: "Cannot install the program as a user with the root permission."

SQLSTATE: None

Description: This program cannot be installed by the user **root**.

Solution: Change the permission of the user or install the program as a non-**root** user.

GAUSS-50304: "The new user [%s] is not the same as the old user [%s]."

SQLSTATE: None

Description: The old and new users do not match.

Solution: Change the user to match the old user.

GAUSS-50305: "The user is not matched with the user group."

SQLSTATE: None

Description: The user and user group do not match.

Solution: Check and modify the user and user group to make them match.

GAUSS-50306: "The password of %s is incorrect."

SQLSTATE: None

Description: The password is incorrect.

Solution: Correct the password.

GAUSS-50307: "User password has expired."

SQLSTATE: None

Description: The user password has expired.

Solution: Check and modify the password.

GAUSS-50308: "Failed to obtain user information."

SQLSTATE: None

Description: Failed to obtain the user information.

Solution: Check whether the user information is correct.

GAUSS-50309: "Failed to obtain password change times of data base super user"

SQLSTATE: None

Description: Failed to obtain the number of password changes of the initial database user.

Solution: Check whether the password of the initial user has been changed. If not, change the password.

GAUSS-50310: "Failed to obtain password expiring days."

SQLSTATE: None

Description: Failed to obtain the password expiring date.

Solution:Contact technical support.

GAUSS-50311: "Failed to change password for %s."

SQLSTATE: None

Description: Failed to change the password for the user.

Solution: Check whether you have the read permission on the **/temp/temp.***[user]* file and whether the file content is correct.

GAUSS-50312: "There are other users in the group %s on %s, skip to delete group."

SQLSTATE: None

Description: Failed to delete the group because there are other users in the group.

Solution: Delete other users from the group and then delete the group.

GAUSS-50313: "Failed to delete %s group."

SQLSTATE: None

Description: Failed to delete the group.

Solution: Check whether the group to be deleted exists. Check whether the cluster status is normal.

GAUSS-50314: "Failed to delete %s user."

SQLSTATE: None

Description: Failed to delete the user.

Solution: Check whether the user to be deleted exists.

GAUSS-50315: "The user %s is not matched with the owner of %s."

SQLSTATE: None

Description: The user does not match the owner of the path.

Solution: Modify the owner of the path.

GAUSS-50316: "Group [%s] does not exist."

SQLSTATE: None

Description: The group does not exist.

Solution: Execute the pre-processing script again and modify the **-G** parameter.

GAUSS-50317: "Failed to check user and password."

SQLSTATE: None

Description: Failed to check the user and password.

Solution: Check whether the user and password on each node in the cluster are correct.

GAUSS-50318: "Failed to add %s user."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50319: "Failed to add %s group."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50320: "Failed to set '%s' to '%s' in /etc/ssh/sshd_config."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-50321: "Failed to get configuration of '%s' from /etc/ssh/sshd_config."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-50400 - GAUSS-50499

<br/>

GAUSS-50400: "The remaining space of device [%s] cannot be less than %s."

SQLSTATE: None

Description: The disk space cannot be less than the specified value.

Solution: Clear the disk space, expand the disk capacity, or replace the disk.

GAUSS-50401: "The usage of the device [%s] space cannot be greater than %s."

SQLSTATE: None

Description: The disk usage cannot be greater than the specified value.

Solution: Clear the disk space, expand the disk capacity, or replace the disk.

GAUSS-50402: "The usage of INODE cannot be greater than %s."

SQLSTATE: None

Description: The INODE usage cannot exceed the specified value.

Solution: Ensure that the INODE usage does not exceed the specified value.

GAUSS-50403: "The IO scheduling policy is incorrect."

SQLSTATE: None

Description: The I/O scheduling policy is abnormal.

Solution: Use **gs_checkos** to set this parameter. For detailed operations, see instructions provided for **gs_checkos**.

GAUSS-50404: "The XFS mount type must be %s."

SQLSTATE: None

Description: The XFS mount type must be the specified type.

Solution: Change the mount type of XFS to the specified type.

GAUSS-50405: "The pre-read block size must be 16384."

SQLSTATE: None

Description: The size of the pre-read block must be 16384.

Solution: Use **gs_checkos** to set this parameter. For detailed operations, see instructions provided for **gs_checkos**.

GAUSS-50406: "Failed to obtain disk read and write rates."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-50407: "Failed to clean shared semaphore."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-50408: "Failed to obtain disk read-ahead memory block."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-50409: "The remaining space of dns cannot support shrink."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-50410: "Failed to check if remaining space of dns support shrink."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-50411: "The remaining space cannot be less than %s."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-50500 - GAUSS-50599

<br/>

GAUSS-50500: "The SWAP partition is smaller than the actual memory."

SQLSTATE: None

Description: The swap partition size is smaller than the actual memory size.

Solution: Adjust the size of the swap partition to a value greater than the actual memory size.

GAUSS-50501: "Shared_buffers must be less than shmmax. Please check it."

SQLSTATE: None

Description: The value of **Shared_buffers** must be smaller than that of **shmmax**.

Solution: Set the value of **shmmax** on all the hosts in the **/etc/sysctl.conf** file of the cluster to be greater than that of **shmall**. Run the **/sbin/sysctl -p** command to make the parameter effective, and run the command again.

GAUSS-50502: "Failed to obtain %s information."

SQLSTATE: None

Description: Failed to obtain the swap partition or memory information.

Solution: Check whether the system is normal.

<br/>

## GAUSS-50600 - GAUSS-50699

<br/>

GAUSS-50600: "The IP address cannot be pinged, which is caused by network faults."

SQLSTATE: None

Description: Failed to ping the IP address.

Solution: Change the network settings so that the IP address can be pinged.

GAUSS-50601: "The port [%s] is occupied."

SQLSTATE: None

Description: The port is occupied.

Solution: Check whether the resources that occupy the port number are available. If the resources are idle, release the port number. Specify other port numbers.

GAUSS-50602: "Failed to bind network adapters."

SQLSTATE: None

Description: Failed to bind the NIC.

Solution:Contact technical support.

GAUSS-50603: "The IP address is invalid."

SQLSTATE: None

Description: The IP address is invalid.

Solution: Correct the IP address.

GAUSS-50604: "Failed to obtain network interface card of backIp(%s)."

SQLSTATE: None

Description: Failed to obtain the NIC.

Solution: Check whether the network is normal. Check whether the NIC is normal.

GAUSS-50605: "Failed to obtain back IP subnet mask."

SQLSTATE: None

Description: Failed to obtain the subnet mask.

Solution: Check whether the network is normal.

GAUSS-50606: "Back IP(s) do not have the same subnet mask."

SQLSTATE: None

Description: The alternate IP address cannot have the same subnet mask.

Solution: Set a subnet mask to the alternate IP address.

GAUSS-50607:"Failed to obtain configuring virtual IP line number position of network startup file."

SQLSTATE: None

Description: Failed to obtain the virtual IP address in the NIC startup file.

Solution: Manually check the NIC configuration file to see whether the file is damaged.

GAUSS-50608:"Failed to writing virtual IP setting cmds into init file."

SQLSTATE: None

Description: Failed to write the virtual IP configuration command in the initialization file.

Solution: Manually check the NIC configuration file to see whether the file is damaged.

GAUSS-50609:"Failed to check port: %s."

SQLSTATE: None

Description: Failed to check the port.

Solution: Ensure that the port is enabled and not occupied.

GAUSS-50610:"Failed to get the range of random port."

SQLSTATE: None

Description: Failed to obtain the port range.

Solution: Check the cause of the failure and obtain the range again.

GAUSS-50611: "Failed to obtain network card bonding information."

SQLSTATE: None

Description: Failed to obtain the NIC binding information.

Solution: Check whether the **/proc/net/bonding/**_NIC__ _*ID* file exists, whether the **BONDING_OPTS** (the Red Hat environment) or **BONDING_MODULE_OPTS** (the CentOS environment) strings exist.

GAUSS-50612: "Failed to obtain network card %s value."

SQLSTATE: None

Description: Failed to obtain the value of the NIC RTU/RX.

Solution: Check whether the network is normal and whether the NIC is normal.

GAUSS-50613: "Failed to set network card %s value."

SQLSTATE: None

Description: Failed to set the NIC information.

Solution: Check whether the network is normal and whether the NIC is normal.

GAUSS-50614: "Failed to check network information."

SQLSTATE: None

Description: Failed to check the NIC information.

Solution: Check whether **ifconfig** is available. If it is, query it again.

GAUSS-50615:"IP %s and IP %s are not in the same network segment."

SQLSTATE: None

Description: The network segments of the two IP addresses are different.

Solution: Set the network segments of the two IP addresses to the same network segment. Then, perform the operation again.

GAUSS-50616:"Failed to get network interface."

SQLSTATE: None

Description: Failed to obtain the network interface.

Solution: Ensure that the IP address exists, **ifconfig** is available, and IP information exists in the NIC configuration file. Then, perform the operation again.

GAUSS-50617: "The node of XML configure file has the same virtual IP."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-50700 - GAUSS-50799

<br/>

GAUSS-50700: "The firewall should be disabled."

SQLSTATE: None

Description: The firewall is not disabled.

Solution: Disable the firewall.

GAUSS-50701: "The firewall should be opened."

SQLSTATE: None

Description: The firewall is not enabled.

Solution: Enable the firewall.

<br/>

## GAUSS-50800 - GAUSS-50899

<br/>

GAUSS-50800: "Regular tasks are not started."

SQLSTATE: None

Description: The scheduled task is not started.

Solution: Start the scheduled task.

GAUSS-50801: "Failed to set up tasks."

SQLSTATE: None

Description: Failed to set the task.

Solution: Perform the operation again.

GAUSS-50802: "Failed to %s·service."

SQLSTATE: None

Description: Failed to restart the scheduled task service.

Solution: View the logs to identify the detailed error information.

GAUSS-50803: "Failed to check user cron."

SQLSTATE: None

Description: Failed to check the scheduled task.

Solution: Verify whether the cluster status is normal and whether the **/bin/om_monitor/[cron]** file exists.

<br/>

## GAUSS-50900 - GAUSS-50999

<br/>

GAUSS-50900: "The NTPD service is not installed."

SQLSTATE: None

Description: The clock services are not installed.

Solution: Install the clock services.

GAUSS-50901: "The NTPD service is not started."

SQLSTATE: None

Description: The clock services are not enabled.

Solution: Enable the clock services.

GAUSS-50902: "The system time is different."

SQLSTATE: None

Description: The system time is different from the actual time.

Solution: Correct the system time.
