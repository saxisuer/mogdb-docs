---
title: GAUSS-06501 - GAUSS-06600
summary: GAUSS-06501 - GAUSS-06600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-06501 - GAUSS-06600

<br/>

## GAUSS-06501 - GAUSS-06510

<br/>

GAUSS-06501: "canceling statement due to %s.%s"

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06502: "canceling statement due to %s"

SQLSTATE: None

Description: Internal system error.

Solution:Please contact engineer to support."

GAUSS-06503: "canceling data redistribution task"

SQLSTATE: 57014

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06504: "canceling statement due to failover, pending"

SQLSTATE: 08000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06505: "canceling snapshot task"

SQLSTATE: 57014

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06506: "Postgres-XC: must start as either a Coordinator (-coordinator) or Datanode (-datanode)\n"

SQLSTATE: 42601

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06507: "Cannot execute DDL in a transaction block when need reconnect pooler"

SQLSTATE: 25001

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06508: "invalid number of data nodes when initializing global node definition."

SQLSTATE: 22000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06509: "invalid frontend message type '%c'."

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06510: "query_string is NULL."

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06511 - GAUSS-06520

<br/>

GAUSS-06511: "Too long query_string."

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06512: "Invalid packet path, remoteConnType[%d], remote_host[%s], remote_port[%s]."

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06513: "Acceptter in pooler stateless resue mode reset connection params %d > sql[%d]."

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06514: "Too long schema_name."

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06515: "The overrideStack list has been reach the max length."

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06516: "Invalid message type %d for procedure overrideStack."

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06517: "invalid execute message"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06518: "Too long close_target."

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06519: "invalid describe message"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06520: "gtm mode unconsistency, remote mode is %s, local mode is %s."

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06521 - GAUSS-06530

<br/>

GAUSS-06521: "Received an invalid commit csn: %lu."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06522: "Need to set support_batch_bind=true if executing batch"

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06523: "Portal run not complete for one in Batch bind-execute: name %s, query %s"

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06524: "Batch param of distribute key only support const"

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06525: "Failed to get DataNode id for Batch bind-execute: name %s, query %s"

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06526: "unexpected batch_count %d get from inputmessage"

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06527: "unexpected maxlen %d "

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06528: "Batch bind-execute message with invalid batch count: %d"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06529: "Not support portal_name %s for Batch bind-execute."

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06530: "Batch bind-execute message with invalid parameter number: %d"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06531 - GAUSS-06540

<br/>

GAUSS-06531: "conflict stmt name in Batch bind-execute message: bind %s, describe %s"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06532: "conflict portal name in Batch bind-execute message: bind %s, describe %s"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06533: "invalid DESCRIBE message subtype in Batch bind-execute message: %d"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06534: "conflict portal name in Batch bind-execute message: bind %s, execute %s"

SQLSTATE: 08P01

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06535: "Not support max_row in Batch bind-execute message: %d"

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06536: "invalid value in Batch bind-execute message: %d"

SQLSTATE: 08P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06537: "No thread pool worker left while waiting for session close. This is a very rare case when all thread pool workers happen to encounter FATAL problems before session close."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06538: "epoll receive %d events which exceed the limitation %d"

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06539: "Exceed stream thread pool limitation %d in group %d"

SQLSTATE: 58000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06540: "Fail to find a free slot for stream"

SQLSTATE: 53000

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06541 - GAUSS-06550

<br/>

GAUSS-06541: "Unexpected receive proc signal."

SQLSTATE: 08000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06542: "receive more connection message %d than expect %d"

SQLSTATE: 08P01

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06543: "undefined state %d for session attach"

SQLSTATE: 42P24

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06544: "memory usage reach the max_dynamic_memory"

SQLSTATE: YY006

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06545: "Invalid attribute for thread pool."

SQLSTATE: OP0A3

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06546: "Unsupported LLVM report type!"

SQLSTATE: 0A000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06547: "Cannot get the llvm::Intrinsic::smul_with_overflow function!"

SQLSTATE: CG002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06548: "Failed on getting IR function : LLVMIRbpcharne!\n"

SQLSTATE: CG001

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06549: "Cannot get the llvm::Intrinsic::sasub_with_overflow function!\n"

SQLSTATE: CG002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06550: "Failed on getting IR function : LLVMIRtextlike!\n"

SQLSTATE: CG001

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06551 - GAUSS-06560

<br/>

GAUSS-06551: "Failed on getting IR function : LLVMIRtextnlike!\n"

SQLSTATE: CG001

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06552: "Unexpected NULL right operator!"

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06553: "Failed to get function Intrinsic::prefetch!\n"

SQLSTATE: CG002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06154: "Abnormal data package received, package length is %d, input buffer length is %d"

SQLSTATE: None

Description: An abnormal data packet is received. The lengths of the data packet and the received buffer are displayed.

Solution: Ensure that the data file is correct.

GAUSS-06555: "Failed to get the const node of the operation!\n"

SQLSTATE: XX005

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06556: "Failed on getting IR function : LLVMIRbpchareq!"

SQLSTATE: CG001

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06557: "unrecognized booltexttype: %d"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06558: "Failed on getting IR function : LLVMIRmemcmp!\n"

SQLSTATE: CG001

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06559: "Codegen failed on the procedure of ExecVecTargetList!"

SQLSTATE: CG000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06560: "Not supported expr node %d yet!"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06561 - GAUSS-06570

<br/>

GAUSS-06561: "Unexpected batch information from ExprContext."

SQLSTATE: XX006

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06562: "Codegen keyMatch failed: unsupported data type!\n"

SQLSTATE: 42804

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06563: "Failed to get inner hash key!"

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06564: "Failed to get outer hash key!"

SQLSTATE: XX005

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06565: "Codegen keyMatch failed for %d-th clause of hash join!\n"

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06566: "Codegen fast keyMatch failed: unsupported data type!\n"

SQLSTATE: 42804

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06567: "Codegen buildHashTable failed: unsupported data type!\n"

SQLSTATE: 42804

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06568: "Codegen probeHashTable failed: unsupported data type!\n"

SQLSTATE: 42804

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06569: "Unsupported operation %u in FastAgg."

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06570: "Failed on generating HashBatchCodeGen!\n"

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06571 - GAUSS-06580

<br/>

GAUSS-06571: "Failed on generating MatchOneKey Function!\n"

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06572: "Unexpected NULL project information."

SQLSTATE: XX005

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06573: "Unsupported agg function %u!"

SQLSTATE: XX004

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06574: "Type %u is not supported yet in hashBatch"

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06575: "Type %u is not supported yet in match_key"

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06576: "Unexpected operation %u!"

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06577: "Cannot get the llvm::Intrinsic::sadd_with_overflow function!"

SQLSTATE: CG002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06578: "Invalid LLVM type %d"

SQLSTATE: 22023

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06579: "unrecognized node type: %d when rescan"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06580: "unrecognized node type: %d when restore scan position"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06581 - GAUSS-06590

<br/>

GAUSS-06581: "cache lookup failed for relation %u when check backward scan for Index."

SQLSTATE: 29P01

Description:Internal system error.

Solution: Contact technical support.

GAUSS-06582: "cache lookup failed for access method %u when test backward scan for Index %s"

SQLSTATE: 29P01

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06583: "cursor '%s' does not exist when executing Current Of Expr."

SQLSTATE: 34000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06584: "cursor '%s' is not positioned on a row when the cursor uses for UPDATE/SHARE"

SQLSTATE: 24000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06585: "cursor '%s' is not positioned on a row when the cursor doesn't use for UPDATE/SHARE"

SQLSTATE: 24000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06586: "could not find hash function for hash operator %u for TupleHashTable, column number %d, total column number %d."

SQLSTATE: 42883

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06587: "invalid node identifier for update/delete"

SQLSTATE: 42704

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06588: "unrecognized result from subplan for BitmapAnd."

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06589: "unrecognized result from subplan for BitmapHeapScan."

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06590: "prefetch and main iterators are out of sync for BitmapHeapScan."

SQLSTATE: 22000

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-06591 - GAUSS-06600

<br/>

GAUSS-06591: "can't initialize bitmap index scans using unusable index '%s'"

SQLSTATE: XX002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06592: "no local indexes found for partition %s BitmapIndexScan"

SQLSTATE: 42809

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06593: "can't initialize bitmap index scans using unusable local index '%s' for partition"

SQLSTATE: XX002

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06594: "unrecognized result from BitmapIndexScan subplan when execute BitmapOr"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06595: "unrecognized result from non-BitmapIndexScan subplan when execute BitmapOr"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06596: "ExtensibleNodeMethods '%s' was not registered"

SQLSTATE: 42704

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06597: "unrecognized join type: %d for hashjoin"

SQLSTATE: XX004

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06598: "could not write hashvalue %u to hash-join temporary file, written length %lu."

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06599: "could not write tuple to hash-join temporary file: written length %lu, tuple length %u"

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.

GAUSS-06600: "could not read from hash-join temporary file: read length %zu"

SQLSTATE: None

Description: Internal system error.

Solution: Contact technical support.
