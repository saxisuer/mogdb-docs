---
title: DCL Syntax Overview
summary: DCL Syntax Overview
author: Zhang Cuiping
date: 2021-05-17
---

# DCL Syntax Overview

Data control language (DCL) is used to create users and roles and set or modify database users or role rights.

## Defining a Role

A role is used to manage permissions. For database security, management and operation permissions can be granted to different roles. For details about related SQL statements, see [Table 1](#sqlstatement1).

**Table 1** SQL statements for defining a role<a id="sqlstatement1"></a>

| Description              | SQL Statement                                                |
| :----------------------- | :----------------------------------------------------------- |
| Creating a role          | [CREATE ROLE](../../reference-guide/sql-syntax/CREATE-ROLE.md) |
| Altering role attributes | [ALTER ROLE](../../reference-guide/sql-syntax/ALTER-ROLE.md) |
| Dropping a role          | [DROP ROLE](../../reference-guide/sql-syntax/DROP-ROLE.md)   |

## Defining a User

A user is used to log in to a database. Different permissions can be granted to users for managing data accesses and operations of the users. For details about related SQL statements, see [Table 2](#sqlstatement2).

**Table 2** SQL statements for defining a user<a id="sqlstatement2"></a>

| Description              | SQL Statement                                                |
| :----------------------- | :----------------------------------------------------------- |
| Creating a User          | [CREATE USER](../../reference-guide/sql-syntax/CREATE-USER.md) |
| Altering user attributes | [ALTER USER](../../reference-guide/sql-syntax/ALTER-USER.md) |
| Dropping a user          | [DROP USER](../../reference-guide/sql-syntax/DROP-USER.md)   |

## Granting Rights

MogDB provides a statement for granting rights to data objects and roles. For details, see [GRANT](../../reference-guide/sql-syntax/GRANT.md).

## Revoking Rights

MogDB provides a statement for revoking rights. For details, see [REVOKE](../../reference-guide/sql-syntax/REVOKE.md).

## Setting Default Rights

MogDB allows users to set rights for objects that will be created. For details, see [ALTER DEFAULT PRIVILEGES](../../reference-guide/sql-syntax/ALTER-DEFAULT-PRIVILEGES.md).

## Shutting Down The Current Node

MogDB allows users to run the **shutdown** command to shut down the current database node. For details, see [SHUTDOWN](../../reference-guide/sql-syntax/SHUTDOWN.md).
