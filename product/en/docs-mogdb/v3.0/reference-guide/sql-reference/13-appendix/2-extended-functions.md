---
title: Extended Functions
summary: Extended Functions
author: Zhang Cuiping
date: 2021-05-18
---

# Extended Functions

The following table lists the extended functions supported by MogDB. These functions are for reference only.

<table>
    <tr>
        <th>Category</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td rowspan=2>Access privilege inquiry function</td>
        <td>has_sequence_privilege(user, sequence, privilege)</td>
        <td>Queries whether a specified user has privilege for sequences.</td>
    </tr>
    <tr>
        <td>has_sequence_privilege(sequence, privilege)</td>
        <td>Queries whether the current user has privilege for sequence.</td>
    </tr>
    <tr>
        <td rowspan=2>Trigger function</td>
        <td>pg_get_triggerdef(oid)</td>
        <td>Gets CREATE [ CONSTRAINT ] TRIGGER command for triggers.</td>
    </tr>
    <tr>
        <td>pg_get_triggerdef(oid, boolean)</td>
        <td>Gets CREATE [ CONSTRAINT ] TRIGGER command for triggers.</td>
    </tr>
</table>
