---
title: dolphin
summary: dolphin
author: Zhang Cuiping
date: 2022-06-27
---

# dolphin

## Introduction

dolphin is a MySQL compatibility extension package of MogDB, in which MySQL data types and functions are added.

## dolphin Installation

For details, see [gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md) or [gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

## dolphin Usage

- Create the dolphin extension.

  ```sql
  CREATE EXTENSION dolphin;
  ```

- Check all modes. 

  ```text
  \dn
  ```

## Example

- Set sql_mode to sql_mode_full_group

  ```sql
  create extension dolphin;
  set sql_mode = 'sql_mode_full_group';
  create table test_group(a int, b int, c int, d int);
  create table test_group1(a int, b int, c int, d int);
  insert into test_group values(1,2,3,4);
  insert into test_group values(1,3,4,5);
  insert into test_group values(2,4,5,6);
  insert into test_group values(3,5,6,7);
  insert into test_group values(2,4,4,5);
  insert into test_group values(3,3,4,5);
  insert into test_group values(3,3,4,6);
  insert into test_group1 values(1,2,3,4);
  insert into test_group1 values(1,3,4,5);
  select a, b from test_group group by a;
  ERROR:  column "test_group.b" must appear in the GROUP BY clause or be used in an aggregate function
  LINE 1: select a, b from test_group group by a;
                    ^
  select a, d as items, (select count(*) from test_group t where t.a = i.a and b in (select b from test_group1 where c = 4)) as third from test_group i group by a;
  ERROR:  column "i.d" must appear in the GROUP BY clause or be used in an aggregate function
  LINE 1: select a, d as items, (select count(*) from test_group t whe...
                    ^
  select t.a, (select sum(b) from test_group i where i.b = t.b ) from test_group t where t.a > 1+1 or (t.b < 8 and t.b > 1) group by t.a;
  ERROR:  subquery uses ungrouped column "t.b" from outer query
  LINE 1: ...t.a, (select sum(b) from test_group i where i.b = t.b ) from...
                                                               ^
  set sql_mode = '';
  select a, b from test_group group by a;
   a | b 
  ---+---
   1 | 2
   3 | 5
   2 | 4
  (3 rows)
  
  select a, d as items, (select count(*) from test_group t where t.a = i.a and b in (select b from test_group1 where c = 4)) as third from test_group i group by a;
   a | items | third 
  ---+-------+-------
   1 |     4 |     1
   3 |     7 |     2
   2 |     6 |     0
  (3 rows)
  
  select t.a, (select sum(b) from test_group i where i.b = t.b ) from test_group t where t.a > 1+1 or (t.b < 8 and t.b > 1) group by t.a;
   a | sum 
  ---+-----
   1 |   2
   3 |   5
   2 |   8
  (3 rows)
  ```

- Set sql_mode to sql_mode_strict

  ```sql
  set sql_mode = 'sql_mode_strict';
  create table test_int(a int);
  insert into test_int(a) values(-55);
  insert into test_int(a) values(55);
  insert into test_int(a) values(35589898923423423423423423);
  insert into test_int(a) values(-3553434343434343432434);
  insert into test_int(a) values('a888');
  insert into test_int(a) values('88123423433834343423434aa');
  insert into test_int(a) values('-88aa');
  insert into test_int(a) values(1234233434343434343434343434343.55);
  insert into test_int(a) values(12.55);
  insert into test_int(a) values(-12.55);
  insert into test_int(a) values(-12123451343434343434343434343434.55);
  insert into test_int(a) values(1.4e300);
  insert into test_int(a) values(-1.4e300);
  select * from test_int;
        a      
  -------------
           -55
            55
    2147483647
   -2147483648
             0
    2147483647
           -88
    2147483647
            13
           -13
   -2147483648
    2147483647
   -2147483648
  (13 rows)
  
  
  set sql_mode = 'sql_mode_strict';
  insert into test_int_strict(a) values(-55);
  insert into test_int_strict(a) values(55);
  insert into test_int_strict(a) values(35589898923423423423423423);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values(-3553434343434343432434);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values('a888');
  ERROR:  invalid input syntax for integer: "a888"
  LINE 1: insert into test_int_strict(a) values('a888');
                                                ^
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values('88123423433834343423434aa');
  ERROR:  value "88123423433834343423434aa" is out of range for type integer
  LINE 1: insert into test_int_strict(a) values('881234234338343434234...
                                                ^
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values('-88aa');
  ERROR:  invalid input syntax for integer: "-88aa"
  LINE 1: insert into test_int_strict(a) values('-88aa');
                                                ^
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values(1234233434343434343434343434343.55);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values(12.55);
  insert into test_int_strict(a) values(-12.55);
  insert into test_int_strict(a) values(-12123451343434343434343434343434.55);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values(1.4e300);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  insert into test_int_strict(a) values(-1.4e300);
  ERROR:  integer out of range
  CONTEXT:  referenced column: a
  select * from test_int_strict;
    a  
  -----
   -55
    55
    13
   -13
  (4 rows)
  ```

- timestamp on update example
  - on update syntax: empty value, modified value, the query result that is not changed on update
  
    ```sql
    create table update_test_e(a int, b timestamp on update current_timestamp);
    CREATE TABLE
    insert into update_test_e values(1);
    INSERT 0 1
    insert into update_test_e values(2);
    INSERT 0 1
    select * from update_test_e;
     a | b
    ---+---
     1 |
     2 |
    (2 rows)
    update update_test_e set a = 11 where a = 1;
    UPDATE 1
    select * from update_test_e;
     a  |             b
    ----+----------------------------
      2 |
     11 | 2022-06-27 14:04:34.873645
    (2 rows)
    update update_test_e set a = 11 where a = 11;
    UPDATE 1
    select * from update_test_e;
    select * from update_test_e;
     a  |             b
    ----+----------------------------
      2 |
     11 | 2022-06-27 14:04:34.873645
    (2 rows)
    drop table update_test_e;
    DROP TABLE
    ```
  
  - on update syntax: default expression and on update expression
  
    ```sql
    create table update_test_f(a int, b timestamp default current_timestamp on update current_timestamp);
    CREATE TABLE
    insert into update_test_f values(1);
    INSERT 0 1
    insert into update_test_f values(2);
    INSERT 0 1
    select * from update_test_f;
     a |             b
    ---+----------------------------
     1 | 2022-06-27 14:07:36.174175
     2 | 2022-06-27 14:07:38.05313
    (2 rows)
    update update_test_f set a = 11 where a = 1;
    UPDATE 1
    select * from update_test_f;
     a  |             b
    ----+---------------------------
      2 | 2022-06-27 14:07:38.05313
     11 | 2022-06-27 14:07:49.04924
    (2 rows)
    drop table update_test_f;
    DROP TABLE
    ```
  
  - on update syntax: on update included in multiple fields
  
    ```sql
    create table update_test_g(a int, b timestamp default current_timestamp on update current_timestamp, c timestamp on update current_timestamp);
    CREATE TABLE
    insert into update_test_g values(1);
    INSERT 0 1
    insert into update_test_g values(2);
    INSERT 0 1
    select * from update_test_g;
     a |             b              | c
    ---+----------------------------+---
     1 | 2022-06-27 14:09:19.004397 |
     2 | 2022-06-27 14:09:22.574669 |
    (2 rows)
    update update_test_g set a = 11 where a = 1;
    UPDATE 1
    select * from update_test_g;
     a  |             b              |             c
    ----+----------------------------+----------------------------
      2 | 2022-06-27 14:09:22.574669 |
     11 | 2022-06-27 14:09:36.574072 | 2022-06-27 14:09:36.574072
    (2 rows)
    drop table update_test_g;
    DROP TABLE
    ```
  
  - on update syntax: with tab as update query
  
    ```sql
    create table update_test_h(a int, b timestamp default current_timestamp on update current_timestamp);
    CREATE TABLE
    insert into update_test_h values(1);
    INSERT 0 1
    select * from update_test_h;
     a |             b
    ---+----------------------------
     1 | 2022-06-27 14:11:27.673113
    (1 row)
    with tab as (update update_test_h set a=10 where a=1 returning 1) select * from tab;
     ?column?
    ----------
            1
    (1 row)
    select * from update_test_h;
     a  |             b
    ----+----------------------------
     10 | 2022-06-27 14:11:34.761167
    (1 row)
    drop table update_test_h;
    DROP TABLE
    ```
  
  - on update syntax: prepare query
  
    ```sql
    CREATE TABLE t2 (a int, b timestamp on update current_timestamp);
    CREATE TABLE
    INSERT INTO t2 VALUES(1);
    INSERT 0 1
    select * from t2;
     a | b
    ---+---
     1 |
    (1 row)
    PREPARE insert_reason(integer) AS UPDATE t2 SET a = $1;
    PREPARE
    EXECUTE insert_reason(52);
    UPDATE 1
    select * from t2;
     a  |             b
    ----+----------------------------
     52 | 2022-06-27 14:12:46.736842
    (1 row)
    drop table t2;
    DROP TABLE
    ```
  
  - on update syntax: single table query in stored procedure
  
    ```sql
    create table t1(a int, b timestamp on update current_timestamp);
    CREATE TABLE
    insert into t1 values(1);
    INSERT 0 1
    
    CREATE OR REPLACE FUNCTION TEST1(val integer)
    RETURNS VOID AS $$
    declare
    val int;
    begin
    update t1 set a=val;
    end;
    $$
    LANGUAGE sql;
    CREATE FUNCTION
    CREATE OR REPLACE FUNCTION TEST2(val integer)
    RETURNS integer AS $$
    update t1 set a=val;
    select 1;
    $$
    LANGUAGE sql;
    CREATE FUNCTION
    
    select TEST2(11);
     test2
    -------
         1
    (1 row)
    select * from t1;
     a  |             b
    ----+----------------------------
     11 | 2022-06-27 14:15:44.620784
    (1 row)
    select TEST1(2);
     test1
    -------
    
    (1 row)
    select * from t1;
     a |             b
    ---+----------------------------
       | 2022-06-27 14:15:52.286877
    (1 row)
    DROP FUNCTION TEST1;
    DROP FUNCTION
    DROP FUNCTION TEST2;
    DROP FUNCTION
    drop table t1;
    DROP TABLE
    ```
  
  - on update syntax: multiple table query in stored procedure
  
    ```sql
    CREATE TABLE t1(a int, b timestamp DEFAULT current_timestamp on update current_timestamp);
    CREATE TABLE
    CREATE TABLE t2(a int, b timestamp DEFAULT current_timestamp, c varchar, d timestamp on update current_timestamp);
    CREATE TABLE
    CREATE TABLE t3(a int, b char, c timestamp on update current_timestamp, d text);
    CREATE TABLE
    INSERT INTO t1 VALUES(1);
    INSERT 0 1
    INSERT INTO t2(a,c) VALUES(1,'test');
    INSERT 0 1
    INSERT INTO t3(a,b,d) VALUES(1,'T','asfdsaf');
    INSERT 0 1
    SELECT * FROM t1;
     a |             b
    ---+----------------------------
     1 | 2022-06-27 14:19:32.058004
    (1 row)
    SELECT * FROM t2;
     a |             b             |  c   | d
    ---+---------------------------+------+---
     1 | 2022-06-27 14:19:36.30913 | test |
    (1 row)
    SELECT * FROM t3;
     a | b | c |    d
    ---+---+---+---------
     1 | T |   | asfdsaf
    (1 row)
    CREATE OR REPLACE FUNCTION test1(src int, dest int) RETURNS void AS $$
      BEGIN
        UPDATE t1 SET a = dest WHERE a = src;
        UPDATE t2 SET a = dest WHERE a = src;
        UPDATE t3 SET a = dest WHERE a = src;
      END;
    $$ LANGUAGE PLPGSQL;
    CREATE FUNCTION
    SELECT test1(1,10);
     test1
    -------
    
    (1 row)
    SELECT * FROM t1;
     a  |             b
    ----+----------------------------
     10 | 2022-06-27 14:20:21.690661
    (1 row)
    SELECT * FROM t2;
     a  |             b             |  c   |             d
    ----+---------------------------+------+----------------------------
     10 | 2022-06-27 14:19:36.30913 | test | 2022-06-27 14:20:21.690892
    (1 row)
    SELECT * FROM t3;
     a  | b |             c              |    d
    ----+---+----------------------------+---------
     10 | T | 2022-06-27 14:20:21.691079 | asfdsaf
    (1 row)
    DROP FUNCTION test1;
    DROP FUNCTION
    DROP TABLE t1;
    DROP TABLE
    DROP TABLE t2;
    DROP TABLE
    DROP TABLE t3;
    DROP TABLE
    ```
  
  - on update syntax: multiple table query with tab as update
  
    ```sql
    CREATE TABLE t1(a int, b timestamp on update current_timestamp);
    CREATE TABLE
    CREATE TABLE t2(a int, b timestamp on update current_timestamp);
    CREATE TABLE
    INSERT INTO t1 VALUES(1);
    INSERT 0 1
    INSERT INTO t2 VALUES(2);
    INSERT 0 1
    SELECT * FROM t1;
     a | b
    ---+---
     1 |
    (1 row)
    SELECT * FROM t2;
     a | b
    ---+---
     2 |
    (1 row)
    with a as (update t1 set a=a+10 returning 1), b as (update t2 set a=a+10 returning 1) select * from a,b;
     ?column? | ?column?
    ----------+----------
            1 |        1
    (1 row)
    SELECT * FROM t1;
     a  |             b
    ----+----------------------------
     11 | 2022-06-27 14:23:27.506413
    (1 row)
    SELECT * FROM t2;
     a  |             b
    ----+----------------------------
     12 | 2022-06-27 14:23:27.506413
    (1 row)
    with a as (update t1 set a=a+10), b as (update t2 set a=a+10) select 1;
     ?column?
    ----------
            1
    (1 row)
    SELECT * FROM t1;
     a  |             b
    ----+----------------------------
     21 | 2022-06-27 14:23:38.998855
    (1 row)
    SELECT * FROM t2;
     a  |             b
    ----+----------------------------
     22 | 2022-06-27 14:23:38.998855
    (1 row)
    DROP TABLE t1;
    DROP TABLE
    DROP TABLE t2;
    DROP TABLE
    ```
  
  - on update syntax: alter table query
  
    ```sql
    CREATE TABLE t4(a int, b timestamp);
    ALTER TABLE
    \d t4
                    Table "public.t4"
     Column |            Type             | Modifiers
    --------+-----------------------------+-----------
     a      | integer                     |
     b      | timestamp without time zone |
    alter table t4 modify b timestamp on update current_timestamp;
    ALTER TABLE
    \d t4
                             Table "public.t4"
     Column |            Type             |          Modifiers
    --------+-----------------------------+-----------------------------
     a      | integer                     |
     b      | timestamp without time zone | on update pg_systimestamp()
    alter table t4 modify b timestamp;
    ALTER TABLE
    \d t4
                    Table "public.t4"
     Column |            Type             | Modifiers
    --------+-----------------------------+-----------
     a      | integer                     |
     b      | timestamp without time zone |
    alter table t4 alter b set default now();
    ALTER TABLE
    \d t4
                      Table "public.t4"
     Column |            Type             |   Modifiers
    --------+-----------------------------+---------------
     a      | integer                     |
     b      | timestamp without time zone | default now()
    alter table t4 alter b drop default;
    ALTER TABLE
    \d t4
                    Table "public.t4"
     Column |            Type             | Modifiers
    --------+-----------------------------+-----------
     a      | integer                     |
     b      | timestamp without time zone |
    
    alter table t4 alter b set default now();
    ALTER TABLE
    \d t4
                      Table "public.t4"
     Column |            Type             |   Modifiers
    --------+-----------------------------+---------------
     a      | integer                     |
     b      | timestamp without time zone | default now()
    alter table t4 modify b timestamp on update current_timestamp;
    ALTER TABLE
    \d t4
                                    Table "public.t4"
     Column |            Type             |                 Modifiers
    --------+-----------------------------+-------------------------------------------
     a      | integer                     |
     b      | timestamp without time zone | default now() on update pg_systimestamp()
    alter table t4 alter b drop default;
    ALTER TABLE
    \d t4
                             Table "public.t4"
     Column |            Type             |          Modifiers
    --------+-----------------------------+-----------------------------
     a      | integer                     |
     b      | timestamp without time zone | on update pg_systimestamp()
    alter table t4 modify b timestamp;
    ALTER TABLE
    \d t4
                    Table "public.t4"
     Column |            Type             | Modifiers
    --------+-----------------------------+-----------
     a      | integer                     |
     b      | timestamp without time zone |
    alter table t4 modify b timestamp on update current_timestamp;
    ALTER TABLE
    \d t4
                             Table "public.t4"
     Column |            Type             |          Modifiers
    --------+-----------------------------+-----------------------------
     a      | integer                     |
     b      | timestamp without time zone | on update pg_systimestamp()
    alter table t4 alter b set default now();
    ALTER TABLE
    \d t4;
                                    Table "public.t4"
     Column |            Type             |                 Modifiers
    --------+-----------------------------+-------------------------------------------
     a      | integer                     |
     b      | timestamp without time zone | default now() on update pg_systimestamp()
    alter table t4 modify b timestamp;
    ALTER TABLE
    \d t4
                      Table "public.t4"
     Column |            Type             |   Modifiers
    --------+-----------------------------+---------------
     a      | integer                     |
     b      | timestamp without time zone | default now()
    alter table t4 alter b drop default;
    ALTER TABLE
    \d t4
                    Table "public.t4"
     Column |            Type             | Modifiers
    --------+-----------------------------+-----------
     a      | integer                     |
     b      | timestamp without time zone |
    alter table t4 modify b timestamp on update current_timestamp;
    ALTER TABLE
    \d t4
                             Table "public.t4"
     Column |            Type             |          Modifiers
    --------+-----------------------------+-----------------------------
     a      | integer                     |
     b      | timestamp without time zone | on update pg_systimestamp()
    alter table t4 modify b timestamp on update localtimestamp;
    ALTER TABLE
    \d t4
                                          Table "public.t4"
     Column |            Type             |                      Modifiers
    --------+-----------------------------+------------------------------------------------------
     a      | integer                     |
     b      | timestamp without time zone | on update ('now'::text)::timestamp without time zone
    ```
  
  - on update syntax: like table including defaults
  
    ```sql
    CREATE TABLE t5(id int, a timestamp default now() on update current_timestamp, b timestamp on update current_timestamp, c timestamp default now());
    CREATE TABLE
    \d t5
                                    Table "public.t5"
     Column |            Type             |                 Modifiers
    --------+-----------------------------+-------------------------------------------
     id     | integer                     |
     a      | timestamp without time zone | default now() on update pg_systimestamp()
     b      | timestamp without time zone | on update pg_systimestamp()
     c      | timestamp without time zone | default now()
    create table t6 (like t5 including defaults);
    CREATE TABLE
    \d t6
                                    Table "public.t6"
     Column |            Type             |                 Modifiers
    --------+-----------------------------+-------------------------------------------
     id     | integer                     |
     a      | timestamp without time zone | default now() on update pg_systimestamp()
     b      | timestamp without time zone | on update pg_systimestamp()
     c      | timestamp without time zone | default now()
    ```

## Related Page

[SQL Mode](../../reference-guide/guc-parameters/SQL-mode.md)