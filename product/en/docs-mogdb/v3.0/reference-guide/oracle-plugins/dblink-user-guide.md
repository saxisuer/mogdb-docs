---
title: DBlink User Guide
summary: DBlink User Guide
author: Guo Huan
date: 2021-11-29
---

# dblink

## dblink Overview

dblink is used to execute queries in a remote database. It usually refers to `SELECT`, and it can also be any SQL statement that returns rows.

When two `text` parameters are given, the first one is searched by the name for persistent connection; if it is found, the command is executed on the connection. If not found, the first parameter is treated as the same connection information string as `dblink_connect`, and the connection is only implemented during execution of this command.

<br/>

## dblink Installation

For details, please refer to [gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md) or [gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md).

<br/>

## dblink Usage

### Create Extensions In the Database

```sql
create extension dblink;
```

### Check Whether the Extension Was Created Successfully

```
\dx
```

### Connect to the Remote Database to Perform Query Operations

```sql
select * from dblink('dbname=postgres host=127.0.0.1 port=12345 user=test password=Test123456'::text, 'select * from dblink_test'::text)t(id int, name varchar);
```

### Create Connections

```sql
select dblink_connect('dblink_conn','hostaddr=127.0.0.1 port=12345 dbname=postgres user=test password=Test123456');
```

### Operate on Database Tables

(View does not support query operations)

```sql
select dblink_exec('dblink_conn', 'create table ss(id int, name int)');
select dblink_exec('dblink_conn', 'insert into ss values(2,1)');
select dblink_exec('dblink_conn', 'update ss set name=2 where id=1');
select dblink_exec('dblink_conn', 'delete from ss where id=1');
```

### Close Connections

```sql
select dblink_disconnect('dblink_conn')
```
