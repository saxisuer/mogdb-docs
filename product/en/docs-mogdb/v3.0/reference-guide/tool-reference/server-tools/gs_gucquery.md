---
title: gs_gucquery
summary: gs_gucquery
author: Zhang Cuiping
date: 2022-06-22
---

# gs_gucquery

## Introduction

gs_gucquery automatically collects, organizes, and exports MogDB GUC values and perform difference comparison. 

## Syntax

- Export GUC values.

  ```bash
  gs_gucquery -t query [-o OUTPUTFILE] [-p PORT]
  ```

- Compare GUC values.

  ```bash
  gs_gucquery -t compare --source-file=OUTPUTFILE --target-file=OUTPUTFILE --diff-file=OUTPUTDIR
  ```

## Parameter Description

| Parameter     | Description                                                  |
| ------------- | ------------------------------------------------------------ |
| -t            | Specifies the gs_gucquery command type. <br />Values: **query** and **compare** |
| --source-file | Specifies the directory for storing the source file to be compared. The file name should be included in the directory. |
| --target-file | Specifies the directory for storing the target file (storing the exported GUC values) to be compared. The file name should be included in the directory. |
| --diff-file   | Specifies the directory of the file for storing the difference comparison result. |
| -o            | Specifies the directory of the file for collecting the GUC values.<br />If this parameter is not specified, the result will be output in the terminal. |
| -p            | Specifies the port of the MogDB server where GUC values are to be exported. |

## Example

1. Export MogDB GUC values.

   ```bash
   gs_gucquery -t query -o /data/gucRes
   ```

2. Compare MogDB GUC values.

   ```bash
   gs_gucquery -t compare --source-file=/data/gucRes/GucQuery_20220203091229.csv --target-file /data/gucRes/GucQuery_20220204101330.csv --diff-file /data/gucRes/diffRes
   ```
