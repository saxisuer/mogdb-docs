---
title: gs_backup
summary: gs_backup
author: Zhang Cuiping
date: 2021-06-07
---

# gs_backup

## Background

After MogDB is deployed, problems and exceptions may occur during database running. **gs_backup** is provided by MogDB to back up MogDB, restore important data, and display help and version information.

## Prerequisites

- The MogDB database can be connected.
- There are backup files in the backup directory for each node when the restoration is performed. If the backup file of a node is missing, copy a backup file from other nodes (for binary files, the node name in the file name needs to be changed).
- Log in to the OS as the OS user **omm** to run the **gs_backup** command.

## Syntax

- Back up the database host.

  ```bash
  gs_backup -t backup --backup-dir=BACKUPDIR [-h HOSTNAME] [--parameter] [--binary] [--all] [-l LOGFILE]
  ```

- Restore the database host.

  ```bash
  gs_backup -t restore --backup-dir=BACKUPDIR [-h HOSTNAME] [--parameter] [--binary] [--all] [-l LOGFILE]
  ```

- Display help information.

  ```bash
  gs_backup -? | --help
  ```

- Display version information.

  ```bash
  gs_backup -V | --version
  ```

## Parameter Description

The **gs_backup** tool can use the following types of parameters:

- Parameters for backing up the database host:

  - -h

    Specifies the host name for storing the backup files

    Value range: a host name If the host name is not specified, the current cluster will be backed up.

  - -backup-dir=BACKUPDIR

    Specifies the backup file path.

  - -parameter

    Backs up parameter files. If **-parameter**, **-binary**, and **-all** are not specified, only parameters files are backed up.

  - -binary

    Backs up the binary files in the **app** directory.

  - -all

    Backs up binary files in the **app** directory, the **pg_hba.conf** file, and the **postgsql.conf** file.

  - -l

    Specifies a log file and its storage path.

    Default value: **$GAUSSLOG/om/gs_backup-YYYY-MM-DD_hhmmss.log**.

- Parameters for restoring the database host:

  - -h

    Specifies the name of the host to restore.

    Value range: a host name If the host is not specified, the corresponding backup node is restored.

  - -backup-dir=BACKUPDIR

    Restores the file obtaining path.

  - -parameter

    Restores parameter files. If **-parameter**, **-binary**, and **-all** are not specified, only parameters files are restored.

  - -binary

    Restores binary files.

  - -all

    Restores binary files and parameter files.

  - -l

    Specifies a log file and its storage path.

    Default value: **$GAUSSLOG/om/gs_backup-YYYY-MM-DD_hhmmss.log**.

- Other parameters:

  - -?, -help

    Displays help information.

  - -V, -version

    Displays version information.

  - -t

    Specify the type of operation.

    Value range: backup or restore.

## Example

- Back up the database host.

  ```bash
  gs_backup -t backup --backup-dir=/opt/software/gaussdb/backup_dir -h plat1 --parameter
  Parsing configuration files.
  Successfully parsed the configuration file.
  Performing remote backup.
  Remote backup succeeded.
  Successfully backed up cluster files.
  ```
  
- Execute the **gs_backup** script to restore the database host.

  ```bash
  gs_backup -t restore --backup-dir=/opt/software/gaussdb/backup_dir -h plat1 --parameter
  Parsing configuration files.
  Successfully parsed the configuration file.
  Performing remote restoration.
  Successfully restored cluster files.
  ```
