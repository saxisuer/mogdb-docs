---
title: transfer.py
summary: transfer.py
author: Zhang Cuiping
date: 2021-11-17
---

# transfer.py

## Function

In non-single-node mode, you can use the **transfer.py** tool to send the dictionary file used for full-text search and the files required by the PostGIS plug-in to other nodes.

## Prerequisites

- Before running this tool, ensure that the following files exist in the **$GAUSSHOME** directory. Otherwise, an error will be reported. The error code is 52200:
  - script/gspylib/common/DbClusterInfo.py
  - script/gspylib/common/Common.py
  - script/gspylib/common/GaussLog.py
  - script/gspylib/threads/SshTool.py
- The environment variable *$GPHOME* is set correctly.

## Syntax

- Send the sourcefile to the **destinationpath** path of all nodes.

  ```
  transfer.py 1 sourcefile destinationpath
  ```

- Send the sourcefile to the same path on the standby node of **pgxc_node_name**.

  ```
  transfer.py 2 sourcefile pgxc_node_name
  ```

- Display the help information.

  ```
  transfer.py -? | --help
  ```

## Parameter Description

- Options are as follows:

  - **1**: sends the current file to the destination file paths of all nodes.
  - **2**: sends the current file to the same path on the standby node of the destination node.

- sourcefile

  Path of the source file to be sent.

- destinationpath

  Destination file path.

- pgxc_node_name

  Destination node name.
