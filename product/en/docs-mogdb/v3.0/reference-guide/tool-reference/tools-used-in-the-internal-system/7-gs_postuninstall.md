---
title: gs_postuninstall
summary: gs_postuninstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_postuninstall

## Background

**gs_postuninstall**, provided by MogDB, is used to delete data configured before MogDB installation.

## Prerequisites

- The uninstallation of MogDB is successful.
- User **root** is trustworthy and available.
- Only user **root** is authorized to run the **gs_postuninstall** command.

## Syntax

- Delete the user, user group, and virtual IP address in MogDB.

  ```
  gs_postuninstall -U USER -X XMLFILE [-L] [--delete-user] [--delete-group] [-l LOGFILE]
  ```

- Display help information.

  ```
  gs_postuninstall -? | --help
  ```

- Display version information.

  ```
  gs_postuninstall -V | --version
  ```

## Parameter Description

- -U

  Specifies the OS username of MogDB.

  Value range: a string. It must comply with the naming convention.

- -X

  Specifies the path of the MogDB configuration file.

  Value range: storage paths of XML files

- -L

  Clears only the host environment.

  If a host environment in MogDB is cleared, MogDB cannot perform full environment clearance.

- -delete-user

  Deletes the OS user specified by the **-U** parameter.

  In the Red Hat environment, the username and the user group name are the same. If this parameter is specified, the **-delete-group** parameter must be specified.

- -delete-group

  Deletes the user group where the OS user belongs to (the **-delete-user** parameter must be specified when selecting this option).

- -l

Specifies a log file name and its path. A timestamp will be added automatically to the log file name.

When **-l** is not specified and **gaussdbLogPath** is not set in the XML file, the default value of the **-l** parameter is **/var/log/gaussdb/om/gs_local-YYYY-MMDD_hhmmss.log**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** After executing gs_postuninstall, the system will automatically delete the directories related to MogDB (including $GAUSSLOG). You are advised to set the log file path to a path outside the MogDB database.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Example

Clear the host environment.

```
gs_postuninstall -U omm  -X /opt/software/mogdb/clusterconfig.xml --delete-user
Parsing the configuration file.
Successfully parsed the configuration file.
Check log file path.
Successfully checked log file path.
Checking unpreinstallation.
Successfully checked unpreinstallation.
Deleting the instance's directory.
Successfully deleted the instance's directory.
Deleting the installation directory.
Successfully deleted the installation directory.
Deleting the temporary directory.
Successfully deleted the temporary directory.
Deleting remote OS user.
Successfully deleted remote OS user.
Deleting software packages and environmental variables of other nodes.
Successfully deleted software packages and environmental variables of other nodes.
Deleting logs of other nodes.
Successfully deleted logs of other nodes.
Deleting software packages and environmental variables of the local node.
Successfully deleted software packages and environmental variables of the local nodes.
Deleting local OS user.
Successfully deleted local OS user.
Deleting local node's logs.
Successfully deleted local node's logs.
Successfully cleaned environment.
```

## Helpful Links

gs_preinstall and gs_uninstall
