---
title: pg_archivecleanup
summary: pg_archivecleanup
author: Zhang Cuiping
date: 2021-11-17
---

# pg_archivecleanup

## Function

pg_archivecleanup is a tool used to clear old archive logs.

## Syntax

```
pg_archivecleanup [OPTION]... ARCHIVELOCATION OLDESTKEPTWALFILE
```

## Parameter Description

- -d

  Displays debug logs on the stderr.

- -n

  Displays the name of the file to be deleted.

- -V,-version

  Displays the version of **pg_archivecleanup** and exits.

- -x extension

  Provides an extension option to select only files with the corresponding extension format when the program is used as a separate tool.

- -?,-help

  Displays the help information related to the **pg_archivecleanup** command line parameters.

## Examples

Delete logs generated earlier than 000000010000000000000010.

```
pg_archivecleanup [OPTION]... ARCHIVELOCATION OLDESTKEPTWALFILE
```

## Extended Applications

Set the **archive_cleanup_command** parameter in the **recovery.conf** file during the standby node recovery.

```
archive_cleanup_command = 'pg_archivecleanup /mnt/server/archiverdir %r'
```

After the recovery is complete, delete unnecessary files from the archive directory.
