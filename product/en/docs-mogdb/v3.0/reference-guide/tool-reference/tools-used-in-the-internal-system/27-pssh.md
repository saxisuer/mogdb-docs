---
title: pssh
summary: pssh
author: Zhang Cuiping
date: 2021-11-17
---

# pssh

## Basic Function

This tool is used to execute access between database nodes and send commands to remote nodes.

## Syntax

```
python3 pssh [OPTIONS]
```

## Parameter Description

- -help

  Displays help information.

- -H HOSTNAME

  Specifies the host list.

- -h HOSTFILE

  Specifies the full path of the file to be written into the host list.

- -t TIMEOUT

  Specifies the connection timeout interval, in seconds. The default value is **300s**.

- -p PARALLEL

  Specifies the maximum number of concurrent tasks. The default value is **32**.

- -o OUTDIR

  Specifies the full file path of the execution result.

- -e ERRDIR

  Specifies the full path of the error information output file.

- -P

  Specifies the command executed on the online remote host.

- -s

  Displays the execution result.

- -x EXTRA

  Specifies the extra command line parameters.

- -i

  Displays the output and errors of all nodes.

- -O OPT

  Specifies the added SSH parameters in offline mode.
