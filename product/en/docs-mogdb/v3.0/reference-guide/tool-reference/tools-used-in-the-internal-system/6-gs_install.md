---
title: gs_install
summary: gs_install
author: Zhang Cuiping
date: 2021-06-07
---

# gs_install

## Background

Database deployment is a complex process. **gs_install**, provided by MogDB, is used to install and deploy MogDB.

Installation and deployment of MogDB require you to specify a configuration file, in which the program installation path, instance data directory, the primary/standby relationship, the number of instances, and the service IP address of each instance are specified.

## Prerequisites

- You have successfully executed the **gs_preinstall** script.
- The **locale** parameter for each server is set to the same value.
- MogDB users must be set during the preinstallation process.

## Syntax

- Install MogDB.

  ```
  gs_install -X XMLFILE [--gsinit-parameter="PARAMETER" [...]] [--dn-guc="PARAMETER" [...]] [--alarm-component=ALARMCOMPONENT] [--time-out=SECS] [-l LOGFILE]
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**

- Display help information.

  ```
  gs_install -? | --help
  ```

- Display version information.

  ```
  gs_install -V | --version
  ```

## Parameter Description

- -X

  MogDB configuration file.

  Value range: xml configuration file.

- -l

  Specifies a log file and its path.

  When **-l** is not specified but **gaussdbLogPath** is set in the XML file, the default value of the **-l** parameter is the combination value of **gaussdbLogPath/User name/om/gs_install-YYYY-MM-DD_hhmmss.log**.

- -gsinit-parameter=PARAM

  Specifies the data instance parameters.

  For details about the value range, see parameter description in **gs_initdb**. Among which, the settings of the **-A**, **-D**, **-U**, **-C**, and **-X** parameters do not take effect.

- -dn-guc=PARAM

  Set a parameter.

  For details about the value range, see parameter description in **gs_guc**.

- -time-out=SECS

  Specifies the timeout threshold for startup. Unit: s

  If the value of **-time-out** is too small, the instance startup may time out due to a large number of instances. If the instance failed to be started within the specified timeout period, an error will be reported. MogDB still runs at the backend. You can wait for a while and then query the MogDB status to check whether MogDB is started successfully.

  Value range: a positive integer. If there is no special requirement, you do not need to set this parameter. The system automatically adjusts the timeout threshold.

- -alarm-component=alarm_component

  Specifies the absolute path of the component to report alarms.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Example

Use **gs_install** to install MogDB.

```
gs_install -X /opt/software/mogdb/clusterconfig.xml
Parsing the configuration file.
Check preinstall on every node.
Successfully checked preinstall on every node.
Creating the backup directory.
Successfully created the backup directory.
begin deploy..
Installing the cluster.
begin prepare Install Cluster..
Checking the installation environment on all nodes.
begin install Cluster..
Installing applications on all nodes.
Successfully installed APP.
begin init Instance..
encrypt cipher and rand files for database.
Please enter password for database:
Please repeat for database:
begin to create CA cert files
The sslcert will be generated in /opt/gaussdb/cluster/app/share/sslcert/om
Cluster installation is completed.
Configuring.
Deleting instances from all nodes.
Successfully deleted instances from all nodes.
Checking node configuration on all nodes.
Initializing instances on all nodes.
Updating instance configuration on all nodes.
Check consistence of memCheck and coresCheck on DN nodes.
Successful check consistence of memCheck and coresCheck on all nodes.
Configuring pg_hba on all nodes.
Configuration is completed.
Successfully started cluster.
Successfully installed application.
```
