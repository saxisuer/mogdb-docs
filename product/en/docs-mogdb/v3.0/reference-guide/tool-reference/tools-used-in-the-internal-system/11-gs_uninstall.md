---
title: gs_uninstall
summary: gs_uninstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_uninstall

## Background

gs_uninstall, provided by , can be used to uninstall a cluster.

## Syntax

- Uninstalling MogDB

  ```
  gs_uninstall [--delete-data] [-L] [-l LOGFILE]
  ```

- Display help information.

  ```
  gs_uninstall -? | --help
  ```

- Display the version number.

  ```
  gs_uninstall -V | --version
  ```

## Parameter Description

- -delete-data

  Deletes the data file.

- -L

  Uninstalls the local host only. If a host in MogDB is uninstalled, MogDB cannot be performed full uninstallation.

- -l

    Specifies a log file name and its absolute path that can be accessed. A timestamp will be added automatically to the log file name.

  - When **-l** is not specified and **gaussdbLogPath** is not set in the XML file, the default value of the **-l** parameter is *$GAUSSLOG***/om/gs_uninstall-YYYY-MM-DD_hhmmss.log**.

  - When **-l** is not specified but **gaussdbLogPath** is set in the XML file, the default value of the **-l** parameter is **gaussdbLogPath/***Username***/om/gs_uninstall-YYYY-MM-DD_hhmmss.log**.

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** When executing **gs_uninstall**, the system will automatically delete the directories related to MogDB. You are advised to set the log file path to a path outside the MogDB database.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Example

Execute the **gs_uninstall** script as a database user to uninstall MogDB.

```
gs_uninstall --delete-data
Checking uninstallation.
Successfully checked uninstallation.
Stopping the cluster.
Successfully stopped the cluster.
Successfully deleted instances.
Uninstalling application.
Successfully uninstalled application.
Uninstallation succeeded.
```

## Related Commands

[gs_install](6-gs_install.md) and [gs_postuninstall](7-gs_postuninstall.md)
