---
title: HLL
summary: HLL
author: Guo Huan
date: 2021-04-06
---

# HLL

HyperLoglog (HLL) is an approximation algorithm for efficiently counting the number of distinct values in a data set. It features faster computing and lower space usage. You only need to store HLL data structures, instead of data sets. When new data is added to a data set, make hash calculation on the data and insert the result to an HLL. Then, you can obtain the final result based on the HLL.

[Table 1](#biao1) compares HLL with other algorithms.

**Table 1** Comparison between HLL and other algorithms <a id="biao1"> </a>

| Item                      | Sorting Algorithm     | Hash Algorithm        | HLL                                   |
| :------------------------ | :-------------------- | :-------------------- | :------------------------------------ |
| Time complexity           | O(nlogn)              | O(n)                  | O(n)                                  |
| Space complexity          | O(n)                  | O(n)                  | log(logn)                             |
| Error rate                | 0                     | 0                     | ≈0.8%                                 |
| Storage space requirement | Size of original data | Size of original data | The maximum size is 16 KB by default. |

HLL has advantages over others in the computing speed and storage space requirement. In terms of time complexity, the sorting algorithm needs O(nlogn) time for sorting, and the hash algorithm and HLL need O(n) time for full table scanning. In terms of storage space requirements, the sorting algorithm and hash algorithm need to store raw data before collecting statistics, whereas the HLL algorithm needs to store only the HLL data structures rather than the raw data, and thereby occupying a fixed space of about 16 KB.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - In the current default specifications, the maximum number of distinct values that can be calculated is about 1.1e + 15, and the error rate is 0.8%. If the calculation result exceeds the maximum, the error rate of the calculation result will increase, or the calculation will fail and an error will be reported.
> - When using this feature for the first time, you need to evaluate the distinct values of the service, properly select configuration parameters, and perform verification to ensure that the accuracy meets requirements.
>   - By default, the distinct value is 1.1e + 15. If the distinct value is NaN, you need to adjust log2m or use another algorithm to calculate the distinct value.
>   - The hash algorithm has an extremely low probability of collision. However, you are still advised to select 2 or 3 hash seeds for verification when using the hash algorithm for the first time. If there is only a small difference between the distinct values, you can select any one of the seeds as the hash seed.

[Table 2](#hyper) describes main HLL data structures.

**Table 2** Main HLL data structures <a id="hyper"> </a>

| Data Type | Description                                                  |
| :-------- | :----------------------------------------------------------- |
| hll       | The HLL header is a 27-byte field. By default, the data length ranges from 0 KB to 16 KB. The distinct value can be obtained. |

When you create an HLL data type, 0 to 4 input parameters are supported. The parameter meanings and specifications are the same as those of the **hll_empty** function. The first parameter is **log2m**, indicating the logarithm of the number of buckets, and its value ranges from 10 to 16. The second parameter is **log2explicit**, indicating the threshold in explicit mode, and its value ranges from 0 to 12. The third parameter is **log2sparse**, indicating the threshold of the Sparse mode, and its value ranges from 0 to 14. The fourth parameter is **duplicatecheck**, indicating whether to enable duplicatecheck, and its value ranges from 0 to 1. When the input parameter is set to **-1**, the default value of the HLL parameter is used. You can run the **\d** or **\d+** command to view the parameters of the HLL type.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** When the HLL data type is created, the result varies depending on the input parameter behavior:
>
> - When creating an HLL type, do not set the input parameter or set it to **-1**. Use the default value of the corresponding HLL parameter.
> - If a valid value is set for the input parameter, the corresponding HLL parameter uses the input value.
> - If the input value is invalid, an error is reported when the HLL type is created.

```sql
-- Create an HLL table without specifying input parameters.
MogDB=# create table t1 (id integer, set hll);
MogDB=# \d t1
      Table "public.t1"
 Column |  Type   | Modifiers
--------+---------+-----------
 id     | integer |
 set     | hll     |

-- Create an HLL table, specify the first two input parameters, and use the default values for the last two input parameters.
MogDB=# create table t2 (id integer, set hll(12,4));
MogDB=# \d t2
          Table "public.t2"
 Column |      Type      | Modifiers
--------+----------------+-----------
 id     | integer        |
 set     | hll(12,4,12,0) |

-- Create an HLL table, specify the third input parameter, and use default values for other parameters.
MogDB=# create table t3(id int, set hll(-1,-1,8,-1));
MogDB=# \d t3
          Table "public.t3"
 Column |      Type      | Modifiers
--------+----------------+-----------
 id     | integer        |
 set     | hll(14,10,8,0) |

-- When a user creates an HLL table and specifies an invalid input parameter, an error is reported.
MogDB=# create table t4(id int, set hll(5,-1));
ERROR:  log2m = 5 is out of range, it should be in range 10 to 16, or set -1 as default
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** When inserting an HLL object to an HLL table, ensure that the parameters of the HLL type are the same as those of the inserted object. Otherwise, an error is reported.

```sql
-- Create an HLL table:
MogDB=# create table t1(id integer, set hll(14));

-- Insert an HLL object to a table. The insertion succeeds because parameter types are consistent.
MogDB=# insert into t1 values (1, hll_empty(14,-1));

-- Insert an HLL object to a table. The insertion fails because parameter types are inconsistent.
MogDB=# insert into t1(id, set) values (1, hll_empty(14,5));
ERROR:  log2explicit does not match: source is 5 and dest is 10
```

The following describes HLL application scenarios.

- Scenario 1: "Hello World"

  The following example shows how to use the HLL data type:

  ```sql
  -- Create a table with the HLL type:
  MogDB=# create table helloworld (id integer, set hll);

  -- Insert an empty HLL to the table:
  MogDB=# insert into helloworld(id, set) values (1, hll_empty());

  -- Add a hashed integer to the HLL:
  MogDB=# update helloworld set set = hll_add(set, hll_hash_integer(12345)) where id = 1;

  -- Add a hashed string to the HLL:
  MogDB=# update helloworld set set = hll_add(set, hll_hash_text('hello world')) where id = 1;

  -- Obtain the number of distinct values of the HLL:
  MogDB=# select hll_cardinality(set) from helloworld where id = 1;
   hll_cardinality
  -----------------
                 2
  (1 row)

  -- Delete the table.
  MogDB=#  drop table helloworld;
  ```

- Scenario 2: Collect statistics about website visitors.

  The following example shows how an HLL collects statistics on the number of users visiting a website within a period of time:

  ```sql
  -- Create a raw data table to show that a user has visited the website at a certain time:
  MogDB=# create table facts (
           date            date,
           user_id         integer
  );

   -- Create a raw data table to show that a user has visited the website at a certain time:
  MogDB=# insert into facts values ('2019-02-20', generate_series(1,100));
  MogDB=# insert into facts values ('2019-02-21', generate_series(1,200));
  MogDB=# insert into facts values ('2019-02-22', generate_series(1,300));
  MogDB=# insert into facts values ('2019-02-23', generate_series(1,400));
  MogDB=# insert into facts values ('2019-02-24', generate_series(1,500));
  MogDB=# insert into facts values ('2019-02-25', generate_series(1,600));
  MogDB=# insert into facts values ('2019-02-26', generate_series(1,700));
  MogDB=# insert into facts values ('2019-02-27', generate_series(1,800));

  -- Create another table and specify an HLL column:
  MogDB=# create table daily_uniques (
      date            date UNIQUE,
      users           hll
  );

  -- Group data by date and insert the data into the HLL:
  MogDB=# insert into daily_uniques(date, users)
      select date, hll_add_agg(hll_hash_integer(user_id))
      from facts
      group by 1;

  -- Calculate the numbers of users visiting the website every day:
  MogDB=# select date, hll_cardinality(users) from daily_uniques order by date;
      date    | hll_cardinality
  ------------+------------------
   2019-02-20 |              100
   2019-02-21 | 200.217913059312
   2019-02-22 |  301.76494508014
   2019-02-23 | 400.862858326446
   2019-02-24 | 502.626933349694
   2019-02-25 | 601.922606454213
   2019-02-26 | 696.602316769498
   2019-02-27 | 798.111731634412
  (8 rows)

  -- Calculate the number of users who had visited the website in the week from February 20, 2019 to February 26, 2019:
  MogDB=# select hll_cardinality(hll_union_agg(users)) from daily_uniques where date >= '2019-02-20'::date and date <= '2019-02-26'::date;
   hll_cardinality
  ------------------
   702.941844662509
  (1 row)

  -- Calculate the number of users who had visited the website yesterday but have not visited the website today:
  MogDB=# SELECT date, (#hll_union_agg(users) OVER two_days) - #users AS lost_uniques FROM daily_uniques WINDOW two_days AS (ORDER BY date ASC ROWS 1 PRECEDING);
      date    | lost_uniques
  ------------+--------------
   2019-02-20 |            0
   2019-02-21 |            0
   2019-02-22 |            0
   2019-02-23 |            0
   2019-02-24 |            0
   2019-02-25 |            0
   2019-02-26 |            0
   2019-02-27 |            0
  (8 rows)

  -- Delete the table.
  MogDB=# drop table facts;
  MogDB=# drop table daily_uniques;
  ```

- Scenario 3: The data to be inserted does not meet the requirements of the HLL data structure.

  When inserting data into a column of the HLL type, ensure that the data meets the requirements of the HLL data structure. If the data does not meet the requirements after being parsed, an error will be reported. In the following example, **E\\1234** to be inserted does not meet the requirements of the HLL data structure after being parsed. As a result, an error is reported.

  ```sql
  MogDB=# create table test(id integer, set hll);
  MogDB=# insert into test values(1, 'E\\1234');
  ERROR:  not a hll type, size=6 is not enough
  MogDB=# drop table test;
  ```
