---
title: Character Data Types
summary: Character Data Types
author: Guo Huan
date: 2021-04-06
---

# Character Data Types

Table 1 lists the character data types supported by MogDB. For string operators and related built-in functions, see [Character Processing Functions and Operators](../../reference-guide/functions-and-operators/3-character-processing-functions-and-operators.md).

**Table 1** Character data types

| Name                                  | Description                                                  | Storage Space                                                |
| :------------------------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| CHAR(n)<br/>CHARACTER(n)<br/>NCHAR(n) | Fixed-length character string, blank padded. **n** indicates the string length. If it is not specified, the default precision **1** is used. | The maximum size is 10 MB.                                   |
| VARCHAR(n)<br/>CHARACTER VARYING(n)   | Variable-length string. **n** indicates the string length. In PostgreSQL-compatible mode, **n** indicates the string length. In other compatibility modes, **n** indicates the byte length. | The maximum size is 10 MB.                                   |
| VARCHAR2(n)                           | Variable-length string. It is the alias of the VARCHAR(n) type. **n** indicates the string length. | The maximum size is 10 MB.                                   |
| NVARCHAR(n)                           | Variable-length string. It is the alias of the NVARCHAR2(n) type. **n** indicates the string length. | The maximum size is 10 MB.                                   |
| NVARCHAR2(n)                          | Variable-length string. **n** indicates the string length.   | The maximum size is 10 MB.                                   |
| TEXT                                  | Variable-length string.                                      | The maximum size is 1 GB minus 1 byte. However, the size of the column description header and the size of the tuple (less than 1 GB minus 1 byte) where the column is located must also be considered. Therefore, the maximum size of the TEXT type may be less than 1 GB minus 1 byte. |
| CLOB                                  | A big text object. It is the alias of the TEXT type.         | The maximum size is 4 GB minus 1 byte. However, the size of the column description header and the size of the tuple (less than 4 GB minus 1 byte) where the column is located must also be considered. Therefore, the maximum size of the CLOB type may be less than 4 GB minus 1 byte. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> 1. In addition to the restriction on the size of each column, the total size of each tuple cannot exceed 1 GB minus 1 byte and is affected by the control header information of the column, the control header information of the tuple, and whether null fields exist in the tuple.
> 2. NCHAR is the alias of the bpchar type, and NCHAR(n) is the alias of the VARCHAR(n) type.
> 3. Only advanced packages related to dbe_lob support CLOBs whose size is greater than 1 GB. System functions do not support CLOBs whose size is greater than 1 GB.

In MogDB, there are two other fixed-length character types, as shown in Table 2. The **name** type exists only for the storage of identifiers in the internal system catalogs and is not intended for use by general users. Its length is currently defined as 64 bytes (63 usable characters plus terminator). The type **"char"** only uses one byte of storage. It is internally used in the system catalogs as a simplistic enumeration type.

**Table 2** Special character types

| Name   | Description                    | Storage Space |
| :----- | :----------------------------- | :------------ |
| name   | Internal type for object names | 64 bytes      |
| "char" | Single-byte internal type      | 1 byte        |

## Example

```sql
-- Create a table.
MogDB=# CREATE TABLE char_type_t1
(
    CT_COL1 CHARACTER(4)
);

-- Insert data.
MogDB=# INSERT INTO char_type_t1 VALUES ('ok');

-- Query data in the table.
MogDB=# SELECT ct_col1, char_length(ct_col1) FROM char_type_t1;
 ct_col1 | char_length
---------+-------------
 ok      |           4
(1 row)

-- Delete the table.
MogDB=# DROP TABLE char_type_t1;
```

```sql
-- Create a table.
MogDB=# CREATE TABLE char_type_t2
(
    CT_COL1 VARCHAR(5)
);

-- Insert data.
MogDB=# INSERT INTO char_type_t2 VALUES ('ok');

MogDB=# INSERT INTO char_type_t2 VALUES ('good');

-- Specify the type length. An error is reported if an inserted string exceeds this length.
MogDB=# INSERT INTO char_type_t2 VALUES ('too long');
ERROR:  value too long for type character varying(5)
CONTEXT:  referenced column: ct_col1

-- Specify the type length. A string exceeding this length is truncated.
MogDB=# INSERT INTO char_type_t2 VALUES ('too long'::varchar(5));

-- Query data.
MogDB=# SELECT ct_col1, char_length(ct_col1) FROM char_type_t2;
 ct_col1 | char_length
---------+-------------
 ok      |           2
 good    |           4
 too l   |           5
(3 rows)

-- Delete data.
MogDB=# DROP TABLE char_type_t2;
```
