---
title: Data Type Used by the Ledger Database
summary: Data Type Used by the Ledger Database
author: Zhang Cuiping
date: 2021-10-25
---

# Data Type Used by the Ledger Database

The ledger database uses the hash16 data type to store row-level hash digests or table-level hash digests, and uses the hash32 data type to store global hash digests or history table verification hashes.

**Table 1** Hash type of the ledger database

| Name   | Description         | Storage Space | Value Range                     |
| :---------- | :------------------------ | :------------ | :-------------------------------- |
| HASH16 | Stored as an unsigned 64-bit integer              | 8 bytes       | 0 to +18446744073709551615                              |
| HASH32 | Stored as a group of 16 unsigned integer elements | 16 bytes      | Value range of an unsigned integer array of 16 elements |

The hash16 data type is used to store row-level or table-level hash digests in the ledger database. After obtaining the hash sequence of a 16-character hexadecimal string, the system invokes the **hash16in** function to convert the sequence into an unsigned 64-bit integer and stores the integer in a hash16 variable. For example:

```sql
Hexadecimal string: e697da2eaa3a775b; 64-bit unsigned integer: 16615989244166043483
Hexadecimal string: ffffffffffffffff; 64-bit unsigned integer: 18446744073709551615
```

The hash32 data type is used to store the global hash digest or history table verification hash in the ledger database. After obtaining the hash sequence of a 32-character hexadecimal string, the system invokes the **hash32in** function to convert the sequence to an array containing 16 unsigned integer elements. For example:

```sql
Hexadecimal string: 685847ed1fe38e18f6b0e2b18c00edee
Hash32 array: [104,88,71,237,31,227,142,24,246,176,226,177,140,0,237,238]
```
