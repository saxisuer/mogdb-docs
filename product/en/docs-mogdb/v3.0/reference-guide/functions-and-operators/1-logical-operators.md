---
title: Logical Operators
summary: Logical Operators
author: Zhang Cuiping
date: 2021-04-20
---

# Logical Operators

The usual logical operators include AND, OR, and NOT. SQL uses a three-valued logical system with true, false, and null, which represents "unknown". Their priorities are NOT &gt; AND &gt; OR.

[Table 1](#Operation rules) lists the calculation rules, where a and b represent logical expressions.

**Table 1** Operation rules<a id="Operation rules"> </a>

| a     | b     | a **AND** b Result | a **OR** b Result | **NOT** a Result |
| :---- | :---- | :----------------- | :---------------- | :--------------- |
| TRUE  | TRUE  | TRUE               | TRUE              | FALSE            |
| TRUE  | FALSE | FALSE              | TRUE              | FALSE            |
| TRUE  | NULL  | NULL               | TRUE              | FALSE            |
| FALSE | FALSE | FALSE              | FALSE             | TRUE             |
| FALSE | NULL  | FALSE              | NULL              | TRUE             |
| NULL  | NULL  | NULL               | NULL              | NULL             |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> The operators AND and OR are commutative, that is, you can switch the left and right operand without affecting the result.
