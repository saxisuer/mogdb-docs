---
title: Internal Functions
summary: Internal Functions
author: Zhang Cuiping
date: 2021-06-15
---

# Internal Functions

The following functions of MogDB use internal data types, which cannot be directly called by users.

- Selection rate calculation functions

  | areajoinsel      | areasel         | arraycontjoinsel | arraycontsel   | contjoinsel    | contsel          | eqjoinsel    |
  | ---------------- | --------------- | ---------------- | -------------- | -------------- | ---------------- | ------------ |
  | eqsel            | iclikejoinsel   | iclikesel        | icnlikejoinsel | icnlikesel     | icregexeqjoinsel | icregexeqsel |
  | icregexnejoinsel | icregexnesel    | likejoinsel      | likesel        | neqjoinsel     | neqsel           | nlikejoinsel |
  | nlikesel         | positionjoinsel | positionsel      | regexeqjoinsel | regexeqsel     | regexnejoinsel   | regexnesel   |
  | scalargtjoinsel  | scalargtsel     | scalarltjoinsel  | scalarltsel    | tsmatchjoinsel | tsmatchsel       | -            |

- Statistics collection functions

  | array_typanalyze | range_typanalyze | ts_typanalyze |
  | ---------------- | ---------------- | ------------- |
  | local_rto_stat   |                  |               |

- Internal functions for sorting

  | bpchar_sortsupport | bytea_sortsupport | date_sortsupport | numeric_sortsupport | timestamp_sortsupport |
  | ------------------ | ----------------- | ---------------- | ------------------- | --------------------- |

- Internal functions for full-text retrieval

  | dispell_init    | dispell_lexize    | dsimple_init        | dsimple_lexize      | dsnowball_init   | dsnowball_lexize   | dsynonym_init |
  | --------------- | ----------------- | ------------------- | ------------------- | ---------------- | ------------------ | ------------- |
  | dsynonym_lexize | gtsquery_compress | gtsquery_consistent | gtsquery_decompress | gtsquery_penalty | gtsquery_picksplit | gtsquery_same |
  | gtsquery_union  | ngram_end         | ngram_lextype       | ngram_start         | pound_end        | pound_lextype      | pound_start   |
  | prsd_end        | prsd_headline     | prsd_lextype        | prsd_start          | thesaurus_init   | thesaurus_lexize   | zhprs_end     |
  | zhprs_getlexeme | zhprs_lextype     | zhprs_start         | -                   | -                | -                  | -             |

- Internal type processing functions

  | abstimerecv                       | euc_jis_2004_to_utf8 | int2recv              | line_recv          | oidvectorrecv_extend           | tidrecv                 | utf8_to_koi8u          |
  | --------------------------------- | -------------------- | --------------------- | ------------------ | ------------------------------ | ----------------------- | ---------------------- |
  | anyarray_recv                     | euc_jp_to_mic        | int2vectorrecv        | lseg_recv          | path_recv                      | time_recv               | utf8_to_shift_jis_2004 |
  | array_recv                        | euc_jp_to_sjis       | int4recv              | macaddr_recv       | pg_node_tree_recv              | time_transform          | utf8_to_sjis           |
  | ascii_to_mic                      | euc_jp_to_utf8       | int8recv              | mic_to_ascii       | point_recv                     | timestamp_recv          | utf8_to_uhc            |
  | ascii_to_utf8                     | euc_kr_to_mic        | internal_out          | mic_to_big5        | poly_recv                      | timestamp_transform     | utf8_to_win            |
  | big5_to_euc_tw                    | euc_kr_to_utf8       | interval_recv         | mic_to_euc_cn      | pound_nexttoken                | timestamptz_recv        | uuid_recv              |
  | big5_to_mic                       | euc_tw_to_big5       | interval_transform    | mic_to_euc_jp      | prsd_nexttoken                 | timetz_recv             | varbit_recv            |
  | big5_to_utf8                      | euc_tw_to_mic        | iso_to_koi8r          | mic_to_euc_kr      | range_recv                     | tintervalrecv           | varbit_transform       |
  | bit_recv                          | euc_tw_to_utf8       | iso_to_mic            | mic_to_euc_tw      | rawrecv                        | tsqueryrecv             | varchar_transform      |
  | boolrecv                          | float4recv           | iso_to_win1251        | mic_to_iso         | record_recv                    | tsvectorrecv            | varcharrecv            |
  | box_recv                          | float8recv           | iso_to_win866         | mic_to_koi8r       | regclassrecv                   | txid_snapshot_recv      | void_recv              |
  | bpcharrecv                        | gb18030_to_utf8      | iso8859_1_to_utf8     | mic_to_latin1      | regconfigrecv                  | uhc_to_utf8             | win_to_utf8            |
  | btoidsortsupport                  | gbk_to_utf8          | iso8859_to_utf8       | mic_to_latin2      | regdictionaryrecv              | unknownrecv             | win1250_to_latin2      |
  | bytearecv                         | gin_extract_tsvector | johab_to_utf8         | mic_to_latin3      | regoperatorrecv                | utf8_to_ascii           | win1250_to_mic         |
  | byteawithoutorderwithequalcolrecv | gtsvector_compress   | json_recv             | mic_to_latin4      | regoperrecv                    | utf8_to_big5            | win1251_to_iso         |
  | cash_recv                         | gtsvector_consistent | koi8r_to_iso          | mic_to_sjis        | regprocedurerecv               | utf8_to_euc_cn          | win1251_to_koi8r       |
  | charrecv                          | gtsvector_decompress | koi8r_to_mic          | mic_to_win1250     | regprocrecv                    | utf8_to_euc_jis_2004    | win1251_to_mic         |
  | cidr_recv                         | gtsvector_penalty    | koi8r_to_utf8         | mic_to_win1251     | regtyperecv                    | utf8_to_euc_jp          | win1251_to_win866      |
  | cidrecv                           | gtsvector_picksplit  | koi8r_to_win1251      | mic_to_win866      | reltimerecv                    | utf8_to_euc_kr          | win866_to_iso          |
  | circle_recv                       | gtsvector_same       | koi8r_to_win866       | namerecv           | shift_jis_2004_to_euc_jis_2004 | utf8_to_euc_tw          | win866_to_koi8r        |
  | cstring_recv                      | gtsvector_union      | koi8u_to_utf8         | ngram_nexttoken    | shift_jis_2004_to_utf8         | utf8_to_gb18030         | win866_to_mic          |
  | date_recv                         | hll_recv             | latin1_to_mic         | numeric_recv       | sjis_to_euc_jp                 | utf8_to_gbk             | win866_to_win1251      |
  | domain_recv                       | hll_trans_recv       | latin2_to_mic         | numeric_transform  | sjis_to_mic                    | utf8_to_iso8859         | xidrecv                |
  | euc_cn_to_mic                     | hstore_recv          | latin2_to_win1250     | nvarchar2recv      | sjis_to_utf8                   | utf8_to_iso8859_1       | xidrecv4               |
  | euc_cn_to_utf8                    | inet_recv            | latin3_to_mic         | oidrecv            | smalldatetime_recv             | utf8_to_johab           | xml_recv               |
  | euc_jis_2004_to_shift_jis_2004    | int1recv             | latin4_to_mic         | oidvectorrecv      | textrecv                       | utf8_to_koi8r           | cstore_tid_out         |
  | i16toi1                           | int16                | int16_bool            | int16eq            | int16div                       | int16ge                 | int16gt                |
  | int16in                           | int16le              | int16lt               | int16mi            | int16mul                       | int16ne                 | int16out               |
  | int16pl                           | int16recv            | int16send             | numeric_bool       | int2vectorin_extend            | int2vectorout_extend    | int2vectorrecv_extend  |
  | int2vectorsend_extend             | jsonb_in             | jsonb_out             | jsonb_recv         | jsonb_send                     | complex_array_in        | bool_int1              |
  | bool_int2                         | bool_int8            | bpchar_float4         | bpchar_float8      | bpchar_int4                    | bpchar_int8             | bpchar_numeric         |
  | bpchar_timestamp                  | f4toi1               | f8toi1                | float4_bpchar      | float4_text                    | float4_varchar          | float8_bpchar          |
  | float8_text                       | float8_varchar       | i1tof4                | i1tof8             | i1toi2                         | i1toi4                  | i1toi8                 |
  | i2toi1                            | i4toi1               | i8toi1                | int1_avg_accum     | int1_bool                      | int1_bpchar             | int1_numeric           |
  | int1_nvarchar2                    | int1_text            | int1_varchar          | int1abs            | int1and                        | int1cmp                 | int1div                |
  | int1eq                            | int1ge               | int1gt                | int1in             | int1inc                        | int1larger              | int1le                 |
  | int1lt                            | int1mi               | int1mod               | int1mul            | int1ne                         | int1not                 | int1or                 |
  | int1out                           | int1pl               | int1shl               | int1shr            | int1smaller                    | int1um                  | int1up                 |
  | int1xor                           | int2_bool            | int2_bpchar           | int2_text          | int2_varchar                   | int4_bpchar             | int4_text              |
  | int4_varchar                      | int8_bool            | int8_bpchar           | int8_text          | int8_varchar                   | job_submit              | numeric_bpchar         |
  | numeric_int1                      | numeric_text         | numeric_varchar       | nvarchar2in        | nvarchar2out                   | nvarchar2send           | oidvectorin_extend     |
  | oidvectorout_extend               | oidvectorsend_extend | rawcmp                | raweq              | rawge                          | rawgt                   | rawin                  |
  | rawle                             | rawlike              | rawlt                 | rawne              | rawnlike                       | rawout                  | rawsend                |
  | text_float4                       | text_float8          | text_int1             | text_int2          | text_int4                      | text_int8               | text_numeric           |
  | timestamp_text                    | timestamp_varchar    | varchar_float4        | varchar_float8     | varchar_int4                   | varchar_int8            | varchar_numeric        |
  | xidout4                           | xidsend4             | calculate_quantile_of | calculate_value_at | large_seq_rollback_ntree       | large_seq_upgrade_ntree | -                      |

- Internal functions for aggregation operations

  | array_agg_finalfn            | array_agg_transfn              | bytea_string_agg_finalfn         | bytea_string_agg_transfn     | date_list_agg_noarg2_transfn      | date_list_agg_transfn        | float4_list_agg_noarg2_transfn      |
  | ---------------------------- | ------------------------------ | -------------------------------- | ---------------------------- | --------------------------------- | ---------------------------- | ----------------------------------- |
  | float4_list_agg_transfn      | float8_list_agg_noarg2_transfn | float8_list_agg_transfn          | int2_list_agg_noarg2_transfn | int2_list_agg_transfn             | int4_list_agg_noarg2_transfn | int4_list_agg_transfn               |
  | int8_list_agg_noarg2_transfn | int8_list_agg_transfn          | interval_list_agg_noarg2_transfn | interval_list_agg_transfn    | list_agg_finalfn                  | list_agg_noarg2_transfn      | list_agg_transfn                    |
  | median_float8_finalfn        | median_interval_finalfn        | median_transfn                   | mode_final                   | numeric_list_agg_noarg2_transfn   | numeric_list_agg_transfn     | ordered_set_transition              |
  | percentile_cont_float8_final | percentile_cont_interval_final | string_agg_finalfn               | string_agg_transfn           | timestamp_list_agg_noarg2_transfn | timestamp_list_agg_transfn   | timestamptz_list_agg_noarg2_transfn |
  | timestamptz_list_agg_transfn | checksumtext_agg_transfn       | -                                | -                            | -                                 | -                            | -                                   |
  | json_agg_finalfn             | json_agg_transfn               | json_object_agg_finalfn          | json_object_agg_transfn      | -                                 | -                            | -                                   |

- Hash internal functions

  | hashbeginscan | hashbuild  | hashbuildempty | hashbulkdelete | hashcostestimate | hashendscan  | hashgetbitmap     |
  | ------------- | ---------- | -------------- | -------------- | ---------------- | ------------ | ----------------- |
  | hashgettuple  | hashinsert | hashmarkpos    | hashmerge      | hashrescan       | hashrestrpos | hashvacuumcleanup |
  | hashvarlena   | -          | -              | -              | -                | -            | -                 |

- Internal functions of the B-tree index

  | cbtreebuild  | cbtreecanreturn   | cbtreecostestimate | cbtreegetbitmap   | cbtreegettuple    | btbeginscan         | btbuild             |
  | ------------ | ----------------- | ------------------ | ----------------- | ----------------- | ------------------- | ------------------- |
  | btbuildempty | btbulkdelete      | btcanreturn        | btcostestimate    | btendscan         | btfloat4sortsupport | btfloat8sortsupport |
  | btgetbitmap  | btgettuple        | btinsert           | btint2sortsupport | btint4sortsupport | btint8sortsupport   | btmarkpos           |
  | btmerge      | btnamesortsupport | btrescan           | btrestrpos        | bttextsortsupport | btvacuumcleanup     | cbtreeoptions       |

- Internal functions of the GiST index

  | gist_box_compress         | gist_box_consistent      | gist_box_decompress  | gist_box_penalty          | gist_box_picksplit       | gist_box_same      | gist_box_union       |
  | ------------------------- | ------------------------ | -------------------- | ------------------------- | ------------------------ | ------------------ | -------------------- |
  | gist_circle_compress      | gist_circle_consistent   | gist_point_compress  | gist_point_consistent     | gist_point_distance      | gist_poly_compress | gist_poly_consistent |
  | gistbeginscan             | gistbuild                | gistbuildempty       | gistbulkdelete            | gistcostestimate         | gistendscan        | gistgetbitmap        |
  | gistinsert                | gistmarkpos              | gistmerge            | gistrescan                | gistrestrpos             | gistvacuumcleanup  | range_gist_compress  |
  | range_gist_decompress     | range_gist_penalty       | range_gist_picksplit | range_gist_same           | range_gist_union         | spg_kd_choose      | spg_kd_config        |
  | spg_kd_picksplit          | spg_quad_choose          | spg_quad_config      | spg_quad_inner_consistent | spg_quad_leaf_consistent | spg_quad_picksplit | spg_text_choose      |
  | spg_text_inner_consistent | spg_text_leaf_consistent | spg_text_picksplit   | spgbeginscan              | spgbuild                 | spgbuildempty      | spgbulkdelete        |
  | spgcostestimate           | spgendscan               | spggetbitmap         | spggettuple               | spginsert                | spgmarkpos         | spgmerge             |
  | spgrestrpos               | spgvacuumcleanup         | -                    | -                         | -                        | -                  | -                    |

- Internal functions of the GIN index

  | gin_cmp_prefix    | gin_extract_tsquery  | gin_tsquery_consistent    | gin_tsquery_triconsistent               | ginarrayconsistent | ginarrayextract         | ginarraytriconsistent   |
  | ----------------- | -------------------- | ------------------------- | --------------------------------------- | ------------------ | ----------------------- | ----------------------- |
  | ginbeginscan      | ginbuild             | ginbuildempty             | ginbulkdelete                           | gincostestimate    | ginendscan              | gingetbitmap            |
  | gininsert         | ginmarkpos           | ginmerge                  | ginqueryarrayextract                    | ginrescan          | ginrestrpos             | ginvacuumcleanup        |
  | cginbuild         | cgingetbitmap        | -                         | -                                       | -                  | -                       | -                       |
  | gin_compare_jsonb | gin_consistent_jsonb | gin_consistent_jsonb_hash | gin_extract_jsonbgin_extract_jsonb_hash | cginbuild          | gin_extract_jsonb_query | gin_triconsistent_jsonb |

- Internal functions of the Psort index

  | psortbuild | psortcanreturn | psortcostestimate | psortgetbitmap | psortgettuple |
  | ---------- | -------------- | ----------------- | -------------- | ------------- |

- Internal functions of the UBTree index

  | ubtbeginscan     | ubtbuild   | ubtbuildempty | ubtbulkdelete | ubtcanreturn |
  | ---------------- | ---------- | ------------- | ------------- | ------------ |
  | ubtcostestimate  | ubtendscan | ubtgetbitmap  | ubtgettuple   | ubtinsert    |
  | ubtmarkpos       | ubtmerge   | ubtoptions    | ubtrescan     | ubtrestrpos  |
  | ubtvacuumcleanup |            |               |               |              |

- plpgsql internal function

  plpgsql_inline_handler

- Set-related internal functions

  | array_indexby_delete | array_indexby_length | array_integer_deleteidx | array_integer_exists | array_integer_first | array_integer_last |
  | -------------------- | -------------------- | ----------------------- | -------------------- | ------------------- | ------------------ |
  | array_integer_next   | array_integer_prior  | array_varchar_deleteidx | array_varchar_exists | array_varchar_first | array_varchar_last |
  | array_varchar_next   | array_varchar_prior  | -                       | -                    | -                   | -                  |

- External table-related internal functions

  | dist_fdw_handler | roach_handler | streaming_fdw_handler | dist_fdw_validator | file_fdw_handler | file_fdw_validator | log_fdw_handler |
  | ---------------- | ------------- | --------------------- | ------------------ | ---------------- | ------------------ | --------------- |

- Auxiliary function for the primary database node to remotely read the data page from the standby database node.

  **gs_read_block_from_remote** is used to read the pages of a non-segment-page table file. By default, only the initial user can view the data. Other users can use the data only after being granted with permissions.

  **gs_read_segment_block_from_remote** is used to read the pages of a segment-page table file. By default, only the initial user can view the data. Other users can use the data only after being granted with permissions.

- Auxiliary function for the primary database node to remotely read the data file from the standby database node.

  The **gs_read_file_from_remote** command is used to read a specified file. After obtaining the file size using the **gs_read_file_size_from_remote** function, **gs_repair_file** reads the remote file segment by segment using this function. By default, only the initial user can view the data. Other users can use the data only after being granted with permissions.

  The **gs_read_file_size_from_remote** command is used to read the size of a specified file. This command is used to read the size of a specified file. Before using the **gs_repair_file** function to repair a file, you need to obtain the size of the file from the remote end to verify the missing file information and repair the missing files one by one. By default, only the initial user can view the data. Other users can use the data only after being granted with permissions.

- Ledger database functions

  get_dn_hist_relhash

- AI feature functions

  | create_snapshot         | create_snapshot_internal | prepare_snapshot_internal | prepare_snapshot | manage_snapshot_internal | archive_snapshot | publish_snapshot |
  | ----------------------- | ------------------------ | ------------------------- | ---------------- | ------------------------ | ---------------- | ---------------- |
  | purge_snapshot_internal | purge_snapshot           | sample_snapshot           | -                | -                        | -                | -                |

  - Functions of **PKG_SERVICE**

  | isubmit_on_nodes | submit_on_nodes | -    | -    | -    | -    | -    |
  | ---------------- | --------------- | ---- | ---- | ---- | ---- | ---- |

- Other functions

  | to_tsvector_for_batch      | value_of_percentile            | session_context   | bind_variable               | job_update         | job_cancel        | job_finish                 |
  | -------------------------- | ------------------------------ | ----------------- | --------------------------- | ------------------ | ----------------- | -------------------------- |
  | similar_escape             | table_skewness (unavailable)   | timetz_text       | time_text                   | reltime_text       | abstime_text      | _pg_keysequal              |
  | analyze_query(unavailable) | analyze_workload (unavailable) | ssign_table_type  | gs_comm_proxy_thread_status | gs_txid_oldestxmin | pg_cancel_session | pg_stat_segment_space_info |
  | remote_segment_space_info  | set_cost_params                | set_weight_params | start_collect_workload      | tdigest_in         | tdigest_merge     | tdigest_merge_to_one       |
  | tdigest_mergep             | tdigest_out                    | pg_get_delta_info | disable_conn                | -                  | -                 | -                          |
