---
title: Other Functions
summary: Other Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Other Functions

- plan_seed()

  Description: Obtains the seed value of the previous query statement (internal use).

  Return type: int

- pg_stat_get_env()

  Description: Obtains the environment variable information of the current node. Only users with the **sysadmin** or **monitor admin** permission can access the environment variable information.

  Return type: record

  Example:

  ```sql
  MogDB=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/GaussDB_Kernel_TRUNK/install,/data1/GaussDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  Description: Clears the global plan cache that is not used on nodes.

  Return type: Boolean

- pg_catalog.plancache_status()

  Description: Displays information about the global plan cache on nodes. The information returned by the function is the same as that in GLOBAL_PLANCACHE_STATUS.

  Return type: record

- textlen(text)

  Description: Provides the method of querying the logical length of text.

  Return type: int

- threadpool_status()

  Description: Displays the status of worker threads and sessions in the thread pool.

  Return type: record

- get_local_active_session()

  Description: Provides sampling records of the historical active sessions stored in the memory of the current node.

  Return type: record

- pg_stat_get_thread()

  Description: Provides status information about all threads on the current node. Users with the **sysadmin** or **monitor admin** permission can view information about all threads, and common users can view only their own thread information.

  Return type: record

- pg_stat_get_sql_count()

  Description: Provides the counts of the **SELECT**, **UPDATE**, **INSERT**, **DELETE**, and **MERGE INTO** statements executed on the current node. Users with the **sysadmin** or **monitor admin** permission can view information about all users, and common users can view only their own statistics.

  Return type: record

- pg_stat_get_data_senders()

  Description: Provides detailed information about the data-copy sending thread active at the moment.

  Return type: record

- get_wait_event_info()

  Description: Provides detailed information about the wait event.

  Return type: record

- generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring)

  Description: Generates system diagnosis reports based on two snapshots. You need to run the command in the **postgres** database. By default, the initial user or the user with the **monadmin** permission can access the database. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 1** generate_wdr_report parameter description

  | Parameter     | Description                                                  | Range                                                        |
  | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | begin_snap_id | Snapshot ID that starts the diagnosis report period.         | N/A                                                          |
  | end_snap_id   | Snapshot ID that ends the diagnosis report period. By default, the value of **end_snap_id** is greater than that of **begin_snap_id**. | N/A                                                          |
  | report_type   | Specifies the type of the generated report.                  | - **summary**<br/>- **detail**<br/>- **all**: Both **summary** and **detail** types are included. |
  | report_scope  | Specifies the scope for a report to be generated.            | - **cluster**: database-level information<br/>- **node**: node-level information |
  | node_name     | When **report\_scope** is set to **node**, set this parameter to the name of the corresponding node. (You can run the **select \* from pg_node_env;** command to query the node name.)If **report\_scope** is set to **cluster**, this parameter can be omitted, left blank, or set to **NULL**. | - **cluster**: This value is omitted, left blank or set to **NULL**.<br/>- **node**: indicates the node name in MogDB. |

- create_wdr_snapshot()

  Description: Manually generates system diagnosis snapshots. This function requires the **sysadmin** permission.

  Return type: text

- kill_snapshot()

  Description: Kills the WDR snapshot backend thread. Users who invoke this function must have the **SYSADMIN** permission, the **REPLICATION** permission, or inherit the **gs_role_replication** permission of the built-in role.

  Return type: void

- capture_view_to_json(text,integer)

  Description: Saves the view result to the directory specified by GUC: **perf_directory**. If **is_crossdb** is set to **1**, the view is accessed once for all databases. If the value of **is_crossdb** is **0**, the current database is accessed only once. Only users with the **sysadmin** or **monitor admin** permission can execute this function.

  Return type: int

- reset_unique_sql

  Description: Clears the unique SQL statements in the memory of the database node. (The **sysadmin** permission is required.)

  Return type: Boolean

  **Table 2** reset_unique_sql parameter description

  | Parameter   | Type | Description                                                  |
  | :---------- | :--- | :----------------------------------------------------------- |
  | scope       | text | Clearance scope type. The options are as follows:<br/>- **GLOBAL**: Clears all nodes. If the value is **GLOBAL**, this function can be executed only on the primary node.<br/>- **LOCAL**: Clears the current node. |
  | clean_type  | text | - BY_USERID: Unique SQL statements are cleared based on user IDs.<br/>- BY_CNID: Unique SQL statements are cleared based on primary node IDs.<br/>- **ALL**: All data is cleared. |
  | clean_value | int8 | Clearance value corresponding to the clearance type.         |

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  >
  > - The values **GLOBAL** and **LOCAL** of **scope** apply to distributed databases. For MogDB, the values have the same meaning, indicating that the local node is cleared.
  > - The value **BY_CNID** of **clean_type** applies only to distributed databases and is invalid for MogDB.

- wdr_xdb_query(db_name_str text, query text)

  Description: Provides the capability of executing local cross-database queries. For example, when connecting to the Postgres database, access tables in the **test** database.

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  Return type: record

- pg_wlm_jump_queue(pid int)

    Description: Moves a task to the top of the queue of the primary node of the database.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- gs_wlm_switch_cgroup(pid int, cgroup text)

    Description: Moves a job to another Cgroup to change the job priority.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

    Description: Records information about the memory context **MemoryContextName** of the thread **tid** into the *threadid***_timestamp.log** file in the *$GAUSSLOG***/pg_log/***${node_name}***/dumpmem** directory. *threadid* can be obtained from *sessid* in the **GS_SESSION_MEMORY_DETAIL** view. In the officially released version, only the **MemoryContextName** that is an empty string (two single quotation marks indicate that the input is an empty string) is accepted. In this case, all memory context information is recorded. Otherwise, no operation is performed. For the DEBUG version for internal development and test personnel to debug, you can specify the **MemoryContextName** to be counted. In this case, all the memory usage of the context is recorded in the specified file. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pg_shared_memctx_detail(MemoryContextName text)

    Description: Records information about the memory context **MemoryContextName** into the *threadid***_timestamp.log** file in the *$GAUSSLOG***/pg_log/***${node_name}***/dumpmem** directory. This function is provided only for internal development and test personnel to debug in the DEBUG version. Calling this function in the officially released version does not involve any operation. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- local_bgwriter_stat()

  Description: Displays the information about pages flushed by the bgwriter thread of this instance, number of pages in the candidate buffer chain, and buffer elimination information.

  Return type: record

- local_candidate_stat()

  Description: Displays the number of pages in the candidate buffer chain of this instance and buffer elimination information, including the normal buffer pool and segment buffer pool.

  Return type: record

- local_ckpt_stat()

  Description: Displays the information about checkpoints and flushing pages of the current instance.

  Return type: record

- local_double_write_stat()

  Description: Displays the doublewrite file status of the current instance.

  Return type: record

  **Table 3** local_double_write_stat parameters

  | Parameter             | Type | Description                                                  |
  | :-------------------- | :--- | :----------------------------------------------------------- |
  | node_name             | text | Instance name                                                |
  | curr_dwn              | int8 | Sequence number of the doublewrite file                      |
  | curr_start_page       | int8 | Start page for restoring the doublewrite file                |
  | file_trunc_num        | int8 | Number of times that the doublewrite file is reused          |
  | file_reset_num        | int8 | Number of reset times after the doublewrite file is full     |
  | total_writes          | int8 | Total number of I/Os of the doublewrite file                 |
  | low_threshold_writes  | int8 | Number of I/Os for writing doublewrite files with low efficiency (the number of I/O flushing pages at a time is less than 16) |
  | high_threshold_writes | int8 | Number of I/Os for writing doublewrite files with high efficiency (the number of I/O flushing pages at a time is more than 421) |
  | total_pages           | int8 | Total number of pages that are flushed to the doublewrite file area |
  | low_threshold_pages   | int8 | Number of pages that are flushed with low efficiency         |
  | high_threshold_pages  | int8 | Number of pages that are flushed with high efficiency        |
  | file_id               | int8 | ID of the current doublewrite file                           |

- local_single_flush_dw_stat()

  Description: Displays the elimination of dual-write files on a single page in the instance.

  Return type: record

- local_pagewriter_stat()

  Description: Displays the page flushing information and checkpoint information of the current instance.

  Return type: record

- local_redo_stat()

  Description: Displays the replay status of the current standby instance.

  Return type: record

  Note: The returned replay status includes the current replay position and the replay position of the minimum restoration point.

- local_recovery_status()

  Description: Displays log flow control information about the primary and standby nodes.

  Return type: record

- gs_wlm_node_recover(boolean isForce)

  Description: Obtains top SQL query statement-level statistics recorded in the current memory. If the input parameter is not **0**, the information is cleared from the memory.

  Return type: record

- gs_wlm_node_clean(cstring nodename)

  Description: Clears data after the dynamic load management node is faulty. Only administrators can execute this function. This function is called by the database instance management module. You are not advised to directly call this function. This view is not supported in a centralized or standalone system.

  Return type: Boolean

- gs_cgroup_map_ng_conf(group name)

  Description: Reads the Cgroup configuration file of a specified logical database.

  Return type: record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  Description: Switches the Cgroup of a specified session.

  Return type: record

- hdfs_fdw_handler()

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- hdfs_fdw_validator(text[], oid)

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- comm_client_info()

  Description: Queries information about active client connections of a single node.

  Return type: SETOF record

- pg_sync_cstore_delta(text)

  Description: Synchronizes the delta table structure of a specified column-store table with that of the column-store primary table.

  Return type: bigint

- pg_sync_cstore_delta()

  Description: Synchronizes the delta table structure of all column-store tables with that of the column-store primary table.

  Return type: bigint

- pg_get_flush_lsn()

  Description: Returns the position of the Xlog flushed from the current node.

  Return type: text

- pg_get_sync_flush_lsn()

  Description: Returns the position of the Xlog flushed by the majority on the current node.

  Return type: text

- gs_create_log_tables()

  Description: Creates foreign tables and views for run logs and performance logs.

  Return type: void

  Example:

  ```
  MogDB=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains full SQL information at the database level. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 4** dbe_perf.get_global_full_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | :-------------- | :----------------------- | :--------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains slow SQL information at the database level. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 5** dbe_perf.get_global_slow_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | :-------------- | :----------------------- | :--------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- statement_detail_decode(detail text, format text, pretty boolean)

  Description: Parses the **details** column in a full or slow SQL statement. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: text

  **Table 6** statement_detail_decode parameter description

  | Parameter  | Type    | Description                                                  |
  | :--------- | :------ | :----------------------------------------------------------- |
  | **detail** | text    | Set of events generated by the SQL statement (unreadable).   |
  | format     | text    | Parsing output format. The value is **plaintext**.           |
  | pretty     | boolean | Whether to display the text in pretty format when **format** is set to **plaintext**. The options are as follows:The value **true** indicates that events are separated by **\n**.The value **false** indicates that events are separated by commas (,). |

- get_prepared_pending_xid

  Description: Returns nextxid when restoration is complete.

  Parameter: nan

  Return type: text

- pg_clean_region_info

  Description: Clears the regionmap.

  Parameter: nan

  Return type: character varying

- pg_get_delta_info

  Description: Obtains delta information from a single DN.

  Parameter: rel text, schema_name text

  Return type: part_name text, live_tuple bigint, data_size bigint, and blocknum bigint

- pg_get_replication_slot_name

  Description: Obtains the slot name.

  Parameter: nan

  Return type: text

- pg_get_running_xacts

  Description: Obtains running xact.

  Parameter: **nan**

  Return type: handle integer, gxid xid, state tinyint, node text, xmin xid, vacuum boolean, timeline bigint, prepare_xid xid, pid bigint, and next_xid xid

- pg_get_variable_info

  Description: Obtains the shared memory variable *cache*.

  Parameter: nan

  Return type: node_name text, nextOid oid, nextXid xid, oldestXid xid, xidVacLimit xid, oldestXidDB oid, lastExtendCSNLogpage xid, startExtendCSNLogpage xid, nextCommitSeqNo xid, latestCompletedXid xid, and startupMaxXid xid

- pg_get_xidlimit

  Description: Obtains transaction ID information from the shared memory.

  Parameter: nan

  Return type: nextXid xid, oldestXid xid, xidVacLimit xid, xidWarnLimit xid, xidStopLimit xid, xidWrapLimit xid, and oldestXidDB oid

- get_global_user_transaction()

  Description: Returns transaction information about each user on all nodes.

  Return type: node_name name, usename name, commit_counter bigint, rollback_counter bigint, resp_min bigint, resp_max bigint, resp_avg bigint, resp_total bigint, bg_commit_counter bigint, bg_rollback_counter bigint, bg_resp_min bigint, bg_resp_max bigint, bg_resp_avg bigint, and bg_resp_total bigint

- pg_collation_for

  Description: Returns the sorting rule corresponding to the input parameter string.

  Parameter: any (Explicit type conversion is required for constants.)

  Return type: text

- pgxc_unlock_for_sp_database(name Name)

  Description: Releases a specified database lock.

  Parameter: database name

  Return type: Boolean

- pgxc_lock_for_sp_database(name Name)

  Description: Locks a specified database.

  Parameter: database name

  Return type: Boolean

- copy_error_log_create()

  Description: Creates the error table (**public.pgxc_copy_error_log**) required for creating the **COPY FROM** error tolerance mechanism.

  Return type: Boolean

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - This function attempts to create the **public.pgxc_copy_error_log** table. For details about the table, see Table 7.
  > - Create the B-tree index on the **relname** column and execute **REVOKE ALL on public.pgxc_copy_error_log FROM public** to manage permissions for the error table (the permissions are the same as those of the **COPY** statement).
  > - **public.pgxc_copy_error_log** is a row-store table. Therefore, this function can be executed and **COPY** error tolerance is available only when row-store tables can be created in the database instance. Note that after the GUC parameter **enable_hadoop_env** is enabled, row-store tables cannot be created in the database instance (the default value is **off** for MogDB).
  > - Same as the error table and the **COPY** statement, the function requires **sysadmin** or higher permissions.
  > - If the **public.pgxc_copy_error_log** table or the **copy_error_log_relname_idx** index exists before the function creates it, the function will report an error and roll back.

  **Table 7** Error table public.pgxc_copy_error_log

  | Column     | Type                     | Description                                                  |
  | :--------- | :----------------------- | :----------------------------------------------------------- |
  | relname    | character varying        | Table name in the form of *Schema name***.***Table name*     |
  | begintime  | timestamp with time zone | Time when a data format error was reported                   |
  | filename   | character varying        | Name of the source data file where a data format error occurs |
  | lineno     | bigint                   | Number of the row where a data format error occurs in a source data file |
  | rawrecord  | text                     | Raw record of a data format error in the source data file    |
  | **detail** | text                     | Error details                                                |

- dynamic_func_control(scope text, function_name text, action text, “{params}” text[])

  Description: Dynamically enables built-in functions. Currently, only full SQL statements can be dynamically enabled.

  Return type: record

  **Table 8** Parameter description of dynamic_func_control

  | Parameter     | Type   | Description                                                  |
  | :------------ | :----- | :----------------------------------------------------------- |
  | scope         | text   | Scope where the function is to be dynamically enabled. Currently, only **LOCAL** is supported. |
  | function_name | text   | Function name. Currently, only **STMT** is supported.        |
  | action        | text   | When **function_name** is set to **STMT**, the value of **action** can only be **TRACK**, **UNTRACK**, **LIST**, or **CLEAN**.<br/>- **TRACK**: records the full SQL information of normalized SQL statements.<br/>- **UNTRACK**: cancels the recording of full SQL information of normalized SQL statements.<br/>- **LIST**: lists normalized SQL information that is recorded in the current track.<br/>- **CLEAN**: cleans normalized SQL information that is recorded in the current track. |
  | params        | text[] | When **function_name** is set to **STMT**, the parameters corresponding to different actions are set as follows:<br/>- **TRACK**: **'{“Normalized SQLID”, “L0/L1/L2”}'**<br/>- **UNTRACK**: **'{“Normalized SQLID”}'**<br/>- LIST - '{}'<br/>- CLEAN - '{}' |

- gs_parse_page_bypath(path text, blocknum bigint, relation_type text, read_memory boolean)

  Description: Parses a specified table page and returns the path for storing the parsed content.

  Return type: text

  Note: Only the system administrator or O&M administrator can execute this function.

  **Table 9** gs_parse_page_bypath parameters

  | Parameter     | Type    | Description                                                  |
  | :------------ | :------ | :----------------------------------------------------------- |
  | path          | text    | - For an ordinary table or segment-page table, the relative path is *Tablespace name***/***Database OID***/***Relfilenode of the table (physical file name)*. For example, **base/16603/16394**.<br/>- You can run the **pg_relation_filepath(table_name text)** command to query the relative path of the table file.<br/>- Valid path formats are as follows:<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
  | blocknum      | bigint  | - **-1**: Information about all blocks<br/>- **0-***MaxBlockNumber*: Information about the corresponding block |
  | relation_type | text    | - **heap**: Astore table<br/>- **uheap**: Ustore table<br/>- btree_index: B-tree index<br/>- ubtree_index: UBTree index<br/>- **segment**: Segment-page |
  | read_memory   | boolean | - **false**: The system parses the page from the disk file.<br/>- **true**: The system attempts to parse the page from the shared buffer. If the page does not exist in the shared buffer, the system parses the page from the disk file. |

- gs_xlogdump_lsn(start_lsn text, end_lsn text)

  Description: Parses Xlogs within the specified LSN range and returns the path for storing the parsed content. You can use **pg_current_xlog_location()** to obtain the current Xlog position.

  Return type: text

  Parameters: LSN start position and LSN end position

  Note: Only the system administrator or O&M administrator can execute this function.

- gs_xlogdump_xid(c_xid xid)

  Description: Parses Xlogs of a specified XID and returns the path for storing the parsed content. You can use **txid_current()** to obtain the current XID.

  Parameter: XID

  Return type: text

  Note: Only the system administrator or O&M administrator can execute this function.

- gs_xlogdump_tablepath(path text, blocknum bigint, relation_type text)

  Description: Parses logs corresponding to a specified table page and returns the path for storing the parsed content.

  Return type: text

  Note: Only the system administrator or O&M administrator can execute this function.

  **Table 10** gs_xlogdump_tablepath parameters

  | Parameter     | Type   | Description                                                  |
  | :------------ | :----- | :----------------------------------------------------------- |
  | path          | text   | - For an ordinary table or segment-page table, the relative path is *Tablespace name***/***Database OID***/***Relfilenode of the table (physical file name)*. For example, **base/16603/16394**.<br/>- You can run the **pg_relation_filepath(table_name text)** command to query the relative path of the table file.<br/>- Valid path formats are as follows:<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
  | blocknum      | bigint | - **-1**: Information about all blocks<br/>- **0-***MaxBlockNumber*: Information about the corresponding block |
  | relation_type | text   | - **heap**: Astore table<br/>- **uheap**: Ustore table<br/>- btree_index: B-tree index<br/>- **ubtree_index**: UBTree index<br/>- **segment**: Segment-page |

- gs_xlogdump_parsepage_tablepath(path text, blocknum bigint, relation_type text, read_memory boolean)

  Description: Parses the specified table page and logs corresponding to the table page and returns the path for storing the parsed content. It can be regarded as one execution of **gs_parse_page_bypath** and **gs_xlogdump_tablepath**. The prerequisite for executing this function is that the table file exists. To view logs of deleted tables, call **gs_xlogdump_tablepath**.

  Return type: text

  Note: Only the system administrator or O&M administrator can execute this function.

  **Table 11** gs_xlogdump_parsepage_tablepath parameters

  | Parameter     | Type    | Description                                                  |
  | :------------ | :------ | :----------------------------------------------------------- |
  | path          | text    | - For an ordinary table or segment-page table, the relative path is *Tablespace name***/***Database OID***/***Relfilenode of the table (physical file name)*. For example, **base/16603/16394**.<br/>- You can run the **pg_relation_filepath(table_name text)** command to query the relative path of the table file.<br/>- Valid path formats are as follows:<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
  | blocknum      | bigint  | - **-1**: Information about all blocks<br/>- **0-***MaxBlockNumber*: Information about the corresponding block |
  | relation_type | text    | - **heap**: Astore table<br/>- **uheap**: Ustore table<br/>- btree_index: B-tree index<br/>- ubtree_index: UBTree index<br/>- **segment**: Segment-page |
  | read_memory   | boolean | - **false**: The system parses the page from the disk file.<br/>- **true**: The system attempts to parse the page from the shared buffer. If the page does not exist in the shared buffer, the system parses the page from the disk file. |

- gs_index_verify(Oid oid, uint32:wq blkno)

  Description: Checks whether the sequence of keys on the UBtree index page or index tree is correct.

  Return type: record

  **Table 12** gs_index_verify parameters

  | Parameter | Type   | Description                                                  |
  | :-------- | :----- | :----------------------------------------------------------- |
  | oid       | Oid    | - Index file relfilenode, which can be queried using **select relfilenode from pg_class where relname='***Index file name***'**. |
  | blkno     | uint32 | - **0**: indicates that all pages in the index tree are checked.<br/>- If the value is greater than 0, the index page whose page code is equal to the value of **blkno** is checked. |

- gs_index_recycle_queue(Oid oid, int type, uint32 blkno)

  Description: Parses the UBtree index recycling queue information.

  Return type: record

  **Table 13** gs_index_recycle_queue parameters

  | Parameter | Type   | Description                                                  |
  | :-------- | :----- | :----------------------------------------------------------- |
  | oid       | Oid    | - Index file relfilenode, which can be queried using **select relfilenode from pg_class where relname='***Index file name***'**. |
  | type      | int    | - **0**: indicates that the entire queue to be recycled is parsed.<br/>- **1**: indicates that the entire empty page queue is parsed.<br/>- **2**: indicates that a single page is parsed. |
  | blkno     | uint32 | - ID of the recycling queue page. This parameter is valid only when **type** is set to **2**. The value of **blkno** ranges from 1 to 4294967294. |

- gs_stat_wal_entrytable(int64 idx)

  Description: Exports the content of the write-ahead log insertion status table in the Xlog.

  Return type: record

  **Table 14** gs_stat_wal_entrytable parameters

  | Category         | Parameter Name | Type   | Description                                                  |
  | :--------------- | :------------- | :----- | :----------------------------------------------------------- |
  | Input parameter  | idx            | int64  | - **-1**: queries all elements in an array.<br/>- **0-***Maximum value*: content of a specific array element. |
  | Output parameter | idx            | uint64 | Records the subscripts in the corresponding array.           |
  | Output parameter | endlsn         | uint64 | Records the LSN label.                                       |
  | Output parameter | lrc            | int32  | Records the corresponding LRC.                               |
  | Output parameter | status         | uint32 | Determines whether the Xlog corresponding to the current entry has been completely copied to the WAL buffer:<br/>- **0**: Not copied.<br/>- **1**: Copied |

- gs_walwriter_flush_position()

  Description: Outputs the refresh position of write-ahead logs.

  Return type: record

  **Table 15** gs_walwriter_flush_position parameters

  | Category         | Parameter Name          | Type   | Description                                                  |
  | :--------------- | :---------------------- | :----- | :----------------------------------------------------------- |
  | Output parameter | last_flush_status_entry | int32  | Subscript index obtained after the Xlog flushes the tblEntry of the last flushed disk. |
  | Output parameter | last_scanned_lrc        | int32  | LRC obtained after the Xlog flushes the last tblEntry scanned last time. |
  | Output parameter | curr_lrc                | int32  | Latest LRC usage in the WALInsertStatusEntry status table. The LRC indicates the LRC value corresponding to the WALInsertStatusEntry when the next Xlog record is written. |
  | Output parameter | curr_byte_pos           | uint64 | The latest Xlog position after the Xlog is written to the WAL file, which is also the next Xlog insertion point. |
  | Output parameter | prev_byte_size          | uint32 | Length of the previous Xlog record.                          |
  | Output parameter | flush_result            | uint64 | Position of the current global Xlog flush.                   |
  | Output parameter | send_result             | uint64 | Xlog sending position on the current host.                   |
  | Output parameter | shm_rqst_write_pos      | uint64 | The write position of the LogwrtRqst request in the XLogCtl recorded in the shared memory. |
  | Output parameter | shm_rqst_flush_pos      | uint64 | The flush position of the LogwrtRqst request in the XLogCtl recorded in the shared memory. |
  | Output parameter | shm_result_write_pos    | uint64 | The write position of the LogwrtResult request in the XLogCtl recorded in the shared memory. |
  | Output parameter | shm_result_flush_pos    | uint64 | The flush position of the LogwrtResult request in the XLogCtl recorded in the shared memory. |
  | Output parameter | curr_time               | text   | Current time.                                                |

- gs_walwriter_flush_stat(int operation)

  Description: Collects statistics on the frequency of writing and synchronizing write-ahead logs, data volume, and Xlog file information.

  Return type: record

  **Table 16** gs_walwriter_flush_stat parameters

  | Category         | Parameter Name               | Type   | Description                                                  |
  | :--------------- | :--------------------------- | :----- | :----------------------------------------------------------- |
  | Input parameter  | operation                    | int    | - **-1**: Disable the statistics function. (Default value)<br/>- **0**: Enable the statistics function.<br/>- **1**: Query statistics.<br/>- **2**: Reset statistics. |
  | Output parameter | write_times                  | uint64 | Number of times that the Xlog calls the **write** API.       |
  | Output parameter | sync_times                   | uint64 | Number of times that the Xlog calls the **sync** API.        |
  | Output parameter | total_xlog_sync_bytes        | uint64 | Total number of backend thread requests for writing data to Xlogs. |
  | Output parameter | total_actual_xlog_sync_bytes | uint64 | Total number of Xlogs that call the **sync** API for disk flushing. |
  | Output parameter | avg_write_bytes              | uint32 | Number of Xlogs written each time the **XLogWrite** API is called. |
  | Output parameter | avg_actual_write_bytes       | uint32 | Number of Xlogs written each time the **write** API is called. |
  | Output parameter | avg_sync_bytes               | uint32 | Average number of Xlogs for each synchronization request.    |
  | Output parameter | avg_actual_sync_bytes        | uint32 | Actual Xlog amount of disk flushing by calling **sync** each time. |
  | Output parameter | total_write_time             | uint64 | Total time of calling the write operation (unit: μs).        |
  | Output parameter | total_sync_time              | uint64 | Total time of calling the sync operation (unit: μs).         |
  | Output parameter | avg_write_time               | uint32 | Average time for calling the **write** API each time (unit: μs). |
  | Output parameter | avg_sync_time                | uint32 | Average time for calling the **sync** API each time (unit: μs). |
  | Output parameter | curr_init_xlog_segno         | uint64 | ID of the latest Xlog segment file.                          |
  | Output parameter | curr_open_xlog_segno         | uint64 | ID of the Xlog segment file that is being written.           |
  | Output parameter | last_reset_time              | text   | Time when statistics were last collected.                    |
  | Output parameter | curr_time                    | text   | Current time.                                                |

- gs_comm_proxy_thread_status()

  Description: Collects statistics on data packets sent and received by the proxy communication library **comm_proxy** when a user-mode network is configured for the database instance.

  Parameter: nan

  Return type: record

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The query result of this function is displayed only when the user-mode network is deployed in a centralized environment and **enable_dfx in comm_proxy_attr** is set to **true**. In other scenarios, an error message is displayed, indicating that queries are not supported.
