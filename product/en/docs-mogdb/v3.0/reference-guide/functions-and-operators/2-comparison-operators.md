---
title: Comparison Operators
summary: Comparison Operators
author: Zhang Cuiping
date: 2021-04-20
---

# Comparison Operators

Comparison operators are available for the most data types and return Boolean values.

All comparison operators are binary operators. Only data types that are the same or can be implicitly converted can be compared using comparison operators.

Table 1 describes comparison operators provided by MogDB.

**Table 1** Comparison operators

| Operator           | Description              |
| :----------------- | :----------------------- |
| &lt;               | Less than                |
| &gt;               | Greater than             |
| &lt;=              | Less than or equal to    |
| &gt;=              | Greater than or equal to |
| =                  | Equal to                 |
| &lt;&gt;, != or ^= | Not equal to             |

Comparison operators are available for all relevant data types. All comparison operators are binary operators that returned values of Boolean type. The calculation priority of the inequality sign is higher than that of the equality sign. If the entered data is different and cannot be implicitly converted, the comparison fails. For example, an expression such as 1&lt;2&lt;3 is invalid because the less-than sign (&lt;) cannot be used to compare Boolean values and 3.
