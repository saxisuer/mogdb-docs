---
title: Communication Library Parameters
summary: Communication Library Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Communication Library Parameters

This section describes parameter settings and value ranges for communication libraries.

## tcp_keepalives_idle

**Parameter description**: Specifies the interval for transmitting keepalive signals on an OS that supports the **TCP_KEEPIDLE** socket option. If no keepalive signal is transmitted, the connection is in idle mode.

This parameter is a USERSET parameter. Set it based on instructions provided in **Table GUC Parameters**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPIDLE**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.
> - 0 selects the system default.
> - This parameter is not shared between different sessions, which means that this parameter may have different values in different sessions.
> - When showing this parameter, it is the parameter value in the current session connection, not the value of the guc copy.

**Value range:** 0 to 3600. The unit is s.

**Default value**: **0**

## tcp_keepalives_interval

**Parameter description:** Specifies the response time before retransmission on an OS that supports the **TCP_KEEPINTVL** socket option.

This parameter is a USERSET parameter. Set it based on instructions provided in **Table GUC Parameters**.

**Value range**: 0 to 180. The unit is s.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPINTVL**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.
> - 0 selects the system default.
> - This parameter is not shared between different sessions, which means that this parameter may have different values in different sessions.
> - When showing this parameter, it is the parameter value in the current session connection, not the value of the guc copy.

## tcp_keepalives_count

**Parameter description**: Specifies the number of keepalive signals that can be waited before the MogDB server is disconnected from the client on an OS that supports the **TCP_KEEPCNT** socket option.

This parameter is a USERSET parameter. Set it based on instructions provided in **Table GUC Parameters**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPCNT**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.
> - 0 selects the system default.
> - This parameter is not shared between different sessions, which means that this parameter may have different values in different sessions.
> - When showing this parameter, it is the parameter value in the current session connection, not the value of the guc copy.

**Value range**: 0 to 100. **0** indicates that the connection is immediately broken if MogDB does not receive a keepalived signal from the client.

**Default value:** **0**

## comm_proxy_attr

**Parameter description**: Specifies the parameters related to the communication proxy library.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - This parameter applies only to the centralized ARM standalone system running EulerOS 2.9.
> - This function takes effect when the thread pool is enabled, that is, **enable_thread_pool** is set to **on**.
> - When setting this parameter, you need to set the GUC parameter **local_bind_address** to the IP address of the NIC of the **libos_kni**.
> - **Parameter template**: comm_proxy_attr = '{enable_libnet:true, enable_dfx:false, numa_num:4, numa_bind:[[30,31],[62,63],[94,95],[126,127]]}'
> - Parameters that need to be configured include:
>   - **enable_libnet**: whether to enable the user-mode protocol. The options are as follows: **true** and **false**.
>   - **enable_dfx**: whether to enable the communication proxy library view. The options are as follows: **true** and **false**.
>   - **numa_num**: number of NUMA nodes in the system. 2P and 4P servers are supported. The value can be: **4** or **8**.
>   - **numa_bind**: core binding parameter of the agent thread. Each numa has two CPUs. There are a total of **numa_num** groups. The value range is as follows: [0, Number of CPUs - 1].

This parameter is a POSTMASTER parameter. Set it based on instructions provided in **Table GUC Parameters**.

**Value range**: a string, consisting of one or more characters

**Default value**: **none**
