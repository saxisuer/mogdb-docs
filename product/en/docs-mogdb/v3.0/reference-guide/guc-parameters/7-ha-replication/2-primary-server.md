---
title: Primary Server
summary: Primary Server
author: Zhang Cuiping
date: 2021-04-20
---

# Primary Server

## synchronous_standby_names

**Parameter description**: Specifies a comma-separated list of names of potential standby servers that support synchronous replication.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - The current synchronous standby server is on the top of the list. If the current synchronous standby server is disconnected, it will be replaced immediately with the next-highest-priority standby server. Name of the next-highest-priority standby server is added to the list.
> - The standby node name can be specified by setting the environment variable **PGAPPNAME**.

**Value range**: a string. If this parameter is set to *****, the name of any standby server that provides synchronous replication is matched. The value can be configured in the following format:

- ANY *num_sync* (*standby_name* [, …]) [, ANY *num_sync* (*standby_name* [, …])]

- [FIRST] *num_sync* (*standby_name* [, …])

- *standby_name* [, …]

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - In the preceding command, *num_sync* indicates the number of standby nodes that need to wait for responses from the transaction, *standby_name* indicates the name of the standby node, and FIRST and ANY specify the policies for selecting standby nodes for synchronous replication from the listed servers.
  > - **ANY N (dn_instanceId1, dn_instanceId2,…)** indicates that any *N* host names in the brackets are selected as the name list of standby nodes for synchronous replication. For example, **ANY 1(dn_instanceId1, dn_instanceId2)** indicates that any one of **dn_instanceId1** and **dn_instanceId2** is used as the standby node for synchronous replication.
  > - **FIRST N (dn_instanceId1, dn_instanceId2, …)** indicates that the first N primary node names in the brackets are selected as the standby node name list for synchronous replication based on the priority. For example, **FIRST 1 (dn_instanceId1, dn_instanceId2)** indicates that **dn_instanceId1** is selected as the standby node for synchronous replication.
  > - The meanings of dn_instanceId1, dn_instanceId2, … are the same as those of FIRST 1 (dn_instanceId1, dn_instanceId2, …).

If you use the gs_guc tool to set this parameter, perform the following operations:

```
gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY NODE 1(dn_instanceId1, dn_instanceId2)'";
```

or

```
gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY 1(AZ1, AZ2)'";
```

**Default value**: *****

## most_available_sync

**Parameter description**: Specifies whether to block the primary server when the primary-standby synchronization fails.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the primary server is not blocked when the synchronization fails.
- **off** indicates that the primary server is blocked when the synchronization fails.

**Default value**: **off**

## keep_sync_window

**Parameter description**: Specifies the delay for entering the maximum availability mode.

- If **most_available_sync** is set to **on**, when synchronous standby nodes are faulty in primary/standby scenarios and the number of configured synchronous standby nodes is insufficient (for details, see the meaning of **synchonous_standby_name**), setting **keep_sync_window** will retain the maximum protection mode within the time window specified by **keep_sync_window**. That is, committing transactions on the primary node is blocked, delay the primary node to enter the maximum availability mode.
- If synchronous standby nodes recover from faults and the number of synchronous standby nodes meets the configuration requirements, transactions are not blocked.
- You are advised to set **keep_sync_window** to 5s. This prevents the monitoring system from incorrectly reporting network instability.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is second.

- The value **0** indicates that the **keep_sync_window** is not set, that is, the maximum availability mode is entered directly.
- Other values indicate the size of the timeout window.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Setting this parameter may affect the RPO. If the primary node is faulty within the configured timeout window, the data generated from the time when the primary node is blocked to the time when the primary node is faulty may be lost.

## enable_stream_replication

**Parameter description**: Specifies whether data and logs are synchronized between primary and standby servers, and between primary and secondary servers.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - This parameter is used for performance testing in scenarios where data synchronization to standby server is enabled and where it is disabled. If this parameter is set to **off**, tests on abnormal scenarios, such as switchover and faults, cannot be performed to prevent inconsistency between the primary, standby, and secondary servers.
> - This parameter is a restricted parameter, and you are advised not to set it to **off** in normal service scenarios.
> - Currently, primary/standby/secondary deployment is not supported by default.

**Value range**: Boolean

- **on** indicates that data and log synchronization is enabled.
- **off** indicates that data and log synchronization is disabled.

**Default value**: **on**

## enable_mix_replication

**Parameter description**: Specifies how WAL files and data are replicated between primary and standby servers, and between primary and secondary servers.

This parameter is an INTERNAL parameter. Its default value is **off** and cannot be modified.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - This parameter cannot be modified in normal service scenarios. That is, the WAL file and data page mixed replication mode is disabled.
> - Currently, primary/standby/secondary deployment is not supported by default.

**Value range**: Boolean

- **on** indicates that the WAL file and data page mixed replication mode is enabled.
- **off** indicates that the WAL file and data page mixed replication mode is disabled.

**Default value**: **off**

## vacuum_defer_cleanup_age

**Parameter description**: Specifies the number of transactions by which **VACUUM** will defer the cleanup of invalid row-store table records, so that **VACUUM** and **VACUUM FULL** do not clean up deleted tuples immediately.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000. **0** means no delay.

**Default value**: **0**

## data_replicate_buffer_size

**Parameter description**: Specifies the amount of memory used by queues when the sender sends data pages to the receiver. The value of this parameter affects the buffer size used during the replication from the primary server to the standby server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 4096 to 1072693248. The unit is KB.

**Default value**: **16MB** (16448 KB)

## walsender_max_send_size

**Parameter description**: Specifies the size of the WAL or Sender buffers on the primary server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 8 to *INT_MAX*. The unit is KB.

**Default value**: **8MB** (8192 KB)

## enable_data_replicate

**Parameter description**: Specifies how data is synchronized between primary and standby servers when the data is imported to a row-store table.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the primary and standby servers synchronize data using data pages when the data is imported to a row-store table. When **replication_type** is set to **1**, this parameter cannot be set to **on**. If this parameter is set to **on** using the GUC tool, its value will be forcibly changed to **off**.
- **off** indicates that the primary and standby servers synchronize data using Xlogs when the data is imported to a row-store table.

**Default value**: **off**

## ha_module_debug

**Parameter description**: Specifies the replication status log of a specific data block during data replication.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the status of each data block is recorded in logs during data replication.
- **off** indicates that the status of each data block is not recorded in logs during data replication.

**Default value**: **off**

## enable_incremental_catchup

**Parameter description**: Specifies the data catchup mode between the primary and standby nodes. Currently, primary/standby/secondary deployment is not supported by default.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the standby server uses the incremental catchup mode. That is, the standby server scans local data files on the standby server to obtain the list of differential data files between the primary and standby servers and then performs catchup between the primary and standby servers.
- **off** indicates that the standby server uses the full catchup mode. That is, the standby server scans all local data files on the primary server to obtain the list of differential data files between the primary and standby servers and performs catchup between the primary and standby servers.

**Default value**: **on**

## wait_dummy_time

**Parameter description**: Specifies the maximum duration for the primary server to wait for the standby and secondary servers to start and send the scanning lists when incremental data catchup is enabled in MogDB.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *INT_MAX*. The unit is second.

**Default value**: **300**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - The unit can only be second.
> - Currently, primary/standby/secondary deployment is not supported by default.

## catchup2normal_wait_time

**Parameter description**: Specifies the maximum duration that the primary server is blocked during the data catchup on the standby server in the case of a single synchronous standby server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from -1 to 10000. The unit is ms.

- The value **-1** indicates that the primary server is blocked until the data catchup on the standby server is complete.
- The value **0** indicates that the primary server is not blocked during the data catchup on the standby server.
- Other values indicate the maximum duration that the primary server is blocked during the data catchup on the standby server. For example, if this parameter is set to **5000**, the primary server is blocked until the data catchup on the standby server is complete in 5s.

**Default value**: **-1**

## sync_config_strategy

**Parameter description**: Specifies the strategy for synchronizing configuration files between the primary and standby servers, and the standby and cascaded standby servers.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: enumerated values

- **all_node**: If this parameter is set to **all_node** on a primary server, configuration files on the primary server can be automatically synchronized to all standby servers. If this parameter is set to **all_node** on a standby server, the current standby server is allowed to send a synchronization request to the primary server and configuration files on the current standby server can be automatically synchronized to all cascaded standby servers. If this parameter is set to **all_node** on a cascaded standby server, the current cascaded standby server is allowed to send a synchronization request to its standby server.
- **only_sync_node**: If this parameter is set to **only_sync_node** on a primary server, configuration files on the primary server can be automatically synchronized to only all standby servers. If this parameter is set to **only_sync_node** on a standby server, the current standby server is allowed to send a synchronization request to the primary server and configuration files on the current standby server cannot be automatically synchronized to all cascaded standby servers. If this parameter is set to **only_sync_node** on a cascaded standby server, the current cascaded standby server is allowed to send a synchronization request to its standby server.
- **none_node**: If this parameter is set to **none_node** on a primary server, configuration files on the primary server cannot be automatically synchronized to only all standby servers. If this parameter is set to **none_node** on a standby server, the current standby server is not allowed to send a synchronization request to the primary server and configuration files on the current standby server cannot be automatically synchronized to all cascaded standby servers. If this parameter is set to **none_node** on a cascaded standby server, the current cascaded standby server is not allowed to send a synchronization request to its standby server.

**Default value**: **all_node**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - In a MogDB database instance that contains a primary node, a standby node, and a cascaded standby node, the primary node is a sender relative to the standby node and the standby node is a receiver relative to the primary node, while the standby node is a sender relative to the cascaded standby node and the cascaded standby node is a receiver relative to the standby node. 
> - The sender actively synchronizes the configuration file to the receiver, and the receiver requests the sender to synchronize the configuration file, which are two independent events, so that the configuration files are synchronized. If you do not want to synchronize configuration files, set this parameter to **none_node** on the receiver. If the sender is a standby node, set this parameter to **none_node** only. If the sender is a primary node, set this parameter to **none_node** when the primary node does not synchronize with any standby node; or set this parameter to **only_sync_node** when the primary node synchronizes with synchronous standby nodes only and does not synchronize with asynchronous standby nodes. 
> - To be specific, the sender sends a configuration file which directly overwrites the corresponding parameter in the configuration file of the receiver. After the policy for synchronizing configuration files is set, even if you modify configuration parameters of the receiver, the modification does not take effect because the sender immediately overwrites these parameters. 
> - The following configuration parameters are not synchronized even if the policy for synchronizing configuration files is set: application_name, archive_command, audit_directory, available_zone, comm_control_port, comm_sctp_port, listen_addresses, log_directory, port, replconninfo1, replconninfo2, replconninfo3, replconninfo4, replconninfo5, replconninfo6, replconninfo7, replconninfo8, replconninfo9, replconninfo10, replconninfo11, replconninfo12, replconninfo13, replconninfo14, replconninfo15, replconninfo16, replconninfo17, replconninfo18, ssl, ssl_ca_file, ssl_cert_file, ssl_ciphers, ssl_crl_file, ssl_key_file, ssl_renegotiation_limit, ssl_cert_notify_time, synchronous_standby_names, local_bind_address, perf_directory, query_log_directory, asp_log_directory, streaming_router_port, enable_upsert_to_merge, archive_dest, recovery_min_apply_delay, and sync_config_strategy.
