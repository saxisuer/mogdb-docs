---
title: Miscellaneous Parameters
summary: Miscellaneous Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Miscellaneous Parameters

## enable_default_ustore_table

**Parameter description:** Specifies whether to enable the UStore storage engine by default. If this parameter is set to **on**, all created tables are UStore tables.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** [off,on]

**Default value**: **off**

## reserve_space_for_nullable_atts

**Parameter description**: Specifies whether to reserve space for the nullable attribute of an Ustore table. If this parameter is set to **on**, space is reserved for the nullable attribute of the Ustore table by default.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** [off,on]

**Default value**: **on**

## ustore_attr

**Parameter description:** Specifies the UStore test parameters.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

You can set **enable_ustore_partial_seqscan** (copy selective columns only during sequential scanning in the ustore table), **enable_candidate_buf_usage_count** (whether dirty pages are evicted and added to the use count weight), and **ustats_tracker_naptime** (time for reloading the statistics file). **umax_search_length_for_prune** (number of blocks to be pruned before table extension) and **ustore_unit_test** (starting the UStore white box test). The setting method is ustore_attr ='**enable_ustore_partial_seqscan**=on'.

**Value range**: a string

## server_version

**Parameter description**: Specifies the server version number.

This parameter is a fixed parameter of the INTERNAL type. It can be viewed but cannot be modified. This parameter is inherited from the PostgreSQL kernel, indicating that the current database kernel is compatible with the server_version version corresponding to PostgreSQL. This parameter is reserved to ensure the ecosystem compatibility of the northbound external tool interface (query when the tool is connected). This parameter is not recommended. To query the server version, use the opengauss_version() function.

**Value range**: a string

**Default value**: **9.2.4**

## server_version_num

**Parameter description**: Specifies the server version number.

This parameter is a fixed parameter of the INTERNAL type. It can be viewed but cannot be modified. This parameter is inherited from the PostgreSQL kernel, indicating that the current database kernel is compatible with the server_version_num version corresponding to PostgreSQL. This parameter is reserved to ensure the ecosystem compatibility of the northbound external tool API. (query when the tool is connected).

**Value range**: an integer

**Default value**: **90204**

## block_size

**Parameter description**: Specifies the block size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value**:**8192**

**Default value**: **8192**

## segment_size

**Parameter description**: Specifies the segment file size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Unit**: 8 KB

**Default value**: 131072, that is, 1 GB

## max_index_keys

**Parameter description**: Specifies the maximum number of index keys supported by the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: **32**

## integer_datetimes

**Parameter description**: Specifies whether the date and time are in the 64-bit integer format.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: Boolean

- **on** indicates that the 64-bit integer format is used.
- **off** indicates that the 64-bit integer format is not used.

**Default value**: **on**

## lc_collate

**Parameter description:** Specifies the locale in which sorting of textual data is done.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the MogDB installation and deployment.

## lc_ctype

**Parameter description**: Specifies the locale that determines character classifications. For example, it specifies what a letter and its upper-case equivalent are.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the MogDB installation and deployment.

## max_identifier_length

**Parameter description**: Specifies the maximum identifier length.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: an integer

**Default value**: **63**

## server_encoding

**Parameter description**: Specifies the database encoding (character set).

By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value:** determined by the current system environment when the database is created.

## enable_upgrade_merge_lock_mode

**Parameter description**: If this parameter is set to **on**, the delta merge operation internally increases the lock level, and errors can be prevented when update and delete operations are performed at the same time.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- If this parameter is set to **on**, the delta merge operation internally increases the lock level. In this way, when the **DELTAMERGE** operation is concurrently performed with the **UPDATE** or **DELETE** operation, one operation can be performed only after the previous one is complete.
- If this parameter is set to **off**, and any two of the **DELTAMERGE**, **UPDATE**, and **DELETE** operations are concurrently performed to data in a row in the delta table of the HDFS table, errors will be reported during the later operation, and the operation will stop.

**Default value**: **off**

## transparent_encrypted_string

**Parameter description**: Specifies a sample string that is transparently encrypted. Its value is generated by encrypting **TRANS_ENCRYPT_SAMPLE_STRING** using a database secret key. The ciphertext is used to check whether the DEK obtained during secondary startup is correct. If it is incorrect, database nodes will not be started. This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md). This parameter applies only to the DWS scenario in the current version.

**Value range**: a string. An empty string indicates that MogDB is a not encrypted.

**Default value**: empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Do not set this parameter manually. Otherwise, MogDB may become faulty.

## transparent_encrypt_kms_url

**Parameter description**: Specifies the URL for obtaining the database secret key to be transparently encrypted. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes. The format is **kms://***Protocol***@***KMS host name 1***;***KMS host name 2***:***KMS port number***/kms**, for example, **kms://https@linux175:29800/**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md). This parameter applies only to the DWS scenario in the current version.

**Value range**: a string

**Default value**: empty

## transparent_encrypt_kms_region

**Parameter description**: Specifies the deployment region of MogDB. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes. This parameter applies only to the DWS scenario in the current version.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: empty

## basebackup_timeout

**Parameter description**: Specifies the timeout period for connection during which no read/write operations are performed after backup transmission is complete. When the gs_basebackup tool is used for transmission, if a higher compression rate is specified, the timeout may occur after the tablespace transmission is complete. (The transmission data needs to be compressed in the client.)

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is second. **0**: indicates that the parameter is disabled.

**Default value**: 600s

## datanode_heartbeat_interval

**Parameter description**: Specifies the interval at which heartbeat messages are sent between heartbeat threads. You are advised to set this parameter to a value no more than wal_receiver_timeout/2.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1000 to 60000. The unit is ms.

Default value: **1s**

## max_concurrent_autonomous_transactions

**Parameter description**: Specifies the maximum number of autonomous transaction connections, that is, the maximum number of concurrent autonomous transactions executed at the same time. If this parameter is set to **0**, autonomous transactions cannot be executed.

This parameter is an INTERNAL parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: 0-1024

**Default value:** **10**
