---
title: Kernel Resource Usage
summary: Kernel Resource Usage
author: Zhang Cuiping
date: 2021-04-20
---

# Kernel Resource Usage

This section describes kernel resource parameters. Whether these parameters take effect depends on OS settings.

## max_files_per_process

**Parameter description**: Specifies the maximum number of simultaneously open files allowed by each server process. If the kernel is enforcing a proper limit, setting this parameter is not required.

However, on some platforms, such as most Berkeley Software Distribution (BSD) systems, the kernel allows individual processes to open much more files than the system can support. If the message "Too many open files" is displayed, set this parameter to a smaller value. Generally, the system must meet this requirement: Number of file descriptors >= Maximum number of concurrent statements x Number of database nodes x **max_files_per_process** x 3

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 25 to 2147483647

**Default value**: **1000**

## shared_preload_libraries

**Parameter description:** Specifies one or more shared libraries to be preloaded at server start. If multiple libraries are to be loaded, separate their names using commas (,). For example, **$libdir/mylib** will cause **mylib.so** (or on some platforms, **mylib.sl**) to be preloaded before the loading of the standard library directory.

You can preinstall the MogDB's stored procedure library using the **$libdir/pl** *XXX* syntax as described in the preceding text. *XXX* can only be **pgsql**, **perl**, **tcl**, or **python**.

By preloading a shared library and initializing it as required, the library startup time is avoided when the library is first used. However, the time to start each new server process may increase, even if that process never uses the library. Therefore, set this parameter only for libraries that will be used in most sessions.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If a specified library is not found, the MogDB service will fail to start.
> - Each MogDB-supported library has a special mark that is checked to guarantee compatibility. Therefore, libraries that do not support MogDB cannot be loaded in this way.

**Value range**: a string

**Default value**: empty
