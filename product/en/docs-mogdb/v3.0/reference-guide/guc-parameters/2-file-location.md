---
title: File Location
summary: File Location
author: Zhang Cuiping
date: 2021-04-20
---

# File Location

After a database has been installed, three configuration files (**postgresql.conf**, **pg_hba.conf**, and **pg_ident.conf**) are automatically generated and saved in the data directory. You can use the methods described in this section to change the names and save paths of these configuration files.

When changing the storage directory of a configuration file, set **data_directory** in **postgresql.conf** to the actual data directory.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If a configuration file is incorrectly modified, the database will be seriously affected. Do not modify the configuration files mentioned in this section after installation.

## data_directory

**Parameter description**: Specifies the data directory of MogDB. You can set this parameter using one of the following methods:

- Set it when you install MogDB.
- This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, consisting of one or more characters.

**Default value**: Specify this parameter during installation. If this parameter is not specified during installation, the database is not initialized by default.

## config_file

**Parameter description**: Specifies the configuration file (**postgresql.conf**) of the primary server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md). This parameter cannot be modified based on Method 4 in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: a string, consisting of one or more characters.

**Default value**: **postgresql.conf** (The absolute directory of this file may be displayed in the actual situation.)

## hba_file

**Parameter description**: Specifies the configuration file (**pg_hba.conf**) for host-based authentication (HBA). You can set this parameter only in the configuration file **postgresql.conf**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **pg_hba.conf** (The absolute directory of this file may be displayed in the actual situation.)

## ident_file

**Parameter description**: Specifies the configuration file (**pg_ident.conf)** for client authentication.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **pg_ident.conf** (The absolute directory of this file may be displayed in the actual situation.)

## external_pid_file

**Parameter description**: Specifies the extra PID file that can be used by the server management program.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This parameter takes effect only after the database restarts.

**Value range**: a string

**Default value**: empty
