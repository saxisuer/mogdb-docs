---
title: System Performance Snapshot
summary: System Performance Snapshot
author: Zhang Cuiping
date: 2021-04-20
---

# System Performance Snapshot

## enable_wdr_snapshot

**Parameter description**: Specifies whether to enable the database monitoring snapshot function.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the database monitoring snapshot function is enabled.
- **off** indicates that the database monitoring snapshot function is disabled.

**Default value**: **off**

## wdr_snapshot_retention_days

**Parameter description**: Specifies the number of days for storing database monitoring snapshot data in the system. When the number of snapshots generated during database running exceeds the maximum number of snapshots that can be generated within the retention period, the system clears the snapshot data with the smallest **snapshot_id** at the interval specified by **wdr_snapshot_interval**.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 8

**Default value**: **8**

## wdr_snapshot_query_timeout

**Parameter description**: Specifies the execution timeout for the SQL statements associated with database monitoring snapshot operations. If the SQL statement execution is not complete and a result is not returned within the specified time, the snapshot operation fails.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 100 to *INT_MAX*. The unit is s.

**Default value**: **100s**

## wdr_snapshot_interval

**Parameter description**: Specifies the interval at which the background thread Snapshot automatically performs snapshot operations on the database monitoring data.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10 to 60. The unit is m.

**Default value**: **1h**

## asp_flush_mode

**Parameter description**: Specifies the mode for the ASP to update data to the disk. The value can be **file** (default value), **table** (system catalog), or **all** (system catalog and file). Only the sysadmin user can access this parameter.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, which can be **table**, **file**, or **all**

**Default value**: **table**

## asp_flush_rate

**Parameter description**: When the number of samples reaches the value of **asp_sample_num**, the samples in the memory are updated to the disk based on a certain proportion. **asp_flush_rate** indicates the update proportion. If this parameter is set to **10**, it indicates that the update ratio is 10:1.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 10

**Default value**: **10**

## asp_log_filename

**Parameter description**: Specifies the file name format when writing files using ASP. Only the sysadmin user can access this parameter.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **asp-%Y-%m-%d_%H%M%S.log**

## asp_retention_days

**Parameter description**: Specifies the maximum number of days for reserving ASP samples when they are written to the system catalog.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 7

**Default value**: **2**

## asp_sample_interval

**Parameter description**: Specifies the sampling interval.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 10. The unit is s.

**Default value**: **1s**

## asp_sample_num

**Parameter description**: Specifies the maximum number of samples allowed in the LOCAL_ACTIVE_SESSION view. Only the sysadmin user can access this parameter.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10000 to 100000

**Default value**: **100000**

## enable_asp

**Parameter description:**Specifies whether to enable the active session profile function.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on**: The function is enabled.
- **off**: The function is disabled.

**Default value**: **on**
