---
title: Audit Switch
summary: Audit Switch
author: Zhang Cuiping
date: 2021-04-20
---

# Audit Switch

## audit_enabled

**Parameter description**: Specifies whether to enable or disable the audit process. After the audit process is enabled, the auditing information written by the background process can be read from the pipe and written into audit files.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the auditing function is enabled.
- **off** indicates that the auditing function is disabled.

**Default value**: **on**

## audit_directory

**Parameter description**: Specifies the storage directory of audit files. It is a customizable path related to the data directory.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value:** **pg_audit** If **om** is used for MogDB deployment, audit logs are stored in **$GAUSSLOG/pg_audit/\*Instance name\***.

## audit_data_format

**Parameter description**: Audits the format of log files. Currently, only the binary format is supported.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **binary**

## audit_rotation_interval

**Parameter description**: Specifies the interval of creating an audit log file. If the difference between the current time and the time when the previous audit log file is created is greater than the value of **audit_rotation_interval**, a new audit log file will be generated.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *INT_MAX*/60. The unit is min.

**Default value:** **1d**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Adjust this parameter only when required. Otherwise, **audit_resource_policy** may fail to take effect. To control the storage space and time of audit logs, set the **[audit_resource_policy](#audit_resource_policy)**, **[audit_space_limit](#audit_space_limit)**, and **[audit_file_remain_time](#audit_file_remain_time)** parameters.

## audit_rotation_size

**Parameter description**: Specifies the maximum capacity of an audit log file. If the total number of messages in an audit log exceeds the value of **audit_rotation_size**, the server will generate a new audit log file.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1024 to 1048576. The unit is KB.

**Default value**: **10 MB**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Adjust this parameter only when required. Otherwise, **audit_resource_policy** may fail to take effect. To control the storage space and time of audit logs, set the **audit_resource_policy**, **audit_space_limit**, and **audit_file_remain_time** parameters.

## audit_resource_policy

**Parameter description**: Specifies the policy for determining whether audit logs are preferentially stored by space or time.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that audit logs are preferentially stored by space. A maximum of **[audit_space_limit](#audit_space_limit)** logs can be stored.
- **off** indicates that audit logs are preferentially stored by time. A minimum duration of **[audit_file_remain_time](#audit_file_remain_time)** logs must be stored.

**Default value**: **on**

## audit_file_remain_time

**Parameter description**: Specifies the minimum duration required for recording audit logs. This parameter is valid only when **[audit_resource_policy](#audit_resource_policy)** is set to **off**.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 730. The unit is day. **0** indicates that the storage duration is not limited.

**Default value**: **90**

## audit_space_limit

**Parameter description**: Specifies the total disk space occupied by audit files.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1024 KB to 1024 GB. The unit is KB.

**Default value**: **1GB**

## audit_file_remain_threshold

**Parameter description**: Specifies the maximum number of audit files in the audit directory.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 1048576

**Default value**: **1048576**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Ensure that this parameter is set to **1048576**. Adjust this parameter only when required. Otherwise, **audit_resource_policy** may fail to take effect. To control the storage space and time of audit logs, set the **audit_resource_policy**, **audit_space_limit**, and **audit_file_remain_time** parameters.

## audit_thread_num

**Parameter description**: Specifies the number of audit threads.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** an integer ranging from 1 to 48

**Default value**: **1**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** When **audit_dml_state** is enabled and high performance is required, you are advised to increase the value of this parameter to ensure that audit messages can be processed and recorded in a timely manner.