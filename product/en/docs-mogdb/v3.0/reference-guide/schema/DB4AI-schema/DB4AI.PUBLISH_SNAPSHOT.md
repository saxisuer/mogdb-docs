---
title: DB4AI.PUBLISH_SNAPSHOT
summary: DB4AI.PUBLISH_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PUBLISH_SNAPSHOT

**PUBLISH_SNAPSHOT** publishes snapshots for the DB4AI feature. You can invoke **PUBLISH SNAPSHOT** to implement this function.

**Table 1** DB4AI.PUBLISH_SNAPSHOT input parameters and return values

| Parameter | Type                    | Description                                                  |
| :-------- | :---------------------- | :----------------------------------------------------------- |
| i_schema  | IN NAME                 | Name of the schema storing snapshots. The default value is the current user or **PUBLIC**. |
| i_name    | IN NAME                 | Snapshot name                                                |
| res       | OUT db4ai.snapshot_name | Result                                                       |
