---
title: DB4AI.PURGE_SNAPSHOT
summary: DB4AI.PURGE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PURGE_SNAPSHOT

**PURGE_SNAPSHOT** deletes snapshots for the DB4AI feature. You can invoke **PURGE SNAPSHOT** to implement this function.

**Table 1** DB4AI.PURGE_SNAPSHOT input parameters and return values

| Parameter | Type                    | Description                          |
| :-------- | :---------------------- | :----------------------------------- |
| i_schema  | IN NAME                 | Name of the schema storing snapshots |
| i_name    | IN NAME                 | Snapshot name                        |
| res       | OUT db4ai.snapshot_name | Result                               |
