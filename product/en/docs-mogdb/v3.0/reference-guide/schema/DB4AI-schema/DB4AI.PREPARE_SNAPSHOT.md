---
title: DB4AI.PREPARE_SNAPSHOT
summary: DB4AI.PREPARE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PREPARE_SNAPSHOT

**PREPARE_SNAPSHOT** prepares data for model training and interprets snapshots for the DB4AI feature. A snapshot provides a complete sequence of data and documents that are changed by all applications. You can invoke **PREPARE SNAPSHOT** to implement this function.

**Table 1** DB4AI.PREPARE_SNAPSHOT input parameters and return values

| Parameter  | Type                    | Description                                                  |
| :--------- | :---------------------- | :----------------------------------------------------------- |
| i_schema   | IN NAME                 | Name of the schema storing snapshots. The default value is the current user or **PUBLIC**. |
| i_parent   | IN NAME                 | Parent snapshot name                                         |
| i_commands | IN TEXT[]               | DDL and DML commands for modifying snapshots                 |
| i_vers     | IN NAME                 | Version suffix                                               |
| i_comment  | IN TEXT                 | Data comment                                                 |
| res        | OUT db4ai.snapshot_name | Result                                                       |
