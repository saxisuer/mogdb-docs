---
title: DB4AI.CREATE_SNAPSHOT_INTERNAL
summary: DB4AI.CREATE_SNAPSHOT_INTERNAL
author: Guo Huan
date: 2021-11-15
---

# DB4AI.CREATE_SNAPSHOT_INTERNAL

**CREATE_SNAPSHOT_INTERNAL** is a built-in execution function of the DB4AI.CREATE_SNAPSHOT function. The function involves information verification and cannot be directly invoked.

**Table 1** DB4AI.CREATE_SNAPSHOT_INTERNAL input parameters and return values

| Parameter  | Type      | Description                     |
| :--------- | :-------- | :------------------------------ |
| s_id       | IN BIGINT | Snapshot ID                     |
| i_schema   | IN NAME   | Namespace storing the snapshot  |
| i_name     | IN NAME   | Snapshot name                   |
| i_commands | IN TEXT[] | SQL commands for obtaining data |
| i_comment  | IN TEXT   | Snapshot description            |
| i_owner    | IN NAME   | Snapshot owner                  |
