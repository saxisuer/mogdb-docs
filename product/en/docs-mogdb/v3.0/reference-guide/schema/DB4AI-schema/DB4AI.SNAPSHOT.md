---
title: DB4AI.SNAPSHOT
summary: DB4AI.SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.SNAPSHOT

DB4AI.SNAPSHOT records the snapshots stored by the current user through the DB4AI.SNAPSHOT feature.

**Table 1** Attributes of the DB4AI.SNAPSHOT table

| Name      | Type                        | Description                                                  | Instance                   |
| :-------- | :-------------------------- | :----------------------------------------------------------- | :------------------------- |
| id        | bigint                      | ID of the current snapshot                                   | 1                          |
| parent_id | bigint                      | ID of the parent snapshot                                    | 0                          |
| matrix_id | bigint                      | Matrix ID of the snapshot in CSS mode. Otherwise, the value is **NULL**. | 0                          |
| root_id   | bigint                      | ID of the initial snapshot, which is constructed from the operated data using db4ai.create_snapshot(). | 0                          |
| schema    | name                        | Schema for exporting the snapshot view                       | public                     |
| name      | name                        | Snapshot name, including the version suffix                  | example0@1.1.0             |
| owner     | name                        | Name of the user who creates the snapshot                    | nw                         |
| commands  | text[]                      | A complete list of SQL statements that record how to generate this snapshot from its root snapshot | {DELETE,"WHERE id > 7"}    |
| comment   | text                        | Snapshot description                                         | inherits from @1.0.0       |
| published | boolean                     | **TRUE**, only when the snapshot has been published          | f                          |
| archived  | boolean                     | **TRUE**, only when the snapshot has been archived           | f                          |
| created   | timestamp without time zone | Timestamp marking the snapshot creation date                 | 2021-08-25 10:59:52.955604 |
| row_count | bigint                      | Number of data rows in the snapshot                          | 8                          |
