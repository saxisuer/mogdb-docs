---
title: DB4AI.ARCHIVE_SNAPSHOT
summary: DB4AI.ARCHIVE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.ARCHIVE_SNAPSHOT

**ARCHIVE_SNAPSHOT** archives snapshots for the DB4AI feature. You can invoke **ARCHIVE SNAPSHOT** to implement this function. After taking effect, the snapshot cannot be used for training.

**Table 1** DB4AI.ARCHIVE_SNAPSHOT input parameters and return values

| Parameter | Type                    | Description                                                  |
| :-------- | :---------------------- | :----------------------------------------------------------- |
| i_schema  | IN NAME                 | Name of the schema storing snapshots. The default value is the current user. |
| i_name    | IN NAME                 | Snapshot name                                                |
| res       | OUT db4ai.snapshot_name | Result                                                       |
