---
title: _PG_FOREIGN_TABLE_COLUMNS
summary: _PG_FOREIGN_TABLE_COLUMNS
author: Guo Huan
date: 2021-06-23
---

# _PG_FOREIGN_TABLE_COLUMNS

**_PG_FOREIGN_TABLE_COLUMNS** displays column information about a foreign table. Only the sysadmin user has the permission to view this view.

**Table 1** _PG_FOREIGN_TABLE_COLUMNS columns

| Name          | Type   | Description                                                  |
| ------------- | ------ | ------------------------------------------------------------ |
| nspname       | name   | Schema name                                                  |
| relname       | name   | Table name                                                   |
| attname       | name   | Column name                                                  |
| attfdwoptions | text[] | Attribute-level foreign data wrapper options, expressed in a string in the format of *keyword***=***value* |
