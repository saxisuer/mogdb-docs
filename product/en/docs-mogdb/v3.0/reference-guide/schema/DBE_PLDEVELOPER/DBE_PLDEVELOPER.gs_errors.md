---
title: DBE_PLDEVELOPER.gs_errors
summary: DBE_PLDEVELOPER.gs_errors
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER.gs_errors

Records errors that occur during PLPGSQL object (stored procedure, function, package, and package body) compilation. For details, see the following column description.

After the **plsql_show_all_error** parameter is enabled, if an error occurs during compilation, the error is skipped and the compilation continues, and the error information is recorded in **gs_errors**. If the **plsql_show_all_error** parameter is disabled, related information is not inserted into this table.

**Table 1** DBE_PLDEVELOPER.gs_errors columns

| **Name** | **Type** | **Description**                                              |
| -------- | -------- | ------------------------------------------------------------ |
| id       | oid      | Object ID.                                                   |
| owner    | bigint   | ID of the user who creates the object.                       |
| nspid    | oid      | Schema ID of an object.                                      |
| name     | name     | Object name.                                                 |
| type     | text     | Object type (**procedure**/**function**/**package**/**package body**). |
| line     | integer  | Line number.                                                 |
| src      | text     | Original statement for creating an object.                   |