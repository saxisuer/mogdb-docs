---
title: DBE_PLDEVELOPER.gs_source
summary: DBE_PLDEVELOPER.gs_source
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER.gs_source

Records PLPGSQL object (stored procedure, function, package, and package body) compilation information. For details, see the following column description.

When the **plsql_show_all_error** parameter is enabled, information about PL/PGSQL object compilation success or failure is recorded in this table. When the **plsql_show_all_error** parameter is disabled, only information about correct compilation is inserted into this table.

**Table 1** DBE_PLDEVELOPER.gs_source columns

| **Name** | **Type** | **Description**                                              |
| -------- | -------- | ------------------------------------------------------------ |
| id       | oid      | Object ID.                                                   |
| owner    | bigint   | ID of the user who creates the object.                       |
| nspid    | oid      | Schema ID of an object.                                      |
| name     | name     | Object name.                                                 |
| type     | text     | Object type (**procedure**/**function**/**package**/**package body**). |
| status   | boolean  | Determines whether the creation is successful.               |
| src      | text     | Original statement for creating an object.                   |