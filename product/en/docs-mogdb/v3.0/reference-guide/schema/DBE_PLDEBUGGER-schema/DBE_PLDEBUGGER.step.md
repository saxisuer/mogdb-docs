---
title: DBE_PLDEBUGGER.step
summary: DBE_PLDEBUGGER.step
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.step

During debugging on the debug end, if a stored procedure is being executed, the stored procedure continues to be executed and information such as the line number in the first line of the stored procedure is returned. If the executed object is not a stored procedure, the return is the same as that for **next**. After the SQL statement is executed, information such as the line number in the next line is returned.

**Table 1** step input parameters and return values

| Name     | Type        | Description                                                  |
| :------- | :---------- | :----------------------------------------------------------- |
| funcoid  | OUT oid     | Function ID                                                  |
| funcname | OUT text    | Function name                                                |
| lineno   | OUT integer | Number of the next line in the current debugging process     |
| query    | OUT text    | Source code of the next line of the function that is being debugged |
