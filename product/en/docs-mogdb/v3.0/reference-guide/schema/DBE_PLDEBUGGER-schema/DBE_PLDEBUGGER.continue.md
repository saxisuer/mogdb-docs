---
title: DBE_PLDEBUGGER.continue
summary: DBE_PLDEBUGGER.continue
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.continue

Executes the current stored procedure until reaching the next breakpoint or end, and returns the line number of the next execution and the corresponding query.

The function prototype is as follows:

```sql
DBE_PLDEBUGGER.continue()
RETURN Record;
```

**Table 1** continue input parameters and return values

| Name     | Type        | Description                                                  |
| :------- | :---------- | :----------------------------------------------------------- |
| funcoid  | OUT oid     | Function ID                                                  |
| funcname | OUT text    | Function name                                                |
| lineno   | OUT integer | Number of the next line in the current debugging process     |
| query    | OUT text    | Source code of the next line of the function that is being debugged |
