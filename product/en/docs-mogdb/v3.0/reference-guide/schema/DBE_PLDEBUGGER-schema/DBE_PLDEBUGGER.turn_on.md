---
title: DBE_PLDEBUGGER.turn_on
summary: DBE_PLDEBUGGER.turn_on
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.turn_on

This function is used to mark a stored procedure as debuggable. After **turn_on** is executed, the server can execute the stored procedure for debugging. You need to manually obtain the OID of the stored procedure based on the PG_PROC system catalog and transfer it to the function. After **turn_on** is executed, the execution of the stored procedure in the current session is hung before the first SQL statement to wait for the debugging instruction from the debug end. This setting is cleared by default after the session is disconnected. Currently, stored procedures and functions with autonomous transactions enabled cannot be debugged.

The function prototype is as follows:

```sql
DBE_PLDEBUGGER.turn_on(Oid)
RETURN Record;
```

**Table 1** turn_on input parameters and return values

| Name     | Type        | Description                  |
| :------- | :---------- | :--------------------------- |
| func_oid | IN oid      | Function OID                 |
| nodename | OUT text    | Node name                    |
| port     | OUT integer | Number of the connected port |
