---
title: DBE_PLDEBUGGER.enable_breakpoint
summary: DBE_PLDEBUGGER.enable_breakpoint
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEBUGGER.enable_breakpoint

During debugging on the debug end, calls enable_breakpoint to enable breakpoints.

**Table 1** enable_breakpoint input parameters and return values

| Name         | Type       | Description                          |
| :----------- | :--------- | :----------------------------------- |
| breakpointno | IN integer | Breakpoint number                    |
| result       | OUT bool   | Whether this operation is successful |