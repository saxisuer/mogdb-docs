---
title: DBE_PLDEBUGGER.local_debug_server_info
summary: DBE_PLDEBUGGER.local_debug_server_info
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.local_debug_server_info

This function is used to query the OID of the stored procedure that has been turned on in the current connection. You can use **funcoid** and **pg_proc** together to determine which stored procedures are to be debugged.

**Table 1** local_debug_server_info returned input parameters and return values

| Name     | Type       | Description          |
| :------- | :--------- | :------------------- |
| nodename | OUT text   | Node name            |
| port     | OUT bigint | Port number          |
| funcoid  | OUT oid    | Stored procedure OID |
