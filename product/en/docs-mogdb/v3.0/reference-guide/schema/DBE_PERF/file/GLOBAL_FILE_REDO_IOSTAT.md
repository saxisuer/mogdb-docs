---
title: GLOBAL_FILE_REDO_IOSTAT
summary: GLOBAL_FILE_REDO_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_FILE_REDO_IOSTAT

**GLOBAL_FILE_REDO_IOSTAT** records statistics about redo logs and WALs on each node in MogDB.

**Table 1** GLOBALXC_FILE_REDO_IOSTAT columns

| **Name**  | **Type** | **Description**                                              |
| :-------- | :------- | :----------------------------------------------------------- |
| node_name | name     | Node name                                                    |
| phywrts   | bigint   | Number of times writing into the WAL buffer                  |
| phyblkwrt | bigint   | Number of blocks written into the WAL buffer                 |
| writetim  | bigint   | Duration of writing into XLOG files (unit: μs)               |
| avgiotim  | bigint   | Average duration of writing into XLOG files (unit: μs). **avgiotim** = **writetim**/**phywrts** |
| lstiotim  | bigint   | Duration of the last writing into XLOG files (unit: μs)      |
| miniotim  | bigint   | Minimum duration of writing into XLOG files (unit: μs)       |
| maxiowtm  | bigint   | Maximum duration of writing into XLOG files (unit: μs)       |
