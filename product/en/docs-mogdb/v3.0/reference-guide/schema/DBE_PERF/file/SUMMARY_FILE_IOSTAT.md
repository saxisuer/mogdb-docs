---
title: SUMMARY_FILE_IOSTAT
summary: SUMMARY_FILE_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_FILE_IOSTAT

**SUMMARY_FILE_IOSTAT** records statistics about data file I/Os in MogDB to indicate I/O performance and detect performance problems such as abnormal I/O operations.

**Table 1** SUMMARY_FILE_IOSTAT columns

| **Name**  | **Type** | **Description**                                      |
| :-------- | :------- | :--------------------------------------------------- |
| filenum   | oid      | File ID                                              |
| dbid      | oid      | Database ID                                          |
| spcid     | oid      | Tablespace ID                                        |
| phyrds    | numeric  | Number of times of reading physical files            |
| phywrts   | numeric  | Number of times of writing into physical files       |
| phyblkrd  | numeric  | Number of times of reading physical file blocks      |
| phyblkwrt | numeric  | Number of times of writing into physical file blocks |
| readtim   | numeric  | Total duration of reading (unit: μs)                 |
| writetim  | numeric  | Total duration of writing (unit: μs)                 |
| avgiotim  | bigint   | Average duration of reading and writing (unit: μs)   |
| lstiotim  | bigint   | Duration of the last file reading (unit: μs)         |
| miniotim  | bigint   | Minimum duration of reading and writing (unit: μs)   |
| maxiowtm  | bigint   | Maximum duration of reading and writing (unit: μs)   |
