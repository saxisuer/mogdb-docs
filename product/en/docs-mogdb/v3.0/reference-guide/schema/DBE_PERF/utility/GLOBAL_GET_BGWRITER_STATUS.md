---
title: GLOBAL_GET_BGWRITER_STATUS
summary: GLOBAL_GET_BGWRITER_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_GET_BGWRITER_STATUS

**GLOBAL_GET_BGWRITER_STATUS** displays the information about pages flushed by the bgwriter threads of all instances in the cluster, number of pages in the candidate buffer chain, and buffer eviction information.

**Table 1** GLOBAL_GET_BGWRITER_STATUS columns

| Name                        | Type    | Description                                                  |
| :-------------------------- | :------ | :----------------------------------------------------------- |
| node_name                   | text    | Instance name                                                |
| bgwr_actual_flush_total_num | bigint  | Total number of dirty pages flushed by the bgwriter thread from the startup time to the current time |
| bgwr_last_flush_num         | integer | Number of dirty pages flushed by the bgwriter thread in the previous batch |
| candidate_slots             | integer | Number of pages in the current candidate buffer chain.       |
| get_buffer_from_list        | bigint  | Number of times that pages are obtained from the candidate buffer chain during buffer eviction. |
| get_buffer_clock_sweep      | bigint  | Number of times that pages are obtained from the original eviction solution during buffer eviction. |
