---
title: GLOBAL_TRANSACTIONS_RUNNING_XACTS
summary: GLOBAL_TRANSACTIONS_RUNNING_XACTS
author: Guo Huan
date: 2021-11-15
---

# GLOBAL_TRANSACTIONS_RUNNING_XACTS

**GLOBAL_TRANSACTIONS_RUNNING_XACTS** displays information about the running transactions on each node in the cluster.

**Table 1** GLOBAL_TRANSACTIONS_RUNNING_XACTS columns

| **Name**    | **Type** | **Description**                                              |
| :---------- | :------- | :----------------------------------------------------------- |
| handle      | integer  | Slot handle in the transaction manager corresponding to the transaction. The value is fixed at -1. |
| gxid        | xid      | Transaction ID                                               |
| state       | tinyint  | Transaction status (**3**: prepared or **0**: starting)      |
| node        | text     | Node name                                                    |
| xmin        | xid      | Minimum transaction ID on the node                           |
| vacuum      | boolean  | Whether the current transaction is lazy vacuum               |
| timeline    | bigint   | Number of database restarts                                  |
| prepare_xid | xid      | Transaction ID in the **prepared** state. If the status is not **prepared**, the value is **0**. |
| pid         | bigint   | Thread ID corresponding to the transaction                   |
| next_xid    | xid      | Transaction ID sent from other nodes to the current node. The value is fixed at 0. |
