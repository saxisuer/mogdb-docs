---
title: Table 3
summary: Table 3
author: Guo Huan
date: 2021-04-19
---

**Table 3** List of wait events corresponding to I/Os<a id="Table3"> </a>

| wait_event                 | Description                                                  |
| :------------------------- | :----------------------------------------------------------- |
| BufFileRead                | Reads data from a temporary file to a specified buffer.      |
| BufFileWrite               | Writes the content of a specified buffer to a temporary file. |
| ControlFileRead            | Reads the **pg_control** file, mainly during database startup, checkpoint execution, and primary/standby verification. |
| ControlFileSync            | Flushes the **pg_control** file to a disk, mainly during database initialization. |
| ControlFileSyncUpdate      | Flushes the **pg_control** file to a disk, mainly during database startup, checkpoint execution, and primary/standby verification. |
| ControlFileWrite           | Writes the **pg_control** file, mainly during database initialization. |
| ControlFileWriteUpdate     | Updates the **pg_control** file, mainly during database startup, checkpoint execution, and primary/standby verification. |
| CopyFileRead               | Reads a file during file copying.                            |
| CopyFileWrite              | Writes a file during file copying.                           |
| DataFileExtend             | Writes a file during file extension.                         |
| DataFileFlush              | Flushes a table data file to a disk.                         |
| DataFileImmediateSync      | Flushes a table data file to a disk immediately.             |
| DataFilePrefetch           | Reads a table data file asynchronously.                      |
| DataFileRead               | Reads a table data file synchronously.                       |
| DataFileSync               | Flushes table data file modifications to a disk.             |
| DataFileTruncate           | Truncates a table data file.                                 |
| DataFileWrite              | Writes a table data file.                                    |
| LockFileAddToDataDirRead   | Reads the **postmaster.pid** file.                           |
| LockFileAddToDataDirSync   | Flushes the **postmaster.pid** file to a disk.               |
| LockFileAddToDataDirWrite  | Writes PID information into the **postmaster.pid** file.     |
| LockFileCreateRead         | Read the LockFile file **%s.lock**.                          |
| LockFileCreateSync         | Flushes the LockFile file **%s.lock** to a disk.             |
| LockFileCreateWRITE        | Writes PID information into the LockFile file **%s.lock**.   |
| RelationMapRead            | Reads the mapping file between system catalogs and storage locations. |
| RelationMapSync            | Flushes the mapping file between system catalogs and storage locations to a disk. |
| RelationMapWrite           | Writes the mapping file between system catalogs and storage locations. |
| ReplicationSlotRead        | Reads a stream replication slot file during a restart.       |
| ReplicationSlotRestoreSync | Flushes a stream replication slot file to a disk during a restart. |
| ReplicationSlotSync        | Flushes a temporary stream replication slot file to a disk during checkpoint execution. |
| ReplicationSlotWrite       | Writes a temporary stream replication slot file during checkpoint execution. |
| SLRUFlushSync              | Flushes the **pg_clog**, **pg_subtrans**, and **pg_multixact** files to a disk, mainly during checkpoint execution and database shutdown. |
| SLRURead                   | Reads the **pg_clog**, **pg_subtrans**, and **pg_multixact** files. |
| SLRUSync                   | Writes dirty pages into the **pg_clog**, **pg_subtrans**, and **pg_multixact** files, and flushes the files to a disk, mainly during checkpoint execution and database shutdown. |
| SLRUWrite                  | Writes the **pg_clog**, **pg_subtrans**, and **pg_multixact** files. |
| TimelineHistoryRead        | Reads the timeline history file during database startup.     |
| TimelineHistorySync        | Flushes the timeline history file to a disk during database startup. |
| TimelineHistoryWrite       | Writes to the timeline history file during database startup. |
| TwophaseFileRead           | Reads the **pg_twophase** file, mainly during two-phase transaction commit and restoration. |
| TwophaseFileSync           | Flushes the **pg_twophase** file to a disk, mainly during two-phase transaction commit and restoration. |
| TwophaseFileWrite          | Writes the **pg_twophase** file, mainly during two-phase transaction commit and restoration. |
| WALBootstrapSync           | Flushes an initialized WAL file to a disk during database initialization. |
| WALBootstrapWrite          | Writes an initialized WAL file during database initialization. |
| WALCopyRead                | Read operation generated when an existing WAL file is read for replication after archiving and restoration. |
| WALCopySync                | Flushes a replicated WAL file to a disk after archiving and restoration. |
| WALCopyWrite               | Write operation generated when an existing WAL file is read for replication after archiving and restoration. |
| WALInitSync                | Flushes a newly initialized WAL file to a disk during log reclaiming or writing. |
| WALInitWrite               | Initializes a newly created WAL file to **0** during log reclaiming or writing. |
| WALRead                    | Reads data from Xlogs during redo operations on two-phase files. |
| WALSyncMethodAssign        | Flushes all open WAL files to a disk.                        |
| WALWrite                   | Writes a WAL file.                                           |
| WALBufferAccess            | WAL Buffer access (for performance reasons, only the number of accesses are counted in the kernel code, and the access time is not counted). |
| WALBufferFull              | When the WAL Buffer is full, write wal file related processing. |
| DoubleWriteFileRead        | Doublewrites and reads a file.                               |
| DoubleWriteFileSync        | Doublewrites a file and forcibly flushes files to disks.     |
| DoubleWriteFileWrite       | Doublewrites a file and writes a file.                       |
| PredoProcessPending        | Waits for the playback of other records to complete during parallel log playback. |
| PredoApply                 | Waits for other threads to play back logs to the LSN of the current thread during parallel log playback. |
| DisableConnectFileRead     | Reads the HA lock fragment logic file.                       |
| DisableConnectFileSync     | Forcibly flushes the HA lock fragment logic file to disks.   |
| DisableConnectFileWrite    | Writes the HA lock fragment logic file.                      |
