---
title: Table 2
summary: Table 2
author: Guo Huan
date: 2021-04-19
---

**Table 2** List of wait events corresponding to lightweight locks<a id="Table2"> </a>

| wait_event                               | Description                                                  |
| :--------------------------------------- | :----------------------------------------------------------- |
| ShmemIndexLock                           | Used to protect the primary index table, a hash table, in shared memory. |
| OidGenLock                               | Used to prevent different threads from generating the same OID. |
| XidGenLock                               | Used to prevent two transactions from obtaining the same transaction ID. |
| ProcArrayLock                            | Used to prevent concurrent access to or concurrent modification on ProcArray shared arrays. |
| SInvalReadLock                           | Used to prevent concurrent execution with invalid message deletion. |
| SInvalWriteLock                          | Used to prevent concurrent execution with invalid message write and deletion. |
| WALInsertLock                            | Used to prevent concurrent execution with WAL insertion.     |
| WALWriteLock                             | Used to prevent concurrent write from a WAL buffer to a disk. |
| ControlFileLock                          | Used to prevent concurrent read/write or concurrent write/write on the **pg_control** file. |
| CheckpointLock                           | Used to prevent multi-checkpoint concurrent execution.       |
| CLogControlLock                          | Used to prevent concurrent access to or concurrent modification on the Clog control data structure. |
| SubtransControlLock                      | Used to prevent concurrent access to or concurrent modification on the subtransaction control data structure. |
| MultiXactGenLock                         | Used to allocate a unique MultiXact ID in serial mode.       |
| MultiXactOffsetControlLock               | Used to prevent concurrent read/write or concurrent write/write on **pg_multixact/offset**. |
| MultiXactMemberControlLock               | Used to prevent concurrent read/write or concurrent write/write on **pg_multixact/members**. |
| RelCacheInitLock                         | Used to add a lock before any operations are performed on the **init** file when messages are invalid. |
| CheckpointerCommLock                     | Used to send file flush requests to a checkpointer. The request structure needs to be inserted to a request queue in serial mode. |
| TwoPhaseStateLock                        | Used to prevent concurrent access to or modification on two-phase information sharing arrays. |
| TablespaceCreateLock                     | Used to check whether a tablespace already exists.           |
| BtreeVacuumLock                          | Used to prevent **VACUUM** from clearing pages that are being used by B-tree indexes. |
| AutovacuumLock                           | Used to access the autovacuum worker array in serial mode.   |
| AutovacuumScheduleLock                   | Used to distribute tables requiring **VACUUM** in serial mode. |
| AutoanalyzeLock                          | Used to obtain and release resources related to a task that allows for autoanalyze execution. |
| SyncScanLock                             | Used to determine the start position of a relfilenode during heap scanning. |
| NodeTableLock                            | Used to protect a shared structure that stores database node information. |
| PoolerLock                               | Used to prevent two threads from simultaneously obtaining the same connection from a connection pool. |
| RelationMappingLock                      | Used to wait for the mapping file between system catalogs and storage locations to be updated. |
| AsyncCtlLock                             | Used to prevent concurrent access to or concurrent modification on the sharing notification status. |
| AsyncQueueLock                           | Used to prevent concurrent access to or concurrent modification on the sharing notification queue. |
| SerializableXactHashLock                 | Used to prevent concurrent read/write or concurrent write/write on a sharing structure for serializable transactions. |
| SerializableFinishedListLock             | Used to prevent concurrent read/write or concurrent write/write on a shared linked list for completed serial transactions. |
| SerializablePredicateLockListLock        | Used to protect a linked list of serializable transactions that have locks. |
| OldSerXidLock                            | Used to protect a structure that records serializable transactions that have conflicts. |
| FileStatLock                             | Used to protect a data structure that stores statistics file information. |
| SyncRepLock                              | Used to protect Xlog synchronization information during primary/standby replication. |
| DataSyncRepLock                          | Used to protect data page synchronization information during primary/standby replication. |
| CStoreColspaceCacheLock                  | Used to add a lock when CU space is allocated for a column-store table. |
| CStoreCUCacheSweepLock                   | Used to add a lock when CU caches used by a column-store table are cyclically washed out. |
| MetaCacheSweepLock                       | Used to add a lock when metadata is cyclically washed out.   |
| ExtensionConnectorLibLock                | Adds a lock when a specific dynamic library is loaded or uninstalled in ODBC connection initialization scenarios. |
| SearchServerLibLock                      | Used to add a lock on the file read operation when a specific dynamic library is initially loaded in GPU-accelerated scenarios. |
| LsnXlogChkFileLock                       | Used to serially update the Xlog flush points for primary and standby servers recorded in a specific structure. |
| ReplicationSlotAllocationLock            | Used to add a lock when a primary server allocates stream replication slots during primary/standby replication. |
| ReplicationSlotControlLock               | Used to prevent concurrent update of stream replication slot status during primary/standby replication. |
| ResourcePoolHashLock                     | Used to prevent concurrent access to or concurrent modification on a resource pool table, a hash table. |
| WorkloadStatHashLock                     | Used to prevent concurrent access to or concurrent modification on a hash table that contains SQL requests from the primary node of the database side. |
| WorkloadIoStatHashLock                   | Used to prevent concurrent access to or concurrent modification on a hash table that contains I/O information of the current database node. |
| WorkloadCGroupHashLock                   | Used to prevent concurrent access to or concurrent modification on a hash table that contains Cgroup information. |
| OBSGetPathLock                           | Used to prevent concurrent read/write or concurrent write/write on an OBS path. |
| WorkloadUserInfoLock                     | Used to prevent concurrent access to or concurrent modification on a hash table that contains user information about load management. |
| WorkloadRecordLock                       | Used to prevent concurrent access to or concurrent modification on a hash table that contains requests received by the primary node of the databases during adaptive memory management. |
| WorkloadIOUtilLock                       | Used to protect a structure that records **iostat** and CPU load information. |
| WorkloadNodeGroupLock                    | Used to prevent concurrent access to or concurrent modification on a hash table that contains node group information in memory. |
| JobShmemLock                             | Used to protect global variables in the shared memory that is periodically read during a scheduled task. |
| OBSRuntimeLock                           | Used to obtain environment variables, for example, **GASSHOME**. |
| LLVMDumpIRLock                           | Used to export the assembly language for dynamically generating functions. |
| LLVMParseIRLock                          | Used to compile and parse a finished IR function from the IR file at the start position of a query. |
| CriticalCacheBuildLock                   | Used to load caches from a shared or local cache initialization file. |
| WaitCountHashLock                        | Used to protect a shared structure in user statement counting scenarios. |
| BufMappingLock                           | Used to protect operations on a shared-buffer mapping table. |
| LockMgrLock                              | Used to protect the information about a common lock structure. |
| PredicateLockMgrLock                     | Used to protect the information about a lock structure that has serializable transactions. |
| OperatorRealTLock                        | Used to prevent concurrent access to or concurrent modification on a global structure that contains real-time data at the operator level. |
| OperatorHistLock                         | Used to prevent concurrent access to or concurrent modification on a global structure that contains historical data at the operator level. |
| SessionRealTLock                         | Used to prevent concurrent access to or concurrent modification on a global structure that contains real-time data at the query level. |
| SessionHistLock                          | Used to prevent concurrent access to or concurrent modification on a global structure that contains historical data at the query level. |
| CacheSlotMappingLock                     | Used to protect global CU cache information.                 |
| BarrierLock                              | Used to ensure that only one thread is creating a barrier at a time. |
| dummyServerInfoCacheLock                 | Used to protect a global hash table where the information about MogDB connections is cached. |
| RPNumberLock                             | Used by a database node on a computing MogDB to count the number of threads for a task where plans are being executed. |
| ClusterRPLock                            | Used to control concurrent access on MogDB load data maintained in a CCN of MogDB. |
| CBMParseXlogLock                         | Used to protect the lock used when CBM parses Xlogs.         |
| RelfilenodeReuseLock                     | Used to prevent the link to a reused column attribute file from being canceled by mistake. |
| RcvWriteLock                             | Used to prevent concurrent call of **WalDataRcvWrite**.      |
| PercentileLock                           | Used to protect global percentile buffers.                   |
| CSNBufMappingLock                        | Used to protect CSN pages.                                   |
| UniqueSQLMappingLock                     | Used to protect a unique SQL hash table.                     |
| DelayDDLLock                             | Used to prevent concurrent DDL operations.                   |
| CLOG Ctl                                 | Used to prevent concurrent access to or concurrent modification on the Clog control data structure. |
| Async Ctl                                | Used to protect asynchronization buffers.                    |
| MultiXactOffset Ctl                      | Used to protect SLRU buffers of a MultiXact offset.          |
| MultiXactMember Ctl                      | Used to protect SLRU buffer of a MultiXact member.           |
| OldSerXid SLRU Ctl                       | Used to protect SLRU buffers of old transaction IDs.         |
| ReplicationSlotLock                      | Used to protect a replication slot.                          |
| PGPROCLock                               | Used to protect the PGPROC structure.                        |
| MetaCacheLock                            | Used to protect meta caches.                                 |
| DataCacheLock                            | Used to protect data caches.                                 |
| InstrUserLock                            | Used to protect a user hash table.                           |
| BadBlockStatHashLock                     | Used to protect the hash table **global_bad_block_stat**     |
| BufFreelistLock                          | Used to ensure the atomicity of free list operations in the shared buffer. |
| CUSlotListLock                           | Used to control concurrent operations on the slot of the column-store buffer. |
| AddinShmemInitLock                       | Used to protect the initialization of the shared memory object. |
| AlterPortLock                            | Used to protect the coordinator node from changing the registration port number. |
| FdwPartitionCaheLock                     | Management lock of the buffer of the HDFS partitioned table. |
| DfsConnectorCacheLock                    | Management lock of the DFSConnector buffer.                  |
| DfsSpaceCacheLock                        | Management lock of the HDFS tablespace management buffer.    |
| FullBuildXlogCopyStartPtrLock            | Used to protect Xlog copy operations in the full build.      |
| DfsUserLoginLock                         | Used for HDFS user login and authentication.                 |
| LogicalReplicationSlotPersistentDataLock | Used to protect data in the replication slot during logical replication. |
| WorkloadSessionInfoLock                  | Used to protect the access to the session info memory hash table of the load management module. |
| InstrWorkloadLock                        | Used to protect the access to the memory hash table that stores load management statistics. |
| PgfdwLock                                | Used by the management instance to establish a connection to the foreign server. |
| InstanceTimeLock                         | Used to obtain time information of sessions in an instance.  |
| XlogRemoveSegLock                        | Used to protect Xlog segment file recycling.                 |
| DnUsedSpaceHashLock                      | Used to update space usage information corresponding to a session. |
| CsnMinLock                               | Used to calculate CSNmin.                                    |
| GPCCommitLock                            | Used to protect the addition of the global Plan Cache hash table. |
| GPCClearLock                             | Used to protect the clearing of the global plan cache hash table. |
| GPCTimelineLock                          | Used to protect the timeline check of the global plan cache hash table. |
| TsTagsCacheLock                          | Used to manage the time series tag cache.                    |
| InstanceRealTLock                        | Used to protect the update of the hash table that stores shared instance statistics. |
| CLogBufMappingLock                       | Used to manage the cache of commit logs.                     |
| GPCMappingLock                           | Used to manage the global plan cache.                        |
| GPCPrepareMappingLock                    | Used to manage the global plan cache.                        |
| BufferIOLock                             | Used to protect I/O operations on pages in the shared buffer. |
| BufferContentLock                        | Used to protect the read and modification of the page content in the shared buffer. |
| CSNLOG Ctl                               | Used for CSN log management.                                 |
| DoubleWriteLock                          | Used to manage doublewrite operations.                       |
| RowPageReplicationLock                   | Used to manage data page replication of row-store.           |
| extension                                | Other lightweight locks                                      |
