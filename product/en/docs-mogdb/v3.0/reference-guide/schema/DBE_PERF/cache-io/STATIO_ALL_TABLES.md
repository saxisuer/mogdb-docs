---
title: STATIO_ALL_TABLES
summary: STATIO_ALL_TABLES
author: Guo Huan
date: 2021-04-19
---

# STATIO_ALL_TABLES

**STATIO_ALL_TABLES** contains one row for each table (including TOAST tables) in the current database, showing I/O statistics about specific tables.

**Table 1** STATIO_ALL_TABLES columns

| Name            | Type   | Description                                                  |
| :-------------- | :----- | :----------------------------------------------------------- |
| relid           | oid    | Table OID                                                    |
| schemaname      | name   | Name of the schema that the table is in                      |
| relname         | name   | Table name                                                   |
| heap_blks_read  | bigint | Number of disk blocks read from the table                    |
| heap_blks_hit   | bigint | Number of cache hits in the table                            |
| idx_blks_read   | bigint | Number of disk blocks read from all indexes in the table     |
| idx_blks_hit    | bigint | Number of cache hits in indexes in the table                 |
| toast_blks_read | bigint | Number of disk blocks read from the TOAST table (if any) in the table |
| toast_blks_hit  | bigint | Number of buffer hits in the TOAST table (if any) in the table |
| tidx_blks_read  | bigint | Number of disk blocks read from the TOAST table index (if any) in the table |
| tidx_blks_hit   | bigint | Number of buffer-hits in the TOAST table index (if any) in the table |
