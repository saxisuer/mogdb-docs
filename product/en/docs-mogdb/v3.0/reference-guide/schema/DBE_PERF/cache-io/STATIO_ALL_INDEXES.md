---
title: STATIO_ALL_INDEXES
summary: STATIO_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# STATIO_ALL_INDEXES

**STATIO_ALL_INDEXES** contains one row for each index in the current database, showing I/O statistics about specific indexes.

**Table 1** STATIO_ALL_INDEXES columns

| Name          | Type   | Description                                     |
| :------------ | :----- | :---------------------------------------------- |
| relid         | oid    | OID of the table that the index is created for  |
| indexrelid    | oid    | OID of the index                                |
| schemaname    | name   | Name of the schema that the index is in         |
| relname       | name   | Name of the table that the index is created for |
| indexrelname  | name   | Index name                                      |
| idx_blks_read | bigint | Number of disk blocks read from the index       |
| idx_blks_hit  | bigint | Number of cache hits in the index               |
