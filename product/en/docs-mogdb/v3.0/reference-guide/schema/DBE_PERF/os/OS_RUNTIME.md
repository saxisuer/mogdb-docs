---
title: OS_RUNTIME
summary: OS_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# OS_RUNTIME

**OS_RUNTIME** displays the running status of the current OS.

**Table 1** OS_RUNTIME columns

| Name       | Type    | Description                                              |
| :--------- | :------ | :------------------------------------------------------- |
| id         | integer | ID                                                       |
| name       | text    | Name of the OS running status                            |
| value      | numeric | Value of the OS running status                           |
| comments   | text    | Remarks of the OS running status                         |
| cumulative | boolean | Whether the value of the OS running status is cumulative |
