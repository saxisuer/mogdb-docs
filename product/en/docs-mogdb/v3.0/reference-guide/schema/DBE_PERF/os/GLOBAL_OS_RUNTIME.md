---
title: GLOBAL_OS_RUNTIME
summary: GLOBAL_OS_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_OS_RUNTIME

**GLOBAL_OS_RUNTIME** provides OS running status information about all normal nodes in MogDB.

**Table 1** GLOBAL_OS_RUNTIME columns

| **Name**   | **Type** | **Description**                                          |
| :--------- | :------- | :------------------------------------------------------- |
| node_name  | name     | Node name                                                |
| id         | integer  | ID                                                       |
| name       | text     | Name of the OS running status                            |
| value      | numeric  | Value of the OS running status                           |
| comments   | text     | Remarks of the OS running status                         |
| cumulative | boolean  | Whether the value of the OS running status is cumulative |
