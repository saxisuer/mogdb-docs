---
title: SUMMARY_STAT_ALL_INDEXES
summary: SUMMARY_STAT_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_ALL_INDEXES

**SUMMARY_STAT_ALL_INDEXES** contains every row of each index in databases in MogDB, showing statistics about accesses to specific indexes.

**Table 1** SUMMARY_STAT_ALL_INDEXES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| schemaname    | name     | Name of the schema that the index is in                      |
| relname       | name     | Name of the table for the index                              |
| indexrelname  | name     | Index name                                                   |
| idx_scan      | numeric  | Number of index scans initiated on the index                 |
| idx_tup_read  | numeric  | Number of index entries returned by scans on the index       |
| idx_tup_fetch | numeric  | Number of live table rows fetched by simple index scans using the index |
