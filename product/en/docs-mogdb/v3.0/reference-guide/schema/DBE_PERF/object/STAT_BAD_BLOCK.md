---
title: STAT_BAD_BLOCK
summary: STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# STAT_BAD_BLOCK

**STAT_BAD_BLOCK** displays information about table and index read failures on the current node.

**Table 1** STAT_BAD_BLOCK columns

| **Name**     | **Type**                 | **Description**                         |
| :----------- | :----------------------- | :-------------------------------------- |
| nodename     | text                     | Database process name                   |
| databaseid   | integer                  | OID of the database                     |
| tablespaceid | integer                  | OID of the tablespace                   |
| relfilenode  | integer                  | File node of this relation              |
| bucketid     | smallint                 | ID of the bucket for consistent hashing |
| forknum      | integer                  | Fork number                             |
| error_count  | integer                  | Number of errors                        |
| first_time   | timestamp with time zone | Time when the first bad block occurred  |
| last_time    | timestamp with time zone | Time when the last bad block occurred   |
