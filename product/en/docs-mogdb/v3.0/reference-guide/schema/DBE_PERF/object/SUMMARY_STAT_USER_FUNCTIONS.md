---
title: SUMMARY_STAT_USER_FUNCTIONS
summary: SUMMARY_STAT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_USER_FUNCTIONS

**SUMMARY_STAT_USER_FUNCTIONS** displays statistics about user-defined views on all database nodes.

**Table 1** SUMMARY_STAT_USER_FUNCTIONS columns

| **Name**   | **Type**         | **Description**                                              |
| :--------- | :--------------- | :----------------------------------------------------------- |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Function name                                                |
| calls      | numeric          | Number of times that the function has been called            |
| total_time | double precision | Total time spent in this function and all other functions called by it (unit: ms) |
| self_time  | double precision | Total time spent in this function, excluding other functions called by it (unit: ms) |
