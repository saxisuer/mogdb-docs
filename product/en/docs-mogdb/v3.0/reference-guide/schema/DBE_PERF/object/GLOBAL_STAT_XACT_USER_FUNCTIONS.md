---
title: GLOBAL_STAT_XACT_USER_FUNCTIONS
summary: GLOBAL_STAT_XACT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_XACT_USER_FUNCTIONS

**GLOBAL_STAT_XACT_USER_FUNCTIONS** displays statistics about function executions in transactions on each node.

**Table 1** GLOBAL_STAT_XACT_USER_FUNCTIONS columns

| **Name**   | **Type**         | **Description**                                              |
| :--------- | :--------------- | :----------------------------------------------------------- |
| node_name  | name             | Node name                                                    |
| funcid     | oid              | OID of a function                                            |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Function name                                                |
| calls      | bigint           | Number of times that the function has been called            |
| total_time | double precision | Total time spent in this function and all other functions called by it |
| self_time  | double precision | Total time spent in this function, excluding other functions called by it |
