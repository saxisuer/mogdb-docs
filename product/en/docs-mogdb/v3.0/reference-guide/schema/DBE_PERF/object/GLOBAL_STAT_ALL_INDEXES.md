---
title: GLOBAL_STAT_ALL_INDEXES
summary: GLOBAL_STAT_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_ALL_INDEXES

**GLOBAL_STAT_ALL_INDEXES** contains every row of each index in databases on each node, showing statistics about accesses to specific indexes.

**Table 1** GLOBAL_STAT_ALL_INDEXES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| node_name     | name     | Node name                                                    |
| relid         | oid      | OID of the table for this index                              |
| indexrelid    | oid      | OID of the index                                             |
| schemaname    | name     | Name of the schema that the index is in                      |
| relname       | name     | Name of the table for the index                              |
| indexrelname  | name     | Index name                                                   |
| idx_scan      | bigint   | Number of index scans initiated on the index                 |
| idx_tup_read  | bigint   | Number of index entries returned by scans on the index       |
| idx_tup_fetch | bigint   | Number of live table rows fetched by simple index scans using the index |
