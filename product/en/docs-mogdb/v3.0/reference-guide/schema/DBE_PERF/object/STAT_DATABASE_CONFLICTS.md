---
title: STAT_DATABASE_CONFLICTS
summary: STAT_DATABASE_CONFLICTS
author: Guo Huan
date: 2021-04-19
---

# STAT_DATABASE_CONFLICTS

**STAT_DATABASE_CONFLICTS** displays statistics about database conflicts on the current node.

**Table 1** STAT_DATABASE_CONFLICTS columns

| **Name**         | **Type** | **Description**                   |
| :--------------- | :------- | :-------------------------------- |
| datid            | oid      | Database ID                       |
| datname          | name     | Database name                     |
| confl_tablespace | bigint   | Number of conflicting tablespaces |
| confl_lock       | bigint   | Number of conflicting locks       |
| confl_snapshot   | bigint   | Number of conflicting snapshots   |
| confl_bufferpin  | bigint   | Number of conflicting buffers     |
| confl_deadlock   | bigint   | Number of conflicting deadlocks   |
