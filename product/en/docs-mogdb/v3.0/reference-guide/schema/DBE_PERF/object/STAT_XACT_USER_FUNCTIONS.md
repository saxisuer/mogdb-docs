---
title: STAT_XACT_USER_FUNCTIONS
summary: STAT_XACT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# STAT_XACT_USER_FUNCTIONS

**STAT_XACT_USER_FUNCTIONS** displays statistics about function executions in the current transaction.

**Table 1** STAT_XACT_USER_FUNCTIONS columns

| Name       | Type             | Description                                                  |
| :--------- | :--------------- | :----------------------------------------------------------- |
| funcid     | oid              | OID of a function                                            |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Function name                                                |
| calls      | bigint           | Number of times the function has been called                 |
| total_time | double precision | Total time spent in the function and all other functions called by it |
| self_time  | double precision | Total time spent in this function, excluding other functions called by it |
