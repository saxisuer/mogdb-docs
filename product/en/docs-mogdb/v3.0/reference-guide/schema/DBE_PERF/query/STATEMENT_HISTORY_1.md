---
title: STATEMENT_HISTORY
summary: STATEMENT_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_HISTORY

**STATEMENT_HISTORY** displays information about execution statements on the current node. To query this system catalog, you must have the **sysadmin** or **monitor admin** permission. The result can be queried only in the system database but cannot be queried in the user database

**Table 1** STATEMENT_HISTORY columns

| Name                 | Type                     | Description                                                  |
| :------------------- | :----------------------- | :----------------------------------------------------------- |
| dbname               | name                     | Database name.                                               |
| schemaname           | name                     | Schema name.                                                 |
| origin_node          | integer                  | Node name.                                                   |
| user_name            | name                     | Username.                                                    |
| application_name     | text                     | Name of the application that sends a request.                |
| client_addr          | text                     | IP address of the client that sends a request.               |
| client_port          | integer                  | Port number of the client that sends a request.              |
| unique_query_id      | bigint                   | ID of the unique SQL statement.                          |
| debug_query_id       | bigint                   | ID of the debug SQL statement.                              |
| query                | text                     | Unique SQL statement.                                    |
| start_time           | timestamp with time zone | Time when a statement starts.                                |
| finish_time          | timestamp with time zone | Time when a statement ends.                                  |
| slow_sql_threshold   | bigint                   | Standard for slow SQL statement execution.                   |
| transaction_id       | bigint                   | Transaction ID.                                              |
| thread_id            | bigint                   | ID of an execution thread.                                   |
| session_id           | bigint                   | Session ID of a user.                                        |
| n_soft_parse         | bigint                   | Number of soft parsing times. The value of **n_soft_parse** plus the value of **n_hard_parse** may be greater than the value of **n_calls** because the number of subqueries is not counted in the value of **n_calls**. |
| n_hard_parse         | bigint                   | Number of hard parsing times. The value of **n_soft_parse** plus the value of **n_hard_parse** may be greater than the value of **n_calls** because the number of subqueries is not counted in the value of **n_calls**. |
| query_plan           | text                     | Statement execution plan.                                    |
| n_returned_rows      | bigint                   | Number of rows in the result set returned by the **SELECT** statement. |
| n_tuples_fetched     | bigint                   | Number of rows randomly scanned.                             |
| n_tuples_returned    | bigint                   | Number of rows sequentially scanned.                         |
| n_tuples_inserted    | bigint                   | Number of rows inserted.                                     |
| n_tuples_updated     | bigint                   | Number of rows updated.                                      |
| n_tuples_deleted     | bigint                   | Number of rows deleted.                                      |
| n_blocks_fetched     | bigint                   | Number of buffer block access times.                         |
| n_blocks_hit         | bigint                   | Number of buffer block hits.                                 |
| db_time              | bigint                   | Valid DB time, which is accumulated if multiple threads are involved (unit: μs). |
| cpu_time             | bigint                   | CPU time (unit: μs).                                         |
| execution_time       | bigint                   | Execution time in the executor (unit: μs).                   |
| parse_time           | bigint                   | SQL parsing time (unit: μs).                                 |
| plan_time            | bigint                   | SQL plan generation time (unit: μs).                         |
| rewrite_time         | bigint                   | SQL rewriting time (unit: μs).                               |
| pl_execution_time    | bigint                   | Execution time of PL/pgSQL (unit: μs).                       |
| pl_compilation_time  | bigint                   | Compilation time of PL/pgSQL (unit: μs).                     |
| data_io_time         | bigint                   | I/O time (unit: μs).                                         |
| net_send_info        | text                     | Network status of messages sent through a physical connection, including the time (unit: μs), number of calls, and throughput (unit: byte). This can be used to analyze the network overhead of SQL statements in a distributed system and is not supported in standalone system. Example: {“time”:xxx, “n_calls”:xxx, “size”:xxx}. |
| net_recv_info        | text                     | Network status of messages received through a physical connection, including the time (unit: μs), number of calls, and throughput (unit: byte). This column can be used to analyze the network overhead of SQL in a distributed system. This column is not supported in a standalone mode. Example: {“time”:xxx, “n_calls”:xxx, “size”:xxx}. |
| net_stream_send_info | text                     | Network status of messages sent through a logical connection, including the time (unit: μs), number of calls, and throughput (unit: byte). This column can be used to analyze the network overhead of SQL in a distributed system. This column is not supported in a standalone mode. Example: {“time”:xxx, “n_calls”:xxx, “size”:xxx}. |
| net_stream_recv_info | text                     | Network status of messages received through a logical connection, including the time (unit: μs), number of calls, and throughput (unit: byte). This column can be used to analyze the network overhead of SQL in a distributed system. This column is not supported in a standalone mode. Example: {“time”:xxx, “n_calls”:xxx, “size”:xxx}. |
| lock_count           | bigint                   | Number of locks.                                             |
| lock_time            | bigint                   | Time required for locking.                                   |
| lock_wait_count      | bigint                   | Number of lock waits.                                        |
| lock_wait_time       | bigint                   | Time required for lock waiting.                              |
| lock_max_count       | bigint                   | Maximum number of locks.                                     |
| lwlock_count         | bigint                   | Number of lightweight locks (reserved).                      |
| lwlock_wait_count    | bigint                   | Number of lightweight lock waits.                            |
| lwlock_time          | bigint                   | Time required for lightweight locking (reserved).            |
| lwlock_wait_time     | bigint                   | Time required for lightweight lock waiting.                  |
| details              | bytea                    | List of statement lock events, which are recorded in time sequence. The number of records is affected by the **track_stmt_details_size** parameter.<br/>Events include:<br/>- Start locking.<br/>- Complete locking.<br/>- Start lock waiting.<br/>- Complete lock waiting.<br/>- Start unlocking.<br/>- Complete unlocking.<br/>- Start lightweight lock waiting.<br/>- Complete lightweight lock waiting. |
| is_slow_sql          | boolean                  | Whether the SQL statement is a slow SQL statement.           |
| trace_id             | text                     | Trace ID transmitted by a driver which is related to one application request. |

## Related Feature

This feature corresponding to the system catalog `statement_history`. Its main purpose is to record SQL statements and running information generated during the database running, to ensure that the SQL information can still be queried even if the database is restarted.

General usage syntax:

```sql
MogDB=# select * from DBE_PERF.statement_history;
```

It is mainly controlled by the following parameters:

- `log_duration`: indicates whether to record slow queries.

- `log_min_duration_statement`: marks the slow query time (unit: millisecond) of a SQL statement. `0` indicates that the slow query time of all SQL statements is recorded. `-1` indicates that no information is recorded.

- `track_stmt_stat_level`: The default value is `OFF, L0`. If the first value is not `OFF`, all SQL statements are recorded. If the first value is `OFF` and the second value is not `OFF`, only slow SQL statements are recorded.

- `track_stmt_parameter`: tracks the statement in detail.

The code logic needs to meet one of the following conditions:

1. Dynamic statement tracking is enabled: STMT is tracked using `dynamic_func_control`.

2. `track_stmt_stat_level` tracks a SQL statement whose first value is `L0` or higher.

3. `track_stmt_stat_level` tracks a SQL statement whose second value is `L0` or higher. The statement runtime is greater than the value of `log_min_duration_statement`, the value of `log_min_duration_statement` is greater than or equal to 0, and `track_stmt_statement` is not enabled.

4. `track_stmt_parameter` is enabled and the first value of `track_stmt_stat_level` (consumed DBTIME) is greater than 0.