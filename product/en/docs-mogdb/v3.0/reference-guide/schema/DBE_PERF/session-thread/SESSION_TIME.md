---
title: SESSION_TIME
summary: SESSION_TIME
author: Guo Huan
date: 2021-04-19
---

# SESSION_TIME

**SESSION_TIME** collects statistics about the running time of session threads and time consumed in each execution phase on the current node.

**Table 1** SESSION_TIME columns

| Name      | Type    | Description              |
| :-------- | :------ | :----------------------- |
| sessid    | text    | Thread start time and ID |
| stat_id   | integer | Statistics ID            |
| stat_name | text    | Session type name        |
| value     | bigint  | Session value            |
