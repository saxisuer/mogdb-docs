---
title: SESSION_MEMORY_DETAIL
summary: SESSION_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# SESSION_MEMORY_DETAIL

**SESSION_MEMORY_DETAIL** collects statistics about thread memory usage by MemoryContext node.

**Table 1** SESSION_MEMORY_DETAIL columns

| Name        | Type     | Description                             |
| :---------- | :------- | :-------------------------------------- |
| sessid      | text     | Thread start time and ID                |
| sesstype    | text     | Thread name                             |
| contextname | text     | Name of the memory context              |
| level       | smallint | Level of memory context importance      |
| parent      | text     | Name of the parent memory context       |
| totalsize   | bigint   | Size of the applied memory (unit: byte) |
| freesize    | bigint   | Size of the idle memory (unit: byte)    |
| usedsize    | bigint   | Size of the used memory (unit: byte)    |
