---
title: PG_STATIO_ALL_INDEXES
summary: PG_STATIO_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_ALL_INDEXES

**PG_STATIO_ALL_INDEXES** contains one row for each index in the current database, showing I/O statistics about accesses to that specific index.

**Table 1** PG_STATIO_ALL_INDEXES columns

| Name          | Type   | Description                                     |
| :------------ | :----- | :---------------------------------------------- |
| relid         | oid    | OID of the table for this index                 |
| indexrelid    | oid    | OID of the index                                |
| schemaname    | name   | Name of the schema that the index is in         |
| relname       | name   | Name of the table that the index is created for |
| indexrelname  | name   | Index name                                      |
| idx_blks_read | bigint | Number of disk blocks read from the index       |
| idx_blks_hit  | bigint | Number of cache hits in the index               |
