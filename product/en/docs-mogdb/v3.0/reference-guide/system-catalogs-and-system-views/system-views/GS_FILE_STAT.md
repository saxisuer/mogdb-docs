---
title: GS_FILE_STAT
summary: GS_FILE_STAT
author: Guo Huan
date: 2021-04-19
---

# GS_FILE_STAT

**GS_FILE_STAT** records statistics about data file I/O to indicate I/O performance and detect performance problems such as abnormal I/O operations.

**Table 1** GS_FILE_STAT columns

| Name      | Type   | Description                                              |
| :-------- | :----- | :------------------------------------------------------- |
| filenum   | oid    | File identifier                                          |
| dbid      | oid    | Database ID                                              |
| spcid     | oid    | Tablespace ID                                            |
| phyrds    | bigint | Number of times of reading physical files                |
| phywrts   | bigint | Number of times of writing into physical files           |
| phyblkrd  | bigint | Number of times of reading physical file blocks          |
| phyblkwrt | bigint | Number of times of writing into physical file blocks     |
| readtim   | bigint | Total duration of reading, in microseconds               |
| writetim  | bigint | Total duration of writing, in microseconds               |
| avgiotim  | bigint | Average duration of reading and writing, in microseconds |
| lstiotim  | bigint | Duration of the last file reading, in microseconds       |
| miniotim  | bigint | Minimum duration of reading and writing, in microseconds |
| maxiowtm  | bigint | Maximum duration of reading and writing, in microseconds |
