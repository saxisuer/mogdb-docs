---
title: GS_GSC_MEMORY_DETAIL
summary: GS_GSC_MEMORY_DETAIL
author: Guo Huan
date: 2022-05-12
---

# GS_GSC_MEMORY_DETAIL

**GS_GSC_MEMORY_DETAIL** displays the memory usage of the global system cache of the current process on the current node. This view is displayed only when GSC is enabled. Note that the query is separated by the database memory context. Therefore, some memory statistics are missing. The memory context corresponding to the missing memory statistics is **GlobalSysDBCache**.

**Table 1** GS_GSC_MEMORY_DETAIL columns

| Name      | Type    | Description                                    |
| :-------- | :------ | :--------------------------------------------- |
| db_id     | integer | Database ID.                                   |
| totalsize | bigint  | Total size of the shared memory, in bytes.     |
| freesize  | bigint  | Remaining size of the shared memory, in bytes. |
| usedsize  | bigint  | Used size of the shared memory, in bytes.      |