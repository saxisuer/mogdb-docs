---
title: GS_TOTAL_MEMORY_DETAIL
summary: GS_TOTAL_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GS_TOTAL_MEMORY_DETAIL

**GS_TOTAL_MEMORY_DETAIL** collects statistics about memory usage of the current database node. This view is unavailable when **enable_memory_limit** is set to **off**.

**Table 1** GS_TOTAL_MEMORY_DETAIL columns

| Name         | Type    | Description                                                  |
| :----------- | :------ | :----------------------------------------------------------- |
| nodename     | text    | Node name                                                    |
| memorytype   | text    | Memory type. The value must be one of the following:<br/>- **max_process_memory**: memory occupied by a MogDB instance<br/>- **process_used_memory**: memory occupied by a MogDB process<br/>- **max_dynamic_memory**: maximum dynamic memory<br/>- **dynamic_used_memory**: used dynamic memory<br/>- **dynamic_peak_memory**: dynamic peak value of the memory<br/>- **dynamic_used_shrctx**: maximum dynamic shared memory context<br/>- **dynamic_peak_shrctx**: dynamic peak value of the shared memory context<br/>- **max_shared_memory**: maximum shared memory<br/>- **shared_used_memory**: used shared memory<br/>- **max_cstore_memory**: maximum memory allowed by the column<br/>- **cstore_used_memory**: memory used in column storage<br/>- **max_sctpcomm_memory**: maximum memory allowed for the communication library<br/>- **sctpcomm_used_memory**: memory used by the communication library<br/>- **sctpcomm_peak_memory**: memory peak of the communication library<br/>- **other_used_memory**: other used memory |
| memorymbytes | integer | Size of allocated memory-typed memory                        |
