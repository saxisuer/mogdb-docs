---
title: GS_WLM_SESSION_INFO
summary: GS_WLM_SESSION_INFO
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_SESSION_INFO

**GS_WLM_SESSION_INFO** displays load management information about a completed job executed on the database instance. Only the user with sysadmin permission can query this view.

For details about the columns, see Table 1 in [GS_WLM_SESSION_HISTORY](GS_WLM_SESSION_HISTORY.md).