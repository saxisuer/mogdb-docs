---
title: PG_WLM_STATISTICS
summary: PG_WLM_STATISTICS
author: Guo Huan
date: 2022-05-13
---

# PG_WLM_STATISTICS

**PG_WLM_STATISTICS** displays information about workload management after the task is complete or the exception has been handled. Only the user with sysadmin permission can query this view.

**Table 1** PG_WLM_STATISTICS columns

| Name               | Type    | Description                                                  |
| :----------------- | :------ | :----------------------------------------------------------- |
| statement          | text    | Statement executed for exception handling.                   |
| block_time         | bigint  | Block time before the statement is executed.                 |
| elapsed_time       | bigint  | Elapsed time when the statement is executed.                 |
| total_cpu_time     | bigint  | Total time used by the CPU on the database instance when the statement is executed for exception handling. |
| qualification_time | bigint  | Period when the statement checks the skew.                   |
| cpu_skew_percent   | integer | CPU usage skew on the database instance when the statement is executed for exception handling. |
| control_group      | text    | Cgroup used when the statement is executed for exception handling. |
| status             | text    | Statement status after statement are executed for exception handling, including:<br/>- **pending**: waiting to be executed.<br/>- **running**: being executed.<br/>- **finished**: finished normally.<br/>- **abort**: terminated unexpectedly. |
| action             | text    | Actions when statements are executed for exception handling, including:<br/>- **abort**: terminating the operation.<br/>- **adjust**: executing the Cgroup adjustment operations. Currently, you can only perform the demotion operation.<br/>- **finish**: finished normally. |
