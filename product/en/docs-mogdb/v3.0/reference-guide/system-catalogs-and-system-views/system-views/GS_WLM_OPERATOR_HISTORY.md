---
title: GS_WLM_OPERATOR_HISTORY
summary: GS_WLM_OPERATOR_HISTORY
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_OPERATOR_HISTORY

**GS_WLM_OPERATOR_HISTORY** displays records of operators in jobs that have been executed by the current user on the current primary database node. Only the user with sysadmin permission can query this view.

Columns in this view are the same as those in Table 1 in [GS_WLM_OPERATOR_INFO](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_OPERATOR_INFO.md).