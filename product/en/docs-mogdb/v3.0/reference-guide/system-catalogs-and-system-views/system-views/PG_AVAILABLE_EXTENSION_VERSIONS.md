---
title: PG_AVAILABLE_EXTENSION_VERSIONS
summary: PG_AVAILABLE_EXTENSION_VERSIONS
author: Guo Huan
date: 2021-04-19
---

# PG_AVAILABLE_EXTENSION_VERSIONS

**PG_AVAILABLE_EXTENSION_VERSIONS** displays extension versions of certain database features.

**Table 1** PG_AVAILABLE_EXTENSION_VERSIONS columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| name        | name    | Extension name                                               |
| version     | text    | Version name                                                 |
| installed   | Boolean | Whether this extension version is installed                  |
| superuser   | Boolean | Whether only system administrators are allowed to install the extension |
| relocatable | Boolean | Whether the extension can be relocated to another schema     |
| schema      | name    | Name of the schema that the extension must be installed into (**NULL** if the extension is partially or fully relocatable) |
| requires    | name[]  | Names of prerequisite extensions (**NULL** if none)          |
| comment     | text    | Comment string from the extension's control file             |
