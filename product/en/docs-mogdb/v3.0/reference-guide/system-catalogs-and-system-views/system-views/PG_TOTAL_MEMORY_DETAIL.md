---
title: PG_TOTAL_MEMORY_DETAIL
summary: PG_TOTAL_MEMORY_DETAIL
author: Guo Huan
date: 2021-06-15
---

# PG_TOTAL_MEMORY_DETAIL

**PG_TOTAL_MEMORY_DETAIL** displays memory usage of a node in the database.

**Table 1** PG_TOTAL_MEMORY_DETAIL columns

| Name         | Type    | Description                                |
| :----------- | :------ | :----------------------------------------- |
| nodename     | text    | Specifies the node name.                   |
| memorytype   | text    | Memory name                                |
| memorymbytes | integer | Size of the used memory in the unit of MB. |
