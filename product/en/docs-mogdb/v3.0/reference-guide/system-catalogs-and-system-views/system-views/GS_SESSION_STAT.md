---
title: GS_SESSION_STAT
summary: GS_SESSION_STAT
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_STAT

**GS_SESSION_STAT** collects statistics about session states based on session threads or the **AutoVacuum** thread.

**Table 1** GS_SESSION_STAT columns

| Name     | Type    | Description                     |
| :------- | :------ | :------------------------------ |
| sessid   | text    | Thread ID and start time        |
| statid   | integer | Statistics ID                   |
| statname | text    | Name of the statistics session  |
| statunit | text    | Unit of the statistics session  |
| value    | bigint  | Value of the statistics session |
