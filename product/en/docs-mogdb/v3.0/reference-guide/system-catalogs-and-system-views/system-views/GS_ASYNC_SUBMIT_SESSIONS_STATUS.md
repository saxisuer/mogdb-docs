---
title: GS_ASYNC_SUBMIT_SESSIONS_STATUS
summary: GS_ASYNC_SUBMIT_SESSIONS_STATUS
author: Guo Huan
date: 2022-06-13
---

# GS_ASYNC_SUBMIT_SESSIONS_STATUS

**GS_ASYNC_SUBMIT_SESSIONS_STATUS** checks the status of a session submitted asynchronously. 

**表 1** GS_ASYNC_SUBMIT_SESSIONS_STATUS fields

| Name                               | Type | Description                                             |
| ------------------------------------- | ------- | ------------------------------------------------------------ |
| nodename                              | text    | Node name                                               |
| groupid                               | int     | Thread group ID                                    |
| waiting_xlog_flush_session_num        | int     | Number of sessions waiting for log flushing |
| waiting_sync_rep_receive_session_num | int     | Number of sessions waiting for backing up and synchronizing the receive logs |
| waiting_sync_rep_write_session_num   | int     | Number of sessions waiting for backing up and synchronizing the write logs |
| waiting_sync_rep_flush_session_num   | int     | Number of sessions waiting for backing up and synchronizing the flush logs |
| waiting_sync_rep_apply_session_num   | int     | Number of sessions waiting for backing up and synchronizing the apply logs |
| waiting_sync_paxos_session_num        | integer | Number of sessions waiting for DCF to synchronize logs |
| waiting_commit_session_num            | int     | Number of sessions with log flushing finished (and backup synchronization) but with asynchronous submission uncompleted |
| finished_commit_session_num           | int     | Number of sessions with asynchronous submission finished |
