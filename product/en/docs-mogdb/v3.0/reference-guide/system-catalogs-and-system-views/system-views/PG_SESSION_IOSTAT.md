---
title: PG_SESSION_IOSTAT
summary: PG_SESSION_IOSTAT
author: Guo Huan
date: 2022-05-13
---

# PG_SESSION_IOSTAT

**PG_SESSION_IOSTAT** shows I/O load management information about the task currently executed by the user. Only users with the **sysadmin** or **monitor admin** permission can query this view.

IOPS is counted by ones for column store and by 10 thousands for row store.

**Table 1** PG_SESSION_IOSTAT columns

| Name           | Type    | Description                                                  |
| :------------- | :------ | :----------------------------------------------------------- |
| query_id       | bigint  | Job ID                                                       |
| mincurriops    | integer | Minimum I/O of the job across database instances             |
| maxcurriops    | integer | Maximum I/O of the job across database instances             |
| minpeakiops    | integer | Minimum peak I/O of the current job across database instances |
| maxpeakiops    | integer | Maximum peak I/O of the current job across database instances |
| io_limits      | integer | **io_limits** set for the job                                |
| io_priority    | text    | **io_priority** set for the job                              |
| query          | text    | Job                                                          |
| node_group     | text    | Unsupported currently                                        |
| curr_io_limits | integer | Real-time **io_limits** value when **io_priority** is used to control I/Os |