---
title: GS_WLM_SESSION_STATISTICS
summary: GS_WLM_SESSION_STATISTICS
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_SESSION_STATISTICS

**GS_WLM_SESSION_STATISTICS** displays load management information about jobs being executed by the current user on the database instance. Only users with the **sysadmin** permission can query this view.

**Table 1** GS_WLM_SESSION_STATISTICS columns

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| datid               | oid                      | OID of the database the backend is connected to              |
| dbname              | name                     | Name of the database the backend is connected to             |
| schemaname          | text                     | Schema name                                                  |
| nodename            | text                     | Name of the database instance where the statement is executed |
| username            | name                     | Username used for connecting to the backend                  |
| application_name    | text                     | Name of the application connected to the backend             |
| client_addr         | inet                     | IP address of the client connected to the backend. If this column is null, it indicates either that the client is connected via a Unix socket on the server machine or that this is an internal process, such as autovacuum. |
| client_hostname     | text                     | Host name of the connected client, as reported by a reverse DNS lookup of client_addr. This column will be non-null only for IP connections and only when log_hostname is enabled. |
| client_port         | integer                  | TCP port number that the client uses for communication with the backend (**–1** if a Unix socket is used) |
| query_band          | text                     | Job type, which is specified by the GUC parameter **query_band**. The default value is a null string. |
| pid                 | bigint                   | ID of the backend thread                                     |
| sessionid           | bigint                   | Session ID                                                   |
| block_time          | bigint                   | Block time before the statement is run. The unit is ms.      |
| start_time          | timestamp with time zone | Time when the statement execution starts                     |
| duration            | bigint                   | Duration that the statement has been executed (unit: ms)     |
| estimate_total_time | bigint                   | Estimated execution time of the statement. The unit is ms.   |
| estimate_left_time  | bigint                   | Estimated remaining time of statement execution. The unit is ms. |
| enqueue             | text                     | Resource status in workload management                       |
| resource_pool       | name                     | Resource pool used by the user                               |
| control_group       | text                     | Cgroup used by the statement                                 |
| estimate_memory     | integer                  | Estimated memory used by the statement. The unit is MB.      |
| min_peak_memory     | integer                  | Minimum memory peak of the statement across the database instances, in MB |
| max_peak_memory     | integer                  | Maximum memory peak of the statement across the database instances, in MB |
| average_peak_memory | integer                  | Average memory usage during statement execution, in MB       |
| memory_skew_percent | integer                  | Memory usage skew of the statement among the database instances |
| spill_info          | text                     | Information about statement spill to the database instances:<br/>- **None**: The statement has not been spilled to disks on the database instances.<br/>- **All**: The statement has been spilled to disks on the database instances.<br/>- **[a:b]**: The statement has been spilled to disks on *a* of *b* database instances. |
| min_spill_size      | integer                  | Minimum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| max_spill_size      | integer                  | Maximum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| average_spill_size  | integer                  | Average spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| spill_skew_percent  | integer                  | Database instance spill skew when a spill occurs             |
| min_dn_time         | bigint                   | Minimum execution time of the statement across the database instances (unit: ms) |
| max_dn_time         | bigint                   | Maximum execution time of the statement across the database instances (unit: ms) |
| average_dn_time     | bigint                   | Average execution time of the statement across the database instances (unit: ms) |
| dntime_skew_percent | integer                  | Execution time skew of the statement among the database instances |
| min_cpu_time        | bigint                   | Minimum CPU time of the statement across the database instances (unit: ms) |
| max_cpu_time        | bigint                   | Maximum CPU time of the statement across the database instances (unit: ms) |
| total_cpu_time      | bigint                   | Total CPU time of the statement across the database instances (unit: ms) |
| cpu_skew_percent    | integer                  | CPU time skew of the statement among the database instances  |
| min_peak_iops       | integer                  | Minimum IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| max_peak_iops       | integer                  | Maximum IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| average_peak_iops   | integer                  | Average IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| iops_skew_percent   | integer                  | I/O skew of the statement among the database instances       |
| warning             | text                     | Warning. The following warnings are displayed:<br/>- Spill file size large than 256MB<br/>- Broadcast size large than 100MB<br/>- Early spill<br/>- Spill times is greater than 3<br/>- Spill on memory adaptive<br/>- Hash table conflict |
| queryid             | bigint                   | Internal query ID used for statement execution               |
| query               | text                     | Statement that is being executed                             |
| query_plan          | text                     | Execution plan of the statement                              |
| node_group          | text                     | Logical database instance of the user to which the statement belongs |
| top_cpu_dn          | text                     | Top *N* CPU usage                                            |
| top_mem_dn          | text                     | Top *N* memory usage                                         |