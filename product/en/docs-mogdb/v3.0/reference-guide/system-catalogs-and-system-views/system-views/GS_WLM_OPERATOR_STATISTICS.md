---
title: GS_WLM_OPERATOR_STATISTICS
summary: GS_WLM_OPERATOR_STATISTICS
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_OPERATOR_STATISTICS

**GS_WLM_OPERATOR_STATISTICS** displays the operators of the jobs that are being executed by the current user. Only users with the **sysadmin** permission can query this view.

**Table 1** GS_WLM_OPERATOR_STATISTICS columns

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| queryid             | bigint                   | Internal query ID used for statement execution               |
| pid                 | bigint                   | Thread ID of the backend                                     |
| plan_node_id        | integer                  | Plan node ID of the execution plan                           |
| plan_node_name      | text                     | Name of the operator corresponding to the plan node ID       |
| start_time          | timestamp with time zone | Time when the operator starts to process the first data record |
| duration            | bigint                   | Total execution time of the operator, in ms                  |
| status              | text                     | Execution status of the current operator, which can be **finished** or **running** |
| query_dop           | integer                  | DOP of the operator                                          |
| estimated_rows      | bigint                   | Number of rows estimated by the optimizer                    |
| tuple_processed     | bigint                   | Number of elements returned by the operator                  |
| min_peak_memory     | integer                  | Minimum peak memory used by the operator on database instances (unit: MB) |
| max_peak_memory     | integer                  | Maximum peak memory used by the operator on database instances (unit: MB) |
| average_peak_memory | integer                  | Average peak memory used by the operator on database instances (unit: MB) |
| memory_skew_percent | integer                  | Memory usage skew of the operator among database instances   |
| min_spill_size      | integer                  | Minimum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| max_spill_size      | integer                  | Maximum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| average_spill_size  | integer                  | Average spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| spill_skew_percent  | integer                  | Database instance spill skew when a spill occurs             |
| min_cpu_time        | bigint                   | Minimum execution time of the operator on database instances (unit: ms) |
| max_cpu_time        | bigint                   | Maximum execution time of the operator on database instances (unit: ms) |
| total_cpu_time      | bigint                   | Total execution time of the operator on database instances (unit: ms) |
| cpu_skew_percent    | integer                  | Execution time skew among database instances                 |
| warning             | text                     | Warning. The following warnings are displayed:<br/>- Sort/SetOp/HashAgg/HashJoin spill<br/>- Spill file size large than 256MB<br/>- Broadcast size large than 100MB<br/>- Early spill<br/>- Spill times is greater than 3<br/>- Spill on memory adaptive<br/>- Hash table conflict |