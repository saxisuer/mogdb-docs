---
title: GET_GLOBAL_PREPARED_XACTS
summary: GET_GLOBAL_PREPARED_XACTS
author: Zhang Cuiping
date: 2021-10-11
---

# GET_GLOBAL_PREPARED_XACTS (Discarded)

This view is not supported in centralized and MogDB systems.
