---
title: PG_STATIO_SYS_SEQUENCES
summary: PG_STATIO_SYS_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_SYS_SEQUENCES

**PG_STATIO_SYS_SEQUENCES** displays I/O status information about all the sequences in the namespace.

**Table 1** PG_STATIO_SYS_SEQUENCES columns

| Name       | Type   | Description                                  |
| :--------- | :----- | :------------------------------------------- |
| relid      | oid    | OID of this sequence                         |
| schemaname | name   | Name of the schema where the sequence is in  |
| relname    | name   | Name of the sequence                         |
| blks_read  | bigint | Number of disk blocks read from the sequence |
| blks_hit   | bigint | Cache hits in the sequence                   |
