---
title: GS_INSTANCE_TIME
summary: GS_INSTANCE_TIME
author: Guo Huan
date: 2021-04-19
---

# GS_INSTANCE_TIME

**PV_INSTANCE_TIME** records time consumption information of the current node. The time consumption information is classified into the following types:

- DB_TIME: effective time spent by jobs in multi-core scenarios
- CPU_TIME: CPU time spent
- EXECUTION_TIME: time spent within executors
- PARSE_TIME: time spent on parsing SQL statements
- PLAN_TIME: time spent on generating plans
- REWRITE_TIME: time spent on rewriting SQL statements
- PL_EXECUTION_TIME: execution time of the PL/pgSQL stored procedure
- PL_COMPILATION_TIME: compilation time of the PL/pgSQL stored procedure
- NET_SEND_TIME: time spent on the network
- DATA_IO_TIME: I/O time spent

**Table 1** GS_INSTANCE_TIME columns

| **Name**  | **Type** | **Description**   |
| :-------- | :------- | :---------------- |
| stat_id   | integer  | Statistics ID     |
| stat_name | text     | Type name         |
| value     | bigint   | Time value, in μs |
