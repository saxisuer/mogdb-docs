---
title: GS_WLM_CGROUP_INFO
summary: GS_WLM_CGROUP_INFO
author: Guo Huan
date: 2021-06-15
---

# GS_WLM_CGROUP_INFO

**GS_WLM_CGROUP_INFO** displays information about a Cgroup for a job that is being executed.

**Table 1** GS_WLM_CGROUP_INFO columns

| Name         | Type     | Description                                |
| :----------- | :------- | :----------------------------------------- |
| cgoup_name   | text     | Cgroup name                                |
| priority     | interger | Priority of the job                        |
| usage_pecent | interger | Percentage of resources used by the Cgroup |
| shares       | bigint   | CPU quota allocated to the Cgroup          |
| cpuacct      | bigint   | Allocated CPU quota                        |
| cpuset       | text     | Allocated CPU cores                        |
| relpath      | text     | Relative path of the Cgroup                |
| valid        | text     | Whether the Cgroup is valid                |
| node_group   | text     | Name of the logical cluster                |
