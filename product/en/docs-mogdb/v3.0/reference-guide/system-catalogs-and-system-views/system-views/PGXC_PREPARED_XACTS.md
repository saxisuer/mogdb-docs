---
title: PGXC_PREPARED_XACTS
summary: PGXC_PREPARED_XACTS
author: Guo Huan
date: 2022-05-13
---

# PGXC_PREPARED_XACTS

**PGXC_PREPARED_XACTS** displays two-phase transactions in the **prepared** phase. Only users with the **system admin** or **monitor admin** permission can view the information.

**Table 1** PGXC_PREPARED_XACTS columns

| Name               | Type | Description                                                  |
| :----------------- | :--- | :----------------------------------------------------------- |
| pgxc_prepared_xact | text | Displays the two-phase transaction currently in the **prepared** phase. |