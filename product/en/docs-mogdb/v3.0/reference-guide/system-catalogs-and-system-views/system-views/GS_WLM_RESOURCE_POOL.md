---
title: GS_WLM_RESOURCE_POOL
summary: GS_WLM_RESOURCE_POOL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_RESOURCE_POOL

**GS_WLM_RESOURCE_POOL** records statistics on a resource pool.

**Table 1** GS_WLM_RESOURCE_POOL columns

| Name          | Type    | Description                                      |
| :------------ | :------ | :----------------------------------------------- |
| rpoid         | oid     | OID of the resource pool                         |
| respool       | name    | Name of the resource pool                        |
| control_group | name    | Unsupported currently                            |
| parentid      | oid     | OID of the parent resource pool                  |
| ref_count     | integer | Number of jobs associated with the resource pool |
| active_points | integer | Number of used points in the resource pool       |
| running_count | integer | Number of jobs running in the resource pool      |
| waiting_count | integer | Number of jobs queuing in the resource pool      |
| io_limits     | integer | IOPS upper limit of the resource pool            |
| io_priority   | integer | I/O priority of the resource pool                |
