---
title: GS_SESSION_CPU_STATISTICS
summary: GS_SESSION_CPU_STATISTICS
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_CPU_STATISTICS

**GS_SESSION_CPU_STATISTICS** shows load management information about CPU usage of ongoing complex jobs executed by the current user.

**Table 1** GS_SESSION_CPU_STATISTICS columns

| Name           | Type                     | Description                                                  |
| :------------- | :----------------------- | :----------------------------------------------------------- |
| datid          | oid                      | OID of the database that the backend is connected to         |
| usename        | name                     | Name of the user logged in to the backend                    |
| pid            | bigint                   | Thread ID of the backend                                     |
| start_time     | timestamp with time zone | Time when the statement starts to be executed                |
| min_cpu_time   | bigint                   | Minimum CPU time of the statement across the database nodes, in ms |
| max_cpu_time   | bigint                   | Maximum CPU time of the statement across the database nodes, in ms |
| total_cpu_time | bigint                   | Total CPU time of the statement across the database nodes, in ms |
| query          | text                     | Statement being executed                                     |
| node_group     | text                     | Unsupported currently                                        |
| top_cpu_dn     | text                     | CPU usage                                                    |
