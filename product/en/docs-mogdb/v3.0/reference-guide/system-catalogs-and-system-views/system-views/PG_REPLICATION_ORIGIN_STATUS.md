---
title: PG_REPLICATION_ORIGIN_STATUS
summary: PG_REPLICATION_ORIGIN_STATUS
author: Guo Huan
date: 2022-05-12
---

# PG_REPLICATION_ORIGIN_STATUS

**PG_REPLICATION_ORIGIN_STATUS** displays the replication status of the replication source.

**Table 1** PG_REPLICATION_ORIGIN_STATUS columns

| Name        | Type | Description                           |
| :---------- | :--- | :------------------------------------ |
| local_id    | oid  | Replication source ID.                |
| external_id | text | Name of the replication source.       |
| remote_lsn  | LSN  | Remote LSN of the replication source. |
| local_lsn   | LSN  | Local LSN of the replication source.  |