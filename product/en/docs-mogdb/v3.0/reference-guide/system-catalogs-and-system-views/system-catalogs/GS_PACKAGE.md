---
title: GS_PACKAGE
summary: GS_PACKAGE
author: Guo Huan
date: 2022-05-12
---

# GS_PACKAGE

**GS_PACKAGE** records package information.

**Table 1** GS_PACKAGE columns

| Name           | Type    | Description                                                |
| :------------- | :------ | :--------------------------------------------------------- |
| oid            | oid     | Row identifier (hidden attribute, which must be specified) |
| pkgnamespace   | oid     | Schema to which a package belongs                          |
| pkgowner       | oid     | Owner of a package                                         |
| pkgname        | name    | Name of a package                                          |
| pkgspecsrc     | text    | Package specification                                      |
| pkgbodydeclsrc | text    | Package body                                               |
| pkgbodyinitsrc | text    | Package initialization source                              |
| pkgacl         | aclitem | Access permission                                          |
| pkgsecdef      | boolean | Whether a user has the definer permission on the package.  |