---
title:  PG_AMPROC
summary:  PG_AMPROC
author: Guo Huan
date: 2021-04-19
---

# PG_AMPROC

**PG_AMPROC** records information about the support procedures associated with the access method operator families. There is one row for each support procedure that belongs to an operator family.

**Table 1** PG_AMPROC columns

| Name            | Type     | Reference       | Description                                                  |
| :-------------- | :------- | :-------------- | :----------------------------------------------------------- |
| oid             | oid      | -               | Row identifier (hidden attribute, which must be specified)   |
| amprocfamily    | oid      | PG_OPFAMILY.oid | Operator family of this entry                                |
| amproclefttype  | oid      | PG_TYPE.oid     | Left-hand input data type of the associated operator. For details about the possible values and their description, see **pg_type.h**. |
| amprocrighttype | oid      | PG_TYPE.oid     | Right-hand input data type of the associated operator. For details about the possible values and their description, see **pg_type.h**. |
| amprocnum       | smallint | -               | Support procedure number                                     |
| amproc          | regproc  | PG_PROC.proname | OID of the procedure                                         |

The usual interpretation of the **amproclefttype** and **amprocrighttype** columns is that they identify the left and right input types of the operator(s) that a particular support procedure supports. For some access methods, these match the input data type(s) of the support procedure itself; for others not. There is a notion of "default" support procedures for an index, which are those with **amproclefttype** and **amprocrighttype** both equal to the index opclass's **opcintype**.
