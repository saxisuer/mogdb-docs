---
title: PG_AUTH_HISTORY
summary: PG_AUTH_HISTORY
author: Guo Huan
date: 2021-04-19
---

# PG_AUTH_HISTORY

**PG_AUTH_HISTORY** records the authentication history of a role. This system catalog is accessible only to system administrators.

**Table 1** PG_AUTH_HISTORY columns

| Name         | Type                     | Description                                                  |
| :----------- | :----------------------- | :----------------------------------------------------------- |
| oid          | oid                      | Row identifier (hidden attribute, which must be specified)   |
| roloid       | oid                      | ID of a role                                                 |
| passwordtime | timestamp with time zone | Time of password creation and change                         |
| rolpassword  | text                     | Ciphertext of the role password. The encryption mode is determined by the GUC parameter **password_encryption_type**. |
