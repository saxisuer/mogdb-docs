---
title: PG_RLSPOLICY
summary: PG_RLSPOLICY
author: Guo Huan
date: 2021-04-19
---

# PG_RLSPOLICY

**PG_RLSPOLICY** records row-level access control policies.

**Table 1** PG_RLSPOLICY columns

| Name          | Type         | Description                                                  |
| :------------ | :----------- | :----------------------------------------------------------- |
| oid           | oid          | Row identifier (hidden attribute, which must be specified)   |
| polname       | name         | Name of an access control policy                             |
| polrelid      | oid          | OID of the table object on which the row-level access control policy takes effect |
| polcmd        | "char"       | SQL operations affected by the row-level access control policy |
| polpermissive | boolean      | Attribute of the row-level access control policy. **t** indicates an expression that uses the OR condition, and **f** indicates an expression that uses the AND condition. |
| polroles      | oid[]        | OID list of users affected by the row-level access control policy. If this parameter is not specified, all users are affected. |
| polqual       | pg_node_tree | Expression of the row-level access control policy            |
