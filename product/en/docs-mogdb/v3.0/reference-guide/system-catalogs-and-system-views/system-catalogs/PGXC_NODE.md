---
title: PGXC_NODE
summary: PGXC_NODE
author: Guo Huan
date: 2022-05-13
---

# PGXC_NODE

**PGXC_NODE** records information about cluster nodes. The **PGXC_NODE** system catalog has specific meanings only in distributed scenarios. MogDB can only query the table definition.

**Table 1** PGXC_NODE columns

| Name             | Type    | Description                                                  |
| :--------------- | :------ | :----------------------------------------------------------- |
| oid              | oid     | Row identifier (hidden attribute, which must be specified)   |
| node_name        | name    | Node name                                                    |
| node_type        | “char”  | Node type<br/>- **C**: coordinator node (CN)<br/>- **D**: data node (DN)<br/>- **S**: standby node |
| node_port        | integer | Port number of the node                                      |
| node_host        | name    | Host name or IP address of a node. (If a virtual IP address is configured, its value is a virtual IP address.) |
| node_port1       | integer | Port number of a replication node                            |
| node_host1       | name    | Host name or IP address of a replication node. (If a virtual IP address is configured, its value is a virtual IP address.) |
| hostis_primary   | Boolean | Whether a primary/standby switchover occurs on the current node<br/>- **t** (true): yes<br/>- **f** (false): no |
| nodeis_primary   | Boolean | Whether the current node is preferred to execute non-query operations in the **replication** table<br/>- **t** (true): yes<br/>- **f** (false): no |
| nodeis_preferred | Boolean | Whether the current node is preferred to execute queries in the **replication** table<br/>- **t** (true): yes<br/>- **f** (false): no |
| node_id          | integer | Node identifier. The value is obtain by calculating the value of **node_name** using the hash function. |
| sctp_port        | integer | Port used by the TCP proxy communication library or SCTP communication library of the primary node to listen to the data channel |
| control_port     | integer | Port used by the TCP proxy communication library or SCTP communication library of the primary node to listen to the control channel |
| sctp_port1       | integer | Port used by the TCP proxy communication library or SCTP communication library of the standby node to listen to the data channel |
| control_port1    | integer | Port used by the TCP proxy communication library or SCTP communication library of the standby node to listen to the control channel |
| nodeis_central   | Boolean | Whether the current node is a CN. It is used only for CNs and is invalid for DNs.<br/>- **t** (true): yes<br/>- **f** (false): no |
| nodeis_active    | Boolean | Whether the current node is normal. It is used to mark whether the CN is removed and is invalid for DNs.<br/>- **t** (true): yes<br/>- **f** (false): no |
