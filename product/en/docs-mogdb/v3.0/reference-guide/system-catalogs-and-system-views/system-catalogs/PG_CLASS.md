---
title: PG_CLASS
summary: PG_CLASS
author: Guo Huan
date: 2021-04-19
---

# PG_CLASS

**PG_CLASS** records database objects and their relationship.

**Table 1** PG_CLASS columns

| Name             | Type             | Description                                                  |
| :--------------- | :--------------- | :----------------------------------------------------------- |
| oid              | oid              | Row identifier (hidden attribute, which must be specified)   |
| relname          | name             | Name of an object, such as a table, index, or view           |
| relnamespace     | oid              | OID of the namespace that contains the relationship          |
| reltype          | oid              | Data type that corresponds to the table's row type. The index is 0 because the index does not have **PG_TYPE** records. |
| reloftype        | oid              | OID of the composite type (**0** for other types)            |
| relowner         | oid              | Owner of the relationship                                    |
| relam            | oid              | Access method used, such as B-tree and hash, if this is an index |
| relfilenode      | oid              | Name of the on-disk file of this relationship (**0** if such file does not exist) |
| reltablespace    | oid              | Tablespace in which this relationship is stored. If the value is **0**, the default tablespace in this database is used. This column is meaningless if the relationship has no on-disk file. |
| relpages         | double precision | Size of the on-disk representation of the table in pages (of size BLCKSZ). This is only an estimate used by the optimizer. |
| reltuples        | double precision | Number of rows in the table. This is only an estimate used by the optimizer. |
| relallvisible    | integer          | Number of pages marked as all visible in the table. This column is used by the optimizer for optimizing SQL execution. It is updated by **VACUUM**, **ANALYZE**, and a few DDL statements such as **CREATE INDEX**. |
| reltoastrelid    | oid              | OID of the TOAST table associated with the table (**0** if no TOAST table exists).<br/>The TOAST table stores large columns "offline" in a secondary table. |
| reltoastidxid    | oid              | OID of the index for a TOAST table (**0** for a table other than a TOAST table) |
| reldeltarelid    | oid              | OID of a Delta table<br/>Delta tables are attached to column-store tables. They store long tail data generated during storage data import. |
| reldeltaidx      | oid              | OID of the index for a Delta table                           |
| relcudescrelid   | oid              | OID of a CU description table.<br/>CU description tables (Desc tables) belong to column-store tables. They control whether storage data in the HDFS table directory is visible. |
| relcudescidx     | oid              | OID of the index for a CU description table                  |
| relhasindex      | boolean          | Its value is **true** if this column is a table and has (or recently had) at least one index.<br/>It is set by **CREATE INDEX** but is not immediately cleared by **DROP INDEX**. If the **VACUUM** process detects that a table has no index, it clears the **relhasindex** column and sets the value to **false**. |
| relisshared      | boolean          | Its value is **true** if the table is shared across all database nodes in MogDB. Only certain system catalogs (such as **PG_DATABASE**) are shared. |
| relpersistence   | "char"           | - **p**: permanent table<br/>- **u**: non-log table<br/>- **g**: temporary table |
| relkind          | "char"           | - **r**: ordinary table<br/>- **i**: index<br/>- **I**: GLOBAL index of a partitioned table<br/>- **s**: sequence<br/>- **L**: large sequence<br/>- **v**: view<br/>- **c**: composite type<br/>- **t**: TOAST table<br/>- **f**: foreign table<br/>- **m**: materialized view |
| relnatts         | smallint         | Number of user columns in the relationship (excluding system columns). **PG_ATTRIBUTE** has the same number of rows as the user columns. |
| relchecks        | smallint         | Number of check constraints in the table. For details, see the system catalog **PG_CONSTRAINT**. |
| relhasoids       | boolean          | Its value is **true** if an OID is generated for each row of the relationship. |
| relhaspkey       | boolean          | Its value is **true** if the table has (or once had) a primary key. |
| relhasrules      | boolean          | Its value is **true** if the table has rules. For details, see the system catalog **PG_REWRITE**. |
| relhastriggers   | boolean          | The value is **true** if the table has (or once had) triggers. Triggers of the table and view are recorded in the system catalog **PG_TRIGGER**. |
| relhassubclass   | boolean          | Its value is **true** if the table has (or once had) any inheritance child table. |
| relcmprs         | tinyint          | Whether the compression feature is enabled for the table. Note that only batch insertion triggers compression, so ordinary CRUD does not trigger compression.<br/>- **0**: Tables that do not support compression (primarily system catalogs, on which the compression attribute cannot be modified).<br/>- **1**: The compression feature of the table data is NOCOMPRESS or has no specified keyword.<br/>- **2**: The compression feature of the table data is COMPRESS. |
| relhasclusterkey | boolean          | Whether the local cluster storage is used                    |
| relrowmovement   | boolean          | Whether row migration is allowed when the partitioned table is updated.<br/>- **true**: Row migration is allowed.<br/>- **false**: Row migration is not allowed. |
| parttype         | "char"           | Whether the table or index has the property of a partitioned table.<br/>- **p**: The table or index has the property of a partitioned table.<br/>- **n**: The table or index does not have the property of a partitioned table.<br/>- **v**: The table is a value partitioned table in HDFS.<br/>- **s**: The table is a level-2 partitioned table. |
| relfrozenxid     | xid32            | All transaction IDs before this one have been replaced with a permanent ("frozen") transaction ID in the table. This column is used to track whether the table needs to be vacuumed to prevent transaction ID wraparound (or to allow **PG_CLOG** to be shrunk). The value is **0** (**InvalidTransactionId**) if the relationship is not a table.<br/>To ensure forward compatibility, this column is reserved. The **relfrozenxid64** column is added to record the information. |
| relacl           | aclitem[]        | Access permissions.<br/>The command output of the query is as follows:<br/>`rolename=xxxx/yyyy  - Assigning permissions to a role`<br/>`=xxxx/yyyy - Assigning the permission to public`<br/>**xxxx** indicates assigned permissions, and **yyyy** indicates roles with the assigned permissions. For details on permission descriptions, see [Table 2](#Description of permissions). |
| relreplident     | "char"           | Identifier of a decoding column in logical decoding:<br/>- **d**: default (primary key, if any)<br/>- **n**: none<br/>- **f**: all columns<br/>- **i**: The indisreplident of the index is specified or the default index is used. |
| relfrozenxid64   | xid              | All transaction IDs before this one have been replaced with a permanent ("frozen") transaction ID in the table. This column is used to track whether the table needs to be vacuumed to prevent transaction ID wraparound (or to allow **PG_CLOG** to be shrunk). The value is **0** (**InvalidTransactionId**) if the relationship is not a table. |
| relbucket        | oid              | Bucket information in **pg_hashbucket**.                     |
| relbucketkey     | int2vector       | Column number of a hash partition.                           |
| relminmxid       | xid              | All multi-transaction IDs before this one have been replaced with a transaction ID in the table. This is used to track whether the table needs to be vacuumed in order to prevent multi-transaction IDs wraparound (or to allow **pg_clog** to be shrunk). The value is **0** (**InvalidTransactionId**) if the relationship is not a table. |

**Table 2** Description of permissions<a id="Description of permissions"> </a>

| Parameter | Parameter Description                           |
| :-------- | :---------------------------------------------- |
| r         | SELECT (read)                                   |
| w         | UPDATE (write)                                  |
| a         | INSERT (insert)                                 |
| d         | DELETE                                          |
| D         | TRUNCATE                                        |
| x         | REFERENCES                                      |
| t         | TRIGGER                                         |
| X         | EXECUTE                                         |
| U         | USAGE                                           |
| C         | CREATE                                          |
| c         | CONNECT                                         |
| T         | TEMPORARY                                       |
| A         | ALTER                                           |
| P         | DROP                                            |
| m         | COMMENT                                         |
| i         | INDEX                                           |
| v         | VACUUM                                          |
| *         | Authorization options for preceding permissions |
