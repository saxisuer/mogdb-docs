---
title: PG_OPERATOR
summary: PG_OPERATOR
author: Guo Huan
date: 2021-04-19
---

# PG_OPERATOR

**PG_OPERATOR** records information about operators.

**Table 1** PG_OPERATOR columns

| Name         | Type    | Reference        | Description                                                  |
| :----------- | :------ | :--------------- | :----------------------------------------------------------- |
| oid          | oid     | -                | Row identifier (hidden attribute, which must be specified)   |
| oprname      | name    | -                | Name of an operator                                          |
| oprnamespace | oid     | PG_NAMESPACE.oid | OID of the namespace that contains the operator              |
| oprowner     | oid     | PG_AUTHID.oid    | Owner of the operator                                        |
| oprkind      | "char"  | -                | - **b**: infix ("both")<br/>- **l**: prefix ("left")<br/>- **r**: postfix ("right") |
| oprcanmerge  | boolean | -                | Whether the operator supports merge joins<br />- **t (true)**: yes<br />- **f (false)**: no |
| oprcanhash   | boolean | -                | Whether the operator supports hash joins<br />- **t (true)**: yes<br />- **f (false)**: no |
| oprleft      | oid     | PG_TYPE.oid      | Type of the left operand                                     |
| oprright     | oid     | PG_TYPE.oid      | Type of the right operand                                    |
| oprresult    | oid     | PG_TYPE.oid      | Type of the result                                           |
| oprcom       | oid     | PG_OPERATOR.oid  | Commutator of this operator, if any                          |
| oprnegate    | oid     | PG_OPERATOR.oid  | Negator of this operator, if any                             |
| oprcode      | regproc | PG_PROC.proname  | Function that implements the operator                        |
| oprrest      | regproc | PG_PROC.proname  | Restriction selectivity estimation function for the operator |
| oprjoin      | regproc | PG_PROC.proname  | Join selectivity estimation function for the operator        |
