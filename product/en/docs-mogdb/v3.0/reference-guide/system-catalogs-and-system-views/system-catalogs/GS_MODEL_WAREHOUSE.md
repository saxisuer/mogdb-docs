---
title: GS_MODEL_WAREHOUSE
summary: GS_MODEL_WAREHOUSE
author: Guo Huan
date: 2022-05-12
---

# GS_MODEL_WAREHOUSE

**GS_MODEL_WAREHOUSE** stores AI engine training models, including the models and detailed description of the training process.

**Table 1** GS_MODEL_WAREHOUSE columns

| Name                  | Data Type | Description                                                |
| :-------------------- | :-------- | :--------------------------------------------------------- |
| oid                   | oid       | Hidden column                                              |
| modelname             | name      | Unique constraint                                          |
| modelowner            | oid       | OID of a model owner                                       |
| createtime            | timestamp | Time when a model is created                               |
| processedtuples       | int       | Number of tuples involved in training                      |
| discardedtuples       | int       | Number of unqualified tuples not involved in training      |
| preprocesstime        | real      | Data preprocessing time                                    |
| exectime              | real      | Training duration                                          |
| iterations            | int       | Iteration round                                            |
| outputtype            | oid       | OID of the output data type                                |
| modeltype             | text      | AI operator type                                           |
| query                 | text      | Query statement executed to create a model                 |
| modeldata             | bytea     | Stored binary model information                            |
| weight                | real[]    | Currently, this column applies only to GD operator models. |
| hyperparametersnames  | text[]    | Involved hyperparameter name                               |
| hyperparametersvalues | text[]    | Hyperparameter value                                       |
| hyperparametersoids   | oid[]     | OID of the data type corresponding to a hyperparameter     |
| coefnames             | text[]    | Model parameter                                            |
| coefvalues            | text[]    | Value of a model parameter                                 |
| coefoids              | oid[]     | OID of the data type corresponding to a model parameter    |
| trainingscoresname    | text[]    | Method used to measure model performance                   |
| trainingscoresvalue   | real[]    | Value used to measure model performance                    |
| modeldescribe         | text[]    | Model description                                          |