---
title: PG_SECLABEL
summary: PG_SECLABEL
author: Guo Huan
date: 2021-04-19
---

# PG_SECLABEL

**PG_SECLABEL** records security labels on database objects.

See also PG_SHSECLABEL, which provides a similar function for security labels of database objects that are shared across MogDB.

**Table 1** PG_SECLABEL columns

| Name     | Type    | Reference      | Description                                            |
| :------- | :------ | :------------- | :----------------------------------------------------- |
| objoid   | oid     | Any OID column | OID of the object that this security label pertains to |
| classoid | oid     | PG_CLASS.oid   | OID of the system catalog where the object appears     |
| objsubid | integer | -              | Column number for a security label on a table column   |
| provider | text    | -              | Label provider associated with the label               |
| label    | text    | -              | Security label applied to the object                   |
