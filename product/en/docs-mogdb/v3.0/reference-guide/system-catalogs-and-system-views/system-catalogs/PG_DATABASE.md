---
title: PG_DATABASE
summary: PG_DATABASE
author: Guo Huan
date: 2021-04-19
---

# PG_DATABASE

**PG_DATABASE** records information about available databases.

**Table 1** PG_DATABASE columns

| Name             | Type      | Description                                                  |
| :--------------- | :-------- | :----------------------------------------------------------- |
| oid              | oid       | Row identifier (hidden attribute, which must be specified)   |
| datname          | name      | Database name                                                |
| datdba           | oid       | Owner of the database, usually the user who created it       |
| encoding         | integer   | Character encoding for the database                          |
| datcollate       | name      | Sequence used by the database                                |
| datctype         | name      | Character type used by the database                          |
| datistemplate    | boolean   | Whether the database can be used as a template database      |
| datallowconn     | boolean   | If the value is **false**, no one can connect to this database. This column is used to protect the **template0** database from being altered. |
| datconnlimit     | integer   | Maximum number of concurrent connections allowed on this database. The value **-1** indicates no limit. |
| datlastsysoid    | oid       | Last system OID in the database                              |
| datfrozenxid     | xid32     | Tracks whether the database needs to be vacuumed to prevent transaction ID wraparound.<br/>To ensure forward compatibility, this column is reserved. The **datfrozenxid64** column is added to record the information. |
| dattablespace    | oid       | Default tablespace of the database                           |
| datcompatibility | name      | Database compatibility mode. Currently, four compatibility modes are supported: A, B, C, and PG, indicating that the Oracle, MySQL, Teradata, and Postgres databases are compatible. |
| datacl           | aclitem[] | Access permissions                                           |
| datfrozenxid64   | xid       | Tracks whether the database needs to be vacuumed to prevent transaction ID wraparound. |
| datminmxid       | xid       | All multi-transaction IDs before this one have been replaced with a transaction ID in the database. This is used to track whether the database needs to be vacuumed in order to prevent transaction IDs wraparound (or to allow **pg_clog** to be shrunk). It is the minimum **pg_class.relminmxid** value of all tables in the database. |
