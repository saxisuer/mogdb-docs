---
title: PG_USER_MAPPING
summary: PG_USER_MAPPING
author: Guo Huan
date: 2021-04-19
---

# PG_USER_MAPPING

**PG_USER_MAPPING** records mappings from local users to remote.

This system catalog is accessible only to system administrators. Common users can query the PG_USER_MAPPINGS view.

**Table 1** PG_USER_MAPPING columns

| Name      | Type   | Reference             | Description                                                  |
| :-------- | :----- | :-------------------- | :----------------------------------------------------------- |
| oid       | oid    | -                     | Row identifier (hidden attribute, which must be specified)   |
| umuser    | oid    | PG_AUTHID.oid         | OID of the local role being mapped (**0** if the user mapping is public) |
| umserver  | oid    | PG_FOREIGN_SERVER.oid | OID of the foreign server that contains the mapping          |
| umoptions | text[] | -                     | User mapping specific options, expressed in a string in the format of keyword=value |
