---
title: PG_REPLICATION_ORIGIN
summary: PG_REPLICATION_ORIGIN
author: Guo Huan
date: 2022-05-12
---

# PG_REPLICATION_ORIGIN

**PG_REPLICATION_ORIGIN** contains all created replication sources and is a globally shared table. That is, each node has only one copy, not one copy per database.

**Table 1** PG_REPLICATION_ORIGIN columns

| Name    | Type | Description                                            |
| :------ | :--- | :----------------------------------------------------- |
| roident | oid  | Unique replication source identifier within a cluster. |
| roname  | text | External user-defined replication source name.         |