---
title: PG_PUBLICATION_REL
summary: PG_PUBLICATION_REL
author: Guo Huan
date: 2022-05-12
---

# PG_PUBLICATION_REL

**PG_PUBLICATION_REL** contains mappings between tables and publications in the current database. This is a many-to-many mapping.

**Table 1** PG_PUBLICATION_REL columns

| Name    | Type | Reference | Description                 |
| :------ | :--- | :-------- | :-------------------------- |
| prpubid | oid  | -         | Reference to a publication. |
| prrelid | oid  | -         | Reference to a table.       |