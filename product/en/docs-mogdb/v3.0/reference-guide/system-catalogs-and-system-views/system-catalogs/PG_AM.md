---
title: PG_AM
summary: PG_AM
author: Guo Huan
date: 2021-04-19
---

# PG_AM

**PG_AM** records information about index access methods. There is one row for each index access method supported by the system.

**Table 1** PG_AM columns

| Name            | Type     | Reference       | Description                                                  |
| :-------------- | :------- | :-------------- | :----------------------------------------------------------- |
| oid             | oid      | -               | Row identifier (hidden attribute, which must be specified)   |
| amname          | name     | -               | Name of the access method                                    |
| amstrategies    | smallint | -               | Number of operator strategies for the access method (**0** if the access method does not have a fixed set of operator strategies) |
| amsupport       | smallint | -               | Number of support routines for the access method             |
| amcanorder      | boolean  | -               | Whether the access method supports ordered scans sorted by the indexed column's value |
| amcanorderbyop  | boolean  | -               | Whether the access method supports ordered scans sorted by the result of an operator on the indexed column |
| amcanbackward   | boolean  | -               | Whether the access method supports backward scanning         |
| amcanunique     | boolean  | -               | Whether the access method supports unique indexes            |
| amcanmulticol   | boolean  | -               | Whether the access method supports multi-column indexes      |
| amoptionalkey   | boolean  | -               | Whether the access method supports scanning without any constraint for the first index column |
| amsearcharray   | boolean  | -               | Whether the access method supports **ScalarArrayOpExpr** searches |
| amsearchnulls   | boolean  | -               | Whether the access method supports **IS NULL/NOT NULL** searches |
| amstorage       | boolean  | -               | Whether the index storage data type can differ from the column data type |
| amclusterable   | boolean  | -               | Whether an index of this type can be clustered on            |
| ampredlocks     | boolean  | -               | Whether an index of this type manages fine-grained predicate locks |
| amkeytype       | oid      | PG_TYPE.oid     | Type of data stored in index (**0** if it is not a fixed type) |
| aminsert        | regproc  | PG_PROC.proname | "Insert this tuple" function                                 |
| ambeginscan     | regproc  | PG_PROC.proname | "Prepare for index scan" function                            |
| amgettuple      | regproc  | PG_PROC.proname | "Next valid tuple" function (**0** if none)                  |
| amgetbitmap     | regproc  | PG_PROC.proname | "Fetch all valid tuples" function (**0** if none)            |
| amrescan        | regproc  | PG_PROC.proname | "(Re)start index scan" function                              |
| amendscan       | regproc  | PG_PROC.proname | "Clean up after index scan" function                         |
| ammarkpos       | regproc  | PG_PROC.proname | "Mark current scan position" function                        |
| amrestrpos      | regproc  | PG_PROC.proname | "Restore marked scan position" function                      |
| ammerge         | regproc  | PG_PROC.proname | "Merge multiple indexes" function                            |
| ambuild         | regproc  | PG_PROC.proname | "Build new index" function                                   |
| ambuildempty    | regproc  | PG_PROC.proname | "Build empty index" function                                 |
| ambulkdelete    | regproc  | PG_PROC.proname | Bulk-delete function                                         |
| amvacuumcleanup | regproc  | PG_PROC.proname | Post-**VACUUM** cleanup function                             |
| amcanreturn     | regproc  | PG_PROC.proname | Function to check whether the index supports index-only scans (**0** if none) |
| amcostestimate  | regproc  | PG_PROC.proname | Function to estimate cost of an index scan                   |
| amoptions       | regproc  | PG_PROC.proname | Function to parse and validate **reloptions** for an index   |
