---
title: PGXC_GROUP
summary: PGXC_GROUP
author: Guo Huan
date: 2022-05-13
---

# PGXC_GROUP

**PGXC_GROUP** records information about node groups. The **PGXC_GROUP** system catalog has specific meanings only in distributed scenarios. MogDB can only query the table definition.

**Table 1** PGXC_GROUP columns

| Name              | Type             | Description                                                  |
| :---------------- | :--------------- | :----------------------------------------------------------- |
| oid               | oid              | Row identifier (hidden attribute, which must be specified)   |
| group_name        | name             | Node group name                                              |
| in_redistribution | “char”           | Whether redistribution is required. The value must be one of the following:<br/>- **n**: The node group is not redistributed.<br/>- **y**: The source node group is in redistribution.<br/>- **t**: The destination node group is in redistribution. |
| group_members     | oidvector_extend | Node OID list of the node group                              |
| group_buckets     | text             | Distributed data bucket group                                |
| is_installation   | Boolean          | Whether to install a sub-cluster<br/>- **t** (true): yes<br/>- **f** (false): no |
| group_acl         | aclitem[]        | Access permission                                            |
| group_kind        | “char”           | Node group type. The value must be one of the following:<br/>- **i**: installation node group<br/>- **n**: node group in a common non-logical cluster<br/>- **v**: node group in a logical cluster<br/>- **e**: elastic cluster |
| group_parent      | oid              | For a child node group, this field indicates the OID of the parent node group. For a parent node group, this field is left blank. |
