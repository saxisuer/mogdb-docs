---
title: PLAN_TABLE_DATA
summary: PLAN_TABLE_DATA
author: Guo Huan
date: 2021-04-19
---

# PLAN_TABLE_DATA

**PLAN_TABLE_DATA** stores plan information collected by **EXPLAIN PLAN**. Different from the **PLAN_TABLE** view, the system catalog **PLAN_TABLE_DATA** stores **EXPLAIN PLAN** information collected by all sessions and users.

**Table 1** PLAN_TABLE_DATA columns

| Name         | Type           | Description                                                  |
| :----------- | :------------- | :----------------------------------------------------------- |
| session_id   | text           | Session that inserts the data. Its value consists of a service thread start timestamp and a service thread ID. Values are constrained by **NOT NULL**. |
| user_id      | oid            | User who inserts the data. Values are constrained by **NOT NULL**. |
| statement_id | varchar2(30)   | Query tag specified by a user                                |
| plan_id      | bigint         | Query ID The ID is automatically generated in the plan generation phase and is used by kernel engineers for debugging. |
| id           | int            | Node ID in a plan                                            |
| operation    | varchar2(30)   | Operation description                                        |
| options      | varchar2(255)  | Operation action                                             |
| object_name  | name           | Name of an operated object. It is defined by users.          |
| object_type  | varchar2(30)   | Object type                                                  |
| object_owner | name           | Schema to which the object belongs. It is defined by users.  |
| projection   | varchar2(4000) | Returned column information                                  |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - **PLAN_TABLE_DATA** records data of all users and sessions on the current node. Only administrators can access all the data. Common users can view their own data in the PLAN_TABLE view.
> - Data is automatically inserted into **PLAN_TABLE_DATA** after **EXPLAIN PLAN** is executed. Therefore, do not manually insert data into or update data in **PLAN_TABLE_DATA**. Otherwise, data in **PLAN_TABLE_DATA** may be disordered. To delete data from **PLAN_TABLE_DATA**, you are advised to use the PLAN_TABLE view.
> - Information in the **statement_id**, **object_name**, **object_owner**, and **projection** columns is stored in letter cases specified by users and information in other columns is stored in uppercase.
