---
title: GS_GLOBAL_CONFIG
summary: GS_GLOBAL_CONFIG
author: Guo Huan
date: 2022-05-12
---

# GS_GLOBAL_CONFIG

**GS_GLOBAL_CONFIG** records the parameter values specified by users during database instance initialization. In addition, it also stores weak passwords set by users. Initial database users can write, modify, and delete parameters in system catalogs using ALTER and DROP.

**Table 1** GS_GLOBAL_CONFIG columns

| Name  | Type | Description                                                  |
| :---- | :--- | :----------------------------------------------------------- |
| name  | name | Specifies the preset parameter name, weak password name, or parameter required by users during database instance initialization. |
| value | text | Specifies the preset parameter value, weak password value, or parameter value required by users during database instance initialization. |