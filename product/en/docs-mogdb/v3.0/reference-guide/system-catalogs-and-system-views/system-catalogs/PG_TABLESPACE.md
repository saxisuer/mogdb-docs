---
title: PG_TABLESPACE
summary: PG_TABLESPACE
author: Guo Huan
date: 2021-04-19
---

# PG_TABLESPACE

**PG_TABLESPACE** records tablespace information.

**Table 1** PG_TABLESPACE columns

| Name       | Type      | Description                                                  |
| :--------- | :-------- | :----------------------------------------------------------- |
| oid        | oid       | Row identifier (hidden attribute, which must be specified)   |
| spcname    | name      | Tablespace name                                              |
| spcowner   | oid       | Owner of the tablespace, usually the user who created it     |
| spcacl     | aclitem[] | Access permissions. For details, see [GRANT](../../../reference-guide/sql-syntax/GRANT.md) and [REVOKE](../../../reference-guide/sql-syntax/REVOKE.md). |
| spcoptions | text[]    | Options of the tablespace                                    |
| spcmaxsize | text      | Maximum size of the available disk space, in bytes           |
| relative   | boolean   | Whether the storage path specified by the tablespace is a relative path |
