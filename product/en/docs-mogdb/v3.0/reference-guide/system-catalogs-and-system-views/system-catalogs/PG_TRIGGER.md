---
title: PG_TRIGGER
summary: PG_TRIGGER
author: Guo Huan
date: 2021-04-19
---

# PG_TRIGGER

**PG_TRIGGER** records trigger information.

**Table 1** PG_TRIGGER columns

| Name           | Type         | Description                                                  |
| :------------- | :----------- | :----------------------------------------------------------- |
| oid            | oid          | Row identifier (hidden attribute, which must be specified)   |
| tgrelid        | oid          | OID of the table where the trigger is located                |
| tgname         | name         | Trigger name                                                 |
| tgfoid         | oid          | The function to be called by the trigger.                    |
| tgtype         | smallint     | Trigger type                                                 |
| tgenabled      | "char"       | - **O**: The trigger is triggered in origin or local mode.<br/>- **D**: The trigger is disabled.<br/>- **R**: The trigger is triggered in replica mode.<br/>- **A**: The trigger is always triggered. |
| tgisinternal   | boolean      | Whether the trigger is an internal trigger                   |
| tgconstrrelid  | oid          | Table referenced by the integrity constraint                 |
| tgconstrindid  | oid          | Index of the integrity constraint                            |
| tgconstraint   | oid          | OID of the constraint trigger in **PG_CONSTRAINT**           |
| tgdeferrable   | boolean      | Whether the constraint trigger is of the DEFERRABLE type     |
| tginitdeferred | boolean      | Whether the trigger is of the INITIALLY DEFERRED type        |
| tgnargs        | smallint     | Number of input parameters of the trigger function           |
| tgattr         | int2vector   | Column ID specified by the trigger. If no column is specified, an empty array is used. |
| tgargs         | bytea        | Parameter transferred to the trigger                         |
| tgqual         | pg_node_tree | WHEN condition of the trigger (**NULL** if the WHEN condition does not exist) |
| tgowner        | oid          | trigger owner                                                |
