---
title: GS_DB_PRIVILEGE
summary: GS_DB_PRIVILEGE
author: Guo Huan
date: 2022-05-12
---

# GS_DB_PRIVILEGE

**GS_DB_PRIVILEGE** records the granting of ANY permissions. Each record corresponds to a piece of authorization information.

**Table 1** GS_DB_PRIVILEGE columns

| Name           | Type    | Description                                                  |
| :------------- | :------ | :----------------------------------------------------------- |
| oid            | oid     | Row identifier (hidden attribute, which must be specified).  |
| roleid         | oid     | User ID.                                                     |
| privilege_type | text    | ANY permission of a user. For details about the value, see Table 1 in [GRANT](../../../reference-guide/sql-syntax/GRANT.md). |
| admin_option   | boolean | Whether the ANY permission recorded in the privilege_type column can be re-granted.<br/>- **t**: yes.<br/>- **f**: no. |