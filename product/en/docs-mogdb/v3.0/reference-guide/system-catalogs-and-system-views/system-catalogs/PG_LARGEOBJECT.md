---
title: PG_LARGEOBJECT
summary: PG_LARGEOBJECT
author: Guo Huan
date: 2021-04-19
---

# PG_LARGEOBJECT

**PG_LARGEOBJECT** records data making up large objects. A large object is identified by an OID assigned when it is created. Each large object is broken into segments or "pages" small enough to be conveniently stored as rows in **PG_LARGEOBJECT**. The amount of data per page is defined as **LOBLKSIZE**.

This system catalog is accessible only to system administrators.

**Table 1** PG_LARGEOBJECT columns

| Name   | Type    | Reference                   | Description                                                  |
| :----- | :------ | :-------------------------- | :----------------------------------------------------------- |
| loid   | oid     | PG_LARGEOBJECT_METADATA.oid | Identifier of the large object that includes this page       |
| pageno | integer | -                           | Page number of this page within its large object (counting from zero) |
| data   | bytea   | -                           | Data stored in the large object. This will never be more than **LOBLKSIZE** bytes and might be less. |

Each row of **PG_LARGEOBJECT** holds data for one page of a large object, beginning at byte offset (**pageno \* LOBLKSIZE**) within the object. The implementation allows sparse storage: pages might be missing, and might be shorter than **LOBLKSIZE** bytes even if they are not the last page of the object. Missing regions within a large object read as zeroes.
