---
title: PG_CONVERSION
summary: PG_CONVERSION
author: Guo Huan
date: 2021-04-19
---

# PG_CONVERSION

**PG_CONVERSION** describes encoding conversion information.

**Table 1** PG_CONVERSION columns

| Name           | Type    | Reference        | Description                                                |
| :------------- | :------ | :--------------- | :--------------------------------------------------------- |
| oid            | oid     | -                | Row identifier (hidden attribute, which must be specified) |
| conname        | name    | -                | Conversion name (unique within a namespace)                |
| connamespace   | oid     | PG_NAMESPACE.oid | OID of the namespace that contains this conversion         |
| conowner       | oid     | PG_AUTHID.oid    | Owner of the conversion                                    |
| conforencoding | integer | -                | Source encoding ID                                         |
| contoencoding  | integer | -                | Destination encoding ID                                    |
| conproc        | regproc | PG_PROC.proname  | Conversion procedure                                       |
| condefault     | boolean | -                | Whether the default conversion is used                     |
