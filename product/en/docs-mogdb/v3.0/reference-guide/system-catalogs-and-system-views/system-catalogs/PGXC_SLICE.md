---
title: PGXC_SLICE
summary: PGXC_SLICE
author: Guo Huan
date: 2022-05-13
---

# PGXC_SLICE

**PGXC_SLICE** is a system catalog created for recording range distribution and list distribution details. Currently, range interval cannot be used to automatically scale out shards. It is reserved in the system catalog.

The **PGXC_SLICE** system catalog has specific meanings only in distributed scenarios. MogDB can only query the table definition.

**Table 1** PGXC_SLICE columns

| Name            | Type    | Description                                                  |
| :-------------- | :------ | :----------------------------------------------------------- |
| relname         | name    | Table name or shard name, which is distinguished by **type**. |
| type            | char    | When the value is **t**, **relname** indicates the table name. When the value is **s**, **relname** indicates the shard name. |
| strategy        | char    | **- r**: range distribution table<br/>- **l**: list distribution table<br/>This value will be extended for subsequent interval shards. |
| relid           | oid     | OID of the distribution table to which the tuple belongs.    |
| referenceoid    | oid     | OID of the referenced distribution table, which is used for slice reference table creation syntax. |
| sindex          | int     | Position of the current boundary in a shard when the table is a list distribution table. |
| interval        | text[]  | Reserved column                                              |
| transitboundary | text[]  | Reserved column                                              |
| transitno       | int     | Reserved column                                              |
| nodeoid         | oid     | When **relname** is set to a shard name, **nodeoid** indicates the OID of the DN where the shard data is stored. |
| boundaries      | text[]  | When **relname** is set to a shard name, this parameter indicates the boundary value of the shard. |
| specified       | boolean | Whether the DN corresponding to the current segment is explicitly specified in the DDL. |
| sliceorder      | int     | User-defined shard sequence.                                 |