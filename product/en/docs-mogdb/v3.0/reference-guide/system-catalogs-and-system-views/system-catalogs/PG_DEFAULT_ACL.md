---
title: PG_DEFAULT_ACL
summary: PG_DEFAULT_ACL
author: Guo Huan
date: 2021-04-19
---

# PG_DEFAULT_ACL

**PG_DEFAULT_ACL** records initial permissions assigned to newly created objects.

**Table 1** PG_DEFAULT_ACL columns

| Name            | Type      | Description                                                  |
| :-------------- | :-------- | :----------------------------------------------------------- |
| oid             | oid       | Row identifier (hidden attribute, which must be specified)   |
| defaclrole      | oid       | ID of the role associated with the permission                |
| defaclnamespace | oid       | Namespace associated with the permission (**0** if no ID)    |
| defaclobjtype   | "char"    | Object type of the permission<br />- **r** indicates a table or view.<br />- **S** indicates a sequence.<br />- **f** indicates a function.<br />- **T** indicates a type.<br />- **K** indicates the client master key.<br />- **k** indicates the column encryption key. |
| defaclacl       | aclitem[] | Access permissions that this type of object should have on creation |
