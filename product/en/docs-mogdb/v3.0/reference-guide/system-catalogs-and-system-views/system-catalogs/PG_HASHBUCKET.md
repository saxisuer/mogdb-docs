---
title: PG_HASHBUCKET
summary: PG_HASHBUCKET
author: Guo Huan
date: 2022-05-13
---

# PG_HASHBUCKET

**PG_HASHBUCKET** records hash bucket information.

**Table 1** PG_HASHBUCKET columns

| Name          | Type             | Description                                                  |
| :------------ | :--------------- | :----------------------------------------------------------- |
| oid           | oid              | Row identifier (hidden attribute, which must be specified)   |
| bucketid      | oid              | Hash value calculated for a bucket vector. The hash value can be used to accelerate the search for a bucket vector. |
| bucketcnt     | integer          | Number of shards                                             |
| bucketmapsize | integer          | Total number of shards on all DNs                            |
| bucketref     | integer          | Reserved column with **1** as its default value              |
| bucketvector  | oidvector_extend | Records all bucket IDs contained in the bucket information in this row. A unique index is created in this column. Tables with the same bucket ID share the **PG_HASHBUCKET** data in the same row. |