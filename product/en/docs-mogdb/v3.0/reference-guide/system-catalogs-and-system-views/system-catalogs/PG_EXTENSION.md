---
title: PG_EXTENSION
summary: PG_EXTENSION
author: Guo Huan
date: 2021-04-19
---

# PG_EXTENSION

**PG_EXTENSION** records information about the installed extensions. Default extensions of openGauss: PLPGSQL and MOT_FDW.

**Table 1** PG_EXTENSION

| Name           | Type    | Description                                                  |
| :------------- | :------ | :----------------------------------------------------------- |
| oid            | oid     | Database object ID                                           |
| extname        | name    | Extension name                                               |
| extowner       | oid     | Owner of the extension                                       |
| extnamespace   | oid     | Namespace containing the extension's exported objects        |
| extrelocatable | Boolean | Whether the extension can be relocated to another namespace. **true** indicates that relocation is allowed. |
| extversion     | text    | Version number of the extension                              |
| extconfig      | oid[]   | Configuration information about the extension                |
| extcondition   | text[]  | Filter conditions for the extension's configuration information |
