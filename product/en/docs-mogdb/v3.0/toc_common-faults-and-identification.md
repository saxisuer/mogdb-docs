<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 3.0

## Common Faults and Identification

+ [Common Fault Locating Methods](/common-faults-and-identification/common-fault-locating-methods.md)
+ Common Fault Locating Cases
  + Core Fault Locating
    + [Core Dump Occurs due to Full Disk Space](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/1-core-dump-occurs-due-to-full-disk-space.md)
    + [Core Dump Occurs Due to Incorrect Settings of GUC Parameter log_directory](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/2-core-dump-occurs-due-to-incorrect-settings-of-guc-parameter-log-directory.md)
    + [Core Dump Occurs when RemoveIPC Is Enabled](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/3-core-dump-occurs-when-removeipc-is-enabled.md)
    + [Core Dump Occurs After Installation on x86](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/4-core-dump-occurs-after-installation-on-x86.md)
  + Permission/Session/Data Type Fault Location
    + [Forcibly Terminating a Session](/common-faults-and-identification/common-fault-locating-cases/13-forcibly-terminating-a-session.md)
    + [Different Data Is Displayed for the Same Table Queried By Multiple Users](/common-faults-and-identification/common-fault-locating-cases/19-different-data-is-displayed.md)
    + [An Error Occurs During Integer Conversion](/common-faults-and-identification/common-fault-locating-cases/22-an-error-occurs-during-integer-conversion.md)
  + Service/High Availability/Concurrency Fault Location
    + [Standby Node in the Need Repair (WAL) State](/common-faults-and-identification/common-fault-locating-cases/3-standby-node-in-the-need-repair-state.md)
    + [Service Startup Failure](/common-faults-and-identification/common-fault-locating-cases/5-service-startup-failure.md)
    + [Primary Node Is Hung in Demoting During a Switchover](/common-faults-and-identification/common-fault-locating-cases/9-primary-node-is-hung-in-demoting.md)
    + ["too many clients already" Is Reported or Threads Failed To Be Created in High Concurrency Scenarios](/common-faults-and-identification/common-fault-locating-cases/23-too-many-clients-already.md)
  + Table/Partition Table Fault Location
    + [Table Size Does not Change After VACUUM FULL Is Executed on the Table](/common-faults-and-identification/common-fault-locating-cases/17-table-size-does-not-change.md)
    + [An Error Is Reported When the Table Partition Is Modified](/common-faults-and-identification/common-fault-locating-cases/18-an-error-is-reported-when-the-table-partition-is-modified.md)
  + File System/Disk/Memory Fault Location
    + [After You Run the du Command to Query Data File Size In the XFS File System, the Query Result Is Greater than the Actual File Size](/common-faults-and-identification/common-fault-locating-cases/7-after-you-run-the-du-command.md)
    + [File Is Damaged in the XFS File System](/common-faults-and-identification/common-fault-locating-cases/8-file-is-damaged-in-the-xfs-file-system.md)
    + [Insufficient Memory](/common-faults-and-identification/common-fault-locating-cases/4-insufficient-memory.md)
    + ["Error:No space left on device" Is Displayed](/common-faults-and-identification/common-fault-locating-cases/6-error-no-space-left-on-device-is-displayed.md)
    + [When the TPC-C is running and a disk to be injected is full, the TPC-C stops responding](/common-faults-and-identification/common-fault-locating-cases/2-when-the-tpcc-is-running.md)
    + [Disk Space Usage Reaches the Threshold and the Database Becomes Read-only](/common-faults-and-identification/common-fault-locating-cases/10-disk-space-usage-reaches-the-threshold.md)
  + SQL Fault Location
    + ["Lock wait timeout" Is Displayed When a User Executes an SQL Statement](/common-faults-and-identification/common-fault-locating-cases/16-lock-wait-timeout-is-displayed.md)
    + [Analyzing Whether a Query Statement Is Blocked](/common-faults-and-identification/common-fault-locating-cases/14-analyzing-whether-a-query-statement-is-blocked.md)
    + [Low Query Efficiency](/common-faults-and-identification/common-fault-locating-cases/15-low-query-efficiency.md)
    + [Slow Response to a Query Statement](/common-faults-and-identification/common-fault-locating-cases/11-slow-response-to-a-query-statement.md)
    + [Analyzing the Status of a Query Statement](/common-faults-and-identification/common-fault-locating-cases/12-analyzing-the-status-of-a-query-statement.md)
  + Index Fault Location
    + [When a User Specifies Only an Index Name to Modify the Index, A Message Indicating That the Index Does Not Exist Is Displayed](/common-faults-and-identification/common-fault-locating-cases/20-when-a-user-specifies-only-an-index-name.md)
    + [Reindexing Fails](/common-faults-and-identification/common-fault-locating-cases/21-reindexing-fails.md)
    + [B-tree Index Faults](/common-faults-and-identification/common-fault-locating-cases/24-b-tree-index-faults.md)
