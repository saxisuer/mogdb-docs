---
title: Mogeaver 简介
summary: Mogeaver 简介
author: Bin.Liu
date: 2022-06-15
---

# Mogeaver

## About Mogeaver

Mogeaver [moˈgi:və(r) ] is based on the popular open source graphical tool [DBeaver](https://dbeaver.io/), in strict compliance with DBeaver Community Edition [ASL](https://dbeaver.io/product/dbeaver_license.txt) based on the open source protocol, secondary development and packaging have been carried out to support the graphical development and management of the MogDB database, and to support the graphical creation, modification, and debugging of stored procedures and custom functions in the database. and packages.

## Why choose Mogeaver

- Multi-platform support

  - Support macOS Intel version, support macOS Apple Silicon version,

  - Support Windows, including but not limited to Windows 10, Windows 11

  - Support Linux, including but not limited to openEuler, Kylin Linux, UOS, Ubuntu, CentOS

- Compatible with MogDB features

  - Support to create database to choose different compatibility mode (A, B, PG)

  - Support manage MogDB database roles (role)

  - Support manage Large sequences (supported after MogDB 3.0)

  - Support manage partition table (Partition Table)

  - Support manage jobs in the database (Job)

  - Support modification and viewing of BLOB and CLOB fields

- Support graphical debugging

  - Support dbe_pldebugger debugging function of MogDB server
  - Support breakpoint debugging and single-step debugging
  - Support viewing real-time changes of variable values ​​during debugging
  - Support positioning and jumping to the exact code error location

## Use Mogeaver

For how to use Mogeaver, see [Use Mogeaver to Access MogDB](../quick-start/mogdb-access/use-gui-tools-to-access-mogdb/mogeaver-usage.md).
