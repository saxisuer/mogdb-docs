---
title: Release Notes
summary: Release Notes
author: Guo Huan
date: 2022-09-27
---

# Release Notes

| Version             | Release Date | Overview                                                     |
| ------------------- | ------------ | ------------------------------------------------------------ |
| [3.0.2](./3.0.2.md) | 2022.09.16   | MogDB 3.0.2 is a patch version of MogDB 3.0.0. Function modifications include version upgrade optimization (support direct upgrade from v2.0 to v3.0), and the gsql '\l' command adds the function of displaying the database. MogDB 3.0.2 fixed the known defects, and synchronize the fixes for openGauss 3.0.1. |
| [3.0.1](./3.0.1.md) | 2022.07.30   | MogDB 3.0.1 is a patch version of MogDB 3.0.0, which fixed bugs of database kernel, CM and extensions. |
| [3.0.0](./3.0.0.md) | 2022.06.30   | MogDB 3.0.0 is an enhanced version based on MogDB 2.1 and incorporated with new features of openGauss 3.0.0, such as row-store execution to vectorized execution, parallel logical decoding, publication-subscription, CM (Cluster Manager). At the same time, MogDB 3.0.0 support asynchronous commit of autonomous transactions, parallelism control achieved during index creation, dynamic partition pruning, optimized COPY (Import), monitoring the run status of SQLs, Brin Index, Bloom Index and other self-developed features. |