---
title: When to Use List Partitioning
summary: When to Use List Partitioning
author: Guo Huan
date: 2022-06-14
---

# When to Use List Partitions

List partitioning explicitly map rows to partitions based on discrete values.

In example 1, account managers who analyze accounts by region can make effective use of partition pruning.

**Example 1 Creating a list-partitioned table**

```sql
CREATE TABLE accounts
( id             NUMBER
, account_number NUMBER
, customer_id    NUMBER
, branch_id      NUMBER
, region         VARCHAR(2)
, status         VARCHAR2(1)
)
PARTITION BY LIST (region)
( PARTITION p_northwest VALUES ('OR', 'WA')
, PARTITION p_southwest VALUES ('AZ', 'UT', 'NM')
, PARTITION p_northeast VALUES ('NY', 'VM', 'NJ')
, PARTITION p_southeast VALUES ('FL', 'GA')
, PARTITION p_northcentral VALUES ('SD', 'WI')
, PARTITION p_southcentral VALUES ('OK', 'TX')
);
```