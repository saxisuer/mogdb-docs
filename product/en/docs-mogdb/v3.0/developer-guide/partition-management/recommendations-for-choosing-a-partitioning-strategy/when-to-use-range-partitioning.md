---
title: When to Use Range Partitioning
summary: When to Use Range Partitioning
author: Guo Huan
date: 2022-06-14
---

# When to Use Range Partitioning

Range partitioning is useful for organizing similar data, especially date and time data.

Range partitioning is a convenient way to partition historical data. The boundaries of a range partition define the order of partitions in a table or index

Most SQL statements that access range partitions focus on time ranges. For example, SQL statements select data from a specific time period. If each partition represents a month of data, the amount of data scanned is reduced to a fraction of the total. This optimization method is called partition pruning.

Range partitions are also useful when you need to periodically load new data and clean up old data, because range partitions are easy to add or remove. For example, the system typically keeps a scrolling data window that keeps data online for the last 36 months. Range partitioning simplifies this process.

In summary, you can consider using range partitioning when:

- It is often necessary to perform range predicate scans on easily partitioned columns in large tables.
- You want to maintain a scrolling data window.
- Large tables cannot be managed within a specified time range, such as backup and restoration, but can be divided into smaller logical blocks based on partition range columns.

**Example 1 Creating a range-partitioned table**

```sql
CREATE TABLE employees (
    id INT NOT NULL,
    fname VARCHAR(30),
    lname VARCHAR(30),
    hired DATE NOT NULL DEFAULT '1970-01-01',
    separated DATE DEFAULT '9999-12-31',
    job_code INT,
    store_id INT NOT NULL
);
```