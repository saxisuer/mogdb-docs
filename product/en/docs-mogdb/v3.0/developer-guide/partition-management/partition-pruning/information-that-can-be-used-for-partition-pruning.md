---
title: Information that Can Be Pruned
summary: Information that Can Be Pruned
author: Guo Huan
date: 2022-06-14
---

# Information that Can Be Pruned

Partition pruning can be performed on partitioned columns.

MogDB prunes partitions when you use range, `LIKE`, equality, and `IN` list predicates on range or list partition columns, and equality and `IN` list predicates on hash partition columns.

In example 1, table prune_tt01 is partitioned by range on column A.

MogDB uses predicates on partition columns to perform partition pruning, as shown below.

**Example 1 Createing a table with partition pruning**

```sql
CREATE TABLE prune_tt01(a int, b int)
PARTITION BY RANGE(a)
(
  PARTITION prune_tt01_p1 VALUES LESS THAN(5),
  PARTITION prune_tt01_p2 VALUES LESS THAN(10),
  PARTITION prune_tt01_p3 VALUES LESS THAN(15),
  PARTITION prune_tt01_p4 VALUES LESS THAN(MAXVALUE)
);
INSERT INTO prune_tt01 VALUES (generate_series(1, 20), generate_series(1,20));
CREATE INDEX index_prune_tt01 ON prune_tt01 USING btree(a) LOCAL;

select * from prune_tt01 where a > 8 ;
```