---
title: Static Partition Pruning
summary: Static Partition Pruning
author: Guo Huan
date: 2022-06-14
---

# Static Partition Pruning

MogDB determines when static pruning is used primarily based on static predicates.

If MogDB can identify which consecutive set of partitions was accessed at parsing time, the **Selected Partitions** column in the execution plan shows the start and end values of the partition being accessed. For any other partition pruning case (including dynamic partition pruning), MogDB displays **Selected Partitions**.

Static partition pruning is available via the [EXPLAIN VERBOSE](../../../reference-guide/sql-syntax/EXPLAIN.md) statement to view the execution plan. In the following example, there are four partitions in the prune_tt01 table. The query plan shows that partitions 3 and 4 are scanned by SeqScan. Therefore, it is judged that partitions 1 and 2 have been cut out.

```sql
MogDB=# \d+ prune_tt01
                      Table "public.prune_tt01"
 Column |  Type   | Modifiers | Storage | Stats target | Description 
--------+---------+-----------+---------+--------------+-------------
 a      | integer |           | plain   |              | 
 b      | integer |           | plain   |              | 
Indexes:
    "index_prune_tt01" btree (a) LOCAL TABLESPACE pg_default
Partition By RANGE(a)
Number of partitions: 4 (View pg_partition to check each partition range.)
Has OIDs: no
Options: orientation=row, compression=no
```

```sql
MogDB=# explain verbose select * from prune_tt01 where a>12;
                                             QUERY PLAN                                             
----------------------------------------------------------------------------------------------------
 Partition Iterator  (cost=13.80..27.75 rows=716 width=8)
   Output: a, b
   Iterations: 2
   Selected Partitions:  3..4
   ->  Partitioned Bitmap Heap Scan on public.prune_tt01  (cost=13.80..27.75 rows=716 width=8)
         Output: a, b
         Recheck Cond: (prune_tt01.a > 12)
         ->  Partitioned Bitmap Index Scan on index_prune_tt01  (cost=0.00..13.62 rows=716 width=0)
               Index Cond: (prune_tt01.a > 12)
(9 rows)
```

<br/>

The detailed information for static partition pruning is shown in the following table.

**Table 1** Detailed information for static partition pruning

| No.  | Constraint Name                | Constraint Range                                             |
| ---- | ------------------------------ | ------------------------------------------------------------ |
| 1    | Partition table type           | Range partition, list partition, and hash partition          |
| 2    | Partition expression type      | - A partition constraint can be a single expression, such as `a >12`<br/>- A partition constraint can be a BOOL expression, such as `a > 2 and a < 12`<br/>- A partition constraint can be an array, such as `a in (2, 3)`<br/>- A partition constraint can be a constant expression, such as `1 = 1`<br/>- A partition constraint can be the `Is (NOT)NULL` expression, such as `a IS NULL` |
| 3    | Partition expression operator  | - Range partition table supports five operators, including `=`, `>`, `>=`, `<`, and `<=`.<br/>- List partition and hash partition supports only the `=` operator. |
| 4    | Partition expression parameter | One side is partition key, and the other side is constance, such as `a > 12`. |
| 5    | Second-level partition         | Combination of range, list, and hash partition tables, such as Range-List partition table. |
| 6    | Partition pruning result       | Explain verbose shows the pruned partition list.             |
