---
title: How to Judge that Parition Pruning Is Used
summary: How to Judge that Parition Pruning Is Used
author: Guo Huan
date: 2022-06-22
---

### How to Judge that Parition Pruning Is Used

Whether MogDB uses partition pruning is reflected from the execution plan of SQLs. You can check it using EXPLAIN VERBOSE or EXPLAIN ANALYZE.

The partition pruning information is reflected in the execution plan column Iterations and Selected Partitions.