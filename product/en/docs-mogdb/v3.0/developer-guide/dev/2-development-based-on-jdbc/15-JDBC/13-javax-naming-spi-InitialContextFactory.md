---
title: javax.naming.spi.InitialContextFactory
summary: javax.naming.spi.InitialContextFactory
author: Guo Huan
date: 2021-05-17
---

# javax.naming.spi.InitialContextFactory

This section describes **javax.naming.spi.InitialContextFactory**, the initial context factory interface.

**Table 1** Support status for javax.naming.spi.InitialContextFactory

| Method Name                                         | Return Type | Support JDBC 4 |
| :-------------------------------------------------- | :---------- | :------------- |
| getInitialContext(Hashtable&lt;?,?&gt; environment) | Context     | Yes            |
