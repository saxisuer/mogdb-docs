---
title: java.sql.Connection
summary: java.sql.Connection
author: Guo Huan
date: 2021-05-17
---

# java.sql.Connection

This section describes **java.sql.Connection**, the interface for connecting to a database.

**Table 1** Support status for java.sql.Connection

| Method Name                                                  | Return Type                      | JDBC 4 Is Supported Or Not |
| :----------------------------------------------------------- | :------------------------------- | :------------------------- |
| abort(Executor executor)                                     | void                             | Yes                        |
| clearWarnings()                                              | void                             | Yes                        |
| close()                                                      | void                             | Yes                        |
| commit()                                                     | void                             | Yes                        |
| createArrayOf(String typeName, Object[] elements)            | Array                            | Yes                        |
| createBlob()                                                 | Blob                             | Yes                        |
| createClob()                                                 | Clob                             | Yes                        |
| createSQLXML()                                               | SQLXML                           | Yes                        |
| createStatement()                                            | Statement                        | Yes                        |
| createStatement(int resultSetType, int resultSetConcurrency) | Statement                        | Yes                        |
| createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) | Statement                        | Yes                        |
| getAutoCommit()                                              | Boolean                          | Yes                        |
| getCatalog()                                                 | String                           | Yes                        |
| getClientInfo()                                              | Properties                       | Yes                        |
| getClientInfo(String name)                                   | String                           | Yes                        |
| getHoldability()                                             | int                              | Yes                        |
| getMetaData()                                                | DatabaseMetaData                 | Yes                        |
| getNetworkTimeout()                                          | int                              | Yes                        |
| getSchema()                                                  | String                           | Yes                        |
| getTransactionIsolation()                                    | int                              | Yes                        |
| getTypeMap()                                                 | Map&lt;String,Class&lt;?&gt;&gt; | Yes                        |
| getWarnings()                                                | SQLWarning                       | Yes                        |
| isClosed()                                                   | Boolean                          | Yes                        |
| isReadOnly()                                                 | Boolean                          | Yes                        |
| isValid(int timeout)                                         | boolean                          | Yes                        |
| nativeSQL(String sql)                                        | String                           | Yes                        |
| prepareCall(String sql)                                      | CallableStatement                | Yes                        |
| prepareCall(String sql, int resultSetType, int resultSetConcurrency) | CallableStatement                | Yes                        |
| prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) | CallableStatement                | Yes                        |
| prepareStatement(String sql)                                 | PreparedStatement                | Yes                        |
| prepareStatement(String sql, int autoGeneratedKeys)          | PreparedStatement                | Yes                        |
| prepareStatement(String sql, int[] columnIndexes)            | PreparedStatement                | Yes                        |
| prepareStatement(String sql, int resultSetType, int resultSetConcurrency) | PreparedStatement                | Yes                        |
| prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) | PreparedStatement                | Yes                        |
| prepareStatement(String sql, String[] columnNames)           | PreparedStatement                | Yes                        |
| releaseSavepoint(Savepoint savepoint)                        | void                             | Yes                        |
| rollback()                                                   | void                             | Yes                        |
| rollback(Savepoint savepoint)                                | void                             | Yes                        |
| setAutoCommit(boolean autoCommit)                            | void                             | Yes                        |
| setClientInfo(Properties properties)                         | void                             | Yes                        |
| setClientInfo(String name,String value)                      | void                             | Yes                        |
| setHoldability(int holdability)                              | void                             | Yes                        |
| setNetworkTimeout(Executor executor, int milliseconds)       | void                             | Yes                        |
| setReadOnly(boolean readOnly)                                | void                             | Yes                        |
| setSavepoint()                                               | Savepoint                        | Yes                        |
| setSavepoint(String name)                                    | Savepoint                        | Yes                        |
| setSchema(String schema)                                     | void                             | Yes                        |
| setTransactionIsolation(int level)                           | void                             | Yes                        |
| setTypeMap(Map&lt;String,Class&lt;?&gt;&gt; map)             | void                             | Yes                        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The AutoCommit mode is used by default within the interface. If you disable it by running **setAutoCommit(false)**, all the statements executed later will be packaged in explicit transactions, and you cannot execute statements that cannot be executed within transactions.
