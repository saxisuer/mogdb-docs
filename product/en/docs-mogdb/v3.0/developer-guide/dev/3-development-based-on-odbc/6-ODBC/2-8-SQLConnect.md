---
title: SQLConnect
summary: SQLConnect
author: Guo Huan
date: 2021-05-17
---

# SQLConnect

## Function

SQLConnect is used to establish a connection between a driver and a data source. After the connection is established, the connection handle can be used to access all information about the data source, including its application operating status, transaction processing status, and error information.

## Prototype

```
SQLRETURN  SQLConnect(SQLHDBC        ConnectionHandle,
                      SQLCHAR        *ServerName,
                      SQLSMALLINT    NameLength1,
                      SQLCHAR        *UserName,
                      SQLSMALLINT    NameLength2,
                      SQLCHAR        *Authentication,
                      SQLSMALLINT    NameLength3);
```

## Parameter

**Table 1** SQLConnect parameters

| **Keyword**      | **Parameter Description**                         |
| :--------------- | :------------------------------------------------ |
| ConnectionHandle | Connection handle, obtained from SQLAllocHandle.  |
| ServerName       | Name of the data source to connect.               |
| NameLength1      | Length of **ServerName**.                         |
| UserName         | Username of the database in the data source.      |
| NameLength2      | Length of **UserName**.                           |
| Authentication   | User password of the database in the data source. |
| NameLength3      | Length of **Authentication**.                     |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.
- **SQL_STILL_EXECUTING** indicates that the statement is being executed.

## Precautions

If SQLConnect returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_DBC** and **ConnectionHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See [Examples](2-23-Examples.md).
