---
title: SQLFreeHandle
summary: SQLFreeHandle
author: Guo Huan
date: 2021-05-17
---

# SQLFreeHandle

## Function

SQLFreeHandle is used to release resources associated with a specific environment, connection, or statement handle. It replaces the ODBC 2.x functions: SQLFreeEnv, SQLFreeConnect, and SQLFreeStmt.

## Prototype

```
SQLRETURN SQLFreeHandle(SQLSMALLINT   HandleType,
                        SQLHANDLE     Handle);
```

## Parameter

**Table 1** SQLFreeHandle parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| HandleType  | Type of handle to be freed by SQLFreeHandle. The value must be one of the following:<br/>- SQL_HANDLE_ENV<br/>- SQL_HANDLE_DBC<br/>- SQL_HANDLE_STMT<br/>- SQL_HANDLE_DESC<br/>If **HandleType** is not one of the preceding values, SQLFreeHandle returns **SQL_INVALID_HANDLE**. |
| Handle      | Name of the handle to be freed.                              |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLFreeHandle returns **SQL_ERROR**, the handle is still valid.

## Example

See [Examples](2-23-Examples.md).
