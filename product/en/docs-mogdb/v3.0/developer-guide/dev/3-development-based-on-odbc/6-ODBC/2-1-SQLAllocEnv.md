---
title: SQLAllocEnv
summary: SQLAllocEnv
author: Guo Huan
date: 2021-05-17
---

# SQLAllocEnv

In ODBC 3.x, SQLAllocEnv (an ODBC 2.x function) was deprecated and replaced by SQLAllocHandle. For details, see [SQLAllocHandle](2-3-SQLAllocHandle.md).
