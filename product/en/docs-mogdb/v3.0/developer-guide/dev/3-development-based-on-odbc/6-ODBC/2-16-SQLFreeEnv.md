---
title: SQLFreeEnv
summary: SQLFreeEnv
author: Guo Huan
date: 2021-05-17
---

# SQLFreeEnv

In ODBC 3.x, SQLFreeEnv (an ODBC 2.x function) was deprecated and replaced by SQLFreeHandle. For details, see [SQLFreeHandle](2-15-SQLFreeHandle.md).
