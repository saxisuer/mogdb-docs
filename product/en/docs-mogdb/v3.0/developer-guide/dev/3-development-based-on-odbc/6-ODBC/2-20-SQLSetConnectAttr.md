---
title: SQLSetConnectAttr
summary: SQLSetConnectAttr
author: Guo Huan
date: 2021-05-17
---

# SQLSetConnectAttr

## Function

SQLSetConnectAttr is used to set connection attributes.

## Prototype

```
SQLRETURN SQLSetConnectAttr(SQLHDBC       ConnectionHandle
                            SQLINTEGER    Attribute,
                            SQLPOINTER    ValuePtr,
                            SQLINTEGER    StringLength);
```

## Parameter

**Table 1** SQLSetConnectAttr parameters

| **Keyword**      | **Parameter Description**                                    |
| :--------------- | :----------------------------------------------------------- |
| ConnectionHandle | Connection handle.                                           |
| Attribute        | Attribute to set.                                            |
| ValuePtr         | Pointer to the **Attribute** value. **ValuePtr** depends on the **Attribute** value, and can be a 32-bit unsigned integer value or a null-terminated string. If the **ValuePtr** parameter is a driver-specific value, it may be a signed integer. |
| StringLength     | If **ValuePtr** points to a string or a binary buffer, **StringLength** is the length of ***ValuePtr**. If **ValuePtr** points to an integer, **StringLength** is ignored. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLSetConnectAttr returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_DBC** and **ConnectionHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See [Examples](2-23-Examples.md).
