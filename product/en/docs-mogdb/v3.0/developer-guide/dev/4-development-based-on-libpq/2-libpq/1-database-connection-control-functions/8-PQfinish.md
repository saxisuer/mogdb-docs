---
title: PQfinish
summary: PQfinish
author: Guo Huan
date: 2021-05-17
---

# PQfinish

## Function

PQfinish is used to close the connection to the server and release the memory used by the PGconn object.

## Prototype

```
void PQfinish(PGconn *conn);
```

## Parameter

**Table 1** PQfinish parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |

## Precautions

If the server connection attempt fails (as indicated by PQstatus), the application should call PQfinish to release the memory used by the PGconn object. The PGconn pointer must not be used again after PQfinish has been called.

## Example

For details, see [Example](../../libpq-example.md).
