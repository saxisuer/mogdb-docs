---
title: PQsendPrepare
summary: PQsendPrepare
author: Guo Huan
date: 2021-05-17
---

# PQsendPrepare

## Function

PQsendPrepare is used to send a request to create a prepared statement with given parameters, without waiting for completion.

## Prototype

```c
int PQsendPrepare(PGconn *conn,
                  const char *stmtName,
                  const char *query,
                  int nParams,
                  const Oid *paramTypes);
```

## Parameters

**Table 1** PQsendPrepare parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |
| stmtName    | Name of **stmt** to be executed.                             |
| query       | Query string to be executed.                                 |
| nParams     | Parameter quantity.                                          |
| paramTypes  | Array of the parameter type.                                 |

## Return Value

**int** indicates the execution result. **1** indicates successful execution and **0** indicates an execution failure. The failure cause is stored in **conn-&gt;errorMessage**.

## Precautions

PQsendPrepare is an asynchronous version of PQprepare. If it can dispatch a request, **1** is returned. Otherwise, **0** is returned. After a successful calling of PQsendPrepare, call PQgetResult to check whether the server successfully created the prepared statement. PQsendPrepare parameters are handled in the same way as PQprepare parameters. Like PQprepare, PQsendPrepare cannot work on connections using protocol 2.0.

## Example

For details, see Example.
