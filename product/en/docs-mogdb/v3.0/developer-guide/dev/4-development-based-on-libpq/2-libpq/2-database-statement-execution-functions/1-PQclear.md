---
title: PQclear
summary: PQclear
author: Guo Huan
date: 2021-05-17
---

# PQclear

## Function

PQclear is used to release the storage associated with PGresult. Any query result should be released by PQclear when it is no longer needed.

## Prototype

```
void PQclear(PGresult *res);
```

## Parameters

**Table 1** PQclear parameter

| **Keyword** | **Parameter Description**                      |
| :---------- | :--------------------------------------------- |
| res         | Object pointer that contains the query result. |

## Precautions

PGresult is not automatically released. That is, it does not disappear when a new query is submitted or even if you close the connection. To delete it, you must call PQclear. Otherwise, memory leakage occurs.

## Example

For details, see Example.
