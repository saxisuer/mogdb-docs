---
title: PQgetCancel
summary: PQgetCancel
author: Guo Huan
date: 2021-05-17
---

# PQgetCancel

## Function

PQgetCancel is used to create a data structure that contains the information required to cancel a command issued through a specific database connection.

## Prototype

```c
PGcancel *PQgetCancel(PGconn *conn);
```

## Parameter

**Table 1** PQgetCancel parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |

## Return Value

**PGcancel** points to the object pointer that contains the cancel information.

## Precautions

PQgetCancel creates a PGcancel object for a given PGconn connection object. If the given connection object (**conn**) is NULL or an invalid connection, PQgetCancel will return NULL. The PGcancel object is an opaque structure that cannot be directly accessed by applications. It can be transferred only to PQcancel or PQfreeCancel.

## Example

For details, see Example.
