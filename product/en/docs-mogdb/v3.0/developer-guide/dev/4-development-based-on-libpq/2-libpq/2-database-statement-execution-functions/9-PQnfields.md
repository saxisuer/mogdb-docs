---
title: PQnfields
summary: PQnfields
author: Guo Huan
date: 2021-05-17
---

# PQnfields

## Function

PQnfields is used to return the number of columns (fields) in each row of the query result.

## Prototype

```
int PQnfields(const PGresult *res);
```

## Parameter

**Table 1**

| **Keyword** | **Parameter Description** |
| :---------- | :------------------------ |
| res         | Operation result handle.  |

## Return Value

Value of the int type

## Example

For details, see Example.
