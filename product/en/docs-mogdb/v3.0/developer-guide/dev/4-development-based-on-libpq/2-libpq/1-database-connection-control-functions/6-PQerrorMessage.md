---
title: PQerrorMessage
summary: PQerrorMessage
author: Guo Huan
date: 2021-05-17
---

# PQerrorMessage

## Function

PQerrorMessage is used to return error information on a connection.

## Prototype

```
char* PQerrorMessage(const PGconn* conn);
```

## Parameter

**Table 1**

| **Keyword** | **Parameter Description** |
| :---------- | :------------------------ |
| conn        | Connection handle.        |

## Return Value

char pointers

## Example

For details, see [Example](../../libpq-example.md).
