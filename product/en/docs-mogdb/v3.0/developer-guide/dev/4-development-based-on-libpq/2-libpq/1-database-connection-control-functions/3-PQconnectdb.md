---
title: PQconnectdb
summary: PQconnectdb
author: Guo Huan
date: 2021-05-17
---

# PQconnectdb

## Function

PQconnectdb is used to establish a new connection with the database server.

## Prototype

```
PGconn *PQconnectdb(const char *conninfo);
```

## Parameter

**Table 1** PQconnectdb parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conninfo    | Connection string. For details about the fields in the string, see Connection Characters. |

## Return Value

**PGconn \*** points to the object pointer that contains a connection. The memory is applied for by the function internally.

## Precautions

- This function establishes a new database connection using the parameters taken from the string **conninfo**.
- The input parameter can be empty, indicating that all default parameters can be used. It can contain one or more values separated by spaces or contain a URL.

## Example

For details, see [Example](../../libpq-example.md).
