---
title: PQstatus
summary: PQstatus
author: Guo Huan
date: 2021-05-17
---

# PQstatus

## Function

PQstatus is used to return the connection status.

## Prototype

```
ConnStatusType PQstatus(const PGconn *conn);
```

## Parameter

**Table 1** PQ status parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |

## Return Value

**ConnStatusType** indicates the connection status. The enumerated values are as follows:

```
CONNECTION_STARTED
Waiting for the connection to be established.

CONNECTION_MADE
Connection succeeded; waiting to send

CONNECTION_AWAITING_RESPONSE
Waiting for a response from the server.

CONNECTION_AUTH_OK
Authentication received; waiting for backend startup to complete.

CONNECTION_SSL_STARTUP
Negotiating SSL encryption.

CONNECTION_SETENV
Negotiating environment-driven parameter settings.

CONNECTION_OK
Normal connection.

CONNECTION_BAD
Failed connection.
```

## Precautions

The connection status can be one of the preceding values. After the asynchronous connection procedure is complete, only two of them, **CONNECTION_OK** and **CONNECTION_BAD**, can return. **CONNECTION_OK** indicates that the connection to the database is normal. **CONNECTION_BAD** indicates that the connection attempt fails. Generally, the **CONNECTION_OK** state remains until PQfinish is called. However, a communication failure may cause the connection status to become to **CONNECTION_BAD** before the connection procedure is complete. In this case, the application can attempt to call PQreset to restore the communication.

## Example

For details, see [Example](../../libpq-example.md).
