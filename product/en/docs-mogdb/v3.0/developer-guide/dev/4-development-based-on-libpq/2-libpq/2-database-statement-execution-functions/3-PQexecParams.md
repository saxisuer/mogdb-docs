---
title: PQexecParams
summary: PQexecParams
author: Guo Huan
date: 2021-05-17
---

# PQexecParams

## Function

PQexecParams is used to run a command to bind one or more parameters.

## Prototype

```
PGresult* PQexecParams(PGconn* conn,
                       const char* command,
                       int nParams,
                       const Oid* paramTypes,
                       const char* const* paramValues,
                       const int* paramLengths,
                       const int* paramFormats,
                       int resultFormat);
```

## Parameter

**Table 1**

| **Keyword**  | **Parameter Description**           |
| :----------- | :---------------------------------- |
| conn         | Connection handle.                  |
| command      | SQL text string.                    |
| nParams      | Number of parameters to be bound.   |
| paramTypes   | Types of parameters to be bound.    |
| paramValues  | Values of parameters to be bound.   |
| paramLengths | Parameter lengths.                  |
| paramFormats | Parameter formats (text or binary). |
| resultFormat | Result format (text or binary).     |

## Return Value

PGresult pointers
