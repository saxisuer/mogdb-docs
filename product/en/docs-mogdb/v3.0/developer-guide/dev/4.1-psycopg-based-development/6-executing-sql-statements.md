---
title: Executing SQL Statements
summary: Executing SQL Statements
author: Zhang Cuiping
date: 2021-10-11
---

# Executing SQL Statements

1. Construct an operation statement and use **%s** as a placeholder. During execution, psycopg2 will replace the placeholder with the parameter value. You can add the RETURNING clause to obtain the automatically generated column values.
2. The **cursor.execute** method is used to perform operations on one row, and the **cursor.executemany** method is used to perform operations on multiple rows.