---
title: Closing the Connection
summary: Closing the Connection
author: Zhang Cuiping
date: 2021-10-11
---

# Closing the Connection

After you complete required data operations in a database, close the database connection. Call the close method such as **connection.close()** to close the connection.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** This method closes the database connection and does not automatically call **commit()**. If you just close the database connection without calling **commit()** first, changes will be lost.