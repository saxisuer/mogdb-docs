---
title: Loading a Driver
summary: Loading a Driver
author: Zhang Cuiping
date: 2021-10-11
---

# Loading a Driver

- Before using the driver, perform the following operations:

  1. Decompress the driver package of the corresponding version and copy psycopg2 to the **site-packages** folder in the Python installation directory as the **root** user.
  2. Change the **psycopg2** directory permission to **755**.
  3. Add the **psycopg2** directory to the environment variable *$PYTHONPATH* and validate it.
  4. For non-database users, configure the **lib** directory in *LD_LIBRARY_PATH* after decompression.

- Load a database driver before creating a database connection:

  ```bash
  import  psycopg2
  ```
