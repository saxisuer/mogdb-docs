---
title: cursor.close()
summary: cursor.close()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.close()

## Function

This method closes the cursor of the current connection.

## Prototype

```
cursor.close()
```

## Parameter

None

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).