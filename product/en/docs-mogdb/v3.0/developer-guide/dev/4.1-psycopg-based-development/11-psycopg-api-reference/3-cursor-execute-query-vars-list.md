---
title: cursor.execute(query,vars_list)
summary: cursor.execute(query,vars_list)
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.execute(query,vars_list)

## Function

This method executes the parameterized SQL statements (that is, placeholders instead of SQL literals). The psycopg2 module supports placeholders marked with **%s**.

## Prototype

```
curosr.execute(query,vars_list)
```

## Parameters

**Table 1** curosr.execute parameters

| **Keyword** | **Description**                                              |
| :---------- | :----------------------------------------------------------- |
| query       | SQL statement to be executed.                                |
| vars_list   | Variable list, which matches the **%s** placeholder in the query. |

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).
