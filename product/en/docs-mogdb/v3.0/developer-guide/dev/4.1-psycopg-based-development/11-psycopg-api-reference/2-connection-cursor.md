---
title: connection.cursor()
summary: connection.cursor()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.cursor()

## Function

This method returns a new cursor object.

## Prototype

```
cursor(name=None, cursor_factory=None, scrollable=None, withhold=False)
```

## Parameter

**Table 1** connection.cursor parameters

| **Keyword**    | **Description**                                              |
| :------------- | :----------------------------------------------------------- |
| name           | Cursor name. The default value is **None**.                  |
| cursor_factory | Creates a non-standard cursor. The default value is **None**. |
| scrollable     | Sets the SCROLL option. The default value is **None**.       |
| withhold       | Sets the HOLD option. The default value is **False**.        |

## Return Value

Cursor object (used for cusors that are programmed using Python in the entire database)

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).
