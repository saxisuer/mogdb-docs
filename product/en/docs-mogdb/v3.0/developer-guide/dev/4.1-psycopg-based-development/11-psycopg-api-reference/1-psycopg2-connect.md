---
title: psycopg2.connect()
summary: psycopg2.connect()
author: Zhang Cuiping
date: 2021-10-11
---

# psycopg2.connect()

## Function

This method creates a database session and returns a new connection object.

## Prototype

```
conn=psycopg2.connect(dbname="test",user="postgres",password="secret",host="127.0.0.1",port="5432")
```

## Parameter

**Table 1** psycopg2.connect parameters

| **Keyword** | **Description**                                              |
| :---------- | :----------------------------------------------------------- |
| dbname      | Database name.                                               |
| user        | Username.                                                    |
| password    | Password.                                                    |
| host        | Database IP address. The default type is UNIX socket.        |
| port        | Connection port number. The default value is **5432**.       |
| sslmode     | SSL mode, which is used for SSL connection.                  |
| sslcert     | Path of the client certificate, which is used for SSL connection. |
| sslkey      | Path of the client key, which is used for SSL connection.    |
| sslrootcert | Path of the root certificate, which is used for SSL connection. |

## Return Value

Connection object (for connecting to the openGauss DB instance)

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).
