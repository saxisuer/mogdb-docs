---
title: Introduction
summary: Introduction
author: Zhang Cuiping
date: 2021-05-17
---

# Introduction

The foreign data wrapper (FDW) of the MogDB can implement cross-database operations between MogDB databases and remote databases. Currently, the following remote servers are supported: Oracle, MySQL(MariaDB), PostgreSQL/openGauss/MogDB(postgres_fdw), file_fdw, dblink.
