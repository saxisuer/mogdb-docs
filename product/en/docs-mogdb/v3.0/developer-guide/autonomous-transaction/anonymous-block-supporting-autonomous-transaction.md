---
title: Anonymous Block Supporting Autonomous Transaction
summary: Anonymous Block Supporting Autonomous Transaction
author: Guo Huan
date: 2021-10-15
---

# Anonymous Block Supporting Autonomous Transaction

An autonomous transaction can be defined in an anonymous block. The identifier of an autonomous transaction is **PRAGMA AUTONOMOUS_TRANSACTION**. The syntax of an autonomous transaction is the same as that of creating an anonymous block. The following is an example.

```sql
MogDB=# create table t1(a int ,b text);
CREATE TABLE

START TRANSACTION;
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

    insert into t1 values(1,'you are so cute,will commit!');
END;
/
MogDB=# insert into t1 values(1,'you will rollback!');
INSERT 0 1
MogDB=# rollback;
ROLLBACK

MogDB=# select * from t1;
a | b
---+------------------------------
1 | you are so cute,will commit!
(1 row)
```

In the preceding example, an anonymous block containing an autonomous transaction is finally executed before a transaction block to be rolled back, which directly illustrates a characteristic of the autonomous transaction, that is, rollback of the primary transaction does not affect content that has been committed by the autonomous transaction.
