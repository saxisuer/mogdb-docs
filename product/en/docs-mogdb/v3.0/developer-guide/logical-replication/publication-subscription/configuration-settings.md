---
title: Configuration Settings
summary: Configuration Settings
author: Guo Huan
date: 2022-04-29
---

# Configuration Settings

Publication-subscription requires some configuration options to be set.

- On the publisher, **wal_level** must be set to **logical**, and the value of **max_replication_slots** must be at least the minimum number of subscriptions expected to be connected. **max_wal_senders** should be set to at least the value of **max_replication_slots** plus the number of physical replicas that are connected at the same time.
- **max_replication_slots** must also be set on the subscriber. It must be set to at least the number of subscriptions that will be added to the subscriber. **max_logical_replication_workers** must be set to at least the minimum number of subscriptions.