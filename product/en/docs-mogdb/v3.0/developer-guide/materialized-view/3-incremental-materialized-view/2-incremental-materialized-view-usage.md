---
title: Usage
summary: Usage
author: Guo Huan
date: 2021-05-21
---

# Usage

## Syntax

- Create a incremental materialized view.

  ```
  CREATE INCREMENTAL MATERIALIZED VIEW [ view_name ] AS { query_block };
  ```

- Fully refresh a materialized view.

  ```
  REFRESH MATERIALIZED VIEW [ view_name ];
  ```

- Incrementally refresh a materialized view.

  ```
  REFRESH INCREMENTAL MATERIALIZED VIEW [ view_name ];
  ```

- Delete a materialized view.

  ```
  DROP MATERIALIZED VIEW [ view_name ];
  ```

- Query a materialized view.

  ```
  SELECT * FROM [ view_name ];
  ```

## Examples

```
-- Prepare data.
MogDB=# CREATE TABLE t1(c1 int, c2 int);
MogDB=# INSERT INTO t1 VALUES(1, 1);
MogDB=# INSERT INTO t1 VALUES(2, 2);

-- Create an incremental materialized view.
MogDB=# CREATE INCREMENTAL MATERIALIZED VIEW mv AS SELECT * FROM t1;
CREATE MATERIALIZED VIEW

-- Insert data.
MogDB=# INSERT INTO t1 VALUES(3, 3);
INSERT 0 1

-- Incrementally refresh a materialized view.
MogDB=# REFRESH INCREMENTAL MATERIALIZED VIEW mv;
REFRESH MATERIALIZED VIEW

-- Query the materialized view result.
MogDB=# SELECT * FROM mv;
 c1 | c2
----+----
  1 |  1
  2 |  2
  3 |  3
(3 rows)

-- Insert data.
MogDB=# INSERT INTO t1 VALUES(4, 4);
INSERT 0 1

-- Fullly refresh a materialized view.
MogDB=# REFRESH MATERIALIZED VIEW mv;
REFRESH MATERIALIZED VIEW

-- Query the materialized view result.
MogDB=# select * from mv;
 c1 | c2
----+----
  1 |  1
  2 |  2
  3 |  3
  4 |  4
(4 rows)

-- Delete a materialized view.
MogDB=# DROP MATERIALIZED VIEW mv;
DROP MATERIALIZED VIEW
```
