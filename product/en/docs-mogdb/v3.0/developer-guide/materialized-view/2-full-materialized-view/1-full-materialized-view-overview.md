---
title: Overview
summary: Overview
author: Liuxu
date: 2021-05-21
---

# Overview

Full materialized views can be fully refreshed only. The syntax for creating a full materialized view is similar to the CREATE TABLE AS syntax.
