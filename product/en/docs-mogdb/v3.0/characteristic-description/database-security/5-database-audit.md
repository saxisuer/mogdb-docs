---
title: Database Audit
summary: Database Audit
author: Guo Huan
date: 2022-05-07
---

# Database Audit

## Availability

This feature is available as of MogDB 1.1.0.

## Introduction

Audit logs record user operations performed on database startup and stopping, as well as connection, DDL, DML, and DCL operations.

## Benefits

The audit log mechanism enhances the database capability of tracing unauthorized operations and collecting evidence.

## Description

Database security is essential for a database system. MogDB writes all user operations in the database into audit logs. Database security administrators can use the audit logs to reproduce a series of events that cause faults in the database and identify unauthorized users, unauthorized operations, and the time when these operations are performed.

## Enhancements

None

## Constraints

None

## Dependencies

None