---
title: Data Encryption and Storage
summary: Data Encryption and Storage
author: Guo Huan
date: 2022-05-07
---

# Data Encryption and Storage

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Imported data is encrypted before stored.

## Benefits

You can use encrypted import interfaces to encrypt sensitive information and store it in a table.

## Description

MogDB provides the encryption functions **gs_encrypt_aes128()** and **gs_encrypt()**, and decryption functions **gs_decrypt_aes128()** and **gs_decrypt()**. Before you import data to a certain column in a table, you can use this function to encrypt the data. The function can be called using a statement in the following format:

```shell
gs_encrypt_aes128(column, key), gs_encrypt (decryptstr, keystr, decrypttype)
```

In the preceding command, **key** indicates the initial password specified by the user, which is used to derive the encryption key. To encrypt an entire table, you need to write an encryption function for each column.

If a user with the required permission wants to view specific data, the user can decrypt required columns using the decryption function interface **gs_decrypt_aes128(***column***, ***key\***)**. To invoke the interface, run the following command:

```shell
gs_decrypt_aes128(column, key), gs_decrypt(decryptstr, keystr, decrypttype)
```

## Enhancements

None.

## Constraints

None.

## Dependencies

None.