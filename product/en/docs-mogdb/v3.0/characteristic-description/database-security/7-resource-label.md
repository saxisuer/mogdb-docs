---
title: Resource Label
summary: Resource Label
author: Guo Huan
date: 2022-05-07
---

# Resource Label

## Availability

This feature is available as of MogDB 1.1.0.

## Introduction

Database resources refer to database objects, such as databases, schemas, tables, columns, views, and triggers. The more the database objects are, the more complex the classification management of database resources is. The resource label mechanism is a technology that classifies and labels database resources with certain features to implement resource classification management. After adding labels to some resources in a database, administrators can perform operations such as data audit or anonymization using the labels to implement security management on labeled database resources.

## Benefits

Proper resource labels can be used to effectively classify data objects, improve management efficiency, and simplify security policy configuration. To perform unified audit or data anonymization on a group of database resources, the administrator can allocate a resource label to these resources first. The label indicates that the database resources have a certain feature or require unified configuration of a certain policy. The administrator can directly perform operations on the resource label, which greatly reduces the complexity of policy configuration and information redundancy as well as improves management efficiency.

## Description

The resource label mechanism selectively classifies resources in the current database. Administrators can use the following SQL syntax to create a resource label and add the label to a group of database resources:

```sql
CREATE RESOURCE LABEL schm_lb ADD SCHEMA(schema_for_label);
CREATE RESOURCE LABEL tb_lb ADD TABLE(schema_for_label.table_for_label);
CREATE RESOURCE LABEL col_lb ADD COLUMN(schema_for_label.table_for_label.column_for_label);
CREATE RESOURCE LABEL multi_lb ADD SCHEMA(schema_for_label), TABLE(table_for_label);
```

**schema_for_label**, **table_for_label**, and **column_for_label** indicate the schema, table, and column to be labeled, respectively. The **schm_lb** label is added to schema **schm_for_label**, **tb_lb** is added to table **table_for_label**, **col_lb** is added to column **column_for_label**, and **multi_lb** is added to schema **schm_for_label** and table **table_for_label**. You can perform unified audit or dynamic data anonymization using the configured resource labels, that is, manage all labeled database resources.

Currently, resource labels support the following database resource types: schema, table, column, view, and function.

## Enhancements

None

## Constraints

- Resource labels can be created only by a user with the **POLADMIN** and **SYSADMIN** attributes or an initial user.
- Resource labels cannot be created for temporary tables.
- Columns in the same basic table can belong to only one resource tag.

## Dependencies

None