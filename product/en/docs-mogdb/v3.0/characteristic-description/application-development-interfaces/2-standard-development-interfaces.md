---
title: Standard Development Interfaces
summary: Standard Development Interfaces
author: Guo Huan
date: 2022-05-07
---

# Standard Development Interfaces

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Standard ODBC 3.5 and JDBC 4.0 APIs are supported.

## Benefits

Standard ODBC and JDBC interfaces are provided to ensure quick migration of user services to MogDB.

## Description

Currently, the standard ODBC 3.5 and JDBC 4.0 APIs are supported. The ODBC interface supports SUSE Linux, Windows 32-bit, and Windows 64-bit platforms. The JDBC API supports all platforms.

## Enhancements

The function of connecting JDBC to a third-party log framework is added. JDBC can interconnect with a third-party log framework to meet users' log management and control requirements.

## Constraints

None

## Dependencies

None