---
title: MogDB-Oracle Compatibility
summary: MogDB-Oracle Compatibility
author: Zhang Cuiping
date: 2022-06-17
---

# MogDB-Oracle Compatibility

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

MogDB is compatible with Oracle's related functions and package functions using whale extension.

In the function part, it mainly adds instrb, nls_charset_id, nls_charset_name, nls_lower, nls_upper, ora_hash, remainder, replace, show, show_parameter, to_timestamp, to_yminterval, tz_offset, nullif, ratio_to_report, etc.

packages are generally only used in stored procedures, and according to ORACLE data package rules, new packages are placed under the corresponding schema. The supported Oracle management packages are dbms_random, dbms_output, dbms_lock, dbms_application_info, dbms_metadata, dbms_job, dbms_utility.

For more information about the functions and the packages, please see the [whale](. /... /reference-guide/oracle-plugins/whale.md).

## Benefits

MogDB's compatibility with Oracle is enhanced by using the whale extension to enhance MogDB functions.

## Related Pages

[whale](../../reference-guide/oracle-plugins/whale.md), [Character Processing Functions and Operators](../../reference-guide/functions-and-operators/3-character-processing-functions-and-operators.md), [Mathematical Functions and Operators](../../reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md), [Date and Time Processing Functions and Operators](../../reference-guide/functions-and-operators/8-date-and-time-processing-functions-and-operators.md), [HLL Functions and Operators](../../reference-guide/functions-and-operators/13.1-hll-functions-and-operators.md), [Window Functions](../../reference-guide/functions-and-operators/18-window-functions.md), [System Information Functions](../../reference-guide/functions-and-operators/23-system-information-functions.md)