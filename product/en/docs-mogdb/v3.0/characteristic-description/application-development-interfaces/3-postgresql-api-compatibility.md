---
title: PostgreSQL API Compatibility
summary: PostgreSQL API Compatibility
author: Guo Huan
date: 2022-05-07
---

# PostgreSQL API Compatibility

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Compatible with PostgreSQL clients and standard APIs.

## Benefits

Compatible with the PostgreSQL clients and standard APIs, and can be seamlessly interconnected with PostgreSQL ecosystem tools.

## Description

Compatible with PostgreSQL clients and standard APIs.

## Enhancements

None

## Constraints

None

## Dependencies

None