---
title: Index Creation Parallel Control
summary: Index Creation Parallel Control
author: Guo Huan
date: 2022-06-17
---

# Index Creation Parallel Control

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

This feature supports specifying the degree of parallelism directly in the index creation statement, making more efficient use of resources while increasing usage flexibility.

## Benefits

This feature adds the option to specify the degree of parallelism in the index creation syntax, so that DBAs and delivery testers can control the degree of concurrency of index creation from the syntax level to achieve optimal execution.

## Description

Add parallel syntax when creating indexes to control the parallelism of index creation.

## Related Pages

[CREATE INDEX](../../reference-guide/sql-syntax/CREATE-INDEX.md)