---
title: JDBC Client Load Balancing and Read/Write Isolation
summary: JDBC Client Load Balancing and Read/Write Isolation
author: Guo Huan
date: 2022-05-07
---

# JDBC Client Load Balancing and Read/Write Isolation

## Availability

This feature is available since MogDB 2.1.0.

## Introduction

The JDBC client provides load balancing and read/write isolation capabilities.

## Benefits

Load balancing and read/write isolation can be configured on the JDBC client.

## Description

The IP addresses and port numbers of multiple nodes on the client are configured to adapt to HA switchover between multiple AZs and remote DR switchover. The connection-level read/write isolation configuration is supported. Preferentially connecting to read-only nodes is supported. Multiple read-only nodes are evenly distributed.

## Enhancements

None.

## Constraints

None.

## Dependencies

None.