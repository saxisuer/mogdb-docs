---
title: Data Compression in OLTP Scenarios
summary: Data Compression in OLTP Scenarios
author: Guo Huan
date: 2022-05-10
---

# Data Compression in OLTP Scenarios

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

The feature supports row-store data compression in OLTP scenarios, provides a general compression algorithm, and implements transparent compression of data pages and maintenance of page storage locations to achieve high compression and high performance. Disk persistence is implemented using two types of files: compressed address file (with the file name extension .pca) and compressed data file (with the file name extension .pcd).

## Benefits

Typically, it is applicable where the database disk space needs to be reduced.

## Description

Data compression in OLTP scenarios can reduce the disk storage space of row tables and index data and improve performance in I/O-intensive database systems.

## Constraints

- Only row-store tables and BTree indexes are supported. The feature cannot be used on Ustore and segment-page storage engines.
- Compressed table index files are accessed using mmap. **max_map_count** must be set properly based on the number of compressed table files.