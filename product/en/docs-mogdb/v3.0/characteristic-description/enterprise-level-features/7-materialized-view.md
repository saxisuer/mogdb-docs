---
title: Materialized View
summary: Materialized View
author: Guo Huan
date: 2022-05-07
---

# Materialized View

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

A materialized view is a special physical table, which is relative to a common view. A common view is a virtual table and has many application limitations. Any query on a view is actually converted into a query on an SQL statement, and performance is not actually improved. The materialized view actually stores the results of the statements executed by the SQL statement, and is used to cache the results.

## Benefits

The materialized view function is used to improve query efficiency.

## Description

Full materialized views and incremental materialized views are supported. Full materialized views can only be updated in full mode. Incremental materialized views can be updated asynchronously. You can run statements to update new data to materialized views.

## Enhancements

None

## Constraints

Only simple filter queries and UNION ALL statements are supported for base tables.

## Dependencies

None