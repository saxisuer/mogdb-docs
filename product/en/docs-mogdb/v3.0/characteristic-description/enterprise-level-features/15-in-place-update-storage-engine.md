---
title: In-place Update Storage Engine
summary: In-place Update Storage Engine
author: Guo Huan
date: 2022-05-07
---

# In-place Update Storage Engine

## Availability

This feature is available since MogDB 2.1.0.

## Introduction

The in-place update storage engine is a new storage mode added to MogDB. The row storage engine used by the earlier versions of MogDB is in append update mode. The append update has good performance in addition, deletion, and HOT (Heap Only Tuple) update (that is, update on the same page) in the service. However, in a non-HOT UPDATE scenario across data pages, garbage collection is not efficient. The Ustore storage engine can solve this problem.

## Benefits

The in-place update storage engine can effectively reduce storage space occupation after tuples are updated for multiple times.

## Description

The in-place update storage engine solves the problems of space expansion and large tuples of the Append update storage engine. The design of efficient rollback segments is the basis of the in-place update storage engine.

## Enhancements

None.

## Constraints

None.

## Dependencies

None.