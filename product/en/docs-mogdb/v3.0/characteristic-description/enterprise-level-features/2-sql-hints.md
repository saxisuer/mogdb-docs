---
title: SQL Hints
summary: SQL Hints
author: Guo Huan
date: 2022-05-07
---

# SQL Hints

## Availability

This feature is available as of MogDB 1.1.0.

## Introduction

SQL hints can be used to override execution plans.

## Benefits

Improves SQL query performance.

## Description

In plan hints, you can specify a join order; join, stream, and scan operations, the number of rows in a result, and redistribution skew information to tune an execution plan, improving query performance.

## Enhancements

None

## Constraints

None

## Dependencies

None