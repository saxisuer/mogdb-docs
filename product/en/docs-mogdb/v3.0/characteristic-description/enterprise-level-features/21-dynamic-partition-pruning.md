---
title: Dynamic Partition Pruning
summary: Dynamic Partition Pruning
author: Guo Huan
date: 2022-06-17
---

# Dynamic Partition Pruning

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

This feature mainly implements the following functions.

1. Support NEST LOOP for partition pruning.

2. Support bind variables for partition pruning.

3. Support subqueries for partition pruning.

4. Support using EXPLAIN ANALYZE to view the dynamic partition pruning results.

## Benefits

This feature mainly optimizes the partition pruning feature, introduces dynamic partition pruning, and supports viewing partition pruning results by EXPLAIN ANALYZE. This feature improves the query performance of partitioned tables by pruning off the unneeded partitions and then scanning the partitioned tables during SQL execution.

## Related Pages

[Static Partition Pruning](../../developer-guide/partition-management/partition-pruning/static-partition-pruning.md), [Dynamic Partition Pruning](../../developer-guide/partition-management/partition-pruning/dynamic-partition-pruning.md)
