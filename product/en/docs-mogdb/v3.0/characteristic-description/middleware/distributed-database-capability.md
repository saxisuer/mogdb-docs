---
title: Distributed Database Capability
summary: Distributed Database Capability
author: Guo Huan
date: 2022-05-10
---

# Distributed Database Capability

## Availability

This feature is available since MogDB 2.1.0.

## Introduction

This feature uses the distributed middleware shardingsphere to provide MogDB the distributed database capability. When 16 Kunpeng 920 (128 cores) nodes are used for networking (1 x shardingsphere-proxy, 7 x shardingsphere-jdbc, 8 x MogDB), the perfect sharding performance is greater than 10 million transactions per minute C (tpmC).

## Benefits

A distributed database that logically has no resource restriction can be built over the middleware.

## Description

With the sharding capability of shardingsphere, multiple MogDB databases can logically form a larger database with distributed transactions and elastic scaling capabilities. The usage method is the same as that of an MogDB database.

## Enhancements

None.

## Constraints

None.

## Dependencies

Shardingsphere middleware