---
title: Fault Diagnosis
summary: Fault Diagnosis
author: Zhang Cuiping
date: 2022-06-17
---

# Fault Diagnosis

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

In order to quickly locate faults, collect system fault information, export fault data and then rectify faults, MogDB 3.0 has enhanced OM functions and gstrace diagnostic capabilities.

## Benefits

The enhanced fault diagnosis capability can facilitate R&D personnel to rectify faults in time and ensure the normal operation of the system.

## Description

The gs_check tool can compare the difference of scenario check results and output a difference analysis report to help users locate the problem quickly.

The gs_watch tool can monitor MogDB processes and automatically call gs_collector to collect the system status when a process crash is found for later analysis.

The gs_gucquery tool can automatically collect, organize, and export GUC values, and compare the changes of GUC values at different moment.

gstrace diagnostic capability is enhanced. It supports opening the trace item of one or more component (module) and function by module name and function name. It enhances the number of gstrace points in the code and the ability to express gstrace output information. It supports export of new key data structures PGPROC and user session data. It realizes fault injection, including system call error report simulation and variable content saving write.

## Related Pages

[gs_check](../../reference-guide/tool-reference/server-tools/1-gs_check.md), [gs_gucquery](../../reference-guide/tool-reference/server-tools/gs_gucquery.md), [gstrace](../../reference-guide/tool-reference/tools-used-in-the-internal-system/16-gstrace.md), [gs_watch](../../reference-guide/tool-reference/server-tools/gs_watch.md)
