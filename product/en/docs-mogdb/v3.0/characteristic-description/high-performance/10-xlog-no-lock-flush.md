---
title: Xlog no Lock Flush
summary: Xlog no Lock Flush
author: Guo Huan
date: 2022-05-07
---

# Xlog no Lock Flush

## Availability

This feature is available since MogDB 2.0.0.

## Introduction

Canceled the WalInsertLock contention and dedicated WalWriter disk write thread.

## Benefits

The system performance is further improved on the basis that the original Xlog functions remain unchanged.

## Description

This feature optimizes the WalInsertLock mechanism by using log sequence numbers (LSNs) and log record counts (LRCs) to record the copy progress of each backend. The backend can directly copy logs to the WalBuffer without contending for the WalInsertLock. In addition, a dedicated WALWriter thread is used to write logs, and the backend thread does not need to ensure the Xlog flushing.

## Enhancements

None.

## Constraints

None.

## Dependencies

None.