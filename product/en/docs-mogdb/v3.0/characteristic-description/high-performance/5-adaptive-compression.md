---
title: Adaptive Compression
summary: Adaptive Compression
author: Guo Huan
date: 2022-05-07
---

# Adaptive Compression

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Data compression is the major technology used in current databases. Various compression algorithms are used for different data types. If pieces of data of the same type have different characteristics, their compression algorithms and results will also be different. Adaptive compression chooses the suitable compression algorithm for data based on the data type and characteristics, achieving high performance in compression ratio, import, and query.

## Benefits

Importing and frequently querying a huge amount of data are the main application scenarios. When you import data, adaptive compression greatly reduces the data volume, increases I/O operation efficiency several times, and clusters data before storage, achieving fast data import. In this way, only a small number of I/O operations is required and data is quickly decompressed in a query. Data can be quickly retrieved and the query result is quickly returned.

## Description

Currently, the database has implemented various compression algorithms on column store, including RLE, DELTA, BYTEPACK/BITPACK, LZ4, ZLIB, and LOCAL DICTIONARY. The following table lists data types and the compression algorithms suitable for them.

| -                                                            | RLE  | DELTA | BITPACK/BYTEPACK | LZ4  | ZLIB | LOCAL DICTIONARY |
| :----------------------------------------------------------- | :--- | :---- | :--------------- | :--- | :--- | :--------------- |
| Smallint/Int/Bigint/Oid<br/>Decimal/Real/Double<br/>Money/Time/Date/Timestamp | √    | √     | √                | √    | √    | -                |
| Tinterval/Interval/Time with time zone/                      | -    | -     | -                | -    | √    | -                |
| Numeric/Char/Varchar/Text/Nvarchar2<br/>and other supported data types | √    | √     | √                | √    | √    | √                |

## Enhancements

The compression level of compression algorithms can be adjusted.

## Constraints

None

## Dependencies

It depends on LZ4 or ZLIB.