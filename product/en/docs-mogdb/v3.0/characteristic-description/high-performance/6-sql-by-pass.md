---
title: SQL by pass
summary: SQL by pass
author: Guo Huan
date: 2022-05-07
---

# SQL by pass

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Query performance is improved by customizing an execution scheme for typical queries in the TP scenario.

## Benefits

The TP query performance is improved.

## Description

In a typical OLTP scenario, simple queries account for a large proportion. This type of queries involves only single tables and simple expressions. To accelerate such query, the SQL bypass framework is proposed. After simple mode judgment is performed on such query at the parse layer, the query enters a special execution path and skips the classic execution framework, including operator initialization and execution, expression, and projection. Instead, it directly rewrites a set of simple execution paths and directly invokes storage interfaces, greatly accelerating the execution of simple queries.

## Enhancements

None

## Constraints

None

## Dependencies

None