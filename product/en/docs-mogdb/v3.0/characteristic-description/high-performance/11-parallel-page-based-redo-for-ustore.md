---
title: Parallel Page-based Redo For Ustore
summary: Parallel Page-based Redo For Ustore
author: Guo Huan
date: 2022-05-07
---

# Parallel Page-based Redo For Ustore

## Availability

This feature is available since MogDB 2.1.0.

## Introduction

Optimized Ustore inplace update WAL write and improved the degree of parallelism for Ustore DML operation replay.

## Benefits

The WAL space used by the update operation is reduced, and the degree of parallelism for Ustore DML operation replay is improved.

## Description

Prefixes and suffixes are used to reduce the write times of WAL update. Replay threads are classified to solve the problem that most Ustore DML WALs are replayed on multiple pages. In addition, Ustore data pages are replayed based on **blkno**.

## Enhancements

None.

## Constraints

None.

## Dependencies

This feature depends on the Ustore engine.