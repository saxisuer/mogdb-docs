---
title: Global SysCache
summary: Global SysCache
author: Guo Huan
date: 2022-05-10
---

# Global SysCache

## 可获得性

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

Global SysCache is the global and local caches of system catalog data. Figure 1 illustrates the principle.

**Figure 1** Global SysCache principle

 ![Global-SysCache原理图](https://cdn-mogdb.enmotech.com/docs-media/mogdb/characteristic-description/Global-SysCache.png)

## Benefits

Global SysCache reduces the cache memory usage of database processes and improves the concurrent expansion capability of a database.

## Description

Global SysCache decouples the system cache from sessions and binds them to threads to reduce the memory usage together with the thread pool feature. In addition, it is used to improve the cache hit rate and ensure stable performance.

## Enhancements

Supports more concurrent queries.

## Constraints

- Set **enable_global_syscache** to **on**. You are advised to set **enable_thread_pool** to **on**.
- When the number of databases is large and the value of **global_syscache_threshold** is small, memory control cannot work properly and the performance deteriorates.
- Distributed time series tasks are not supported. The memory control and performance of these tasks are not affected by the GSC feature.
- If **wal_level** is set to **minimal** or **archive**, the query performance of the standby server deteriorates and short connections are used.

## Dependencies

The memory reduction capability of this feature depends on the thread pool feature.