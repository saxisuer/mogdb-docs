---
title: CM
summary: CM
author: Guo Huan
date: 2022-05-10
---

# CM

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

Cluster manager (CM) is a database management software, which consists of cm_server, cm_agent, om_monitor, cm_ctl. Its core features are as follows.

- cm_server

  Send commands (such as start, stop, status query, switchover and failover of database instances) to cm_agent on each node and receive responses;

  Receive the database instance status information reported by cm_agent on each node;

  Highly available arbitration of cm_server itself on each node;

  Highly available arbitration of database instances on each node.

- cm_agent

  Receive and execute commands from cm_server, such as start, stop, status query, switchover and failover of database instances;

  Monitor the status of the database instance running on the current node and report it to cm_server.

- cm_monitor

  Monitor the cm_agent service running on the current node to ensure its availability.

- cm_ctl

  Client tool for CM.

## Benefits

It manages and monitors the running status of functional units and physical resources in a database system, ensuring stable running of the system.

## Description

It supports customized resource monitoring and provides capabilities such as monitoring of the primary/standby database status, network communication faults, file system faults, and automatic primary/standby switchover upon faults. It also provides various database management capabilities, such as starting and stopping nodes and instances, querying database instance status, performing primary/standby switchover, and managing logs.

## Enhancements

- CM Highly Available Self-Arbitration with embedded arbitration algorithms and logic that does not rely on third-party components to achieve arbitration;

- Customized Resource Monitoring.

## Constraints

In scenarios where there are one primary node and one standby node, CM supports only basic capabilities, such as installation, startup, stop, and detection.

## Dependencies

Highly available arbitration relies on DCF, DCC, and CBB components.