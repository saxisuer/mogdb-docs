---
title: Container-based MogDB Installation
summary: Container-based MogDB Installation
author: Guo Huan
date: 2021-04-01
---

# Container-based MogDB Installation

This document introduces the installation of single instance and primary/standby instances in a MogDB docker container. MogDB container does not support the MogHA and OM tools, and currently supports at most one primary node and eight standby nodes. MogDB 3.0 container version supports the latest version of compat-tools and plugin functions.<br/>

In consideration of narrowing the container image size for quick download and deployment, it is recommended that the x86 and ARM-based OSs depended by MogDB running in a docker container will be Ubuntu and Debian since MogDB 2.0. <br/>

A MogDB container based on the x86-64 architecture runs in Ubuntu 18.04.<br/>

A MogDB container based on the ARM64 architecture runs in Debian 10.

<br/>

## Docker-based Deployment

### Before You Start

The installation of a MogDB container is independent of the host OS. All OSs that can run a container enginer are supported, such as Linux, Windows, and macOS.

Before installing the MogDB container, you need to prepare the container runnning environment, such as Docker.

Docker is an open source application container engine based on Go and compliant with the Apache 2.0 protocol. Docker allows developers to package their applications and dependency packages into a lightweight, portable container that can then be distributed to any popular Linux machine and virtualized.

Docker Desktop download address: <https://www.docker.com/products/docker-desktop>

<br/>

### Installation Procedures

1. Run Docker.

2. Run the following command to obtain the latest version of the MogDB image file.

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
   ```
   
   > Note: MogDB image supports x86 and ARM architectures, and the system will automatically download the corresponding image according to the server architecture that initiates the command, no need to specify manually.

3. Run the following command to create a running directory for MogDB, using **mogdb** as an example below.

   ```bash
   mkdir /mogdb
   ```

4. Continue with the following command to create a new container, and name it **mogdb** to run a MogDB instance:

   ```bash
   docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123  -v /mogdb:/var/lib/mogdb  -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
   ```

   For Windows OS:
   - If Docker uses Hyper-V as the engine, run the following commands in sequence to create the logical object **mogdata** of the volume in Hyper-V, and then create the container:

      ```bash
      docker volume create mogdata

      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v mogdata:/var/lib/mogdb -p 15453:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
      ```

   - If Docker uses WSL 2 as the engine, run the following command to create the container:

      ```bash
      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v C:\mogdb:/var/lib/mogdb -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
      ```

   > Note:
   >
   > - MogDB uses port 5432 in the container for listening by default. If you want to access the database from outside the container, you need to specify the `-p` parameter in `docker run` command. For example, the above command will allow access to the container database using port 15432.
   > - Once the container is deleted, all the data and configuration in the container will be lost. After re-running a container from the image, all the data is presented in the initialized state. So for the database container, in order to prevent the loss of data due to the demise of the container or corruption, the operation of persistent storage data is required. This is achieved by specifying the `-v` parameter in `docker run` command. For example, the above command will specify that all data files of MogDB will be stored under **/mogdb** of the host. In addition, If you use Podman, there will be a target path check. Therefore you need to create the target path in advance (step 4).

5. Execute bash in the **mogdb** container via the **exec** command:

   ```bash
   docker exec -it mogdb bash
   ```

   Now, the installation of MogDB in the container is complete.

<br/>

### Using MogDB

After the installation is complete and enter the container, switch to omm user by running "su - omm", you can access the database via gsql and use MogDB properly.

```bash
root@384ac97543bd:/# su - omm
omm@384ac97543bd:~$ gsql -d postgres
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

<br/>

### Environment Variables

To flexibly use an MogDB image, you can set additional parameters. In the future, more control parameters will be added. The current version supports the setting of the following variables:

**GS_PASSWORD**

This parameter is mandatory when the MogDB image is used. The value cannot be empty or undefined. This parameter specifies the passwords of superuser **omm** and test user **mogdb** of the MogDB database. During the MogDB installation, the superuser **omm** is created by default. This username cannot be changed. The test user **mogdb** is created in  **entrypoint.sh**.

The local trust mechanism is configured for the MogDB image. Therefore, no password is required for connecting to the database in the container. However, if you want to connect to the database from other hosts or containers, you need to enter the password.

The password must contain at least eight characters, including uppercase letters, lowercase letters, digits, and special characters ( \# ? ! @ $ % ^ & * \- ). ** ! $ &** must be escaped using a backslash (\\).

**GS_NODENAME**

Specifies the database node name. The default value is  **mogdb**.

**GS_USERNAME**

Specifies the username for connecting to the database. The default value is  **mogdb**.

**GS_PORT**

Specifies the database port. The default value is  **5432**.

<br/>

## Kubernetes-based Deployment

MogDB 2.1 and later supports Kubernetes-based deployment.

- x86
  
  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.0.1/k8s_amd.yaml
  pod/mogdb created
  ```

- arm

  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.0.1/k8s_arm.yaml
  pod/mogdb created
  ```

- Kubernetes connect the pod

  Connect to MogDB through the gsql command in the pod.

  ```
  $ kubectl exec -it mogdb -- bash
  root@mogdb:/# su - omm
  omm@mogdb:~$ gsql -d postgres
  gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
  Non-SSL connection (SSL connection is recommended when requiring high-security)
  Type "help" for help.
  
  MogDB=#
  ```

<br/>

## Sample dataset: Mogila

MogDB Container Edition has a built-in sample dataset mogila. Mogila is a database representing a DVD rental store, containing information about films (like title, category, actresses), rental stores (like address, staff members, customers) and rentals, where a customer rents a film from a store through its staff. After installing MogDB Container Edition, you can use the mogila database for various functional tests.

After the installation is complete and enter the container, switch to omm user by running `su - omm`, and execute `gsql -d mogila` to access the mogila database.

```sql
root@07456ed8f523:/# su - omm
omm@07456ed8f523:~$ gsql -d mogila
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#\dt
                             List of relations
 Schema |     Name      | Type  | Owner |             Storage
--------+---------------+-------+-------+----------------------------------
 public | actor         | table | mogdb | {orientation=row,compression=no}
 public | address       | table | mogdb | {orientation=row,compression=no}
 public | category      | table | mogdb | {orientation=row,compression=no}
 public | city          | table | mogdb | {orientation=row,compression=no}
 public | country       | table | mogdb | {orientation=row,compression=no}
 public | customer      | table | mogdb | {orientation=row,compression=no}
 public | film          | table | mogdb | {orientation=row,compression=no}
 public | film_actor    | table | mogdb | {orientation=row,compression=no}
 public | film_category | table | mogdb | {orientation=row,compression=no}
 public | inventory     | table | mogdb | {orientation=row,compression=no}
 public | language      | table | mogdb | {orientation=row,compression=no}
 public | payment       | table | mogdb | {orientation=row,compression=no}
 public | rental        | table | mogdb | {orientation=row,compression=no}
 public | staff         | table | mogdb | {orientation=row,compression=no}
 public | store         | table | mogdb | {orientation=row,compression=no}
(15 rows)

MogDB=#
```

For detailed introduction and query example of mogila sample dataset, please refer to [Using Sample Dataset Mogila](mogila.md).

<br/>

## What's Next

MogDB container does not support MogHA and OM tools. It is only used for testing, and currently supports at most one primary node and eight standby nodes. MogDB Enterprise Edition includes MogHA component. The basic functions of the MogDB container and Enterprise Edition are identical. It is recommended that MogDB Enterprise Edition is used in the production environment.
