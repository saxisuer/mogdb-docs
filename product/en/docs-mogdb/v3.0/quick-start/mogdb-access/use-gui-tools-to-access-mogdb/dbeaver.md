---
title: Use DBeaver to Access MogDB
summary: Use DBeaver to Access MogDB
author: Guo Huan
date: 2021-08-31
---

# Use DBeaver to Access MogDB

This document introduces how to access MogDB through DBeaver.

<br/>

## Introduction

**Dbeaver** is a cross-platform database developer tool, including SQL programming, database management and analysis. It supports any database system that adapts to the JDBC driver. At the same time, the tool also supports some non-JDBC data sources, such as MongoDB, Cassandra, Redis, DynamoDB, etc.

* This tool provides many powerful features, such as metadata editor, SQL editor, rich text data editor, ERD, data import/export/migration, SQL execution plan, etc.
* The tool is developed based on the eclipse platform.
* Adapted databases include MySQL/MariaDB, PostgreSQL, Greenplum, Oracle, DB2 LUW, Exasol, SQL Server, Sybase/SAP ASE, SQLite, Firebird, H2, HSQLDB, Derby, Teradata, Vertica, Netezza, Informix, etc.

<br/>

## Download and Installation

Dbeaver is an open source software with the download address as follows: [https://dbeaver.io/download/](https://dbeaver.io/download/)

Select and download the appropriate installation package according to your operating system, and double-click the package to install it after the download is complete.

<br/>

## Connection String Configuration

When the software is opened for the first time, the **Connect to a database** window will pop up, as shown in the figure below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-20.png)

You can also choose **Database &gt; New Database Connection** to open this window.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-21.png)

Choose **Popular &gt; PostgreSQL**, click **Next**, and download the PostgreSQL driver file on the pop-up page.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-22.png)

After the download is complete, the **Connect a database** window will pop up, fill in the database information in this window, and then click **Finish** to establish a database connection.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-23.png)

<br/>

## Interface Display

After a successful connection, the software interface is shown below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-24.png)

<br/>

## Follow-Up Procedure

If you want to learn more about the usage instructions of DBeaver, please choose **Help &gt; Help Contents**, or visit the following page to browse the official documents online: [https://dbeaver.com/docs/wiki/](https://dbeaver.com/docs/wiki/).

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-25.png)
