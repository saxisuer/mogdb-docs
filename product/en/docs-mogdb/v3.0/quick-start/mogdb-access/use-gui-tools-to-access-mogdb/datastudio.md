---
title: Use Data Studio to Access MogDB
summary: Use Data Studio to Access MogDB
author: Guo Huan
date: 2021-08-31
---

# Use Data Studio to Access MogDB

This document introduces how to access MogDB through Data Studio.

<br/>

## Introduction

**Data Studio** is an Integrated Development Environment (IDE) that helps database developers build the application conveniently. It supports essential features of the database.
This tool allows working with database objects with minimal programming knowledge. Data Studio provides you with various features, such as

- creating and managing database objects
- executing SQL statements or SQL scripts
- editing and executing PL/SQL statements
- viewing the execution plan and cost through the GUI
- exporting table data

The database objects that are created and managed include

- database
- schema
- function
- procedure
- table
- sequence
- index
- view
- tablespace
- synonym

It also provides SQL assistance for various queries/procedures/functions executed in the SQL Terminal and PL/SQL Viewer.

<br/>

## System Requirements

This section provides the minimum system requirements for using Data Studio.

**Hardware Requirements**

Table 1 Hardware Requirements for Data Studio

| **Hardware Requirement** | **Configuration**                                                     |
| ------------ | ------------------------------------------------------------ |
| CPU          | x86 64-bit                                                     |
| Available RAM     | A minimum of 1 GB memory                                                  |
| Available hard disk     | A minimum of 1 GB space reserved for Data Studio installation directory and 100 MB space reserved for user's HOME directory. |
|Network requirements|Gigabit Ethernet|

**Operating System Requirements**

The supported versions of Microsoft Windows running on a universal x86 server include Windows 7 (64-bit), Windows 10 (64-bit), Windows 2012 (64-bit), and Windows 2016 (64-bit).

**Software Requirements**

Java 1.8.0_181 or later

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:**
>
> * Please refer to [https://java.com/en/download/help/path.html](https://java.com/en/download/help/path.html) to set the Java Home path.
> * To ensure the optimal experience, the recommended minimum screen resolution is 1080 x 768. Below this resolution, the interface will be abnormal.

<br/>

## Download and Installation

The download address of Data Studio is as follows: <https://opengauss.org/en/download.html>

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/datastudio-2.png)

After downloading, unzip the installation package and double-click **Data Studio.exe** to run the software.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-2.png)

<br/>

## Connection String Configuration

When the software is opened for the first time, the **New Database Connection** window will pop up, as shown in the figure below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-15.png)

You can also choose **File &gt; New Connection** to open this window.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-16.png)

Fill in the information on the **General** tab, and click **OK** to establish a database connection.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-17.png)

<br/>

## Interface Display

After a successful connection, the software interface is shown below, with examples of syntax and operation lists on the right.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-18.png)

<br/>

## Follow-Up Procedure

If you want to learn more about the usage instructions of Data Studio, please choose **Help &gt; User Manual**, or visit the following page to browse the official documents online: [Data Studio User Manual](https://opengauss.obs.cn-south-1.myhuaweicloud.com/2.0.0/Data%20Studio%20User%20Manual.pdf).

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-19.png)
