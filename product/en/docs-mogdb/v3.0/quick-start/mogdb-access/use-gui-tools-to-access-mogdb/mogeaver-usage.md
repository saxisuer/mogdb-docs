---
title: Use Mogeaver to Access MogDB
summary: Use Mogeaver to Access MogDB
author: Guo Huan
date: 2022-06-27
---

# Use Mogeaver to Access MogDB

This document introduces how to access MogDB through Mogeaver.

<br/>

## Introduction

Mogeaver [moˈgi:və(r) ] is based on the popular open source graphical tool [DBeaver](https://dbeaver.io/), in strict compliance with the DBeaver Community Edition's [ASL](https://dbeaver.io/product/dbeaver_license.txt) open source protocol based on the secondary development and packaging, support for MogDB database graphical development and management, support for graphical way to create, modify, debug the database stored procedures, custom functions and packages.

<br/>

## Download and Installation

[Release Note](../../../mogeaver/mogeaver-release-notes.md)

<br/>

> MacOS If you encounter corruption, do
>
> sudo xattr -r -d com.apple.quarantine /Applications/Mogeaver.app

## Connection String Configuration

The following steps are based on the Windows version as an example.

After opening the mogeaver program, click on "**Database -> New Database Connection**".

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-7.png)

Select the MogDB database in the pop-up window, and click "**Next**".

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-8.png)

Fill in the information of the database to be connected, and then click "**Finish**" to establish the database connection.

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-9.png)

<br/>

## Interface Display

After a successful connection, the software interface is shown below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-10.png)

<br/>

## Next Steps

If you want to learn more about the usage instructions of Mogeaver, please choose **Help &gt; Help Contents**, or visit the following page to browse the official documents online: [https://dbeaver.com/docs/wiki/](https://dbeaver.com/docs/wiki/).

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-11.png)
