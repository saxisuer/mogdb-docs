---
title: WebSphere Used for Configuring MogDB (PostgreSQL) Data Sources
summary: WebSphere Used for Configuring MogDB (PostgreSQL) Data Sources
author: Guo Huan
date: 2021-08-09
---

# WebSphere Used for Configuring MogDB (PostgreSQL) Data Sources

## Overview

WebSphere can be used for configuring MogDB data sources. So does it for configuring PostgreSQL data sources. For details, see the following sections.

<br/>

## Prerequisites

### Confirmation of the WebSphere JDK Version

```bash
C:\Windows\system32>D:\IBM1\WebSphere\AppServer\java\bin\java -version
java version "1.6.0"
Java(TM) SE Runtime Environment (build pwa6460_26sr8fp26ifix-20160708_01(SR8 FP26+IX90174))
IBM J9 VM (build 2.6, JRE 1.6.0 Windows 8 amd64-64 Compressed References 20160406_298378 (JIT enabled, AOT enabled)
J9VM - R26_Java626_SR8_20160406_0830_B298378
JIT  - tr.r11_20160328_114192
GC   - R26_Java626_SR8_20160406_0830_B298378_CMPRSS
J9CL - 20160406_298378)
JCL  - 20160507_01

C:\Windows\system32>
```

Find the target JDK and check its version.

The JAR packages of the JDBC driver vary depending on the JDK version. Only when you are clear about the JDK version can you download the corresponding driver.

### Downloading of the JDBC Driver Package

```
The current version is JDBC 42.2.23.

This JDBC is the target driver to be downloaded unless you have special requirements (run the old application programs or JVM). It supports PostgreSQL 8.2 or later and requires Java 6 or a higher version. It also supports the SSL and javax.sql packages.

JDBC 4.2 applies when you use Java 8.
JDBC 4.1 applies when you use Java 7.
JDBC 4.0 applies when you use Java 6.
```

The links for downloading JDBC of certain versions corresponding to the JDK of specific versions are as follows:

| JDK Version | Website of the Corresponding JDBC Driver                     |
| ----------- | ------------------------------------------------------------ |
| 1.6         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre6.jar> |
| 1.7         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre7.jar> |
| 1.8         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jar>  |

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-1.png)

The best choice is to use JDBC  42.2.23 because it supports PostgreSQL 8.2 or a higher version and is the most stable version.

Download website: [https://jdbc.postgresql.org/download/](https://jdbc.postgresql.org/download/)

<br/>

## Configuration Process

### Confirmation of Related Database Information

| Database Name | User     | Password | Port | IP Address |
| ------------- | -------- | -------- | ---- | ---------- |
| postgres      | postgres | 123456   | 5432 |            |

### Confirmation of Port Connection

On the middleware server, access the IP port using Telnet and make sure that the database port can be accessed.

### Console Configuration

#### Configuring the JDBC Provider

a. Create a JDBC provider.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-1.png)

b. Create another JDBC provider.

org.postgresql.ds.PGConnectionPoolDataSource

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-2.png)

c. Specify the driver path.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-3.png)

d. Click **Next** to save the configuration.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-4.png)

#### Configuring Data Sources

a. Create a data source.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-5.png)

b. Fill JNDI with related information.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-6.png)

c. Select the existing JDBC provider.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-7.png)

d. Select the default option.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-8.png)

e. Click **Next**.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-9.png)

f. Save the configuration.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-10.png)

#### Configuring Database Authentication

a. Click the configured pgtest data source.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-11.png)

b. Configure the JAAS - J2C authentication data.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-12.png)

c. Create authentication.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-13.png)

d. Specify the user and password and specify the alias as you will.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-14.png)

e. Save the configuration.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-15.png)

f. Click the configured pgtest data source again and configure security settings.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-16.png)

g. Save the configuration.

#### Customizing Properties

a. Click the configured pgtest data source.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-17.png)

 b. Customize the properties.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-18.png)

c. Click the filter icon.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-19.png)

d. Filter serverName.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-20.png)

e. Click **serverName**.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-21.png)

f. Fill the IP address of the target database.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-22.png)

g. Save the configuration.

h. Similarly, filter portNumber/databaseName.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-23.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-24.png)

#### Restarting the Server to Test Connection

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-25.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-26.png)

<br/>

## MogDB Connected Using the openGauss Driver

openGauss JDBC provides only JDK 1.8. Applications developed using JDK 1.8 can connect MogDB using the openGauss driver.

Applications developed using JDK 1.6 or 1.7 can connect MogDB using only the JDBC driver from the PostgreSQL official website.

Website for downloading the openGauss driver:

[https://opengauss.org/en/download/](https://opengauss.org/en/download/)

<br/>

## Common Questions

The driver version does not match the JDK version.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-27.png)
