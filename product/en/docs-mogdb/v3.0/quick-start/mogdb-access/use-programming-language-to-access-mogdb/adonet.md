---
title: Use .NET to Access MogDB
summary: Use .NET to Access MogDB
author: Guo Huan
date: 2022-07-28
---

# Use .NET to Access MogDB

Npgsql is the open source .NET data provider for PostgreSQL. It allows you to connect and interact with PostgreSQL server using .NET. openGauss provides a driver for .NET based on [Npgsql](https://www.npgsql.org/), which is also applicable to the .NET language for connecting to MogDB.

You can view the source code and operating instructions for this driver by clicking on the following links.

[Source Code](https://gitee.com/opengauss/openGauss-connector-adonet), [Operating Instructions](https://gitee.com/opengauss/openGauss-connector-adonet/blob/master/README.md)
