---
title: Use Python to Access MogDB
summary: Use Python to Access MogDB
author: Guo Huan
date: 2022-03-25
---

# Use Python to Access MogDB

Psycopg is a Python API used to execute SQL statements and provides a unified access API for PostgreSQL and GaussDB. Applications can perform data operations based on psycopg. Psycopg2 is an encapsulation of libpq and is implemented using the C language, which is efficient and secure. It provides cursors on both clients and servers, asynchronous communication and notification, and the COPY TO and COPY FROM functions. Psycopg2 supports multiple types of Python out-of-the-box and adapts to PostgreSQL data types. Through the flexible object adaptation system, you can extend and customize the adaptation. Psycopg2 is compatible with Unicode and Python 3.

MogDB supports the psycopg2 feature and allows psycopg2 to be connected in SSL mode. For detailed instructions on using Python to access MogDB database, please refer to [Development Based on Psycopg](../../../developer-guide/dev/4.1-psycopg-based-development/1-psycopg-based-development.md) chapter in "Developer's Guide".

Driver Download:

- [psycopg2](https://opengauss.org/en/download/)
- [sqlalchemy-mogdb](https://pypi.org/project/sqlalchemy-mogdb/)
- [py-opengauss](https://gitee.com/opengauss/openGauss-connector-python-pyog)
