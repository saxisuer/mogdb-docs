---
title: Use C/C++ to Access MogDB
summary: Use C/C++ to Access MogDB
author: Guo Huan
date: 2022-03-25
---

# Use C/C++ to Access MogDB

libpq is the C application programmer's interface to PostgreSQL. libpq is a set of library functions that allow client programs to pass queries to the PostgreSQL backend server and to receive the results of these queries. libpq is also the underlying engine for several other PostgreSQL application interfaces, including those written for C++, Perl, Python, Tcl and ECPG.

For detailed instructions on using C/C++ to access MogDB database, please refer to [Development Based on libpq](../../../developer-guide/dev/4-development-based-on-libpq/dependent-header-files-of-libpq.md) chapter in "Developer's Guide".

Driver Download: [openGauss Download Page-openGauss Connectors-libpq_*.*.*](
https://opengauss.org/en/download.html)
