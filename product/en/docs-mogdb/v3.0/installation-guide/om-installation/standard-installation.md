---
title: Standard Installation
summary: Standard Installation
author: Zhang Cuiping
date: 2021-04-2
---

# Standard Installation

## Creating an XML Configuration File

Before installing MogDB, you need to create the cluster_config.xml file. The file contains the information about the server where MogDB is deployed, installation path, IP address, and port number. This file is used to guide how to deploy the MogDB. You need to configure the XML configuration file according to the actual deployment requirements.

<br/>

### Configuring the Database Name and Directories

Obtain the XML file template from **script/gspylib/etc/conf/cluster_config_template.xml**. The following is only an example. You can replace it as required. 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
<CLUSTER>
<PARAM name="clusterName" value="Cluster_template"/>
<PARAM name="nodeNames" value="node1_hostname,node2_hostname"/>
<PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app"/>
<PARAM name="gaussdbLogPath" value="/var/log/omm"/>
<PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
<PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om"/>
<PARAM name="corePath" value="/opt/mogdb/corefile"/>
<PARAM name="backIp1s" value="192.168.0.1,192.168.0.2"/>
</CLUSTER>
<DEVICELIST>
<DEVICE sn="node1_hostname">
<PARAM name="name" value="node1_hostname"/>
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<PARAM name="backIp1" value="192.168.0.1"/>
<PARAM name="sshIp1" value="192.168.0.1"/>
<!--  dn  -->
<PARAM name="dataNum" value="1"/>
<PARAM name="dataPortBase" value="15400"/>
<PARAM name="dataNode1" value="/opt/mgodb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
<PARAM name="dataNode1_syncNum" value="0"/>
</DEVICE>
<DEVICE sn="node2_hostname">
<PARAM name="name" value="node2_hostname"/>
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<PARAM name="backIp1" value="192.168.0.2"/>
<PARAM name="sshIp1" value="192.168.0.2"/>
</DEVICE>
</DEVICELIST>
</ROOT>
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE**:
>
> - The **/opt/mogdb/install/om** directory is used to store tools such as mutual trust. To avoid permission problems, do not store instance data in the directory.
> - The installation and data directories must be empty or do not exist. Otherwise, the installation may fail.
> - When configuring database instances, ensure that the configured directories are not coupled with each other. This means that the configured directories must not be associated with each other. If any directory is deleted, the other directories will not be deleted accordingly. For example, **gaussdbAppPath** is **/opt/mogdb/install/app** and **gaussdbLogPath** is **/opt/mogdb/install/app/omm**. If the directory specified by **gaussdbAppPath** is deleted, that specified by **gaussdbLogPath** will also be deleted, causing unexpected problems.
> - If the installation script is required to automatically create installation users, ensure that the configured directories are not coupled with the default user directories created by the system.
> - The MogDB and instance paths cannot contain double backslashes (\\) or the following characters: “|”,“;”,”&“,”$“,”<“,”>“,”`“,”\”,“'”,”\“”,“{”,“}”,“(”,“)”,”[“,”]“,”~“,”*“, and “?”.
> - When configuring the database node name, run the **hostname** command to obtain the host name of the database node and replace **node1_hostname** and **node2_hostname** in the example with the obtained host name.
> - During **dcf_config** configuration, the roles include leader, follower, passive, and logger. The roles that can vote include leader, follower, and logger. During role networking configuration, at least three roles can vote. Therefore, at least three nodes are required in DCF mode.

**Table 1** Parameter Description

<table>
  <tr>
    <td><b>Parameter</b></td>
    <td><b>Description</b></td>
  </tr>
  <tr>
    <td>clusterName</td>
    <td>MogDB name.</td>
  </tr>
  <tr>
    <td>nodeNames</td>
    <td>Host name in the MogDB.</td>
  </tr>
  <tr>
    <td>backIp1s</td>
    <td>
      Intranet IP address of the host in the backend storage network. All the
      MogDB hosts communicate with each other on this network.
    </td>
  </tr>
  <tr>
    <td>gaussdbAppPath</td>
    <td>
      Installation directory of the MogDB program. This directory must meet the
      following requirements:<br />- The disk space is greater than 1 GB.<br />-
      This directory is independent of other directories required by the
      database.
    </td>
  </tr>
  <tr>
    <td>gaussdbLogPath</td>
    <td>
      Directory that stores run logs and operation logs of the MogDB. This
      directory must meet the following requirements:<br />- You are advised to
      plan the disk space based on the number of database nodes on the host.
      Reserve 1 GB space for database nodes and reserve redundant space.<br />-
      This directory is independent of other directories required by the
      MogDB.<br />
      This directory is optional. If the directory is not specified,
      <b>$GAUSSLOG/installation uername</b> will be specified as the log
      directory by default during MogDB installation.
    </td>
  </tr>
  <tr>
    <td>tmpdbPath</td>
    <td>
      Directory for storing temporary database files.<br />
      If <b>tmpdbPath</b> is not set, the file is stored in
      <b>/opt/mogdb/tools/perfadm_db</b> by default.
    </td>
  </tr>
  <tr>
    <td>gaussdbToolPath</td>
    <td>
      Directory for storing MogDB system tools. This directory is used to store
      tools for mutual trust. This directory must meet the following
      requirements:<br />- The disk space is greater than 100 MB. <br />- This
      directory cannot be changed and is independent of other directories
      required by the database.<br />
      This directory is optional. If this parameter is not specified,
      <b>/opt/mogdb/tools</b> is specified as the database system tool directory
      by default during MogDB installation.
    </td>
  </tr>
  <tr>
    <td>corePath</td>
    <td>Directory for storing the MogDB core file.</td>
  </tr>
  <tr>
    <td>enable_dcf</td>
    <td>
      Determines whether to enable the DCF mode.
    </td>
  </tr>
   <tr>
    <td>dcf_config</td>
    <td>
      DCF startup node information, which is configured when the DCF mode is enabled.
    </td>
  </tr>
</table>

### Configuring the Basic Host Information

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE**:
>
> - Each server must contain the following information. The following uses **node1** as an example.
> - The information in bold is only an example. You can replace it as required. Each line of information is commented out.

```xml
<!-- Information about node deployment on each server-->
<DEVICELIST>
<!-- Information about the node deployment on node1 -->
<DEVICE sn="node1_hostname">
<!-- Host name of node1 -->
<PARAM name="name" value="node1_hostname"/>
<!-- AZ where node1 is located and AZ priority -->
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<!-- IP address of node1. If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address. -->
<PARAM name="backIp1" value="192.168.0.1"/>
<PARAM name="sshIp1" value="192.168.0.1"/>
<!-- Specifies whether node1 is a cascaded standby node. on indicates that the instance is a cascaded standby node. In addition, the cascaded standby node must be configured with a standby node in the same AZ.-->
<PARAM name="cascadeRole" value="on"/>
```

**Table 2** Parameter Description

| Parameter  | Description                                                  |
| :--------- | :----------------------------------------------------------- |
| name       | Host name.                                                   |
| azName     | The value is a character string that cannot contain special characters, for example, AZ1, AZ2, and AZ3. |
| azPriority | AZ priority.                                                 |
| backIp1    | Intranet IP address of the host in the backend storage network. All the MogDB hosts communicate with each other on this network. |
| sshIp1     | External IP address of the SSH reliable channel. If no external network is used, you can leave it empty or set it to the value of **backIp1**. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE**:
>
> Only one IP address can be configured for all IP parameters (including **backIp**, **sshIp**, and **listenIp**) in the configuration file. Excessive IP addresses are ignored.
> For example, you can configure **backIp1** and **backIp2** in the XML configuration file as follows:
> When the file is parsed, only **backIp1** takes effect.
>
> ```xml
> <PARAM name="backIp1" value="192.168.0.1"/>
> <PARAM name="backIp2" value="192.168.0.2"/>
> ```

### Configuring Primary Database Node Information

The information in bold is only an example. You can replace it as required. Each line of information is commented out.

**Configure the following information for the primary database node:**

```xml
<!--DBnode-->
<PARAM name="dataNum" value="1"/>
<!--Database node port number-->
<PARAM name="dataPortBase" value=""/>
<!--Listening IP address of the database node-->
<PARAM name="dataListenIp1" value="172.31.13.124,172.31.6.198"/>
<!--Data directory on the primary database node and data directories of standby nodes-->
<PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
<!--Xlog directories on the primary database node and on the standby database node-->
<PARAM name="dataNodeXlogPath1" value="/home/omm/gauss_xlog,/home/omm/gauss_xlog "/>
<!--Number of nodes for which the synchronization mode is set on the database node-->
<PARAM name="dataNode1_syncNum" value="0"/>
```

**Table 3** Parameter Description

| Parameter         | Description                                                  |
| :---------------- | :----------------------------------------------------------- |
| dataNum           | Number of database nodes to be deployed on the current host. |
| dataPortBase      | Basic port number of the database node. The default value is **40000**. |
| dataListenIp1     | Listening IP address. If this parameter is not set, the value is determined based on **backIp1** of the corresponding host.<br />The first and second IP addresses are the IP addresses of the hosts where the primary and standby nodes reside, respectively. |
| dataNode1         | Directory for storing data of the database nodes on the current host. It is a data storage directory, and should be on a data disk. |
| dataNodeXlogPath1 | Path for storing Xlogs in the current database. This parameter is optional. This directory is used to store database Xlogs and can only be an absolute path. If this parameter is not specified, the log file is stored in the **pg_xlog** directory of the data directory by default. |
| dataNode1_syncNum | Number of nodes in synchronous mode in the current database. This parameter is optional. The value ranges from 0 to the number of standby database nodes. |

### Configuring the CM Server (Primary/Non-Primary) Information

**Configure the following information for the non-primary CMS nodes:**

```xml
<!-- cm -->
<PARAM name="cmServerPortStandby" value="25500"/>
<PARAM name="cmDir" value="/opt/mogdb/data/cm"/>
```

**Table 4** Parameter Description

| Parameter           | Description                                                  |
| :------------------ | :----------------------------------------------------------- |
| cmServerPortBase    | Port number of the primary CM server. The default value is **5000**. |
| cmServerPortStandby | Port number of the standby CM server. The default value is **5500**. |
| cmServerListenIp1   | IP address used by the CM server to listen to the CM agent connection requests or the database administrator's cluster management requests. |
| cmServerHaIp1       | IP addresses for communication between the primary and standby CM servers.<br />In the parameter value, the first and second IP addresses are the IP addresses of the hosts where the primary and standby CM servers reside, respectively. If this parameter is not set, the value is determined based on backIp1 of the hosts where the primary and standby CM servers reside. |
| cmDir               | CM data file directory. The directory is used to store the data files and parameter files used by the CM server and CM agent.<br />This parameter must be set on every host in the cluster. |

**Table 5** Directory Description

| Directory       | Description                                                  |
| :-------------- | :----------------------------------------------------------- |
| /opt/mogdb/data | Directory that stores instance data of the cluster. It is a data storage directory, and should be on a data disk. This directory must meet the following requirements: The disk space is planned according to the actual service data volume. Directories for storing data of each instance are independent from each other. [Table 6](#6) lists planning for the data directory of each instance in this document. |

**Table 6** Database instance data directories <a id="6"> </a>

| Instance Name | Instance Data Directory                                      |
| :------------ | :----------------------------------------------------------- |
| CM            | /opt/mogdb/data/cm                                           |
| Primary DN    | /opt/mogdb/data/masterX <br />In the directory, *X* is a consecutive positive integer starting from 1, identifying the Xth DN on the current host. |
| Standby DN    | /opt/mogdb/data/slaveX<br />In the directory, *X* is a consecutive positive integer starting from 1, identifying the Xth standby DN on the current host. |

### Example

#### Configuration File for Single-Instance Deployment

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- Overall information about MogDB -->
    <CLUSTER>
        <!-- Database name -->
        <PARAM name="clusterName" value="dbCluster" />
        <!-- Name of the host where the database is located -->
        <PARAM name="nodeNames" value="node1_hostname" />
        <!-- Database installation directory-->
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <!-- Log directory-->
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <!-- Temporary file directory-->
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp" />
        <!-- Database tool directory-->
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <!-- Directory for storing database core files-->
        <PARAM name="corePath" value="/opt/mogdb/corefile" />
        <!-- IP address of the node is consistent with the name of database node in the list. -->
        <PARAM name="backIp1s" value="192.168.0.1"/>
    </CLUSTER>
    <!-- Information about node deployment on each server -->
    <DEVICELIST>
        <!-- Information about node deployment on node1 -->
        <DEVICE sn="node1_hostname">
            <!-- Host name of node1 -->
            <PARAM name="name" value="node1_hostname"/>
            <!-- AZ where node1 is located and AZ priority -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address.-->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>

      <!--dbnode-->
      <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

> Note: The host name and IP address need to be replaced. For other parameters, retain the default values.

#### Configuration File for One Primary and One Standby

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- Overall information about MogDB -->
    <CLUSTER>
        <!-- Database name -->
        <PARAM name="clusterName" value="Cluster_template" />
        <!-- Name of the host where the database is located -->
        <PARAM name="nodeNames" value="node1_hostname,node2_hostname" />
        <!-- Database installation directory-->
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <!-- Log directory-->
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <!-- Temporary file directory-->
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
        <!-- Database tool directory-->
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <!-- Directory for storing database core files-->
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <!-- IP address of the node is consistent with the name of database node in the list. -->
        <PARAM name="backIp1s" value="192.168.0.1,192.168.0.2"/>
    </CLUSTER>
    <!-- Information about node deployment on each server -->
    <DEVICELIST>
        <!-- Information about node deployment on node1 -->
        <DEVICE sn="node1_hostname">
            <!-- Host name of node1 -->
            <PARAM name="name" value="node1_hostname"/>
            <!-- AZ where node1 is located and AZ priority -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>

      <!--dn-->
            <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>

        <!-- Information about node deployment on node2 and name indicating the host name -->
        <DEVICE sn="node2_hostname">
            <!-- Host name of node2 -->
            <PARAM name="name" value="node2_hostname"/>
            <!-- AZ where node2 is located and AZ priority -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for node2, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="192.168.0.2"/>
            <PARAM name="sshIp1" value="192.168.0.2"/>
  </DEVICE>
    </DEVICELIST>
</ROOT>
```

> Note: The host name and IP address need to be replaced. For other parameters, retain the default values.

#### Configuration File for One Primary and Two Standbys

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- Overall information about MogDB -->
    <CLUSTER>
        <PARAM name="clusterName" value="Cluster_template" />
        <PARAM name="nodeNames" value="node1_hostname,node2_hostname,node3_hostname" />

        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="backIp1s" value="192.168.0.1,192.168.0.2,192.168.0.3"/>

    </CLUSTER>
    <!-- Information about node deployment on each server -->
    <DEVICELIST>
        <!-- Information about node deployment on node1 -->
        <DEVICE sn="node1_hostname">
            <PARAM name="name" value="node1_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for node2, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>
            <!--Information about node deployment on CM server-->
            <PARAM name="cmsNum" value="1"/>
            <PARAM name="cmServerPortBase" value="15000"/>
            <PARAM name="cmServerListenIp1" value="192.168.0.1,192.168.0.2,192.168.0.3"/>
            <PARAM name="cmServerHaIp1" value="192.168.0.1,192.168.0.2,192.168.0.3"/>
            <PARAM name="cmServerlevel" value="1"/>
            <PARAM name="cmServerRelation" value="node1_hostname,node2_hostname,node3_hostname"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
      <!--dn-->
            <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn,node3_hostname,/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>

        <!-- Information about node deployment on node2 and name indicating the host name -->
        <DEVICE sn="node2_hostname">
            <PARAM name="name" value="node2_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for node2, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="192.168.0.2"/>
            <PARAM name="sshIp1" value="192.168.0.2"/>
            <!-- cm -->
            <PARAM name="cmServerPortStandby" value="15000"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
  </DEVICE>

        <!-- Information about node deployment on node3 and name indicating the host name -->
        <DEVICE sn="node3_hostname">
            <PARAM name="name" value="node3_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for node2, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="192.168.0.3"/>
            <PARAM name="sshIp1" value="192.168.0.3"/>
            <!-- cm -->
            <PARAM name="cmServerPortStandby" value="15000"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
  </DEVICE>
    </DEVICELIST>
</ROOT>
```

> Note: The host name and IP address need to be replaced. For other parameters, retain the default values.  

<br/>

## Downloading an Installation Package

Before installation, you can visit [MogDB download page](https://www.mogdb.io/en/downloads/mogdb/) to download the installation package applicable to your current operating system and CPU. The following takes the CentOS system as an example. Each installation package and its functions are introduced in the following table.

| **Package**                           | **Function**                                                 |
| ------------------------------------- | ------------------------------------------------------------ |
| MogDB-x.x.x-CentOS-x86_64.tar     | Contains all files of all installation packages              |
| MogDB-x.x.x-CentOS-64bit.tar.gz      | (Primary/Standby) MogDB primary/standby (Datanode). <br />Stores business data, performs data query tasks, and returns execution results to the client. <br />MogDB instances include primary instance and standby instance, currently supports at most one primary node and eight standby nodes. It is recommended to deploy the primary and standby MogDB instances on different physical servers. |
| MogDB-x.x.x-CentOS-64bit.sha256       | Hash value of sha256 of MogDB-x.x.x-CentOS-64bit.tar.gz     |
| MogDB-x.x.x-CentOS-64bit-Libpq.tar.gz | Driver for C program to access MogDB database                |
| MogDB-x.x.x-CentOS-64bit-om.tar.gz    | Operation and maintenance management module (Operation Manager). <br />Provides management interfaces and tools for daily database operation and maintenance and configuration management. |
| MogDB-x.x.x-CentOS-64bit-om.sha256    | Hash value of sha256 of MogDB-x.x.x-CentOS-64bit-om.tar.gz   |
| MogDB-x.x.x-CentOS-64bit-tools.tar.gz | Client Driver. <br />Receives the access request from the application and returns the execution result to the application.<br /> The client driver is used for communicating with the MogDB instance, sending application SQL commands, and receiving the execution results of the MogDB instance. |
| upgrade_sql.tar.gz                    | SQL commands required for the upgrade                        |
| upgrade_sql.sha256                    | Hash value of sha256 of upgrade_sql.tar.gz                   |

<br/>

## Initializing the Environment

This section takes the CentOS OS 7.6 as an example to describe the environment initialization process.

**Procedure**

1. Run the following command as user root to install the required environment dependency packages.

   ```
   yum install -y libaio-devel gcc gcc-c++ zlib-devel expect
   ```

2. Log in to any host where the MogDB is to be installed as user **root** and create a directory for storing the installation package as planned.

   ```
   mkdir -p /opt/software/mogdb
   chmod 755 -R /opt/software
   ```

3. Upload the software package **MogDB-x.x.x-CentOS-x86_64.tar** and the configuration file **clusterconfig.xml** to the created directory. If the configuration file is not created, run the `vi clusterconfig.xml` command to create and edit the configuration file.

   ```
   cd /opt/software/mogdb
   ```

   > Note:
   > In environments that allow access to external networks, the installation package can be downloaded directly via the `wget` command. For example:
   >
   > ```
   > wget -c https://cdn-mogdb.enmotech.com/mogdb-media/3.0.1/MogDB-3.0.1-CentOS-x86_64.tar
   > ```

4. Go to the directory for storing the uploaded software package and decompress the following packages in sequence.

   ```
   tar -xvf  MogDB-x.x.x-CentOS-x86_64.tar
   tar -xvf  MogDB-x.x.x-CentOS-64bit-om.tar.gz
   ```

5. Go to the directory for storing tool scripts.

   ```
   cd /opt/software/mogdb/script
   ```

6. To ensure that the openssl version is correct, upload the lib library in the software package before pre-installation. Run the following command. **{packagePath}** indicates the directory where the software package is located. The value is **/opt/software/mogdb** in this example.

   ```
   export LD_LIBRARY_PATH={packagePath}/script/gspylib/clib:$LD_LIBRARY_PATH
   ```

7. To ensure successful installation, run the following command to check whether the values of **hostname** and **/etc/hostname** are the same. During the pre-installation, **hostname** will be checked.

   ```
   hostname
   vi /etc/hostname
   ```

8. If the openEuler OS is used, comment out **sysctl -w vm.min_free_kbytes=112640 &> /dev/null** in the **performance.sh** file.

   ```
   vi /etc/profile.d/performance.sh
   ```

9. Install python3 for the CentOS.

   Note: For standard installation in the CentOS system, you must install Python 3.6.x. Python of a higher version is not supported currently.

   ```
   wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
   mkdir -p /usr/local/python3
   tar -zxvf Python-3.6.5.tgz
   cd Python-3.6.5
   ./configure --prefix=/usr/local/python3 --enable-shared CFLAGS=-fPIC && make && make install
   ln -s /usr/local/python3/bin/python3 /usr/bin/python3
   ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
   ```

   > Note:
   >
   > - If the error "zipimport.ZipImportError: can't decompress data; zlib not available" is reported, run the **yum -y install zlib*** command and install again.
   >
   > - If an error similar to "find:'/run/user/1000/gvfs':Permission denied" is reported, it can be ignored.
   >

<br/>

## Initializing the Script

```
/opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml

Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Distributing package.
Begin to distribute package to tool path.
Successfully distribute package to tool path.
Begin to distribute package to package path.
Successfully distribute package to package path.
Successfully distributed package.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Setting SCTP service.
Successfully set SCTP service.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting Cgroup.
Successfully set Cgroup.
Set ARM Optimization.
Successfully set ARM Optimization.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```

> Note:
>
> - During the running, the system will ask whether to create the user omm, you need to enter **yes**. Additionally, you need to enter related passwords and make sure that the password complexity requirement is met, including uppercase and lower case letters, characters, and digits, such as **Enmo@123**.
>
> - If an error similar to the following is displayed, the version of Python is high.
>
>   ```
>   [root@localhost mogdb]# /opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /mogdb/etc/mogdb_clusterconfig.xml
>   Failed to execute cmd: rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib /psutil/_psutil_posix.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' . Error:
>   cp: cannot stat '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9': No such file or directory
>   ```

<br/>

## Performing the Installation

Modify the user and user group of the **lib** and **script** folder in the installation directory:

```
chown -R omm:dbgrp /opt/software/mogdb/lib
chown -R omm:dbgrp /opt/software/mogdb/script
```

Run the following commands in sequence to install MogDB:

```
su - omm
gs_install -X /opt/software/mogdb/clusterconfig.xml --gsinit-parameter="--locale=en_US.UTF-8" --gsinit-parameter="--encoding=UTF-8"

Parsing the configuration file.
Check preinstall on every node.
Successfully checked preinstall on every node.
Creating the backup directory.
Successfully created the backup directory.
begin deploy..
Installing the cluster.
begin prepare Install Cluster..
Checking the installation environment on all nodes.
begin install Cluster..
Installing applications on all nodes.
Successfully installed APP.
begin init Instance..
encrypt ciper and rand files for database.
Please enter password for database:
Please repeat for database:
begin to create CA cert files
The sslcert will be generated in /opt/mogdb/cluster/app/share/sslcert/om
Cluster installation is completed.
Configuring.
Deleting instances from all nodes.
Successfully deleted instances from all nodes.
Checking node configuration on all nodes.
Initializing instances on all nodes.
Updating instance configuration on all nodes.
Check consistence of memCheck and coresCheck on DN nodes.
Successful check consistence of memCheck and coresCheck on all nodes.
Configuring pg_hba on all nodes.
Configuration is completed.
Successfully started cluster.
Successfully installed application.
```

> Note: During the installation, the user needs to enter the password of the database as prompted. The password must meet the complexity requirement. To ensure that the user can use the database normally, make note of the database password.
>

<br/>

## Connecting To MogDB

After installation, you can connect to MogDB by running **gsql -d postgres -p 15400 -r** under the **omm** user, where **-p 15400** is the database port number. You need to replace it according to the actual situation. Additionally, you can run the **\copyright** command to check the copyright information.

```sql
[omm@hostname ~]$ gsql -d postgres -p 15400 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# \copyright
MogDB Kernel Database Management System
Copyright (c) Yunhe Enmo (Beijing) Information Technology Co., Ltd. Copyright © 2009-2022 , All rights reserved.
```

<br/>

## Modifying the Synchronous Node

By default, the standby database is an asynchronous database. If you need to configure it as a synchronous database, configure it as follows:

```
[omm@hostname ~]$ sed -i "/synchronous_standby_names/synchronous_standby_names = 'dn_6002'" /gaussdb/data/db1/postgresql.conf
[omm@hostname ~]$ gs_om -t restart
```

The value **dn_6002** is special, which is fixed. It is **dn_6001** for the primary database, **dn_6002** for the first standby database, and **dn_6003** for the second standby database in sequence.

'dn_6002,dn_6003'  indicates that the first and second standby databases are synchronous databases.

The data storing directory needs to be replaced as required.
