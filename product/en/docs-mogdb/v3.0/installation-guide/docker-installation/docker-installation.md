---
title: Container-based Installation
summary: Container-based Installation
author: Guo Huan
date: 2021-06-01
---

# Container-based Installation

This document introduces the installation of single instance and primary/standby instances in a MogDB docker container. MogDB container does not support the MogHA and OM tools, and currently supports at most one primary node and eight standby nodes. MogDB 2.1 container version supports the latest version of compat-tools and plugin functions.<br/>

In consideration of narrowing the container image size for quick download and deployment, it is recommended that the x86 and ARM-based OSs depended by MogDB running in a docker container will be Ubuntu and Debian since MogDB 3.0. <br/>

A MogDB container based on the x86-64 architecture runs in Ubuntu 18.04.<br/>

A MogDB container based on the ARM64 architecture runs in Debian 10.

<br/>

## Docker-based Deployment

### Single Instance Installation

#### Before You Start

The installation of a MogDB container is independent of the host OS. All OSs that can run a container enginer are supported, such as Linux, Windows, and macOS.

Before installing the MogDB container, you need to prepare the container runnning environment, such as Docker.

Docker is an open source application container engine based on Go and compliant with the Apache 2.0 protocol. Docker allows developers to package their applications and dependency packages into a lightweight, portable container that can then be distributed to any popular Linux machine and virtualized.

Docker Desktop download address: <https://www.docker.com/products/docker-desktop>

#### Installation Procedures

1. Run Docker.

2. Run the following command to obtain the latest version of the MogDB image file.

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
   ```
   
   > Note: MogDB image supports x86 and ARM architectures, and the system will automatically download the corresponding image according to the server architecture that initiates the command, no need to specify manually.

3. Run the following command to create a running directory for MogDB, using **mogdb** as an example below.

   ```bash
   mkdir /mogdb
   ```

4. Continue with the following command to create a new container, and name it **mogdb** to run a MogDB instance:

   ```bash
   docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123  -v /mogdb:/var/lib/mogdb  -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
   ```

   For Windows OS:
   - If Docker uses Hyper-V as the engine, run the following commands in sequence to create the logical object **mogdata** of the volume in Hyper-V, and then create the container:

      ```bash
      docker volume create mogdata

      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v mogdata:/var/lib/mogdb -p 15453:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
      ```

   - If Docker uses WSL 2 as the engine, run the following command to create the container:

      ```bash
      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v C:\mogdb:/var/lib/mogdb -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1
      ```

   > Note:
   >
   > - MogDB uses port 5432 in the container for listening by default. If you want to access the database from outside the container, you need to specify the `-p` parameter in `docker run` command. For example, the above command will allow access to the container database using port 15432.
   > - Once the container is deleted, all the data and configuration in the container will be lost. After re-running a container from the image, all the data is presented in the initialized state. So for the database container, in order to prevent the loss of data due to the demise of the container or corruption, the operation of persistent storage data is required. This is achieved by specifying the `-v` parameter in `docker run` command. For example, the above command will specify that all data files of MogDB will be stored under **/mogdb** of the host. In addition, If you use Podman, there will be a target path check. Therefore you need to create the target path in advance (step 4).

5. Open the docker terminal:

   ```bash
   docker exec -it mogdb bash
   ```

   Now, the single instance installation in a MogDB container is complete.

#### Using MogDB

After the installation is complete and enter the container, switch to omm user by running "su - omm", you can access the database via gsql and use MogDB properly.

```
root@384ac97543bd:/# su - omm
omm@384ac97543bd:~$ gsql -d postgres
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

<br/>

#### Environment Variables

To flexibly use an MogDB image, you can set additional parameters. In the future, more control parameters will be added. The current version supports the setting of the following variables.

**GS_PASSWORD**

This parameter is mandatory when the MogDB image is used. The value cannot be empty or undefined. This parameter specifies the passwords of superuser **omm** and test user **mogdb** of the MogDB database. During the MogDB installation, the superuser **omm** is created by default. This username cannot be changed. The test user **mogdb** is created in  **entrypoint.sh**.

The local trust mechanism is configured for the MogDB image. Therefore, no password is required for connecting to the database in the container. However, if you want to connect to the database from other hosts or containers, you need to enter the password.

The password must contain at least eight characters, including uppercase letters, lowercase letters, digits, and special characters ( \# ? ! @ $ % ^ & * \- ). **!$&**must be escaped using a backslash ().

**GS_NODENAME**

Specifies the database node name. The default value is  **mogdb**.

**GS_USERNAME**

Specifies the username for connecting to the database. The default value is  **mogdb**.

**GS_PORT**

Specifies the database port. The default value is  **5432**.

<br/>

### Primary/Standby Installation

#### Before You Begin

To implement primary/standby installation in a MogDB container, please complete the steps in the previous document **Single Instance Installation** first.

#### Procedures

1. Switch to user **root**.

   ```bash
   su - root
   ```

2. Create custom networks to create fixed IPs that are easy for containers to use.

   ```bash
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. Create a primary database container.

   ```bash
   docker run --name mogdb_master 
   --network myNetwork --ip 172.18.0.10 --privileged=true 
   --hostname mogdb_master --detach 
   --env GS_PORT=6432 
   --env OG_SUBNET=172.18.0.0/16 
   --env GS_PASSWORD=Enmo@1234 
   --env NODE_NAME=mogdb_master 
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" 
   swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.0.1 -M primary
   ```

4. Create a standby database container.

   ```bash
    docker run --name mogdb_slave_one 
    --network myNetwork --ip 172.18.0.11 --privileged=true 
    --hostname mogdb_slave_one --detach 
    --env GS_PORT=6432 
    --env OG_SUBNET=172.18.0.0/16 
    --env GS_PASSWORD=Enmotech@1234 
    --env NODE_NAME=mogdb_slave_one 
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" 
    enmotech/mogdb:3.0.1 -M standby
   ```

5. Query the status of the primary database.

   ```
   [root@localhost ~]# docker exec -it mogdb_master bash 
   [root@mogdb_master /] # su - omm
   [omm@mogdb_master ~] $ gs_ctl query -D /var/lib/mogdb/data/
   [2021-04-07 13:28:15.492][323][][gs_ctl] : gs_ctl query ,datadir is /var/lib/mogdb/data
    HA state:
         local_role                   : Primary
         static_connections           : 1
         db_state                     : Normal
         detail_information           : Normal
         
    Senders info:
         sender_pid                   : 286
         local_role                   : Primary
         peer_role                    : Standby
         peer_state                   : Normal
         state                        : Streaming
         sender_sent_location         : 0/40007D0
         sender_write_location        : 0/40007D0
         sender_flush_location        : 0/40007D0
         sender_replay_location       : 0/40007D0
         receiver_received_location   : 0/40007D0
         receiver_write_location      : 0/40007D0
         receiver_flush_location      : 0/40007D0
         receiver_replay_location     : 0/40007D0
         sync_percent                 : 100%
         sync_state                   : Sync
         sync_priority                : 1
         sync_most_available          : On
         channel                      : 172.18.0.10:6439-->172.18.0.11: 45640
   
    Receiver info: 
   No information
   ```

6. Query the status of the standby database.

   ```
   [root@localhost ~]# docker exec -it mogdb_slave_one bash
   [root@mogdb_slave_one /] # su - omm
   Last login: Wed Apr 7 13:32:23 UTC 2021 on pts/0
   [omm@mogdb_slave_one ~] $ gs_ctl query -D /var/lib/mogdb/data/
   [2021-04-07 14:46:15.492][393][][gs_ctl] : gs_ctl query ,datadir is /var/lib/mogdb/data
    HA state:
         local_role                   : Standby
         static_connections           : 1
         db_state                     : Normal
         detail_information           : Normal
         
    Senders info:
   No information
    Receiver info: 
         receiver_pid                 : 312
         local_role                   : Standby
         peer_role                    : Primary
         peer_state                   : Normal
         state                        : Normal
         sender_sent_location         : 0/4005D50
         sender_write_location        : 0/4005D50
         sender_flush_location        : 0/4005D50
         sender_replay_location       : 0/4005D50
         receiver_received_location   : 0/4005D50
         receiver_write_location      : 0/4005D50
         receiver_flush_location      : 0/4005D50
         receiver_replay_location     : 0/4005D50
         sync_percent                 : 100%
         channel                      : 172.18.0.11:45640<--172.18.0.10:6439
   ```
   
   > Note: As shown in the figures above, the senders info and the receiver info indicate that the status of the primary and standby databases is normal.

#### Read/Write Testing

Primary database write test:

```sql
[omm@mogdb_master ~] $ gsql -p6432
gsql ((MogDB 3.0.1 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

omm=# create table test(ID int);
CREATE TABLE
omm=# insert into test values(1);
INSERT 0 1
```

Standby database read test:

```sql
[omm@mogdb_slave_one ~] $ gsql -p6432
gsql ((MogDB 3.0.1 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

omm=# select * from test;
 id 
----
  1
(1 row)

omm=# delete from test;
ERROR: cannot execute DELETE in a read-only transaction
```

> Note: The result shows that the primary database is writable and the standby database is readable but not writable.

#### Switchover Testing

Switch **mogdb_slave_one** as the primary database and **mogdb_master** as the standby database.

Run the switchover command in **mogdb_slave_one**.

```
[omm@mogdb_slave_one ~] $ gs_ctl switchover -D /var/lib/mogdb/data/
[2021-04-07 15:31:45.246][428][][gs_ctl]: gs_ctl switchover ,datadir is /var/lib/mogdb/data
[2021-04-07 15:31:45.247][428][][gs_ctl]: switchover term(1)
[2021-04-07 15:31:45.251][428][][gs_ctl]: waiting for server to switchover......
[2021-04-07 15:31:45.325][428][][gs_ctl]: done
[2021-04-07 15:31:45.325][428][][gs_ctl]: switchover completed (/var/lib/mogdb/data)
```

Query the status of **mogdb_slave_one**.

```
[omm@mogdb_slave_one ~] $ gs_ctl query -D /var/lib/mogdb/data/
[2021-04-07 15:42:45.246][459][][gs_ctl]: gs_ctl query ,datadir is /var/lib/mogdb/data
 HA state:
      local_role                   : Primary
      static_connections           : 1
      db_state                     : Normal
      detail_information           : Normal
      
 Senders info:
      sender_pid                   : 442
      local_role                   : Primary
      peer_role                    : Standby
      peer_state                   : Normal
      state                        : Streaming
      sender_sent_location         : 0/40102B8
      sender_write_location        : 0/40102B8
      sender_flush_location        : 0/40102B8
      sender_replay_location       : 0/40102B8
      receiver_received_location   : 0/40102B8
      receiver_write_location      : 0/40102B8
      receiver_flush_location      : 0/40102B8
      receiver_replay_location     : 0/40102B8
      sync_percent                 : 100%
      sync_state                   : Sync
      sync_priority                : 1
      sync_most_available          : On
      channel                      : 172.18.0.11:6439-->172.18.0.10: 49696

 Receiver info: 
No information
```

Query the status of **mogdb_master**.

```
[omm@mogdb_master ~] $ gs_ctl query -D /var/lib/mogdb/data/
[2021-04-07 15:46:35.246][619][][gs_ctl]: gs_ctl query ,datadir is /var/lib/mogdb/data
 HA state:
      local_role                   : Standby
      static_connections           : 1
      db_state                     : Normal
      detail_information           : Normal
      
 Senders info:
No information
 Receiver info: 
      receiver_pid                 : 590
      local_role                   : Standby
      peer_role                    : Primary
      peer_state                   : Normal
      state                        : Normal
      sender_sent_location         : 0/4010718
      sender_write_location        : 0/4010718
      sender_flush_location        : 0/4010718
      sender_replay_location       : 0/4010718
      receiver_received_location   : 0/4010718
      receiver_write_location      : 0/4010718
      receiver_flush_location      : 0/4010718
      receiver_replay_location     : 0/4010718
      sync_percent                 : 100%
      channel                      : 172.18.0.10:49696<--172.18.0.11:6439
```

You can find that **mogdb_master** becomes the standby database and **mogdb_slave_one** becomes the primary database. The switchover is successful.

#### Read/Write Verification

Perform write validation on the primary database **mogdb_slave_one**.

```sql
[omm@mogdb_slave_one ~] $ gsql -p6432
gsql ((MogDB 3.0.1 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

omm=# select * from test;
 id 
----
  1
(1 row)

omm=# insert into test values(2);
INSERT 0 1
```

Perform read validation on the standby database **mogdb_master**.

```sql
[omm@mogdb_master ~] $ gsql -p6432
gsql ((MogDB 3.0.1 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

omm=# select * from test;
 id 
----
  1
  2
(2 row)

omm=# delete from test;
ERROR: cannot execute DELETE in a read-only transaction
```

As you can see, the original standby database is writable after being switched as the primary database, and the original primary database is readable but not writable after being switched as the standby database.

<br/>

## Kubernetes-based Deployment

MogDB 2.1 and later supports Kubernetes-based deployment.

- x86
  
  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.0.1/k8s_amd.yaml
  pod/mogdb created
  ```

- arm

  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.0.1/k8s_arm.yaml
  pod/mogdb created
  ```

- Kubernetes connect the pod

  Connect to MogDB through the gsql command in the pod.

  ```
  $ kubectl exec -it mogdb -- bash
  root@mogdb:/# su - omm
  omm@mogdb:~$ gsql -d postgres
  gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
  Non-SSL connection (SSL connection is recommended when requiring high-security)
  Type "help" for help.
  
  MogDB=#
  ```

<br/>

## What's Next

MogDB container does not support MogHA and OM tools. It is only used for testing, and currently supports at most one primary node and eight standby nodes. MogDB Enterprise Edition includes MogHA component. The basic functions of the MogDB container and Enterprise Edition are identical. It is recommended that MogDB Enterprise Edition is used in the production environment.
