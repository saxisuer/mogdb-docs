---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-04-26
---

# Overview

Java Database Connectivity (JDBC) is a Java API for running SQL statements. It provides unified access interfaces for different relational databases, based on which applications process data. The MogDB library supports JDBC 4.0 and requires JDK 1.8 for code compiling. It does not support JDBC-ODBC bridge.
