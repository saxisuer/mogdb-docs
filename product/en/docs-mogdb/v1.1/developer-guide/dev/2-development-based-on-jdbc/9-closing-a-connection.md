---
title: Closing a Connection
summary: Closing a Connection
author: Guo Huan
date: 2021-04-26
---

# Closing a Connection

After you complete required data operations in the database, close the database connection.

Call the close method to close the connection, for example, **Connection conn = null; conn.close()**.
