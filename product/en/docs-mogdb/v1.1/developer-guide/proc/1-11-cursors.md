---
title: Cursors
summary: Cursors
author: Guo Huan
date: 2021-03-04
---

# Cursors

## Overview

To process SQL statements, the stored procedure process assigns a memory segment to store context association. Cursors are handles or pointers pointing to context regions. With cursors, stored procedures can control alterations in context regions.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If JDBC is used to call a stored procedure whose returned value is a cursor, the returned cursor cannot be used.

Cursors are classified into explicit cursors and implicit cursors. [Table 1](#Table 1) shows the usage conditions of explicit and implicit cursors for different SQL statements.

**Table 1** Cursor usage conditions<a id="Table 1"> </a>

| SQL Statement                             | Cursor               |
| :---------------------------------------- | :------------------- |
| Non-query statements                      | Implicit             |
| Query statements with single-line results | Implicit or explicit |
| Query statements with multi-line results  | Explicit             |

## Explicit Cursor

An explicit cursor is used to process query statements, particularly when query results are multiple records.

**Procedure**

An explicit cursor performs the following six PL/SQL steps to process query statements:

1. **Define a static cursor:** Define a cursor name and its corresponding **SELECT** statement.

   [Figure 1](#static_cursor_define) shows the syntax diagram for defining a static cursor.

   **Figure 1** static_cursor_define::=<a id="static_cursor_define"> </a>

   ![static_cursor_define](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-1.jpg)

   Parameter description:

   - **cursor_name**: defines a cursor name.

   - **parameter**: specifies cursor parameters. Only input parameters are allowed in the following format:

     ```
     parameter_name datatype
     ```

   - **select_statement**: specifies a query statement.

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   > The system automatically determines whether the cursor can be used for backward fetches based on the execution plan.

   **Define a dynamic cursor:** Define a **ref** cursor, which means that the cursor can be opened dynamically by a set of static SQL statements. First define the type of the **ref** cursor first and then the cursor variable of this cursor type. Dynamically bind a **SELECT** statement through **OPEN FOR** when the cursor is opened.

   [Figure 2](#cursor_typename) and [Figure 3](#dynamic_cursor_define) show the syntax diagrams for defining a dynamic cursor.

   **Figure 2** cursor_typename::=<a id="cursor_typename"> </a>

   ![cursor_typename](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-2.png)

   **Figure 3** dynamic_cursor_define::=<a id="dynamic_cursor_define"> </a>

   ![dynamic_cursor_define](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-3.png)

2. **Open the static cursor:** Execute the **SELECT** statement corresponding to the cursor. The query result is placed in the workspace and the pointer directs to the head of the workspace to identify the cursor result set. If the cursor query statement carries the **FOR UPDATE** option, the **OPEN** statement locks the data row corresponding to the cursor result set in the database table.

   [Figure 4](#open_static_cursor) shows the syntax diagram for opening a static cursor.

   **Figure 4** open_static_cursor::=<a id="open_static_cursor"> </a>

   ![open_static_cursor](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-4.png)

   **Open the dynamic cursor:** Use the **OPEN FOR** statement to open the dynamic cursor and the SQL statement is dynamically bound.

   [Figure 5](#open_dynamic_cursor) shows the syntax diagrams for opening a dynamic cursor.

   **Figure 5** open_dynamic_cursor::=<a id="open_dynamic_cursor"> </a>

   ![open_dynamic_cursor](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-5.png)

   A PL/SQL program cannot use the OPEN statement to repeatedly open a cursor.

3. **Fetch cursor data**: Retrieve data rows in the result set and place them in specified output variables.

   [Figure 6](#fetch_cursor) shows the syntax diagrams for fetching cursor data.

   **Figure 6** fetch_cursor::=<a id="fetch_cursor"> </a>

   ![fetch_cursor](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-6.png)

4. Process the record.

5. Continue to process until the active set has no record.

6. **Close the cursor**: When fetching and finishing the data in the cursor result set, close the cursor immediately to release system resources used by the cursor and invalidate the workspace of the cursor so that the **FETCH** statement cannot be used to fetch data any more. A closed cursor can be reopened by an OPEN statement.

   [Figure 7](#close_cursor) shows the syntax diagram for closing a cursor.

   **Figure 7** close_cursor::=<a id="close_cursor"> </a>

   ![close_cursor](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-7.jpg)

**Attribute**

Cursor attributes are used to control program procedures or know program status. When a DML statement is executed, the PL/SQL opens a built-in cursor and processes its result. A cursor is a memory segment for maintaining query results. It is opened when a DML statement is executed and closed when the execution is finished. An explicit cursor has the following attributes:

- **%FOUND** attribute: returns **TRUE** if the last fetch returns a row.
- **%NOTFOUND** attribute: works opposite to the **%FOUND** attribute.
- **%ISOPEN** attribute: returns **TRUE** if the cursor has been opened.
- **%ROWCOUNT** attribute: returns the number of records fetched from the cursor.

## Implicit Cursor

Implicit cursors are automatically set by the system for non-query statements such as modify or delete operations, along with their workspace. Implicit cursors are named **SQL**, which is defined by the system.

**Overview**

Implicit cursor operations, such as definition, open, value-grant, and close operations, are automatically performed by the system and do not need users to process. Users can use only attributes related to implicit cursors to complete operations. In workspace of implicit cursors, the data of the latest SQL statement is stored and is not related to explicit cursors defined by users.

Format call:**SQL%**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> **INSERT**, **UPDATE**, **DELETE**, and **SELECT** statements do not need defined cursors.

**Attributes**

An implicit cursor has the following attributes:

- **SQL%FOUND**: Boolean attribute, which returns **TRUE** if the last fetch returns a row.
- **SQL%NOTFOUND**: Boolean attribute, which works opposite to the **SQL%FOUND** attribute.
- **SQL%ROWCOUNT**: numeric attribute, which returns the number of records fetched from the cursor.
- **SQL%ISOPEN**: Boolean attribute, whose value is always **FALSE**. Close implicit cursors immediately after an SQL statement is run.

**Examples**

```sql
-- Delete all employees in a department from the hr.staffs table. If the department has no employees, delete the department from the hr.sections table.
CREATE OR REPLACE PROCEDURE proc_cursor3()
AS
    DECLARE
    V_DEPTNO NUMBER(4) := 100;
    BEGIN
        DELETE FROM hr.staffs WHERE section_ID = V_DEPTNO;
        -- Proceed based on cursor status.
        IF SQL%NOTFOUND THEN
        DELETE FROM hr.sections WHERE section_ID = V_DEPTNO;
        END IF;
    END;
/

CALL proc_cursor3();

-- Delete the stored procedure and the temporary table.
DROP PROCEDURE proc_cursor3;
```

## Cursor Loop

Use of cursors in WHILE and LOOP statements is called a cursor loop. Generally, OPEN, FETCH, and CLOSE statements are involved in this kind of loop. The following describes a loop that simplifies a cursor loop without the need for these operations. This kind of loop is applicable to a static cursor loop, without executing four steps about a static cursor.

**Syntax**

[Figure 8](#FOR_AS_loop) shows the syntax diagram of the **FOR AS** loop.

**Figure 8** FOR_AS_loop::=<a id="FOR_AS_loop"> </a>

![for_as_loop](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/cursors-8.png)

**Precautions**

- The **UPDATE** operation for the queried table is not allowed in the loop statement.
- The variable loop_name is automatically defined and is valid only in this loop. Its type is the same as that in the select_statement query result. The value of **loop_name** is the query result of **select_statement**.
- The **%FOUND**, **%NOTFOUND**, and **%ROWCOUNT** attributes access the same internal variable in MogDB. Transactions and the anonymous block do not support multiple cursor accesses at the same time.
