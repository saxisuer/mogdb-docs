---
title: Stored Procedure
summary: Stored Procedure
author: Guo Huan
date: 2021-03-04
---

# Stored Procedure

In MogDB, business rules and logics are saved as stored procedures.

A stored procedure is a combination of SQL and PL/SQL. Stored procedures can move the code that executes business rules from applications to databases. Therefore, the code storage can be used by multiple programs at a time.

For details about how to create and call a stored procedure, see CREATE PROCEDURE.

The application methods for PL/pgSQL functions mentioned in PL/pgSQL Functions are similar to those for stored procedures. Unless otherwise specified, the following sections apply to stored procedures and PL/pgSQL functions.
