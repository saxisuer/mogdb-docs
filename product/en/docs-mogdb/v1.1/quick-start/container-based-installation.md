---
title: Container-based MogDB Installation
summary: Container-based MogDB Installation
author: Guo Huan
date: 2021-04-01
---

# Container-based MogDB Installation

This document introduces the installation of MogDB in a container. MogDB container does not support the MogHA and OM tools, and currently supports at most one primary node and one standby node.

## Before You Begin

The architectures and operating system versions supported by MogDB are as follows:

- x86-64 CentOS 7.6
- ARM64 openEuler 20.03 LTS

Before installing the MogDB container, you need to install Docker.

Docker is an open source application container engine based on Go and compliant with the Apache 2.0 protocol. Docker allows developers to package their applications and dependency packages into a lightweight, portable container that can then be distributed to any popular Linux machine and virtualized.

Please visit [Docker](https://docs.docker.com/) website and use the appropriate method to complete the installation according to your operating system.

## Installation Procedures

1. Run Docker.

2. Run one of the following commands according to your system architecture to obtain the latest version of the MogDB image file.

   x86-64:

   ```
   docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   ARM64:

   ```
   docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_arm
   ```

   ![c3XvSP.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-12.png)

3. Run the following command to create a running directory for MogDB, using **mymogdb** as an example below.

   ```
   mkdir /mymogdb
   ```

4. Continue with the following command to create a new container, and name it **mymogdb** to run a MogDB instance:

   ```
   docker run --name mymogdb  -d -e GS_PASSWORD=Secretpassword@123   -v /mymogdb:/var/lib/opengauss  -p 15432:5432  swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   > Note: MogDB uses port 5432 in the container for listening by default. If you want to access the database from outside the container, you need to specify the `-p` parameter in `docker run` command. For example, the above command will allow access to the container database using port 15432. Once the container is deleted, all the data and configuration in the container will be lost. After re-running a container from the image, all the data is presented in the initialized state. So for the database container, in order to prevent the loss of data due to the demise of the container or corruption, the operation of persistent storage data is required. This is achieved by specifying the `-v` parameter in `docker run` command. For example, the above command will specify that all data files of MogDB will be stored under **/mymogdb** of the host. In addition, If you use Podman, there will be a target path check. Therefore you need to create the target path in advance (step 4).

5. Execute bash in the **mymogdb** container via the **exec** command:

   ```
   docker exec -it mymogdb /bin/bash
   ```

   Now, the installation of MogDB in the container is complete.

## Using MogDB

Once the installation is complete, switch to the **omm** user by running the following command, and then you can access the database via gsql and use MogDB properly.

```
su - omm
gsql -d postgres
```

![c3jMTJ.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-13.png)

## Environment Variables

To flexibly use an MogDB image, you can set additional parameters. In the future, more control parameters will be added. The current version supports the setting of the following variables:

**GS_PASSWORD**

This parameter is mandatory when the MogDB image is used. The value cannot be empty or undefined. This parameter specifies the passwords of superuser **omm** and test user **mogdb** of the MogDB database. During the MogDB installation, the superuser **omm** is created by default. This username cannot be changed. The test user **mogdb** is created in  **entrypoint.sh**.

The local trust mechanism is configured for the MogDB image. Therefore, no password is required for connecting to the database in the container. However, if you want to connect to the database from other hosts or containers, you need to enter the password.

The password must contain at least eight characters, including uppercase letters, lowercase letters, digits, and special characters ( \# ? ! @ $ % ^ & * \- ). **!$&**must be escaped using a backslash ().

**GS_NODENAME**

Specifies the database node name. The default value is  **mogdb**.

**GS_USERNAME**

Specifies the username for connecting to the database. The default value is  **mogdb**.

**GS_PORT**

Specifies the database port. The default value is  **5432**.

<br/>

## What's Next

MogDB container does not support MogHA and OM tools. It is only used for testing, and currently supports at most one primary node and one standby node. MogDB Enterprise Edition includes MogHA component which supports one primary node and eight standbys at most. The basic functions of the MogDB container and Enterprise Edition are identical. It is recommended that MogDB Enterprise Edition is used in the production environment.
