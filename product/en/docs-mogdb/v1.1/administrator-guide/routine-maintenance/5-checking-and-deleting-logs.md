---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# Checking and Deleting Logs

You are advised to check OS logs and database run logs monthly for monitoring system status and troubleshooting, and to delete database run logs monthly for saving disk space.

## Checking OS Logs

You are advised to monthly check OS logs to detect and prevent potential OS problems.

**Procedure**

Run the following command to check OS log files:

```
vim /var/log/messages
```

(Pay attention to words like **kernel**, **error**, and **fatal** in logs generated within the last month and handle the problems based on the alarm information.)

## Checking MogDB Run Logs

A database can still run when errors occur during the execution of some operations. However, data may be inconsistent before and after the error occurrences. Therefore, you are advised to monthly check MogDB run logs to detect potential problems in time.

**Prerequisites**

- The host used for collecting logs is running properly, and the network connection is normal. Database installation users trust each other.
- An OS tool (for example, **gstack**) that the log collection tool requires has been installed. If it is not installed, an error message is displayed, and this collection item is skipped.

**Procedure**

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to collect database logs:<a id="2"> </a>

   ```
   gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59"
   ```

   In the command, **20160616 01:01** indicates the start time of the log and **20160616 23:59** indicates the end time of the log.

3. Based on command output in [2](#2), access the related log collection directory, decompress collected database logs, and check these logs.

   Assume that collected logs are stored in **/opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158.tar.gz**.

   ```
   tar -xvzf /opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158.tar.gz
   cd /opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158
   ```

**Examples**

- Run the **gs_collector** command together with parameters **-begin-time** and **-end-time**:

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59"
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected  files
  All results are stored in /tmp/gaussdba_mppdb/collector_20160616_175615.tar.gz.
  ```

- Run the **gs_collector** command together with parameters **-begin-time**, **-end-time**, and **-h**:

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -h plat2
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected  files
  All results are stored in /tmp/gaussdba_mppdb/collector_20160616_190225.tar.gz.
  ```

- Run the **gs_collector** command together with parameters **-begin-time**, **-end-time**, and **-f**:

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -f /opt/software/mogdb/output
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected  files
  All results are stored in /opt/software/mogdb/output/collector_20160616_190511.tar.gz.
  ```

- Run the **gs_collector** command together with parameters **-begin-time**, **-end-time**, and **-keyword**:

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" --keyword="os"
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected files.
  All results are stored in /tmp/gaussdba_mppdb/collector_20160616_190836.tar.gz.
  ```

- Run the **gs_collector** command together with parameters **-begin-time**, **-end-time**, and **-o**:

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -o /opt/software/mogdb/output
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected files.
  All results are stored in /opt/software/mogdb/output/collector_20160726_113711.tar.gz.
  ```

- Run the **gs_collector** command together with parameters **-begin-time**, **-end-time**, and **-l** (the file name extension must be .log):

  ```bash
  gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -l /opt/software/mogdb/logfile.log
  ```

  If information similar to the following is displayed, the logs have been archived:

  ```
  Successfully collected files.
  All results are stored in /opt/software/mogdb/output/collector_20160726_113711.tar.gz.
  ```

## Cleaning Run Logs

A large number of run logs will be generated during database running and occupy huge disk space. You are advised to delete expired run logs and retain logs generated within one month.

**Procedure**

1. Log in as the OS user **omm** to any host in the MogDB Kernel cluster.

2. Clean logs.

   a. Back up logs generated over one month ago to other disks.

   b. Access the directory where logs are stored.

   ```
   cd $GAUSSLOG
   ```

   c. Access the corresponding sub-directory and run the following command to delete logs generated one month ago:

   ```
   rm log name
   ```

   The naming convention of a log file is **mogdb-**year*-*month*-*day_**HHMMSS**.
