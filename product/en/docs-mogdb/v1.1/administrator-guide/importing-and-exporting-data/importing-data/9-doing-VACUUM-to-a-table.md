---
title: Doing VACUUM to a Table
summary: Doing VACUUM to a Table
author: Guo Huan
date: 2021-03-04
---

# Doing VACUUM to a Table

If a large number of rows were updated or deleted during import, run **VACUUM FULL** before **ANALYZE**. A large number of UPDATE and DELETE operations generate huge disk page fragments, which reduces query efficiency. **VACUUM FULL** can restore disk page fragments and return them to the OS.

Run the **VACUUM FULL** statement.

Do **VACUUM FULL** to the **product_info** table.

```sql
VACUUM FULL product_info
```

```sql
VACUUM
```
