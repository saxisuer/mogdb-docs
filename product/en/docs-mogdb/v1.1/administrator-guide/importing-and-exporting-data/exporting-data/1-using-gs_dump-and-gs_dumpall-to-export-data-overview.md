---
title: Using gs_dump and gs_dumpall to Export Data Overview
summary: Using gs_dump and gs_dumpall to Export Data Overview
author: Guo Huan
date: 2021-03-04
---

# Using gs_dump and gs_dumpall to Export Data Overview

MogDB provides **gs_dump** and **gs_dumpall** to export required database objects and related information. You can use a tool to import the exported data to a target database for database migration. **gs_dump** exports a single database or its objects. **gs_dumpall** exports all databases or global objects in MogDB. For details, see [Table 1](#Scenarios).

**Table 1** Scenarios<a id="Scenarios"> </a>

<table>
    <tr>
        <th>Application Scenario</th>
        <th>Export Granularity</th>
        <th>Export Format</th>
        <th>Import Method</th>
    </tr>
    <tr>
        <td rowspan="3">Exporting a single database</td>
        <td>Database-level export<br/>
            - Export full information of a cluster.<br/>You can use the exported information to create a database containing the same data as the current one.<br/>
            - Export all object definitions of a database, including the definitions of the database, functions, schemas, tables, indexes, and stored procedures.
            <br/>You can use the exported object definitions to quickly create a database that is the same as the current one, except that the new database does not have data.<br/>
            - Export data of a cluster.</td>
        <td rowspan="3">
            - Plaintext<br/>
            - Custom<br/>
            - Directory<br/>
            - .tar
        </td>
        <td rowspan="3">
            - For details about how to import data files in text format, see Using a gsql Meta-Command to Import Data.<br/>
            - For details about how to import data files in .tar, directory, or custom format, see Using gs_restore to Import Data.</td>
    </tr>
    <tr>
        <td>Schema-level export<br/>
            - Export full information of a schema.<br/>
            - Export data of a schema.<br/>
            - Export all object definitions of a schema, including the definitions of tables, stored procedures, and indexes.
        </td>
    </tr>
    <tr>
        <td>Table-level export<br/>
            - Export full information of a table.<br/>
            - Export data of a table.<br/>
            - Export the definition of a table.
        </td>
    </tr>
    <tr>
        <td rowspan="2">Exporting all databases</td>
        <td>Database-level export<br/>
            - Export full information of a cluster.<br/>You can use the exported information to create a host environment containing the same databases, global objects, and data as the current one.<br/>
            - Export all object definitions of a database, including the definitions of tablespaces, databases, functions, schemas, tables, indexes, and stored procedures.<br/>
            You can use the exported object definitions to quickly create a host environment that is the same as the current one, containing the same databases and tablespaces but no data.<br/>
            - Export data only.</td>
        <td rowspan="2">Plaintext</td>
        <td rowspan="2">For details about how to import data files, see Using a gsql Meta-Command to Import Data.</td>
    </tr>
    <tr>
        <td>Global object export<br/>
- Export tablespaces.<br/>
- Export roles.<br/>
- Export tablespaces and roles.</td>
    </tr>
</table>

**gs_dump** and **gs_dumpall** use **-U** to specify the user that performs the export. If the specified user does not have the required permissions, data cannot be exported. In this case, you can set **-role** in the **gs_dump** or **gs_dumpall** command to the role that has the permissions. Then, **gs_dump** or **gs_dumpall** uses the specified role to export data. See Table 1 for application scenarios and Data Export By a User Without Required Permissions for operation details.

**gs_dump** and **gs_dumpall** encrypt the exported data files. These files are decrypted before being imported. In this way, data disclosure is prevented, protecting database security.

When **gs_dump** or **gs_dumpall** is used to export data from a cluster, other users can still access (read and write) databases in MogDB.

**gs_dump** and **gs_dumpall** can export complete, consistent data. For example, if **gs_dump** is executed to export database A or **gs_dumpall** is executed to export all databases from MogDB at T1, data of database A or all databases in MogDB at that time point will be exported, and modifications on the databases after that time point will not be exported.

**Precautions**

- Do not modify an exported file or its content. Otherwise, restoration may fail.

- If there are more than 500,000 objects (data tables, views, and indexes) in a database, you are advised to use **gs_guc** to set the following parameters for database nodes. This operation is not required if the parameter values are greater than the recommended ones.

  ```
  gs_guc set -N all -I all -c 'max_prepared_transactions = 1000'
  gs_guc set -N all -I all -c 'max_locks_per_transaction = 512'
  ```

- For data consistency and integrity, **gs_dump** and **gs_dumpall** set a share lock for a table to dump. If a share lock has been set for the table in other transactions, **gs_dump** and **gs_dumpall** lock the table after it is released. If the table cannot be locked within the specified time, the dump fails. You can customize the timeout duration to wait for lock release by specifying the **-lock-wait-timeout** parameter.

- During an export, **gs_dumpall** reads all tables in a database. Therefore, you need to connect to the database as a MogDB administrator to export a complete file. When you use **gsql** to execute SQL scripts, cluster administrator permissions are also required to add users and user groups, and create databases.
