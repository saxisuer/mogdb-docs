---
title: Standby Node in the Need Repair (WAL) State
summary: Standby Node in the Need Repair (WAL) State
author: Guo Huan
date: 2021-05-24
---

# Standby Node in the **Need Repair (WAL)** State

## Symptom

The **Need Repair (WAL)** fault occurs on a standby node of the MogDB.

## Cause Analysis

The primary and standby DB instances are disconnected due to network faults or insufficient disk space. As a result, logs are not synchronized between the primary and standby DB instances, and the database cluster fails to start.

## Procedure

Run the **gs_ctl build -D** command to rebuild the faulty node. For details, see the build parameter in the *MogDB Tool Reference*.
