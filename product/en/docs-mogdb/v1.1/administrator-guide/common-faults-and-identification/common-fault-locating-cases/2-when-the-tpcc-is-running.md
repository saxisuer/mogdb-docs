---
title: When the TPC-C is running and a disk to be injected is full, the TPC-C stops responding.
summary: When the TPC-C is running and a disk to be injected is full, the TPC-C stops responding.
author: Guo Huan
date: 2021-05-24
---

# When the TPC-C is running and a disk to be injected is full, the TPC-C stops responding

## Symptom

When the TPC-C is running and a disk to be injected is full, the TPC-C stops responding. After the fault is rectified, the TPC-C automatically continues to run.

## Cause Analysis

When the disk where the performance log (**gs_profile**) is located is full, the database cannot write data and enters the infinite waiting state. As a result, the TPC-C stops responding. After the disk space insufficiency fault is rectified, performance logs can be properly written, and the TPC-C is restored.

## Procedure

Externally monitor the disk usage and periodically clean up the disk.
