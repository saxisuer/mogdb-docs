---
title: Modifying OS Configuration
summary: Modifying OS Configuration
author: Zhang Cuiping
date: 2021-04-2
---

# Modifying OS Configuration

## Disabling the Firewall

```
systemctl disable firewalld.service
systemctl stop firewalld.service
setenforce=0
sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
```

## Setting Character Set Parameters

Set the same character set for all database nodes. You can add **export LANG=Unicode** in the **/etc/profile** file.

Note: This setting can be ignored temporarily.

## Setting the Time Zone and Time

Set the same time zone for all database nodes by copying the **/etc/localtime** time zone file to the **/usr/share/zoneinfo/** directory.

cp /usr/share/zoneinfo/$Locale/​\$Time zone /etc/localtime

Run the **date -s** command to set the time of each host to the same time. For example:

```
date -s Mon May 11 16:42:11 CST 2020
```

## Disabling the Swap Memory

Run the **swapoff -a** command on each database node to disable the swap memory.

```
swapoff -a
```

Note: It is recommended that the swap memory is disabled when the memory size is greater than 32 GB.

## Setting the NIC MTU

Set the NIC MTU value on each database node to the same value. For X86, the recommended MTU value is 1500. For ARM, the recommended MTU value is 8192.

```
ifconfig NIC ID mtu Value
```
