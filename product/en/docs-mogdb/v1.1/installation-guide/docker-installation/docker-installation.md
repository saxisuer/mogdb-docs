---
title: Container-based Installation
summary: Container-based Installation
author: Guo Huan
date: 2021-06-01
---

# Container-based Installation

This document introduces the installation of single instance and primary/standby instances in a MogDB docker container. MogDB container does not support the MogHA and OM tools, and currently supports at most one primary node and one standby node.

<br/>

## Single Instance Installation

### Before You Start

The architectures and operating system versions supported by MogDB are as follows:

- x86-64 CentOS 7.6
- ARM64 openEuler 20.03 LTS

Before installing the MogDB container, you need to install Docker.

Docker is an open source application container engine based on Go and compliant with the Apache 2.0 protocol. Docker allows developers to package their applications and dependency packages into a lightweight, portable container that can then be distributed to any popular Linux machine and virtualized.

Please visit [Docker](https://docs.docker.com/) website and use the appropriate method to complete the installation according to your operating system.

### Installation Procedures

1. Run Docker.

2. Run one of the following commands according to your system architecture to obtain the latest version of the MogDB image file.

   x86-64:

   ```
   docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   ARM64:

   ```
   mydocker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_arm
   ```

   Take the x86-64 machine as an example:

   ![c3XvSP.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-12.png)

3. Run the following command to create a running directory for MogDB, using **mymogdb** as an example below.

   ```
   mkdir /mymogdb
   ```

4. Continue with the following command to create a new container, and name it **mymogdb** to run a MogDB instance:

   ```
   docker run --name mymogdb  -d -e GS_PASSWORD=Secretpassword@123   -v /mymogdb:/var/lib/opengauss  -p 15432:5432  swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   > Note: MogDB uses port 5432 in the container for listening by default. If you want to access the database from outside the container, you need to specify the `-p` parameter in `docker run` command. For example, the above command will allow access to the container database using port 15432. Once the container is deleted, all the data and configuration in the container will be lost. After re-running a container from the image, all the data is presented in the initialized state. So for the database container, in order to prevent the loss of data due to the demise of the container or corruption, the operation of persistent storage data is required. This is achieved by specifying the `-v` parameter in `docker run` command. For example, the above command will specify that all data files of MogDB will be stored under **/mymogdb** of the host. In addition, If you use Podman, there will be a target path check. Therefore you need to create the target path in advance (step 4).

5. Open the docker terminal:

   ```
   docker exec -it mymogdb /bin/bash
   ```

   Now, the single instance installation in a MogDB container is complete.

### Using MogDB

Once the installation is complete, switch to the **omm** user by running the following command, and then you can access the database via gsql and use MogDB properly.

```
su - omm
gsql -d postgres
```

![c3jMTJ.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-13.png)

### Environment Variables

To flexibly use an MogDB image, you can set additional parameters. In the future, more control parameters will be added. The current version supports the setting of the following variables.

**GS_PASSWORD**

This parameter is mandatory when the MogDB image is used. The value cannot be empty or undefined. This parameter specifies the passwords of superuser **omm** and test user **mogdb** of the MogDB database. During the MogDB installation, the superuser **omm** is created by default. This username cannot be changed. The test user **mogdb** is created in  **entrypoint.sh**.

The local trust mechanism is configured for the MogDB image. Therefore, no password is required for connecting to the database in the container. However, if you want to connect to the database from other hosts or containers, you need to enter the password.

The password must contain at least eight characters, including uppercase letters, lowercase letters, digits, and special characters ( \# ? ! @ $ % ^ & * \- ). **!$&**must be escaped using a backslash ().

**GS_NODENAME**

Specifies the database node name. The default value is  **mogdb**.

**GS_USERNAME**

Specifies the username for connecting to the database. The default value is  **mogdb**.

**GS_PORT**

Specifies the database port. The default value is  **5432**.

<br/>

## Primary/Standby Installation

### Before You Begin

To implement primary/standby installation in a MogDB container, please complete the steps in the previous document **Single Instance Installation** first.

### Procedures

1. Switch to user **root**.

   ```
   su - root
   ```

2. Create custom networks to create fixed IPs that are easy for containers to use.

   ```
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. Create a primary database container.

   ```
   docker run --name mogdb_master \
   --network myNetwork --ip 172.18.0.10 --privileged=true \
   --hostname mogdb_master --detach \
   --env GS_PORT=6432 \
   --env OG_SUBNET=172.18.0.0/16 \
   --env GS_PASSWORD=Enmo@1234 \
   --env NODE_NAME=mogdb_master \
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" \
   swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd -M primary
   ```

   ![docker-installation-1](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-1.png)

4. Create a standby database container.

   ```
    docker run --name mogdb_slave_one \
    --network myNetwork --ip 172.18.0.11 --privileged=true \
    --hostname mogdb_slave_one --detach \
    --env GS_PORT=6432 \
    --env OG_SUBNET=172.18.0.0/16 \
    --env GS_PASSWORD=Enmotech@1234 \
    --env NODE_NAME=mogdb_slave_one \
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" \
    enmotech/opengauss:1.0.1 -M standby
   ```

    ![docker-installation-2](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-2.png)

5. Query the status of the primary database.

   ```
   docker exec -it mogdb_master bash
   su - omm
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-3](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-3.png)

6. Query the status of the standby database.

   ```
   docker exec -it mogdb_slave_one bash
   su - omm
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-4](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-4.png)

   > Note: As shown in the figures above, the senders info and the receiver info indicate that the status of the primary and standby databases is normal.

### Read/Write Testing

Primary database write test:

```
gsql -p6432
create table test(ID int);
insert into test values(1);
```

![docker-installation-5](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-5.png)

Standby database read test:

```
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-6](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-6.png)

> Note: The result shows that the primary database is writable and the standby database is readable but not writable.

### Switchover Testing

Switch **mogdb_slave_one** as the primary database and **mogdb_master** as the standby database.

Run the switchover command in **mogdb_slave_one**.

```
gs_ctl switchover -D /var/lib/opengauss/data/
```

![docker-installation-7](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-7.png)

Query the status of **mogdb_slave_one**.

```
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-8](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-8.png)

Query the status of **mogdb_master**.

```
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-9](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-9.png)

You can find that **mogdb_master** becomes the standby database and **mogdb_slave_one** becomes the primary database. The switchover is successful.

### Read/Write Verification

Perform write validation on the primary database **mogdb_slave_one**.

```
gsql -p6432
select * from test;
insert into test values(2);
```

![docker-installation-10](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-10.png)

Perform read validation on the standby database **mogdb_master**.

```
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-11](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-11.png)

As you can see, the original standby database is writable after being switched as the primary database, and the original primary database is readable but not writable after being switched as the standby database.

<br/>

## What's Next

MogDB container does not support MogHA and OM tools. It is only used for testing, and currently supports at most one primary node and one standby node. MogDB Enterprise Edition includes MogHA component which supports one primary node and eight standbys at most. The basic functions of the MogDB container and Enterprise Edition are identical. It is recommended that MogDB Enterprise Edition is used in the production environment.
