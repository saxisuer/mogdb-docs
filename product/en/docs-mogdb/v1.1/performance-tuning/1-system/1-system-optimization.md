---
title: Optimizing OS Parameters
summary: Optimizing OS Parameters
author: Liu Xu
date: 2021-03-04
---

# Optimizing OS Parameters

You can improve the MogDB performance by modifying key parameters of the OS based on actual service requirements during the performance optimization.

## Prerequisites

You have checked whether the OS parameters are set to the suggested values using **gs_check**. If not, modify them as required.

## Memory Parameters

In the **sysctl.conf** file, change the value of **vm.extfrag_threshold** to **1000** (reference value). If the file does not contain memory parameters, add them manually.

```bash
vim /etc/sysctl.conf
```

Run the following command to make the modification take effect:

```sql
sysctl -p
```

## Network Parameters

- In the **sysctl.conf** file, modify network parameters. If the file does not contain such parameters, add them manually. For details, see [Table 1](#table 1).

  ```
  vim /etc/sysctl.conf
  ```

  Run the following command to make the modification take effect:

  ```
  sysctl -p
  ```

  <a id="table 1">**Table 1**</a> Network parameters

  | Parameter                    | Reference Value              | Description                                                  |
| :--------------------------- | :--------------------------- | :----------------------------------------------------------- |
  | net.ipv4.tcp_timestamps      | 1                            | Specifies whether to enable quick reclamation of the sockets in TIME-WAIT state during TCP connection establishment. The default value **0** indicates that quick reclamation is disabled, and the value **1** indicates that quick reclamation is enabled. |
  | net.ipv4.tcp_mem             | 94500000 915000000 927000000 | **94500000**: If less than 94,500,000 pages are used by the TCP, the kernel is not affected.<br />**915000000**: If more than 915,000,000 pages are used by the TCP, the kernel enters the **memory pressure** mode.<br />**927000000**: If more than 927,000,000 pages are used by the TCP, the "Out of socket memory." message is displayed. |
  | net.ipv4.tcp_max_orphans     | 3276800                      | Maximum number of the orphan sockets                         |
  | net.ipv4.tcp_fin_timeout     | 60                           | Default timeout period                                       |
  | net.ipv4.ip_local_port_range | 26000 65535                  | Port range that can be used by TCP or UDP                    |

- Use the **ifconfig** command to set the maximum transmission unit (MTU) of 10 GE NICs. The value **8192** is recommended because this setting improves the network bandwidth usage.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  > When **comm_tcp_mode** is set to **false** for database nodes, set the MTU sizes in each node in MogDB to the same value. Otherwise, network communications may fail.

  Example:

  ```bash
  #ifconfig ethx mtu 8192
  #ifconfig ethx
  ethx   Link encap:Ethernet  HWaddr XX:XX:XX:XX:XX:XX
  inet addr:xxx.xxx.xxx.xxx  Bcast:xxx.xxx.xxx.xxx  Mask:xxx.xxx.xxx.0
  inet6 addr: fxxx::9xxx:bxxx:xxxa:1d18/64 Scope:Link
  UP BROADCAST RUNNING MULTICAST  MTU:8192  Metric:1
  RX packets:179849803 errors:0 dropped:0 overruns:0 frame:0
  TX packets:40492292 errors:0 dropped:0 overruns:0 carrier:0
  collisions:0 txqueuelen:1000
  RX bytes:17952090386 (17120.4 Mb)  TX bytes:171359670290 (163421.3 Mb)
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - **ethx** indicates the 10 GE service NIC used in the database.
  > - The first command is used to set the MTU. The second command is used to verify that the MTU has been successfully set. The texts in bold indicate the value of the MTU.
  > - Set the MTU as user **root**.

- Use **ethtool** to set the length of the receiving (**RX**) queue and that of the sending (**TX**) queue for 10 GE NICs. The value **4096** is recommended because this setting improves the network bandwidth usage.

  Example:

  ```
  # ethtool -G ethx rx 4096 tx 4096
  # ethtool -g ethx
  Ring parameters for ethx:
  Pre-set maximums:
  RX:             4096
  RX Mini:        0
  RX Jumbo:       0
  TX:             4096
  Current hardware settings:
  RX:             4096
  RX Mini:        0
  RX Jumbo:       0
  TX:             4096
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - **ethx** indicates the 10 GE service NIC used in the database.
  > - The first command is used to set the lengths of the receiving and sending queues. The second command is to verify whether the lengths have been successfully set. If information similar to the example is displayed, the setting is successful.
  > - Set the lengths of the receiving and sending queues as user **root**.

## I/O Parameters

Set the **hugepage** attribute. To disable the transparent hugepage, add the following content to the end of the first kernel line in the **/boot/grub/menu.lst** file:

```
 transparent_hugepage=never
```

Run the following command to make the modification take effect:

```
 reboot
```
