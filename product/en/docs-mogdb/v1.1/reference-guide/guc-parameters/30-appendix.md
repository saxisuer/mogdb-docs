---
title: Appendices
summary: Appendices
author: Zhang Cuiping
date: 2021-04-20
---

# Appendices

## **Table 1** GUC parameters<a id="GUC parameters"> </a>

| Parameter type. | Remarks                                                      | How to Set                                                   |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| INTERNAL        | Fixed parameter. It is set during database creation and cannot be modified. Users can only view the parameter by running the **SHOW** command or in the **pg_settings** view. | None                                                         |
| POSTMASTER      | Database server parameter. It can be set when the database is started or in the configuration file. | Method 1 in Table 2 [Methods for setting GUC parameters](30-appendix.md).  |
| SIGHUP          | Global database parameter. It can be set when the database is started or be modified later. | Method 1 or 2 in Table 2 [Methods for setting GUC parameters](30-appendix.md). |
| BACKEND         | Session connection parameter. It is specified during session connection creation and cannot be modified after that. The parameter setting becomes invalid when the session is disconnected. This is an internal parameter and not recommended for users to set it. | Method 1 or 2 in Table 2 [Methods for setting GUC parameters](30-appendix.md).<br />NOTE: The parameter setting takes effect when the next session is created. |
| SUSET           | Database administrator parameter. It can be set by common users when or after the database is started. It can also be set by database administrators using SQL statements. | Method 1 or 2 by a common user, or method 3 by a database administrator in Table 2 [Methods for setting GUC parameters](30-appendix.md). |
| USERSET         | Common user parameter. It can be set by any user at any time. | Method 1, 2, or 3 in Table 2 [Methods for setting GUC parameters](30-appendix.md). |

## **Table 2** Methods for setting GUC parameters <a id="Methods for setting GUC parameters"> </a>

| No.       | Description                                                  |
| --------- | ------------------------------------------------------------ |
| Method 1  | 1. Run the following command to set a parameter:<br />`gs_guc set -D datadir -c "paraname=value"`<br />NOTE:<br />If the parameter is a string variable, use **-c** *parameter***="'**value**'"** or **-c "**parameter = 'value'".<br />Run the following command to set a parameter for database nodes at the same time:<br />`gs_guc set -Nall -I all -c "paraname=value"`<br />2. Restart the database to make the setting take effect.<br />NOTE:<br />Restarting MogDB results in operation interruption. Properly plan the restart to avoid affecting users.<br />`gs_om -t stop && gs_om -t start` |
| Method 2  | `gs_guc reload -D datadir -c "paraname=value"`<br />NOTE:<br />Run the following command to set a parameter for database nodes at the same time:<br />`gs_guc reload -N all -I all -c "paraname=value"` |
| Method 3: | Set parameters at database, user, or session levels.<br />- Set a database-level parameter.<br />`postgres=# ALTER DATABASE dbname SET paraname TO value;`<br />The setting takes effect in the next session.<br />- Set a user-level parameter.<br />`postgres=# ALTER USER username SET paraname TO value;`<br />The setting takes effect in the next session.<br />- Set a session-level parameter.<br />`postgres=# SET paraname TO value;`<br />Parameter value in the current session is changed. After you exit the session, the setting becomes invalid. |
