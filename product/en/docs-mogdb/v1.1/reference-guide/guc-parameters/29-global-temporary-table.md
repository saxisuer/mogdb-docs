---
title: Global Temporary Table
summary: Global Temporary Table
author: Zhang Cuiping
date: 2021-04-20
---

# Global Temporary Table

## max_active_global_temporary_table

**Parameter description**: Specifies whether global temporary tables can be created.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000

- **0**: The global temporary table function is disabled.
- &gt; 0: The global temporary table function is enabled.

**Default value**: **1000**

## vacuum_gtt_defer_check_age

**Parameter description**: Checks the differences between the global temporary table relfrozenxid and the ordinary table after VACUUM is executed. WARNING is generated if the difference value exceeds the specified parameter value. Use the default value for this parameter.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000

**Default value**: **10000**
