---
title: Memory Table
summary: Memory Table
author: Zhang Cuiping
date: 2021-04-20
---

# Memory Table

This section describes the parameters in the memory table.

## enable_codegen_mot

**Parameter description**: Specifies whether to enable the native LLVM Lite to perform simple queries. If native LLVM is not supported on the current platform, pseudo LLVM will be used.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **true**

## force_pseudo_codegen_mot

**Parameter description**: Specifies whether to force pseudo LLVM Lite to perform simple queries, even if the current platform supports native LLVM.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **true**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> Even if **force_pseudo_codegen_mot** is set to **false**, **force_pseudo_codegen_mot** is set to **true**. However, the current platform does not support the native LLVM. In this case, the pseudo LLVM is still used.

## enable_codegen_mot_print

**Parameter description**: Specifies whether to print the IR byte code of the generation function. (If pseudo LLVM is used, the pseudo IR byte code is printed.)

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **true**

## codegen_mot_limit

**Parameter description**: Specifies the maximum number of global cache plan sources and the clone plan of each session.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** uint32

**Default value**: **100**

## mot_allow_index_on_nullable_column

**Parameter description**: Specifies whether indexes can be created on the **nullable** column in the memory table.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **true**

## mot_config_file

**Parameter description**: Specifies the main configuration file of the MOT.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **NULL**
