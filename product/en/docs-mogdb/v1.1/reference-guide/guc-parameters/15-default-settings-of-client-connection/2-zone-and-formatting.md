---
title: Zone and Formatting
summary: Zone and Formatting
author: Zhang Cuiping
date: 2021-04-20
---

# Zone and Formatting

This section describes parameters related to the time format setting.

## DateStyle

**Parameter description:** Specifies the display format for date and time values, as well as the rules for interpreting ambiguous date input values.

This variable contains two independent components: the output format specifications (ISO, Postgres, SQL, or German) and the input/output order of year/month/day (DMY, MDY, or YMD). The two components can be set separately or together. The keywords Euro and European are synonyms for DMY; the keywords US, NonEuro, and NonEuropean are synonyms for MDY.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **ISO, MDY**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> **gs_initdb** will initialize this parameter so that its value is the same as that of **[lc_time](#lc_time)**.

**Suggestion**: The ISO format is recommended. Postgres, SQL, and German use abbreviations for time zones, such as **EST**, **WST**, and **CST**. These abbreviations can be ambiguous. For example, **CST** can represent Central Standard Time (USA) UT-6:00, Central Standard Time (Australia) UT+9:30, China Standard Time UT+8:00, and Cuba Standard Time UT-4:00. This may lead to incorrect time zone conversion and cause errors.

## IntervalStyle

**Parameter description:** sets the display format for interval values.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: enumerated values

- **sql_standard** indicates that output matching SQL standards will be generated.
- **postgres** indicates that output matching PostgreSQL 8.4 will be generated when the **[DateStyle](#DateStyle)** parameter is set to **ISO**.
- **postgres_verbose** indicates that output matching PostgreSQL 8.4 will be generated when the **[DateStyle](#DateStyle)** parameter is set to **non_ISO**.
- **iso_8601** indicates that output matching the time interval "format with designators" defined in ISO 8601 will be generated.
- **a** indicates the output result that matches the numtodsinterval function. For details, see numtodsinterval.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The IntervalStyle parameter also affects the interpretation of ambiguous interval input.

**Default value**: **postgres**

## TimeZone

**Parameter description**: Specifies the time zone for displaying and interpreting time stamps.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value:** a string. You can obtain it by querying the PG_TIMEZONE_NAMES view.

**Default value**: **PRC**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> **gs_initdb** will set a time zone value that is consistent with the system environment.

## timezone_abbreviations

**Parameter description:** sets the time zone abbreviations that will be accepted by the server.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value:** a string. You can obtain it by querying the pg_timezone_names view.

**Default value**: **Default**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> **Default** indicates an abbreviation that works in most of the world, which is applicable to most cases. There are also other abbreviations, such as **Australia** and **India** that can be defined for a particular installation. For other time zone abbreviations, you need to set them in the corresponding configuration files before creating the database.

## extra_float_digits

**Parameter description:** adjusts the number of digits displayed for floating-point values, including float4, float8, and geometric data types. The parameter value is added to the standard number of digits (FLT_DIG or DBL_DIG as appropriate).

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from -15 to 3

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - This parameter can be set to **3** to include partially-significant digits. It is especially useful for dumping float data that needs to be restored exactly.
> - This parameter can also be set to a negative value to suppress unwanted digits.

**Default value**: **0**

## client_encoding

**Parameter description:** sets the client-side encoding (character set).

Set this parameter as needed. Try to keep the client code and server code consistent to improve efficiency.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: encoding compatible with PostgreSQL. **UTF8** indicates that the database encoding is used.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - You can run the **locale -a** command to check and set the system-supported zone and the corresponding encoding format.
> - By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.
> - To use consistent encoding for communication within the MogDB, you are advised to retain the default value of **client_encoding**. Modification to this parameter in the **postgresql.conf** file (by using the **gs_guc** tool, for example) does not take effect.

**Default value**: **UTF8**

**Recommended value**:**SQL_ASCII** or **UTF8**

## lc_messages

**Parameter description:** sets the language in which messages are displayed.

- Acceptable values are system-related.

- On some systems, this zone category does not exist. Setting this variable will still work, but there will be no effect. In addition, translated messages for the desired language may not exist. In this case, you can still see the English messages.

  This parameter is a SUSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - You can run the **locale -a** command to check and set the system-supported zone and the corresponding encoding format.
> - By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

**Default value**: **C**

## lc_monetary

**Parameter description**: Specifies the display format of monetary values. It affects the output of functions such as **to_char**. Acceptable values are system-related.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - You can run the **locale -a** command to check and set the system-supported zone and the corresponding encoding format.
> - By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

**Default value**: **C**

## lc_numeric

**Parameter description:** sets the display format of numbers. It affects the output of functions such as to_char. Acceptable values are system-related.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - You can run the **locale -a** command to check and set the system-supported zone and the corresponding encoding format.
> - By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

**Default value**: **C**

## lc_time

**Parameter description**: Specifies the display format of time and locale. It affects the output of functions such as **to_char**. Acceptable values are system-related.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - You can run the **locale -a** command to check and set the system-supported zone and the corresponding encoding format.
> - By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

**Default value**: **C**

## default_text_search_config

**Parameter description**: Specifies the text search configuration.

If the specified text search configuration does not exist, an error will be reported. If the specified text search configuration is deleted, set **default_text_search_config** again. Otherwise, an error will be reported, indicating incorrect configuration.

- The text search configuration is used by text search functions that do not have an explicit argument specifying the configuration.

- When a configuration file matching the environment is determined, **gs_initdb** will initialize the configuration file with a setting that corresponds to the environment.

  This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> MogDB supports the following two configurations: pg_catalog.english and pg_catalog.simple.

**Default value**: **pg_catalog.english**
