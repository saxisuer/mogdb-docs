---
title: Miscellaneous Parameters
summary: Miscellaneous Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Miscellaneous Parameters

## server_version

**Parameter description**: Specifies the server version number.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: a string

**Default value**: **9.2.4**

## server_version_num

**Parameter description**: Specifies the server version number.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: an integer

**Default value**: **90204**

## block_size

**Parameter description**: Specifies the block size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value**:**8192**

**Default value**: **8192**

## segment_size

**Parameter description**: Specifies the segment file size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Unit**: 8 KB

**Default value**: 131072, that is, 1 GB

## max_index_keys

**Parameter description**: Specifies the maximum number of index keys supported by the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: **32**

## integer_datetimes

**Parameter description**: Specifies whether the date and time are in the 64-bit integer format.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: Boolean

- **on** indicates that the 64-bit integer format is used.
- **off** indicates that the 64-bit integer format is not used.

**Default value**: **on**

## lc_collate

**Parameter description:** Specifies the locale in which sorting of textual data is done.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the MogDB installation and deployment.

## lc_ctype

**Parameter description**: Specifies the locale that determines character classifications. For example, it specifies what a letter and its upper-case equivalent are.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the MogDB installation and deployment.

## max_identifier_length

**Parameter description**: Specifies the maximum identifier length.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: an integer

**Default value**: **63**

## server_encoding

**Parameter description**: Specifies the database encoding (character set).

By default, gs_initdb will initialize the setting of this parameter based on the current system environment. You can also run the **locale** command to check the current configuration environment.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value:** determined by the current system environment when the database is created.

## enable_upgrade_merge_lock_mode

**Parameter description**: If this parameter is set to **on**, the delta merge operation internally increases the lock level, and errors can be prevented when update and delete operations are performed at the same time.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- If this parameter is set to **on**, the delta merge operation internally increases the lock level. In this way, when the **DELTAMERGE** operation is concurrently performed with the **UPDATE** or **DELETE** operation, one operation can be performed only after the previous one is complete.
- If this parameter is set to **off**, and any two of the **DELTAMERGE**, **UPDATE**, and **DELETE** operations are concurrently performed to data in a row in the delta table of the HDFS table, errors will be reported during the later operation, and the operation will stop.

**Default value**: **off**

## job_queue_processes

**Parameter description:** Specifies the number of jobs that can be concurrently executed. This parameter is a POSTMASTER parameter. You can set it using **gs_guc**, and you need to restart **mogdb** to make the setting take effect.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: 0 to 1000

Function:

- Setting **job_queue_processes** to **0** indicates that the scheduled job function is disabled and that no job will be executed. (Enabling scheduled jobs may affect the system performance. At sites where this function is not required, you are advised to disable it.)
- Setting **job_queue_processes** to a value that is greater than **0** indicates that the scheduled job function is enabled and this value is the maximum number of jobs that can be concurrently processed.

After the scheduled job function is enabled, the job_scheduler thread polls the **pg_job** system catalog at a scheduled interval. The scheduled job check is performed every second by default.

Too many concurrent jobs consume many system resources, so you need to set the number of concurrent jobs to be processed. If the current number of concurrent jobs reaches the value of **job_queue_processes** and some of them expire, these jobs will be postponed to the next polling period. Therefore, you are advised to set the polling interval (the **Interval** parameter of the submit interface) based on the execution duration of each job to avoid the problem that jobs in the next polling period cannot be properly processed because of overlong job execution time.

Note: If the number of concurrent jobs is large and the value is too small, these jobs will wait in queues. However, a large parameter value leads to large resource consumption. You are advised to set this parameter to **100** and change it based on the system resource condition.

**Default value:** **10**

## ngram_gram_size

**Parameter description**: Specifies the length of the ngram parser segmentation.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 4

**Default value:** **2**

## ngram_grapsymbol_ignore

**Parameter description**: Specifies whether the ngram parser ignores graphical characters.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on**: The ngram parser ignores graphical characters.
- **off**: The ngram parser does not ignore graphical characters.

**Default value**: **off**

## ngram_punctuation_ignore

**Parameter description**: Specifies whether the ngram parser ignores punctuations.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on**: The ngram parser ignores punctuations.
- **off**: The ngram parser does not ignore punctuations.

**Default value**: **on**

## transparent_encrypted_string

**Parameter description**: Specifies a sample string that is transparently encrypted. Its value is generated by encrypting **TRANS_ENCRYPT_SAMPLE_STRING** using a database secret key. The ciphertext is used to check whether the DEK obtained during secondary startup is correct. If it is incorrect, database nodes will not be started. This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string. An empty string indicates that MogDB is a not encrypted.

**Default value**: empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Do not set this parameter manually. Otherwise, MogDB may become faulty.

## transparent_encrypt_kms_url

**Parameter description**: Specifies the URL for obtaining the database secret key to be transparently encrypted. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes. The format is **kms://***Protocol***@***KMS host name 1***;***KMS host name 2***:***KMS port number***/kms**, for example, **kms://https@linux175:29800/**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: empty

## enable_auto_explain

**Parameter description**: Specifies whether to enable the execution plan of automatic print. This parameter is used for locating slow storage or query and is valid only for the primary database node.

**Value range**: Boolean

- **true**: The execution plan of automatic print is enabled.
- **false**: The execution plan of automatic print is disabled.

**Default value**: false

## auto_explain_level

**Parameter description**: Specifies the log level for controlling the execution plan of automatic print.

**Value range**: enumerated values

- **LOG**: indicates that the execution plan is printed in the log.
- **NOTICE**: indicates that the execution plan is printed in the notice form.

**Default value**: LOG

## transparent_encrypt_kms_region

**Parameter description**: Specifies the deployment region of MogDB. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: empty

## basebackup_timeout

**Parameter description**: Specifies the timeout period for connection during which no read/write operations are performed after backup transmission is complete. When the gs_basebackup tool is used for transmission, if a higher compression rate is specified, the timeout may occur after the tablespace transmission is complete. (The transmission data needs to be compressed in the client.)

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is second. **0**: indicates that the parameter is disabled.

**Default value**: 600s

## behavior_compat_options

**Parameter description**: Specifies database compatibility behavior. Multiple items are separated by commas (,).

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Currently, only compatibility configuration items in [Table 1](#compatibility) are supported.
> - Multiple items are separated by commas (,), for example, **set behavior_compat_options='end_month_calculate,display_leading_zero';**.

**Table 1** Compatibility configuration items<a id="compatibility"> </a>

| Configuration Item        | Behavior                                                     |
| ------------------------- | ------------------------------------------------------------ |
| display_leading_zero      | Specifies how floating point numbers are displayed.If this item is not specified, for a decimal number between -1 and 1, the 0 before the decimal point is not displayed. For example, 0.25 is displayed as **.25**.If this item is specified, for a decimal number between -1 and 1, the 0 before the decimal point is displayed. For example, 0.25 is displayed as **0.25**. |
| end_month_calculate       | Specifies the calculation logic of the add_months function.Assume that the two parameters of the add_months function are **param1** and **param2**, and that the month of **param1** and **param2** is **result**.If this item is not specified, and the **Day** of **param1** indicates the last day of a month shorter than **result**, the **Day** in the calculation result will equal that in **param1**. For example:<br />`postgres=# select add_months('2018-02-28',3) from dual; add_months 2018-05-28 00:00:00 (1 row)`<br />If this item is specified, and the **Day** of **param1** indicates the last day of a month shorter than **result**, the **Day** in the calculation result will equal that in **result**. For example:<br />`postgres=# select add_months('2018-02-28',3) from dual; add_months 2018-05-31 00:00:00 (1 row)` |
| compat_analyze_sample     | Specifies the sampling behavior of the ANALYZE operation.If this item is specified, the sample collected by the ANALYZE operation will be limited to around 30,000 records, DBnode memory consumption and maintaining the stability of ANALYZE. |
| bind_schema_tablespace    | Binds a schema with the tablespace with the same name.If a tablespace name is the same as *sche_name*, **default_tablespace** will also be set to *sche_name* if **search_path** is set to *sche_name*. |
| bind_procedure_searchpath | Specifies the search path of the database object for which no schema name is specified.If no schema name is specified for a stored procedure, the search is performed in the schema to which the stored procedure belongs.If the stored procedure is not found, the following operations are performed:If this item is not specified, the system reports an error and exits.If this item is specified, the search continues based on the settings of **search_path**. If the issue persists, the system reports an error and exits. |
| correct_to_number         | Controls the compatibility of the to_number() result.If this item is specified, the result of the to_number() function is the same as that of PG11. Otherwise, the result is the same as that of the O database. |
| unbind_dive_bound         | Controls the range check on the result of integer division.If this item is specified, you do not need to check the range of the division result. For example, the result of INT_MIN/(-1) can be *INT_MAX*+1. If this item is not specified, an out-of-bounds error is reported because the result is greater than*INT_MAX*. |
| merge_update_multi        | Performs an update if multiple rows are matched for **MERGE INTO**.If this item is specified, no error is reported if multiple rows are matched. Otherwise, an error is reported (same as the O database). |
| return_null_string        | Specifies how to display the empty result (empty string ") of the lpad() and rpad() functions.If this item is not specified, the empty string is displayed as **NULL**.<br />`postgres=# select length(lpad('123',0,'*')) from dual; length (1 row)`<br />If this item is specified, the empty string is displayed as single quotation marks (").<br />`postgres=# select length(lpad('123',0,'*')) from dual; length 0 (1 row)` |
| compat_concat_variadic    | Specifies the compatibility of variadic results of the concat() and concat_ws() functions.If this item is specified and a concat function has a parameter of the variadic type, different result formats in O and Teradata are retained. If this item is not specified and a concat function has a parameter of the variadic type, the result format of O is retained for both O and Teradata. This option has no effect on MY because MY has no variadic type. |
| merge_update_multi        | Controls the UPDATE operation if a data record in the destination table conflicts with multiple source data records when **MERGE INTO … WHEN MATCHED THEN UPDATE** (see MERGE INTO) and **INSERT … ON DUPLICATE KEY UPDATE** (see INSERT) are used.If this item is specified and the preceding scenario exists, the system performs multiple UPDATE operations on the conflicting row. If this item is not specified and the preceding scenario exists, an error is reported, that is, the MERGE or INSERT operation fails. |

## table_skewness_warning_threshold

**Parameter description**: Specifies the threshold for triggering a table skew alarm.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a floating point number ranging from 0 to 1

**Default value**: **1**

## table_skewness_warning_rows

**Parameter description**: Specifies the minimum number of rows for triggering a table skew alarm.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*

**Default value**: **100000**

## datanode_heartbeat_interval

**Parameter description**: Specifies the interval at which heartbeat messages are sent between heartbeat threads. You are advised to set this parameter to a value no more than wal_receiver_timeout/2.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1000 to 60000. The unit is ms.

Default value:**1s**

## bgwriter_thread_num

**Parameter description**: Specifies the number of bgwriter threads for flushing pages after the incremental checkpoint is enabled. Dirty pages to be evicted are flushed to disks, and non-dirty pages are placed in the candidate buffer chain. This parameter helps accelerate buffer eviction and improve performance.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 8

- To test the effect of disabling this feature during development, you can set this parameter to **0**. However, if this parameter is set to **0**, the value will be changed to **1** in the code and the feature cannot be disabled.
- If this parameter is set to a value ranging from 1 to 8, the corresponding number of background threads are started to maintain the candidate buffer chain. Dirty pages that meet the conditions are flushed to disks, and non-dirty pages are added to the candidate list.

**Default value**: **2**

## candidate_buf_percent_target

**Parameter description**: Specifies the expected percentage of available buffers in the shared_buffer when the incremental checkpoint is enabled and the value of bgwriter_thread_num is not 0. When the number of available buffers in the current candidate buffer is less than the target value, the bgwriter thread starts to flush dirty pages that meet the conditions to disks.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: double precision floating point number ranging from 0.1 to 0.85

**Default value**: **0.3**

## pagewriter_thread_num

**Parameter description**: Specifies the number of threads for background page flushing after the incremental checkpoint is enabled. Dirty pages are flushed in sequence to disks, promoting recovery points.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 8

**Default value**: **2**

## pagewriter_threshold

**Parameter description**: Specifies the number of dirty pages that must be reached for the thread to continue refreshing dirty pages without sleeping when incremental checkpointing is enabled. This parameter does not take effect any more.

Use **dirty_page_percent_max** to set the value.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to *INT_MAX*

**Default value**: **818**

## dirty_page_percent_max

**Parameter description**: Specifies the percentage of dirty pages to shared_buffers after the incremental checkpoint is enabled. When the percentage is reached, the background thread for flushing pages continues flushing dirty pages without sleeping.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a floating point number ranging from 0.1 to 1

**Default value**: **0.9**

## pagewriter_sleep

**Parameter description**: Specifies the time in which the thread continues refreshing dirty pages when incremental checkpointing is enabled and the number of dirty pages does not reach the value of **pagewriter_threshold**.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 3600000. The unit is ms.

**Default value**: **2000ms**

## remote_read_mode

**Parameter description**: Specifies whether to enable the remote read function. This function allows pages on the standby server to be read when reading pages on the primary server fails.

**Value range**: enumerated values

- **off** indicates that the remote read function is disabled.
- **non_authentication** indicates that the remote read function is enabled but certificate authentication is not required.
- **authentication** indicates that the remote read function is enabled and certificate authentication is required.

**Default value**: **authentication**
