---
title: Connection Settings
summary: Connection Settings
author: Zhang Cuiping
date: 2021-04-20
---

# Connection Settings

This section describes parameters related to client-server connection modes.

## listen_addresses

**Parameter description**: Specifies the TCP/IP addresses that a server listens to for connections from the client. This parameter specifies the IP address used by the MogDB server to listen on, such as IPv4 address or IPv6 address(if supported). A server may have multiple NICs, and each NIC can be bound with multiple IP address. This parameter specifies the IP addresses which the MogDB server is bound. And the client can use the IP address specified in this parameter to connect to MogDB or send requests to MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**:

- Host name or IP address. Multiple values are separated with commas (,).
- Asterisk (*) or **0.0.0.0**, indicating that all IP addresses will be listened to, which is not recommended due to potential security risks. This parameter must be used together with valid addresses (for example, the local IP address). Otherwise, the build may fail.
- If the parameter is not specified, the server does not listen to any IP address. In this case, only Unix domain sockets can be used for database connections.

**Default value**: **localhost**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> **localhost** indicates that only local loopback is allowed.

## local_bind_address

**Parameter description**: Specifies the host IP address bound to the current node for connecting to other nodes in MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0.0.0.0** (The actual value is specified in the configuration file during installation.)

## port

**Parameter description**: Specifies the TCP port listened to by MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

After the port parameter is modified, you need to execute the following command to refresh the static configuration file.

```
gs_om -t generateconf -X XMLFILE [--distribute] [-l LOGFILE]
```

**Value range**: an integer ranging from 1 to 65535

**Default value**: **5432** (The actual value is specified in the configuration file during installation.)

## max_connections

**Parameter description**: Specifies the maximum number of concurrent connections to the database. This parameter influences the concurrent processing capability of MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 10 to 262143

**Default value**: **5000** for each database node If the default value is greater than the maximum number supported by the kernel (determined when the **gs_initdb** command is executed), an error message is displayed.

**Setting suggestions**:

Retain the default value of this parameter on the primary node of the database.

If this parameter is set to a large value, MogDB requires more System V shared memories or semaphores, which may exceed the default maximum configuration of the OS. In this case, modify the value as needed.

## max_inner_tool_connections

**Parameter description**: Specifies the maximum number of concurrent connections for tools allowed to connect to a database. This parameter controls the concurrent connection capability of tools that are connected to MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *MIN (262143, max_connections)*. For details about the calculation method of max_connections, see the above content.

**Default value**: 50

If the default value exceeds the upper limit (judged when **gs_initdb** is executed) supported by the kernel, an error will be prompted.

**Setting suggestions**:

You are advised to set this parameter to the default value on a primary database node.

Setting this parameter to a higher value may make MogDB require more SystemV to share memory or semaphore. As a result, this may exceed the default upper limit of the OS. In this case, adjust the value properly.

## sysadmin_reserved_connections

**Parameter description**: Specifies the minimum number of connections reserved for administrators. You are advised not to set this parameter to a large value.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *MIN* (which takes the smaller value between **262143** and **max_connections**)

**Default value**: **3**

## unix_socket_directory

**Parameter description**: Specifies the Unix domain socket directory that the MogDB server listens to for connections from the client.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

The parameter length limit varies by OS. If the length is exceeded, the error "Unix-domain socket path xxx is too long" will be reported.

**Value range**: a string

**Default value**: empty. The actual value is specified by the configuration file during installation.

## unix_socket_group

**Parameter description**: Specifies the group of the Unix domain socket (the user of a socket is the user that starts the server). This parameter can work with **[unix_socket_permissions](#unix_socket_permissions)** to control socket access.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. If this parameter is set to an empty string, the default group of the current user is used.

**Default value**: empty

## unix_socket_permissions

**Parameter description**: Specifies access permissions for the Unix domain socket.

The Unix domain socket uses the usual permission set of the Unix file system. The value of this parameter should be a number (acceptable for the **chmod** and **umask** commands). If a user-defined octal format is used, the number must start with 0.

You are advised to set it to **0770** (only allowing access from users connecting to the database and users in the same group as them) or **0700** (only allowing access from users connecting to the database).

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: 0000 to 0777

**Default value**: **0700**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> In the Linux OS, a document has one document attribute and nine permission attributes, which consists of the read (r), write (w), and execute (x) permissions of the Owner, Group, and Others groups.
> The r, w, and x permissions are represented by the following numbers:
> r: 4
> w: 2
> x: 1
> -: 0
> The three attributes in a group are accumulative.
> For example, **-rwxrwx-** indicates the following permissions:
> owner = rwx = 4+2+1 = 7
> group = rwx = 4+2+1 = 7
> others = - = 0+0+0 = 0
> The permission of the file is 0770.

## application_name

**Parameter description**: Specifies the client name used in the current connection request.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: empty (The actual value is the name of the application connected to the backend.)

## connection_info

**Parameter description**: Specifies the database connection information, including the driver type, driver version, driver deployment path, and process owner.

This parameter is a USERSET parameter used for O&M. You are advised not to change the parameter value.

**Value range**: a string

**Default value**: empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - An empty string indicates that the driver connected to the database does not support automatic setting of the **connection_info** parameter or the parameter is not set by users in applications.
> - The following is an example of the concatenated value of **connection_info**:
>
> ```
> {"driver_name":"ODBC","driver_version": "(mogdb Kernel V500R001C00 build 290d125f) compiled at 2020-05-08 02:59:43 commit 2143 last mr 131 debug","driver_path":"/usr/local/lib/psqlodbcw.so","os_user":"omm"}
> ```
>
> **driver_name** and **driver_version** are displayed by default. Whether **driver_path** and **os_user** are displayed is determined by users.
