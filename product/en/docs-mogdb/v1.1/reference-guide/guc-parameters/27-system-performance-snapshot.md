---
title: System Performance Snapshot
summary: System Performance Snapshot
author: Zhang Cuiping
date: 2021-04-20
---

# System Performance Snapshot

## enable_wdr_snapshot

**Parameter description**: Specifies whether to enable the database monitoring snapshot function.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the database monitoring snapshot function is enabled.
- **off** indicates that the database monitoring snapshot function is disabled.

**Default value**: **off**

## wdr_snapshot_retention_days

**Parameter description**: Specifies the number of days database monitoring snapshots are retained. When the value of **wdr_snapshot_retention_days** is exceeded, the system cleans up snapshots with the smallest snapshot_id at the interval specified by **wdr_snapshot_interval**.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 8

**Default value**: **8**

## wdr_snapshot_query_timeout

**Parameter description**: Specifies the execution timeout for the SQL statements associated with database monitoring snapshot operations. If the SQL statement execution is not complete and a result is not returned within the specified time, the snapshot operation fails.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 100 to *INT_MAX*. The unit is s.

**Default value**: **100s**

## wdr_snapshot_interval

**Parameter description**: Specifies the interval at which the background thread Snapshot automatically performs snapshot operations on the database monitoring data.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10 to 60. The unit is m.

**Default value**: **1h**
