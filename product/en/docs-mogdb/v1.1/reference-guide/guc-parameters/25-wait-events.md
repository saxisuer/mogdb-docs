---
title: Wait Events
summary: Wait Events
author: Zhang Cuiping
date: 2021-04-20
---

# Wait Events

## enable_instr_track_wait

**Parameter description**: Specifies whether to enable real-time collection of wait event information.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the function of collecting wait event information is enabled.
- **off** indicates that the function of collecting wait event information is disabled.

**Default value**: **on**
