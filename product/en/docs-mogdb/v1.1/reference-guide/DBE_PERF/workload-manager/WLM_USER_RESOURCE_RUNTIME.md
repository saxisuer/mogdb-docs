---
title: WLM_USER_RESOURCE_RUNTIME
summary: WLM_USER_RESOURCE_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# WLM_USER_RESOURCE_RUNTIME

**WLM_USER_RESOURCE_RUNTIME** displays resource usage of all users. Only administrators can query this view. This view is valid only when the GUC parameter **use_workload_manager** is set to **on**.

**Table 1** WLM_USER_RESOURCE_RUNTIME columns

| Name              | Type    | Description                                                  |
| :---------------- | :------ | :----------------------------------------------------------- |
| username          | name    | Username                                                     |
| used_memory       | integer | Used memory (unit: MB)                                       |
| total_memory      | integer | Available memory (unit: MB) The value **0** indicates that the available memory is not limited and depends on the maximum memory available in the database. |
| used_cpu          | integer | Number of CPU cores in use                                   |
| total_cpu         | integer | Total number of CPU cores of the Cgroup associated with the user on the node |
| used_space        | bigint  | Used storage space (unit: KB)                                |
| total_space       | bigint  | Available storage space (unit: KB). The value **-1** indicates that the space is not limited. |
| used_temp_space   | bigint  | Used temporary space (reserved column; unit: KB)             |
| total_temp_space  | bigint  | Available temporary space (reserved column; unit: KB). The value **-1** indicates that the maximum temporary storage space is not limited. |
| used_spill_space  | bigint  | Used space for storing spilled data (reserved column; unit: KB) |
| total_spill_space | bigint  | Available space for storing spilled data (reserved column; unit: KB). The value **-1** indicates that the maximum space for storing spilled data is not limited. |
