---
title: STATEMENT_RESPONSETIME_PERCENTILE
summary: STATEMENT_RESPONSETIME_PERCENTILE
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_RESPONSETIME_PERCENTILE

**STATEMENT_RESPONSETIME_PERCENTILE** obtains the response times of 80% and 95% SQL statements in the MogDB.

**Table 1** STATEMENT_RESPONSETIME_PERCENTILE columns

| Name | Type   | Description                                                 |
| :--- | :----- | :---------------------------------------------------------- |
| p80  | bigint | Response time of 80% SQL statements in the MogDB (unit: μs) |
| p95  | bigint | Response time of 95% SQL statements in the MogDB (unit: μs) |
