---
title: STATEMENT_COMPLEX_HISTORY
summary: STATEMENT_COMPLEX_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COMPLEX_HISTORY

**STATEMENT_COMPLEX_HISTORY** displays load management information about a completed job executed on the primary database node. Columns in this view are the same as those in **[Appendices - Table 5](../../../reference-guide/DBE_PERF/appendices/table-5.md)**.
