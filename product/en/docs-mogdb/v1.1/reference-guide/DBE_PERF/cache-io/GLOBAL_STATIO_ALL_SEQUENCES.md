---
title: GLOBAL_STATIO_ALL_SEQUENCES
summary: GLOBAL_STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_ALL_SEQUENCES

**GLOBAL_STATIO_ALL_SEQUENCES** contains every row of each sequence in databases on each node, showing I/O statistics about specific sequences.

**Table 1** GLOBAL_STATIO_ALL_SEQUENCES columns

| **Name**   | **Type** | **Description**                              |
| :--------- | :------- | :------------------------------------------- |
| node_name  | name     | Node name                                    |
| relid      | oid      | OID of the sequence                          |
| schemaname | name     | Name of the schema that the sequence is in   |
| relname    | name     | Sequence name                                |
| blks_read  | bigint   | Number of disk blocks read from the sequence |
| blks_hit   | bigint   | Number of cache hits in the sequence         |
