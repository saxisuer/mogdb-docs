---
title: SUMMARY_STATIO_ALL_INDEXES
summary: SUMMARY_STATIO_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_INDEXES

**SUMMARY_STATIO_ALL_INDEXES** contains every row of each index in databases in MogDB, showing I/O statistics about specific indexes.

**Table 1** SUMMARY_STATIO_ALL_INDEXES columns

| **Name**      | **Type** | **Description**                                 |
| :------------ | :------- | :---------------------------------------------- |
| schemaname    | name     | Name of the schema that the index is in         |
| relname       | name     | Name of the table that the index is created for |
| indexrelname  | name     | Index name                                      |
| idx_blks_read | numeric  | Number of disk blocks read from the index       |
| idx_blks_hit  | numeric  | Number of cache hits in the index               |
