---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-04-19
---

# Overview

In the **DBE_PERF** schema, views are used to diagnose performance issues and are also the data source of WDR snapshots. After the database is installed, only the initial user have permissions for the **DBE_PERF** schema by default. If the database is upgraded from an earlier version, permissions for the **DBE_PERF** schema are the same as those of the earlier version to ensure forward compatibility. Organization views are divided based on multiple dimensions, such as OS, instance, and memory. These views comply with the following naming rules:

- Views starting with **GLOBAL_**: Request data from database nodes and return the data without processing them.
- Views starting with **SUMMARY_**: Summarize data in MogDB. In most cases, data from database nodes (sometimes only the primary database node) is processed, collected, and returned.
- Views that do not start with **GLOBAL_** or **SUMMARY_**: Local views that do not request data from other database nodes.
