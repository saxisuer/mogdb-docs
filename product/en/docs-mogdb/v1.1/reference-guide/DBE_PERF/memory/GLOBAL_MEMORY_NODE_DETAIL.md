---
title: GLOBAL_MEMORY_NODE_DETAIL
summary: GLOBAL_MEMORY_NODE_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_MEMORY_NODE_DETAIL

The memory usage of all normal nodes in MogDB is displayed.

**Table 1** GLOBAL_MEMORY_NODE_DETAIL fields

| **Name**     | **Type** | **Description**                                              |
| :----------- | :------- | :----------------------------------------------------------- |
| nodename     | text     | Node name.                                                   |
| memorytype   | text     | Memory name.<br/>- **max_process_memory**: memory occupied by the MogDB instance<br/>- **process_used_memory**: memory occupied by a process<br/>- **max_dynamic_memory**: maximum dynamic memory<br/>- **dynamic_used_memory**: used dynamic memory<br/>- **dynamic_peak_memory**: dynamic peak value of the memory<br/>- **dynamic_used_shrctx**: maximum dynamic shared memory context<br/>- **dynamic_peak_shrctx**: dynamic peak value of the shared memory context<br/>- **max_shared_memory**: maximum shared memory<br/>- **shared_used_memory**: used shared memory<br/>- **max_cstore_memory**: maximum memory allowed by the column<br/>- **cstore_used_memory**: memory used in column storage<br/>- **max_sctpcomm_memory**: maximum memory size allowed for the SCTP communications<br/>- **sctpcomm_used_memory**: memory used for SCTP communications<br/>- **sctpcomm_peak_memory**: memory peak of SCTP communications<br/>- **other_used_memory**: other used memory<br/>- **gpu_max_dynamic_memory**: maximum dynamic memory of GPU<br/>- **gpu_dynamic_used_memory**: used dynamic memory of GPU<br/>- **gpu_dynamic_peak_memory**: dynamic peak value of the GPU memory<br/>- **pooler_conn_memory**: applied memory in the connection pool<br/>- **pooler_freeconn_memory**: memory occupied by idle connections in the connection pool<br/>- **storage_compress_memory**: memory used by the storage module for compression<br/>- **udf_reserved_memory**: reserved memory for the UDF |
| memorymbytes | integer  | Size of the used memory in the unit of MB.                   |
