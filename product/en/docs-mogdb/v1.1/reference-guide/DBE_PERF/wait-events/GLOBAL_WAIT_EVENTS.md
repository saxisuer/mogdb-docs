---
title: GLOBAL_WAIT_EVENTS
summary: GLOBAL_WAIT_EVENTS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_WAIT_EVENTS

**GLOBAL_WAIT_EVENTS** displays statistics about wait events on each node.

**Table 1** GLOBAL_WAIT_EVENTS columns

| **Name**        | **Type**                 | **Description**                      |
| :-------------- | :----------------------- | :----------------------------------- |
| nodename        | text                     | Database process name                |
| type            | text                     | Event type                           |
| event           | text                     | Event name                           |
| wait            | bigint                   | Number of waiting times              |
| failed_wait     | bigint                   | Number of waiting failures           |
| total_wait_time | bigint                   | Total waiting time (unit: μs)        |
| avg_wait_time   | bigint                   | Average waiting time (unit: μs)      |
| max_wait_time   | bigint                   | Maximum waiting time (unit: μs)      |
| min_wait_time   | bigint                   | Minimum waiting time (unit: μs)      |
| last_updated    | timestamp with time zone | Last time when the event was updated |
