---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-04-19
---

# Overview

Global plan cache (GPC) views are valid only when **enable_global_plancache** and the thread pool are enabled.
