---
title: SESSION_STAT
summary: SESSION_STAT
author: Guo Huan
date: 2021-04-19
---

# SESSION_STAT

**SESSION_STAT** collects statistics about session status on the current node based on session threads or the **AutoVacuum** thread.

**Table 1** SESSION_STAT columns

| Name     | Type    | Description                     |
| :------- | :------ | :------------------------------ |
| sessid   | text    | Thread start time and ID        |
| statid   | integer | Statistics ID                   |
| statname | text    | Name of the statistics session  |
| statunit | text    | Unit of the statistics session  |
| value    | bigint  | Value of the statistics session |
