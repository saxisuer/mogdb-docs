---
title: GLOBAL_SESSION_TIME
summary: GLOBAL_SESSION_TIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_SESSION_TIME

**GLOBAL_SESSION_TIME** collects statistics about the running time of session threads and time consumed in each execution phase on each node.

**Table 1** GLOBAL_SESSION_TIME columns

| **Name**  | **Type** | **Description**          |
| :-------- | :------- | :----------------------- |
| node_name | name     | Node name                |
| sessid    | text     | Thread start time and ID |
| stat_id   | integer  | Statistics ID            |
| stat_name | text     | Session type             |
| value     | bigint   | Session value            |
