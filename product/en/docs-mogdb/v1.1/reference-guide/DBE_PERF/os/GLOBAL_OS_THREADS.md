---
title: GLOBAL_OS_THREADS
summary: GLOBAL_OS_THREADS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_OS_THREADS

**GLOBAL_OS_THREADS** provides status information about threads on all normal nodes in MogDB.

**Table 1** GLOBAL_OS_THREADS columns

| **Name**      | **Type**                 | **Description**                                         |
| :------------ | :----------------------- | :------------------------------------------------------ |
| node_name     | text                     | Node name                                               |
| pid           | bigint                   | ID of the thread running under the current node process |
| lwpid         | integer                  | Lightweight thread ID corresponding to **pid**          |
| thread_name   | text                     | Name of the thread corresponding to **pid**             |
| creation_time | timestamp with time zone | Creation time of the thread corresponding to **pid**    |
