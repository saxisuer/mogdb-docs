---
title: GLOBAL_REPLICATION_STAT
summary: GLOBAL_REPLICATION_STAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REPLICATION_STAT

**GLOBAL_REPLICATION_STAT** displays information about log synchronization status on each node, such as the locations where the sender sends logs and where the receiver receives logs.

**Table 1** GLOBAL_REPLICATION_STAT columns

| **Name**                 | **Type**                 | **Description**                                              |
| :----------------------- | :----------------------- | :----------------------------------------------------------- |
| node_name                | name                     | Node name                                                    |
| pid                      | bigint                   | PID of the thread                                            |
| usesysid                 | oid                      | User system ID                                               |
| usename                  | name                     | Username                                                     |
| application_name         | text                     | Program name                                                 |
| client_addr              | inet                     | Client address                                               |
| client_hostname          | text                     | Client name                                                  |
| client_port              | integer                  | Port of the client                                           |
| backend_start            | timestamp with time zone | Start time of the program                                    |
| state                    | text                     | Log replication status (catch-up or consistent streaming)    |
| sender_sent_location     | text                     | Location where the transmit sends logs                       |
| receiver_write_location  | text                     | Location where the receive end writes logs                   |
| receiver_flush_location  | text                     | Location where the receive end flushes logs                  |
| receiver_replay_location | text                     | Location where the receive end replays logs                  |
| sync_priority            | integer                  | Priority of synchronous duplication (**0** indicates asynchronization.) |
| sync_state               | text                     | Synchronization status:<br/>- Asynchronous replication<br/>- Synchronous replication<br/>- Potential synchronization |
