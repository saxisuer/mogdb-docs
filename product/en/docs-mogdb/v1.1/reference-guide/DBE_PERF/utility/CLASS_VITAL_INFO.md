---
title: CLASS_VITAL_INFO
summary: CLASS_VITAL_INFO
author: Guo Huan
date: 2021-04-19
---

# CLASS_VITAL_INFO

**CLASS_VITAL_INFO** is used to check whether the OIDs of the same table or index are consistent for WDR snapshots.

**Table 1** CLASS_VITAL_INFO columns

| Name       | Type   | Description                                                  |
| :--------- | :----- | :----------------------------------------------------------- |
| relid      | oid    | Table OID                                                    |
| schemaname | name   | Schema name                                                  |
| relname    | name   | Table name                                                   |
| relkind    | "char" | Object type. Its value can be:<br/>- **r**: ordinary table<br/>- **t**: TOAST table<br/>- **i**: index |
