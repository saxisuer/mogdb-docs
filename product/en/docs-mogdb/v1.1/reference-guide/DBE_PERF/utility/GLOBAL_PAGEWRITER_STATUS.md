---
title: GLOBAL_PAGEWRITER_STATUS
summary: GLOBAL_PAGEWRITER_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_PAGEWRITER_STATUS

**GLOBAL_PAGEWRITER_STATUS** displays the page flushing information and checkpoint information about all instances in MogDB.

**Table 1** GLOBAL_PAGEWRITER_STATUS columns

| Name                        | Type    | Description                                                  |
| :-------------------------- | :------ | :----------------------------------------------------------- |
| node_name                   | text    | node name                                                    |
| pgwr_actual_flush_total_num | bigint  | Total number of dirty pages flushed from the startup time to the current time |
| pgwr_last_flush_num         | integer | Number of dirty pages flushed in the previous batch          |
| remain_dirty_page_num       | bigint  | Estimated number of dirty pages that are not flushed         |
| queue_head_page_rec_lsn     | text    | **recovery_lsn** of the first dirty page in the dirty page queue of the current instance |
| queue_rec_lsn               | text    | **recovery_lsn** of the dirty page queue of the current instance |
| current_xlog_insert_lsn     | text    | Write position of Xlogs in the current instance              |
| ckpt_redo_point             | text    | Checkpoint of the current instance                           |
