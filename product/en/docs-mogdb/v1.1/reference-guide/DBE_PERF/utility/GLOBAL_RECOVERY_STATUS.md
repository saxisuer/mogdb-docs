---
title: GLOBAL_RECOVERY_STATUS
summary: GLOBAL_RECOVERY_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_RECOVERY_STATUS

**GLOBAL_RECOVERY_STATUS** displays log flow control information about the primary and standby nodes.

**Table 1** GLOBAL_RECOVERY_STATUS columns

| Name               | Type    | Description                                                  |
| :----------------- | :------ | :----------------------------------------------------------- |
| node_name          | text    | Node name (including the primary and standby nodes)          |
| standby_node_name  | text    | Name of the standby node process                             |
| source_ip          | text    | IP address of the primary node                               |
| source_port        | integer | Port number of the primary node                              |
| dest_ip            | text    | IP address of the standby node                               |
| dest_port          | integer | Port number of the standby node                              |
| current_rto        | bigint  | Current log flow control time of the standby node (unit: s)  |
| target_rto         | bigint  | Expected flow control time of the standby node specified by the corresponding GUC parameter (unit: s) |
| current_sleep_time | bigint  | Sleep time required to achieve the expected flow control time (unit: μs) |
