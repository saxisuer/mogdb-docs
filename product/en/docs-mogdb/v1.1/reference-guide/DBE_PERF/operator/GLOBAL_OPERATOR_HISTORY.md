---
title: GLOBAL_OPERATOR_HISTORY
summary: GLOBAL_OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_OPERATOR_HISTORY

**GLOBAL_OPERATOR_HISTORY** displays records of operators in jobs that have been executed by the current user on the primary database node.

**Table 1** GLOBAL_OPERATOR_HISTORY columns<a id="GLOBAL_OPERATOR_HISTORY columns"> </a>

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| queryid             | bigint                   | Internal query ID used for statement execution               |
| pid                 | bigint                   | Thread ID of the backend                                     |
| plan_node_id        | integer                  | Plan node ID of the execution plan                           |
| plan_node_name      | text                     | Name of the operator corresponding to the plan node ID       |
| start_time          | timestamp with time zone | Time when the operator starts to process the first data record |
| duration            | bigint                   | Total execution time of the operator (unit: ms)              |
| query_dop           | integer                  | DOP of the operator                                          |
| estimated_rows      | bigint                   | Number of rows estimated by the optimizer                    |
| tuple_processed     | bigint                   | Number of elements returned by the operator                  |
| min_peak_memory     | integer                  | Minimum peak memory used by the operator on database nodes (unit: MB) |
| max_peak_memory     | integer                  | Maximum peak memory used by the operator on database nodes (unit: MB) |
| average_peak_memory | integer                  | Average peak memory used by the operator on database nodes (unit: MB) |
| memory_skew_percent | integer                  | Memory usage skew of the operator among database nodes       |
| min_spill_size      | integer                  | Minimum spilled data among database nodes when a spill occurs (unit: MB) (default value:**0**) |
| max_spill_size      | integer                  | Maximum spilled data among database nodes when a spill occurs (unit: MB) (default value:**0**) |
| average_spill_size  | integer                  | Average spilled data among database nodes when a spill occurs (unit: MB) (default value:**0**) |
| spill_skew_percent  | integer                  | Database node spill skew when a spill occurs                 |
| min_cpu_time        | bigint                   | Minimum execution time of the operator on database nodes (unit: ms) |
| max_cpu_time        | bigint                   | Maximum execution time of the operator on database nodes (unit: ms) |
| total_cpu_time      | bigint                   | Total execution time of the operator on database nodes (unit: ms) |
| cpu_skew_percent    | integer                  | Execution time skew among database nodes                     |
| warning             | text                     | Warning. The following warnings are displayed:<br/>- Sort/SetOp/HashAgg/HashJoin spill<br/>- Spill file size large than 256MB<br/>- Broadcast size large than 100MB<br/>- Early spill<br/>- Spill times is greater than 3<br/>- Spill on memory adaptive<br/>- Hash table conflict |
