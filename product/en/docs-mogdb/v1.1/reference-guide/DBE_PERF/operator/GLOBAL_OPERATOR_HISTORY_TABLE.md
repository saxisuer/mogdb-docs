---
title: GLOBAL_OPERATOR_HISTORY_TABLE
summary: GLOBAL_OPERATOR_HISTORY_TABLE
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_OPERATOR_HISTORY_TABLE

**GLOBAL_OPERATOR_HISTORY_TABLE** displays the records about operators of completed jobs on the primary database node. Data is dumped from the kernel to the system catalog **GS_WLM_OPERATOR_INFO**. **GLOBAL_OPERATOR_HISTORY_TABLE** is a collection view for querying the system catalog **GS_WLM_OPERATOR_INFO** on the primary database node. Columns in this view are the same as those in **[GLOBAL_OPERATOR_HISTORY -> Table 1](GLOBAL_OPERATOR_HISTORY.md)**.
