---
title: GLOBAL_STAT_BAD_BLOCK
summary: GLOBAL_STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_BAD_BLOCK

**GLOBAL_STAT_BAD_BLOCK** displays information about table and index read failures on each node.

**Table 1** GLOBAL_STAT_BAD_BLOCK columns

| **Name**     | **Type**                 | **Description**                        |
| :----------- | :----------------------- | :------------------------------------- |
| node_name    | text                     | Node name                              |
| databaseid   | integer                  | OID of the database                    |
| tablespaceid | integer                  | OID of the tablespace                  |
| relfilenode  | integer                  | File node of this relation             |
| forknum      | integer                  | Fork number                            |
| error_count  | integer                  | Number of errors                       |
| first_time   | timestamp with time zone | Time when the first bad block occurred |
| last_time    | timestamp with time zone | Time when the last bad block occurred  |
