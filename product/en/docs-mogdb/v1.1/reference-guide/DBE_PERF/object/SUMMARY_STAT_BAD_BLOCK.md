---
title: SUMMARY_STAT_BAD_BLOCK
summary: SUMMARY_STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_BAD_BLOCK

**SUMMARY_STAT_BAD_BLOCK** displays information about table and index read failures in MogDB.

**Table 1** SUMMARY_STAT_BAD_BLOCK columns

| **Name**     | **Type**                 | **Description**                        |
| :----------- | :----------------------- | :------------------------------------- |
| databaseid   | integer                  | OID of the database                    |
| tablespaceid | integer                  | OID of the tablespace                  |
| relfilenode  | integer                  | File node of this relation             |
| forknum      | bigint                   | Fork number                            |
| error_count  | bigint                   | Number of errors                       |
| first_time   | timestamp with time zone | Time when the first bad block occurred |
| last_time    | timestamp with time zone | Time when the last bad block occurred  |
