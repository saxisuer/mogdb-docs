---
title: SUMMARY_STAT_DATABASE
summary: SUMMARY_STAT_DATABASE
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_DATABASE

**SUMMARY_STAT_DATABASE** contains every row of each database in the cluster, showing database-wide statistics.

**Table 1** SUMMARY_STAT_DATABASE

| **Name**       | **Type**                 | **Description**                                              |
| :------------- | :----------------------- | :----------------------------------------------------------- |
| datname        | name                     | Name of the database                                         |
| numbackends    | bigint                   | Number of backends currently connected to this database. This is the only column in this view that returns a value reflecting the current state; all other columns return the accumulated values since the last reset. |
| xact_commit    | numeric                  | Number of transactions in this database that have been committed |
| xact_rollback  | numeric                  | Number of transactions in this database that have been rolled back |
| blks_read      | numeric                  | Number of disk blocks read in this database                  |
| blks_hit       | numeric                  | Number of times disk blocks were found in the buffer cache (unnecessary as the number includes only hits in the PostgreSQL buffer cache) |
| tup_returned   | numeric                  | Number of rows returned by queries in this database          |
| tup_fetched    | numeric                  | Number of rows fetched by queries in this database           |
| tup_inserted   | bigint                   | Number of rows inserted by queries in this database          |
| tup_updated    | bigint                   | Number of rows updated by queries in this database           |
| tup_deleted    | bigint                   | Number of rows deleted by queries in this database           |
| conflicts      | bigint                   | Number of queries canceled due to database recovery conflicts (conflicts occurring only on the standby server). For details, see STAT_DATABASE_CONFLICTS. |
| temp_files     | numeric                  | Number of temporary files created by queries in this database. All temporary files are counted, regardless of why the temporary file was created (for example, sorting or hashing), and regardless of the **log_temp_files** setting. |
| temp_bytes     | numeric                  | Total amount of data written to temporary files by queries in this database. All temporary files are counted, regardless of why the temporary file was created, and regardless of the **log_temp_files** setting. |
| deadlocks      | bigint                   | Number of deadlocks detected in this database                |
| blk_read_time  | double precision         | Time spent reading data file blocks by backends in this database (unit: ms) |
| blk_write_time | double precision         | Time spent writing into data file blocks by backends in this database (unit: ms) |
| stats_reset    | timestamp with time zone | Time at which the current statistics were reset              |
