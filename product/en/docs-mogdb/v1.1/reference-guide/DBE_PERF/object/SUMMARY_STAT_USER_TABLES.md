---
title: SUMMARY_STAT_USER_TABLES
summary: SUMMARY_STAT_USER_TABLES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_USER_TABLES

**SUMMARY_STAT_USER_TABLES** displays the status information about user-defined ordinary tables in all namespaces in MogDB.

**Table 1** SUMMARY_STAT_USER_TABLES columns

| **Name**          | **Type**                 | **Description**                                              |
| :---------------- | :----------------------- | :----------------------------------------------------------- |
| schemaname        | name                     | Name of the schema that contains the table                   |
| relname           | name                     | Table name                                                   |
| seq_scan          | numeric                  | Number of sequential scans initiated on this table           |
| seq_tup_read      | numeric                  | Number of live rows fetched by sequential scans              |
| idx_scan          | numeric                  | Number of index scans initiated on the table                 |
| idx_tup_fetch     | numeric                  | Number of live rows fetched by index scans                   |
| n_tup_ins         | numeric                  | Number of rows inserted                                      |
| n_tup_upd         | numeric                  | Number of rows updated                                       |
| n_tup_del         | numeric                  | Number of rows deleted                                       |
| n_tup_hot_upd     | numeric                  | Number of rows HOT updated (with no separate index update required) |
| n_live_tup        | numeric                  | Estimated number of live rows                                |
| n_dead_tup        | numeric                  | Estimated number of dead rows                                |
| last_vacuum       | timestamp with time zone | Last time at which this table was manually vacuumed (not counting **VACUUM FULL**) |
| last_autovacuum   | timestamp with time zone | Last time at which this table was vacuumed by the autovacuum daemon |
| last_analyze      | timestamp with time zone | Last time at which this table was manually analyzed          |
| last_autoanalyze  | timestamp with time zone | Last time at which this table was analyzed by the autovacuum daemon |
| vacuum_count      | numeric                  | Number of times the table has been manually vacuumed (not counting **VACUUM FULL**) |
| autovacuum_count  | numeric                  | Number of times the table has been vacuumed by the autovacuum daemon |
| analyze_count     | numeric                  | Number of times the table has been manually analyzed         |
| autoanalyze_count | numeric                  | Number of times the table has been analyzed by the autovacuum daemon |
