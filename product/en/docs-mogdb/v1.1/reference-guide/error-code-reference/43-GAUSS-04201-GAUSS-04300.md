---
title: GAUSS-04201 - GAUSS-04300
summary: GAUSS-04201 - GAUSS-04300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04201 - GAUSS-04300

<br/>

## GAUSS-04201 - GAUSS-04210

<br/>

GAUSS-04201: "unsupported portal strategy"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04202: "bogus direction"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04203: "invalid argument size %d in function call message"

SQLSTATE: 08P01

Description: An invalid parameter value (**argsize** is less than -1) is entered when the function is called.

Solution: Check the number of parameters called by the function.

GAUSS-04204: "function %s has more than %d arguments"

SQLSTATE: XX000

Description: The SQL parameter is incorrect.

Solution: Modify the SQL statement.

GAUSS-04205: "current transaction is aborted, commands ignored until end of transaction block"

SQLSTATE: 25P02

Description: Non-commit and rollback commands are run when the transaction is rolled back.

Solution: Ensure that the previous transaction has been submitted or rolled back when the SQL statement is executed.

GAUSS-04206: "function call message contains %d arguments but function requires %d"

SQLSTATE: 08P01

Description: When the function is called, the number of parameters entered is inconsistent with that defined by the function.

Solution: Check the number of parameters called by the function.

GAUSS-04207: "function call message contains %d argument formats but %d arguments"

SQLSTATE: 08P01

Description: When the function is called, the number of formatted parameters entered is inconsistent with that defined by the function.

Solution: Check the number of parameters called by the function.

GAUSS-04208: "incorrect binary data format in function argument %d"

SQLSTATE: 22P03

Description: Incorrect binary data format is found in the parameters of the function.

Solution: Check the format of parameters called by the function.

GAUSS-04209: "invalid frontend message type %d"

SQLSTATE: 08P01

Description: The message type is invalid.

Solution: Ensure that the input message type is valid.

GAUSS-04210: "could not set timer for session timeout"

SQLSTATE: 58000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04211 - GAUSS-04220

<br/>

GAUSS-04211: "could not disable timer for session timeout"

SQLSTATE: 58000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04214: "bind message has %d parameter formats but %d parameters"

SQLSTATE: 08P01

Description: The number of bound parameters is inconsistent with the number of accepted parameters when the API is used.

Solution: Ensure that the number of accepted parameters is consistent with the number of predicted parameters.

GAUSS-04215: "cannot insert multiple commands into a prepared statement"

SQLSTATE: 42601

Description: Multiple commands are inserted in the prepared statement.

Solution: Insert only one command.

GAUSS-04217: "bind message supplies %d parameters, but prepared statement '%s' requires %d"

SQLSTATE: 08P01

Description: The number of parameters provided by the bind message is incorrect.

Solution: Provide the correct parameter.

GAUSS-04218: "incorrect binary data format in bind parameter %d"

SQLSTATE: 22P03

Description: Incorrect binary data is found in the bind parameter.

Solution: Provide the correct parameter.

GAUSS-04219: "portal '%s' does not exist"

SQLSTATE: 34000

Description: The portal does not exist.

Solution: Specify a correct portal name.

GAUSS-04220: "floating-point exception"

SQLSTATE: 22P01

Description: The floating point number is incorrect. For example, the value is out of range or divided by 0.

Solution: Enter a valid floating point number.

<br/>

## GAUSS-04221 - GAUSS-04230

<br/>

GAUSS-04221: "unrecognized conflict mode: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04222: "terminating autovacuum process due to administrator command"

SQLSTATE: 57P01

Description: The **autovacuum** process is terminated by the administrator.

Solution: Contact the system administrator to check whether the **autovacuum** process is terminated. Let the administrator to restart the process if it has been terminated.

GAUSS-04223: "terminating connection due to conflict with recovery"

SQLSTATE: 40001

Description: The current request conflicts with the data recovery process performed on the database server. The requested connection fails.

Solution: Try to reconnect the database later and run the command again.

GAUSS-04224: "terminating connection due to conflict with recovery"

SQLSTATE: 57P04

Description: The current request conflicts with the data recovery process performed on the database server. The requested connection fails.

Solution: Try to reconnect the database later and run the command again.

GAUSS-04225: "terminating connection due to administrator command"

SQLSTATE: 57P01

Description: The administrator terminates the current connection.

Solution: Contact the system administrator to obtain the reason why the connection is terminated.

GAUSS-04226: "connection to client lost"

SQLSTATE: 08006

Description: The connection is lost.

Solution: Check whether the network connection is normal.

GAUSS-04229: "canceling autovacuum task"

SQLSTATE: 57014

Description: The **autovauum** task is cancelled.

Solution: Find the reason why the process is cancelled.

GAUSS-04230: "canceling statement due to conflict with recovery"

SQLSTATE: 40001

Description: The connection is cancelled due to the conflict with recovery.

Solution: Reconnect to the database and enter the command again.

<br/>

## GAUSS-04231 - GAUSS-04240

<br/>

GAUSS-04231: "canceling statement due to %s request"

SQLSTATE: 57014

Description: The user or a CN cancels this operation.

Solution: Ensure that the reason to cancel the operation is valid. If the operation needs to be canceled, ignore the error. If the operation cancellation is abnormal, analyze the error and rectify it.

GAUSS-04232: "stack depth limit exceeded"

SQLSTATE: 54001

Description: The stack depth exceeds the upper limit, that is, the value of **max_stack_depth_bytes** (the default value is **100x1024**).

Solution: Ensure that the depth of the recursive call function is proper. If the depth is out of limit, expand **max_stack_depth_bytes**.

GAUSS-04233: "local xids can be used only in single user mode"

SQLSTATE: 42601

Description: The local XID can be used only in single-user mode.

Solution: The **-single** parameter needs to be added.

GAUSS-04235: "invalid command-line argument for server process: %s"

SQLSTATE: 42601

Description: An invalid command-line parameter is entered for the server process.

Solution: Use **-help** to check the parameter function and enter the correct parameter.

GAUSS-04236: "%s: invalid command-line argument: %s"

SQLSTATE: 42601

Description: An invalid command-line parameter is entered when the database is being connected.

Solution: Use **-help** to check the parameter function and enter the correct parameter.

GAUSS-04238: "%s: no database nor user name specified"

SQLSTATE: 22023

Description: Neither the database nor the username is specified when the database is being connected.

Solution: Add the database and username parameter.

GAUSS-04239: "invalid CLOSE message subtype %d"

SQLSTATE: 08P01

Description: An invalid **CLOSE** command is entered in the front end or the standard input.

Solution: Ensure the type of **CLOSE** is **S** or **P**.

GAUSS-04240: "invalid DESCRIBE message subtype %d"

SQLSTATE: 08P01

Description: An invalid **DESCRIBE** command is entered in the front end or the standard input.

Solution: Ensure the type of **DESCRIBE** is **S** or **P**.

<br/>

## GAUSS-04241 - GAUSS-04250

<br/>

GAUSS-04241: "Invalid command received"

SQLSTATE: XX000

Description: An invalid command capitalized with **b** (not bP, bX, or bE) is entered in the front end or the standard input.

Solution: Enter the correct command.

GAUSS-04242: "cannot execute %s in a read-only transaction"

SQLSTATE: 25006

Description: The update database type operation is performed in a read-only transaction.

Solution: Ensure that the update type operation is performed in non-read-only transactions.

GAUSS-04243: "cannot execute %s during recovery"

SQLSTATE: 25006

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04244: "cannot execute %s within security-restricted operation"

SQLSTATE: 42501

Description: Security-restricted operations are executed.

Solution:Contact technical support.

GAUSS-04245: "Invalid transaciton_id to prepare."

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04247: "non-DECLARE CURSOR PlannedStmt passed to ProcessUtility"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04248: "CREATE not supported for TEMP and non-TEMP objects"

SQLSTATE: 0A000

Description: A statement is used for supporting both temporary and non-temporary objects.

Solution: Change the SQL statement and use multiple statements to support temporary and non-temporary objects.

GAUSS-04249: "Postgres-XC does not support FOREIGN DATA WRAPPER yet"

SQLSTATE: 0A000

Description: The **FOREIGN DATA WRAPPER** is not supported.

Solution: Internal system error.Contact technical support.

GAUSS-04250: "Postgres-XC does not support USER MAPPING yet"

SQLSTATE: 0A000

Description: The **CREATE USER MAPPING** is not supported.

Solution: Do not use **CREATE USER MAPPING**.

<br/>

## GAUSS-04251 - GAUSS-04260

<br/>

GAUSS-04251: "PGXC does not support concurrent INDEX yet"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04253: "unrecognized alter domain type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04254: "PGXC does not support GRANT on multiple object types"

SQLSTATE: 0A000

Description: The MogDB **GRANT** syntax does not support performing the view and table operations at the same time.

Solution: Specify only one type of objects (table or view) at a time.

GAUSS-04255: "unrecognized define stmt type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04257: "DROP not supported for TEMP and non-TEMP objects"

SQLSTATE: 0A000

Description: The **DROP** statement cannot drop temporary and non-temporary objects at the same time.

Solution: Drop temporary objects and non-temporary objects respectively.

GAUSS-04258: "%s is not yet supported."

SQLSTATE: 0A000

Description: The features in the blacklist are not supported when the **support_extended_features** switch is set to **off**.

Solution: Set the **support_extended_features** switch to **on** and do not perform the alter or delete operation on the objects in the blacklist.

GAUSS-04260: "Failed to send snapshot to Datanode %u"

SQLSTATE: XX000

Description: Internal system error. Failed to send the snapshot to the DN.

Solution: Internal system error. Contact technical support.

<br/>

## GAUSS-04261 - GAUSS-04270

<br/>

GAUSS-04261: "Failed to send queryid to Datanode %u"

SQLSTATE: XX000

Description: Communication error.

Solution: Check the network status or whether any node fault occurs.

GAUSS-04265: "delete or update failed because lock conflict"

SQLSTATE: XX000

Description: The deletion and update conditions are complex. As a result, the two rows in the same CU are deleted or updated with a long interval, and a lock conflict occurs while a tuple is deleted or updated.

Solution: Modify the deletion and update conditions to simplify execution.

GAUSS-04266: "delete or update row conflict"

SQLSTATE: XX000

Description: Data is deleted or updated multiple times.

Solution: Ensure that no row is deleted or updated multiple times.

GAUSS-04267: "GiST page split into too many halves (%d, maximum %d)"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04270: "PER NODE REJECT LIMIT must be greater than 0"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04271 - GAUSS-04280

<br/>

GAUSS-04271: "Invalid name \'%s\' in REMOTE LOG"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04272: "PER NODE REJECT LIMIT only available on READ ONLY foreign table"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04273: "PER NODE REJECT LIMIT only available with LOG INTO or REMOTE LOG"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04275: "REMOTE LOG only available on in NORMAL mode"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04276: "FILL_MISSING_FIELDS only available on READ ONLY foreign table"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04277: "MODE only available on READ ONLY foreign table"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04279: "can not specify relative local locations"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04280: "could not fetch expected length:%m"

SQLSTATE: XX000

Description: An error occurs when error data is recorded in the error table.

Solution: Locate the data row where the error occurred based on the error context and check whether invalid characters exist. Contact technical support.

<br/>

## GAUSS-04281 - GAUSS-04290

<br/>

GAUSS-04283: "pg_largeobject entry for OID %u, page %d has invalid data field size %d"

SQLSTATE: XX001

Description: The length of the fragment of a large object is invalid and the data is damaged.

Solution: Contact technical support. Alternatively, try using backup data to restore the data.

GAUSS-04284: "exceeded maxAllocatedDescs (%d) while trying to open file '%s:%d'"

SQLSTATE: 53000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04285: "exceeded maxAllocatedDescs (%d) while trying to open file '%s'"

SQLSTATE: 53000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04286: "exceeded maxAllocatedDescs (%d) while trying to open directory '%s'"

SQLSTATE: 53000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04287: "must have at least one column"

SQLSTATE: 0A000

Description: The **CREATE TABLE** syntax does not specify the column information of the table.

Solution: Ensure that at least one column is specified in the **CREATE TABLE ***table_name\***(***{column_name data_type}\***)** syntax.

GAUSS-04290: "length of field '%s' longer than limit of \'%d\'"

SQLSTATE: 22P04

Description: The length of the column in a GDS foreign table exported in FIXED format is greater than the declared table length.

Solution: Modify the foreign table definition based on the error message.

<br/>

## GAUSS-04291 - GAUSS-04300

<br/>

GAUSS-04291: "tsquery is too large"

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04292: "bit string length exceeds the maximum allowed (%d)"

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04293: "language validation function %u called for language %u instead of %u"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04294:"The name of memory context is too long(>=%dbytes)"

SQLSTATE: 42602

Description: The name of the memory context exceeds the limit **MEMORY_CONTEXT_NAME_LEN**.

Solution: Ensure that the memory context is valid.

GAUSS-04295:"Fail to send signal to backend(tid:%lu). "

SQLSTATE: XX000

Description: Failed to send an internal signal to the backend thread.

Solution: The backend may have exited. Let the user check whether the backend exists.

GAUSS-04296:"out of memory when preparing zlib encoder. "

SQLSTATE: 53200

Description: Failed to allocate a large volume of memory required for compression because the system memory is insufficient.

Solution: Release unnecessary processes in the system and import data again.

GAUSS-04297:"level %d is invalid when preparing zlib encoder. "

SQLSTATE: XX000

Description: The zlib compression level is incorrect.

Solution: Use the low or middle compression level during the import.

GAUSS-04298: "version is incompatible when preparing zlib encoder."

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04299:"error %d occurs when preparing zlib encoder. "

SQLSTATE: XX000

Description: System resources are insufficient.

Solution: Release unnecessary memory in the system and import data again.

GAUSS-04300:"RLE decompress failed, expected bytes %d, real size %d "

SQLSTATE: XX001

Description: The data is damaged.

Solution: Use backed up data for restoration.
