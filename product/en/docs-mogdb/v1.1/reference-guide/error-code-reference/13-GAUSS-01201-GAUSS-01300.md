---
title: GAUSS-01201 - GAUSS-01300
summary: GAUSS-01201 - GAUSS-01300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01201 - GAUSS-01300

<br/>

## GAUSS-01201 - GAUSS-01210

<br/>

GAUSS-01201: "can not specify EOL in BINARY mode"

SQLSTATE: 0A000

Description: The terminator fails to be specified in BINARY mode.

Solution: Do not use terminators in BINARY mode.

GAUSS-01202: "FIX specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42000

Description:**FIX** is not used in **COPY FROM** or foreign table scenarios.

Solution: Use **FIX** only in **COPY FROM** or foreign table scenarios.

GAUSS-01203: "FILEHEADER specification only available using HEAD"

SQLSTATE: 42601

Description: The file to read HEADER information from is specified, but whether this information is required is not specified.

Solution: Specify **HEADER** in the export statement or remove the name of the file to be read.

GAUSS-01204: "table '%s' does not have OIDs"

SQLSTATE: 42703

Description: The OID item is specified when data is imported, but there is no such OID column in the user table.

Solution: Ensure that the data to be imported has OIDs consistent with the user table definition. Delete the imported OID item or add an OID column to the user table.

GAUSS-01206: "unexpected rewrite result"

SQLSTATE: XX000

Description: Internal system error. The imported SQL statement is not rewritten as expected.

Solution: Runtime error. Contact technical support.

GAUSS-01207: "COPY (SELECT INTO) is not supported"

SQLSTATE: 0A000

Description:**COPY SELECT INTO** is not supported.

Solution: Do not use **SELECT INTO** in **COPY**.

GAUSS-01208: "FORCE QUOTE column '%s' not referenced by COPY"

SQLSTATE: 42P10

Description: In CSV COPY TO mode, quotation marks are not used for non-null values in each specified column.

Solution: Use quotation marks for all not-null values in each specified column. Null values are not quoted.

GAUSS-01209: "FORCE NOT NULL column '%s' not referenced by COPY"

SQLSTATE: 42P10

Description: In CSV COPY FROM mode, the specified column is empty.

Solution: Enter a value for the specified column.

GAUSS-01210: "could not close file '%s': %m"

SQLSTATE: XX000

Description: Failed to close a specified file.

Solution: Ensure that the specified file is not damaged and the permission is sufficient.

<br/>

## GAUSS-01211 - GAUSS-01220

<br/>

GAUSS-01211: "cannot copy from view '%s'"

SQLSTATE: 42809

Description: Failed to copy data from a view.

Solution: Do not copy data from a view.

GAUSS-01212: "cannot copy from foreign table '%s'"

SQLSTATE: 42809

Description: Failed to copy data from a foreign table.

Solution: Do not copy data from a foreign table.

GAUSS-01213: "cannot copy from sequence '%s'"

SQLSTATE: 42809

Description: Failed to copy data from a sequence.

Solution: Do not copy data from a sequence.

GAUSS-01214: "cannot copy from non-table relation '%s'"

SQLSTATE: 42809

Description: Failed to copy data from a non-table relation.

Solution: Do not copy data from a non-table relation.

GAUSS-01215: "relative path not allowed for COPY to file"

SQLSTATE: 42602

Description: The path format of the target file to be copied cannot be a relative path.

Solution: Use an absolute path as a target file path.

GAUSS-01216: "could not open file '%s' for writing: %m"

SQLSTATE: XX000

Description: Failed to open a file.

Solution: Ensure that the file is not damaged and the permission is sufficient.

GAUSS-01217: "'%s' is a directory"

SQLSTATE: 42809

Description: The specified parameter is a directory name but not a file name.

Solution: Specify a file name rather than a directory name in **COPY**.

GAUSS-01218: "could not chmod file '%s' : %m"

SQLSTATE: XX000

Description: Failed to change the file permission.

Solution: Ensure that the permission and user group of the file are correct.

GAUSS-01219: "Invalid file format"

SQLSTATE: XX000

Description: The file format is not supported.

Solution: Use a TXT, binary, or CSV format that is supported during copy.

GAUSS-01220: "null value in column '%s' violates not-null constraint"

SQLSTATE: 23502

Description: A null value is entered in a column having the not-null constraint.

Solution: Enter valid values in columns having the not-null constraint.

<br/>

## GAUSS-01221 - GAUSS-01230

<br/>

GAUSS-01221: "Column Store unsupport CHECK constraint"

SQLSTATE: XX000

Description: Currently, column storage does not support the check constraint.

Solution: Delete the check constraint.

GAUSS-01222: "cannot copy to view '%s'"

SQLSTATE: 42809

Description: Failed to copy data to a view.

Solution: Do not copy data to a view.

GAUSS-01223: "cannot copy to foreign table '%s'"

SQLSTATE: 42809

Description: Failed to copy data to a foreign table.

Solution: Do not copy data to a foreign table.

GAUSS-01224: "cannot copy to sequence '%s'"

SQLSTATE: 42809

Description: Failed to copy data to a sequence.

Solution: Do not copy data to a sequence.

GAUSS-01225: "cannot copy to non-table relation '%s'"

SQLSTATE: 42809

Description: Failed to copy data to a non-table relation.

Solution: Do not copy data to a non-table relation.

GAUSS-01226: "invalid COPY file header (COPY SEND)"

SQLSTATE: 22P04

Description: The copy file header is invalid.

Solution: The file is damaged. Check it.

GAUSS-01227: "Copy failed on a Datanode"

SQLSTATE: 08000

Description: Failed to import data.

Solution: Check logs on DNs to identify the error type and take required measures.

GAUSS-01228: "Missing data when batch insert compressed data !"

SQLSTATE: XX000

Description: Data is lost while compressed data is imported to a row-store table.

Solution: Run **ALTER TABLE** to disable the compression feature of the row-store table and import the data again.

GAUSS-01229: "Non-shippable ROW triggers not supported with COPY"

SQLSTATE: 0A000

Description: Row triggers are used in **COPY**.

Solution: Do not use row triggers in **COPY**.

GAUSS-01230: "could not open file '%s' for reading: %m"

SQLSTATE: XX000

Description: Failed to open a file.

Solution: The file does not exist, the user does not have permission, or the disk space is full. Identify the cause and resolve the problem.

<br/>

## GAUSS-01231 - GAUSS-01240

<br/>

GAUSS-01231: "COPY file signature not recognized"

SQLSTATE: 22P04

Description: Failed to recognize the file signature during copy operation.

Solution: The file is damaged. Check it.

GAUSS-01232: "invalid COPY file header (missing flags)"

SQLSTATE: 22P04

Description: The file header is missing during copy operation.

Solution: The file is damaged. Check it.

GAUSS-01233: "unrecognized critical flags in COPY file header"

SQLSTATE: 22P04

Description: Failed to recognize the file header during copy operation.

Solution: The file is damaged. Check it.

GAUSS-01236: "missing data for OID column"

SQLSTATE: 22P04

Description: The OID value is null during copy operation.

Solution: Ensure that no null value exists in the first column of a file.

GAUSS-01237: "null OID in COPY data"

SQLSTATE: 22P04

Description: The OID value is null during copy operation.

Solution: Ensure that the OID value of a table to which data needs to be imported is not null.

GAUSS-01238: "invalid OID in COPY data"

SQLSTATE: 22P04

Description: The OID value is invalid.

Solution: Check the **COPY** statement and ensure that columns correspond to actual data.

GAUSS-01239: "missing data for column '%s'"

SQLSTATE: 22P04

Description: In the **COPY** statement, the number of columns expected to be imported is greater than the actual number of columns. In this case, some columns lack data.

Solution: Check the **COPY** statement and ensure that the number of columns expected to be imported is consistent with the actual number of columns.

GAUSS-01240: "received copy data after EOF marker"

SQLSTATE: 22P04

Description: Data is obtained after the EOF marker occurs because the marker is not defined in the previous protocol.

Solution: Ensure that the size of data defined in the current protocol does not exceed the expected one.

<br/>

## GAUSS-01241 - GAUSS-01250

<br/>

GAUSS-01241: "row field count is %d, expected %d"

SQLSTATE: 22P04

Description: The number of expected attributes is inconsistent with the actual number.

Solution: Internal error. Contact technical support. If the number of attributes in the target table is inconsistent with that in the source table, modify the column number to ensure that the number of columns in the source file is consistent with that in the target file. "

GAUSS-01242: "literal carriage return found in data"

SQLSTATE: 22P04

Description: A carriage return character exists in the copied data.

Solution: Ensure that no carriage return character exists in the copied data.

GAUSS-01243: "literal newline found in data"

SQLSTATE: 22P04

Description: Unrecognized data format exists in the CSV file.

Solution: Ensure that the data format and content meet the constraints.

GAUSS-01244: "end-of-copy marker does not match previous newline style"

SQLSTATE: 22P04

Description: Abnormal characters exist in the file.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01245: "end-of-copy marker corrupt"

SQLSTATE: 22P04

Description: Abnormal characters exist in the file.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01246: "unterminated CSV quoted field"

SQLSTATE: 22P04

Description: Abnormal characters exist in the file.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01247: "unexpected EOF in COPY data"

SQLSTATE: 22P04

Description: The copied file cannot be read.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01248: "invalid field size"

SQLSTATE: 22P04

Description: Invalid data size is detected when the copied file is read.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01249: "incorrect binary data format"

SQLSTATE: 22P03

Description: Incorrect binary data format exists.

Solution: Ensure that the data format is valid. For details, see en-us_topic_0237122096.html.

GAUSS-01250: "Failed to initialize Datanodes for COPY"

SQLSTATE: 08000

Description: Failed to initialize connections between DNs during copy operation.

Solution: Check the following items in sequence: 1) whether the cluster is in normal state; 2) whether the connection between nodes is normal; 3) whether the network connection between current nodes is normal; 4) whether memory is sufficient on all nodes and no OOM problem occurs.

<br/>

## GAUSS-01251 - GAUSS-01260

<br/>

GAUSS-01251: "could not read symbolic link '%s': %m"

SQLSTATE: XX000

Description: Failed to read a symbolic link file.

Solution: Ensure that the symbolic link file is correct.

GAUSS-01252: "symbolic link '%s' target is too long"

SQLSTATE: XX000

Description: The length of the symbolic link file name is too long.

Solution: Ensure that the symbolic link file is correct.

GAUSS-01253: "Invalid URL '%s' in LOCATION"

SQLSTATE: XX000

Description: Invalid URL exists in GDS.

Solution: Ensure that the URL value and format are correct.

GAUSS-01254: "unrecognized URL '%s'"

SQLSTATE: XX000

Description: Invalid URL exists in GDS.

Solution: Ensure that the URL value and format are correct.

GAUSS-01255: "wrong URL format '%s'"

SQLSTATE: XX000

Description: Invalid URL exists in GDS.

Solution: Ensure that the URL value and format are correct.

GAUSS-01256: "unable to open file '%s'"

SQLSTATE: XX000

Description: Failed to open a file.

Solution: Ensure that the file format and permission are correct.

GAUSS-01257: "unable to fseek file '%s'"

SQLSTATE: XX000

Description: Failed to set the file pointer position.

Solution: Ensure that the file format and permission are correct.

GAUSS-01258: "no files found to import"

SQLSTATE: XX000

Description: No files are imported. An error is reported when **raise_errors_if_no_files** is enabled.

Solution: Disable **raise_errors_if_no_files**.

GAUSS-01259: "%s"

SQLSTATE: XX000

Description: Syntax error.

Solution: Ensure that the SQL syntax is correct.

GAUSS-01260: "Incomplete Message from GDS ."

SQLSTATE: XX000

Description: Unexpected message data is received from GDS.

Solution: Check the network running status, for example, whether the packet loss rate is high. If the network is running properly, an internal error occurs. Contact technical support.

<br/>

## GAUSS-01261 - GAUSS-01270

<br/>

GAUSS-01261: "unimplemented bulkload mode"

SQLSTATE: XX000

Description: The import mode is not supported.

Solution: Select the normal, shared, or private mode.

GAUSS-01262: "relative path not allowed for writable foreign table file"

SQLSTATE: 42602

Description: A relative path is used in the writable foreign table.

Solution: Use an absolute path in the writable foreign table.

GAUSS-01264: "Found invalid error recored"

SQLSTATE: XX000

Description: Invalid data records exist in tuples.

Solution: Locate the data row where the error occurred based on the error context and check whether invalid characters exist. If they do not, an internal error occurred. Contact technical support.

GAUSS-01265: "could not cache error info:%m"

SQLSTATE: XX000

Description: Failed to cache error information.

Solution: Locate the data row where the error occurred based on the error context and check whether invalid characters exist. If they do not, an internal error occurred. Contact technical support.

GAUSS-01266: "could not fetch error record:%m"

SQLSTATE: XX000

Description: Failed to obtain error information.

Solution: Locate the data row where the error occurred based on the error context and check whether invalid characters exist. If they do not, an internal error occurred. Contact technical support.

GAUSS-01267: "incomplete error record"

SQLSTATE: XX000

Description: Incomplete error information.

Solution: Locate the data row where the error occurred based on the error context and check whether invalid characters exist. If they do not, an internal error occurred. Contact technical support.

GAUSS-01268: "access method '%s' does not exist"

SQLSTATE: 42704

Description: Tuples cannot be obtained from the system cache.

Solution: System catalog information is incorrect. Contact technical support.

GAUSS-01269: "must specify at least one column"

SQLSTATE: 42P17

Description: No column is specified when you create an index.

Solution: Specify at least one column when you create an index.

GAUSS-01270: "cannot use more than %d columns in an index"

SQLSTATE: 54011

Description: The number of specified columns in an index cannot exceed 32.

Solution: Specify a maximum of 32 columns in an index.

<br/>

## GAUSS-01271 - GAUSS-01280

<br/>

GAUSS-01271: "non-partitioned table does not support local partitioned indexes "

SQLSTATE: 0A000

Description: Local partitioned indexes cannot be created in non-partitioned tables.

Solution: To create a partitioned index in local mode, rebuild the base table as a partitioned table.

If you do not need to create a partitioned index in local mode, delete the **local** parameter from **Create unique index…local**.

GAUSS-01274: "cannot create index on foreign table '%s'"

SQLSTATE: 42809

Description: Indexes cannot be created on foreign tables.

Solution: Do not create indexes on foreign tables.

GAUSS-01275: "cannot create indexes on temporary tables of other sessions"

SQLSTATE: 0A000

Description: Indexes cannot be created on temporary tables of other sessions.

Solution: Do not create indexes on temporary tables of other sessions.

GAUSS-01276: "when creating partitioned index, get table partitions failed"

SQLSTATE: XX000

Description: Failed to obtain partitioned tables during the creation of partitioned indexes.

Solution: Check whether the partition definition of partitioned tables is correct. If it is not, rebuild partitioned tables.

GAUSS-01278: "number of partitions of LOCAL index must equal that of the underlying table"

SQLSTATE: 42P17

Description: The number of partitions in a partitioned table is smaller than the number of partitioned indexes.

Solution: Rebuild the partitioned table to ensure that the number of partitions is equal to the number of partitioned indexes.

GAUSS-01279: "unique index columns must contain the partition key"

SQLSTATE: 42P17

Description: The unique index columns do not contain a partition key.

Solution: Ensure that the unique index columns contain a partition key.

GAUSS-01280: "unique index columns must contain the partition key and collation must be default collation"

SQLSTATE: 42P17

Description: The unique index columns do not contain a partition key.

Solution: Ensure that the unique index columns contain a partition key and the default sorting mode is used.

<br/>

## GAUSS-01281 - GAUSS-01290

<br/>

GAUSS-01281: "access method '%s' does not support unique indexes"

SQLSTATE: 0A000

Description: Unique indexes are not supported.

Solution: Do not create unique indexes described in the error message.

GAUSS-01282: "access method '%s' does not support multicolumn indexes"

SQLSTATE: 0A000

Description: Combination indexes are not supported.

Solution: Do not create unique indexes described in the error message.

GAUSS-01283: "access method '%s' does not support exclusion constraints"

SQLSTATE: 0A000

Description: Exclusion constraints are not supported.

Solution: Do not add exclusive constraints described in the error message.

GAUSS-01284: "Cannot create index whose evaluation cannot be enforced to remote nodes"

SQLSTATE: 0A000

Description: MogDB primary key constraint takes effect through a unique B-tree index. The index cannot be created if the primary key constraint does not contain a distribution column.

Solution: Check whether the primary key constraint contains a distribution column.

GAUSS-01285: "unknown constraint type"

SQLSTATE: XX000

Description: The constraint type defined during index creation is incorrect.

Solution: The keyword for creating an index can only be **PRIMARY KEY**, **UNIQUE**, or **EXCLUDE**. Use a one of them to rebuild the index.

GAUSS-01286: "fail to get index info when create index partition"

SQLSTATE: 42P17

Description: Failed to obtain indexes during the creation of partitioned indexes.

Solution: Re-create the partition index. If the system still displays a failure message, an internal error occurs. Contact technical support.

GAUSS-01287: "index partition with name '%s' already exists"

SQLSTATE: 42704

Description: The name of a partitioned index exists.

Solution: Modify the name of a partitioned index to avoid duplication.

GAUSS-01288: "unsupport partitioned strategy"

SQLSTATE: 42P17

Description: The partitioning policy is not supported.

Solution: Currently, the partitioned index can be created only for RANGE or INTERNAL partitioned tables. Change the partitioned table type to **RANGE** or **INTERNAL** and rebuild the partitioned index.

GAUSS-01289: "collation '%s' does not exist"

SQLSTATE: 42P17

Description: The sorting mode of indexes does not exist.

Solution: Ensure that the sorting mode of indexes is correct.

GAUSS-01290: "cannot use subquery in index predicate"

SQLSTATE: 0A000

Description: A subquery is used in an expression index.

Solution: Do not use a subquery in an expression index.

<br/>

## GAUSS-01291 - GAUSS-01300

<br/>

GAUSS-01291: "cannot use aggregate in index predicate"

SQLSTATE: 42803

Description: Aggregate operations are performed in an expression index.

Solution: Do not use an aggregate function in an expression index.

GAUSS-01292: "functions in index predicate must be marked IMMUTABLE"

SQLSTATE: 42P17

Description: A volatile function is used in an expression index.

Solution: Do not use a volatile function in an expression index.

GAUSS-01293: "cannot use subquery in index expression"

SQLSTATE: 0A000

Description: A subquery is used in an expression index.

Solution: Do not use a subquery in an expression index.

GAUSS-01294: "cannot use aggregate function in index expression"

SQLSTATE: 42803

Description: Aggregate operations are performed in an expression index.

Solution: Do not use an aggregate function in an expression index.

GAUSS-01295: "functions in index expression must be marked IMMUTABLE"

SQLSTATE: 42P17

Description: A volatile function is used in an expression index.

Solution: Do not use a volatile function in an expression index.

GAUSS-01296: "could not determine which collation to use for index expression"

SQLSTATE: 42P22

Description: The sorting mode cannot be determined in an expression index.

Solution: Specify the sorting mode during the creation of an expression index.

GAUSS-01297: "operator %s is not commutative"

SQLSTATE: 42809

Description: Operators are not commutative.

Solution: Use commutative operators in exclusive constraints.

GAUSS-01298: "cache lookup failed for opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01299: "operator %s is not a member of operator family '%s'"

SQLSTATE: 42809

Description: The operator is not a member of an expected operator family.

Solution: Use an operator which is a member of an expected operator family.

GAUSS-01300: "access method '%s' does not support ASC/DESC options"

SQLSTATE: 0A000

Description: ASC or DESC options are used to process indexes.

Solution: Rebuild the index and set the access method (**amcanorder** in **pg_am**) of the index column to true.
