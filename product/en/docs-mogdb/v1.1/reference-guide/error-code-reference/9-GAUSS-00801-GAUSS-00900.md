---
title: GAUSS-00801 - GAUSS-00900
summary: GAUSS-00801 - GAUSS-00900
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00801 - GAUSS-00900

<br/>

## GAUSS-00801 - GAUSS-00810

<br/>

GAUSS-00801: "null conpfeqop for constraint %u"

SQLSTATE: XX005

Description: The constraint attribute is null.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00802: "conpfeqop is not a 1-D Oid array"

SQLSTATE: 42804

Description: The constraint array is not one-dimensional.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00803: "cannot change owner of sequence '%s'"

SQLSTATE: 0A000

Description: When **ALTER TABLE OWNER** is run, the owner of a sequence cannot be changed by the owner itself.

Solution: Cancel this operation.

GAUSS-00804: "'%s' is a composite type"

SQLSTATE: 42809

Description: When **ALTER TABLE OWNER** is run, the owner of a composite type cannot be changed.

Solution: Cancel this operation.

GAUSS-00805: "'%s' is not a table, view, sequence, or foreign table"

SQLSTATE: 42809

Description: When **ALTER TABLE OWNER** is run, the object whose owner to be changed is not a common table, view, sequence, or foreign table.

Solution: Modify the object to a common table, view, sequence, or foreign table.

GAUSS-00806: "index '%s' for table '%s' does not exist"

SQLSTATE: 42704

Description: When **ALTER TABLE CLUSTER ON** is run, the index for a table does not exist.

Solution: Cancel this operation.

GAUSS-00807: "cannot have multiple SET TABLESPACE subcommands"

SQLSTATE: 42601

Description: When **ALTER TABLE SET TABLESPACE** is run, multiple **SET TABLESPACE** sub-commands are run.

Solution: Run one **SET TABLESPACE** sub-command at a time.

GAUSS-00808: "relkind of psort tuple shouldn't be '%c'."

SQLSTATE: XX000

Description: The table type related to psort is incorrect.

Solution: Check whether the table is column-store.

GAUSS-00809: "psort tuple doesn't have the correct ORIENTATION value."

SQLSTATE: XX000

Description: The table type related to psort is incorrect.

Solution: Check whether the table is column-store.

GAUSS-00810: "cache lookup failed for relation %u"

SQLSTATE: XX000

Description: When **ALTER TABLE SET TABLESPACE** is run, the relational table does not exist.

Solution: Modify this operation.

<br/>

## GAUSS-00811 - GAUSS-00820

<br/>

GAUSS-00811: "'%s' is not a table, index, or TOAST table"

SQLSTATE: 42809

Description: The set, reset, replace, or reloptions operation is not performed on a table, index, or a toast table.

Solution: Perform this operation on a table, index, or toast table only.

GAUSS-00812: "can not set tablespace for partition of neither table nor index"

SQLSTATE: 42601

Description: The tablespace is set for objects other than tables and indexes in a partitioned table.

Solution: Perform this operation on a table or index only.

GAUSS-00816: "cache lookup failed for partition %u"

SQLSTATE: XX000

Description: Failed to find the partition in the cache.

Solution: Ensure that the partition is valid.

GAUSS-00817: "cannot move system relation '%s'"

SQLSTATE: 0A000

Description: When **ALTER TABLE SET TABLESPACE** is run, the system catalog cannot be moved.

Solution: Cancel this operation.

GAUSS-00819: "cannot change inheritance of typed table"

SQLSTATE: 42809

Description: When **ALTER TABLE INHERIT** is run, the table to which a parent table is added is a typed table.

Solution: Cancel this operation.

GAUSS-00820: "relation '%s' is a partitioned table"

SQLSTATE: XX000

Description: When **ALTER TABLE INHERIT** is run, the table to which a parent table is added is a partitioned table.

Solution: Cancel this operation.

<br/>

## GAUSS-00821 - GAUSS-00830

<br/>

GAUSS-00821: "cannot inherit to temporary relation of another session"

SQLSTATE: 42809

Description: When **ALTER TABLE INHERIT** is run, the temporary relational table of another session cannot be inherited.

Solution: Cancel this operation.

GAUSS-00822: "circular inheritance not allowed"

SQLSTATE: 42P07

Description: When **ALTER TABLE INHERIT** is run, circular inheritance is forbidden.

Solution: Cancel this operation.

GAUSS-00823: "table '%s' without OIDs cannot inherit from table '%s' with OIDs"

SQLSTATE: 42809

Description: When **ALTER TABLE INHERIT** is run, tables without OIDs cannot inherit other tables.

Solution: Add OIDs to tables.

GAUSS-00824: "column '%s' in child table must be marked NOT NULL"

SQLSTATE: 42804

Description: When **ALTER TABLE INHERIT** is run, a parent table has the NOT NULL constraint, but the column of a child table does not have the constraint.

Solution: Add the NOT NUll constraint to the column attribute of the child table.

GAUSS-00825: "child table is missing column '%s'"

SQLSTATE: 42804

Description: When **ALTER TABLE INHERIT** is run, a child table lacking a column cannot inherit a parent table.

Solution: Add the missing column to the child table, or cancel this operation.

GAUSS-00826: "child table '%s' has different definition for check constraint '%s'"

SQLSTATE: 42804

Description: When **ALTER TABLE INHERIT** is run, a child table and a parent table have different definitions on a constraint.

Solution: Ensure that the parent and child tables have the same definition on a constraint.

GAUSS-00827: "constraint '%s' conflicts with non-inherited constraint on child table '%s'"

SQLSTATE: 42P17

Description: When **ALTER TABLE INHERIT** is run, the constraint on a parent table conflicts with the non-inherited constraint on a child table.

Solution: Modify the constraint on the parent or child table.

GAUSS-00828: "child table is missing constraint '%s'"

SQLSTATE: 42804

Description: When **ALTER TABLE INHERIT** is run, a constraint is missing in a child table.

Solution: Modify this operation.

GAUSS-00829: "relation '%s' is not a parent of relation '%s'"

SQLSTATE: 42P01

Description: When **ALTER TABLE NO INHERIT** is run to delete parent table constraints on a child table, the child table does not have the parent table relationship.

Solution: Do not delete the constraints directly.

GAUSS-00830: "typed tables cannot inherit"

SQLSTATE: 42809

Description: When **ALTER TABLE OF** is run, the inherited table cannot be of composite type.

Solution: Cancel this operation.

<br/>

## GAUSS-00831 - GAUSS-00840

<br/>

GAUSS-00831: "table is missing column '%s'"

SQLSTATE: 42804

Description: The column does not exist.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00832: "table has column '%s' where type requires '%s'"

SQLSTATE: 42804

Description: The column names of tables and types are inconsistent.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00833: "table '%s' has different type for column '%s'"

SQLSTATE: 42804

Description: The attributes of tables and types are inconsistent.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00834: "table has extra column '%s'"

SQLSTATE: 42804

Description: The table contains redundant columns.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00835: "'%s' is not a typed table"

SQLSTATE: 42809

Description: The DROP OF operation is performed on a non-typed table.

Solution: Ensure that the table is a typed table.

GAUSS-00837: "Distribution mode cannot be altered"

SQLSTATE: 0A000

Description: The **ALTER TABLE** statement is used to modify the distribution mode.

Solution: Do not use the **ALTER TABLE** statement to modify the distribution mode.

GAUSS-00838: "Cannot alter table to distribution incompatible with existing constraints"

SQLSTATE: 0A000

Description: The redistribution of a table is incompatible with the constraints on the table.

Solution: Modify the redistribution to be compatible with the constraints.

GAUSS-00839: "Node list is empty: one node at least is mandatory"

SQLSTATE: 42710

Description: When **DELETE NODE LIST** is run, the node list to be deleted is empty.

Solution: Do not run **DELETE NODE LIST** on empty node lists.

GAUSS-00840: "PGXC Node %s: object not in relation node list"

SQLSTATE: 42710

Description: When **DELETE NODE LIST** is run, the node to be deleted does not exist in a specified node list.

Solution: Do not run **DELETE NODE LIST** on nodes that does not exist in the list.

<br/>

## GAUSS-00841 - GAUSS-00850

<br/>

GAUSS-00841: "PGXC Node %s: object already in relation node list"

SQLSTATE: 42710

Description: When **ADD NODE LIST** is run, the node to be added already exists in a specified node list.

Solution: This operation is not required.

GAUSS-00842: "cannot move an owned sequence into another schema"

SQLSTATE: 0A000

Description: When **ALTER TABLE SET SCHEMA** is run, the sequence of an owner is moved to another schema.

Solution: Do not move the sequence of an owner to another schema.

GAUSS-00843: "relation '%s' already exists in schema '%s'"

SQLSTATE: 42P07

Description: When **ALTER RELATION NAMESPACE** is run, the relation whose namespace is to be modified already exists in the schema.

Solution: This operation is not required.

GAUSS-00844: "failed to change schema dependency for relation '%s'"

SQLSTATE: XX000

Description: Failed to change the schema dependency of the table.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-00845: "permission denied: system catalog '%s' can not be altered"

SQLSTATE: 42501

Description: The RENAME, SET SCHEMA, or ALTER TABLE operation is performed on a system catalog.

Solution: Do not modify a system catalog.

GAUSS-00846: "'%s' is not a sequence"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00847: "'%s' is not a foreign table"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00848: "'%s' is not a composite type"

SQLSTATE: 42809

Description: The object is not a composite type.

Solution: Check whether the operation object is a composite type.

GAUSS-00849: "'%s' is not an index"

SQLSTATE: 42809

Description: An index operation is performed on a non-index table.

Solution: Perform this operation on an index table.

GAUSS-00850: "'%s' is a foreign table, which does not support column constraints."

SQLSTATE: 42809

Description: The foreign table and does not support column constraints.

Solution: Do not add column constraints to foreign tables.

<br/>

## GAUSS-00851 - GAUSS-00860

<br/>

GAUSS-00851: "cannot %s '%s' because it is in use"

SQLSTATE: 55006

Description: When an SQL statement is executed, the data partition to be queried is being accessed.

Solution: Run this SQL statement after the data partition access is complete.

GAUSS-00852: "there is no column for a partitioned table!"

SQLSTATE: 42601

Description: When the partition key sequence is obtained from a partitioned table, the column of the partitioned table is empty.

Solution: Ensure that the partitioned table has no empty column.

GAUSS-00853: "there is no partition key!"

SQLSTATE: 42601

Description: When the partition key sequence is obtained from a partitioned table, the value of all partition keys is null.

Solution: Set the values of partition keys to valid ones.

GAUSS-00854: "duplicate partition key: %s"

SQLSTATE: 42701

Description: When the partition key sequence is obtained from a partitioned table, the partitioned table contains duplicate partition keys.

Solution: Modify the partitioned table.

GAUSS-00857: "column %s cannot serve as an interval partitioning column because of its datatype"

SQLSTATE: 42804

Description: The data type of a column does not meet the requirements on interval partition columns.

Solution: Modify the data type of the column to meet the requirements.

GAUSS-00858: "column %s cannot serve as a range partitioning column because of its datatype"

SQLSTATE: 42804

Description: The data type of a column does not meet the data range requirements on the columns of partitioned tables.

Solution: Modify the data type of the column to meet the requirements.

GAUSS-00859: "partition bound list contains too few elements"

SQLSTATE: 42601

Description: When a partition value is modified to the array, the number of the elements contained in the partition bound list is less than that of target elements.

Solution: Change the number of the target elements to that of the elements contained in the partition bound list.

GAUSS-00860: "partition bound list contains too many elements"

SQLSTATE: 42601

Description: When a partition value is modified to the array, the number of the elements contained in the partition bound list is greater than that of target elements.

Solution: Change the number of the target elements to that of the elements contained in the partition bound list.

<br/>

## GAUSS-00861 - GAUSS-00870

<br/>

GAUSS-00865: "invalid range partiiton table definition"

SQLSTATE: 42601

Description: When the **ComparePartitionValue** function is called, the definition of a partitioned table is invalid.

Solution: Select a valid partitioned table.

GAUSS-00866: "there is no partition key"

SQLSTATE: 42601

Description: When the **ComparePartitionValue** function is called, no partition value exists.

Solution: Select a valid partitioned table.

GAUSS-00868: "partition bound of partition '%s' is too low"

SQLSTATE: 42601

Description: When the **ComparePartitionValue** function is called, the partition bound of a partitioned table is excessively low.

Solution: Check the table creation statement to ensure that the boundary values of the partition progressively increase.

GAUSS-00869: "can not add partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitions cannot be added to a non-partitioned table.

Solution: Modify the table to a partitioned table, or cancel this operation.

GAUSS-00870: "can not drop partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitions cannot be deleted from a non-partitioned table.

Solution: Cancel this operation.

<br/>

## GAUSS-00871 - GAUSS-00880

<br/>

GAUSS-00871: "can not set unusable index partition against NON-PARTITIONED index"

SQLSTATE: XX000

Description: Unusable index partitions are set for a non-partitioned table.

Solution: Set index partitions for a partitioned table.

GAUSS-00872: "can not set all index unusable on one partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Unusable full index partitions are set for a non-partitioned table.

Solution: Set full index partitions for a partitioned table.

GAUSS-00873: "can not enable row movement against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Row movements are not enabled for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00874: "can not disable row movement against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Row movements cannot be canceled for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00875: "can not truncate partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitions cannot be truncated for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00876: "can not exchange partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitions cannot be modified for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00877: "can not merge partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitioned cannot be merged for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00878: "can not split partition against NON-PARTITIONED table"

SQLSTATE: XX000

Description: Partitions cannot be split for a non-partitioned table.

Solution: Change the non-partitioned table to a partitioned table.

GAUSS-00880: "the current relation have already reached max number of partitions"

SQLSTATE: XX000

Description: The maximum number of partitions of a relational table has been reached.

Solution: Add partitions to the relational table after reducing the number of its partitions.

<br/>

## GAUSS-00881 - GAUSS-00890

<br/>

GAUSS-00882: "upper boundary of adding partition MUST overtop last existing partition"

SQLSTATE: XX000

Description: In a relational table, the upper boundary value of a new partition is smaller than that of the last partition.

Solution: Modify the upper boundary value of the new partition.

GAUSS-00883: "could not acquire AccessExclusiveLock on dest table partition '%s', %s failed"

SQLSTATE: XX000

Description: The AccessExclusiveLock of the partition is not obtained.

Solution: This problem may be caused by concurrency conflicts. Try again later.

GAUSS-00884: "Cannot drop the only partition of a partitioned table"

SQLSTATE: XX000

Description: When the statement on deleting partitions is executed, the last partition cannot be deleted from a relational table.

Solution: Cancel this operation.

GAUSS-00886: "no local index defined on partition %u"

SQLSTATE: XX000

Description: No local index is found in a partition.

Solution: Check whether the partition and index names to be queried exist. If they do not exist, cancel this operation.

GAUSS-00887: "can not set unusable index for relation %s , as it is not a index"

SQLSTATE: XX000

Description: The index for setting a relational table is invalid.

Solution: Use a valid index.

GAUSS-00888: "could not find tuple for relation %u"

SQLSTATE: XX000

Description: When **ModifyRowMovement** is executed, the tuple of a relational table cannot be obtained.

Solution: Check the tuple attribute of the relational table, or cancel this operation.

GAUSS-00889: "cannot truncate a partition owned by partitioned table which is referenced in a foreign key constraint"

SQLSTATE: XX000

Description: The statement on truncating partitions is run for a partitioned table referenced in a foreign key constraint.

Solution: Cancel the reference of the foreign key constraint, or cancel this operation.

GAUSS-00890: "can not merge index partition %s bacause it is unusable local index"

SQLSTATE: XX000

Description: The statement on merging index partitions is run for an unusable local index.

Solution: Ensure that the local index is usable, or cancel this operation.

<br/>

## GAUSS-00891 - GAUSS-00900

<br/>

GAUSS-00891: "source partitions must be at least two partitions"

SQLSTATE: XX000

Description: When **MergePartition** is run, the number of raw partitions is less than two.

Solution: Do not merge partitions when the number of raw partitions is no less than 2.

GAUSS-00892: "merge partitions of relation '%s', source partitions must be no more than %d partitions"

SQLSTATE: XX000

Description: The number of partitions to be merged exceeds the maximum.

Solution: Reduce the partitions to be merged.

GAUSS-00893: "merge partitions cannot process inusable index relation \"%s\""

SQLSTATE: XX000

Description: When **MergePartition** is run, the table contains an unusable local index.

Solution: Ensure that the local index is usable before merging partitions.

GAUSS-00895: "source partitions must be continuous and in ascending order of boundary"

SQLSTATE: XX000

Description: When **MergePartition** is run, raw partitions do not meet the requirements on continuity and boundary values listed in ascending order.

Solution: Run this statement when raw partitions meet the requirements.

GAUSS-00896: "target partition's name '%s' already exists"

SQLSTATE: 42710

Description: When **MergePartition** is run, the name of a target partition already exists.

Solution: Ensure that the name of the target partition is unique.

GAUSS-00898: "could not acquire AccessExclusiveLock on dest table partition '%s', MERGE PARTITIONS failed"

SQLSTATE: XX000

Description: Failed to obtain the exclusive lock of merged partitions.

Solution: Do not perform other operations than merging partitions.

GAUSS-00899: "Specified partition does not exist"

SQLSTATE: 42P01

Description: When **ExchangePartition** is run, the specified partition does not exist.

Solution: Run this statement after confirming the name of the specified partition.
