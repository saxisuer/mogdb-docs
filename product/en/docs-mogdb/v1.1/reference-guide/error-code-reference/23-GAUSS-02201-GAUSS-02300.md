---
title: GAUSS-02201 - GAUSS-02300
summary: GAUSS-02201 - GAUSS-02300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-02201 - GAUSS-02300

<br/>

## GAUSS-02201 - GAUSS-02210

<br/>

GAUSS-02202: "Failed to PREPARE the transaction on one or more nodes"

SQLSTATE: XX000

Description: Failed to prepare the required transaction on the node.

Solution: Internal system error. Contact technical support.

GAUSS-02209: "Error while running COPY"

SQLSTATE: XX000

Description: An unexpected response comes from the DN during information collection.

Solution: Internal system error. Contact technical support.

GAUSS-02210: "could not obtain connection from pool"

SQLSTATE: XX000

Description: Failed to obtain the connection from the connection pool.

Solution: Increase the **max_pool_size** value.

<br/>

## GAUSS-02211 - GAUSS-02220

<br/>

GAUSS-02211: "cannot run transaction to remote nodes during recovery"

SQLSTATE: XX000

Description: Failed to execute a transaction on the remote node during cluster restoration.

Solution: Commit the transaction after the restoration is complete.

GAUSS-02212: "Failed to get next transaction ID"

SQLSTATE: XX000

Description: An internal error occurs. Failed to obtain the next transaction ID.

Solution: Internal system error. Contact technical support.

GAUSS-02214: "Failed to send command to Datanodes"

SQLSTATE: XX000

Description: Failed to deliver the command to the DN.

Solution: Correct the cluster connection status.

GAUSS-02215: "Unexpected response from Datanode %u"

SQLSTATE: XX000

Description: An internal system error occurs. An unexpected response comes from the DN.

Solution: Internal system error. Contact technical support.

GAUSS-02216: "Could not begin transaction on Datanodes %u."

SQLSTATE: XX000

Description: Failed to start the transaction on the primary node.

Solution: Correct the cluster connection status.

GAUSS-02217: "Failed to read response from Datanodes when ending query"

SQLSTATE: XX000

Description: An internal error occurs. Failed to read the response from the DN when the query ends.

Solution: Internal system error. Contact technical support.

GAUSS-02218: "Failed to close Datanode cursor"

SQLSTATE: XX000

Description: An internal error occurs. Failed to close the cursor on the DN.

Solution: Correct the cluster connection status.

GAUSS-02219: "Number of user-supplied parameters do not match the number of remote parameters"

SQLSTATE: XX000

Description: An internal error occurs. The number of parameters provided by the user does not match that of parameters required for the query.

Solution: Internal system error. Contact technical support.

GAUSS-02220: "cannot run EXECUTE DIRECT with utility inside a transaction block"

SQLSTATE: 25001

Description: Failed to execute the **EXECUTE DIRECT** statement in a transaction.

Solution: Execute the **EXECUTE DIRECT** statement outside the transaction.

<br/>

## GAUSS-02221 - GAUSS-02230

<br/>

GAUSS-02221: "Could not begin transaction on Datanodes"

SQLSTATE: XX000

Description: Failed to start the transaction on the primary node.

Solution: Correct the cluster connection status.

GAUSS-02223: "Could not begin transaction on coordinators"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02225: "Unexpected response from coordinator %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02228: "prepared transaction with identifier '%s' does not exist"

SQLSTATE: XX000

Description: The transaction identifier parameter does not exist.

Solution: Check the input parameter and perform the operation again.

GAUSS-02229: "NULL junk attribute"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02230: "Failed to read response from Datanode %u when ending query"

SQLSTATE: XX000

Description: Failed to receive results from the DN.

Solution: Check the DN status, restart the cluster, and query again.

<br/>

## GAUSS-02231 - GAUSS-02240

<br/>

GAUSS-02232: "pgxc_pool_reload cannot run inside a transaction block"

SQLSTATE: 25001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02234: "must define Database name or user name"

SQLSTATE: 42000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02236: "The CREATE BARRIER PREPARE message is expected to arrive at a Coordinator from another Coordinator"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02237: "The CREATE BARRIER END message is expected to arrive at a Coordinator from another Coordinator"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02238: "The CREATE BARRIER EXECUTE message is expected to arrive from a Coordinator"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02239: "Failed to send CREATE BARRIER PREPARE request to the node"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02241 - GAUSS-02250

<br/>

GAUSS-02241: "CREATE BARRIER PREPARE command failed with error %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02242: "Failed to send CREATE BARRIER EXECUTE request to the node"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02244: "CREATE BARRIER command is not expected from another Coordinator"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02250: "cannot open %s query as cursor"

SQLSTATE: 42P11

Description: Failed to open the plan as a cursor.

Solution: Check whether the cursor definition is correct.

<br/>

## GAUSS-02251 - GAUSS-02260

<br/>

GAUSS-02252: "%s is not allowed in a non-volatile function"

SQLSTATE: 0A000

Description: Failed to use the statement for a non-volatile function.

Solution: Check whether the function definition is correct.

GAUSS-02253: "invalid portal in SPI cursor operation"

SQLSTATE: XX000

Description: Failed to obtain variables during execution.

Solution: Internal system error. Contact technical support.

GAUSS-02258: "Failed to read response from Datanodes. Detail: %s\n"

SQLSTATE: XX000

Description: The connection at the transmit end is disconnected. The cause is specified in **Detail**.

Solution: Check whether the peer DN is faulty or whether the network is normal.

GAUSS-02260: "Unhandled datatype for modulo or hash distribution\n"

SQLSTATE: XX000

Description: The data distribution type is invalid.

Solution: Check whether the distribution column definition in the table is correct.

<br/>

## GAUSS-02261 - GAUSS-02270

<br/>

GAUSS-02265: "BitmapOr doesn't support zero inputs"

SQLSTATE: XX000

Description: An internal system error occurs. The input of the **BitmapOr** operator is **NULL**.

Solution:Contact technical support.

GAUSS-02266: "could not identify CTID variable"

SQLSTATE: 42809

Description: The obtained variable is not the CTID variable.

Solution: View the execution plan and check whether the type returned by the expression is CTID. If not, contact technical support.

GAUSS-02268: "invalid operation on partition, allowed are UPDATE/DELETE/SELECT"

SQLSTATE: XX000

Description: Partitioned tables can be only deleted, queried, and modified.

Solution: Check whether the current operation performed for the partitioned table is correct.

<br/>

## GAUSS-02271 - GAUSS-02280

<br/>

GAUSS-02271: "could not determine actual type of argument declared %s"

SQLSTATE: 42804

Description: Failed to determine the data type of the parameter.

Solution: Check whether the function definition is correct.

GAUSS-02272: "%s is not allowed in a SQL function"

SQLSTATE: 0A000

Description: SQL functions cannot contain transaction statements.

Solution: Check whether the function definition is correct.

GAUSS-02273: "could not determine actual result type for function declared to return type %s"

SQLSTATE: 42804

Description: Failed to determine the return type of the function.

Solution: Check whether the function definition is correct.

GAUSS-02274: "failed to fetch lazy-eval tuple"

SQLSTATE: 22P08

Description: An internal system error occurs. Failed to obtain a tuple from the **tuplestore**.

Solution:Contact technical support.

GAUSS-02275: "return type mismatch in function declared to return %s"

SQLSTATE: 42P13

Description: The return type is inconsistent with that defined by the function.

Solution: Check whether the function definition is correct.

GAUSS-02277: "mergejoin clause is not an OpExpr"

SQLSTATE: XX000

Description: A plan error occurs. The **MergeJoin** condition is not an arithmetic expression.

Solution: Generated plan error. Contact technical support.

GAUSS-02278: "unsupported mergejoin strategy %d"

SQLSTATE: XX000

Description: A plan error occurs. The sorting policy used by **MergeJoin** is incorrect.

Solution: Internal system error. Contact technical support.

GAUSS-02279: "cannot merge using non-equality operator %u"

SQLSTATE: XX000

Description: A plan error occurs. The connection policy used by **MergeJoin** is incorrect.

Solution: Internal system error. Contact technical support.

GAUSS-02280: "missing support function %d(%u,%u) in opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02281 - GAUSS-02290

<br/>

GAUSS-02281: "unrecognized mergejoin state: %d"

SQLSTATE: XX000

Description: An execution error occurs. The execution policy chosen by **MergeJoin** is incorrect.

Solution: Internal system error. Contact technical support.

GAUSS-02282: "RIGHT JOIN is only supported with merge-joinable join conditions"

SQLSTATE: 0A000

Description: A plan error occurs. The **merge right join** is not supported.

Solution: Internal system error. Contact technical support.

GAUSS-02283: "FULL JOIN is only supported with merge-joinable join conditions"

SQLSTATE: 0A000

Description: For **MergeJoin**, **FULL JOIN** cannot process non-constant expressions (**JOIN** clauses).

Solution: Check whether the **JOIN** clauses are constants during **FULL JOIN** invocation in the SQL statement.

GAUSS-02284: "non-MVCC snapshots are not supported in index-only scans"

SQLSTATE: 0A000

Description: The index-only scan does not support MVCC snapshots.

Solution:Contact technical support.

GAUSS-02285: "EvalPlanQual recheck is not supported in index-only scans"

SQLSTATE: 0A000

Description: The index-only scan does not support recheck during concurrent update.

Solution:Contact technical support.

GAUSS-02288: "could not find hash function for hash operator %u"

SQLSTATE: 42883

Description: The corresponding hash function cannot be found.

Solution: Check whether the data type supports the hash function.

GAUSS-02289: "cursor '%s' is not a SELECT query"

SQLSTATE: 24000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02290: "cursor '%s' is held from a previous transaction"

SQLSTATE: 24000

Description: The cursor is held by the previous transaction.

Solution: Check the cursor status.

<br/>

## GAUSS-02291 - GAUSS-02300

<br/>

GAUSS-02291: "cursor '%s' has multiple FOR UPDATE/SHARE references to table '%s'"

SQLSTATE: 24000

Description: Multiple cursors are defined for the table.

Solution: Check whether the cursor is repeatedly defined in the SQL statement.

GAUSS-02292: "cursor '%s' does not have a FOR UPDATE/SHARE reference to table '%s'"

SQLSTATE: 24000

Description: In SQL statements containing **CURRENT OF** and **UPDATE**, the **UPDATE/SHARE** syntax for the table is not defined.

Solution: Check whether the update/share operation has been properly defined for the table. Alternatively, check whether the optimizer works properly.

GAUSS-02294: "cursor '%s' is not a simply updatable scan of table '%s'"

SQLSTATE: 24000

Description: Failed to find the corresponding scan nodes by executing query statements containing the **CURRENT OF** syntax and not containing the **UPDATE** syntax.

Solution: Check whether the operation syntax is properly defined in the SQL statement. Alternatively, check whether the optimizer works properly.

GAUSS-02295: "type of parameter %d (%s) does not match that when preparing the plan (%s)"

SQLSTATE: 42804

Description: The parameter type returned during execution is inconsistent with that of expressions described in the execution plan.

Solution: 1. Check whether the types of expression parameters in the SQL statements are consistent. 2. Check the executable code to ensure that the parameter attribute is correct. "

GAUSS-02296: "no value found for parameter %d"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02297: "LIMIT subplan failed to run backwards"

SQLSTATE: XX005

Description: The **NULL** is returned in advance when the backward scan is performed on the **LIMIT** lower-layer operator.

Solution:Contact technical support.

GAUSS-02298: "impossible LIMIT state: %d"

SQLSTATE: XX006

Description: The LIMIT state is incorrect.

Solution:Contact technical support.

GAUSS-02299: "OFFSET must not be negative"

SQLSTATE: 2201X

Description: The **OFFSET count** value is an invalid negative value.

Solution: Change the **count** value to a value greater than or equal to 0.

GAUSS-02300: "LIMIT must not be negative"

SQLSTATE: 2201W

Description: The **LIMIT count** value is an invalid negative value.
