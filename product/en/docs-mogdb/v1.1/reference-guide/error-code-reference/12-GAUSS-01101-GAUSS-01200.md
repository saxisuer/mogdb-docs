---
title: GAUSS-01101 - GAUSS-01200
summary: GAUSS-01101 - GAUSS-01200
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01101 - GAUSS-01200

<br/>

## GAUSS-01101 - GAUSS-01110

<br/>

GAUSS-01101: "%s requires an integer value"

SQLSTATE: 42601

Description: The parameter provided is not of the integer type.

Solution: Check the SQL statement and provide the correct parameter.

GAUSS-01102: "invalid argument for %s: '%s'"

SQLSTATE: 42601

Description: Invalid parameter.

Solution: Check the SQL statement and provide the correct parameter.

GAUSS-01103: "table is not partitioned"

SQLSTATE: 0A000

Description: This table is not a partitioned table.

Solution: Check the table name and ensure that the table is partitioned.

GAUSS-01104: "cannot cluster temporary tables of other sessions"

SQLSTATE: 0A000

Description: Temporary tables created in other sessions cannot be clustered.

Solution: Do not cluster temporary tables created in other sessions.

GAUSS-01105: "there is no previously clustered index for table '%s'"

SQLSTATE: 42704

Description: Previously clustered index of this table does not exist.

Solution: Ensure that this table has been previously clustered if the clustered index is not provided. The system will use the previously clustered index. Otherwise, provide the clustered index.

GAUSS-01106: "cannot cluster a shared catalog"

SQLSTATE: 0A000

Description: A system catalog shared by multiple databases cannot be clustered.

Solution: Do not cluster a system catalog shared by multiple databases.

GAUSS-01107: "cannot vacuum temporary tables of other sessions"

SQLSTATE: 0A000

Description: Temporary tables created in other sessions cannot be vacuumed.

Solution: Do not vacuum temporary tables created in other sessions.

GAUSS-01108: "'%s' is not an index for table '%s'"

SQLSTATE: 42809

Description: This index is not of the table and cannot be clustered.

Solution: Ensure that the index in the SQL statement matches the corresponding table.

GAUSS-01109: "cannot cluster on index '%s' because access method does not support clustering"

SQLSTATE: 0A000

Description: The index type cannot be clustered.

Solution: Do not cluster this type of index.

GAUSS-01110: "cannot cluster on partial index '%s'"

SQLSTATE: 0A000

Description: Partial indexes cannot be clustered.

Solution: Do not cluster partial indexes.

<br/>

## GAUSS-01111 - GAUSS-01120

<br/>

GAUSS-01111: "cannot cluster on invalid index '%s'"

SQLSTATE: 0A000

Description: The specified indexes are invalid and remain after **CREATE INDEX CONCURRENTLY** failed to be run.

Solution: Do not cluster invalid indexes.

GAUSS-01112: "cannot cluster on invalid index %u"

SQLSTATE: XX000

Description: Invalid index.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01113: "CLUSTER does not support lossy index conditions"

SQLSTATE: XX000

Description: Lossy index conditions cannot be used during **CLUSTER** execution.

Solution: Do not use hash and gist indexes.

GAUSS-01114: "unexpected HeapTupleSatisfiesVacuum result"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01115: "can not cluster partition %s using %s bacause of unusable local index"

SQLSTATE: XX000

Description: The clustered partitioned table cannot use the local index, because it is unavailable.

Solution: Rebuild the partitioned table.

GAUSS-01117: "cannot swap mapped relation '%s' with non-mapped relation"

SQLSTATE: XX000

Description: During scale-out and redistribution, the physical files of two tables fail to be exchanged using the gs_switch_relfilenode function, because the physical file in a table is invalid.

Solution: Check whether the physical file in the table specified in the gs_switch_relfilenode function is valid. If it is invalid, delete the table and rebuild the table using **CREATE TABLE**. Then, rebuild its physical file.

GAUSS-01118: "cannot change tablespace of mapped relation '%s'"

SQLSTATE: XX000

Description: During scale-out and redistribution, the tablespaces of two tables are inconsistent and fail to be exchanged using the gs_switch_relfilenode function.

Solution: Delete the current table, rebuild tablespace using **CREATE TABLESPACE**, and rebuild the table using **CREATE TABLE**.

GAUSS-01119: "cannot swap toast by links for mapped relation '%s'"

SQLSTATE: XX000

Description: During scale-out and redistribution, the physical files of two tables fail to be exchanged using the gs_switch_relfilenode function, because the toast table in one of the tables is empty.

Solution: Delete the current table, rebuild it using **CREATE TABLE**, and rebuild its physical file.

GAUSS-01120: "could not find relation mapping for relation '%s', OID %u"

SQLSTATE: XX000

Description: During scale-out, the physical files of two tables fail to be exchanged using the gs_switch_relfilenode function, because the physical file in a table is invalid.

Solution: Check whether the physical file in the table specified in the gs_switch_relfilenode function is valid. If it is invalid, delete the table and rebuild the table using **CREATE TABLE**. Then, rebuild its physical file.

<br/>

## GAUSS-01121 - GAUSS-01130

<br/>

GAUSS-01121: "cannot swap toast files by content when there's only one"

SQLSTATE: XX000

Description: During scale-out and redistribution, the physical files of two tables fail to be exchanged using the gs_switch_relfilenode function, because the toast table in one of the tables is empty.

Solution: Delete the current table, rebuild it using **CREATE TABLE**, and rebuild its physical file.

GAUSS-01122: "cannot swap toast files by links for system catalogs"

SQLSTATE: XX000

Description: During scale-out and redistribution, when the physical files of two tables are exchanged using the gs_switch_relfilenode function, the toast physical files of system catalogs cannot be exchanged.

Solution: During scale-out and redistribution, the toast physical files of system catalogs cannot be exchanged using the gs_switch_relfilenode function. Check whether the function parameters contain any system catalog names.

GAUSS-01123: "expected one dependency record for TOAST table, found %ld"

SQLSTATE: XX000

Description: During scale-out and redistribution, the physical files of two tables fail to be exchanged using the gs_switch_relfilenode function, because more than one object depends on the toast table.

Solution: Check the number of objects the toast table depends on in the **pg_depend** table. If more than one object exists, delete them and exchange the physical files using the gs_switch_relfilenode function.

GAUSS-01124: "expected none dependency record for partiton's TOAST table, found %ld"

SQLSTATE: XX000

Description: While the partition files of two tables are exchanged during partitioned table rebuilding, toast table files (if any) in the two tables are also exchanged. The partition files fail to be exchanged because the toast tables have dependent objects.

Solution: Rebuild the required partitioned table using **CREATE TABLE PARTITION**.

GAUSS-01125: "expected one dependency record for CUDesc/Delta table, found %ld"

SQLSTATE: XX000

Description: During scale-out and redistribution, the physical files of two column-store tables fail to be exchanged using the gs_switch_relfilenode function, because more than one object depends on the column-store table.

Solution: Check the number of objects the column-store table depends on in the **pg_depend** table. If more than one object exists, delete them and exchange the physical files using the gs_switch_relfilenode function.

GAUSS-01127: "invalid statement name: must not be empty"

SQLSTATE: 42P14

Description: The statement name is empty.

Solution: Specify the statement name.

GAUSS-01128: "could not determine data type of parameter $%d"

SQLSTATE: 42P18

Description: The parameter data type cannot be recognized.

Solution: Change the parameter value to a value recognizable to the system. For the value types recognizable to the system, see en-us_topic_0237121926.html.

GAUSS-01129: "utility statements cannot be prepared"

SQLSTATE: 42P14

Description: DDL statements cannot be prepared.

Solution: Do not prepare DDL statements.

GAUSS-01130: "EXECUTE does not support variable-result cached plans"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01131 - GAUSS-01140

<br/>

GAUSS-01131: "prepared statement is not a SELECT"

SQLSTATE: 42809

Description: The prepared statement in **CREATE TABLE AS EXECUTE** is not a **SELECT** statement.

Solution: Change the prepared statement to a **SELECT** statement.

GAUSS-01132: "wrong number of parameters for prepared statement '%s'"

SQLSTATE: 42601

Description: The number of parameters required in the prepared statement does not match the number of parameters obtained.

Solution: Ensure that the number of parameters in **EXECUTE** is the same as that in **PREPARE**. Then, run **PREPARE** and **EXECUTE** again.

GAUSS-01136: "parameter $%d of type %s cannot be coerced to the expected type %s"

SQLSTATE: 42804

Description: The parameter type in a prepared statement cannot be converted to the expected parameter type.

Solution: Add required type conversions or ensure that the parameter type can be converted.

GAUSS-01137: "Passing parameters in PREPARE statement is not supported"

SQLSTATE: 0A000

Description: Parameters cannot be remotely passed in prepared statements.

Solution: Do not pass distributed parameters in prepared statements.

GAUSS-01138: "prepared statement '%s' already exists"

SQLSTATE: 42P05

Description: The prepared statement already exists.

Solution: Check the **pg_prepared_statements** view. Do not create duplicate prepared statements.

GAUSS-01139: "prepared statement '%s' does not exist"

SQLSTATE: 26000

Description: The prepared statement to be executed does not exist.

Solution: Check whether the executed prepared statement exists in the **pg_prepared_statements** view.

GAUSS-01140: "EXPLAIN EXECUTE does not support variable-result cached plans"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01141 - GAUSS-01150

<br/>

GAUSS-01141: "datanode statement '%s' does not exist"

SQLSTATE: 26000

Description: The **EXECUTE DIRECT** syntax does not contain the specified DN information.

Solution: Specify the DN information in the **EXECUTE DIRECT** syntax.

GAUSS-01142: "must be system admin to create procedural language '%s'"

SQLSTATE: 42501

Description: A non-administrator user creates a stored procedure language.

Solution: Create a stored procedure language as a system administrator.

GAUSS-01143: "function %s must return type 'language_handler'"

SQLSTATE: 42809

Description: The return value of a stored procedure language is incorrect.

Solution: Change the language type to **language_handler**.

GAUSS-01144: "unsupported language '%s'"

SQLSTATE: 42704

Description: The stored procedure language is not supported.

Solution: View **pg_pltemplate** and use a language existing in the list.

GAUSS-01145: "must be system admin to create custom procedural language"

SQLSTATE: 42501

Description: A non-administrator user creates a stored procedure language.

Solution: Create a stored procedure language as a system administrator.

GAUSS-01146: "language '%s' already exists"

SQLSTATE: 42710

Description: The language already exists.

Solution: View **pg_language** to check whether the language exists.

GAUSS-01147: "cache lookup failed for language %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01148: "COPY BINARY is not supported to stdout or from stdin"

SQLSTATE: 0A000

Description: The binary data cannot be copied to the standard input GUI or from the standard output GUI.

Solution: Do not copy binary data to the standard input GUI or from the standard output GUI.

GAUSS-01149: "could not write to COPY file: %m"

SQLSTATE: XX000

Description: Data cannot be copied to a specified file.

Solution: Ensure that you have permission to access the specified file.

GAUSS-01150: "connection lost during COPY to stdout"

SQLSTATE: 08006

Description: The connection is lost during the copy operation.

Solution: Ensure that the network communication between the client and the server is normal.

<br/>

## GAUSS-01151 - GAUSS-01160

<br/>

GAUSS-01151: "COPY_FILE_SEGMENT does not implement in CopySendEndOfRow"

SQLSTATE: XX000

Description: The target data source specified in **COPY TO** is not supported.

Solution: Change the target data source.

GAUSS-01152: "COPY_GDS does not implement in CopySendEndOfRow"

SQLSTATE: XX000

Description: The target data source specified in **COPY TO** is not supported.

Solution: Change the target data source.

GAUSS-01153: "could not read from COPY file: %m"

SQLSTATE: XX000

Description: Failed to read data from a specified copy file.

Solution: Check whether the specified file is damaged and the permission is sufficient.

GAUSS-01154: "unexpected EOF on client connection with an open transaction"

SQLSTATE: 08006

Description: The connection fails when the data is copied to the standard input.

Solution: Ensure that the network communication between the client and the server is normal.

GAUSS-01155: "COPY from stdin failed: %s"

SQLSTATE: 57014

Description: Failed to copy data from the standard input.

Solution: Ensure that the network communication between the client and the server is normal.

GAUSS-01156: "unexpected message type 0x%02X during COPY from stdin"

SQLSTATE: 08P01

Description: The data type copied from the standard input is incorrect.

Solution: Ensure that the data type in the standard input is correct.

GAUSS-01157: "COPY_BUFFER not allowed in this context"

SQLSTATE: XX000

Description: The target data source specified in **COPY TO** is not supported.

Solution: Change the target data source.

GAUSS-01158: "unimplemented CopyDest mode"

SQLSTATE: XX000

Description: The target data source specified in **COPY TO** is not supported.

Solution: Change the target data source.

GAUSS-01159: "must be system admin to COPY to or from a file"

SQLSTATE: 42501

Description: A non-administrator user performs the copy operation.

Solution: Perform the copy operation as a system administrator.

GAUSS-01160: "position of field '%s' can not be less then 0"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01161 - GAUSS-01170

<br/>

GAUSS-01161: "length of field '%s' can not be less then 0"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01162: "max length of data row cannot greater than 1GB"

SQLSTATE: 42601

Description: The length of the row is greater than 1 GB.

Solution: Ensure that the maximum length of the data row is not greater than 1 GB.

GAUSS-01163: "max length of data row cannot greater than 1GB"

SQLSTATE: 42601

Description: The length of the row is greater than 1 GB.

Solution: Ensure that the maximum length of the data row is not greater than 1 GB.

GAUSS-01164: "pre-field '%s' can not be covered by field '%s'"

SQLSTATE: 42601

Description: The data area is covered.

Solution: Ensure that the data is within the specified range and is not covered.

GAUSS-01165: "default conversion function for encoding '%s' to '%s' does not exist"

SQLSTATE: 42883

Description: No function is used to convert between the encoding format specified in **COPY** and the database encoding format.

Solution: Change the encoding format specified in **COPY**.

GAUSS-01166: "invalid user-define header file '%s'"

SQLSTATE: XX000

Description: A customized header file involved in **COPY** is invalid.

Solution:Contact technical support.

GAUSS-01167: "no data in user-define header file '%s'"

SQLSTATE: XX000

Description: A customized header file involved in **COPY** is invalid.

Solution:Contact technical support.

GAUSS-01168: "user-define header should not longger than 1MB"

SQLSTATE: XX000

Description: A customized header file involved in **COPY** is invalid.

Solution:Contact technical support.

GAUSS-01169: "COPY format '%s' not recognized"

SQLSTATE: 22023

Description: The data file format specified in **COPY** not supported.

Solution: Ensure that the data file format is correct.

GAUSS-01170: "null value string is too long"

SQLSTATE: 22023

Description: An excessively long null value string is used when the data is copied.

Solution: Ensure that the length of the null string specified in **COPY** does not exceed 100 characters.

<br/>

## GAUSS-01171 - GAUSS-01180

<br/>

GAUSS-01171: "argument to option '%s' must be a list of column names"

SQLSTATE: 22023

Description: The parameter option is not one of the column names.

Solution: Ensure that the parameter option is one of the column names.

GAUSS-01172: "argument to option '%s' must be a valid encoding name"

SQLSTATE: 22023

Description: The parameter option is not a valid encoding name.

Solution: Ensure that the parameter option is a known encoding name.

GAUSS-01174: "invalid value of FIX"

SQLSTATE: 22023

Description: The value of the fix option cannot be recognized.

Solution: Ensure that the value is correct.

GAUSS-01175: "option '%s' not recognized"

SQLSTATE: 42601

Description: The value of a copy option cannot be recognized.

Solution: Ensure that the value is correct.

GAUSS-01176: "cannot specify DELIMITER in BINARY/FIXED mode"

SQLSTATE: 42601

Description: A delimiter fails to be specified in BINARY/FIXED mode.

Solution: Do not specify a delimiter in BINARY/FIXED.

GAUSS-01177: "cannot specify NULL in BINARY/FIXED mode"

SQLSTATE: 42601

Description: The null character fails to be specified in BINARY/FIXED mode.

Solution: Do not specify a null character in BINARY/FIXED mode.

GAUSS-01178: "FORMATTER only can be specified in FIXED mode"

SQLSTATE: 42601

Description:**FORMATTER** is not specified in FIXED mode.

Solution: Specify **FORMATTER** in FIXED mode.

GAUSS-01179: "FORMATTER must be specified in FIXED mode"

SQLSTATE: 42601

Description:**FORMATTER** is not specified in FIXED mode.

Solution: Specify **FORMATTER** in FIXED mode.

GAUSS-01180: "FIX only can be specified in FIXED mode"

SQLSTATE: 42601

Description:**FIX** is not specified in FIXED mode.

Solution: Specify **FIX** in FIXED mode.

<br/>

## GAUSS-01181 - GAUSS-01190

<br/>

GAUSS-01181: "COPY delimiter must be less than %d bytes"

SQLSTATE: 0A000

Description: The copy delimiter specified exceeds the maximum length.

Solution: Specify a delimiter with its length within 10 bytes.

GAUSS-01182: "COPY delimiter cannot be newline or carriage return"

SQLSTATE: 22023

Description: Line breaks or carriage returns are used as copy delimiters.

Solution: Use only horizontal tabs or commas as copy delimiters. For details, see en-us_topic_0237122096.html.

GAUSS-01183: "COPY null representation cannot use newline or carriage return"

SQLSTATE: 22023

Description: Line breaks or carriage returns are used as copy null characters.

Solution: Do not use line breaks or carriage returns as copy null characters.

GAUSS-01184: "delimiter '%s' cannot contain any characters in'%s'"

SQLSTATE: 22023

Description: When a non-CSV file is imported, the specified delimiter contains forbidden characters, such as lowercase letters (a-z), digits (0-9), dots (.), and backslashes ().

Solution: Change the delimiter to ensure no forbidden characters are included.

GAUSS-01185: "COPY HEADER available only in CSV mode"

SQLSTATE: 0A000

Description: The copy header is not in CSV mode.

Solution: Ensure that the copy header is in CSV mode.

GAUSS-01186: "COPY quote available only in CSV mode"

SQLSTATE: 0A000

Description: The copy quote is not in CSV mode.

Solution: Ensure that the copy quote is in CSV mode.

GAUSS-01188: "delimiter cannot contain quote character"

SQLSTATE: 22023

Description: When a CSV file is imported, the delimiter contains quotation characters.

Solution: Change the quotation characters to allowed CSV delimiters.

GAUSS-01189: "COPY escape available only in CSV mode"

SQLSTATE: 0A000

Description: The copy escape is not in CSV mode.

Solution: Ensure that the copy escape is in CSV mode.

<br/>

## GAUSS-01191 - GAUSS-01200

<br/>

GAUSS-01191: "COPY force quote available only in CSV mode"

SQLSTATE: 0A000

Description: The copy force quote is not in CSV mode.

Solution: Ensure that the force quote is in CSV mode.

GAUSS-01194: "COPY force not null only available using COPY FROM"

SQLSTATE: 0A000

Description: The force not-null character supports only COPY FROM operation.

Solution: Ensure that the force non-null character is in the **COPY FROM** statement.

GAUSS-01195: "COPY delimiter must not appear in the NULL specification"

SQLSTATE: 0A000

Description: The copy delimiter exists in the null specification.

Solution: Do not use a value in the null specification as a delimiter.

GAUSS-01197: "CSV quote character must not appear in the NULL specification"

SQLSTATE: 0A000

Description: The copy quote character exists in the null specification.

Solution: Do not use a value in the null specification as a delimiter.

GAUSS-01198: "SHARED mode can not be used with CSV format"

SQLSTATE: 0A000

Description: The shared mode is used with CSV format.

Solution: Do not use the shared mode with the CSV format.

GAUSS-01199: "HEADER FILE only available using COPY TO or WRITE ONLY foreign table"

SQLSTATE: 0A000

Description: The header file is not used in **COPY TO** and foreign table scenarios.

Solution: Use the header file only in **COPY TO** and foreign table scenarios.
