---
title: Third-Party Library Error Codes
summary: Third-Party Library Error Codes
author: Zhang Cuiping
date: 2021-03-11
---

# Third-Party Library Error Codes

<br/>

A third-party error code contains five characters, the first three indicating the error type and the last two indicating the subclass. The five characters can be digits or uppercase letters, which represent various errors or warnings.

**Table 1** liborc error codes

| Error Level | Error Code | Description                                                  |
| :---------- | :--------- | :----------------------------------------------------------- |
| ORC_INFO    | ORC00      | Invalid content (INVALID_ERROR_CODE)                         |
| ORC_ERROR   | ORC01      | Function that is not supported or cannot be implemented (NOTIMPLEMENTEDYET) |
| ORC_ERROR   | ORC02      | Compilation error (PARSEERROR)                               |
| ORC_ERROR   | ORC03      | Logic error (LOGICERROR)                                     |
| ORC_ERROR   | ORC04      | Range error (RANGEERROR)                                     |
| ORC_ERROR   | ORC05      | Write error (WRITEERROR)                                     |
| ORC_FATAL   | ORC06      | Interruption (ASSERTERROR)                                   |
| ORC_ERROR   | ORC07      | Memory error (MEMORYERROR)                                   |
| ORC_ERROR   | ORC08      | Other errors (OTHERERROR)                                    |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> 1. Errors of ORC_ERROR and higher levels are printed using **psql**, and their detailed stack information is recorded in the corresponding CN and DN logs. Lower-level logs are only printed in the corresponding CN and DN logs.
> 2. You can locate and rectify the fault based on the error code and information.
