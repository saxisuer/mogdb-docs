---
title: GAUSS-01601 - GAUSS-01700
summary: GAUSS-01601 - GAUSS-01700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01601 - GAUSS-01700

<br/>

## GAUSS-01601 - GAUSS-01610

<br/>

GAUSS-01601: "invalid paramid: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01602: "Upper-level Var found where not expected"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01603: "Upper-level Aggref found where not expected"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01604: "Aggref found where not expected"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01605: "Upper-level PlaceHolderVar found where not expected"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01606: "PlaceHolderVar found where not expected"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-01607: "could not devise a query plan for the given query"

SQLSTATE: XX000

Description: The plan fails to be generated.

Solution: Check whether the query is proper, for example, whether the join mode is proper.

GAUSS-01608: "ORDER/GROUP BY expression not found in targetlist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01610: "relation %s is not partitioned"

SQLSTATE: XX000

Description: Internal system error. Check whether the index contains invalid partitions.

Solution:Contact technical support.

<br/>

## GAUSS-01611 - GAUSS-01620

<br/>

GAUSS-01611: "no local indexes found for partition %s"

SQLSTATE: 42809

Description: Internal system error. Index partitions are not defined in table partitions.

Solution:Contact technical support.

GAUSS-01612: "bit map error when searching for unusable index partition"

SQLSTATE: XX000

Description: Internal system error. Bit errors occur when you search for invalid index partitions.

Solution: Roll back the transaction.

GAUSS-01613: "bit map error after searching for unusable index partition"

SQLSTATE: XX000

Description: Internal system error. Bit errors occur when you search for invalid index partitions.

Solution: Roll back the transaction.

GAUSS-01614: "relation of oid='%u' is not partitioned table"

SQLSTATE: XX000

Description: The table specified in the **PARTITION FOR** clause is not a partitioned one.

Solution: Ensure that the table specified in the **PARTITION FOR** clause is a partitioned one.

GAUSS-01616: "fail to find partition with oid %u for partitioned table %u"

SQLSTATE: 42P01

Description: Internal system error. The partition specified in the partitioned table cannot be found.

Solution:Contact technical support.

GAUSS-01617: "partitionPruningForExpr: parameter can not be null"

SQLSTATE: XX000

Description: Internal system error. During pruning, the returned pruning parameter is null.

Solution:Contact technical support.

GAUSS-01618: "get null for partition pruning"

SQLSTATE: XX000

Description: Internal system error. During pruning, the returned internal pruning result is null.

Solution:Contact technical support.

GAUSS-01619: "For every node in same expression, pruning result's intervalOffset MUST be same"

SQLSTATE: XX000

Description: Internal system error. For nodes in the same expression, the sequence number of their minimum interval partitions in the pruning result must be the same.

Solution:Contact technical support.

GAUSS-01620: "pruning result(PartitionIdentifier) is invalid"

SQLSTATE: XX000

Description: Internal system error. The pruning result filtered out based on the specified range is invalid.

Solution:Contact technical support.

<br/>

## GAUSS-01621 - GAUSS-01630

<br/>

GAUSS-01621: "unsupported partition key column range mode"

SQLSTATE: 42P17

Description: Internal system error. An internal structure based on the partition sequence number is invalid.

Solution:Contact technical support.

GAUSS-01623: "Unupport partition strategy '%c'"

SQLSTATE: XX000

Description: Internal system error. The partitioning policy is not supported.

Solution:Contact technical support.

GAUSS-01624: "Expected TargetEntry node, but got node with type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01625: "can not generate shippable query for base relations of type other than plain tables"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01626: "can not handle multiple relations in a single baserel"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01627: "a join relation path should have both left and right paths"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01628: "unexpected unshippable quals in JOIN tree"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01629: "join with unshippable join clauses can not be shipped"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01631 - GAUSS-01640

<br/>

GAUSS-01631: "Creating remote query plan for relations of type %d is not supported"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01632: "expected a VAR node but got node of type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01633: "No distribution information found for remote query path"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01634: "creating remote query plan for relations of type %d is not supported"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01635: "Unexpected node type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01637: "Duplicate node_ids not expected in source target list"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01638: "Duplicate ctids not expected in source target list"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01640: "cache lookup failed for attribute %s of relation %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01641 - GAUSS-01650

<br/>

GAUSS-01641: "Source data plan's target list does not contain ctid colum"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01642: "Unexpected command type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01645: "column store doesn't support backward scan"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01646: "Distributed key column can't be updated in current version"

SQLSTATE: 42P10

Description: The distribution column is updated.

Solution: Do not update the distribution column.

GAUSS-01647: "could not find array type for datatype %s"

SQLSTATE: XX000

Description: The data type does not support the array sublink mode.

Solution: Modify the subquery type.

GAUSS-01648: "unexpected PARAM_SUBLINK ID: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01649: "unexpected outer reference in CTE query"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01650: "could not find plan for CteScan referencing plan ID %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01661 - GAUSS-01670

<br/>

GAUSS-01661: "failed to deconstruct sort operators into partitioning/ordering operators"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01662: "failed to assign all NestLoopParams to plan nodes"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01663: "MergeAppend child's targetlist doesn't match MergeAppend"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01664: "failed to find unique expression in subplan tlist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01665: "could not find compatible hash operator for operator %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01666: "could not find ordering operator for equality operator %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01667: "could not find param ID for CTE '%s'"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-01668: "unsupported RTE kind %d in build_one_column_tlist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01669: "outer pathkeys do not match mergeclauses"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01670: "inner pathkeys do not match mergeclauses"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01671 - GAUSS-01680

<br/>

GAUSS-01671: "indexqual clause is not binary opclause"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01672: "inconsistent results from adjust_rowcompare_for_index"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01673: "indexorderby clause is not binary opclause"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01674: "unsupported indexorderby type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01675: "index key does not match expected index column"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01676: "could not find pathkey item to sort"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01678: "Fail to create path for partitioned table by the lack of info"

SQLSTATE: XX000

Description:Internal system error. The scan operator is set incorrectly.

Solution:Contact technical support.

GAUSS-01679: "Only Scan operator have patition attribute"

SQLSTATE: XX000

Description: Non-scan operators are set in a partitioned table

Solution:Contact technical support.

<br/>

## GAUSS-01681 - GAUSS-01690

<br/>

GAUSS-01682: "NestLoopParam was not reduced to a simple Var"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01683: "variable not found in subplan target lists"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01684: "variable not found in subplan target list"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01685: "variable not found in base remote scan target lists"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01687: "SELECT FOR UPDATE/SHARE cannot be applied to the nullable side of an outer join"

SQLSTATE: 0A000

Description: The **SELECT FOR UPDATE/SHARE** statement is used to a table whose outer join is null.

Solution:Contact technical support.

GAUSS-01688: "JOIN qualification cannot refer to other relations"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01691 - GAUSS-01700

<br/>

GAUSS-01692: "cannot insert into view '%s'"

SQLSTATE: 55000

Description: Data is inserted into a view.

Solution: Do not insert data into any view.

GAUSS-01693: "cannot update view '%s'"

SQLSTATE: 55000

Description: The view cannot be updated.

Solution: Use the **ON UPDATE DO INSTEAD** rule or **INSTEAD OF UPDATE** trigger.

GAUSS-01694: "cannot delete from view '%s'"

SQLSTATE: 55000

Description: Data cannot be deleted from the view.

Solution: Use the **ON UPDATE DO INSTEAD** rule or **INSTEAD OF UPDATE** trigger.

GAUSS-01697: "invalid fork name"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01698: "invalid relation file path %s."

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01699: "invalid relation file path %s: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01700: "invalid relpersistence: %c"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.
