---
title: GAUSS-02501 - GAUSS-02600
summary: GAUSS-02501 - GAUSS-02600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-02501 - GAUSS-02600

<br/>

## GAUSS-02501 - GAUSS-02510

<br/>

GAUSS-02501: "could not create unique index '%s'"

SQLSTATE: 23505

Description: Internal system error. Failed to create a unique index.

Solution: Contact technical support.

GAUSS-02502: "reversedirection_index_hash is not implemented"

SQLSTATE: 0A000

Description: Internal system error. Failed to implement the function.

Solution: Contact technical support.

GAUSS-02503: "copytup_datum() should not be called"

SQLSTATE: 0A000

Description: Internal system error. Failed to call the function because it is not implemented.

Solution: Contact technical support.

GAUSS-02504: "Failed to fetch from data node cursor"

SQLSTATE: 22P08

Description: Internal system error. Failed to obtain data from the DN cursor.

Solution: Contact technical support.

GAUSS-02505: "Node id %d is incorrect"

SQLSTATE: XX006

Description: Internal system error. The obtained node ID is incorrect.

Solution: Contact technical support.

GAUSS-02506: "%s"

SQLSTATE: XX000

Description: Syntax error.

Solution: Ensure that the SQL statement syntax is correct.

GAUSS-02507: "Unexpected response from the data nodes"

SQLSTATE: XX006

Description: Internal system error. An unexpected response comes from the DN.

Solution: Contact technical support.

GAUSS-02508: "invalid BatchSort state"

SQLSTATE: XX006

Description: Internal system error. The column-store sorting status is invalid.

Solution: Contact technical support.

GAUSS-02509: "invalid batchsort state"

SQLSTATE: XX006

Description: Internal system error. The column-store sorting status is invalid.

Solution: Contact technical support.

GAUSS-02510: "batchsort_restorepos failed"

SQLSTATE: D0011

Description: Internal system error. The storage information is incorrect during column-store sorting.

Solution: Contact technical support.

<br/>

## GAUSS-02511 - GAUSS-02520

<br/>

GAUSS-02511: "failed to initialize hash table '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02512: "cannot insert into frozen hashtable '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02513: "out of shared memory"

SQLSTATE: 53200

Description: The shared memory is insufficient.

Solution: Increase the value of the **max_locks_per_transaction** parameter or release the required memory.

GAUSS-02514: "unrecognized hash action code: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02515: "cannot freeze shared hashtable '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02516: "cannot freeze hashtable '%s' because it has active scans"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02517: "hash table '%s' corrupted"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02518: "too many active hash_seq_search scans, cannot start one on '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02519: "no hash_seq_search scan for hash table '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02520: "could not change directory to '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02521 - GAUSS-02530

<br/>

GAUSS-02521: "could not get current working directory: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02522: "cannot set parameter '%s' within security-restricted operation"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02524: "role '%s' is not permitted to login"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02525: "too many connections for role '%s'"

SQLSTATE: 53300

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02526: "permission denied to set session authorization"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02527: "invalid role OID: %u"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02528: "could not create lock file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02529: "could not open lock file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02530: "could not read lock file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02531 - GAUSS-02540

<br/>

GAUSS-02532: "lock file '%s' already exists"

SQLSTATE: F0001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02533: "pre-existing shared memory block (key %lu, ID %lu) is still in use"

SQLSTATE: F0001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02534: "could not remove old lock file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02535: "could not write lock file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02536: "'%s' is not a valid data directory"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02537: "database files are incompatible with server"

SQLSTATE: 22023

Description: Database files are incompatible with the software version.

Solution: Use the correct software version to rebuild the database.

GAUSS-02538: "could not set timer for authorization timeout"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02539: "could not disable timer for authorization timeout"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02540: "database '%s' has disappeared from pg_database"

SQLSTATE: 3D000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02541 - GAUSS-02550

<br/>

GAUSS-02541: "database '%s' is not currently accepting connections"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02542: "permission denied for database '%s'"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02543: "too many connections for database '%s'"

SQLSTATE: 53300

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02544: "database locale is incompatible with operating system"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02545: "bad backend ID: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02546: "new replication connections are not allowed during database shutdown"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02547: "must be system admin to connect during database shutdown"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02548: "must be system admin to connect in binary upgrade mode"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02551 - GAUSS-02560

<br/>

GAUSS-02551: "database %u does not exist"

SQLSTATE: 3D000

Description: The database does not exist.

Solution: Internal system error. Contact technical support.

GAUSS-02552: "Forbid remote connection with trust method!"

SQLSTATE: 42601

Description: Remotely connecting to the database using the trusted method is not allowed.

Solution: Check the connection security settings.

GAUSS-02554: "not able to set up signal action handler"

SQLSTATE: 53000

Description: Failed to create a signal processing function for a thread.

Solution: Internal system error. Contact technical support.

GAUSS-02555: "failed to create timer for thread"

SQLSTATE: XX000

Description: Failed to create a timer for a thread.

Solution: Internal system error. Contact technical support.

GAUSS-02556: "could not find function '%s' in file '%s'"

SQLSTATE: 42883

Description: Failed to find the function definition.

Solution: Ensure that the function is properly defined in the statement.

GAUSS-02557: "could not load library '%s': %s"

SQLSTATE: XX000

Description: Failed to load the library.

Solution: Check the library based on the prompted error information and troubleshoot the fault.

GAUSS-02558: "incompatible library '%s': missing magic block"

SQLSTATE: XX000

Description: The library is incompatible because the magic numbers are missing.

Solution: Check whether the library file is damaged.

GAUSS-02559: "incompatible library '%s': version mismatch"

SQLSTATE: XX000

Description: The library version is incompatible.

Solution: Ensure that the library file uses a compatible version.

GAUSS-02560: "incompatible library '%s': magic block mismatch"

SQLSTATE: XX000

Description: The library is incompatible because the magic numbers do not match.

Solution: Ensure that the library file uses a compatible version.

<br/>

## GAUSS-02561 - GAUSS-02570

<br/>

GAUSS-02563: "invalid macro name in dynamic library path: %s"

SQLSTATE: 42602

Description: The dynamic library address contains invalid path macros.

Solution: Check whether the path macro in the dynamic library address is valid.

GAUSS-02564: "zero-length component in parameter 'dynamic_library_path'"

SQLSTATE: 42602

Description: The length of the dynamic library address is 0, which is invalid.

Solution: Check the dynamic library path.

GAUSS-02565: "component in parameter 'dynamic_library_path' is not an absolute path"

SQLSTATE: 42602

Description: The dynamic library address is not an absolute path.

Solution: Check the dynamic library path.

GAUSS-02566: "init_MultiFuncCall cannot be called more than once"

SQLSTATE: 2F000

Description: Failed to call the **init_MultiFuncCall** function for multiple times.

Solution: Check the calling logic.

GAUSS-02567: "could not determine actual result type for function '%s' declared to return type %s"

SQLSTATE: 42804

Description: Failed to determine the return type of the function.

Solution: Check the return type of the function.

GAUSS-02568: "proallargtypes is not a 1-D Oid array"

SQLSTATE: XX000

Description: The function parameter is invalid.

Solution: Check the input parameter.

GAUSS-02569: "proargnames must have the same number of elements as the function has arguments"

SQLSTATE: 22023

Description: An internal system error occurs. The number of function parameters does not match **proargnames**.

Solution: Check the input parameter.

<br/>

## GAUSS-02571 - GAUSS-02580

<br/>

GAUSS-02572: "number of aliases does not match number of columns"

SQLSTATE: 42804

Description: The number of aliases does not match the number of columns.

Solution: Query the number of columns using the **\d+ ***tablename* command. Then, check whether the number of aliases of the current statement and that of columns are consistent.

GAUSS-02573: "no column alias was provided"

SQLSTATE: 42804

Description: Column aliases are not provided.

Solution: Provide the column aliases.

GAUSS-02574: "could not determine row description for function returning record"

SQLSTATE: 42804

Description: Failed to determine the **RECORD** type definition of the RECORD return type function.

Solution: Internal system error. Contact technical support.

GAUSS-02575: "internal function '%s' is not in internal lookup table"

SQLSTATE: 42883

Description: Failed to find the function definition in the function lookup table.

Solution: Ensure that the function in the SQL statement is valid.

GAUSS-02576: "unrecognized function API version: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02577: "language %u has old-style handler"

SQLSTATE: 42P13

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02578: "null result from info function '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02579: "unrecognized API version %d reported by info function '%s'"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02580: "fmgr_oldstyle received NULL pointer"

SQLSTATE: XX005

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02581 - GAUSS-02590

<br/>

GAUSS-02581: "function %u has too many arguments (%d, maximum is %d)"

SQLSTATE: 54023

Description: The function has too many input parameters.

Solution: Run the **\df** command to check whether the number of parameters defined by the function is consistent with that in the current query statement. The maximum number of function parameters is 16.

GAUSS-02583: "input function %u returned non-NULL"

SQLSTATE: XX005

Description: After the column data is processed, a null string is returned, which conflicts with the NOT NULL constraint.

Solution: Ensure that the column data is not empty and meets the NOT NULL constraint.

GAUSS-02584: "input function %u returned NULL"

SQLSTATE: XX005

Description: After the column data is processed, a not-null string is returned, which conflicts with the NULL constraint.

Solution: Ensure that the column data is empty and meets the NULL constraint.

GAUSS-02585: "receive function %u returned non-NULL"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02586: "receive function %u returned NULL"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02587: "could not reopen file '%s' as stderr: %m"

SQLSTATE: XX000

Description: Failed to open the file as the stderr redirection file.

Solution: Check whether files exist.

GAUSS-02588: "could not reopen file '%s' as stdout: %m"

SQLSTATE: XX000

Description: Failed to open the file as the stdout redirection file.

Solution: Check whether files exist.

GAUSS-02590: "buffer %d is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02591 - GAUSS-02600

<br/>

GAUSS-02591: "catcache reference %p is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02592: "catcache list reference %p is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02594: "partcache reference %s is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02595: "fakerel reference %s is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02596: "plancache reference %p is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02597: "tupdesc reference %p is not owned by resource owner %s"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-02598: "snapshot reference %p is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02599: "temporery file %d is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02600: "pthread mutex %p is not owned by resource owner %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.
