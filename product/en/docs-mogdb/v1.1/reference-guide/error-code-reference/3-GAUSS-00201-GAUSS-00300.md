---
title: GAUSS-00201 - GAUSS-00300
summary: GAUSS-00201 - GAUSS-00300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00201 - GAUSS-00300

<br/>

## GAUSS-00201 - GAUSS-00210

<br/>

GAUSS-00202: "unrecognized join type: %d"

SQLSTATE: XX004

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00203: "unrecognized set op: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00204: "bogus InhOption value: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00206: "subquery in FROM must have an alias"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00207: "unexpected non-SELECT command in subquery in FROM"

SQLSTATE: XX000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00210: "cannot use aggregate function in function expression in FROM"

SQLSTATE: 42803

Description: The function expression in the **FROM** clause is an aggregate function.

Solution: Do not use an aggregate function in the **FROM** clause.

<br/>

## GAUSS-00211 - GAUSS-00220

<br/>

GAUSS-00211: "cannot use window function in function expression in FROM"

SQLSTATE: 42P20

Description: The function expression in the **FROM** clause is a window function.

Solution: Do not use a window function in the **FROM** clause.

GAUSS-00212: "column name '%s' appears more than once in USING clause"

SQLSTATE: 42701

Description: During join operation, when the **USING** clause is used, the same column name appears in the clause more than once.

Solution: Ensure that the same column name appears only once in the **USING** clause.

GAUSS-00214: "column '%s' specified in USING clause does not exist in left table"

SQLSTATE: 42703

Description: Columns listed in the **USING** clause cannot be found in the left table.

Solution: Ensure that the column name used is in the left and right tables.

GAUSS-00217: "column alias list for '%s' has too many entries"

SQLSTATE: 42601

Description: The number of columns in the object alias exceeds the actual number of columns.

Solution: Ensure that the number of columns in the object alias is consistent with the actual number of columns.

GAUSS-00218: "argument of %s must not contain variables"

SQLSTATE: 42P10

Description: A clause similar to **LIMIT** uses a variable.

Solution: Do not use a variable in a clause similar to **LIMIT**.

GAUSS-00219: "argument of %s must not contain aggregate functions"

SQLSTATE: 42803

Description: A clause similar to **LIMIT** uses an aggregate function.

Solution: Do not use an aggregate function in a clause similar to **LIMIT**.

GAUSS-00220: "argument of %s must not contain window functions"

SQLSTATE: 42P20

Description: A clause similar to **LIMIT** uses a window function.

Solution: Do not use a window function in a clause similar to **LIMIT**.

<br/>

## GAUSS-00221 - GAUSS-00230

<br/>

GAUSS-00221: "%s '%s' is ambiguous"

SQLSTATE: 42702

Description: The column specified by **GROUP BY/ORDER BY** is ambiguous.

Solution: Ensure that the column specified by **GROUP BY/ORDER BY** is correct.

GAUSS-00222: "non-integer constant in %s"

SQLSTATE: 42601

Description: The constant specified by **GROUP BY/ORDER BY** is not of the integer type.

Solution: Ensure that the constant specified by **GROUP BY/ORDER BY** is an integer type.

GAUSS-00223: "%s position %d is not in select list"

SQLSTATE: 42P10

Description: The position of the constant specified by **GROUP BY/ORDER BY** is not in the current list.

Solution: Ensure that the constant position specified by **GROUP BY/ORDER BY** is in the current list.

GAUSS-00224: "window '%s' is already defined"

SQLSTATE: 42P20

Description: The window name has been defined.

Solution: Use an undefined window name.

GAUSS-00225: "window '%s' does not exist"

SQLSTATE: 42704

Description: The specified window object does not exist.

Solution: Define a window object before using it.

GAUSS-00226: "cannot override PARTITION BY clause of window '%s'"

SQLSTATE: 42P20

Description: The **PARTITION BY** clause in the window function is overwritten.

Solution: Ensure that the **PARTITION BY** clause in the window function is not overwritten.

GAUSS-00227: "cannot override ORDER BY clause of window '%s'"

SQLSTATE: 42P20

Description: The **ORDER BY** clause in the window function is overwritten.

Solution: Ensure that the **ORDER BY** clause in the window function is not overwritten.

GAUSS-00228: "cannot override frame clause of window '%s'"

SQLSTATE: 42P20

Description: The frame clause in the window function is overwritten.

Solution: Ensure that the frame clause in the window function is not overwritten.

GAUSS-00229: "in an aggregate with DISTINCT, ORDER BY expressions must appear in argument list"

SQLSTATE: 42P10

Description: In an aggregate operation, parameters in **DISTINCT** and **ORDER BY** expressions do not exist in the target list.

Solution: Ensure that the parameters exist in the target list.

GAUSS-00230: "SELECT DISTINCT ON expressions must match initial ORDER BY expressions"

SQLSTATE: 42P10

Description: When **DISTINCT ON** and **ORDER BY** are used simultaneously, their expressions are different.

Solution: Ensure that their expressions are the same.

<br/>

## GAUSS-00231 - GAUSS-00240

<br/>

GAUSS-00231: "get_matching_location: no matching sortgroupref"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00232: "operator %s is not a valid ordering operator"

SQLSTATE: 42809

Description: The operator specified using **ORDER BY** is an invalid sequencing operator.

Solution: When using **ORDER BY** to specify an operator, use only > or < for sequencing.

GAUSS-00233: "unrecognized sortby_dir: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00234: "unrecognized sortby_nulls: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00235: "window frame with value offset is not implemented"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00236: "aggregate function calls cannot be nested"

SQLSTATE: 42803

Description: The aggregate function is invoked in nested mode.

Solution: Ensure that the aggregate function is not invoked in nested mode.

GAUSS-00237: "aggregate function calls cannot contain window function calls"

SQLSTATE: 42803

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00238: "cache lookup failed for aggregate %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00239: "window function calls cannot be nested"

SQLSTATE: 42P20

Description: The window function is invoked in nested mode.

Solution: Cancel the nested invoking of the window function.

GAUSS-00240: "aggregates not allowed in WHERE clause"

SQLSTATE: 42803

Description: An aggregate function is used in the **WHERE** clause.

Solution: Do not use an aggregate function in the **WHERE** clause.

<br/>

## GAUSS-00241 - GAUSS-00250

<br/>

GAUSS-00241: "aggregates not allowed in JOIN conditions"

SQLSTATE: 42803

Description: The **JOIN** condition contains an aggregate function.

Solution: Do not use an aggregate function in the **JOIN** condition.

GAUSS-00242: "aggregates not allowed in GROUP BY clause"

SQLSTATE: 42803

Description: The **GROUP BY** clause contains an aggregate function.

Solution: Do not use an aggregate function in the **GROUP BY** clause.

GAUSS-00243: "aggregate functions not allowed in a recursive query's recursive term"

SQLSTATE: 42P19

Description: The recursive query uses an aggregate function.

Solution: Do not use an aggregate function in the recursive query.

GAUSS-00244: "window functions not allowed in WHERE clause"

SQLSTATE: 42P20

Description: The **WHERE** clause contains a window function.

Solution: Do not use a window function in the **WHERE** clause.

GAUSS-00245: "window functions not allowed in JOIN conditions"

SQLSTATE: 42P20

Description: The **JOIN** condition contains a window function.

Solution: Do not use a window function in the **JOIN** condition.

GAUSS-00246: "window functions not allowed in HAVING clause"

SQLSTATE: 42P20

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00247: "window functions not allowed in GROUP BY clause"

SQLSTATE: 42P20

Description: The **GROUP BY** clause contains a window function.

Solution: Do not use a window function in the **GROUP BY** clause.

GAUSS-00248: "window functions not allowed in window definition"

SQLSTATE: 42P20

Description: The **PARTITION** or **ORDER BY** clause contains a window function.

Solution: Do not use a window function in the **PARTITION** or **ORDER BY** clause.

GAUSS-00249: "column '%s.%s' must appear in the GROUP BY clause or be used in an aggregate function"

SQLSTATE: 42803

Description: The **GROUP BY** clause contains an aggregate function or lacks variables existing in the target list.

Solution: Do not use an aggregate function in the **GROUP BY** clause or include variables existing in the target list.

GAUSS-00250: "subquery uses ungrouped column '%s.%s' from outer query"

SQLSTATE: 42803

Description: The subquery uses an external ungrouped column.

Solution: Ensure that the subquery uses an external grouped column.

<br/>

## GAUSS-00251 - GAUSS-00260

<br/>

GAUSS-00251: "yylex_init() failed: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00252: "Invalid RTE found"

SQLSTATE: XX000

Description: The reference is not found.

Solution: Modify the SQL statement.

GAUSS-00253: "cannot assign to system column '%s'"

SQLSTATE: 0A000

Description: A value cannot be assigned to the system column.

Solution: Do not perform any operation on the system column.

GAUSS-00254: "cannot set an array element to DEFAULT"

SQLSTATE: 0A000

Description: The default value is set for an array element.

Solution: Do not set the default value of for an array element.

GAUSS-00255: "cannot set a subfield to DEFAULT"

SQLSTATE: 0A000

Description: The default value is set for an array subscript.

Solution: Do not set the default value for an array subscript.

GAUSS-00256: "column '%s' is of type %s but expression is of type %s"

SQLSTATE: 42804

Description: Data types at both sides of the expression are inconsistent.

Solution: Switch the data type at either side to make them consistent.

GAUSS-00257: "cannot assign to field '%s' of column '%s' because its type %s is not a composite type"

SQLSTATE: 42804

Description: A column is specified for a non-composite variable.

Solution: Do not specify a column for a non-composite variable.

GAUSS-00258: "cannot assign to field '%s' of column '%s' because there is no such column in data type %s"

SQLSTATE: 42703

Description: A column is specified for a non-composite variable.

Solution: Do not specify a column for a non-composite variable.

GAUSS-00259: "cannot assign to system column '%s'"

SQLSTATE: 42703

Description: A value cannot be assigned to the system column.

Solution: Do not perform any operation on the system column.

GAUSS-00260: "array assignment to '%s' requires type %s but expression is of type %s"

SQLSTATE: 42804

Description: The object to which a value is assigned does not match the actual expression type.

Solution: Modify the SQL statement.

<br/>

## GAUSS-00261 - GAUSS-00270

<br/>

GAUSS-00261: "subfield '%s' is of type %s but expression is of type %s"

SQLSTATE: 42804

Description: Data types at both sides of the expression are inconsistent.

Solution: Switch the data type at either side to make them consistent.

GAUSS-00262: "column '%s' specified more than once"

SQLSTATE: 42701

Description: A column is specified multiple times when inserting data to a table.

Solution: Insert data in each column only once.

GAUSS-00263: "SELECT * with no tables specified is not valid"

SQLSTATE: 42601

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00265: "Un-support feature"

SQLSTATE: XX000

Description: Feature not supported.

Solution: Do not use it.

GAUSS-00268: "VALUES lists must all be the same length"

SQLSTATE: 42601

Description: In the **INSERT … VALUES** statement, the number of values does not match that of table columns.

Solution: 1. Run **\d tblname** to check columns in the table. 2. Ensure that the number of values matches that of table columns. 3. Modify the syntax of the **INSERT … VALUES** statement.

GAUSS-00269: "VALUES must not contain table references"

SQLSTATE: 0A000

Description: The **SELECT** clause in the **INSERT … VALUES** statement contains a join list.

Solution: Ensure that the **SELECT** clause in the **INSERT … VALUES** statement does not contain a join list. Modify the INSERT statement based on its syntax.

GAUSS-00270: "VALUES must not contain OLD or NEW references"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Modify the SQL statement.

<br/>

## GAUSS-00271 - GAUSS-00280

<br/>

GAUSS-00271: "cannot use aggregate function in VALUES"

SQLSTATE: 42803

Description: The **VALUES** clause in the **INSERT** statement contains an aggregate function.

Solution: Ensure that the **VALUES** clause in the **INSERT** statement does not contain an aggregate function. Modify the **INSERT** statement based on its syntax.

GAUSS-00272: "cannot use window function in VALUES"

SQLSTATE: 42P20

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00273: "INSERT has more expressions than target columns"

SQLSTATE: 42601

Description: The number of valid expressions or values in the **INSERT** statement is greater than the corresponding number of target columns.

Solution: Ensure that the number of valid expressions or values in the **INSERT** statement is consistent with the corresponding number of target columns.

GAUSS-00274: "INSERT has more target columns than expressions"

SQLSTATE: 42601

Description: The number of target columns in the **INSERT** statement is greater than the corresponding number of valid expressions or values.

Solution: Ensure that the number of target columns in the **INSERT** statement is consistent with the corresponding number of valid expressions or values. If they are inconsistent, modify the **INSERT** statement.

GAUSS-00275: "SELECT … INTO is not allowed here"

SQLSTATE: 42601

Description: The syntax is incorrectly parsed. Specifically, the **SELECT** statement contains an **INTO** clause, which is not supported.

Solution: Modify the **SELECT** statement to ensure that it does not contain an **INTO** clause.

GAUSS-00276: "DEFAULT can only appear in a VALUES list within INSERT"

SQLSTATE: 42601

Description: The syntax is incorrectly parsed. Specifically, the **SELECT** statement contains the default expression or value, which is not supported.

Solution: Ensure that the syntax of the **SELECT** statement is correct.

GAUSS-00277: "SELECT FOR UPDATE/SHARE cannot be applied to VALUES"

SQLSTATE: 0A000

Description: The expression or value in the **SELECT** statement contains the **SELECT FOR UPDATE/SHARE** clause, which is not supported.

Solution: Ensure that the **SELECT** statement is correct.

GAUSS-00278: "SELECT FOR UPDATE/SHARE is not allowed with UNION/INTERSECT/EXCEPT"

SQLSTATE: 0A000

Description: The clause collection (UNION/INTERSECT/EXCEPT) in the **SELECT** statement contains the **SELECT FOR UPDATE/SHARE** clause, which is not supported.

Solution: Ensure that the **SELECT** statement is correct.

GAUSS-00279: "invalid UNION/INTERSECT/EXCEPT ORDER BY clause"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00280: "INTO is only allowed on first SELECT of UNION/INTERSECT/EXCEPT"

SQLSTATE: 42601

Description: The syntax is incorrectly parsed. The position of the **INTO** clause in the **SELECT..UNION/INTERSECT/EXCEPT** statement is incorrect.

Solution: Ensure that the **INTO** clause exists only in the **SELECT..UNION/INTERSECT/EXCEPT** statement.

<br/>

## GAUSS-00281 - GAUSS-00290

<br/>

GAUSS-00281: "UNION/INTERSECT/EXCEPT member statement cannot refer to other relations of same query level"

SQLSTATE: 42P10

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00282: "each %s query must have the same number of columns"

SQLSTATE: 42601

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-00283: "cannot use aggregate function in UPDATE"

SQLSTATE: 42803

Description: In the **UPDATE** syntax, the value in the column to be updated cannot be an aggregate function.

Solution: Ensure that the column value to be updated in the **UPDATE** syntax is not an aggregate function. If the column value to be updated is an aggregate function, modify the **UPDATE** syntax.

GAUSS-00284: "cannot use window function in UPDATE"

SQLSTATE: 42P20

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00285: "UPDATE target count mismatch - internal error"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00286: "column '%s.%s' of relation '%s' does not exist"

SQLSTATE: 42703

Description: In the **UPDATE** syntax, the column name to be updated does not exist.

Solution: Ensure that the column name to be updated in the **UPDATE** syntax is correct.

GAUSS-00287: "cannot use aggregate function in RETURNING"

SQLSTATE: 42803

Description: The **RETURNING** clause in the **INSERT/UPDATE/DELETE** syntax contains an aggregate function, which is not supported.

Solution: Ensure that the **RETURNING** clause in the **INSERT/UPDATE/DELETE** syntax is correct.

GAUSS-00288: "cannot use window function in RETURNING"

SQLSTATE: 42P20

Description: Syntax not supported.

Solution: Modify the SQL statement.

<br/>

## GAUSS-00291 - GAUSS-00300

<br/>

GAUSS-00291: "unexpected non-SELECT command in DECLARE CURSOR"

SQLSTATE: XX000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00292: "DECLARE CURSOR must not contain data-modifying statements in WITH"

SQLSTATE: 0A000

Description: The **WITH** clause in the **DECLARE CURSOR** syntax contains the **INSERT/UPDATE/DELETE** statement.

Solution: Check and modify the query statement.

GAUSS-00293: "DECLARE CURSOR WITH HOLD … FOR UPDATE/SHARE is not supported"

SQLSTATE: 0A000

Description: The **DECLARE CURSOR WITH HOLD … FOR SelectStmt** clause contains the **UPDATE/SHARE** statement, which is not supported.

Solution:DECLARE CURSOR WITH HOLD … Check and modify the query statement.

GAUSS-00294: "DECLARE SCROLL CURSOR … FOR UPDATE/SHARE is not supported"

SQLSTATE: 0A000

Description: The **DECLARE SCROLL CURSOR… FOR SelectStmt** clause contains the **UPDATE/SHARE** statement, which is not supported.

Solution: Check and modify the query statement.

GAUSS-00295: "DECLARE INSENSITIVE CURSOR … FOR UPDATE/SHARE is not supported"

SQLSTATE: 0A000

Description: The **DECLARE INSENSITIVE CURSOR … FOR SelectStmt** clause contains the **UPDATE/SHARE** statement, which is not supported.

Solution: Check and modify the query statement.

GAUSS-00298: "PGXC Node %s: object not defined"

SQLSTATE: 42704

Description: The specified node object is abnormal or does not exist.

Solution: Ensure that the specified node name exists and is operating properly. If the specified node is abnormal, restart the node.

GAUSS-00300: "EXECUTE DIRECT cannot execute DML queries"

SQLSTATE: 0A000

Description:EXECUTE DIRECT ON ( nodename [, … In the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax, a DML statement is executed on a DN, which is not supported.

Solution: Do not execute a DML statement in the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax.
