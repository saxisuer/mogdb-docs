---
title: GAUSS-00101 - GAUSS-00200
summary: GAUSS-00101 - GAUSS-00200
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00101 - GAUSS-00200

<br/>

## GAUSS-00101 - GAUSS-00110

<br/>

GAUSS-00101: "table '%s' has %d columns available but %d columns specified"

SQLSTATE: 42P10

Description: The number of table columns to be queried is greater than the total number of table columns.

Solution: Ensure that the number of table columns to be queried is less than or equal to the total number of table columns.

GAUSS-00102: "too many column aliases specified for function %s"

SQLSTATE: 42P10

Description: A function alias contains more than one column.

Solution: Ensure that a function alias contains only one column.

GAUSS-00103: "relation '%s' is not partitioned table"

SQLSTATE: 42P01

Description: The table is not a partitioned table.

Solution: Before performing an operation on the table, use the system catalog to ensure that the table is a partitioned table.

GAUSS-00104: "partition '%s' of relation '%s' does not exist"

SQLSTATE: 42P01

Description: The partitioned table does not exist.

Solution: When using a partitioned table, query the **pg_partition** table to check whether the partitioned table exists.

GAUSS-00105: "The partition number is invalid or out-of-range"

SQLSTATE: XX000

Description: The value inserted to the partition is out of the partition range.

Solution: Ensure that the value to be inserted to a partition is within the range of the partition.

GAUSS-00106: "unsupported partition type"

SQLSTATE: XX000

Description: The partition type is not supported.

Solution: Change the partition type. Currently, row-store and column-store tables support only range partitioning.

GAUSS-00107: "relation '%s.%s' does not exist"

SQLSTATE: 42P01

Description: The table object in the specified schema does not exist.

Solution: Use an object that exists.

GAUSS-00108: "relation '%s' does not exist"

SQLSTATE: 42P01

Description: The object does not exist.

Solution: Use an object that exists.

GAUSS-00109: "a column definition list is only allowed for functions returning 'record'"

SQLSTATE: 42601

Description: The column definition list (or table definition) is not allowed for functions that return values of a type other than record.

Solution: Do not use table definitions to specify return values for functions that do not return values of the record type.

GAUSS-00110: "a column definition list is required for functions returning 'record'"

SQLSTATE: 42601

Description: The column definition list (or table definition) is required for functions that return values of the record type.

Solution: Use table definitions to specify return values for functions that return values of the record type.

<br/>

## GAUSS-00111 - GAUSS-00120

<br/>

GAUSS-00111: "column '%s' cannot be declared SETOF"

SQLSTATE: 42P16

Description: In the value returned for the function, a column is specified to SETOF.

Solution: Ensure that no column is specified to SETOF.

GAUSS-00112: "function '%s' in FROM has unsupported return type %s"

SQLSTATE: 42804

Description: The value returned for the function is of an unsupported type.

Solution: Switch the type to another common type and try again. Check whether this problem persists. Contact technical support.

GAUSS-00113: "VALUES lists '%s' have %d columns available but %d columns specified"

SQLSTATE: 42P10

Description: The number of parameters in the **VALUES** expression is inconsistent with the number of specified columns.

Solution: Ensure that the number of parameters in the **VALUES** expression is consistent with the number of specified columns.

GAUSS-00114: "joins can have at most %d columns"

SQLSTATE: 54000

Description: The number of columns in a join is greater than the upper limit (32,767).

Solution: Ensure that the number of columns in a join is less than or equal to the upper limit (32767).

GAUSS-00115: "WITH query '%s' does not have a RETURNING clause"

SQLSTATE: 0A000

Description: In the non-SELECT with statement, no **RETURNING** clause is specified.

Solution: Specify a **RETURNING** clause.

GAUSS-00116: "function in FROM has unsupported return type"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00117: "unrecognized RTE kind: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00118: "invalid attnum %d for rangetable entry %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00119: "cache lookup failed for attribute %d of relation %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00120: "subquery %s does not have attribute %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-00121 - GAUSS-00130

<br/>

GAUSS-00121: "column %d of relation '%s' does not exist"

SQLSTATE: 42703

Description: The column does not exist.

Solution: Ensure that the specified column exists in the table.

GAUSS-00122: "values list %s does not have attribute %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00123: "invalid varattno %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00124: "invalid attribute number %d"

SQLSTATE: 42704

Description: The table attribute ID is invalid.

Solution:Contact technical support.

GAUSS-00125: "invalid reference to FROM-clause entry for table '%s'"

SQLSTATE: 42P01

Description: In a **FROM** clause, the reference to the table is invalid.

Solution: Use correct syntax when referencing the table in a **FROM** clause.

GAUSS-00126: "missing FROM-clause entry for table '%s'"

SQLSTATE: 42P01

Description: The **WITH** statement lacks a **FROM** clause.

Solution: Ensure that the **WITH** statement contains a **FROM** clause.

GAUSS-00127: "Password must be quoted"

SQLSTATE: 42601

Description: The password does not use quotation marks ("").

Solution: Use quotation marks when specifying a password.

GAUSS-00128: "unrecognized role option '%s'"

SQLSTATE: 42601

Description: An unidentified user option is used.

Solution: Use the user option provided in the R&D support document.

GAUSS-00129: "'SET %s TO rolename' not yet supported"

SQLSTATE: 42601

Description:**set name to rolename** is not supported.

Solution: Use **set name rolename**.

GAUSS-00130: "'SET %s = rolename' not yet supported"

SQLSTATE: 42601

Description:**set name = rolename** is not supported.

Solution: Use **set name rolename**.

<br/>

## GAUSS-00131 - GAUSS-00140

<br/>

GAUSS-00131: "current database cannot be changed"

SQLSTATE: 0A000

Description: A system catalog is set in the current database, which is not allowed.

Solution: Do not set a system catalog.

GAUSS-00132: "time zone interval must be HOUR or HOUR TO MINUTE"

SQLSTATE: 42601

Description: The time interval format is incorrect.

Solution: The format must be **HOUR** or **HOUR TO MINUTE**.

GAUSS-00133: "interval precision specified twice"

SQLSTATE: 42601

Description: The interval precision is specified more than once.

Solution: Specify the interval precision only once.

GAUSS-00134: "MATCH PARTIAL not yet implemented"

SQLSTATE: 0A000

Description: Partial matching is used for reference.

Solution: Internal system error. Contact technical support.

GAUSS-00135: "unrecognized distribution option '%s'"

SQLSTATE: 42601

Description: The distribution column option is unidentified.

Solution: Use a supported distribution column option. For details, see en-us_topic_0237122117.html.

GAUSS-00136: "Invalid option %s"

SQLSTATE: 42601

Description: The option is invalid.

Solution: Use a valid option.

GAUSS-00137: "constraint declared INITIALLY DEFERRED must be DEFERRABLE"

SQLSTATE: 42601

Description: The constraint set to **INITIALLY DEFERRED** is not **DEFERRABLE**.

Solution: Ensure that the constraint set to **INITIALLY DEFERRED** is **DEFERRABLE**.

GAUSS-00138: "conflicting constraint properties"

SQLSTATE: 42601

Description: The constraints conflict with each other.

Solution: Do not use constraints that conflict with each other simultaneously.

GAUSS-00139: "CREATE ASSERTION is not yet implemented"

SQLSTATE: 0A000

Description: The **CREATE ASSERTION** statement is not supported.

Solution: Do not use an unsupported statement.

GAUSS-00140: "DROP ASSERTION is not yet implemented"

SQLSTATE: 0A000

Description: The **DROP ASSERTION** statement is not supported.

Solution: Do not use an unsupported statement.

<br/>

## GAUSS-00141 - GAUSS-00150

<br/>

GAUSS-00141: "missing argument"

SQLSTATE: 42601

Description: The parameter is missing when an operator is deleted.

Solution: Use the parameter defined for an operator when deleting the operator.

GAUSS-00142: "WITH CHECK OPTION is not implemented"

SQLSTATE: 0A000

Description: This option is not supported.

Solution: Use a supported option.

GAUSS-00144: "CREATE TABLE AS EXECUTE not yet supported"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Use a supported syntax. For details, see en-us_topic_0237121923.html.

GAUSS-00145: "number of columns does not match number of values"

SQLSTATE: 42601

Description: The number of columns to be modified is inconsistent with the number of provided values.

Solution: Ensure that the number of columns to be modified is consistent with the number of provided values.

GAUSS-00146: "Sort method %s is not supported!"

SQLSTATE: XX000

Description: The sorting method is not supported.

Solution: Use a supported option. For details, see en-us_topic_0237121923.html.

GAUSS-00148: "VALUES in FROM must have an alias"

SQLSTATE: 42601

Description: The **VALUES** statement does not use an alias in the **FROM** clause.

Solution: Use an alias of the **VALUES** statement in the **FROM** clause.

GAUSS-00149: "precision for type float must be at least 1 bit"

SQLSTATE: 22023

Description: The float precision is set to a value less than 1.

Solution: Set the float precision to a value greater than or equal to 1.

GAUSS-00150: "precision for type float must be less than 54 bits"

SQLSTATE: 22023

Description: The float precision is set to a value greater than 54.

Solution: Set the float precision to a value less than or equal to 54.

<br/>

## GAUSS-00151 - GAUSS-00160

<br/>

GAUSS-00151: "UNIQUE predicate is not yet implemented"

SQLSTATE: 0A000

Description: The **UNIQUE** clause is not implemented.

Solution: Use a clause that has been implemented.

GAUSS-00152: "RANGE PRECEDING is only supported with UNBOUNDED"

SQLSTATE: 0A000

Description:**UNBOUNDED** is not specified when **RANGE PRECEDING** is used.

Solution: Specify **UNBOUNDED** when using **RANGE PRECEDING**.

GAUSS-00153: "RANGE FOLLOWING is only supported with UNBOUNDED"

SQLSTATE: 0A000

Description:**UNBOUNDED** is not specified when **RANGE FOLLOWING** is used.

Solution: Specify **UNBOUNDED** when using **RANGE FOLLOWING**.

GAUSS-00154: "frame start cannot be UNBOUNDED FOLLOWING"

SQLSTATE: 42P20

Description:**UNBOUNDED FOLLOWING** is specified for the frame start.

Solution: Do not set the frame start to **UNBOUNDED FOLLOWING**.

GAUSS-00155: "frame starting from following row cannot end with current row"

SQLSTATE: 42P20

Description: The frame starts from the next row and ends in the current row.

Solution: Ensure that the frame does not end in the current row if it starts from the next row.

GAUSS-00156: "frame end cannot be UNBOUNDED PRECEDING"

SQLSTATE: 42P20

Description:**UNBOUNDED PRECEDING** is specified for the frame end.

Solution: Do not set the frame end to **UNBOUNDED PRECEDING**.

GAUSS-00157: "frame starting from current row cannot have preceding rows"

SQLSTATE: 42P20

Description: The frame starts from the current row and ends in the previous row.

Solution: Ensure that the frame does not end in the previous row if it starts from the current row.

GAUSS-00158: "frame starting from following row cannot have preceding rows"

SQLSTATE: 42P20

Description: The frame starts from the next row and ends in the previous row.

Solution: Ensure that the frame does not end in the previous row if it starts from the next row.

GAUSS-00159: "type modifier cannot have parameter name"

SQLSTATE: 42601

Description: The parameter schema for the number of rows contains a parameter.

Solution: Ensure that the parameter schema for the number of rows does not contain a parameter.

GAUSS-00160: "wrong number of parameters on left side of OVERLAPS expression"

SQLSTATE: 42601

Description: The number of left parameters in an overlaps function is greater than 2.

Solution: Use no more than two left parameters in an overlaps function.

<br/>

## GAUSS-00161 - GAUSS-00170

<br/>

GAUSS-00161: "wrong number of parameters on right side of OVERLAPS expression"

SQLSTATE: 42601

Description: The number of right parameters in an overlaps function is greater than 2.

Solution: Use no more than two left parameters in an overlaps function.

GAUSS-00162: "multiple ORDER BY clauses not allowed"

SQLSTATE: 42601

Description: Multiple **ORDER BY** clauses are used in the **INSERT INTO SELECT** statement.

Solution: Use one **ORDER BY** clause in the **INSERT INTO SELECT** statement.

GAUSS-00163: "multiple OFFSET clauses not allowed"

SQLSTATE: 42601

Description: Multiple **OFFSET** clauses are used in the **INSERT INTO SELECT** statement.

Solution: Use one **OFFSET** clause in the **INSERT INTO SELECT** statement.

GAUSS-00164: "multiple LIMIT clauses not allowed"

SQLSTATE: 42601

Description: Multiple **LIMIT** clauses are used in the **INSERT INTO SELECT** statement.

Solution: Use one **LIMIT** clause in the **INSERT INTO SELECT** statement.

GAUSS-00165: "multiple WITH clauses not allowed"

SQLSTATE: 42601

Description: Multiple **WITH** clauses are used in the **INSERT INTO SELECT** statement.

Solution: Use one **WITH** clause in the **INSERT INTO SELECT** statement.

GAUSS-00166: "OUT and INOUT arguments aren't allowed in TABLE functions"

SQLSTATE: 42601

Description: The **OUT** and **INOUT** parameters are used in table functions.

Solution: Do not use the **OUT** and **INOUT** parameters in table functions.

GAUSS-00167: "multiple COLLATE clauses not allowed"

SQLSTATE: 42601

Description: Multiple **COLLATE** clauses are specified.

Solution: Specify only one **COLLATE** clause.

GAUSS-00168: "unexpected node type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00169: "%s constraints cannot be marked DEFERRABLE"

SQLSTATE: 0A000

Description: The constraint that cannot be marked with **DEFERRABLE** is used.

Solution: Modify the SQL statement.

GAUSS-00170: "%s constraints cannot be marked NOT VALID"

SQLSTATE: 0A000

Description: The constraint that cannot be marked with **NOT VALID** is used.

Solution: Modify the SQL statement.

<br/>

## GAUSS-00171 - GAUSS-00180

<br/>

GAUSS-00171: "%s constraints cannot be marked NO INHERIT"

SQLSTATE: 0A000

Description: The constraint that cannot be marked with **NO INHERIT** is used.

Solution: Modify the SQL statement.

GAUSS-00172: "undefined or invalid action for statement ' alter system '"

SQLSTATE: 42704

Description: The option length is not 1 in the **ALTER SYSTEM** statement.

Solution: Ensure that the option length is 1.

GAUSS-00173: "unsupported action '%s' for statement ' alter system '"

SQLSTATE: 42704

Description: An unsupported option is used in the **ALTER SYSTEM** statement.

Solution: Use a supported option. For details, see en-us_topic_0237122075.html.

GAUSS-00174: "undefined action '%s' for statement ' alter system '"

SQLSTATE: 42704

Description: An unsupported option is used in the **ALTER SYSTEM** statement.

Solution: Use a supported option. For details, see en-us_topic_0237122075.html.

GAUSS-00175: "missing or invalid session ID"

SQLSTATE: 42704

Description: The **Session ID** value used in the **ALTER SYSTEM** statement is incorrect.

Solution: Use a correct **Session ID** value. For details about **Session ID** values, see the **dv_sessions** view.

GAUSS-00176: "parameter '%s' is assigned more than once"

SQLSTATE: 42710

Description: Duplicate parameter names exist.

Solution: Ensure that the parameter names in a function are different.

GAUSS-00177: "parameter '%s' is undefined"

SQLSTATE: 42P02

Description: Undefined parameter.

Solution: Modify the SQL statement.

GAUSS-00178: "the parameter located '%d' have been assigned"

SQLSTATE: 42710

Description: A value has been assigned to the parameter mode.

Solution: Modify the SQL statement.

GAUSS-00179: "parameter mode %c doesn't exist"

SQLSTATE: XX000

Description: An invalid parameter mode is used.

Solution: Use a valid parameter mode.

GAUSS-00180: "output argument located '%d' doesnot assigned"

SQLSTATE: XX000

Description: The type of an output parameter is not set.

Solution: Set types for all parameters.

<br/>

## GAUSS-00181 - GAUSS-00190

<br/>

GAUSS-00181: "function '%s' doesn't exist "

SQLSTATE: 42883

Description: The function is not defined.

Solution: Modify the SQL statement.

GAUSS-00182: "function '%s' isn't exclusive "

SQLSTATE: 42723

Description: Duplicate function definitions exist.

Solution: Ensure that no duplicate functions exist in the system.

GAUSS-00183: "function '%s' with %d parameters doesn't exist "

SQLSTATE: 42883

Description: The function is not defined.

Solution: Modify the SQL statement.

GAUSS-00184: "invalid name: %s"

SQLSTATE: 42601

Description: The variable name is invalid.

Solution: Name a variable in compliance with naming rules. For details, see en-us_topic_0237120321.html.

GAUSS-00185: "WITH query name '%s' specified more than once"

SQLSTATE: 42712

Description: The name of a **WITH** clause is specified multiple times.

Solution: Specify the name of a **WITH** clause only once.

GAUSS-00187: "unexpected utility statement in WITH"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00188: "WITH clause containing a data-modifying statement must be at the top level"

SQLSTATE: 0A000

Description: The non-SELECT with statement is not placed at the beginning of the whole query.

Solution: Place the non-SELECT with statement at the beginning of the whole query.

GAUSS-00189: "wrong number of output columns in WITH"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00190: "recursive query '%s' column %d has type %s in non-recursive term but type %s overall"

SQLSTATE: 42804

Description: The column type in the non-recursive query is inconsistent with that in the recursive query.

Solution: Convert the output type in the non-recursive query to that in the recursive query.

<br/>

## GAUSS-00191 - GAUSS-00200

<br/>

GAUSS-00191: "recursive query '%s' column %d has collation '%s' in non-recursive term but collation '%s' overall"

SQLSTATE: 42P21

Description: The column character set in the recursive query is inconsistent with that of that in the non-recursive query.

Solution: Convert the output character set of the non-recursive query to that of the recursive query.

GAUSS-00192: "WITH query '%s' has %d columns available but %d columns specified"

SQLSTATE: 42P10

Description: The number of parameters specified in the recursive query is inconsistent with the number of parameters required for the actual query.

Solution: Ensure that the number of parameters specified in the recursive query is consistent with the number of parameters required.

GAUSS-00193: "mutual recursion between WITH items is not implemented"

SQLSTATE: 0A000

Description: Recursion between queries is not implemented.

Solution: Use a supported function.

GAUSS-00194: "recursive query '%s' must not contain data-modifying statements"

SQLSTATE: 42P19

Description: The recursive query contains non-query statements.

Solution: Ensure that the recursive query contains only query statements.

GAUSS-00196: "missing recursive reference"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00197: "ORDER BY in a recursive query is not implemented"

SQLSTATE: 0A000

Description:**ORDER BY** is used in a recursive query.

Solution: Do not use **ORDER BY** in a recursive query.

GAUSS-00198: "OFFSET in a recursive query is not implemented"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00199: "LIMIT in a recursive query is not implemented"

SQLSTATE: 0A000

Description:**LIMIT** is used in a recursive query.

Solution: Do not use **LIMIT** in a recursive query.

GAUSS-00200: "FOR UPDATE/SHARE in a recursive query is not implemented"

SQLSTATE: 0A000

Description:**FOR UPDATE/SHARE** is used in a recursive query.

Solution: Do not use **FOR UPDATE/SHARE** in a recursive query.
