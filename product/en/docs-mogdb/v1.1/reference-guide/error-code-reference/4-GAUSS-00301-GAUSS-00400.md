---
title: GAUSS-00301 - GAUSS-00400
summary: GAUSS-00301 - GAUSS-00400
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00301 - GAUSS-00400

<br/>

## GAUSS-00301 - GAUSS-00310

<br/>

GAUSS-00301: "EXECUTE DIRECT cannot execute this utility query"

SQLSTATE: 0A000

Description: In the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax, a DML statement is executed on a DN, which is not supported.

Solution: Do not execute a DDL statement in the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax.

GAUSS-00302: "EXECUTE DIRECT cannot execute locally this utility query"

SQLSTATE: 0A000

Description: In the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax, a **Set** variable statement is executed on a DN.

Solution: Do not execute a **Set** variable statement in the **EXECUTE DIRECT ON ( nodename [, … ] ) query** syntax.

GAUSS-00303: "SELECT FOR UPDATE/SHARE is not allowed with DISTINCT clause"

SQLSTATE: 0A000

Description: The **DISTINCT** clause cannot be used in the **SELECT FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00304: "SELECT FOR UPDATE/SHARE is not allowed with GROUP BY clause"

SQLSTATE: 0A000

Description: The **GROUP BY** clause cannot be used in the **SELECT … FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00305: "SELECT FOR UPDATE/SHARE is not allowed with HAVING clause"

SQLSTATE: 0A000

Description: The **HAVING** clause cannot be used in the **SELECT … FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00306: "SELECT FOR UPDATE/SHARE is not allowed with aggregate functions"

SQLSTATE: 0A000

Description: Aggregate functions cannot be used in the **SELECT … FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00307: "SELECT FOR UPDATE/SHARE is not allowed with window functions"

SQLSTATE: 0A000

Description: Window functions cannot be used in the **SELECT … FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00308: "SELECT FOR UPDATE/SHARE is not allowed with set-returning functions in the target list"

SQLSTATE: 0A000

Description: Functions with defined return values cannot be used in the **SELECT … FOR UPDATE/SHARE** syntax.

Solution: Modify the statement.

GAUSS-00309: "SELECT FOR UPDATE/SHARE must specify unqualified relation names"

SQLSTATE: 42601

Description: In the **SELECT … FOR UPDATE/SHARE…OF table_name** syntax, the specified table name contains database or schema references.

Solution: Ensure that the specified table name does not contain database or schema references.

GAUSS-00310: "SELECT FOR UPDATE/SHARE cannot be used with column table '%s'"

SQLSTATE: 0A000

Description: The table specified in the **SELECT … FOR UPDATE/SHARE…OF table_name** syntax is a column-store table.

Solution: Modify the syntax to ensure that the specified table cannot be a column-store table.

<br/>

## GAUSS-00311 - GAUSS-00320

<br/>

GAUSS-00311: "SELECT FOR UPDATE/SHARE cannot be applied to a join"

SQLSTATE: 0A000

Description: The range table in the **FOR UPDATE/SHARE…OF table_name** syntax is a join table.

Solution: Modify the syntax to ensure that the specified table is not a join table.

GAUSS-00312: "SELECT FOR UPDATE/SHARE cannot be applied to a function"

SQLSTATE: 0A000

Description: The range table in the **SELECT … FOR UPDATE/SHARE…OF table_name** syntax is a function.

Solution: Modify the syntax to ensure that the specified table is not a function.

GAUSS-00313: "SELECT FOR UPDATE/SHARE cannot be applied to a WITH query"

SQLSTATE: 0A000

Description: The range table in the **SELECT … FOR UPDATE/SHARE…OF table_name** syntax is a list identified by the **WITH** clause.

Solution: Modify the syntax to ensure that the specified range table is not a list identified by the **WITH** clause.

GAUSS-00314: "unrecognized RTE type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00315: "relation '%s' in FOR UPDATE/SHARE clause not found in FROM clause"

SQLSTATE: 42P01

Description: *table_name* in the **SELECT … FROM table1_name FOR UPDATE/SHARE…OF table_name** syntax does not exist in the **FROM** clause.

Solution: Ensure that *table_name* exists in the **FROM** clause.

GAUSS-00316: "unsupport 'like clause including partition' for partitioned table"

SQLSTATE: 42P16

Description: In the **CREATE TABLE … LIKE source_table** syntax, the table name in the **LIKE** clause is specified as a partitioned table.

Solution: Modify the syntax to ensure that the **PARTITION BY** clause is used if the table name is specified as a partitioned table.

GAUSS-00317: "unsupport 'like clause including reloptions' together with 'with'"

SQLSTATE: 42P16

Description: In the **CREATE TABLE … LIKE source_table** syntax, the **LIKE** clause contains **INCLUDING RELOPTIONS**, which is used together with the **WITH** clause in the source table. As a result, a syntax conflict occurs.

Solution: Do not use **INCLUDING RELOPTIONS** together with the **WITH** clause in the source table. Use either of them because both storage parameters will be copied to a new table. "

GAUSS-00318: "array of serial is not implemented"

SQLSTATE: 0A000

Description: The column type in the **CREATE TABLE** or **ALTER TABLE** syntax is a serial array, causing an error.

Solution: Ensure that the column type is not a serial array.

GAUSS-00319: "conflicting NULL/NOT NULL declarations for column '%s' of table '%s'"

SQLSTATE: 42601

Description: The non-null column names specified in the **CREATE TABLE** or **ALTER TABLE** syntax are in conflict with the null column names specified in the constraint clause.

Solution: Ensure that the null column names specified in the constraint clause are consistent with the column names specified in the column name list.

GAUSS-00320: "multiple default values specified for column '%s' of table '%s'"

SQLSTATE: 42601

Description: Multiple names are specified for a column in the constraint clause of the **CREATE TABLE** or **ALTER TABLE** syntax.

Solution: Specify only one name for a column.

<br/>

## GAUSS-00321 - GAUSS-00330

<br/>

GAUSS-00321: "column exclusion constraints are not supported"

SQLSTATE: XX000

Description: Syntax not supported.

Solution: Modify the SQL statement.

GAUSS-00322: "unrecognized constraint type: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00323: "invalid context for constraint type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00324: "'%s' is not a table, view, composite type, or foreign table"

SQLSTATE: 42809

Description: *source_table* in the **CREATE TABLE … LIKE source_table** syntax is not a relational table, view table, composite table, or foreign table.

Solution: Ensure that *source_table* is a relational table, view, composite type, or foreign table.

GAUSS-00325: "could not specify 'INCLUDING PARTITION' for non-partitioned-table relation:'%s'"

SQLSTATE: 42809

Description:**INCLUDING PARTITION** is specified for a partitioned table in the **CREATE TABLE … LIKE source_table** syntax.

Solution: Do not specify **INCLUDING PARTITION** for a partitioned table in the **CREATE TABLE … LIKE source_table** syntax.

GAUSS-00326: "could not specify 2 or more 'INCLUDING PARTITION' clauses, only one is allowed"

SQLSTATE: 42809

Description: More than one **LIKE…INCLUDING PARTITION** clause is specified in the **CREATE TABLE … (LIKE source_table INCLUDING PARTITION)** syntax.

Solution: Specify only one **LIKE…INCLUDING PARTITION** clause in the **CREATE TABLE … (LIKE source_table INCLUDING PARTITION)** syntax.

GAUSS-00327: "could not specify 2 or more 'INCLUDING RELOPTIONS' clauses, only one is allowed"

SQLSTATE: 42809

Description: More than one **INCLUDING RELOPTIONS** clause is specified in the **LIKE** clause of the **CREATE TABLE … LIKE source_table** syntax.

Solution: Specify only one **INCLUDING RELOPTIONS** clause in the **LIKE** clause of the **CREATE TABLE … LIKE source_table** syntax.

GAUSS-00328: "could not specify 2 or more 'INCLUDING DISTRIBUTION' clauses, only one is allowed"

SQLSTATE: 42809

Description: More than one **INCLUDING DISTRIBUTION** clause is specified in the **LIKE** clause of the **CREATE TABLE … LIKE source_table** syntax.

Solution: Specify only one **INCLUDING DISTRIBUTION** clause in the **LIKE** clause of the **CREATE TABLE … LIKE source_table** syntax.

GAUSS-00329: "Postgres-XC does not support VIEW in LIKE clauses"

SQLSTATE: 0A000

Description: Views cannot be used in the **LIKE** clause of the **CREATE TABLE … LIKE** syntax.

Solution: Delete views from the **LIKE** clause of the **CREATE TABLE … LIKE** syntax.

GAUSS-00330: "cannot convert whole-row table reference"

SQLSTATE: 0A000

Description: Syntax not supported.

Solution: Modify the SQL statement.

<br/>

## GAUSS-00331 - GAUSS-00340

<br/>

GAUSS-00331: "cache lookup failed on source like relation %u for reloptions"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00332: "null partition key value for relation '%s'"

SQLSTATE: XX000

Description: The null partition key value cannot be used.

Solution: Correct the partition key value.

GAUSS-00333: "partition key column's number of relation '%s' is not a 1-D smallint array"

SQLSTATE: XX000

Description: The number of partition key columns or column type is incorrect for **INCLUDING PARTITION** specified in the **LIKE** clause of the **CREATE TABLE … LIKE** syntax.

Solution: Ensure that the number of partition key columns is 1 and the column type is an integer.

GAUSS-00334: "partition key column's number of %s not in the range of all its columns"

SQLSTATE: XX000

Description: The number of partition key columns for **INCLUDING PARTITION** specified in the **LIKE** clause of the **CREATE TABLE … LIKE** syntax exceeds the column quantity range of the current table.

Solution: Ensure that the number of partition key columns ranges from 1 to the maximum number of allowed columns of the current table.

GAUSS-00335: "' including partition ' for list partitioned relation: '%s' not implemented yet"

SQLSTATE: XX000

Description: The partition type is incorrect for **INCLUDING PARTITION** specified in the **LIKE** clause of the **CREATE TABLE … LIKE** syntax.

Solution: Ensure that the partition type is range or hash partitioning. List partitioning is not supported.

GAUSS-00336: "cache lookup failed for constraint %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00337: "null conexclop for constraint %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00338: "cache lookup failed for operator %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00339: "too few entries in indexprs list"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00340: "cache lookup failed for collation %u"

SQLSTATE: 22P06

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-00341 - GAUSS-00350

<br/>

GAUSS-00341: "cache lookup failed for opclass %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00342: "Partitioned table does not support EXCLUDE index"

SQLSTATE: 0A000

Description: The exclusion constraint cannot be specified in the **CREATE TABLE … CONSTRAINT** syntax when creating a partitioned table.

Solution: Modify the syntax to specify other constraints when creating a partitioned table.

GAUSS-00343: "Invalid PRIMARY KEY/UNIQUE constraint for partitioned table"

SQLSTATE: 0A000

Description: In the **CREATE TABLE … CONSTRAINT** syntax, the partition key column for the partitioned table is not contained in the **PRIMARY KEY** or **UNIQUE** constraint for creating a partitioned table.

Solution: Ensure that the partition key column for the partitioned table is contained.

GAUSS-00344: "multiple primary keys for table '%s' are not allowed"

SQLSTATE: 42P16

Description: More than one primary key for **INCLUDING INDEXES** is specified in the **LIKE** clause of the **CREATE TABLE … LIKE source_table** syntax.

Solution: Specify only one primary key.

GAUSS-00345: "cannot use an existing index in CREATE TABLE"

SQLSTATE: 0A000

Description: A constraint is incorrectly specified for the index column in the **CREATE TABLE** syntax.

Solution: Add a new constraint to the data table using the **ALTER TABLE … ADD table_constraint** syntax.

GAUSS-00346: "index '%s' does not exist"

SQLSTATE: 42704

Description: The attribute name for specifying constraints in the **CREATE/ALTER TABLE** syntax does not exist in the relational table.

Solution: Ensure that the attribute name exists in the relational table.

GAUSS-00347: "index '%s' is already associated with a constraint"

SQLSTATE: 55000

Description: Usage not supported.

Solution: Create another index.

GAUSS-00348: "index '%s' does not belong to table '%s'"

SQLSTATE: 55000

Description: The attribute name for specifying constraints in the **CREATE/ALTER TABLE table_name** syntax does not exist in *table_name*.

Solution: Ensure that the attribute name exists.

GAUSS-00349: "index '%s' is not valid"

SQLSTATE: 55000

Description: The attribute name for specifying constraints in the **CREATE/ALTER TABLE** syntax is invalid.

Solution: Ensure that the attribute name exists in a relational or schema table.

GAUSS-00350: "'%s' is not a unique index"

SQLSTATE: 42809

Description: In the **CREATE TABLE … CONSTRAINT** syntax, the index in the **PRIMARY KEY** or **UNIQUE** constraint for creating a partitioned table is not unique.

Solution: Ensure that the index is unique.

<br/>

## GAUSS-00351 - GAUSS-00360

<br/>

GAUSS-00351: "index '%s' contains expressions"

SQLSTATE: 42809

Description: In the **CREATE TABLE … CONSTRAINT** syntax, the index in the **PRIMARY KEY** or **UNIQUE** constraint for creating a partitioned table contains an expression.

Solution: Ensure that the index in the **PRIMARY KEY** or **UNIQUE** constraint for creating a partitioned table in the **CREATE TABLE … CONSTRAINT** syntax does not contain an expression.

GAUSS-00352: "'%s' is a partial index"

SQLSTATE: 42809

Description: Usage not supported.

Solution: Replace it with a valid index.

GAUSS-00353: "'%s' is a deferrable index"

SQLSTATE: 42809

Description: Usage not supported.

Solution: Replace it with a valid index.

GAUSS-00354: "index '%s' is not a btree"

SQLSTATE: 42809

Description: The index specified in the B-tree function is not a B-tree index.

Solution: Use an index function corresponding to the index type.

GAUSS-00355: "index '%s' does not have default sorting behavior"

SQLSTATE: 42809

Description: Usage not supported.

Solution: Replace it with a valid index.

GAUSS-00356: "inherited relation '%s' is not a table"

SQLSTATE: 42809

Description: The table specified in the **INHERITS** clause of the **CREATE TABLE … INHERITS parent_table** syntax is an invalid data table.

Solution: Ensure that the table specified in the **INHERITS** clause of the **CREATE TABLE … INHERITS parent_table** syntax exists.

GAUSS-00357: "column '%s' named in key does not exist"

SQLSTATE: 42703

Description: The column specified by the constraint or **INHERITS** clause of the **CREATE TABLE** syntax does not exist in a table.

Solution: If the column does not exist, replace the inherited column name or re-create a table.

GAUSS-00358: "column '%s' appears twice in primary key constraint"

SQLSTATE: 42701

Description: Two identical primary keys exist in the **UNIQUE** constraint specified in the **PRIMARY KEY** clause of the **CREATE TABLE … CONSTRAINT** syntax.

Solution: Change or delete a primary key.

GAUSS-00359: "column '%s' appears twice in unique constraint"

SQLSTATE: 42701

Description: Two identical primary keys exist in the **PRIMARY KEY** constraint specified in the **UNIQE** clause of the **CREATE TABLE … CONSTRAINT** syntax.

Solution: Change or delete a primary key.

<br/>

## GAUSS-00361 - GAUSS-00370

<br/>

GAUSS-00361: "access method '%s' does not support column store"

SQLSTATE: 0A000

Description: The access method specified in the **CREATE INDEX…USING method** syntax does not support the column storage table.

Solution: Modify the syntax to ensure that the access method can only be the psort type.

GAUSS-00362: "index expression cannot return a set"

SQLSTATE: 42804

Description: The index expression of one or multiple columns specified in the **CREATE INDEX** syntax returns a set.

Solution: Modify the syntax to ensure that the created index expression does not return a set.

GAUSS-00363: "index expressions and predicates can refer only to the table being indexed"

SQLSTATE: 42P10

Description: The created expression of one or multiple columns based on this table specified in the **CREATE INDEX** syntax belongs to different tables.

Solution: Ensure that the created expression of one or multiple columns based on this table specified in the **CREATE INDEX** syntax belongs to the same table.

GAUSS-00364: "unrecognized event type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-00365: "rule WHERE condition cannot contain references to other relations"

SQLSTATE: 42P17

Description: The **WHERE** condition expression in the **CREATE RULE** syntax uses tables other than **NEW** and **OLD** for reference.

Solution: Modify the syntax to ensure that the **WHERE** condition expression only use **NEW** and **OLD** for reference.

GAUSS-00368: "Rule may not use NOTIFY, it is not yet supported"

SQLSTATE: 42P17

Description: The rule action command specified in the **CREATE RULE** syntax incorrectly uses the **NOTIFY** clause.

Solution: Ensure that the rule action command specified in the **CREATE RULE** syntax contains only the **SELECT**, **INSERT**, **UPDATE**, or **DELETE** clause. Modify the syntax because the **NOTIFY** clause is not supported in the current version.

GAUSS-00369: "rules with WHERE conditions can only have SELECT, INSERT, UPDATE, or DELETE actions"

SQLSTATE: 42P17

Description: The **CREATE RULE** syntax incorrectly uses the functional data definition statements, such as **CREATE**, **DESTROY**, **COPY**, and **VACUUM**.

Solution: Do not use those statements. Currently, only the **SELECT**, **INSERT**, **UPDATE**, or **DELETE** clause is supported in the **CREATE RULE** syntax.

GAUSS-00370: "conditional UNION/INTERSECT/EXCEPT statements are not implemented"

SQLSTATE: 0A000

Description: The query tree generated based on the user-created rule filters **UNION**, **INTERSECT**, and **EXCEPT**. Currently, this execution mode is not supported.

Solution: Alternatively, rewrite the rule to avoid unsupported behavior. Contact technical support.

<br/>

## GAUSS-00371 - GAUSS-00380

<br/>

GAUSS-00371: "ON SELECT rule cannot use OLD"

SQLSTATE: 42P17

Description: The name of the referenced table in the **WHERE** condition expression of the **CREATE RULE name AS ON SELECT** syntax is **OLD**.

Solution: Change the referenced table name to **non-OLD**.

GAUSS-00372: "ON SELECT rule cannot use NEW"

SQLSTATE: 42P17

Description: The name of the referenced table in the **WHERE** condition expression of the **CREATE RULE name AS ON SELECT** syntax is **NEW**.

Solution: Change the referenced table name to **non-NEW**.

GAUSS-00373: "ON INSERT rule cannot use OLD"

SQLSTATE: 42P17

Description: The name of the referenced table in the **WHERE** condition expression of the **CREATE RULE name AS ON INSERT** syntax is **OLD**.

Solution: Change the referenced table name to **non-OLD**.

GAUSS-00374: "ON DELETE rule cannot use NEW"

SQLSTATE: 42P17

Description: The name of the referenced table in the **WHERE** condition expression of the **CREATE RULE name AS ON DELETE** syntax is **NEW**.

Solution: Change the referenced table name to **non-NEW**.

GAUSS-00377: "missing definition of adding partition"

SQLSTATE: XX000

Description: When the **ALTER TABLE ADD PARTITION** syntax is used to add a partition to a specified partitioned table, invalid values exist in the partition columns, indexes, constraints, or parent tables defined in the partition.

Solution: Change invalid values to valid values.

GAUSS-00378: "misplaced DEFERRABLE clause"

SQLSTATE: 42601

Description: The constraint types of columns specified by the **column_constraint** clause in the **CREATE TABLE** or **ALTER TABLE** syntax are deferrable types.

Solution: Ensure that the constraint types of columns can be deferrable only when the types are primary key, UNIQUE, exclusion, or foreign key.

GAUSS-00379: "multiple DEFERRABLE/NOT DEFERRABLE clauses not allowed"

SQLSTATE: 42601

Description: Deferrable types and non-deferrable types exist in the **column_constraint** clause of the **CREATE TABLE** or **ALTER TABLE** syntax.

Solution: Modify the **column_constraint** clause to ensure that either deferrable types or non-deferrable types exist.

GAUSS-00380: "misplaced NOT DEFERRABLE clause"

SQLSTATE: 42601

Description: The constraint types of columns specified by the **column_constraint** clause in the **CREATE TABLE** or **ALTER TABLE** syntax are non-deferrable types.

Solution: Ensure that the constraint types of columns can be non-deferrable only when the types are primary key, UNIQUE, exclusion, or foreign key.

<br/>

## GAUSS-00381 - GAUSS-00390

<br/>

GAUSS-00381: "misplaced INITIALLY DEFERRED clause"

SQLSTATE: 42601

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-00382: "multiple INITIALLY IMMEDIATE/DEFERRED clauses not allowed"

SQLSTATE: 42601

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-00383: "misplaced INITIALLY IMMEDIATE clause"

SQLSTATE: 42601

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-00384: "CREATE specifies a schema (%s) different from the one being created (%s)"

SQLSTATE: 42P15

Description: In the **CREATE SCHEMA schema_name schema_element…** syntax, the schema name contained in the **CREATE TABLE**, **CREATE VIEW**, **CREATE INDEX**, **CREATE PARTITION**, or **GRANT** clause specified by schema_element does not match **schema_name**.

Solution: In the **CREATE SCHEMA schema_name schema_element…** syntax, ensure that the schema name contained in the **CREATE TABLE**, **CREATE VIEW**, **CREATE INDEX**, **CREATE PARTITION**, or **GRANT** clause specified by **schema_element** matches **schema_name**. "

GAUSS-00385: "unsupport inherits clause for partitioned table"

SQLSTATE: 42601

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-00386: "inherited relation '%s' is a foreign table"

SQLSTATE: 42809

Description: Usage not supported.

Solution: Modify the SQL statement.

GAUSS-00387: "inherited relation '%s' is a partitioned table"

SQLSTATE: XX000

Description: *parent_table* in the **CREATE TABLE table_name (…) INHERITS (parent_table [, … ])** syntax is a partitioned table.

Solution: Ensure that the inherited *parent_table* required by the syntax is not a partitioned table.

GAUSS-00389: "Typed table can't not be partitioned"

SQLSTATE: 42601

Description: The **CREATE … TABLE OF type_name …** syntax cannot be used in this version currently.

Solution: Use other syntax.

GAUSS-00390: "unsupported feature with temporary/unlogged table for partitioned table"

SQLSTATE:42601

Description: The partitioned table in the **CREATE TEMPORARY/UNLOGGED TABLE table_name …** syntax does not support the **TEMPORARY** or **UNLOGGED** attribute.

Solution: Change the partitioned table to a non-partitioned table.

<br/>

## GAUSS-00391 - GAUSS-00400

<br/>

GAUSS-00392: "too many partition keys for partitioned table"

SQLSTATE: 42P16

Description: When a created table is a range partitioned table, the number of specified partition columns exceeds 4 (the maximum number).

Solution: Ensure that the maximum number of partition columns is not greater than 4 when the created table is a range partitioned table.

GAUSS-00393: "too many partitions for partitioned table"

SQLSTATE: 42P16

Description: When the **PARTITION BY RANGE(partition_key)(range_partition_definition_list)…** syntax is used to create a range partitioned table, the number of specified columns for partition values exceeds 32767 (the maximum number). "

Solution: Ensure that the number of specified columns for partition values is not greater than 32767.

GAUSS-00394: "Range partitioned table with INTERVAL was forbidden"

SQLSTATE: 42P16

Description: The **PARTITION BY RANGE(partition_key) INTERVAL** clause (interval partition) cannot be used in the current version.

Solution: Do not use the PARTITION BY RANGE(partition_key) INTERVAL clause (interval partition) in the current version.

GAUSS-00395: "Range partitioned table with INTERVAL clause has more than one column"

SQLSTATE: 42P16

Description: Usage not supported.

Solution: Modify the SQL statement.

GAUSS-00397: "duplicate partition name: '%s'"

SQLSTATE: 42710

Description: When the **PARTITION BY RANGE(partition_key)(range_partition_definition_list)…** syntax is used to create a range partitioned table, duplicate partition names exist in the specified lists for partition values.

Solution: Check whether duplicate partition names exist in the specified lists for partition values. If yes, change the duplicate partition names.

GAUSS-00398: "column '%s' appears twice in partial cluster key constraint"

SQLSTATE: 42701

Description: Duplicate column names exist in the **columnList** list of the **CREATE TABLE … CONSTRAINT PARTIAL CLUSTER KEY (columnList)** syntax.

Solution: Change the duplicate column names.

GAUSS-00399: "column name '%s' conflicts with a system column name"

SQLSTATE: 42701

Description: The column names specified in the **CREATE TABLE** syntax are the same as those retained in the system catalog.

Solution: Change the column names as prompted to ensure that the column names are different.

GAUSS-00400: "access method 'psort' does not support WHERE clause"

SQLSTATE: 0A000

Description: When psort is specified for the **INDEX** clause in the **CREATE INDEX** or **ALTER TABLE** syntax, the **INDEX** clause contains a **WHERE** clause.

Solution: Modify the syntax to ensure that the **WHERE** clause does not exist.
