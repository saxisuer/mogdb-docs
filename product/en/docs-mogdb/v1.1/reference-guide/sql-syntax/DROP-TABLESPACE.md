---
title: DROP TABLESPACE
summary: DROP TABLESPACE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TABLESPACE

## Function

**DROP TABLESPACE** deletes a tablespace.

## Precautions

- Only the tablespace owner or a user granted with the DROP permission can run the **DROP TABLESPACE** command. The system administrator has this permission by default.
- The tablespace to be deleted should not contain any database objects. Otherwise, an error will be reported.
- **DROP TABLESPACE** cannot be rolled back and therefore cannot be run in transaction blocks.
- During execution of **DROP TABLESPACE**, database queries by other sessions using **\db** may fail and need to be reattempted.
- If **DROP TABLESPACE** fails to be executed, run **DROP TABLESPACE IF EXISTS**.

## Syntax

```sql
DROP TABLESPACE [ IF EXISTS ] tablespace_name;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified tablespace does not exist.

- **tablespace_name**

  Specifies the name of the tablespace to be deleted.

  Value range: an existing tablespace name

## Examples

See **Examples** in **CREATE TABLESPACE**.

## Suggestions

- drop database

  Do not delete databases during transactions.
