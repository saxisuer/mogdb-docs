---
title: ALTER RULE
summary: ALTER RULE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER RULE

## Function

**ALTER RULE** modifies rule definitions.

## Precautions

- Only the user who has applied a table or view of a specified rule can run the **ALTER RULE** command.
- Currently, only the rule name can be modified.

## Syntax

```sql
ALTER RULE name ON table_name RENAME TO new_name
```

## Parameter Description

- **name**

  Specifies the name of the rule to be modified.

- **table_name**

  Specifies the name of the table for which the rule is to be created.

  Value range: an existing table name in the database.

- **new_name**

  Specifies the new name of the rule.

## Example

```sql
ALTER RULE notify_all ON emp RENAME TO notify_me;
```
