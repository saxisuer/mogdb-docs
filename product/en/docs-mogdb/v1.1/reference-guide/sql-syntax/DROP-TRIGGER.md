---
title: DROP TRIGGER
summary: DROP TRIGGER
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TRIGGER

## Function

**DROP TRIGGER** deletes a trigger.

## Precautions

Only the owner of a trigger or a system administrator has the **DROP TRIGGER** permission.

## Syntax

```sql
DROP TRIGGER [ IF EXISTS ] trigger_name ON table_name [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified trigger does not exist.

- **trigger_name**

  Specifies the name of the trigger to be deleted.

  Value range: an existing trigger name

- **table_name**

  Specifies the name of the table containing the trigger.

  Value range: name of the table containing the trigger

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects that depend on the trigger.
  - **RESTRICT**: refuses to delete the trigger if any objects depend on it. This is the default action.

## Examples

For details, see **Examples** in CREATE TRIGGER.
