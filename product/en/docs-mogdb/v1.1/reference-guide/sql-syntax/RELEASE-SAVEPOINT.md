---
title: RELEASE SAVEPOINT
summary: RELEASE SAVEPOINT
author: Zhang Cuiping
date: 2021-05-18
---

# RELEASE SAVEPOINT

## Function

**RELEASE SAVEPOINT** destroys a savepoint previously defined in the current transaction.

Destroying a savepoint makes it unavailable as a rollback point, but it has no other user visible behavior. It does not undo the effects of statements executed after the savepoint was established. To do that, use **ROLLBACK TO SAVEPOINT**. Destroying a savepoint when it is no longer needed allows the system to reclaim some resources earlier than transaction end.

**RELEASE SAVEPOINT** also destroys all savepoints that were established after the named savepoint was established.

## Precautions

- Specifying a savepoint name that was not previously defined causes an error.
- It is not possible to release a savepoint when the transaction is in an aborted state.
- If multiple savepoints have the same name, only the one that was most recently defined is released.

## Syntax

```sql
RELEASE [ SAVEPOINT ] savepoint_name;
```

## Parameter Description

**savepoint_name**

Specifies the name of the savepoint you want to destroy.

## Examples

```sql
-- Create a table.
mogdb=# CREATE TABLE tpcds.table1(a int);

-- Start a transaction.
mogdb=# START TRANSACTION;

-- Insert data.
mogdb=# INSERT INTO tpcds.table1 VALUES (3);

-- Establish a savepoint.
mogdb=# SAVEPOINT my_savepoint;

-- Insert data.
mogdb=# INSERT INTO tpcds.table1 VALUES (4);

-- Delete the savepoint.
mogdb=# RELEASE SAVEPOINT my_savepoint;

-- Commit the transaction.
mogdb=# COMMIT;

-- Query the table content, which should contain both 3 and 4.
mogdb=# SELECT * FROM tpcds.table1;

-- Delete the table.
mogdb=# DROP TABLE tpcds.table1;
```
