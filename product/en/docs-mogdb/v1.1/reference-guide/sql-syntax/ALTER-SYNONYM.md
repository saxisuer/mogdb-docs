---
title: ALTER SYNONYM
summary: ALTER SYNONYM
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SYNONYM

## Function

**ALTER SYNONYM** modifies the attributes of the **SYNONYM** object.

## Precautions

- Currently, only the owner of the **SYNONYM** object can be changed.
- Only the system administrator has the permission to modify the owner of the **SYNONYM** object.
- The new owner must have the **CREATE** permission on the schema where the **SYNONYM** object resides.

## Syntax

```sql
ALTER SYNONYM synonym_name
    OWNER TO new_owner;
```

## Parameter Description

- **synonym**

  Specifies the name of the synonym to be modified, which can contain the schema name.

  Value range: a string. It must comply with the naming convention rule.

- **new_owner**

  Specifies the new owner of the **SYNONYM** object.

  Value range: a string. It must be a valid username.

## Examples

```sql
-- Create synonym t1.
mogdb=#  CREATE OR REPLACE SYNONYM t1 FOR ot.t1;

-- Create a user u1.
mogdb=# CREATE USER u1 PASSWORD 'user@111';

-- Change the owner of synonym t1 to u1.
mogdb=# ALTER SYNONYM t1 OWNER TO u1;

-- Delete synonym t1.
mogdb=# DROP SYNONYM t1;

-- Delete user u1.
mogdb=# DROP USER u1;
```
