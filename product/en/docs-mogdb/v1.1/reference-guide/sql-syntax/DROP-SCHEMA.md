---
title: DROP SCHEMA
summary: DROP SCHEMA
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SCHEMA

## Function

**DROP SCHEMA** deletes a schema from the current database.

## Precautions

Only the schema owner or a user granted with the DROP permission can run the **DROP SCHEMA** command. The system administrator has this permission by default.

## Syntax

```sql
DROP SCHEMA [ IF EXISTS ] schema_name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified schema does not exist.

- **schema_name**

  Specifies the name of the schema to be deleted.

  Value range: an existing schema name

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes all the objects contained in the schema.
  - **RESTRICT**: refuses to delete the schema if the schema contains objects. This is the default action.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** Schemas beginning with **pg_temp** or **pg_toast_temp** are for internal use. Do not delete them. Otherwise, unexpected consequences may be incurred.
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The schema currently being used cannot be deleted. To delete it, switch to another schema first.

## Examples

See **Examples** in **CREATE SCHEMA**.
