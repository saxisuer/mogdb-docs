---
title: DROP PROCEDURE
summary: DROP PROCEDURE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP PROCEDURE

## Function

**DROP PROCEDURE** deletes a stored procedure.

## Precautions

None

## Syntax

```sql
DROP PROCEDURE [ IF EXISTS  ] procedure_name ;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified stored procedure does not exist.

- **procedure_name**

  Specifies the name of the stored procedure to be deleted.

  Value range: an existing stored procedure name
