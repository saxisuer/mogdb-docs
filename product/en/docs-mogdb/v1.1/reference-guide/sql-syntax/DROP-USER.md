---
title: DROP USER
summary: DROP USER
author: Zhang Cuiping
date: 2021-05-18
---

# DROP USER

## Function

**DROP USER** deletes a user and the schema with the same name as the user.

## Precautions

- **CASCADE** is used to delete the objects (excluding databases) that depend on the user. **CASCADE** cannot delete locked objects unless the objects are unlocked or the processes locking the objects are killed.
- In MogDB, the **enable_kill_query** configuration parameter exists in the **postgresql.conf** file. This parameter affects **CASCADE**.
  - If **enable_kill_query** is **on** and **CASCADE** is used, the statement automatically kills the processes locking dependent objects and then deletes the specified user.
  - If **enable_kill_query** is **off** and **CASCADE** is used, the statement waits until the processes locking dependent objects stop and then deletes the specified user.
- If the dependent objects are other databases or reside in other databases, manually delete them before deleting the user from the current database. **DROP USER** cannot delete objects across databases.
- If a data source depends on the user, the user cannot be deleted directly. You need to manually delete the data source first.

## Syntax

```sql
DROP USER [ IF EXISTS ] user_name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified user does not exist.

- **user_name**

  Specifies the name of the user to be deleted.

  Value range: an existing username

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects that depend on the user.

  - **RESTRICT**: refuses to delete the user if any objects depend on it. This is the default action.

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
    > In MogDB, the **enable_kill_query** configuration parameter exists in the **postgresql.conf** file. This parameter affects **CASCADE**.
    >
    > - If **enable_kill_query** is **on** and **CASCADE** is used, the statement automatically kills the processes locking dependent objects and then deletes the specified user.
    > - If **enable_kill_query** is **off** and **CASCADE** is used, the statement waits until the processes locking dependent objects stop and then deletes the specified user.

## Examples

See **Example** in **CREATE USER**.
