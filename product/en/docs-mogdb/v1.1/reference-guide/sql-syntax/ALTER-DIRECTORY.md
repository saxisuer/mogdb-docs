---
title: ALTER DIRECTORY
summary: ALTER DIRECTORY
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER DIRECTORY

## Function

**ALTER DIRECTORY** modifies a directory.

## Precautions

- Currently, only the directory owner can be changed.
- The owner can only be a user with the **sysadmin** permission.

## Syntax

```sql
ALTER DIRECTORY directory_name
    OWNER TO new_owner;
```

## Parameter Description

**directory_name**

Specifies the name of a directory to be modified. The value must be an existing directory name.

## Examples

```sql
-- Create a directory.
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

-- Change the owner of the directory.
mogdb=# ALTER DIRECTORY dir OWNER TO system;

-- Delete the directory.
mogdb=# DROP DIRECTORY dir;
```
