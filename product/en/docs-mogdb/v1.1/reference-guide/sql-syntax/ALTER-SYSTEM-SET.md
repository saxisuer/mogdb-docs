---
title: ALTER SYSTEM SET
summary: ALTER SYSTEM SET
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SYSTEM SET

## Function

The **ALTER SYSTEM SET** command is used to set GUC parameters of the POSTMASTER, SIGHUP, and BACKEND levels. This command writes parameters into the configuration file. The time to take effect varies according to the level.

## Precautions

- Only users with administrator permissions can run this command.
- The effective time of GUC parameters at different levels is as follows:
  - The GUC parameters at the POSTMASTER level take effect only after the system is restarted.
  - The GUC parameters at the BACKEND level take effect only after the session is reconnected.
  - The GUC parameters at the SIGHUP level take effect immediately. (Actually, there is a slight delay to wait for the thread reloading the parameter.)
- You can set the **audit_set_parameter** parameter to specify whether the operation is audited.
- The operation can be synchronized to the standby server.
- The operation is the same as **gs_guc**, which does not pay attention to whether the database is a primary or standby node or whether the database is read-only.
- The operation cannot be executed in a transaction because it cannot be rolled back.

## Syntax

```sql
ALTER SYSTEM SET parameter TO value;
```

## Parameter Description

- **parameter**

  GUC parameter

- **value**

  GUC parameter value

## Examples

```sql
-- Set a SIGHUP-level parameter audit_enabled.
mogdb=#  alter system set audit_enabled to off;
ALTER SYSTEM SET
mogdb=#  show audit_enabled;
 audit_enabled
---------------
 off
(1 row)

-- The setting of the POSTMASTER-level parameter enable_thread_pool takes effect after the system is restarted.
mogdb=# alter system set enable_thread_pool to on;
NOTICE:  please restart the database for the POSTMASTER level parameter to take effect.
ALTER SYSTEM SET
```
