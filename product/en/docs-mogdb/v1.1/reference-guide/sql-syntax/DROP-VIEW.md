---
title: DROP VIEW
summary: DROP VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# DROP VIEW

## Function

**DROP VIEW** forcibly deletes a view from the database.

## Precautions

Only the view owner or a user granted with the DROP permission can run the **DROP VIEW** command. The system administrator has this permission by default.

## Syntax

```sql
DROP VIEW [ IF EXISTS ] view_name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified view does not exist.

- **view_name**

  Specifies the name of the view to be deleted.

  Value range: an existing view name

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects (such as other views) that depend on the view.
  - **RESTRICT**: refuses to delete the view if any objects depend on it. This is the default action.

## Examples

See **Examples** in **CREATE VIEW**.
