---
title: CREATE SERVER
summary: CREATE SERVER
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SERVER

## Function

**CREATE SERVER** defines a new foreign server.

## Syntax

```sql
CREATE SERVER server_name
    FOREIGN DATA WRAPPER fdw_name
    OPTIONS ( { option_name ' value ' } [, ...] ) ;
```

## Parameter Description

- **server_name**

  Specifies the server name.

  Value range: a string containing no more than 63 characters

- **fdw_name**

  Specifies the name of the foreign data wrapper.

  Value range: oracle_fdw, mysql_fdw, postgres_fdw, mot_fdw

- **OPTIONS ( { option_name ' value ' } [, …] )**

    Specifies options for the server. These options typically define the connection details of the server, but the actual names and values depend on the foreign data wrapper of the server.

  - Options supported by oracle_fdw are as follows:

    - **dbserver**

        Connection string of the remote Oracle database.

    - **isolation_level** (default value: **serializable**)

        Oracle database transaction isolation level.

        Value range: serializable, read_committed, read_only

  - Options supported by mysql_fdw are as follows:

    - **host** (default value: **127.0.0.1**)

        IP address of the MySQL server or MariaDB.

    - **port** (default value: **3306**)

        Listening port number of the MySQL server or MariaDB.

  - The options supported by postgres_fdw are the same as those supported by libpq. For details, see *Connection Character Strings*. Note that the following options cannot be set:

    - **user** and **password**

        The user name and password are specified when the user mapping is created.

    - **client_encoding**

        The encoding mode of the local server is automatically obtained and set.

    - **application_name**

        This option is always set to **postgres_fdw**.

In addition to the connection parameters supported by libpq, the following options are provided:

- **use_remote_estimate**

  Controls whether postgres_fdw issues the EXPLAIN command to obtain the estimated run time. The default value is **false**.

- **fdw_startup_cost**

  Estimates the startup time required for a foreign table scan, including the time to establish a connection, analyze the request at the remote server, and generate a plan. The default value is **100**.

- **fdw_typle_cost**

  Specifies the additional consumption when each tuple is scanned on a remote server. The value specifies the extra consumption of data transmission between servers. The default value is **0.01**.
