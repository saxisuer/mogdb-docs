---
title: ALTER FOREIGN TABLE
summary: ALTER FOREIGN TABLE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER FOREIGN TABLE

## Function

**ALTER FOREIGN TABLE** modifies a foreign table.

## Syntax

```sql
ALTER FOREIGN TABLE [ IF EXISTS ]  table_name
    OPTIONS ( {[ ADD | SET | DROP ] option ['value']}[, ... ]);
```

## Parameter Description

- **table_name**

  Specifies the name of an existing foreign table to be modified.

  Value range: an existing foreign table name.

- **option**

  Specifies the option of a foreign table or foreign table column to be modified. **ADD**, **SET**, and **DROP** are operations to be performed. If no operation is set explicitly, the default value **ADD** is used. The option name must be unique (although table options and table column options can share the same name). The name and value of the option are also validated by a class library of a foreign data wrapper.

  - Options supported by oracle_fdw are as follows:

    - **table**

        Name of a table on the Oracle server. The value must be the same as the table name recorded in the Oracle system catalog. Generally, the value consists of uppercase letters.

    - **schema**

        Schema (or owner) corresponding to the table. The value must be the same as the table name recorded in the Oracle system catalog. Generally, the value consists of uppercase letters.

  - Options supported by mysql_fdw are as follows:

    - **dbname**

        Name of the MySQL database.

    - **table_name**

        Name of a table in the MySQL database.

  - Options supported by postgres_fdw are as follows:

    - **schema_name**

        Schema name of a remote server. If this option is not specified, the schema name of the foreign table is used as the schema name of the remote server.

    - **table_name**

        Table name of a remote server. If this option is not specified, the name of the foreign table is used as the table name of the remote server.

    - **column_name**

        Column name of a table on a remote server. If this option is not specified, the column name of the foreign table is used as the column name of a table on a remote server.

- **value**

  Specifies the new value of **option**.
