---
title: REINDEX
summary: REINDEX
author: Zhang Cuiping
date: 2021-05-18
---

# REINDEX

## Function

**REINDEX** rebuilds an index using the data stored in the index's table, replacing the old copy of the index.

There are several scenarios in which **REINDEX** can be used:

- An index has become corrupted, and no longer contains valid data.
- An index has become "bloated", that is, it contains many empty or nearly-empty pages.
- You have altered a storage parameter (such as a fill factor) for an index, and wish that the change takes full effect.
- An index build with the **CONCURRENTLY** option failed, leaving an "invalid" index.

## Precautions

**REINDEX DATABASE** and **REINDEX SYSTEM** type cannot be performed in transaction blocks.

## Syntax

- Rebuild a general index.

  ```sql
  REINDEX { INDEX | [INTERNAL] TABLE | DATABASE | SYSTEM } name [ FORCE ];
  ```

- Rebuild an index partition.

  ```sql
  REINDEX  { INDEX|  [INTERNAL] TABLE} name
      PARTITION partition_name [ FORCE  ];
  ```

## Parameter Description

- **INDEX**

  Recreates the specified index.

- **INTERNAL TABLE**

  Recreates the Desc table index of a column-store table or Hadoop internal table. The TOAST table (if any) of the table is reindexed as well.

- **TABLE**

  Recreates all indexes of a specified table. If a table has a TOAST table, the table will also be reindexed.

- **DATABASE**

  Recreates all indexes within the current database.

- **SYSTEM**

  Recreates all indexes on system catalogs within the current database. Indexes on user tables are not processed.

- **name**

  Specifies the name of the index, table, or database whose index needs to be recreated. Tables and indexes can be schema-qualified.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > **REINDEX DATABASE** and **SYSTEM** can create indexes for only the current database. Therefore, **name** must be the same as the current database name.

- **FORCE**

  Specifies an invalid option, which will be ignored.

- **partition_name**

  Specifies the name of the partition or index partition to be recreated.

  Value range:

  - If **REINDEX INDEX** is used, specify the name of an index partition.
  - If it is **REINDEX TABLE**, specify the name of a partition.
  - If it is **REINDEX INTERNAL TABLE**, specify the name of a partition in a column-store partitioned table.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> **REINDEX DATABASE** and **REINDEX SYSTEM** type cannot be performed in transaction blocks.

## Examples

```sql
-- Create a row-store table tpcds.customer_t1 and create an index on the c_customer_sk column in the table.
mogdb=# CREATE TABLE tpcds.customer_t1
(
    c_customer_sk             integer               not null,
    c_customer_id             char(16)              not null,
    c_current_cdemo_sk        integer                       ,
    c_current_hdemo_sk        integer                       ,
    c_current_addr_sk         integer                       ,
    c_first_shipto_date_sk    integer                       ,
    c_first_sales_date_sk     integer                       ,
    c_salutation              char(10)                      ,
    c_first_name              char(20)                      ,
    c_last_name               char(30)                      ,
    c_preferred_cust_flag     char(1)                       ,
    c_birth_day               integer                       ,
    c_birth_month             integer                       ,
    c_birth_year              integer                       ,
    c_birth_country           varchar(20)                   ,
    c_login                   char(13)                      ,
    c_email_address           char(50)                      ,
    c_last_review_date        char(10)
)
WITH (orientation = row);

mogdb=# CREATE INDEX tpcds_customer_index1 ON tpcds.customer_t1 (c_customer_sk);

mogdb=# INSERT INTO tpcds.customer_t1 SELECT * FROM tpcds.customer WHERE c_customer_sk < 10;

-- Rebuild a single index.
mogdb=# REINDEX INDEX tpcds.tpcds_customer_index1;

-- Rebuild all indexes in the tpcds.customer_t1 table:
mogdb=# REINDEX TABLE tpcds.customer_t1;

Delete the tpcds.customer_t1 table.
mogdb=# DROP TABLE tpcds.customer_t1;
```

## Suggestions

- INTERNAL TABLE

  This scenario is used for fault recovery. You are not advised to perform concurrent operations.

- DATABASE

  You can not reindex a database in a transaction.

- SYSTEM

  You can not reindex system catalogs in transactions.
