---
title: DROP RULE
summary: DROP RULE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP RULE

## Function

**DROP RULE** deletes a rule.

## Syntax

```sql
DROP RULE [ IF EXISTS ] name ON table_name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Reports a notice If a rule does not exist.

- **name**

  Specifies the name of the current rule to be deleted.

- **table_name**

  Specifies the name of a table to which the rule is applied.

- **CASCADE**

  Automatically deletes the objects that depend on the rule.

- **RESTRICT**

  Refuses to delete the rule if any objects depend on it. This is the default action.

## Example

```sql
--Delete the rule named newrule.
DROP RULE newrule ON mytable;
```
