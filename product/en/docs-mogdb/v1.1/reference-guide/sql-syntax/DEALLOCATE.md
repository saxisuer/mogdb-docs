---
title: DEALLOCATE
summary: DEALLOCATE
author: Zhang Cuiping
date: 2021-05-10
---

# DEALLOCATE

## Function

**DEALLOCATE** deallocates a previously prepared statement. If you do not explicitly deallocate a prepared statement, it is deallocated when the session ends.

The **PREPARE** keyword is always ignored.

## Precautions

None

## Syntax

```sql
DEALLOCATE [ PREPARE ] { name | ALL };
```

## Parameter Description

- **name**

  Specifies the name of the prepared statement to be deallocated.

- **ALL**

  Deallocates all prepared statements.

## Examples

None
