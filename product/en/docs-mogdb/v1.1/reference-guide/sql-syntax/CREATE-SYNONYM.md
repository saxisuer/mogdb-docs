---
title: CREATE SYNONYM
summary: CREATE SYNONYM
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SYNONYM

## Function

**CREATE SYNONYM** creates a synonym object. A synonym is an alias of a database object and is used to record the mapping between database object names. You can use synonyms to access associated database objects.

## Precautions

- The user of a synonym should be its owner.
- If the schema name is specified, create a synonym in the specified schema. Otherwise create a synonym in the current schema.
- Database objects that can be accessed using synonyms include tables, views, functions, and stored procedures.
- To use synonyms, you must have the required permissions on associated objects.
- The following DML statements support synonyms: **SELECT**, **INSERT**, **UPDATE**, **DELETE**, **EXPLAIN**, and **CALL**.

## Syntax

```sql
CREATE [ OR REPLACE ] SYNONYM synonym_name
    FOR object_name;
```

## Parameter Description

- **synonym**

  Specifies the name of the synonym to be created, which can contain the schema name.

  Value range: a string. It must comply with the naming convention.

- **object_name**

  Specifies the name of an object that is associated (optionally with schema names).

  Value range: a string. It must comply with the naming convention.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > **object_name** can be the name of an object that does not exist.

## Example

```sql
-- Create schema ot.
mogdb=# CREATE SCHEMA ot;

-- Create table ot.t1 and its synonym t1.
mogdb=# CREATE TABLE ot.t1(id int, name varchar2(10));
mogdb=# CREATE OR REPLACE SYNONYM t1 FOR ot.t1;

-- Use synonym t1.
mogdb=# SELECT * FROM t1;
mogdb=# INSERT INTO t1 VALUES (1, 'ada'), (2, 'bob');
mogdb=# UPDATE t1 SET t1.name = 'cici' WHERE t1.id = 2;

-- Create synonym v1 and its associated view ot.v_t1.
mogdb=# CREATE SYNONYM v1 FOR ot.v_t1;
mogdb=# CREATE VIEW ot.v_t1 AS SELECT * FROM ot.t1;

-- Use synonym v1.
mogdb=# SELECT * FROM v1;

-- Create overloaded function ot.add and its synonym add.
mogdb=# CREATE OR REPLACE FUNCTION ot.add(a integer, b integer) RETURNS integer AS
$$
SELECT $1 + $2
$$
LANGUAGE sql;

mogdb=# CREATE OR REPLACE FUNCTION ot.add(a decimal(5,2), b decimal(5,2)) RETURNS decimal(5,2) AS
$$
SELECT $1 + $2
$$
LANGUAGE sql;

mogdb=# CREATE OR REPLACE SYNONYM add FOR ot.add;

-- Use synonym add.
mogdb=# SELECT add(1,2);
mogdb=# SELECT add(1.2,2.3);

-- Create stored procedure ot.register and its synonym register.
mogdb=# CREATE PROCEDURE ot.register(n_id integer, n_name varchar2(10))
SECURITY INVOKER
AS
BEGIN
    INSERT INTO ot.t1 VALUES(n_id, n_name);
END;
/

mogdb=# CREATE OR REPLACE SYNONYM register FOR ot.register;

-- Use synonym register to invoke the stored procedure.
mogdb=# CALL register(3,'mia');

-- Delete the synonym.
mogdb=# DROP SYNONYM t1;
mogdb=# DROP SYNONYM IF EXISTS v1;
mogdb=# DROP SYNONYM IF EXISTS add;
mogdb=# DROP SYNONYM register;
mogdb=# DROP SCHEMA ot CASCADE;
```
