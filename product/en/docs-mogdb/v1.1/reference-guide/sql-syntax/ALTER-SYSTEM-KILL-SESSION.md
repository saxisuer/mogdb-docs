---
title: ALTER SYSTEM KILL SESSION
summary: ALTER SYSTEM KILL SESSION
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SYSTEM KILL SESSION

## Function

**ALTER SYSTEM KILL SESSION** ends a session.

## Precautions

None

## Syntax

```sql
ALTER SYSTEM KILL SESSION 'session_sid, serial' [ IMMEDIATE ];
```

## Parameter Description

- **session_sid, serial**

  Specifies the SID and SERIAL of a session (To obtain the values, see the example.)

- **IMMEDIATE**

  Specifies that a session will be ended instantly after the statement is executed.

## Example

```sql
-- Query session information.
mogdb=#
SELECT sa.sessionid AS sid,0::integer AS serial#,ad.rolname AS username FROM pg_stat_get_activity(NULL) AS sa
LEFT JOIN pg_authid ad ON(sa.usesysid = ad.oid)WHERE sa.application_name <> 'JobScheduler';
       sid       | serial# | username
-----------------+---------+----------
 140131075880720 |       0 | omm
 140131025549072 |       0 | omm
 140131073779472 |       0 | omm
 140131071678224 |       0 | omm
 140131125774096 |       0 |
 140131127875344 |       0 |
 140131113629456 |       0 |
 140131094742800 |       0 |
(8 rows)

-- End the session whose SID is 140131075880720.
mogdb=#  ALTER SYSTEM KILL SESSION '140131075880720,0' IMMEDIATE;
```
