---
title: DROP FUNCTION
summary: DROP FUNCTION
author: Zhang Cuiping
date: 2021-05-10
---

# DROP FUNCTION

## Function

**DROP FUNCTION** deletes a function.

## Precautions

- If a function involves operations on temporary tables, **DROP FUNCTION** cannot be used.
- Only the function owner or a user granted with the DROP permission can run the **DROP FUNCTION** command. The system administrator has this permission by default.

## Syntax

```sql
DROP FUNCTION [ IF EXISTS ] function_name
[ ( [ {[ argmode ] [ argname ] argtype} [, ...] ] ) [ CASCADE | RESTRICT ] ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified function does not exist.

- **function_name**

  Specifies the name of the function to be deleted.

  Value range: an existing function name

- **argmode**

  Specifies the parameter mode of the function.

- **argname**

  Specifies the parameter name of the function.

- **argtype**

  Specifies the parameter type of the function.

## Examples

For details, see **Examples**.
