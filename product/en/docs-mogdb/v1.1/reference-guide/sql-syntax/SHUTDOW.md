---
title: SHUTDOW
summary: SHUTDOW
author: Zhang Cuiping
date: 2021-05-18
---

# SHUTDOW

## Function

**SHUTDOWN** is used to shut down the currently connected database node.

## Precautions

Only the user with the administrator permission can run this command.

## Syntax

```sql
SHUTDOWN
  {
          |
    fast  |
    immediate
  };
```

## Parameter Description

- **""**

  If the shutdown mode is not specified, the default mode **fast** is used.

- **fast**

  Rolls back all active transactions, forcibly disconnects the client, and shuts down the database node without waiting for the client to disconnect.

- **immediate**

  Shuts down the server forcibly. Fault recovery will occur on the next startup.

## Examples

```sql
-- Shut down the current database node.
mogdb=# SHUTDOWN;

-- Shut down the current database node in fast mode.
mogdb=# SHUTDOWN FAST;
```
