---
title: DO
summary: DO
author: Zhang Cuiping
date: 2021-05-10
---

# DO

## Function

**DO** executes an anonymous code block.

The code block is treated as though it were the body of a function with no parameters, returning **void**. It is parsed and executed a single time.

## Precautions

- he procedural language to be used must already have been installed into the current database by means of **CREATE LANGUAGE**. **plpgsql** is installed by default, but other languages are not.
- The user must have the **USAGE** permission on the procedural language, or must be a system administrator if the language is untrusted.

## Syntax

```sql
DO [ LANGUAGE lang_name ] code;
```

## Parameter Description

- **lang_name**

  Specifies the name of the procedural language the code is written in. If omitted, the default is **plpgsql**.

- **code**

  Specifies the procedural language code to be executed. This must be specified as a string literal.

## Example

```sql
-- Create the webuser user.
mogdb=# CREATE USER webuser PASSWORD 'Bigdata@123';

-- Grant all permissions on all views in the tpcds schema to the webuser user.
mogdb=# DO $$DECLARE r record;
BEGIN
    FOR r IN SELECT c.relname table_name,n.nspname table_schema FROM pg_class c,pg_namespace n
             WHERE c.relnamespace = n.oid AND n.nspname = 'tpcds' AND relkind IN ('r','v')
    LOOP
        EXECUTE 'GRANT ALL ON ' || quote_ident(r.table_schema) || '.' || quote_ident(r.table_name) || ' TO webuser';
    END LOOP;
END$$;


-- Delete the webuser user.
mogdb=# DROP USER webuser CASCADE;
```
