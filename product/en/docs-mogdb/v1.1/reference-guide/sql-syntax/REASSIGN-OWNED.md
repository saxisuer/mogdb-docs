---
title: REASSIGN OWNED
summary: REASSIGN OWNED
author: Zhang Cuiping
date: 2021-05-18
---

# REASSIGN OWNED

## Function

**REASSIGN OWNED** changes the owner of the database object.

**REASSIGN OWNED** requires that the system change owners of all the database objects owned by **old_roles** to **new_role**.

## Precautions

- **REASSIGN OWNED** is often executed before role deletion.
- To run the REASSIGN OWNED statement, you must have the permissions of the original and target roles.

## Syntax

```sql
REASSIGN OWNED BY old_role [, ...] TO new_role;
```

## Parameter Description

- **old_role**

  Specifies the role name of the old owner.

- **new_role**

  Specifies the role name of the new owner.

## Examples

None
