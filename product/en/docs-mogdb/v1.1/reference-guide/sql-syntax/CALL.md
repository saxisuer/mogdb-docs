---
title: CALL
summary: CALL
author: Zhang Cuiping
date: 2021-05-10
---

# CALL

## Function

**CALL** can be used to call defined functions and stored procedures.

## Precautions

None

## Syntax

```sql
CALL [schema.] {func_name| procedure_name} ( param_expr );
```

## Parameter Description

- **schema**

  Specifies the name of the schema where a function or stored procedure is located.

- **func_name**

  Specifies the name of the function or stored procedure to be called.

  Value range: an existing function name

- **param_expr**

  Specifies a list of parameters in the function. Use := or => to separate a parameter name and its value. This method allows parameters to be placed in any order. If only parameter values are in the list, the value order must be the same as that defined in the function or stored procedure.

  Value range: an existing function parameter name or stored procedure parameter name

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > The parameters include input parameters (whose name and type are separated by IN) and output parameters (whose name and type are separated by OUT). When you run the **CALL** statement to call a function or stored procedure, the parameter list must contain an output parameter for non-overloaded functions. You can set the output parameter to a variable or any constant. For details, see **Examples**. For an overloaded package function, the parameter list can have no output parameter, but the function may not be found. If an output parameter is contained, it must be a constant.

## Examples

```sql
-- Create a function func_add_sql, calculate the sum of two integers, and return the result.
mogdb=# CREATE FUNCTION func_add_sql(num1 integer, num2 integer) RETURN integer
AS
BEGIN
RETURN num1 + num2;
END;
/

-- Transfer by parameter value.
mogdb=# CALL func_add_sql(1, 3);

-- Transfer by naming tag method.
mogdb=# CALL func_add_sql(num1 => 1,num2 => 3);
mogdb=# CALL func_add_sql(num2 := 2, num1 := 3);

-- Delete the function.
mogdb=# DROP FUNCTION func_add_sql;

-- Create a function with output parameters.
mogdb=# CREATE FUNCTION func_increment_sql(num1 IN integer, num2 IN integer, res OUT integer)
RETURN integer
AS
BEGIN
res := num1 + num2;
END;
/

-- Transfer a constant as an output parameter.
mogdb=# CALL func_increment_sql(1,2,1);

-- Delete the function.
mogdb=# DROP FUNCTION func_increment_sql;
```
