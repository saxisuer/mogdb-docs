---
title: Other Functions
summary: Other Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Other Functions

- plan_seed()

  Description: Obtains the seed value of the previous query statement (internal use).

  Return type: int

- pg_stat_get_env()

  Description: Obtains the environment variable information of the current node. Only the sysadmin and monitor admin users can access the environment variable information.

  Return type: record

  Example:

  ```sql
  mogdb=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/MogDB_Kernel_TRUNK/install,/data1/MogDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  Description: Clears the global plan cache that is not used on nodes.

  Return type: bool

- pg_catalog.plancache_status()

  Description: Displays information about the global plan cache on nodes. The information returned by the function is the same as that in **GLOBAL_PLANCACHE_STATUS**.

  Return type: record

- textlen()

  Description: Provides the method of querying the logical length of text.

  Return type: int

- threadpool_status()

  Description: Displays the status of worker threads and sessions in the thread pool.

  Return type: record

- get_local_active_session()

  Description: Provides sampling records of the historical active session status stored in the memory of the current node.

  Return type: record

- get_global_active_session()

  Description: Provides sampling records of the historical active session status stored in the memory of all nodes.

  Return type: record

- get_global_gs_asp()

  Description: Provides sampling records of the historical active session status stored in the **gs_asp** system catalog of all nodes.

  Return type: record

- get_datanode_active_session()

  Description: Provides sampling records of historical active session status stored in the memory of other nodes in the database of the primary node.

  Return type: record

- get_datanode_active_session_hist()

  Description: Provides sampling records of historical active session status stored in the **gs_asp** system catalog of other nodes in the database of the primary node.

  Return type: record

- pg_stat_get_thread()

  Description: Provides information about the status of all threads under the current node.

  Return type: record

- pg_stat_get_sql_count()

  Description: Provides the counts of the **SELECT**, **UPDATE**, **INSERT**, **DELETE**, and **MERGE INTO** statements executed on the current node.

  Return type: record

- pg_stat_get_data_senders()

  Description: Provides detailed information about the data-copy sending thread active at the moment.

  Return type: record

- generate_wdr_report(begin_snap_id Oid, end_snap_id Oid, int report_type, int report_scope, int node_name)

  Description: Generates system diagnosis reports based on two snapshots.

  Return type: record

  **Table 1** generate_wdr_report parameter description

  | Parameter     | Description                                                  | Value Range                                                  |
  | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | begin_snap_id | Snapshot ID that starts the diagnosis report period.         | -                                                            |
  | end_snap_id   | Snapshot ID that ends the diagnosis report period. By default, the value of **end_snap_id** is greater than that of **begin_snap_id**. | -                                                            |
  | report_type   | Specifies the type of the generated report.                  | **summarydetailall**: Both **summary** and **detail** types are included. |
  | report_scope  | Specifies the scope for a report to be generated.            | **node**: a node in MogDB                                    |
  | node_name     | When **report\_scope** is set to **node**, set this parameter to the name of the corresponding node. | -                                                            |

- create_wdr_snapshot(stat text, dbid Oid)

  Description: Manually generates system diagnosis snapshots.

  Return type: record

  **Table 2** create_wdr_snapshot parameter description

  | Parameter | Description                                                  |
  | --------- | ------------------------------------------------------------ |
  | stat      | Specifies the name of the node for each snapshot. By default, the status information of all nodes is collected. |
  | dbid      | ID of the database whose status information is to be collected. |

- reset_unique_sql

  Description: Clears the Unique SQL statements in the memory of the database node. (The sysadmin permission is required.)

  Return type: bool

  **Table 3** reset_unique_sql parameter description

  | Parameter   | Type | Description                                                  |
  | ----------- | ---- | ------------------------------------------------------------ |
  | scope       | text | Clearance scope type. The options are as follows:**'GLOBAL'**: Clears all nodes. If the value is **'GLOBAL'**, this function can be executed only on the primary node.**'LOCAL'**: Clears the current node. |
  | clean_type  | text | **'BY_USERID'**: The Unique SQL statements are cleared based on user IDs.**'BY_CNID'**: The Unique SQL statements are cleared based on primary node IDs.**'ALL'**: All data is cleared. |
  | clean_value | int8 | Clearance value corresponding to the clearance type.         |

- wdr_xdb_query(db_name_str text, query text)

  Description: Provides the capability of executing cross-database queries at local. For example, when connecting to the Postgres database, access tables in the **test** database.

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  Return type: record

- pg_wlm_jump_queue(pid int)

    Description: Moves a task to the top of the primary database node queue.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- gs_wlm_switch_cgroup(pid int, cgroup text)

    Description: Moves a job to other Cgroup to improve the job priority.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

    Description: Record information about the memory context **MemoryContextName** of the thread **tid** into the *threadid***_timestamp.log** file in the *\$GAUSSLOG/pg_log/${node_name}*/dumpmem directory. *threadid* can be obtained from *sessid* in the **PV_SESSION_MEMORY_DETAIL** table. In the officially released version, only the **MemoryContextName** that is an empty string (two single quotation marks indicate that the input is an empty string) is accepted. In this case, all memory context information is recorded. Otherwise, no operation is performed. For the DEBUG version for internal development and test personnel to debug, you can specify the **MemoryContextName** to be counted. In this case, all the memory usage of the context is recorded in the specified file. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pg_shared_memctx_detail(MemoryContextName text)

    Description: Record information about the memory context **MemoryContextName** into the *threadid***_timestamp.log** file in the *$GAUSSLOG***/pg_log/***${node_name}***/dumpmem** directory. This function is provided only for internal development and test personnel to debug in the DEBUG version. Calling this function in the officially released version does not involve any operation. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- local_bgwriter_stat()

  Description: Displays the information about pages flushed by the bgwriter thread of this instance, number of pages in the candidate buffer chain, and buffer elimination information.

  Return type: record

- local_ckpt_stat()

  Description: Displays the information about checkpoints and flushing pages of the current instance.

  Return type: record

- local_double_write_stat()

  Description: Displays the doublewrite file status of the current instance.

  Return type: record

- local_single_flush_dw_stat()

  Description: Displays the elimination of dual-write files on a single page in the instance.

  Return type: record

- local_pagewriter_stat()

  Description: Displays the page flushing information and checkpoint information of the current instance.

  Return type: record

- local_redo_stat()

  Description: Displays the replay status of the current standby instance.

  Return type: record

  Note: The returned replay status includes the current replay position and the replay position of the minimum restoration point.

- local_recovery_status()

  Description: Displays log flow control information about the primary and standby instances.

  Return type: record

- local_rto_status()

  Description: Displays log flow control information about the primary and standby instances.

  Return type: record

- gs_wlm_node_recover(boolean isForce)

  Description: Obtains top SQL query statement-level statistics recorded in the current memory. If the input parameter is not 0, the information is cleared from the memory.

  Return type: record

- gs_cgroup_map_ng_conf(group name)

  Description: Reads the Cgroup configuration file of a specified logical database.

  Return type: record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  Description: Switches the control group of a specified session.

  Return type: record

- hdfs_fdw_handler()

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- hdfs_fdw_validator(text[], oid)

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- comm_client_info()

  Description: Queries information about active client connections of a single node.

  Return type: SETOF record

- pg_sync_cstore_delta(text)

  Description: Synchronizes the delta table structure of a specified column-store table with that of the column-store primary table.

  Return type: bigint

- pg_sync_cstore_delta()

  Description: Synchronizes the delta table structure of all column-store tables with that of the column-store primary table.

  Return type: bigint

- gs_create_log_tables()

  Description: Creates foreign tables and views for run logs and performance logs.

  Return type: void

  Example:

  ```sql
  mogdb=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains full SQL information at the database level.

  Return type: record

  **Table 4** dbe_perf.get_global_full_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | --------------- | ------------------------ | ---------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains slow SQL information at the database level.

  Return type: record

  **Table 5** dbe_perf.get_global_slow_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | --------------- | ------------------------ | ---------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- statement_detail_decode(detail text, format text, pretty boolean)

  Description: Parses the details column in a full or slow SQL statement.

  Return type: text

  **Table 6** statement_detail_decode parameter description

  | Parameter  | Type    | Description                                                  |
  | ---------- | ------- | ------------------------------------------------------------ |
  | **detail** | text    | A set of events generated by the SQL statement (unreadable). |
  | format     | text    | Parsing output format. The value is **plaintext**.           |
  | pretty     | boolean | Whether to display the text in pretty format when **format** is set to **plaintext**. The options are as follows:The value **true** indicates that events are separated by **\n**.The value **false** indicates that events are separated by commas (,). |

- pg_control_system()

  Description: Returns the system control file status.

  Return type: SETOF record

- pg_control_checkpoint()

  Description: Returns the system checkpoint status.

  Return type: SETOF record

- get_delta_info

  Description: Obtains the data storage information of the delta table of a column-store table.

  Parameter: rel text

  Return type: part_name text, total_live_tuple bigint, total_data_size bigint, max_blocknum bigint

- get_prepared_pending_xid

  Description: Returns next XID when the restoration is complete.

  Parameter: nan

  Return type: text

- pg_clean_region_info

  Description: Clears regionmap.

  Parameter: nan

  Return type: character varying

- pg_get_delta_info

  Description: Obtains delta information from a single DN.

  Parameter: rel text, schema_name text

  Return type: part_name text, live_tuple bigint, data_size bigint, blocknum bigint

- pg_get_replication_slot_name

  Description: Obtains the slot name.

  Parameter: nan

  Return type: text

- pg_get_running_xacts

  Description: Obtains XACT in the running status.

  Parameter: nan

  Return type: handle integer, gxid xid, state tinyint, node text, xmin xid, vacuum boolean, timeline bigint, prepare_xid xid, pid bigint, next_xid xid

- pg_get_variable_info

  Description: Obtains shared memory variable cache.

  Parameter: nan

  Return type: node_name text, nextOid oid, nextXid xid, oldestXid xid, xidVacLimit xid, oldestXidDB oid, lastExtendCSNLogpage xid, startExtendCSNLogpage xid, nextCommitSeqNo xid, latestCompletedXid xid, startupMaxXid xid

- pg_get_xidlimit

  Description: Obtains the ID of the transaction from shared memory.

  Parameter: nan

  Return type: nextXid xid, oldestXid xid, xidVacLimit xid, xidWarnLimit xid, xidStopLimit xid, xidWrapLimit xid, oldestXidDB oid

- pg_relation_compression_ratio

  Description: Queries the compression rate of a table. The default return value is 1.0.

  Parameter: text

  Return type: real

- pg_relation_with_compression

  Description: Queries whether a table is compressed.

  Parameter: text

  Return type: boolean

- pg_stat_file_recursive

  Description: Lists all files in the path.

  Parameter: location text

  Return type: path text, filename text, size bigint, isdir boolean

- pg_stat_get_activity_for_temptable

  Description: Returns records of backend processes related to the temporary table.

  Parameter: nan

  Return type: datid oid, timelineid integer, tempid integer, sessionid bigint

- pg_stat_get_activity_ng

  Description: Returns records of backend processes related to the node group.

  Parameter: pid bigint

  Return type: datid oid, pid bigint, sessionid bigint, node_group text

- pg_stat_get_cgroup_info

  Description: Returns the cgroup information.

  Parameter: nan

  Return type: cgroup_name text, percent integer, usage_percent integer, shares bigint, usage bigint, cpuset text, relpath text, valid text, node_group text

- pg_stat_get_realtime_info_internal

  Description: Returns the real-time information. The interface is invalid currently, and the return information is FailedToGetSessionInfo.

  Parameter: oid, oid, bigint, cstring, oid

  Return type: text

- pg_stat_get_session_wlmstat

  Description: Returns the load information of the current session.

  Parameter: pid integer

  Return type: datid oid, threadid bigint, sessionid bigint, threadpid integer, usesysid oid, appname text, query text, priority bigint, block_time bigint, elapsed_time bigint, total_cpu_time bigint, skew_percent integer, statement_mem integer, active_points integer, dop_value integer, current_cgroup text, current_status text, enqueue_state text, attribute text, is_plana boolean, node_group text, srespool name

- pg_stat_get_wlm_ec_operator_info

  Description: Obtains the EC execution plan operator information from the internal hash table.

  Parameter: nan

  Return type: queryid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, ec_operator integer, ec_status text, ec_execute_datanode text, ec_dsn text, ec_username text, ec_query text, ec_libodbc_type text, ec_fetch_count bigint

- pg_stat_get_wlm_instance_info

  Description: Returns the load information of the current instance.

  Parameter: nan

  Return type: instancename text, timestamp timestamp with time zone, used_cpu integer, free_memory integer, used_memory integer, io_await double precision, io_util double precision, disk_read double precision, disk_write double precision, process_read bigint, process_write bigint, logical_read bigint, logical_write bigint, read_counts bigint, write_counts bigint

- pg_stat_get_wlm_instance_info_with_cleanup

  Description: Returns the load information of the current instance and saves the information to the system table.

  Parameter: nan

  Return type: instancename text, timestamp timestamp with time zone, used_cpu integer, free_memory integer, used_memory integer, io_await double precision, io_util double precision, disk_read double precision, disk_write double precision, process_read bigint, process_write bigint, logical_read bigint, logical_write bigint, read_counts bigint, write_counts bigint

- pg_stat_get_wlm_node_resource_info

  Description: Obtains the resource information of the current node.

  Parameter: nan

  Return type: min_mem_util integer, max_mem_util integer, min_cpu_util integer, max_cpu_util integer, min_io_util integer, max_io_util integer, used_mem_rate integer

- pg_stat_get_wlm_operator_info

  Description: Obtains the execution plan operator information from the internal hash table.

  Parameter: nan

  Return type: queryid bigint, pid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, query_dop integer, estimated_rows bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, warning text

- pg_stat_get_wlm_realtime_ec_operator_info

  Description: Obtains the EC execution plan operator information from the internal hash table.

  Parameter: nan

  Return type: queryid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, ec_operator integer, ec_status text, ec_execute_datanode text, ec_dsn text, ec_username text, ec_query text, ec_libodbc_type text, ec_fetch_count bigint

- pg_stat_get_wlm_realtime_operator_info

  Description: Obtains the real-time execution plan operator information from the internal hash table.

  Parameter: nan

  Return type: queryid bigint, pid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, status text, query_dop integer, estimated_rows bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, warning text

- pg_stat_get_wlm_realtime_session_info

  Description: Returns the load information of the real-time session.

  Parameter: nan

  Return type: nodename text, threadid bigint, block_time bigint, duration bigint, estimate_total_time bigint, estimate_left_time bigint, schemaname text, query_band text, spill_info text, control_group text, estimate_memory integer, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_dn_time bigint, max_dn_time bigint, average_dn_time bigint, dntime_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, min_peak_iops integer, max_peak_iops integer, average_peak_iops integer, iops_skew_percent integer, warning text, query text, query_plan text, cpu_top1_node_name text, cpu_top2_node_name text, cpu_top3_node_name text, cpu_top4_node_name text, cpu_top5_node_name text, mem_top1_node_name text, mem_top2_node_name text, mem_top3_node_name text, mem_top4_node_name text, mem_top5_node_name text, cpu_top1_value bigint, cpu_top2_value bigint, cpu_top3_value bigint, cpu_top4_value bigint, cpu_top5_value bigint, mem_top1_value bigint, mem_top2_value bigint, mem_top3_value bigint, mem_top4_value bigint, mem_top5_value bigint, top_mem_dn text, top_cpu_dn text

- pg_stat_get_wlm_session_info_internal

  Description: Returns the load information of the session.

  Parameter: oid, bigint, bigint, oid

  Return type: SETOF text

- pg_stat_get_wlm_session_iostat_info

  Description: Returns the load IO information of the session.

  Parameter: nan

  Return type: threadid bigint, maxcurr_iops integer, mincurr_iops integer, maxpeak_iops integer, minpeak_iops integer, iops_limits integer, io_priority integer

- pg_stat_get_wlm_statistics

  Description: Returns the load statistics of the session.

  Parameter: nan

  Return type: statement text, block_time bigint, elapsed_time bigint, total_cpu_time bigint, qualification_time bigint, skew_percent integer, control_group text, status text, action text

- pg_stat_get_workload_struct_info

  Description: Returns the load management data structure.

  Parameter: nan

  Return type: text

- pg_test_err_contain_err

  Description: Tests the error type and returns the information.

  Parameter: integer

  Return type: void

- pv_session_memory_detail_tp

  Description: Returns the memory usage of the session. For details, see pv_session_memory_detail.

  Parameter: nan

  Return type: sessid text, sesstype text, contextname text, level smallint, parent text, totalsize bigint, freesize bigint, usedsize bigint
