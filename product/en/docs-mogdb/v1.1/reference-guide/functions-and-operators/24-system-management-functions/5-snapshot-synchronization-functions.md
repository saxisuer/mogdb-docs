---
title: Snapshot Synchronization Functions
summary: Snapshot Synchronization Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Snapshot Synchronization Functions

Snapshot synchronization functions save the current snapshot and return its identifier.

pg_export_snapshot()

Description: Saves the current snapshot and returns its identifier.

Return type: text

Note:**pg_export_snapshot** saves the current snapshot and returns a text string identifying the snapshot. This string must be passed to clients that want to import the snapshot. A snapshot can be imported when the **set transaction snapshot snapshot_id;** command is executed. Doing so is possible only when the transaction is set to the **SERIALIZABLE** or **REPEATABLE READ** isolation level. MogDB does not support these two isolation levels currently. The output of the function cannot be used as the input of **set transaction snapshot**.

pg_export_snapshot_and_csn()

Description: Saves the current snapshot and returns its identifier. Compared with **pg_export_snapshot()**, **pg_export_snapshot()** returns a CSN, indicating the CSN of the current snapshot.

Return type: text
