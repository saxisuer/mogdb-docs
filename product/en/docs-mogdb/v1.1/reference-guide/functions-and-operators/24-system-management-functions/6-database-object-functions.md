---
title: Database Object Functions
summary: Database Object Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Database Object Functions

## Database Object Size Functions

Database object size functions calculate the actual disk space used by database objects.

- pg_column_size(any)

  Description: Specifies the number of bytes used to store a particular value (possibly compressed)

  Return type: int

  Note:**pg_column_size** displays the space for storing an independent data value.

  ```sql
  mogdb=# SELECT pg_column_size(1);
   pg_column_size
  ----------------
                4
  (1 row)
  ```

- pg_database_size(oid)

  Description: Specifies the disk space used by the database with the specified OID.

  Return type: bigint

- pg_database_size(name)

  Description: Specifies the disk space used by the database with the specified name.

  Return type: bigint

  Note:**pg_database_size** receives the OID or name of a database and returns the disk space used by the corresponding object.

  Example:

  ```sql
  mogdb=# SELECT pg_database_size('mogdb');
   pg_database_size
  ------------------
           51590112
  (1 row)
  ```

- pg_relation_size(oid)

  Description: Specifies the disk space used by the table with a specified OID or index.

  Return type: bigint

- get_db_source_datasize()

  Description: Estimates the total size of non-compressed data in the current database.

  Return type: bigint

  Note: (1) **ANALYZE** must be performed before this function is called. (2) Calculate the total size of non-compressed data by estimating the compression rate of column-store tables.

  Example:

  ```sql
  mogdb=# analyze;
  ANALYZE
  mogdb=# select get_db_source_datasize();
   get_db_source_datasize
  ------------------------
              35384925667
  (1 row)
  ```

- pg_relation_size(text)

  Description: Specifies the disk space used by the table with a specified name or index. The table name can be schema-qualified.

  Return type: bigint

- pg_relation_size(relation regclass, fork text)

  Description: Specifies the disk space used by the specified bifurcating tree ('main', 'fsm', or 'vm') of a certain table or index.

  Return type: bigint

- pg_relation_size(relation regclass)

  Description: Is an abbreviation of **pg_relation_size(…, 'main')**.

  Return type: bigint

  Note:**pg_relation_size** receives the OID or name of a table, index, or compressed table, and returns the size.

- pg_partition_size(oid,oid)

  Description: Specifies the disk space used by the partition with a specified OID. The first **oid** is the OID of the table and the second **oid** is the OID of the partition.

  Return type: bigint

- pg_partition_size(text, text)

  Description: Specifies the disk space used by the partition with a specified name. The first **text** is the table name and the second **text** is the partition name.

  Return type: bigint

- pg_partition_indexes_size(oid,oid)

  Description: Specifies the disk space used by the index of the partition with a specified OID. The first **oid** is the OID of the table and the second **oid** is the OID of the partition.

  Return type: bigint

- pg_partition_indexes_size(text,text)

  Description: Specifies the disk space used by the index of the partition with a specified name. The first **text** is the table name and the second **text** is the partition name.

  Return type: bigint

- pg_indexes_size(regclass)

  Description: Specifies the total disk space used by the index appended to the specified table.

  Return type: bigint

- pg_size_pretty(bigint)

  Description: Converts a size in bytes expressed as a 64-bit integer into a human-readable format with size units.

  Return type: text

- pg_size_pretty(numeric)

  Description: Converts a size in bytes expressed as a numeric value into a human-readable format with size units.

  Return type: text

  Note:**pg_size_pretty** formats the results of other functions into a human-readable format. KB/MB/GB/TB can be used.

- pg_table_size(regclass)

  Description: Specifies the disk space used by the specified table, excluding indexes (but including TOAST, free space mapping, and visibility mapping).

  Return type: bigint

- pg_tablespace_size(oid)

  Description: Specifies the disk space used by the tablespace with a specified OID.

  Return type: bigint

- pg_tablespace_size(name)

  Description: Specifies the disk space used by the tablespace with a specified name.

  Return type: bigint

  Note:

  **pg_tablespace_size** receives the OID or name of a database and returns the disk space used by the corresponding object.

- pg_total_relation_size(oid)

  Description: Specifies the disk space used by the table with a specified OID, including the index and the compressed data.

  Return type: bigint

- pg_total_relation_size(regclass)

  Description: Specifies the total disk space used by the specified table, including all indexes and TOAST data.

  Return type: bigint

- pg_total_relation_size(text)

  Description: Specifies the disk space used by the table with a specified name, including the index and the compressed data. The table name can be schema-qualified.

  Return type: bigint

  Note:**pg_total_relation_size** receives the OID or name of a table or a compressed table, and returns the sizes of the data, related indexes, and the compressed table in bytes.

- datalength(any)

  Description: Specifies the number of bytes used by an expression of a specified data type (data management space, data compression, or data type conversion is not considered).

  Return type: int

  Note:**datalength** is used to calculate the space of an independent data value.

  Example:

  ```sql
  mogdb=# SELECT datalength(1);
  datalength
  ------------
  4
  (1 row)
  ```

  The following table lists the supported data types and calculation methods.

  | Data Type              | Storage Space                                                |                                                              |      |
  | ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- |
  | Numeric data types     | Integer types                                                | TINYINT                                                      | 1    |
  | SMALLINT               | 2                                                            |                                                              |      |
  | INTEGER                | 4                                                            |                                                              |      |
  | BINARY_INTEGER         | 4                                                            |                                                              |      |
  | BIGINT                 | 8                                                            |                                                              |      |
  | Any-precision types    | DECIMAL                                                      | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |      |
  | NUMERIC                | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |                                                              |      |
  | NUMBER                 | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |                                                              |      |
  | Sequence integer       | SMALLSERIAL                                                  | 2                                                            |      |
  | SERIAL                 | 4                                                            |                                                              |      |
  | BIGSERIAL              | 8                                                            |                                                              |      |
  | Floating point types   | FLOAT4                                                       | 4                                                            |      |
  | DOUBLE PRECISION       | 8                                                            |                                                              |      |
  | FLOAT8                 | 8                                                            |                                                              |      |
  | BINARY_DOUBLE          | 8                                                            |                                                              |      |
  | FLOAT[(p)]             | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |                                                              |      |
  | DEC[(p[,s])]           | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |                                                              |      |
  | INTEGER[(p[,s])]       | Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately. |                                                              |      |
  | Boolean data types     | Boolean type                                                 | BOOLEAN                                                      | 1    |
  | Character data types   | Character types                                              | CHAR                                                         | n    |
  | CHAR(n)                | n                                                            |                                                              |      |
  | CHARACTER(n)           | n                                                            |                                                              |      |
  | NCHAR(n)               | n                                                            |                                                              |      |
  | VARCHAR(n)             | n                                                            |                                                              |      |
  | CHARACTER              | Actual number of bytes of a character                        |                                                              |      |
  | VARYING(n)             | Actual number of bytes of a character                        |                                                              |      |
  | VARCHAR2(n)            | Actual number of bytes of a character                        |                                                              |      |
  | NVARCHAR2(n)           | Actual number of bytes of a character                        |                                                              |      |
  | TEXT                   | Actual number of bytes of a character                        |                                                              |      |
  | CLOB                   | Actual number of bytes of a character                        |                                                              |      |
  | Time data types        | Time types                                                   | DATE                                                         | 8    |
  | TIME                   | 8                                                            |                                                              |      |
  | TIMEZ                  | 12                                                           |                                                              |      |
  | TIMESTAMP              | 8                                                            |                                                              |      |
  | TIMESTAMPZ             | 8                                                            |                                                              |      |
  | SMALLDATETIME          | 8                                                            |                                                              |      |
  | INTERVAL DAY TO SECOND | 16                                                           |                                                              |      |
  | INTERVAL               | 16                                                           |                                                              |      |
  | RELTIME                | 4                                                            |                                                              |      |
  | ABSTIME                | 4                                                            |                                                              |      |
  | TINTERVAL              | 12                                                           |                                                              |      |

## Database Object Position Functions

- pg_relation_filenode(relation regclass)

  Description: Specifies the ID of a filenode with the specified relationship.

  Return type: oid

  Description:**pg_relation_filenode** receives the OID or name of a table, index, sequence, or compressed table, and returns the **filenode** number allocated to it. The **filenode** is the basic component of the file name used by the relationship. For most tables, the result is the same as that of **pg_class.relfilenode**. For the specified system directory, **relfilenode** is **0** and this function must be used to obtain the correct value. If a relationship that is not stored is transmitted, such as a view, this function returns **NULL**.

- pg_relation_filepath(relation regclass)

  Description: Specifies the name of a file path with the specified relationship.

  Return type: text

  Description:**pg_relation_filepath** is similar to **pg_relation_filenode**, except that **pg_relation_filepath** returns the whole file path name for the relationship (relative to the data directory **PGDATA** of MogDB).

- pg_filenode_relation(tablespace oid, filenode oid)

  Description: Obtains the table names corresponding to the tablespace and relfilenode.

  Return type: regclass

- pg_partition_filenode(partition_oid)

  Description: Obtains the filenode corresponding to the OID lock of a specified partitioned table.

  Return type: oid

- pg_partition_filepath(partition_oid)

  Description: Specifies the file path name of a partition.

  Return type: text
