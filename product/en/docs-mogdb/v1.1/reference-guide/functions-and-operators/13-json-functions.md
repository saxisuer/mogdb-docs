---
title: JSON Functions
summary: JSON Functions
author: Zhang Cuiping
date: 2021-04-20
---

# JSON Functions

JSON functions are available for creating JSON data (see JSON Types).

- array_to_json(anyarray [, pretty_bool])

  Description: Returns the array as JSON. A multi-dimensional array becomes a JSON array of arrays. Line feeds will be added between dimension-1 elements if **pretty_bool** is **true**.

  Return type: json

  For example:

  ```sql
  mogdb=# SELECT array_to_json('{{1,5},{99,100}}'::int[]);
  array_to_json
  ------------------
  [[1,5],[99,100]]
  (1 row)
  ```

- row_to_json(record [, pretty_bool])

  Description: Returns the row as JSON. Line feeds will be added between level-1 elements if **pretty_bool** is **true**.

  Return type: json

  For example:

  ```sql
  mogdb=# SELECT row_to_json(row(1,'foo'));
       row_to_json
  ---------------------
   {"f1":1,"f2":"foo"}
  (1 row)
  ```
