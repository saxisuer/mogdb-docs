---
title: AI Feature Functions
summary: AI Feature Functions
author: Zhang Cuiping
date: 2021-04-20
---

# AI Feature Functions

- gs_index_advise(text)

  Description: Recommends an index for a single query statement.

  Parameter: SQL statement string

  Return type: record

  Single-query Index Recommendation describes the examples.

- hypopg_create_index(text)

  Description: Creates a virtual index.

  Parameter: character string of the statement for creating an index

  Return type: record

  Virtual Index describes the examples.

- hypopg_display_index()

  Description: Displays information about all created virtual indexes.

  Parameter: none

  Return type: record

  Virtual Index describes the examples.

- hypopg_drop_index(oid)

  Description: Deletes a specified virtual index.

  Parameter: OID of the index

  Return type: Boolean

  Virtual Index describes the examples.

- hypopg_reset_index()

  Description: Clears all virtual indexes.

  Parameter: none

  Return type: none

  Virtual Index describes the examples.

- hypopg_estimate_size(oid)

  Description: Estimates the space required for creating a specified index.

  Parameter: OID of the index

  Return type: int8

  Virtual Index describes the examples.
