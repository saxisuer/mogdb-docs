---
title: Other System Functions
summary: Other System Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Other System Functions

MOGDB provides a number of functions and operators for built-in data types. Users can also define their own functions and operators. The PSQL commands, \df and \do can list the current functions and operators one by one. If the user is concerned about the portability of these functions and operators, it is supposed to be noticed that most of the functions and operators described in this chapter are not specified by the SQL standard, except for the arithmetic and comparison operators and the functions noted directly above.The expanded functions described in this chapter also exist in other SQL database management systems, and in most cases, different versions of these functions are implemented in a compatible and consistent manner.

The built-in functions and operators of MogDB are inherited from the open-source Postgres-XC and PostgreSQL. For details about the following functions, visit the following websites:

<http://postgres-xc.sourceforge.net/docs/1_1/functions.html>

<https://www.postgresql.org/docs/9.2/functions.html>

| abbrev        | abs           | abstime       | abstimeeq      | abstimege   | abstimegt   | abstimein      |
| ------------- | ------------- | ------------- | -------------- | ----------- | ----------- | -------------- |
| abstimele     | abstimelt     | abstimene     | abstimeout     | abstimerecv | abstimesend | aclcontains    |
| acldefault    | aclexplode    | aclinsert     | aclitemeq      | aclitemin   | aclitemout  | aclremove      |
| acos          | add_months    | age           | any_in         | any_out     | anyarray_in | anyarray_out   |
| anyarray_recv | anyarray_send | anyelement_in | anyelement_out | anyenum_in  | anyenum_out | anynonarray_in |

- abbrev
- abs
- abstime
- abstimeeq
- abstimege
- abstimegt
- abstimein
- abstimele
- abstimelt
- abstimene
- abstimeout
- abstimerecv
- abstimesend
- aclcontains
- acldefault
- aclexplode
- aclinsert
- aclitemeq
- aclitemin
- aclitemout
- aclremove
- acos
- add_months
- age
- any_in
- any_out
- anyarray_in
- anyarray_out
- anyarray_recv
- anyarray_send
- anyelement_in
- anyelement_out
- anyenum_in
- anyenum_out
- anynonarray_in
- anynonarray_out
- anyrange_in
- anyrange_out
- anytextcat
- area
- areajoinsel
- areasel
- array_agg
- array_agg_finalfn
- array_agg_transfn
- array_append
- array_cat
- array_dims
- array_eq
- array_extend
- array_fill
- array_ge
- array_gt
- array_in
- array_larger
- array_le
- array_length
- array_lower
- array_lt
- array_ndims
- array_ne
- array_out
- array_prepend
- array_recv
- array_send
- array_smaller
- array_to_json
- array_to_string
- array_typanalyze
- array_upper
- arraycontained
- arraycontains
- arraycontjoinsel
- arraycontsel
- arrayoverlap
- ascii
- asin
- atan
- atan2
- avg
- bigint_tid
- bit
- bit_and
- bit_in
- bit_length
- bit_or
- bit_out
- bit_recv
- bit_send
- bitand
- bitcat
- bitcmp
- biteq
- bitge
- bitgt
- bitle
- bitlt
- bitne
- bitnot
- bitor
- bitshiftleft
- bitshiftright
- bittypmodin
- bittypmodout
- bitxor
- bool
- bool_and
- bool_int1
- bool_int2
- bool_int8
- bool_or
- booland_statefunc
- booleq
- boolge
- boolgt
- boolin
- boolle
- boollt
- boolne
- boolor_statefunc
- boolout
- boolrecv
- boolsend
- box
- box_above
- box_above_eq
- box_add
- box_below
- box_below_eq
- box_center
- box_contain
- box_contain_pt
- box_contained
- box_distance
- box_div
- box_eq
- box_ge
- box_gt
- box_in
- box_intersect
- box_le
- box_left
- box_lt
- box_mul
- box_out
- box_overabove
- box_overbelow
- box_overlap
- box_overleft
- box_overright
- box_recv
- box_right
- box_same
- box_send
- box_sub
- bpchar
- bpchar_date
- bpchar_float4
- bpchar_float8
- bpchar_int4
- bpchar_int8
- bpchar_larger
- bpchar_numeric
- bpchar_pattern_ge
- bpchar_pattern_gt
- bpchar_pattern_le
- bpchar_pattern_lt
- bpchar_smaller
- bpchar_sortsupport
- bpchar_timestamp
- bpcharcmp
- bpchareq
- bpcharge
- bpchargt
- bpchariclike
- bpcharicnlike
- bpcharicregexeq
- bpcharicregexne
- bpcharin
- bpcharle
- bpcharlike
- bpcharlt
- bpcharne
- bpcharnlike
- bpcharout
- bpcharrecv
- bpcharregexeq
- bpcharregexne
- bpcharsend
- bpchartypmodin
- bpchartypmodout
- broadcast
- btabstimecmp
- btarraycmp
- btbeginscan
- btboolcmp
- btbpchar_pattern_cmp
- btbuild
- btbuildempty
- btbulkdelete
- btcanreturn
- btcharcmp
- btcostestimate
- btendscan
- btfloat48cmp
- btfloat4cmp
- btfloat4sortsupport
- btfloat84cmp
- btfloat8cmp
- btfloat8sortsupport
- btgetbitmap
- btgettuple
- btinsert
- btint24cmp
- btint28cmp
- btint2cmp
- btint2sortsupport
- btint42cmp
- btint48cmp
- btint4cmp
- btint4sortsupport
- btint82cmp
- btint84cmp
- btint8cmp
- btint8sortsupport
- btmarkpos
- btmerge
- btnamecmp
- btnamesortsupport
- btoidcmp
- btoidsortsupport
- btoidvectorcmp
- btoptions
- btrecordcmp
- btreltimecmp
- btrescan
- btrestrpos
- btrim
- bttext_pattern_cmp
- bttextcmp
- bttextsortsupport
- bttidcmp
- bttintervalcmp
- btvacuumcleanup
- bucketabstime
- bucketbool
- bucketbpchar
- bucketbytea
- bucketcash
- bucketchar
- bucketdate
- bucketfloat4
- bucketfloat8
- bucketint1
- bucketint2
- bucketint2vector
- bucketint4
- bucketint8
- bucketinterval
- bucketname
- bucketnumeric
- bucketnvarchar2
- bucketoid
- bucketoidvector
- bucketraw
- bucketreltime
- bucketsmalldatetime
- buckettext
- buckettime
- buckettimestamp
- buckettimestamptz
- buckettimetz
- bucketuuid
- bucketvarchar
- bytea_sortsupport
- bytea_string_agg_finalfn
- bytea_string_agg_transfn
- byteacat
- byteacmp
- byteaeq
- byteage
- byteagt
- byteain
- byteale
- bytealike
- bytealt
- byteane
- byteanlike
- byteaout
- bytearecv
- byteasend
- cash_cmp
- cash_div_cash
- cash_div_flt4
- cash_div_flt8
- cash_div_int1
- cash_div_int2
- cash_div_int4
- cash_div_int8
- cash_eq
- cash_ge
- cash_gt
- cash_in
- cash_le
- cash_lt
- cash_mi
- cash_mul_flt4
- cash_mul_flt8
- cash_mul_int1
- cash_mul_int2
- cash_mul_int4
- cash_mul_int8
- cash_ne
- cash_out
- cash_pl
- cash_recv
- cash_send
- cash_words
- cashlarger
- cashsmaller
- cbrt
- cbtreebuild
- cbtreecanreturn
- cbtreecostestimate
- cbtreegetbitmap
- cbtreegettuple
- cbtreeoptions
- ceil
- ceiling
- center
- cginbuild
- cgingetbitmap
- char
- char_length
- character_length
- chareq
- charge
- chargt
- charin
- charle
- charlt
- charne
- charout
- charrecv
- charsend
- check_engine_status
- checksum
- checksumtext_agg_transfn
- chr
- cideq
- cidin
- cidout
- cidr
- cidr_in
- cidr_out
- cidr_recv
- cidr_send
- cidrecv
- cidsend
- circle
- circle_above
- circle_add_pt
- circle_below
- circle_center
- circle_contain
- circle_contain_pt
- circle_contained
- circle_distance
- circle_div_pt
- circle_eq
- circle_ge
- circle_gt
- circle_in
- circle_le
- circle_left
- circle_lt
- circle_mul_pt
- circle_ne
- circle_out
- circle_overabove
- circle_overbelow
- circle_overlap
- circle_overleft
- circle_overright
- circle_recv
- circle_right
- circle_same
- circle_send
- circle_sub_pt
- clock_timestamp
- close_lb
- close_ls
- close_lseg
- close_pb
- close_pl
- close_ps
- close_sb
- close_sl
- col_description
- comm_client_info
- complex_array_in
- concat
- concat_ws
- contjoinsel
- contsel
- convert
- convert_from
- convert_to
- convert_to_nocase
- corr
- cos
- cot
- count
- covar_pop
- covar_samp
- create_wdr_snapshot
- cstore_tid_out
- cstring_in
- cstring_out
- cstring_recv
- cstring_send
- cume_dist
- cupointer_bigint
- current_database
- current_query
- current_schema
- current_schemas
- current_setting
- current_user
- currtid
- currtid2
- currval
- cursor_to_xml
- cursor_to_xmlschema
- database_to_xml
- database_to_xml_and_xmlschema
- database_to_xmlschema
- datalength
- date
- date_bpchar
- date_cmp
- date_cmp_timestamp
- date_cmp_timestamptz
- date_eq
- date_eq_timestamp
- date_eq_timestamptz
- date_ge
- date_ge_timestamp
- date_ge_timestamptz
- date_gt
- date_gt_timestamp
- date_gt_timestamptz
- date_in
- date_larger
- date_le
- date_le_timestamp
- date_le_timestamptz
- date_list_agg_noarg2_transfn
- date_list_agg_transfn
- date_lt
- date_lt_timestamp
- date_lt_timestamptz
- date_mi
- date_mi_interval
- date_mii
- date_ne
- date_ne_timestamp
- date_ne_timestamptz
- date_out
- date_part
- date_pl_interval
- date_pli
- date_recv
- date_send
- date_smaller
- date_sortsupport
- date_text
- date_trunc
- date_varchar
- daterange
- daterange_canonical
- daterange_subdiff
- datetime_pl
- datetimetz_pl
- dcbrt
- decode
- definer_current_user
- degrees
- dense_rank
- dexp
- diagonal
- diameter
- disable_conn
- dispell_init
- dispell_lexize
- dist_cpoly
- dist_lb
- dist_pb
- dist_pc
- dist_pl
- dist_ppath
- dist_ps
- dist_sb
- dist_sl
- div
- dlog1
- dlog10
- domain_in
- domain_recv
- dpow
- dround
- dsimple_init
- dsimple_lexize
- dsqrt
- dsynonym_init
- dsynonym_lexize
- dtrunc
- elem_contained_by_range
- empty_blob
- encode
- encode_plan_node
- enum_cmp
- enum_eq
- enum_first
- enum_ge
- enum_gt
- enum_in
- enum_larger
- enum_last
- enum_le
- enum_lt
- enum_ne
- enum_out
- enum_range
- enum_recv
- enum_send
- enum_smaller
- eqjoinsel
- eqsel
- every
- exec_hadoop_sql
- exec_on_extension
- exp
- f4toi1
- f8toi1
- factorial
- family
- fdw_handler_in
- fdw_handler_out
- first_value
- float4
- float4_accum
- float4_bpchar
- float4_list_agg_noarg2_transfn
- float4_list_agg_transfn
- float4_text
- float4_varchar
- float48div
- float48eq
- float48ge
- float48gt
- float48le
- float48lt
- float48mi
- float48mul
- float48ne
- float48pl
- float4abs
- float4div
- float4eq
- float4ge
- float4gt
- float4in
- float4larger
- float4le
- float4lt
- float4mi
- float4mul
- float4ne
- float4out
- float4pl
- float4recv
- float4send
- float4smaller
- float4um
- float4up
- float8
- float8_accum
- float8_avg
- float8_bpchar
- float8_collect
- float8_corr
- float8_covar_pop
- float8_covar_samp
- float8_interval
- float8_list_agg_noarg2_transfn
- float8_list_agg_transfn
- float8_regr_accum
- float8_regr_avgx
- float8_regr_avgy
- float8_regr_collect
- float8_regr_intercept
- float8_regr_r2
- float8_regr_slope
- float8_regr_sxx
- float8_regr_sxy
- float8_regr_syy
- float8_stddev_pop
- float8_stddev_samp
- float8_text
- float8_var_pop
- float8_var_samp
- float8_varchar
- float84div
- float84eq
- float84ge
- float84gt
- float84le
- float84lt
- float84mi
- float84mul
- float84ne
- float84pl
- float8abs
- float8div
- float8eq
- float8ge
- float8gt
- float8in
- float8larger
- float8le
- float8lt
- float8mi
- float8mul
- float8ne
- float8out
- float8pl
- float8recv
- float8send
- float8smaller
- float8um
- float8up
- floor
- flt4_mul_cash
- flt8_mul_cash
- fmgr_c_validator
- fmgr_internal_validator
- fmgr_sql_validator
- format
- format_type
- generate_series
- generate_subscripts
- generate_wdr_report
- get_hostname
- get_nodename
- get_prepared_pending_xid
- get_schema_oid
- getbucket
- getdatabaseencoding
- getpgusername
- gin_clean_pending_list
- gin_cmp_prefix
- gin_cmp_tslexeme
- gin_extract_tsquery
- gin_extract_tsvector
- gin_tsquery_consistent
- gin_tsquery_triconsistent
- ginarrayconsistent
- ginarrayextract
- ginarraytriconsistent
- ginbeginscan
- ginbuild
- ginbuildempty
- ginbulkdelete
- gincostestimate
- ginendscan
- gingetbitmap
- gininsert
- ginmarkpos
- ginmerge
- ginoptions
- ginqueryarrayextract
- ginrescan
- ginrestrpos
- ginvacuumcleanup
- gist_box_compress
- gist_box_consistent
- gist_box_decompress
- gist_box_penalty
- gist_box_picksplit
- gist_box_same
- gist_box_union
- gist_circle_compress
- gist_circle_consistent
- gist_point_compress
- gist_point_consistent
- gist_point_distance
- gist_poly_compress
- gist_poly_consistent
- gistbeginscan
- gistbuild
- gistbuildempty
- gistbulkdelete
- gistcostestimate
- gistendscan
- gistgetbitmap
- gistgettuple
- gistinsert
- gistmarkpos
- gistmerge
- gistoptions
- gistrescan
- gistrestrpos
- gistvacuumcleanup
- global_comm_get_client_info
- global_comm_get_recv_stream
- global_comm_get_send_stream
- global_comm_get_status
- gs_all_control_group_info
- gs_all_nodegroup_control_group_info
- gs_cgroup_map_ng_conf
- gs_extend_library
- gs_fault_inject
- gs_get_next_xid_csn
- gs_get_nodegroup_tablecount
- gs_password_notifytime
- gs_stat_get_wlm_plan_operator_info
- gs_switch_relfilenode
- gs_total_nodegroup_memory_detail
- gs_wlm_get_resource_pool_info
- gs_wlm_get_session_info
- gs_wlm_get_user_info
- gs_wlm_get_user_session_info
- gs_wlm_get_workload_records
- gs_wlm_node_clean
- gs_wlm_node_recover
- gs_wlm_readjust_user_space
- gs_wlm_readjust_user_space_through_username
- gs_wlm_readjust_user_space_with_reset_flag
- gs_wlm_rebuild_user_resource_pool
- gs_wlm_session_respool
- gtsquery_compress
- gtsquery_consistent
- gtsquery_decompress
- gtsquery_penalty
- gtsquery_picksplit
- gtsquery_same
- gtsquery_union
- gtsvector_compress
- gtsvector_consistent
- gtsvector_decompress
- gtsvector_penalty
- gtsvector_picksplit
- gtsvector_same
- gtsvector_union
- gtsvectorin
- gtsvectorout
- has_any_column_privilege
- has_column_privilege
- has_database_privilege
- has_directory_privilege
- has_foreign_data_wrapper_privilege
- has_function_privilege
- has_language_privilege
- has_nodegroup_privilege
- has_schema_privilege
- has_sequence_privilege
- has_server_privilege
- has_table_privilege
- has_tablespace_privilege
- has_type_privilege
- hash_aclitem
- hash_array
- hash_numeric
- hash_range
- hashbeginscan
- hashbpchar
- hashbuild
- hashbuildempty
- hashbulkdelete
- hashchar
- hashcostestimate
- hashendscan
- hashenum
- hashfloat4
- hashfloat8
- hashgetbitmap
- hashgettuple
- hashinet
- hashinsert
- hashint1
- hashint2
- hashint2vector
- hashint4
- hashint8
- hashmacaddr
- hashmarkpos
- hashmerge
- hashname
- hashoid
- hashoidvector
- hashoptions
- hashrescan
- hashrestrpos
- hashtext
- hashvacuumcleanup
- hashvarlena
- height
- hextoraw
- host
- hostmask
- i1tof4
- i1tof8
- i1toi2
- i1toi4
- i1toi8
- i2toi1
- i4toi1
- i8toi1
- iclikejoinsel
- iclikesel
- icnlikejoinsel
- icnlikesel
- icregexeqjoinsel
- icregexeqsel
- icregexnejoinsel
- icregexnesel
- inet_client_addr
- inet_client_port
- inet_in
- inet_out
- inet_recv
- inet_send
- inet_server_addr
- inet_server_port
- inetand
- inetmi
- inetmi_int8
- inetnot
- inetor
- inetpl
- initcap
- instr
- int1_avg_accum
- int1_bool
- int1_bpchar
- int1_mul_cash
- int1_numeric
- int1_nvarchar2
- int1_text
- int1_varchar
- int1abs
- int1and
- int1cmp
- int1div
- int1eq
- int1ge
- int1gt
- int1in
- int1inc
- int1larger
- int1le
- int1lt
- int1mi
- int1mod
- int1mul
- int1ne
- int1not
- int1or
- int1out
- int1pl
- int1recv
- int1send
- int1shl
- int1shr
- int1smaller
- int1um
- int1up
- int1xor
- int2
- int2_accum
- int2_avg_accum
- int2_bool
- int2_bpchar
- int2_list_agg_noarg2_transfn
- int2_list_agg_transfn
- int2_mul_cash
- int2_sum
- int2_text
- int2_varchar
- int24div
- int24eq
- int24ge
- int24gt
- int24le
- int24lt
- int24mi
- int24mul
- int24ne
- int24pl
- int28div
- int28eq
- int28ge
- int28gt
- int28le
- int28lt
- int28mi
- int28mul
- int28ne
- int28pl
- int2abs
- int2and
- int2div
- int2eq
- int2ge
- int2gt
- int2in
- int2larger
- int2le
- int2lt
- int2mi
- int2mod
- int2mul
- int2ne
- int2not
- int2or
- int2out
- int2pl
- int2recv
- int2send
- int2shl
- int2shr
- int2smaller
- int2um
- int2up
- int2vectoreq
- int2vectorin
- int2vectorout
- int2vectorrecv
- int2vectorsend
- int2xor
- int4
- int4_accum
- int4_avg_accum
- int4_bpchar
- int4_list_agg_noarg2_transfn
- int4_list_agg_transfn
- int4_mul_cash
- int4_sum
- int4_text
- int4_varchar
- int42div
- int42eq
- int42ge
- int42gt
- int42le
- int42lt
- int42mi
- int42mul
- int42ne
- int42pl
- int48div
- int48eq
- int48ge
- int48gt
- int48le
- int48lt
- int48mi
- int48mul
- int48ne
- int48pl
- int4abs
- int4and
- int4div
- int4eq
- int4ge
- int4gt
- int4in
- int4inc
- int4larger
- int4le
- int4lt
- int4mi
- int4mod
- int4mul
- int4ne
- int4not
- int4or
- int4out
- int4pl
- int4range
- int4range_canonical
- int4range_subdiff
- int4recv
- int4send
- int4shl
- int4shr
- int4smaller
- int4um
- int4up
- int4xor
- int8
- int8_accum
- int8_avg
- int8_avg_accum
- int8_avg_collect
- int8_bool
- int8_bpchar
- int8_list_agg_noarg2_transfn
- int8_list_agg_transfn
- int8_mul_cash
- int8_sum
- int8_sum_to_int8
- int8_text
- int8_varchar
- int82div
- int82eq
- int82ge
- int82gt
- int82le
- int82lt
- int82mi
- int82mul
- int82ne
- int82pl
- int84div
- int84eq
- int84ge
- int84gt
- int84le
- int84lt
- int84mi
- int84mul
- int84ne
- int84pl
- int8abs
- int8and
- int8div
- int8eq
- int8ge
- int8gt
- int8in
- int8inc
- int8inc_any
- int8inc_float8_float8
- int8larger
- int8le
- int8lt
- int8mi
- int8mod
- int8mul
- int8ne
- int8not
- int8or
- int8out
- int8pl
- int8pl_inet
- int8range
- int8range_canonical
- int8range_subdiff
- int8recv
- int8send
- int8shl
- int8shr
- int8smaller
- int8um
- int8up
- int8xor
- integer_pl_date
- inter_lb
- inter_sb
- inter_sl
- internal_in
- internal_out
- interval
- interval_accum
- interval_avg
- interval_cmp
- interval_collect
- interval_div
- interval_eq
- interval_ge
- interval_gt
- interval_hash
- interval_in
- interval_larger
- interval_le
- interval_list_agg_noarg2_transfn
- interval_list_agg_transfn
- interval_lt
- interval_mi
- interval_mul
- interval_ne
- interval_out
- interval_pl
- interval_pl_date
- interval_pl_time
- interval_pl_timestamp
- interval_pl_timestamptz
- interval_pl_timetz
- interval_recv
- interval_send
- interval_smaller
- interval_transform
- interval_um
- intervaltypmodin
- intervaltypmodout
- intinterval
- isclosed
- isempty
- isfinite
- ishorizontal
- isopen
- isparallel
- isperp
- isvertical
- json_in
- json_out
- json_recv
- json_send
- justify_days
- justify_hours
- justify_interval
- kill_snapshot
- lag
- language_handler_in
- language_handler_out
- last_day
- last_value
- lastval
- lead
- left
- length
- lengthb
- like
- like_escape
- likejoinsel
- likesel
- line
- line_distance
- line_eq
- line_horizontal
- line_in
- line_interpt
- line_intersect
- line_out
- line_parallel
- line_perp
- line_recv
- line_send
- line_vertical
- list_agg_finalfn
- list_agg_noarg2_transfn
- list_agg_transfn
- listagg
- ln
- lo_close
- lo_creat
- lo_create
- lo_export
- lo_import
- lo_lseek
- lo_open
- lo_tell
- lo_truncate
- lo_unlink
- local_ckpt_stat
- local_double_write_stat
- local_pagewriter_stat
- local_recovery_status
- local_redo_stat
- local_rto_stat
- log
- loread
- lower
- lower_inc
- lower_inf
- lowrite
- lpad
- lseg
- lseg_center
- lseg_distance
- lseg_eq
- lseg_ge
- lseg_gt
- lseg_horizontal
- lseg_in
- lseg_interpt
- lseg_intersect
- lseg_le
- lseg_length
- lseg_lt
- lseg_ne
- lseg_out
- lseg_parallel
- lseg_perp
- lseg_recv
- lseg_send
- lseg_vertical
- ltrim
- macaddr_and
- macaddr_cmp
- macaddr_eq
- macaddr_ge
- macaddr_gt
- macaddr_in
- macaddr_le
- macaddr_lt
- macaddr_ne
- macaddr_not
- macaddr_or
- macaddr_out
- macaddr_recv
- macaddr_send
- makeaclitem
- masklen
- max
- md5
- min
- mktinterval
- mod
- model_train_opt
- money
- mul_d_interval
- multiply
- name
- nameeq
- namege
- namegt
- nameiclike
- nameicnlike
- nameicregexeq
- nameicregexne
- namein
- namele
- namelike
- namelt
- namene
- namenlike
- nameout
- namerecv
- nameregexeq
- nameregexne
- namesend
- neqjoinsel
- neqsel
- netmask
- network
- network_cmp
- network_eq
- network_ge
- network_gt
- network_le
- network_lt
- network_ne
- network_sub
- network_subeq
- network_sup
- network_supeq
- next_day
- nextval
- ngram_end
- ngram_lextype
- ngram_nexttoken
- ngram_start
- nlikejoinsel
- nlikesel
- node_oid_name
- notlike
- now
- npoints
- nth_value
- ntile
- numeric
- numeric_abs
- numeric_accum
- numeric_add
- numeric_avg
- numeric_avg_accum
- numeric_avg_collect
- numeric_bpchar
- numeric_cmp
- numeric_collect
- numeric_div
- numeric_div_trunc
- numeric_eq
- numeric_exp
- numeric_fac
- numeric_ge
- numeric_gt
- numeric_in
- numeric_inc
- numeric_int1
- numeric_larger
- numeric_le
- numeric_list_agg_noarg2_transfn
- numeric_list_agg_transfn
- numeric_ln
- numeric_log
- numeric_lt
- numeric_mod
- numeric_mul
- numeric_ne
- numeric_out
- numeric_power
- numeric_recv
- numeric_send
- numeric_smaller
- numeric_sortsupport
- numeric_sqrt
- numeric_stddev_pop
- numeric_stddev_samp
- numeric_sub
- numeric_text
- numeric_transform
- numeric_uminus
- numeric_uplus
- numeric_var_pop
- numeric_var_samp
- numeric_varchar
- numerictypmodin
- numerictypmodout
- numnode
- numrange
- numrange_subdiff
- numtoday
- numtodsinterval
- nvarchar2
- nvarchar2in
- nvarchar2out
- nvarchar2recv
- nvarchar2send
- nvarchar2typmodin
- nvarchar2typmodout
- obj_description
- octet_length
- oid
- oideq
- oidge
- oidgt
- oidin
- oidlarger
- oidle
- oidlt
- oidne
- oidout
- oidrecv
- oidsend
- oidsmaller
- oidvectoreq
- oidvectorge
- oidvectorgt
- oidvectorin
- oidvectorin_extend
- oidvectorle
- oidvectorlt
- oidvectorne
- oidvectorout
- oidvectorout_extend
- oidvectorrecv
- oidvectorrecv_extend
- oidvectorsend
- oidvectorsend_extend
- oidvectortypes
- on_pb
- on_pl
- on_ppath
- on_ps
- on_sb
- on_sl
- opaque_in
- opaque_out
- ordered_set_transition
- overlaps
- overlay
- path
- path_add
- path_add_pt
- path_center
- path_contain_pt
- path_distance
- path_div_pt
- path_in
- path_inter
- path_length
- path_mul_pt
- path_n_eq
- path_n_ge
- path_n_gt
- path_n_le
- path_n_lt
- path_npoints
- path_out
- path_recv
- path_send
- path_sub_pt
- percent_rank
- percentile_cont
- percentile_cont_float8_final
- percentile_cont_interval_final
- pg_autovac_coordinator
- pg_available_extension_versions
- pg_available_extensions
- pg_buffercache_pages
- pg_cancel_invalid_query
- pg_char_to_encoding
- pg_check_xidlimit
- pg_clean_region_info
- pg_collation_for
- pg_comm_delay
- pg_comm_recv_stream
- pg_comm_send_stream
- pg_comm_status
- pg_control_group_config
- pg_create_physical_replication_slot
- pg_current_sessid
- pg_current_sessionid
- pg_current_userid
- pg_cursor
- pg_encoding_max_length
- pg_encoding_to_char
- pg_extension_config_dump
- pg_extension_update_paths
- pg_filenode_relation
- pg_get_replication_slot_name
- pg_get_replication_slots
- pg_get_xidlimit
- pg_lock_status
- pg_log_comm_status
- pg_logical_slot_get_binary_changes
- pg_logical_slot_peek_binary_changes
- pg_node_tree_in
- pg_node_tree_out
- pg_node_tree_recv
- pg_node_tree_send
- pg_notify
- pg_parse_clog
- pg_partition_filenode
- pg_pool_ping
- pg_pool_validate
- pg_prepared_statement
- pg_prepared_xact
- pg_relation_compression_ratio
- pg_relation_with_compression
- pg_resume_bkp_flag
- pg_sequence_parameters
- pg_show_all_settings
- pg_stat_file_recursive
- pg_stat_get_activity_for_temptable
- pg_stat_get_activity_ng
- pg_stat_get_bgwriter_stat_reset_time
- pg_stat_get_buf_fsync_backend
- pg_stat_get_cgroup_info
- pg_stat_get_checkpoint_sync_time
- pg_stat_get_checkpoint_write_time
- pg_stat_get_db_blk_read_time
- pg_stat_get_db_blk_write_time
- pg_stat_get_db_conflict_all
- pg_stat_get_db_conflict_bufferpin
- pg_stat_get_db_conflict_snapshot
- pg_stat_get_db_conflict_startup_deadlock
- pg_stat_get_db_conflict_tablespace
- pg_stat_get_db_stat_reset_time
- pg_stat_get_db_temp_bytes
- pg_stat_get_db_temp_files
- pg_stat_get_file_stat
- pg_stat_get_function_total_time
- pg_stat_get_mem_mbytes_reserved
- pg_stat_get_partition_tuples_hot_updated
- pg_stat_get_pooler_status
- pg_stat_get_realtime_info_internal
- pg_stat_get_redo_stat
- pg_stat_get_role_name
- pg_stat_get_session_wlmstat
- pg_stat_get_status
- pg_stat_get_stream_replications
- pg_stat_get_wal_receiver
- pg_stat_get_wal_senders
- pg_stat_get_wlm_ec_operator_info
- pg_stat_get_wlm_instance_info
- pg_stat_get_wlm_instance_info_with_cleanup
- pg_stat_get_wlm_node_resource_info
- pg_stat_get_wlm_operator_info
- pg_stat_get_wlm_realtime_ec_operator_info
- pg_stat_get_wlm_realtime_operator_info
- pg_stat_get_wlm_realtime_session_info
- pg_stat_get_wlm_session_info
- pg_stat_get_wlm_session_info_internal
- pg_stat_get_wlm_session_iostat_info
- pg_stat_get_wlm_statistics
- pg_stat_get_workload_struct_info
- pg_stat_get_xact_blocks_fetched
- pg_stat_get_xact_blocks_hit
- pg_stat_get_xact_function_calls
- pg_stat_get_xact_function_self_time
- pg_stat_get_xact_function_total_time
- pg_stat_get_xact_numscans
- pg_stat_get_xact_tuples_fetched
- pg_stat_get_xact_tuples_returned
- pg_stat_reset
- pg_sync_cstore_delta
- pg_tde_info
- pg_test_err_contain_err
- pg_timezone_abbrevs
- pg_timezone_names
- pgxc_get_csn
- pgxc_get_thread_wait_status
- pgxc_lock_for_sp_database
- pgxc_max_datanode_size
- pgxc_node_str
- pgxc_pool_connection_status
- pgxc_snapshot_status
- pgxc_stat_dirty_tables
- pgxc_unlock_for_sp_database
- pi
- plainto_tsquery
- plancache_clean
- plancache_status
- point
- point_above
- point_add
- point_below
- point_distance
- point_div
- point_eq
- point_horiz
- point_in
- point_left
- point_mul
- point_ne
- point_out
- point_recv
- point_right
- point_send
- point_sub
- point_vert
- poly_above
- poly_below
- poly_center
- poly_contain
- poly_contain_pt
- poly_contained
- poly_distance
- poly_in
- poly_left
- poly_npoints
- poly_out
- poly_overabove
- poly_overbelow
- poly_overlap
- poly_overleft
- poly_overright
- poly_recv
- poly_right
- poly_same
- poly_send
- polygon
- position
- positionjoinsel
- positionsel
- postgresql_fdw_validator
- pound_end
- pound_lextype
- pound_nexttoken
- pound_start
- pow
- power
- prepare_statement_status
- prsd_end
- prsd_headline
- prsd_lextype
- prsd_nexttoken
- prsd_start
- psortbuild
- psortcanreturn
- psortcostestimate
- psortgetbitmap
- psortgettuple
- psortoptions
- pt_contained_circle
- pt_contained_poly
- pv_builtin_functions
- pv_compute_pool_workload
- pv_os_run_info
- pv_session_memory
- pv_session_memory_detail
- pv_session_stat
- pv_session_time
- pv_thread_memory_detail
- pv_total_memory_detail
- query_to_xml
- query_to_xml_and_xmlschema
- query_to_xmlschema
- querytree
- quote_ident
- quote_literal
- quote_nullable
- radians
- radius
- random
- range_adjacent
- range_after
- range_before
- range_cmp
- range_contained_by
- range_contains
- range_contains_elem
- range_eq
- range_ge
- range_gist_compress
- range_gist_consistent
- range_gist_decompress
- range_gist_penalty
- range_gist_picksplit
- range_gist_same
- range_gist_union
- range_gt
- range_in
- range_intersect
- range_le
- range_lt
- range_minus
- range_ne
- range_out
- range_overlaps
- range_overleft
- range_overright
- range_recv
- range_send
- range_typanalyze
- range_union
- rank
- rawcat
- rawcmp
- raweq
- rawge
- rawgt
- rawin
- rawle
- rawlike
- rawlt
- rawne
- rawnlike
- rawout
- rawrecv
- rawsend
- rawtohex
- read_disable_conn_file
- record_eq
- record_ge
- record_gt
- record_in
- record_le
- record_lt
- record_ne
- record_out
- record_recv
- record_send
- regclass
- regclassin
- regclassout
- regclassrecv
- regclasssend
- regconfigin
- regconfigout
- regconfigrecv
- regconfigsend
- regdictionaryin
- regdictionaryout
- regdictionaryrecv
- regdictionarysend
- regexeqjoinsel
- regexeqsel
- regexnejoinsel
- regexnesel
- regexp_matches
- regexp_replace
- regexp_split_to_array
- regexp_split_to_table
- regoperatorin
- regoperatorout
- regoperatorrecv
- regoperatorsend
- regoperin
- regoperout
- regoperrecv
- regopersend
- regprocedurein
- regprocedureout
- regprocedurerecv
- regproceduresend
- regprocin
- regprocout
- regprocrecv
- regprocsend
- regr_avgx
- regr_avgy
- regr_count
- regr_intercept
- regr_r2
- regr_slope
- regr_sxx
- regr_sxy
- regr_syy
- regtypein
- regtypeout
- regtyperecv
- regtypesend
- reltime
- reltimeeq
- reltimege
- reltimegt
- reltimein
- reltimele
- reltimelt
- reltimene
- reltimeout
- reltimerecv
- reltimesend
- remote_ckpt_stat
- remote_double_write_stat
- remote_pagewriter_stat
- remote_recovery_status
- remote_redo_stat
- remote_rto_stat
- repeat
- replace
- report_fatal
- reset_unique_sql
- reverse
- RI_FKey_cascade_del
- RI_FKey_cascade_upd
- RI_FKey_check_ins
- RI_FKey_check_upd
- RI_FKey_noaction_del
- RI_FKey_noaction_upd
- RI_FKey_restrict_del
- RI_FKey_restrict_upd
- RI_FKey_setdefault_del
- RI_FKey_setdefault_upd
- RI_FKey_setnull_del
- RI_FKey_setnull_upd
- right
- round
- row_number
- row_to_json
- rpad
- rtrim
- scalargtjoinsel
- scalargtsel
- scalarltjoinsel
- scalarltsel
- schema_to_xml
- schema_to_xml_and_xmlschema
- schema_to_xmlschema
- session_user
- sessionid2pid
- set_bit
- set_byte
- set_config
- set_hashbucket_info
- set_masklen
- set_working_grand_version_num_manually
- setseed
- setval
- setweight
- shell_in
- shell_out
- shobj_description
- sign
- signal_backend
- similar_escape
- sin
- slope
- smalldatetime_cmp
- smalldatetime_eq
- smalldatetime_ge
- smalldatetime_gt
- smalldatetime_hash
- smalldatetime_in
- smalldatetime_larger
- smalldatetime_le
- smalldatetime_lt
- smalldatetime_ne
- smalldatetime_out
- smalldatetime_recv
- smalldatetime_send
- smalldatetime_smaller
- smgreq
- smgrin
- smgrne
- smgrout
- spg_kd_choose
- spg_kd_config
- spg_kd_inner_consistent
- spg_kd_picksplit
- spg_quad_choose
- spg_quad_config
- spg_quad_inner_consistent
- spg_quad_leaf_consistent
- spg_quad_picksplit
- spg_text_choose
- spg_text_config
- spg_text_inner_consistent
- spg_text_leaf_consistent
- spg_text_picksplit
- spgbeginscan
- spgbuild
- spgbuildempty
- spgbulkdelete
- spgcanreturn
- spgcostestimate
- spgendscan
- spggetbitmap
- spggettuple
- spginsert
- spgmarkpos
- spgmerge
- spgoptions
- spgrescan
- spgrestrpos
- spgvacuumcleanup
- split_part
- sqrt
- statement_timestamp
- stddev
- stddev_pop
- stddev_samp
- string_agg
- string_agg_finalfn
- string_agg_transfn
- string_to_array
- strip
- strpos
- substr
- substrb
- substring
- substring_inner
- sum
- suppress_redundant_updates_trigger
- sysdate
- table_data_skewness
- table_distribution
- table_to_xml
- table_to_xml_and_xmlschema
- table_to_xmlschema
- tablespace_oid_name
- tan
- text
- text_date
- text_float4
- text_float8
- text_ge
- text_gt
- text_int1
- text_int2
- text_int4
- text_int8
- text_larger
- text_le
- text_lt
- text_numeric
- text_pattern_ge
- text_pattern_gt
- text_pattern_le
- text_pattern_lt
- text_smaller
- text_timestamp
- textanycat
- textcat
- texteq
- texticlike
- texticnlike
- texticregexeq
- texticregexne
- textin
- textlen
- textlike
- textne
- textnlike
- textout
- textrecv
- textregexeq
- textregexne
- textsend
- thesaurus_init
- thesaurus_lexize
- threadpool_status
- tideq
- tidge
- tidgt
- tidin
- tidlarger
- tidle
- tidlt
- tidne
- tidout
- tidrecv
- tidsend
- tidsmaller
- time
- time_cmp
- time_eq
- time_ge
- time_gt
- time_hash
- time_in
- time_larger
- time_le
- time_lt
- time_mi_interval
- time_mi_time
- time_ne
- time_out
- time_pl_interval
- time_recv
- time_send
- time_smaller
- time_transform
- timedate_pl
- timemi
- timenow
- timeofday
- timepl
- timestamp
- timestamp_cmp
- timestamp_cmp_date
- timestamp_cmp_timestamptz
- timestamp_diff
- timestamp_eq
- timestamp_eq_date
- timestamp_eq_timestamptz
- timestamp_ge
- timestamp_ge_date
- timestamp_ge_timestamptz
- timestamp_gt
- timestamp_gt_date
- timestamp_gt_timestamptz
- timestamp_hash
- timestamp_in
- timestamp_larger
- timestamp_le
- timestamp_le_date
- timestamp_le_timestamptz
- timestamp_list_agg_noarg2_transfn
- timestamp_list_agg_transfn
- timestamp_lt
- timestamp_lt_date
- timestamp_lt_timestamptz
- timestamp_mi
- timestamp_mi_interval
- timestamp_ne
- timestamp_ne_date
- timestamp_ne_timestamptz
- timestamp_out
- timestamp_pl_interval
- timestamp_recv
- timestamp_send
- timestamp_smaller
- timestamp_sortsupport
- timestamp_text
- timestamp_transform
- timestamp_varchar
- timestamptypmodin
- timestamptypmodout
- timestamptz
- timestamptz_cmp
- timestamptz_cmp_date
- timestamptz_cmp_timestamp
- timestamptz_eq
- timestamptz_eq_date
- timestamptz_eq_timestamp
- timestamptz_ge
- timestamptz_ge_date
- timestamptz_ge_timestamp
- timestamptz_gt
- timestamptz_gt_date
- timestamptz_gt_timestamp
- timestamptz_in
- timestamptz_larger
- timestamptz_le
- timestamptz_le_date
- timestamptz_le_timestamp
- timestamptz_list_agg_noarg2_transfn
- timestamptz_list_agg_transfn
- timestamptz_lt
- timestamptz_lt_date
- timestamptz_lt_timestamp
- timestamptz_mi
- timestamptz_mi_interval
- timestamptz_ne
- timestamptz_ne_date
- timestamptz_ne_timestamp
- timestamptz_out
- timestamptz_pl_interval
- timestamptz_recv
- timestamptz_send
- timestamptz_smaller
- timestamptztypmodin
- timestamptztypmodout
- timestampzone_text
- timetypmodin
- timetypmodout
- timetz
- timetz_cmp
- timetz_eq
- timetz_ge
- timetz_gt
- timetz_hash
- timetz_in
- timetz_larger
- timetz_le
- timetz_lt
- timetz_mi_interval
- timetz_ne
- timetz_out
- timetz_pl_interval
- timetz_recv
- timetz_send
- timetz_smaller
- timetzdate_pl
- timetztypmodin
- timetztypmodout
- timezone
- tinterval
- tintervalct
- tintervalend
- tintervaleq
- tintervalge
- tintervalgt
- tintervalin
- tintervalle
- tintervalleneq
- tintervallenge
- tintervallengt
- tintervallenle
- tintervallenlt
- tintervallenne
- tintervallt
- tintervalne
- tintervalout
- tintervalov
- tintervalrecv
- tintervalrel
- tintervalsame
- tintervalsend
- tintervalstart
- to_ascii
- to_char
- to_date
- to_hex
- to_number
- to_timestamp
- to_tsquery
- to_tsvector
- to_tsvector_for_batch
- total_cpu
- total_memory
- track_model_train_opt
- transaction_timestamp
- translate
- trigger_in
- trigger_out
- trunc
- ts_headline
- ts_lexize
- ts_match_qv
- ts_match_tq
- ts_match_tt
- ts_match_vq
- ts_parse
- ts_rank
- ts_rank_cd
- ts_rewrite
- ts_stat
- ts_token_type
- ts_typanalyze
- tsmatchjoinsel
- tsmatchsel
- tsq_mcontained
- tsq_mcontains
- tsquery_and
- tsquery_cmp
- tsquery_eq
- tsquery_ge
- tsquery_gt
- tsquery_le
- tsquery_lt
- tsquery_ne
- tsquery_not
- tsquery_or
- tsqueryin
- tsqueryout
- tsqueryrecv
- tsquerysend
- tsrange
- tsrange_subdiff
- tstzrange
- tstzrange_subdiff
- tsvector_cmp
- tsvector_concat
- tsvector_eq
- tsvector_ge
- tsvector_gt
- tsvector_le
- tsvector_lt
- tsvector_ne
- tsvector_update_trigger
- tsvector_update_trigger_column
- tsvectorin
- tsvectorout
- tsvectorrecv
- tsvectorsend
- txid_current
- txid_current_snapshot
- txid_snapshot_in
- txid_snapshot_out
- txid_snapshot_recv
- txid_snapshot_send
- txid_snapshot_xip
- txid_snapshot_xmax
- txid_snapshot_xmin
- txid_visible_in_snapshot
- unique_key_recheck
- unknownin
- unknownout
- unknownrecv
- unknownsend
- unnest
- update_pgjob
- upper
- upper_inc
- upper_inf
- uuid_cmp
- uuid_eq
- uuid_ge
- uuid_gt
- uuid_hash
- uuid_in
- uuid_le
- uuid_lt
- uuid_ne
- uuid_out
- uuid_recv
- uuid_send
- var_pop
- var_samp
- varbit
- varbit_in
- varbit_out
- varbit_recv
- varbit_send
- varbit_transform
- varbitcmp
- varbiteq
- varbitge
- varbitgt
- varbitle
- varbitlt
- varbitne
- varbittypmodin
- varbittypmodout
- varchar
- varchar_date
- varchar_float4
- varchar_float8
- varchar_int4
- varchar_int8
- varchar_numeric
- varchar_timestamp
- varchar_transform
- varcharin
- varcharout
- varcharrecv
- varcharsend
- varchartypmodin
- varchartypmodout
- variance
- version
- void_in
- void_out
- void_recv
- void_send
- wdr_xdb_query
- width
- width_bucket
- xideq
- xideq4
- xideqint4
- xideqint8
- xidin
- xidin4
- xidlt
- xidlt4
- xidout
- xidout4
- xidrecv
- xidrecv4
- xidsend
- xidsend4
- xml
- xml_in
- xml_is_well_formed
- xml_is_well_formed_content
- xml_is_well_formed_document
- xml_out
- xml_recv
- xml_send
- xmlagg
- xmlcomment
- xmlconcat2
- xmlexists
- xmlvalidate
- xpath
- xpath_exists
- zhprs_end
- zhprs_getlexeme
- zhprs_lextype
- zhprs_start
