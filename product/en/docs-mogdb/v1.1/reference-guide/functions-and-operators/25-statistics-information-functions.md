---
title: Statistics Information Functions
summary: Statistics Information Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Statistics Information Functions

Statistics information functions are divided into the following two categories: functions that access databases, using the OID of each table or index in a database to mark the database for which statistics are generated; functions that access servers, identified by the server process ID, whose value ranges from 1 to the number of currently active servers.

- pg_stat_get_db_conflict_tablespace(oid)

  Description: Number of queries canceled due to a conflict between the restored tablespace and the deleted tablespace in the database.

  Return type: bigint

- pg_control_group_config

  Description: Prints cgroup configurations on the current node.

  Return type: record

- pg_stat_get_db_stat_reset_time(oid)

  Description: Last time when database statistics were reset. It is initialized to the system time during the first connection to each database. The reset time is updated when you call **pg_stat_reset** on the database and execute **pg_stat_reset_single_table_counters** against any table or index in it.

  Return type: timestamptz

- pg_stat_get_function_total_time(oid)

  Description: Total wall clock time spent in the function, in microseconds. The time spent on this function calling other functions is included.

  Return type: bigint

- pg_stat_get_xact_tuples_returned(oid)

  Description: Number of rows read through sequential scans when the parameter is a table in the current transaction, or number of index entries returned when the parameter is an index.

  Return type: bigint

- pg_stat_get_xact_numscans(oid)

  Description: Number of sequential scans performed when the parameter is a table in the current transaction, or number of index scans performed when the parameter is an index.

  Return type: bigint

- pg_stat_get_xact_blocks_fetched(oid)

  Description: Number of disk block fetch requests for a table or index in the current transaction.

  Return type: bigint

- pg_stat_get_xact_blocks_hit(oid)

  Description: Number of disk block fetch requests for tables or indexes found in cache in the current transaction.

  Return type: bigint

- pg_stat_get_xact_function_calls(oid)

  Description: Number of times the function is called in the current transaction.

  Return type: bigint

- pg_stat_get_xact_function_self_time(oid)

  Description: Time spent on this function in the current transaction, excluding the time spent on this function internally calling other functions.

  Return type: bigint

- pg_stat_get_xact_function_total_time(oid)

  Description: Total wall clock time (in microseconds) spent on the function in the current transaction, including the time spent on this function internally calling other functions.

  Return type: bigint

- pg_stat_get_wal_senders()

  Description: Queries walsender information on the primary server.

  Return type: setofrecord

  The following table describes return fields.

  **Table 1** Return field description

  | Field                      | Type                     | Description                              |
  | -------------------------- | ------------------------ | ---------------------------------------- |
  | pid                        | bigint                   | Thread ID of the WAL sender              |
  | sender_pid                 | integer                  | Lightweight thread ID of the WAL sender. |
  | local_role                 | text                     | Type of the primary node                 |
  | peer_role                  | text                     | Type of the standby node                 |
  | peer_state                 | text                     | Status of the standby node               |
  | state                      | text                     | Status of the WAL sender                 |
  | catchup_start              | timestamp with time zone | Startup time of a catchup task           |
  | catchup_end                | timestamp with time zone | End time of a catchup task               |
  | sender_sent_location       | text                     | Sending position of the primary node     |
  | sender_write_location      | text                     | Writing position of the primary node     |
  | sender_flush_location      | text                     | Flushing position of the primary node    |
  | sender_replay_location     | text                     | Redo position of the primary node        |
  | receiver_received_location | text                     | Receiving position of the standby node   |
  | receiver_write_location    | text                     | Writing position of the standby node     |
  | receiver_flush_location    | text                     | Flushing position of the standby node    |
  | receiver_replay_location   | text                     | Redo position of the standby node        |
  | sync_percent               | text                     | Synchronization percentage               |
  | sync_state                 | text                     | Synchronization status                   |
  | sync_priority              | text                     | Priority of synchronous replication      |
  | sync_most_available        | text                     | Maximum availability mode                |
  | channel                    | text                     | Channel information of the WAL sender    |

- pg_stat_get_stream_replications()

  Description: Queries the primary/standby replication status.

  Return type: setofrecord

  The following table describes return values.

  **Table 2** Return value description

  | Return Parameter   | Type    | Description           |
  | ------------------ | ------- | --------------------- |
  | local_role         | text    | Local role            |
  | static_connections | integer | Connection statistics |
  | db_state           | text    | Database status       |
  | detail_information | text    | Detailed information  |

- pg_stat_get_db_numbackends(oid)

  Description: Number of active server processes for a database

  Return type: integer

- pg_stat_get_db_xact_commit(oid)

  Description: Number of transactions committed in a database

  Return type: bigint

- pg_stat_get_db_xact_rollback(oid)

  Description: Number of transactions rolled back in a database

  Return type: bigint

- pg_stat_get_db_blocks_fetched(oid)

  Description: Number of disk blocks fetch requests for a database

  Return type: bigint

- pg_stat_get_db_blocks_hit(oid)

  Description: Number of disk block fetch requests found in cache for a database

  Return type: bigint

- pg_stat_get_db_tuples_returned(oid)

  Description: Number of tuples returned for a database

  Return type: bigint

- pg_stat_get_db_tuples_fetched(oid)

  Description: Number of tuples fetched for a database

  Return type: bigint

- pg_stat_get_db_tuples_inserted(oid)

  Description: Number of tuples inserted in a database

  Return type: bigint

- pg_stat_get_db_tuples_updated(oid)

  Description: Number of tuples updated in a database

  Return type: bigint

- pg_stat_get_db_tuples_deleted(oid)

  Description: Number of tuples deleted in a database

  Return type: bigint

- pg_stat_get_db_conflict_lock(oid)

  Description: Number of lock conflicts in a database

  Return type: bigint

- pg_stat_get_db_deadlocks(oid)

  Description: Number of deadlocks in a database

  Return type: bigint

- pg_stat_get_numscans(oid)

  Description: Number of sequential row scans done if parameters are in a table or number of index scans done if parameters are in an index

  Return type: bigint

- pg_stat_get_role_name(oid)

  Description: Obtains the username based on the user OID. Only the sysadmin and monitor admin users can access.

  Return type: text

  Example:

  ```sql
  mogdb=# select pg_stat_get_role_name(10);
   pg_stat_get_role_name
  -----------------------
   aabbcc
  (1 row)
  ```

- pg_stat_get_tuples_returned(oid)

  Description: Number of sequential row scans done if parameters are in a table or number of index entries returned if parameters are in an index

  Return type: bigint

- pg_stat_get_tuples_fetched(oid)

  Description: Number of table rows fetched by bitmap scans if parameters are in a table, or table rows fetched by simple index scans using the index if parameters are in an index

  Return type: bigint

- pg_stat_get_tuples_inserted(oid)

  Description: Number of rows inserted into table

  Return type: bigint

- pg_stat_get_tuples_updated(oid)

  Description: Number of rows updated in table

  Return type: bigint

- pg_stat_get_tuples_deleted(oid)

  Description: Number of rows deleted from table

  Return type: bigint

- pg_stat_get_tuples_changed(oid)

  Description: Total number of inserted, updated, and deleted rows after the table was last analyzed or autoanalyzed

  Return type: bigint

- pg_stat_get_tuples_hot_updated(oid)

  Description: Number of rows hot updated in table

  Return type: bigint

- pg_stat_get_live_tuples(oid)

  Description: Number of live rows in table

  Return type: bigint

- pg_stat_get_dead_tuples(oid)

  Description: Number of dead rows in table

  Return type: bigint

- pg_stat_get_blocks_fetched(oid)

  Description: Number of disk block fetch requests for table or index

  Return type: bigint

- pg_stat_get_blocks_hit(oid)

  Description: Number of disk block requests found in cache for table or index

  Return type: bigint

- pg_stat_get_partition_tuples_inserted(oid)

  Description: Number of rows in the corresponding table partition

  Return type: bigint

- pg_stat_get_partition_tuples_updated(oid)

  Description: Number of rows that have been updated in the corresponding table partition

  Return type: bigint

- pg_stat_get_partition_tuples_deleted(oid)

  Description: Number of rows deleted from the corresponding table partition

  Return type: bigint

- pg_stat_get_partition_tuples_changed(oid)

  Description: Total number of inserted, updated, and deleted rows after the table partition was last analyzed or autoanalyzed

  Return type: bigint

- pg_stat_get_partition_live_tuples(oid)

  Description: Number of live rows in partitioned table

  Return type: bigint

- pg_stat_get_partition_dead_tuples(oid)

  Description: Number of dead rows in partitioned table

  Return type: bigint

- pg_stat_get_xact_tuples_inserted(oid)

  Description: Number of tuple inserted into the active subtransactions related to the table.

  Return type: bigint

- pg_stat_get_xact_tuples_deleted(oid)

  Description: Number of deleted tuples in the active subtransactions related to a table

  Return type: bigint

- pg_stat_get_xact_tuples_hot_updated(oid)

  Description: Number of hot updated tuples in the active subtransactions related to a table

  Return type: bigint

- pg_stat_get_xact_tuples_updated(oid)

  Description: Number of updated tuples in the active subtransactions related to a table

  Return type: bigint

- pg_stat_get_xact_partition_tuples_inserted(oid)

  Description: Number of inserted tuples in the active subtransactions related to a table partition

  Return type: bigint

- pg_stat_get_xact_partition_tuples_deleted(oid)

  Description: Number of deleted tuples in the active subtransactions related to a table partition

  Return type: bigint

- pg_stat_get_xact_partition_tuples_hot_updated(oid)

  Description: Number of hot updated tuples in the active subtransactions related to a table partition

  Return type: bigint

- pg_stat_get_xact_partition_tuples_updated(oid)

  Description: Number of updated tuples in the active subtransactions related to a table partition

  Return type: bigint

- pg_stat_get_last_vacuum_time(oid)

  Description: Last time when the autovacuum thread is manually started to clear a table

  Return type: timestamptz

- pg_stat_get_last_autovacuum_time(oid)

  Description: Time of the last vacuum initiated by the autovacuum daemon on this table

  Return type: timestamptz

- pg_stat_get_vacuum_count(oid)

  Description: Number of times a table is manually cleared

  Return type: bigint

- pg_stat_get_autovacuum_count(oid)

  Description: Number of times the autovacuum daemon is started to clear a table

  Return type: bigint

- pg_stat_get_last_analyze_time(oid)

  Description: Last time when a table starts to be analyzed manually or by the autovacuum thread

  Return type: timestamptz

- pg_stat_get_last_autoanalyze_time(oid)

  Description: Time of the last analysis initiated by the autovacuum daemon on this table

  Return type: timestamptz

- pg_stat_get_analyze_count(oid)

  Description: Number of times a table is manually analyzed

  Return type: bigint

- pg_stat_get_autoanalyze_count(oid)

  Description: Number of times the autovacuum daemon analyzes a table

  Return type: bigint

- pg_total_autovac_tuples(bool,bool)

  Description: Returns tuple records related to the total autovac, such as **nodename**, **nspname**, **relname**, and tuple IUDs. The input parameters specify whether to query **relation** and **local** information, respectively.

  Return type: setofrecord

  The following table describes return parameters.

  **Table 3** Return parameter description

  | Return Parameter      | Type   | Description                                        |
  | --------------------- | ------ | -------------------------------------------------- |
  | nodename              | name   | Node name                                          |
  | nspname               | name   | Name of a namespace                                |
  | relname               | name   | Name of an object, such as a table, index, or view |
  | partname              | name   | Partition name                                     |
  | n_dead_tuples         | bigint | Number of dead rows in a table partition           |
  | n_live_tuples         | bigint | Number of live rows in a table partition           |
  | changes_since_analyze | bigint | Number of changes generated by ANALYZE             |

- pg_autovac_status(oid)

  Description: Returns autovac information, such as **nodename**, **nspname**, **relname**, **analyze**, **vacuum**, thresholds of **analyze** and **vacuum**, and the number of analyzed or vacuumed tuples. Only the sysadmin user can use this function.

  Return type: setofrecord

  The following table describes return parameters.

  **Table 4** Return parameter description

  | Return Parameter | Type    | Description                                        |
  | ---------------- | ------- | -------------------------------------------------- |
  | nspname          | text    | Name of a namespace                                |
  | relname          | text    | Name of an object, such as a table, index, or view |
  | nodename         | text    | Node name                                          |
  | doanalyze        | Boolean | Whether to execute **ANALYZE**                     |
  | anltuples        | bigint  | Number of ANALYZE tuples                           |
  | anlthresh        | bigint  | ANALYZE threshold                                  |
  | dovacuum         | Boolean | Whether to execute **VACUUM**                      |
  | vactuples        | bigint  | Number of VACUUM tuples                            |
  | vacthresh        | bigint  | VACUUM threshold                                   |

- pg_autovac_timeout(oid)

  Description: Returns the number of consecutive timeouts during the autovac operation on a table. If the table information is invalid or the node information is abnormal, **NULL** will be returned.

  Return type: bigint

- pg_autovac_dbnode(oid)

  Description: Returns the name of the dbnode performing the autovac operation on a table. If the table information is invalid or the node information is abnormal, **NULL** will be returned.

  Return type: text

- pg_stat_get_last_data_changed_time(oid)

  Description: Returns the time when **INSERT**, **UPDATE**, **DELETE**, or **EXCHANGE**/**TRUNCATE**/**DROP** **PARTITION** was performed last time on a table. The data in the **last_data_changed** column of the **PG_STAT_ALL_TABLES** view is calculated by using this function. The performance of obtaining the last modification time by using the view is poor when the table has a large amount of data. In this case, you are advised to use the function.

  Return type: timestamptz

- pg_stat_set_last_data_changed_time(oid)

  Description: Manually changes the time when **INSERT**, **UPDATE**, **DELETE**, or **EXCHANGE**/**TRUNCATE**/**DROP** **PARTITION** was performed last time.

  Return type: void

- pg_backend_pid()

  Description: Thread ID of the server thread attached to the current session

  Return type: integer

- pg_stat_get_activity(integer)

  Description: Returns a record about the backend with the specified PID. A record for each active backend in the system is returned if **NULL** is specified. The return results are a subnet of those (excluding the **connection_info** column) in the **PG_STAT_ACTIVITY** view.

  Example:

  ```sql
  mogdb=# select * from pg_stat_get_activity(140036483839744);
   datid |       pid       |    sessionid    | usesysid | application_name | state  |                        query                         | waiting |          xact_start
         |          query_start          |         backend_start         |         state_change          | client_addr | client_hostname | client_port | enqueue |     query_i
  d
  -------+-----------------+-----------------+----------+------------------+--------+------------------------------------------------------+---------+------------------------
  -------+-------------------------------+-------------------------------+-------------------------------+-------------+-----------------+-------------+---------+------------
  -------
   15914 | 140036483839744 | 140036483839744 |       10 | gsql             | active | select * from pg_stat_get_activity(140036483839744); | f       | 2020-06-24 10:53:19.583
  666+08 | 2020-06-24 10:53:19.583598+08 | 2020-06-24 10:00:03.471893+08 | 2020-06-24 10:53:19.583601+08 |             |                 |          -1 |         | 72902018968
  108794
  (1 row)
  ```

  Return type: setofrecord

  The following table describes return parameters.

  **Table 5** Return parameter description

  | Return Parameter | Type                     | Description                                                  |
  | ---------------- | ------------------------ | ------------------------------------------------------------ |
  | datid            | oid                      | OID of the database that the user session connects to in the backend |
  | pid              | bigint                   | Thread ID of the backend                                     |
  | sessionid        | bigint                   | Session ID                                                   |
  | usesysid         | oid                      | Name of the user logged in to the backend                    |
  | application_name | text                     | Name of the application connected to the backend             |
  | state            | text                     | Overall status of this backend.                              |
  | query            | text                     | Text of this backend's most recent query. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
  | waiting          | Boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true**. |
  | xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active).If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
  | query_start      | timestamp with time zone | Time when the currently active query was started, or time when the last query was started if **state** is not **active** |
  | backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
  | state_change     | timestamp with time zone | Time when the **state** was last changed                     |
  | client_addr      | inet                     | IP address of the client connected to the backend. If this column is **NULL**, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as **AUTOVACUUM**. |
  | client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of **client\_addr**. This column will be non-null only for IP connections and only when **log_hostname** is enabled. |
  | client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
  | enqueue          | text                     | Unsupported currently                                        |
  | query_id         | bigint                   | ID of a query                                                |

- pg_stat_get_activity_with_conninfo(integer)

  Description: Returns a record about the backend with the specified PID. A record for each active backend in the system is returned if **NULL** is specified. The return results are a subnet of those in the **PG_STAT_ACTIVITY** view.

  Return type: setofrecord

  The following table describes return values.

  **Table 6** Return value description

  | Return Value     | Return Type              | Description                                                  |
  | ---------------- | ------------------------ | ------------------------------------------------------------ |
  | datid            | oid                      | OID of the database that the user session connects to in the backend |
  | pid              | bigint                   | Thread ID of the backend                                     |
  | sessionid        | bigint                   | Session ID                                                   |
  | usesysid         | oid                      | Name of the user logged in to the backend                    |
  | application_name | text                     | Name of the application connected to the backend             |
  | state            | text                     | Overall status of this backend                               |
  | query            | text                     | Text of this backend's most recent query. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
  | waiting          | Boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true** |
  | xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active). If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
  | query_start      | timestamp with time zone | Time when the currently active query was started, or time when the last query was started if **state** is not **active** |
  | backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
  | state_change     | timestamp with time zone | Time when the **state** was last changed                     |
  | client_addr      | inet                     | IP address of the client connected to the backend. If this column is **NULL**, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as **AUTOVACUUM**. |
  | client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of **client\_addr**. This column will be non-null only for IP connections and only when **log_hostname** is enabled. |
  | client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
  | enqueue          | text                     | Unsupported currently                                        |
  | query_id         | bigint                   | ID of a query                                                |
  | connection_info  | text                     | A string in JSON format recording the driver type, driver version, driver deployment path, and process owner of the connected database |

- pg_user_iostat(text)

  Description: Displays the I/O load management information about the job currently executed by the user.

  Return type: record

  The following table describes return fields.

  | Name          | Type | Description                                                  |
  | ------------- | ---- | ------------------------------------------------------------ |
  | userid        | oid  | User ID.                                                     |
  | min_curr_iops | int4 | Minimum I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | max_curr_iops | int4 | Maximum I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by thousands for row storage. |
  | min_peak_iops | int4 | Minimum peak I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by thousands for row storage. |
  | max_peak_iops | int4 | Maximum peak I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by thousands for row storage. |
  | io_limits     | int4 | **io_limits** set for the resource pool specified by the user. The IOPS is counted by ones for column storage and by thousands for row storage. |
  | io_priority   | text | **io_priority** set for the user. The IOPS is counted by ones for column storage and by thousands for row storage. |

- pg_stat_get_function_calls(oid)

  Description: Number of times the function has been called

  Return type: bigint

- pg_stat_get_function_self_time(oid)

  Description: Time spent in only this function. The time spent on this function calling other functions is excluded.

  Return type: bigint

- pg_stat_get_backend_idset()

  Description: Set of currently active server process numbers (from 1 to the number of active server processes)

  Return type: setofinteger

- pg_stat_get_backend_pid(integer)

  Description: Thread ID of the given server thread

  Return type: bigint

- pg_stat_get_backend_dbid(integer)

  Description: ID of the database connected to the given server process

  Return type: oid

- pg_stat_get_backend_userid(integer)

  Description: User ID of the given server process

  Return type: oid

- pg_stat_get_backend_activity(integer)

  Description: Active command of the given server process, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: text

- pg_stat_get_backend_waiting(integer)

  Description: True if the given server process is waiting for a lock, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: Boolean

- pg_stat_get_backend_activity_start(integer)

  Description: The time at which the given server process's currently executing query was started, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: timestamp with time zone

- pg_stat_get_backend_xact_start(integer)

  Description: The time at which the given server process's currently executing transaction was started, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: timestamp with time zone

- pg_stat_get_backend_start(integer)

  Description: The time at which the given server process was started, or **NULL** if the current user is neither a system administrator nor the same user as that of the session being queried

  Return type: timestamp with time zone

- pg_stat_get_backend_client_addr(integer)

  Description: IP address of the client connected to the given server process. If the connection is over a Unix domain socket, or if the current user is neither a system administrator nor the same user as that of the session being queried, **NULL** will be returned.

  Return type: inet

- pg_stat_get_backend_client_port(integer)

  Description: TCP port number of the client connected to the given server process If the connection is over a Unix domain socket, **-1** will be returned. If the current user is neither a system administrator nor the same user as that of the session being queried, **NULL** will be returned.

  Return type: integer

- pg_stat_get_bgwriter_timed_checkpoints()

  Description: The number of times the background writer has started timed checkpoints (because the **checkpoint_timeout** time has expired)

  Return type: bigint

- pg_stat_get_bgwriter_requested_checkpoints()

  Description: The number of times the background writer has started checkpoints based on requests from the backend because **checkpoint_segments** has been exceeded or the **CHECKPOINT** command has been executed

  Return type: bigint

- pg_stat_get_bgwriter_buf_written_checkpoints()

  Description: The number of buffers written by the background writer during checkpoints

  Return type: bigint

- pg_stat_get_bgwriter_buf_written_clean()

  Description: The number of buffers written by the background writer for routine cleaning of dirty pages

  Return type: bigint

- pg_stat_get_bgwriter_maxwritten_clean()

  Description: The number of times the background writer has stopped its cleaning scan because it has written more buffers than specified in the **bgwriter_lru_maxpages** parameter

  Return type: bigint

- pg_stat_get_buf_written_backend()

  Description: The number of buffers written by the backend because they needed to allocate a new buffer

  Return type: bigint

- pg_stat_get_buf_alloc()

  Description: The total number of buffer allocations

  Return type: bigint

- pg_stat_clear_snapshot()

  Description: Discards the current statistics snapshot.

  Return type: void

- pg_stat_reset()

  Description: Resets all statistics counters for the current database to zero (requires system administrator permissions).

  Return type: void

- gs_stat_reset()

  Description: Resets all statistics counters for the current database on each node to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_shared(text)

  Description: Resets all statistics counters for the current database in each node in a shared cluster to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_single_table_counters(oid)

  Description: Resets statistics for a single table or index in the current database to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_single_function_counters(oid)

  Description: Resets statistics for a single function in the current database to zero (requires system administrator permissions).

  Return type: void

- pg_stat_session_cu(int, int, int)

  Description: Obtains the compression unit (CU) hit statistics of sessions running on the current node.

  Return type: record

- gs_get_stat_session_cu(text, int, int, int)

  Description: Obtains the CU hit statistics of all running sessions in MogDB.

  Return type: record

- gs_get_stat_db_cu(text, text, int, int, int)

  Description: Obtains the CU hit statistics of a database in MogDB.

  Return type: record

- pg_stat_get_cu_mem_hit(oid)

  Description: Obtains the number of CU memory hits of a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_cu_hdd_sync(oid)

  Description: Obtains the times CU is synchronously read from a disk by a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_cu_hdd_asyn(oid)

  Description: Obtains the times CU is asynchronously read from a disk by a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_db_cu_mem_hit(oid)

  Description: Obtains the CU memory hit in a database of the current node.

  Return type: bigint

- pg_stat_get_db_cu_hdd_sync(oid)

  Description: Obtains the times CU is synchronously read from a disk by a database of the current node.

  Return type: bigint

- pgxc_get_wlm_current_instance_info(text, int default null)

  Description: Queries the current resource usage on the primary database node and reads the data that is not stored in section "5.19.2.3 GS_WLM_INSTANCE_HISTORY" system catalog in the memory. The input parameters are the node name (**ALL**, **C**, **D**, or *instance name*) and the maximum number of records returned by each node. The returned value is **GS_WLM_INSTANCE_HISTORY**.

  Return type: setofrecord

- pgxc_get_wlm_history_instance_info(text, TIMESTAMP, TIMESTAMP, int default null)

  Description: Queries the historical resource usage on the primary database node and reads the data in section "GS_WLM_INSTANCE_HISTORY" system catalog in the memory. The input parameters are as follows: node name (**ALL**, **C**, **D**, or *instance name*), start time, end time, and maximum number of records returned for each instance. The returned value is **GS_WLM_INSTANCE_HISTORY**.

  Return type: setofrecord

- fenced_udf_process()

  Description: Shows the number of local UDF Master and Work processes.

  Return type: record

- total_cpu()

  Description: Obtains the CPU time used by the current node, in jiffies.

  Return type: bigint

- total_memory()

  Description: Obtains the size of the virtual memory used by the current node, in KB.

  Return type: bigint

- pg_stat_get_db_cu_hdd_asyn(oid)

  Description: Obtains the times CU is asynchronously read from a disk by a database of the current node.

  Return type: bigint

- pg_stat_bad_block(text, int, int, int, int, int, timestamp with time zone, timestamp with time zone)

  Description: Obtains damage information about pages or CUs after the current node is started.

  Example: select * from pg_stat_bad_block();

  Return type: record

- pg_stat_bad_block_clear()

  Description: Deletes the page and CU damage information that is read and recorded on the node. (System administrator rights are required.)

  Return type: void

- gs_respool_exception_info(pool text)

  Description: Queries for the query rule of a specified resource pool.

  Return type: record

- gs_control_group_info(pool text)

  Description: Queries for information about Cgroups associated with a resource pool.

  Return type: record

  The following table describes return fields.

  | Attribute | Attribute Value     | Description                                             |
  | --------- | ------------------- | ------------------------------------------------------- |
  | name      | class_a:workload_a1 | Name of class and workload                              |
  | class     | class_a             | Class Cgroup name                                       |
  | workload  | workload_a1         | Workload Cgroup name                                    |
  | type      | DEFWD               | Cgroup type (Top, CLASS, BAKWD, DEFWD, and TSWD)        |
  | gid       | 87                  | Cgroup ID                                               |
  | shares    | 30                  | Percentage of CPU resources to those on the parent node |
  | limits    | 0                   | Percentage of CPU cores to those on the parent node     |
  | rate      | 0                   | Allocation raio in Timeshare                            |
  | cpucores  | 0-3                 | CPU cores                                               |

- gs_all_control_group_info()

  Description: Collects information about all Cgroups in the database.

  Return type: record

- gs_get_control_group_info()

  Description: Collects information about all Cgroups.

  Return type: record

- get_instr_workload_info(integer)

  Description: Obtains the transaction volume and time information on the primary database node.

  Return type: record

  | Attribute           | Attribute Value | Description                                                  |
  | ------------------- | --------------- | ------------------------------------------------------------ |
  | resourcepool_oid    | 10              | OID of the resource pool (the logic is equivalent to the load) |
  | commit_counter      | 4               | Number of front-end transactions that were committed         |
  | rollback_counter    | 1               | Number of front-end transactions that were rolled back       |
  | resp_min            | 949             | Minimum response time of front-end transactions (unit: μs)   |
  | resp_max            | 201891          | Maximum response time of front-end transactions (unit: μs)   |
  | resp_avg            | 43564           | Average response time of front-end transactions (unit: μs)   |
  | resp_total          | 217822          | Total response time of front-end transactions (unit: μs)     |
  | bg_commit_counter   | 910             | Number of background transactions that were committed        |
  | bg_rollback_counter | 0               | Number of background transactions that were rolled back      |
  | bg_resp_min         | 97              | Minimum response time of background transactions (unit: μs)  |
  | bg_resp_max         | 678080687       | Maximum response time of background transactions (unit: μs)  |
  | bg_resp_avg         | 327847884       | Average response time of background transactions (unit: μs)  |
  | bg_resp_total       | 298341575300    | Total response time of background transactions (unit: μs)    |

- pv_instance_time()

  Description: Obtains the time consumed in each execution phase on the current node.

  Return type: record

  | Stat_name Attribute | Attribute Value | Description                                                  |
  | ------------------- | --------------- | ------------------------------------------------------------ |
  | DB_TIME             | 1062385         | Total end-to-end wall time consumed by all threads (unit: μs) |
  | CPU_TIME            | 311777          | Total CPU time consumed by all threads (unit: μs)            |
  | EXECUTION_TIME      | 380037          | Total time consumed on the executor (unit: μs)               |
  | PARSE_TIME          | 6033            | Total time consumed for parsing SQL statements (unit: μs)    |
  | PLAN_TIME           | 173356          | Total time consumed for generating an execution plan (unit: μs) |
  | REWRITE_TIME        | 2274            | Total time consumed on query rewriting (unit: μs)            |
  | PL_EXECUTION_TIME   | 0               | Total time consumed for executing PL/SQL statements (unit: μs) |
  | PL_COMPILATION_TIME | 557             | Total time consumed for SQL compilation (unit: μs)           |
  | NET_SEND_TIME       | 1673            | Total time consumed for sending data over network (unit: μs) |
  | DATA_IO_TIME        | 426622          | Total time consumed for data read and write (unit: μs)       |

- DBE_PERF.get_global_instance_time()

  Description: Provides the time consumption of each key phase of MogDB. This function is supported only on the primary node of the database. To query this function, you must have the **sysadmin** permission.

  Return type: record

- get_instr_unique_sql()

  Description: Obtains information about execution statements (normalized SQL statements) on the current node as a user with the **sysadmin** permission.

  Return type: record

- reset_unique_sql(text, text, bigint)

  Description: Resets information about system execution statements (normalized SQL statements) information as a user with the **sysadmin** permission. The value of the first parameter can be **global**  or **local**. **global** indicates that information on all nodes is cleared, and **local** indicates that only information on the current node is cleared. The value of the second parameter can be **ALL**, **BY_USERID**, or **BY_CNID**. **ALL** indicates that all information is cleared. **BY_USERID** indicates that the SQL information of the user specified by **USERID** is cleared. **BY_CNID** indicates that the SQL information related to the primary node of the database in the system is cleared. The third parameter indicates **CNID** and **USERID**. If the second parameter is set to **ALL**, the third parameter does not take effect and can be set to any value.

  Return type: Boolean

- get_instr_wait_event(NULL)

  Description: Obtains the statistics on wait events of the current node.

  Return type: record

- get_instr_user_login()

  Description: Obtains the number of user login and logout times of the current node as a user with the **sysadmin** permission.

  Return type: record

- get_instr_rt_percentile()

  Description: Obtains the response time distribution for 80% and 95% SQL statements in the CCN node. This information of MogDB is stored on the CCN node. The query result from other nodes is **0**.

  Return type: record

- get_node_stat_reset_time()

  Description: Obtains statistics about reset (restart, primary/standby switchover, and database deletion) time of the current node.

  Return type: record

- DBE_PERF.get_global_os_runtime()

  Description: Displays the running status of the current OS. This function is supported only on the primary node of the database. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_os_threads()

  Description: Provides information about the threads under all normal nodes of MogDB. This function is supported only on the primary node of the database. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_workload_sql_count()

  Description: Provides statistics about the number of SELECT, UPDATE, INSERT, DELETE, DDL, DML, and DCL statements of different service loads in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_workload_sql_elapse_time()

  Description: Provides statistics about the number of SELECT, UPDATE, INSERT, and DELETE statements and response time information (TOTAL, AVG, MIN, and MAX) for different loads in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_workload_transaction()

  Description: Obtains the transaction volume and time information on all nodes of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_stat()

  Description: Obtains the session status information on all nodes of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > The status information contains the following 17 items:**commit**, **rollback**, **sql**, **table_scan**, **blocks_fetched**, **physical_read_operation**, **shared_blocks_dirtied**, **local_blocks_dirtied**, **shared_blocks_read**, **local_blocks_read**, **blocks_read_time**, **blocks_write_time, sort_imemory**, **sort_idisk**, **cu_mem_hit**, **cu_hdd_sync_read**, and **cu_hdd_asyread**.

- DBE_PERF.get_global_session_time()

  Description: Provides the time consumed in each key phase of each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_memory()

  Description: Displays statistics about memory usage at the session level on each node in the unit of MB, including all the memory allocated to Postgres and stream threads on DNs for jobs currently executed by users. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_memory_detail()

  Description: Displays statistics about thread memory usage on each node by MemoryContext node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- gs_session_memory_detail_tp()

  Description: Collects statistics about thread memory usage by MemoryContext node. When **enable_thread_pool** is set to **on**, this view contains memory usage of all threads and sessions.

  Return type: record

- reate_wlm_operator_info(int flag)

  Description: Clears top SQL operator-level statistics recorded in the current memory. If the input parameter is greater than 0, the information is archived to **gs_wlm_operator_info** and **gs_wlm_ec_operator_info**. Otherwise, the information is not archived. Only the administrator can execute this function.

  Return type: int

- create_wlm_session_info(int flag)

  Description: Clears top SQL query statement-level statistics recorded in the current memory. Only the administrator can execute this function.

  Return type: int

- pg_stat_get_wlm_session_info(int flag)

  Description: Obtains top SQL query statement-level statistics recorded in the current memory. If the input parameter is not 0, the information is cleared from the memory. Only the system admin and monitor admin users can execute this function.

  Return type: record

- gs_wlm_get_resource_pool_info()

  Description: Obtains the resource usage statistics of all users.

  Return type: record

- gs_wlm_get_all_user_resource_info()

  Description: Obtains the resource usage statistics of all users.

  Return type: record

- gs_wlm_get_user_info()

  Description: Obtains information about all users.

  Return type: record

- gs_wlm_get_workload_records()

  Description: Obtains all job information in dynamic load management. This function is valid only when dynamic load management is enabled.

  Return type: record

- gs_wlm_persisitent_user_resource_info()

  Description: Archives all user resource usage statistics to the **gs_wlm_user_resource_history** system catalog.

  Return type: record

- gs_wlm_readjust_user_space()

  Description: Corrects the storage space usage of all users. Only the administrator can execute this function.

  Return type: record

- gs_wlm_readjust_user_space_through_username(text name)

  Description: Corrects the storage space usage of a specified user. Common users can use this function to modify only their own usage. Only the administrator can modify the usage of all users. If the value of **name** is **0000**, the usage of all users needs to be modified.

  Return type: record

- gs_wlm_readjust_user_space_with_reset_flag(text name, boolean isfirst)

  Description: Corrects the storage space usage of a specified user. If the input parameter **isfirst** is set to **true**, statistics are collected from 0. Otherwise, statistics are collected from the previous result. Common users can use this function to modify only their own usage. Only the administrator can modify the usage of all users. If the value of **name** is **0000**, the usage of all users needs to be modified.

  Return type: record

- gs_wlm_session_respool()

  Description: Obtains the session resource pool information about all background threads.

  Return type: record

- gs_wlm_get_session_info()

  Description: This API has been discarded and is unavailable currently.

- gs_wlm_get_user_session_info()

  Description: This API has been discarded and is unavailable currently.

- gs_total_nodegroup_memory_detail

  Description: Returns information about the memory used by the current logical cluster in the database, in MB.

  Return type: SETOF record

- global_comm_client_info()

  Description: Queries information about active client connections of global nodes. For details about the columns returned by the function, see the **COMM_CLIENT_INFO** column.

  Return type: record

- pgxc_get_wlm_ec_operator_history()

  Description: Displays the operator information when the execution of ExtensionConnector (EC) jobs cached on all primary nodes is complete. The information is cleared every 3 minutes. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_ec_operator_info()

  Description: Displays the operator information when the execution of ExtensionConnector (EC) jobs on all primary nodes is complete. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_ec_operator_statistics()

  Description: Displays the operator information when ExtensionConnector (EC) jobs on all primary nodes are being executed. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_operator_history()

  Description: Displays the operator information when the execution of jobs cached on all primary nodes is complete. The information is cleared every 3 minutes. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_operator_info()

  Description: Displays the operator information when the execution of jobs on all primary nodes is complete. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_operator_statistics()

  Description: Displays the operator information when jobs on all primary nodes are being executed. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_session_history()

  Description: Displays the load management information when the execution of jobs cached on all primary nodes is complete. The information is cleared every 3 minutes. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_session_info()

  Description: Displays the load management information when the execution of jobs cached on all primary nodes is complete. Only the system administrator can execute this function.

  Return type: record

- pgxc_get_wlm_session_info_bytime(tag text, begin timestamp, end timestamp, limit int)

    Description: Displays load management records of jobs whose start or end time is within a time range on all primary nodes. Only the system administrator can execute this function.

    Parameter description:

  - **tag**: The value can only be **'start_time'** or **'finish_time'**, indicating that the query is restricted by the start time or end time of the job.
  - **begin**: start time of a time range.
  - **end**: end time of a time range.
  - **limit**: number of returned records.

    Return type: record

- pgxc_get_wlm_session_statistics()

  Description: Displays load management records when jobs on all primary nodes are being executed. Only the system administrator can execute this function.

  Return type: record

- pgxc_stat_activity_with_conninfo()

  Description:**PGXC_STAT_ACTIVITY** shows query information about the current user on all the CNs in the current cluster. Only the system administrator has the permission to query this view.

  Return type: SETOF record

- pgxc_wlm_get_workload_records()

  Description: Displays the status information when jobs on all primary nodes are being executed. Only the system administrator can execute this function.

  Return type: record

- DBE_PERF.get_global_active_session()

  Description: Displays a summary of samples in the **ACTIVE SESSION PROFILE** memory on all nodes.

  Return type: record

- DBE_PERF.get_global_session_stat_activity()

  Description: Displays information about threads that are running on each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_thread_wait_status()

  Description: Displays the block waiting status of backend threads and auxiliary threads on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_wlm_user_resource_runtime()

  Description: Displays the resource usage of all users. This parameter is valid only when **use_workload_manager** is set to **on**. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_history_table()

  Description: Displays the operator-related records (persistent) generated after jobs are executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_history()

  Description: Displays the operator-related records generated after jobs are executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_runtime()

  Description: Displays real-time operator-related records of jobs executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_history()

  Description: Displays the historical records of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_history_table()

  Description: Displays the historical records (persistent) of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_runtime()

  Description: Displays the real-time information of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_memory_node_detail()

  Description: Displays the memory usage of a certain database on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_shared_memory_detail()

  Description: Displays the usage information about all the shared memory contexts of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_indexes

  Description: Displays statistics about each index displayed in a row in the current database, showing I/O statistics about accesses to that specific index. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_local_toastname_and_toastindexname()

  Description: Provides the mapping between the name and index of the local TOAST table and its associated tables.

  Return type: record

- DBE_PERF.get_summary_statio_all_indexes()

  Description: Collects statistics about each index displayed in a row in the current databases of all nodes and displays the I/O statistics of a specific index. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_sequences()

  Description: Provides I/O status information about all sequences in the namespace. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_tables()

  Description: Displays the I/O statistics about each table in databases on each node as a user with the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_all_tables()

  Description: Collects I/O statistics about each table in databases in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_local_toast_relation()

  Description: Provides the mapping between the name of the local toast table and its associated table. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_indexes()

  Description: Displays the I/O status information about all system catalog indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_sys_indexes()

  Description: Collects the I/O status information about all system catalog indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_sequences()

  Description: Provides the I/O status information about all the system sequences in the namespace. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_tables()

  Description: Provides I/O status information about all system catalogs in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_sys_tables()

  Description: Displays the I/O status information of all system catalogs in the namespace in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_indexes()

  Description: Displays the I/O status information about all user relationship table indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_user_indexes()

  Description: Displays the I/O status information about all user relationship table indexes in namespaces in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_sequences()

  Description: Displays the I/O status information about all user sequences in the namespace of each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_tables()

  Description: Displays the displays I/O status information about all user relationship tables in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_user_tables()

  Description: Displays the I/O status information about all user relationship tables in namespaces in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_stat_db_cu()

  Description: Queries CU hits in a database and in each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_all_indexes()

  Description: Displays statistics of each index in databases on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_all_indexes()

  Description: Collects statistics on each index in all databases on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_sys_tables()

  Description: Displays statistics about the system catalogs of all the namespaces in **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_sys_tables()

  Description: Collects statistics about the system catalogs of all the namespaces in **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_sys_indexes()

  Description: Displays index status information about all the system catalogs in the **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_sys_indexes()

  Description: Collects statistics about index status information about all the system catalogs in the **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_tables()

  Description: Displays the status information about customized ordinary tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_user_tables()

  Description: Collects statistics about the status information about customized ordinary tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_indexes()

  Description: Displays the status information about the index of customized ordinary tables in all databases. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_user_indexes()

  Description: Collects statistics about the status information about the index of customized ordinary tables in all databases. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_database()

  Description: Displays database statistics of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_database_conflicts()

  Description: Collects statistics on the database of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_all_tables()

  Description: Displays transaction status information about all ordinary tables and TOAST tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_all_tables()

  Description: Collects statistics about transaction status information about all ordinary tables and TOAST tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_sys_tables()

  Description: Displays transaction status information about all system catalogs in namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_sys_tables()

  Description: Collects statistics about transaction status information about all system catalogs in namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_user_tables()

  Description: Displays the transaction status information of the user tables in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_user_tables()

  Description: Collects statistics about the transaction status information of the user tables in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_functions()

  Description: Displays the transaction status information of customized functions in the namespaces on all nodes. To query the function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_user_functions()

  Description: Collects statistics about the transaction status information of customized functions in the namespaces on all nodes. To query the function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_bad_block()

  Description: Displays information about table and index read failures on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_file_redo_iostat()

  Description: Collects statistics on information about table and index read failures on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_file_iostat()

  Description: Displays statistics about data file I/Os on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_locks()

  Description: Displays lock information of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_replication_slots()

  Description: Displays logical replication information on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_bgwriter_stat()

  Description: Displays statistics about the background writer process's activities on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_replication_stat()

  Description: Displays information about log synchronization status on each node, such as the locations where the sender sends logs and where the receiver receives logs. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_transactions_running_xacts()

  Description: Displays information about running transactions on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_transactions_running_xacts()

  Description: Collects statistics of information about running transactions on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_transactions_prepared_xacts()

  Description: Displays information about transactions that are currently prepared for two-phase commit on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_transactions_prepared_xacts()

  Description: Collects statistics information about transactions that are currently prepared for two-phase commit on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statement()

  Description: Displays the status information of the historically-executed statements on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_count()

  Description: Displays the number of SELECT, UPDATE, INSERT, and DELETE statements and response time information (TOTAL, AVG, MIN, and MAX) on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_config_settings()

  Description: Displays GUC parameter configuration information on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_wait_events()

  Description: Displays the wait event status information on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_statement_responsetime_percentile()

  Description: Obtains the response time distribution for 80% and 95% SQL statements of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_user_login()

  Description: Collects statistics about number of user login and logout times on each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_record_reset_time()

  Description: Displays the statistics about reset (restart, primary/standby switchover, and database deletion) time of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- gs_wlm_user_resource_info(name text)

  Description: Queries for a user's resource quota and resource usage.

  Return type: record

- pg_stat_get_file_stat()

  Description: Rrecords statistics about data file I/Os to indicate I/O performance and detect performance problems such as abnormal I/O operations.

  Return type: record

- pg_stat_get_redo_stat()

  Description: Displays statistics on the replay of session thread logs.

  Return type: record

- pg_stat_get_status(int8)

  Description: Allows you to test the block waiting status about the backend thread and auxiliary thread of the current instance.

  Return type: record

- get_local_rel_iostat()

  Description: Queries the accumulated I/O status of data files on the current node.

  Return type: record

- DBE_PERF.get_global_rel_iostat()

  Description: Displays statistics about data file I/Os on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.global_threadpool_status()

  Description: Displays the status of worker threads and sessions in thread pools on all nodes. For details about the columns returned by the function, see **GLOBAL_THREADPOOL_STATUS**.

  Return type: record

- remote_bgwriter_stat()

  Description: Displays the information about pages flushed by the bgwriter threads of all instances in the cluster, number of pages in the candidate buffer chain, and buffer elimination information (except for the local node and not available on the DN).

  Return type: record

  **Table 7** remote_bgwriter_stat parameter description

  | Parameter                   | Type    | Description                                                  |
  | --------------------------- | ------- | ------------------------------------------------------------ |
  | node_name                   | text    | Instance name                                                |
  | bgwr_actual_flush_total_num | bigint  | Total number of dirty pages flushed by the bgwriter thread from the startup time to the current time |
  | bgwr_last_flush_num         | integer | Number of dirty pages flushed by the bgwriter thread in the previous batch |
  | candidate_slots             | integer | Number of pages in the current candidate buffer chain.       |
  | get_buffer_from_list        | bigint  | Number of times that pages are obtained from the candidate buffer chain during buffer elimination. |
  | get_buf_clock_sweep         | bigint  | Number of times that pages are obtained from the original elimination solution during buffer elimination. |

- remote_ckpt_stat()

  Description: Displays the checkpoint information and log flushing information about all instances in the cluster (except the current node).

  Return type: record

  **Table 8** remote_ckpt_stat parameter description

  | Parameter                | Type | Description                                                  |
  | ------------------------ | ---- | ------------------------------------------------------------ |
  | node_name                | text | Instance name                                                |
  | ckpt_redo_point          | text | Checkpoint of the current instance                           |
  | ckpt_clog_flush_num      | int8 | Number of Clog flushing pages from the startup time to the current time |
  | ckpt_csnlog_flush_num    | int8 | Number of CSN log flushing pages from the startup time to the current time |
  | ckpt_multixact_flush_num | int8 | Number of MultiXact flushing pages from the startup time to the current time |
  | ckpt_predicate_flush_num | int8 | Number of predicate flushing pages from the startup time to the current time |
  | ckpt_twophase_flush_num  | int8 | Number of two-phase flushing pages from the startup time to the current time |

- remote_double_write_stat()

  Description: Displays doublewrite file status of all instances in the cluster (except the current node).

  Return type: record

  **Table 9** remote_double_write_stat parameter description

  | Parameter             | Type | Description                                                  |
  | --------------------- | ---- | ------------------------------------------------------------ |
  | node_name             | text | Instance name                                                |
  | curr_dwn              | int8 | Sequence number of the doublewrite file                      |
  | curr_start_page       | int8 | Start page for restoring the doublewrite file                |
  | file_trunc_num        | int8 | Number of times that the doublewrite file is reused          |
  | file_reset_num        | int8 | Number of reset times after the doublewrite file is full     |
  | total_writes          | int8 | Total number of I/Os of the doublewrite file                 |
  | low_threshold_writes  | int8 | Number of I/Os for writing doublewrite files with low efficiency (the number of I/O flushing pages at a time is less than 16) |
  | high_threshold_writes | int8 | Number of I/Os for writing doublewrite files with high efficiency (the number of I/O flushing pages at a time is more than 421) |
  | total_pages           | int8 | Total number of pages that are flushed to the doublewrite file area |
  | low_threshold_pages   | int8 | Number of pages that are flushed with low efficiency         |
  | high_threshold_pages  | int8 | Number of pages that are flushed with high efficiency        |

- remote_pagewriter_stat()

  Description: Displays the page flushing information and checkpoint information about all instances in the cluster (except the current node).

  Return type: record

  **Table 10** remote_pagewriter_stat parameter description

  | Parameter                   | Type | Description                                                  |
  | --------------------------- | ---- | ------------------------------------------------------------ |
  | node_name                   | text | Instance name                                                |
  | pgwr_actual_flush_total_num | int8 | Total number of dirty pages flushed from the startup time to the current time |
  | pgwr_last_flush_num         | int4 | Number of dirty pages flushed in the previous batch          |
  | remain_dirty_page_num       | int8 | Estimated number of dirty pages that are not flushed         |
  | queue_head_page_rec_lsn     | text | **recovery_lsn** of the first dirty page in the dirty page queue of the current instance |
  | queue_rec_lsn               | text | **recovery_lsn** of the dirty page queue of the current instance |
  | current_xlog_insert_lsn     | text | Write position of Xlogs in the current instance              |
  | ckpt_redo_point             | text | Checkpoint of the current instance                           |

- remote_recovery_status()

  Description: Displays log flow control information about the primary and standby nodes (except the current node).

  Return type: record

  **Table 11** remote_recovery_status parameter description

  | Parameter          | Type | Description                                                  |
  | ------------------ | ---- | ------------------------------------------------------------ |
  | node_name          | text | Node name (including the primary and standby nodes)          |
  | standby_node_name  | text | Name of the standby node                                     |
  | source_ip          | text | IP address of the primary node                               |
  | source_port        | int4 | Port number of the primary node                              |
  | dest_ip            | text | IP address of the standby node                               |
  | dest_port          | int4 | Port number of the standby node                              |
  | current_rto        | int8 | Current log flow control time of the standby node (unit: s)  |
  | target_rto         | int8 | Expected flow control time of the standby node specified by the corresponding GUC parameter (unit: s) |
  | current_sleep_time | int8 | Sleep time required to achieve the expected flow control time (unit: μs) |

- remote_redo_stat()

  Description: Displays the log replay status of all instances in the cluster (except the current node).

  Return type: record

  **Table 12** remote_redo_stat parameter description

  | Parameter                 | Type | Description                                                  |
  | ------------------------- | ---- | ------------------------------------------------------------ |
  | node_name                 | text | Instance name                                                |
  | redo_start_ptr            | int8 | Start point for replaying the instance logs                  |
  | redo_start_time           | int8 | Start time (UTC) when the instance logs are replayed         |
  | redo_done_time            | int8 | End time (UTC) when the instance logs are replayed           |
  | curr_time                 | int8 | Current time (UTC) of the instance                           |
  | min_recovery_point        | int8 | Position of the minimum consistency point for the instance logs |
  | read_ptr                  | int8 | Position for reading the instance logs                       |
  | last_replayed_read_ptr    | int8 | Position for replaying the instance logs                     |
  | recovery_done_ptr         | int8 | Replay position after the instance is started                |
  | read_xlog_io_counter      | int8 | Number of I/Os when the instance reads and replays logs      |
  | read_xlog_io_total_dur    | int8 | Total I/O latency when the instance reads and replays logs   |
  | read_data_io_counter      | int8 | Number of data page I/O reads during replay in the instance  |
  | read_data_io_total_dur    | int8 | Total I/O latency of data page reads during replay in the instance |
  | write_data_io_counter     | int8 | Number of data page I/O writes during replay in the instance |
  | write_data_io_total_dur   | int8 | Total I/O latency of data page writes during replay in the instance |
  | process_pending_counter   | int8 | Number of synchronization times of log distribution threads during replay in the instance |
  | process_pending_total_dur | int8 | Total synchronization latency of log distribution threads during replay in the instance |
  | apply_counter             | int8 | Number of synchronization times of replay threads during replay in the instance |
  | apply_total_dur           | int8 | Total synchronization latency of replay threads during replay in the instance |
  | speed                     | int8 | Log replay rate of the current instance                      |
  | local_max_ptr             | int8 | Maximum number of replay logs received by the local host after the instance is started |
  | primary_flush_ptr         | int8 | Log point where the host flushes logs to a disk              |
  | worker_info               | text | Replay thread information of the instance. If concurrent replay is not enabled, the value is **NULL**. |

Example:

The function **pg_backend_pid** shows the ID of the current server thread.

```sql
mogdb=# SELECT pg_backend_pid();
 pg_backend_pid
-----------------
 139706243217168
(1 row)
```

The function **pg_stat_get_backend_pid** shows the ID of a given server thread.

```sql
mogdb=# SELECT pg_stat_get_backend_pid(1);
 pg_stat_get_backend_pid
-------------------------
         139706243217168
(1 row)
```
