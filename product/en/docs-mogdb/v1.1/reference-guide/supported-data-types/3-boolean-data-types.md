---
title: Boolean Data Types
summary: Boolean Data Types
author: Guo Huan
date: 2021-04-06
---

# Boolean Data Types

**Table 1** Boolean types

| Name    | Description  | Storage Space | Value                                               |
| :------ | :----------- | :------------ | :-------------------------------------------------- |
| BOOLEAN | Boolean type | 1 byte        | - **true**<br/>- **false**<br/>- **null** (unknown) |

Valid literal values for the "true" state include:

TRUE, 't', 'true', 'y', 'yes', '1', 'TRUE',true, and an integer ranging from 1 to 2^63-1 or from -1 to -2^63.

Valid literal values for the "false" state include:

FALSE, 'f', 'false', 'n', 'no', '0', 'FALSE', false, 0

**TRUE** and **FALSE** are standard expressions, compatible with SQL statements.

## Example

Boolean values are displayed using the letters t and f.

```sql
-- Create a table.
mogdb=# CREATE TABLE bool_type_t1
(
    BT_COL1 BOOLEAN,
    BT_COL2 TEXT
);

-- Insert data.
mogdb=# INSERT INTO bool_type_t1 VALUES (TRUE, 'sic est');

mogdb=# INSERT INTO bool_type_t1 VALUES (FALSE, 'non est');

-- View data.
mogdb=# SELECT * FROM bool_type_t1;
 bt_col1 | bt_col2
---------+---------
 t       | sic est
 f       | non est
(2 rows)

mogdb=# SELECT * FROM bool_type_t1 WHERE bt_col1 = 't';
 bt_col1 | bt_col2
---------+---------
 t       | sic est
(1 row)

-- Delete the table.
mogdb=# DROP TABLE bool_type_t1;
```
