---
title: Network Address
summary: Network Address
author: Guo Huan
date: 2021-04-06
---

# Network Address

MogDB offers data types to store IPv4, IPv6, and MAC addresses.

It is better to use these types instead of plain text types to store network addresses, because these types offer input error checking and specialized operators and functions (see Network Address Functions and Operators).

**Table 1** Network address types

| Name    | Storage Space | Description                     |
| :------ | :------------ | :------------------------------ |
| cidr    | 7 or 19 bytes | IPv4 or IPv6 networks           |
| inet    | 7 or 19 bytes | IPv4 or IPv6 hosts and networks |
| macaddr | 6 bytes       | MAC address                     |

When sorting **inet** or **cidr** data types, IPv4 addresses will always sort before IPv6 addresses, including IPv4 addresses encapsulated or mapped to IPv6 addresses, such as ::10.2.3.4 or ::ffff:10.4.3.2.

## cidr

The **cidr** type (Classless Inter-Domain Routing) holds an IPv4 or IPv6 network specification. The format for specifying networks is **address/y** where **address** is the network represented as an IPv4 or IPv6 address, and **y** is the number of bits in the netmask. If **y** is omitted, it is calculated using assumptions from the older classful network numbering system, except it will be at least large enough to include all of the octets written in the input.

**Table 2** **cidr** type input examples

| cidr Input                           | cidr Output                          | abbrev(cidr)                     |
| :----------------------------------- | :----------------------------------- | :------------------------------- |
| 192.168.100.128/25                   | 192.168.100.128/25                   | 192.168.100.128/25               |
| 192.168/24                           | 192.168.0.0/24                       | 192.168.0/24                     |
| 192.168/25                           | 192.168.0.0/25                       | 192.168.0.0/25                   |
| 192.168.1                            | 192.168.1.0/24                       | 192.168.1/24                     |
| 192.168                              | 192.168.0.0/24                       | 192.168.0/24                     |
| 10.1.2                               | 10.1.2.0/24                          | 10.1.2/24                        |
| 10.1                                 | 10.1.0.0/16                          | 10.1/16                          |
| 10                                   | 10.0.0.0/8                           | 10/8                             |
| 10.1.2.3/32                          | 10.1.2.3/32                          | 10.1.2.3/32                      |
| 2001:4f8:3:ba::/64                   | 2001:4f8:3:ba::/64                   | 2001:4f8:3:ba::/64               |
| 2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128 | 2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128 | 2001:4f8:3:ba:2e0:81ff:fe22:d1f1 |
| ::ffff:1.2.3.0/120                   | ::ffff:1.2.3.0/120                   | ::ffff:1.2.3⁄120                 |
| ::ffff:1.2.3.0/128                   | ::ffff:1.2.3.0/128                   | ::ffff:1.2.3.0/128               |

## inet

The **inet** type holds an IPv4 or IPv6 host address, and optionally its subnet, all in one field. The subnet is represented by the number of network address bits present in the host address (the "netmask"). If the netmask is 32 and the address is IPv4, then the value does not indicate a subnet, only a single host. In IPv6, the address length is 128 bits, so 128 bits specify a unique host address.

The input format for this type is **address/y** where address is an IPv4 or IPv6 address and **y** is the number of bits in the netmask. If the **/y** portion is missing, the netmask is 32 for IPv4 and 128 for IPv6, so the value represents just a single host. On display, the **/y** portion is suppressed if the netmask specifies a single host.

The essential difference between the **inet** and **cidr** data types is that **inet** accepts values with nonzero bits to the right of the netmask, whereas **cidr** does not.

## macaddr

The **macaddr** type stores MAC addresses, known for example from Ethernet card hardware addresses (although MAC addresses are used for other purposes as well). Input is accepted in the following formats:

```
'08:00:2b:01:02:03'
'08-00-2b-01-02-03'
'08002b:010203'
'08002b-010203'
'0800.2b01.0203'
'08002b010203'
```

These examples would all specify the same address. Upper and lower cases are accepted for the digits a through f. Output is always in the first of the forms shown.
