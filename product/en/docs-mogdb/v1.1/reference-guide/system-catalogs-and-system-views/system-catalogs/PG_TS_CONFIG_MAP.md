---
title: PG_TS_CONFIG_MAP
summary: PG_TS_CONFIG_MAP
author: Guo Huan
date: 2021-04-19
---

# PG_TS_CONFIG_MAP

**PG_TS_CONFIG_MAP** contains entries showing which text search dictionaries should be consulted, and in what order, for each output token type of each text search configuration's parser.

**Table 1** PG_TS_CONFIG_MAP columns

| Name         | Type    | Reference        | Description                                         |
| :----------- | :------ | :--------------- | :-------------------------------------------------- |
| mapcfg       | oid     | PG_TS_CONFIG.oid | OID of the PG_TS_CONFIG entry owning this map entry |
| maptokentype | integer | -                | Token type emitted by the configuration's parser    |
| mapseqno     | integer | -                | Order for consulting this entry                     |
| mapdict      | oid     | PG_TS_DICT.oid   | OID of the text search dictionary to consult        |
