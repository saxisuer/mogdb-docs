---
title: PG_FOREIGN_SERVER
summary: PG_FOREIGN_SERVER
author: Guo Huan
date: 2021-04-19
---

# PG_FOREIGN_SERVER

**PG_FOREIGN_SERVER** records foreign server definitions. A foreign server describes a source of external data, such as a remote server. Foreign servers are accessed via foreign-data wrappers.

**Table 1** PG_FOREIGN_SERVER columns

| Name       | Type      | Reference                   | Description                                                  |
| :--------- | :-------- | :-------------------------- | :----------------------------------------------------------- |
| oid        | oid       | -                           | Row identifier (hidden attribute, which must be specified)   |
| srvname    | name      | -                           | Name of a foreign server                                     |
| srvowner   | oid       | PG_AUTHID.oid               | Owner of the foreign server                                  |
| srvfdw     | oid       | PG_FOREIGN_DATA_WRAPPER.oid | OID of the foreign-data wrapper on this foreign server       |
| srvtype    | text      | -                           | Type of the server (optional)                                |
| srvversion | text      | -                           | Version of the server (optional)                             |
| srvacl     | aclitem[] | -                           | Access permissions                                           |
| srvoptions | text[]    | -                           | Option used for foreign servers, expressed in a string in the format of keyword=value |
