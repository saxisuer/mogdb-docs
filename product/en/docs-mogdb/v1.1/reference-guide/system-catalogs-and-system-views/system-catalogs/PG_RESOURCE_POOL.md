---
title: PG_RESOURCE_POOL
summary: PG_RESOURCE_POOL
author: Guo Huan
date: 2021-04-19
---

# PG_RESOURCE_POOL

**PG_RESOURCE_POOL** provides information about database resource pools.

**Table 1** PG_RESOURCE_POOL columns

| Name              | Type    | Description                                                  |
| :---------------- | :------ | :----------------------------------------------------------- |
| oid               | oid     | Row identifier (hidden attribute, which must be specified)   |
| respool_name      | name    | Name of a resource pool                                      |
| mem_percent       | integer | Percentage of the memory configuration                       |
| cpu_affinity      | bigint  | Value of cores bound to the CPU                              |
| control_group     | name    | Name of the Cgroup where the resource pool is located        |
| active_statements | integer | Maximum number of concurrent statements in the resource pool |
| max_dop           | integer | Maximum degree of parallelism                                |
| memory_limit      | name    | Maximum memory of the resource pool                          |
| parentid          | oid     | OID of the parent resource pool                              |
| io_limits         | integer | Upper limit of I/O operations per second. It is counted by ones for column storage and by 10 thousands for row storage. |
| io_priority       | name    | I/O priority set for jobs that consume many I/O resources. It takes effect when I/O usage reaches 90%. |
| nodegroup         | name    | Name of the logical MogDB to which the resource pool belongs |
| is_foreign        | boolean | Whether the resource pool can be used for users outside the logical MogDB. If it is set to **true**, the resource pool controls the resources of common users who do not belong to the current resource pool. |
