---
title: PG_CAST
summary: PG_CAST
author: Guo Huan
date: 2021-04-19
---

# PG_CAST

**PG_CAST** records the conversion relationship between data types.

**Table 1** PG_CAST columns

| Name        | Type   | Description                                                  |
| :---------- | :----- | :----------------------------------------------------------- |
| oid         | oid    | Row identifier (hidden attribute, which must be specified)   |
| castsource  | oid    | OID of the source data type                                  |
| casttarget  | oid    | OID of the target data type                                  |
| castfunc    | oid    | OID of the conversion function (**0** if no conversion function is required) |
| castcontext | "char" | Conversion mode between the source and target data types.<br/>- 'e': Only explicit conversion can be performed (using the CAST or :: syntax).<br/>- 'i': Only implicit conversion can be performed.<br/>- 'a': Both explicit and implicit conversion can be performed between data types. |
| castmethod  | "char" | Conversion method.<br/>- 'f': Conversion is performed using the specified function in the **castfunc** column.<br/>- 'b': Binary forcible conversion rather than the specified function in the **castfunc** column is performed between data types. |
