---
title: PG_SHSECLABEL
summary: PG_SHSECLABEL
author: Guo Huan
date: 2021-04-19
---

# PG_SHSECLABEL

**PG_SHSECLABEL** records security labels on shared database objects. Security labels can be manipulated with the **SECURITY LABEL** command.

For an easier way to view security labels, see PG_SECLABELS.

See also PG_SECLABEL, which provides a similar function for security labels involving objects within a single database.

Unlike most system catalogs, **PG_SHSECLABEL** is shared across all databases of MogDB. There is only one copy of **PG_SHSECLABEL** per MogDB, not one per database.

**Table 1** PG_SHSECLABEL columns

| Name     | Type | Reference      | Description                                            |
| :------- | :--- | :------------- | :----------------------------------------------------- |
| objoid   | oid  | Any OID column | OID of the object that this security label pertains to |
| classoid | oid  | PG_CLASS.oid   | OID of the system catalog where the object appears     |
| provider | text | -              | Label provider associated with the label               |
| label    | text | -              | Security label applied to the object                   |
