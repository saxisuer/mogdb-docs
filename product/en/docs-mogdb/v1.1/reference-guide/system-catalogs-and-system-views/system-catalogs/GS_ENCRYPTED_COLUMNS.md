---
title: GS_ENCRYPTED_COLUMNS
summary: GS_ENCRYPTED_COLUMNS
author: Guo Huan
date: 2021-04-19
---

# GS_ENCRYPTED_COLUMNS

**GS_ENCRYPTED_COLUMNS** records information about encrypted columns in the encrypted equality feature. Each record corresponds to an encrypted column.

**Table 1** GS_ENCRYPTED_COLUMNS columns

| Name                   | Type      | Description                                                  |
| :--------------------- | :-------- | :----------------------------------------------------------- |
| rel_id                 | oid       | Table OID                                                    |
| column_name            | name      | Name of an encrypted column.                                 |
| column_key_id          | oid       | A foreign key, which is the CEK OID                          |
| encryption_type        | int1      | Encryption type. The value can be **DETERMINISTIC** or **RANDOMIZED**. |
| data_type_original_oid | oid       | ID of the original data type of the encrypted column         |
| data_type_original_mod | int4      | Modifiers of the original data type of the encrypted column  |
| create_date            | timestamp | Time when an encrypted column is created                     |
