---
title: PG_PARTITION
summary: PG_PARTITION
author: Guo Huan
date: 2021-04-19
---

# PG_PARTITION

**PG_PARTITION** records all partitioned tables, table partitions, TOAST tables on table partitions, and index partitions in the database. Partitioned index information is not stored in the system catalog **PG_PARTITION**.

**Table 1** PG_PARTITION columns

| Name               | Type             | Description                                                  |
| :----------------- | :--------------- | :----------------------------------------------------------- |
| oid                | oid              | Row identifier (hidden attribute, which must be specified)   |
| relname            | name             | Names of the partitioned tables, table partitions, TOAST tables on table partitions, and index partitions |
| parttype           | "char"           | Object type<br/>- **r**: partitioned table<br/>- **p**: table partition<br/>- **x**: index partition<br/>- **t**: TOAST table |
| parentid           | oid              | OID of the partitioned table in **PG_CLASS** when the object is a partitioned table or table partition<br/>OID of the partitioned index when the object is an index partition |
| rangenum           | integer          | Reserved                                                     |
| intervalnum        | integer          | Reserved                                                     |
| partstrategy       | "char"           | Partition policy of the partitioned table<br/>- **r**: range partition<br/>- **v**: numeric partition<br/>- **i**: interval partition |
| relfilenode        | oid              | Physical storage locations of the table partition, index partition, and TOAST table on the table partition |
| reltablespace      | oid              | OID of the tablespace containing the table partition, index partition, and TOAST table on the table partition |
| relpages           | double precision | Statistics: numbers of data pages of the table partition and index partition |
| reltuples          | double precision | Statistics: numbers of tuples of the table partition and index partition |
| relallvisible      | integer          | Statistics: number of visible data pages of the table partition and index partition |
| reltoastrelid      | oid              | OID of the TOAST table corresponding to the table partition  |
| reltoastidxid      | oid              | OID of the TOAST table index corresponding to the table partition |
| indextblid         | oid              | OID of the table partition corresponding to the index partition |
| indisusable        | Boolean          | Whether the index partition is available                     |
| reldeltarelid      | oid              | OID of a Delta table                                         |
| reldeltaidx        | oid              | OID of the index for a Delta table                           |
| relcudescrelid     | oid              | OID of a CU description table                                |
| relcudescidx       | oid              | OID of the index for a CU description table                  |
| relfrozenxid       | xid32            | Frozen transaction ID<br/>To ensure forward compatibility, this column is reserved. The **relfrozenxid64** column is added to record the information. |
| intspnum           | integer          | Number of tablespaces that the interval partition belongs to |
| partkey            | int2vector       | Column number of the partition key                           |
| intervaltablespace | oidvector        | Tablespace that the interval partition belongs to. Interval partitions fall in the tablespaces in the round-robin manner. |
| interval           | text[]           | Interval value of the interval partition                     |
| boundaries         | text[]           | Upper boundary of the range partition and interval partition |
| transit            | text[]           | Transit of the interval partition                            |
| reloptions         | text[]           | Storage property of a partition used for collecting online scale-out information. Same as **pg_class.reloptions**, it is expressed in a string in the format of keyword=value. |
| relfrozenxid64     | xid              | Frozen transaction ID                                        |
