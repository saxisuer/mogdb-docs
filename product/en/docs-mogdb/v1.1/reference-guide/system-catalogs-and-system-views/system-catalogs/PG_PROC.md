---
title: PG_PROC
summary: PG_PROC
author: Guo Huan
date: 2021-04-19
---

# PG_PROC

**PG_PROC** records information about functions or procedures.

**Table 1** PG_PROC columns

| Name             | Type         | Description                                                  |
| :--------------- | :----------- | :----------------------------------------------------------- |
| oid              | oid          | Row identifier (hidden attribute, which must be specified)   |
| proname          | name         | Function name                                                |
| pronamespace     | oid          | OID of the namespace that contains the function              |
| proowner         | oid          | Owner of the function                                        |
| prolang          | oid          | Implementation language or call interface of the function    |
| procost          | real         | Estimated execution cost                                     |
| prorows          | real         | Estimated number of rows that are influenced                 |
| provariadic      | oid          | Data type of parameter element                               |
| protransform     | regproc      | Simplified call method for the function                      |
| prokind          | "char"       | Function kind, 'f' for a normal function, 'p' for a procedure, 'a' for an aggregate function, or 'w' for a window function |
| prosecdef        | Boolean      | Whether the function is a security definer (or a "setuid" function) |
| proleakproof     | Boolean      | This function has no side effect. If no leakproof treatment is provided for parameters, the function throws errors. |
| proisstrict      | Boolean      | The function returns null if any call parameter is null. In that case, the function is actually not called at all. Functions that are not "strict" must be prepared to process null inputs. |
| proretset        | Boolean      | The function returns a set (multiple values of a specified data type). |
| provolatile      | "char"       | Whether the function's result depends only on its input parameters, or is affected by outside factors.<br/>- **i**: for "immutable" functions, which always deliver the same result for the same inputs.<br/>- **s**: for "stable" functions, whose results (for fixed inputs) do not change within a scan.<br/>- **v**: for "volatile" functions, whose results may change at any time. Use **v** also for functions with side-effects, so that the engine cannot get optimized if volatile functions are called. |
| pronargs         | smallint     | Number of parameters                                         |
| pronargdefaults  | smallint     | Number of parameters that have default values                |
| prorettype       | oid          | Data type of return values                                   |
| proargtypes      | oidvector    | Array that stores the data types of function parameters. This array includes only input parameters (including **INOUT** parameters), and represents the call signature (interface) of the function. |
| proallargtypes   | oid[]        | Array that contains the data types of function parameters. This array includes all parameter types (including **OUT** and **INOUT** parameters); however, if all the parameters are **IN** parameters, this column is null. Note that array subscripting is 1-based, whereas for historical reasons, **proargtypes** is subscripted from 0. |
| proargmodes      | "char"[]     | Array that stores the modes of function parameters, encoded as **i** for **IN** parameters, **o** for **OUT** parameters, and **b** for **INOUT** parameters, **v** for **VARIADIC** parameters. If all the parameters are **IN** parameters, this column is null. Note that subscripts correspond to positions of **proallargtypes**, not **proargtypes**. |
| proargnames      | text[]       | Array that stores the names of the function parameters. Parameters without a name are set to empty strings in the array. If none of the parameters have a name, this column is null. Note that subscripts correspond to positions of **proallargtypes**, not **proargtypes**. |
| proargdefaults   | pg_node_tree | Expression tree of the default value. This is the list of **pronargdefaults** elements. |
| prosrc           | text         | A definition that describes a function or stored procedure. In an interpreting language, it is the function source code, a link symbol, a file name, or any body content specified when a function or stored procedure is created, depending on how a language or call is used. |
| probin           | text         | Additional information about how to call the function. Again, the interpretation is language-specific. |
| proconfig        | text[]       | Function's local settings for run-time configuration variables. |
| proacl           | aclitem[]    | Access permissions.                                          |
| prodefaultargpos | int2vector   | Locations of the function default values. Not only the last few parameters have default values. |
| fencedmode       | Boolean      | Execution mode of a function, indicating whether the function is executed in fence or not fence mode. If the execution mode is **fence,** the function is executed in the fork process that is reworked. For the C function created by the user, the default value of the fencedmode field is true, that is, fence mode; the system built-in function, the fencedmode field is all false, that is, not fence mode. |
| proshippable     | Boolean      | Whether the function can be pushed down to database nodes. The default value is **false**.<br/>- Functions of the IMMUTABLE type can always be pushed down to the database nodes.<br/>- A STABLE or VOLATILE function can be pushed down to the database nodes only if SHIPPABLE is specified for it. |
| propackage       | Boolean      | Whether the function supports overloading. The default value is **false**. |
