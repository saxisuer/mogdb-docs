---
title: PG_ATTRIBUTE
summary: PG_ATTRIBUTE
author: Guo Huan
date: 2021-04-19
---

# PG_ATTRIBUTE

**PG_ATTRIBUTE** records information about table columns.

**Table 1** PG_ATTRIBUTE columns

| Name          | Type      | Description                                                  |
| :------------ | :-------- | :----------------------------------------------------------- |
| attrelid      | oid       | Table to which a column belongs                              |
| attname       | name      | Column name                                                  |
| atttypid      | oid       | Column type                                                  |
| attstattarget | integer   | Level of details of statistics collected for this column by **ANALYZE**.<br/>- The value **0** indicates that no statistics should be collected.<br/>- A negative value indicates that the system default statistic object is used.<br/>- The exact meaning of positive values is data type-dependent.<br/>For scalar data types, **attstattarget** is both the target number of "most common values" to collect, and the target number of histogram bins to create. |
| attlen        | smallint  | Copy of **typlen** in **PG_TYPE** of the column's type       |
| attnum        | smallint  | Number of the column                                         |
| attndims      | integer   | Number of dimensions if the column is an array (**0** in other cases) |
| attcacheoff   | integer   | This column is always **-1** on disk. When it is loaded into a row descriptor in the memory, it may be updated to cache the offset of the columns in the row. |
| atttypmod     | integer   | Type-specific data supplied at the table creation time (for example, the maximum length of a **varchar** column). This column is used as the third parameter when passing to type-specific input functions and length coercion functions. The value will generally be **-1** for types that do not need ATTTYPMOD. |
| attbyval      | Boolean   | Copy of **typbyval** in **PG_TYPE** of this column's type    |
| attstorage    | "char"    | Copy of **typstorage** in **PG_TYPE** of this column's type  |
| attalign      | "char"    | Copy of **typalign** in **PG_TYPE** of this column's type    |
| attnotnull    | Boolean   | A non-null constraint. It is possible to change this column to enable or disable the constraint. |
| atthasdef     | Boolean   | This column has a default value, in which case there will be a corresponding entry in the **PG_ATTRDEF** table that actually defines the value. |
| attisdropped  | Boolean   | Indicates that this column has been deleted and is no longer valid. A deleted column is still physically present in the table but is ignored by the analyzer, so it cannot be accessed through SQL. |
| attislocal    | Boolean   | Indicates that this column is locally defined in the relationship. Note that a column can be locally defined and inherited simultaneously. |
| attcmprmode   | tinyint   | Compressed modes for a specific column. The compressed mode includes:<br/>- ATT_CMPR_NOCOMPRESS<br/>- ATT_CMPR_DELTA<br/>- ATT_CMPR_DICTIONARY<br/>- ATT_CMPR_PREFIX<br/>- ATT_CMPR_NUMSTR |
| attinhcount   | integer   | Number of direct ancestors that this column has. A column with an ancestor cannot be dropped nor renamed. |
| attcollation  | oid       | Defined collation of a column                                |
| attacl        | aclitem[] | Permissions for column-level access                          |
| attoptions    | text[]    | Property-level options                                       |
| attfdwoptions | text[]    | Property-level external data options                         |
| attinitdefval | bytea     | **attinitdefval** stores the default value expression. **ADD COLUMN** in the row-store table must use this column. |
| attkvtype     | tinyint   | Specifies the key value type for a column. Value:<br/>0. ATT_KV_UNDEFINED: default value<br/>1. ATT_KV_TAG: dimension<br/>2. ATT_KV_FIELD: indicator<br/>3. ATT_KV_TIMETAG: time column |
