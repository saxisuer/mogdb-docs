---
title: PG_ATTRDEF
summary: PG_ATTRDEF
author: Guo Huan
date: 2021-04-19
---

# PG_ATTRDEF

**PG_ATTRDEF** records default values of columns.

**Table 1** PG_ATTRDEF columns

| Name    | Type         | Description                                                 |
| :------ | :----------- | :---------------------------------------------------------- |
| oid     | oid          | Row identifier (hidden attribute, which must be specified)  |
| adrelid | oid          | Table to which a column belongs                             |
| adnum   | smallint     | Number of columns                                           |
| adbin   | pg_node_tree | Internal representation of the default value of the column  |
| adsrc   | text         | Internal representation of the human-readable default value |
