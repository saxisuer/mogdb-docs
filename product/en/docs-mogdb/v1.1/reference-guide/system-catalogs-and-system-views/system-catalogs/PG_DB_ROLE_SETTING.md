---
title: PG_DB_ROLE_SETTING
summary: PG_DB_ROLE_SETTING
author: Guo Huan
date: 2021-04-19
---

# PG_DB_ROLE_SETTING

**PG_DB_ROLE_SETTING** records the default values of configuration items bound to each role and database when the database is running.

**Table 1** PG_DB_ROLE_SETTING columns

| Name        | Type   | Description                                                  |
| :---------- | :----- | :----------------------------------------------------------- |
| setdatabase | oid    | Database corresponding to the configuration items (**0** if no database is specified) |
| setrole     | oid    | Role corresponding to the configuration items (**0** if no role is specified) |
| setconfig   | text[] | Default value of configuration items when the database is running |
