---
title: GS_OPT_MODEL
summary: GS_OPT_MODEL
author: Guo Huan
date: 2021-04-19
---

# GS_OPT_MODEL

**GS_OPT_MODEL** is a data table used when the AI engine is enabled to predict the planned time. It records the configurations, training results, features, corresponding system functions, and training history of machine learning models.

**Table 1** GS_OPT_MODEL columns

| Name          | Type     | Description                                                  |
| :------------ | :------- | :----------------------------------------------------------- |
| template_name | name     | Template name of the machine learning model, which determines the interfaces invoked for training and prediction. Currently, only rlstm is implemented. |
| model_name    | name     | Model name. Each model corresponds to a set of parameters, training logs, and model coefficients in the AI engine online learning process. The name must be unique. |
| datname       | name     | Name of the database served by the model. Each model is specific to a single database. This parameter determines data used for training. |
| ip            | name     | IP address of the host where the AI engine is deployed       |
| port          | integer  | Listening port number of the AI engine                       |
| max_epoch     | integer  | Maximum number of iterations in an epoch                     |
| learning_rate | real     | Learning rate of model training. The default value **1** is recommended. |
| dim_red       | real     | Number of model feature dimensions whose retention is reduced |
| hidden_units  | integer  | Number of neurons in the model's hidden layer. If the model cannot be converged for a long time, increase the value of this parameter. |
| batch_size    | integer  | Size of a batch in each iteration. It is recommended that the size be greater than or equal to the total training data volume to accelerate model convergence. |
| feature_size  | integer  | Length of the model feature, which is used to trigger retraining. This parameter is automatically updated after model training and does not need to be specified. |
| available     | boolean  | Whether the model is converged. This parameter does not need to be specified. |
| Is_training   | boolean  | Whether the model is being trained. This parameter does not need to be specified. |
| label         | "char"[] | Target task of the model.<br/>- S: startup time<br/>- T: total time<br/>- R: rows<br/>- M: peak memory<br/>Currently, {S, T} or {R} is recommended due to model performance restrictions. |
| max           | bigint[] | Maximum value of each task label of the model, which is used to trigger retraining. This parameter does not need to be specified. |
| acc           | real[]   | Accuracy of each model task. This parameter does not need to be specified. |
| description   | text     | Model comment                                                |
