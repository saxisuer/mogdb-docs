---
title: PG_STATISTIC_EXT
summary: PG_STATISTIC_EXT
author: Guo Huan
date: 2021-04-19
---

# PG_STATISTIC_EXT

**PG_STATISTIC_EXT** records extended statistics of tables in a database, such as statistics of multiple columns. Statistics of expressions will be supported later. You can specify the extended statistics to collect. This system catalog is accessible only to system administrators.

**Table 1** PG_STATISTIC_EXT columns

| Name          | Type         | Description                                                  |
| :------------ | :----------- | :----------------------------------------------------------- |
| starelid      | oid          | Table or index that the described column belongs to          |
| starelkind    | "char"       | Type of an object                                            |
| stainherit    | Boolean      | Whether to collect statistics for objects that have inheritance relationship |
| stanullfrac   | real         | Percentage of column entries that are null                   |
| stawidth      | integer      | Average stored width, in bytes, of non-null entries          |
| stadistinct   | real         | Number of distinct, non-**NULL** data values in the column for database nodes<br/>- A value greater than 0 is the actual number of distinct values.<br/>- A value less than 0 is the negative of a multiplier for the number of rows in the table. (For example, **stadistinct=-0.5** indicates that values in a column appear twice on average.)<br/>- The value **0** indicates that the number of distinct values is unknown. |
| stadndistinct | real         | Number of unique non-null data values in the **dn1** column<br/>- A value greater than 0 is the actual number of distinct values.<br/>- A value less than 0 is the negative of a multiplier for the number of rows in the table. (For example, **stadistinct=-0.5** indicates that values in a column appear twice on average.)<br/>- The value **0** indicates that the number of distinct values is unknown. |
| stakindN      | smallint     | Code number stating that the type of statistics is stored in slot N of the **pg_statistic** row<br/>Value range: 1 to 5 |
| staopN        | oid          | Operator used to generate the statistics stored in slot N. For example, a histogram slot shows the < operator that defines the sort order of the data.<br/>Value range: 1 to 5 |
| stakey        | int2vector   | Array of a column ID                                         |
| stanumbersN   | real[]       | Numerical statistics of the appropriate type for slot N. The value is **NULL** if the slot does not involve numerical values.<br/>Value range: 1 to 5 |
| stavaluesN    | anyarray     | Column data values of the appropriate type for slot N. The value is **NULL** if the slot type does not store any data values. Each array's element values are actually of the specific column's data type so there is no way to define these columns' type more specifically than anyarray.<br/>Value range: 1 to 5 |
| staexprs      | pg_node_tree | Expression corresponding to the extended statistics information. |
