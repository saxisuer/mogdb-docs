---
title: PG_USER
summary: PG_USER
author: Guo Huan
date: 2021-04-19
---

# PG_USER

**PG_USER** provides information about database users. By default, only the initial user and users with the sysadmin attribute can view the information. Other users can view the information only after being granted permissions.

**Table 1** PG_USER columns

| Name            | Type                     | Description                                                  |
| :-------------- | :----------------------- | :----------------------------------------------------------- |
| usename         | name                     | Username                                                     |
| usesysid        | oid                      | ID of this user                                              |
| usecreatedb     | Boolean                  | Whether the user has the permissions to create databases     |
| usesuper        | Boolean                  | whether the user is the initial system administrator with the highest rights |
| usecatupd       | Boolean                  | whether the user can directly update system tables. Only the initial system administrator whose **usesysid** is **10** has this permission. It is unavailable for other users. |
| userepl         | Boolean                  | Whether the user has the permissions to duplicate data streams |
| passwd          | text                     | Encrypted user password. The value is displayed as \*\*\*\*\*\*\*\*. |
| valbegin        | timestamp with time zone | Start time for account validity (**null** if no start time)  |
| valuntil        | timestamp with time zone | End time for account validity (**null** if no end time)      |
| respool         | name                     | Resource pool where the user is in                           |
| parent          | oid                      | Parent user OID                                              |
| spacelimit      | text                     | Storage space of the permanent table                         |
| tempspacelimit  | text                     | Storage space of the temporary table                         |
| spillspacelimit | text                     | Operator disk flushing space                                 |
| useconfig       | text[]                   | Session defaults for runtime configuration variables         |
| nodegroup       | name                     | Name of the logical MogDB associated with the user. If no logical MogDB is associated, this column is left blank. |
