---
title: GS_WLM_USER_INFO
summary: GS_WLM_USER_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_USER_INFO

**GS_WLM_USER_INFO** displays user statistics.

**Table 1** GS_WLM_USER_INFO columns

| Name       | Type     | Description                           |
| :--------- | :------- | :------------------------------------ |
| userid     | oid      | OID of a user                         |
| username   | name     | Username                              |
| sysadmin   | boolean  | Whether the user is the administrator |
| rpoid      | oid      | OID of the associated resource pool   |
| respool    | name     | Name of the associated resource pool  |
| parentid   | oid      | OID of the user group                 |
| totalspace | bigint   | Available space limit of the user     |
| spacelimit | bigint   | User table space limit                |
| childcount | interger | Number of child users                 |
| childlist  | text     | Child user list                       |
