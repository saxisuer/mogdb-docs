---
title: PG_STATIO_ALL_SEQUENCES
summary: PG_STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_ALL_SEQUENCES

**PG_STATIO_ALL_SEQUENCES** contains the I/O statistics of each sequence in the current database.

**Table 1** PG_STATIO_ALL_SEQUENCES columns

| Name       | Type   | Description                                  |
| :--------- | :----- | :------------------------------------------- |
| relid      | oid    | OID of this sequence                         |
| schemaname | name   | Name of the schema where the sequence is in  |
| relname    | name   | Name of the sequence                         |
| blks_read  | bigint | Number of disk blocks read from the sequence |
| blks_hit   | bigint | Cache hits in the sequence                   |
