---
title: PG_TOTAL_USER_RESOURCE_INFO
summary: PG_TOTAL_USER_RESOURCE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_TOTAL_USER_RESOURCE_INFO

**PG_TOTAL_USER_RESOURCE_INFO** displays resource usage of all users. Only administrators can query this view. This view is valid only when use_workload_manager is set to **on**. I/O monitoring items are valid only when **enable_logical_io_statistics** is set to **on**.

**Table 1** PG_TOTAL_USER_RESOURCE_INFO columns

| Name              | Type             | Description                                                  |
| :---------------- | :--------------- | :----------------------------------------------------------- |
| username          | name             | Username                                                     |
| used_memory       | integer          | Used memory, in MB                                           |
| total_memory      | integer          | Available memory, in MB. The value **0** indicates that the available memory is not limited and depends on the maximum memory available in the database. |
| used_cpu          | double precision | Number of CPU cores in use. CPU usage data is collected only in complex jobs, and the value is the CPU usage of the related Cgroup. |
| total_cpu         | integer          | Total number of CPU cores of the Cgroup associated with the user on the node |
| used_space        | bigint           | Used permanent table storage space, in KB                    |
| total_space       | bigint           | Available permanent table storage space, in KB (**-1** if the storage space is not limited) |
| used_temp_space   | bigint           | Used temporary space, in KB                                  |
| total_temp_space  | bigint           | Total available temporary space, in KB (**-1** if the temporary space is not limited) |
| used_spill_space  | bigint           | Size of the used operator flushing space, in KB              |
| total_spill_space | bigint           | Total size of the available operator flushing space, in KB (**-1** if the space is not limited) |
| read_kbytes       | bigint           | Primary database node: total bytes read by the user's complex jobs on all database nodes in the last 5 seconds, in KB<br/>Database node: total bytes read by the user's complex jobs from the instance startup time to the current time, in KB |
| write_kbytes      | bigint           | Primary database node: total bytes written by the user's complex jobs on all database nodes in the last 5 seconds, in KB<br/>Database node: total bytes written by the user's complex jobs from the instance startup time to the current time, in KB |
| read_counts       | bigint           | Primary database node: total number of read times of the user's complex jobs on all database nodes in the last 5 seconds<br/>Database node: total number of read times of the user's complex jobs from the instance startup time to the current time |
| write_counts      | bigint           | Primary database node: total number of write times of the user's complex jobs on all database nodes in the last 5 seconds<br/>Database node: total number of write times of the user's complex jobs from the instance startup time to the current time |
| read_speed        | double precision | Primary database node: average read rate of the user's complex jobs on a single database node in the last 5 seconds, in KB/s<br/>Database node: average read rate of the user's complex jobs on the database node in the last 5 seconds, in KB/s |
| write_speed       | double precision | Primary database node: average write rate of the user's complex jobs on a single database node in the last 5 seconds, in KB/s<br/>Database node: average write rate of the user's complex jobs on the database node in the last 5 seconds, in KB/s |
