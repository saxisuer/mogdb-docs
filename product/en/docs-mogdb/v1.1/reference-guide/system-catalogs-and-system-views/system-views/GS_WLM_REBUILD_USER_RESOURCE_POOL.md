---
title: GS_WLM_REBUILD_USER_RESOURCE_POOL
summary: GS_WLM_REBUILD_USER_RESOURCE_POOL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_REBUILD_USER_RESOURCE_POOL

**GS_WLM_REBUILD_USER_RESOURCE_POOL** is used to rebuild a user's resource pool information in memory on the current connection node, with no output. This view is only used as a remedy when resource pool information is missing or misplaced.
