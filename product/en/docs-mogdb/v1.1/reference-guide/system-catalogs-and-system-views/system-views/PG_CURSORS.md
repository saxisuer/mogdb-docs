---
title: PG_CURSORS
summary: PG_CURSORS
author: Guo Huan
date: 2021-04-19
---

# PG_CURSORS

**PG_CURSORS** displays cursors that are currently available.

**Table 1** PG_CURSORS columns

| Name          | Type                     | Description                                                  |
| :------------ | :----------------------- | :----------------------------------------------------------- |
| name          | text                     | Cursor name                                                  |
| statement     | text                     | Query statement when the cursor is declared to change        |
| is_holdable   | Boolean                  | **True** if the cursor is holdable (it can be accessed after the transaction that declared the cursor has committed); **false** otherwise |
| is_binary     | Boolean                  | Whether the cursor was declared BINARY. If it was, the value is **true**. |
| is_scrollable | Boolean                  | Whether the cursor is scrollable (it allows rows to be retrieved in a nonsequential manner). If it is, the value is **true**. |
| creation_time | timestamp with time zone | Timestamp at which the cursor is declared                    |
