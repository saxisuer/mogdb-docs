---
title: GS_WLM_PLAN_OPERATOR_HISTORY
summary: GS_WLM_PLAN_OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_PLAN_OPERATOR_HISTORY

**GS_WLM_PLAN_OPERATOR_HISTORY** displays the execution plan operator-level records of the current user on the primary database node after the job is complete.

Columns in this view are the same as those in GS_WLM_PLAN_OPERATOR_INFO -> Table 1.
