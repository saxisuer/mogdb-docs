---
title: GS_WLM_OPERATOR_HISTORY
summary: GS_WLM_OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_OPERATOR_HISTORY

**GS_WLM_OPERATOR_HISTORY** displays records of operators in jobs that have been executed by the current user on the current primary database node.

Columns in this view are the same as those in GS_WLM_OPERATOR_INFO -> Table 1.
