---
title: PG_SESSION_WLMSTAT
summary: PG_SESSION_WLMSTAT
author: Guo Huan
date: 2021-04-19
---

# PG_SESSION_WLMSTAT

**PG_SESSION_WLMSTAT** displays corresponding load management information about the task currently executed by the user.

**Table 1** PG_SESSION_WLMSTAT columns

| Name             | Type    | Description                                                  |
| :--------------- | :------ | :----------------------------------------------------------- |
| datid            | oid     | OID of the database that the backend is connected to         |
| datname          | name    | Name of the database that the backend is connected to        |
| threadid         | bigint  | Thread ID of the backend                                     |
| sessionid        | bigint  | Session ID                                                   |
| processid        | integer | Process ID of the backend                                    |
| usesysid         | oid     | OID of the user logged in to the backend                     |
| appname          | text    | Name of the application connected to the backend             |
| usename          | name    | Name of the user logged in to the backend                    |
| priority         | bigint  | Priority of Cgroup where the statement is located            |
| attribute        | text    | Attributes of the statement:<br/>- **Ordinary**: default attribute of a statement before it is parsed by the database<br/>- **Simple**: simple statement<br/>- **Complicated**: complicated statement<br/>- **Internal**: internal statement of the database |
| block_time       | bigint  | Pending duration of the statement by now, in seconds         |
| elapsed_time     | bigint  | Actual execution duration of the statement by now, in seconds |
| total_cpu_time   | bigint  | Total CPU usage duration of the statement on the database node in the last period (unit: s) |
| cpu_skew_percent | integer | CPU usage skew percentage of the statement on the database node in the last period |
| statement_mem    | integer | **statement_mem** used for executing the statement (reserved column) |
| active_points    | integer | Number of concurrently active points occupied by the statement in the resource pool |
| dop_value        | integer | DOP value obtained by the statement from the resource pool   |
| control_group    | text    | Unsupported currently                                        |
| status           | text    | Status of the statement, including:<br/>- **pending**: waiting to be executed<br/>- **running**: being executed<br/>- **finished**: finished normally<br/>- **aborted**: terminated unexpectedly<br/>- **active**: normal status except for those above<br/>- **unknown**: unknown status |
| enqueue          | text    | Unsupported currently                                        |
| resource_pool    | name    | Current resource pool where the statements are located       |
| query            | text    | Latest query at the backend. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
| is_plana         | bool    | Unsupported currently                                        |
| node_group       | text    | Unsupported currently                                        |
