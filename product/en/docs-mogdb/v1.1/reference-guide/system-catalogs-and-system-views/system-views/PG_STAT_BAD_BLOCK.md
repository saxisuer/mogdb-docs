---
title: PG_STAT_BAD_BLOCK
summary: PG_STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_BAD_BLOCK

**PG_STAT_BAD_BLOCK** shows statistics about Page or CU verification failures after a node is started.

**Table 1** PG_STAT_BAD_BLOCK columns

| Name         | Type                     | Description                             |
| :----------- | :----------------------- | :-------------------------------------- |
| nodename     | text                     | Node name                               |
| databaseid   | integer                  | OID of a database                       |
| tablespaceid | integer                  | Tablespace OID                          |
| relfilenode  | integer                  | File object ID                          |
| bucketid     | smallint                 | ID of the bucket for consistent hashing |
| forknum      | integer                  | File type                               |
| error_count  | integer                  | Number of verification failures         |
| first_time   | timestamp with time zone | Time of the first verification failure  |
| last_time    | timestamp with time zone | Time of the latest verification failure |
