---
title: PG_EXT_STATS
summary: PG_EXT_STATS
author: Guo Huan
date: 2021-04-19
---

# PG_EXT_STATS

**PG_EXT_STATS** allows for access to extension statistics stored in the PG_STATISTIC_EXT system catalog. The extension statistics means multiple columns of statistics.

**Table 1** PG_EXT_STATS columns

| Name                   | Type       | Reference               | Description                                                  |
| :--------------------- | :--------- | :---------------------- | :----------------------------------------------------------- |
| schemaname             | name       | PG_NAMESPACE.nspname    | Name of the schema that contains a table                     |
| tablename              | name       | PG_CLASS.relname        | Table name                                                   |
| attname                | int2vector | PG_STATISTIC_EXT.stakey | Columns to be combined for collecting statistics             |
| inherited              | Boolean    | -                       | Includes inherited sub-columns if the value is **true**; otherwise, it indicates the column in a specified table. |
| null_frac              | real       | -                       | Percentage of column combinations that are null to all records |
| avg_width              | integer    | -                       | Average width of column combinations, in byte                |
| n_distinct             | real       | -                       | - Estimated number of distinct values in a column combination if the value is greater than 0<br/>- Negative of the number of distinct values divided by the number of rows if the value is less than 0<br/>    1. The negated form is used when **ANALYZE** believes that the number of distinct values is likely to increase as the table grows.<br/>    2. The positive form is used when the column seems to have a fixed number of possible values. For example, **-1** indicates that the number of distinct values is the same as the number of rows for a column combination.<br/>- The number of distinct values is unknown if the value is **0**. |
| n_dndistinct           | real       | -                       | Number of unique not-null data values in the **dn1** column combination<br/>- Exact number of distinct values if the value is greater than **0**.<br/>- Negative of the number of distinct values divided by the number of rows if the value is less than **0**. For example, if a value in a column combination appears twice in average, **n\_dndistinct** equals **-0.5**.<br/>- The number of distinct values is unknown if the value is **0**. |
| most_common_vals       | anyarray   | -                       | List of the most common values in a column combination. If this combination does not have the most common values, **most_common_vals** will be **NULL**. None of the most common values in **most_common_vals** is **NULL**. |
| most_common_freqs      | real[]     | -                       | List of the frequencies of the most common values, that is, the number of occurrences of each value divided by the total number of rows (**NULL** if **most_common_vals** is **NULL**) |
| most_common_vals_null  | anyarray   | -                       | List of the most common values in a column combination. If this combination does not have the most common values, **most\_common\_vals\_null** will be **NULL**. At least one of the common values in **most\_common\_vals\_null** is **NULL**. |
| most_common_freqs_null | real[]     | -                       | List of the frequencies of the most common values, that is, the number of occurrences of each value divided by the total number of rows (**NULL** if **most\_common\_vals\_null** is **NULL**) |
| histogram_bounds       | anyarray   | -                       | Boundary value list of the histogram                         |
