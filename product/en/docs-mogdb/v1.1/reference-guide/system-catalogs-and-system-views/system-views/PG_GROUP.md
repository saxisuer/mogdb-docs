---
title: PG_GROUP
summary: PG_GROUP
author: Guo Huan
date: 2021-04-19
---

# PG_GROUP

**PG_GROUP** displays the database role authentication and the relationship between roles.

**Table 1** PG_GROUP columns

| Name     | Type  | Description                                        |
| :------- | :---- | :------------------------------------------------- |
| groname  | name  | Group name                                         |
| grosysid | oid   | Group ID                                           |
| grolist  | oid[] | An array, including all the role IDs in this group |
