---
title: PG_STATIO_SYS_TABLES
summary: PG_STATIO_SYS_TABLES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_SYS_TABLES

**PG_STATIO_SYS_TABLES** shows I/O status information about all the system catalogs in the namespace.

**Table 1** PG_STATIO_SYS_TABLES columns

| Name            | Type   | Description                                                  |
| :-------------- | :----- | :----------------------------------------------------------- |
| relid           | oid    | Table OID                                                    |
| schemaname      | name   | Name of the schema the table is in                           |
| relname         | name   | Table name                                                   |
| heap_blks_read  | bigint | Number of disk blocks read from the table                    |
| heap_blks_hit   | bigint | Number of cache hits in the table                            |
| idx_blks_read   | bigint | Number of disk blocks read from all indexes in the table     |
| idx_blks_hit    | bigint | Number of cache hits in indexes in the table                 |
| toast_blks_read | bigint | Number of disk blocks read from the TOAST table (if any) in the table |
| toast_blks_hit  | bigint | Number of buffer hits in the TOAST table (if any) in the table |
| tidx_blks_read  | bigint | Number of disk blocks read from the TOAST table index (if any) in the table |
| tidx_blks_hit   | bigint | Number of buffer-hits in the TOAST table index (if any) in the table |
