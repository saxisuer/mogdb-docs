---
title: PLAN_TABLE
summary: PLAN_TABLE
author: Guo Huan
date: 2021-04-19
---

# PLAN_TABLE

**PLAN_TABLE** displays plan information collected by **EXPLAIN PLAN**. Plan information is in a session-level lifecycle. After a session exits, the data will be deleted. Data is isolated between sessions and between users.

**Table 1** PLAN_TABLE columns

| Name         | Type           | Description                                                  |
| :----------- | :------------- | :----------------------------------------------------------- |
| statement_id | varchar2(30)   | Query tag specified by a user                                |
| plan_id      | bigint         | Query ID                                                     |
| id           | int            | ID of each operator in a generated plan                      |
| operation    | varchar2(30)   | Operation description of an operator in a plan               |
| options      | varchar2(255)  | Operation action                                             |
| object_name  | name           | Object name corresponding to the operation, which is not the object alias used in the query. The object name is defined by users. |
| object_type  | varchar2(30)   | Object type                                                  |
| object_owner | name           | Schema to which the object belongs. It is defined by users.  |
| projection   | varchar2(4000) | Returned column information                                  |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - A valid **object_type** value consists of a relkind type defined in PG_CLASS (**TABLE**, **INDEX**, **SEQUENCE**, **VIEW**, **COMPOSITE TYPE**, or **TOASTVALUE TOAST**) and the rtekind type used in the plan (**SUBQUERY**, **JOIN**, **FUNCTION**, **VALUES**, **CTE**, or **REMOTE_QUERY**).
> - For RangeTableEntry (RTE), **object_owner** is the object description used in the plan. Non-user-defined objects do not have **object_owner**.
> - Information in the **statement_id**, **object_name**, **object_owner**, and **projection** columns is stored in letter cases specified by users and information in other columns is stored in uppercase.
> - **PLAN_TABLE** supports only **SELECT** and **DELETE** and does not support other DML operations.
