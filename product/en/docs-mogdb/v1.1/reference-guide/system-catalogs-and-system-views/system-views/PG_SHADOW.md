---
title: PG_SHADOW
summary: PG_SHADOW
author: Guo Huan
date: 2021-04-19
---

# PG_SHADOW

**PG_SHADOW** displays properties of all roles that are marked as **rolcanlogin** in **PG_AUTHID**.

The name stems from the fact that this view should not be readable by the public since it contains passwords. PG_USER is a publicly readable view on **PG_SHADOW** that blanks out the password column.

**Table 1** PG_SHADOW columns

| Name            | Type                     | Reference         | Description                                                  |
| :-------------- | :----------------------- | :---------------- | :----------------------------------------------------------- |
| usename         | name                     | PG_AUTHID.rolname | Username                                                     |
| usesysid        | oid                      | PG_AUTHID.oid     | ID of this user                                              |
| usecreatedb     | Boolean                  | -                 | Indicates that the user can create databases.                |
| usesuper        | Boolean                  | -                 | Indicates that the user is an administrator.                 |
| usecatupd       | Boolean                  | -                 | Users can update a view. Even the system administrator cannot do this unless this column is **true**. |
| userepl         | Boolean                  | -                 | User can initiate streaming replication and put the system in and out of backup mode. |
| passwd          | text                     | -                 | Password (possibly encrypted); **null** if none. See PG_AUTHID for details about how encrypted passwords are stored. |
| valbegin        | timestamp with time zone | -                 | Account validity period start time (**null** if no start time) |
| valuntil        | timestamp with time zone | -                 | Password expiry time (**null** if no expiration)             |
| respool         | name                     | -                 | Resource pool used by the user                               |
| parent          | oid                      | -                 | Parent resource pool                                         |
| spacelimit      | text                     | -                 | Storage space of the permanent table                         |
| tempspacelimit  | text                     | -                 | Storage space of the temporary table                         |
| spillspacelimit | text                     | -                 | Operator disk flushing space                                 |
