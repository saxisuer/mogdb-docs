---
title: GS_OS_RUN_INFO
summary: GS_OS_RUN_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_OS_RUN_INFO

**GS_OS_RUN_INFO** displays the running status of the OS.

**Table 1** GS_OS_RUN_INFO columns

| Name       | Type    | Description                                              |
| :--------- | :------ | :------------------------------------------------------- |
| id         | integer | ID                                                       |
| name       | text    | Name of the OS running status                            |
| value      | numeric | Value of the OS running status.                          |
| comments   | text    | Remarks of the OS running status                         |
| cumulative | Boolean | Whether the value of the OS running status is cumulative |
