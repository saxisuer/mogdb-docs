---
title: PG_TOTAL_USER_RESOURCE_INFO_OID
summary: PG_TOTAL_USER_RESOURCE_INFO_OID
author: Guo Huan
date: 2021-04-19
---

# PG_TOTAL_USER_RESOURCE_INFO_OID

**PG_TOTAL_USER_RESOURCE_INFO_OID** displays resource usage of all users. Only administrators can query this view. This view is valid only when use_workload_manager is set to **on**.

**Table 1** PG_TOTAL_USER_RESOURCE_INFO_OID columns

| Name              | Type             | Description                                                  |
| :---------------- | :--------------- | :----------------------------------------------------------- |
| userid            | oid              | User ID                                                      |
| used_memory       | integer          | Used memory, in MB                                           |
| total_memory      | integer          | Available memory, in MB. The value **0** indicates that the available memory is not limited and depends on the maximum memory available in the database. |
| used_cpu          | double precision | Number of CPU cores in use                                   |
| total_cpu         | integer          | Total number of CPU cores of the Cgroup associated with the user on the node |
| used_space        | bigint           | Used storage space, in KB                                    |
| total_space       | bigint           | Available storage space, in KB. The value **-1** indicates that the space is not limited. |
| used_temp_space   | bigint           | Used temporary storage space, in KB                          |
| total_temp_space  | bigint           | Total available temporary space, in KB. The value **-1** indicates that the space is not limited. |
| used_spill_space  | bigint           | Used disk space for spilling, in KB                          |
| total_spill_space | bigint           | Total available disk space for spilling, in KB. The value **-1** indicates that the space is not limited. |
| read_kbytes       | bigint           | The number of bytes read during the monitoring interval, in KB/s. |
| write_kbytes      | bigint           | The number of bytes written during the monitoring interval, in KB/s. |
| read_counts       | bigint           | Number of read operations during the monitoring interval.    |
| write_counts      | bigint           | Number of write operations during the monitoring interval.   |
| read_speed        | double precision | Byte rate of read operations, in b/s.                        |
| write_speed       | double precision | Byte rate of write operations, in b/s.                       |
