---
title: GS_WLM_SESSION_INFO_ALL
summary: GS_WLM_SESSION_INFO_ALL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_SESSION_INFO_ALL

**GS_WLM_SESSION_INFO_ALL** displays load management information about a completed job executed on the primary database node.

**Table 1** GS_WLM_SESSION_INFO_ALL columns

| 名称                | 类型     | 描述                                                        |
| :------------------ | :------- | :---------------------------------------------------------- |
| userid              | oid      | OID of a user                                               |
| username            | name     | Username                                                    |
| sysadmin            | boolean  | Whether the user is the administrator                       |
| rpoid               | oid      | OID of the associated resource pool                         |
| respool             | name     | Name of the associated resource pool                        |
| parentid            | oid      | OID of the user group                                       |
| totalspace          | bigint   | Available space limit of the user                           |
| spacelimit          | bigint   | User table space limit                                      |
| childcount          | interger | Number of child users                                       |
| childlist           | text     | Child user list                                             |
| n_returned_rows     | bigint   | The number of rows in the result set returned by **SELECT** |
| n_tuples_fetched    | bigint   | Random scan row                                             |
| n_tuples_returned   | bigint   | Sequential scan row                                         |
| n_tuples_inserted   | bigint   | Insert row                                                  |
| n_tuples_updated    | bigint   | Update row                                                  |
| n_tuples_deleted    | bigint   | Delete row                                                  |
| n_blocks_fetched    | bigint   | The number of block accesses to the buffer                  |
| n_blocks_hit        | bigint   | The number of block hits for the buffer                     |
| db_time             | bigint   | Effective DB time spent, multiple threads will accumulate   |
| cpu_time            | bigint   | CPU time spent (in microseconds)                            |
| execution_time      | bigint   | time spent within executors (in microseconds)               |
| parse_time          | bigint   | time spent on parsing SQL statements (in microseconds)      |
| plan_time           | bigint   | time spent on generating plans (in microseconds)            |
| rewrite_time        | bigint   | time spent on rewriting SQL statements (in microseconds)    |
| pl_execution_time   | bigint   | execution time of the PL/pgSQL (in microseconds)            |
| pl_compilation_time | bigint   | compilation time of the PL/pgSQL (in microseconds)          |
| net_send_time       | bigint   | time spent on the network (in microseconds)                 |
| data_io_time        | bigint   | I/O time spent (in microseconds)                            |
| is_slow_query       | bigint   | Whether it is a slow SQL record                             |
