---
title: PG_GET_INVALID_BACKENDS
summary: PG_GET_INVALID_BACKENDS
author: Guo Huan
date: 2021-04-19
---

# PG_GET_INVALID_BACKENDS

**PG_GET_INVALID_BACKENDS** provides information about background threads on the primary database node that are connected to the current standby server.

**Table 1** PG_GET_INVALID_BACKENDS columns

| Name          | Type                     | Description                                         |
| :------------ | :----------------------- | :-------------------------------------------------- |
| pid           | bigint                   | Thread ID                                           |
| node_name     | text                     | Node information connected to the background thread |
| dbname        | name                     | Name of the connected database                      |
| backend_start | timestamp with time zone | Background thread startup time                      |
| query         | text                     | Query statement executed by the background thread   |
