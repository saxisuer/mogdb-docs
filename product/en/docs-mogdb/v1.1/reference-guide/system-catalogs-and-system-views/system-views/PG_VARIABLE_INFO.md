---
title: PG_VARIABLE_INFO
summary: PG_VARIABLE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_VARIABLE_INFO

**PGXC_VARIABLE_INFO** records information about transaction IDs and OIDs of the current node in MogDB.

**Table 1** PG_VARIABLE_INFO columns

| Name                  | Type | Description                                                  |
| :-------------------- | :--- | :----------------------------------------------------------- |
| node_name             | text | Node name                                                    |
| nextOid               | oid  | OID generated next time for the node                         |
| nextXid               | xid  | Transaction ID generated next time for the node              |
| oldestXid             | xid  | Oldest transaction ID on the node                            |
| xidVacLimit           | xid  | Critical point (transaction ID) that triggers forcible autovacuum |
| oldestXidDB           | oid  | OID of the database that has the minimum datafrozenxid on the node |
| lastExtendCSNLogpage  | xid  | Number of the last extended csnlog page                      |
| startExtendCSNLogpage | xid  | Number of the page from which csnlog extending starts        |
| nextCommitSeqNo       | xid  | CSN generated next time for the node                         |
| latestCompletedXid    | xid  | Latest transaction ID on the node after the transaction commission or rollback |
| startupMaxXid         | xid  | Last transaction ID before the node is powered off           |
