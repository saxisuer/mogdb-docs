---
title: GS_SESSION_MEMORY
summary: GS_SESSION_MEMORY
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_MEMORY

**GS_SESSION_MEMORY** collects statistics about memory usage at the session level, including all the memory allocated to Postgres and Stream threads on DNs for jobs currently executed by users.

**Table 1** GS_SESSION_MEMORY columns

| Name     | Type    | Description                                                  |
| :------- | :------ | :----------------------------------------------------------- |
| sessid   | text    | Thread start time and ID                                     |
| init_mem | integer | Memory allocated to the currently executed jobs before they enter the executor, in MB |
| used_mem | integer | Memory allocated to the currently executed jobs, in MB       |
| peak_mem | integer | Peak memory allocated to the currently executed jobs, in MB  |
