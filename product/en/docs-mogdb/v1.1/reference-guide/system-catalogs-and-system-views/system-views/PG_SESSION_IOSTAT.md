---
title: PG_SESSION_IOSTAT
summary: PG_SESSION_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# PG_SESSION_IOSTAT

**PG_SESSION_IOSTAT** shows I/O load management information about the task currently executed by the user.

IOPS is counted by ones for column-storage and by 10 thousands for row storage.

**Table 1** PG_SESSION_IOSTAT columns

| Name        | Type    | Description                                               |
| :---------- | :------ | :-------------------------------------------------------- |
| query_id    | bigint  | Job ID                                                    |
| mincurriops | integer | Minimum I/O of the job across database nodes              |
| maxcurriops | integer | Maximum I/O of the job across database nodes              |
| minpeakiops | integer | Minimum peak I/O of the current job across database nodes |
| maxpeakiops | integer | Maximum peak I/O of the current job across database nodes |
| io_limits   | integer | **io_limits** set for the job                             |
| io_priority | text    | **io_priority** set for the job                           |
| query       | text    | Job                                                       |
| node_group  | text    | Unsupported currently                                     |
