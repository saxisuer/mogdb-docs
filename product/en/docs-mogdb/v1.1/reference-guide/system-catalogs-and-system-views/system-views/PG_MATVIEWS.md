---
title: PG_MATVIEWS
summary: PG_MATVIEWS
author: Guo Huan
date: 2021-04-19
---

# PG_MATVIEWS

**PG_MATVIEWS** provides information about each materialized view in the database.

**Table 1** PG_MATVIEWS columns

| Name         | Type    | Reference             | Description                                                  |
| :----------- | :------ | :-------------------- | :----------------------------------------------------------- |
| schemaname   | name    | PG_NAMESPACE.nspname  | Name of the schema of a materialized view.                   |
| matviewname  | name    | PG_CLASS.relname      | Name of a materialized view.                                 |
| matviewowner | name    | PG_AUTHID.Erolname    | Owner of the materialized view.                              |
| tablespace   | name    | PG_TABLESPACE.spcname | Tablespace name of the materialized view. If the default tablespace of the database is used, the value is null. |
| hasindexes   | boolean |                       | This column is true if the materialized view has (or has recently had) any indexes. |
| definition   | text    | -                     | Definition of a materialized view (a reconstructed SELECT query). |
