---
title: Roadmap
summary: Roadmap
author: Guo Huan
date: 2021-04-14
---

# Roadmap

This document introduces the roadmap for MogDB 2.0.0. MogDB 2.0.0 retains the features of the previous version and will add the following new features:

## MogDB Server

- Support setting the primary node to global read-only, which can prevent brain-split in the case of network isolation
- Support for pg trgm plugin (full-text indexing plugin)
- Support for custom operators and custom operator classes
- Reduce resource consumption for database metrics collection
- Optimize partition creation syntax
- Copy and add functions to view xlog related information when the receiver thread is not started
- Support sorting of CJK (East Asian character set), with better performance for sorting large data volumes
- Support for xid-based flashback queries
- Support session settings of **Show** command for obfuscation query

## MogDB Client

- Go client: support for the SHA256 encryption algorithm
- Python client: support for the SHA256 encryption algorithm and support for automatic identification of the primary node
- C client: support for automatic identification of the primary node
- ODBC client: support for automatic identification of the primary node

## MogHA

- Support for automatic restart of primary instance
- Support for database CPU usage limit setting
- Support for customization of HA switching
- Support for WEB interface, API and so on, facilitating small-scale deployment (EA phase)

## MogDB plugin

- Compatibility support for **pg_repack** plug-in used for online data file space reclamation
- Compatibility support for **wal2json** plug-in used for the export and heterogeneous replication of the current logical log
- Support for **pg_dirtyread** plug-in used for data recovery and query under special circumstances
- Support for **walminer** plug-in used for online WAL log parsing (no reliance on logical logs)
- Support for **db_link** plug-in used for connecting to PostgreSQL databases from MogDB

## MogDB toolkit

- **gs_xlogdump**, used for offline export of WAL logs
- **gs_filedump**, used for offline export of data files
