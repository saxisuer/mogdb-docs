---
title: Feature Introduction
summary: Feature Introduction
author: Zhang Cuiping
date: 2021-04-02
---

# Feature Introduction

- Standard SQLs

  Supports SQL92, SQL99, SQL2003, and SQL2011 standards, GBK and UTF-8 character sets, SQL standard functions and analytic functions, and SQL Procedural Language.

- Database storage management

  Supports tablespaces where different tables can be stored in different locations.

- Primary/standby deployment

  Supports the ACID properties, single-node fault recoveries, primary/standby data synchronization, and primary/standby switchover.

- APIs

  Supports standard JDBC 4.0 and ODBC 3.5 features.

- Management tools

  Provides installation and deployment tools, instance start and stop tools, and backup and restoration tools.

- Security management

  Supports SSL network connections, user permission management, password management, security auditing, and other functions, to ensure data security at the management, application, system, and network layers.

- AI

  Supports parameter self-tuning, slow SQL discovery, single query index recommendation, virtual index, workload index recommendation, database metric collection, forecasting, and exception monitoring; compatible with the MADlib ecosystem and supports high-performance AI algorithms in the library.

- Supports LIST partition and HASH partition.

  - The list partitioning function divides the key values in the records to be inserted into a table into multiple lists (the lists do not overlap in different partitions) based on a column of the table, and then creates a partition for each list to store the corresponding data.
  - The hash partitioning function uses the internal hash algorithm to divide records to be inserted into a table into partitions based on a column of the table. If you specify the **PARTITION** parameter when running the **CREATE TABLE** statement, data in the table will be partitioned.

- Supports equality query in a fully-encrypted database

  Encrypted database is a database system which is designed to deal with encrypted data. The data is stored in the database server in envrypted form. The database supports the retrieval and calculation of ciphertext data, and the lexical parsing, syntax parsing, execution plan generation, transaction consistency guarantee and storage related to query tasks inherit the original database capabilities. The performance is deteriorated by less than 10% compared with unencrypted operations.

- Enhances primary/standby HA.

  - Supports the cascaded standby node which replicates logs from the standby node to reduce the service processing pressure of the primary node.
  - Scales the number of standby nodes to 8 nodes.
  - Supports the **catchup2normal_wait_time** parameter configuration. After the standby node starts and establishes a connection with the primary node, it is in log catchup mode. If the difference between logs that are caught up is less than the value of **catchup2normal_wait_time**, the standby node is changed to synchronous mode.

- Supports non-synchronization of configuration files. The configuration parameters of primary and standby nodes vary according to the specifications of hardware where nodes are deployed. Therefore, the original synchronization function is modified and you do not have to synchronize parameter configuration files between the primary and standby nodes.

- Expands the data type compatibility.

  Both char and varchar are compatible with PostgreSQL mode. When the length is calculated, the length of the character instead of the length of the byte is returned.

- Adds monitoring dimensions.

  Monitors the **sort&hash** information about **work_mem** in the view returned by **get_instr_unique_sql()**.

  Adds the **WAIT_EVENT_WAL_BUFFER_ACCESS** and **WAIT_EVENT_WAL_BUFFER_FULL** wait events to the **get_instr_wait_event** view to monitor **wal_buffer**. **WAIT_EVENT_WAL_BUFFER_ACCESS** counts the number of times that the WAL buffer is accessed. (In consideration of performance, the access duration is not measured.) **WAIT_EVENT_WAL_BUFFER_FULL** collects statistics on the number of access times and access duration when the WAL buffer is full.

- Supports AI4DB: intelligent O&M.

  - Index recommendation: automatically recommends proper indexes for the SELECT query based on the access conditions of SQL statements; Provides virtual indexes for users to view the execution plan without really creating an index. Recommends workload-level indexes based on the workload information provided by users.
  - Users can deploy the database information collection platform in one-click mode, forecast the future trend based on the collected data, and detect exceptions in advance.

- Supports DB4AI: AI algorithm in the library.

  Compatible with the MADlib ecosystem, supports more than 70 algorithms, and provides better performance than the native PostgreSQL. Adds common algorithm suites XGBoost, prophet, GBDT, and hierarchical clustering to supplement the shortcomings of the MADlib ecosystem.

- Supports PL/Python.

  Supports the Python language as the SQL programming language.

- **gs_basebackup** supports standby node backup.

  **gs_basebackup** can back up data from the standby node to reduce the service processing pressure of the primary node.

- Refines permission management models.

  Supports DDL permission granting and revoking.

- Rebuilds autonomous transactions.

  Rebuilds the inter-process communication method used by the original autonomous transaction implementation to the inter-thread communication method, which is simpler.

- Rebuilds parallel queries.

  Rebuilds a unified parallel framework to replace the original parallel query framework and the distributed cross-node parallel query framework.

- Supports the sysdate data type.

  Sysdate returns the current date and time, which are the time zone and time of the Linux OS on a host machine where the database is located.

- Supports standby node adding or deleting.

  Provides the OM tool to support online scale-out and scale-in of standby nodes which can be dynamically added or deleted without affecting services.

- Supports multiple Python versions.

  On CentOS, the database installation depends on Python 3.6. The released 1.1.0 version can be installed in Python 3.7. You can also build third-party libraries based on the specified Python 3.* version to adapt to more Python versions.

- Supports adding an index online.

  Uses the **create index concurrently** syntax to create indexes online without blocking DML.

- Provides the upgrade tool.

  Supports the upgrade from 1.0 to 2.0.

  Note:

  The following parameters in the 1.0 version are automatically deleted from the 2.0 version during the upgrade. Evaluate the impact before the upgrade.

  | enable_beta_nestloop_fusion     | enable_upsert_to_merge       | force_parallel_mode  | gs_clean_timeout              | max_background_workers   |
  | ------------------------------- | ---------------------------- | -------------------- | ----------------------------- | ------------------------ |
  | max_parallel_workers_per_gather | min_parallel_table_scan_size | pagewriter_threshold | parallel_leader_participation | parallel_setup_cost      |
  | parallel_tuple_cost             | parctl_min_cost              | tcp_recv_timeout     | transaction_sync_naptime      | transaction_sync_timeout |
  | twophase_clean_workers          | wal_compression              | -                    | -                             | -                        |

- Decouples installation from the OM tool.

  The OM tool is decoupled from the database kernel in the 1.0 version:

  1. The OM tool is stored in the openGauss-OM repository which will be used to manage the OM tool code.
  2. The OM tool and kernel are packaged separately. You can place the images of the OM tool and kernel in the same directory and use the OM tool to install them. The installation method remains unchanged. If only the kernel is concerned, you can decompress the kernel image and run it separately.

- Supports delayed standby databases

  Delays the time for a standby node to replay XLOG records, compared with the primary node.

- Supports logical replication of standby nodes

  Supports logical decoding on a standby node,this can reduce host pressure

- Provides enhanced capacity expansion tool

  Optimizes the scale-out tool to support online scale-out without interrupting services and allows the standby node to be scaled out as a cascaded standby node.

- Supports gray upgrade

  Optimizes the upgrade tool and support business online upgrade. Currently, only the gray upgrade from version 1.0 to 2.0 is supported.

- Backup machine IO write amplification optimization

  Optimize the IO of the standby machine, smooth the IO volume of the standby machine checkpoint brushing, and solve the problem that the large amount of standby machine IO affects the query performance.

- WDR diagnostic report adds database operation indicators

  Adds four database running metrics: Effective CPU, WalWrite NoWait, Soft Parse, and Non-Parse CPU.

- Features of the Data Studio client tool

  Data Studio supports multiple features of the openGauss kernel. The details are as follows:

  - The pldebugger debugging function is added.
  - The rollback of the pldebugger debugging function is added. Before using Data Studio for debugging, add an option to ensure that the debugging function is rolled back after data is modified.
  - The XML and serial types are supported. Columns are added to the table. The column type can be XML or serial(big|normal|small).
  - Foreign table objects can be created and displayed in Data Studio.
  - The partial_cluster_key constraint of column-store tables is supported.
  - Global temporary tables support DDL display and export.
  - The LOCAL and GLOBAL flags are supported when a partitioned table is created.
  - The MOT table is displayed.
