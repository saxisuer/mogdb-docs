---
title: Comparison Between MogDB and openGauss
summary: Comparison Between MogDB and openGauss
author: Guo Huan
date: 2021-04-02
---

# Comparison Between MogDB and openGauss

## Relationship Between MogDB and openGauss

MogDB is an enterprise database that is packaged and improved on the basis of openGauss open source kernel and it is more friendly to enterprise applications. On the basis of openGauss kernel, MogDB adds the MogHA component for automatic management and high availability under the primary-standby architecture, which is crucial for enterprise applications. At the same time, MogDB Manager includes backup and recovery, monitoring, automatic installation and other components for enterprise-level ease of use requirements.

MogDB is a commercial product that is sold according to an established license pricing system and is supported by Enmo's professional services.

## Introduction to openGauss

openGauss is an open source relational database management system. The kernel of openGauss is derived from PostgreSQL and is distributed under Mulan PSL v2. openGauss kernel is open source, anyone and any organization can download the source code for compilation and installation without any cost; The openGauss community also regularly releases compiled binary installation files, and the current release strategy is to release one stable frequently supported version per year (end of March each year) and one radical version with new features (end of September each year).

openGauss is a standalone database. It has the basic features of relational databases as well as enhanced features.

For more details, please visit openGauss official website: <https://opengauss.org>

### Basic Features

- Standard SQLs

  Supports SQL92, SQL99, SQL2003, and SQL2011 standards, GBK and UTF-8 character sets, SQL standard functions and analytic functions, and SQL Procedural Language.

- Database storage management

  Supports tablespaces where different tables can be stored in different locations.

- Primary/standby deployment

  Supports the ACID properties, single-node fault recoveries, primary/standby data synchronization, and primary/standby switchover.

- APIs

  Supports standard JDBC 4.0 and ODBC 3.5.

- Management tools

  Provides installation and deployment tools, instance start and stop tools, and backup and restoration tools.

- Security management

  Supports SSL network connections, user permission management, password management, security auditing, and other functions, to ensure data security at the management, application, system, and network layers.

### Enhanced Features

- Data Partitioning

  Data partitioning is a general function for most database products. In openGauss, data is partitioned horizontally with a user-specified policy. This operation splits a table into multiple partitions that are not overlapped.

- Vectorized Executor and Hybrid Row-Column Storage Engine

  In a wide table containing a huge amount of data, a query usually only involves certain columns. In this case, the query performance of the row-store engine is poor. For example, a single table containing the data of a meteorological agency has 200 to 800 columns. Among these columns, only ten of them are frequently accessed. In this case, a vectorized executor and column-store engine can significantly improve performance by saving storage space.

- High Availability (HA) Transaction Processing

  openGauss manages transactions and guarantees the ACID properties. openGauss provides a primary/standby HA mechanism to reduce the service interruption time when the primary node is faulty. It protects key user programs and continuously provides external services, minimizing the impact of hardware, software, and human faults on services to ensure service continuity.

- High Concurrency and High Performance

  openGauss supports 10,000 concurrent connections through server thread pools. It supports thread nucleophilicity and millions of tpmC using the NUMA-based kernel data structure, manages TB-level large memory buffers through efficient hot and cold data elimination, achieves multi-version access without read/write blocks using CSN-based snapshots, and avoids performance fluctuation caused by full-page writes using incremental checkpoints.

- SQL Self-Diagnosis

  To locate performance issues of a query, you can use **EXPLAIN PERFORMANCE** to query its execution plan. However, this method produces many logs, requires to modify service logic, and depends on expertise to locate problems. SQL self-diagnosis enables users to locate performance issues more efficiently.

- Equality Query in a Fully-encrypted Database

    The encrypted database allows the client to encrypt sensitive data within the client application. During the query period, the entire service data flow exists in the form of ciphertext during data processing. It has the following advantages:

  - Protects data privacy and security throughout the lifecycle on the cloud.
  - Resolves trust issues by making the public cloud, consumer cloud, and development users keep their own keys.
  - Enables partners to better comply with personal privacy protection laws and regulations with the help of the full encryption capability.

- Memory Table

  With memory tables, all data access is lock-free and concurrent, optimizing data processing and meeting real-time requirements.

- Primary/Standby Deployment

  The primary/standby deployment mode supports synchronous and asynchronous replication. Applications are deployed based on service scenarios. For synchronous replication, one primary node and two standby nodes are deployed. This ensures reliability but affects performance. For asynchronous replication, one primary node and one standby node are deployed. This has little impact on performance, but data may be lost when exceptions occur. openGauss supports automatic recovery of damaged pages. When a page on the primary node is damaged, the damaged page can be automatically recovered on the standby node. Besides, openGauss supports concurrent log recovery on the standby node to minimize the service unavailability time when the primary node is down.

  In addition, in primary/standby deployment mode, if the read function of the standby node is enabled, the standby node supports read operations instead of write operations (such as table creation, data insertion, and data deletion), reducing the pressure on the primary node.

- AI Capabilities

  - Automatic parameter optimization
  - Slow SQL discovery
  - Index recommendation
  - Time series prediction and exception detection
  - DB4AI function
  - SQL execution time prediction
  - Database monitoring

- Logical Log Replication

  In logical replication, the primary database is called the source database, and the standby database is called the target database. The source database parses the WAL file based on the specified logical parsing rules and parses the DML operations into certain logical change information (standard SQL statements). The source database sends standard SQL statements to the target database. After receiving the SQL statements, the target database applies them to implement data synchronization. Logical replication involves only DML operations. Logical replication can implement cross-version replication, heterogeneous database replication, dual-write database replication, and table-level replication.

- Automatic WDR Performance Analysis Report

  Periodically and proactively analyzes run logs and WDR reports (which are automatically generated in the background and can be triggered by key indicator thresholds such as the CPU usage, memory usage, and long SQL statement proportions) and generates reports in HTML and PDF formats. The performance report can be automatically generated. The WDR generates a performance report between two different time points based on the system performance snapshot data at two different time points. The report is used to diagnose database kernel performance faults.

- Incremental Backup and Restoration (beta)

  Supports full backup and incremental backup of the database, manages backup data, and views the backup status. Supports combination of incremental backups and deletion of expired backups. The database server dynamically tracks page changes, and when a relational page is updated, the page is marked for backup. The incremental backup function requires that the GUC parameter enable_cbm_tracking be enabled to allow the server to track the modification page.

- Point-In-Time Recovery

  Point-in-time recovery (PITR) uses basic hot backup, write-ahead logs (WALs), and archived WALs for backup and recovery. Replaying a WAL record can be stopped at any point of time, so that there is a consistent snapshot of the database at any point of time. That is, you can restore the database to the state at any time since the backup starts. During recovery, you can specify a recovery stop point with a terminal ID (TID), time, and license serial number (LSN).

## Advantages of MogDB

openGauss is a standalone database where data is stored on a single physical node and data access tasks are pushed to service nodes. In this way, high concurrency of servers enables quick data processing. In addition, data can be copied to the standby server through log replication, ensuring high reliability and scalability.

openGauss is a stand-alone database. To use openGauss in formal commercial projects, you need to build complete tool chain capabilities such as database monitoring and primary/standby switchover.

At the product level, MogDB adds MogHA enterprise-class high availability components and feature-rich graphical management tool MogDB Manager to the original functions of openGauss, and continuously enhances the openGauss kernel along the established route. MogDB can maximize the high-availability deployment capabilities of multiple machine rooms, and can reach 2.5 million tpmC on 4-CPU server. MogDB Manager contains a variety of practical components, such as MTK database migration, MIT performance monitoring, RWT performance pressure testing, PTK automated deployment, etc., which greatly makes up the shortcomings of openGauss open source database and enriches various enterprise-class functions.

At the service level, Enmo has decades of experience in database operation and maintenance, and can provide complete services to ensure a more stable database, smoother application transformation, and less risk, making up for the disadvantages of openGauss open source database human operation and maintenance shortage, while reducing maintenance costs.
