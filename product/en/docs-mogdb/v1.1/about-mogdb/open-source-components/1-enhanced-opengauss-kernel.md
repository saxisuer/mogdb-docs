---
title: Enhanced openGauss Kernel
summary: Enhanced openGauss Kernel
author: Liuxu
date: 2021-06-09
---

# Enhanced openGauss Kernel

<br/>

MogDB is an enterprise-level database which is more friendly to enterprise applications, encapsulated and improved on the basis of openGauss open source kernel. Based on the openGauss kernel, MogDB adds MogHA, TimeSeries (for IoT scenarios), Distributed (realize each data node as an openGauss kernel distributed environment), Oracle view compatibility (DBA and V$ views) and other plug-ins. The MogHA plug-in is used for automated HA management in the primary/standby architecture, which is crucial for enterprise applications. At the same time, MogDB Manager management software is developed, which includes backup and recovery, performance monitoring, health check toolkit, automated deployment, database migration, data synchronization, compatibility analysis, performance pressure test and data recovery from a corrupted database for a variety of enterprise-level usability requirements.
