---
title: MogDB Network Exception Testing
summary: MogDB Network Exception Testing
author: Liu Xu
date: 2021-03-04
---

# MogDB Network Exception Testing (Need to Be Reviewed Later)

## Test Scope

- The service NIC of the primary node is abnormal.

- The heartbeat NIC of the primary node is abnormal.

- Both the service and heartbeat NICs of the primary node are abnormal.

- The service NIC of the standby node is abnormal.

- The heartbeat NIC of the standby node is abnormal.

- The VIP of the primary node is disconnected.

## Test Environment

| Category    |    Server Configuration     | Client Configuration | Quantity |
| ----------- | :-------------------------: | :------------------: | :------: |
| CPU         |         Kunpeng 920         |     Kunpeng 920      |   128    |
| Memory      |        DDR4,2933MT/s        |    DDR4,2933MT/s     | 2048 GB  |
| Hard disk   |          Nvme 3.5T          |       Nvme 3T        |    4     |
| File system |             Xfs             |         Xfs          |    4     |
| OS          |    openEuler 20.03 (LTS)    |      Kylin V10       |          |
| Database    | MogDB1.1.0 software package |                      |          |
| Test tool   |           pgbench           |                      |          |

## The Service NIC of the Primary Database Node Is Abnormal

Test solution:

1. Disable the heartbeat NIC and then enable it.
2. Observe the database status.

Test procedure:

1. Record the original cluster status.

2. Disable the heartbeat NIC and then enable it.

3. Observe the status of the standby database node.

   The standby database node cannot be connected to the primary database node.

4. Query the cluster status.

   No primary/standby switchover occurs.

Test result:

1. No primary/standby switchover occurs.
2. When the network of the primary database node restores, the status of the standby database node becomes normal.
3. When the service NIC of the primary database node is abnormal, TPS decreases from 9000 to 0. After 15s when the service NIC is enabled, TPS increases from 0 to 9000.

## The Heartbeat NIC of the Primary Database Node Is Abnormal

Test solution:

1. Disable the heartbeat NIC and then enable it.
2. Observe the database status.

Test procedure:

1. Observe the database status.

2. Disable the heartbeat NIC and then enable it.

3. Observe the database status.

   No primary/standby switchover occurs.

4. Observe the cluster status.

Test result:

1. No primary/standby switchover occurs.

2. TPS does not change.

## Both the Service and Heartbeat NICs of the Primary Database Node Are Abnormal

Test solution:

1. Disable the heartbeat NIC and then enable it.

2. Observe the database status.

Test procedure:

1. Observe the original database status.

2. Disable the heartbeat NIC and then enable it.

   The primary database node is normal.

Test result:

1. No primary/standby switchover occurs.

2. When the service NIC is disconnected, TPS drops from 9000 to 0. After the service NIC is restored to normal, TPS increases to 9000.

## The Service NIC of the Standby Database Node Is Abnormal

Test solution:

1. Disable the heartbeat NIC and then enable it.

2. Observe the database status.

Test procedure:

1. Record the original cluster status.

2. Disable the heartbeat NIC and then enable it.

3. Observe the status of the primary database node.

4. Ping the IP address of the service NIC of the standby database node.

   The IP address cannot be pinged.

   No primary/standby switchover occurs.

Test result:

1. The monitor script of the standby database node reports the heartbeat error indicating that no primary/standby switchover occurs.

2. When the service NIC of the standby database node is abnormal, TPS of the primary database node increases to 13000. After the service NIC of the standby database node is restored to normal, TPS of the primary database node is restored to 9000.

## The Heartbeat NIC of the Standby Database Node Is Abnormal

Test solution:

1. Disable the heartbeat NIC 60s and then enable it.

2. Observe the database status.

Test procedure:

1. Observe the original database status.

2. Disable the heartbeat NIC 60s and then enable it.

3. After the script is executed, observe the database status.
   No primary/standby switchover occurs. The monitor script of the primary database node reports an error that the IP address of the service NIC of the standby database node cannot be pinged.

Test result:

No primary/standby switchover occurs.

## The VIP of the Primary Database Node Is Disconnected

Test solution:

Disable the NIC **bond0:1** and then enable it.

Test procedure:

1. Disable the NIC **bond0:1** and then enable it.

2. Observe the database status.

3. Observe the VIP of the primary database node.

Test result:

1. The VIP of the primary database node is automatically connected.

2. When the VIP is disconnected and then connected, the TPS status is restored to normal.
