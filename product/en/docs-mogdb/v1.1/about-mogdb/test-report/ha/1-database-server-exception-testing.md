---
title: MogDB Database Server Exception Testing
summary: MogDB Database Server Exception Testing
author: Liu Xu
date: 2021-03-04
---

# MogDB Database Server Exception Testing

## Test Objective

The test aims to test the MogDB availability and stability in the scenarios where only one of the primary, standby, and arbitration MogDB nodes crashes, both primary and standby MogDB nodes crash, both primary and arbitration MogDB nodes crash, or both standby and arbitration MogDB nodes crash.

## Test Environment

| Category    |     Server Configuration     | Client Configuration | Quantity |
| ----------- | :--------------------------: | :------------------: | :------: |
| CPU         |         Kunpeng 920          |     Kunpeng 920      |   128    |
| Memory      |        DDR4,2933MT/s         |    DDR4,2933MT/s     | 2048 GB  |
| Hard disk   |          Nvme 3.5T           |       Nvme 3T        |    4     |
| File system |             Xfs              |         Xfs          |    4     |
| OS          |    openEuler 20.03 (LTS)     |      Kylin V10       |          |
| Database    | MogDB 1.1.0 software package |                      |          |
| Test tool   |           pgbench            |                      |          |

## The Primary Database Node Crashes Abnormally

Test solution:

1. Restart the primary database node.
2. Monitor the TPS change.
3. Add a new standby node.
4. Monitor the cluster status and TPS change.

Test procedure:

1. Restart the primary database node.

   ```
   reboot
   ```

2. Make the original primary database node work as a standby database node in the cluster.

   ```
   gs_ctl -b full build -D /gaussdata/openGauss/db1
   ```

3. Synchronize logs to the new standby database node until the synchronization is complete.

Test result:

- After the primary database node is shut down,  TPS drops from 9000 to 0, which lasts for 50s or so. After a VIP is added to the new primary database node (node 2), TPS increases from 0 to 13000 or so.
- When the old primary database node is added to the cluster working as a new standby database node and operated for data synchronization, TPS drops from 13000 to 0. As the synchronization is complete, TPS is restored to 9000.

## The Standby Database Node Crashes Abnormally

Test solution:

1. Restart the standby database node.

2. Monitor the TPS change.

3. Add the standby database node to the cluster.

4. Monitor the cluster status and TPS change.

Test procedure:

1. Restart the standby database node.

   ```
   reboot
   ```

2. Add the standby database node to the cluster.

   ```
   gs_ctl start -D /gaussdata/openGauss/db1 -M standby
   ```

3. Synchronize data to the standby database node until the synchronization is complete.

   ```
   Standby Catchup
   ```

Test result:

- After the standby database node is shut down, TPS increases from 9000 or so to 13000 or so.
- After the standby database node is added to the cluster and operated for data synchronization, TPS drops from 13000 to 0. As the synchronization is complete, TPS is restored to 9000.

## The Arbitration Node Crashes Abnormally

Test solution:

1. Add a VIP to the value of the **ping_list** parameter in the **node.conf** file.
2. Simulate the scenario where the arbitration node crashes.

Test procedure:

1. Add a VIP to the server IP addresses.

   ```
   ifconfig ens160:1 192.168.122.111 netmask 255.255.255.0
   ```

2. Add a VIP to the value of the **ping_list** parameter in the **node.conf** file. Synchronize the configuration file between the primary and standby database nodes and restart the monitor script.

3. When the monitor script is running normally, disconnect the VIP of the simulated arbitration node and then observe the change.

   ```
   ifconfig ens160:1 down
   ```

4. Manually connect the VIP of the simulated arbitration node and observe the change.

Test result:

- The monitor script of the primary and standby database nodes reports that the VIP of the arbitration node fails the ping operation. no other change occurs.
- TPS does not change.

## Both Primary and Standby Database Nodes Crash Abnormally

Test solution:

1. Restart both the primary and standby database nodes.
2. Start the **node_run** scripts of the primary and standby database nodes.
3. Manually enable the primary and standby databases.

Test procedure:

1. Restart both the primary and standby database nodes.

   ```
   reboot
   reboot
   ```

2. Start the **node_run** scripts of the primary and standby database nodes.

3. Manually enable the primary and standby databases.

Test result:

When the database is disabled, TPS drops to 0. After 15s the cluster is started, TPS increases to 9000.

## Both Primary and Arbitration Nodes Crash Abnormally

Test solution:

Shut down the primary database node and disconnect the IP address of the arbitration node.

Test procedure:

1. Shut down the primary database node and disconnect the IP address of the arbitration node.

2. Check the status of the standby database node.

3. Start the primary database node and its monitor script.

   ```
   gs_ctl start -D /gaussdata/openGauss/db1 -M primary
   ```

4. Check the cluster status.

Test result:

- After the primary database node and arbitration node are shut down, TPS drops from 9000 to 0.

- When the primary database node restores, TPS increases to 9000.

## Both Standby and Arbitration Nodes Crash Abnormally

Test solution:

Shut down the standby database node and disconnect the IP address of the arbitration node.

Test procedure:

1. Shut down the standby database node and disconnect the IP address of the arbitration node.

2. Check the status of the primary database node.

   No change occurs.

3. After the standby database node restores, start the monitor script of the standby database node.

   The error reported by the monitor script of the primary database node disappears.

4. Start the standby database node.

5. Check the cluster status.
