---
title: Routine Maintenance Testing
summary: Routine Maintenance Testing
author: Guo Huan
date: 2021-04-25
---

# Routine Maintenance Testing

## Test Scope

1. HA tool used for the switchover test
2. gs_ctl used for the switchover test
3. HA tool used for the failover test
4. gs_ctl used for the failover test
5. gs_ctl and HA contradiction test
6. Split-brain test for avoiding deploying two primary database nodes

## Test Environment

| Category    | Server Configuration  | Client Configuration | Quantity |
| ----------- | :-------------------: | :------------------: | :------: |
| CPU         |      Kunpeng 920      |     Kunpeng 920      |   128    |
| Memory      |     DDR4,2933MT/s     |    DDR4,2933MT/s     |  2048G   |
| Hard disk   |       Nvme 3.5T       |       Nvme 3T        |    4     |
| File system |          Xfs          |         Xfs          |    4     |
| OS          | openEuler 20.03 (LTS) |      Kylin V10       |          |
| Database    |      MogDB1.1.0       |                      |          |
| Tool        |        pgbench        |                      |          |

## HA Tool Used for the Switchover Test

Test procedure:

1. Query the status of databases.

2. Run the switch command on the standby database, and then query the database status.

    ```bash
    /home/omm/venv/bin/python3 /home/omm/openGaussHA_standlone/switch.py -config /home/omm/openGaussHA_standlone/node.conf --switchover
    ```

3. Query the VIP of the new primary database node.

Test result:

The primary/standby switchover is normal.

## gs_ctl Used for the Switchover Test

Test procedure:

1. Query the database status.
2. Run the command to perform the switchover operation and query the status when finished.
3. Query the VIP.

Test result:

The primary/standby switchover is normal.

## HA Tool Used for the Failover Test

Test procedure:

1. Query the current status of the database.

2. Run the HA failover command.

3. Observe the cluster status.

4. The original standby database node works as a new primary database node.

Test result:

1. The original standby database node becomes the new primary database node with a VIP.

2. The original primary database node is killed.

3. TPS increases from 6000 to 13000 in about 10s after failover.

## gs_ctl Used for the Failover Test

Test procedure:

1. Query the current status of the database.
2. Run the gs_ctl failover command.
3. Query the status of the new primary database node.
4. Query the status of the original primary database node.
5. Observe script changes.

Test result:

1. The new primary database node waits for 10s and then automatically shuts down, the original primary database node is not affected, and the VIP is still attached to the original primary database node.
2. About 5s or 6s after failover of the standby database node, the TPS increases from 9000 to 13000. No other changes occur except fluctuations.

## gs_ctl and HA Contradiction Test

Test procedure:

1. Record the cluster status.

2. Stop the primary and standby database nodes at the same time.

3. Use gs_ctl to start the primary and standby database nodes. Designate the original standby database node as the primary database node, vice versa.

4. Observe the script and cluster status.

Test result:

1. HA configuration is adjusted according to the actual situation.
2. TPS drops from 9000 to 0 after the primary and standby database nodes are shut down. At this time, HA detects the cluster configuration change and attaches the VIP to the new primary database node within 10s. After about 5s or 6s, TPS rises from 0 to 13000.

## Split-Brain Test for Avoiding Deploying Two Primary Database Nodes

Test procedure:

1. Query the current status of the cluster.

2. Use the gs_ctl command to restart the standby database node to make it work as the primary database node.

Test result:

1. After timeout for 10s, the primary database node that was restarted by an abnormal operation is killed by the monitoring script.

2. Repair the standby database node and add it to the cluster.

    ```bash
    gs_ctl build -b full -D /gaussdata/openGauss/db1
    ```

3. TPS increases from 6000 to 13000 after the standby database is restarted to work as the primary database node.

4. TPS drops from 13000 to 9000 (same as that in the primary/standby deployment scenario) after the standby database node is added to the cluster.
