---
title: Introduction
summary: SELECT
author: Zhang Cuiping
date: 2022-08-07
---

<CustomContainer title="MogDB" subtitle="Yunhe Enmo develops a stable and easy-to-use enterprise-ready relational database based on the openGauss open source database. Find the guide, samples, and references you need to use MogDB.">

<CustomGroup label="Introduction">

[MogDB Introduction](./overview.md)

[Characteristic Description](./characteristic-description/characteristic-description-overview.md)

[Release Notes](./about-mogdb/mogdb-new-feature/release-note.md)

</CustomGroup>

<CustomGroup label="Try MogDB">

[MogDB Playground](./quick-start/mogdb-playground.md)

</CustomGroup>

<CustomGroup label="Deploy">

[PTK-based Installation](./installation-guide/ptk-based-installation.md)

[MogDB in Container](./installation-guide/docker-installation/docker-installation.md)

[MogDB on Kubernetes](../../docs-mogdb-stack/v1.0/quick-start.md)

</CustomGroup>

<CustomGroup label="Development">

[Development Based on JDBC](./developer-guide/dev/2-development-based-on-jdbc/3-development-process.md)

[Data Import and Export](./administrator-guide/importing-and-exporting-data/importing-data/1-import-modes.md)

</CustomGroup>

<CustomGroup label="Maintenance">

[Routine Maintenance](./administrator-guide/routine-maintenance/0-starting-and-stopping-mogdb.md)

[Backup and Restoration](./administrator-guide/br/1-1-br.md)

[Upgrade Guide](./administrator-guide/upgrade-guide.md)

[Common Fault Locating Methods](./common-faults-and-identification/common-fault-locating-methods.md)

[MogDB Stack](../../docs-mogdb-stack/v1.0/overview.md)

[MogHA](../../docs-mogha/v2.3/overview.md)

[MogDB Tools](./reference-guide/tool-reference/tool-overview.md)

</CustomGroup>

<CustomGroup label="Optimization">

[System Optimization](./performance-tuning/1-system/1-optimizing-os-parameters.md)

[SQL Optimization](./performance-tuning/2-sql/1-query-execution-process.md)

[WDR Snapshot](./performance-tuning/3-wdr/wdr-snapshot-schema.md)

[TPCC Performance Tuning Guide](./performance-tuning/TPCC-performance-tuning-guide.md)

</CustomGroup>

<CustomGroup label="Tools">

[PTK - Provisioning Toolkit](../../docs-ptk/v0.4/overview.md)

[MTK - Database Migration Toolkit](../../docs-mtk/v2.0/overview.md)

[SCA - SQL Compatibility Analyzer](../../docs-sca/v5.1/overview.md)

[MVD - MogDB Verify Data](../../docs-mvd/v2.4/overview.md)

[MDB - MogDB Data Bridge](../../docs-mdb/v1.0/overview.md)

[Mogeaver - Graphical Tool](./mogeaver/mogeaver-overview.md)

</CustomGroup>

</CustomContainer>
