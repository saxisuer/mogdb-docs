---
title: Terms of Use
summary: Terms of Use
author: Guo Huan
date: 2021-06-01
---

# Terms of Use

**Copyright © 2009-2022 Yunhe Enmo (Beijing) Information Technology Co., Ltd. All rights reserved.**

Your replication, use, modification, and distribution of this document are governed by the Creative Commons License Attribution-ShareAlike 4.0 International Public License (CC BY-SA 4.0). You can visit <https://creativecommons.org/licenses/by-sa/4.0/> to view a human-readable summary of (and not a substitute for) CC BY-SA 4.0. For the complete CC BY-SA 4.0, visit <https://creativecommons.org/licenses/by-sa/4.0/legalcode>.

Certain document contents on this website are from the official openGauss website (<https://opengauss.org/en/docs/1.1.0/docs/Quickstart/Quickstart.html>)。

**Trademarks and Permissions**

MogDB is a trademark of Yunhe Enmo (Beijing) Information Technology Co., Ltd. All other trademarks and registered trademarks mentioned in this document are the property of their respective holders.

**Disclaimer**

This document is used only as a guide. Unless otherwise specified by applicable laws or agreed by both parties in written form, all statements, information, and recommendations in this document are provided "AS IS" without warranties, guarantees or representations of any kind, including but not limited to non-infringement, timeliness, and specific purposes.
