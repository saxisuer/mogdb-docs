---
title: mogdb-monitor
summary: mogdb-monitor
author: Guo Huan
date: 2022-04-14
---

# mogdb-monitor

mogdb-monitor is a MogDB database cluster monitoring and deployment tool, with the help of the current very popular open source monitoring system prometheus framework, combined with the opengauss_exporter developed by Enmo database team, you can achieve a full range of detection of MogDB database.

The core monitoring component opengauss_exporter has the following features.

- Support all versions of MogDB/openGauss database
- Support for monitoring database clusters
- Support primary and standby database judgment within a cluster
- Support for automatic database discovery
- Support for custom query
- Supports online loading of configuration files
- Support for configuring the number of concurrent threads
- Support data collection information caching

In terms of grafana display, Enmo also provide a complete set of dashboard, both an instance-level dashboard showing detailed information of each instance and a display big screen showing summary information of all instances, which, combined with the alertmanager component, can trigger alerts that meet the rules to relevant personnel in the first place.

<br/>

Please refer to [mogdb-monitor repository page](https://gitee.com/enmotech/mogdb-monitor) for details on how to obtain and use component.
