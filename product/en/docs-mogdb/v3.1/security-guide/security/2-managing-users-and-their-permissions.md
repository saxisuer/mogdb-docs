---
title: Managing Users and Their Permissions
summary: Managing Users and Their Permissions
author: Guo Huan
date: 2021-04-26
---

# Managing Users and Their Permissions

## Default Permission Mechanism

A user who creates an object is the owner of this object. By default, Separation of Duties is disabled after database installation. A database system administrator has the same permissions as object owners. After an object is created, only the object owner or system administrator can query, modify, and delete the object, and grant permissions for the object to other users through GRANT by default.

To enable another user to use the object, grant required permissions to the user or the role that contains the user.

MogDB supports the following permissions: SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, CREATE, CONNECT, EXECUTE, USAGE, ALTER, DROP, COMMENT, INDEX, and VACUUM. Permission types are associated with object types. For permission details, see GRANT.

To remove permissions, run **REVOKE**. Object owners have implicit permissions (such as ALTER, DROP, COMMENT, INDEX, VACUUM, GRANT, and REVOKE) on objects. That is, once becoming the owner of an object, the owner is immediately granted the implicit permissions on the object. Object owners can remove their own common permissions, for example, making tables read-only to themselves or others, except the system administrator.

System catalogs and views are visible to either system administrators or all users. System catalogs and views that require system administrator permissions can be queried only by system administrators. For details, see System Catalogs and System Views.

The database provides the object isolation feature. If this feature is enabled, users can view only the objects (tables, views, columns, and functions) that they have the permission to access. System administrators are not affected by this feature. For details, see ALTER DATABASE.

## Administrators

### Initial Users

The account automatically generated during MogDB installation is called an initial user. An initial user is the system, monitoring, O&M, and security policy administrator who has the highest-level permissions in the system and can perform all operations. This account has the same name as the OS user used for MogDB installation. You need to manually set the password during the installation. After the first login, change the initial user's password in time.

An initial user bypasses all permission checks. You are advised to use an initial user as a database administrator only for database management other than service running.

### System Administrators

A system administrator is an account with the **SYSADMIN** attribute. By default, a database system administrator has the same permissions as object owners but does not have the object permissions in **dbe_perf** mode.

To create a system administrator, connect to the database as the initial user or a system administrator and run the **CREATE USER** or **ALTER USER** statement with **SYSADMIN** specified.

```sql
MogDB=# CREATE USER sysadmin WITH SYSADMIN password "xxxxxxxxx";
```

or

```sql
MogDB=# ALTER USER joe SYSADMIN;
```

To run the **ALTER USER** statement, the user must exist.

### Monitor Administrators

A monitoring administrator is an account with the **MONADMIN** attribute that has the privilege to view views and functions in **dbe_perf** mode and to grant or withdraw privileges to objects in **dbe_perf** mode.

To create a monitor administrator, connect to the database as the system administrator and run the **CREATE USER** or **ALTER USER** statement with **MONADMIN** specified.

```sql
MogDB=# CREATE USER monadmin WITH MONADMIN password "xxxxxxxxx";
```

or

```sql
MogDB=# ALTER USER joe MONADMIN;
```

To run the **ALTER USER** statement, the user must exist.

### O&M Administrators

An O&M administrator is an account with the **OPRADMIN** attribute and has permission to perform backup restores using the Roach tool.

To create an O&M administrator, connect to the database as the initial user and run the **CREATE USER** or **ALTER USER** statement with **OPRADMIN** specified.

```sql
MogDB=# CREATE USER opradmin WITH OPRADMIN password "xxxxxxxxx";
```

or

```sql
MogDB=# ALTER USER joe OPRADMIN;
```

To run the **ALTER USER** statement, the user must exist.

### Security Policy Administrators

A security policy administrator is an account with **POLADMIN** attributes and has permission to create resource tags, desensitization policies, and unified audit policies.

To create an security policy administrator, connect to the database as the system administrator and run the **CREATE USER** or **ALTER USER** statement with **POLADMIN** specified.

```sql
MogDB=# CREATE USER poladmin WITH POLADMIN password "xxxxxxxxx";
```

or

```sql
MogDB=# ALTER USER joe POLADMIN;
```

To run the **ALTER USER** statement, the user must exist.

## Separation of Duties

Descriptions in Default Permission Mechanism and Administrators are about the initial situation after a cluster is created. By default, a system administrator with the **SYSADMIN** attribute has the highest-level permissions.

In actual service management, you can set separation of duties to prevent system administrators from having excessive centralized permissions, which may cause high risks. Some permissions of the system administrator are transferred to the security administrator and audit administrator, implementing separation of duties among the system administrator, security administrator, and audit administrator.

After separation of duties is enabled, a system administrator does not have the **CREATEROLE** attribute (security administrator) and **AUDITADMIN** attribute (audit administrator). That is, the system administrator does not have the permissions to create roles and users and the permissions to view and maintain database audit logs. For details about the **CREATEROLE** and **AUDITADMIN** attributes, see CREATE ROLE.

After separation of duties is enabled, system administrators have the permissions only for the objects owned by them.

Separation of duties does not take effect for an initial user. Therefore, you are advised to use an initial user as a database administrator only for database management other than service running.

To enable separation of duties, set **enableSeparationOfDuty** to **on**.

For details about permission changes before and after enabling separation of duties, see [Table 1](#Table-2.3.1) and [Table 2](#Table-2.3.2).

**Table 1** Default user permissions<a id="Table-2.3.1"> </a>

<table>
    <tr>
        <th>Object Name</th>
        <th>Initial User (ID: 10)</th>
        <th>System Administrator</th>
        <th>Monitor Administrator</th>
        <th>O&M Administrator</th>
        <th>Security Policy Administrator</th>
        <th>Security Administrator</th>
        <th>Audit Administrator</th>
        <th>Common User</th>
    </tr>
    <tr>
     <td>Tablespaces</td>
        <td rowspan="7">Has all permissions except the one to access private tables.</td>
        <td>Can create, modify, delete, access, or grant permissions for tablespaces.</td>
        <td colspan="6">Cannot create, modify, delete, or grant permissions for tablespaces and can access tablespaces if the access permission is granted.</td>
    </tr>
    <tr>
     <td>Tables</td>
        <td>Has permissions for all tables.</td>
        <td colspan="6">Has all permissions for their own tables, but does not have permissions for other users' tables.</td>
    </tr>
    <tr>
        <td>Indexes</td>
        <td>Can create indexes on all tables.</td>
        <td colspan="6">Can create indexes on their own tables.</td>
    </tr>
    <tr>
        <td>Schemas</td>
        <td>Has all permissions for all schemas except dbe_perf.</td>
        <td>Has all permissions for their own schemas and dbe_perf, but does not have permissions for other users' schemas.</td>
        <td colspan="5">Has all permissions for their own schemas, but does not have permissions for other users' schemas.</td>
    </tr>
    <tr>
        <td>Functions</td>
        <td>Has all permissions for all functions except those in the dbe_perf schema.</td>
        <td>Has permissions for their own functions and those in the dbe_perf schema, has the call permission for other users' functions in the public schema, but does not have permissions for other users' functions in other schemas.</td>
        <td colspan="5">Has permissions for their own functions, has the call permission for other users' functions in the public schema, but does not have permissions for other users' functions in other schemas.</td>
    </tr>
    <tr>
        <td>Customized views</td>
        <td>Has all permissions on all views except the dbe_perf schema view.</td>
        <td>Has permissions for their own views and the dbe_perf schema view, but does not have permissions for other users' views.</td>
        <td colspan="5">Has permissions for their own views, but does not have permissions for other users' views.</td>
    </tr>
    <tr>
     <td>System catalogs and system views</td>
        <td>Has permissions to query all system catalogs and views.</td>
        <td colspan="6">Has permissions to query only some system catalogs and views. For details, see System Catalogs and System Views.</td>
    </tr>
</table>
**Table 2** Changes in permissions after separation of duties<a id="Table-2.3.2"> </a>

<table>
    <tr>
        <th>Object Name</th>
        <th>Initial User (ID: 10)</th>
        <th>System Administrator</th>
        <th>Monitor Administrator</th>
        <th>O&M Administrator</th>
        <th>Security Policy Administrator</th>
        <th>Security Administrator</th>
        <th>Audit Administrator</th>
        <th>Common User</th>
    </tr>
    <tr>
     <td>Tablespaces</td>
        <td rowspan="7">N/A.<br/>Has all permissions except the one to access private tables.</td>
        <td>N/A</td>
        <td colspan="6">N/A</td>
    </tr>
    <tr>
     <td>Tables</td>
        <td>Permissions reduced<br/>Has all permissions for their own tables and other users' tables in the public schema, but does not have permissions for other users' tables in other schemas.</td>
        <td colspan="6">N/A</td>
    </tr>
    <tr>
        <td>Indexes</td>
        <td>Permissions reduced<br/>Can create indexes for their own tables and other users' tables in the public schema.</td>
        <td colspan="6">N/A</td>
    </tr>
    <tr>
        <td>Schemas</td>
        <td>Permissions reduced<br/>Has all permissions for their own schemas, but does not have permissions for other users' schemas.</td>
        <td>N/A</td>
        <td colspan="5">N/A</td>
    </tr>
    <tr>
        <td>Functions</td>
        <td>Permissions reduced<br/>Has all permissions for their own functions and other users' functions in the public schema, but does not have permissions for other users' functions in other schemas.</td>
        <td>N/A</td>
        <td colspan="5">N/A</td>
    </tr>
    <tr>
        <td>Customized views</td>
        <td>Permissions reduced<br/>Has all permissions for their own views and other users' views in the public schema, but does not have permissions for other users' views in other schemas.</td>
        <td>N/A</td>
        <td colspan="5">N/A</td>
    </tr>
    <tr>
     <td>System catalogs and system views</td>
        <td>N/A</td>
        <td colspan="6">N/A</td>
    </tr>
</table>

## Users

You can use  **CREATE USER**  and  **ALTER USER**  to create and manage database users, respectively. MogDB contains one or more named database users and roles that are shared across MogDB. However, these users and roles do not share data. That is, a user can connect to any database, but after the connection is successful, any user can access only the database declared in the connection request.

In non-separation-of-duties scenarios, MogDB user accounts can be created and deleted only by a system administrator or a security administrator with the **CREATEROLE** attribute. In separation-of-duties scenarios, a user account can be created only by an initial user or a security administrator.

When a user logs in, MogDB authenticates the user. A user can own databases and database objects (such as tables), and grant permissions of these objects to other users and roles. In addition to system administrators, users with the **CREATEDB** attribute can create databases and grant permissions on these databases.

**Adding, Modifying, and Deleting Users**

- To create a user, use the SQL statement **CREATE USER**.

  For example, create a user **joe** and set the **CREATEDB** attribute for the user.

  ```sql
  MogDB=# CREATE USER joe WITH CREATEDB PASSWORD "XXXXXXXXX";
  CREATE ROLE
  ```

- To create a system administrator, use the **CREATE USER** statement with the **SYSADMIN** parameter.

- To delete an existing user, use **DROP USER**.

- To change a user account (for example, rename the user or change the password), use **ALTER USER**.

- To view a user list, query the **PG_USER** view.

  ```sql
  MogDB=# SELECT * FROM pg_user;
  ```

- To view user attributes, query the system catalog **PG_AUTHID**.s

  ```sql
  MogDB=# SELECT * FROM pg_authid;
  ```

**Private Users**

If multiple service departments use different database user accounts to perform service operations and a database maintenance department at the same level uses database administrator accounts to perform maintenance operations, service departments may require that database administrators, without specific authorization, can manage (**DROP**, **ALTER**, and **TRUNCATE**) their data but cannot access (**INSERT**, **DELETE**, **UPDATE**, **SELECT**, and **COPY**) the data. That is, the management permissions of database administrators for tables need to be isolated from their access permissions to improve the data security of common users.

In separation-of-duties mode, a database administrator does not have permissions for the tables in schemas of other users. In this case, database administrators have neither management permissions nor access permissions, which does not meet the requirements of the service departments mentioned above. Therefore, MogDB provides private users to solve the problem. That is, create private users with the **INDEPENDENT** attribute in non-separation-of-duties mode.

```sql
MogDB=# CREATE USER user_independent WITH INDEPENDENT IDENTIFIED BY "1234@abc";
```

System administrators and security administrators with the **CREATEROLE** attribute can manage (**DROP**, **ALTER**, and **TRUNCATE**) objects of private users but cannot access (**INSERT**, **DELETE**, **SELECT**, **UPDATE**, **COPY**, **GRANT**, **REVOKE**, and **ALTER OWNER**) the objects before being authorized.

> **NOTICE:** PG_STATISTIC and PG_STATISTIC_EXT store sensitive information about statistical objects, such as high-frequency MCVs. The system administrator can still access the two system catalogs to obtain the statistics of the tables to which private users belong.

**Permanent Users**

MogDB provides the permanent user solution, that is, create a permanent user with the **PERSISTENCE** property.

```sql
MogDB=# CREATE USER user_persistence WITH persistence IDENTIFIED BY "1234@abc";
```

Only the initial user is allowed to create, modify, and delete permanent users with the **PERSISTENCE** attribute.

## Roles

A role is a set of users. After a role is granted to a user through **GRANT**, the user will have all the permissions of the role. It is recommended that roles be used to efficiently grant permissions. For example, you can create different roles of design, development, and maintenance personnel, grant the roles to users, and then grant specific data permissions required by different users. When permissions are granted or revoked at the role level, these changes take effect on all members of the role.

MogDB provides an implicitly defined group **PUBLIC** that contains all roles. By default, all new users and roles have the permissions of **PUBLIC**. For details about the default permissions of **PUBLIC**, see GRANT. To revoke permissions of **PUBLIC** from a user or role, or re-grant these permissions to them, add the **PUBLIC** keyword in the **REVOKE** or **GRANT** statement.

To view all roles, query the system catalog **PG_ROLES**.

```sql
SELECT * FROM PG_ROLES;
```

**Adding, Modifying, and Deleting Roles**

In non-separation-of-duties scenarios, a role can be created, modified, and deleted only by a system administrator or a user with the **CREATEROLE** attribute. In separation-of-duties scenarios, a role can be created, modified, and deleted only by an initial user or a user with the **CREATEROLE** attribute.

- To create a role, use **CREATE ROLE**.
- To add or delete users in an existing role, use **ALTER ROLE**.
- To delete a role, use **DROP ROLE**. **DROP ROLE** deletes only a role, rather than member users in the role.

## Built-in roles

MogDB provides a group of default roles whose names start with **gs_role_**. These roles are provided to access to specific, typically high-privileged operations. You can grant these roles to other users or roles within the database so that they can use specific functions. These roles should be given with great care to ensure that they are used where they are needed. [Table 3](#table1) describes the permissions of built-in roles.

**Table 3** Permission description of built-in roles <a id="table1"></a>

| Role                   | Permission                                                   |
| :--------------------- | :----------------------------------------------------------- |
| gs_role_copy_files     | Permission to run the **copy… to/from filename** command. However, the GUC parameter **enable_copy_server_files** must be set first to enable the function of copying server files. |
| gs_role_signal_backend | Permission to call the **pg_cancel_backend**, **pg_terminate_backend**, and **pg_terminate_session** functions to cancel or terminate other sessions. However, this role cannot perform operations on sessions of the initial user or **PERSISTENCE** user. |
| gs_role_tablespace     | Permission to create a tablespace.                           |
| gs_role_replication    | Permission to call logical replication functions, such as **kill_snapshot**, **pg_create_logical_replication_slot**, **pg_create_physical_replication_slot**, **pg_drop_replication_slot**, **pg_replication_slot_advance**, **pg_create_physical_replication_slot_extern**, **pg_logical_slot_get_changes**, **pg_logical_slot_peek_changes**, **pg_logical_slot_get_binary_changes**, and **pg_logical_slot_peek_binary_changes**. |
| gs_role_account_lock   | Permission to lock and unlock users. However, this role cannot lock or unlock the initial user or users with the **PERSISTENCE** attribute. |
| gs_role_pldebugger     | Permission to debug functions in **dbe_pldebugger**.         |

The restrictions on built-in roles are as follows:

- The role names starting with **gs_role_** are reserved for built-in roles in the database. Do not create users or roles starting with **gs_role_** or rename existing users or roles starting with **gs_role_**.

- Do not perform **ALTER** or **DROP** operations on built-in roles.

- By default, built-in roles do not have the **LOGIN** permission and do not have preset passwords.

- The **gsql** meta-commands **\\du** and **\\dg** do not display information about built-in roles. However, if *pattern* is set to a specific built-in role, the information is displayed.

- When separation-of-duty is disabled, the initial user, users with the **SYSADMIN** permission, and users with the **ADMIN OPTION** built-in role permission have the permission to perform **GRANT** and **REVOKE** operations on built-in roles. When separation of duty is enabled, the initial user and users with the **ADMIN OPTION** built-in role permission have the permission to perform **GRANT** and **REVOKE** operations on built-in roles. Example:

  ```sql
  GRANT gs_role_signal_backend TO user1;
  REVOKE gs_role_signal_backend FROM user1;
  ```

## Schemas

Schemas allow multiple users to use the same database without interference. In this way, database objects can be organized into logical groups that are easy to manage, and third-party applications can be added to corresponding schemas without causing conflicts.

Each database has one or more schemas. Each schema contains tables and other types of objects. When a database is initially created, it has a schema named **public** by default, and all users have the usage permission on the schema. Only the system administrator and initial users can create functions, stored procedures, and synonyms in the public schema. Other users cannot create the three types of objects even if they have the create permission. You can group database objects by schema. A schema is similar to an OS directory but cannot be nested. By default only the initializing user can create objects in pg_catalog mode.

The same database object name can be used in different schemas of the same database without causing conflicts. For example, both **a_schema** and **b_schema** can contain a table named **mytable**. Users with required permissions can access objects across multiple schemas of the same database.

When you run the **CREATE USER** command to create a user, the system creates a schema with the same name as the user in the database where the command is executed.

Database objects are generally created in the first schema in a database search path. For details about the first schema and how to change the schema order, see [Search Path](#Search-Path).

**Creating, Modifying, and Deleting Schemas**

- To create a schema, use **CREATE SCHEMA**. By default, the initial user and system administrator can create schemas. Other users can create schemas in the database only when they have the CREATE permission on the database. For details about how to grant the permission, see the syntax in GRANT.

- To change the name or owner of a schema, use **ALTER SCHEMA**. The schema owner can change the schema.

- To delete a schema and its objects, use **DROP SCHEMA**. Schema owners can delete schemas.

- To create a table in a schema, use the **schema_name.table_name** format to specify the table. If *schema_name* is not specified, the table will be created in the first schema in search path.

- To view the owner of a schema, perform the following join query on the system catalogs **PG_NAMESPACE** and **PG_USER**. Replace **schema_name** in the statement with the name of the schema to be queried.

  ```sql
  MogDB=# SELECT s.nspname,u.usename AS nspowner FROM pg_namespace s, pg_user u WHERE nspname='schema_name' AND s.nspowner = u.usesysid;
  ```

- To view a list of all schemas, query the system catalog **PG_NAMESPACE**.

  ```sql
  MogDB=# SELECT * FROM pg_namespace;
  ```

- To view a list of tables in a schema, query the system catalog **PG_TABLES**. For example, the following query will return a table list from **PG_CATALOG** in the schema.

  ```sql
  MogDB=# SELECT distinct(tablename),schemaname from pg_tables where schemaname = 'pg_catalog';
  ```

**Search Path<a id="Search-Path"> </a>**

A search path is defined in the **search_path** parameter. The parameter value is a list of schema names separated by commas (,). If no target schema is specified during object creation, the object will be added to the first schema listed in the search path. If there are objects with the same name across different schemas and no schema is specified for an object query, the object will be returned from the first schema containing the object in the search path.

- To view the current search path, use **SHOW**.

  ```sql
  MogDB=# SHOW SEARCH_PATH;
   search_path
  ----------------
   "$user",public
  (1 row)
  ```

  The default value of **search_path** is `"$user",public.` **$user** indicates the name of the schema with the same name as the current session user. If the schema does not exist, **$user** will be ignored. By default, after a user connects to a database that has schemas with the same name, objects will be added to all the schemas. If there are no such schemas, objects will be added to only the **public** schema.

- To change the default schema of the current session, run the **SET** statement.

  Run the following command to set **search_path** to **myschema** and **public** (**myschema** will be searched first):

  ```sql
  MogDB=# SET SEARCH_PATH TO myschema, public;
  SET
  ```

## Setting User Permissions

- To grant permissions for an object to a user, see GRANT.

  When permissions for a table or view in a schema are granted to a user or role, the **USAGE** permission of the schema must be granted together. Otherwise, the user or role can only see these objects but cannot access them.

  In the following example, permissions for the schema **tpcds** are first granted to user **joe**, and then the **SELECT** permission for the **tpcds.web_returns** table is also granted.

  ```sql
  MogDB=# GRANT USAGE ON SCHEMA tpcds TO joe;
  MogDB=# GRANT SELECT ON TABLE tpcds.web_returns to joe;
  ```

- Grant a role to a user to allow the user to inherit the object permissions of the role.

  1. Create a role.

     Create a role **lily** and grant the system permission **CREATEDB** to the role.

     ```sql
     MogDB=# CREATE ROLE lily WITH CREATEDB PASSWORD "XXXXXXXXX";
     ```

  2. Grant object permissions to the role by using **GRANT**.

     For example, first grant permissions for the schema **tpcds** to the role **lily**, and then grant the **SELECT** permission of the **tpcds.web_returns** table to **lily**.

     ```sql
     MogDB=# GRANT USAGE ON SCHEMA tpcds TO lily;
     MogDB=# GRANT SELECT ON TABLE tpcds.web_returns to lily;
     ```

  3. Grant the role permissions to a user.

     ```sql
     MogDB=# GRANT lily to joe;
     ```

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
     > When the permissions of a role are granted to a user, the attributes of the role are not transferred together.

- To revoke user permissions, use **REVOKE**.

## Row-Level Access Control

The row-level access control feature enables database access control to be accurate to each row of data tables. In this way, the same SQL query may return different results for different users.

You can create a row-level access control policy for a data table. The policy defines an expression that takes effect only for specific database users and SQL operations. When a database user accesses the data table, if a SQL statement meets the specified row-level access control policies of the data table, the expressions that meet the specified condition will be combined by using **AND** or **OR** based on the attribute type (**PERMISSIVE** | **RESTRICTIVE**) and applied to the execution plan in the query optimization phase.

Row-level access control is used to control the visibility of row-level data in tables. By predefining filters for data tables, the expressions that meet the specified condition can be applied to execution plans in the query optimization phase, which will affect the final execution result. Currently, the SQL statements that can be affected include **SELECT**, **UPDATE**, and **DELETE**.

Scenario 1: A table summarizes the data of different users. Users can view only their own data.

```sql
--Create users alice, bob, and peter.
MogDB=# CREATE ROLE alice PASSWORD 'Enmo@123';
MogDB=# CREATE ROLE bob PASSWORD 'Enmo@123';
MogDB=# CREATE ROLE peter PASSWORD 'Enmo@123';

--Create the all_data table that contains user information.
MogDB=# CREATE TABLE all_data(id int, role varchar(100), data varchar(100));

--Insert data into the data table.
MogDB=# INSERT INTO all_data VALUES(1, 'alice', 'alice data');
MogDB=# INSERT INTO all_data VALUES(2, 'bob', 'bob data');
MogDB=# INSERT INTO all_data VALUES(3, 'peter', 'peter data');

--Grant the read permission for the all_data table to users alice, bob, and peter.
MogDB=# GRANT SELECT ON all_data TO alice, bob, peter;

--Enable row-level access control.
MogDB=# ALTER TABLE all_data ENABLE ROW LEVEL SECURITY;

--Create a row-level access control policy to specify that the current user can view only their own data.
MogDB=# CREATE ROW LEVEL SECURITY POLICY all_data_rls ON all_data USING(role = CURRENT_USER);

--View table details.
MogDB=# \d+ all_data
                               Table "public.all_data"
 Column |          Type          | Modifiers | Storage  | Stats target | Description
--------+------------------------+-----------+----------+--------------+-------------
 id     | integer                |           | plain    |              |
 role   | character varying(100) |           | extended |              |
 data   | character varying(100) |           | extended |              |
Row Level Security Policies:
    POLICY "all_data_rls"
      USING (((role)::name = "current_user"()))
Has OIDs: no
Location Nodes: ALL DATANODES
Options: orientation=row, compression=no, enable_rowsecurity=true

--Switch to user alice and run SELECT * FROM public.all_data.
MogDB=# SELECT * FROM public.all_data;
 id | role  |    data
----+-------+------------
  1 | alice | alice data
(1 row)

MogDB=# EXPLAIN(COSTS OFF) SELECT * FROM public.all_data;
                           QUERY PLAN
----------------------------------------------------------------
 Streaming (type: GATHER)
   Node/s: All datanodes
   ->  Seq Scan on all_data
         Filter: ((role)::name = 'alice'::name)
 Notice: This query is influenced by row level security feature
(5 rows)

--Switch to user peter and run SELECT * FROM public.all_data.
MogDB=# SELECT * FROM public.all_data;
 id | role  |    data
----+-------+------------
  3 | peter | peter data
(1 row)

MogDB=#  EXPLAIN(COSTS OFF) SELECT * FROM public.all_data;
                           QUERY PLAN
----------------------------------------------------------------
 Streaming (type: GATHER)
   Node/s: All datanodes
   ->  Seq Scan on all_data
         Filter: ((role)::name = 'peter'::name)
 Notice: This query is influenced by row level security feature
(5 rows)
```

>**Notice:** PG_STATISTIC system table and PG_STATISTIC_EXT system table store some sensitive information of statistical objects, such as high frequency value MCV. After the separation of powers is carried out, the system administrator can still obtain the information in the statistical information by accessing the two system tables.

## Setting Security Policies

### Setting Account Security Policies

**Background**

For data security purposes, MogDB provides a series of security measures, such as automatically locking and unlocking accounts, manually locking and unlocking abnormal accounts, and deleting accounts that are no longer used.

**Automatically Locking and Unlocking Accounts**

- If the number of incorrect password attempts (**failed_login_attempts**) of an account reaches the upper limit (**10** by default), the system automatically locks the account. Smaller parameter values result in higher account security. However, if the value of this parameter is set too small, inconvenience may occur.

- If the time during which a user is locked exceeds the preset value (**password_lock_time**, one day by default), the system automatically unlocks the user. Larger parameter values bring higher account security. However, if the value of this parameter is set too large, inconvenience may occur.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - The integral part of the **password_lock_time** value indicates the number of days and its decimal part can be converted into hours, minutes, and seconds.
  > - If the **failed_login_attempts** parameter is set to **0**, an account is never locked due to incorrect password attempts. If the **password_lock_time** parameter is set to **0**, an account is quickly unlocked after it is locked due to incorrect password attempts. Therefore, only when both parameters are set to positive values, the following operations can be performed: password failure check, account locking, and account unlocking.
  > - The default values of the two parameters meet the security requirements. You can change the parameter values as needed for higher security. You are advised to retain the default values.

Configure the **failed_login_attempts** parameter.

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to connect to the database:

   ```bash
   gsql -d mogdb -p 8000
   ```

   **mogdb** is the name of the database to be connected, and **8000** is the port number of the database primary node.

3. View the current value.

   ```sql
   MogDB=# SHOW failed_login_attempts;
    failed_login_attempts
   -----------------------
    10
   (1 row)
   ```

   If the command output is not **10**, run the **\q** command to exit the database.

4. Run the following command to set the parameter to its default value **10**:

   ```bash
   gs_guc reload -D /mogdb/data/dbnode -c "failed_login_attempts=10"
   ```

Configure the **password_lock_time** parameter.

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to connect to the database:

   ```bash
   gsql -d mogdb -p 8000
   ```

   **mogdb** is the name of the database to be connected, and **8000** is the port number of the database primary node.

3. View the current value.

   ```sql
   MogDB=# SHOW password_lock_time;
    password_lock_time
   -----------------------
    1
   (1 row)
   ```

   If the command output is not **1**, run the **\q** command to exit the database.

4. Run the following command to set the parameter to its default value **1**:

   ```bash
   gs_guc reload -N all -I all -c "password_lock_time=1"
   ```

**Manually Locking and Unlocking Accounts**

Once detecting that an account is stolen or the account is used to access the database without being authorized, administrators can manually lock the account.

Administrators can manually unlock the account if the account becomes normal again.

For details about how to create a user, see Users. To manually lock and unlock user **joe**, run commands in the following format:

- To manually lock the account, run the following command:

  ```sql
  MogDB=# ALTER USER joe ACCOUNT LOCK;
  ALTER ROLE
  ```

- To manually unlock the account, run the following command:

  ```sql
  MogDB=# ALTER USER joe ACCOUNT UNLOCK;
  ALTER ROLE
  ```

**Deleting Accounts That Are No Longer Used**

Administrators can delete an account that is no longer used. This operation cannot be rolled back.

When an account to be deleted is in the active state, it is deleted after the session is disconnected.

For example, if you want to delete account **joe**, run the following command:

```sql
MogDB=# DROP USER joe  CASCADE;
DROP ROLE
```

### Setting the Validity Period of an Account

**Precautions**

When creating a user, you need to specify the validity period of the user, including the start time and end time.

To enable a user not within the validity period to use its account, set a new validity period.

**Procedure**

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to connect to the database:

   ```bash
   gsql -d mogdb -p 8000
   ```

   **mogdb** is the name of the database to be connected, and **8000** is the port number of the database primary node.

3. Run the following command to create a user and specify the start time and end time:

   ```sql
   CREATE USER joe WITH PASSWORD 'XXXXXXXXX' VALID BEGIN '2015-10-10 08:00:00' VALID UNTIL '2016-10-10 08:00:00';
   ```

   The user is created if the following information is displayed:

   ```sql
   CREATE ROLE
   ```

4. If the user is not within the specified validity period, run the following command to set the start time and end time of a new validity period.

   ```sql
   ALTER USER joe WITH VALID BEGIN '2016-11-10 08:00:00' VALID UNTIL '2017-11-10 08:00:00';
   ```

   The start time and end time of the new validity period is set successfully if the following information is displayed:

   ```sql
   ALTER ROLE
   ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> If **VALID BEGIN** is not specified in the **CREATE ROLE** or **ALTER ROLE** statement, the start time of the validity period is not limited. If **VALID UNTIL** is not specified, the end time of the validity period is not limited. If both of the parameters are not specified, the user is always valid.

### Setting Password Security Policies

**Procedure**

User passwords are stored in the system catalog **pg_authid**. To prevent password leakage, MogDB encrypts user passwords before storing them. The encryption algorithm is determined by the configuration parameter **password_encryption_type**.

- If parameter **password_encryption_type** is set to **0**, passwords are encrypted using MD5. MD5 is not recommended because it is insecure.
- If parameter **password_encryption_type** is set to **1**, passwords are encrypted using SHA-256 and MD5. MD5 is not recommended because it is insecure.
- If parameter **password_encryption_type** is set to **2**, passwords are encrypted using SHA-256. This is the default configuration.
- If parameter **password_encryption_type** is set to **3**, passwords are encrypted using sm3. This is the default configuration.

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to connect to the database:

   ```bash
   gsql -d postgres -p 8000
   ```

   **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

3. View the configured encryption algorithm.

   ```sql
   MogDB=# SHOW password_encryption_type;
    password_encryption_type
   --------------------------
    2
   (1 row)
   ```

   If the command output is **0** or **1**, run the **\q** command to exit the database.

4. Set **gs_guc reload -Z coordinator -D** using a secure encryption algorithm:

   ```bash
   gs_guc reload -N all -I all -c "password_encryption_type=2"
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** To prevent password leakage, when running **CREATE USER/ROLE** to create a database user, do not specify the **UNENCRYPTED** attribute. In this way, the password of the newly created user must be encrypted for storage.

5. Configure password security parameters.

   - Password complexity

     You need to specify a password when initializing a database, creating a user, or modifying a user. The password must meet the complexity check rules (see password_policy). Otherwise, you are prompted to enter the password again.

     - If parameter **password_policy** is set to **1**, the default password complexity rule is used to check passwords.
     - If parameter **password_policy** is set to **0**, the password complexity is not verified. You are not advised to set the parameter to this value because it is insecure. Password complexity is skipped only if the **password_policy** parameter is set to **0** for all MogDB nodes.

     Configure the **password_policy** parameter.

     a. Run the following command to connect to the database:

     ```bash
     gsql -d postgres -p 8000
     ```

     **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

     b. View the current value.

     ```sql
     MogDB=# SHOW password_policy;
      password_policy
     ---------------------
       1
     (1 row)
     ```

     If the command output is not **1**, run the **\q** command to exit the database.

     c. Run the following command to set the parameter to its default value **1**:

     ```bash
     gs_guc reload -N all -I all -c "password_policy=1"
     ```

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:**
     >
     > The password complexity requirements are as follows:
     >
     > - Minimum number of uppercase letters (A-Z) (**password_min_uppercase**)
     >
     > - Minimum number of lowercase letters (a-z) (**password_min_lowercase**)
     >
     > - Minimum number of digits (0-9) (**password_min_digital**)
     >
     > - Minimum number of special characters (**password_min_special**) ([Table 4](#Table-2.9.3.1) lists special characters.)
     >
     > - Minimum password length (**password_min_length**)
     >
     > - Maximum password length (**password_max_length**)
     >
     > - A password must contain at least three types of the characters (uppercase letters, lowercase letters, digits, and special characters)
     >
     > - A password is case insensitive and cannot be the username or the username spelled backwards.
     >
     > - A new password cannot be the current password and the current password spelled backwards.
     >
     > - A weak password is not allowed.
     >
     > - A weak password means that the password complexity is low and is easily cracked. For different users or groups, the definition of a weak password varies. Users need to define the weak password rule.
     >
     > - The password of a weak password dictionary is stored in the gs_global_config system table. When a user needs to set a password during user creation or modification, the system will compare the password set by a user with the passwords in the weak password dictionary. If the set password is defined as a weak password, the system prompts the user of changing the password.
     >
     > - The weak password dictionary is left blank by default. The user can run the following command to create or delete a weak password dictionary.
     >
     >   ```sql
     >   MogDB=# CREATE WEAK PASSWORD DICTIONARY WITH VALUES ('password1'), ('password2');
     >   MogDB=# DROP WEAK PASSWORD DICTIONARY;
     >   ```

   - Password reuse

     An old password can be reused only when it meets the requirements on reuse days (**password_reuse_time**) and reuse times (**password_reuse_max**). [Table 5](#Table-2.9.3.2) lists the parameter configurations.

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The default values of the **password_reuse_time** and **password_reuse_max** parameters are **60** and **0**, respectively. Large values of the two parameters bring higher security. However, if the values of the parameters are set too large, inconvenience may occur. The default values of the two parameters meet the security requirements. You can change the parameter values as needed for higher security.

     Configure the **password_reuse_time** parameter.

     a. Run the following command to connect to the database:

     ```bash
     gsql -d postgres -p 8000
     ```

     **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

     b. View the current value.

     ```sql
     MogDB=# SHOW password_reuse_time;
      password_reuse_time
     ---------------------
      60
     (1 row)
     ```

     If the command output is not **60**, run the **\q** command to exit the database.

     c. Run the following command to set the parameter to its default value **60**:

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** You are not advised to set the parameter to **0**. This value is valid only when **password_reuse_time** for all MogDB nodes is set to **0**.

     ```bash
     gs_guc reload -N all -I all -c "password_reuse_time=60"
     ```

     Configure the **password_reuse_max** parameter.

     a. Run the following command to connect to the database:

     ```bash
     gsql -d postgres -p 8000
     ```

     **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

     b. View the current value.

     ```sql
     MogDB=# SHOW password_reuse_max;
      password_reuse_max
     --------------------
      0
     (1 row)
     ```

     If the command output is not **0**, run the **\q** command to exit the database.

     c. Run the following command to set the parameter to its default value **0**:

     ```bash
     gs_guc reload -N all -I all -c "password_reuse_max = 0"
     ```

   - Password validity period

     A validity period (**password_effect_time**) is set for each database user password. If the password is about to expire (**password_notify_time**), the system displays a message to remind the user to change it upon login.

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Considering the usage and service continuity of a database, the database still allows a user to log in after the password expires. A password change notification is displayed every time the user logs in to the database until the password is changed.

     Configure the **password_effect_time** parameter.

     a. Run the following command to connect to the database:

     ```bash
     gsql -d postgres -p 8000
     ```

     **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

     b. View the current value.

     ```sql
     MogDB=# SHOW password_effect_time;
      password_effect_time
     ----------------------
      90
     (1 row)
     ```

     If the command output is not **90**, run the **\q** command to exit the database.

     c. Run the following command to set the parameter to **90** (**0** is not recommended):

     ```bash
     gs_guc reload -N all -I all -c "password_effect_time = 90"
     ```

     Configure the **password_notify_time** parameter.

     a. Run the following command to connect to the database:

     ```bash
     gsql -d postgres -p 8000
     ```

     **postgres** is the name of the database to be connected, and **8000** is the port number of the database primary node.

     b. View the current value.

     ```sql
     MogDB=# SHOW password_notify_time;
      password_notify_time
     ----------------------
      7
     (1 row)
     ```

     c. If **7** is not displayed, run the following command to set the parameter to **7** (**0** is not recommended):

     ```bash
     gs_guc reload -N all -I all -c "password_notify_time = 7"
     ```

   - Password change

     - During database installation, an OS user with the same name as the initial user is created. The password of the OS user needs to be periodically changed for account security.

       To change the password of user **user1**, run the following command:

     ```sql
       passwd user1
     ```

     Change the password as prompted.

     - System administrators and common users need to periodically change their passwords to prevent the accounts from being stolen.

     For example, to change the password of user **user1**, connect to the database as the system administrator and run the following commands:

     ```sql
       MogDB=# ALTER USER user1 IDENTIFIED BY "1234@abc" REPLACE "5678@def";
     ALTER ROLE
     ```

       > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** **1234@abc** and **5678@def** represent the new password and the original password of user **user1**, respectively. If the new password does not have the required complexity, the change will not take effect.

   - Administrators can change their own and common users' passwords. If common users forget their passwords, they can ask administrators to change the passwords.

       To change the password of user **joe**, run the following command:

        ```sql
        MogDB=# ALTER USER joe IDENTIFIED BY "xxxxxxxx";
        ALTER ROLE
        ```

       > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
       >
       > - System administrators are not allowed to change passwords for each other.
       > - A system administrator can change the password of a common user without being required to provide the common user's old password.
       > - A system administrator can change their own password but is required to provide the old password.

   - Password verification

     Password verification is required when you set the user or role in the current session. If the entered password is inconsistent with the stored password of the user, an error is reported.

     If user **joe** needs to be set, run the following command:

     ```sql
     MogDB=# SET ROLE joe PASSWORD "abc@1234";
     ERROR:  Invalid username/password,set role denied.
     ```

     **Table 4** Special characters<a id="Table-2.9.3.1"> </a>

     | No.  | Character | No.  | Character  | No.  | Character | No.  | Character |
     | :--- | :-------- | :--- | :--------- | :--- | :-------- | :--- | :-------- |
     | 1    | ~         | 9    | *          | 17   | \|        | 25   | &lt;      |
     | 2    | !         | 10   | (          | 18   | [         | 26   | .         |
     | 3    | @         | 11   | )          | 19   | {         | 27   | &gt;      |
     | 4    | #         | 12   | -          | 20   | }         | 28   | /         |
     | 5    | $         | 13   | _          | 21   | ]         | 29   | ?         |
     | 6    | %         | 14   | =          | 22   | ;         | -    | -         |
     | 7    | ^         | 15   | +          | 23   | :         | -    | -         |
     | 8    | &         | 16   | &lt;/p&gt; | 24   | ,         | -    | -         |

     **Table 5** Parameter description for reuse days and reuse times<a id="Table-2.9.3.2"> </a>

     | Parameter                                                    | Value Range                                                  | Description                                                  |
     | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
     | Number of days during which a password cannot be reused (**password_reuse_time**) | Positive number or **0**. The integral part of a positive number indicates the number of days and its decimal part can be converted into hours, minutes, and seconds.<br/>By default, the number of days is set to **60**. | - If the parameter value is changed to a smaller one, new passwords will be checked based on the new parameter value.<br/>- If the parameter value is changed to a larger one (for example, changed from **a** to **b**), the historical passwords before **b** days probably can be reused because these historical passwords may have been deleted. New passwords will be checked based on the new parameter value.<br/>NOTE:<br/>The absolute time is used. Historical passwords are recorded using absolute time and unaffected by local time changes. |
     | Number of consecutive times that a password cannot be reused (**password_reuse_max**) | Positive integer or **0**.The value **0** indicates that the number of consecutive times that a password cannot be reused is not checked. | - If the parameter value is changed to a smaller one, new passwords will be checked based on the new parameter value.<br/>- If the parameter value is changed to a larger one (for example, changed from **a** to **b**), the historical passwords before the last **b** passwords probably can be reused because these historical passwords may have been deleted. New passwords will be checked based on the new parameter value. |

6. Set user password expiration.

   A user with the CREATEROLE permission can create a user with the password expiration feature. The command format is as follows:

   ```sql
   MogDB=# CREATE USER joe PASSWORD "abc@1234" EXPIRED;
      CREATE ROLE
   ```

   A user with the CREATEROLE permission can force a user password to expire or force a user to change the forcibly expired password. The command format is as follows:

      ```sql
      MogDB=# ALTER USER joe PASSWORD EXPIRED;
      ALTER ROLE
      ```

      ```sql
      MogDB=# ALTER USER joe PASSWORD "abc@2345" EXPIRED;
      ALTER ROLE
      ```

      > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
      >
      > - After logging in to the database, a user with the password expiration feature is prompted to change the password when the user tries to perform a simple or extended query. The user can then execute the statement after changing the password.
      > - Only initial users, system administrators, or users who have permission to create users can set password expiration. Password expiration can be set by system administrators for themselves and other system administrators. Additionally, the password expiration cannot be set for initial users.
