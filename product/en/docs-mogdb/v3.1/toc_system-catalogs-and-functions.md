<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# MogDB Documentation 3.1

## System Catalogs and Functions

+ System Catalogs and System Views
  + [Overview of System Catalogs and System Views](./reference-guide/system-catalogs-and-system-views/overview-of-system-catalogs-and-system-views.md)
  + System Catalogs
    + [GS_ASP](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_ASP.md)
    + [GS_AUDITING_POLICY](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_AUDITING_POLICY.md)
    + [GS_AUDITING_POLICY_ACCESS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_AUDITING_POLICY_ACCESS.md)
    + [GS_AUDITING_POLICY_FILTERS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_AUDITING_POLICY_FILTERS.md)
    + [GS_AUDITING_POLICY_PRIVILEGES](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_AUDITING_POLICY_PRIVILEGES.md)
    + [GS_CLIENT_GLOBAL_KEYS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_CLIENT_GLOBAL_KEYS.md)
    + [GS_CLIENT_GLOBAL_KEYS_ARGS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_CLIENT_GLOBAL_KEYS_ARGS.md)
    + [GS_COLUMN_KEYS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_COLUMN_KEYS.md)
    + [GS_COLUMN_KEYS_ARGS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_COLUMN_KEYS_ARGS.md)
    + [GS_DB_PRIVILEGE](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_DB_PRIVILEGE.md)
    + [GS_ENCRYPTED_COLUMNS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_ENCRYPTED_COLUMNS.md)
    + [GS_ENCRYPTED_PROC](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_ENCRYPTED_PROC.md)
    + [GS_GLOBAL_CHAIN](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_GLOBAL_CHAIN.md)
    + [GS_GLOBAL_CONFIG](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_GLOBAL_CONFIG.md)
    + [GS_MASKING_POLICY](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MASKING_POLICY.md)
    + [GS_MASKING_POLICY_ACTIONS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MASKING_POLICY_ACTIONS.md)
    + [GS_MASKING_POLICY_FILTERS](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MASKING_POLICY_FILTERS.md)
    + [GS_MATVIEW](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MATVIEW.md)
    + [GS_MATVIEW_DEPENDENCY](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MATVIEW_DEPENDENCY.md)
    + [GS_MODEL_WAREHOUSE](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_MODEL_WAREHOUSE.md)
    + [GS_OPT_MODEL](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)
    + [GS_PACKAGE](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_PACKAGE.md)
    + [GS_POLICY_LABEL](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_POLICY_LABEL.md)
    + [GS_RECYCLEBIN](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_RECYCLEBIN.md)
    + [GS_TXN_SNAPSHOT](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_TXN_SNAPSHOT.md)
    + [GS_UID](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_UID.md)
    + [GS_WLM_EC_OPERATOR_INFO](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_EC_OPERATOR_INFO.md)
    + [GS_WLM_INSTANCE_HISTORY](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_INSTANCE_HISTORY.md)
    + [GS_WLM_OPERATOR_INFO](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_OPERATOR_INFO.md)
    + [GS_WLM_PLAN_ENCODING_TABLE](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_PLAN_ENCODING_TABLE.md)
    + [GS_WLM_PLAN_OPERATOR_INFO](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_PLAN_OPERATOR_INFO.md)
    + [GS_WLM_SESSION_QUERY_INFO_ALL](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_SESSION_QUERY_INFO_ALL.md)
    + [GS_WLM_USER_RESOURCE_HISTORY](./reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_USER_RESOURCE_HISTORY.md)
    + [PG_AGGREGATE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AGGREGATE.md)
    + [PG_AM](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AM.md)
    + [PG_AMOP](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AMOP.md)
    + [PG_AMPROC](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AMPROC.md)
    + [PG_APP_WORKLOADGROUP_MAPPING](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_APP_WORKLOADGROUP_MAPPING.md)
    + [PG_ATTRDEF](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_ATTRDEF.md)
    + [PG_ATTRIBUTE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_ATTRIBUTE.md)
    + [PG_AUTH_HISTORY](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AUTH_HISTORY.md)
    + [PG_AUTH_MEMBERS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AUTH_MEMBERS.md)
    + [PG_AUTHID](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_AUTHID.md)
    + [PG_CAST](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_CAST.md)
    + [PG_CLASS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_CLASS.md)
    + [PG_COLLATION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_COLLATION.md)
    + [PG_CONSTRAINT](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_CONSTRAINT.md)
    + [PG_CONVERSION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_CONVERSION.md)
    + [PG_DATABASE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DATABASE.md)
    + [PG_DB_ROLE_SETTING](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DB_ROLE_SETTING.md)
    + [PG_DEFAULT_ACL](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DEFAULT_ACL.md)
    + [PG_DEPEND](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DEPEND.md)
    + [PG_DESCRIPTION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DESCRIPTION.md)
    + [PG_DIRECTORY](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_DIRECTORY.md)
    + [PG_ENUM](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_ENUM.md)
    + [PG_EXTENSION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_EXTENSION.md)
    + [PG_EXTENSION_DATA_SOURCE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_EXTENSION_DATA_SOURCE.md)
    + [PG_FOREIGN_DATA_WRAPPER](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_FOREIGN_DATA_WRAPPER.md)
    + [PG_FOREIGN_SERVER](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_FOREIGN_SERVER.md)
    + [PG_FOREIGN_TABLE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_FOREIGN_TABLE.md)
    + [PG_HASHBUCKET](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_HASHBUCKET.md)
    + [PG_INDEX](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_INDEX.md)
    + [PG_INHERITS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_INHERITS.md)
    + [PG_JOB](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_JOB.md)
    + [PG_JOB_PROC](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_JOB_PROC.md)
    + [PG_LANGUAGE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_LANGUAGE.md)
    + [PG_LARGEOBJECT](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_LARGEOBJECT.md)
    + [PG_LARGEOBJECT_METADATA](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_LARGEOBJECT_METADATA.md)
    + [PG_NAMESPACE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_NAMESPACE.md)
    + [PG_OBJECT](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_OBJECT.md)
    + [PG_OPCLASS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_OPCLASS.md)
    + [PG_OPERATOR](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_OPERATOR.md)
    + [PG_OPFAMILY](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_OPFAMILY.md)
    + [PG_PARTITION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_PARTITION.md)
    + [PG_PLTEMPLATE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_PLTEMPLATE.md)
    + [PG_PROC](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_PROC.md)
    + [PG_PUBLICATION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_PUBLICATION.md)
    + [PG_PUBLICATION_REL](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_PUBLICATION_REL.md)
    + [PG_RANGE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_RANGE.md)
    + [PG_REPLICATION_ORIGIN](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_REPLICATION_ORIGIN.md)
    + [PG_RESOURCE_POOL](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_RESOURCE_POOL.md)
    + [PG_REWRITE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_REWRITE.md)
    + [PG_RLSPOLICY](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_RLSPOLICY.md)
    + [PG_SECLABEL](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SECLABEL.md)
    + [PG_SET](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SET.md)
    + [PG_SHDEPEND](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SHDEPEND.md)
    + [PG_SHDESCRIPTION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SHDESCRIPTION.md)
    + [PG_SHSECLABEL](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SHSECLABEL.md)
    + [PG_STATISTIC](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_STATISTIC.md)
    + [PG_STATISTIC_EXT](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_STATISTIC_EXT.md)
    + [PG_SUBSCRIPTION](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SUBSCRIPTION.md)
    + [PG_SYNONYM](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_SYNONYM.md)
    + [PG_TABLESPACE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TABLESPACE.md)
    + [PG_TRIGGER](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TRIGGER.md)
    + [PG_TS_CONFIG](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_CONFIG.md)
    + [PG_TS_CONFIG_MAP](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_CONFIG_MAP.md)
    + [PG_TS_DICT](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_DICT.md)
    + [PG_TS_PARSER](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_PARSER.md)
    + [PG_TS_TEMPLATE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_TEMPLATE.md)
    + [PG_TYPE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TYPE.md)
    + [PG_USER_MAPPING](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_USER_MAPPING.md)
    + [PG_USER_STATUS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_USER_STATUS.md)
    + [PG_WORKLOAD_GROUP](./reference-guide/system-catalogs-and-system-views/system-catalogs/PG_WORKLOAD_GROUP.md)
    + [PGXC_CLASS](./reference-guide/system-catalogs-and-system-views/system-catalogs/PGXC_CLASS.md)
    + [PGXC_GROUP](./reference-guide/system-catalogs-and-system-views/system-catalogs/PGXC_GROUP.md)
    + [PGXC_NODE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PGXC_NODE.md)
    + [PGXC_SLICE](./reference-guide/system-catalogs-and-system-views/system-catalogs/PGXC_SLICE.md)
    + [PLAN_TABLE_DATA](./reference-guide/system-catalogs-and-system-views/system-catalogs/PLAN_TABLE_DATA.md)
    + [STATEMENT_HISTORY](./reference-guide/system-catalogs-and-system-views/system-catalogs/STATEMENT_HISTORY.md)
  + System Views
    + [DV_SESSION_LONGOPS](./reference-guide/system-catalogs-and-system-views/system-views/DV_SESSION_LONGOPS.md)
    + [DV_SESSIONS](./reference-guide/system-catalogs-and-system-views/system-views/DV_SESSIONS.md)
    + [GET_GLOBAL_PREPARED_XACTS(Discarded)](./reference-guide/system-catalogs-and-system-views/system-views/GET_GLOBAL_PREPARED_XACTS.md)
    + [GS_AUDITING](./reference-guide/system-catalogs-and-system-views/system-views/GS_AUDITING.md)
    + [GS_AUDITING_ACCESS](./reference-guide/system-catalogs-and-system-views/system-views/GS_AUDITING_ACCESS.md)
    + [GS_AUDITING_PRIVILEGE](./reference-guide/system-catalogs-and-system-views/system-views/GS_AUDITING_PRIVILEGE.md)
    + [GS_ASYNC_SUBMIT_SESSIONS_STATUS](./reference-guide/system-catalogs-and-system-views/system-views/GS_ASYNC_SUBMIT_SESSIONS_STATUS.md)
    + [GS_CLUSTER_RESOURCE_INFO](./reference-guide/system-catalogs-and-system-views/system-views/GS_CLUSTER_RESOURCE_INFO.md)
    + [GS_COMPRESSION](./reference-guide/system-catalogs-and-system-views/system-views/GS_COMPRESSION.md)
    + [GS_DB_PRIVILEGES](./reference-guide/system-catalogs-and-system-views/system-views/GS_DB_PRIVILEGES.md)
    + [GS_FILE_STAT](./reference-guide/system-catalogs-and-system-views/system-views/GS_FILE_STAT.md)
    + [GS_GSC_MEMORY_DETAIL](./reference-guide/system-catalogs-and-system-views/system-views/GS_GSC_MEMORY_DETAIL.md)
    + [GS_INSTANCE_TIME](./reference-guide/system-catalogs-and-system-views/system-views/GS_INSTANCE_TIME.md)
    + [GS_LABELS](./reference-guide/system-catalogs-and-system-views/system-views/GS_LABELS.md)
    + [GS_LSC_MEMORY_DETAIL](./reference-guide/system-catalogs-and-system-views/system-views/GS_LSC_MEMORY_DETAIL.md)
    + [GS_MASKING](./reference-guide/system-catalogs-and-system-views/system-views/GS_MASKING.md)
    + [GS_MATVIEWS](./reference-guide/system-catalogs-and-system-views/system-views/GS_MATVIEWS.md)
    + [GS_OS_RUN_INFO](./reference-guide/system-catalogs-and-system-views/system-views/GS_OS_RUN_INFO.md)
    + [GS_REDO_STAT](./reference-guide/system-catalogs-and-system-views/system-views/GS_REDO_STAT.md)
    + [GS_SESSION_CPU_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_CPU_STATISTICS.md)
    + [GS_SESSION_MEMORY](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY.md)
    + [GS_SESSION_MEMORY_CONTEXT](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_CONTEXT.md)
    + [GS_SESSION_MEMORY_DETAIL](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_DETAIL.md)
    + [GS_SESSION_MEMORY_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_STATISTICS.md)
    + [GS_SESSION_STAT](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_STAT.md)
    + [GS_SESSION_TIME](./reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_TIME.md)
    + [GS_SQL_COUNT](./reference-guide/system-catalogs-and-system-views/system-views/GS_SQL_COUNT.md)
    + [GS_STAT_SESSION_CU](./reference-guide/system-catalogs-and-system-views/system-views/GS_STAT_SESSION_CU.md)
    + [GS_THREAD_MEMORY_CONTEXT](./reference-guide/system-catalogs-and-system-views/system-views/GS_THREAD_MEMORY_CONTEXT.md)
    + [GS_TOTAL_MEMORY_DETAIL](./reference-guide/system-catalogs-and-system-views/system-views/GS_TOTAL_MEMORY_DETAIL.md)
    + [GS_WLM_CGROUP_INFO](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_CGROUP_INFO.md)
    + [GS_WLM_EC_OPERATOR_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_EC_OPERATOR_STATISTICS.md)
    + [GS_WLM_OPERATOR_HISTORY](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_OPERATOR_HISTORY.md)
    + [GS_WLM_OPERATOR_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_OPERATOR_STATISTICS.md)
    + [GS_WLM_PLAN_OPERATOR_HISTORY](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_PLAN_OPERATOR_HISTORY.md)
    + [GS_WLM_REBUILD_USER_RESOURCE_POOL](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_REBUILD_USER_RESOURCE_POOL.md)
    + [GS_WLM_RESOURCE_POOL](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_RESOURCE_POOL.md)
    + [GS_WLM_SESSION_HISTORY](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_SESSION_HISTORY.md)
    + [GS_WLM_SESSION_INFO](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_SESSION_INFO.md)
    + [GS_WLM_SESSION_INFO_ALL](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_SESSION_INFO_ALL.md)
    + [GS_WLM_SESSION_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_SESSION_STATISTICS.md)
    + [GS_WLM_USER_INFO](./reference-guide/system-catalogs-and-system-views/system-views/GS_WLM_USER_INFO.md)
    + [GS_WRITE_TERM_LOG](./reference-guide/system-catalogs-and-system-views/system-views/GS_WRITE_TERM_LOG.md)
    + [MPP_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/MPP_TABLES.md)
    + [PG_AVAILABLE_EXTENSION_VERSIONS](./reference-guide/system-catalogs-and-system-views/system-views/PG_AVAILABLE_EXTENSION_VERSIONS.md)
    + [PG_AVAILABLE_EXTENSIONS](./reference-guide/system-catalogs-and-system-views/system-views/PG_AVAILABLE_EXTENSIONS.md)
    + [PG_COMM_DELAY](./reference-guide/system-catalogs-and-system-views/system-views/PG_COMM_DELAY.md)
    + [PG_COMM_RECV_STREAM](./reference-guide/system-catalogs-and-system-views/system-views/PG_COMM_RECV_STREAM.md)
    + [PG_COMM_SEND_STREAM](./reference-guide/system-catalogs-and-system-views/system-views/PG_COMM_SEND_STREAM.md)
    + [PG_COMM_STATUS](./reference-guide/system-catalogs-and-system-views/system-views/PG_COMM_STATUS.md)
    + [PG_CONTROL_GROUP_CONFIG](./reference-guide/system-catalogs-and-system-views/system-views/PG_CONTROL_GROUP_CONFIG.md)
    + [PG_CURSORS](./reference-guide/system-catalogs-and-system-views/system-views/PG_CURSORS.md)
    + [PG_EXT_STATS](./reference-guide/system-catalogs-and-system-views/system-views/PG_EXT_STATS.md)
    + [PG_GET_INVALID_BACKENDS](./reference-guide/system-catalogs-and-system-views/system-views/PG_GET_INVALID_BACKENDS.md)
    + [PG_GET_SENDERS_CATCHUP_TIME](./reference-guide/system-catalogs-and-system-views/system-views/PG_GET_SENDERS_CATCHUP_TIME.md)
    + [PG_GROUP](./reference-guide/system-catalogs-and-system-views/system-views/PG_GROUP.md)
    + [PG_GTT_ATTACHED_PIDS](./reference-guide/system-catalogs-and-system-views/system-views/PG_GTT_ATTACHED_PIDS.md)
    + [PG_GTT_RELSTATS](./reference-guide/system-catalogs-and-system-views/system-views/PG_GTT_RELSTATS.md)
    + [PG_GTT_STATS](./reference-guide/system-catalogs-and-system-views/system-views/PG_GTT_STATS.md)
    + [PG_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_INDEXES.md)
    + [PG_LOCKS](./reference-guide/system-catalogs-and-system-views/system-views/PG_LOCKS.md)
    + [PG_NODE_ENV](./reference-guide/system-catalogs-and-system-views/system-views/PG_NODE_ENV.md)
    + [PG_OS_THREADS](./reference-guide/system-catalogs-and-system-views/system-views/PG_OS_THREADS.md)
    + [PG_PREPARED_STATEMENTS](./reference-guide/system-catalogs-and-system-views/system-views/PG_PREPARED_STATEMENTS.md)
    + [PG_PREPARED_XACTS](./reference-guide/system-catalogs-and-system-views/system-views/PG_PREPARED_XACTS.md)
    + [PG_PUBLICATION_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_PUBLICATION_TABLES.md)
    + [PG_REPLICATION_ORIGIN_STATUS](./reference-guide/system-catalogs-and-system-views/system-views/PG_REPLICATION_ORIGIN_STATUS.md)
    + [PG_REPLICATION_SLOTS](./reference-guide/system-catalogs-and-system-views/system-views/PG_REPLICATION_SLOTS.md)
    + [PG_RLSPOLICIES](./reference-guide/system-catalogs-and-system-views/system-views/PG_RLSPOLICIES.md)
    + [PG_ROLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_ROLES.md)
    + [PG_RULES](./reference-guide/system-catalogs-and-system-views/system-views/PG_RULES.md)
    + [PG_RUNNING_XACTS](./reference-guide/system-catalogs-and-system-views/system-views/PG_RUNNING_XACTS.md)
    + [PG_SECLABELS](./reference-guide/system-catalogs-and-system-views/system-views/PG_SECLABELS.md)
    + [PG_SESSION_IOSTAT](./reference-guide/system-catalogs-and-system-views/system-views/PG_SESSION_IOSTAT.md)
    + [PG_SESSION_WLMSTAT](./reference-guide/system-catalogs-and-system-views/system-views/PG_SESSION_WLMSTAT.md)
    + [PG_SETTINGS](./reference-guide/system-catalogs-and-system-views/system-views/PG_SETTINGS.md)
    + [PG_SHADOW](./reference-guide/system-catalogs-and-system-views/system-views/PG_SHADOW.md)
    + [PG_STAT_ACTIVITY](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_ACTIVITY.md)
    + [PG_STAT_ACTIVITY_NG](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_ACTIVITY_NG.md)
    + [PG_STAT_ALL_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_ALL_INDEXES.md)
    + [PG_STAT_ALL_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_ALL_TABLES.md)
    + [PG_STAT_BAD_BLOCK](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_BAD_BLOCK.md)
    + [PG_STAT_BGWRITER](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_BGWRITER.md)
    + [PG_STAT_DATABASE](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_DATABASE.md)
    + [PG_STAT_DATABASE_CONFLICTS](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_DATABASE_CONFLICTS.md)
    + [PG_STAT_REPLICATION](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_REPLICATION.md)
    + [PG_STAT_SUBSCRIPTION](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_SUBSCRIPTION.md)
    + [PG_STAT_SYS_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_SYS_INDEXES.md)
    + [PG_STAT_SYS_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_SYS_TABLES.md)
    + [PG_STAT_USER_FUNCTIONS](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_USER_FUNCTIONS.md)
    + [PG_STAT_USER_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_USER_INDEXES.md)
    + [PG_STAT_USER_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_USER_TABLES.md)
    + [PG_STAT_XACT_ALL_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_XACT_ALL_TABLES.md)
    + [PG_STAT_XACT_SYS_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_XACT_SYS_TABLES.md)
    + [PG_STAT_XACT_USER_FUNCTIONS](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_XACT_USER_FUNCTIONS.md)
    + [PG_STAT_XACT_USER_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STAT_XACT_USER_TABLES.md)
    + [PG_STATIO_ALL_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_ALL_INDEXES.md)
    + [PG_STATIO_ALL_SEQUENCES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_ALL_SEQUENCES.md)
    + [PG_STATIO_ALL_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_ALL_TABLES.md)
    + [PG_STATIO_SYS_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_SYS_INDEXES.md)
    + [PG_STATIO_SYS_SEQUENCES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_SYS_SEQUENCES.md)
    + [PG_STATIO_SYS_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_SYS_TABLES.md)
    + [PG_STATIO_USER_INDEXES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_USER_INDEXES.md)
    + [PG_STATIO_USER_SEQUENCES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_USER_SEQUENCES.md)
    + [PG_STATIO_USER_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATIO_USER_TABLES.md)
    + [PG_STATS](./reference-guide/system-catalogs-and-system-views/system-views/PG_STATS.md)
    + [PG_TABLES](./reference-guide/system-catalogs-and-system-views/system-views/PG_TABLES.md)
    + [PG_TDE_INFO](./reference-guide/system-catalogs-and-system-views/system-views/PG_TDE_INFO.md)
    + [PG_THREAD_WAIT_STATUS](./reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md)
    + [PG_TIMEZONE_ABBREVS](./reference-guide/system-catalogs-and-system-views/system-views/PG_TIMEZONE_ABBREVS.md)
    + [PG_TIMEZONE_NAMES](./reference-guide/system-catalogs-and-system-views/system-views/PG_TIMEZONE_NAMES.md)
    + [PG_TOTAL_MEMORY_DETAIL](./reference-guide/system-catalogs-and-system-views/system-views/PG_TOTAL_MEMORY_DETAIL.md)
    + [PG_TOTAL_USER_RESOURCE_INFO](./reference-guide/system-catalogs-and-system-views/system-views/PG_TOTAL_USER_RESOURCE_INFO.md)
    + [PG_TOTAL_USER_RESOURCE_INFO_OID](./reference-guide/system-catalogs-and-system-views/system-views/PG_TOTAL_USER_RESOURCE_INFO_OID.md)
    + [PG_USER](./reference-guide/system-catalogs-and-system-views/system-views/PG_USER.md)
    + [PG_USER_MAPPINGS](./reference-guide/system-catalogs-and-system-views/system-views/PG_USER_MAPPINGS.md)
    + [PG_VARIABLE_INFO](./reference-guide/system-catalogs-and-system-views/system-views/PG_VARIABLE_INFO.md)
    + [PG_VIEWS](./reference-guide/system-catalogs-and-system-views/system-views/PG_VIEWS.md)
    + [PG_WLM_STATISTICS](./reference-guide/system-catalogs-and-system-views/system-views/PG_WLM_STATISTICS.md)
    + [PGXC_PREPARED_XACTS](./reference-guide/system-catalogs-and-system-views/system-views/PGXC_PREPARED_XACTS.md)
    + [PLAN_TABLE](./reference-guide/system-catalogs-and-system-views/system-views/PLAN_TABLE.md)
+ Functions and Operators
  + [Logical Operators](./reference-guide/functions-and-operators/1-logical-operators.md)
  + [Comparison Operators](./reference-guide/functions-and-operators/2-comparison-operators.md)
  + [Character Processing Functions and Operators](./reference-guide/functions-and-operators/3-character-processing-functions-and-operators.md)
  + [Binary String Functions and Operators](./reference-guide/functions-and-operators/4-binary-string-functions-and-operators.md)
  + [Bit String Functions and Operators](./reference-guide/functions-and-operators/5-bit-string-functions-and-operators.md)
  + [Mode Matching Operators](./reference-guide/functions-and-operators/6-mode-matching-operators.md)
  + [Mathematical Functions and Operators](./reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md)
  + [Date and Time Processing Functions and Operators](./reference-guide/functions-and-operators/8-date-and-time-processing-functions-and-operators.md)
  + [Type Conversion Functions](./reference-guide/functions-and-operators/9-type-conversion-functions.md)
  + [Geometric Functions and Operators](./reference-guide/functions-and-operators/10-geometric-functions-and-operators.md)
  + [Network Address Functions and Operators](./reference-guide/functions-and-operators/11-network-address-functions-and-operators.md)
  + [Text Search Functions and Operators](./reference-guide/functions-and-operators/12-text-search-functions-and-operators.md)
  + [JSON/JSONB Functions and Operators](./reference-guide/functions-and-operators/13-json-functions.md)
  + [HLL Functions and Operators](./reference-guide/functions-and-operators/13.1-hll-functions-and-operators.md)
  + [SEQUENCE Functions](./reference-guide/functions-and-operators/14-sequence-functions.md)
  + [Array Functions and Operators](./reference-guide/functions-and-operators/15-array-functions-and-operators.md)
  + [Range Functions and Operators](./reference-guide/functions-and-operators/16-range-functions-and-operators.md)
  + [Aggregate Functions](./reference-guide/functions-and-operators/17-aggregate-functions.md)
  + [Window Functions(Analysis Functions)](./reference-guide/functions-and-operators/18-window-functions.md)
  + [Security Functions](./reference-guide/functions-and-operators/19-security-functions.md)
  + [Ledger Database Functions](./reference-guide/functions-and-operators/20.1-ledger-database-functions.md)
  + [Encrypted Equality Functions](./reference-guide/functions-and-operators/20-encrypted-equality-functions.md)
  + [Set Returning Functions](./reference-guide/functions-and-operators/21-set-returning-functions.md)
  + [Conditional Expression Functions](./reference-guide/functions-and-operators/22-conditional-expressions-functions.md)
  + [System Information Functions](./reference-guide/functions-and-operators/23-system-information-functions.md)
  + System Administration Functions
    + [Configuration Settings Functions](./reference-guide/functions-and-operators/24-system-management-functions/1-configuration-settings-functions.md)
    + [Universal File Access Functions](./reference-guide/functions-and-operators/24-system-management-functions/2-universal-file-access-functions.md)
    + [Server Signal Functions](./reference-guide/functions-and-operators/24-system-management-functions/3-server-signal-functions.md)
    + [Backup and Restoration Control Functions](./reference-guide/functions-and-operators/24-system-management-functions/4-backup-and-restoration-control-functions.md)
    + [Snapshot Synchronization Functions](./reference-guide/functions-and-operators/24-system-management-functions/5-snapshot-synchronization-functions.md)
    + [Database Object Functions](./reference-guide/functions-and-operators/24-system-management-functions/6-database-object-functions.md)
    + [Advisory Lock Functions](./reference-guide/functions-and-operators/24-system-management-functions/7-advisory-lock-functions.md)
    + [Logical Replication Functions](./reference-guide/functions-and-operators/24-system-management-functions/8-logical-replication-functions.md)
    + [Segment-Page Storage Functions](./reference-guide/functions-and-operators/24-system-management-functions/9.1-segment-page-storage-functions.md)
    + [Other Functions](./reference-guide/functions-and-operators/24-system-management-functions/9-other-functions.md)
    + [Undo System Functions](./reference-guide/functions-and-operators/24-system-management-functions/10-undo-system-functions.md)
  + [Statistics Information Functions](./reference-guide/functions-and-operators/25-statistics-information-functions.md)
  + [Trigger Functions](./reference-guide/functions-and-operators/26-trigger-functions.md)
  + [Hash Function](./reference-guide/functions-and-operators/27.1-hash-function.md)
  + [Prompt Message Function](./reference-guide/functions-and-operators/27.2-prompt-message-function.md)
  + [Global Temporary Table Functions](./reference-guide/functions-and-operators/27-global-temporary-table-functions.md)
  + [Fault Injection System Function](./reference-guide/functions-and-operators/28.1-fault-injection-system-function.md)
  + [AI Feature Functions](./reference-guide/functions-and-operators/28-ai-feature-functions.md)
  + [Dynamic Data Masking Functions](./reference-guide/functions-and-operators/29.1-dynamic-data-masking-functions.md)
  + [Other System Functions](./reference-guide/functions-and-operators/29-other-system-functions.md)
  + [Internal Functions](./reference-guide/functions-and-operators/30-internal-functions.md)
  + [Global SysCache Feature Functions](/reference-guide/functions-and-operators/32-global-syscache-feature-functions.md)
  + [Data Damage Detection and Repair Functions](/reference-guide/functions-and-operators/33-data-damage-detection-and-repair-functions.md)
  + [Obsolete Functions](./reference-guide/functions-and-operators/31-obsolete-functions.md)
