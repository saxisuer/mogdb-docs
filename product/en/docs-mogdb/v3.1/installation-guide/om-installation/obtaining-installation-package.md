---
title: Obtaining the Installation Package
summary: Obtaining the Installation Package
author: Zhang Cuiping
date: 2021-06-11
---

## Obtaining the Installation Package

You can obtain the installation package from the MogDB website.

MogDB website: <https://www.mogdb.io/en/downloads/mogdb/>

**Procedure**

This section takes the openEuler OS as an example to describe how to obtain the target installation package.

1. Upload the downloaded standard installation package and decompress it to obtain the single-node installation package.

   ```bash
   tar -xvf MogDB-x.x.x-openEuler-arm64.tar.gz
   ```

   > **Note**: The MogDB website provides the standard installation package **MogDB-x.x.x-openEuler-arm64.tar**. You need to decompress it to obtain the single-node installation package **MogDB-x.x.x-openEuler-64bit.tar.gz**.

2. Check the installation package.

   Decompress the installation package and check whether the installation directory and files are complete. Run the following commands in the directory where the installation package is stored:

   ```bash
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.gz
   ls -1b
   ```

   Run the **ls** command to display information similar to the following:

   ```bash
   total 90296
   drwx------ 3 root root     4096 Mar 31 21:18 bin
   drwx------ 3 root root     4096 Mar 31 21:18 etc
   drwx------ 3 root root     4096 Mar 31 21:18 include
   drwx------ 4 root root     4096 Mar 31 21:18 jre
   drwx------ 5 root root     4096 Mar 31 21:18 lib
   -rw------- 1 root root 92427499 Apr  1 09:43 MogDB-x.x.x-openEuler-64bit.tar.gz
   drwx------ 5 root root     4096 Mar 31 21:18 share
   drwx------ 2 root root     4096 Mar 31 21:18 simpleInstall
   -rw------- 1 root root       32 Mar 31 21:18 version.cfg
   ```