---
title: Installation Overview
summary: Installation Overview
author: Zhang Cuiping
date: 2021-04-02
---

# Installation Overview

MogDB can be deployed in standalone or in HA mode. In standalone mode, multiple database instances can be deployed on a single host. However, this mode is not recommended for data security purposes. In the HA deployment mode, one primary server and at least one standby server are supported, and a maximum of eight standby servers are supported.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**Note**:
>
> - During OM installation, only one database system can be deployed on a physical server. To deploy multiple database systems on a single server, you are advised to install MogDB using [PTK-based installation](../ptk-based-installation.md).
>
> - MogDB supports PTK and OM installation modes. Currently, MogDB provides technical support for only PTK installation problems. 