---
title: PTK-based Installation
summary: PTK-based Installation
author: Yao Qian
date: 2022-06-02
---

# PTK-based Installation

This document introduces how to install MogDB using PTK.

PTK (Provisioning Toolkit) is a software installation, and operation and maintenance tool developed for MogDB to facilitate the installation and deployment of the MogDB database.

You can run the following command to check operating systems where MogDB can be installed using PTK.

```bash
[root@hostname ~]# ptk candidate os
  software |               version                
-----------+--------------------------------------
  OS       | CentOS 7 (x86_64)                    
           | CentOS 8 (arm64)                     
           | CentOS 8 (x86_64)                    
           | EulerOS 2 (arm64)                    
           | EulerOS 2 (x86_64)                   
           | Kylin V10 (arm64)                    
           | Kylin V10 (x86_64)                   
           | NeoKylin V7 (x86_64)                 
           | Oracle Linux 7 (x86_64)              
           | Oracle Linux 8 (x86_64)              
           | Red Hat Enterprise Linux 7 (x86_64)  
           | Red Hat Enterprise Linux 8 (x86_64)  
           | Rocky Linux 7 (x86_64)               
           | Rocky Linux 8 (x86_64)               
           | SLES 12 (arm64)                      
           | UOS 20 (arm64)                       
           | UOS 20 (x86_64)                      
           | Ubuntu 18 (x86_64)                   
           | openEuler 20 (arm64)                 
           | openEuler 20 (x86_64)                
           | openEuler 22 (arm64)                 
           | openEuler 22 (x86_64)  
```

> **Note**: The PTK tool itself can run on a variety of operating systems, including Linux, macOS, and Windows. However, since MogDB currently only runs on Linux, you need to ensure that the server on which you want to run the MogDB database is a Linux operating system.

## Installation Preparation

For details about environment requirements and operating system configuration, see [Environment Requirement](installation-preparation/environment-requirement.md)和[Operating System Configuration](installation-preparation/os-configuration.md).

## PTK Installation

For details, see [PTK Installation](https://docs.mogdb.io/en/ptk/v0.4/install).

## Installing MogDB Using PTK

### Prepare a Topology Configuration File

PTK installation requires the user to provide the configuration file **config.yaml**. PTK supports single-node installation and multi-node installation. The following uses the installation of only one node and one primary and standby node as an example.

**Single-node installation**

```yaml
# config.yaml
global:
    cluster_name: mogdb1
    user: omm
    group: omm
    base_dir: /opt/mogdb
db_servers:
    - host: 127.0.0.1
      db_port: 26000
```

If default settings are used in the configuration file, PTK will perform the following operations:

* MogDB is installed on the local server.
* The operating system user for running the database is omm, the user group name is omm, and the user does not have a default password.
* The database is installed in the **/opt/mogdb** directory where four directories **app**, **data**, **log**, and **tool** will be created for storing database software, data files, database logs, and database-related tools.
* The listening port of the database is 26000.

**Primary/standby node installation**

```yaml
# config.yaml
global:
    cluster_name: mogdb_cluster1
    user: omm
    group: omm
    base_dir: /opt/mogdb
db_servers:
    - host: 192.168.0.1
      db_port: 26000
      role: primary
      ssh_option:
        port: 22
        user: root
        password: [Enter the SSH login password]
    - host: 192.168.0.2
      db_port: 26000
      role: standby
      ssh_option:
        port: 22
        user: root
        password: [Enter the SSH login password]
```

### Perform System Check on the Local Server

```shell
ptk checkos -f config.yaml
```

Make sure the check result includes `OK` or `Warning`. If `Abnormal` occurs, the users needs to modify system parameters based on logs.

### Install MogDB

```shell
ptk install -f config.yaml
```

The MogDB of the latest version will be installed by default. You can check the version from the download page of the official website. During the installation, you will be prompted to enter the initial password of the database user, and make sure that you take a note of the password. After automatic installation of MogDB using PTK, the database instance will be started.

PTK supports customization of the installation package and manual downloading of the installation package. For example, run the following command to install MogDB using the installation package in the current directory.

```shell
ptk install -f config.yaml --pkg ./MogDB-3.1.0-openEuler-arm64.tar.gz
```

After successful installation, you can check the instance information by running the `ptk ls`command.

```bash
[root@hostname]# ptk ls
  cluster_name |     instances      | user |    data_dir     | db_version   
---------------+--------------------+------+-----------------+--------------
  mogdb1       | 172.16.0.127:26000 | omm  | /opt/mogdb/data | MogDB-3.1.0  
```

### Access MogDB

```shell
su - omm
gsql -d postgres -p 26000
```

## Uninstalling MogDB Using PTK

> **Note**: Once a database is uninstalled, it cannot be recovered. Please execute operation with caution.

You can run the following command to uninstall the database.

```bash
ptk uninstall (-f CONFIG.YAML|--name CLUSTER_NAME)
```

Before uninstallation, PTK questions you in an interactive way to confirm the topology information of the cluster to be deleted, whether to delete the system user, and whether to delete the database data. Please confirm your answer to each question to avoid data loss due to incorrect operations.

Before uninstallation, if you confirm deletion of the data directory, PTK will delete only the data directory and will not delete its parent directory. You need to manually delete the parent directory.

If uninstallation is successful, information similar to the following is displayed. 

```bash
[root@hostname]# ptk uninstall -n mogdb1
=============================
global:
  cluster_name: mogdb1
  user: omm
  group: omm
  app_dir: /opt/mogdb/app
  data_dir: /opt/mogdb/data
  log_dir: /opt/mogdb/log
  tool_dir: /opt/mogdb/tool
  tmp_dir: /opt/mogdb/tmp
db_servers:
- host: 172.16.0.127
  db_port: 26000
  role: primary
  az_name: AZ1
  az_priority: 1

=============================
Do you really want to uninstall this cluster? Please confirm carefully[Y|Yes](default=N) y
Do you want to delete db data '/opt/mogdb/data'?[Y|Yes](default=N) y
Do you want to delete user 'omm'?[Y|Yes](default=N) y
INFO[2022-07-08T10:27:42.820] check db dirs owner                           host=172.16.0.127
INFO[2022-07-08T10:27:42.828] clean crontab                                 host=172.16.0.127
INFO[2022-07-08T10:27:42.894] kill omm's processes                          host=172.16.0.127
INFO[2022-07-08T10:27:42.970] remove files /opt/mogdb/app,/opt/mogdb/tool,/opt/mogdb/cm,/opt/mogdb/tmp,/opt/mogdb/data,/opt/mogdb/log  host=172.16.0.127
INFO[2022-07-08T10:27:43.073] delete os user omm                            host=172.16.0.127
INFO[2022-07-08T10:27:43.213] clearing /etc/cron.allow                      host=172.16.0.127
INFO[2022-07-08T10:27:43.217] clearing /etc/security/limits.conf            host=172.16.0.127
```

> **Note**: You can specify a database cluster to be uninstalled by config.yaml or the cluster name. The prerequisite of specifying a database cluster by the cluster name is that you can find the cluster by running `ptk ls`.

## Related Page

For details about PTK usage, see [About PTK](https://docs.mogdb.io/en/ptk/v0.4/overview).