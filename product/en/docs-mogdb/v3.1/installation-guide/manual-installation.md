---
title: Manually Deploying MogDB/openGauss
summary: Manually Deploying MogDB/openGauss
author: Zhang Cuiping
date: 2021-10-14
---

# Manually Deploying MogDB/openGauss in the Standalone, Primary/Standby, and Cascaded Modes

## Scenarios

On some operating systems not yet supported by the installer, the installer (such as OM) will report the error that the operating system is not supported, so you can install MogDB manually.

## Get The Installation Package

Visit [MogDB Download Page](https://www.mogdb.io/downloads/mogdb) to download the installation package for the corresponding platform. The `MogDB-x.x.x-openEuler-64bit.tar.gz` obtained after unpacking the standard installation package is the kernel package required for manual installation.

## 1. Preparation

a. Disable the firewall.

```bash
systemctl disable firewalld.service
systemctl stop firewalld.service
setenforce=0
sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
```

b. Install the dependency.

```bash
yum install libaio-devel -y
```

c. Create the related directory, user, and user group.

```bash
groupadd dbgrp -g 2000
useradd omm -g 2000 -u 2000
echo "Enmo@123" | passwd --stdin omm
mkdir -p /opt/mogdb/software
mkdir -p /opt/mogdb/data
chown -R omm:dbgrp /opt/mogdb
```

d. Upload and decompress the binary file.

```bash
[root@mogdb-kernel-0001 software]# pwd
/opt/mogdb/software
[root@mogdb-kernel-0001 software]# ls -lrt
total 90236
-r-------- 1 root root 92401412 Jun 13 06:14 MogDB-3.1.0-openEuler-64bit.tar.gz
chown omm:dbgrp MogDB-3.1.0-openEuler-64bit.tar.gz
su - omm
cd /opt/mogdb/software/
tar -xf MogDB-3.1.0-openEuler-64bit.tar.gz
```

## 2. Database Initialization (Standalone)

a. Configure environment variables.

```bash
echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
source /home/omm/.bashrc
```

b. Initialize the database.

```bash
bin/gs_initdb --pgdata=/opt/mogdb/data --nodename=primary --pwpasswd=Enmo@123 --encoding=UTF-8 --locale=en_US.UTF-8
```

c. Modify initialized parameters.

```bash
echo "port=26000" >> /opt/mogdb/data/postgresql.conf
echo "listen_addresses = '0.0.0.0'" >> /opt/mogdb/data/postgresql.conf
echo "password_encryption_type = 0" >> /opt/mogdb/data/postgresql.conf
echo "log_directory = 'pg_log'" >> /opt/mogdb/data/postgresql.conf
echo "remote_read_mode=non_authentication" >> /opt/mogdb/data/postgresql.conf
echo "host all all 0.0.0.0/0 md5" >> /opt/mogdb/data/pg_hba.conf
```

d. Start the database.

```bash
gs_ctl start -D /opt/mogdb/data
```

**Standalone installation is complete.**

## 3. Primary/Standby Installation

a. Install the primary database.

- Configure the connection.

  ```bash
  echo "replconninfo1='localhost=172.16.0.106 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.245 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'
  " >> /opt/mogdb/data/postgresql.conf
  ```

  **localhost indicates the IP address of the primary database, and remotehost indicates the IP address of the standby database.**

- Start the primary database in primary mode.

  ```bash
  gs_ctl restart -D /opt/mogdb/data/ -M primary
  ```

b. Install the standby database.

- For details about the preparation, see [Preparation](manual-installation.md#1-preparation).

- Configure environment variables.

  ```bash
  echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
  echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
  echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
  source /home/omm/.bashrc
  # Upload configuration files of the primary database to the standby database.
  scp /opt/mogdb/data/pg_hba.conf /opt/mogdb/data/postgresql.conf 172.16.0.245:/opt/mogdb/data/
  ```

- Configure the connection.

  ```bash
  sed -i "/^replconninfo1/creplconninfo1=‘localhost=172.16.0.245 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.106 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004’" /opt/mogdb/data/postgresql.conf
  ```

  **localhost indicates the IP address of the standby database, and the remotehost indicates the IP address of the primary database.**

- Build the primary/standby relationship.

  ```bash
  gs_ctl build -D /opt/mogdb/data/ -b full -M standby
  ```

- Query the status of the primary and standby databases.

  - Primary database

    ```bash
    [omm@mogdb-kernel-0001 data]$ gs_ctl query -D /opt/mogdb/data/
    [2021-06-13 07:51:41.119][159054][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
    HA state:
            local_role                     : Primary
            static_connections             : 1
            db_state                       : Normal
            detail_information             : Normal

    Senders info:
            sender_pid                     : 159041
            local_role                     : Primary
            peer_role                      : Standby
            peer_state                     : Normal
            state                          : Streaming
            sender_sent_location           : 0/14000258
            sender_write_location          : 0/14000258
            sender_flush_location          : 0/14000258
            sender_replay_location         : 0/14000258
            receiver_received_location     : 0/14000258
            receiver_write_location        : 0/14000258
            receiver_flush_location        : 0/14000258
            receiver_replay_location       : 0/14000258
            sync_percent                   : 100%
            sync_state                     : Sync
            sync_priority                  : 1
            sync_most_available            : Off
            channel                        : 172.16.0.106:26001-->172.16.0.245:60856

    Receiver info:
    No information
    ```

  - Standby database

    ```bash
    [omm@mogdb-kernel-0002 data]$ gs_ctl query -D /opt/mogdb/data/
    [2021-06-13 07:51:32.743][123204][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
    HA state:
            local_role                     : Standby
            static_connections             : 1
            db_state                       : Normal
            detail_information             : Normal
    
    Senders info:
    No information
    Receiver info:
            receiver_pid                   : 123194
            local_role                     : Standby
            peer_role                      : Primary
            peer_state                     : Normal
            state                          : Normal
            sender_sent_location           : 0/14000140
            sender_write_location          : 0/14000140
            sender_flush_location          : 0/14000140
            sender_replay_location         : 0/14000140
            receiver_received_location     : 0/14000140
            receiver_write_location        : 0/14000140
            receiver_flush_location        : 0/14000140
            receiver_replay_location       : 0/14000140
            sync_percent                   : 100%
            channel                        : 172.16.0.245:60856<--172.16.0.106:26001
    ```

**Primary/standby installation is complete.**

## 4. Cascaded Installation

a. For details about the primary/standby installation, see [Preparation](manual-installation.md#1-preparation), [Database Initialization](manual-installation.md#2-database-initialization-standalone), and [Primary/Standby Installation](manual-installation.md#3-primarystandby-installation).

b. Add the replication channel.

- Primary database

  ```bash
  gsql -d postgres -p26000 -c "alter system set replconninfo2 to ‘localhost=172.16.0.106 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.127 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004 iscascade=true’;"
  ```

- Standby database

  ```bash
  gsql -d postgres -p26000 -c "alter system set replconninfo2 to ‘localhost=172.16.0.245 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.127 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004 iscascade=true’;"
  ```

c. Operate with the cascaded database.

- For details about the preparation, see [Preparation](manual-installation.md#1-preparation).

- Configure environment variables.

  ```bash
  echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
  echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
  echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
  source /home/omm/.bashrc
  ```

- Upload configuration files of the standby database to the cascaded database.

  ```bash
  scp /opt/mogdb/data/pg_hba.conf /opt/mogdb/data/postgresql.conf 172.16.0.127:/opt/mogdb/data/
  ```

- Configure the connection.

  ```bash
  sed -i "/^replconninfo1/creplconninfo1='localhost=172.16.0.127 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.106 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'" /opt/mogdb/data/postgresql.conf
  sed -i "/replconninfo2/creplconninfo2='localhost=172.16.0.127 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.245 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'" /opt/mogdb/data/postgresql.conf
  ```

  **localhost indicates the IP address of the cascaded database, and remotehost indicates the IP addresses of the primary and standby databases.**

- Build the primary/standby relationship.

  ```bash
  gs_ctl build -D /opt/mogdb/data/ -b full -M cascade_standby
  ```

d. Query the status of the primary, standby, and cascaded databases.

- Primary database

  ```bash
  [omm@mogdb-kernel-0001 ~]$ gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:37:03.281][207069][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Primary
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal

   Senders info:
          sender_pid                     : 206143
          local_role                     : Primary
          peer_role                      : Standby
          peer_state                     : Normal
          state                          : Streaming
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          sync_state                     : Sync
          sync_priority                  : 1
          sync_most_available            : Off
          channel                        : 172.16.0.106:26001-->172.16.0.245:34586

   Receiver info:
  No information
  ```

- Standby database

  ```bash
  gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:37:09.128][147065][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Standby
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal

   Senders info:
          sender_pid                     : 147043
          local_role                     : Standby
          peer_role                      : Cascade Standby
          peer_state                     : Normal
          state                          : Streaming
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          sync_state                     : Async
          sync_priority                  : 0
          sync_most_available            : Off
          channel                        : 172.16.0.245:26001-->172.16.0.127:49110

   Receiver info:
          receiver_pid                   : 146771
          local_role                     : Standby
          peer_role                      : Primary
          peer_state                     : Normal
          state                          : Normal
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          channel                        : 172.16.0.245:34586<--172.16.0.106:26001
  ```

- Cascaded database

  ```bash
  gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:36:56.223][273241][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Cascade Standby
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal
  
   Senders info:
  No information
   Receiver info:
          receiver_pid                   : 273237
          local_role                     : Cascade Standby
          peer_role                      : Standby
          peer_state                     : Normal
          state                          : Normal
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          channel                        : 172.16.0.127:49110<--172.16.0.245:26001
  ```

**Cascaded installation is complete.**
