---
title: 发布记录
summary: 发布记录
author: Bin.Liu
date: 2022-06-17
---

# 发布记录

> MacOS If you encounter corruption, do
>
> sudo xattr -r -d com.apple.quarantine /Applications/Mogeaver.app

## 22.2.1

2022-09-30

- [mogeaver-ce-22.2.1-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.2.1-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.2.1-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.2.1-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.2.1-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-win32.win32.x86_64.zip)

Synchronize the DBeaver code.

## 22.1.5

2022-08-22

- [mogeaver-ce-22.1.5-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.1.5-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.1.5-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.1.5-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.1.5-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-win32.win32.x86_64.zip)

### Features

- Opening the script editor part of the SQL causes Hang to die
- Create Schema Issue
- other usage issues

### Fix

- Some issue

## 22.1.1

2022-06-30

- [mogeaver-ce-22.1.1-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.1.1-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.1.1-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.1.1-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.1.1-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-win32.win32.x86_64.zip)

### Features

- Support creating database selection compatibility mode
- Support MogDB role (role)
- Support Large sequences (MogDB 3.0)
- Support partition viewing function (not support create partition table in GUI)
- Support Package (Go to source code function similar to Oracle has not been implemented yet)
- Support JOB
- Packaged the debug function to support DBE_PLDEBUGGER server debugging.
- Support BLOB, CLOB modification and view
