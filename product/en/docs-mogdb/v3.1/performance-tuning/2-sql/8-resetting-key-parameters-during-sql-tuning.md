---
title: Resetting Key Parameters During SQL Tuning
summary: Resetting Key Parameters During SQL Tuning
author: Guo Huan
date: 2021-03-16
---

# Resetting Key Parameters During SQL Tuning

| Parameter/Reference Value | Description                                                  |
| :------------------------ | :----------------------------------------------------------- |
| enable_nestloop=on        | Specifies how the optimizer uses **Nest Loop Join**. If this parameter is set to **on**, the optimizer preferentially uses **Nest Loop Join**. If it is set to **off**, the optimizer preferentially uses other methods, if any.<br/>NOTE:<br/>If you only want to temporarily change the value of this parameter during the current database connection (that is, the current session), execute the following SQL statement:<br/>`SET enable_nestloop to off;`<br/>By default, this parameter is set to **on**. Change the value as required. Generally, nested loop join has the poorest performance among the three **JOIN** methods (nested loop join, merge join, and hash join). You are advised to set this parameter to **off**. |
| enable_bitmapscan=on      | Specifies whether the optimizer uses bitmap scanning. If the value is **on**, bitmap scanning is used. If the value is **off**, it is not used.<br/>NOTE:<br/>If you only want to temporarily change the value of this parameter during the current database connection (that is, the current session), execute the following SQL statement:<br/>`SET enable_bitmapscan to off;`<br/>The bitmap scanning applies only in the query condition where **a &gt; 1 and b &gt; 1** and indexes are created on columns **a** and **b**. During performance tuning, if the query performance is poor and bitmapscan operators are in the execution plan, set this parameter to **off** and check whether the performance is improved. |
| enable_hashagg=on         | Specifies whether to enable the optimizer's use of Hash-aggregation plan types. |
| enable_hashjoin=on        | Specifies whether to enable the optimizer's use of Hash-join plan types. |
| enable_mergejoin=on       | Specifies whether to enable the optimizer's use of Hash-merge plan types. |
| enable_indexscan=on       | Specifies whether to enable the optimizer's use of index-scan plan types. |
| sql_beta_feature          | Specifies whether to enable the optimizer's use of index-only-scan plan types. |
| enable_seqscan=on         | Specifies whether the optimizer uses bitmap scanning. It is impossible to suppress sequential scans entirely, but setting this variable to **off** encourages the optimizer to choose other methods if available. |
| enable_sort=on            | Specifies the optimizer sorts. It is impossible to fully suppress explicit sorts, but setting this variable to **off** allows the optimizer to preferentially choose other methods if available. |
| rewrite_rule              | Specifies whether the optimizer enables the **LAZY\_AGG** and **MAGIC_SET** rewriting rules. |
| sql_beta_feature          | Controls whether the optimizer is enabled. SEL SEMI POISSON/SEL EXPR INSTR/PARAM PATH GEN/RAND COST OPT/PARAM PATH OPT/PAGE EST Opt test function. |
