---
title: Viewing WDRs
summary: Viewing WDRs
author: Guo Huan
date: 2022-04-23
---

# Viewing WDRs

## Database Stat

The following table describes columns in the Database Stat report.

**Table 1** Columns in the Database Stat report

| Column         | Description                                                  |
| :------------- | :----------------------------------------------------------- |
| DB Name        | Database name.                                               |
| Backends       | Number of backends connected to this database.               |
| Xact Commit    | Number of transactions in this database that have been committed. |
| Xact Rollback  | Number of transactions in this database that have been rolled back. |
| Blks Read      | Number of disk blocks read in this database.                 |
| Blks Hit       | Number of times that disk blocks have been found in the cache. |
| Tuple Returned | Number of rows sequentially scanned.                         |
| Tuple Fetched  | Number of rows randomly scanned.                             |
| Tuple Inserted | Number of rows inserted by queries in this database.         |
| Tuple Updated  | Number of rows updated by queries in this database.          |
| Tup Deleted    | Number of rows deleted by queries in this database.          |
| Conflicts      | Number of queries canceled due to conflicts.                 |
| Temp Files     | Number of temporary files created by queries in this database. |
| Temp Bytes     | Total amount of data written to temporary files by queries in this database. |
| Deadlocks      | Number of deadlocks detected in this database.               |
| Blk Read Time  | Time spent reading data file blocks by backends in this database (unit: ms). |
| Blk Write Time | Time spent writing into data file blocks by backends in this database (unit: ms). |
| Stats Reset    | Time at which the current statistics were reset.             |

## Load Profile

The following table lists metrics in the Load Profile report.

**Table 2** Metrics in the Load Profile report

| Metric                    | Description                                                |
| :------------------------ | :--------------------------------------------------------- |
| DB Time(us)               | Total elapsed time of a job.                               |
| CPU Time(us)              | Total CPU time used for job running.                       |
| Redo size(blocks)         | Size of the generated WAL (blocks).                        |
| Logical read (blocks)     | Number of logical reads (blocks) on a table or an index.   |
| Physical read (blocks)    | Number of physical reads (blocks) on a table or an index.  |
| Physical write (blocks)   | Number of physical writes (blocks) on a table or an index. |
| Read IO requests          | Number of times that a table or an index is read.          |
| Write IO requests         | Number of times that a table or an index is written.       |
| Read IO (MB)              | Table or index read size (MB).                             |
| Write IO (MB)             | Table or index write size (MB).                            |
| Logons                    | Number of logins.                                          |
| Executes (SQL)            | Number of times the SQL statement is executed.             |
| Rollbacks                 | Number of rolled-back transactions.                        |
| Transactions              | Number of transactions.                                    |
| SQL response time P95(us) | Response time of 95% SQL statements.                       |
| SQL response time P80(us) | Response time of 80% SQL statements.                       |

## Instance Efficiency Percentages

The following table lists metrics in the Instance Efficiency Percentages report.

**Table 3** Metrics in the Instance Efficiency Percentages report

| Metric            | Description                                                  |
| :---------------- | :----------------------------------------------------------- |
| Buffer Hit %      | Hit ratio of the buffer pool.                                |
| Effective CPU %   | Ratio of the CPU time to the DB time.                        |
| WalWrite NoWait % | Ratio of the number of events that access the WAL buffer to the total number of wait events. |
| Soft Parse %      | Ratio of the number of soft parsing times to the total number of parsing times. |
| Non-Parse CPU %   | Ratio of the non-parse time to the total execution time.     |

## Top 10 Events by Total Wait Time

The following table lists columns in the Top 10 Events by Total Wait Time report.

**Table 4** Columns in the Top 10 Events by Total Wait Time report

| Column              | Description                         |
| :------------------ | :---------------------------------- |
| Event               | Name of a wait event.               |
| Waits               | Number of wait times.               |
| Total Wait Time(us) | Total wait time, in microseconds.   |
| Avg Wait Time(us)   | Average wait time, in microseconds. |
| Type                | Wait event type.                    |

## Wait Classes by Total Wait Time

The following table lists columns in the Wait Classes by Total Wait Time report.

**Table 5** Columns in the Wait Classes by Total Wait Time report

| Column              | Description                                                  |
| :------------------ | :----------------------------------------------------------- |
| Type                | Wait events are classified as follows:<br/>- STATUS。<br/>- LWLOCK_EVENT。<br/>- LOCK_EVENT。<br/>- IO_EVENT。 |
| Waits               | Number of wait times.                                        |
| Total Wait Time(us) | Total wait time, in microseconds.                            |
| Avg Wait Time(us)   | Average wait time, in microseconds.                          |

## Host CPU

The following table describes columns in the Host CPU report.

**Table 6** Columns in the Host CPU report

| Column             | Description                                      |
| :----------------- | :----------------------------------------------- |
| Cpus               | Number of processors.                            |
| Cores              | Number of CPU cores.                             |
| Sockets            | Number of CPU sockets.                           |
| Load Average Begin | Load average value of the start snapshot.        |
| Load Average End   | Load average value of the end snapshot.          |
| %User              | Percentage of CPU time spent in the user mode.   |
| %System            | Percentage of CPU time spent in the kernel mode. |
| %WIO               | Percentage of CPU time spent in wait I/O.        |
| %Idle              | Percentage of CPU idle time.                     |

## IO Profile

The following table lists metrics in the IO Profile report.

**Table 7** Metrics in the IO Profile report

| Metric            | Description                         |
| :---------------- | :---------------------------------- |
| Database requests | Number of database I/O times.       |
| Database (MB)     | Database I/O data volume.           |
| Database (blocks) | Number of database I/O data blocks. |
| Redo requests     | Number of redo I/O times.           |
| Redo (MB)         | Redo I/O data volume.               |

## Memory Statistics

The following table lists metrics in the Memory Statistics report.

**Table 8** Columns in the Memory Statistics report

| Metric              | Description                                             |
| :------------------ | :------------------------------------------------------ |
| shared_used_memory  | Size of used shared memory (MB).                        |
| max_shared_memory   | Maximum shared memory (MB).                             |
| process_used_memory | Memory used by processes (MB).                          |
| max_process_memory  | Maximum memory that can be allocated to a process (MB). |

## Time Model

The following table describes metrics in the Time Model report.

**Table 9** Metrics in the Time Model report

| Metric              | Description                                                  |
| :------------------ | :----------------------------------------------------------- |
| DB_TIME             | Total end-to-end wall time consumed by all threads (unit: μs). |
| EXECUTION_TIME      | Total time consumed on the executor (unit: μs).              |
| PL_EXECUTION_TIME   | Total time consumed for executing PL/SQL statements (unit: μs). |
| CPU_TIME            | Total CPU time consumed by all threads (unit: μs).           |
| PLAN_TIME           | Total time consumed for generating an execution plan (unit: μs). |
| REWRITE_TIME        | Total time consumed on query rewriting (unit: μs).           |
| PL_COMPILATION_TIME | Total time consumed for SQL compilation (unit: μs).          |
| PARSE_TIME          | Total time consumed for parsing SQL statements (unit: μs).   |
| NET_SEND_TIME       | Total time consumed for sending data over network (unit: μs). |
| DATA_IO_TIME        | Total time consumed for data read and write (unit: μs).      |

## SQL Statistics

The following table describes columns in the SQL Statistics report.

**Table 10** Columns in the SQL Statistics report

| Column                | Description                                                  |
| :-------------------- | :----------------------------------------------------------- |
| Unique SQL Id         | ID of the normalized SQL statement.                          |
| Node Name             | Node name.                                                   |
| User Name             | Username.                                                    |
| Tuples Read           | Number of tuples that are read.                              |
| Calls                 | Number of calls.                                             |
| Min Elapse Time(us)   | Minimum execution time (unit: us).                           |
| Max Elapse Time(us)   | Maximum execution time (unit: us).                           |
| Total Elapse Time(us) | Total execution time (unit: us).                             |
| Avg Elapse Time(us)   | Average execution time (unit: us).                           |
| Returned Rows         | Number of rows returned by SELECT.                           |
| Tuples Affected       | Number of rows affected by INSERT, UPDATE, and DELETE.       |
| Logical Read          | Number of logical reads on the buffer.                       |
| Physical Read         | Number of physical reads on the buffer.                      |
| CPU Time(us)          | CPU time (unit: us).                                         |
| Data IO Time(us)      | Time spent on I/O (unit: us).                                |
| Sort Count            | Number of sorting execution times.                           |
| Sort Time(us)         | Sorting execution time (unit: us).                           |
| Sort Mem Used(KB)     | Size of work memory used during sorting (unit: KB).          |
| Sort Spill Count      | Number of file writes when data is flushed to disks during sorting. |
| Sort Spill Size(KB)   | File size used when data is flushed to disks during sorting (unit: KB). |
| Hash Count            | Number of hashing execution times.                           |
| Hash Time(us)         | Hashing execution time (unit: us).                           |
| Hash Mem Used(KB)     | Size of work memory used during hashing (unit: KB).          |
| Hash Spill Count      | Number of file writes when data is flushed to disks during hashing. |
| Hash Spill Size(KB)   | File size used when data is flushed to disks during hashing (unit: KB). |
| SQL Text              | Normalized SQL character string.                             |

## Wait Events

The following table describes columns in the Wait Events report.

**Table 11** Columns in the Wait Events report

| Column               | Description                                                  |
| :------------------- | :----------------------------------------------------------- |
| Type                 | Wait events are classified as follows:<br/>- STATUS<br/>- LWLOCK_EVENT<br/>- LOCK_EVENT<br/>- IO_EVENT |
| Event                | Name of a wait event.                                        |
| Total Wait Time (us) | Total wait time (unit: us).                                  |
| Waits                | Total number of wait times.                                  |
| Failed Waits         | Number of wait failures.                                     |
| Avg Wait Time (us)   | Average wait time (unit: us).                                |
| Max Wait Time (us)   | Maximum wait time (unit: us).                                |

## Cache IO Stats

Cache IO Stats contains two tables: User table and User index. The columns in the tables are described as follows.

### User table IO activity ordered by heap blks hit ratio

**Table 12** Columns in the User table IO activity ordered by heap blks hit ratio report

| Column               | Description                                                  |
| :------------------- | :----------------------------------------------------------- |
| DB Name              | Database name.                                               |
| Schema Name          | Schema name.                                                 |
| Table Name           | Table name.                                                  |
| %Heap Blks Hit Ratio | Buffer pool hit ratio of the table.                          |
| Heap Blks Read       | Number of disk blocks read from the table.                   |
| Heap Blks Hit        | Number of cache hits in the table.                           |
| Idx Blks Read        | Number of disk blocks read from all indexes on the table.    |
| Idx Blks Hit         | Number of cache hits in the table .                          |
| Toast Blks Read      | Number of disk blocks read from the TOAST table (if any) in the table. |
| Toast Blks Hit       | Number of buffer hits in the TOAST table (if any) in the table. |
| Tidx Blks Read       | Number of disk blocks read from the TOAST table index (if any) in the table. |
| Tidx Blks Hit        | Number of buffer hits in the TOAST table index (if any) in the table. |

### User index IO activity ordered by idx blks hit ratio

**Table 13** Columns in the User index IO activity ordered by idx blks hit ratio report

| Column              | Description                                               |
| :------------------ | :-------------------------------------------------------- |
| DB Name             | Database name.                                            |
| Schema Name         | Schema name.                                              |
| Table Name          | Table name.                                               |
| Index Name          | Index name.                                               |
| %Idx Blks Hit Ratio | Index hit ratio.                                          |
| Idx Blks Read       | Number of disk blocks read from all indexes on the table. |
| Idx Blks Hit        | Number of cache hits in the table.                        |

## Utility status

**Utility status** contains two tables: **Replication slot** and **Replication stat**. Columns in the tables are described as follows:

### Replication slot

**Table 14** Columns in the Replication slot report

| Column        | Description                                    |
| :------------ | :--------------------------------------------- |
| Slot Name     | Replication node name.                         |
| Slot Type     | Type of the replication node.                  |
| DB Name       | Name of the database on the replication node.  |
| Active        | Replication node status.                       |
| Xmin          | Transaction ID of the replication node.        |
| Restart Lsn   | Xlog file information on the replication node. |
| Dummy Standby | Replication node as a dummy standby.           |

### Replication stat

**Table 15** Columns in the Replication stat report

| Column                   | Description                               |
| :----------------------- | :---------------------------------------- |
| Thread Id                | PID of the thread.                        |
| Usesys Id                | User system ID.                           |
| Username                 | Username.                                 |
| Application Name         | Application name.                         |
| Client Addr              | Client address.                           |
| Client Hostname          | Client host name.                         |
| Client Port              | Port of the client.                       |
| Backend Start            | Start time of an application.             |
| State                    | Log replication status.                   |
| Sender Sent Location     | Location where the sender sends logs.     |
| Receiver Write Location  | Location where the receiver writes logs.  |
| Receiver Flush Location  | Location where the receiver flushes logs. |
| Receiver Replay Location | Location where the receiver replays logs. |
| Sync Priority            | Synchronization priority.                 |
| Sync State               | Synchronization status.                   |

## Object stats

Object stats contains three tables: User Tables stats, User index stats, and Bad lock stats. Columns in the tables are described as follows:

### User Tables stats

**Table 16** Columns in the User Tables stats report

| Column            | Description                                                  |
| :---------------- | :----------------------------------------------------------- |
| DB Name           | Database name.                                               |
| Schema            | Schema name.                                                 |
| Relname           | Relation name.                                               |
| Seq Scan          | Number of sequential scans initiated on this table.          |
| Seq Tup Read      | Number of live rows fetched by sequential scans.             |
| Index Scan        | Number of index scans initiated on the table.                |
| Index Tup Fetch   | Number of live rows fetched by index scans.                  |
| Tuple Insert      | Number of rows inserted.                                     |
| Tuple Update      | Number of rows updated.                                      |
| Tuple Delete      | Number of rows deleted.                                      |
| Tuple Hot Update  | Number of rows HOT updated (with no separate index updated). |
| Live Tuple        | Estimated number of live rows.                               |
| Dead Tuple        | Estimated number of dead rows.                               |
| Last Vacuum       | Last time at which this table was manually vacuumed (not counting **VACUUM FULL**). |
| Last Autovacuum   | Last time at which this table was vacuumed by the autovacuum daemon. |
| Last Analyze      | Last time at which this table was manually analyzed.         |
| Last Autoanalyze  | Last time at which this table was analyzed by the autovacuum daemon. |
| Vacuum Count      | Number of times the table has been manually vacuumed (not counting **VACUUM FULL**). |
| Autovacuum Count  | Number of times the table has been vacuumed by the autovacuum daemon. |
| Analyze Count     | Number of times the table has been manually analyzed.        |
| Autoanalyze Count | Number of times the table has been analyzed by the autovacuum daemon. |

### User index stats

**Table 17** Columns in the User index stats report

| Column            | Description                                                  |
| :---------------- | :----------------------------------------------------------- |
| DB Name           | Database name.                                               |
| Schema            | Schema name.                                                 |
| Relname           | Relation name.                                               |
| Index Relname     | Index name.                                                  |
| Index Scan        | Number of index scans initiated on the index.                |
| Index Tuple Read  | Number of index entries returned by scans on the index.      |
| Index Tuple Fetch | Number of live table rows fetched by simple index scans using the index. |

### Bad lock stats

**Table 18** Columns in the Bad lock stats report

| Column        | Description            |
| :------------ | :--------------------- |
| DB Id         | OID of the database.   |
| Tablespace Id | Tablespace OID.        |
| Relfilenode   | File object ID.        |
| Fork Number   | File type.             |
| Error Count   | Number of failures.    |
| First Time    | First occurrence time. |
| Last Time     | Last occurrence time.  |

## Configuration settings

The following table describes columns in the Configuration settings report.

**Table 19** Columns in the Configuration settings report

| Column        | Description                                             |
| :------------ | :------------------------------------------------------ |
| Name          | GUC name.                                               |
| Abstract      | GUC description.                                        |
| Type          | Data type.                                              |
| Curent Value  | Current value.                                          |
| Min Value     | Valid minimum value.                                    |
| Max Value     | Valid maximum value.                                    |
| Category      | GUC type.                                               |
| Enum Values   | All enumerated values.                                  |
| Default Value | Default parameter value used upon the database startup. |
| Reset Value   | Default parameter value used upon the database reset.   |

## SQL Detail

The following table describes columns in the SQL Detail report.

**Table 20** Columns in the SQL Detail report

| Column        | Description                                           |
| :------------ | :---------------------------------------------------- |
| Unique SQL Id | ID of the normalized SQL statement.                   |
| User Name     | Username.                                             |
| Node Name     | Node name. This column is not displayed in node mode. |
| SQL Text      | Normalized SQL text.                                  |
