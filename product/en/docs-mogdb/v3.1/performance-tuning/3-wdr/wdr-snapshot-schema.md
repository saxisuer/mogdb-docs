---
title: WDR Snapshot Schema
summary: WDR Snapshot Schema
author: Zhang Cuiping
date: 2021-03-11
---

# WDR Snapshot Schema

After the WDR snapshot function is enabled (**enable_wdr_snapshot** is set to **on**), schema **snapshot** is created in user tablespace **pg_default** in database **postgres** to flush WDR snapshot data. By default, the initial user or the **monadmin** user can access the snapshot schema.

You can set the parameter **wdr_snapshot_retention_days** to automatically manage the snapshot lifecycle.

<br/>

## Original Information Table of WDR Snapshots

### SNAPSHOT.SNAPSHOT <a id='SNAPSHOT'> </a>

**SNAPSHOT** records the index information, start time, and end time of WDR snapshots stored in the current system. The results can only be queried in the system library, but not in the user library.

**Table 1** SNAPSHOT attributes

| Name        | Type      | Description                  | Example                       |
| :---------- | :-------- | :--------------------------- | :---------------------------- |
| snapshot_id | bigint    | WDR snapshot ID              | 1                             |
| start_ts    | timestamp | Start time of a WDR snapshot | 2019-12-28 17:11:27.423742+08 |
| end_ts      | timestamp | End time of a WDR snapshot   | 2019-12-28 17:11:43.67726+08  |

<br/>

### SNAPSHOT.TABLES_SNAP_TIMESTAMP <a id='tables_snap_timestamp'> </a>

**TABLES_SNAP_TIMESTAMP** records the start time and end time of data collection, as well as corresponding databases, and table objects for all stored WDR snapshots.

**Table 2** TABLES_SNAP_TIMESTAMP attributes

| Name        | Type      | Description                              | Example                       |
| :---------- | :-------- | :--------------------------------------- | :---------------------------- |
| snapshot_id | bigint    | WDR snapshot ID                          | 1                             |
| db_name     | text      | Database corresponding to a WDR snapshot | tpcc1000                      |
| tablename   | text      | Table corresponding to a WDR snapshot    | snap_xc_statio_all_indexes    |
| start_ts    | timestamp | Start time of a WDR snapshot             | 2019-12-28 17:11:27.425849+08 |
| end_ts      | timestamp | End time of a WDR snapshot               | 2019-12-28 17:11:27.707398+08 |

<br/>

### SNAP_SEQ

**SNAP_SEQ** is an ascending sequence, which provides IDs for WDR snapshots.

<br/>

## WDR Snapshot Data Table

The naming rule of a WDR snapshot data table is **snap_{Source data table}**.

WDR snapshot data tables come from all views in **DBE_PERF** Schema.

All WDR Snapshot data tables can be queried by running the following command.

```sql
select * from pg_catalog.pg_tables where schemaname='snapshot';
```

The following table lists all WDR Snapshot data tables and related introduction pages for your reference.

| schemaname | tablename                                                    |
| ---------- | ------------------------------------------------------------ |
| snapshot   | [tables_snap_timestamp](#tables_snap_timestamp)              |
| snapshot   | [snapshot](#SNAPSHOT)                                        |
| snapshot   | [snap_global_os_runtime](../../reference-guide/schema/DBE_PERF/os/GLOBAL_OS_RUNTIME.md) |
| snapshot   | [snap_global_os_threads](../../reference-guide/schema/DBE_PERF/os/GLOBAL_OS_THREADS.md) |
| snapshot   | [snap_global_instance_time](../../reference-guide/schema/DBE_PERF/instance/GLOBAL_INSTANCE_TIME.md) |
| snapshot   | [snap_summary_workload_sql_count](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_SQL_COUNT.md) |
| snapshot   | [snap_summary_workload_sql_elapse_time](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_SQL_ELAPSE_TIME.md) |
| snapshot   | [snap_global_workload_transaction](../../reference-guide/schema/DBE_PERF/workload/GLOBAL_WORKLOAD_TRANSACTION.md) |
| snapshot   | [snap_summary_workload_transaction](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_TRANSACTION.md) |
| snapshot   | [snap_global_thread_wait_status](../../reference-guide/schema/DBE_PERF/session-thread/GLOBAL_THREAD_WAIT_STATUS.md) |
| snapshot   | [snap_global_memory_node_detail](../../reference-guide/schema/DBE_PERF/memory/GLOBAL_MEMORY_NODE_DETAIL.md) |
| snapshot   | [snap_global_shared_memory_detail](../../reference-guide/schema/DBE_PERF/memory/GLOBAL_SHARED_MEMORY_DETAIL.md) |
| snapshot   | [snap_global_stat_db_cu](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STAT_DB_CU.md) |
| snapshot   | [snap_global_stat_database](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_DATABASE.md) |
| snapshot   | [snap_summary_stat_database](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_DATABASE.md) |
| snapshot   | [snap_global_stat_database_conflicts](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_DATABASE_CONFLICTS.md) |
| snapshot   | [snap_summary_stat_database_conflicts](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_DATABASE_CONFLICTS.md) |
| snapshot   | [snap_global_stat_bad_block](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_BAD_BLOCK.md) |
| snapshot   | [snap_summary_stat_bad_block](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_BAD_BLOCK.md) |
| snapshot   | [snap_global_file_redo_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_FILE_REDO_IOSTAT.md) |
| snapshot   | [snap_summary_file_redo_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_FILE_REDO_IOSTAT.md) |
| snapshot   | [snap_global_rel_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_REL_IOSTAT.md) |
| snapshot   | [snap_summary_rel_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_REL_IOSTAT.md) |
| snapshot   | [snap_global_file_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_FILE_IOSTAT.md) |
| snapshot   | [snap_summary_file_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_FILE_IOSTAT.md) |
| snapshot   | [snap_global_replication_slots](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REPLICATION_SLOTS.md) |
| snapshot   | [snap_global_bgwriter_stat](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_BGWRITER_STAT.md) |
| snapshot   | [snap_global_replication_stat](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REPLICATION_STAT.md) |
| snapshot   | [snap_global_transactions_running_xacts](../../reference-guide/schema/DBE_PERF/transaction/GLOBAL_TRANSACTIONS_RUNNING_XACTS.md) |
| snapshot   | [snap_summary_transactions_running_xacts](../../reference-guide/schema/DBE_PERF/transaction/SUMMARY_TRANSACTIONS_RUNNING_XACTS.md) |
| snapshot   | [snap_global_transactions_prepared_xacts](../../reference-guide/schema/DBE_PERF/transaction/GLOBAL_TRANSACTIONS_PREPARED_XACTS.md) |
| snapshot   | [snap_summary_transactions_prepared_xacts](../../reference-guide/schema/DBE_PERF/transaction/SUMMARY_TRANSACTIONS_PREPARED_XACTS.md) |
| snapshot   | [snap_summary_statement](../../reference-guide/schema/DBE_PERF/query/SUMMARY_STATEMENT.md) |
| snapshot   | [snap_global_statement_count](../../reference-guide/schema/DBE_PERF/query/GLOBAL_STATEMENT_COUNT.md) |
| snapshot   | [snap_summary_statement_count](../../reference-guide/schema/DBE_PERF/query/SUMMARY_STATEMENT_COUNT.md) |
| snapshot   | [snap_global_config_settings](../../reference-guide/schema/DBE_PERF/configuration/GLOBAL_CONFIG_SETTINGS.md) |
| snapshot   | [snap_global_wait_events](../../reference-guide/schema/DBE_PERF/wait-events/GLOBAL_WAIT_EVENTS.md) |
| snapshot   | [snap_summary_user_login](../../reference-guide/schema/DBE_PERF/utility/SUMMARY_USER_LOGIN.md) |
| snapshot   | [snap_global_ckpt_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_CKPT_STATUS.md) |
| snapshot   | [snap_global_double_write_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_DOUBLE_WRITE_STATUS.md) |
| snapshot   | [snap_global_pagewriter_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_PAGEWRITER_STATUS.md) |
| snapshot   | [snap_global_redo_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REDO_STATUS.md) |
| snapshot   | [snap_global_rto_status](../../reference-guide/schema/DBE_PERF/rto/global_rto_status.md) |
| snapshot   | [snap_global_recovery_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_RECOVERY_STATUS.md) |
| snapshot   | [snap_global_threadpool_status](../../reference-guide/schema/DBE_PERF/session-thread/GLOBAL_THREADPOOL_STATUS.md) |
| snapshot   | [snap_statement_responsetime_percentile](../../reference-guide/schema/DBE_PERF/query/STATEMENT_RESPONSETIME_PERCENTILE.md) |
| snapshot   | [snap_global_statio_all_indexes](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_INDEXES.md) |
| snapshot   | [snap_summary_statio_all_indexes](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_INDEXES.md) |
| snapshot   | [snap_global_statio_all_sequences](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_SEQUENCES.md) |
| snapshot   | [snap_summary_statio_all_sequences](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_SEQUENCES.md) |
| snapshot   | [snap_global_statio_all_tables](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_TABLES.md) |
| snapshot   | [snap_summary_statio_all_tables](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_TABLES.md) |
| snapshot   | [snap_global_stat_all_indexes](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_ALL_INDEXES.md) |
| snapshot   | [snap_summary_stat_all_indexes](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_ALL_INDEXES.md) |
| snapshot   | [snap_summary_stat_user_functions](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_USER_FUNCTIONS.md) |
| snapshot   | [snap_global_stat_user_functions](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_USER_FUNCTIONS.md) |
| snapshot   | [snap_global_stat_all_tables](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_ALL_TABLES.md) |
| snapshot   | [snap_summary_stat_all_tables](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_ALL_TABLES.md) |
| snapshot   | [snap_class_vital_info](../../reference-guide/schema/DBE_PERF/utility/CLASS_VITAL_INFO.md) |
| snapshot   | [snap_global_record_reset_time](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_RECORD_RESET_TIME.md) |

<br/>

## Performance Report Generated Based on WDR Snapshot

A performance report is generated by summarizing and collecting statistics based on WDR snapshot data tables.

**Prerequisites**

A report can be generated after the WDR snapshot function is enabled (that is, **enable_wdr_snapshot** is set to **on**) and the number of snapshots is greater than or equal to 2.

**Procedure**

1. Run the following command to connect the postgres database.

   ```bash
   gsql -d postgres -p <Port number> -r
   ```

2. Run the following command to query the generated snapshot and obtain **snapshot_id**:

   ```sql
   select * from snapshot.snapshot;
   ```

3. (Optional) Run the following command on CCN to manually create a snapshot. If only one snapshot exists in the database or to view the monitoring data of the database in the current period, manually create a snapshot.

   ```sql
   select create_wdr_snapshot();
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:**
   > Run the **cm_ctl query -Cdvi** command. The command output returned in the **Central Coordinator State** part is the CCN information.

4. Perform the following steps to generate a performance report:

   a. Run the following command to generate a formatted performance report file:

   ```bash
   \a \t \o Server file path
   ```

   The parameters in the preceding command are described as follows:

   - **\\a**: switches the unaligned mode.
   - **\\t**: switches the information and row count footer of the output column name.
   - **\\o**: specifies that all the query results are sent to the server file.
   - *Server file path*: indicates the path for storing the generated performance report file. The user must have the read and write permissions on the path.

   b. Run the following command to write the queried information to the performance report:

   ```sql
   select generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring);
   ```

   The description of the parameters in the preceding command is as follows:

   **Table 3** Parameters of the generate_wdr_report function

   | Parameter                      | Description                  | Value Range                        |
   | :----------------------------- | :--------------------------- | :--------------------------------- |
   | begin_snap_id | ID of a snapshot when a query starts, which is specified by **snapshot_id** in the **snapshot.snaoshot** table. | -                                                            |
   | end_snap_id   | ID of a snapshot when a query ends. By default, the value of **end_snap_id** is greater than that of **begin_snap_id table**. | -                                                            |
   | report_type   | Type of the generated report. The value can be **summary**, **detail**, or **all**. | **summary**: Summary data<br />**detail**: Detailed data<br />**all**: summary data and detailed data |
   | report_scope  | Range of the generated report. The value can be **cluster** or **node**. | **cluster**: database-level information<br />**node**: node-level information |
   | node_name     | When **report\_scope** is set to **node**, set this parameter to the name of the corresponding node. (You can run the `select * from pg_node_env;` command to query the node name.)If **report\_scope** is set to **cluster**, this parameter can be omitted, left blank, empty or set to **NULL**. | **node**: a node name in MogDB<br />**cluster**: This value is omitted, left blank,empty or set to **NULL**. |

   c. Run the following commands to disable the output options and format the output:

   ```bash
   \o \a \t
   ```

5. View the WDR report as required.

**Table 4** WDR report

| Item                                                  | Description                                                  |
| :---------------------------------------------------- | :----------------------------------------------------------- |
| Database Stat (database scope)                        | Performance statistics information in database dimensions, including transaction, write/read, row activity, write conflict, deadlock, and so on |
| Load Profile (database scope)                         | Performance statistics information in database dimensions, including CPU time, DB time, logical read/physical read, IO performance, login/logout, workload intensity, and workload performance, and so on |
| Instance Efficiency Percentages (database/node scope) | Buffer Hit (buffer hit rate), Effective CPU (CPU usage), WalWrite NoWait (success rate of obtaining Wal Buffer), Soft Parse (soft parsing rate), and Non-parse CPU (percentage of CPU time spent in non-parsing activities) at the database or node level |
| Top 10 Events by Total Wait Time (node scope)         | Event that consumes the most time                            |
| Wait Classes by Total Wait Time (node scope)          | Class of wait events that consume the most time              |
| Host CPU (node scope)                                 | CPU usage of the host                                        |
| Memory Statistics (node scope)                        | memory statistics in the kernel                              |
| Object stats (node scope)                             | Performance statistics information in the table and index dimensions |
| Database Configuration (node scope)                   | Node configuration                                           |
| SQL Statistics (node scope)                           | Performance statistics of a SQL statement in all dimensions, including end-to-end time, row activity, cache hit rate, CPU usage, time consumption segmentation |
| SQL Detail (node scope)                               | SQL statement details                                        |

**Examples**

```sql
-- To query enable_wdr_snapshot, set this parameter to on.
mogdb=# show enable_wdr_snapshot;
 enable_wdr_snapshot
---------------------
 on
(1 row)

-- Query the snapshots that have been generated.
mogdb=# select * from snapshot.snapshot;
 snapshot_id |           start_ts            |            end_ts
-------------+-------------------------------+-------------------------------
           1 | 2020-09-07 10:20:36.763244+08 | 2020-09-07 10:20:42.166511+08
           2 | 2020-09-07 10:21:13.416352+08 | 2020-09-07 10:21:19.470911+08
(2 rows)

-- Generate the formatted performance report wdrTestNode.html.
mogdb=# \a \t \o /home/om/wdrTestNode.html
Output format is unaligned.
Showing only tuples.

-- Write data into the performance report wdrTestNode.html.
mogdb=# select generate_wdr_report(1, 2, 'all', 'node', 'dn');

-- Close the performance report wdrTestNode.html.
mogdb=# \o

-- Generate the formatted performance report wdrTestCluster.html.
mogdb=# \o /home/om/wdrTestCluster.html

-- Write data into the performance report wdrTestCluster.html.
mogdb=# select generate_wdr_report(1, 2, 'all', 'cluster', '');

-- Close the performance report wdrTestCluster.html.
mogdb=# \o \a \t
Output format is aligned.
Tuples only is off.
```