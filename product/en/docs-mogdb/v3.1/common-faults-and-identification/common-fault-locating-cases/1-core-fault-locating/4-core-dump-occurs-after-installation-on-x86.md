---
title: Core Dump Occurs After Installation on x86
summary: Core Dump Occurs After Installation on x86
author: Guo Huan
date: 2021-12-09
---

# Core Dump Occurs After Installation on x86

## Symptom

The core dump occurs after the installation of MogDB on x86 architecture machine is completed, and the error in the following figure displays

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-after-installation-on-x86-1.png)

## Cause Analysis

The x86 architecture does not include the rdtscp instruction set, and the deployment of MogDB fails. This problem is common in the case of virtualized installation of Linux server on local windows system, but the virtualization version is too low.

## Procedure

Run the `lscpu | grep rdtscp` command to see if the rdtscp instruction set is supported.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-after-installation-on-x86-2.png)

Support for this instruction set can be enabled through the host's admin side settings. Set the cloud host CPU mode to **host-passthrough**, and then reboot.
