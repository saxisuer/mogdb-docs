---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# Data Security Maintenance Suggestions

To ensure data security in MogDB Kernel and prevent accidents, such as data loss and illegal data access, read this section carefully.

**Preventing Data Loss**

You are advised to plan routine physical backup and store backup files in a reliable medium. If a serious error occurs in the system, you can use the backup files to restore the system to the state at the backup point.

**Preventing Illegal Data Access**

- You are advised to manage database users based on their permission hierarchies. A database administrator creates users and grants permissions to the users based on service requirements to ensure users properly access the database.
- You are advised to deploy MogDB Kernel servers and clients (or applications developed based on the client library) in trusted internal networks. If the servers and clients must be deployed in an untrusted network, enable SSL encryption before services are started to ensure data transmission security. Note that enabling the SSL encryption function compromises database performance.

**Preventing System Logs from Leaking Personal Data**

- Delete personal data before sending debug logs to others for analysis.

   **NOTE:** The log level **log_min_messages** is set to **DEBUG**x (*x* indicates the debug level and the value ranges from 1 to 5). The information recorded in debug logs may contain personal data.

- Delete personal data before sending system logs to others for analysis. If the execution of a SQL statement fails, the error SQL statement will be recorded in a system log by default. SQL statements may contain personal data.

- Set **log_min_error_statement** to **PANIC** to prevent error SQL statements from being recorded in system logs. If this function is disabled, it is difficult to locate fault causes when a fault occurs.
