---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# Checking OS Parameters

## Check Method

Use the **gs_checkos** tool provided by MogDB to check the OS status.

**Prerequisites**

- The hardware and network are working properly.
- The trust relationship of user **root** among the hosts is normal.
- Only user **root** is authorized to run the **gs_checkos** command.

**Procedure**

1. Log in to a server as user **root**.

2. Run the following command to check OS parameters of servers where the MogDB nodes are deployed:

   ```
   gs_checkos -i A
   ```

   The purpose of checking the OS parameters is to ensure that MogDB is preinstalled properly and can efficiently operate after it is installed.

**Examples**

Before running the **gs_checkos** command, execute pre-processing scripts by running **gs_preinstall** to prepare the environment. The following uses parameter **A** as an example:

```
gs_checkos -i A
Checking items:
    A1. [ OS version status ]                                   : Normal
    A2. [ Kernel version status ]                               : Normal
    A3. [ Unicode status ]                                      : Normal
    A4. [ Time zone status ]                                    : Normal
    A5. [ Swap memory status ]                                  : Normal
    A6. [ System control parameters status ]                    : Normal
    A7. [ File system configuration status ]                    : Normal
    A8. [ Disk configuration status ]                           : Normal
    A9. [ Pre-read block size status ]                          : Normal
    A10.[ IO scheduler status ]                                 : Normal
    A11.[ Network card configuration status ]                   : Normal
    A12.[ Time consistency status ]                             : Warning
    A13.[ Firewall service status ]                             : Normal
    A14.[ THP service status ]                                  : Normal
Total numbers:14. Abnormal numbers:0. Warning number:1.
```

The following uses parameter **B** as an example:

```
gs_checkos -i B
Setting items:
    B1. [ Set system control parameters ]                       : Normal
    B2. [ Set file system configuration value ]                 : Normal
    B3. [ Set pre-read block size value ]                       : Normal
    B4. [ Set IO scheduler value ]                              : Normal
    B5. [ Set network card configuration value ]                : Normal
    B6. [ Set THP service ]                                     : Normal
    B7. [ Set RemoveIPC value ]                                 : Normal
    B8. [ Set Session Process ]                                 : Normal
Total numbers:6. Abnormal numbers:0. Warning number:0.
```

## Exception Handling

If you use the **gs_checkos** tool to check the OS and the command output shows **Abnormal**, run the following command to view detailed error information:

```
gs_checkos -i A --detail
```

The **Abnormal** state cannot be ignored because the OS in this state affects cluster installation. The **Warning** state does not affect cluster installation and thereby can be ignored.

- If the check result for OS version status (**A1**) is **Abnormal**, replace OSs out of the mixed programming scope with those within the scope.

- If the check result for kernel version status (**A2**) is **Warning**, the platform kernel versions in the cluster are inconsistent.

- If the check result for Unicode status (**A3**) is **Abnormal**, set the same character set for all the hosts. You can add **export LANG=***unicode* to the **/etc/profile** file.

  ```
  vim /etc/profile
  ```

- If the check result for time zone status (**A4**) is **Abnormal**, set the same time zone for all the hosts. You can copy the time zone file in the **/usr/share/zoneinfo/** directory as the **/etc/localtime** file.

  ```
  cp /usr/share/zoneinfo/$primary time zone/$secondary time zone /etc/localtime
  ```

- If the check result for swap memory status (**A5**) is **Abnormal**, a possible cause is that the swap memory is larger than the physical memory. You can troubleshoot this issue by reducing the swap memory or increasing the physical memory.

- If the check result for system control parameter status (**A6**) is **Abnormal**, troubleshoot this issue in either of the following two ways:

  - Run the following command:

    ```
    gs_checkos -i B1
    ```

  - Modify the **/etc/sysctl.conf** file based on the error message and run **sysctl -p** to make it take effect.

    ```
    vim /etc/sysctl.conf
    ```

- If the check result for file system configuration status (**A7**) is **Abnormal**, run the following command to troubleshoot this issue:

  ```
  gs_checkos -i B2
  ```

- If the check result for disk configuration status (**A8**) is **Abnormal**, set the disk mounting format to **rw,noatime,inode64,allocsize=16m**.

  Run the **man mount** command to mount the XFS parameter:

  ```
  rw,noatime,inode64,allocsize=16m
  ```

  You can also set the XFS parameter in the **/etc/fstab** file. For example:

  ```
  /dev/data /data xfs rw,noatime,inode64,allocsize=16m 0 0
  ```

- If the check result for pre-read block size status (**A9**) is **Abnormal**, run the following command to troubleshoot this issue:

  ```
  gs_checkos -i B3
  ```

- If the check result for I/O scheduling status (**A10**) is **Abnormal**, run the following command to troubleshoot this issue:

  ```
  gs_checkos -i B4
  ```

- If the check result for NIC configuration status (**A11**) is **Warning**, run the following command to troubleshoot this issue:

  ```
  gs_checkos -i B5
  ```

- If the check result for time consistency status (**A12**) is **Abnormal**, verify that the NTP service has been installed and started and has synchronized time from the NTP clock.

- If the check result for firewall status (**A13**) is **Abnormal**, disable the firewall. Run the following commands:

  - SUSE:

    ```
    SuSEfirewall2 stop
    ```

  - RedHat7:

    ```
    systemctl disable firewalld
    ```

  - RedHat6:

    ```
    service iptables stop
    ```

- If the check result for THP service status (**A14**) is **Abnormal**, run the following command to troubleshoot this issue:

  ```
  gs_checkos -i B6
  ```
