---
title: Checking Time Consistency
summary: Checking Time Consistency
author: Zhang Cuiping
date: 2021-03-04
---

# Checking Time Consistency

Database transaction consistency is guaranteed by a logical clock and is not affected by OS time. However, OS time inconsistency will lead to problems, such as abnormal backend O&M and monitoring functions. Therefore, you are advised to monthly check time consistency among nodes.

**Procedure**

1. Log in as the OS user **omm** to any host in the MogDB Kernel cluster.

2. Create a configuration file for recording each cluster node. (You can specify the *mpphosts* file directory randomly. It is recommended that the file be stored in the **/tmp** directory.)

   ```bash
   vim /tmp/mpphosts
   ```

   Add the host name of each node.

   ```
   plat1
   plat2
   plat3
   ```

3. Save the configuration file.

   ```
   :wq!
   ```

4. Run the following command and write the time on each node into the **/tmp/sys_ctl-os1.log** file:

   ```
   for ihost in `cat /tmp/mpphosts`; do ssh -n -q $ihost "hostname;date"; done > /tmp/sys_ctl-os1.log
   ```

5. Check time consistency between the nodes based on the command output. The time difference should not exceed 30s.

   ```
   cat /tmp/sys_ctl-os1.log
   plat1
   Thu Feb  9 16:46:38 CST 2017
   plat2
   Thu Feb  9 16:46:49 CST 2017
   plat3
   Thu Feb  9 16:46:14 CST 2017
   ```
