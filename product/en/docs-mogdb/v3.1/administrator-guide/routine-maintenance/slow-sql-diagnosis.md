---
title: Slow SQL Diagnosis
summary: Slow SQL Diagnosis
author: Guo Huan
date: 2022-04-13
---

# Slow SQL Diagnosis

## Background

If the SQL statement execution performance does not meet expectations, you can view the SQL statement execution information to analyze the behavior and diagnose problems that occur during the execution.

## Prerequisites

- The database instance is running properly.

- The GUC parameter **track_stmt_stat_level** is properly set for querying the SQL statement information.

- Only the system administrator and monitor administrator can perform this operation.

  ```sql
  Run the following command to check the execution information about the SQL statements in the database instance:
  gsql> select * from dbe_perf.get_global_full_sql_by_timestamp(start_timestamp, end_timestamp); 
  Run the following command to check the execution information about the slow SQL statements in the database instance:
  gsql> select * from dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp, end_timestamp);
  Check the execution information about the SQL statement on the current node.
  gsql> select * from statement_history;
  ```
