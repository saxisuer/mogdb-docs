---
title: Character Set Support
summary: Character Set Support
author: Guo Huan
date: 2022-07-26
---

# Character Set Support

The character set support in MogDB allows you to store text in a variety of character sets (also called encodings), including single-byte character sets such as the ISO 8859 series and multiple-byte character sets such as EUC (Extended Unix Code), UTF-8, and Mule internal code. All supported character sets can be used transparently by clients, but a few are not supported for use within the server (that is, as a server-side encoding). The default character set is selected while initializing your MogDB database cluster using `gs_initdb`. It can be overridden when you create a database, so you can have multiple databases each with a different character set.

An important restriction, however, is that each database's character set must be compatible with the database's `LC_CTYPE` (character classification) and `LC_COLLATE` (string sort order) locale settings. For `C` or `POSIX` locale, any character set is allowed, but for other locales there is only one character set that will work correctly. (On Windows, however, UTF-8 encoding can be used with any locale.)

## Supported Character Sets

Table1shows the character sets available for use in MogDB.

**Table1 MogDB Character Sets**

| Name             | Description                       | Language                       | Server? | Bytes/Char | Aliases                                       |
| ---------------- | --------------------------------- | ------------------------------ | ------- | ---------- | --------------------------------------------- |
| `BIG5`           | Big Five                          | Traditional Chinese            | No      | 1-2        | `WIN950`, `Windows950`                        |
| `EUC_CN`         | Extended UNIX Code-CN             | Simplified Chinese             | Yes     | 1-3        |                                               |
| `EUC_JP`         | Extended UNIX Code-JP             | Japanese                       | Yes     | 1-3        |                                               |
| `EUC_JIS_2004`   | Extended UNIX Code-JP, JIS X 0213 | Japanese                       | Yes     | 1-3        |                                               |
| `EUC_KR`         | Extended UNIX Code-KR             | Korean                         | Yes     | 1-3        |                                               |
| `EUC_TW`         | Extended UNIX Code-TW             | Traditional Chinese, Taiwanese | Yes     | 1-3        |                                               |
| `GB18030`        | National Standard                 | Chinese                        | Yes     | 1-4        |                                               |
| `GBK`            | Extended National Standard        | Simplified Chinese             | Yes     | 1-2        | `WIN936`, `Windows936`                        |
| `ISO_8859_5`     | ISO 8859-5, ECMA 113              | Latin/Cyrillic                 | Yes     | 1          |                                               |
| `ISO_8859_6`     | ISO 8859-6, ECMA 114              | Latin/Arabic                   | Yes     | 1          |                                               |
| `ISO_8859_7`     | ISO 8859-7, ECMA 118              | Latin/Greek                    | Yes     | 1          |                                               |
| `ISO_8859_8`     | ISO 8859-8, ECMA 121              | Latin/Hebrew                   | Yes     | 1          |                                               |
| `JOHAB`          | JOHAB                             | Korean (Hangul)                | No      | 1-3        |                                               |
| `KOI8R`          | KOI8-R                            | Cyrillic (Russian)             | Yes     | 1          | `KOI8`                                        |
| `KOI8U`          | KOI8-U                            | Cyrillic (Ukrainian)           | Yes     | 1          |                                               |
| `LATIN1`         | ISO 8859-1, ECMA 94               | Western European               | Yes     | 1          | `ISO88591`                                    |
| `LATIN2`         | ISO 8859-2, ECMA 94               | Central European               | Yes     | 1          | `ISO88592`                                    |
| `LATIN3`         | ISO 8859-3, ECMA 94               | South European                 | Yes     | 1          | `ISO88593`                                    |
| `LATIN4`         | ISO 8859-4, ECMA 94               | North European                 | Yes     | 1          | `ISO88594`                                    |
| `LATIN5`         | ISO 8859-9, ECMA 128              | Turkish                        | Yes     | 1          | `ISO88599`                                    |
| `LATIN6`         | ISO 8859-10, ECMA 144             | Nordic                         | Yes     | 1          | `ISO885910`                                   |
| `LATIN7`         | ISO 8859-13                       | Baltic                         | Yes     | 1          | `ISO885913`                                   |
| `LATIN8`         | ISO 8859-14                       | Celtic                         | Yes     | 1          | `ISO885914`                                   |
| `LATIN9`         | ISO 8859-15                       | LATIN1 with Euro and accents   | Yes     | 1          | `ISO885915`                                   |
| `LATIN10`        | ISO 8859-16, ASRO SR 14111        | Romanian                       | Yes     | 1          | `ISO885916`                                   |
| `MULE_INTERNAL`  | Mule internal code                | Multilingual Emacs             | Yes     | 1-4        |                                               |
| `SJIS`           | Shift JIS                         | Japanese                       | No      | 1-2        | `Mskanji`, `ShiftJIS`, `WIN932`, `Windows932` |
| `SHIFT_JIS_2004` | Shift JIS, JIS X 0213             | Japanese                       | No      | 1-2        |                                               |
| `SQL_ASCII`      | unspecified (see text)            | any                            | Yes     | 1          |                                               |
| `UHC`            | Unified Hangul Code               | Korean                         | No      | 1-2        | `WIN949`, `Windows949`                        |
| `UTF8`           | Unicode, 8-bit                    | all                            | Yes     | 1-4        | `Unicode`                                     |
| `WIN866`         | Windows CP866                     | Cyrillic                       | Yes     | 1          | `ALT`                                         |
| `WIN874`         | Windows CP874                     | Thai                           | Yes     | 1          |                                               |
| `WIN1250`        | Windows CP1250                    | Central European               | Yes     | 1          |                                               |
| `WIN1251`        | Windows CP1251                    | Cyrillic                       | Yes     | 1          | `WIN`                                         |
| `WIN1252`        | Windows CP1252                    | Western European               | Yes     | 1          |                                               |
| `WIN1253`        | Windows CP1253                    | Greek                          | Yes     | 1          |                                               |
| `WIN1254`        | Windows CP1254                    | Turkish                        | Yes     | 1          |                                               |
| `WIN1255`        | Windows CP1255                    | Hebrew                         | Yes     | 1          |                                               |
| `WIN1256`        | Windows CP1256                    | Arabic                         | Yes     | 1          |                                               |
| `WIN1257`        | Windows CP1257                    | Baltic                         | Yes     | 1          |                                               |
| `WIN1258`        | Windows CP1258                    | Vietnamese                     | Yes     | 1          | `ABC`, `TCVN`, `TCVN5712`, `VSCII`            |

Not all client APIs support all the listed character sets. The `SQL_ASCII` setting behaves considerably differently from the other settings. When the server character set is `SQL_ASCII`, the server interprets byte values 0-127 according to the ASCII standard, while byte values 128-255 are taken as uninterpreted characters. No encoding conversion will be done when the setting is `SQL_ASCII`. Thus, this setting is not so much a declaration that a specific encoding is in use, as a declaration of ignorance about the encoding. In most cases, if you are working with any non-ASCII data, it is unwise to use the `SQL_ASCII` setting because MogDB will be unable to help you by converting or validating non-ASCII characters.

## Setting the Character Set

`gs_initdb` defines the default character set (encoding) for a MogDB cluster. See [gs_initdb](../../reference-guide/tool-reference/tools-used-in-the-internal-system/5-gs_initdb.md) for details.

You can specify a non-default encoding at database creation time use this SQL command, provided that the encoding is compatible with the selected locale:

```sql
CREATE DATABASE chinese WITH ENCODING 'UTF8' LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8' TEMPLATE=template0;
```

Notice that the above commands specify copying the `template0` database. When copying any other database, the encoding and locale settings cannot be changed from those of the source database, because that might result in corrupt data. For more information see [CREATE DATABASE](../../reference-guide/sql-syntax/CREATE-DATABASE.md).

The encoding for a database is stored in the system catalog `pg_database`. You can see it by using the `gsql` `-l` option or the `\l` command.

```bash
$ gsql -l
                              List of databases
   Name    | Owner | Encoding |   Collate   |    Ctype    | Access privileges
-----------+-------+----------+-------------+-------------+-------------------
 chinese   | omm   | UTF8     | en_US.UTF8  | en_US.UTF8  |
 mogdb     | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 mogila    | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 postgres  | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm           +
           |       |          |             |             | omm=CTc/omm
 template1 | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm           +
           |       |          |             |             | omm=CTc/omm
(6 rows)
```

> **Important:** On most modern operating systems, MogDB can determine which character set is implied by the `LC_CTYPE` setting, and it will enforce that only the matching database encoding is used. On older systems it is your responsibility to ensure that you use the encoding expected by the locale you have selected. A mistake in this area is likely to lead to strange behavior of locale-dependent operations such as sorting.
>
> MogDB will allow superusers to create databases with `SQL_ASCII` encoding even when `LC_CTYPE` is not `C` or `POSIX`. As noted above, `SQL_ASCII` does not enforce that the data stored in the database has any particular encoding, and so this choice poses risks of locale-dependent misbehavior. Using this combination of settings is deprecated and may someday be forbidden altogether.

## Automatic Character Set Conversion Between Server and Client

MogDB supports automatic character set conversion between server and client for certain character set combinations. The conversion information is stored in the `pg_conversion` system catalog. MogDB comes with some predefined conversions, as shown in Table 2. 

**Table2 Client/Server Character Set Conversions**

| Server Character Set | Available Client Character Sets                              |
| -------------------- | ------------------------------------------------------------ |
| `BIG5`               | not supported as a server encoding                           |
| `EUC_CN`             | EUC_CN, `MULE_INTERNAL`, `UTF8`                              |
| `EUC_JP`             | EUC_JP, `MULE_INTERNAL`, `SJIS`, `UTF8`                      |
| `EUC_JIS_2004`       | EUC_JIS_2004, `SHIFT_JIS_2004`, `UTF8`                       |
| `EUC_KR`             | EUC_KR, `MULE_INTERNAL`, `UTF8`                              |
| `EUC_TW`             | EUC_TW, `BIG5`, `MULE_INTERNAL`, `UTF8`                      |
| `GB18030`            | not supported as a server encoding                           |
| `GBK`                | not supported as a server encoding                           |
| `ISO_8859_5`         | ISO_8859_5, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN866`, `WIN1251` |
| `ISO_8859_6`         | ISO_8859_6, `UTF8`                                           |
| `ISO_8859_7`         | ISO_8859_7, `UTF8`                                           |
| `ISO_8859_8`         | ISO_8859_8, `UTF8`                                           |
| `JOHAB`              | not supported as a server encoding                           |
| `KOI8R`              | KOI8R, `ISO_8859_5`, `MULE_INTERNAL`, `UTF8`, `WIN866`, `WIN1251` |
| `KOI8U`              | KOI8U, `UTF8`                                                |
| `LATIN1`             | LATIN1, `MULE_INTERNAL`, `UTF8`                              |
| `LATIN2`             | LATIN2, `MULE_INTERNAL`, `UTF8`, `WIN1250`                   |
| `LATIN3`             | LATIN3, `MULE_INTERNAL`, `UTF8`                              |
| `LATIN4`             | LATIN4, `MULE_INTERNAL`, `UTF8`                              |
| `LATIN5`             | LATIN5, `UTF8`                                               |
| `LATIN6`             | LATIN6, `UTF8`                                               |
| `LATIN7`             | LATIN7, `UTF8`                                               |
| `LATIN8`             | LATIN8, `UTF8`                                               |
| `LATIN9`             | LATIN9, `UTF8`                                               |
| `LATIN10`            | LATIN10, `UTF8`                                              |
| `MULE_INTERNAL`      | MULE_INTERNAL, `BIG5`, `EUC_CN`, `EUC_JP`, `EUC_KR`, `EUC_TW`, `ISO_8859_5`, `KOI8R`, `LATIN1` to `LATIN4`, `SJIS`, `WIN866`, `WIN1250`, `WIN1251` |
| `SJIS`               | not supported as a server encoding                           |
| `SHIFT_JIS_2004`     | not supported as a server encoding                           |
| `SQL_ASCII`          | any (no conversion will be performed)                        |
| `UHC`                | not supported as a server encoding                           |
| `UTF8`               | all supported encodings                                      |
| `WIN866`             | WIN866, `ISO_8859_5`, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN1251` |
| `WIN874`             | WIN874, `UTF8`                                               |
| `WIN1250`            | WIN1250, `LATIN2`, `MULE_INTERNAL`, `UTF8`                   |
| `WIN1251`            | WIN1251, `ISO_8859_5`, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN866` |
| `WIN1252`            | WIN1252, `UTF8`                                              |
| `WIN1253`            | WIN1253, `UTF8`                                              |
| `WIN1254`            | WIN1254, `UTF8`                                              |
| `WIN1255`            | WIN1255, `UTF8`                                              |
| `WIN1256`            | WIN1256, `UTF8`                                              |

To enable automatic character set conversion, you have to tell MogDB the character set (encoding) you would like to use in the client. There are several ways to accomplish this:

- Using the `\encoding` command in gsql. `\encoding` allows you to change client encoding on the fly. For example, to change the encoding to `SJIS`, type:

  ```sql
  \encoding SJIS
  ```

- [libpq](../../developer-guide/dev/4-development-based-on-libpq/2-libpq/1-database-connection-control-functions/1-database-connection-control-functions-overview.md) has functions to control the client encoding.

- Using `SET client_encoding TO`. Setting the client encoding can be done with this SQL command:

  ```bash
  SET CLIENT_ENCODING TO 'value';
  ```

  Also you can use the standard SQL syntax `SET NAMES` for this purpose:

  ```bash
  SET NAMES 'value';
  ```

  To query the current client encoding:

  ```bash
  SHOW client_encoding;
  ```

  To return to the default encoding:

  ```bash
  RESET client_encoding;
  ```

- Using `PGCLIENTENCODING`. If the environment variable `PGCLIENTENCODING` is defined in the client's environment, that client encoding is automatically selected when a connection to the server is made. (This can subsequently be overridden using any of the other methods mentioned above.)

- Using the configuration variable [client_encoding](../../reference-guide/guc-parameters/15-default-settings-of-client-connection/2-zone-and-formatting.md#client_encoding). If the `client_encoding` variable is set, that client encoding is automatically selected when a connection to the server is made. (This can subsequently be overridden using any of the other methods mentioned above.)

If the conversion of a particular character is not possible — suppose you chose `EUC_JP` for the server and `LATIN1` for the client, and some Japanese characters are returned that do not have a representation in `LATIN1` — an error is reported.

If the client character set is defined as `SQL_ASCII`, encoding conversion is disabled, regardless of the server's character set. Just as for the server, use of `SQL_ASCII` is unwise unless you are working with all-ASCII data.

## Further Reading

These are good sources to start learning about various kinds of encoding systems.

- CJKV Information Processing: Chinese, Japanese, Korean & Vietnamese Computing

  Contains detailed explanations of `EUC_JP`, `EUC_CN`, `EUC_KR`, `EUC_TW`.

- [http://www.unicode.org/](http://www.unicode.org/)

  The web site of the Unicode Consortium.

- RFC 3629

  UTF-8 (8-bit UCS/Unicode Transformation Format) is defined here.
