---
title: Import Modes
summary: Import Modes
author: Guo Huan
date: 2021-03-04
---

# Import Modes

You can use **INSERT**, **COPY**, or **\copy** (a **gsql** meta-command) to import data to the MogDB database. The methods have different characteristics. For details, see Table 1.

**Table 1** Import modes<a id="Import modes"> </a>

| Mode                               | Characteristics                                              |
| :--------------------------------- | :----------------------------------------------------------- |
| INSERT                             | Insert one or more rows of data, or insert data from a specified table. |
| COPY                               | Run the **COPY FROM STDIN** statement to write data into the MogDB database.<br/>Service data does not need to be stored in files when it is written from other databases to the MogDB database through the CopyManager interface driven by JDBC. |
| **\copy**, a **gsql** meta-command | Different from the SQL **COPY** statement, the **\copy** command can read data from or write data into only local files on a **gsql** client.<br/>NOTE:<br/>**\copy** applies only to small-scale data import in good format. It does not preprocess invalid characters or provide error tolerance. Therefore, **\copy** cannot be used in scenarios where abnormal data exists. **COPY** is preferred for data import. |
