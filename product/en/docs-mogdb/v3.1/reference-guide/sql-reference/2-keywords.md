---
title: Keywords
summary: Keywords
author: Zhang Cuiping
date: 2021-05-17
---

# Keywords

The SQL contains reserved and non-reserved words. Standards require that reserved keywords not be used as other identifiers. Non-reserved keywords have special meanings only in a specific environment and can be used as identifiers in other environments.

The naming rules for identifiers are as follows:

- An identifier name can only contain letters, underscores, digits (0-9), and dollar signs ($).

- An identifier name must start with a letter (a to z) or an underscore (_).

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - The naming rules are recommended but not mandatory.
  > - In special cases, double quotation marks can be used to avoid special character errors.

**Table 1** SQL keywords

| Keyword                     | MogDB                                        |
| :-------------------------- | :------------------------------------------- |
| ABORT                       | Non-reserved                                 |
| ABS                         | N/A                                          |
| ABSOLUTE                    | Non-reserved                                 |
| ACCESS                      | Non-reserved                                 |
| ACCOUNT                     | Non-reserved                                 |
| ACTION                      | Non-reserved                                 |
| ADA                         | N/A                                          |
| ADD                         | Non-reserved                                 |
| ADMIN                       | Non-reserved                                 |
| AFTER                       | Non-reserved                                 |
| AGGREGATE                   | Non-reserved                                 |
| ALGORITHM                   | Non-reserved                                 |
| ALIAS                       | N/A                                          |
| ALL                         | Reserved                                     |
| ALLOCATE                    | N/A                                          |
| ALSO                        | Non-reserved                                 |
| ALTER                       | Non-reserved                                 |
| ALWAYS                      | Non-reserved                                 |
| ANALYSE                     | Reserved                                     |
| ANALYZE                     | Reserved                                     |
| AND                         | Reserved                                     |
| ANY                         | Reserved                                     |
| APP                         | Non-reserved                                 |
| ARE                         | N/A                                          |
| ARRAY                       | Reserved                                     |
| AS                          | Reserved                                     |
| ASC                         | Reserved                                     |
| ASENSITIVE                  | N/A                                          |
| ASSERTION                   | Non-reserved                                 |
| ASSIGNMENT                  | Non-reserved                                 |
| ASYMMETRIC                  | Reserved                                     |
| AT                          | Non-reserved                                 |
| ATOMIC                      | N/A                                          |
| ATTRIBUTE                   | Non-reserved                                 |
| AUTHID                      | Reserved                                     |
| AUTHORIZATION               | Reserved (functions and types allowed)       |
| AUTOEXTEND                  | Non-reserved                                 |
| AUTOMAPPED                  | Non-reserved                                 |
| AVG                         | N/A                                          |
| BACKWARD                    | Non-reserved                                 |
| BARRIER                     | Non-reserved                                 |
| BEFORE                      | Non-reserved                                 |
| BEGIN                       | Non-reserved                                 |
| BEGIN_NON_ANOYBLOCK         | Non-reserved                                 |
| BETWEEN                     | Non-reserved (excluding functions and types) |
| BIGINT                      | Non-reserved (excluding functions and types) |
| BINARY                      | Reserved (functions and types allowed)       |
| BINARY_DOUBLE               | Non-reserved (excluding functions and types) |
| BINARY_INTEGER              | Non-reserved (excluding functions and types) |
| BIT                         | Non-reserved (excluding functions and types) |
| BITVAR                      | N/A                                          |
| BIT_LENGTH                  | N/A                                          |
| BLOB                        | Non-reserved                                 |
| BOOLEAN                     | Non-reserved (excluding functions and types) |
| BOTH                        | Reserved                                     |
| BUCKETS                     | Reserved                                     |
| BREADTH                     | N/A                                          |
| BY                          | Non-reserved                                 |
| BYTEAWITHOUTODER            | Non-reserved (excluding functions and types) |
| BYTEAWITHOUTORDERWITHEQUAL  | Non-reserved (excluding functions and types) |
| C                           | N/A                                          |
| CACHE                       | Non-reserved                                 |
| CALL                        | Non-reserved                                 |
| CALLED                      | Non-reserved                                 |
| CARDINALITY                 | N/A                                          |
| CASCADE                     | Non-reserved                                 |
| CASCADED                    | Non-reserved                                 |
| CASE                        | Reserved                                     |
| CAST                        | Reserved                                     |
| CATALOG                     | Non-reserved                                 |
| CATALOG_NAME                | N/A                                          |
| CHAIN                       | Non-reserved                                 |
| CHAR                        | Non-reserved (excluding functions and types) |
| CHARACTER                   | Non-reserved (excluding functions and types) |
| CHARACTERISTICS             | Non-reserved                                 |
| CHARACTER_LENGTH            | N/A                                          |
| CHARACTER_SET_CATALOG       | N/A                                          |
| CHARACTER_SET_NAME          | N/A                                          |
| CHARACTER_SET_SCHEMA        | N/A                                          |
| CHAR_LENGTH                 | N/A                                          |
| CHECK                       | Reserved                                     |
| CHECKED                     | N/A                                          |
| CHECKPOINT                  | Non-reserved                                 |
| CLASS                       | Non-reserved                                 |
| CLEAN                       | Non-reserved                                 |
| CLASS_ORIGIN                | N/A                                          |
| CLIENT                      | Non-reserved                                 |
| CLIENT_MASTER_KEY           | Non-reserved                                 |
| CLIENT_MASTER_KEYS          | Non-reserved                                 |
| CLOB                        | Non-reserved                                 |
| CLOSE                       | Non-reserved                                 |
| CLUSTER                     | Non-reserved                                 |
| COALESCE                    | Non-reserved (excluding functions and types) |
| COBOL                       | N/A                                          |
| COLLATE                     | Reserved                                     |
| COLLATION                   | Reserved (functions and types allowed)       |
| COLLATION_CATALOG           | N/A                                          |
| COLLATION_NAME              | N/A                                          |
| COLLATION_SCHEMA            | N/A                                          |
| COLUMN                      | Reserved                                     |
| COLUMN_ENCRYPTION_KEY       | Non-reserved                                 |
| COLUMN_ENCRYPTION_KEYS      | Non-reserved                                 |
| COLUMN_NAME                 | N/A                                          |
| COMPACT                     | Reserved (functions and types allowed)       |
| COMPATIBLE_ILLEGAL_CHARS    | Non-reserved                                 |
| COMMAND_FUNCTION            | N/A                                          |
| COMPLETE                    | Non-reserved                                 |
| COMMAND_FUNCTION_CODE       | N/A                                          |
| COMMENT                     | Non-reserved                                 |
| COMMENTS                    | Non-reserved                                 |
| COMMIT                      | Non-reserved                                 |
| COMMITTED                   | Non-reserved                                 |
| COMPRESS                    | Non-reserved                                 |
| COMPLETION                  | N/A                                          |
| CONCURRENTLY                | Reserved (functions and types allowed)       |
| CONDITION                   | N/A                                          |
| CONDITION_NUMBER            | N/A                                          |
| CONFIGURATION               | Non-reserved                                 |
| CONNECT                     | N/A                                          |
| CONNECTION                  | Non-reserved                                 |
| CONNECTION_NAME             | N/A                                          |
| CONSTRAINT                  | Reserved                                     |
| CONSTRAINTS                 | Non-reserved                                 |
| CONSTRAINT_CATALOG          | N/A                                          |
| CONSTRAINT_NAME             | N/A                                          |
| CONSTRAINT_SCHEMA           | N/A                                          |
| CONSTRUCTOR                 | N/A                                          |
| CONTAINS                    | N/A                                          |
| CONTENT                     | Non-reserved                                 |
| CONTINUE                    | Non-reserved                                 |
| CONVERSION                  | Non-reserved                                 |
| CONVERT                     | N/A                                          |
| COPY                        | Non-reserved                                 |
| CORRESPONDING               | N/A                                          |
| COST                        | Non-reserved                                 |
| COUNT                       | N/A                                          |
| CREATE                      | Reserved                                     |
| CROSS                       | Reserved (functions and types allowed)       |
| CSV                         | Non-reserved                                 |
| CUBE                        | Non-reserved                                 |
| CURRENT                     | Non-reserved                                 |
| CURRENT_CATALOG             | Reserved                                     |
| CURRENT_DATE                | Reserved                                     |
| CURRENT_PATH                | N/A                                          |
| CURRENT_ROLE                | Reserved                                     |
| CURRENT_SCHEMA              | Reserved (functions and types allowed)       |
| CURRENT_TIME                | Reserved                                     |
| CURRENT_TIMESTAMP           | Reserved                                     |
| CURRENT_USER                | Reserved                                     |
| CURSOR                      | Non-reserved                                 |
| CURSOR_NAME                 | N/A                                          |
| CYCLE                       | Non-reserved                                 |
| DATA                        | Non-reserved                                 |
| DATABASE                    | Non-reserved                                 |
| DATAFILE                    | Non-reserved                                 |
| DATE_FORMAT                 | Non-reserved                                 |
| DATATYPE_CL                 | Non-reserved                                 |
| DATE                        | Non-reserved (excluding functions and types) |
| DELTAMERGE                  | Reserved (functions and types allowed)       |
| DATETIME_INTERVAL_CODE      | N/A                                          |
| DATETIME_INTERVAL_PRECISION | N/A                                          |
| DAY                         | Non-reserved                                 |
| DBCOMPATIBILITY             | Non-reserved                                 |
| DEALLOCATE                  | Non-reserved                                 |
| DEC                         | Non-reserved (excluding functions and types) |
| DECIMAL                     | Non-reserved (excluding functions and types) |
| DECLARE                     | Non-reserved                                 |
| DECODE                      | Non-reserved (excluding functions and types) |
| DEFAULT                     | Reserved                                     |
| DEFAULTS                    | Non-reserved                                 |
| DEFERRABLE                  | Reserved                                     |
| DEFERRED                    | Non-reserved                                 |
| DEFINED                     | N/A                                          |
| DEFINER                     | Non-reserved                                 |
| DELETE                      | Non-reserved                                 |
| DELIMITER                   | Non-reserved                                 |
| DELIMITERS                  | Non-reserved                                 |
| DELTA                       | Non-reserved                                 |
| DEPTH                       | N/A                                          |
| DEREF                       | N/A                                          |
| DESC                        | Reserved                                     |
| DESCRIBE                    | N/A                                          |
| DESCRIPTOR                  | N/A                                          |
| DESTROY                     | N/A                                          |
| DESTRUCTOR                  | N/A                                          |
| DETERMINISTIC               | Non-reserved                                 |
| DIAGNOSTICS                 | N/A                                          |
| DICTIONARY                  | Non-reserved                                 |
| DIRECT                      | Non-reserved                                 |
| DIRECTORY                   | Non-reserved                                 |
| DISABLE                     | Non-reserved                                 |
| DISCARD                     | Non-reserved                                 |
| DISCONNECT                  | Non-reserved                                 |
| DISPATCH                    | N/A                                          |
| DISTINCT                    | Reserved                                     |
| DISTRIBUTE                  | Non-reserved                                 |
| DISTRIBUTION                | Non-reserved                                 |
| DO                          | Reserved                                     |
| DOCUMENT                    | Non-reserved                                 |
| DOMAIN                      | Non-reserved                                 |
| DOUBLE                      | Non-reserved                                 |
| DROP                        | Non-reserved                                 |
| DUPLICATE                   | Non-reserved                                 |
| DYNAMIC                     | N/A                                          |
| DYNAMIC_FUNCTION            | N/A                                          |
| DYNAMIC_FUNCTION_CODE       | N/A                                          |
| EACH                        | Non-reserved                                 |
| ELSE                        | Reserved                                     |
| ELASTIC                     | Non-reserved                                 |
| ENABLE                      | Non-reserved                                 |
| ENCODING                    | Non-reserved                                 |
| ENCRYPTED                   | Non-reserved                                 |
| ENCRYPTED_VALUE             | Non-reserved                                 |
| ENCRYPTION                  | Non-reserved                                 |
| ENCRYPTION_TYPE             | Non-reserved                                 |
| END                         | Reserved                                     |
| END-EXEC                    | N/A                                          |
| ENFORCED                    | Non-reserved                                 |
| ENUM                        | Non-reserved                                 |
| EOL                         | Non-reserved                                 |
| ERRORS                      | Non-reserved                                 |
| EQUALS                      | N/A                                          |
| ESCAPE                      | Non-reserved                                 |
| ESCAPING                    | Non-reserved                                 |
| EVERY                       | Non-reserved                                 |
| EXCEPT                      | Reserved                                     |
| EXCEPTION                   | N/A                                          |
| EXCHANGE                    | Non-reserved                                 |
| EXCLUDE                     | Non-reserved                                 |
| EXCLUDED                    | Reserved                                     |
| EXCLUDING                   | Non-reserved                                 |
| EXCLUSIVE                   | Non-reserved                                 |
| EXEC                        | N/A                                          |
| EXECUTE                     | Non-reserved                                 |
| EXISTING                    | N/A                                          |
| EXISTS                      | Non-reserved (excluding functions and types) |
| EXPIRED_P                   | Non-reserved                                 |
| EXPLAIN                     | Non-reserved                                 |
| EXTENSION                   | Non-reserved                                 |
| EXTERNAL                    | Non-reserved                                 |
| EXTRACT                     | Non-reserved (excluding functions and types) |
| FALSE                       | Reserved                                     |
| FAMILY                      | Non-reserved                                 |
| FAST                        | Non-reserved                                 |
| FETCH                       | Reserved                                     |
| FENCED                      | Reserved                                     |
| FILEHEADER                  | Non-reserved                                 |
| FINAL                       | N/A                                          |
| FIRST                       | Non-reserved                                 |
| FIXED                       | Non-reserved                                 |
| FILL_MISSING_FIELDS         | Non-reserved                                 |
| FLOAT                       | Non-reserved (excluding functions and types) |
| FOLLOWING                   | Non-reserved                                 |
| FOR                         | Reserved                                     |
| FORCE                       | Non-reserved                                 |
| FOREIGN                     | Reserved                                     |
| FORMATTER                   | Non-reserved                                 |
| FORTRAN                     | N/A                                          |
| FORWARD                     | Non-reserved                                 |
| FOUND                       | N/A                                          |
| FREE                        | N/A                                          |
| FREEZE                      | Reserved (functions and types allowed)       |
| FROM                        | Reserved                                     |
| FULL                        | Reserved (functions and types allowed)       |
| FUNCTION                    | Non-reserved                                 |
| FUNCTIONS                   | Non-reserved                                 |
| G                           | N/A                                          |
| GENERAL                     | N/A                                          |
| GENERATED                   | N/A                                          |
| GET                         | N/A                                          |
| GLOBAL                      | Non-reserved                                 |
| GO                          | N/A                                          |
| GOTO                        | N/A                                          |
| GRANT                       | Reserved                                     |
| GRANTED                     | Non-reserved                                 |
| GREATEST                    | Non-reserved (excluding functions and types) |
| GROUP                       | Reserved                                     |
| GROUPING                    | Non-reserved (excluding functions and types) |
| HANDLER                     | Non-reserved                                 |
| HAVING                      | Reserved                                     |
| HDFSDIRECTORY               | Reserved (functions and types allowed)       |
| HEADER                      | Non-reserved                                 |
| HIERARCHY                   | N/A                                          |
| HOLD                        | Non-reserved                                 |
| HOST                        | N/A                                          |
| HOUR                        | Non-reserved                                 |
| IDENTIFIED                  | Non-reserved                                 |
| IDENTITY                    | Non-reserved                                 |
| IF                          | Non-reserved                                 |
| IGNORE                      | N/A                                          |
| IGNORE_EXTRA_DATA           | Non-reserved                                 |
| ILIKE                       | Reserved (functions and types allowed)       |
| IMMEDIATE                   | Non-reserved                                 |
| IMMUTABLE                   | Non-reserved                                 |
| IMPLEMENTATION              | N/A                                          |
| IMPLICIT                    | Non-reserved                                 |
| IN                          | Reserved                                     |
| INTERNAL                    | Non-reserved                                 |
| INCLUDING                   | Non-reserved                                 |
| INCREMENT                   | Non-reserved                                 |
| INDEX                       | Non-reserved                                 |
| INDEXES                     | Non-reserved                                 |
| INDICATOR                   | N/A                                          |
| INFIX                       | N/A                                          |
| INHERIT                     | Non-reserved                                 |
| INHERITS                    | Non-reserved                                 |
| INITIAL                     | Non-reserved                                 |
| INITIALIZE                  | N/A                                          |
| INITIALLY                   | Reserved                                     |
| INITRANS                    | Non-reserved                                 |
| INLINE                      | Non-reserved                                 |
| INNER                       | Reserved (functions and types allowed)       |
| INOUT                       | Non-reserved (excluding functions and types) |
| INPUT                       | Non-reserved                                 |
| INSENSITIVE                 | Non-reserved                                 |
| INSERT                      | Non-reserved                                 |
| INSTANCE                    | N/A                                          |
| INSTANTIABLE                | N/A                                          |
| INSTEAD                     | Non-reserved                                 |
| INT                         | Non-reserved (excluding functions and types) |
| INTEGER                     | Non-reserved (excluding functions and types) |
| INTERSECT                   | Reserved                                     |
| INTERVAL                    | Non-reserved (excluding functions and types) |
| INTO                        | Reserved                                     |
| INVOKER                     | Non-reserved                                 |
| IS                          | Reserved                                     |
| ISNULL                      | Non-reserved                                 |
| ISOLATION                   | Non-reserved                                 |
| ITERATE                     | N/A                                          |
| JOIN                        | Reserved (functions and types allowed)       |
| K                           | N/A                                          |
| KEY                         | Non-reserved                                 |
| KEY_PATH                    | Non-reserved                                 |
| KEY_MEMBER                  | N/A                                          |
| KEY_STORE                   | Non-reserved                                 |
| KEY_TYPE                    | N/A                                          |
| KILL                        | Non-reserved                                 |
| LABEL                       | Non-reserved                                 |
| LANGUAGE                    | Non-reserved                                 |
| LARGE                       | Non-reserved                                 |
| LAST                        | Non-reserved                                 |
| LATERAL                     | N/A                                          |
| LC_COLLATE                  | Non-reserved                                 |
| LC_CTYPE                    | Non-reserved                                 |
| LEADING                     | Reserved                                     |
| LEAKPROOF                   | Non-reserved                                 |
| LEAST                       | Non-reserved (excluding functions and types) |
| LEFT                        | Reserved (functions and types allowed)       |
| LENGTH                      | N/A                                          |
| LESS                        | Reserved                                     |
| LEVEL                       | Non-reserved                                 |
| LIKE                        | Reserved (functions and types allowed)       |
| LIMIT                       | Reserved                                     |
| LISTEN                      | Non-reserved                                 |
| LOAD                        | Non-reserved                                 |
| LOCAL                       | Non-reserved                                 |
| LOCALTIME                   | Reserved                                     |
| LOCALTIMESTAMP              | Reserved                                     |
| LOCATION                    | Non-reserved                                 |
| LOCATOR                     | N/A                                          |
| LOCK                        | Non-reserved                                 |
| LOG                         | Non-reserved                                 |
| LOGGING                     | Non-reserved                                 |
| LOOP                        | Non-reserved                                 |
| LOWER                       | N/A                                          |
| MAP                         | N/A                                          |
| MAPPING                     | Non-reserved                                 |
| MASTER                      | Non-reserved                                 |
| MATCH                       | Non-reserved                                 |
| MATCHED                     | Non-reserved                                 |
| MAX                         | N/A                                          |
| MAXEXTENTS                  | Non-reserved                                 |
| MAXSIZE                     | Non-reserved                                 |
| MAXTRANS                    | Non-reserved                                 |
| MAXVALUE                    | Reserved                                     |
| MERGE                       | Non-reserved                                 |
| MESSAGE_LENGTH              | N/A                                          |
| MESSAGE_OCTET_LENGTH        | N/A                                          |
| MESSAGE_TEXT                | N/A                                          |
| METHOD                      | N/A                                          |
| MIN                         | N/A                                          |
| MINEXTENTS                  | Non-reserved                                 |
| MINUS                       | Reserved                                     |
| MINUTE                      | Non-reserved                                 |
| MINVALUE                    | Non-reserved                                 |
| MOD                         | N/A                                          |
| MODE                        | Non-reserved                                 |
| MODIFIES                    | N/A                                          |
| MODIFY                      | Reserved                                     |
| MODULE                      | N/A                                          |
| MONTH                       | Non-reserved                                 |
| MORE                        | N/A                                          |
| MOVE                        | Non-reserved                                 |
| MOVEMENT                    | Non-reserved                                 |
| MUMPS                       | N/A                                          |
| NAME                        | Non-reserved                                 |
| NAMES                       | Non-reserved                                 |
| NATIONAL                    | Non-reserved (excluding functions and types) |
| NATURAL                     | Reserved (functions and types allowed)       |
| NCHAR                       | Non-reserved (excluding functions and types) |
| NCLOB                       | N/A                                          |
| NEW                         | N/A                                          |
| NEXT                        | Non-reserved                                 |
| NO                          | Non-reserved                                 |
| NOCOMPRESS                  | Non-reserved                                 |
| NOCYCLE                     | Non-reserved                                 |
| NODE                        | Non-reserved                                 |
| NOLOGGING                   | Non-reserved                                 |
| NOMAXVALUE                  | Non-reserved                                 |
| NOMINVALUE                  | Non-reserved                                 |
| NONE                        | Non-reserved (excluding functions and types) |
| NOT                         | Reserved                                     |
| NOTHING                     | Non-reserved                                 |
| NOTIFY                      | Non-reserved                                 |
| NOTNULL                     | Reserved (functions and types allowed)       |
| NOWAIT                      | Non-reserved                                 |
| NULL                        | Reserved                                     |
| NULLABLE                    | N/A                                          |
| NULLIF                      | Non-reserved (excluding functions and types) |
| NULLS                       | Non-reserved                                 |
| NUMBER                      | Non-reserved (excluding functions and types) |
| NUMERIC                     | Non-reserved (excluding functions and types) |
| NUMSTR                      | Non-reserved                                 |
| NVARCHAR2                   | Non-reserved (excluding functions and types) |
| NVL                         | Non-reserved (excluding functions and types) |
| OBJECT                      | Non-reserved                                 |
| OCTET_LENGTH                | N/A                                          |
| OF                          | Non-reserved                                 |
| OFF                         | Non-reserved                                 |
| OFFSET                      | Reserved                                     |
| OIDS                        | Non-reserved                                 |
| OLD                         | N/A                                          |
| ON                          | Reserved                                     |
| ONLY                        | Reserved                                     |
| OPEN                        | N/A                                          |
| OPERATION                   | N/A                                          |
| OPERATOR                    | Non-reserved                                 |
| OPTIMIZATION                | Non-reserved                                 |
| OPTION                      | Non-reserved                                 |
| OPTIONS                     | Non-reserved                                 |
| OR                          | Reserved                                     |
| ORDER                       | Reserved                                     |
| ORDINALITY                  | N/A                                          |
| OUT                         | Non-reserved (excluding functions and types) |
| OUTER                       | Reserved (functions and types allowed)       |
| OUTPUT                      | N/A                                          |
| OVER                        | Non-reserved                                 |
| OVERLAPS                    | Reserved (functions and types allowed)       |
| OVERLAY                     | Non-reserved (excluding functions and types) |
| OVERRIDING                  | N/A                                          |
| OWNED                       | Non-reserved                                 |
| OWNER                       | Non-reserved                                 |
| PACKAGE                     | Non-reserved                                 |
| PAD                         | N/A                                          |
| PARAMETER                   | N/A                                          |
| PARAMETERS                  | N/A                                          |
| PARAMETER_MODE              | N/A                                          |
| PARAMETER_NAME              | N/A                                          |
| PARAMETER_ORDINAL_POSITION  | N/A                                          |
| PARAMETER_SPECIFIC_CATALOG  | N/A                                          |
| PARAMETER_SPECIFIC_NAME     | N/A                                          |
| PARAMETER_SPECIFIC_SCHEMA   | N/A                                          |
| PARSER                      | Non-reserved                                 |
| PARTIAL                     | Non-reserved                                 |
| PARTITION                   | Non-reserved                                 |
| PARTITIONS                  | Non-reserved                                 |
| PASCAL                      | N/A                                          |
| PASSING                     | Non-reserved                                 |
| PASSWORD                    | Non-reserved                                 |
| PATH                        | N/A                                          |
| PCTFREE                     | Non-reserved                                 |
| PER                         | Non-reserved                                 |
| PERM                        | Non-reserved                                 |
| PERCENT                     | Non-reserved                                 |
| PERFORMANCE                 | Reserved                                     |
| PLACING                     | Reserved                                     |
| PLAN                        | Non-reserved                                 |
| PLANS                       | Non-reserved                                 |
| PLI                         | N/A                                          |
| POOL                        | Non-reserved                                 |
| POLICY                      | Non-reserved                                 |
| POSITION                    | Non-reserved (excluding functions and types) |
| POSTFIX                     | N/A                                          |
| PRECEDING                   | Non-reserved                                 |
| PRECISION                   | Non-reserved (excluding functions and types) |
| PREFERRED                   | Non-reserved                                 |
| PREFIX                      | Non-reserved                                 |
| PREORDER                    | N/A                                          |
| PREPARE                     | Non-reserved                                 |
| PREPARED                    | Non-reserved                                 |
| PRESERVE                    | Non-reserved                                 |
| PRIMARY                     | Reserved                                     |
| PRIOR                       | Non-reserved                                 |
| PRIVATE                     | Non-reserved                                 |
| PRIVILEGE                   | Non-reserved                                 |
| PRIVILEGES                  | Non-reserved                                 |
| PROCEDURAL                  | Non-reserved                                 |
| PROCEDURE                   | Reserved                                     |
| PROFILE                     | Non-reserved                                 |
| PUBLIC                      | N/A                                          |
| QUERY                       | Non-reserved                                 |
| QUOTE                       | Non-reserved                                 |
| RANDOMIZED                  | Non-reserved                                 |
| RANGE                       | Non-reserved                                 |
| RAW                         | Non-reserved                                 |
| READ                        | Non-reserved                                 |
| READS                       | N/A                                          |
| REAL                        | Non-reserved (excluding functions and types) |
| REASSIGN                    | Non-reserved                                 |
| REBUILD                     | Non-reserved                                 |
| RECHECK                     | Non-reserved                                 |
| RECURSIVE                   | Non-reserved                                 |
| REF                         | Non-reserved                                 |
| REFERENCES                  | Reserved                                     |
| REFERENCING                 | N/A                                          |
| REINDEX                     | Non-reserved                                 |
| REJECT                      | Reserved                                     |
| RELATIVE                    | Non-reserved                                 |
| RELEASE                     | Non-reserved                                 |
| RELOPTIONS                  | Non-reserved                                 |
| REMOTE                      | Non-reserved                                 |
| RENAME                      | Non-reserved                                 |
| REPEATABLE                  | Non-reserved                                 |
| REPLACE                     | Non-reserved                                 |
| REPLICA                     | Non-reserved                                 |
| RESET                       | Non-reserved                                 |
| RESIZE                      | Non-reserved                                 |
| RESOURCE                    | Non-reserved                                 |
| RESTART                     | Non-reserved                                 |
| RESTRICT                    | Non-reserved                                 |
| RESULT                      | N/A                                          |
| RETURN                      | Non-reserved                                 |
| RETURNED_LENGTH             | N/A                                          |
| RETURNED_OCTET_LENGTH       | N/A                                          |
| RETURNED_SQLSTATE           | N/A                                          |
| RETURNING                   | Reserved                                     |
| RETURNS                     | Non-reserved                                 |
| REUSE                       | Non-reserved                                 |
| REVOKE                      | Non-reserved                                 |
| RIGHT                       | Reserved (functions and types allowed)       |
| ROLE                        | Non-reserved                                 |
| ROLLBACK                    | Non-reserved                                 |
| ROLLUP                      | Non-reserved                                 |
| ROUTINE                     | N/A                                          |
| ROUTINE_CATALOG             | N/A                                          |
| ROUTINE_NAME                | N/A                                          |
| ROUTINE_SCHEMA              | N/A                                          |
| ROW                         | Non-reserved (excluding functions and types) |
| ROWS                        | Non-reserved                                 |
| ROW_COUNT                   | N/A                                          |
| RULE                        | Non-reserved                                 |
| ROWNUM                      | Reserved                                     |
| SAVEPOINT                   | Non-reserved                                 |
| SCALE                       | N/A                                          |
| SCHEMA                      | Non-reserved                                 |
| SCHEMA_NAME                 | N/A                                          |
| SCOPE                       | N/A                                          |
| SCROLL                      | Non-reserved                                 |
| SEARCH                      | Non-reserved                                 |
| SECOND                      | Non-reserved                                 |
| SECTION                     | N/A                                          |
| SECURITY                    | Non-reserved                                 |
| SELECT                      | Reserved                                     |
| SELF                        | N/A                                          |
| SENSITIVE                   | N/A                                          |
| SEQUENCE                    | Non-reserved                                 |
| SEQUENCES                   | Non-reserved                                 |
| SERIALIZABLE                | Non-reserved                                 |
| SERVER                      | Non-reserved                                 |
| SERVER_NAME                 | N/A                                          |
| SESSION                     | Non-reserved                                 |
| SESSION_USER                | Reserved                                     |
| SET                         | Non-reserved                                 |
| SETOF                       | Non-reserved (excluding functions and types) |
| SETS                        | N/A                                          |
| SHARE                       | Non-reserved                                 |
| SHIPPABLE                   | Non-reserved                                 |
| SHOW                        | Non-reserved                                 |
| SHUTDOWN                    | Non-reserved                                 |
| SIMILAR                     | Reserved (functions and types allowed)       |
| SIMPLE                      | Non-reserved                                 |
| SIZE                        | Non-reserved                                 |
| SMALLDATETIME_FORMAT        | Non-reserved                                 |
| SMALLDATETIME               | Non-reserved (excluding functions and types) |
| SMALLINT                    | Non-reserved (excluding functions and types) |
| SNAPSHOT                    | Non-reserved                                 |
| SOME                        | Reserved                                     |
| SOURCE                      | Non-reserved                                 |
| SPACE                       | Non-reserved                                 |
| SPECIFIC                    | N/A                                          |
| SPECIFICTYPE                | N/A                                          |
| SPECIFIC_NAME               | N/A                                          |
| SPILL                       | Non-reserved                                 |
| SPLIT                       | Non-reserved                                 |
| SQL                         | N/A                                          |
| SQLCODE                     | N/A                                          |
| SQLERROR                    | N/A                                          |
| SQLEXCEPTION                | N/A                                          |
| SQLSTATE                    | N/A                                          |
| SQLWARNING                  | N/A                                          |
| STABLE                      | Non-reserved                                 |
| STANDALONE                  | Non-reserved                                 |
| START                       | Non-reserved                                 |
| STATE                       | N/A                                          |
| STATEMENT                   | Non-reserved                                 |
| STATEMENT_ID                | Non-reserved                                 |
| STATIC                      | N/A                                          |
| STATISTICS                  | Non-reserved                                 |
| STDIN                       | Non-reserved                                 |
| STDOUT                      | Non-reserved                                 |
| STORAGE                     | Non-reserved                                 |
| STORE                       | Non-reserved                                 |
| STRICT                      | Non-reserved                                 |
| STRIP                       | Non-reserved                                 |
| STRUCTURE                   | N/A                                          |
| STYLE                       | N/A                                          |
| SUBCLASS_ORIGIN             | N/A                                          |
| SUBLIST                     | N/A                                          |
| SUBSTRING                   | Non-reserved (excluding functions and types) |
| SUM                         | N/A                                          |
| SYMMETRIC                   | Reserved                                     |
| SYNONYM                     | Non-reserved                                 |
| SYS_REFCURSOR               | Non-reserved                                 |
| SYSDATE                     | Reserved                                     |
| SYSID                       | Non-reserved                                 |
| SYSTEM                      | Non-reserved                                 |
| SYSTEM_USER                 | N/A                                          |
| TABLE                       | Reserved                                     |
| TABLES                      | Non-reserved                                 |
| TABLE_NAME                  | N/A                                          |
| TIME_FORMAT                 | Non-reserved                                 |
| TIMESTAMP_FORMAT            | Non-reserved                                 |
| TEMP                        | Non-reserved                                 |
| TEMPLATE                    | Non-reserved                                 |
| TEMPORARY                   | Non-reserved                                 |
| TERMINATE                   | N/A                                          |
| TEXT                        | Non-reserved                                 |
| THAN                        | Non-reserved                                 |
| THEN                        | Reserved                                     |
| TIME                        | Non-reserved (excluding functions and types) |
| TIMESTAMP                   | Non-reserved (excluding functions and types) |
| TIMESTAMPDIFF               | Non-reserved (excluding functions and types) |
| TIMEZONE_HOUR               | N/A                                          |
| TIMEZONE_MINUTE             | N/A                                          |
| TINYINT                     | Non-reserved (excluding functions and types) |
| TO                          | Reserved                                     |
| TRAILING                    | Reserved                                     |
| TRANSACTION                 | Non-reserved                                 |
| TRANSACTIONS_COMMITTED      | N/A                                          |
| TRANSACTIONS_ROLLED_BACK    | N/A                                          |
| TRANSACTION_ACTIVE          | N/A                                          |
| TRANSFORM                   | N/A                                          |
| TRANSFORMS                  | N/A                                          |
| TRANSLATE                   | N/A                                          |
| TRANSLATION                 | N/A                                          |
| TREAT                       | Non-reserved (excluding functions and types) |
| TRIGGER                     | Non-reserved                                 |
| TRIGGER_CATALOG             | N/A                                          |
| TRIGGER_NAME                | N/A                                          |
| TRIGGER_SCHEMA              | N/A                                          |
| TRIM                        | Non-reserved (excluding functions and types) |
| TRUE                        | Reserved                                     |
| TRUNCATE                    | Non-reserved                                 |
| TRUSTED                     | Non-reserved                                 |
| TSFIELD                     | Non-reserved                                 |
| TSTAG                       | Non-reserved                                 |
| TSTIME                      | Non-reserved                                 |
| TYPE                        | Non-reserved                                 |
| TYPES                       | Non-reserved                                 |
| UESCAPE                     | N/A                                          |
| UNBOUNDED                   | Non-reserved                                 |
| UNCOMMITTED                 | Non-reserved                                 |
| UNDER                       | N/A                                          |
| UNENCRYPTED                 | Non-reserved                                 |
| UNION                       | Reserved                                     |
| UNIQUE                      | Reserved                                     |
| UNKNOWN                     | Non-reserved                                 |
| UNLIMITED                   | Non-reserved                                 |
| UNLISTEN                    | Non-reserved                                 |
| UNLOCK                      | Non-reserved                                 |
| UNLOGGED                    | Non-reserved                                 |
| UNNAMED                     | N/A                                          |
| UNNEST                      | N/A                                          |
| UNTIL                       | Non-reserved                                 |
| UNUSABLE                    | Non-reserved                                 |
| UPDATE                      | Non-reserved                                 |
| UPPER                       | N/A                                          |
| USAGE                       | N/A                                          |
| USER                        | Reserved                                     |
| USER_DEFINED_TYPE_CATALOG   | N/A                                          |
| USER_DEFINED_TYPE_NAME      | N/A                                          |
| USER_DEFINED_TYPE_SCHEMA    | N/A                                          |
| USING                       | Reserved                                     |
| VACUUM                      | Non-reserved                                 |
| VALID                       | Non-reserved                                 |
| VALIDATE                    | Non-reserved                                 |
| VALIDATION                  | Non-reserved                                 |
| VALIDATOR                   | Non-reserved                                 |
| VALUE                       | Non-reserved                                 |
| VALUES                      | Non-reserved (excluding functions and types) |
| VARCHAR                     | Non-reserved (excluding functions and types) |
| VARCHAR2                    | Non-reserved (excluding functions and types) |
| VARIABLE                    | N/A                                          |
| VARIADIC                    | Reserved                                     |
| VARYING                     | Non-reserved                                 |
| VCGROUP                     | Non-reserved                                 |
| VERBOSE                     | Reserved (functions and types allowed)       |
| VERSION                     | Non-reserved                                 |
| VERIFY                      | Reserved                                     |
| VIEW                        | Non-reserved                                 |
| VOLATILE                    | Non-reserved                                 |
| WHEN                        | Reserved                                     |
| WHENEVER                    | N/A                                          |
| WHERE                       | Reserved                                     |
| WHITESPACE                  | Non-reserved                                 |
| WINDOW                      | Reserved                                     |
| WITH                        | Reserved                                     |
| WITHIN                      | Non-reserved                                 |
| WITHOUT                     | Non-reserved                                 |
| WORK                        | Non-reserved                                 |
| WORKLOAD                    | Non-reserved                                 |
| WRAPPER                     | Non-reserved                                 |
| WRITE                       | Non-reserved                                 |
| XML                         | Non-reserved                                 |
| XMLATTRIBUTES               | Non-reserved (excluding functions and types) |
| XMLCONCAT                   | Non-reserved (excluding functions and types) |
| XMLELEMENT                  | Non-reserved (excluding functions and types) |
| XMLEXISTS                   | Non-reserved (excluding functions and types) |
| XMLFOREST                   | Non-reserved (excluding functions and types) |
| XMLPARSE                    | Non-reserved (excluding functions and types) |
| XMLPI                       | Non-reserved (excluding functions and types) |
| XMLROOT                     | Non-reserved (excluding functions and types) |
| XMLSERIALIZE                | Non-reserved (excluding functions and types) |
| YEAR                        | Non-reserved                                 |
| YES                         | Non-reserved                                 |
| ZONE                        | Non-reserved                                 |