---
title: Snowball Dictionary
summary: Snowball Dictionary
author: Zhang Cuiping
date: 2021-05-17
---

# Snowball Dictionary

A **Snowball** dictionary is based on a project by Martin Porter and is used for stem analysis, providing stemming algorithms for many languages. MogDB provides predefined **Snowball** dictionaries of many languages. You can query the [PG_TS_DICT](../../../../reference-guide/system-catalogs-and-system-views/system-catalogs/PG_TS_DICT.md) system catalog to view the predefined **Snowball** dictionaries and supported stemming algorithms.

A **Snowball** dictionary recognizes everything, no matter whether it is able to simplify the word. Therefore, it should be placed at the end of the dictionary list. It is useless to place it before any other dictionary because a token will never pass it through to the next dictionary.

For details about the syntax of **Snowball** dictionaries, see [CREATE TEXT SEARCH DICTIONARY](../../../../reference-guide/sql-syntax/CREATE-TEXT-SEARCH-DICTIONARY.md).
