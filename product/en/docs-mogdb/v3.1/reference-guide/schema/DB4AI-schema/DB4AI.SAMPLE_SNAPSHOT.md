---
title: DB4AI.SAMPLE_SNAPSHOT
summary: DB4AI.SAMPLE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.SAMPLE_SNAPSHOT

**SAMPLE_SNAPSHOT** samples basic data to generate snapshots for the DB4AI feature. You can invoke **SAMPLE SNAPSHOT** to implement this function.

**Table 1** DB4AI.SAMPLE_SNAPSHOT input parameters and return values

| Parameter         | Type                    | Description                                                |
| :---------------- | :---------------------- | :--------------------------------------------------------- |
| i_schema          | IN NAME                 | Name of the schema storing snapshots                       |
| i_parent          | IN NAME                 | Parent snapshot name                                       |
| i_sample_infixes  | IN NAME[]               | Sample snapshot name infix                                 |
| i_sample_ratios   | IN NUMBER[]             | Size of each sample, which is used as the sample set ratio |
| i_stratify        | IN NAME[]               | Layering strategy                                          |
| i_sample_comments | IN TEXT[]               | Sample snapshot description                                |
| res               | OUT db4ai.snapshot_name | Result                                                     |
