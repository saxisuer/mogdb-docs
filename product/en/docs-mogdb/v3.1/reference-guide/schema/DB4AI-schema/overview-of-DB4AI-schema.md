---
title: Overview of DB4AI Schema
summary: Overview of DB4AI Schema
author: Guo Huan
date: 2021-11-15
---

# Overview of DB4AI Schema

The DB4AI schema is used to store and manage dataset versions in the AI feature. The schema holds a snapshot of the original view of the data tables, a change record for each data version, and management information for the version snapshot. This schema is intended for common users. In this schema, users can query the snapshot version information created by DB4AI.SNAPSHOT.
