---
title: Schemas
summary: Schemas
author: zhang cuiping
date: 2022-10-24
---

# Schemas

The following table describes the schemas of MogDB.

**Table 1** Schemas supported by MogDB

| Schema         | Description                                                  |
| :------------- | :----------------------------------------------------------- |
| blockchain     | Stores the user history table that is automatically created when a tamper-proof table is created in the ledger database. |
| cstore         | Stores auxiliary tables related to column-store tables, such as CUDesc and Delta tables. |
| db4ai          | Manages data of different versions in AI training.           |
| dbe_perf       | Diagnoses performance issues and is also the data source of WDR snapshots. After a database is installed, only the initial user and monitoring administrator have permission to view views and functions in this schema by default. |
| dbe_pldebugger | Debugs PL/pgSQL functions and stored procedures.             |
| snapshot       | Manages data related to WDR snapshots. By default, the initial user or monitoring administrator can access the data. |
| sqladvisor     | Is used for distribution column recommendation and is unavailable in MogDB . |
| pg_catalog     | Maintains system catalog information, including system catalogs and all built-in data types, functions, and operators. |
| pg_toast       | Stores large objects (for internal use).                     |
| public         | Public schema. By default, created tables (and other objects) are automatically put into this schema. |
| pkg_service    | Manages information about the package service.               |