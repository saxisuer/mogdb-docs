---
title: DBE_PLDEBUGGER.print_var
summary: DBE_PLDEBUGGER.print_var
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.print_var

During debugging on the debug end, **print_var** is invoked to print the name and value of the specified variable in the current stored procedure. The input parameter **frameno** of this function indicates the stack layer to be traversed. This function can be invoked without **frameno**. By default, the top-layer stack variable is queried.

**Table 1** print_var input parameters and return values

| Name         | Type                  | Description                                                  |
| :----------- | :-------------------- | :----------------------------------------------------------- |
| var_name     | IN text               | Variable                                                     |
| frameno      | IN integer (optional) | Specified stack layer. The default value is the top layer.   |
| varname      | OUT text              | Variable name                                                |
| vartype      | OUT text              | Variable type                                                |
| value        | OUT text              | Variable value                                               |
| package_name | OUT text              | Package name corresponding to the variable. This parameter is reserved and is left empty currently. |
| isconst      | OUT boolean           | Whether it is a constant                                     |
