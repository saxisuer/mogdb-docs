---
title: DBE_PLDEBUGGER.set_var
summary: DBE_PLDEBUGGER.set_var
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.set_var

Changes the variable on the top-layer stack in the specified debugging stored procedure to the value of the input parameter. If a stored procedure contains variables with the same name, set_var supports only the setting of the first variable value.

**Table 1** set_var input parameters and return values

| Name     | Type        | Description                 |
| :------- | :---------- | :-------------------------- |
| var_name | IN text     | Variable name               |
| value    | IN text     | New value                   |
| result   | OUT boolean | Result (success or failure) |
