---
title: DBE_PLDEBUGGER.turn_off
summary: DBE_PLDEBUGGER.turn_off
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.turn_off

This function is used to remove the debugging flag added by **turn_on**. The return value indicates success or failure. You can run the **DBE_PLDEBUGGER.local_debug_server_info** command to query the OID of the stored procedure that has been turned on.

The function prototype is as follows:

```sql
DBE_PLDEBUGGER.turn_off(Oid)
RETURN boolean;
```

**Table 1** turn_off input parameters and return values

| Name     | Type        | Description                    |
| :------- | :---------- | :----------------------------- |
| func_oid | IN oid      | Function OID                   |
| turn_off | OUT boolean | Whether turn-off is successful |
