---
title: DBE_PLDEBUGGER.finish
summary: DBE_PLDEBUGGER.finish
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.finish

Executes the current SQL statement in the stored procedure until the next breakpoint is triggered or the next line of the upper-layer stack is executed.

**Table 1** finish input parameters and return values

| Name     | Type        | Description                                                  |
| :------- | :---------- | :----------------------------------------------------------- |
| funcoid  | OUT oid     | Function ID                                                  |
| funcname | OUT text    | Function name                                                |
| lineno   | OUT integer | Number of the next line in the current debugging process     |
| query    | OUT text    | Source code of the next line of the function that is being debugged |
