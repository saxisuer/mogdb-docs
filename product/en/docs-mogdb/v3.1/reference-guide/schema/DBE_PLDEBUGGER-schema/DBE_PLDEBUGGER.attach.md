---
title: DBE_PLDEBUGGER.attach
summary: DBE_PLDEBUGGER.attach
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.attach

When the server executes a stored procedure, the server hangs the execution before the first statement and waits for attaching with the debug end. The debug end invokes the attach function and transfers node name and port number to attach with the specified stored procedure.

If an error is reported during debugging, the attach operation automatically becomes invalid. If the debug end is attached to another stored procedure during debugging, the debugging of the attached stored procedure becomes invalid.

**Table 1** attach input parameters and return values

| Name     | Type        | Description                                                  |
| :------- | :---------- | :----------------------------------------------------------- |
| nodename | IN text     | Node name                                                    |
| port     | IN integer  | Number of the connected port                                 |
| funcoid  | OUT oid     | Function ID                                                  |
| funcname | OUT text    | Function name                                                |
| lineno   | OUT integer | Number of the next line in the current debugging process     |
| query    | OUT text    | Source code of the next line of the function that is being debugged |
