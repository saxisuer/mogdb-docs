---
title: DBE_PLDEBUGGER.disable_breakpoint
summary: DBE_PLDEBUGGER.disable_breakpoint
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEBUGGER.disable_breakpoint

During debugging on the debug end, call disable_breakpoint to disable breakpoints.

**Table 1** disable_breakpoint input parameters and return values

| Name         | Type       | Description                          |
| :----------- | :--------- | :----------------------------------- |
| breakpointno | IN integer | Breakpoint number                    |
| result       | OUT bool   | Whether this operation is successful |