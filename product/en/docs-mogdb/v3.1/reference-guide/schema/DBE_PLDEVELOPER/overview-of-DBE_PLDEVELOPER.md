---
title: DBE_PLDEVELOPER
summary: DBE_PLDEVELOPER
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER

The system table in DBE_PLDEVELOPER records information required for compiling PLPGSQL packages, functions, and stored procedures.
