---
title: DBE_PLDEVELOPER.gs_source
summary: DBE_PLDEVELOPER.gs_source
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER.gs_source

Records PLPGSQL object (stored procedure, function, package, and package body) compilation information. For details, see the following column description.

When the **plsql_show_all_error** parameter is enabled, information about PL/PGSQL object compilation success or failure is recorded in this table. When the **plsql_show_all_error** parameter is disabled, only information about correct compilation is inserted into this table.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **Caution:**
>
> - The gs_source table records only user-defined original object statements. Even if a user uses ALTER to change the created schema or name, the information in the gs_source table does not change. If the user changes the schema or name of an object, the deleted object still exists in the gs_source table.
>
> - The owner in the gs_source table is the user who creates the table, not the user specified when the user creates the stored procedure or package.
>
> - By default, row-level security is not configured for the gs_source table in the database. If you want to use the database isolation feature, run the following statement to add row-level security:
>
>   ```sql
>   ALTER TABLE dbe_pldeveloper.gs_source ENABLE ROW LEVEL SECURITY; CREATE ROW LEVEL SECURITY POLICY all_data_rls ON dbe_pldeveloper.gs_source USING(owner = (select oid from pg_roles where rolname=current_user));
>   ```

**Table 1** DBE_PLDEVELOPER.gs_source columns

| **Name** | **Type** | **Description**                                              |
| -------- | -------- | ------------------------------------------------------------ |
| id       | oid      | Object ID.                                                   |
| owner    | bigint   | ID of the user who creates the object.                       |
| nspid    | oid      | Schema ID of an object.                                      |
| name     | name     | Object name.                                                 |
| type     | text     | Object type (**procedure**/**function**/**package**/**package body**). |
| status   | boolean  | Determines whether the creation is successful.               |
| src      | text     | Original statement for creating an object.                   |