---
title: _PG_USER_MAPPINGS
summary: _PG_USER_MAPPINGS
author: Guo Huan
date: 2021-06-23
---

# _PG_USER_MAPPINGS

**_PG_USER_MAPPINGS** stores mappings from local users to remote users. Only the sysadmin user has the permission to view this view.

**Table 1** _PG_USER_MAPPINGS columns

| Name                     | Type                              | Description                                                  |
| ------------------------ | --------------------------------- | ------------------------------------------------------------ |
| oid                      | oid                               | OID of the mapping from the local user to a remote user      |
| umoptions                | text[]                            | User mapping specific options, expressed in a string in the format of *keyword***=***value* |
| umuser                   | oid                               | OID of the local user being mapped (**0** if the user mapping is public) |
| authorization_identifier | information_schema.sql_identifier | Role of the local user                                       |
| foreign_server_catalog   | information_schema.sql_identifier | Name of the database where the foreign server is defined in  |
| foreign_server_name      | information_schema.sql_identifier | Name of the foreign server                                   |
| srvowner                 | information_schema.sql_identifier | Owner of the foreign server                                  |
