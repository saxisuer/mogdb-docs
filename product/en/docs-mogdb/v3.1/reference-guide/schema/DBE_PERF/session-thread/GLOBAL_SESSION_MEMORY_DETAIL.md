---
title: GLOBAL_SESSION_MEMORY_DETAIL
summary: GLOBAL_SESSION_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_SESSION_MEMORY_DETAIL

**GLOBAL_SESSION_MEMORY_DETAIL** collects statistics about thread memory usage on each node by MemoryContext node.

**Table 1** GLOBAL_SESSION_MEMORY_DETAIL columns

| **Name**    | **Type** | **Description**                         |
| :---------- | :------- | :-------------------------------------- |
| node_name   | name     | Node name                               |
| sessid      | text     | Thread start time and ID                |
| sesstype    | text     | Thread name                             |
| contextname | text     | Name of the memory context              |
| level       | smallint | Level of memory context importance      |
| parent      | text     | Name of the parent memory context       |
| totalsize   | bigint   | Size of the applied memory (unit: byte) |
| freesize    | bigint   | Size of the idle memory (unit: byte)    |
| usedsize    | bigint   | Size of the used memory (unit: byte)    |
