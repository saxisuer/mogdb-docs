---
title: GLOBAL_SESSION_STAT
summary: GLOBAL_SESSION_STAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_SESSION_STAT

**GLOBAL_SESSION_STAT** collects statistics about session status on each node based on session threads or the **AutoVacuum** thread.

**Table 1** GLOBAL_SESSION_STAT columns

| **Name**  | **Type** | **Description**                 |
| :-------- | :------- | :------------------------------ |
| node_name | name     | Node name                       |
| sessid    | text     | Thread start time and ID        |
| statid    | integer  | Statistics ID                   |
| statname  | text     | Name of the statistics session  |
| statunit  | text     | Unit of the statistics session  |
| value     | bigint   | Value of the statistics session |
