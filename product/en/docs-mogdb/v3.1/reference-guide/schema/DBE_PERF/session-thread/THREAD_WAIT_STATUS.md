---
title: THREAD_WAIT_STATUS
summary: THREAD_WAIT_STATUS
author: Guo Huan
date: 2021-04-19
---

# THREAD_WAIT_STATUS

**THREAD_WAIT_STATUS** allows you to test the block waiting status of the backend thread and auxiliary thread in the current instance. For details about events, see Table 2, Table 3, Table 4, and Table 5 in [PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md).

**Table 1** THREAD_WAIT_STATUS columns

| **Name**    | **Type** | **Description**                                              |
| :---------- | :------- | :----------------------------------------------------------- |
| node_name   | text     | Database process name                                        |
| db_name     | text     | Database name                                                |
| thread_name | text     | Thread name                                                  |
| query_id    | bigint   | Query ID. The value of this column is the same as that of **debug_query_id**. |
| tid         | bigint   | Thread ID of the current thread                              |
| sessionid   | bigint   | Session ID                                                   |
| lwtid       | integer  | Lightweight thread ID of the current thread                  |
| psessionid  | bigint   | Parent thread of the streaming thread                        |
| tlevel      | integer  | Level of the streaming thread                                |
| smpid       | integer  | Concurrent thread ID                                         |
| wait_status | text     | Waiting status of the current thread. For details about the waiting status, see Table 2 in [PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md). |
| wait_event  | text     | If **wait\_status** is **acquire lock**, **acquire lwlock**, or **wait io**, this column describes the lock, lightweight lock, and I/O information, respectively. If **wait\_status** is not any of the three values, this column is empty. |
