---
title: GLOBAL_THREAD_WAIT_STATUS
summary: GLOBAL_THREAD_WAIT_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_THREAD_WAIT_STATUS

**GLOBAL_THREAD_WAIT_STATUS** allows you to test the block waiting status of backend threads and auxiliary threads on all nodes. For details about events, see Table 2, Table 3, Table 4, and  Table 5 in [PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md).

In **GLOBAL_THREAD_WAIT_STATUS**, you can see all the call hierarchy relationships between threads of the SQL statements on all nodes in MogDB, and the block waiting status for each thread. With this view, you can easily locate the causes of process hang and similar issues.

The definitions of **GLOBAL_THREAD_WAIT_STATUS** and **THREAD_WAIT_STATUS** are the same, because the **GLOBAL_THREAD_WAIT_STATUS** view is essentially the query summary of the **THREAD_WAIT_STATUS** view on each node in MogDB.

**Table 1** GLOBAL_THREAD_WAIT_STATUS columns

| **Name**    | **Type** | **Description**                                              |
| :---------- | :------- | :----------------------------------------------------------- |
| node_name   | text     | Node name                                                    |
| db_name     | text     | Database name                                                |
| thread_name | text     | Thread name                                                  |
| query_id    | bigint   | Query ID. The value of this column is the same as that of **debug_query_id**. |
| tid         | bigint   | Thread ID of the current thread                              |
| sessionid   | bigint   | Session ID                                                   |
| lwtid       | integer  | Lightweight thread ID of the current thread                  |
| psessionid  | bigint   | Parent thread of the streaming thread                        |
| tlevel      | integer  | Level of the streaming thread                                |
| smpid       | integer  | Concurrent thread ID                                         |
| wait_status | text     | Waiting status of the current thread. For details about the waiting status, see Table 2 in [PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md). |
| wait_event  | text     | If **wait\_status** is **acquire lock**, **acquire lwlock**, or **wait io**, this column describes the lock, lightweight lock, and I/O information, respectively. If **wait\_status** is not any of the three values, this column is empty. |
