---
title: LOCAL_THREADPOOL_STATUS
summary: LOCAL_THREADPOOL_STATUS
author: Guo Huan
date: 2021-04-19
---

# LOCAL_THREADPOOL_STATUS

**LOCAL_THREADPOOL_STATUS** displays the status of worker threads and sessions in a thread pool. This view is valid only when **enable_thread_pool** is set to **on**.

**Table 1** LOCAL_THREADPOOL_STATUS columns<a id="LOCAL_THREADPOOL_STATUS"> </a>

| Name            | Type    | Description                                                  |
| :-------------- | :------ | :----------------------------------------------------------- |
| node_name       | text    | Database process name                                        |
| group_id        | integer | ID of the thread pool group                                  |
| bind_numa_id    | integer | NUMA ID to which the thread pool group is bound              |
| bind_cpu_number | integer | Information about the CPU to which the thread pool group is bound. If no CPUs are bound, the value is **NULL**. |
| listener        | integer | Number of listener threads in the thread pool group          |
| worker_info     | text    | Information about threads in the thread pool, including:<br/>- **default**: Number of initial threads in the thread pool group<br/>- **new**: Number of new threads in the thread pool group<br/>- **expect**: Expected number of threads in the thread pool group<br/>- **actual**: Actual number of threads in the thread pool group<br/>- **idle**: Number of idle threads in the thread pool group<br/>- **pending**: Number of pending threads in the thread pool group |
| session_info    | text    | Information about sessions in the thread pool, including:<br/>- **total**: Total number of sessions in the thread pool group<br/>- **waiting**: Number of sessions pending scheduling in the thread pool group<br/>- **running**: Number of running sessions in the thread pool group<br/>- **idle**: Number of idle sessions in the thread pool group |
| stream_info     | text    | Information about threads in the stream pool, including:<br />- total: Total number of threads in the stream pool group<br />- running: Number of threads being executed in the stream pool group<br />- idle: Number of idle threads in the stream pool group |
