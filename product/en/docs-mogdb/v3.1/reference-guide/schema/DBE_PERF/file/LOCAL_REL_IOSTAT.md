---
title: LOCAL_REL_IOSTAT
summary: LOCAL_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# LOCAL_REL_IOSTAT

**LOCAL_REL_IOSTAT** displays the accumulated I/O status of all data files on the current node.

**Table 1** LOCAL_REL_IOSTAT columns

| Name      | Type   | Description                                          |
| :-------- | :----- | :--------------------------------------------------- |
| phyrds    | bigint | Number of times of reading physical files            |
| phywrts   | bigint | Number of times of writing into physical files       |
| phyblkrd  | bigint | Number of times of reading physical file blocks      |
| phyblkwrt | bigint | Number of times of writing into physical file blocks |
