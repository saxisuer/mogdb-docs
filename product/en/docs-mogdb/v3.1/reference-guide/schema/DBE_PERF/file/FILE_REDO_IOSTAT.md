---
title: FILE_REDO_IOSTAT
summary: FILE_REDO_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# FILE_REDO_IOSTAT

**FILE_REDO_IOSTAT** records statistics about redo logs (WALs) on the current node.

**Table 1** FILE_REDO_IOSTAT columns

| Name      | Type   | Description                                                  |
| :-------- | :----- | :----------------------------------------------------------- |
| phywrts   | bigint | Number of times writing into the WAL buffer                  |
| phyblkwrt | bigint | Number of blocks written into the WAL buffer                 |
| writetim  | bigint | Duration of writing into XLOG files (unit: μs)               |
| avgiotim  | bigint | Average duration of writing into XLOG files (unit: μs). **avgiotim** = **writetim**/**phywrts** |
| lstiotim  | bigint | Duration of the last writing into XLOG files (unit: μs)      |
| miniotim  | bigint | Minimum duration of writing into XLOG files (unit: μs)       |
| maxiowtm  | bigint | Maximum duration of writing into XLOG files (unit: μs)       |
