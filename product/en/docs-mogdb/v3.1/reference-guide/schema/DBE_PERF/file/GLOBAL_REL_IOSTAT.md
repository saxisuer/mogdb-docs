---
title: GLOBAL_REL_IOSTAT
summary: GLOBAL_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REL_IOSTAT

**GLOBAL_REL_IOSTAT** is the aggregate statistics of MogDB on the cumulative value of the data file IO status, reflecting the IO performance of the data file access.

**Table 1** GLOBAL_REL_IOSTAT columns

| **Name**  | **Type** | **Description**                                      |
| :-------- | :------- | :--------------------------------------------------- |
| node_name | name     | Node name                                            |
| phyrds    | bigint   | Number of times of reading physical files            |
| phywrts   | bigint   | Number of times of writing into physical files       |
| phyblkrd  | bigint   | Number of times of reading physical file blocks      |
| phyblkwrt | bigint   | Number of times of writing into physical file blocks |
