---
title: OPERATOR_HISTORY
summary: OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# OPERATOR_HISTORY

**OPERATOR_HISTORY** displays records of operators in jobs that have been executed by the current user on the current primary database node. Columns in this view are the same as those in Table 1 of [GS_WLM_OPERATOR_INFO](../../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_OPERATOR_INFO.md).