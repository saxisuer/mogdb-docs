---
title: Table 1
summary: Table 1
author: Guo Huan
date: 2021-04-19
---

**Table 1** Waiting status list<a id="Table1"> </a>

| Value                                   | Description                                                  |
| :-------------------------------------- | :----------------------------------------------------------- |
| none                                    | Waiting for no event                                         |
| acquire lock                            | Waiting for locking until the locking succeeds or times out  |
| acquire lwlock                          | Waiting for a lightweight lock                               |
| wait io                                 | Waiting for I/O completion                                   |
| wait cmd                                | Waiting for reading network communication packets to complete |
| wait pooler get conn                    | Waiting for the pooler to obtain the connection              |
| wait pooler abort conn                  | Waiting for the pooler to terminate the connection           |
| wait pooler clean conn                  | Waiting for the pooler to clear connections                  |
| pooler create conn: [nodename], total N | Waiting for the pooler to set up a connection. The connection is being established with the node specified by **nodename**, and there are *N* connections waiting to be set up. |
| get conn                                | Obtaining the connection to other nodes                      |
| set cmd: [nodename]                     | Waiting for running the **SET**, **RESET**, **TRANSACTION BLOCK LEVEL PARA SET**, or **SESSION LEVEL PARA SET** statement on the connection. The statement is being executed on the node specified by **nodename**. |
| cancel query                            | Canceling the SQL statement that is being executed through the connection |
| stop query                              | Stopping the query that is being executed through the connection |
| wait node: nodename, total N, [phase]   | Waiting for receiving data from a connected node. The thread is waiting for data from the plevel thread of the node specified by **nodename**. The data of *N* connections is waiting to be returned. If *phase* is included, the possible phases are as follows:<br/>- **begin**: The transaction is being started.<br/>- **commit**: The transaction is being committed.<br/>- **rollback**: The transaction is being rolled back. |
| wait transaction sync: xid              | Waiting for synchronizing the transaction specified by **xid** |
| wait wal sync                           | Waiting for the completion of WAL of synchronization from the specified LSN to the standby instance |
| wait data sync                          | Waiting for the completion of data page synchronization to the standby instance |
| wait data sync queue                    | Waiting for putting the data pages that are in the row-store or the CU in the column-store into the synchronization queue |
| flush data: nodename, [phase]           | Waiting for sending data to the plevel thread of the node specified by **nodename**. If **phase** is included, the possible phase is **wait quota**, indicating that the current communication flow is waiting for the quota value. |
| stream get conn: [nodename], total N    | Waiting for connecting to the consumer object of the node specified by **nodename** when the stream flow is initialized. There are *N* consumers waiting to be connected. |
| wait producer ready: nodename, total N  | Waiting for each producer to be ready when the stream flow is initialized. The thread is waiting for the procedure of the plevel thread on the **nodename** node to be ready. There are **N** producers waiting to be ready. |
| synchronize quit                        | Waiting for the threads in the stream thread group to quit when the stream plan ends |
| wait stream nodegroup destroy           | Waiting for destroying the stream node group when the stream plan ends |
| wait active statement                   | Waiting for job execution under resource and load control    |
| analyze: [relname], [phase]             | The thread is doing **ANALYZE** to the **relname** table. If **phase** is included, the possible phase is **autovacuum**, indicating that the database automatically enables the AutoVacuum thread to execute **ANALYZE**. |
| vacuum: [relname], [phase]              | The thread is doing **VACUUM** to the **relname** table. If **phase** is included, the possible phase is **autovacuum**, indicating that the database automatically enables the AutoVacuum thread to execute **VACUUM**. |
| vacuum full: [relname]                  | The thread is doing **VACUUM FULL** to the **relname** table. |
| create index                            | An index is being created.                                   |
| HashJoin - [ build hash \| write file ] | The **HashJoin** operator is being executed. In this phase, you need to pay attention to the execution time-consuming.<br/>- **build hash**: The **HashJoin** operator is creating a hash table.<br/>- **write file**: The **HashJoin** operator is writing data to disks. |
| HashAgg - [ build hash \| write file ]  | The **HashAgg** operator is being executed. In this phase, you need to pay attention to the execution time-consuming.<br/>- **build hash**: The **HashAgg** operator is creating a hash table.<br/>- **write file**: The **HashAgg** operator is writing data to disks. |
| HashSetop - [build hash \| write file ] | The **HashSetop** operator is being executed. In this phase, you need to pay attention to the execution time-consuming.<br/>- **build hash**: The **HashSetop** operator is creating a hash table.<br/>- **write file**: The **HashSetop** operator is writing data to disks. |
| Sort \| Sort - write file               | The **Sort** operator is being executed. **write file** indicates that the **Sort** operator is writing data to disks. |
| Material \| Material - write file       | The **Material** operator is being executed. **write file** indicates that the **Material** operator is writing data to disks. |
| NestLoop                                | The **NestLoop** operator is being executed.                 |
| wait memory                             | Waiting for obtaining the memory                             |
| wait sync consumer next step            | Waiting for the consumer to execute the stream operator      |
| wait sync producer next step            | Waiting for the producer to execute the stream operator      |
