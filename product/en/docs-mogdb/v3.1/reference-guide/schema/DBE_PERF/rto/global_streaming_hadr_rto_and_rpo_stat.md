---
title: global_streaming_hadr_rto_and_rpo_stat
summary: global_streaming_hadr_rto_and_rpo_stat
author: zhang cuiping
date: 2022-10-24
---

# global_streaming_hadr_rto_and_rpo_stat

**global_streaming_hadr_rto_and_rpo_stat** displays the log flow control information about the primary and standby database instances for streaming DR. This view can be used only by primary database node of the primary database instance. Statistics cannot be obtained from the standby database node or standby database instance.

**Table 1** global_streaming_hadr_rto_and_rpo_stat parameters

| Parameter               | Type | Description                                                  |
| :---------------------- | :--- | :----------------------------------------------------------- |
| hadr_sender_node_name   | text | Node name, including the primary database instance and the first standby node of the standby database instance. |
| hadr_receiver_node_name | text | Name of the first standby node of the standby database instance. |
| current_rto             | int  | Flow control information, that is, log RTO time of the current primary and standby database instances (unit: second). |
| target_rto              | int  | Flow control information, that is, the RTO time between the target primary and standby database instances (unit: second). |
| current_rpo             | int  | Flow control information, that is, log RPO time of the current primary and standby database instances (unit: second). |
| target_rpo              | int  | Flow control information, that is, the RPO time between the target primary and standby database instances (unit: second). |
| rto_sleep_time          | int  | RTO flow control information, that is, the expected sleep time (unit: μs) required by walsender on the host to reach the specified RTO. |
| rpo_sleep_time          | int  | RPO flow control information, that is, the expected sleep time (unit: μs) required by xlogInsert on the host to reach the specified RPO. |