---
title: global_rto_status
summary: global_rto_status
author: Guo Huan
date: 2021-11-15
---

# global_rto_status

Displays log flow control information about the primary and standby nodes (except the current node and standby DNs).

Return type: record

**Table 1** remote_rto_status parameters

| Parameter | Type | Description                                                  |
| :-------- | :--- | :----------------------------------------------------------- |
| node_name | text | Node name (including the primary and standby nodes)          |
| rto_info  | text | Flow control information, including the current log flow control time (unit: second) of the standby node, the expected flow control time (unit: second) specified by the GUC parameter, and the primary node sleep time (unit: μs) required to reach the expectation |
