---
title: WAIT_EVENTS
summary: WAIT_EVENTS
author: Guo Huan
date: 2021-04-19
---

# WAIT_EVENTS

**WAIT_EVENTS** displays statistics about wait events on the current node. For details about events, see Table 2, Table 3, and Table 4 in [PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md). For details about the extent to which each transaction lock affects the services, see [LOCK](../../../../reference-guide/sql-syntax/LOCK.md).

**Table 1** WAIT_EVENTS columns

| **Name**        | **Type**                 | **Description**                      |
| :-------------- | :----------------------- | :----------------------------------- |
| nodename        | text                     | Database process name                |
| type            | text                     | Event type                           |
| event           | text                     | Event name                           |
| wait            | bigint                   | Number of waiting times              |
| failed_wait     | bigint                   | Number of waiting failures           |
| total_wait_time | bigint                   | Total waiting time (unit: μs)        |
| avg_wait_time   | bigint                   | Average waiting time (unit: μs)      |
| max_wait_time   | bigint                   | Maximum waiting time (unit: μs)      |
| min_wait_time   | bigint                   | Minimum waiting time (unit: μs)      |
| last_updated    | timestamp with time zone | Last time when the event was updated |
