---
title: GLOBAL_LOCKS
summary: GLOBAL_LOCKS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_LOCKS

**GLOBAL_LOCKS** displays information about locks held by open transactions on each node.

**Table 1** GLOBAL_LOCKS columns

| **Name**           | **Type** | **Description**                                              |
| :----------------- | :------- | :----------------------------------------------------------- |
| node_name          | name     | Node name                                                    |
| locktype           | text     | Type of the locked object:**relation**, **extend**, **page**, **tuple**, **transactionid**, **virtualxid**, **object**, **userlock**, or **advisory** |
| database           | oid      | OID of the database in which the locked object exists.<br/>- The OID is **0** if the object is a shared object.<br/>- The OID is **NULL** if the object is a transaction ID. |
| relation           | oid      | OID of the relationship targeted by the lock. The value is **NULL** if the object is not a relationship or part of a relationship. |
| page               | integer  | Page number targeted by the lock within the relationship. The value is **NULL** if the object is not a relationship page or row page. |
| tuple              | smallint | Row number targeted by the lock within the page. The value is **NULL** if the object is not a row. |
| virtualxid         | text     | Virtual ID of the transaction targeted by the lock. The value is **NULL** if the object is not a virtual transaction ID. |
| transactionid      | xid      | ID of the transaction targeted by the lock. The value is **NULL** if the object is not a transaction ID. |
| classid            | oid      | OID of the system catalog that contains the object. The value is **NULL** if the object is not a general database object. |
| objid              | oid      | OID of the locked object within its system catalog. The value is **NULL** if the object is not a general database object. |
| objsubid           | smallint | Column number for a column in the table. The value is **0** if the object is some other object type. The value is **NULL** if the object is not a general database object. |
| virtualtransaction | text     | Virtual ID of the transaction holding or awaiting this lock  |
| pid                | bigint   | Logical ID of the server thread holding or awaiting this lock The value is **NULL** if the lock is held by a prepared transaction. |
| mode               | text     | Lock mode held or desired by this thread                     |
| granted            | boolean  | - The value is **TRUE** if the lock is a held lock.<br/>- The value is **FALSE** if the lock is an awaited lock. |
| fastpath           | boolean  | The value is **TRUE** if the lock is obtained through **fast-path**, and is **FALSE** if the lock is obtained through the main lock table. |
