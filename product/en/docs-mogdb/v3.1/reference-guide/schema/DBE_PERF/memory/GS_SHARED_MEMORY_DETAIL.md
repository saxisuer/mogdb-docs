---
title: GS_SHARED_MEMORY_DETAIL
summary: GS_SHARED_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GS_SHARED_MEMORY_DETAIL

**GS_SHARED_MEMORY_DETAIL** queries the usage information about shared memory contexts on the current node.

**Table 1** GS_SHARED_MEMORY_DETAIL columns

| Name        | Type     | Description                                      |
| :---------- | :------- | :----------------------------------------------- |
| contextname | text     | Name of the memory context                       |
| level       | smallint | Level of the memory context                      |
| parent      | text     | Name of the parent memory context                |
| totalsize   | bigint   | Total size of the shared memory (unit: byte)     |
| freesize    | bigint   | Remaining size of the shared memory (unit: byte) |
| usedsize    | bigint   | Used size of the shared memory (unit: byte)      |
