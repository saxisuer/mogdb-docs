---
title: STATIO_SYS_SEQUENCES
summary: STATIO_SYS_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_SYS_SEQUENCES

**STATIO_SYS_SEQUENCES** shows the I/O status information about all the system sequences in the current namespace.

**Table 1** STATIO_SYS_SEQUENCES columns

| Name       | Type   | Description                                  |
| :--------- | :----- | :------------------------------------------- |
| relid      | oid    | OID of the sequence                          |
| schemaname | name   | Name of the schema that the sequence is in   |
| relname    | name   | Sequence name                                |
| blks_read  | bigint | Number of disk blocks read from the sequence |
| blks_hit   | bigint | Number of cache hits in the sequence         |
