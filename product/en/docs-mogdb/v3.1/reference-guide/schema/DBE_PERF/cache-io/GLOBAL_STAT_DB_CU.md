---
title: GLOBAL_STAT_DB_CU
summary: GLOBAL_STAT_DB_CU
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_DB_CU

**GLOBAL_STAT_DB_CU** is used to query CU hits in each database in MogDB. You can clear it using **pg_stat_reset()**.

**Table 1** GLOBAL_STAT_DB_CU columns

| **Name**      | **Type** | **Description**                        |
| :------------ | :------- | :------------------------------------- |
| node_name1    | text     | Node name                              |
| db_name       | text     | Database name                          |
| mem_hit       | bigint   | Number of memory hits                  |
| hdd_sync_read | bigint   | Number of synchronous hard disk reads  |
| hdd_asyn_read | bigint   | Number of asynchronous hard disk reads |
