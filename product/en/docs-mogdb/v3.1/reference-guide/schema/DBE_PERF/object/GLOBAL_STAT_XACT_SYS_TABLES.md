---
title: GLOBAL_STAT_XACT_SYS_TABLES
summary: GLOBAL_STAT_XACT_SYS_TABLES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_XACT_SYS_TABLES

**GLOBAL_STAT_XACT_SYS_TABLES** displays transaction status information about the system catalogs in namespaces on each node.

**Table 1** GLOBAL_STAT_XACT_SYS_TABLES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| node_name     | name     | Node name                                                    |
| relid         | oid      | OID of the table                                             |
| schemaname    | name     | Name of the schema that contains the table                   |
| relname       | name     | Table name                                                   |
| seq_scan      | bigint   | Number of sequential scans initiated on this table           |
| seq_tup_read  | bigint   | Number of live rows fetched by sequential scans              |
| idx_scan      | bigint   | Number of index scans initiated on the table                 |
| idx_tup_fetch | bigint   | Number of live rows fetched by index scans                   |
| n_tup_ins     | bigint   | Number of rows inserted                                      |
| n_tup_upd     | bigint   | Number of rows updated                                       |
| n_tup_del     | bigint   | Number of rows deleted                                       |
| n_tup_hot_upd | bigint   | Number of rows HOT updated (with no separate index update required) |
