---
title: SUMMARY_STAT_XACT_SYS_TABLES
summary: SUMMARY_STAT_XACT_SYS_TABLES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_XACT_SYS_TABLES

**SUMMARY_STAT_XACT_SYS_TABLES** displays transaction status information about the system catalogs in namespaces in MogDB.

**Table 1** SUMMARY_STAT_XACT_SYS_TABLES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| schemaname    | name     | Name of the schema that contains the table                   |
| relname       | name     | Table name                                                   |
| seq_scan      | numeric  | Number of sequential scans initiated on this table           |
| seq_tup_read  | numeric  | Number of live rows fetched by sequential scans              |
| idx_scan      | numeric  | Number of index scans initiated on the table                 |
| idx_tup_fetch | numeric  | Number of live rows fetched by index scans                   |
| n_tup_ins     | numeric  | Number of rows inserted                                      |
| n_tup_upd     | numeric  | Number of rows updated                                       |
| n_tup_del     | numeric  | Number of rows deleted                                       |
| n_tup_hot_upd | numeric  | Number of rows HOT updated (with no separate index update required) |
