---
title: USER_TRANSACTION
summary: USER_TRANSACTION
author: Guo Huan
date: 2021-11-15
---

# USER_TRANSACTION

**USER_TRANSACTION** collects statistics about transactions executed by users. Common users can view only transactions executed by themselves, whereas user **monadmin** can view transactions executed by all users.

**Table 1** USER_TRANSACTION columns

| **Name**            | **Type** | **Description**                                             |
| :------------------ | :------- | :---------------------------------------------------------- |
| username            | name     | Username                                                    |
| commit_counter      | bigint   | Number of user transactions committed                       |
| rollback_counter    | bigint   | Number of user transactions rolled back                     |
| resp_min            | bigint   | Minimum response time of user transactions (unit: μs)       |
| resp_max            | bigint   | Maximum response time of user transactions (unit: μs)       |
| resp_avg            | bigint   | Average response time of user transactions (unit: μs)       |
| resp_total          | bigint   | Total response time of user transactions (unit: μs)         |
| bg_commit_counter   | bigint   | Number of background transactions committed                 |
| bg_rollback_counter | bigint   | Number of background transactions rolled back               |
| bg_resp_min         | bigint   | Minimum response time of background transactions (unit: μs) |
| bg_resp_max         | bigint   | Maximum response time of background transactions (unit: μs) |
| bg_resp_avg         | bigint   | Average response time of background transactions (unit: μs) |
| bg_resp_total       | bigint   | Total response time of background transactions (unit: μs)   |
