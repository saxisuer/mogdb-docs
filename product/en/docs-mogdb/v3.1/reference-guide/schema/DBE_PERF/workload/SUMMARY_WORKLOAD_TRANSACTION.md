---
title: SUMMARY_WORKLOAD_TRANSACTION
summary: SUMMARY_WORKLOAD_TRANSACTION
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_WORKLOAD_TRANSACTION

**SUMMARY_WORKLOAD_TRANSACTION** displays information about transactions loaded in MogDB.

**Table 1** SUMMARY_WORKLOAD_TRANSACTION columns

| **Name**            | **Type** | **Description**                                             |
| :------------------ | :------- | :---------------------------------------------------------- |
| workload            | name     | Workload name                                               |
| commit_counter      | numeric  | Number of user transactions committed                       |
| rollback_counter    | numeric  | Number of user transactions rolled back                     |
| resp_min            | bigint   | Minimum response time of user transactions (unit: μs)       |
| resp_max            | bigint   | Maximum response time of user transactions (unit: μs)       |
| resp_avg            | bigint   | Average response time of user transactions (unit: μs)       |
| resp_total          | numeric  | Total response time of user transactions (unit: μs)         |
| bg_commit_counter   | numeric  | Number of background transactions committed                 |
| bg_rollback_counter | numeric  | Number of background transactions rolled back               |
| bg_resp_min         | bigint   | Minimum response time of background transactions (unit: μs) |
| bg_resp_max         | bigint   | Maximum response time of background transactions (unit: μs) |
| bg_resp_avg         | bigint   | Average response time of background transactions (unit: μs) |
| bg_resp_total       | numeric  | Total response time of background transactions (unit: μs)   |
