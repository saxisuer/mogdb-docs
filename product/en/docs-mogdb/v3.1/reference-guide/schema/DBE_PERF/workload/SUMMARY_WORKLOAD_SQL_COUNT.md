---
title: SUMMARY_WORKLOAD_SQL_COUNT
summary: SUMMARY_WORKLOAD_SQL_COUNT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_WORKLOAD_SQL_COUNT

**SUMMARY_WORKLOAD_SQL_COUNT** displays the distribution of SQL statements in workloads on the primary database node in MogDB.

**Table 1** SUMMARY_WORKLOAD_SQL_COUNT columns

| **Name**     | **Type** | **Description**                 |
| :----------- | :------- | :------------------------------ |
| node_name    | name     | Database process name           |
| workload     | name     | Workload name                   |
| select_count | bigint   | Number of **SELECT** statements |
| update_count | bigint   | Number of **UPDATE** statements |
| insert_count | bigint   | Number of **INSERT** statements |
| delete_count | bigint   | Number of **DELETE** statements |
| ddl_count    | bigint   | Number of **DDL** statements    |
| dml_count    | bigint   | Number of **DML** statements    |
| dcl_count    | bigint   | Number of **DCL** statements    |
