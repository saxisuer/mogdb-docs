---
title: WORKLOAD_TRANSACTION
summary: WORKLOAD_TRANSACTION
author: Guo Huan
date: 2021-04-19
---

# WORKLOAD_TRANSACTION

**WORKLOAD_TRANSACTION** displays information about transactions loaded on the current node.

**Table 1** WORKLOAD_TRANSACTION columns

| Name                | Type   | Description                                                 |
| :------------------ | :----- | :---------------------------------------------------------- |
| workload            | name   | Workload name                                               |
| commit_counter      | bigint | Number of user transactions committed                       |
| rollback_counter    | bigint | Number of user transactions rolled back                     |
| resp_min            | bigint | Minimum response time of user transactions (unit: μs)       |
| resp_max            | bigint | Maximum response time of user transactions (unit: μs)       |
| resp_avg            | bigint | Average response time of user transactions (unit: μs)       |
| resp_total          | bigint | Total response time of user transactions (unit: μs)         |
| bg_commit_counter   | bigint | Number of background transactions committed                 |
| bg_rollback_counter | bigint | Number of background transactions rolled back               |
| bg_resp_min         | bigint | Minimum response time of background transactions (unit: μs) |
| bg_resp_max         | bigint | Maximum response time of background transactions (unit: μs) |
| bg_resp_avg         | bigint | Average response time of background transactions (unit: μs) |
| bg_resp_total       | bigint | Total response time of background transactions (unit: μs)   |
