---
title: STATEMENT_USER_COMPLEX_HISTORY
summary: STATEMENT_USER_COMPLEX_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_USER_COMPLEX_HISTORY

**STATEMENT_USER_COMPLEX_HISTORY** displays load management information about completed jobs executed on the current primary database node. Data is dumped from the kernel to this system catalog. Columns in this view are the same as those in Table 1 of [GS_SESSION_MEMORY_DETAIL](../../../../reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_DETAIL.md).
