---
title: STATEMENT_COUNT
summary: STATEMENT_COUNT
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COUNT

**STATEMENT_COUNT** displays statistics about five types of running statements (**SELECT**, **INSERT**, **UPDATE**, **DELETE**, and **MERGE INTO**) as well as DDL, DML, and DCL statements on the current node of the database.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> By querying the **STATEMENT_COUNT** view, a common user can view statistics only about this user on the current node, whereas an administrator can view statistics about all users on the current node. When MogDB or the node is restarted, the statistics are cleared and the counting restarts. The system counts when a node receives a query, including a query inside MogDB. For example, when the primary database node receives a query and distributes multiple queries to other database nodes, the queries are counted accordingly on the database nodes.

**Table 1** STATEMENT_COUNT columns

| **Name**            | **Type** | **Description**                                           |
| :------------------ | :------- | :-------------------------------------------------------- |
| node_name           | text     | Database process name                                     |
| user_name           | text     | Username                                                  |
| select_count        | bigint   | Statistical result of the **SELECT** statement            |
| update_count        | bigint   | Statistical result of the **UPDATE** statement            |
| insert_count        | bigint   | Statistical result of the **INSERT** statement            |
| delete_count        | bigint   | Statistical result of the **DELETE** statement            |
| mergeinto_count     | bigint   | Statistical result of the **MERGE INTO** statement        |
| ddl_count           | bigint   | Number of DDL statements                                  |
| dml_count           | bigint   | Number of DML statements                                  |
| dcl_count           | bigint   | Number of DCL statements                                  |
| total_select_elapse | bigint   | Total response time of **SELECT** statements (unit: μs)   |
| avg_select_elapse   | bigint   | Average response time of **SELECT** statements (unit: μs) |
| max_select_elapse   | bigint   | Maximum response time of **SELECT** statements (unit: μs) |
| min_select_elapse   | bigint   | Minimum response time of **SELECT** statements (unit: μs) |
| total_update_elapse | bigint   | Total response time of **UPDATE** statements (unit: μs)   |
| avg_update_elapse   | bigint   | Average response time of **UPDATE** statements (unit: μs) |
| max_update_elapse   | bigint   | Maximum response time of **UPDATE** statements (unit: μs) |
| min_update_elapse   | bigint   | Minimum response time of **UPDATE** statements (unit: μs) |
| total_insert_elapse | bigint   | Total response time of **INSERT** statements (unit: μs)   |
| avg_insert_elapse   | bigint   | Average response time of **INSERT** statements (unit: μs) |
| max_insert_elapse   | bigint   | Maximum response time of **INSERT** statements (unit: μs) |
| min_insert_elapse   | bigint   | Minimum response time of **INSERT** statements (unit: μs) |
| total_delete_elapse | bigint   | Total response time of **DELETE** statements (unit: μs)   |
| avg_delete_elapse   | bigint   | Average response time of **DELETE** statements (unit: μs) |
| max_delete_elapse   | bigint   | Maximum response time of **DELETE** statements (unit: μs) |
| min_delete_elapse   | bigint   | Minimum response time of **DELETE** statements (unit: μs) |
