---
title: GLOBAL_CKPT_STATUS
summary: GLOBAL_CKPT_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_CKPT_STATUS

**GLOBAL_CKPT_STATUS** displays the checkpoint information and log flushing information about all instances in MogDB.

**Table 1** GLOBAL_CKPT_STATUS columns

| Name                     | Type   | Description                                                  |
| :----------------------- | :----- | :----------------------------------------------------------- |
| node_name                | text   | Node name                                                    |
| ckpt_redo_point          | test   | Checkpoint of the current instance                           |
| ckpt_clog_flush_num      | bigint | Number of Clog flushing pages from the startup time to the current time |
| ckpt_csnlog_flush_num    | bigint | Number of CSN log flushing pages from the startup time to the current time |
| ckpt_multixact_flush_num | bigint | Number of MultiXact flushing pages from the startup time to the current time |
| ckpt_predicate_flush_num | bigint | Number of predicate flushing pages from the startup time to the current time |
| ckpt_twophase_flush_num  | bigint | Number of two-phase flushing pages from the startup time to the current time |
