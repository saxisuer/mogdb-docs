---
title: GLOBAL_REDO_STATUS
summary: GLOBAL_REDO_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REDO_STATUS

**GLOBAL_REDO_STATUS** displays the log replay status of all instances in MogDB.

**Table 1** GLOBAL_REDO_STATUS columns

| Name                      | Type   | Description                                                  |
| :------------------------ | :----- | :----------------------------------------------------------- |
| node_name                 | text   | Node name                                                    |
| redo_start_ptr            | bigint | Start point for replaying the instance logs                  |
| redo_start_time           | bigint | Start time (UTC) when the instance logs are replayed         |
| redo_done_time            | bigint | End time (UTC) when the instance logs are replayed           |
| curr_time                 | bigint | Current time (UTC) of the instance                           |
| min_recovery_point        | bigint | Position of the minimum consistency point for the instance logs |
| read_ptr                  | bigint | Position for reading the instance logs                       |
| last_replayed_read_ptr    | bigint | Position for replaying the instance logs                     |
| recovery_done_ptr         | bigint | Replay position after the instance is started                |
| read_xlog_io_counter      | bigint | Number of I/Os when the instance reads and replays logs      |
| read_xlog_io_total_dur    | bigint | Total I/O latency when the instance reads and replays logs   |
| read_data_io_counter      | bigint | Number of data page I/O reads during replay in the instance  |
| read_data_io_total_dur    | bigint | Total I/O latency of data page reads during replay in the instance |
| write_data_io_counter     | bigint | Number of data page I/O writes during replay in the instance |
| write_data_io_total_dur   | bigint | Total I/O latency of data page writes during replay in the instance |
| process_pending_counter   | bigint | Number of synchronization times of log distribution threads during replay in the instance |
| process_pending_total_dur | bigint | Total synchronization latency of log distribution threads during replay in the instance |
| apply_counter             | bigint | Number of synchronization times of replay threads during replay in the instance |
| apply_total_dur           | bigint | Total synchronization latency of replay threads during replay in the instance |
| speed                     | bigint | Log replay rate of the current instance                      |
| local_max_ptr             | bigint | Maximum number of replay logs received by the local host after the instance is started |
| primary_flush_ptr         | bigint | Log point where the host flushes logs to a disk              |
| worker_info               | text   | Replay thread information of the instance. If concurrent replay is not enabled, the value is **NULL**. |
