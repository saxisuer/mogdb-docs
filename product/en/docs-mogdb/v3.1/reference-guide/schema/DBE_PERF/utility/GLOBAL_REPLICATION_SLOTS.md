---
title: GLOBAL_REPLICATION_SLOTS
summary: GLOBAL_REPLICATION_SLOTS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REPLICATION_SLOTS

**GLOBAL_REPLICATION_SLOTS** displays replication node information of each node in MogDB.

**Table 1** GLOBAL_REPLICATION_SLOTS columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| node_name     | name     | Node name                                                    |
| slot_name     | text     | Replication node name                                        |
| plugin        | text     | Plug-in name                                                 |
| slot_type     | text     | Type of the replication node                                 |
| datoid        | oid      | OID of the database on the replication node                  |
| database      | name     | Name of the database on the replication node                 |
| active        | boolean  | Whether the replication node is active                       |
| x_min         | xid      | Transaction ID of the replication node                       |
| catalog_xmin  | xid      | ID of the earliest decoded transaction corresponding to the logical replication slot |
| restart_lsn   | text     | XLOG file information on the replication node                |
| dummy_standby | boolean  | Whether the replication node is a dummy standby node         |
