---
title: GLOBAL_SINGLE_FLUSH_DW_STATUS
summary: GLOBAL_SINGLE_FLUSH_DW_STATUS
author: Guo Huan
date: 2021-11-15
---

# GLOBAL_SINGLE_FLUSH_DW_STATUS

**GLOBAL_SINGLE_FLUSH_DW_STATUS** displays information about doublewrite files eliminated on a single page of all instances in the database. In the displayed information, the information before the slash (/) indicates the page flushing status of the first version, and the information after the slash (/) indicates the page flushing status of the second version.

**Table 1** GLOBAL_SINGLE_FLUSH_DW_STATUS columns

| Name            | Type | Description                                                  |
| :-------------- | :--- | :----------------------------------------------------------- |
| node_name       | text | Instance name                                                |
| curr_dwn        | text | Sequence number of the doublewrite file                      |
| curr_start_page | text | Start position of the current doublewrite file               |
| total_writes    | text | Total number of data write pages in the current doublewrite file |
| file_trunc_num  | text | Number of times that the doublewrite file is reused          |
| file_reset_num  | text | Number of reset times after the doublewrite file is full     |
