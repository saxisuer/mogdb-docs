---
title: GLOBAL_RECORD_RESET_TIME
summary: GLOBAL_RECORD_RESET_TIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_RECORD_RESET_TIME

**GLOBAL_RECORD_RESET_TIME** displays statistics about reset time in MogDB. Restart, primary/standby switchover, and database deletion will cause the time to be reset.

**Table 1** GLOBAL_RECORD_RESET_TIME columns

| Name       | Type                     | Description      |
| :--------- | :----------------------- | :--------------- |
| node_name  | text                     | node name        |
| reset_time | timestamp with time zone | Time to be reset |
