---
title: REPLICATION_SLOTS
summary: REPLICATION_SLOTS
author: Guo Huan
date: 2021-04-19
---

# REPLICATION_SLOTS

**REPLICATION_SLOTS** displays replication node information.

**Table 1** REPLICATION_SLOTS columns

| Name          | Type    | Description                                                  |
| :------------ | :------ | :----------------------------------------------------------- |
| slot_name     | text    | Replication node name                                        |
| plugin        | text    | Plug-in name                                                 |
| slot_type     | text    | Replication node type                                        |
| datoid        | oid     | OID of the database on the replication node                  |
| database      | name    | Name of the database on the replication node                 |
| active        | boolean | Whether the replication node is active                       |
| xmin          | xid     | Transaction ID of the replication node                       |
| catalog_xmin  | xid     | ID of the earliest decoded transaction corresponding to the logical replication slot |
| restart_lsn   | text    | XLOG file information on the replication node                |
| dummy_standby | boolean | Whether the replication node is a dummy standby node         |
