---
title: Configuration Settings Functions
summary: Configuration Settings Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Configuration Settings Functions

Configuration setting functions are used for querying and modifying configuration parameters during running.

- current_setting(setting_name)

  Description: Specifies the current setting.

  Return type: text

  Note:**current_setting** obtains the current setting of **setting_name** by query. It is equivalent to the **SHOW** statement.

  Example:

  ```sql
  MogDB=# SELECT current_setting('datestyle');

   current_setting
  -----------------
   ISO, MDY
  (1 row)
  ```

- set_working_grand_version_num_manually(tmp_version)

    Description: Upgrades new features of the database by switching the authorization version.

    Return type: void

- shell_in(type)

    Description: Inputs a route for the shell type that has not yet been filled.

    Return type: void

- shell_out(type)

    Description: Outputs a route for the shell type that has not yet been filled.

    Return type: void

- set_config(setting_name, new_value, is_local)

  Description: Sets the parameter and returns a new value.

  Return type: text

  Note: **set_config**  sets  **setting_name**  to  **new_value**. If  **is_local**  is set to  **true**,  **new_value**  applies only to the current transaction. If you want  **new_value**  to apply for the current session, set the value to  **false**  instead. The function corresponds to the  **SET**  statement.

  Example:

  ```sql
  MogDB=# SELECT set_config('log_statement_stats', 'off', false);

   set_config
  ------------
   off
  (1 row)
  ```
