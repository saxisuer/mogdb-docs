---
title: Obsolete Functions
summary: Obsolete Functions
author: Zhang Cuiping
date: 2021-06-07
---

# Obsolete Functions

The following functions in MogDB have been discarded in the latest version:

| gs_wlm_get_session_info     | gs_wlm_get_user_session_info       | pgxc_get_csn                          | pgxc_get_stat_dirty_tables                | pgxc_get_thread_wait_status              | pgxc_gtm_snapshot_status               | pgxc_is_committed                  |
| --------------------------- | ---------------------------------- | ------------------------------------- | ----------------------------------------- | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| pgxc_lock_for_backup        | pgxc_lock_for_sp_database          | pgxc_lock_for_transfer                | pgxc_log_comm_status                      | pgxc_max_datanode_size                   | pgxc_node_str                          | pgxc_pool_check                    |
| pgxc_pool_connection_status | pgxc_pool_reload                   | pgxc_prepared_xact                    | pgxc_snapshot_status                      | pgxc_stat_dirty_tables                   | pgxc_unlock_for_sp_database            | pgxc_unlock_for_transfer           |
| pgxc_version                | array_extend                       | prepare_statement_status              | remote_rto_stat                           | dbe_perf.global_slow_query_info          | dbe_perf.global_slow_query_info_bytime | dbe_perf.global_slow_query_history |
| pg_stat_get_pooler_status   | pg_stat_get_wlm_node_resource_info | pg_stat_get_wlm_session_info_internal | DBE_PERF.get_wlm_controlgroup_ng_config() | DBE_PERF.get_wlm_user_resource_runtime() | global_space_shrink                    | pg_pool_validate                   |
| gs_stat_ustore              | table_skewness(text)               | table_skewness(text, text, text)      | -                                         | -                                        | -                                      | -                                  |
