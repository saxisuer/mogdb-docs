---
title: Numeric Data Types
summary: Numeric Data Types
author: Guo Huan
date: 2021-04-06
---

# Numeric Data Types

Table 1 lists all available types. For digit operators and related built-in functions, see [Mathematical Functions and Operators](../../reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md).

**Table 1** Integer types

| Name                                   | Description                                            | Storage Space | Range                                                        |
| :------------------------------------- | :----------------------------------------------------- | :------------ | :----------------------------------------------------------- |
| TINYINT        | Tiny integer, also called INT1                         | 1 byte        | 0-255                                                        |
| SMALLINT       | Small integer, also called INT2                        | 2 bytes       | -32,768 to +32,767                                           |
| INTEGER        | Typical choice for integers, also called INT4          | 4 bytes       | -2,147,483,648 to +2,147,483,647                             |
| BINARY_INTEGER | Alias of INTEGER.                                      | 4 bytes       | -2,147,483,648 to +2,147,483,647                             |
| BIGINT         | Big integer, also called INT8                          | 8 bytes       | -9,223,372,036,854,775,808 to +9,223,372,036,854,775,807     |
| int16          | A large range of 16-bytes integer, cannot be used to create tables. | 16 bytes      | -170,141,183,460,469,231,731,687,303,715,884,105,728 to +170,141,183,460,469,231,731,687,303,715,884,105,727 |

Example:

```sql
-- Create a table containing TINYINT data.
MogDB=# CREATE TABLE int_type_t1
           (
            IT_COL1 TINYINT
           );

-- Insert data to the created table.
MogDB=# INSERT INTO int_type_t1 VALUES(10);

-- View data.
MogDB=# SELECT * FROM int_type_t1;
 it_col1
---------
 10
(1 row)

-- Delete the table.
MogDB=# DROP TABLE int_type_t1;

```

```sql
-- Create a table containing TINYINT, INTEGER, and BIGINT data.
MogDB=# CREATE TABLE int_type_t2
(
    a TINYINT,
    b TINYINT,
    c INTEGER,
    d BIGINT
);

-- Insert data.
MogDB=# INSERT INTO int_type_t2 VALUES(100, 10, 1000, 10000);

-- View data.
MogDB=# SELECT * FROM int_type_t2;
  a  | b  |  c   |   d
-----+----+------+-------
 100 | 10 | 1000 | 10000
(1 row)

-- Delete the table.
MogDB=# DROP TABLE int_type_t2;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Only numbers of the TINYINT, SMALLINT, INTEGER, BIGINT, or INT16 type, that is, integers can be stored. Saving a number with a decimal in any of the data types will result in errors.
> - The INTEGER type is the common choice, as it offers the best balance between range, storage size, and performance. Generally, use the SMALLINT type only if you are sure that the value range is within the SMALLINT value range. The storage speed of INTEGER is much faster. BIGINT is used only when the range of INTEGER is not large enough.

**Table 2** Any-precision types

| Name                                     | Description                         | Storage Space                               | Range                                      |
| :--------------------------------------- | :---------------------------------- | :---------------------------------- | :---------------------------------- |
| NUMERIC[(p[,s])],DECIMAL[(p[,s])] | The value range of **p** (precision) is [1,1000], and the value range of **s** (scale) is [0,*p*].<br />NOTE:<br />**p** indicates the total digits, and **s** indicates the decimal digit. | The precision is specified by users. Every four decimal digits occupy two bytes, and an extra eight-byte overhead is added to the entire data. | Up to 131,072 digits before the decimal point; and up to 16,383 digits after the decimal point when no precision is specified |
| NUMBER[(p[,s])]                   | Alias of the NUMERIC type.                                   | The precision is specified by users. Every four decimal digits occupy two bytes, and an extra eight-byte overhead is added to the entire data. | Up to 131,072 digits before the decimal point; and up to 16,383 digits after the decimal point when no precision is specified |

Example:

```sql
-- Create a table.
MogDB=# CREATE TABLE decimal_type_t1
(
    DT_COL1 DECIMAL(10,4)
);

-- Insert data.
MogDB=# INSERT INTO decimal_type_t1 VALUES(123456.122331);

-- Query data in the table.
MogDB=# SELECT * FROM decimal_type_t1;
   dt_col1
-------------
 123456.1223
(1 row)

-- Delete the table.
MogDB=# DROP TABLE decimal_type_t1;

```

```sql
-- Create a table.
MogDB=# CREATE TABLE numeric_type_t1
(
    NT_COL1 NUMERIC(10,4)
);

-- Insert data.
MogDB=# INSERT INTO numeric_type_t1 VALUES(123456.12354);

-- Query data in the table.
MogDB=# SELECT * FROM numeric_type_t1;
   nt_col1
-------------
 123456.1235
(1 row)

-- Delete the table.
MogDB=# DROP TABLE numeric_type_t1;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Compared to the integer types, the arbitrary precision numbers require larger storage space and have lower storage efficiency, operation efficiency, and poorer compression ratio results. The INTEGER type is the common choice when number types are defined. Arbitrary precision numbers are used only when numbers exceed the maximum range indicated by the integers.
> - When NUMERIC/DECIMAL is used for defining a column, you are advised to specify the precision (p) and scale (s) for the column.

**Table 3** Sequence integer

| Name                 | Description     | Storage Space | Range                |
| :------------------- | :------------------- | :------------ | :----------------------------------|
| SMALLSERIAL | Two-byte auto-incrementing integer   | 2 bytes       | -32,768 to +32,767                                           |
| SERIAL      | Four-byte auto-incrementing integer  | 4 bytes       | -2,147,483,648 to +2,147,483,647                             |
| BIGSERIAL   | Eight-byte auto-incrementing integer | 8 bytes       | -9,223,372,036,854,775,808 to +9,223,372,036,854,775,807     |
| LARGESERIAL | 16-byte serial integer               | 16 bytes      | -170,141,183,460,469,231,731,687,303,715,884,105,728 to +170,141,183,460,469,231,731,687,303,715,884,105,727 |

Example:

```sql
-- Create a table.
MogDB=# CREATE TABLE smallserial_type_tab(a SMALLSERIAL);

-- Insert data.
MogDB=# INSERT INTO smallserial_type_tab VALUES(default);

-- Insert data again.
MogDB=# INSERT INTO smallserial_type_tab VALUES(default);

-- View data.
MogDB=# SELECT * FROM smallserial_type_tab;
 a
---
 1
 2
(2 rows)

-- Create a table.
MogDB=# CREATE TABLE serial_type_tab(b SERIAL);

-- Insert data.
MogDB=# INSERT INTO serial_type_tab VALUES(default);

-- Insert data again.
MogDB=# INSERT INTO serial_type_tab VALUES(default);

-- View data.
MogDB=# SELECT * FROM serial_type_tab;
 b
---
 1
 2
(2 rows)

-- Create a table.
MogDB=# CREATE TABLE bigserial_type_tab(c BIGSERIAL);

-- Insert data.
MogDB=# INSERT INTO bigserial_type_tab VALUES(default);

-- Insert data.
MogDB=# INSERT INTO bigserial_type_tab VALUES(default);

-- View data.
MogDB=# SELECT * FROM bigserial_type_tab;
 c
---
 1
 2
(2 rows)

-- Delete the table.
MogDB=# DROP TABLE smallserial_type_tab;

MogDB=# DROP TABLE serial_type_tab;

MogDB=# DROP TABLE bigserial_type_tab;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> SMALLSERIAL, SERIAL and BIGSERIAL are not real types. They are concepts used for setting a unique identifier for a table. Therefore, an integer column is created and its default value plans to be read from a sequencer. A NOT NULL constraint is used to ensure NULL is not inserted. In most cases you would also want to attach a **UNIQUE** or **PRIMARY KEY** constraint to prevent duplicate values from being inserted unexpectedly, but this is not automatic. The sequencer is set so that it belongs to the column. In this case, when the column or the table is deleted, the sequencer is also deleted. Currently, the **SERIAL** column can be specified only when you create a table. You cannot add the **SERIAL** column in an existing table. In addition, **SERIAL** columns cannot be created in temporary tables. Because SERIAL is not a data type, columns cannot be converted to this type.

**Table 4** Floating point types

| Name                    | Description         | Storage Space       | Range               |
| :---------------------- | :------------------ | :------------------ | :------------------ |
| REAL,FLOAT4             | Single precision floating points, inexact                    | 4 bytes                                                      | -3.402E+38 to 3.402E+38, 6-bit decimal digits                |
| DOUBLE PRECISION,FLOAT8 | Double precision floating points, inexact                    | 8 bytes                                                      | -1.79E+308 to 1.79E+308, 15-bit decimal digits               |
| FLOAT[(p)]              | Floating points, inexact. The value range of **p** (precision) is [1,53].<br />NOTE:<br />**p** is the precision, indicating the total decimal digits. | 4 or 8 bytes                                                 | **REAL** or **DOUBLE PRECISION** is selected as an internal identifier based on precision (p). If no precision is specified, DOUBLE PRECISION is used as the internal identifier. |
| BINARY_DOUBLE           | Alias of DOUBLE PRECISION.                                   | 8 bytes                                                      | -1.79E+308 to 1.79E+308, 15-bit decimal digits               |
| DEC[(p[,s])]            | The value range of **p** (precision) is [1,1000], and the value range of **s** (scale) is [0,*p*].<br />NOTE:<br />**p** indicates the total digits, and **s** indicates the decimal digit. | The precision is specified by users. Every four decimal digits occupy two bytes, and an extra eight-byte overhead is added to the entire data. | Up to 131,072 digits before the decimal point; and up to 16,383 digits after the decimal point when no precision is specified |
| INTEGER[(p[,s])]        | The value range of **p** (precision) is [1,1000], and the value range of **s** (scale) is [0,*p*]. | The precision is specified by users. Every four decimal digits occupy two bytes, and an extra eight-byte overhead is added to the entire data. | -                                                            |

Example:

```sql
-- Create a table.
MogDB=# CREATE TABLE float_type_t2
(
    FT_COL1 INTEGER,
    FT_COL2 FLOAT4,
    FT_COL3 FLOAT8,
    FT_COL4 FLOAT(3),
    FT_COL5 BINARY_DOUBLE,
    FT_COL6 DECIMAL(10,4),
    FT_COL7 INTEGER(6,3)
);

-- Insert data.
MogDB=# INSERT INTO float_type_t2 VALUES(10,10.365456,123456.1234,10.3214, 321.321, 123.123654, 123.123654);

-- View data.
MogDB=# SELECT * FROM float_type_t2 ;
 ft_col1 | ft_col2 |   ft_col3   | ft_col4 | ft_col5 | ft_col6  | ft_col7
---------+---------+-------------+---------+---------+----------+---------
      10 | 10.3655 | 123456.1234 | 10.3214 | 321.321 | 123.1237 | 123.124
(1 row)

-- Delete the table.
MogDB=# DROP TABLE float_type_t2;
```
