---
title: Pseudo-Types
summary: Pseudo-Types
author: Guo Huan
date: 2021-04-06
---

# Pseudo-Types

MogDB type system contains a number of special-purpose entries that are collectively called pseudo-types. A pseudo-type cannot be used as a column data type, but it can be used to declare a function's argument or result type.

Each of the available pseudo-types is useful in situations where a function's behavior does not correspond to simply taking or returning a value of a specific SQL data type. Table 1 lists all pseudo-types.

**Table 1** Pseudo-types

| Name             | Description                                                  |
| :--------------- | :----------------------------------------------------------- |
| any              | Indicates that a function accepts any input data type.       |
| anyelement       | Indicates that a function accepts any data type.             |
| anyarray         | Indicates that a function accepts any array data type.       |
| anynonarray      | Indicates that a function accepts any non-array data type.   |
| anyenum          | Indicates that a function accepts any enum data type.        |
| anyrange         | Indicates that a function accepts any range data type.       |
| cstring          | Indicates that a function accepts or returns a null-terminated C string. |
| internal         | Indicates that a function accepts or returns a server-internal data type. |
| language_handler | Indicates that a procedural language call handler is declared to return **language_handler**. |
| fdw_handler      | Indicates that a foreign-data wrapper handler is declared to return **fdw_handler**. |
| record           | Identifies a function returning an unspecified row type.     |
| trigger          | Indicates that a trigger function is declared to return **trigger**. |
| void             | Indicates that a function returns no value.                  |
| opaque           | Indicates an obsolete type name that formerly served all the above purposes. |

Functions coded in C (whether built in or dynamically loaded) can be declared to accept or return any of these pseudo data types. It is up to the function author to ensure that the function will behave safely when a pseudo-type is used as an argument type.

Functions coded in procedural languages can use pseudo-types only as allowed by their implementation languages. At present the procedural languages all forbid use of a pseudo-type as argument type, and allow only **void** and **record** as a result type. Some also support polymorphic functions using the **anyelement**, **anyarray**, **anynonarray**, **anyenum**, and **anyrange** types.

The **internal** pseudo-type is used to declare functions that are meant only to be called internally by the database system, and not by direct invocation in an SQL query. If a function has at least one **internal**-type argument then it cannot be called from SQL. You are advised not to create any function that is declared to return internal unless it has at least one **internal** argument.

Example:

```sql
-- Create a table.
MogDB=# create table t1 (a int);

-- Insert two data records.
MogDB=# insert into t1 values(1),(2);

-- Create the showall() function:
MogDB=# CREATE OR REPLACE FUNCTION showall() RETURNS SETOF record
AS $$ SELECT count(*) from t1; $$
LANGUAGE SQL;

-- Invoke the showall() function:
MogDB=# SELECT showall();
 showall
---------
 (2)
(1 row)

-- Delete the function:
MogDB=# DROP FUNCTION showall();

-- Delete the table.
MogDB=# drop table t1;
```
