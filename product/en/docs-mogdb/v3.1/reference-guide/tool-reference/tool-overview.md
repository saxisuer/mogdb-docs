---
title: Tool Overview
summary: Tool Overview
author: Zhang Cuiping
date: 2021-06-07
---

# Tool Overview

MogDB provides client and server applications (tools) to help users better maintain MogDB. [Table 1](#tooloverview1) and [Table 2](#tooloverview2) list all the tools provided by MogDB. The tools are stored in **$GPHOME/script** and **$GAUSSHOME/bin** where database servers are installed.

**Table 1** Client tool overview <a id="tooloverview1"> </a>

| Name                            | Description                                                  |
| :------------------------------ | :----------------------------------------------------------- |
| [gsql](./client-tool/1-gsql.md) | **gsql** is a database connection tool provided by MogDB and runs in the command-line interface (CLI). Users can use **gsql** to connect to a server and perform operations and maintenance on the server. In addition to basic functions for performing operations on a database, **gsql** provides several advanced features for users. |

**Table 2** Server tool overview <a id="tooloverview2"> </a>

| Name                                                     | Description                                                  |
| :------------------------------------------------------- | :----------------------------------------------------------- |
| [gs_cgroup](./server-tools/0-gs_cgroup.md)               | **gs_cgroup** is a load management tool provided by MogDB. It can create default Cgroups and user-defined Cgroups, delete default and user-defined Cgroups, update resource quotas and limits, display the configuration files of Cgroups and the Cgroup tree, and delete all Cgroups. |
| [gs_check](./server-tools/1-gs_check.md)                 | **gs_check** has been enhanced to incorporate various check tools, such as **gs_check** and **gs_checkos**. It helps users fully check MogDB runtime, OS, network, and database running environments, as well as perform comprehensive checks on various environments before major operations in MogDB, ensuring smooth operations. |
| [gs_checkos](./server-tools/2-gs_checkos.md)             | **gs_checkos** checks OS information, control parameters, and disk configurations, and configures control parameters, I/O parameters, network parameters, and THP services. |
| [gs_checkperf](./server-tools/3-gs_checkperf.md)         | **gs_checkperf** routinely checks MogDB-level performance (host CPU, Gauss CPU, and I/O usage), node-level performance (CPU, memory, and I/O usage), session-/process-level performance (CPU, memory, and I/O usage), and SSD performance (read and write performance). It provides MogDB load information for users so that they can fine-tune database performance accordingly. |
| [gs_collector](./server-tools/4-gs_collector.md)         | When MogDB is faulty, **gs_collector** collects OS information, log information, and configuration file information to locate the fault. |
| [gs_dump](./server-tools/5-gs_dump.md)                   | **gs_dump** exports database information. It can export complete, consistent data of database objects (such as databases, schemas, tables, and views) without affecting the normal access of users to the databases. |
| [gs_dumpall](./server-tools/6-gs_dumpall.md)             | **gs_dumpall** exports database information. It can export complete, consistent data of MogDB without affecting the normal access of users to the database. |
| [gs_guc](./server-tools/7-gs_guc.md)                     | **gs_guc** is used to set parameters in MogDB configuration files (**postgresql.conf** or **pg_hba.conf**). The default parameter values in a configuration file are consistent with those for a standalone database. |
| [gs_encrypt](./server-tools/7.1-gs_encrypt.md)           | **gs_encrypt** is used to encrypt entered plaintext strings. |
| [gs_om](./server-tools/8-gs_om.md)                       | **gs_om**, provided by MogDB, helps you maintain MogDB, including starting and stopping MogDB, querying the MogDB status, generating static configuration files, updating dynamic configuration files, replacing SSL certificates, starting and stopping Kerberos authentication, and displaying help information and version numbers. |
| [gs_plan_simulator](./server-tools/gs_plan_simulator.md) | **gs_plan_simulator** collects data related to execution plans and reproduces execution plans in other environments to locate execution plan problems. |
| [gs_restore](./server-tools/9-gs_restore.md)             | **gs_restore**, provided by MogDB, is used to import data that was exported using **gs_dump**. It can also be used to import files exported by **gs_dump**. |
| [gs_ssh](./server-tools/10-gs_ssh.md)                    | **gs_ssh**, provided by MogDB, helps users run the same command on multiple nodes in MogDB. |
| [gs_sdr](./server-tools/gs_sdr.md)                       | **gs\_sdr**, provided by MogDB, implements cross-region remote DR without using additional storage media. The tool provides functions such as streaming DR establishment, DR switchover, planned primary/standby switchover, DR removal, DR status monitoring, and displaying the help information and version number. |