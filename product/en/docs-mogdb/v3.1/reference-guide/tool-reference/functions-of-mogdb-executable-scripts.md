---
title: Functions of MogDB Executable Scripts
summary: Functions of MogDB Executable Scripts
author: Guo Huan
date: 2022-06-08
---

# Functions of MogDB Executable Scripts

The following lists common executable scripts of MogDB, which are used only by MogDB APIs.

**Table 1** Script functions

| Script                   | Function                                                     |
| :----------------------- | :----------------------------------------------------------- |
| UpgradeUtility.py        | Upgrade tool.                                                |
| KerberosUtility.py       | Installs or uninstalls the Kerberos authentication mode.     |
| Uninstall.py             | Uninstalls a database instance.                              |
| LocalCheck.py            | Checks the local node.                                       |
| Install.py               | Installs a database node instance.                           |
| CheckUninstall.py        | Checks whether a node has been uninstalled.                  |
| PreInstallUtility.py     | Pre-installation tool.                                       |
| CleanInstance.py         | Deletes a database instance.                                 |
| CleanOsUser.py           | Deletes the node **osuser**.                                 |
| Resetreplconninfo.py     | Resets the local **replconninfo**.                           |
| LocalPerformanceCheck.py | Checks SSD information.                                      |
| CheckUpgrade.py          | Checks environment variables of a node before the upgrade.   |
| InitInstance.py          | Initializes a database.                                      |
| Backup.py                | Backs up binary files and parameter files.                   |
| ConfigInstance.py        | Configures a database instance.                              |
| CheckConfig.py           | Checks the node configuration.                               |
| ConfigHba.py             | Configures the **pghba.cong** file.                          |
| ExecuteSql.py            | Executes database SQL commands.                              |
| LocalCheckOS.py          | Checks local OS information.                                 |
| UnPreinstallUtility.py   | Clears configurations in the pre-installation process.       |
| CheckPreinstall.py       | Checks whether a node has been pre-installed.                |
| CheckInstall.py          | Checks the node installation information.                    |
| GaussStat.py             | Collects information about nodes in a database.              |
| CheckPythonVersion.py    | Checks the Python version.                                   |
| expect.sh                | Enters an interactive password automatically.                |
| CheckSshAgent.py         | Protects the **ssh-agent** process in SSH mutual trust.      |
| LocalCollect.py          | Collects local file information and parameter information.   |
| killall                  | **killall** is a self-developed tool used by the O&M personnel when the **killall** command does not exist in the system. It can replace some functions of the **killall** command in the system. |
| transfer.py              | Transfers the lib file of the C function to all nodes or the standby node. |
| Restore.py               | Restores binary files and parameter files.                   |
| StartInstance.py         | Starts a database.                                           |
| StopInstance.py          | Stops a database.                                            |
| py_pstree.py             | Kills a process.                                             |
| install.sh               | Installation script for a single node.                       |
| one_master_one_slave.sh  | Script for one-click deployment of one primary database and one standby database. |
| sshexkey_encrypt_tool.sh | Script for running the **ssh-keygen** command to generate a key file (encrypted using a passphrase). This script is used by the mutual trust tool. |
| ssh-agent.sh             | Shell script for starting the **ssh-agent** process. This script is used by the mutual trust module. |