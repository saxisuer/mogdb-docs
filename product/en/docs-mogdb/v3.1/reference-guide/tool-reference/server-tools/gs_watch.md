---
title: gs_watch
summary: gs_watch
author: Zhang Cuiping
date: 2022-06-22
---

# gs_watch

## Introduction

The gs_watch tool monitors the MogDB process. When gs_watch is enabled, once MogDB process malfunctions and crashes, gs_watch automatically call gs_collector to collect the system status.

## Syntax

- Enable automatic trigger. 

  ```bash
  gs_watch -t start [-o OUTDIR] [--pid=PID]
  ```

- Disable automatic trigger. 

  ```bash
  gs_watch -t stop [--pid=PID]
  ```

## Parameter Description

| Parameter | Description                                                  |
| --------- | ------------------------------------------------------------ |
| -t        | Specifies the gs_watch command type.<br />Values: **start** and **stop** |
| -o        | Specifies the export path for gs_collector to collect results in the event of a system crash.<br />Note: If this parameter is not specified, the check result is output to the **$GAUSSLOG** directory as a package by default. |
| --pid     | Specifies the process ID.<br />If multiple MogDB processes are started, you need to set this parameter to specify which MogDB process needs to be watched or stopped watching. Otherwise, an error may occur. |

## Example

1. Enable automatic trigger. 

   ```bash
   gs_watch -t start -o /opt/mogdb/data --pid=23550
   ```

2. Disable automatic trigger. 

   ```bash
   gs_watch -t stop --pid=23550
   ```

## Helpful Link

[gs_collector](4-gs_collector.md)