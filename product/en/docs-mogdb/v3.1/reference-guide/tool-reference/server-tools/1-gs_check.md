---
title: gs_check
summary: gs_check
author: Zhang Cuiping
date: 2021-06-07
---

# gs_check

## Background

**gs_check** has been enhanced to unify functions of various check tools, such as gs_check and gs_checkos. It helps you fully check MogDB runtime, OS, network, and database running environments; as well as perform comprehensive checks on various environments before major operations in MogDB, ensuring smooth operation.

## Precautions

- Parameter **-i** or **-e** must be set. **-i** specifies a single item to be checked, and **-e** specifies an inspection scenario where multiple items will be checked.
- If **-i** is not set to a root item or no such items are contained in the check item list of the scenario specified by **-e**, you do not need to enter the name or password of a user with the root permissions.
- You can run **-skip-root-items** to skip root items.
- If the MTU values are inconsistent, the check may be slow or the check process may fail to respond. When the inspection tool displays a message, change the MTU values of the nodes to be the same and then perform the inspection.
- If the switch does not support the configured MTU value, process response failures may be caused due to communication problems even if the MTU values are the same. In this case, you need to adjust the MTU based on the switch.

## Syntax

- Check a single-item.

  ```bash
  gs_check -i ITEM [...] [-U USER] [-L] [-l LOGFILE] [-o OUTPUTDIR] [--skip-root-items][--set][--routing]
  ```

- Check a scenario.

  ```bash
  gs_check -e SCENE_NAME [-U USER] [-L] [-l LOGFILE] [-o OUTPUTDIR] [--skip-root-items] [--time-out=SECS][--set][--routing][--skip-items]
  ```

- Display help information.

  ```bash
  gs_check -? | --help
  ```

- Display version information.

  ```bash
  gs_check -V | --version
  ```

## Parameter Description

- -U

  Specifies the name of the user for running MogDB.

  Value range: Name of the user for running MogDB

- -L

  Specifies that the check is locally performed.

- -i

  Specifies a check item. Its format is **-i Check** *XX*. For details about check items, see [MogDB status checklist](#mogdb-status-checklist).

- -e

  Specifies scenario check items. Default scenarios include **inspect** (routine inspection), **upgrade** (pre-upgrade inspection), **binary_upgrade** (local pre-upgrade inspection), **health** (health check inspection), and **install** (installation). You can also compile scenarios as required.

- -l

  Specifies a log file path, Add the .log suffix when specifying the path.

- -o

  Specifies the path of the check result output folder.

- -skip-root-items

  Skips the check items that require root permissions.

- -skip-items

  Skips specified check items.

- -format

  Specifies the format of the result report.

- -set

  Specifies abnormal items that can be fixed.

- -cid

  Checks the ID used only by the internal check process.

- -time-out

  Specifies the timeout period. The unit is second. If the user-defined timeout period is not less than 1500 seconds, the default value (1500 seconds) is used.

- -routing

  Specifies the network segment for service IP addresses. The format is *IP address*:*Subnet mask*.

- -disk-threshold=PERCENT

  Specifies the alarm threshold when you check disk usage. You can specify the integer value that ranges from 1 to 99. The default value is **90**. This parameter is not mandatory for other check items.

- -?, -help

  Displays help information.

- -V, -version

    Displays version information.

### MogDB status checklist

- OS
    <table>
        <tr>
            <td>Check Item</td>
            <td>Description</td>
            <td>-set Supported or Not</td>
        </tr>
        <tr>
            <td>CheckCPU</td>
            <td>
            Checks the CPU usage of the host. If <b>idle</b> is greater than 30% and
            <b>iowait</b> is less than 30%, this item passes the check. Otherwise,
            this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckFirewall</td>
            <td>
            Checks the firewall status of the host. If the firewall is disabled, this
            item passes the check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckTimeZone</td>
            <td>
            Checks whether nodes in MogDB use the same time zone. If they do, this
            item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckSysParams</td>
            <td>
            Checks whether the values of OS parameters for each node are as expected.
            If the parameters do not meet the requirements of the warning field, a
            warning is reported. If the parameters do not meet the requirements of the
            NG field, this item fails the check, and the failed parameters are
            printed.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckOSVer</td>
            <td>
            Check the OS version of each node in MogDB. If versions are consistent
            with those in the version compatibility list and information about
            versions of OSs running on nodes in MogDB is included in the same
            version list, the item passes the check. Otherwise, this item fails the
            check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckNTPD</td>
            <td>
            Checks the NTPD service. If the service is enabled and the time difference
            across nodes is within 1 minute, this item passes the check. Otherwise,
            this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckTHP</td>
            <td>
            Checks the THP service. If the service is enabled, this item passes the
            check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckSshdService</td>
            <td>
            Checks whether the sshd service is started. If yes, this item passes the
            check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckCrondService</td>
            <td>
            Checks whether the crontab service is started. If yes, this item passes
            the check. Otherwise, this item fails the check.
            </td>
            <td>Yes<br /></td>
        </tr>
        <tr>
            <td>CheckCrontabLeft</td>
            <td>
            Checks whether the crontab file contains remaining Gauss information. If
            no, this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckDirLeft</td>
            <td>
            Checks whether the <b>/opt/enmo/Bigdata/</b>, <b>/var/log/Bigdata/</b>,
            and <b>/home/omm</b> directories exist. If they do not exist or exist only
            in the <b>mount</b> directory, this item passes the check. Otherwise, this
            item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckProcessLeft</td>
            <td>
            Checks whether the mogdb and omm processes exist. If no, this item
            passes the check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckStack</td>
            <td>
            Checks stack depths. If the stack depths across nodes are inconsistent, a
            warning is reported. If the stack depths are consistent and greater than
            or equal to 3072, this item passes the check. If the stack depths are
            consistent but less than 3072, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckOmmUserExist</td>
            <td>
            Checks whether user <b>omm</b> exists. If no, this item passes the check.
            Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckPortConflict</td>
            <td>
            Checks whether database node ports are occupied. If they are not, this
            item passes the check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckSysPortRange</td>
            <td>
            Checks the value range of the system parameter <b>ip_local_port_range</b>.
            If the value range is 26000 to 65535, this item passes the check.
            Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckEtcHosts</td>
            <td>
            If <b>localhost</b> is not configured for <b>/etc/hosts,</b> there is a
            mapping whose comment contains <b>#MogDB</b>, or the names of hosts
            having the same IP address are different, this item fails the check.
            Otherwise, this item passes the check. In addition, if host names are the
            same but IP addresses are different, this item also fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckCpuCount</td>
            <td>
            Checks the number of CPU cores. If the number is different from that of
            available CPUs, this item fails the check. If the two numbers are the same
            but unavailability messages exist, a warning is reported. If the CPU
            information of all nodes is different, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckHyperThread</td>
            <td>
            Checks hyper-threading. If it is started, this item passes the check.
            Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckMemInfo</td>
            <td>
            Checks whether the total memory size of each node is the same. If yes,
            this item passes the check. Otherwise, a warning is reported.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckSshdConfig</td>
            <td>
            Checks the <b>/etc/ssh/sshd_config</b> file.<br />
            (a)PasswordAuthentication=yes; <br />
            (b)MaxStartups=1000; <br />
            (c)UseDNS=no; <br />
            (d) <b>ClientAliveInterval</b> is greater than 10800 or equal to 0. <br />
            If the above information is configured, this item passes the check. If a
            and c configurations are incorrect, a warning is reported. If b and d
            configurations are incorrect, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckMaxHandle</td>
            <td>
            Checks the maximum handle value of the OS. If the value is greater than or
            equal to 1 million, this item passes the check. Otherwise, this item fails
            the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckKernelVer</td>
            <td>
            Checks the kernel version of each node. If the version information is
            consistent, this item passes the check. Otherwise, a warning is reported.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckEncoding</td>
            <td>
            Checks the system code of each node in MogDB. If the codes are
            consistent, this item passes the check. Otherwise, this item fails the
            check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckBootItems</td>
            <td>
            Checks whether there are manually added startup items. If no, this item
            passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckDropCache</td>
            <td>
            Checks whether there is a dropcache process running on each node. If yes,
            this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckFilehandle</td>
            <td>
            Checks the following conditions. If both the conditions are met, this item
            passes the check. Otherwise, this item fails the check.<br />- The number
            of processes opened by each mogdb process does not exceed 800,000.<br />-
            The number of handles used by the slave process does not exceed that of
            handles used by the master process.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckKeyProAdj</td>
            <td>
            Checks all key processes. If the <b>omm_adj</b> value for all key
            processes is <b>0</b>, this item passes the check. Otherwise, this item
            fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckMaxProcMemory</td>
            <td>
            Checks whether the value of <b>max_process_memory</b> on the database
            nodes is greater than 1 GB. If no, this item passes the check. Otherwise,
            this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
    </table>

- Device

    <table>
      <tr>
        <td>Check Item</td>
        <td>Description</td>
        <td>-set Supported or Not</td>
      </tr>
      <tr>
        <td>CheckSwapMemory</td>
        <td>
          Checks the swap memory and total memory sizes. If the check result is 0,
          this item passes the check. Otherwise, a warning is reported. If the
          result is greater than the total memory, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckLogicalBlock</td>
        <td>
          Checks the logical block size of a disk. If the result is <b>512</b>, this
          item passes the check. Otherwise, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckIOrequestqueue</td>
        <td>
          Checks the I/O value. If the value is <b>32768</b>, this item passes the
          check. Otherwise, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckMaxAsyIOrequests</td>
        <td>
          Checks the maximum number of asynchronous requests. If the number of
          asynchronous I/O requests is greater than 104857600 and greater than the
          number of database instances on the current node x 1048576, this item
          passes the check. Otherwise, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckIOConfigure</td>
        <td>
          Checks the I/O configuration. If the result is <b>deadline</b>, this item
          passes the check. Otherwise, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckBlockdev</td>
        <td>
          Checks the size of the pre-read block. If the result is <b>16384</b>, this
          item passes the check. Otherwise, this item fails the check.
        </td>
        <td>Yes</td>
      </tr>
      <tr>
        <td>CheckDiskFormat</td>
        <td>
          Checks the XFS format information about a disk. If the result is
          <b>rw,noatime,inode64,allocsize=16m</b>, this item passes the check.
          Otherwise, a warning is reported.
        </td>
        <td>No</td>
      </tr>
      <tr>
        <td>CheckInodeUsage</td>
        <td>
          Checks MogDB paths <b>GAUSSHOME/PGHOST/GPHOMEE/GAUSSLOG/tmp</b> and
          instance directories.<br />
          Checks the usage of the above directories. If the usage exceeds the
          warning threshold (60% by default), a warning is reported. If the usage
          exceeds the NG threshold (80% by default), this item fails the check. If
          the usage is less than or equal to the NG threshold, this item passes the
          check.
        </td>
        <td>No</td>
      </tr>
      <tr>
        <td>CheckSpaceUsage</td>
        <td>
          Checks MogDB paths <b>GAUSSHOME/PGHOST/GPHOME/GAUSSLOG/tmp </b>and
          instance directories.<br />
          Checks the usage of the above directories. If the usage exceeds the
          warning threshold (70% by default), a warning is reported. If the usage
          exceeds the NG threshold (90% by default), this item fails the check. Also
          checks the available space of the
          <b>GAUSSHOME/PGHOST/GPHOME/GAUSSLOG/tmp/data</b> directory. If the space
          is less than the threshold, this item fails the check. Otherwise, this
          item passes the check.
        </td>
        <td>No</td>
      </tr>
      <tr>
        <td>CheckDiskConfig</td>
        <td>
          Checks whether disk configurations are consistent. If the names, sizes,
          and mount points of disks are the same, this item passes the check. If any
          of them is inconsistent, a warning is reported.
        </td>
        <td>No</td>
      </tr>
      <tr>
        <td>CheckXid</td>
        <td>
          Checks the value of <b>xid</b>. If the value is greater than 1 billion, a
          warning is reported. If the value is greater than 1.8 billion, this item
          fails the check.
        </td>
        <td>No</td>
      </tr>
      <tr>
        <td>CheckSysTabSize</td>
        <td>
          Checks the system catalog capacity of each instance. If the available
          capacity of each disk is greater than the total capacity of system
          catalogs for all instances on the disk, this item passes the check.
          Otherwise, this item fails the check.
        </td>
        <td>No</td>
      </tr>
    </table>

- Cluster

    <table>
        <tr>
            <td>Check Item</td>
            <td>Description</td>
            <td>-set Supported or Not</td>
        </tr>
        <tr>
            <td>CheckClusterState</td>
            <td>
            Checks the fencedUDF status. If it is <b>down</b>, a warning is reported.
            In this case, check the MogDB status. If it is <b>Normal</b>, this
            item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckDBParams</td>
            <td>
            For the primary database node, checks the size of the shared buffer and
            the <b>Sem</b> parameter.<br />
            For database nodes, checks the size of the shared buffer and the maximum
            number of connections.<br />The shared buffer size should be greater than
            128 KB, greater than <b>shmmax</b>, and greater than <b>shmall</b> x
            <b>PAGESIZE</b>. <br />
            If there is the primary database node, <b>Sem</b> must be greater than the
            rounded up result of (Maximum number of database node connections +
            150)/16.<br />If the above items are met, this item passes the check. If
            any of them is not met, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckDebugSwitch</td>
            <td>
            Checks the value of the <b>log_min_messages</b> parameter in the
            configuration file of each instance on each node. If the value is empty,
            the default log level <b>warning</b> is used. In this case, if the actual
            log level is not <b>warning</b>, a warning is reported.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckUpVer</td>
            <td>
            Checks the version of the upgrade package on each node in MogDB. If
            the versions are consistent, this item passes the check. Otherwise, this
            item fails the check. You need to specify the path of the upgrade software
            package.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckDirPermissions</td>
            <td>
            Checks permissions for the node directories (instance Xlog path,
            <b>GAUSSHOME</b>, <b>GPHOME</b>, <b>PGHOST</b>, and <b>GAUSSLOG</b>). If
            the directories allow for the write permission and at most 750 permission,
            this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckEnvProfile</td>
            <td>
            Checks the environment variables (<b>$GAUSSHOME</b>,
            <b>$LD_LIBRARY_PATH</b>, and <b>$PATH</b>) of nodes and those of the CMS,
            CMA, and database node processes. If there are node environment variables
            that are correctly configured and process environment variables exist,
            this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckGaussVer</td>
            <td>
            Checks whether the mogdb version of each node is consistent. If the
            versions are consistent, this item passes the check. Otherwise, this item
            fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckPortRange</td>
            <td>
            Checks the port range. If the value of <b>ip_local_port_range</b> is
            within the threshold (26000 to 65535 by default) and an instance port is
            out of the range, this item passes the check. Otherwise, this item fails
            the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckReadonlyMode</td>
            <td>
            Checks the read only mode. If the value of
            <b>default_transaction_read_only</b> on the database nodes in MogDB is
            <b>off</b>, this item passes the check. Otherwise, this item fails the
            check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckCatchup</td>
            <td>
            Checks whether the CatchupMain function can be found in the mogdb
            process stack. If no, this item passes the check. Otherwise, this item
            fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckProcessStatus</td>
            <td>
            Checks the owner of the mogdb processes. If their owner is only user
            <b>omm</b>, this item passes the check. Otherwise, this item fails the
            check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckSpecialFile</td>
            <td>
            Checks whether the files in the <b>tmp</b> directory (<b>PGHOST</b>), OM
            directory (<b>GPHOME</b>), log directory (<b>GAUSSLOG</b>), data
            directory, and program directory (<b>GAUSSHOME</b>) contain special
            characters or whether there are files that do not belong to user
            <b>omm</b>. If none of them exists, this item passes the check. Otherwise,
            this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckCollector</td>
            <td>
            Checks whether information is successfully collected in the output
            directory. If yes, this item passes the check. Otherwise, this item fails
            the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckLargeFile</td>
            <td>
            Checks whether there is a file over 4 GB in the directory of each database
            node. If there is such a file in any database node directory and its
            subdirectories, this item fails the check. Otherwise, this item passes the
            check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckProStartTime</td>
            <td>
            Checks whether the interval for starting key processes exceeds 5 minutes.
            If no, this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
        <tr>
            <td>CheckDilateSysTab</td>
            <td>
            Checks whether a system catalog is bloated. If no, this item passes the
            check. Otherwise, this item fails the check.
            </td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>CheckMpprcFile</td>
            <td>
            Checks whether the environment variable isolation file is modified. If no,
            this item passes the check. Otherwise, this item fails the check.
            </td>
            <td>No</td>
        </tr>
    </table>

- Database

    <table>
        <tr>
            <td>Check Item</td>
            <td>Description</td>
            <td>-set Supported or Not</td>
        </tr>
        <tr>
          <td>CheckLockNum</td>
          <td>
            Checks the number of database locks. If a result is returned, this item
            passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckArchiveParameter</td>
          <td>
            Checks the database archive parameter. If the parameter is not enabled or
            is enabled for database nodes, this item passes the check. If it is
            enabled but not for database nodes, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckCurConnCount</td>
          <td>
            Checks the number of database connections. If the number is less than 90%
            of the maximum connection quantity, this item passes the check. Otherwise,
            this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckCursorNum</td>
          <td>
            Checks the number of cursors in the database. If a result is returned,
            this item passes the check. Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckMaxDatanode</td>
          <td>
            Checks the maximum number of database nodes. If the number is less than
            the number of nodes configured in the XML file multiplied by the number of
            database nodes (90 x 5 by default), a warning is reported. Otherwise, this
            item passes the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckPgPreparedXacts</td>
          <td>
            Checks the <b>pgxc_prepared_xacts</b> parameter. If no 2PC transactions
            are found, this item passes the check. Otherwise, this item fails the
            check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckPgxcgroup</td>
          <td>
            Checks the number of redistributed records in the <b>pgxc_group</b> table.
            If the result is 0, this item passes the check. Otherwise, this item fails
            the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckLockState</td>
          <td>
            Checks whether MogDB is locked. If no, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckIdleSession</td>
          <td>
            Checks the number of non-idle sessions. If the result is 0, this item
            passes the check. Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckDBConnection</td>
          <td>
            Checks whether the database can be connected. If yes, this item passes the
            check. Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckGUCValue</td>
          <td>
            Checks the result of [(<b>max_connections</b> +
            <b>max_prepared_transactions</b>) x <b>max_locks_per_transaction</b>]. If
            it is greater than or equal to 1 million, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckPMKData</td>
          <td>
            Checks whether the PMK schema of the database contains abnormal data. If
            no, this item passes the check. Otherwise, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckSysTable</td>
          <td>
            Checks the system catalog. If the check can be performed, this item passes
            the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckSysTabSize</td>
          <td>
            Checks the system catalog capacity of each instance. If the available
            capacity of each disk is greater than the total capacity of system
            catalogs for all instances on the disk, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckTableSpace</td>
          <td>
            Checks tablespace paths. If no tablespace path and MogDB path are
            nested and no tablespace paths are nested, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckTableSkew</td>
          <td>
            Checks the skew of table data. If a table has unbalanced data distribution
            among MogDB database nodes, and the database node with the most data
            has over 100,000 records more than the database node with the least data,
            this item fails the check. Otherwise, this item passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckDNSkew</td>
          <td>
            Checks the skew of table data at the database node level. If the database
            node with the most amount of data has 5% more than the database node with
            the smallest amount of data, this item fails the check. Otherwise, this
            item passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckUnAnalyzeTable</td>
          <td>
            Checks for a table that has not been analyzed. If there is such a table
            and the table contains at least one record, this item fails the check.
            Otherwise, this item passes the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckCreateView</td>
          <td>
            If the query statement for creating a view contains sub-queries, and
            parsing and rewriting sub-query results lead to duplicate aliases, the
            check result is failed. Otherwise, the check result is passed.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckHashIndex</td>
          <td>
            Checks whether there are hash indexes. If yes, this item fails the check.
            Otherwise, this item passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckNextvalInDefault</td>
          <td>
            Checks whether a DEFAULT expression contains <b>nextval</b> (sequence). If
            yes, this item fails the check. Otherwise, this item passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckNodeGroupName</td>
          <td>
            Checks whether the name of a Node Group contains non-SQL_ASCII characters.
            If yes, this item fails the check. Otherwise, this item passes the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckPgxcRedistb</td>
          <td>
            Checks whether any temporary table remains in the database after data
            redistribution. If yes, this item fails the check. Otherwise, this item
            passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckReturnType</td>
          <td>
            Checks whether a user-defined function contains invalid return value
            types. If yes, this item fails the check. Otherwise, this item passes the
            check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckSysadminUser</td>
          <td>
            Checks whether there are database administrators in addition to the owner
            of MogDB. If yes, this item fails the check. Otherwise, this item
            passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckTDDate</td>
          <td>
            Checks whether the ORC table in a Teradata database contains columns of
            the date type. If yes, this item fails the check. Otherwise, this item
            passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckDropColumn</td>
          <td>
            Checks whether there are tables on which <b>DROP COLUMN</b> has been
            performed. If yes, this item fails the check. Otherwise, this item passes
            the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckDiskFailure</td>
          <td>
            Checks for disk faults. If there is an error during full data query in
            MogDB, this item fails the check. Otherwise, this item passes the
            check.
          </td>
          <td>No</td>
        </tr>
    </table>

- Network

    <table>
        <tr>
            <td>Check Item</td>
            <td>Description</td>
            <td>-set Supported or Not</td>
        </tr>
        <tr>
          <td>CheckPing</td>
          <td>
            Checks the connectivity of all nodes in MogDB. If all their IP
            addresses can be pinged from each other, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckRXTX</td>
          <td>
            Checks the RX/TX value for <b>backIP</b> of a node. If it is <b>4096</b>,
            this item passes the check. Otherwise, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckMTU</td>
          <td>
            Checks the MTU value of a NIC corresponding to <b>backIP</b> of a node
            (ensure consistent PICs after bonding). If the result is not 8192 or 1500,
            a warning is reported. In this case, if MTU values in MogDB are the
            same, this item passes the check. Otherwise, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckNetWorkDrop</td>
          <td>
            Checks the packet loss rate of each IP address within 1 minute. If the
            rate does not exceed 1%, this item passes the check. Otherwise, this item
            fails the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckBond</td>
          <td>
            Checks whether <b>BONDING_OPTS</b> or <b>BONDING_MODULE_OPTS</b> is
            configured. If no, a warning is reported. In this case, checks whether the
            bond mode of each node is the same. If yes, this item passes the check.
            Otherwise, this item fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckMultiQueue</td>
          <td>
            Checks <b>cat /proc/interrupts</b>. If multiqueue is enabled for NICs and
            different CPUs are bound, this item passes the check. Otherwise, this item
            fails the check.
          </td>
          <td>Yes</td>
        </tr>
        <tr>
          <td>CheckUsedPort</td>
          <td>
            Checks the value of <b>net.ipv4.ip_local_port_range</b>. If the value is
            greater than or equal to the default value of the OS (32768 to 61000),
            this item passes the check. <br />Checks the number of random TCP ports.
            If the number is less than 80% of the total number of random ports, this
            item passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckNICModel</td>
          <td>
            Checks whether NIC models or driver versions are consistent across nodes.
            If yes, this item passes the check. Otherwise, a warning is reported.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckRouting</td>
          <td>
            Checks the number of IP addresses on the service network segment for each
            node. If the number exceeds 1, a warning is reported. Otherwise, this item
            passes the check.
          </td>
          <td>No</td>
        </tr>
        <tr>
          <td>CheckNetSpeed</td>
          <td>
            When the network is fully loaded, checks whether the average NIC receiving
            bandwidth is greater than 600 MB. If yes, this item passes the check.<br />
            When the network is fully loaded, checks the network ping value. If it is
            shorter than 1s, this item passes the check.<br />
            When the network is fully loaded, checks the NIC packet loss rate. If it
            is less than 1%, this item passes the check.
          </td>
          <td>No</td>
        </tr>
    </table>

- Others

    <table>
        <tr>
            <td>Check Item</td>
            <td>Description</td>
            <td>-set Supported or Not</td>
        </tr>
        <tr>
            <td>CheckDataDiskUsage</td>
            <td>
            Checks the usage of the disk database node directory. If the usage is
            lower than 90%, this item passes the check. Otherwise, this item fails the
            check.
            </td>
            <td>No</td>
        </tr>
    </table>

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Constraints on the CheckNetSpeed check item are as follows:
>
> - Do not use **-L** to check CheckNetSpeed, because doing so cannot produce enough network load and the check result will be inaccurate.
> - If the number of nodes is less than six, the network load produced by **speed_test** may not fully occupy the bandwidth, and the check result will be inaccurate.

## Defining a Scenario

1. Log in as the OS user **omm** to the primary node of the database.

2. Create the scenario configuration file **scene_***XXX***.xml** in the **script/gspylib/inspection/config** directory.

3. Write check items to the scenario configuration file in the following format:

   ```xml
   <?xml version="1.0" encoding="utf-8" ?>
   <scene name="XXX" desc="check cluster parameters before XXX.">
   <configuration/>
   <allowitems>
   <item name="CheckXXX"/>
   <item name="CheckXXX"/>
   </allowitems>
   </scene>
   ```

   **item name** indicates the check item name.

   Note: You need to ensure that the user-defined XML file is correct.

4. Run the following command in the **home/package/script/gspylib/inspection/config** directory to deploy the file on each node where the check is to be performed:

   ```bash
   scp scene_upgrade.xml SIA1000068994: home/package/script/gspylib/inspection/config/
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** **home/package/script/gspylib/inspection/config** is the absolute path of the new scenario configuration file.

5. Switch to user **omm** and run the following command on an old node to view the check result:

   ```bash
   gs_check  -e XXX
   ```

## Defining a Check Item

1. Add a check item. Modify the **script/gspylib/inspection/config/items.xml** file in the following format:

   ```xml
   <checkitem id="10010" name="CheckCPU">
   <title>
   <zh>Check the CPU usage.</zh>
   <en>Check CPU Idle and I/O wait</en>
   </title>
   <threshold>
   StandardCPUIdle=30;
   StandardWIO=30
   </threshold>
   <suggestion>
   <zh>If the available space is insufficient and the CPU is heavily loaded, scale out the nodes. If iowait is too high, expand the disk capacity, which is the current performance bottleneck.</zh>.
   </suggestion>
   <standard>
   <zh>Check the CPU usage of the host. If the value of idle is greater than 30% and the value of iowait is less than 30%, this item passes the check. Otherwise, this item fails the check.</zh>
   </standard>
   <category>os</category>
   <permission>user</permission>
   <scope>all</scope>
   <analysis>default</analysis>
   </checkitems>
   ```

   - **id**: specifies the check item ID.

   - **name**: specifies the name of the check script.

   - **title**: specifies the check item description. It allows multiple languages.

     : checks content of Chinese version.

     : checks content of English version.

   - **standard**: specifies the check standards. It allows multiple languages.

   - **suggestion**: provides advice on how to fix check item problems. It allows multiple languages.

   - **threshold**: specifies the check item threshold. Multiple values are separated using semicolons (;), for example, **Key1=Value1;Key2=Value2**.

   - **category**: specifies the check item type. It is optional. Its value can be **os**, **device**, **network**, **cluster**, **database**, or **other**.

   - **permission**: specifies the permission required for checking an item. It is optional. Its value can be **root** or **user** (default).

   - **scope**: specifies the node scope where an item is checked. It is optional. **cn-** indicates that only the primary database node resides is checked. **local-** indicates that only the current node is checked. **all-** is the default value, indicating that all nodes in MogDB are checked.

   - **analysis**: specifies how the check result is analyzed. **default-** is the default value, indicating that the result on every node is checked, and that an item passes the check only if it passes the check on all the nodes. **consistent-** indicates that each node returns a result, and that an item passes the check if all the results are consistent. **custom-** indicates other ways.

   Note: You need to ensure that the user-defined XML file is correct.

2. Create a check script named **Check** *XXXX***.py** in the **script/gspylib/inspection/items** directory. The directory should contain multiple folders, each storing a type of scripts. The format is as follows:

   ```
   class CheckCPU(BaseItem):
   def __init__(self):
   super(CheckCPU, self).__init__(self.__class__.__name__)
   self.idle = None
   self.wio = None
   self.standard = None

   def preCheck(self):
   # check the threshold was set correctly
   if (not self.threshold.has_key('StandardCPUIdle')
   or not self.threshold.has_key('StandardWIO')):
   raise Exception("threshold can not be empty")
   self.idle = self.threshold['StandardCPUIdle']
   self.wio = self.threshold['StandardWIO']

   # format the standard by threshold
   self.standard = self.standard.format(idle=self.idle, iowait=self.wio)

   def doCheck(self):
   cmd = "sar 1 5 2>&1"
   output = SharedFuncs.runShellCmd(cmd)
   self.result.raw = output
   # check the result with threshold
   d = next(n.split() for n in output.splitlines() if "Average" in n)
   iowait = d[-3]
   idle = d[-1]
   rst = ResultStatus.OK
   vals = []
   if (iowait > self.wio):
   rst = ResultStatus.NG
   vals.append("The %s actual value %s is greater than expected value %s" % ("IOWait", iowait, self.wio))
   if (idle < self.idle):
   rst = ResultStatus.NG
   vals.append("The %s actual value %s is less than expected value %s" % ("Idle", idle, self.idle))
   self.result.rst = rst
   if (vals):
   self.result.val = "\n".join(vals)
   ```

   A script is developed based on the BaseItem class, which defines the common check process, result analysis method, and default output format. Extended parameters:

   - **doCheck**: contains specific ways to check an item. The check result is in the following format:

     **result.rst**: (optional) specifies the check result. Its value can be:

     - **OK**: indicates that the item passes the check.
     - **NA**: indicates that the check does not cover the node.
     - **NG**: indicates that the item failed the check.
     - **WARNING**: indicates that the check is complete and that a warning is reported.
     - **ERROR**: indicates that the check is interrupted due to an internal error.

   - **preCheck**: checks prerequisites. Its value can be **cnPreCheck**, which checks whether a primary database node instance is deployed on the current execution node; or **localPreCheck**, which checks whether the current execution node is specified for the check. You can set it using **scope** in the check item configuration file. This method can be reloaded to perform customized pre-checks.

   - **postAnalysis** specifies how the check result is analyzed. Its value can be **default** or **consistent**. You can set it using **analysis** in the check item configuration file. This method can be reloaded to perform customized result analysis.

   Note: The name of a user-defined check item cannot be the same as the name of an existing check item. In addition, you need to ensure that the user-defined check item script is standard.

3. Deploy the script on all execution nodes.

4. Log in to the nodes added in a scale-out as user **root** or to old nodes as user **omm**. Run the following commands as required and view the result:

   To locally perform a check, run the following command:

   ```bash
   gs_check -i CheckXXX  -L
   ```

   To remotely perform a check, run the following command:

   ```bash
   gs_check -i CheckXXX
   ```

## OS Parameters

**Table 2** OS parameters

| Parameter                     | Description                                                  | Recommended Value             |
| :---------------------------- | :----------------------------------------------------------- | :---------------------------- |
| net.ipv4.tcp_max_tw_buckets   | Specifies the maximum number of TCP/IP connections concurrently remaining in the `TIME_WAIT` state. If the number of TCP/IP connections concurrently remaining in the `TIME_WAIT`state exceeds the value of this parameter, the TCP/IP connections in the `TIME_WAIT` state will be released immediately, and alarm information will be printed. | 10000                         |
| net.ipv4.tcp_tw_reuse         | Reuses sockets whose status is `TIME-WAIT` for new TCP connections.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_tw_recycle       | Rapidly reclaims sockets whose status is `TIME-WAIT` in TCP connections.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_keepalive_time   | Specifies how often Keepalived messages are sent through TCP connections when Keepalived is enabled. | 30                            |
| net.ipv4.tcp_keepalive_probes | Specifies the number of Keepalived detection packets sent through a TCP connection before the connection is regarded invalid. The product of the parameter value multiplied by the value of the **tcp_keepalive_intvl** parameter determines the response timeout duration after a Keepalived message is sent through a connection. | 9                             |
| net.ipv4.tcp_keepalive_intvl  | Specifies how often a detection packet is re-sent when the previous packets are not acknowledged. | 30                            |
| net.ipv4.tcp_retries1         | Specifies the maximum TCP reattempts during connection establishment. | 5                             |
| net.ipv4.tcp_syn_retries      | Specifies the maximum SYN packet reattempts in the TCP.      | 5                             |
| net.ipv4.tcp_synack_retries   | Specifies the maximum SYN response packet reattempts in the TCP. | 5                             |
| net.ipv4.tcp_retries2         | Specifies the number of times that the kernel re-sends data to a connected remote host. A smaller value leads to earlier detection of an invalid connection to the remote host, and the server can quickly release this connection.<br />If "connection reset by peer" is displayed, increase the value of this parameter to avoid the problem. | 12                            |
| vm.overcommit_memory          | Specifies the kernel check method during memory allocation.<br />- **0**: The system accurately calculates the current available memory.<br />- **1**: The system returns a success message without a kernel check.<br />- **2**: The system returns a failure message if the memory size you have applied for exceeds the result of the following formula: Total memory size x Value of `vm.overcommit_ratio` / 100 + Total SWAP size.<br />The default value for a kernel is **2**, which is too conservative. The recommended value is **0**. If system loads are high, set this parameter to**1**. | 0                             |
| net.ipv4.tcp_rmem             | Specifies the free memory in the TCP receiver buffer. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. | 8192 250000 16777216          |
| net.ipv4.tcp_wmem             | Specifies the free memory in the TCP sender buffer. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. | 8192 250000 16777216          |
| net.core.wmem_max             | Specifies the maximum size of the socket sender buffer.      | 21299200                      |
| net.core.rmem_max             | Specifies the maximum size of the socket receiver buffer.    | 21299200                      |
| net.core.wmem_default         | Specifies the default size of the socket sender buffer.      | 21299200                      |
| net.core.rmem_default         | Specifies the default size of the socket receiver buffer.    | 21299200                      |
| net.ipv4.ip_local_port_range  | Specifies the range of temporary ports that can be used by a physical server. | 26000-65535                   |
| kernel.sem                    | Specifies the kernel semaphore.                              | 250 6400000 1000 25600        |
| vm.min_free_kbytes            | Specifies the minimum free physical memory reserved for unexpected page breaks. | 5% of the total system memory |
| net.core.somaxconn            | Specifies the maximum length of the listening queue of each port. This is a global parameter. | 65535                         |
| net.ipv4.tcp_syncookies       | Specifies whether to enable SYN cookies to guard the OS against SYN attacks when the SYN waiting queue overflows.<br />- **0**: The SYN cookies are disabled.<br />- **1**: The SYN cookies are enabled. | 1                             |
| net.core.netdev_max_backlog   | Specifies the maximum number of data packets that can be sent to the queue when the rate at which the network device receives data packets is higher than that at which the kernel processes the data packets. | 65535                         |
| net.ipv4.tcp_max_syn_backlog  | Specifies the maximum number of unacknowledged connection requests to be recorded. | 65535                         |
| net.ipv4.tcp_fin_timeout      | Specifies the default timeout.                               | 60                            |
| kernel.shmall                 | Specifies the total shared free memory of the kernel.        | 1073741824           |
| kernel.shmmax                 | Specifies the maximum value of a shared memory segment.      | 4398046511104          |
| net.ipv4.tcp_sack             | Specifies whether selective acknowledgment is enabled. The selective acknowledgment on out-of-order packets can increase system performance. Restricting users to sending only lost packets (for wide area networks) should be enabled, but this will increase CPU usage.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_timestamps       | Specifies whether the TCP timestamp (12 bytes are added in the TCP packet header) enables a more accurate RTT calculation than the retransmission timeout (for details, see RFC 1323) for better performance.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| vm.extfrag_threshold          | When system memory is insufficient, Linux will score the current system memory fragments. If the score is higher than the value of `vm.extfrag_threshold`,`kswapd` triggers memory compaction. When the value of this parameter is close to **1000**, the system tends to swap out old pages when processing memory fragments to meet the application requirements. When the value of this parameter is close to**0**, the system tends to do memory compaction when processing memory fragments. | 500                           |
| vm.overcommit_ratio           | When the system uses the algorithms where memory usage never exceeds the thresholds, the total memory address space of the system cannot exceed the value of **swap+RAM** multiplied by the percentage specified by this parameter. When the value of `vm.overcommit_memory` is set to **2**, this parameter takes effect. | 90                            |
| MTU                           | Specifies the maximum transmission unit (MTU) for a node NIC. The default value in the OS is **1500**. You can set it to **8192** to improve the performance of sending and receiving data using SCTP. | 8192                          |

## File System Parameters

- soft nofile

  Indicates the soft limit. The number of file handles used by a user can exceed this parameter value. However, an alarm will be reported.

  Recommended value: **1000000**

- hard nofile

  Indicates the hard limit. The number of file handles used by a user cannot exceed this parameter value.

  Recommended value: **1000000**

- stack size

  Specifies the thread stack size.

  Recommended value: **3072**

## Examples

Check result of a single item:

```bash
perfadm@lfgp000700749:/opt/huawei/perfadm/tool/script> gs_check -i CheckCPU
Parsing the check items config file successfully
Distribute the context file to remote hosts successfully
Start to health check for the cluster. Total Items:1 Nodes:3

Checking...               [=========================] 1/1
Start to analysis the check result
CheckCPU....................................OK
The item run on 3 nodes.  success: 3

Success. All check items run completed. Total:1  Success:1  Failed:0
For more information please refer to /opt/mogdb/tools/script/gspylib/inspection/output/CheckReport_201902193704661604.tar.gz
```

Local execution result:

```bash
perfadm@lfgp000700749:/opt/huawei/perfadm/tool/script> gs_check -i CheckCPU -L

2017-12-29 17:09:29 [NAM] CheckCPU
2017-12-29 17:09:29 [STD] Check the CPU usage of the host. If the value of idle is greater than 30% and the value of iowait is less than 30%, this item passes the check. Otherwise, this item fails the check.
2017-12-29 17:09:29 [RST] OK

2017-12-29 17:09:29 [RAW]
Linux 4.4.21-69-default (lfgp000700749)  12/29/17  _x86_64_

17:09:24        CPU     %user     %nice   %system   %iowait    %steal     %idle
17:09:25        all      0.25      0.00      0.25      0.00      0.00     99.50
17:09:26        all      0.25      0.00      0.13      0.00      0.00     99.62
17:09:27        all      0.25      0.00      0.25      0.13      0.00     99.37
17:09:28        all      0.38      0.00      0.25      0.00      0.13     99.25
17:09:29        all      1.00      0.00      0.88      0.00      0.00     98.12
Average:        all      0.43      0.00      0.35      0.03      0.03     99.17
```

Check result of a scenario:

```bash
[perfadm@SIA1000131072 Check]$ gs_check -e inspect
Parsing the check items config file successfully
The below items require root privileges to execute:[CheckBlockdev CheckIOConfigure CheckMTU CheckRXTX CheckMultiQueue CheckFirewall CheckSshdService CheckSshdConfig CheckCrondService CheckMaxProcMemory CheckBootItems CheckFilehandle CheckNICModel CheckDropCache]
Please enter root privileges user[root]:
Please enter password for user[root]:
Check root password connection successfully
Distribute the context file to remote hosts successfully
Start to health check for the cluster. Total Items:57 Nodes:3
Checking...               [=========================] 57/57
Start to analysis the check result
CheckClusterState...........................OK
The item run on 3 nodes.  success: 3
CheckDBParams...............................OK
.........................................................................
CheckMpprcFile..............................OK
The item run on 3 nodes.  success: 3

Analysis the check result successfully
Failed. All check items run completed. Total:57   Success:49   Warning:5   NG:3   Error:0
For more information please refer to /opt/huawei/wisequery/script/gspylib/inspection/output/CheckReport_inspect_201902207129254785.tar.gz
```

## Helpful Links

[gs_checkos](2-gs_checkos.md), [gs_checkperf](3-gs_checkperf.md)
