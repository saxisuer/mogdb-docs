---
title: gs_collector
summary: gs_collector
author: Zhang Cuiping
date: 2021-06-07
---

# gs_collector

## Background

When MogDB is faulty, **gs_collector** can be used to collect information about the OS, logs, and configuration file of the cluster, helping you locate the fault. You can use the **-C** parameter to specify the information to be collected. [Table 1](#info) describes the information that can be collected.

**Table 1** Information to be collected by gs_collector <a id="info"></a>.

| TypeName | Content                 | Description             | Default Collection              |
| :------- | :------------------------ | :-----------------------| :-------------------------------- |
| System   | HardWareInfoRunTimeInfo                                      | Collect the status information about the current OS.<br />- HardWareInfo<br />- CPU (cat /proc/cpuinfo)<br />- Memory (cat /proc/meminfo)<br />- Disk (df -h)<br />- RunTimeInfo:<br />- topStatus (ps ux)<br />- ioStat (iostat -xm 2 3)<br />- Network status (cat /proc/net/dev)<br />- Memory usage (free -m) | Yes                                                          |
| Database | For details, see **System catalogs and views supported by** **gs_collector**. | Collect system views or system catalogs.                     | Yes. Information about the **pg_locks**, **pg_stat_activity**, and **pg_thread_wait_status** views is collected by default. |
| Log      | DataNodeClusterManager                                       | Collect **pg_log** and **gs_profile** of the data node.<br />The ClusterManager log information to be collected:<br />- om<br />- gs_ctl<br />- gs_guc<br />- gs_initdb | Yes                                                          |
| Config   | DataNode                                                     | Collect the data node configuration information:<br />- postgresql.conf<br />- mogdb.state<br />- pg_hba.conf<br />- pg_control<br />- pg_replslot<br />- pg_ident.conf | Yes                                                          |
| Gstack   | DataNode                                                     | Run the **gstack** command to obtain the current stack information of the data node. | No                                                           |
| CoreDump | gs_ctl, mogdb, GaussMaster, AioWorker, AlarmChecker, Archiver, Auditor, AutoVacLauncher, AutoVacWorker, AuxMain, BackendMode, BgWriter, BootStrap, Catchup, CBMWriter, Checkpointer, CommAuxStream, CommPoolCleane, CommRcvStream, CommRcvWorker, CommSendStream, CpMonitor, DataRcvWriter, DataReceiver, DataSender, ExtremeRTO, FencedUDFMaster, Heartbeater, JobExecutor, JobScheduler, LWLockMonitor, PageWriter, ParallelRecov, PercentileJob, Reaper, RemoteSrv, StartupProcess, StatCollector, Stream, SysLogger, ThdPoolListener, TwoPhaseCleaner, WalRcvWriter, WalReceiver, WalSender, WalWriter, WDRSnapshot, WlmArbiter, WlmCollector, WlmMonitor | Filter the core files of the mogdb process by time.<br />CAUTION:<br />Configure the core as follows: Add the core file format **core-%e-%p-%t** to the **/proc/sys/mogdb/core_pattern** file. | No                                                           |
| XLog     | DataNode                                                     | Collect Xlogs that are filtered by time.                     | No                                                           |
| Plan     |                                                              | Collect planned reproduction information:<br />- If you enter an asterisk (), the planned reproduction information of all databases is collected.<br />- If you enter a specific database name, the planned reproduction information of the specified database is collected. | No                                                           |

## Prerequisites

An OS tool (such as **gstack**) that **gs_collector** requires has been installed. If it is not installed, an error message is displayed, and this collection item is skipped.

## Syntax

- Collect logs (as a non-**root** user).

  ```bash
  gs_collector --begin-time="BEGINTIME" --end-time="ENDTIME" [-h HOSTNAME | -f HOSTFILE] [--keyword=KEYWORD] [--speed-limit=SPEED] [-o OUTPUT] [-l LOGFILE] [-C CONFIGFILE]
  ```

- Display help information.

  ```bash
  gs_collector -? | --help
  ```

- Display version information.

  ```bash
  gs_collector -V | --version
  ```

## Parameter Description

- -h

  Specifies the name of the host whose information is to be collected.

  Value range: host name

  If this parameter is not specified, information about all hosts is collected by default.

- -f

  Specifies the file of a host name list. The file is a plain text file.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The **-f** and **-h** parameters cannot be used together.

  Value range: a host name list

- -o

  Saves collected logs as a package into a specified folder.

  If this parameter is not specified, the check results are saved as a package into the directory specified by **tmpMppdbPath** in the configuration file.

  If **tmpMppdbPath** is not configured in the configuration file, the check results are saved as a package into the **/tmp/***user name***_mppdb/** directory by default.

- -l

  Specifies a log file and its save path.

- -C

  Specifies the configuration file of the information to be collected. **TypeName** is used to specify the type of the information to be collected. **Content** is used to specify the content of each type of information. **Count** is used to specify the number of times for collecting the information. **Interval** is used to specify the collection interval, in seconds.

  The values of **TypeName** and **Content** cannot be empty.

  **Interval** and **Count** are optional. If **Count** is not specified, the information is collected once by default. If **Interval** is not specified, the interval is 0 seconds. The values of **Interval** and **Count** cannot be smaller than 0.

  If the preceding parameters are not specified, the default configuration file is used.

  The configuration file is in JSON format. The template is as follows:

  ```
  {
      "Collect":
      [
          {"TypeName": "name", "Content":"value","Interval":"seconds", "Count":"count"}
      ]
  }
  ```

  ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**

  For details about the value ranges of **TypeName** and corresponding **Content**, see [Table 1](#info). The **Interval** and **Count** parameters do not take effect when **TypeName** is **Log**, **CoreDump**, **Config**, or **XLog**.

  The content of the default configuration file is as follows:

  ```
  {
   "Collect":
   [
   {"TypeName": "System", "Content":"RunTimeInfo, HardWareInfo","Interval":"0", "Count":"1"},
   {"TypeName": "Log", "Content" : "DataNode,ClusterManager", "Interval":"0", "Count":"1"},
   {"TypeName": "Database", "Content": "pg_locks,pg_stat_activity,pg_thread_wait_status","Interval":"0", "Count":"1"},
   {"TypeName": "Config", "Content": "DataNode", "Interval":"0", "Count":"1"}
   ]
   }
  ```

- -keyword=KEYWORD

  Specifies a log file containing the keyword.

  If the keyword contains spaces, quote the keyword with double quotations marks ("").

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Logs about the performance are binary logs. The keyword collection function does not support performance log collection.

- -begin-time

  Start time of the search time range. The input format: yyyymmdd hh:mm

- -end-time

  End time of the search time range. The input format: yyyymmdd hh:mm

- -speed-limit

  Specifies the log collection rate. The value is a non-negative integer in the unit of MB/s.

  This parameter is used to prevent high disk or network I/O from being generated during log collection. If database nodes are deployed on the same disk where the **$GAUSSLOG/$PGHOST** path is located, they may become faulty due to high I/O. The value of this parameter must not exceed 1⁄3 of the minimum value of the disk and network I/O in MogDB.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Examples

Run the following command to collect OS and log information:

```
gs_collector --begin-time="20180131 23:00" --end-time="20180201 20:00" -h plat1
Successfully parsed the configuration file.
create Dir.
Successfully create dir.
do system check interval 0 : count 1
Collecting OS information.
Successfully collected OS information.
do database check interval 0 : count 1
Collecting catalog statistics.
Successfully collected catalog statistics.
do log check interval 0 : count 1
Collecting Log files.
Successfully collected Log files.
do Config check 0:1
Collecting Config files.
Successfully collected Config files.
Collecting files.
Successfully collected files.
All results are stored in $PGHOST/collector_20200624_134541.tar.gz.
```

Run the following command to query the collected statistics:

```
tar -zxvf $PGHOST/collector_20200624_134541.tar.gz
collector_20200624_134541/
collector_20200624_134541/plat1.tar.gz
collector_20200624_134541/Detail.log
collector_20200624_134541/Summary.log
cd collector_20200624_134541
tar -zxvf plat1.tar.gz
plat1/
plat1/catalogfiles/
plat1/catalogfiles/gs_clean_20200624_134548283647.txt
plat1/catalogfiles/dn_6001_pg_locks_20200624_134547746215.csv
plat1/catalogfiles/dn_6001_pg_thread_wait_status_20200624_134548068716.csv
plat1/catalogfiles/dn_6001_pg_stat_activity_20200624_134547851117.csv
plat1/configfiles/
plat1/logfiles/
plat1/logfiles/log_20200624_134548540916.tar.gz
plat1/coreDumpfiles/
plat1/gstackfiles/
plat1/systemfiles/
plat1/systemfiles/OS_information_20200624_134542218134.txt
plat1/systemfiles/database_system_info_20200624_134546282006.txt
plat1/planSimulatorfiles/
plat1/xlogfiles/
cd plat1/logfiles/
tar -zxvf log_20200624_134548540916.tar.gz
./
./om/
./om/gs_preinstall-2020-06-24_113654.log
./om/gs_local-2020-06-24_113710.log
./om/gs_install-2020-06-24_113911.log
./om/gs_checkperf-2020-06-24_114311.log
./om/gs_collector-2020-06-24_114240.log
./om/gs_backup-2020-06-24_114101.log
./om/gs_om-2020-06-24_114052.log
./bin/
./bin/gs_initdb/
./bin/gs_initdb/gs_initdb-2020-06-24_114010-current.log
./bin/gs_guc/
./bin/gs_guc/gs_guc-2020-06-24_114033-current.log
./bin/gs_ctl/
./bin/gs_ctl/gs_ctl-2020-06-24_114041-current.log
./pg_log/
./pg_log/dn_6001/
./pg_log/dn_6001/postgresql-2020-06-24_114043.log
./pg_log/dn_6001/postgresql-2020-06-24_114330.log
./pg_log/dn_6001/postgresql-2020-06-24_114316.log
./gs_profile/
./gs_profile/dn_6001_6002/
./gs_profile/dn_6001_6002/postgresql-2020-06-24_114330.prf
./gs_profile/dn_6001_6002/postgresql-2020-06-24_114316.prf
./gs_profile/dn_6001_6002/postgresql-2020-06-24_114043.prf
```
