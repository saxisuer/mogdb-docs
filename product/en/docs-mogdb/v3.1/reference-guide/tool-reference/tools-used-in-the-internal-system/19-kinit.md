---
title: kinit
summary: kinit
author: Zhang Cuiping
date: 2021-06-07
---

# kinit

## Function

**kinit**, provided by the cluster authentication service Kerberos, is used for specific users to obtain and cache TGT capabilities.

## Parameter Description

The Kerberos tool is provided by an open-source third party. For details about the parameters, see the Kerberos official document at <https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>.
