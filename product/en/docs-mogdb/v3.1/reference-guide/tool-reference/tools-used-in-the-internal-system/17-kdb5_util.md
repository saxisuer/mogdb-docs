---
title: kdb5_util
summary: kdb5_util
author: Zhang Cuiping
date: 2021-06-07
---

# kdb5_util

## Function

**kdb5_util**, provided by the cluster authentication service Kerberos, is a database management tool used to create, destroy, import, and export databases.

## Parameter Description

The Kerberos tool is provided by an open-source third party. For details about the parameters, see the Kerberos official document at <https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>.
