---
title: krb5kdc
summary: krb5kdc
author: Zhang Cuiping
date: 2021-06-07
---

# krb5kdc

## Function

**krb5kdc**, provided by the cluster authentication service Kerberos, is used to provide column authentication and key management services.

## Parameter Description

The Kerberos tool is provided by an open-source third party. For details about the parameters, see the Kerberos official document at <https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>.
