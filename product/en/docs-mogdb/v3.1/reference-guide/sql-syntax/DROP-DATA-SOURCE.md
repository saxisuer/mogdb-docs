---
title: DROP DATA SOURCE
summary: DROP DATA SOURCE
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DATA SOURCE

## Function

**DROP DATA SOURCE** deletes a data source.

## Important Notes

Only an owner, system administrator, or initial user can delete a data source.

## Syntax

```ebnf+diagram
DropDataSource ::= DROP DATA SOURCE [IF EXISTS] src_name [CASCADE | RESTRICT];
```

## Parameter Description

- **src_name**

  Specifies the name of the data source to be deleted.

  Value range: a string. It must comply with the naming convention.

- **IF EXISTS**

  Reports a notice instead of an error if the specified data source does not exist.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects that depend on the data source.

  - **RESTRICT**: refuses to delete the data source if any objects depend on it. This is the default action.

    Currently, no objects depend on data sources. Therefore, **CASCADE** is equivalent to **RESTRICT**, and they are reserved to ensure backward compatibility.

## Examples

```sql
-- Create a data source.
MogDB=# CREATE DATA SOURCE ds_tst1;

-- Delete the data source.
MogDB=# DROP DATA SOURCE ds_tst1 CASCADE;
MogDB=# DROP DATA SOURCE IF EXISTS ds_tst1 RESTRICT;
```

## Helpful Links

[CREATE DATA SOURCE](CREATE-DATA-SOURCE.md) and [ALTER DATA SOURCE](ALTER-DATA-SOURCE.md)
