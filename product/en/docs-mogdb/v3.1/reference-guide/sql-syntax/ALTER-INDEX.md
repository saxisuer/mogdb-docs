---
title: ALTER INDEX
summary: ALTER INDEX
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER INDEX

## Function

**ALTER INDEX** modifies the definition of an existing index.

It has the following forms:

- IF EXISTS

  Sends a notice instead of an error if the specified index does not exist.

- RENAME TO

  Changes only the name of the index. The stored data is not affected.

- SET TABLESPACE

  This option changes the index tablespace to the specified tablespace and moves index-related data files to the new tablespace.

- SET ( { STORAGE_PARAMETER = value } [, …] )

  Changes one or more index-method-specific storage parameters of an index. Note that the index content will not be modified immediately by this statement. You may need to use **REINDEX** to recreate the index based on different parameters to achieve the expected effect.

- RESET ( { storage_parameter } [, …] )

  Resets one or more index-method-specific storage parameters of an index to the default value. Similar to the **SET** statement, **REINDEX** may be used to completely update the index.

- [ MODIFY PARTITION index_partition_name ] UNUSABLE

  Sets the indexes on a table or index partition to be unavailable.

- REBUILD [ PARTITION index_partition_name ]

  Rebuilds indexes on a table or an index partition.

- RENAME PARTITION

  Renames an index partition.

- MOVE PARTITION

  Modifies the tablespace to which an index partition belongs.

## Precautions

Only the index owner or a user who has the INDEX permission on the table where the index resides can run the **ALTER INDEX** command. The system administrator has this permission by default.

## Syntax

- Rename a table index.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      RENAME TO new_name;
  ```

- Change the tablespace to which a table index belongs.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      SET TABLESPACE tablespace_name;
  ```

- Modify the storage parameter of a table index.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      SET ( {storage_parameter = value} [, ... ] );
  ```

- Reset the storage parameter of a table index.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      RESET ( storage_parameter [, ... ] ) ;
  ```

- Set a table index or an index partition to be unavailable.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      [ MODIFY PARTITION index_partition_name ] UNUSABLE;
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The syntax cannot be used for column-store tables.

- Rebuild a table index or index partition.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX index_name
      REBUILD [ PARTITION index_partition_name ];
  ```

- Rename an index partition.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      RENAME PARTITION index_partition_name TO new_index_partition_name;
  ```

- Modify the tablespace to which an index partition belongs.

  ```ebnf+diagram
  AlterIndex ::= ALTER INDEX [ IF EXISTS ] index_name
      MOVE PARTITION index_partition_name TABLESPACE new_tablespace;
  ```

## Parameter Description

- **index_name**

  Specifies the index name to be modified.

- **new_name**

  Specifies the new name of the index.

  Value range: a string. It must comply with the naming convention rule.

- **tablespace_name**

  Specifies the tablespace name.

  Value range: an existing tablespace name

- **storage_parameter**

  Specifies the name of an index-method-specific parameter.

- **value**

  Specifies the new value for an index-method-specific storage parameter. This might be a number or a word depending on the parameter.

- **new_index_partition_name**

  Specifies the new name of the index partition.

- **index_partition_name**

  Specifies the name of an index partition.

- **new_tablespace**

  Specifies a new tablespace.

## Examples

See **Examples** in [CREATE INDEX](CREATE-INDEX.md).

## Helpful Links

[CREATE INDEX](CREATE-INDEX.md)，[DROP INDEX](DROP-INDEX.md)，[REINDEX](REINDEX.md)
