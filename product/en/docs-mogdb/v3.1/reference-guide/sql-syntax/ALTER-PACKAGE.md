---
title: ALTER PACKAGE
summary: ALTER PACKAGE
author: Guo Huan
date: 2022-05-16
---

# ALTER PACKAGE

## Function

**ALTER PACKAGE** alters the attributes of a package.

## Precautions

Currently, only users with the **ALTER PACKAGE OWNER** permission can run this command. The system administrator has this permission by default. The restrictions are as follows:

- The current user must be the owner of the package or the system administrator and a member of the new owner role.

## Syntax

- Change the owner of a package.

```ebnf+diagram
AlterPackage ::= ALTER PACKAGE package_name OWNER TO new_owner;
```

## Parameter Description

- **package_name**

  Specifies the name of the package to be modified.

  Value range: an existing package name. Only one package can be modified at a time.

- **new_owner**

  Specifies the new owner of a package. To change the owner of a package, the new owner must have the **CREATE** permission on the schema to which the package belongs.

  Value range: an existing user role.

## Examples

For details, see [CREATE PACKAGE](CREATE-PACKAGE.md).

## Helpful Links

[CREATE PACKAGE](CREATE-PACKAGE.md), [DROP PACKAGE](DROP-PACKAGE.md)