---
title: DROP TABLE
summary: DROP TABLE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TABLE

## Function

**DROP TABLE** deletes a table.

## Precautions

- **DROP TABLE** forcibly deletes the specified table and the indexes depending on the table. After the table is deleted, the functions and stored procedures that need to use this table cannot be executed. Deleting a partitioned table also deletes all partitions in the table.
- The owner of a table, users granted with the  **DROP**  permission on the table, or users granted with the  **DROP ANY TABLE**  permission can delete the specified table. The system administrator has the permission to delete the table by default.

## Syntax

```ebnf+diagram
DropTable ::= DROP TABLE [ IF EXISTS ]
    { [schema'.']table_name } [, ...] [ CASCADE | RESTRICT ] [ PURGE ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified table does not exist.

- **schema**

  Specifies the schema name.

- **table_name**

  Specifies the name of the table to be deleted.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects (such as views) that depend on the table.
  - **RESTRICT**: refuses to delete the table if any objects depend on it. This is the default action.

## Examples

See **Example** in **CREATE TABLE**.

## Helpful Links

[ALTER TABLE](ALTER-TABLE.md) and [CREATE TABLE](CREATE-TABLE.md)
