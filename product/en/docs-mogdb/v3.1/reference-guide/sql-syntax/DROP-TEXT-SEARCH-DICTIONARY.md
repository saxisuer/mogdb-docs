---
title: DROP TEXT SEARCH DICTIONARY
summary: DROP TEXT SEARCH DICTIONARY
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TEXT SEARCH DICTIONARY

## Function

**DROP TEXT SEARCH DICTIONARY** deletes a full-text retrieval dictionary.

## Precautions

- Predefined dictionaries do not support the **DROP** operations.
- Only the owner of a dictionary or a system administrator has the **DROP TEXT SEARCH DICTIONARY** permission.
- Execute **DROP…CASCADE** only when necessary because this operation will delete the text search configurations that use this dictionary.

## Syntax

```ebnf+diagram
DropTextSearchDictionary ::= DROP TEXT SEARCH DICTIONARY [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified full-text retrieval dictionary does not exist.

- **name**

  Specifies the name of the dictionary to be deleted. (If you do not specify a schema name, the dictionary in the current schema will be used.)

  Value range: an existing dictionary name

- **CASCADE**

  Automatically deletes the objects that depend on the full-text retrieval dictionary and other objects that depend on these objects.

  If any text search configuration uses the dictionary, the **DROP** statement will fail. You can add **CASCADE** to delete all text search configurations and dictionaries that use this dictionary.

- **RESTRICT**

  Refuses to delete the full-text retrieval dictionary if any object depends on it. This is the default action.

## Examples

```sql
-- Delete the english dictionary.
MogDB=# DROP TEXT SEARCH DICTIONARY english;
```

## Helpful Links

[ALTER TEXT SEARCH DICTIONARY](ALTER-TEXT-SEARCH-DICTIONARY.md) and [CREATE TEXT SEARCH DICTIONARY](CREATE-TEXT-SEARCH-DICTIONARY.md)
