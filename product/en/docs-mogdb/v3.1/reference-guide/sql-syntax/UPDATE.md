---
title: UPDATE
summary: UPDATE
author: Zhang Cuiping
date: 2021-05-18
---

# UPDATE

## Function

**UPDATE** updates data in a table. **UPDATE** changes the values of the specified columns in all rows that satisfy the condition. The **WHERE** clause clarifies conditions. The columns to be modified need to be mentioned in the **SET** clause; columns not explicitly modified retain their previous values.

## Precautions

- The owner of a table, users granted with the  **UPDATE**  permission on the table, or users granted with the  **UPDATE ANY TABLE**  permission can update data in the table. The system administrator has the permission to update data in the table by default.
- You must have the **SELECT** permission on all tables involved in the expressions or conditions.
- For column-store tables, the **RETURNING** clause is currently not supported.
- Column-store tables do not support non-deterministic update. If you update data in one row with multiple rows of data in a column-store table, an error will be reported.
- Memory space that records update operations in column-store tables is not reclaimed. You need to clean it by executing **VACUUM FULL table_name**.
- Currently, **UPDATE** cannot be used in column-store replication tables.

## Syntax

```ebnf+diagram
Update ::= UPDATE [/*+ plan_hint */] [ ONLY ] table_name [ partition_clause ] [ * ] [ [ AS ] alias ] SET {column_name = { expression | DEFAULT } |( column_name [, ...] ) = {( { expression | DEFAULT } [, ...] ) |sub_query }}[, ...] [ FROM from_list] [ WHERE condition ] [ RETURNING {* | {output_expression [ [ AS ] output_name ]} [, ...] }];
```

Where sub_query can be:

```ebnf+diagram
sub_query ::= SELECT [ ALL | DISTINCT [ ON ( expression [, ...] ) ] ]
{ * | {expression [ [ AS ] output_name ]} [, ...] }
[ FROM from_item [, ...] ]
[ WHERE condition ]
[ GROUP BY grouping_element [, ...] ]
[ HAVING condition [, ...] ]
```

## Parameter Description

- **plan_hint** clause

  Follows the **UPDATE** keyword in the **/\*+ \*/** format. It is used to optimize the plan of an **UPDATE** statement block. For details, see [Hint-based Tuning](../../performance-tuning/2-sql/9-hint-based-tuning.md). In each statement, only the first /\***+** _plan_hint _\*/ comment block takes effect as a hint. Multiple hints can be written.

- **table_name**

  Specifies the name (optionally schema-qualified) of the table to be updated.

  Value range: an existing table name

- **partition\_clause**

    Updates a specified partition.

    PARTITION \{ \( partition\_name \) | FOR \( partition\_value \[, ...\] \) \} |

    SUBPARTITION \{ \( subpartition\_name \) | FOR \( subpartition\_value \[, ...\] \) \}

    For details about the keywords, see  [SELECT](SELECT.md).

    For details, see  [CREATE TABLE SUBPARTITION](CREATE-TABLE-SUBPARTITION.md).

- **alias**

  Specifies a substitute name for the target table.

  Value range: a string. It must comply with the identifier naming convention rule.

- **column_name**

  Specifies the name of the column to be modified.

  You can refer to this column by specifying the target table alias and the column name. For example:

  UPDATE foo AS f SET f.col_name = 'namecol';

  Value range: an existing column

- **expression**

  Specifies a value assigned to a column or an expression that assigns the value.

- **DEFAULT**

  Specifies the default value of a column.

  The value is **NULL** if no specified default value has been assigned to it.

- **sub_query**

  Specifies a subquery.

  This statement can be executed to update a table with information for other tables in the same database. For details about clauses in the **SELECT** statement, see **SELECT**.

- **from_list**

  Specifies a list of table expressions, allowing columns from other tables to appear in the **WHERE** condition and the update expressions. This is similar to the list of tables that can be specified in the **FROM** clause of a **SELECT** statement.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  > Note that the target table must not appear in the **from_list**, unless you intend a self-join (in which case it must appear with an alias in the **from_list**).

- **condition**

  Specifies an expression that returns a value of type Boolean. Only rows for which this expression returns **true** are updated. You are not advised to use numeric types such as int for  **condition**, because such types can be implicitly converted to bool values \(non-zero values are implicitly converted to  **true**  and  **0**  is implicitly converted to  **false**\), which may cause unexpected results.

- **output_expression**

  Specifies an expression to be computed and returned by the **UPDATE** statement after each row is updated.

  Value range: The expression can use any column names of the table named by **table_name** or tables listed in **FROM**. Write * to return all columns.

- **output_name**

  Specifies a name to use for a returned column.

## Examples

```sql
-- Create a table student1.
MogDB=# CREATE TABLE student1
(
   stuno     int,
   classno   int
);

-- Insert data.
MogDB=# INSERT INTO student1 VALUES(1,1);
MogDB=# INSERT INTO student1 VALUES(2,2);
MogDB=# INSERT INTO student1 VALUES(3,3);

-- View data.
MogDB=# SELECT * FROM student1;

-- Update the values of all records.
MogDB=# UPDATE student1 SET classno = classno*2;

-- View data.
MogDB=# SELECT * FROM student1;

-- Delete the table.
MogDB=# DROP TABLE student1;
```
