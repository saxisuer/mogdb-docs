---
title: CREATE MODEL
summary: CREATE MODEL
author: Zhang Cuiping
date: 2021-11-01
---

# CREATE MODEL

## Function

**CREATE MODEL** trains a machine learning model and saves the model.

## Precautions

- The model name must be unique. Pay attention to the naming format.
- The AI training duration fluctuates greatly, and in some cases, the training duration is long. If the duration specified by the GUC parameter **statement_timeout** is too long, the training will be interrupted. You are advised to set **statement_timeout** to **0** so that the statement execution duration is not limited.

## Syntax

```ebnf+diagram
CreateModel ::= CREATE MODEL model_name USING algorithm_name
[FEATURES { {expression [ [ AS ] output_name ]} [, ...] }]
[TARGET { {expression [ [ AS ] output_name ]} [, ...] }]
FROM { table_name | select_query }
WITH hyperparameter_name = { hyperparameter_value | DEFAULT } [, ...]
```

## Parameter Description

- **model_name**

  Name of the training model, which must be unique.

  Value range: a string. It must comply with the identifier naming convention.

- **architecture_name**

  Algorithm type of the training model.

  Value range: a string. Currently, the value can be **logistic_regression**, **linear_regression**, **svm_classification**, or **kmeans**.

- **attribute_list**

  Enumerated input column name of the training model.

  Value range: a string. It must comply with the naming convention of data attributes.

- **attribute_name**

  Target column name of the retraining model in a supervised learning task (simple expression processing can be performed).

  Value range: a string. It must comply with the naming convention of data attributes.

- **subquery**

  Data source.

  Value range: a string. It must comply with the SQL syntax of databases.

- **hyper_parameter_name**

  Hyperparameter name of the machine learning model.

  Value range: a string. The value range varies depending on the algorithms.

- **hp_value**

  Hyperparameter value.

  Value range: a string. The value range varies depending on the algorithms.

## Examples

```sql
CREATE MODEL price_model USING logistic_regression
 FEATURES size, lot
 TARGET price
 FROM HOUSES
 (WITH learning_rate=0.88, max_iterations=default);
```

## Helpful Links

[DROP MODEL](DROP-MODEL.md) and [PREDICT BY](PREDICT-BY.md)
