---
title: ALTER TEXT SEARCH DICTIONARY
summary: ALTER TEXT SEARCH DICTIONARY
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER TEXT SEARCH DICTIONARY

## Function

**ALTER TEXT SEARCH DICTIONARY** modifies the definition of a full-text search dictionary, including its parameters, name, owner, and schema.

## Precautions

- Predefined dictionaries do not support the **ALTER** operations.
- Only the owner of a dictionary or a system administrator can perform the **ALTER** operations.
- After a dictionary is created or modified, any modification to the customized dictionary definition file in the **filepath** directory does not affect the dictionary in the database. To use these modifications in the database, run the **ALTER TEXT SEARCH DICTIONARY** statement to update the definition file of the corresponding dictionary.

## Syntax

- Modify the dictionary definition.

  ```ebnf+diagram
  AlterTextSearchDictionary ::= ALTER TEXT SEARCH DICTIONARY name (
      option [ = value ] [, ... ]
  );
  ```

- Rename a dictionary.

  ```ebnf+diagram
  AlterTextSearchDictionary ::= ALTER TEXT SEARCH DICTIONARY name RENAME TO new_name;
  ```

- Set the schema of the dictionary.

  ```ebnf+diagram
  AlterTextSearchDictionary ::= ALTER TEXT SEARCH DICTIONARY name SET SCHEMA new_schema;
  ```

- Change the owner of the dictionary.

  ```ebnf+diagram
  AlterTextSearchDictionary ::= ALTER TEXT SEARCH DICTIONARY name OWNER TO new_owner;
  ```

## Parameter Description

- **name**

  Specifies the name of an existing dictionary. (If you do not specify a schema name, the dictionary in the current schema will be used.)

  Value range: an existing dictionary name

- **option**

  Specifies the parameter name to be modified. Each type of dictionaries has a template containing their custom parameters. Parameters function in a way irrelevant to their setting sequence. For details about the parameters, see **option**.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - The value of **TEMPLATE** in the dictionary cannot be changed.
  > - To specify a dictionary, specify both the dictionary definition file path (**FILEPATH**) and the file name.
  > - The name of a dictionary definition file can contain only lowercase letters, digits, and underscores (_).

- **value**

  Specifies the new value of a parameter. If the equal sign (=) and *value* are omitted, the previous settings of the option are deleted and the default value is used.

  Value range: valid values defined by **option**.

- **new_name**

  Specifies the new name of a dictionary.

  Value range: a string, which complies with the identifier naming convention. A value can contain a maximum of 63 characters.

- **new_owner**

  Specifies the new owner of a dictionary.

  Value range: an existing username

- **new_schema**

  Specifies the new schema of a dictionary.

  Value range: an existing schema

## Examples

```sql
-- Modify the definition of stop words in Snowball dictionaries. Retain the values of other parameters.
MogDB=# ALTER TEXT SEARCH DICTIONARY my_dict ( StopWords = newrussian, FilePath = 'file:///home/dicts' );

-- Modify the Language parameter in Snowball dictionaries and delete the definition of stop words.
MogDB=# ALTER TEXT SEARCH DICTIONARY my_dict (Language = dutch, StopWords);

-- Update the dictionary definition and do not change any other content.
MogDB=# ALTER TEXT SEARCH DICTIONARY my_dict ( dummy );
```

## Helpful Links

[CREATE TEXT SEARCH DICTIONARY](CREATE-TEXT-SEARCH-DICTIONARY.md), [DROP TEXT SEARCH DICTIONARY](DROP-TEXT-SEARCH-DICTIONARY.md)
