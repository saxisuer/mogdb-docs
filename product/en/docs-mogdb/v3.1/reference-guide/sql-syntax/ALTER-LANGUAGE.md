---
title: ALTER LANGUAGE
summary: ALTER LANGUAGE
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER LANGUAGE

## Function

**ALTER LANGUAGE**  modifies the definition of a procedural language. A single-node system or centralized system does not support modifying procedural languages.

## Syntax

```ebnf+diagram
AlterLanguage ::= ALTER [ PROCEDURAL ] LANGUAGE name RENAME TO new_name ALTER [ PROCEDURAL ] LANGUAGE name OWNER TO new_owner;
```

## Parameter Description

- **name**

  Name of a language.

- **new_name**

  New name of a language.

- **new_owner**

  New owner of a language.

## Compatibility

The SQL standard does not contain the **ALTER LANGUAGE** statement.