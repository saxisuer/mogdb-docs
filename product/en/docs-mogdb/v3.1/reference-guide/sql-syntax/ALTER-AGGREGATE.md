---
title: ALTER AGGREGATE
summary: ALTER AGGREGATE
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER AGGREGATE

## Function

**ALTER AGGREGATE** modifies the definition of an aggregate function.

## Precautions

To use **ALTER AGGREGATE**, you must be the owner of the aggregate function. To change the schema of an aggregate function, you must have the **CREATE** permission on the new schema. To change the owner, you must be a direct or indirect member of the new role, and the role must have the **CREATE** permission on the aggregate function's schema. (This restricts the owner from doing anything except for deleting and recreating aggregate functions. However, a user with the SYSADMIN permission can change the ownership of an aggregate function in any way.)

## Syntax

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) RENAME TO new_name;
```

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) OWNER TO new_owner;
```

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) SET SCHEMA new_schema;
```

## Parameter Description

- **name**

  Name (optionally schema-qualified) of an existing aggregate function.

- **argtype**

  Input data type of the aggregate function. To reference a zero-parameter aggregate function, you can write an asterisk (*) instead of a list of input data types.

- **new_name**

  New name of the aggregate function.

- **new_owner**

  New owner of the aggregate function.

- **new_schema**

  New schema of the aggregate function.

## Examples

Rename the aggregate function **myavg** that accepts integer-type parameters to **my_average**.

```sql
ALTER AGGREGATE myavg(integer) RENAME TO my_average;
```

Change the owner of the aggregate function **myavg** that accepts integer-type parameters to **joe**.

```sql
ALTER AGGREGATE myavg(integer) OWNER TO joe;
```

Move the aggregate function **myavg** that accepts integer-type parameters to **myschema**.

```sql
ALTER AGGREGATE myavg(integer) SET SCHEMA myschema;
```

## Compatibility

The SQL standard does not contain the **ALTER AGGREGATE** statement.
