---
title: RESET
summary: RESET
author: Zhang Cuiping
date: 2021-05-18
---

# RESET

## Function

**RESET** restores run-time parameters to their default values. The default values are parameter default values complied in the **postgresql.conf** configuration file.

**RESET** is an alternative spelling for:

SET configuration_parameter TO DEFAULT

## Precautions

**RESET** and **SET** have the same transaction behavior. Their impact will be rolled back.

## Syntax

```ebnf+diagram
Reset ::= RESET {configuration_parameter | CURRENT_SCHEMA | TIME ZONE | TRANSACTION ISOLATION LEVEL | SESSION AUTHORIZATION | ALL };
```

## Parameter Description

- **configuration_parameter**

  Specifies the name of a settable run-time parameter.

  Value range: run-time parameters. You can view them by running the **SHOW ALL** statement.

- **CURRENT_SCHEMA**

  Specifies the current schema.

- **TIME ZONE**

  Specifies the time zone.

- **TRANSACTION ISOLATION LEVEL**

  Specifies the transaction isolation level.

- **SESSION AUTHORIZATION**

  Specifies the session authorization.

- **ALL**

  Resets all settable run-time parameters to default values.

## Examples

```sql
-- Reset timezone to the default value.
MogDB=# RESET timezone;

-- Set all parameters to their default values.
MogDB=# RESET ALL;
```

## Helpful Links

[SET](SET.md) and [SHOW](SHOW.md)
