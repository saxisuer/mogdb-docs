---
title: GRANT
summary: GRANT
author: Zhang Cuiping
date: 2021-05-18
---

# GRANT

## Function

**GRANT**  grants permissions to roles and users.

**GRANT**  is used in the following scenarios:

- **Granting system permissions to roles or users**

  System permissions are also called user attributes, including  **SYSADMIN**,  **CREATEDB**,  **CREATEROLE**,  **AUDITADMIN**,  **MONADMIN**,  **OPRADMIN**,  **POLADMIN**,  **INHERIT**,  **REPLICATION**,  **VCADMIN**, and  **LOGIN**.

  They can be specified only by the  **CREATE ROLE**  or  **ALTER ROLE**  statement. The  **SYSADMIN**  permissions can be granted and revoked using  **GRANT ALL PRIVILEGE**  and  **REVOKE ALL PRIVILEGE**, respectively. System permissions cannot be inherited by a user from a role, and cannot be granted using  **PUBLIC**.

- **Granting database object permissions to roles or users**

  Grant permissions on a database object \(table, view, column, database, function, schema, or tablespace\) to a role or user.

  **GRANT**  gives specific permissions on a database object to one or more roles. These permissions are added to those already granted, if any.

  The keyword  **PUBLIC**  indicates that the permissions are to be granted to all roles, including those that might be created later.  **PUBLIC**  can be thought of as an implicitly defined group that always includes all roles. Any particular role will have the sum of permissions granted directly to it, permissions granted to any role it is presently a member of, and permissions granted to  **PUBLIC**.

  If  **WITH GRANT OPTION**  is specified, the recipient of the permission can in turn grant it to others. Without a grant option, the recipient cannot do that. This option cannot be granted to  **PUBLIC**, which is a unique openGauss attribute.

  openGauss grants the permissions for objects of certain types to  **PUBLIC**. By default, permissions on tables, columns, sequences, foreign data sources, foreign servers, schemas, and tablespaces are not granted to  **PUBLIC**, but the following permissions are granted to  **PUBLIC**:  **CONNECT**  and  **CREATE TEMP TABLE**  permissions on databases,  **EXECUTE**  permission on functions, and  **USAGE**  permission on languages and data types \(including domains\). An object owner can revoke the default permissions granted to  **PUBLIC**  and grant permissions to other users as needed. For security purposes, you are advised to create an object and set its permissions in the same transaction so that other users do not have time windows to use the object. In addition, you can restrict the permissions of the  **PUBLIC**  user group by referring to t"Permission Management" in  _Security Hardening Guide_. These default permissions can be modified using the  **ALTER DEFAULT PRIVILEGES**  command.

  By default, an object owner has all permissions on the object. For security purposes, the owner can discard some permissions. However, the ALTER, DROP, COMMENT, INDEX, VACUUM, and re-grantable permissions of the object are inherent permissions implicitly owned by the owner.

- **Granting the permissions of one role or user to others**

  Grant the permissions of one role or user to others. In this case, every role or user can be regarded as a set of one or more database permissions.

  If  **WITH ADMIN OPTION**  is specified, the recipients can in turn grant the permissions to other roles or users or revoke the permissions they have granted to other roles or users. If recipients' permissions are changed or revoked later, the grantees' permissions will also change.

  Database administrators can grant or revoke permissions for any roles or users. Roles with the  **CREATEROLE**  permission can grant or revoke permissions for non-admin roles.

- **Granting ANY permissions to roles or users**

  Grant ANY permissions to a specified role or user. For details about the value range of the ANY permissions, see the syntax. If  **WITH ADMIN OPTION**  is specified, the grantee can grant the ANY permissions to or revoke them from other roles or users. The ANY permissions can be inherited by a role but cannot be granted to  **PUBLIC**. An initial user and the system administrator when separation of duties is disabled can grant the ANY permissions to or revoke them from any role or user.

  Currently, the following ANY permissions are supported:  **CREATE ANY TABLE**,  **ALTER ANY TABLE**,  **DROP ANY TABLE**,  **SELECT ANY TABLE**,  **INSERT ANY TABLE**,  **UPDATE ANY TABLE**,  **DELETE ANY TABLE**,  **CREATE ANY SEQUENCE**,  **CREATE ANY INDEX**,  **CREATE ANY FUNCTION**,  **EXECUTE ANY FUNCTION**,  **CREATE ANY PACKAGE**, **EXECUTE ANY PACKAGE**, and  **CREATE ANYTYPE**. For details about the ANY permission scope, see Table 1.

## Precautions

- It is not allowed to grant the ANY permissions to  **PUBLIC**  or revoke the ANY permissions from  **PUBLIC**.
- The ANY permissions are database permissions and are valid only for database objects that are granted with the permissions. For example,  **SELECT ANY TABLE**  only allows a user to view all user table data in the current database, but the user does not have the permission to view user tables in other databases.
- Even if a user is granted with the ANY permissions, the user cannot perform INSERT, DELETE, UPDATE, and SELECT operations on the objects of private users.
- The ANY permissions and the original permissions do not affect each other.
- If a user is granted with the  **CREATE ANY TABLE**  permission, the owner of a table created in a schema with the same name as the user is the creator of the schema. When the user performs other operations on the table, the user needs to be granted with the corresponding operation permission.

## Syntax

- Grant the table access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | ALTER | DROP | COMMENT | INDEX | VACUUM } [, ...]
        | ALL [ PRIVILEGES ] }
      ON { [ TABLE ] table_name [, ...]
         | ALL TABLES IN SCHEMA schema_name [, ...] }
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the column access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { {{ SELECT | INSERT | UPDATE | REFERENCES | COMMENT } ( column_name [, ...] )} [, ...]
        | ALL [ PRIVILEGES ] ( column_name [, ...] ) }
      ON [ TABLE ] table_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the sequence access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { SELECT | UPDATE | USAGE | ALTER | DROP | COMMENT } [, ...]
        | ALL [ PRIVILEGES ] }
      ON { [ [ LARGE ] SEQUENCE ] sequence_name [, ...]
         | ALL SEQUENCES IN SCHEMA schema_name [, ...] }
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the database access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { CREATE | CONNECT | TEMPORARY | TEMP | ALTER | DROP | COMMENT } [, ...]
        | ALL [ PRIVILEGES ] }
      ON DATABASE database_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the domain access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { USAGE | ALL [ PRIVILEGES ] }
      ON DOMAIN domain_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In the current version, the domain access permission cannot be granted.

- Grant the client master key (CMK) access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { USAGE | DROP } [, ...] | ALL [ PRIVILEGES ] }
      ON CLIENT_MASTER_KEY client_master_key [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the column encryption key (CEK) access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { USAGE | DROP } [, ...] | ALL [ PRIVILEGES ] }
      ON COLUMN_ENCRYPTION_KEY column_encryption_key [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the external data source access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { USAGE | ALL [ PRIVILEGES ] }
      ON FOREIGN DATA WRAPPER fdw_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the external server access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { USAGE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
      ON FOREIGN SERVER server_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the function access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { EXECUTE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
      ON { FUNCTION {function_name ( [ {[ argmode ] [ arg_name ] arg_type} [, ...] ] )} [, ...]
         | ALL FUNCTIONS IN SCHEMA schema_name [, ...] }
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the procedural procedure access permission to a user or role.

    ```ebnf+diagram
    Grant ::= GRANT { { EXECUTE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
        ON { PROCEDURE {proc_name ( [ {[ argmode ] [ arg_name ] arg_type} [, ...] ] )} [, ...]
           | ALL PROCEDURE IN SCHEMA schema_name [, ...] }
        TO { [ GROUP ] role_name | PUBLIC } [, ...]
        [ WITH GRANT OPTION ];
    ```

- Grant the procedural language access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { USAGE | ALL [ PRIVILEGES ] }
      ON LANGUAGE lang_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the large object access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { SELECT | UPDATE } [, ...] | ALL [ PRIVILEGES ] }
      ON LARGE OBJECT loid [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In the current version, the large object access permission cannot be granted.

- Grant the schema access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { CREATE | USAGE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
      ON SCHEMA schema_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** When granting table or view permissions to other users, you also need to grant the **USAGE** permission on the containing schema. Without the **USAGE** permission, the recipients can only see the object names, but cannot access them. This syntax cannot be used to grant the permission to create tables in schemas with the same name, but you can use the syntax for granting permission of a role to another user or role to achieve the same effect.

- Grant the tablespace access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { CREATE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
      ON TABLESPACE tablespace_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- Grant the type access permission to a specified user or role.

  ```ebnf+diagram
  Grant ::= GRANT { { USAGE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
      ON TYPE type_name [, ...]
      TO { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In the current version, the type access permission cannot be granted.

- Grant the data source permission to a role.

  ```ebnf+diagram
  Grant ::= GRANT { USAGE | ALL [PRIVILEGES]}
     ON DATA SOURCE src_name [, ...]
     TO { [GROUP] role_name | PUBLIC } [, ...]
     [WITH GRANT OPTION];
  ```

- Grant the directory permission to a role.

  ```ebnf+diagram
  Grant ::= GRANT { { READ | WRITE | ALTER | DROP } [, ...] | ALL [PRIVILEGES] }
     ON DIRECTORY directory_name [, ...]
     TO { [GROUP] role_name | PUBLIC } [, ...]
     [WITH GRANT OPTION];
  ```

- Grant the package permission to a role.

  ```ebnf+diagram
  Grant ::= GRANT { { EXECUTE | ALTER | DROP | COMMENT } [, ...] | ALL [PRIVILEGES] }
         ON { PACKAGE package_name [, ...]
            | ALL PACKAGES IN SCHEMA schema_name [, ...] }
         TO { [GROUP] role_name | PUBLIC } [, ...]
         [WITH GRANT OPTION];
  ```

- Grant a role's permissions to another user or role.

  ```ebnf+diagram
  Grant ::= GRANT role_name [, ...]
     TO role_name [, ...]
     [ WITH ADMIN OPTION ];
  ```

- Grant the **sysadmin** permission to a role.

  ```ebnf+diagram
  Grant ::= GRANT ALL { PRIVILEGES | PRIVILEGE }
     TO role_name;
  ```

- Grant the ANY permissions to another user or role.

    ```ebnf+diagram
    Grant ::= GRANT { CREATE ANY TABLE | ALTER ANY TABLE | DROP ANY TABLE | SELECT ANY TABLE | INSERT ANY TABLE | UPDATE ANY TABLE |
      DELETE ANY TABLE | CREATE ANY SEQUENCE | CREATE ANY INDEX | CREATE ANY FUNCTION | EXECUTE ANY FUNCTION |
      CREATE ANY PACKAGE | EXECUTE ANY PACKAGE | CREATE ANY TYPE } [, ...]
      TO [ GROUP ] role_name [, ...]
      [ WITH ADMIN OPTION ];
    ```

## Parameter Description

The possible permissions are:

- **SELECT**

  Allows **SELECT** from any column, or the specific columns listed, of the specified table, view, or sequence. The **SELECT** permission on the corresponding field is also required for **UPDATE** or DELETE.

- **INSERT**

  Allows **INSERT** of a new row into a table.

- **UPDATE**

  Allows **UPDATE** of any column of a table. Generally, **UPDATE** also requires the SELECT permission to query which rows need to be updated. **SELECT … FOR UPDATE** and **SELECT … FOR SHARE** also require this permission on at least one column, in addition to the **SELECT** permission.

- **DELETE**

  Allows **DELETE** of a row from a table. Generally, **DELETE** also requires the SELECT permission to query which rows need to be deleted.

- **TRUNCATE**

  Allows **TRUNCATE** on a table.

- **REFERENCES**

  Allows creation of a foreign key constraint referencing a table. This permission is required on both referencing and referenced tables.

- **CREATE**

  - For databases, allows new schemas to be created within the database.
  - For schemas, allows new objects to be created within the schema. To rename an existing object, you must own the object and have the **CREATE** permission on the schema of the object.
  - For tablespaces, allows tables to be created within the tablespace, and allows databases and schemas to be created that have the tablespace as their default tablespace.

- **CONNECT**

  Allows the grantee to connect to the database.

- **EXECUTE**

  Allows calling a function, including use of any operators that are implemented on top of the function.

- **USAGE**

  - For procedural languages, allows use of the language for the creation of functions in that language.
  - For schemas, allows access to objects contained in the schema. Without this permission, it is still possible to see the object names.
  - For sequences, allows use of the **nextval** function.
  - For data sources, specifies access permissions or is used as **ALL PRIVILEGES**.

- **ALTER**

  Allows users to modify properties of a specified object, excluding the owner and schema of the object.

- **DROP**

  Allows users to delete specified objects.

- **COMMENT**

  Allows users to define or modify comments of a specified object.

- **INDEX**

  Allows users to create indexes on specified tables, manage indexes on the tables, and perform REINDEX and CLUSTER operations on the tables.

- **VACUUM**

  Allows users to perform ANALYZE and VACUUM operations on specified tables.

- **ALL PRIVILEGES**

  Grants all available permissions to a user or role at a time. Only a system administrator has the **GRANT ALL PRIVILEGES** permission.

**GRANT** parameters are as follows:

- **role_name**

  Specifies the username.

- **table_name**

  Specifies the table name.

- **column_name**

  Specifies the column name.

- **schema_name**

  Specifies the schema name.

- **database_name**

  Specifies the database name.

- **function_name**

  Specifies the function name.

- **procedure\_name**

  Specifies the stored procedure name.

- **sequence_name**

  Specifies the sequence name.

- **domain_name**

  Specifies the domain type name.

- **fdw_name**

  Specifies the foreign data wrapper name.

- **lang_name**

  Specifies the language name.

- **type_name**

  Specifies the type name.

- **src_name**

  Specifies the data source name.

- **argmode**

  Specifies the parameter mode.

  Value range: a string. It must comply with the identifier naming convention.

- **arg_name**

  Parameter name.

  Value range: a string. It must comply with the identifier naming convention.

- **arg_type**

  Parameter type.

  Value range: a string. It must comply with the identifier naming convention.

- **loid**

  Specifies the identifier of the large object that includes this page.

  Value range: a string. It must comply with the identifier naming convention.

- **tablespace_name**

  Specifies the tablespace name.

- client_master_key

  Name of the client master key.

  Value range: a string. It must comply with the identifier naming convention.

- column_encryption_key

  Name of the column encryption key.

  Value range: a string. It must comply with the identifier naming convention.

- **directory_name**

  Specifies the name of the directory to be deleted.

  Value range: a string. It must comply with the identifier naming convention.

- **WITH GRANT OPTION**

  If **WITH GRANT OPTION** is specified, the recipient of the permission can in turn grant it to others. Without a grant option, the recipient cannot do that. Grant options cannot be granted to **PUBLIC**.

When a non-owner of an object attempts to GRANT permissions on the object:

- The statement will fail outright if the user has no permissions whatsoever on the object.
- As long as some permission is available, the statement will proceed, but it will grant only those permissions for which the user has grant options.
- The **GRANT ALL PRIVILEGES** forms will issue a warning message if no grant options are held, while the other forms will issue a warning if grant options for any of the permissions specifically named in the statement are not held.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Database administrators can access all objects, regardless of object permission settings. This is comparable to the permissions of **root** in a Unix system. As with **root**, it is unwise to operate as a system administrator except when necessary. **GRANT** to a table partition will cause alarms.

- WITH ADMIN OPTION

  If **WITH ADMIN OPTION** is specified for a role, the grantee can grant the role to other roles or users or revoke the role from other roles or users.

  For the ANY permissions, if **WITH ADMIN OPTION** is specified, the grantee can grant the ANY permissions to or revoke them from other roles or users.

**Table 1** ANY permissions

| System Permission    | Description                                                  |
| :------------------- | :----------------------------------------------------------- |
| CREATE ANY TABLE     | Users can create tables or views in the public and user schemas. The users must be granted with the permission to create sequences to create a table that contains serial columns. |
| ALTER ANY TABLE      | Users' **ALTER** permission on tables or views in the public and user schemas. If the users want to modify the unique index of a table to add a primary key constraint or unique constraint to the table, the users must be granted with the index permission for the table. |
| DROP ANY TABLE       | Users' **DROP** permission on tables or views in the public and user schemas. |
| SELECT ANY TABLE     | Users' **SELECT** permission on tables or views in the public and user schemas, which is still subject to row-level access control. |
| UPDATE ANY TABLE     | Users' **UPDATE** permission on tables or views in the public and user schemas, which is still subject to row-level access control. |
| INSERT ANY TABLE     | Users' **INSERT** permission on tables or views in the public and user schemas. |
| DELETE ANY TABLE     | Users' **DELETE** permission on tables or views in the public and user schemas, which is still subject to row-level access control. |
| CREATE ANY FUNCTION  | Users can create functions or stored procedures in the user schemas. |
| EXECUTE ANY FUNCTION | Users' **EXECUTE** permission on functions or stored procedures in the public and user schemas. |
| CREATE ANY PACKAGE   | Users can create packages in the public and user schemas.    |
| EXECUTE ANY PACKAGE  | Users' **EXECUTE** permission on packages in the public and user schemas. |
| CREATE ANY TYPE      | Users can create types in the public and user schemas.       |
| CREATE ANY SEQUENCE  | Users can create sequences in the public and user schemas.   |
| CREATE ANY INDEX     | Users can create indexes in the public and user schemas. The users must be granted with the permission to create tablespaces to create a partitioned table index in a tablespace. |

> **NOTE:** If a user is granted with any ANY permission, the user has the **USAGE** permission on the public and user schemas but does not have the **USAGE** permission on the system schemas except **public** listed in Table 1.

## Examples

**Example 1: Granting system permissions to a user or role**

Create user **joe** and grant the sysadmin permissions to the user.

```sql
MogDB=# CREATE USER joe PASSWORD 'Bigdata@123';
MogDB=# GRANT ALL PRIVILEGES TO joe;
```

Afterward, user **joe** has the sysadmin permissions.

**Example 2: Granting object permissions to a user or role**

1. Revoke the **sysadmin** permission from the **joe** user. Grant the usage permission of the **tpcds** schema and all permissions on the **tpcds.reason** table to **joe**.

   ```sql
   MogDB=# REVOKE ALL PRIVILEGES FROM joe;
   MogDB=# GRANT USAGE ON SCHEMA tpcds TO joe;
   MogDB=# GRANT ALL PRIVILEGES ON tpcds.reason TO joe;
   ```

   Then **joe** has all permissions on the **tpcds.reason** table, including create, retrieve, update, and delete.

2. Grant the retrieve permission of **r_reason_sk**, **r_reason_id**, and **r_reason_desc** columns and the update permission of the **r_reason_desc** column in the **tpcds.reason** table to **joe**.

   ```sql
   MogDB=# GRANT select (r_reason_sk,r_reason_id,r_reason_desc),update (r_reason_desc) ON tpcds.reason TO joe;
   ```

   Then **joe** has the retrieve permission of **r_reason_sk** and **r_reason_id** columns in the **tpcds.reason** table. To enable user **joe** to grant these permissions to other users, execute the following statement:

   ```sql
   MogDB=# GRANT select (r_reason_sk, r_reason_id) ON tpcds.reason TO joe WITH GRANT OPTION;
   ```

   Grant the **mogdb** database connection permission and schema creation permission to user **joe**, and enable this user to grant these permissions to other users.

   ```sql
   MogDB=# GRANT create,connect on database mogdb TO joe WITH GRANT OPTION;
   ```

   Create role **tpcds_manager**, grant the **tpcds** schema access permission and object creation permission to this role, but do not enable this role to grant these permissions to others.

   ```sql
   MogDB=# CREATE ROLE tpcds_manager PASSWORD 'Bigdata@123';
   MogDB=# GRANT USAGE,CREATE ON SCHEMA tpcds TO tpcds_manager;
   ```

   Grant all the permissions on the **tpcds_tbspc** tablespace to user **joe** but do not enable this user to grant these permissions to others.

   ```sql
   MogDB=# CREATE TABLESPACE tpcds_tbspc RELATIVE LOCATION 'tablespace/tablespace_1';
   MogDB=# GRANT ALL ON TABLESPACE tpcds_tbspc TO joe;
   ```

**Example 3: Granting the permissions of one user or role to others**

1. Create role **manager**, grant user **joe**'s permissions to the created role, and enable this role to grant its permissions to others.

   ```sql
   MogDB=# CREATE ROLE manager PASSWORD 'Bigdata@123';
   MogDB=# GRANT joe TO manager WITH ADMIN OPTION;
   ```

2. Create role **senior_manager** and grant **manager**'s permissions to **senior_manager**.

   ```sql
   MogDB=# CREATE ROLE senior_manager PASSWORD 'Bigdata@123';
   MogDB=# GRANT manager TO senior_manager;
   ```

3. Revoke permissions granted to **senior_manager** and **manager** and delete **manager**.

   ```sql
   MogDB=# REVOKE manager FROM joe;
   MogDB=# REVOKE senior_manager FROM manager;
   MogDB=# DROP USER manager;
   ```

**Example: Granting the CMK or CEK permission to other user or role**

1. Connect to an encrypted database.

   ```sql
   gsql -p 57101 mogdb -r -C
   MogDB=#  CREATE CLIENT MASTER KEY MyCMK1 WITH ( KEY_STORE = localkms , KEY_PATH = "key_path_value" , ALGORITHM = RSA_2048);
   CREATE CLIENT MASTER KEY
   MogDB=# CREATE COLUMN ENCRYPTION KEY MyCEK1 WITH VALUES (CLIENT_MASTER_KEY = MyCMK1, ALGORITHM = AEAD_AES_256_CBC_HMAC_SHA256);
   CREATE COLUMN ENCRYPTION KEY
   ```

2. Create a role **newuser** and grant the key permission to **newuser**.

   ```sql
   MogDB=# CREATE USER newuser PASSWORD 'Enmo@123';
   CREATE ROLE
   MogDB=# GRANT ALL ON SCHEMA public TO newuser;
   GRANT
   MogDB=# GRANT USAGE ON COLUMN_ENCRYPTION_KEY MyCEK1 to newuser;
   GRANT
   MogDB=# GRANT USAGE ON CLIENT_MASTER_KEY MyCMK1 to newuser;
   GRANT
   ```

3. Set the user to connect to a database and use a CEK to create an encrypted table.

   ```sql
   MogDB=# SET SESSION AUTHORIZATION newuser PASSWORD 'Enmo@123';
   MogDB=>  CREATE TABLE acltest1 (x int, x2 varchar(50) ENCRYPTED WITH (COLUMN_ENCRYPTION_KEY = MyCEK1, ENCRYPTION_TYPE = DETERMINISTIC));
   NOTICE:  The 'DISTRIBUTE BY' clause is not specified. Using 'x' as the distribution column by default.
   HINT:  Please use 'DISTRIBUTE BY' clause to specify suitable data distribution column.
   CREATE TABLE
   MogDB=> SELECT has_cek_privilege('newuser', 'MyCEK1', 'USAGE');
    has_cek_privilege
   -------------------
    t
   (1 row)
   ```

4. Revoke permissions and delete users.

   ```sql
   MogDB=# REVOKE USAGE ON COLUMN_ENCRYPTION_KEY MyCEK1 FROM newuser;
   MogDB=# REVOKE USAGE ON CLIENT_MASTER_KEY MyCMK1 FROM newuser;
   MogDB=# DROP TABLE newuser.acltest1;
   MogDB=# DROP COLUMN ENCRYPTION KEY MyCEK1;
   MogDB=# DROP CLIENT MASTER KEY MyCMK1;
   MogDB=# DROP SCHEMA IF EXISTS newuser CASCADE;
   MogDB=# REVOKE ALL ON SCHEMA public FROM newuser ;
   MogDB=# DROP ROLE IF EXISTS newuser;
   ```

**Example 4: Revoking permissions and deleting roles and users**

```sql
MogDB=# REVOKE ALL PRIVILEGES ON tpcds.reason FROM joe;
MogDB=# REVOKE ALL PRIVILEGES ON SCHEMA tpcds FROM joe;
MogDB=# REVOKE ALL ON TABLESPACE tpcds_tbspc FROM joe;
MogDB=# DROP TABLESPACE tpcds_tbspc;
MogDB=# REVOKE USAGE,CREATE ON SCHEMA tpcds FROM tpcds_manager;
MogDB=# DROP ROLE tpcds_manager;
MogDB=# DROP ROLE senior_manager;
MogDB=# DROP USER joe CASCADE;
```

## Helpful Links

[REVOKE](REVOKE.md) and [ALTER DEFAULT PRIVILEGES](ALTER-DEFAULT-PRIVILEGES.md)
