---
title: DROP MATERIALIZED VIEW
summary: DROP MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# DROP MATERIALIZED VIEW

## Function

**DROP MATERIALIZED VIEW** forcibly deletes an existing materialized view from the database.

## Precautions

Only the owner of a materialized view or a system administrator has the **DROP MATERIALIZED VIEW** permission.

## Syntax

```ebnf+diagram
DropMaterializedView ::= DROP MATERIALIZED VIEW [ IF EXISTS ] mv_name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified materialized view does not exist.

- **mv_name**

  Name of the materialized view to be deleted.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects that depend on the materialized view.
  - **RESTRICT**: refuses to delete the materialized view if any objects depend on it. This is the default action.

## Example

```sql
-- Delete the materialized view named my_mv.
MogDB=# DROP MATERIALIZED VIEW my_mv;
```

## Helpful Links

[ALTER MATERIALIZED VIEW](ALTER-MATERIALIZED-VIEW.md), [CREATE INCREMENTAL MATERIALIZED VIEW](CREATE-INCREMENTAL-MATERIALIZED-VIEW.md), [CREATE MATERIALIZED VIEW](CREATE-MATERIALIZED-VIEW.md), [CREATE TABLE](CREATE-TABLE.md), [REFRESH INCREMENTAL MATERIALIZED VIEW](REFRESH-INCREMENTAL-MATERIALIZED-VIEW.md), and [REFRESH MATERIALIZED VIEW](REFRESH-MATERIALIZED-VIEW.md)
