---
title: CREATE DIRECTORY
summary: CREATE DIRECTORY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE DIRECTORY

## Function

**CREATE DIRECTORY** creates a directory. The directory defines an alias for a path in the server file system and is used to store data files used by users.

## Precautions

- When enable_access_server_directory=off, only the initial user is allowed to create directory objects; when enable_access_server_directory=on, users with SYSADMIN permissions and users who inherit the built-in role gs_role_directory_create permissions can create directory objects.
- By default, the user who creates a directory has the read and write permissions on the directory.
- The default owner of a directory is the user who creates the directory.
- A directory cannot be created for the following paths:
  - The path contains special characters.
  - The path is a relative path.
  - The path is a symbolic link.
- The following validity check is performed during directory creation:
  - Check whether the path exists in the OS. If it does not exist, a message is displayed, indicating the potential risks.
  - Check whether the database initial user omm has the R/W/X permissions for the OS path. If the user does not have all the permissions, a message is displayed, indicating the potential risks.
- In MogDB, ensure that the path is the same on all the nodes. Otherwise, the path may fail to be found on some nodes when the directory is used.

## Syntax

```ebnf+diagram
CreateDirectory ::= CREATE [OR REPLACE] DIRECTORY directory_name
AS 'path_name';
```

## Parameter Description

- **directory_name**

  Specifies the name of a directory.

  Value range: a string. It must comply with the naming convention.

- **path_name**

  Specifies the OS path for which a directory is to be created.

  Value range: a valid OS path

## Examples

```sql
-- Create a directory.
MogDB=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';
```

## Helpful Links

[ALTER DIRECTORY](ALTER-DIRECTORY.md)，[DROP DIRECTORY](DROP-DIRECTORY.md)
