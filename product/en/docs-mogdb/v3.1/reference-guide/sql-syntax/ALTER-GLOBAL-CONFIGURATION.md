---
title: ALTER GLOBAL CONFIGURATION
summary: ALTER GLOBAL CONFIGURATION
author: Guo Huan
date: 2022-05-16
---

# ALTER GLOBAL CONFIGURATION

## Function

**ALTER GLOBAL CONFIGURATION** adds and modifies the **gs_global_config** system catalog and adds the value of **key-value**.

## Precautions

- Only the initial database user can run this command.
- The keyword cannot be changed to **weak_password**.

## Syntax

```ebnf+diagram
AlterGlobalConfiguration ::= ALTER GLOBAL CONFIGURATION with( { paraname = value } [, ...] );
```

## Parameter Description

- paraname

Parameter name, which is of the text type.

- value

Parameter value, which is of the text type.