---
title: PREDICT BY
summary: PREDICT BY
author: Zhang Cuiping
date: 2021-11-01
---

# PREDICT BY

## Function

**PREDICT BY** uses a trained model to perform inference tasks.

## Precautions

The name of the invoked model can be viewed in the **gs_model_warehouse** system catalog.

## Syntax

```ebnf+diagram
PredictBy ::= PREDICT BY model_name [ (FEATURES attribute [, attribute]) ]
```

## Parameter Description

- model_name

  Name of the model of a speculative task.

  Value range: a string. It must comply with the identifier naming convention.

- attribute

  Name of the input feature column of a speculative task.

  Value range: a string. It must comply with the identifier naming convention.

## Examples

```
SELECT id, PREDICT BY price_model (FEATURES size,lot), price
FROM houses;
```

## Helpful Links

[CREATE MODEL](CREATE-MODEL.md) and [DROP MODEL](DROP-MODEL.md)