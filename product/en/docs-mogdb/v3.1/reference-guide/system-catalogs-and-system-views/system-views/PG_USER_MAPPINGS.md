---
title: PG_USER_MAPPINGS
summary: PG_USER_MAPPINGS
author: Guo Huan
date: 2021-04-19
---

# PG_USER_MAPPINGS

**PG_USER_MAPPINGS** provides access to information about user mappings.

This is essentially a publicly readable view of PG_USER_MAPPING that leaves out the options column if the user has no rights to use it. Common users must be authorized to access this view.

**Table 1** PG_USER_MAPPINGS columns

| Name      | Type    | Reference                 | Description                                                  |
| :-------- | :------ | :------------------------ | :----------------------------------------------------------- |
| umid      | oid     | PG_USER_MAPPING.oid       | OID of the user mapping                                      |
| srvid     | oid     | PG_FOREIGN_SERVER.oid     | OID of the foreign server that contains the mapping          |
| srvname   | name    | PG_FOREIGN_SERVER.srvname | Name of the foreign server                                   |
| umuser    | oid     | PG_AUTHID.oid             | OID of the local role being mapped (**0** if the user mapping is public) |
| usename   | name    | -                         | Name of the local user to be mapped                          |
| umoptions | text[ ] | -                         | User mapping specific options. If the current user is the owner of the foreign server, the value is **keyword=value strings**. Otherwise, the value is **null**. |
