---
title: PG_PUBLICATION_TABLES
summary: PG_PUBLICATION_TABLES
author: Guo Huan
date: 2022-05-12
---

# PG_PUBLICATION_TABLES

**PG_PUBLICATION_TABLES** displays the mapping information between a publication and its published tables. Unlike the underlying system catalog **PG_PUBLICATION_REL**, this view expands publications defined as **FOR ALL TABLES** so that for such publications, there is one row for each eligible table.

**Table 1** PG_PUBLICATION_TABLES columns

| Name       | Type | Description                                 |
| :--------- | :--- | :------------------------------------------ |
| pubname    | name | Publication name.                           |
| schemaname | name | Name of the schema that contains the table. |
| tablename  | name | Table name.                                 |