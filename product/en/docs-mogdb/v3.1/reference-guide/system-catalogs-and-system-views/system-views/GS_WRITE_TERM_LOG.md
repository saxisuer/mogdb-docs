---
title: GS_WRITE_TERM_LOG
summary: GS_WRITE_TERM_LOG
author: Guo Huan
date: 2022-05-12
---

# GS_WRITE_TERM_LOG

**GS_WRITE_TERM_LOG** writes a log to record the current **term** value of the node. The standby node returns **false**. After the data is successfully written to the primary node, **true** is returned.