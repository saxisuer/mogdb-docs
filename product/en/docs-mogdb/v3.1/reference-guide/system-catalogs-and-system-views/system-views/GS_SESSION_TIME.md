---
title: GS_SESSION_TIME
summary: GS_SESSION_TIME
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_TIME

**GS_SESSION_TIME** collects statistics about the running time of session threads and time consumed in each execution phase.

**Table 1** GS_SESSION_TIME columns

| Name      | Type    | Description              |
| :-------- | :------ | :----------------------- |
| sessid    | text    | Thread ID and start time |
| stat_id   | integer | Statistics ID            |
| stat_name | text    | Session type             |
| value     | bigint  | Session value            |
