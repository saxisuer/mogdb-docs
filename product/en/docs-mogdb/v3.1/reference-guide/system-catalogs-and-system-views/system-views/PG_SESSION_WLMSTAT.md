---
title: PG_SESSION_WLMSTAT
summary: PG_SESSION_WLMSTAT
author: Guo Huan
date: 2022-05-13
---

# PG_SESSION_WLMSTAT

**PG_SESSION_WLMSTAT** displays corresponding load management information about the task currently executed by the user. Only the user with sysadmin permission can query this view.

**Table 1** PG_SESSION_WLMSTAT columns

| Name             | Type    | Description                                                  |
| :--------------- | :------ | :----------------------------------------------------------- |
| datid            | oid     | OID of the database this backend is connected to.            |
| datname          | name    | Name of the database the backend is connected to.            |
| threadid         | bigint  | ID of the backend thread.                                    |
| sessionid        | bigint  | Session ID.                                                  |
| processid        | integer | PID of the backend thread.                                   |
| usesysid         | oid     | OID of the user logged in to the backend.                    |
| appname          | text    | Name of the application connected to the backend.            |
| usename          | name    | Name of the user logged in to the backend.                   |
| priority         | bigint  | Priority of Cgroup where the statement is located.           |
| attribute        | text    | Attributes of the statement:<br/>- **Ordinary**: default attribute of a statement before it is parsed by the database.<br/>- **Simple**: simple statement.<br/>- **Complicated**: complicated statements.<br/>- **Internal**: internal statement of the database.<br/>- **Unknown**: unknown. |
| block_time       | bigint  | Pending duration of the statement by now, in seconds.        |
| elapsed_time     | bigint  | Actual execution duration of the statement by now, in seconds. |
| total_cpu_time   | bigint  | Total CPU usage duration of the statement on the database instances in the last period (unit: s). |
| cpu_skew_percent | integer | CPU usage skew percentage of the statement on the database instances in the last period. |
| statement_mem    | integer | **statement_mem** used for executing the statement (reserved column). |
| active_points    | integer | Number of concurrently active points occupied by the statement in the resource pool. |
| dop_value        | integer | DOP value obtained by the statement from the resource pool.  |
| control_group    | text    | Unsupported currently.                                       |
| status           | text    | Specifies the status of the statement, including:<br/>- **pending**: waiting to be executed.<br/>- **running**: being executed.<br/>- **finished**: finished normally. (If **enqueue** is set to **StoredProc** or **Transaction**, this state indicates that only certain jobs in the statement have been executed. This state persists until the finish of this statement.)<br/>- **aborted**: terminated unexpectedly.<br/>- **active**: normal status except for those above.<br/>- **unknown**: unknown status. |
| enqueue          | text    | Unsupported currently.                                       |
| resource_pool    | name    | Specifies the current resource pool where the statements are located. |
| query            | text    | Specifies the latest query at the backend. If **state** is **active**, this column shows the executing query. In all other states, it shows the last query that was executed. |
| is_plana         | boolean | Unsupported currently.                                       |
| node_group       | text    | Unsupported currently.                                       |
