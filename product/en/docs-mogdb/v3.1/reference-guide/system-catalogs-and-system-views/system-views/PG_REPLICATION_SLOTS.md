---
title: PG_REPLICATION_SLOTS
summary: PG_REPLICATION_SLOTS
author: Guo Huan
date: 2021-04-19
---

# PG_REPLICATION_SLOTS

**PG_REPLICATION_SLOTS** contains replication node information.

**Table 1** PG_REPLICATION_SLOTS columns

| Name          | Type    | Description                                                  |
| :------------ | :------ | :----------------------------------------------------------- |
| slot_name     | text    | Name of a replication node                                   |
| plugin        | text    | Name of the output plug-in corresponding to the logical replication slot |
| slot_type     | text    | Type of the replication node                                 |
| datoid        | oid     | OID of the database on the replication node                  |
| database      | name    | Name of the database on the replication node                 |
| active        | Boolean | Whether the replication node is active                       |
| xmin          | xid     | Transaction ID of the replication node                       |
| catalog_xmin  | xid     | ID of the earliest-decoded transaction corresponding to the logical replication slot |
| restart_lsn   | text    | XLOG file information on the replication node                |
| dummy_standby | Boolean | Whether the replication node is a dummy standby node         |
