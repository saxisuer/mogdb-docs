---
title: GS_LSC_MEMORY_DETAIL
summary: GS_LSC_MEMORY_DETAIL
author: Guo Huan
date: 2022-05-12
---

# GS_LSC_MEMORY_DETAIL

**GS_LSC_MEMORY_DETAIL** collects statistics about the local SysCache memory usage of all threads based on the MemoryContext node. The statistics are available only when GSC is enabled.

**Table 1** GS_LSC_MEMORY_DETAIL columns

| Name        | Type     | Description                                                  |
| :---------- | :------- | :----------------------------------------------------------- |
| threadid    | text     | Thread start time + thread ID (string: *timestamp*.*sessionid*) |
| tid         | bigint   | Thread ID                                                    |
| thrdtype    | text     | Thread type. It can be any thread type in the system, such as postgresql and wlmmonitor. |
| contextname | text     | Name of the memory context                                   |
| level       | smallint | Hierarchy of the memory context                              |
| parent      | text     | Name of the parent memory context                            |
| totalsize   | bigint   | Total size of the memory context, in bytes                   |
| freesize    | bigint   | Total size of released memory in the current memory context, in bytes |
| usedsize    | bigint   | Total size of used memory in the current memory context, in bytes |