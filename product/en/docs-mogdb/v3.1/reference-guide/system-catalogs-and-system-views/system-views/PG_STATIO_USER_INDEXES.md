---
title: PG_STATIO_USER_INDEXES
summary: PG_STATIO_USER_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_USER_INDEXES

**PG_STATIO_USER_INDEXES** displays I/O status information about all the user relationship table indexes in the namespace.

**Table 1** PG_STATIO_USER_INDEXES columns

| Name          | Type   | Description                                    |
| :------------ | :----- | :--------------------------------------------- |
| relid         | oid    | OID of the table that the index is created for |
| indexrelid    | oid    | OID of the index                               |
| schemaname    | name   | Name of the schema that the index is in        |
| relname       | name   | Name of the table for the index                |
| indexrelname  | name   | Index name                                     |
| idx_blks_read | bigint | Number of disk blocks read from the index      |
| idx_blks_hit  | bigint | Number of cache hits in the index              |
