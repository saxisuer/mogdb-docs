---
title: PG_STAT_ACTIVITY
summary: PG_STAT_ACTIVITY
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_ACTIVITY

**PG_STAT_ACTIVITY** shows information about the current user's queries. The columns save information about the last query.

**Table 1** PG_STAT_ACTIVITY columns

| Name             | Type                     | Description                                                  |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| datid            | oid                      | OID of the database that the user session connects to in the backend |
| datname          | name                     | Name of the database that the user session connects to in the backend |
| pid              | bigint                   | Thread ID of the backend                                     |
| sessionid        | bigint                   | Session ID                                                   |
| usesysid         | oid                      | OID of the user logged in to the backend                     |
| usename          | name                     | Name of the user logged in to the backend                    |
| application_name | text                     | Name of the application connected to the backend             |
| client_addr      | inet                     | IP address of the client connected to the backend. If this column is null, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as autovacuum. |
| client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of **client\_addr**. This column will be non-null only for IP connections and only when **log_hostname** is enabled. |
| client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
| backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
| xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active) If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
| query_start      | timestamp with time zone | Time when the currently active query was started, or if **state** is not **active**, when the last query was started |
| state_change     | timestamp with time zone | Time when the **state** was last changed                     |
| waiting          | boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true**. |
| enqueue          | text                     | Unsupported currently                                        |
| state            | text                     | Overall status of this backend. The value must be one of the following:<br/>- **active**: The backend is executing a query.<br/>- **idle**: The backend is waiting for a new client command.<br/>- **idle in transaction**: The backend is in a transaction, but there is no statement being executed in the transaction.<br/>- **idle in transaction** (aborted): The backend is in a transaction, but there are statements failed in the transaction.<br/>- **fastpath function call**: The backend is executing a fast-path function.<br/>- **disabled**: This state is reported if **track_activities** is disabled in this backend.<br/>NOTE:<br/>Common users can view their own session status only. The state information of other accounts is empty. For example, after user **judy** is connected to the database, the state information of user **joe** and the initial user omm in **pg_stat_activity** is empty.<br/>`SELECT datname, usename, usesysid, state,pid FROM pg_stat_activity;`<br/><br/>datname  \| usename \| usesysid \| state  \|       pid<br/>----+---+----+---+------<br/>postgres \| omm     \|       10 \|        \| 139968752121616<br/>postgres \| omm     \|       10 \|        \| 139968903116560<br/>db_tpcc \| judy    \|    16398 \| active \| 139968391403280<br/>postgres \| omm     \|       10 \|        \| 139968643069712<br/>postgres \| omm     \|       10 \|        \| 139968680818448<br/>postgres \| joe     \|    16390 \|        \| 139968563377936<br/>(6 rows) |
| resource_pool    | name                     | Resource pool used by the user                               |
| query_id         | bigint                   | ID of a query                                                |
| query            | text                     | Text of this backend's most recent query. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
| connection_info  | text                     | A string in JSON format recording the driver type, driver version, driver deployment path, and process owner of the connected database. |
| unique_sql_id    | bigint                   | Unique SQL statement ID.                                     |
| trace_id         | text                     | Driver-specific trace ID, which is associated with an application request. |
