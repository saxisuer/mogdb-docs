---
title: GS_SESSION_MEMORY_DETAIL
summary: GS_SESSION_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_MEMORY_DETAIL

**GS_SESSION_MEMORY_DETAIL** collects statistics about thread memory usage by MemoryContext node. When **enable_thread_pool** is set to **on**, this view contains memory usage of all threads and sessions. This view is unavailable when **enable_memory_limit** is set to **off**.

The memory context **TempSmallContextGroup** collects information about all memory contexts whose value in the **totalsize** column is less than 8192 bytes in the current thread, and the number of the collected memory contexts is recorded in the **usedsize** column. Therefore, the **totalsize** and **freesize** columns for **TempSmallContextGroup** in the view display the corresponding information about all the memory contexts whose value in the **totalsize** column is less than 8192 bytes in the current thread, and the **usedsize** column displays the number of these memory contexts.

You can run the **"select \* from gs_session_memctx_detail(threadid,");** statement to record information about all memory contexts of a thread into the **threadid*_*timestamp.log** file in the **/tmp/dumpmem** directory. **threadid** can be obtained from the following table.

**Table 1** GS_SESSION_MEMORY_DETAIL columns

| Name        | Type     | Description                                                  |
| :---------- | :------- | :----------------------------------------------------------- |
| sessid      | text     | Thread start time + thread ID (string:**timestamp*.*threadid**) |
| sesstype    | text     | Thread name                                                  |
| contextname | text     | Name of the memory context                                   |
| level       | smallint | Hierarchy of the memory context                              |
| parent      | text     | Name of the parent memory context                            |
| totalsize   | bigint   | Total size of the memory context, in bytes                   |
| freesize    | bigint   | Total size of released memory in the current memory context, in bytes |
| usedsize    | bigint   | Size of used memory in the memory context, in bytes. For **TempSmallContextGroup**, this parameter specifies the number of collected memory contexts. |
