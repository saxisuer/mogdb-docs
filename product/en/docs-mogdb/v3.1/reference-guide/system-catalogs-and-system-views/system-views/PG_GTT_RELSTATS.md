---
title: PG_GTT_RELSTATS
summary: PG_GTT_RELSTATS
author: Guo Huan
date: 2021-04-19
---

# PG_GTT_RELSTATS

**PG_GTT_RELSTATS** displays basic information about all global temporary tables of the current session by calling **pg_get_gtt_relstats**.

**Table 1** PG_GTT_RELSTATS columns

| Name          | Type    | Description                                                  |
| :------------ | :------ | :----------------------------------------------------------- |
| schemaname    | name    | Schema name.                                                 |
| tablename     | name    | Name of a global temporary table.                            |
| relfilenode   | oid     | File object ID.                                              |
| relpages      | integer | Number of disk pages of a global temporary table.            |
| reltuples     | real    | Number of records in a global temporary table.               |
| relallvisible | integer | Number of pages that are marked as all visible.              |
| relfrozenxid  | xid     | All transaction IDs before this one have been replaced with a permanent ("frozen") transaction ID in the table. |
| relminmxid    | xid     | This interface is reserved.                                  |
