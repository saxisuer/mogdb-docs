---
title: PG_AVAILABLE_EXTENSIONS
summary: PG_AVAILABLE_EXTENSIONS
author: Guo Huan
date: 2021-04-19
---

# PG_AVAILABLE_EXTENSIONS

**PG_AVAILABLE_EXTENSIONS** displays extended information about certain database features.

**Table 1** PG_AVAILABLE_EXTENSIONS columns

| Name              | Type | Description                                                  |
| :---------------- | :--- | :----------------------------------------------------------- |
| name              | name | Extension name                                               |
| default_version   | text | Name of the default version (**NULL** if none is specified)  |
| installed_version | text | Currently installed version of the extension (**NULL** if no version is installed) |
| comment           | text | Comment string from the extension's control file             |
