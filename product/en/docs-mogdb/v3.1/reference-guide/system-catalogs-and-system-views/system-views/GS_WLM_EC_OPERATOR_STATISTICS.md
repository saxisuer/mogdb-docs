---
title: GS_WLM_EC_OPERATOR_STATISTICS
summary: GS_WLM_EC_OPERATOR_STATISTICS
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_EC_OPERATOR_STATISTICS

**GS_WLM_EC_OPERATOR_STATISTICS** displays operators of the Extension Connector jobs that are being executed by the current user. Only users with the **sysadmin** permission can query this view.

**Table 1** GS_WLM_EC_OPERATOR_STATISTICS columns

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| queryid             | bigint                   | Internal query ID used for Extension Connector statement execution |
| plan_node_id        | integer                  | Plan node ID of the execution plan of an Extension Connector operator |
| start_time          | timestamp with time zone | Time when the Extension Connector operator starts to process the first data record |
| ec_status           | text                     | Status of an Extension Connector job<br/>- **EC_STATUS_INIT**: initialized<br/>- **EC_STATUS_CONNECTED**: connected<br/>- **EC_STATUS_EXECUTED**: executed<br/>- **EC_STATUS_FETCHING**: fetching<br/>- **EC_STATUS_END**: ended |
| ec_execute_datanode | text                     | Name of the DN executing an Extension Connector job          |
| ec_dsn              | text                     | DSN used by an Extension Connector job                       |
| ec_username         | text                     | Username used by an Extension Connector job to access a remote database instance (the value is null if the remote cluster type is SPARK.) |
| ec_query            | text                     | Statement sent by an Extension Connector job to a remote database instance |
| ec_libodbc_type     | text                     | Type of the unixODBC driver used by an Extension Connector job<br/>- Type 1: corresponds to **libodbc.so.1**.<br/>- Type 2: corresponds to **libodbc.so.2**. |
| ec_fetch_count      | bigint                   | Number of data records processed by an Extension Connector job |
