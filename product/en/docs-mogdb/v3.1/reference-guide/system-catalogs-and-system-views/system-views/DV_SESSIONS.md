---
title: DV_SESSIONS
summary: DV_SESSIONS
author: Guo Huan
date: 2022-05-12
---

# DV_SESSIONS

**DV_SESSIONS** displays all session information about the current session. By default, only the system administrator can access this view. Common users can access the view only after being authorized.

**Table 1** DV_SESSIONS columns

| Name     | Type    | Description                                                  |
| :------- | :------ | :----------------------------------------------------------- |
| sid      | bigint  | OID of the active backend thread of the current session      |
| serial#  | integer | Sequence number of the backend thread of the current activity, which is **0** in openGauss |
| user#    | oid     | OID of the user that has logged in to the backend thread. The OID is **0** if the backend thread is a global auxiliary thread. |
| username | name    | Name of the user logged in to the backend process. The value is null if the backend thread is a global auxiliary thread.<br/>**application_name** can be identified by associating with **pg_stat_get_activity()**.<br/>Example:<br/>`select s.*,a.application_name from DV_SESSIONS as s left join pg_stat_get_activity(NULL) as a on s.sid=a.sessionid;` |