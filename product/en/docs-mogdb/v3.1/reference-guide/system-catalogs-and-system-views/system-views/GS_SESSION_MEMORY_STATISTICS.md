---
title: GS_SESSION_MEMORY_STATISTICS
summary: GS_SESSION_MEMORY_STATISTICS
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_MEMORY_STATISTICS

**GS_SESSION_MEMORY_STATISTICS** displays load management information about memory usage of ongoing complex jobs executed by the current user.

**Table 1** GS_SESSION_MEMORY_STATISTICS columns

| Name            | Type                     | Description                                                  |
| :-------------- | :----------------------- | :----------------------------------------------------------- |
| datid           | oid                      | OID of the database that the backend is connected to         |
| usename         | name                     | Name of the user logged in to the backend                    |
| pid             | bigint                   | Thread ID of the backend                                     |
| start_time      | timestamp with time zone | Time when the statement starts to be executed                |
| min_peak_memory | integer                  | Minimum memory peak of the statement across the database nodes, in MB |
| max_peak_memory | integer                  | Maximum memory peak of the statement across the database nodes, in MB |
| spill_info      | text                     | Information about statement spill to the database nodes<br/>**None**: The statement has not been spilled to disks on the database nodes.<br/>**All**: The statement has been spilled to disks on the database nodes.<br/>**[a:b]**: The statement has been spilled to disks on *a* of *b* database nodes. |
| query           | text                     | Statement being executed                                     |
| node_group      | text                     | Unsupported currently                                        |
| top_mem_dn      | text                     | Memory usage                                                 |
