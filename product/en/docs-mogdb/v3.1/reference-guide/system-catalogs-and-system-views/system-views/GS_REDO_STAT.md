---
title: GS_REDO_STAT
summary: GS_REDO_STAT
author: Guo Huan
date: 2021-04-19
---

# GS_REDO_STAT

**GS_REDO_STAT** displays statistics on the replay of session thread logs.

**Table 1** GS_REDO_STAT columns

| Name      | Type   | Description                                                  |
| :-------- | :----- | :----------------------------------------------------------- |
| phywrts   | bigint | Number of times that data is written during log replay       |
| phyblkwrt | bigint | Number of data blocks written during log replay              |
| writetim  | bigint | Total time required for writing data during log replay       |
| avgiotim  | bigint | Average time required for writing data during log replay     |
| lstiotim  | bigint | Time consumed by the last data write operation during log replay |
| miniotim  | bigint | Minimum time consumed by a single data write operation during log replay |
| maxiowtm  | bigint | Maximum time consumed by a single data write operation during log replay |
