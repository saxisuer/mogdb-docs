---
title: PG_RLSPOLICIES
summary: PG_RLSPOLICIES
author: Guo Huan
date: 2021-04-19
---

# PG_RLSPOLICIES

**PG_RLSPOLICIES** contains row-level access control policies.

**Table 1** PG_RLSPOLICIES columns

| Name             | Type   | Description                                                  |
| :--------------- | :----- | :----------------------------------------------------------- |
| schemaname       | name   | Name of the schema of the table object to which a row-level access control policy is applied |
| tablename        | name   | Name of the table object to which the row-level access control policy is applied |
| policyname       | name   | Name of the row-level access control policy                  |
| policypermissive | text   | Attribute of the row-level access control policy             |
| policyroles      | name[] | List of users affected by the row-level access control policy. If this parameter is not specified, all users are affected. |
| policycmd        | text   | SQL operations affected by the row-level access control policy |
| policyqual       | text   | Expression of the row-level access control policy            |
