---
title: DV_SESSION_LONGOPS
summary: DV_SESSION_LONGOPS
author: Guo Huan
date: 2022-05-12
---

# DV_SESSION_LONGOPS

**DV_SESSION_LONGOPS** displays the progress of ongoing operations. Users can access this view only after being authorized.

**Table 1** DV_SESSION_LONGOPS columns

| Name      | Type    | Description                                                  |
| :-------- | :------ | :----------------------------------------------------------- |
| sid       | bigint  | OID of the running backend process                           |
| serial#   | integer | Sequence number of the running backend process, which is **0** in MogDB |
| sofar     | integer | Completed workload, which is null in MogDB                   |
| totalwork | integer | Total workload, which is null in MogDB                       |