---
title: PG_STAT_ACTIVITY_NG
summary: PG_STAT_ACTIVITY_NG
author: Guo Huan
date: 2022-05-12
---

# PG_STAT_ACTIVITY_NG

**PG_STAT_ACTIVITY_NG** displays information about all queries in the logical database instance of the current user.

**Table 1** PG_STAT_ACTIVITY_NG columns

| Name             | Type                     | Description                                                  |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| datid            | oid                      | OID of the database that the user session connects to in the backend |
| datname          | name                     | Name of the database that the user session connects to in the backend |
| pid              | bigint                   | ID of the backend thread                                     |
| sessionid        | bigint                   | Session ID                                                   |
| usesysid         | oid                      | OID of the user logged in to the backend                     |
| usename          | name                     | Name of the user logged in to the backend                    |
| application_name | text                     | Name of the application connected to the backend             |
| client_addr      | inet                     | IP address of the client connected to the backend If this column is null, it indicates either that the client is connected via a Unix socket on the server machine or this is an internal process such as autovacuum. |
| client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of client_addr. This column will be non-null only for IP connections and only when log_hostname is enabled. |
| client_port      | integer                  | TCP port number that the client uses for communication with the backend (**–1** if a Unix socket is used) |
| backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
| xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active). If the current query is the first of its transaction, this column is equal to the **query_start** column. |
| query_start      | timestamp with time zone | Time when the currently active query was started, or time when the last query was started if the value of **state** is not **active** |
| state_change     | timestamp with time zone | Time when **state** was last modified                        |
| waiting          | boolean                  | Whether the backend is currently waiting for a lock. If yes, the value is **true**. Otherwise, the value is **false**. |
| enqueue          | text                     | Queuing status of a statement. Possible values are:<br/>- **waiting in queue**: The statement is in the queue.<br/>- **Empty**: The statement is running. |
| state            | text                     | Overall status of the backend. Its value can be:<br/>- **active**: The backend is executing a query.<br/>- **idle**: The backend is waiting for a new client command.<br/>- **idle in transaction**: The backend is in a transaction, but there is no statement being executed in the transaction.<br/>- **idle in transaction** (aborted): The backend is in a transaction, but there are statements failed in the transaction.<br/>- **fastpath function call**: The backend is executing a fast-path function.<br/>- **disabled**: This state is reported if **track_activities** is disabled in this backend.<br/>NOTE:<br/>Common users can view status of their own sessions only. The state information of other accounts is empty. For example, after user **judy** is connected to the database, the state information of user **joe** and the initial user **omm** in **pg_stat_activity** is empty.<br/>- `SELECT datname, usename, usesysid, state,pid FROM pg_stat_activity_ng;`<br/>datname  \| usename \| usesysid \| state  \|       pid <br/> ———-+———+———-+——–+—————– <br/> postgres \| omm     \|       10 \|        \| 139968752121616<br/> postgres \| omm     \|       10 \|        \| 139968903116560<br/> db_tpcds \| judy    \|    16398 \| active \| 139968391403280<br/> postgres \| omm     \|       10 \|        \| 139968643069712<br/> postgres \| omm     \|       10 \|        \| 139968680818448<br/> postgres \| joe     \|    16390 \|        \| 139968563377936 <br/>(6 rows) |
| resource_pool    | name                     | Resource pool used by the user                               |
| query_id         | bigint                   | Query ID                                                     |
| query            | text                     | Latest query at the backend. If the value of **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
| node_group       | text                     | Logical database instance of the user to which the statement belongs. |
