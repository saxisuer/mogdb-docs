---
title: GS_WLM_SESSION_INFO_ALL
summary: GS_WLM_SESSION_INFO_ALL
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_SESSION_INFO_ALL

**GS_WLM_SESSION_INFO** displays load management information about a completed job executed on the database instance. Only the user with the **sysadmin** or **monitor admin** permission can query this view.

**Table 1** GS_WLM_SESSION_INFO_ALL columns

| Name                | Type     | Description                                                  |
| :------------------ | :------- | :----------------------------------------------------------- |
| userid              | oid      | OID of the user.                                             |
| username            | name     | Username.                                                    |
| sysadmin            | boolean  | Whether the user is the administrator.                       |
| rpoid               | oid      | OID of the associated resource pool.                         |
| respool             | name     | Name of the associated resource pool.                        |
| parentid            | oid      | OID of the user group.                                       |
| totalspace          | bigint   | Available space limit of the user.                           |
| spacelimit          | bigint   | User table space limit.                                      |
| childcount          | interger | Number of child users.                                       |
| childlist           | text     | Child user list.                                             |
| n_returned_rows     | bigint   | Number of rows in the result set returned by the **SELECT** statement. |
| n_tuples_fetched    | bigint   | Number of rows randomly scanned.                             |
| n_tuples_returned   | bigint   | Number of rows sequentially scanned.                         |
| n_tuples_inserted   | bigint   | Number of rows inserted.                                     |
| n_tuples_updated    | bigint   | Number of rows updated.                                      |
| n_tuples_deleted    | bigint   | Number of rows deleted.                                      |
| n_blocks_fetched    | bigint   | Number of buffer block access times.                         |
| n_blocks_hit        | bigint   | Number of buffer block hits.                                 |
| db_time             | bigint   | Valid DB time, which is accumulated if multiple threads are involved (unit: μs). |
| cpu_time            | bigint   | CPU time (unit: μs).                                         |
| execution_time      | bigint   | Execution time in the executor (unit: μs).                   |
| parse_time          | bigint   | SQL parsing time (unit: μs).                                 |
| plan_time           | bigint   | SQL plan generation time (unit: μs).                         |
| rewrite_time        | bigint   | SQL rewriting time (unit: μs).                               |
| pl_execution_time   | bigint   | Execution time of PL/pgSQL (unit: μs).                       |
| pl_compilation_time | bigint   | Compilation time of PL/pgSQL (unit: μs).                     |
| net_send_time       | bigint   | Network time (unit: μs).                                     |
| data_io_time        | bigint   | I/O time (unit: μs).                                         |
| is_slow_query       | bigint   | Whether the record is a slow SQL record.                     |