---
title: PG_INDEXES
summary: PG_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_INDEXES

**PG_INDEXES** provides access to useful information about each index in the database.

**Table 1** PG_INDEXES columns

| Name       | Type | Reference             | Description                                                 |
| :--------- | :--- | :-------------------- | :---------------------------------------------------------- |
| schemaname | name | PG_NAMESPACE.nspname  | Name of the schema that contains tables and indexes         |
| tablename  | name | PG_CLASS.relname      | Name of the table for which the index serves                |
| indexname  | name | PG_CLASS.relname      | Index name                                                  |
| tablespace | name | PG_TABLESPACE.nspname | Name of the tablespace that contains the index              |
| indexdef   | text | -                     | Index definition (a reconstructed **CREATE INDEX** command) |
