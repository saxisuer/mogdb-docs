---
title: PG_STAT_ALL_INDEXES
summary: PG_STAT_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_ALL_INDEXES

**PG_STAT_ALL_INDEXES** contains one row for each index in the current database, showing statistics about accesses to that specific index.

Indexes can be used via either simple index scans or "bitmap" index scans. In a bitmap scan the output of several indexes can be combined via AND or OR rules, so it is difficult to associate individual heap row fetches with specific indexes when a bitmap scan is used. Therefore, a bitmap scan increments the **pg_stat_all_indexes.idx_tup_read** count(s) for the index(es) it uses, and it increments the **pg_stat_all_tables.idx_tup_fetch** count for the table, but it does not affect **pg_stat_all_indexes.idx_tup_fetch**.

**Table 1** PG_STAT_ALL_INDEXES columns

| Name          | Type   | Description                                                  |
| :------------ | :----- | :----------------------------------------------------------- |
| relid         | oid    | OID of the table for this index                              |
| indexrelid    | oid    | OID of this index                                            |
| schemaname    | name   | Name of the schema that the index is in                      |
| relname       | name   | Name of the table for the index                              |
| indexrelname  | name   | Index name                                                   |
| idx_scan      | bigint | Number of index scans initiated on the index                 |
| idx_tup_read  | bigint | Number of index entries returned by scans on the index       |
| idx_tup_fetch | bigint | Number of live table rows fetched by simple index scans using the index |
