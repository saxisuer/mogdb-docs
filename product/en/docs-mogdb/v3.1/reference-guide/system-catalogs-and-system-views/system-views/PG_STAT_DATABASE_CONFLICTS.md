---
title: PG_STAT_DATABASE_CONFLICTS
summary: PG_STAT_DATABASE_CONFLICTS
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_DATABASE_CONFLICTS

**PG_STAT_DATABASE_CONFLICTS** displays statistics about database conflicts.

**Table 1** PG_STAT_DATABASE_CONFLICTS columns

| Name             | Type   | Description                       |
| :--------------- | :----- | :-------------------------------- |
| datid            | oid    | Database ID                       |
| datname          | name   | Database name                     |
| confl_tablespace | bigint | Number of conflicting tablespaces |
| confl_lock       | bigint | Number of conflicting locks       |
| confl_snapshot   | bigint | Number conflicting snapshots      |
| confl_bufferpin  | bigint | Number of conflicting buffers     |
| confl_deadlock   | bigint | Number of conflicting deadlocks   |
