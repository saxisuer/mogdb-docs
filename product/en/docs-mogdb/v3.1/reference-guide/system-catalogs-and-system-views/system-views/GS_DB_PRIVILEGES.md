---
title: GS_DB_PRIVILEGES
summary: GS_DB_PRIVILEGES
author: Guo Huan
date: 2022-05-12
---

# GS_DB_PRIVILEGES

**GS_DB_PRIVILEGES** displays the granting of ANY permissions. Each record corresponds to a piece of authorization information.

**Table 1** GS_DB_PRIVILEGES columns

| Name           | Type    | Description                                                  |
| :------------- | :------ | :----------------------------------------------------------- |
| rolename       | name    | Username.                                                    |
| privilege_type | text    | ANY permission of a user. For details about the value, see Table 1 in [GRANT](../../../reference-guide/sql-syntax/GRANT.md). |
| admin_option   | boolean | Whether the ANY permission recorded in the privilege_type column can be re-granted.<br/>- **yes**<br/>- **no** |