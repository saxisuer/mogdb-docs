---
title: PG_RUNNING_XACTS
summary: PG_RUNNING_XACTS
author: Guo Huan
date: 2022-05-13
---

# PG_RUNNING_XACTS

**PG_RUNNING_XACTS** displays the running transaction information on the current node.

**Table 1** PG_RUNNING_XACTS columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| handle      | integer | Handle corresponding to the transaction in GTM               |
| gxid        | xid     | Transaction ID                                               |
| state       | tinyint | Transaction status (**3**: prepared or **0**: starting)      |
| node        | text    | Node name                                                    |
| xmin        | xid     | Minimum transaction ID on the node                           |
| vacuum      | Boolean | Whether the current transaction is lazy vacuum<br/>- **t** (true): yes<br/>- **f** (false): no |
| timeline    | bigint  | Number of database restarts                                  |
| prepare_xid | xid     | Transaction ID in the **prepared** state. If the status is not **prepared**, the value is **0**. |
| pid         | bigint  | Thread ID corresponding to the transaction                   |
| next_xid    | xid     | Transaction ID sent from a CN to a DN                        |