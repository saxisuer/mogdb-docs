---
title: GS_ENCRYPTED_COLUMNS
summary: GS_ENCRYPTED_COLUMNS
author: Guo Huan
date: 2021-04-19
---

# GS_ENCRYPTED_COLUMNS

**GS_ENCRYPTED_COLUMNS** records information about encrypted columns in the encrypted equality feature. Each record corresponds to an encrypted column.

**Table 1** GS_ENCRYPTED_COLUMNS columns

| Name                   | Type      | Description                                                  |
| :--------------------- | :-------- | :----------------------------------------------------------- |
| oid                    | oid       | Row identifier (hidden column)                               |
| rel_id                 | oid       | Table OID                                                    |
| column_name            | name      | Name of an encrypted column.                                 |
| column_key_id          | oid       | A foreign key, which is the CEK OID                          |
| encryption_type        | int1      | Encryption type. The value can be **2 (DETERMINISTIC)** or **1 (RANDOMIZED)**. |
| data_type_original_oid | oid       | ID of the original data type of the encrypted column. For details about the value, see **oid** in the [PG_TYPE](PG_TYPE.md) system catalog. |
| data_type_original_mod | int4      | Original data type modifier of the encrypted column. For details about the value, see **atttypmod** in the [PG_ATTRIBUTE](PG_ATTRIBUTE.md) system catalog. The value of **data\_type\_original\_mod** is generally **-1** when data types are not specific. |
| create_date            | timestamp | Time when an encrypted column is created                     |
