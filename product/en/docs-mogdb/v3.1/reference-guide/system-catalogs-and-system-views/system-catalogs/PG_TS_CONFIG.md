---
title: PG_TS_CONFIG
summary: PG_TS_CONFIG
author: Guo Huan
date: 2021-04-19
---

# PG_TS_CONFIG

**PG_TS_CONFIG** contains entries representing text search configurations. A configuration specifies a particular text search parser and a list of dictionaries to use for each of the parser's output token types.

The parser is shown in the **PG_TS_CONFIG** entry, but the token-to-dictionary mapping is defined by subsidiary entries in PG_TS_CONFIG_MAP.

**Table 1** PG_TS_CONFIG columns

| Name         | Type   | Reference        | Description                                                |
| :----------- | :----- | :--------------- | :--------------------------------------------------------- |
| oid          | oid    | -                | Row identifier (hidden attribute, which must be specified) |
| cfgname      | name   | -                | Text search configuration name                             |
| cfgnamespace | oid    | PG_NAMESPACE.oid | OID of the namespace that contains the configuration       |
| cfgowner     | oid    | PG_AUTHID.oid    | Owner of the configuration                                 |
| cfgparser    | oid    | PG_TS_PARSER.oid | OID of the text search parser for this configuration       |
| cfoptions    | text[] | -                | Configuration options                                      |
