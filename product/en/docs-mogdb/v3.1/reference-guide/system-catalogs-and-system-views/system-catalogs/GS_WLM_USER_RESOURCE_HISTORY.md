---
title: GS_WLM_USER_RESOURCE_HISTORY
summary: GS_WLM_USER_RESOURCE_HISTORY
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_USER_RESOURCE_HISTORY

**GS_WLM_USER_RESOURCE_HISTORY** stores information about resources used by users. Each record in this system catalog indicates resource usage of a user at a time point, including the memory, number of CPU cores, storage space, temporary space, operator flushing space, logical I/O traffic, number of logical I/O operations, and logical I/O rate. The memory, CPU, and I/O monitoring items record only resource usage of complex jobs. For I/O monitoring items, this parameter is valid only when **enable_logical_io_statistics** is set to **on**. The function of saving user monitoring data is enabled only when **enable_user_metric_persistent** is set to **on**. Data in the system catalog **GS_WLM_USER_RESOURCE_HISTORY** comes from the **PG_TOTAL_USER_RESOURCE_INFO** view. This system catalog can be queried by users with the **sysadmin** permission only in Postgres.

**Table 1** GS_WLM_USER_RESOURCE_HISTORY

| Name              | Type                     | Description                                                  |
| :---------------- | :----------------------- | :----------------------------------------------------------- |
| username          | text                     | Username                                                     |
| timestamp         | timestamp with time zone | Timestamp                                                    |
| used_memory       | integer                  | Size of the memory being used, in MB                         |
| total_memory      | integer                  | Available memory, in MB The value **0** indicates that the available memory is not limited and depends on the maximum memory available in the database. |
| used_cpu          | real                     | Number of CPU cores in use                                   |
| total_cpu         | integer                  | Total number of CPU cores of the Cgroup associated with the user on the node |
| used_space        | bigint                   | Used storage space, in KB                                    |
| total_space       | bigint                   | Available storage space (unit: KB). The value **–1** indicates that the space is not limited. |
| used_temp_space   | bigint                   | Used temporary storage space, in KB                          |
| total_temp_space  | bigint                   | Available temporary storage space, in KB. The value **–1** indicates that the maximum temporary storage space is not limited. |
| used_spill_space  | bigint                   | Used space of operator flushing, in KB                       |
| total_spill_space | bigint                   | Available storage space for operator flushing, in KB. The value **–1** indicates that the maximum operator flushing space is not limited. |
| read_kbytes       | bigint                   | Byte traffic of read operations in a monitoring period, in KB |
| write_kbytes      | bigint                   | Byte traffic of write operations in a monitoring period, in KB |
| read_counts       | bigint                   | Number of read operations in a monitoring period             |
| write_counts      | bigint                   | Number of write operations in a monitoring period            |
| read_speed        | real                     | Byte rate of read operations in a monitoring period, in KB/s |
| write_speed       | real                     | Byte rate of write operations in a monitoring period, in KB/s |