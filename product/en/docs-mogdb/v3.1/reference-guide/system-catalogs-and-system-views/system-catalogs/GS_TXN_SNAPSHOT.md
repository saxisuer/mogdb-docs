---
title: GS_TXN_SNAPSHOT
summary: GS_TXN_SNAPSHOT
author: Zhang Cuiping
date: 2021-10-11
---

# GS_TXN_SNAPSHOT

**GS_TXN_SNAPSHOT** is a timestamp-CSN mapping table. It periodically samples and maintains an appropriate time range to estimate the CSN value corresponding to the timestamp in the range.

**Table 1** GS_TXN_SNAPSHOT columns

| Name        | Data Type   | Description                            |
| :---------- | :---------- | :------------------------------------- |
| snptime     | timestamptz | Snapshot time                          |
| snpxmin     | bigint      | Minimum transaction ID snapshots       |
| snpcsn      | bigint      | Commit sequence number (CSN) snapshots |
| snpsnapshot | TEXT        | Serialized snapshot text               |
