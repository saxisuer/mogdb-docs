---
title: PGXC_CLASS
summary: PGXC_CLASS
author: Guo Huan
date: 2022-05-13
---

# PGXC_CLASS

**PGXC_CLASS** records the replicated or distributed information for each table. The **PGXC_CLASS** system catalog has specific meanings only in distributed scenarios. MogDB can only query the table definition.

**Table 1** PGXC_CLASS columns

| Name            | Type             | Description                                                  |
| :-------------- | :--------------- | :----------------------------------------------------------- |
| pcrelid         | oid              | Table OID                                                    |
| pclocatortype   | “char”           | Locator type<br/>- **H**: Hash<br/>- **G**: Range<br/>- **L**: List<br/>- **M**: Modulo<br/>- **N**: Round Robin<br/>- **R**: Replication |
| pchashalgorithm | smallint         | Distributed tuple using the hash algorithm                   |
| pchashbuckets   | smallint         | Value of a hash container                                    |
| pgroup          | name             | Name of the node                                             |
| redistributed   | “char”           | Indicates that a table has been redistributed.               |
| redis_order     | integer          | Redistribution sequence. Tables whose values are **0** will not be redistributed in this round of redistribution. |
| pcattnum        | int2vector       | Column number used as a distributed key                      |
| nodeoids        | oidvector_extend | List of distributed table node OIDs                          |
| options         | text             | Extension status information. This is a reserved column in the system. |
