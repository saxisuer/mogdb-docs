---
title: PG_EXTENSION_DATA_SOURCE
summary: PG_EXTENSION_DATA_SOURCE
author: Guo Huan
date: 2021-04-19
---

# PG_EXTENSION_DATA_SOURCE

**PG_EXTENSION_DATA_SOURCE** records information about external data sources. An external data source contains information about an external database, such as its password encoding. It is mainly used with Extension Connector.

**Table 1** PG_EXTENSION_DATA_SOURCE columns

| Name       | Type      | Reference     | Description                                                  |
| :--------- | :-------- | :------------ | :----------------------------------------------------------- |
| oid        | oid       | -             | Row identifier (hidden attribute, which must be specified)   |
| srcname    | name      | -             | Name of an external data source                              |
| srcowner   | oid       | PG_AUTHID.oid | Owner of a foreign data source                               |
| srctype    | text      | -             | Type of an external data source. It is **NULL** by default.  |
| srcversion | text      | -             | Version of an external data source. It is **NULL** by default. |
| srcacl     | aclitem[] | -             | Access permissions.                                          |
| srcoptions | text[]    | -             | Option used for foreign data sources, expressed in a string in the format of keyword=value |
