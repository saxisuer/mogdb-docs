---
title: GS_ENCRYPTED_PROC
summary: GS_ENCRYPTED_PROC
author: Zhang Cuiping
date: 2021-10-11
---

# GS_ENCRYPTED_PROC

**GS_ENCRYPTED_PROC** provides information such as the parameters of encrypted functions and stored procedure functions, original data type of return values, and encrypted columns.

**Table 1** GS_ENCRYPTED_PROC columns

| Name                | Type      | Description                                                  |
| :------------------ | :-------- | :----------------------------------------------------------- |
| oid                 | oid       | Row identifier (hidden column)                               |
| func_id             | oid       | OID of the function, corresponding to the OID row identifier in the **pg_proc** system catalog |
| prorettype_orig     | int4      | Original data type of the return value                       |
| proargcachedcol     | oidvector | OID of the encrypted column corresponding to the **INPUT** parameter of the function, corresponding to the OID row identifier in the **gs_encrypted_columns** system catalog |
| proallargtypes_orig | oid[]     | Original data type of all function parameters                |