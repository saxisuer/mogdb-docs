---
title: PG_JOB
summary: PG_JOB
author: Guo Huan
date: 2021-04-19
---

# PG_JOB

**PG_JOB** records detailed information about jobs created by users. Dedicated threads poll the system catalog **PG_JOB** and trigger jobs based on scheduled job execution time, and update job status in **PG_JOB**. This system catalog belongs to the Shared Relation category. All job records are visible to all databases.

**Table 1** PG_JOB columns

| Name                 | Type                        | Description                                                  |
| :------------------- | :-------------------------- | :----------------------------------------------------------- |
| oid                  | oid                         | Row identifier (hidden attribute, which must be specified)   |
| job_id               | bigint                      | Job ID, primary key, unique (with a unique index)            |
| current_postgres_pid | bigint                      | postgres thread ID of the job if the job has been executed. The default value is **-1**, indicating that the job has not yet been executed. |
| log_user             | name                        | Username of the job creator                                  |
| priv_user            | name                        | Username of the job executor                                 |
| dbname               | name                        | Name of the database in which the job will be executed       |
| node_name            | name                        | Primary database node on which the job will be executed      |
| job_status           | "char"                      | Status of the job. The value can be **r**, **s**, **f**, or **d**. The default value is **s**. The indications are as follows:<br/>Status of job step: r=running, s=successfully finished, f=job failed, d=disable<br/>If a job fails to be executed for 16 consecutive times, **job\_status** is automatically set to **d**, and no more attempt will be made on this job.<br/>Note: When you disable a scheduled task (by setting **job_queue_processes** to **0**), the thread that monitors the job execution is not started, and the job status will not be updated. You can ignore this status. Only when the scheduled task function is enabled (**job_queue_processes** is not **0**), the system updates the value of this column based on the real-time job status. |
| start_date           | timestamp without time zone | Start time of the first job execution, accurate to millisecond |
| next_run_date        | timestamp without time zone | Scheduled time of the next job execution, accurate to millisecond |
| failure_count        | smallint                    | Number of times the job has started and failed. If a job fails to be executed for 16 consecutive times, no more attempt will be made on it. |
| interval             | text                        | Job execution interval                                       |
| last_start_date      | timestamp without time zone | Start time of the last job execution, accurate to millisecond |
| last_end_date        | timestamp without time zone | End time of the last job execution, accurate to millisecond  |
| last_suc_date        | timestamp without time zone | Start time of the last successful job execution, accurate to millisecond |
| this_run_date        | timestamp without time zone | Start time of the ongoing job execution, accurate to millisecond |
| nspname              | name                        | Name of the schema used for job execution                    |
| job_name             | text                        | Name of the DBE_SCHEDULER scheduled task.                    |
| end_date             | timestamp without time zone | Expiration time of the DBE_SCHEDULER scheduled task, accurate to millisecond. |
| enable               | boolean                     | The DBE_SCHEDULER scheduled task enabling status. The options are as follows:<br/>- **true**: enabled<br/>- **false**: disabled |
| failure_msg          | text                        | Error information about the latest task execution.           |
