---
title: PG_ENUM
summary: PG_ENUM
author: Guo Huan
date: 2021-04-19
---

# PG_ENUM

**PG_ENUM** contains entries showing the values and labels for each enumerated type. The internal representation of a given enumerated value is actually the OID of its associated row in **PG_ENUM**.

**Table 1** PG_ENUM columns

| Name          | Type | Reference   | Description                                                  |
| :------------ | :--- | :---------- | :----------------------------------------------------------- |
| oid           | oid  | -           | Row identifier (hidden attribute, which must be specified)   |
| enumtypid     | oid  | PG_TYPE.oid | OID of the **PG_TYPE** entry owning this enumerated value    |
| enumsortorder | real | -           | Sort position of this enumerated value within its enumerated type |
| enumlabel     | name | -           | Textual label for this enumerated value                      |

The OIDs for **PG_ENUM** rows follow a special rule: even-numbered OIDs are guaranteed to be ordered in the same way as the sort ordering of their enumerated type. If two even OIDs belong to the same enumerated type, the smaller OID must have the smaller **enumsortorder** value. Odd-numbered OID values need bear no relationship to the sort order. This rule allows the enumerated comparison routines to avoid catalog lookups in many common cases. The routines that create and alter enumerated types attempt to assign even OIDs to enumerated values whenever possible.

When an enumerated type is created, its members are assigned sort-order positions from 1 to *n*. However, members added later might be given negative or fractional values of **enumsortorder**. The only requirement on these values is that they be correctly ordered and unique within each enumerated type.
