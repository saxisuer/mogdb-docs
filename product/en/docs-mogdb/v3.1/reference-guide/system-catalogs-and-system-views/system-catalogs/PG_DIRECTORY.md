---
title: PG_DIRECTORY
summary: PG_DIRECTORY
author: Guo Huan
date: 2021-04-19
---

# PG_DIRECTORY

**PG_DIRECTORY** stores directories added by users. You can run the **CREATE DIRECTORY** statement to add directories to the system catalog. Currently, only system administrators can perform this operation.

**Table 1** PG_DIRECTORY columns

| Name    | Type      | Description                                                |
| :------ | :-------- | :--------------------------------------------------------- |
| oid     | oid       | Row identifier (hidden attribute, which must be specified) |
| dirname | name      | Name of a directory object                                 |
| owner   | oid       | Owner of a directory object                                |
| dirpath | text      | Directory path.                                            |
| diracl  | aclitem[] | Access permissions.                                        |
