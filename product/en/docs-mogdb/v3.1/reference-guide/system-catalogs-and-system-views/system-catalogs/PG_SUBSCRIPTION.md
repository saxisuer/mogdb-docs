---
title: PG_SUBSCRIPTION
summary: PG_SUBSCRIPTION
author: Guo Huan
date: 2022-05-12
---

# PG_SUBSCRIPTION

**PG_SUBSCRIPTION** contains all existing logical replication subscriptions. This system catalog is accessible only to system administrators.

Unlike most system catalogs, **PG_SUBSCRIPTION** is shared across all databases in a database instance. Each database instance has only one copy, not one copy per database.

**Table 1** PG_SUBSCRIPTION columns

| Name          | Type | Description                                                  |
| :------------ | :--- | :----------------------------------------------------------- |
| subdbid       | oid  | OID of the database where the subscription is located.       |
| subname       | text | Subscription name.                                           |
| subowner      | oid  | Subscription owner.                                          |
| subenabled    | bool | If true, the subscription is enabled and should be replicated. |
| subconninfo   | text | Information about the connection to the publishing database. |
| subslotname   | text | Name of the replication slot in the publishing database. If this parameter is left empty, the value is **NONE**. |
| subsynccommit | text | Value of **synchronous_commit** of the subscription worker.  |