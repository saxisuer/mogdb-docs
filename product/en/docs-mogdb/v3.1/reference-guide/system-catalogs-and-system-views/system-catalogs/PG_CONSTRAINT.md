---
title: PG_CONSTRAINT
summary: PG_CONSTRAINT
author: Guo Huan
date: 2021-04-19
---

# PG_CONSTRAINT

**PG_CONSTRAINT** records check, primary key, and unique constraints on tables.

**Table 1** PG_CONSTRAINT columns

| Name          | Type         | Description                                                  |
| :------------ | :----------- | :----------------------------------------------------------- |
| oid           | oid          | Row identifier (hidden attribute, which must be specified)   |
| conname       | name         | Constraint name (not necessarily unique)                     |
| connamespace  | oid          | OID of the namespace that contains the constraint            |
| contype       | "char"       | - **c**: check constraint<br/>- **p**: primary key constraint<br/>- **u**: unique constraint<br/>- **t**: trigger constraint<br/>- **x**: mutual exclusion constraint <br/>- **f**: foreign key constraint <br/>- **s**: clustering constraint <br/>- **i**: invalid constraint |
| condeferrable | boolean      | Whether the constraint can be deferrable                     |
| condeferred   | boolean      | Whether the constraint can be deferrable by default          |
| convalidated  | boolean      | Whether the constraint is valid. Currently, it can be set to **false** only for foreign key and check constraints. |
| conrelid      | oid          | Table containing this constraint (**0** if it is not a table constraint) |
| contypid      | oid          | Domain containing this constraint (**0** if it is not a domain constraint) |
| conindid      | oid          | ID of the index associated with the constraint               |
| confrelid     | oid          | Referenced table if this constraint is a foreign key. Otherwise, the value is **0**. Currently, foreign keys are not supported. Therefore, the value is fixed at **0**. |
| confupdtype   | "char"       | Foreign key update action code<br/>- **a**: no action<br/>- **r**: restriction<br/>- **c**: cascading<br/>- **n**: The parameter is set to **null**.<br/>- **d**: The default value is used. The value is empty because foreign keys are not supported currently. |
| confdeltype   | "char"       | Foreign key deletion action code<br/>- **a**: no action<br/>- **r**: restriction<br/>- **c**: cascading<br/>- **n**: The parameter is set to **null**.<br/>- **d**: The default value is used. The value is empty because foreign keys are not supported currently. |
| confmatchtype | "char"       | Foreign key match type<br/>- **f**: full match<br/>- **p**: partial match<br/>- **u**: simple match (not specified). The value is empty because foreign keys are not supported currently. |
| conislocal    | boolean      | Whether the local constraint is defined for the relationship |
| coninhcount   | integer      | Number of direct inheritance parent tables that this constraint has. When the value is not **0**, the constraint cannot be deleted or renamed. |
| connoinherit  | boolean      | Whether the constraint can be inherited                      |
| consoft       | boolean      | Whether the column indicates an informational constraint     |
| conopt        | boolean      | Whether you can use the informational constraint to optimize the execution plan |
| conkey        | smallint[]   | Column list of the constrained control if this column is a table constraint |
| confkey       | smallint[]   | List of referenced columns if this column is a foreign key. The value is empty because foreign keys are not supported currently. |
| conpfeqop     | oid[]        | ID list of the equality operators for PK = FK comparisons if this column is a foreign key. The value is empty because foreign keys are not supported currently. |
| conppeqop     | oid[]        | ID list of the equality operators for PK = PK comparisons if this column is a foreign key. The value is empty because foreign keys are not supported currently. |
| conffeqop     | oid[]        | ID list of the equality operators for FK = FK comparisons if this column is a foreign key. The value is empty because foreign keys are not supported currently. |
| conexclop     | oid[]        | ID list of the per-column exclusion operators if this column is an exclusion constraint |
| conbin        | pg_node_tree | Internal representation of the expression if this column is a check constraint |
| consrc        | text         | Human-readable representation of the expression if this column is a check constraint |
| conincluding  | smallint[]   | Not for constraint, but will be included in the attribute column of **INDEX**. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - **consrc** is not updated when referenced objects change and does not track new column names. Instead of relying on this column to update, you are advised to use **pg_get_constraintdef()** to extract the definition of a check constraint.
> - **pg_class.relchecks** must agree with the number of check-constraint entries found in the table for each relationship.
