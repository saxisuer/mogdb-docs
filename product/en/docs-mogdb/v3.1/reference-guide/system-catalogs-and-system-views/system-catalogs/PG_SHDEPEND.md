---
title: PG_SHDEPEND
summary: PG_SHDEPEND
author: Guo Huan
date: 2021-04-19
---

# PG_SHDEPEND

**PG_SHDEPEND** records the dependency between database objects and shared objects, such as roles. Based on this information, MogDB can ensure that those objects are unreferenced before attempting to delete them.

See also PG_DEPEND, which provides a similar function for dependencies involving objects within a single database.

Unlike most system catalogs, **PG_SHDEPEND** is shared across all databases of MogDB. There is only one copy of **PG_SHDEPEND** per MogDB, not one per database.

**Table 1** PG_SHDEPEND columns

| Name       | Type    | Reference       | Description                                                  |
| :--------- | :------ | :-------------- | :----------------------------------------------------------- |
| dbid       | oid     | PG_DATABASE.oid | OID of the database where a dependent object is (**0** for a shared object) |
| classid    | oid     | PG_CLASS.oid    | OID of the system catalog where a dependent object resides   |
| objid      | oid     | Any OID column  | OID of the dependent object                                  |
| objsubid   | integer | -               | Column number for a table column (**objid** and **classid** refer to the table itself); The value is **0** for all other object types. |
| refclassid | oid     | PG_CLASS.oid    | OID of the system catalog where a referenced object is (must be a shared catalog) |
| refobjid   | oid     | Any OID column  | OID of the referenced object                                 |
| deptype    | "char"  | -               | Code segment defining the specific semantics of this dependency relationship. See the following for details. |
| objfile    | text    | -               | Path of a user-defined function library file                 |

In all cases, a **PG_SHDEPEND** entry indicates that the referenced object cannot be dropped without also dropping the dependent object. However, there are several subflavors identified by **deptype**:

- SHARED_DEPENDENCY_OWNER (o)

  The referenced object (which must be a role) is the owner of the dependent object.

- SHARED_DEPENDENCY_ACL (a)

  The referenced object (which must be a role) is mentioned in the access control list (ACL) of the dependent object. A **SHARED_DEPENDENCY_ACL** entry is not made for the owner of the object, since the owner will have a **SHARED_DEPENDENCY_OWNER** entry anyway.

- SHARED_DEPENDENCY_PIN (p)

  There is no dependent object. This type of entry is a signal that the system itself depends on the referenced object, and so that object must never be deleted. Entries of this type are created only by **initdb**. The columns for the dependent object contain zeroes.
  
- SHARED_DEPENDENCY_ DBPRIV(d)

  The referenced object (must be a role) has the ANY permission on the dependent object (the specified OID of the dependent object corresponds to a row in the **GS_DB_PRIVILEGE** system catalog).
