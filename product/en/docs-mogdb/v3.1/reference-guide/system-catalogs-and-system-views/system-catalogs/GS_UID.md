---
title: GS_UID
summary: GS_UID
author: Guo Huan
date: 2022-05-12
---

# GS_UID

**GS_UID** records the unique identification meta information of the hasuids attribute table in the database.

**Table 1** GS_UID columns

| Name       | Type   | Description                                                |
| :--------- | :----- | :--------------------------------------------------------- |
| relid      | oid    | OID of a table.                                            |
| uid_backup | bigint | Largest unique identifier that can be assigned to a table. |