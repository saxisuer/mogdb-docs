---
title: PG_SYNONYM
summary: PG_SYNONYM
author: Guo Huan
date: 2021-07-26
---

# PG_SYNONYM

**PG_SYNONYM** records the mapping between synonym object names and other database object names.

**Table 1** **PG_SYNONYM** columns

| Name         | Type | Description                                                  |
| :----------- | :--- | :----------------------------------------------------------- |
| oid-         | oid  | Database object ID                                           |
| synname      | name | Synonym name                                                 |
| synnamespace | oid  | OID of the namespace that contains the synonym               |
| synowner     | oid  | Owner of the synonym, usually the OID of the user who created it |
| synobjschema | name | Schema name specified by the associated object               |
| synobjname   | name | Name of the associated object                                |
