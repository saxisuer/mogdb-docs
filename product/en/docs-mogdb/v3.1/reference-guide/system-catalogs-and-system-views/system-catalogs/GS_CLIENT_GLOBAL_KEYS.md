---
title: GS_CLIENT_GLOBAL_KEYS
summary: GS_CLIENT_GLOBAL_KEYS
author: Guo Huan
date: 2021-04-19
---

# GS_CLIENT_GLOBAL_KEYS

**GS_CLIENT_GLOBAL_KEYS** records information about the CMK in the encrypted equality feature. Each record corresponds to a CMK.

**Table 1** GS_CLIENT_GLOBAL_KEYS columns

| Name            | Type      | Description                                              |
| :-------------- | :-------- | :------------------------------------------------------- |
| oid             | oid       | Row identifier (hidden column)                           |
| global_key_name | name      | CMK name                                                 |
| key_namespace   | oid       | A namespace OID that contains this CMK                   |
| key_owner       | oid       | CMK owner                                                |
| key_acl         | aclitem[] | Access permissions that this key should have on creation |
| create_date     | timestamp | Time when a key is created                               |
