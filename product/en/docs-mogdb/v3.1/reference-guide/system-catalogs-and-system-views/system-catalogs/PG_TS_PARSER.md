---
title: PG_TS_PARSER
summary: PG_TS_PARSER
author: Guo Huan
date: 2021-04-19
---

# PG_TS_PARSER

**PG_TS_PARSER** contains entries defining text search parsers. A parser is responsible for splitting input text into lexemes and assigning a token type to each lexeme. The new parser must be created by the database system administrator.

**Table 1** PG_TS_PARSER columns

| Name         | Type    | Reference        | Description                                                |
| :----------- | :------ | :--------------- | :--------------------------------------------------------- |
| oid          | oid     | -                | Row identifier (hidden attribute, which must be specified) |
| prsname      | name    | -                | Text search parser name                                    |
| prsnamespace | oid     | PG_NAMESPACE.oid | OID of the namespace that contains the parser              |
| prsstart     | regproc | PG_PROC.proname  | Name of the parser's startup function                      |
| prstoken     | regproc | PG_PROC.proname  | Name of the parser's next-token function                   |
| prsend       | regproc | PG_PROC.proname  | Name of the parser's shutdown function                     |
| prsheadline  | regproc | PG_PROC.proname  | Name of the parser's headline function                     |
| prslextype   | regproc | PG_PROC.proname  | Name of the parser's lextype function                      |
