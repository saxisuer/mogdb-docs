---
title: GS_GLOBAL_CHAIN
summary: GS_GLOBAL_CHAIN
author: Zhang Cuiping
date: 2021-10-11
---

# GS_GLOBAL_CHAIN

**GS_GLOBAL_CHAIN** records information about modification operations performed by users on the tamper-proof user table. Each record corresponds to a table-level modification operation. Users with the audit administrator permission can query this system catalog, but no user is allowed to modify this system catalog.

**Table 1** GS_GLOBAL_CHAIN columns

| Name       | Type                     | Description                                                  |
| :--------- | :----------------------- | :----------------------------------------------------------- |
| blocknum   | bigint                   | Block number, which is the sequence number of the current user operation recorded in the ledger |
| dbname     | name                     | Name of the database, to which the modified tamper-proof user table belongs |
| username   | name                     | Username, which is the name of the user who performs the operation of modifying the user table |
| starttime  | timestamp with time zone | Latest timestamp when a user performs an operation           |
| relid      | oid                      | OID of the modified tamper-proof user table                  |
| relnsp     | name                     | OID of the namespace to which the modified tamper-proof user table belongs |
| relname    | name                     | User table name, which is the name of the modified tamper-proof user table |
| relhash    | hash16                   | Table-level hash change amount generated by the current operation |
| globalhash | hash32                   | Global digest, which is calculated based on the information of the current row and the **globalhash** of the previous row. It connects the entire table to verify the integrity of **GS_GLOBAL_CHAIN** data. |
| txcommand  | text                     | SQL statement whose operations are recorded                  |
