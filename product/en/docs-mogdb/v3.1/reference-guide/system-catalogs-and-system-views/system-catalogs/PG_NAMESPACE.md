---
title: PG_NAMESPACE
summary: PG_NAMESPACE
author: Guo Huan
date: 2021-04-19
---

# PG_NAMESPACE

**PG_NAMESPACE** records namespaces, that is, schema-related information.

**Table 1** PG_NAMESPACE columns

| Name              | Type      | Description                                                  |
| :---------------- | :-------- | :----------------------------------------------------------- |
| oid               | oid       | Row identifier (hidden attribute, which must be specified)   |
| nspname           | name      | Name of a namespace                                          |
| nspowner          | oid       | Owner of the namespace                                       |
| nsptimeline       | bigint    | Timeline when the namespace is created on the database node. This column is for internal use and valid only on the database node. |
| nspacl            | aclitem[] | Access permissions                                           |
| in_redistribution | "char"    | Whether the content is in the redistribution state           |
| nspblockchain     | boolean   | - If the value is **true**, the tamper-proof schema is used.<br />- If the value is **false**, the non-tamper-proof schema is used. |
