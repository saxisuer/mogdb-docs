---
title: PG_DESCRIPTION
summary: PG_DESCRIPTION
author: Guo Huan
date: 2021-04-19
---

# PG_DESCRIPTION

**PG_DESCRIPTION** records optional descriptions (comments) for each database object. Descriptions of many built-in system objects are provided in the initial contents of **PG_DESCRIPTION**.

See also PG_SHDESCRIPTION, which provides a similar function for descriptions involving objects that are shared across MogDB.

**Table 1** PG_DESCRIPTION columns

| Name        | Type    | Reference      | Description                                                  |
| :---------- | :------ | :------------- | :----------------------------------------------------------- |
| objoid      | oid     | Any OID column | OID of the object that this description pertains to          |
| classoid    | oid     | PG_CLASS.oid   | OID of the system catalog where the object appears           |
| objsubid    | integer | -              | Column number for a comment on a table column (**objoid** and **classoid** refer to the table itself); **0** for all other object types |
| description | text    | -              | Arbitrary text that serves as the description of the object  |
