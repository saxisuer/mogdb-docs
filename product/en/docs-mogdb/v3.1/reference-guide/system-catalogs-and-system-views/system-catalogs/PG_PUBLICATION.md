---
title: PG_PUBLICATION
summary: PG_PUBLICATION
author: Guo Huan
date: 2022-05-12
---

# PG_PUBLICATION

**PG_PUBLICATION** contains all publications created in the current database.

**Table 1** PG_PUBLICATION columns

| Name         | Type | Description                                                  |
| :----------- | :--- | :----------------------------------------------------------- |
| pubname      | text | Publication name.                                            |
| pubowner     | oid  | Publication owner.                                           |
| puballtables | bool | If true, this publication automatically includes all tables in the database, including any tables that will be created in the future. |
| pubinsert    | bool | If true, copy the INSERT operation on tables in the publication. |
| pubupdate    | bool | If true, copy the UPDATE operation on tables in the publication. |
| pubdelete    | bool | If true, copy the DELETE operation on tables in the publication. |