---
title: GS_WLM_SESSION_QUERY_INFO_ALL
summary: GS_WLM_SESSION_QUERY_INFO_ALL
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_SESSION_QUERY_INFO_ALL

**GS_WLM_SESSION_QUERY_INFO_ALL** displays load management information about completed jobs executed on the current database instance. The data is dumped from the kernel to the system catalog. If the GUC parameter enable_resource_record is set to **on**, query information in the kernel is imported to the system catalog **GS_WLM_SESSION_QUERY_INFO_ALL** every 3 minutes. This system catalog can be queried by users with the **sysadmin** permission only in Postgres.

**Table 1** GS_WLM_SESSION_QUERY_INFO_ALL columns

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| datid               | oid                      | OID of the database the backend is connected to              |
| dbname              | text                     | Name of the database the backend is connected to             |
| schemaname          | text                     | Schema name                                                  |
| nodename            | text                     | Name of the database instance where the statement is executed |
| username            | text                     | Username used for connecting to the backend                  |
| application_name    | text                     | Name of the application connected to the backend             |
| client_addr         | inet                     | IP address of the client connected to the backend. If this column is null, it indicates either that the client is connected via a Unix socket on the server machine or that this is an internal process such as autovacuum. |
| client_hostname     | text                     | Host name of the connected client, as reported by a reverse DNS lookup of client_addr. This column will be non-null only for IP connections and only when log_hostname is enabled. |
| client_port         | integer                  | TCP port number that the client uses for communication with the backend (**–1** if a Unix socket is used). |
| query_band          | text                     | Job type, which is specified by the GUC parameter **query_band**. The default value is a null string. |
| block_time          | bigint                   | Duration that the statement is blocked before being executed, including the statement parsing and optimization duration (unit: ms) |
| start_time          | timestamp with time zone | Time when the statement execution starts                     |
| finish_time         | timestamp with time zone | Time when the statement execution ends                       |
| duration            | bigint                   | Execution time of a statement. The unit is ms.               |
| estimate_total_time | bigint                   | Estimated execution time of a statement. The unit is ms.     |
| status              | text                     | Final statement execution status, which can be **finished** (normal) or **aborted** (abnormal) |
| abort_info          | text                     | Exception information displayed if the final statement execution status is **aborted** |
| resource_pool       | text                     | Resource pool used by the user                               |
| control_group       | text                     | Cgroup used by the statement                                 |
| estimate_memory     | integer                  | Estimated memory size of the statement                       |
| min_peak_memory     | integer                  | Minimum memory peak of the statement across the database instances, in MB |
| max_peak_memory     | integer                  | Maximum memory peak of the statement across the database instances, in MB |
| average_peak_memory | integer                  | Average memory usage during statement execution, in MB       |
| memory_skew_percent | integer                  | Memory usage skew of the statement among the database instances |
| spill_info          | text                     | Information about statement spill to the database instances:<br/>- **None**: The statement has not been spilled to disks on the database instances.<br/>- **All**: The statement has been spilled to disks on the database instances.<br/>- **[a:b]**: The statement has been spilled to disks on **a** of **b** database instances. |
| min_spill_size      | integer                  | Minimum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| max_spill_size      | integer                  | Maximum spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| average_spill_size  | integer                  | Average spilled data among database instances when a spill occurs (unit: MB). The default value is **0**. |
| spill_skew_percent  | integer                  | Database instance spill skew when a spill occurs             |
| min_dn_time         | bigint                   | Minimum execution time of the statement across the database instances (unit: ms) |
| max_dn_time         | bigint                   | Maximum execution time of the statement across the database instances (unit: ms) |
| average_dn_time     | bigint                   | Average execution time of the statement across the database instances (unit: ms) |
| dntime_skew_percent | integer                  | Execution time skew of the statement among the database instances |
| min_cpu_time        | bigint                   | Minimum CPU time of the statement across the database instances (unit: ms) |
| max_cpu_time        | bigint                   | Maximum CPU time of the statement across the database instances (unit: ms) |
| total_cpu_time      | bigint                   | Total CPU time of the statement across the database instances (unit: ms) |
| cpu_skew_percent    | integer                  | CPU time skew of the statement among the database instances  |
| min_peak_iops       | integer                  | Minimum IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| max_peak_iops       | integer                  | Maximum IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| average_peak_iops   | integer                  | Average IOPS peak of the statement across the database instances. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| iops_skew_percent   | integer                  | I/O skew of the statement among the database instances       |
| warning             | text                     | Warning. The following warnings are displayed:<br/>- Spill file size large than 256MB<br/>- Broadcast size large than 100MB<br/>- Early spill<br/>- Spill times is greater than 3<br/>- Spill on memory adaptive<br/>- Hash table conflict |
| queryid             | bigint                   | Internal query ID used for statement execution               |
| query               | text                     | Statement executed                                           |
| query_plan          | text                     | Execution plan of a statement                                |
| node_group          | text                     | Logical database instance of the user to which the statement belongs |
| cpu_top1_node_name  | text                     | Name of the node with the highest CPU usage                  |
| cpu_top2_node_name  | text                     | Name of the node with the second highest CPU usage           |
| cpu_top3_node_name  | text                     | Name of the node with the third highest CPU usage            |
| cpu_top4_node_name  | text                     | Name of the node with the fourth highest CPU usage           |
| cpu_top5_node_name  | text                     | Name of the node with the fifth highest CPU usage            |
| mem_top1_node_name  | text                     | Name of the node with the highest memory usage               |
| mem_top2_node_name  | text                     | Name of the node with the fifth highest CPU usage            |
| mem_top3_node_name  | text                     | Name of the node with the fifth highest CPU usage            |
| mem_top4_node_name  | text                     | Name of the node with the fifth highest CPU usage            |
| mem_top5_node_name  | text                     | Name of the node with the fifth highest CPU usage            |
| cpu_top1_value      | bigint                   | CPU usage                                                    |
| cpu_top2_value      | bigint                   | CPU usage                                                    |
| cpu_top3_value      | bigint                   | CPU usage                                                    |
| cpu_top4_value      | bigint                   | CPU usage                                                    |
| cpu_top5_value      | bigint                   | CPU usage                                                    |
| mem_top1_value      | bigint                   | Memory usage                                                 |
| mem_top2_value      | bigint                   | Memory usage                                                 |
| mem_top3_value      | bigint                   | Memory usage                                                 |
| mem_top4_value      | bigint                   | Memory usage                                                 |
| mem_top5_value      | bigint                   | Memory usage                                                 |
| top_mem_dn          | text                     | Top *N* memory usage                                         |
| top_cpu_dn          | text                     | Top *N* CPU usage                                            |
| n_returned_rows     | bigint                   | Number of rows in the result set returned by the SELECT statement |
| n_tuples_fetched    | bigint                   | Number of rows randomly scanned                              |
| n_tuples_returned   | bigint                   | Number of rows sequentially scanned                          |
| n_tuples_inserted   | bigint                   | Number of rows inserted                                      |
| n_tuples_updated    | bigint                   | Number of rows updated                                       |
| n_tuples_deleted    | bigint                   | Number of rows deleted                                       |
| n_blocks_fetched    | bigint                   | Number of cache loading times                                |
| n_blocks_hit        | bigint                   | Cache hits                                                   |
| db_time             | bigint                   | Valid DB time, which is accumulated if multiple threads are involved (unit: μs) |
| cpu_time            | bigint                   | CPU time (unit: μs)                                          |
| execution_time      | bigint                   | Execution time in the executor (unit: μs)                    |
| parse_time          | bigint                   | SQL parsing time (unit: μs)                                  |
| plan_time           | bigint                   | SQL plan generation time (unit: μs)                          |
| rewrite_time        | bigint                   | SQL rewriting time (unit: μs)                                |
| pl_execution_time   | bigint                   | Execution time of PL/pgSQL (unit: μs)                        |
| pl_compilation_time | bigint                   | Compilation time of PL/pgSQL (unit: μs)                      |
| net_send_time       | bigint                   | Network time (unit: μs)                                      |
| data_io_time        | bigint                   | I/O time (unit: μs)                                          |
| is_slow_query       | bigint                   | Specifies whether the query is a slow query.<br/>The value **1** indicates a slow query. |
