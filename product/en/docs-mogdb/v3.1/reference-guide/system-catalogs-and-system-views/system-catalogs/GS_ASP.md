---
title: GS_ASP
summary: GS_ASP
author: Guo Huan
date: 2022-05-12
---

# GS_ASP

**GS_ASP** displays the persistent ACTIVE SESSION PROFILE samples. This table can only be queried in the system database, and there is no data in the user database.

**Table 1** GS_ASP columns

| Name              | Type                     | Description                                                  |
| :---------------- | :----------------------- | :----------------------------------------------------------- |
| sampleid          | bigint                   | Sample ID                                                    |
| sample_time       | timestamp with time zone | Sampling time                                                |
| need_flush_sample | boolean                  | Specifies whether the sample needs to be flushed to disks.<br/>- **t** (true): yes<br/>- **f** (false): no |
| databaseid        | oid                      | Database ID                                                  |
| thread_id         | bigint                   | Thread ID                                                    |
| sessionid         | bigint                   | Session ID                                                   |
| start_time        | timestamp with time zone | Start time of a session                                      |
| event             | text                     | Event name                                                   |
| lwtid             | integer                  | Lightweight thread ID of the current thread                  |
| psessionid        | bigint                   | Parent thread of the streaming thread                        |
| tlevel            | integer                  | Level of the streaming thread, which corresponds to the level (**id**) of the execution plan |
| smpid             | integer                  | Concurrent thread ID in SMP execution mode                   |
| userid            | oid                      | ID of a session user                                         |
| application_name  | text                     | Name of an application                                       |
| client_addr       | inet                     | IP address of a client                                       |
| client_hostname   | text                     | Name of a client                                             |
| client_port       | integer                  | TCP port number used by a client to communicate with the backend |
| query_id          | bigint                   | Debug query ID                                               |
| unique_query_id   | bigint                   | Unique query ID                                              |
| user_id           | oid                      | User ID in the key of the unique query                       |
| cn_id             | integer                  | ID of the node that delivers the unique SQL statement. **cn_id** is in the key of the unique query. |
| unique_query      | text                     | Standardized Unique SQL text string                          |
| locktag           | text                     | Information of a lock that the session waits for, which can be parsed using **locktag_decode**. |
| lockmode          | text                     | Mode of a lock that the session waits for<br/>- **LW_EXCLUSIVE**: exclusive lock<br/>- **LW_SHARED**: shared lock<br/>- **LW_WAIT_UNTIL_FREE**: waits for the **LW_EXCLUSIVE** to be available |
| block_sessionid   | bigint                   | Blocks a session from obtaining the session ID of a lock if the session is waiting for the lock. |
| wait_status       | text                     | Provides more details about an event column.                 |
| global_sessionid  | text                     | Global session ID                                            |
| plan_node_id      | int                      | The operator ID of the execution plan tree                   |