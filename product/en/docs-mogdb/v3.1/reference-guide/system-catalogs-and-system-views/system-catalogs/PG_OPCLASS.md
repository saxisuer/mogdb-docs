---
title: PG_OPCLASS
summary: PG_OPCLASS
author: Guo Huan
date: 2021-04-19
---

# PG_OPCLASS

**PG_OPCLASS** defines index access method operator classes.

Each operator class defines semantics for index columns of a particular data type and a particular index access method. An operator class essentially specifies that a particular operator family is applicable to a particular indexable column data type. The set of operators from the family that are actually usable with the indexed column are whichever ones accept the column's data type as their left-hand input.

**Table 1** PG_OPCLASS columns

| Name         | Type    | Reference        | Description                                                  |
| :----------- | :------ | :--------------- | :----------------------------------------------------------- |
| oid          | oid     | -                | Row identifier (hidden attribute, which must be specified)   |
| opcmethod    | oid     | PG_AM.oid        | Index access method operator class served by an operator class |
| opcname      | name    | -                | Name of the operator class                                   |
| opcnamespace | oid     | PG_NAMESPACE.oid | Namespace of the operator class                              |
| opcowner     | oid     | PG_AUTHID.oid    | Owner of the operator class                                  |
| opcfamily    | oid     | PG_OPFAMILY.oid  | Operator family containing the operator class                |
| opcintype    | oid     | PG_TYPE.oid      | Data type that the operator class indexes                    |
| opcdefault   | boolean | -                | The value is **true** if this operator class is the default for **opcintype**. |
| opckeytype   | oid     | PG_TYPE.oid      | Type of data stored in index, or zero if same as **opcintype** |

An operator class's **opcmethod** must match the **opfmethod** of its containing operator family.
