---
title: PG_WORKLOAD_GROUP
summary: PG_WORKLOAD_GROUP
author: Guo Huan
date: 2021-04-19
---

# PG_WORKLOAD_GROUP

**PG_WORKLOAD_GROUP** provides workload group information in the database.

**Table 1** PG_WORKLOAD_GROUP columns

| Name            | Type    | Description                                                |
| :-------------- | :------ | :--------------------------------------------------------- |
| oid             | oid     | Row identifier (hidden attribute, which must be specified) |
| workload_gpname | name    | Workload group name                                        |
| respool_oid     | oid     | ID bound to the resource pool                              |
| act_statements  | integer | Maximum number of active statements in the workload group  |
