---
title: PG_AUTHID
summary: PG_AUTHID
author: Guo Huan
date: 2021-04-19
---

# PG_AUTHID

**PG_AUTHID** records information about database authentication identifiers (roles). The concept of users is contained in that of roles. A user is actually a role whose **rolcanlogin** has been set. Any role, whether its **rolcanlogin** is set or not, can use other roles as members.

For MogDB, only one **PG_AUTHID** exists, which is not available for every database. This system catalog is accessible only to system administrators.

**Table 1** PG_AUTHID columns

| Name             | Type                     | Description                                                  |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| oid              | oid                      | Row identifier (hidden attribute, which must be specified)   |
| rolname          | name                     | Name of a role                                               |
| rolsuper         | boolean                  | Whether the role is the initial system administrator with the highest permission<br />- **t** (true): yes<br />- **f** (false): no |
| rolinherit       | boolean                  | Whether the role automatically inherits permissions of roles of which it is a member<br />- **t** (true): automatically inherited<br />- **f** (false): not automatically inherited |
| rolcreaterole    | boolean                  | Whether the role can create more roles<br />- **t** (true): yes<br />- **f** (false): no |
| rolcreatedb      | boolean                  | Whether the role can create databases<br />- **t** (true): yes<br />- **f** (false): no |
| rolcatupdate     | boolean                  | Whether the role can directly update system catalogs Only the initial system administrator whose **usesysid** is set to **10** has this permission. It is unavailable for other users.<br />- **t** (true): yes<br />- **f** (false): no |
| rolcanlogin      | boolean                  | Whether the role can log in (whether this role can be given as the initial session authorization identifier)<br />- **t** (true): yes<br />- **f** (false): no |
| rolreplication   | boolean                  | Whether the role has the replication permission<br />- **t** (true): yes<br />- **f** (false): no |
| rolauditadmin    | boolean                  | Whether the role has the audit administrator permission<br />- **t** (true): yes<br />- **f** (false): no |
| rolsystemadmin   | boolean                  | Whether the role has system administrator permissions<br />- **t** (true): yes<br />- **f** (false): no |
| rolconnlimit     | integer                  | Maximum number of concurrent connections that the role can make (valid for roles that can log in)<br />The value **-1** indicates there is no limit. |
| rolpassword      | text                     | Password (possibly encrypted); **NULL** if no password       |
| rolvalidbegin    | timestamp with time zone | Account validity start time (**NULL** if no start time)      |
| rolvaliduntil    | timestamp with time zone | Password expiry time (**NULL** if no expiration)             |
| rolrespool       | name                     | Resource pool that a user can use                            |
| roluseft         | boolean                  | Whether the role can perform operations on foreign tables<br />- **t** (true): yes<br />- **f** (false): no |
| rolparentid      | oid                      | OID of a group user to which the user belongs                |
| roltabspace      | text                     | Maximum size of a user data table                            |
| rolkind          | char                     | Special user types, including private users and common users |
| rolnodegroup     | oid                      | Unsupported currently                                        |
| roltempspace     | text                     | Maximum size of a user's temporary table, in KB              |
| rolspillspace    | text                     | Maximum size of data that can be written to disks when a user executes a job, in KB |
| rolexcpdata      | text                     | Query rules that can be set by users (reserved)              |
| rolmonitoradmin  | boolean                  | Whether the role has monitor administrator permissions<br />- **t** (true): yes<br />- **f** (false): no |
| roloperatoradmin | boolean                  | Whether the role has the O&M administrator permission<br />- **t** (true): yes<br />- **f** (false): no |
| rolpolicyadmin   | boolean                  | Whether the role has the security policy administrator permission<br />- **t** (true): yes<br />- **f** (false): no |
