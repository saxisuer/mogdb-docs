---
title: PG_RANGE
summary: PG_RANGE
author: Guo Huan
date: 2021-04-19
---

# PG_RANGE

**PG_RANGE** records information about range types. Entries in PG_TYPE are excluded.

**Table 1** PG_RANGE columns

| Name         | Type    | Reference        | Description                                                  |
| :----------- | :------ | :--------------- | :----------------------------------------------------------- |
| rngtypid     | oid     | PG_TYPE.oid      | OID of the range type                                        |
| rngsubtype   | oid     | PG_TYPE.oid      | OID of the element type (subtype) of this range type         |
| rngcollation | oid     | PG_COLLATION.oid | OID of the collation used for range comparisons (**0** if none) |
| rngsubopc    | oid     | PG_OPCLASS.oid   | OID of the subtype's operator class used for range comparisons |
| rngcanonical | regproc | PG_PROC.proname  | Name of the function to convert a range value into canonical form (**0** if none) |
| rngsubdiff   | regproc | PG_PROC.proname  | Name of the function to return the difference between two element values as **double precision** (**0** if none) |

**rngsubopc** (together with **rngcollation**, if the element type is collatable) determines the sort ordering used by the range type. **rngcanonical** is used when the element type is discrete.
