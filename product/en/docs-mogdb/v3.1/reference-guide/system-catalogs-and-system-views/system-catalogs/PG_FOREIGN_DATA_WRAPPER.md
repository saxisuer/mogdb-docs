---
title: PG_FOREIGN_DATA_WRAPPER
summary: PG_FOREIGN_DATA_WRAPPER
author: Guo Huan
date: 2021-04-19
---

# PG_FOREIGN_DATA_WRAPPER

**PG_FOREIGN_DATA_WRAPPER** records foreign-data wrapper definitions. A foreign-data wrapper is the mechanism by which external data, residing on foreign servers, is accessed.

**Table 1** PG_FOREIGN_DATA_WRAPPER columns

| Name         | Type      | Reference     | Description                                                  |
| :----------- | :-------- | :------------ | :----------------------------------------------------------- |
| oid          | oid       | -             | Row identifier (hidden attribute, which must be specified)   |
| fdwname      | name      | -             | Name of a foreign-data wrapper                               |
| fdwowner     | oid       | PG_AUTHID.oid | Owner of the foreign-data wrapper                            |
| fdwhandler   | oid       | PG_PROC.oid   | References a handler function that is responsible for supplying execution routines for the foreign-data wrapper (**0** if no handler is provided) |
| fdwvalidator | oid       | PG_PROC.oid   | References a validator function that is responsible for checking the validity of the options given to the foreign-data wrapper, as well as options for foreign servers and user mappings using the foreign-data wrapper. The value is **0** if no validator is provided. |
| fdwacl       | aclitem[] | -             | Access permissions.                                          |
| fdwoptions   | text[]    | -             | Foreign-data wrapper specific option, expressed in a string in the format of keyword=value |
