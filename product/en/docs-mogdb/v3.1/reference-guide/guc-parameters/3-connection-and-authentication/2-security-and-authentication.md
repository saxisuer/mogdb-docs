---
title: Security and Authentication (postgresql.conf)
summary: Security and Authentication (postgresql.conf)
author: Zhang Cuiping
date: 2021-04-20
---

# Security and Authentication (postgresql.conf)

This section describes parameters about client-to-server authentication.

## authentication_timeout

**Parameter description**: Specifies the longest duration to wait before the client authentication times out. If a client is not authenticated by the server within the period, the server automatically disconnects from the client so that the client does not occupy connection resources.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 600. The smallest unit is s.

**Default value**: **1min**

## auth_iteration_count

**Parameter description**: Specifies the number of iterations during the generation of encryption information for authentication.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 2048 to 134217728

**Default value**: **10000**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If the number of iterations is too small, the password storage security is reduced. If the number of iterations is too large, the performance deteriorates in scenarios involving password encryption, such as authentication and user creation. Set the number of iterations based on actual hardware conditions. You are advised to retain the default value.

## session_authorization

**Parameter description**: Specifies the user ID of the current session.

This parameter is an internal parameter of the USERSET type and can be set only using the [SET SESSION AUTHORIZATION](../../../reference-guide/sql-syntax/SET-SESSION-AUTHORIZATION.md) syntax.

**Value range**: a string

**Default value**: **NULL**

## session_timeout

**Parameter description**: Specifies the longest duration allowed when no operations are performed on a client after it is connected to the server.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../30-appendix.md).

**Value range**: an integer ranging from 0 to 86400. The smallest unit is s. **0** indicates that the timeout is disabled.

**Default value**: **10min**

## idle_in_transaction_session_timeout

**Parameter description:** Specifies the maximum duration during which no operation is performed if the current session is in a transaction after the connection to the server is established.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../30-appendix.md).

**Value range**: an integer ranging from 0 to 86400. The smallest unit is s. **0** indicates disable the timeout.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The gsql client of MogDB has an automatic reconnection mechanism. For local connection of initialized users, the client reconnects to the server if the connection breaks after the timeout.

## ssl

**Parameter description**: Specifies whether the SSL connection is enabled. Read [Using gsql to Connect to a Database](../../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md) before setting this parameter.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the SSL connection is enabled.
- **off** indicates that the SSL connection is not enabled.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> To enable SSL connections, you also need to configure parameters such as **ssl_cert_file**, **ssl_key_file**, and **ssl_ca_file** and the corresponding files. Incorrect configurations may cause startup failure of MogDB.

**Default value**: **on**

## require_ssl

**Parameter description**: Specifies whether the server requires the SSL connection. This parameter is valid only when **[ssl](#ssl)** is set to **on**. Read [Using gsql to Connect to a Database](../../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md) before setting this parameter.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the server requires the SSL connection.
- **off** indicates that the server does not require the SSL connection.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> MogDB supports SSL when a client connects to the primary node of the database. It is recommended that the SSL connection be enabled only on the primary node of the database.

**Default value**: **off**

## ssl_ciphers

**Parameter description**: Specifies the list of encryption algorithms supported by SSL. Only the sysadmin user can access the list.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. Separate multiple encryption algorithms by semicolons (;). For details about supported encryption algorithms, see [Establishing Secure TCP/IP Connections in SSL Mode](../../../security-guide/security/1-client-access-authentication.md#Establishing Secure TCP/IP Connections in SSL Mode).

**Default value**: **ALL**

## ssl_renegotiation_limit

**Parameter description**: Specifies the allowed traffic volume over an SSL-encrypted channel before the session key is renegotiated. The renegotiation mechanism reduces the probability that attackers use the password analysis method to crack the key based on a huge amount of data but causes big performance losses. The traffic indicates the sum of transmitted and received traffic. The SSL renegotiation mechanism has been disabled because of potential risks. This parameter is reserved for version compatibility and does not take effect.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 2147483647. The unit is KB. **0** indicates that the renegotiation mechanism is disabled.

**Default value**: **0**

## ssl_cert_file

**Parameter description**: Specifies the name of the SSL server certificate file. The path depends on the data directory.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **server.crt**

## ssl_key_file

**Parameter description**: Specifies the name of the SSL private key file. The path depends on the data directory.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **server.key**

## ssl_ca_file

**Parameter description**: Specifies the name of a file that contains CA information. The relative path is relative to the data directory.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. If it is an empty string, no CA file is loaded and client certificate verification is not performed.

**Default value**: **cacert.pem**

## ssl_crl_file

**Parameter description**: Specifies the certificate revocation list (CRL). If the certificate of a client is in the list, the certificate is invalid. A relative path must be used. The path depends on the data directory.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that there is no CRL.

**Default value**: empty

## krb_server_keyfile

**Parameter description**: Specifies the location of the main configuration file of the Kerberos service. For details, see [Configuring Client Access Authentication](../../../security-guide/security/1-client-access-authentication.md#configuring-client-access-authentication).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: empty

## krb_srvname

**Parameter description**: Specifies the Kerberos service name. For details, see [Configuring Client Access Authentication](../../../security-guide/security/1-client-access-authentication.md#configuring-client-access-authentication).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **postgres**

## krb_caseins_users

**Parameter description**: Specifies whether the Kerberos username is case-sensitive.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the Kerberos username is case-insensitive.
- **off** indicates that the Kerberos username is case-sensitive.

**Default value**: **off**

## modify_initial_password

**Parameter description**: After MogDB is installed, there is only one initial user account (whose UID is 10) in the database. When a user logs in to the database using this initial account for the first time, this parameter determines whether the password of the initial account needs to be modified.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

If the initial user password is not specified during the installation process, the initial user password will be blank by default after installation. You need to set the initial user password through the gsql client before performing other operations. This parameter function no longer takes effect, and this parameter is reserved only for compatible upgrade scenarios.

**Value range**: Boolean

- **on** indicates that the password of the initial account needs to be modified upon the first login.
- **off** indicates that the password of the initial account does not need to be modified.

**Default value**: **off**

## password_policy

**Parameter description**: Specifies whether to check the password complexity when you run the **CREATE ROLE/USER** or **ALTER ROLE/USER** command to create or modify an account of MogDB. For details about the password complexity check policy, see [Setting Password Security Policies](../../../security-guide/security/2-managing-users-and-their-permissions.md#setting-password-security-policies).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> For security purposes, do not disable the password complexity policy.

**Value range**: **0** and **1**

- **0** indicates that no password complexity policy is enabled.
- **1** indicates that the default password complexity policy is enabled.

**Default value**: **1**

## password_reuse_time

**Parameter description**: Specifies whether to check the reuse interval of the new password when you run the **ALTER USER** or **ALTER ROLE** command to change a user password. For details about the password reuse policy, see [Setting Password Security Policies](../../../security-guide/security/2-managing-users-and-their-permissions.md#setting-password-security-policies).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> When you change the password, the system checks the values of **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)**.
>
> - If the values of **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)** are both positive numbers, an old password can be reused when it meets either of the reuse restrictions.
> - If the value of **[password_reuse_time](#password_reuse_time)** is **0**, password reuse is restricted based on the number of reuse times, and not on the reuse interval.
> - If the value of **[password_reuse_max](#password_reuse_max)** is **0**, password reuse is restricted based on the reuse interval, and not on the number of reuse times.
> - If the values of both **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)** are **0**, password reuse is not restricted.

**Value range**: a floating point number ranging from 0 to 3650. The unit is day.

- **0** indicates that the password reuse interval is not checked.
- A positive number indicates that a new password cannot be chosen from passwords in history that are newer than the specified number of days.

**Default value**: **60**

## password_reuse_max

**Parameter description**: Specifies whether to check the reuse times of the new password when you run the **ALTER USER** or **ALTER ROLE** command to change a user password. For details about the password reuse policy, see [Setting Password Security Policies](../../../security-guide/security/2-managing-users-and-their-permissions.md#setting-password-security-policies).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> When you change the password, the system checks the values of **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)**.
>
> - If the values of **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)** are both positive numbers, an old password can be reused when it meets either of the reuse restrictions.
> - If the value of **[password_reuse_time](#password_reuse_time)** is **0**, password reuse is restricted based on the number of reuse times, and not on the reuse interval.
> - If the value of **[password_reuse_max](#password_reuse_max)** is **0**, password reuse is restricted based on the reuse interval, and not on the number of reuse times.
> - If the values of both **[password_reuse_time](#password_reuse_time)** and **[password_reuse_max](#password_reuse_max)** are **0**, password reuse is not restricted.

**Value range**: an integer ranging from 0 to 1000

- **0** indicates that the password reuse times are not checked.
- A positive number indicates that the new password cannot be the one whose reuse times exceed the specified number.

**Default value**: **0**

## password_lock_time

**Parameter description**: Specifies the duration before a locked account is automatically unlocked. For details about the policy, see [Setting Password Security Policies](../../../security-guide/security/2-managing-users-and-their-permissions.md#setting-password-security-policies).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The locking and unlocking functions take effect only when the values of **password_lock_time** and **[failed_login_attempts](#failed_login_attempts)** are positive numbers.

**Value range**: a floating point number ranging from 0 to 365. The unit is day.

- **0** indicates that an account is not automatically locked if the password verification fails.
- A positive number indicates the duration after which a locked account is automatically unlocked.

**Default value**: **1d**

## failed_login_attempts

**Parameter description**: Specifies the maximum number of incorrect password attempts before an account is locked. The account will be automatically unlocked after the time specified by **password_lock_time**. Only the sysadmin user can access the account. The automatic account locking policy applies in scenarios such as login and password modification using the **ALTER USER** command. For details about the policy, see [Setting Password Security Policies](../../../security-guide/security/2-managing-users-and-their-permissions.md#setting-password-security-policies).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The locking and unlocking functions take effect only when the values of **failed_login_attempts** and **[password_lock_time](#password_lock_time)** are positive numbers.

**Value range**: an integer ranging from 0 to 1000

- **0** indicates that the automatic locking function does not take effect.
- A positive number indicates that an account is locked when the number of incorrect password attempts reaches the specified number.

**Default value:** **10**

## password_encryption_type

**Parameter description**: Specifies the encryption type of a user password. Changing the value of this parameter does not change the password encryption type of existing users. The new encryption type is applied to passwords of new users or passwords modified after the parameter value is changed.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: **0**, **1**, **2**, and **3**

- **0** indicates that passwords are encrypted with MD5.
- **1** indicates that passwords are encrypted with SHA-256 and MD5.
- **2** indicates that passwords are encrypted with SHA-256.
- **3** indicates that the passwords are encrypted in sm3 mode.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The MD5 encryption algorithm is not recommended because it has lower security and poses security risks.

**Default value:** **2**

## password_min_length

**Parameter description**: Specifies the minimum password length.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer. A password can contain 6 to 999 characters.

**Default value**: **8**

## password_max_length

**Parameter description**: Specifies the maximum password length.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer. A password can contain 6 to 999 characters.

**Default value**: **32**

## password_min_uppercase

**Parameter description**: Specifies the minimum number of uppercase letters that an account password must contain.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 999

- **0** means no limit.
- A positive integer indicates the minimum number of uppercase letters required in a password when you create an account.

**Default value**: **0**

## password_min_lowercase

**Parameter description**: Specifies the minimum number of lowercase letters that an account password must contain.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 999

- **0** means no limit.
- A positive integer indicates the minimum number of lowercase letters required in a password when you create an account.

**Default value**: **0**

## password_min_digital

**Parameter description**: Specifies the minimum number of digits that an account password must contain.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 999

- **0** means no limit.
- A positive integer indicates the minimum number of digits required in a password when you create an account.

**Default value**: **0**

## password_min_special

**Parameter description**: Specifies the minimum number of special characters that an account password must contain.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 999

- **0** means no limit.
- A positive integer indicates the minimum number of special characters required in a password when you create an account.

**Default value**: **0**

## password_effect_time

**Parameter description**: Specifies the validity period of an account password.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to 999. The unit is day.

- **0** indicates that the validity period restriction is disabled.
- A floating point number from 1 to 999 indicates the number of days for which an account password is valid. When the password is about to expire or has expired, the system prompts the user to change the password.

**Default value**: **90**

## password_notify_time

**Parameter description**: Specifies how many days in advance a user is notified before a password expires.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 999. The unit is day.

- **0** indicates that the reminder is disabled.
- A positive integer indicates the number of days prior to password expiration that a user will receive a reminder.

**Default value**: **7**
