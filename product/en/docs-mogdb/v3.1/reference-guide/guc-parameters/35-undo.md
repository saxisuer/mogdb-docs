---
title: Undo
summary: Undo
author: Zhang Cuiping
date: 2021-11-08
---

# Undo

## undo_space_limit_size

**Parameter description**: Specifies the threshold for forcibly recycling undo space. When the undo space usage reaches 80% of the threshold, forcible recycling starts. You can set this parameter to a large value based on service requirements and then set it to a proper value based on the actual undo space usage.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 800 MB to 32 TB

**Default value**: **256 GB**

## undo_limit_size_per_transaction

**Parameter description**: Specifies the undo space threshold of a single transaction. If the threshold is reached, the transaction is rolled back due to an error.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 100 MB to 32 TB

**Default value**: **32 GB**