---
title: Archiving
summary: Archiving
author: Zhang Cuiping
date: 2021-04-20
---

# Archiving

## archive_mode

**Parameter description**: Specifies whether to archive WALs.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> When **wal_level** is set to **minimal**, the **archive_mode** parameter is unavailable.

**Value range**: Boolean

- **on** indicates that the archiving is enabled.
- **off** indicates that the archiving is disabled.

**Default value**: **off**

## archive_command

**Parameter description:** Specifies the command set by the administrator to archive WALs. You are advised to set the archive log path to an absolute path.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If both **archive_dest** and **archive_command**are configured, WALs are preferentially saved to the directory specified by **archive_dest**. The command configured by **archive_command**does not take effect.
>
> - Any **%p** in the string is replaced by the absolute path of the file to archive, and any **%f** is replaced by only the file name. (The relative path is relative to the data directory.) Use **%%** to embed an actual **%** character in the command.
> - This command returns zero only if it succeeds. The following shows the command:
>
> ```
> archive_command = 'cp --remove-destination %p /mnt/server/archivedir/%f'
> ```
>
> - **--remove-destination** indicates that files will be overwritten during the archiving.
>
> - If there are multiple archive commands, write them to the shell script file and set **archive_command**to the command for executing the script. For example:
>
>   ```shell
>   -- Assume that multiple commands are as follows:
>   test ! -f dir/%f && cp %p dir/%f
>   -- The content of the test.sh script is as follows:
>   test ! -f dir/$2 && cp $1 dir/$2
>   -- The archive command is as follows:
>   archive_command='sh dir/test.sh %p %f'
>   ```

**Value range**: a string

**Default value:** **(disabled)**

## archive_dest

**Parameter description:** Specifies the path set by the administrator to archive WALs. You are advised to set the archive log path to an absolute path.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If both **archive_dest** and **archive_command**are configured, WALs are preferentially saved to the directory specified by **archive_dest**. The command configured by **archive_command**does not take effect.
> - If the string is a relative path, it is relative to the data directory. The following is an example:
>
> ```
> archive_dest = '/mnt/server/archivedir/'
> ```

**Value range**: a string

**Default value**: an empty string

## archive_timeout

**Parameter description**: Specifies the archiving period.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - The server is forced to switch to a new WAL segment file when the period specified by this parameter has elapsed since the last file switch.
> - Archived files that are closed early due to a forced switch are still of the same length as full files. Therefore, a very short **archive_timeout** will bloat the archive storage. You are advised to set **archive_timeout** to **60s**.

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is second. **0** indicates that archiving timeout is disabled.

**Default value**: **0**
