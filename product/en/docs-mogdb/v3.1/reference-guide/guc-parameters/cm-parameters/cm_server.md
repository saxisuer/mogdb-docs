---
title: cm_agent Parameters
summary: cm_agent Parameters
author: Guo Huan
date: 2022-05-23
---

# cm_server Parameters

## log_dir

**Parameter description**: specifies the directory for storing cm_server log files. The directory can be an absolute path or a relative path (relative to $GAUSSLOG).

**Value range**: String. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **log** indicating that the cm_server log is generated in the cm directory under **$GAUSSLOG**.

## log_file_size

**Parameter description**: controls the log file size. When the log file size reaches a specified size, a new log file will be created to record logs. 

**Value range**: Integer. The value ranges from 0 to 2047. The unit is MB. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **16 MB**

## log_min_messages

**Parameter description**: controls the level of messages written to the CM_server log file. Each level contains information at all levels following it. The lower the level, the fewer messages are recorded in the server run logs.

**Value range**: Enumeration. The value can be debug5, debug1, log, warning, error, and fatal. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **warning**

## thread_count

**Parameter description**: specifies the number of threads in the cm_server thread pool.

**Value range**: Integer. The value ranges from 2 to 1000. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1000**

## alarm_component

**Parameter description**: sets the location of the alarm component for handling alarms.

**Value range**: String. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- If alarm-type is set to 5 in the preinstall script gs_preinstall, no third-party component is connected to the system and alarms are written to the system_alarm log. In this case, the value of the GUC parameter alarm_component is as follows: **/opt/snas/bin/snas_cm_cmd**.
- If alarm-type in the preinstall script gs_preinstall is set to 1, the system connects to a third-party component. In this case, the value of the GUC parameter alarm_component is the absolute path of the executable program of the third-party component.

**Default value**: **/opt/snas/bin/snas_cm_cmd**

## instance_failover_delay_timeout

**Parameter description**: specifies the delay timeout of the failover standby instance when cm_server detects that the primary instance breaks down. 

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0**

## instance_heartbeat_timeout

**Parameter description**: specifies the instance heartbeat timeout duration. 

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **6**

## cmserver_ha_connect_timeout

**Parameter description**: specifies the timeout duration for the primary cm_server node connecting the standby cm_server node. 

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **2**

## cmserver_ha_heartbeat_timeout

**Parameter description**: specifies the heartbeat timeout duration for the primary and standby cm_server nodes.

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **6**

## phony_dead_effective_time

**Parameter description**: Detects DN process deadlock. If the number of times for deadlock detected is greater than the value of this parameter, the process is considered to be in deadlock and the process is restarted.

**Value range**: Integer. The modification takes effect only after the cm_server is restarted. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **5**

## enable_transaction_read_only

**Parameter description**: controls whether the database is in read-only mode.

**Value range**: Boolean. The value can be on, off, true, false, yes, no, 1, and 0. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **on**

## datastorage_threshold_check_interval

**Parameter description**: specifies the interval at which the disk is used. The user checks disk occupation at the specified interval. 

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10**

## datastorage_threshold_value_check

**Parameter description**: sets the disk usage threshold for the database in read-only mode. When the disk usage exceeds the threshold, the database is automatically set to the read-only mode.

**Value range**: Integer. The value ranges from 1 to 99. The unit is percentage. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **85**

## max_datastorage_threshold_check

**Parameter description**: sets the maximum interval for checking disk usage. After you manually modify the parameter of the read-only mode, the full check of the disk is automatically enabled at a specified interval.

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **43200**

## cmserver_ha_status_interval

**Parameter description**: specifies the interval for synchronizing status information between the primary and standby CM servers.

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## cmserver_self_vote_timeout

**Parameter description**: specifies the cm_server self-arbitration timeout duration. 

**Value range**: Integer. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **6**

## alarm_report_interval

**Parameter description**: specifies the interval at which an alarm is reported. 

**Value range**: Non-negative integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **3**

## alarm_report_max_count

**Parameter description**: specifies the maximum number of times for an alarm to be reported. 

**Value range**: Non-negative integer. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## enable_az_auto_switchover

**Parameter description**: allows cm_server to switch AZ automatically. If this parameter is disabled, when a DN failure occurs, the system does not automatically switch to another AZ even if the AZ is no longer available unless the switchover command is executed manually.

**Value range**: Non-negative integer. **0** indicates that the parameter function is disabled. **1** indicates that the parameter function is enabled. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## instance_keep_heartbeat_timeout

**Parameter description**: The cm_agent periodically detects the instance status and reports it to the cm_server. If the instance status cannot be detected for a long time and the number of times exceeds the threshold, the cm_server sends a command to the agent to restart the instance.

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **40**

## az_switchover_threshold

**Parameter description**: If the failure rate of DN fragments in an AZ (Number of failed DN fragments/Total number of DN fragments x 100%) exceeds this value, automatic AZ switchover is triggered.

**Value range**: Integer. The value ranges from 0 to 100. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **100**

## az_check_and_arbitrate_interval

**Parameter description**: specifies the interval for checking the status of an AZ. When an AZ is abnormal, automatic AZ switchover is triggered.

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **2**

## az_connect_check_interval

**Parameter description**: periodically checks network connections between AZs. This parameter indicates the interval between two consecutive checks.

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **60**

## az_connect_check_delay_time

**Parameter description**: specifies the delay between two retries. There are multiple retries for checking network connection between AZs.

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **150**

## cmserver_demote_delay_on_etcd_fault

**Parameter description**: specifies the time interval for cm_server changed to a standby node from a primary node due to unhealthy etcd. 

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **8**

## instance_phony_dead_restart_interval

**Parameter description**: When a DN instance is dead, it is restarted by cm_agent. When the same instance is killed continuously because of death, the interval cannot be shorter than this value. Otherwise, cm_agent will not send the command.

**Value range**: Integer. The unit is second. The minimum value is 1800. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **21600**

## cm_auth_method

**Parameter description**: CM module port authentication mode. **trust** indicates that port authentication is not configured, and gss indicates that kerberos port authentication is used. Note that the kerberos server and client can be changed to gss only after the Kerberos server and client are installed successfully. Otherwise, the CM module cannot communicate normally and the database status will be affected.

**Value range**: Enumeration. The value can be **trust** and **gss**. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **trust**

## cm_krb_server_keyfile

**Parameter description**: The location of the kerberos server key file must be set to an absolute path. The file name is usually in the ${GAUSSHOME}/kerberos directory and ends in the keytab format. The file name is the same as the user name where the database is running. If the cm_auth_method parameter is changed to gss, the parameter must also be set to a correct path. Otherwise, the database status will be affected.

**Value range**: String. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **${GAUSSHOME}/kerberos/{UserName}.keytab**. The default value cannot take effect and is used as a prompt. 

## cm_server_arbitrate_delay_base_time_out

**Parameter description**: specifies the cm_server basic arbitration delay duration. When the primary cm_server is disconnected, the arbitration starts. After the arbitration delay, a new primary cm_server is selected. The arbitration delay duration is determined by the basic arbitration delay duration, node index (server ID number), and increment duration. The formula is as follows: Arbitration delay duration = Basic arbitration delay duration + Node index x Arbitration delay increment duration

**Value range**: Integer. Index > 0. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10**

## cm_server_arbitrate_delay_incrememtal_time_out

**Parameter description**: cm_server arbitration delay increment duration. When the primary cm_server master disconnected, the arbitration starts. After the arbitration delay, a new primary cm_server is selected. The arbitration delay duration is determined by the basic arbitration delay duration, node index (server ID number), and increment duration. The formula is as follows: Arbitration delay duration = Basic arbitration delay duration + Node index x Arbitration delay increment duration

**Value range**: Integer. index>0. The unit is second. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **3**

## force_promote

**Parameter description**: specifies whether cm_server enables forcible promotion logic, which ensures that basic database functions are available at the cost of losing some data when the database is in the unknown state. **0** indicates that the function is disabled, and **1** indicates that the function is enabled. This parameter also applies to DN.

**Value range**: Integer. The value ranges from 0 to 1. The modification takes effect only after the cm_server is restarted. 

**Default value**: **0**

## switch_rto

**Parameter description**: specifies the wait delay of the cm_server forcible startup logic. When force_promote is set to 1, and one fragment of a database is in the non-primary status, the wait delay starts. After the wait delay duration, the forcible startup logic is executed. 

**Value range**: Integer. The value ranges from 60 to 2147483647. The unit is second. The modification takes effect only after the cm_server is restarted. 

**Default value**: **0**

## backup_open

**Parameter description**: specifies the DR database instance settings. After this function is enabled, CM runs in DR database instance mode.

**Value range**: Integer. The value ranges from 0 to 1. This parameter cannot be enabled for non-DR database instances. 

The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **0** indicates that the function is disabled.
- **1** indicates that the function is enabled.

**Default value**: **0**

## enable_dcf

**Parameter description**: specifies whether to enable the DCF mode. 

**Value range**: Boolean. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **0** indicates that the DCF mode is disabled. 
- **1** indicates that the DCF mode is enabled.

**Default value**: **off**

## install_type

**Parameter description**: distinguishes whether the database instance is based on Dorado.

**Value range**: Integer. The value ranges from 0 to 2. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md). This parameter cannot be enabled for non-DR database instances. 

- **0** indicates that no DR-based database instance is built.
- **1** indicates that the database instance is based on Dorado. 
- **2** indicates that the database instance is based on the stream database instance.

**Default value**: **0**

## enable_ssl

**Parameter description**: specifies whether to enable the SSL certificate.

**Value range**: Boolean. If this parameter function is enabled, the SSL certificate encryption communication is enabled. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **on** indicates that SSL is enabled. 

- **off** indicates that SSL is disabled. 

- **Default value**: **off**

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **Notice**: For security reasons, do not disable this configuration. After shutdown, CM will not encrypt communication, and all information will be transmitted in plain text, which may bring security risks such as eavesdropping, tampering and impersonation.

## ssl_cert_expire_alert_threshold

**Parameter description**: specifies the SSL certificate expiration alarm time. 

**Value range**: Integer. The unit is day. If the certificate expiration time is less than the value that is set, an expiration alarm will be reported. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **90**

## ssl_cert_expire_check_interval

**Parameter description**: specifies the interval for checking whether the SSL certificate expires. 

**Value range**: Integer. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **86400**

## delay_arbitrate_timeout

**Parameter description**: The time when the node becomes a primary node in the same AZ is preferred.

**Value range**: Integer. The value ranges from 0 to 21474836. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0**

## ddb_type

**Parameter description**: specifies whether to switch to the etcd or dcc mode.

**Value range**: Integer. 0: etcd. 1: dcc. The modification takes effect only after the cm_server is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0**

## ddb_log_level

**Parameter description**: sets the ddb log level. 

Disable log: NONE, disabling log printing. It cannot be used with the following log levels.

Enable the log: "RUN_ERR | RUN_WAR | RUN_INF | DEBUG_ERR | DEBUG_WAR | DEBUG_INF | TRACE | PROFILE | OPER" log level can be selected from the above string and used combined with vertical lines. It cannot be configured to be an empty string.

**Value range**: String. RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER。For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: RUN_ERR|RUN_WAR|DEBUG_ERR|OPER|RUN_INF|PROFILE

## ddb_log_backup_file_count

**Parameter description**: specifies the maximum number of log files to be stored. 

**Value range**: Integer. The value ranges from 1 to 100. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10**

## ddb_max_log_file_size

**Parameter description**: specifies the maximum number of bytes for a single log. 

**Value range**: String. The value ranges from 1M to 1000M. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10M**

## ddb_log_suppress_enable

**Parameter description**: specifies whether to enable the log suppression function. 

**Value range**: Integer. 0: disabled. 1: enabled. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## ddb_election_timeout

**Parameter description**: specifies the dcc election timeout duration.

**Value range**: Integer. The value ranges from 1 to 600. The unit is second. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **3**