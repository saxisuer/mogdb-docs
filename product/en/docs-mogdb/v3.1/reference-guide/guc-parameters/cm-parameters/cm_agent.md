---
title: cm_agent Parameters
summary: cm_agent Parameters
author: Guo Huan
date: 2022-05-23
---

# cm_agent Parameters

## log_dir

**Parameter description**: specifies the directory for storing a cm_agent log file. The directory can be an absolute path or a relative path (relative to $GAUSSLOG).

**Value range**: String. The modification takes effect only after the cm_agent is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **log** indicating that the cm_agent log is generated in the cm directory under **$GAUSSLOG**.

## log_file_size

**Parameter description**: controls the log file size. When the log file size reaches a specified size, a new log file will be created to record logs. 

**Value range**: Integer. The value ranges from 0 to 2047. The unit is MB. The modification takes effect only after the cm_agent is restarted. For parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **16 MB**

## log_min_messages

**Parameter description**: controls the level of messages written to the CM_agent log file. Each level contains information at all levels following it. The lower the level, the fewer messages are recorded in the server run logs.

**Value range**: Enumeration. The value can be debug5, debug1, warning, error, log, and fatal. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **warning**

## incremental_build

**Parameter description**: controls whether the mode of reconstructing the standby DN is incremental. If this function is enabled, the standby DN is incrementally recreated. Otherwise, the standby DN is fully recreated.

**Value range**: Boolean. The value can be **on** and **off**. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **on**

## alarm_component

**Parameter description**: sets the location of the alarm component for handling alarms.

**Value range**: String. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- If alarm-type is set to 5 in the preinstall script gs_preinstall, no third-party component is connected to the system and alarms are written to the system_alarm log. In this case, the value of the GUC parameter alarm_component is as follows: **/opt/snas/bin/snas_cm_cmd**.
- If alarm-type in the preinstall script gs_preinstall is set to 1, the system connects to a third-party component. In this case, the value of the GUC parameter alarm_component is the absolute path of the executable program of the third-party component.

**Default value**: **/opt/snas/bin/snas_cm_cmd**

## alarm_report_interval

**Parameter description**: specifies the interval for an alarm to be reported. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Non-negative integer (s)

**Default value**: **1**

## alarm_report_max_count

**Parameter description**: specifies the maximum number of times for an alarm to be reported. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Non-negative integer

**Default value**: **1**

## agent_report_interval

**Parameter description**: specifies the interval at which cm_agent reports the instance status. 

**Value range**: Integer (s). The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## agent_phony_dead_check_interval

**Parameter description**: specifies the interval at which cm_agent detects whether the DN process is dead. 

**Value range**: Integer (s). The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10**

## agent_check_interval

**Parameter description**: specifies the interval at which cm_agent checks the DN or instance status. 

**Value range**: Integer (s). The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **2**

## agent_heartbeat_timeout

**Parameter description**: specifies the cm_server heartbeat timeout duration. 

**Value range**: Integer (s). The value ranges from 2 to 231 minus 1. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **8**

## agent_connect_timeout

**Parameter description**: specifies the timeout duration for cm_agent to connect cm_server.

**Value range**: Integer (s). The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## agent_connect_retries

**Parameter description**: specifies the number of times for cm_agent to connect cm_server.

**Value range**: Integer. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **15**

## agent_kill_instance_timeout

**Parameter description**: specifies the interval at which cm_agent kills all instances on the local node when it fails to connect the cm_server node.

**Value range**: Integer. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0** indicating that cm_agent does not kill all instances on the local node. 

## security_mode

**Parameter description**: controls whether the DN is started in safe mode. If this parameter is set to **on**, the DN is started in safe mode. 

**Value range**: Boolean. The value can be **on** and **off**. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **off**

## upgrade_from

**Parameter description**: used during the local upgrade. It indicates the internal version of the database before the upgrade. You cannot manually change this parameter.

**Value range**: Non-negative integer. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **0**

## process_cpu_affinity

**Parameter description**: controls whether to start the main DN process in kernel binding optimization mode. If this parameter is set to **0**, no kernel binding optimization is performed. Otherwise, perform core binding optimization and set the number of physical CPUs to 2n. The modification takes effect after the database and CM_agent are restarted. Only ARM is supported. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Integer. The value ranges from 0 to 2. 

**Default value**: **0**

## log_threshold_check_interval

**Parameter description**: specifies the interval at which logs are compressed and cleared every 1800 seconds.

**Value range**: Integer (s). The value ranges from 0 to 2147483647. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1800**

## dilatation_shard_count_for_disk_capacity_alarm

**Parameter description**: sets the number of newly added fragments to calculate the threshold for reporting disk capacity alarms in capacity expansion scenarios.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note**: The number of fragments must be consistent with the actual number of fragments.

**Value range**: Integer. The value ranges from 0 to 232 minus 1. If the parameter is set to 0, disk expansion alarm reporting is disabled. If this parameter is set to a value greater than 0, disk expansion alarm reporting is enabled, and the alarm reporting threshold is calculated based on the number of fragments. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **1**

## log_max_size

**Parameter description**: controls the maximum log storage size. 

**Value range**: Integer (MB). The value ranges from 0 to 2147483647. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10240**

## log_max_count

**Parameter description**: specifies the maximum number of logs that can be stored on a hard disk.

**Value range**: Integer. The value ranges from 0 to 10000. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **10000**

## log_saved_days

**Parameter description**: specifies the number of days for which logs can be saved. 

**Value range**: Integer (day). The value ranges from 0 to 1000. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: **90**

## enable_log_compress

**Parameter description**: controls the log compression function. 

**Value range**: Boolean. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **on** indicates that logs can be compressed. 
- **off** indicates that logs cannot be compressed. 

**Default value**: **on**

## log_pattern_%s

**Parameter description**: specifies the log compression format when enable_log_compress is set to **on** and compresses all logs containing %s which is the server tool name. 

**Value range**: [Table 1](#1) lists the values of all parameters. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: Default values are not allowed to be changed. 

**Table 1** Parameters and their default values <a id="1"> </a>

| Parameter (log_pattern_%s)       | Default Value         |
| :------------------------------- | :-------------------- |
| log_pattern_cm_ctl               | cm_ctl-               |
| log_pattern_gs_clean             | gs_clean-             |
| log_pattern_gs_ctl               | gs_ctl-               |
| log_pattern_gs_guc               | gs_guc-               |
| log_pattern_gs_dump              | gs_dump-              |
| log_pattern_gs_dumpall           | gs_dumpall-           |
| log_pattern_gs_restore           | gs_restore-           |
| log_pattern_gs_initcm            | gs_initcm-            |
| log_pattern_gs_initdb            | gs_initdb-            |
| log_pattern_gs_initgtm           | gs_initgtm-           |
| log_pattern_gtm_ctl              | gtm_ctl-              |
| log_pattern_cm_agent             | cm_agent-             |
| log_pattern_system_call          | system_call-          |
| log_pattern_cm_server            | cm_server-            |
| log_pattern_om_monitor           | om_monitor-           |
| log_pattern_gs_local             | gs_local-             |
| log_pattern_gs_preinstall        | gs_preinstall-        |
| log_pattern_gs_install           | gs_install-           |
| log_pattern_gs_replace           | gs_replace-           |
| log_pattern_gs_uninstall         | gs_uninstall-         |
| log_pattern_gs_om                | gs_om-                |
| log_pattern_gs_upgradectl        | gs_upgradectl-        |
| log_pattern_gs_expand            | gs_expand-            |
| log_pattern_gs_shrink            | gs_shrink-            |
| log_pattern_gs_postuninstall     | gs_postuninstall-     |
| log_pattern_gs_backup            | gs_backup-            |
| log_pattern_gs_checkos           | gs_checkos-           |
| log_pattern_gs_collector         | gs_collector-         |
| log_pattern_GaussReplace         | GaussReplace-         |
| log_pattern_GaussOM              | GaussOM-              |
| log_pattern_gs_checkperf         | gs_checkperf-         |
| log_pattern_gs_check             | gs_check-             |
| log_pattern_roach-agent          | roach-agent-          |
| log_pattern_roach-controller     | roach-controller-     |
| log_pattern_postgresql           | postgresql-           |
| log_pattern_gtm                  | gtm-                  |
| log_pattern_sessionstat          | sessionstat-          |
| log_pattern_sync                 | sync-                 |
| log_pattern_system_alarm         | system_alarm-         |
| log_pattern_pg_perf              | pg_perf-              |
| log_pattern_slow_query_log       | slow_query_log-       |
| log_pattern_asp                  | asp-                  |
| log_pattern_etcd                 | etcd-                 |
| log_pattern_gs_cgroup            | gs_cgroup-            |
| log_pattern_pscp                 | pscp-                 |
| log_pattern_postgresql-query-log | postgresql-query-log- |
| log_pattern_gs_hotpatch          | gs_hotpatch-          |
| log_pattern_pssh                 | pssh-                 |

## agent_backup_open

**Parameter description**: Database instance disaster and recovery settings. After this function is enabled, CM runs in database instance disaster and recovery mode.

**Value range**: Integer. The value can be **0** and **1**. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **0** indicates that the function is disabled. 
- **1** indicates that the function is enabled.

**Default value**: **0**

## enable_xc_maintenance_mode

**Parameter description**: controls whether the PGXC_node system table can be modified when database instances are in read-only mode.

**Value range**: Boolean. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **on** indicates that the PGXC_node system table can be modified.
- **off** indicates that the PGXC_node system table cannot be modified.

**Default value**: **on**

## unix_socket_directory

**Parameter description**: specifies the directory for storing the unix sockets.

**Value range**: String. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value:** ”

## enable_dcf

**Parameter description**: specifies whether to enable the DCF mode.

**Value range**: Boolean. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **0** indicates that the DCF mode is disabled.
- **1** indicates that the DCF mode is enabled.

**Default value**: **0**

## disaster_recovery_type

**Parameter description**: specifies the type of the disaster and recovery relationship between the primary and standby database instances.

**Value range**: Integer. The value ranges from 0 to 2. The modification takes effect after cm_agent is restarted. For details about parameter modification, see [Table 2 Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

- **0** indicates that the disaster and recovery relationship is not established. 
- **1** indicates that the OBS disaster and recovery relationship. 
- **2** indicates the streaming disaster and recovery relationship.

**Default value**: **0**