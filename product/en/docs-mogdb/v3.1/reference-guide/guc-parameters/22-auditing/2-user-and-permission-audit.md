---
title: User and Permission Audit
summary: User and Permission Audit
author: Zhang Cuiping
date: 2021-04-20
---

# User and Permission Audit

## audit_login_logout

**Parameter description**: Specifies whether to audit the MogDB user's login (including login success and failure) and logout.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 7

- **0** indicates that the function of auditing users' logins and logouts is disabled.
- **1** indicates that only successful user logins are audited.
- **2** indicates that only failed user logins are audited.
- **3** indicates that successful and failed user logins are audited.
- **4** indicates that only user logouts are audited.
- **5** indicates that successful user logouts and logins are audited.
- **6** indicates that failed user logouts and logins are audited.
- **7** indicates that successful user logins, failed user logins, and logouts are audited.

**Default value**: **7**

## audit_database_process

**Parameter description**: Specifies whether to audit the MogDB user's login (including login success and failure) and logout.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing MogDB start, stop, recovery, and switchover operations of a database is disabled.
- **1** indicates that the function of auditing MogDB start, stop, recovery, and switchover operations of a database is enabled.

**Default value**: **1**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note**:
>
> When MogDB starts, DN performs the switchover process. Therefore, when DN starts, the type in the audit log is system_switch.

## audit_user_locked

**Parameter description**: Specifies whether to audit the MogDB user's locking and unlocking.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing user's locking and unlocking is disabled.
- **1** indicates that the function of auditing user's locking and unlocking is enabled.

**Default value**: **1**

## audit_user_violation

**Parameter description**: Specifies whether to audit the access violation operations of a user.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing the access violation operations of a user is disabled.
- **1** indicates that the function of auditing the access violation operations of a user is enabled.

**Default value**: **0**

## audit_grant_revoke

**Parameter description**: Specifies whether to audit the granting and reclaiming of the MogDB user's permission.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing the granting and reclaiming of a user's permission is disabled.
- **1** indicates that the function of auditing the granting and reclaiming of a user's permission is enabled.

**Default value**: **1**
