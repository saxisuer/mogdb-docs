---
title: Standby Server
summary: Standby Server
author: Zhang Cuiping
date: 2021-04-20
---

# Standby Server

## hot_standby

**Parameter description**: Specifies whether to allow connections and queries on a standby server during its recovery.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If this parameter is set to **on**, [wal_level](../../../reference-guide/guc-parameters/6-write-ahead-log/1-settings.md#wal_level) must be set to **hot_standby**. Otherwise, the database startup fails.
> - In an HA system, **hot_standby** cannot be set to **off**, because this setting can affect other features of the HA system.
> - If the **hot_standby** parameter was disabled and the **wal_level** parameter was set to a value lower than **hot_standby**, perform the following operations to ensure that the logs to be replayed on the standby node can be queried on the standby node before enabling the **hot_standby** parameter again: 
>   1. Change the **wal_level** value of the primary and standby nodes to **hot_standby** or higher, and restart the instances for the change to take effect. 
>   2. Perform the checkpoint operation on the primary node and query the **pg_stat_get_wal_senders()** function to ensure that the **receiver_replay_location** value of each standby node is the same as the **sender_flush_location** value of the primary node. Ensure that the value adjustment of **wal_level** is synchronized to standby nodes and takes effect, and standby nodes do not need to replay low-level logs. 
>   3. Set the **hot_standby** parameter of the primary and standby nodes to **on**, and restart the instances for the setting to take effect.

**Value range**: Boolean

- **on** indicates that connections and queries are allowed on the standby server during the recovery.
- **off** indicates that connections and queries are not allowed on the standby server during the recovery.

**Default value**: **on**

## max_standby_archive_delay

**Parameter description**: Specifies the wait period before queries on a standby server are canceled when the queries conflict with WAL processing and archiving in hot standby mode.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> **-1** indicates that the standby server waits until the conflicting queries are complete.

**Value range**: an integer ranging from -1 to *INT_MAX*. The unit is ms.

**Default value**: **3s** (3000 ms)

## max_standby_streaming_delay

**Parameter description**: Specifies the wait period before queries on a standby server are canceled when the queries conflict with WAL data receiving through streaming replication in hot standby mode.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> **-1** indicates that the standby server waits until the conflicting queries are complete.

**Value range**: an integer ranging from -1 to *INT_MAX*. The unit is ms.

**Default value**: **3s** (3000 ms)

## wal_receiver_status_interval

**Parameter description**: Specifies the maximum interval for notifying the primary server of the WAL Receiver status.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is ms.

**Default value**: **5s** (5000 ms)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** If this parameter is set to **0**, the standby server does not send information, such as the log receiving location, to the primary server. As a result, the transaction commit on the primary server may be blocked, and the switchover may fail. In normal service scenarios, you are not advised to set this parameter to **0**.

## hot_standby_feedback

**Parameter description**: Specifies whether a standby server is allowed to send the result of a query performed on it to the primary server, preventing a query conflict.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the ID of the oldest transaction active on standby servers will be sent to the primary server.
- **off** indicates that the ID of the oldest transaction active on standby servers will not be sent to the primary server.

**Default value**: **off**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> If this parameter is set to **on**, VACUUM on the primary server will not clean up tuples modified in transactions later than the oldest transaction active on standby servers. Therefore, the performance of the primary server will be affected.

## wal_receiver_timeout

**Parameter description:** Specifies the maximum wait period for a standby server to receive data from the primary server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is ms.

**Default value**: **6s** (6000 ms)

## wal_receiver_connect_timeout

**Parameter description**: Specifies the timeout period for a standby server to connect to the primary server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*/1000. The unit is s.

**Default value**: **2s**

## wal_receiver_connect_retries

**Parameter description**: Specifies the maximum attempts that a standby server connects to the primary server

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *INT_MAX*

**Default value**: **1**

## wal_receiver_buffer_size

**Parameter description**: Specifies the size of the memory buffer that stores the Xlogs received by the standby and secondary nodes. Currently, primary/standby/secondary deployment is not supported by default.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 4096 to 1047552. The unit is KB.

**Default value**: **64MB** (65536 KB)

## primary_slotname

**Parameter description**: Specifies the slot name of the primary server corresponding to a standby server. This parameter is used for the mechanisms to verify the primary-standby relationship and delete WALs.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: empty

## max_logical_replication_workers

**Parameter description**: Specifies the maximum number of apply worker threads on the subscriber side.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 262143

**Default value:** **4**

## max_sync_workers_per_subscription

**Parameter description**: Specifies the maximum number of tablesync worker threads for each subscription on the subscriber side.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../30-appendix.md).

**Value range**: an integer ranging from 0 to 262143

**Default value**: **2**