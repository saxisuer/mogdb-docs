---
title: Alarm Detection
summary: Alarm Detection
author: Zhang Cuiping
date: 2021-04-20
---

# Alarm Detection

During the running of the MogDB, error scenarios can be detected and informed to users in time.

## enable_alarm

**Parameter description**: Specifies whether to enable the alarm detection thread to detect fault scenarios that may occur in the database.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](./30-appendix.md).

**Value range**: Boolean

- **on** indicates that the alarm detection thread is enabled.
- **off** indicates that the alarm detection thread is disabled.

**Default value**: **on**

## connection_alarm_rate

**Parameter description**: Specifies the ratio restriction on the maximum number of allowed parallel connections to the database. The maximum number of concurrent connections to the database is **max_connections** x **connection_alarm_rate**.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** a floating point number ranging from 0.0 to 1.0

**Default value**: **0.9**

## alarm_report_interval

**Parameter description**: specifies the interval at which an alarm is reported.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer. The unit is s.

**Default value:** **10**

## alarm_component

**Parameter description**: Certain alarms are suppressed during alarm reporting. That is, the same alarm will not be repeatedly reported by an instance within the period specified by **alarm_report_interval**. Its default value is **10s**. In this case, the parameter specifies the location of the alarm component that is used to process alarm information.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **/opt/snas/bin/snas_cm_cmd**

## table_skewness_warning_threshold

**Parameter description**: Specifies the threshold for triggering a table skew alarm.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a floating point number ranging from 0 to 1

**Default value**: **1**

## table_skewness_warning_rows

**Parameter description**: Specifies the minimum number of rows for triggering a table skew alarm.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*

**Default value**: **100000**
