---
title: Appendices
summary: Appendices
author: Zhang Cuiping
date: 2021-04-20
---

# Appendices

## **Table 1** GUC parameters<a id="GUC parameters"> </a>

| Parameter type. | Remarks                                                      | How to Set                                                   |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| INTERNAL        | Fixed parameter. It is set during database creation and cannot be modified. Users can only view the parameter by running the **SHOW** command or in the **pg_settings** view. | None                                                         |
| POSTMASTER      | Database server parameter. It can be set when the database is started or in the configuration file. | Methods 1 and 4 in Table 2 [Methods for setting GUC parameters](30-appendix.md). |
| SIGHUP          | Global database parameter. It can be set when the database is started or be modified later. | Methods 1, 2, and 4 in Table 2 [Methods for setting GUC parameters](30-appendix.md). |
| BACKEND         | Session connection parameter. It is specified during session connection creation and cannot be modified after that. The parameter setting becomes invalid when the session is disconnected. This is an internal parameter and not recommended for users to set it. | Methods 1, 2, and 4 in Table 2 [Methods for setting GUC parameters](30-appendix.md).<br />NOTE: The parameter setting takes effect when the next session is created. |
| SUSET           | Database administrator parameter. It can be set by common users when or after the database is started. It can also be set by database administrators using SQL statements. | Method 1 or 2 by a common user, or method 3 by a database administrator in Table 2 [Methods for setting GUC parameters](30-appendix.md). |
| USERSET         | Common user parameter. It can be set by any user at any time. | Method 1, 2, and 3 in Table 2 [Methods for setting GUC parameters](30-appendix.md). |

## **Table 2** Methods for setting GUC parameters <a id="Methods for setting GUC parameters"> </a>

| No.      | Description                                                  |
| -------- | ------------------------------------------------------------ |
| Method 1 | 1. Run the following command to set a parameter:<br />`gs_guc set -D datadir -c "paraname=value"`<br />NOTE:<br />If the parameter is a string variable, use **-c parameter="'value'"** or **-c "parameter = 'value'"**.<br />Run the following command to set a parameter for database nodes at the same time:<br />`gs_guc set -N all -I all -c "paraname=value"`<br />Run the following commands to set a cm_agent parameter for database nodes:<br />`gs_guc set -Z cmagent -c "paraname=value"` <br />`gs_guc set -Z cmagent -N all -I all -c "paraname=value"`<br />Run the following commands to set a cm_server parameter for database nodes: <br />`gs_guc set -Z cmserver -c "paraname=value"` <br />`gs_guc set -Z cmserver -N all -I all -c "paraname=value"` <br />2. Restart the database to make the setting take effect.<br />NOTE:<br />Restarting MogDB results in operation interruption. Properly plan the restart to avoid affecting users.<br />`gs_om -t stop && gs_om -t start` |
| Method 2 | `gs_guc reload -D datadir -c "paraname=value"`<br />NOTE:<br />Run the following command to set a parameter for database nodes at the same time:<br />`gs_guc reload -N all -I all -c "paraname=value"` |
| Method 3 | Set parameters at database, user, or session levels.<br />- Set a database-level parameter.<br />`MogDB=# ALTER DATABASE dbname SET paraname TO value;`<br />The setting takes effect in the next session.<br />- Set a user-level parameter.<br />`MogDB=# ALTER USER username SET paraname TO value;`<br />The setting takes effect in the next session.<br />- Set a session-level parameter.<br />`MogDB=# SET paraname TO value;`<br />Parameter value in the current session is changed. After you exit the session, the setting becomes invalid.<br />Note:<br />Session-level parameters set by SET have the highest priority, followed by parameters set by ALTER. Parameter values set by ALTER DATABASE have a higher priority than those set using ALTER USER. Priorities of the first three methods are all higher than those of gs_guc. |
| Method 4 | Use ALTER SYSTEM SET to modify database parameters.<br />- Set a POSTMASTER-level parameter<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />The setting takes effect after the system is restarted.<br />- Set a SIGHUP-level parameter.<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />The setting takes effect immediately. (Actually, there is a slight delay to wait for the thread reloading the parameter.)<br />- Set a BACKEND-level parameter.<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />The setting takes effect in the next session. |
