---
title: AI Features
summary: AI Features
author: Zhang Cuiping
date: 2021-11-08
---

# AI Features

## enable_hypo_index

**Parameter description**: Specifies whether the database optimizer considers the created virtual index when executing the **EXPLAIN** statement. By executing **EXPLAIN** on a specific query statement, you can evaluate whether the index can improve the execution efficiency of the query statement based on the execution plan provided by the optimizer.

This parameter is a **USERSET** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that a virtual index is created during **EXPLAIN** execution.
- **off** indicates that no virtual index is created during **EXPLAIN** execution.

**Default value**: **off**

## db4ai_snapshot_mode

**Parameter description**: There are two snapshot modes: MSS (materialized mode, storing data entities) and CSS (computing mode, storing incremental information).

This parameter is a **USERSET** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, which can be **MSS** or **CSS**

- **MSS** indicates the materialized mode. The DB4AI stores data entities when snapshots are created.
- **CSS** indicates the computing mode. The DB4AI stores incremental information when creating snapshots.

**Default value:** **MSS**

## db4ai_snapshot_version_delimiter

**Parameter description**: Specifies the delimiter for the snapshot version of a data table.

This parameter is a **USERSET** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, consisting of one or more characters.

**Default value**: **@**

## db4ai_snapshot_version_separator

**Parameter description**: Specifies the subversion delimiter of a data table snapshot.

This parameter is a **USERSET** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, consisting of one or more characters.

**Default value**: .

## unix_socket_directory

**Parameter description:** Specifies the path for storing files in the unix_socket communication mode. You can set this parameter only in the configuration file **postgresql.conf**. Before enabling the fenced mode, you need to set this GUC parameter.

This parameter is a **POSTMASTER** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string of 0 or more characters

**Default value:** **"**
