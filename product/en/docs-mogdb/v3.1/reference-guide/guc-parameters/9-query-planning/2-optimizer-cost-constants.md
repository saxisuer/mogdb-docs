---
title: Optimizer Cost Constants
summary: Optimizer Cost Constants
author: Zhang Cuiping
date: 2021-04-20
---

# Optimizer Cost Constants

This section describes the optimizer cost constants. The cost variables described here are measured on an arbitrary scale. Only their relative values matter, therefore scaling them all up or down by the same factor will result in no change in the optimizer's choices. By default, these cost variables are based on the cost of sequential page fetches, that is, **seq_page_cost** is conventionally set to **1.0** and the other cost variables are set with reference to the parameter. However, you can use a different scale, such as actual execution time in milliseconds.

## seq_page_cost

**Parameter description**: Specifies the optimizer's estimated cost of a disk page fetch that is part of a series of sequential fetches.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value**: **1**

## random_page_cost

**Parameter description**: Specifies the optimizer's estimated cost of an out-of-sequence disk page fetch.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Although the server allows you to set **random_page_cost** to a value less than that of **seq_page_cost**, it is not physically sensitive to do so. However, setting them equal makes sense if the database is entirely cached in RAM, because in that case there is no penalty for fetching pages out of sequence. Also, in a heavily-cached database you should lower both values relative to the CPU parameters, since the cost of fetching a page already in RAM is much smaller than it would normally be.

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value**: **4**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - This value can be overwritten for tables and indexes in a particular tablespace by setting the tablespace parameter of the same name.
> - Reducing this value relative to **seq_page_cost** will cause the system to prefer index scans and raising it will make index scans relatively more expensive. You can increase or decrease both values together to change the disk I/O costs relative to CPU costs.

## cpu_tuple_cost

**Parameter description**: Specifies the optimizer's estimated cost of processing each row during a query.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value**: **0.01**

## cpu_index_tuple_cost

**Parameter description**: Specifies the optimizer's estimated cost of processing each index entry during an index scan.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value:** **0.005**

## cpu_operator_cost

**Parameter description**: Specifies the optimizer's estimated cost of processing each operator or function executed during a query.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value**: **0.0025**

## effective_cache_size

**Parameter description**: Specifies the optimizer's assumption about the effective size of the disk cache that is available to a single query.

When setting this parameter you should consider both MogDB's shared buffers and the kernel's disk cache. Also, take into account the expected number of concurrent queries on different tables, since they will have to share the available space.

This parameter has no effect on the size of shared memory allocated by MogDB. It is used only for estimation purposes and does not reserve kernel disk cache. The value is in the unit of disk page. Usually the size of each page is 8192 bytes.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *INT_MAX*. The unit is 8 KB.

A value greater than the default one makes index scans more likely to be used, whereas a value less than the default one makes sequential scans more likely to be used.

**Default value**: **128MB**

## allocate_mem_cost

**Parameter description**: Specifies the query optimizer's estimated cost of creating a hash table for memory space using hash join. This parameter is used for optimization when the hash join estimation is inaccurate.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a floating point number ranging from 0 to *DBL_MAX*

**Default value**: **0**
