---
title: Introduction
summary: Introduction
author: Zhang Cuiping
date: 2021-04-20
---

## Introduction

This section describes the method configuration, cost constants, planning algorithm, and some configuration parameters for the optimizer.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> Two parameters are involved in the optimizer:
>
> - *INT_MAX* indicates the maximum value of the INT data type. The value is **2147483647**.
> - *DBL_MAX* indicates the maximum value of the FLOAT data type.
