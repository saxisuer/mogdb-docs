---
title: Replication Parameters of Two Database Instances
summary: Replication Parameters of Two Database Instances
author: zhang cuiping
date: 2022-10-24
---

# Replication Parameters of Two Database Instances

## RepOriginId

**Parameter description**: This parameter is a session-level GUC parameter. In bidirectional logical replication, set it to a non-zero value to avoid infinite data replication.

This parameter is a USERSET parameter. Set it based on **Method 3** provided in Table 1 [GUC parameters](./30-appendix.md).

**Value range**: an integer ranging from 0 to 2147483647

**Default value**: **0**