---
title: Parameters Related to Efficient Data Compression Algorithm
summary: Parameters Related to Efficient Data Compression Algorithm
author: zhang cuiping
date: 2022-11-01
---

# Parameters Related to Efficient Data Compression Algorithms

## pca_shared_buffer

**Parameter description:** Similar to shared_buffers, this parameter is used to set the size of the page compression block address mapping management buffer.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](./30-appendix.md).

**Value range:** 64 KB to 16 GB.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** 
>
> - If the value is less than 64 KB, an error is reported. 
> - If the value is greater than 16 GB, the parameter can be set successfully. However, in actual running, the automatic memory running is set to 16 GB. 
> - If the parameter does not contain a unit, the default value is 8 KB (the size of a page is 8 KB) multiplied by the parameter value.

**Default value:** **64K**