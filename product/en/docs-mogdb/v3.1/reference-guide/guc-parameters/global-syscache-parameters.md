---
title: Global SysCache Parameters
summary: Global SysCache Parameters
author: Guo Huan
date: 2022-05-23
---

# Global SysCache Parameters

## enable_global_syscache

**Parameter description**: Specifies whether to enable the global system cache function. This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC Parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the global system cache function is enabled.
- **off** indicates that the global system cache function is disabled.

**Default value**: **on**

You are advised to use this parameter together with the thread pool parameter. After this parameter is enabled, you are advised to set **wal_level** of the standby node to **hot_standby** or higher if you need to access the standby node.

## global_syscache_threshold

**Parameter description**: Specifies the maximum memory usage of the global system cache.

This parameter is a PGC_SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC Parameters](30-appendix.md).

The **enable_global_syscache** parameter must be enabled.

**Value range**: an integer ranging from 16384 to 1073741824. The unit is KB.

**Default value**: **163840**

Recommended calculation formula: The smaller value of the number of hot databases and the number of threads x Memory size allocated to each database.

That is, **global_syscache_threshold** = **min(count(hot dbs)**, **count(threads))** x **memofdb**.

The number of hot databases refers to the number of frequently accessed databases. In thread pool mode, the number of threads is the sum of the number of threads in the thread pool and the number of background threads. In non-thread pool mode, the number of hot databases is used.

**memofdb** indicates the average memory allocated to each database. The background noise memory of each database is 2 MB. Each time a table or index is added, 11 KB memory is added.

If this parameter is set to a small value, memory is frequently evicted, and a large number of memory fragments cannot be recycled. As a result, memory control fails.