---
title: GAUSS-05101 - GAUSS-05200
summary: GAUSS-05101 - GAUSS-05200
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05101 - GAUSS-05200

<br/>

## GAUSS-05101 - GAUSS-05110

<br/>

GAUSS-05101: "Could not alloc new memory."

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05102: "Can't get group member"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05103: "null type_loc is invalid"

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05104: "PGXC node %s: cannot be a central node, it has to be a Coordinator"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05105: "the replication type should be multi-standby, now is %d"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05106: "DN matric cannot alloc memory."

SQLSTATE: 53000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05107: "PGXC Node %s(host = %s, port = %d, sctp_port = %d, control_port = %d): object already defined"

SQLSTATE: 42710

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05108: "Datanode %s: can not be central node."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05109: "PGXC node %s is central node already."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05110: "parameter requires a Boolean value"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05111 - GAUSS-05120

<br/>

GAUSS-05112: "Get buckets failed.reason:the buckets number(%d) is not correct(%d)."

SQLSTATE: D0011

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05113: "group_members is null for tuple %u"

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05116: "Can not find redistributed source group with in_redistribution 'y'."

SQLSTATE: 42704

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05119: "NodeGroup name %s can not be preserved group name"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05121 - GAUSS-05130

<br/>

GAUSS-05126: "group_name can not be NULL "

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05131 - GAUSS-05140

<br/>

GAUSS-05133: "The installation group has no members."

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05137: "No destgroup in resize process"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05138: "NodeGroup name %s is invalid."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05141 - GAUSS-05150

<br/>

GAUSS-05145: "cannot drop '%s' because other objects depend on it"

SQLSTATE: 2BP01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05150: "cannot drop '%s' because at least one role %u depend on it"

SQLSTATE: 2BP01

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-05151 - GAUSS-05160

<br/>

GAUSS-05152: "default_storage_nodegroup %s not defined."

SQLSTATE: 42710

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05153: "Bucketmap is not found with given groupoid %u"

SQLSTATE: 42704

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05155: "Connection to database failed: %s"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05156: "execute statement: %s failed: %s"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05157: "PQtuples num is invalid : %d"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05158: "fail to get tables in database %s for query remain table"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05160: "Invalid null pointer attribute for gs_get_nodegroup_tablecount()"

SQLSTATE: 42P24

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05161 - GAUSS-05170

<br/>

AUSS-05161: "can not open pg_database"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05162: "count is invalid, count:%d, tmpCount:%d"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05163: "must be system admin or operator admin in operation mode to manage pooler"

SQLSTATE: 42501

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05164: "pg_pool_ping cannot run inside a transaction block"

SQLSTATE: 25001

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05165: "invalid input/output buffer in node handle"

SQLSTATE: 53200

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05166: "out of memory for node handles"

SQLSTATE: 53200

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05167: "coordinator cannot identify itself"

SQLSTATE: XX000

Description: An internal system error occurs. The **pgxc_node** system catalog may have been damaged or the user has modified the **pgxc_node_name** parameter of the failed node.

Solution: It is not recommended that users modify the **pgxc_node_name** parameter. If the fault is caused by incorrect parameter modification, perform the following operations: 1. Enable the CN in restore mode. Obtain the nodename by querying the **pgxc_node system** table. Restore the configuration parameter based on the nodename obtained. 2. Restore the configuration parameter using the standard log name of this instance generated in OM mode. "

GAUSS-05168: "message len is too short"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05169: "conn cursor overflow"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05170: "Invalid Datanode number"

SQLSTATE: 53200

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05171 - GAUSS-05180

<br/>

GAUSS-05171: "Invalid coordinator number"

SQLSTATE: 53200

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05172: "failed to get pooled connections"

SQLSTATE: 53000

Description: Failed to request connections from the **pooler manager** thread.

Solution: Locate the root cause by viewing the **pooler manager** logs close to the error log. Perform restoration based on such logs.

GAUSS-05173: "replication type is invalid in PGXCNodeGetNodeId (nodeoid = %u, node_type = %c"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05174: "Failed to connect to the compute pool. See log file for more details."

SQLSTATE: 08006

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05175: "Write to replicated table returned different results from the Datanodes on current DN:%s and previous DN:%s."

SQLSTATE: XX001

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05176: "Unexpected TUPDESC response from Datanode"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05177: "Failed to send queryid to %s"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05178: "Failed to send queryid to %s before PREPARE command"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05179: "failed to send %s command to node %u"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05180: "Failed to COMMIT the transaction on nodes: %s."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05181 - GAUSS-05190

<br/>

GAUSS-05181: "Can not find location info for relation oid: %u"

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05182: "foreignPrivateList is NULL"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05183: "Could not send user pl to CN of the compute pool: %s."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05184: "Could not begin transaction on Datanodes."

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05185: "Failed to read response from Datanodes"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05186: "FATAL state of connection to datanode %u"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05187: "No coordinator nodes defined in cluster"

SQLSTATE: 42704

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05188: "Failed to send snapshot to %s"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05189: "Failed to send command to %s"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05190: "Could not begin transaction on Coordinator nodes"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05191 - GAUSS-05200

<br/>

GAUSS-05191: "Failed to send command to coordinators"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05192: "Unexpected response from coordinator"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05193: "TUPDESC message has not been received before DATAROW message from %s"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05194: "Failed to close Datanode statement"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05195: "Unsupport DML two phase commit under gtm free mode."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05196: "from remote cluster: %s"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05197: "10655"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05198: "Could not begin transaction on Datanode."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05199: "Fetch statistics from myself is unexpected. Maybe switchover happened."

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05200: "cooperation analysis: please update to the same version"

SQLSTATE: 08P01

Description: Internal system error.

Solution:Contact technical support.
