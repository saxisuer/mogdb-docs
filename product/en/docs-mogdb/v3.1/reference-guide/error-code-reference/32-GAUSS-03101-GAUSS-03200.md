---
title: GAUSS-03101 - GAUSS-03200
summary: GAUSS-03101 - GAUSS-03200
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03101 - GAUSS-03200

<br/>

## GAUSS-03101 - GAUSS-03110

<br/>

GAUSS-03101: "wrong number of array subscripts"

SQLSTATE: 2202E

Description: The array using variable-length elements must be one-dimensional. An error occurs when you replace array elements with variable-length elements of a multiple-dimensional array.

Solution: Check the input parameter.

GAUSS-03102: "array subscript out of range"

SQLSTATE: 2202E

Description: The one-dimensional index value exceeds the value range allowed for the elements contained in the array.

Solution: Check the input parameter.

GAUSS-03103: "cannot assign null value to an element of a fixed-length array"

SQLSTATE: 22004

Description:**NULL** value cannot be assigned to an element of a fixed-length array.

Solution: Check the input parameter.

GAUSS-03104: "updates on slices of fixed-length arrays not implemented"

SQLSTATE: 0A000

Description: Updates are not supported on arrays that use fixed-length elements.

Solution: Check the input parameter.

GAUSS-03105: "source array too small"

SQLSTATE: 2202E

Description: There is no element of the index number to be replaced in the source array.

Solution: Check the input parameter.

GAUSS-03106: "invalid nargs: %d"

SQLSTATE: XX000

Description: The array parameters are invalid.

Solution: Modify the array parameters in the query.

GAUSS-03107: "null input array"

SQLSTATE: XX000

Description: The array does not contain parameters.

Solution: Add valid parameters to the array.

GAUSS-03108: "null array element not allowed in this context"

SQLSTATE: 22004

Description: If the current array element flag is not **NULL**, the value of the array element cannot be **NULL**.

Solution: Check the input parameter.

GAUSS-03110: "invalid arguments to array_create_iterator"

SQLSTATE: XX000

Description: The parameter of the **array_create_iterator** function is invalid.

Solution: Modify the parameter of the **array_create_iterator** function.

<br/>

## GAUSS-03111 - GAUSS-03120

<br/>

GAUSS-03111: "dimension array or low bound array cannot be null"

SQLSTATE: 22004

Description: The array dimension and filled subscript cannot be **NULL**.

Solution: Check the input parameter.

GAUSS-03112: "could not determine data type of input"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03113: "wrong range of array subscripts"

SQLSTATE: 2202E

Description: The lower limit of the array dimension must be 1.

Solution: Check the input parameter.

GAUSS-03114: "dimension values cannot be null"

SQLSTATE: 22004

Description: Array dimension values cannot be **NULL**.

Solution: Check the input parameter.

GAUSS-03115: "unrecognized encoding: '%s'"

SQLSTATE: 22023

Description: Failed to find the encoding corresponding to the current string.

Solution: Check the input strings.

GAUSS-03116: "overflow - encode estimate too small"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03117: "overflow - decode estimate too small"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03118: "invalid hexadecimal digit: '%c'"

SQLSTATE: 22023

Description: Hexadecimal characters are invalid.

Solution: Check the input parameter. For the data value range, see en-us_topic_0237121926.html.

GAUSS-03119: "invalid hexadecimal data: odd number of digits"

SQLSTATE: 22023

Description: The number of digits of the converted hexadecimal characters is an odd number, which is invalid.

Solution: Check the input parameter.

GAUSS-03120: "unexpected '='"

SQLSTATE: 22023

Description: The equal sign (=) exists.

Solution: Check the input parameter.

<br/>

## GAUSS-03121 - GAUSS-03130

<br/>

GAUSS-03121: "invalid symbol"

SQLSTATE: 22023

Description: The input character is invalid.

Solution: Check the input parameter.

GAUSS-03122: "invalid end sequence"

SQLSTATE: 22023

Description: The string parsing is incomplete.

Solution: Check the input parameter.

GAUSS-03123: "'char' out of range"

SQLSTATE: 22003

Description: Overflows occur when int4 is converted to char.

Solution: Check the input parameter. For the data value range, see en-us_topic_0237121926.html.

GAUSS-03124: "invalid input syntax for type oid: '%s'"

SQLSTATE: 22P02

Description: A syntax error occurs when the input string is converted to the OID type.

Solution: Check the input parameter.

GAUSS-03125: "value '%s' is out of range for type oid"

SQLSTATE: 22003

Description: The value is out of the OID value range.

Solution: Check the input parameter.

GAUSS-03126: "oidvector has too many elements"

SQLSTATE: 22023

Description: The OID array has too many element values.

Solution: A maximum of 666 values are supported.

GAUSS-03127: "invalid oidvector data"

SQLSTATE: 22P03

Description: The type of the elements in the OID array is not OID.

Solution: Check the input parameter.

GAUSS-03129: "typmod array must be type cstring[]"

SQLSTATE: 2202E

Description: The type of the array processed in the current function must be cstring.

Solution: Check the element type of the array.

GAUSS-03130: "typmod array must be one-dimensional"

SQLSTATE: 2202E

Description: The array processed by the current function must be one-dimensional.

Solution: Check the array dimension.

<br/>

## GAUSS-03131 - GAUSS-03140

<br/>

GAUSS-03131: "typmod array must not contain nulls"

SQLSTATE: 22004

Description: The array element processed by the current function cannot be **NULL**.

Solution: Ensure that the array elements are not **NULL**.

GAUSS-03132: "unrecognized key word: '%s'"

SQLSTATE: 22P02

Description: The keyword of ACL can only be **group** or **user**.

Solution: Check whether the ACL keyword is correct.

GAUSS-03133: "missing name"

SQLSTATE: 22P02

Description: The keyword **group** or **user** does not have a name following them.

Solution: Ensure that the ACL syntax is correct.

GAUSS-03134: "missing '=' sign"

SQLSTATE: 22P02

Description: The equal sign (=) used for permission assignment for the group or user is missing.

Solution: Ensure that the ACL syntax is correct.

GAUSS-03135: "invalid mode character: must be one of '%s'"

SQLSTATE: 22P02

Description: The permission mode is invalid. The permission mode must be one of **arwdDxtXUCTc**.

Solution: Ensure that the ACL syntax is correct.

GAUSS-03136: "a name must follow the '/' sign"

SQLSTATE: 22P02

Description: To return the permission to a system administrator, a name must be specified after the slash (/).

Solution: Ensure that the ACL syntax is correct.

GAUSS-03137: "invalid size: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03138: "ACL array contains wrong data type"

SQLSTATE: 22023

Description: The ACL array contains non-ACL elements.

Solution: Ensure that the ACL array is valid.

GAUSS-03139: "ACL arrays must be one-dimensional"

SQLSTATE: 22023

Description: The ACL array can only be one-dimensional.

Solution: Ensure that the ACL array is valid.

GAUSS-03140: "ACL arrays must not contain null values"

SQLSTATE: 22004

Description: The ACL array cannot contain **NULL**.

Solution: Ensure that the ACL array is valid.

<br/>

## GAUSS-03141 - GAUSS-03150

<br/>

GAUSS-03141: "extra garbage at the end of the ACL specification"

SQLSTATE: 22P02

Description: Extra strings are left after the string to be converted to the ACL array is parsed.

Solution: Check the input strings.

GAUSS-03142: "unrecognized objtype abbreviation: %c"

SQLSTATE: XX000

Description: The input parameter of the **acldefault** function is incorrect.

Solution: Do not manually call the **acldefault** function or modify the first input parameter of the function.

GAUSS-03143: "grant options cannot be granted back to your own grantor"

SQLSTATE: 0LP01

Description: The permission is granted back to the original grantor.

Solution: Check the ACL rules.

GAUSS-03144: "dependent privileges exist"

SQLSTATE: 2BP01

Description: The permission to be deleted has dependency permissions.

Solution: Use the **cascade** keyword for cascading delete.

GAUSS-03145: "null ACL"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03146: "aclinsert is no longer supported"

SQLSTATE: 0A000

Description: The **aclinsert** function cannot be used.

Solution: Do not manually call the **aclinsert** function.

GAUSS-03147: "aclremove is no longer supported"

SQLSTATE: 0A000

Description: The **aclremove** function cannot be used.

Solution: Do not manually call the **aclremove** function.

GAUSS-03148: "unrecognized privilege type: '%s'"

SQLSTATE: 22023

Description: The parameters of the **makeaclitem** function are incorrect.

Solution: Do not manually call the **makeaclitem** function or modify the third parameter of the function.

GAUSS-03149: "unrecognized aclright: %d"

SQLSTATE: XX000

Description: The parameters of the **aclexplode** function are incorrect.

Solution: Do not manually call the **aclexplode** function or modify its parameter.

GAUSS-03150: "function '%s' does not exist"

SQLSTATE: 42883

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03151 - GAUSS-03160

<br/>

GAUSS-03151: "must be member of role '%s'"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03152: "int2vector has too many elements"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03153: "invalid int2vector data"

SQLSTATE: 22P03

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03156: "gin_tsquery_consistent requires eight arguments"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03157: "patternsel called for operator without a negator"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03158: "unrecognized consttype: %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03159: "join operator should take two arguments"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03161 - GAUSS-03170

<br/>

GAUSS-03161: "found unexpected null value in index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03162: "could not find RelOptInfo for given relids"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03163: "case insensitive matching not supported on type bytea"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03164: "could not determine which collation to use for ILIKE"

SQLSTATE: 42P22

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03165: "regular-expression matching not supported on type bytea"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03166: "unrecognized ptype: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03167: "missing support function %d for attribute %d of index '%s'"

SQLSTATE: XX000

Description: Certain operations cannot be performed to index columns.

Solution:Contact technical support.

GAUSS-03168: "could not match index to operand"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03169: "unsupported GIN indexqual type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03170: "invalid input value for enum %s: '%s'"

SQLSTATE: 22P02

Description: The input of **enum** is invalid.

Solution: Modify the input of **enum**.

<br/>

## GAUSS-03171 - GAUSS-03180

<br/>

GAUSS-03171: "invalid internal value for enum: %u"

SQLSTATE: 22P03

Description: The **enum** parameter is invalid.

Solution: Modify the **enum** parameter.

GAUSS-03172: "invalid argument for enum_recv"

SQLSTATE: XX000

Description: The **enum_recv** parameter is invalid.

Solution: Modify the **enum_recv** parameter.

GAUSS-03173: "could not determine actual enum type"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03174: "enum %s contains no values"

SQLSTATE: 55000

Description: No value is assigned to **enum**.

Solution: Assign a value to **enum**.

GAUSS-03175: "could not open tablespace directory '%s': %m"

SQLSTATE: XX000

Description: Failed to open the tablespace path.

Solution: Check whether the path corresponding to the tablespace is damaged.

GAUSS-03176: "Parameter value should not be negative."

SQLSTATE: 22023

Description: The parameter value cannot be negative.

Solution: Set the parameter to a positive value.

GAUSS-03177: "tablespace with OID %u does not exist"

SQLSTATE: XX000

Description: The result of checking user access rights in the user exit program indicates that the corresponding tablespace does not exist.

Solution: Check whether the **pg_tablespace** system catalog contains the tablespace definition.

GAUSS-03178: "database with OID %u does not exist"

SQLSTATE: XX000

Description: The result of checking user access rights indicates that the corresponding database does not exist.

Solution: Check whether the database status in the **pg_database** system catalog is normal.

GAUSS-03179: "SPI connect failure - returned %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03180: "failed to execute query '%s' on node '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03181 - GAUSS-03190

<br/>

GAUSS-03181: "array of weight must be one-dimensional"

SQLSTATE: 2202E

Description: The array of the first parameter of the **ts_rank_wtt** function is not a one-dimensional array.

Solution: Set the first parameter of the **ts_rank_wtt** function to a one-dimensional array.

GAUSS-03182: "array of weight is too short"

SQLSTATE: 2202E

Description: Among the parameters of the **ts_rank_wtt** function, the number of elements in a one-dimensional array is smaller than 4.

Solution: Ensure that the number of elements in the one-dimensional array is greater than or equal to 4.

GAUSS-03183: "array of weight must not contain nulls"

SQLSTATE: 22004

Description: The array elements of the first parameter of the **ts_rank_wtt** function contain **NULL** values.

Solution: Do not set **NULL** values for the array elements.

GAUSS-03184: "weight out of range"

SQLSTATE: 22023

Description: The array tuple of the first parameter of the **ts_rank_wtt** function is greater than 1.0.

Solution: Do not set the array tuple to a value greater than 1.

GAUSS-03185: "more than one function named '%s'"

SQLSTATE: 42725

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03186: "more than one operator named %s"

SQLSTATE: 42725

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03187: "missing argument"

SQLSTATE: 42601

Description: The parameter is missing when an operator is deleted.

Solution: Use the parameter defined for an operator when deleting the operator.

GAUSS-03188: "too many arguments"

SQLSTATE: 54023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03189: "expected a left parenthesis"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03190: "expected a right parenthesis"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03191 - GAUSS-03200

<br/>

GAUSS-03191: "expected a type name"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03192: "improper type name"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03193: "input of anonymous composite types is not implemented"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03194: "malformed record literal: '%s'"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03195: "wrong number of columns: %d, expected %d"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03196: "wrong data type: %u, expected %u"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03197: "improper binary format in record column %d"

SQLSTATE: 22P03

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03198: "cannot compare dissimilar column types %s and %s at record column %d"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03199: "cannot compare record types with different numbers of columns"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03200: "invalid input syntax for type tid: '%s'"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.
