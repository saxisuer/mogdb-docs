---
title: GAUSS-04901 - GAUSS-05000
summary: GAUSS-04901 - GAUSS-05000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04901 - GAUSS-05000

<br/>

## GAUSS-04901 - GAUSS-04910

<br/>

GAUSS-04901: "pg_hba.conf rejects replication connection for host '%s', user '%s', %s"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04902: "pg_hba.conf rejects replication connection for host '%s', user '%s'"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04903: "no pg_hba.conf entry for replication connection from host '%s', user '%s', %s"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04904: "no pg_hba.conf entry for replication connection from host '%s', user '%s'"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04905: "Forbid remote connection with initial user."

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04906: "Failed to Generate the random number,errcode:%d"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04907: "Failed to Generate the fake salt"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04908: "Failed to Generate the random serverkey,errcode:%d"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04909: "Failed to Generate the random storedkey,errcode:%d"

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04910: "GSS authentication method is not allowed because %s user password is not disabled."

SQLSTATE: 28000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04911 - GAUSS-04920

<br/>

GAUSS-04911: "empty password returned by client"

SQLSTATE: 28P01

Description: The client returns an empty password.

Solution: Contact technical support.

GAUSS-04912: "Could not get user information on this platform"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04913: "could not look up local user ID %ld: %s"

SQLSTATE: D0011

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04914: "could not create SSL context : %s.)"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04915: "certificate file '%s' has group or world access"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04916: "ca certificate file '%s' has group or world access"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04917: "DH: generating parameters (3072 bits) failed"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04918: "Last read message sequence %u is not equal to the max written message sequence %u"

SQLSTATE: 22000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04919: "alert, failed in revert command buffer, invalid params data len %d pq buffer size %d"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04920: "expected message sequnce is %d, actual message sequence is %d"

SQLSTATE: 22000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04921 - GAUSS-04930

<br/>

GAUSS-04921: "expected message length is %d, actual message length is %d"

SQLSTATE: 22026

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04922: "expected crc is %u, actual crc is %u"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04923: "varno is unvalid"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04924: "name for %s %u not found"

SQLSTATE: 29P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04925: "namespace for %s %u not found"

SQLSTATE: 29P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04926: "seqname of nextval() is not found"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04927: "token should not return NULL."

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04928: "incomplete array structure"

SQLSTATE: 2202E

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04929: "unterminated array structure"

SQLSTATE: 2202E

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04930: "NULL seqName for nextval()"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04931 - GAUSS-04940

<br/>

GAUSS-04931: "NULL seqNamespace for nextval()"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04932: "_readConstraint(): badly contype '%s'…"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04933: "expected '[' to start datum, but got '%s'; length = %lu"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04934: "WITHIN GROUP specified, but %s is not an aggregate function"

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04935: "WITHIN GROUP is required for ordered-set aggregate %s"

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04936: "OVER is not supported for ordered-set aggregate %s"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04937: "DISTINCT + WITHIN GROUP is not supported for ordered-set aggregate %s"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04938: "VARIADIC + WITHIN GROUP is not supported for ordered-set aggregate %s"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04939: "%s is not an ordered-set aggregate, so it cannot have WITHIN GROUP"

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04940: "window function %s requires an OVER clause"

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04941 - GAUSS-04950

<br/>

GAUSS-04941: "window function %s cannot have WITHIN GROUP"

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04942: "window functions cannot allow multiple different order info"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04943: "Operator '(+)' can only be used in WhereClause of Select-Statement or Subquery."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04944: "column reference '%s' is ambiguous."

SQLSTATE: 42702

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04945: "name data must be quoted"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04946: "REBUILD is not supported for multiple commands"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04947: "IF EXISTS is not supported for REBUILD"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04948: "Must use single quoted string for '%s' option."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04949: "Row Level Security is not yet supported for INSERT and MERGE"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04950: "unrecognized row security option '%s'"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04951 - GAUSS-04960

<br/>

GAUSS-04951: "WITH GRANT OPTION is not supported in security mode."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04952: "FENCED mode function is not yet supported in current version."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04953: "COMPACT can only be used with VACUUM FULL"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04954: "COMPACT can not be used with FREEZE"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04955: "COMPACT can not be used with PARTITION"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04956: "COMPACT can not be used with ANALYZE"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04957: "RETURNING clause is not yet supported whithin INSERT ON DUPLICATE KEY UPDATE statement."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04958: "WITH clause is not yet supported whithin INSERT ON DUPLICATE KEY UPDATE statement."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04959: "Subfield name or array subscript of column '%s' is not yet supported whithin INSERT ON DUPLICATE KEY UPDATE statement."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04960: "Update with subquery is not yet supported whithin INSERT ON DUPLICATE KEY UPDATE statement."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04961 - GAUSS-04970

<br/>

GAUSS-04961: "only allow column name within VALUES"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04962: "missing WITHIN keyword."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04963: "cannot use multiple ORDER BY clauses with WITHIN GROUP."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04964: "cannot use DISTINCT with WITHIN GROUP."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04965: "cannot use VARIADIC with WITHIN GROUP."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04966: "cannot use multiple ORDER BY clauses with WITHIN GROUP"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04967: "cannot use DISTINCT with WITHIN GROUP"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04968: "cannot use VARIADIC with WITHIN GROUP"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04969: "timestampdiff syntax is not supported."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04970: "type modifier cannot have ORDER BY"

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04971 - GAUSS-04980

<br/>

GAUSS-04971: "No function matches the given arguments names. You might need to add explicit declare arguments names."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04972: "argname should not be null"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04974: "must be system admin or monitor admin to use EXECUTE DIRECT"

SQLSTATE: 42501

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04975: "Unsupported typeoid: %d for T_Integer value, please add single quota and try again."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04976: "pstate->p_target_relation is NULL unexpectedly"

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04977: "tupleDesc should not be null"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04978: "It is forbidden to use placeholder and dollar quoting together."

SQLSTATE: 42601

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04979: "type '%s' must be in installation group"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04980: "Date type is unsupported for hdfs table in TD-format database."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04981 - GAUSS-04990

<br/>

GAUSS-04981: "u_sess->parser_cxt.opr_cache_hash should not be null"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04982: "OprCacheHash is unexpected null"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04983: "relation '%s.%s' does not exist on%s %s"

SQLSTATE: 42P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04984: "relation '%s' does not exist on%s %s"

SQLSTATE: 42P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04985: "pstate can not be NULL"

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04986: "permission denied to select from foreign table in security mode"

SQLSTATE: 42501

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04987: "relation '%s' does not have hash buckets"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04988: "Do not support create table with INERNAL DATA clause."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04989: "It's not supported to create serial column on temporary table"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04990: "It's not supported to alter table add serial column"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04991 - GAUSS-05000

<br/>

GAUSS-04991: "CREATE TABLE LIKE with column sequence in different NodeGroup is not supported."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04992: "Table %s.%s does not exist in current datanode."

SQLSTATE: 42P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04993: "timeseries store does not support add index "

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04994: "Unsupport cgin index in this version"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04995: "access method '%s' does not support row store"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04996: "access method '%s' does not support dfs store"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04997: "permission denied to alter foreign table in security mode"

SQLSTATE: 42501

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04998: "split partition '%s' does not exist."

SQLSTATE: 42P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04999: "access method 'cbtree' does not support WHERE clause"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05000: "access method 'cbtree' does not support index expressions"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.
