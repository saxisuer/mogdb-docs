---
title: GAUSS-52000 - GAUSS-52999
summary: GAUSS-52000 - GAUSS-52999
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-52000 - GAUSS-52999

<br/>

## GAUSS-52000 - GAUSS-52099

<br/>

GAUSS-52000: "Failed to obtain time zone information about the cluster node."

SQLSTATE: None

Description: Failed to obtain the time zone of the cluster node.

Solution: Check whether the time zone can successfully obtain command **date -R**.

GAUSS-52001: "Time zone information is different among cluster nodes."

SQLSTATE: None

Description: Nodes in the cluster use different time zones.

Solution: Use the **date** command to unify time zones of different nodes.

<br/>

## GAUSS-52100 - GAUSS-52199

<br/>

GAUSS-52100: "Failed to obtain cluster node character sets."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52101: "Character sets are different among cluster nodes."

SQLSTATE: None

Description: Character sets are different among cluster nodes.

Solution: Manually write the required character set forcibly on each node to the **/etc/profile** file.

GAUSS-52102: "The parameter [%s] value is not equal to the expected value."

SQLSTATE: None

Description: The parameter value is not equal to the expected value.

Solution: Set the parameter value to the expected value.

GAUSS-52103: "Failed to forcibly make the character sets to take effect."

SQLSTATE: None

Description: Failed to force the character set to take effect.

Solution: Reinstall the character set.

<br/>

## GAUSS-52200 - GAUSS-52299

<br/>

GAUSS-52200: "Unable to import module: %s."

SQLSTATE: None

Description: Using the default Python execution tool of the system results in imported package errors because the database that Python depends on is lacked.

Solution: Use the Python execution tool of the installation package and go to the directory after the decompression using **./***Tool name* to run the command.

Take the pre-processing script as an example:

Go to the directory for storing tool scripts:

```bash
cd /opt/software/mogdb/script
```

Run the **./gs_preinstall** script:

```bash
./gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml --alarm-type=5 --root-passwd=Gauss_234
```

GAUSS-52201: "The current python version %s is not supported."

SQLSTATE: None

Description: The default Python OS is not 2.6.X or 2.7.X.

Solution: Use the default Python compiler in the OS.

<br/>

## GAUSS-52300 - GAUSS-52399

<br/>

GAUSS-52300: "Failed to set OS parameters."

SQLSTATE: None

Description: Failed to configure OS parameters.

Solution:Contact technical support.

GAUSS-52301: "Failed to check OS parameters."

SQLSTATE: None

Description: Failed to check OS parameters.

Solution: Check whether the **uname -r 2>/dev/null** command is available. Check whether the **grep -Er '\' /etc/keepalived/keepalived.conf** command is executable. Check whether Python and the multiprocessing module have been installed.

<br/>

## GAUSS-52400 - GAUSS-52499

<br/>

GAUSS-52400: "Installation environment does not meet the desired result."

SQLSTATE: None

Description: The installation environment does not meet the requirements.

Solution: Use **gs_checkos** to check OS configurations and ensure that it meets the requirements.

GAUSS-52401: "On systemwide basis, the maximum number of %s is not correct. the current %s value is:"

SQLSTATE: None

Description: The maximum amount of semaphore in a semaphore set, the maximum number of semaphore sets, or the maximum amount of semaphore is incorrect. The current *%s* value is:

Solution: View the semaphore obtained from **cat /proc/sys/kernel/sem** and check whether SEMMSL, SEMMNI, or SEMMNS meet the requirements.

GAUSS-52402: "IP [%s] is not matched with hostname [%s]. \n"

SQLSTATE: None

Description: The IP address does not match the host name.

Solution: Check whether the IP address matches the host name. If they are not matched, modify the **/etc/profile** file.

GAUSS-52403: "Command \"%s\" does not exist or the user has no execute permission on %s."

SQLSTATE: None

Description: The command does not exist or you do not have the execution permission.

Solution: Check whether the command exists. Ensure that you have the execution permission.

<br/>

## GAUSS-52500 - GAUSS-52599

<br/>

GAUSS-52500: "Failed to delete regular tasks."

SQLSTATE: None

Description: Failed to delete regular tasks.

Solution:Contact technical support.

GAUSS-52501: "Run %s script before executing this script."

SQLSTATE: None

Description: Another script needs to be run before you execute this script.

Solution: Check the execution sequence of the scripts.

GAUSS-52502: "Another OM process is being executed. To avoid conflicts, this process ends in advance."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52503: "Failed to execute checkRunStatus.py. Error: %s"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-52600 - GAUSS-52699

<br/>

GAUSS-52600: "Can not obtain any cluster ring."

SQLSTATE: None

Description: Failed to obtain a ring from the database.

Solution: Check whether the node information in the XML file is correctly configured.

GAUSS-52601: "Redistribution failed due to user request."

SQLSTATE: None

Description: Redistribution failed due to user requests.

Solution: Check whether users have performed invalid operations during the scale-in or scale-out process.

GAUSS-52604: " Parameter '-r'[%s] can not be more than the numbers of cluster ring[%s]."

SQLSTATE: None

Description: The number of the **-r** parameters cannot exceed that of the database rings.

Solution: Check whether the number of the **-r** parameters in the **gs_shrink** command exceeds that of database rings configured in the XML file.

GAUSS-52605: "Can not contract local node(%s). "

SQLSTATE: None

Description: The local node cannot be scaled in.

Solution: Ensure that the node where the scale-in command is executed has been configured in the XML.

GAUSS-52606: " Contract too many nodes. It should left three nodes to format a cluster at least."

SQLSTATE: None

Description: Too many nodes are scaled in. Retain at least three nodes in the database after scale-in.

Solution: Check whether the number of nodes in the XML file after the scale-in is smaller than three.

GAUSS-52607: " [%s] does not at the end of instance list."

SQLSTATE: None

Description: The node is not at the end of the instance list.

Solution: Check whether the scale-in nodes configured in the XML file are the nodes at the end of the database in sequence.

GAUSS-52608: " [%s] contains %s instance."

SQLSTATE: None

Description: The code contains CM servers or ETCDs.

Solution: Check whether the scale-in nodes include CM servers or ETCDs. If yes, reconfigure corresponding nodes to exclude them, and then run the command again.

GAUSS-52609: " All contracted nodes do not contain DN instance."

SQLSTATE: None

Description: All the scale-in nodes cannot contain DNs.

Solution: Check whether the scale-in nodes in the XML file contain DNs.

GAUSS-52612: " All dilatation nodes do not contain the DN instance."

SQLSTATE: None

Description: All the scale-out nodes cannot contain DNs.

Solution: Check whether the scale-out nodes in the XML file contain DNs.

GAUSS-52613: " Static configuration is not matched on some nodes. Please handle it first."

SQLSTATE: None

Description: The static configuration file on the node does not match the new XML file.

Solution: Reexecute the pre-processing script.

GAUSS-52614: "Timeout. The current cluster status is %s."

SQLSTATE: None

Description: Timeout occurred.

Solution: Perform the operation again.

GAUSS-52615: "Cluster lock unlocked due to timeout. "

SQLSTATE: None

Description: Failed to unlock the database due to timeout.

Solution: Check whether the database status is normal. Check whether the database can write data. View corresponding logs and detailed error information.

GAUSS-52616: "Can not find a similar instance for [%s %s]. "

SQLSTATE: None

Description: A similar instance [the host name and instance directory] cannot be found.

Solution: Check whether the database status is normal and whether the node information in the XML file is correctly configured.

GAUSS-52617: "Invalid check type. "

SQLSTATE: None

Description: The check type is invalid.

Solution: Ensure that the check types of the node groups in the scale-in module are precheck and postcheck. Other check types are incorrect.

GAUSS-52618: "Failed to delete etcd from node. "

SQLSTATE: None

Description: Failed to delete the ETCD.

Solution:Contact technical support.

GAUSS-52619: "Failed to uninstall application."

SQLSTATE: None

Description: Failed to uninstall the application.

Solution: Perform the operation again.

GAUSS-52620: " Not all nodes found. The following is what we found: %s."

SQLSTATE: None

Description: Failed to find all nodes. The nodes that we found are as follows:

Solution: Check whether the database status is normal and whether the configured scale-in node in the XML file is correct.

GAUSS-52621: " No DNs specified in target create new group."

SQLSTATE: None

Description: No DN is available when you create a node group.

Solution: Check and determine whether the DN configured on the node is normal when you scale in a module and create a node group.

GAUSS-52622: " No new group name specified in target create new group."

SQLSTATE: None

Description: No node group name is available when you create a node group.

Solution: The node group name is empty when you scale in a module and create a node group. Check whether the new node group name is correct. The new node group name value is **group_version2** or **group_version1**.

GAUSS-52625: "The local instance and peer instance does not both in contracted nodes. "

SQLSTATE: None

Description: The local instance and the peer instance are not located on the same scale-in node.

Solution: Check whether the scale-in nodes are formed into a ring.

GAUSS-52627: " The current cluster is locked."

SQLSTATE: None

Description: The database has been locked.

Solution: In the scale-out module, the old node database has been locked. Unlock it and continue scaling out the current database.

GAUSS-52628: "Static configuration has already been updated on all nodes, expansion has been completed possibly. "

SQLSTATE: None

Description: The static configuration file has been updated on each node. The scale-out is probably complete.

Solution: Check whether the database status is normal after the scale-out. If the database includes the scale-out nodes and the database status is normal, do not execute any scale-out commands any more.

GAUSS-52629: "Cluster ring(%s) can not obtain less than three nodes. "

SQLSTATE: None

Description: The number of database rings cannot be less than three.

Solution: Check the XML file configuration in the scale-in scenario to see whether the number of scale-in nodes to be executed is smaller than three.

GAUSS-52630: "Failed to set the read-only mode parameter for all DN instances. "

SQLSTATE: None

Description: Failed to set the read-only mode parameter for all DN instances.

Solution: Check whether the **gs_guc** tool exists and whether the database status is normal. Set the read-only mode again.

GAUSS-52632: "Cluster breakdown or abnormal operation during expanding online, lock process for expansion is lost."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52633: "Can not excute redistribution for shrink excuted failed."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52634: "Redis table record count cannot be more than %d."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-52700 - GAUSS-52799

<br/>

GAUSS-52700: "Failed to update ETCD."

SQLSTATE: None

Description: Failed to update the ETCD.

Solution: Solve the problem based on the replace logs.

GAUSS-52701: "All the CMAgents instances are abnormal. Cannot fix the cluster."

SQLSTATE: None

Description: All the CM Agent instances are damaged and the cluster cannot be restored.

Solution: Use **gs_replace** to perform warm backup.

GAUSS-52702: "The cluster status is Normal. There is no instance to fix."

SQLSTATE: None

Description: The cluster status is normal and no instance needs to be restored.

Solution: Do not perform the restore operation.

GAUSS-52703: "The number of normal ETCD must be greater than half."

SQLSTATE: None

Description: More than half of the ETCDs in the cluster must be normal.

Solution: Check whether the cluster status is normal after the warm backup and whether the number of normal ETCDs in the cluster exceeds half of the whole ETCDs.

GAUSS-52704: "Failed to check the %s condition."

SQLSTATE: None

Description: Failed to check the condition.

Solution: Check whether the corresponding process is normal.

GAUSS-52705: "Failed to obtain ETCD key."

SQLSTATE: None

Description: Failed to obtain the ETCD key.

Solution: View the etcdctl logs and solve the problem.

GAUSS-52706: "Failed to clean ETCD and touch flag file on %s."

SQLSTATE: None

Description: Failed to clear ETCD instances and create flag files on the nodes.

Solution: Check whether there are sufficient permissions on corresponding nodes.

GAUSS-52707: "Failed to install on %s."

SQLSTATE: None

Description: Failed to install the warm backup node.

Solution: Solve the problem based on the replace logs.

GAUSS-52708: "Failed to configure on %s."

SQLSTATE: None

Description: Failed to configure the warm backup node.

Solution: Solve the problem based on the replace logs.

GAUSS-52709: "Failed to check the cluster configuration differences:"

SQLSTATE: None

Description: Failed to check the cluster configuration difference.

Solution: When performing the warm backup or modifying the IP address, check whether the XML file and the current cluster configuration are different. If they are the same, skip the preceding operations. Modify the XML configuration information again.

GAUSS-52710: "Replacement failed."

SQLSTATE: None

Description: Failed to replace the instance.

Solution: View the logs and find out detailed failure information.

GAUSS-52711: "Failed to set CMAgent start mode."

SQLSTATE: None

Description: Failed to set the CM Agent startup mode.

Solution: View the etcdctl logs and solve the problem.

GAUSS-52712: "Failed to read step file with erro:[%s]."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52713: "Failed to repair cm_server, errorinfo:[%s]."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52714: "Failed to write step file, errorinfo:[%s]."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-52800 - GAUSS-52899

<br/>

GAUSS-52800: "Cluster is %s(%s) now."

SQLSTATE: None

Description: The cluster status is **Abnormal** or **Degrade**.

Solution: Check the cluster status based on the log report analysis.

GAUSS-52801: "Only allow to %s one CN. The %s is not matched."

SQLSTATE: None

Description: Only a CN can be added or deleted. The number of CNs, DNs, GTMs, ETCDs, or CM servers does not match that configured in the XML file.

Solution: Check whether other instances are added or deleted in the XML file.

GAUSS-52802: "Only allow to add one CN at the end."

SQLSTATE: None

Description: Only a CN can be added at the end.

Solution: Check the specified XML configuration. Only a CN can be added at the end of the last node in the cluster.

GAUSS-52803: "There is at least one Normal CN after delete CN."

SQLSTATE: None

Description: Retain at least a normal CN after the CNs are deleted.

Solution: Check whether the XML file where CNs to be deleted are located is properly configured. Ensure that at least a normal CN exists in the cluster after all the CNs are deleted.

GAUSS-52804: "Failed to add the Abnormal CN."

SQLSTATE: None

Description: Failed to add an abnormal CN.

Solution: Analyze the failure reason based on the logs when modifying the **pgxc_node** file fails. Re-execute the command.

GAUSS-52805: "Failed to find another instance as model for instance(%s)."

SQLSTATE: None

Description: No instance template is available.

Solution: Check detailed logs and analyze the reason.

GAUSS-52806: "Invalid rollback step: %s."

SQLSTATE: None

Description: The rollback procedure is invalid.

Solution: In the **GPHOST** directory, check whether data in the **GaussManageCN.dat** file that records the CN configuration procedure is correct.

GAUSS-52807: "There is no IP changed."

SQLSTATE: None

Description: No IP address needs to be modified.

Solution: Check whether the IP address has been already used in the cluster. If it has been used, reconfigure the XML file.

GAUSS-52808: "Detected CN %s, but the action is %s."

SQLSTATE: None

Description: The CN to be added or deleted does not match that configured in the XML file.

Solution: Modify the operations of adding or deleting CNs in the command, or modify the CN configuration information in the XML file.

GAUSS-52809: "Only allow to add or delete one CN."

SQLSTATE: None

Description: The number of CNs to be added or deleted is greater than 1.

Solution: Only one CN can be added or deleted at a time.

GAUSS-52810: "There is Abnormal coodinator(s) in cluster, please delete it firstly."

SQLSTATE: None

Description: There is an abnormal CN in the cluster. As a result, other CNs cannot be deleted.

Solution: Delete the abnormal CN, and then delete other CNs.

<br/>

## GAUSS-52900 - GAUSS-52999

<br/>

GAUSS-52900: " Failed to upgrade strategy: %s."

SQLSTATE: None

Description: Failed to upgrade the policy.

Solution: Check the version information file **version.cfg**.

GAUSS-52901: "New·cluster·commitid·cannot·be·same·with·old·cluster·commitid."

SQLSTATE: None

Description: The binary upgrade backup path cannot exist with the major version upgrade backup path.

Solution: Determine the reason of the last upgrade failure and sort backup files.

GAUSS-52902: "Can not support upgrade from %s to %s"

SQLSTATE: None

Description: The version cannot be upgraded.

Solution: Use a supported version or an intermediate version for upgrade.

GAUSS-52903: "The new cluster version number[%s] should be bigger than the old cluster[%s]."

SQLSTATE: None

Description: Version rollback is not supported, which means the version number after the upgrade must be greater than that of the previous version.

Solution: Confirm the version information.

GAUSS-52904: "Please choose right upgrade strategy."

SQLSTATE: None

Description: The right upgrade policy should be selected.

Solution: Revoke a policy and select an interface. Use the upgrade interface properly, or invoke an automatic upgrade interface.

GAUSS-52905: "Upgrade nodes number cannot be more than %d."

SQLSTATE: None

Description: Failed to obtain the current version.

Solution: View the logs and find out detailed error information.

GAUSS-52906: "Grey upgrade nodes number cannot be more than cluster nodes."

SQLSTATE: None

Description: The upgrade initialization failed.

Solution: View the upgrade logs and identify the fault cause. Initialize the upgrade again.

GAUSS-52907: "Failed to cancel the cluster read-only mode"

SQLSTATE: None

Description: Failed to cancel the cluster read-only mode.

Solution: Manually invoke the GUC tool using commands **gs_guc reload -Z coordinator -N all -I all -c "default_transaction_read_only=false"** and **gs_guc reload -Z datanode -N all -I all -c "default_transaction_read_only=false"**. Try to cancel the read-only mode again.

GAUSS-52908: "Failed to set cluster read-only mode."

SQLSTATE: None

Description: Failed to set the read-only mode.

Solution: View the GUC logs. After troubleshooting the error, run the command again.

GAUSS-52909: "Specified upgrade nodes with same step can do upgrade task."

SQLSTATE: None

Description: The mode is invalid.

Solution: Find corresponding log information.

GAUSS-52910: "These nodes %s have been successfully upgraded to new version, no need to upgrade again."

SQLSTATE: None

Description: Failed to check the user definition.

Solution: Check whether errors exist in the user-defined check script. Identify the upgrade cause based on the expectation.

GAUSS-52911: "Last unsuccessfully upgrade nodes %s are not same with current upgrade nodes."

SQLSTATE: None

Description: The full upgrade failed.

Solution: Perform the upgrade again.

GAUSS-52912: "Some nodes were upgraded but were unsuccessfully, cannot use -continue."

SQLSTATE: None

Description: Failed to enable the cluster in normal mode.

Solution: Manually identify the cause. Invoke the **gs_om -t start** command to enable the cluster.

GAUSS-52913: "All nodes have been upgraded. No need to use -continue."

SQLSTATE: None

Description: Rollback failed.

Solution: Perform the operation again.

GAUSS-52914: "The record commitid is not same with current commitid."

SQLSTATE: None

Description: Failed to clear the rollback script.

Solution: Check the cluster status. If the upgrade succeeds, manually delete the remaining files.

GAUSS-52915: "$GAUSSHOME is not a symbolic link."

SQLSTATE: None

Description: The upgrade procedure is invalid.

Solution: In the **GPHOST** path, confirm whether the backup file that records the upgrade procedure is modified.

GAUSS-52916: "Current upgrade status is not pre commit."

SQLSTATE: None

Description: The cluster node does not meet the submission conditions.

Solution: Submit the upgrade project after all nodes are successfully upgraded.

GAUSS-52917: "Failed to drop old pmk schema."

SQLSTATE: None

Description: The upgrade in binary mode fails and rolls back to the primitive cluster.

Solution: View the upgrade logs and identify the fault cause. Perform the upgrade again.

GAUSS-52918: "Failed to record node upgrade step in table %s.%s."

SQLSTATE: None

Description: Failed to find the GTM.

Solution: Invoke the **gs_om -t status -detail** command to query the cluster status. If the GTM cluster in the cluster is abnormal, reinstall the cluster or perform warm backup to restore the cluster.

GAUSS-52919: "Upgrade has already been committed but not finished commit."

SQLSTATE: None

Description: The upgrade failed.

Solution: View the upgrade logs and identify the fault cause. Perform the upgrade again as required.

GAUSS-52920: "Can not use grey upgrade option -continue before upgrade grey nodes."

SQLSTATE: None

Description: Failed to restore the GUC parameters after the full upgrade.

Solution: View the upgrade logs and the GUC logs. Identify the cause and perform the upgrade again.

GAUSS-52921: "Failed to query disk usage with gs_check tool."

SQLSTATE: None

Description: The upgrade type is invalid.

Solution: Use the automatic upgrade interfaces.

GAUSS-52922: "Disk usage exceeds %s, please clean up before upgrading."

SQLSTATE: None

Description: Failed to record the upgrade procedure.

Solution: Confirm that you have the permission on the temporary file directory or that the directory exists.

GAUSS-52923: "Inplace upgrade commit failed.\n%s"

SQLSTATE: None

Description: Failed to record the upgrade status.

Solution: Confirm that you have the permission on the temporary file directory or that the directory exists.

GAUSS-52924: "The %s parameter are only supported in independent deployment mode."

SQLSTATE: None

Description: The information in the upgrade status file is incorrect.

Solution: Modify the upgrade backup file and try to restore the record file based on the upgrade procedure.

GAUSS-52925: "Input upgrade type [%s] is not same with record upgrade type [%s]."

SQLSTATE: None

Description: An incorrect upgrade interface is used.

Solution: Use the automatic upgrade interface or automatic rollback interface.

GAUSS-52926: "The step of upgrade should be digit."

SQLSTATE: None

Description: The upgrade procedure must be a digit.

Solution: Check whether the upgrade backup file is modified and try to restore it.

GAUSS-52929: "Failed to check application version. Output: \n%s."

SQLSTATE: None

Description: Failed to check the cluster version.

Solution: Check the upgrade logs and confirm the version information on each node.

GAUSS-52932: "There is no CN in the remaining old nodes."

SQLSTATE: None

Description: No CNs are available in the old cluster.

Solution: Check the upgrade logs and confirm the instance information on each node.

GAUSS-52933: "There is not a majority of %s on the remaining old nodes."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-52934: "Binary upgrade failed."

SQLSTATE: None

Description: Failed to upgrade.

Solution: Check the upgrade logs or contact R&D engineers to locate the fault.

GAUSS-52935: "Current upgrade version is not same with unfinished upgrade version record."

SQLSTATE: None

Description: The previous upgrade operation is not complete (not committed or rolled back), and the upgrade is intended to be performed to another version.

Solution: Submit or roll back the previous upgrade, and then perform the upgrade of another version.

GAUSS-52936: "Upgrade is not finished, cannot do another task."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.
