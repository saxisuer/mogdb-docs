---
title: GAUSS-07401 - GAUSS-07480
summary: GAUSS-07401 - GAUSS-07480
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-07401 - GAUSS-07480

<br/>

## GAUSS-07401 - GAUSS-07410

<br/>

GAUSS-07401: "unexpected RM_HEAP_ID record type: %u"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07402: "cannot free a copied snapshot"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07403: "cannot free an active snapshot"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07404: "cannot export a snapshot before reaching a consistent state"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07405: "cannot export a snapshot, not all transactions are monitored anymore"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07406: "cannot export a snapshot when MyPgXact->xmin already is valid"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07407: "cannot export a snapshot from within a transaction"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07408: "can only export one snapshot at a time"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07409: "clearing exported snapshot in wrong transaction state"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07410: "xl_heap_new_cid record without a valid CommandId"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07411 - GAUSS-07420

<br/>

GAUSS-07411: "waiting for ourselves"

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07412: "snapbuild state file '%s' has wrong magic %u instead of %d"

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07413: "snapbuild state file '%s' has unsupported version %u instead of %d"

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07414: "could not read file '%s', read %lu of %lu: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07415: "could not read file '%s', size overflow"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07416: "snapbuild state file %s: checksum mismatch, is %u, should be %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07417: "too much output for sql interface"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07418: "must be superuser or replication role to use replication slots"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07419: "could not read from log segment %s, offset %u, length %lu: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07420: "couldn't advance in recovery"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07421 - GAUSS-07430

<br/>

GAUSS-07421: "invalid input syntax for type lsn: '%s' of start_lsn"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07422: "array must be one-dimensional"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07423: "array must not contain nulls"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07424: "array must be TEXTOID"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07425: "array must have even number of elements"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07426: "could not remove directory '%s'"

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07427: "tuplecid value in changequeue"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07428: "output plugin used xid %lu"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07429: "can not found txn which xid = %lu"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07430: "only ever add one set of invalidations"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07431 - GAUSS-07440

<br/>

GAUSS-07431: "txn->subtxns is illegal point"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07432: "could not write to xid %lu's data file: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07433: "could not read from reorderbuffer spill file: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07434: "incomplete read from reorderbuffer spill file: read %d instead of %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07435: "illegality read length %lu"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07436: "could not read from reorderbuffer spill file: read %d instead of %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07437: "got sequence entry %u for toast chunk %u instead of seq 0"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07438: "got sequence entry %u for toast chunk %u instead of seq %d"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07439: "fail to get toast chunk"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07440: "unexpected type of toast chunk"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07441 - GAUSS-07450

<br/>

GAUSS-07441: "toast_rel should not be NULL!"

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07442: "could not read file '%s', read %d instead of %d"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07443: "could not parse fname %s"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07444: "yyscanner init failed: %m"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07445: "we should be a writer when enable_mix_replication is on"

SQLSTATE: 42P08

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07446: "Insert BCM Info to be interrupted."

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07447: "The got BCM Array index is corrupt: index1 %u index2 %u array_index %u BCMElementArrayOffset1 %X/%X BCMElementArrayOffset2 %X/%X"

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07448: "Invalid relation while clearing BCM status: rnode[%u,%u,%u], blocknum[%u], pageoffset[%lu], size[%u], attid[%d]"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07449: "buffer should be valid, but now is %d"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07450: "heap sync hash table not cleaned, num of entries:%ld"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07451 - GAUSS-07460

<br/>

GAUSS-07451: "enable_cbm_tracking must be turn on when enable_mix_replication is on!"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07452: "can't decode in pmState is not run or recovery in progress."

SQLSTATE: LL001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07453: "cannot use a logical replication slot for physical replication"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07454: "Failed to init the xlog reader for the wal sender."

SQLSTATE: 29000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07455: "walsender stop switchover process for catchup is alive, the process need to be restart"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07456: "terminating Walsender process due to replication timeout.now time(%s) timeout time(%s) last recv time(%s) heartbeat time(%s)"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07457: "syncrep_scanner_yylex_init() failed: %m"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07458: "invalid replication node index:%d"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07459: "Refuse WAL streaming, connection mode is %d, connertion IP is %s:%d\n"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07460: "terminating walreceiver due to timeout now time(%s) timeout time(%s) last recv time(%s) heartbeat time(%s)"

SQLSTATE: YY004

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07461 - GAUSS-07470

<br/>

GAUSS-07461: "WAL streaming isn't employed to sync all the replication data log."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07462: "the corrupt data total len is %u bytes, the expected len is %lu bytes."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07463: "the unexpected data flag is %X, the expected data flag is 'd'."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07464: "the start xlog employed for the wal data is invalid."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07465: "the end xlog employed for the wal data is invalid."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07466: "the received wal data is unexpected %u bytes at least more than %lu bytes"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07467: "the received xlog is unexpected %u bytes at least more than %lu bytes."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07468: "fail to push some wal data to the wal streaming writer queue: unexpected wal data flag %c."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07469: "Unexpected seek in the walreceiver buffer. xlogrecptr is (%X:%X) but local xlogptr is (%X:%X)."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07470: "corrupt wal data write len %u bytes, the expected write data_size %u"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-07471 - GAUSS-07480

<br/>

GAUSS-07471: "The Row Store Heap Log SHOULD NOT BE synchronized in the WAL Streaming. Tracking the data header info: rnode[%u/%u/%u] blocknum[%u] pageoffset[%lu] size[%u] queueoffset[%u/%u]."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07472: "Failed to write the wal data: the database path %s doesn't exist."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.
