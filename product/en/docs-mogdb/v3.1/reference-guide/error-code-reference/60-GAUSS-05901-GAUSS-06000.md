---
title: GAUSS-05901 - GAUSS-06000
summary: GAUSS-05901 - GAUSS-06000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05901 - GAUSS-06000

<br/>

## GAUSS-05901 - GAUSS-05910

<br/>

GAUSS-05901: "permission denied to change owner of directory"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05902: "could not open extension control file: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05903: "extension '%s' already exists in schema '%s'"

SQLSTATE: 42710

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05904: "extcondition is not a 1-D Oid array"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05905: "extension is null"

SQLSTATE: XX005

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05906: "EXPLAIN PLAN does not support on datanode or single node."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05907: "EXPLAIN option 'PLAN' can not work with other options."

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05908: "EXPLAIN %s is not supported when declaring a cursor."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05909: "invalid value for parameter explain_perf_mode."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05910: "unrecognized value for EXPLAIN option 'format'."

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05911 - GAUSS-05920

<br/>

GAUSS-05911: "u_sess->instr_cxt.global_instr is NULL"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05912: "statement_id is too long. Input statement_id length=%u, however max length=%d."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05913: "Relation 'plan_table_data' does not exist"

SQLSTATE: 42602

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05914: "Do not specify any information for CREATE POLICY '%s' ON '%s'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05915: "Create row level security policy '%s' failed, because it will result in infinite recursion for DML queries"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05916: "row level policy '%s' for table '%s' already exists"

SQLSTATE: 42710

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05917: "Num of row level policies for relation should less than or equal to %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05918: "Do not specify any information for ALTER POLICY '%s' ON '%s'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05919: "row level security policy '%s' for relation '%s' does not exists"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05920: "Alter row level security policy '%s' failed, because it will result in infinite recursion for DML queries"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05921 - GAUSS-05930

<br/>

GAUSS-05921: "row level policy '%s' for relation '%s' already exists"

SQLSTATE: 42710

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05922: "unrecognized row level security policy command"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05923: "too many roles specified in SQL statement."

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05924: "%s not implemented"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05925: "could not write to staging temporary file while exporting segment %s"

SQLSTATE: 22004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05926: "could not write to local temporary buffile: %m"

SQLSTATE: 22004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05927: "Write-Only table's output directory %s is not empty"

SQLSTATE: 22004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05928: "could not rewind OBS exporting temporary file: %m"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05929: "must be system admin to CREATE TEXT SEARCH DICTIONARY"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05930: "CREATE TEXT SEARCH DICTIONARY in a temp namespace is not supported"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05931 - GAUSS-05940

<br/>

GAUSS-05931: "Not allowed to alter built-in text search dictionary"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05932: "Not allowed to drop built-in text search dictionary"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05933: "Not allowed Template parameter in alter text search dictionary"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05934: "env $GAUSSHOME not found, please set it first"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05935: "failed to bind fd(%d) to ssl"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05936: "m_fd + 1 cannot be greater than FD_SETSIZE"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05937: "The peer GDS has performed an orderly shutdown on current connection."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05938: "exception from GDS '%s':%m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05939: "Message size exceeds the maximum allowed (%d)"

SQLSTATE: OP002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05940: "Invalid command to serialize."

SQLSTATE: OP002

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05941 - GAUSS-05950

<br/>

GAUSS-05941: "Unexpected length of data coming supposedly from GDS. Could be an forged attack package."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05942: "Failed to _ReadError in CMD_TYPE_ERROR, could be a forged package."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05943: "Failed to _ReadFileSwitch in CMD_TYPE_FILE_SWITCH, could be a forged package."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05944: "Failed to _ReadResponse in CMD_TYPE_RESPONSE, could be a forged package."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05945: "The corresponding GDS is of an older version. Please upgrade GDS to match the server version."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05946: "Failed to _ReadResult in CMD_TYPE_QUERY_RESULT_V1, could be a forged package."

SQLSTATE: DB001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05947: "Hash bkt dir '%s' not exists:%m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05948: "could not create directory '%s':%m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05949: "'%s' exists but is not a directory."

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05950: "permission denied to create tablespace in security mode"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05951 - GAUSS-05960

<br/>

GAUSS-05951: "Create tablespace with absolute location can't be allowed"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05952: "tablespace location can only be formed of 'a~z', 'A~Z', '0~9', '-', '_'"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05953: "relative location can only be formed of 'a~z', 'A~Z', '0~9', '-', '_' and two level directory at most"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05954: "The relative location can not be null"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05955: "could not open pg_tblspc directory"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05956: "could not get '%s' status"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05957: "'%s' is not symlink, please check and clean the remains in '%s'"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05958: "target of symbolic link '%s' doesn't exist"

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05959: "target of symbolic link '%s' isn't directory"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05960: "find conflict linkpath '%s' in pg_tblspc, location '%s'"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05961 - GAUSS-05970

<br/>

GAUSS-05961: "permission denied to alter tablespace in security mode"

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05962: "It is unsupported to reset 'filesystem' option."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05963: "It is unsupported to alter general tablespace to hdfs tablespace."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05964: "Tablespace '%u' does not exist."

SQLSTATE: 58P01

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05965: "Can not get tablespace size with SnapshotNow after try 3 times."

SQLSTATE: P0002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05970: "conflicting or redundant option: 'resource pool'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05971 - GAUSS-05980

<br/>

GAUSS-05971: "conflicting or redundant option: 'user group default'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05972: "conflictiong or redundant option: 'spill space'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05975: "Can not create role with resource pool (%s) with foreign users option."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05976: "parent cannot be itself."

SQLSTATE: 0P000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05977: "Role '%s': object not defined."

SQLSTATE: 42704

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-05981 - GAUSS-05990

<br/>

GAUSS-05981: "Independent user is not supported."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05982: "Independent user cannot have sysadmin, auditadmin, vcadmin, createrole, monadmin and opradmin attributes."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05983: "Failed to Generate the random number, errcode:%u"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05984: "Can not alter role with resource pool (%s) with foreign users option."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05985: "resource pool of role '%s' does not exist."

SQLSTATE: 42P26

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05989: "Can not alter Role '%s' to vcadmin."

SQLSTATE: 0LP01

Description: Internal system error.

Solution: Contact technical support.

<br/>

## GAUSS-05991 - GAUSS-06000

<br/>

GAUSS-05991: "Only user himself can remove his own independent attribute."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05992: "Only independent user himself can alter his own password."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05993: "Only system admin can enable user's password."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05994: "Only independent user himself can enable his own password."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05995: "Only system admin can disable user's password."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05996: "Only independent user himself can disable his own password."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05997: "resource pool of role '%s' does not exsist."

SQLSTATE: 42P26

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05998: "Only independent user himself can decide his own membership."

SQLSTATE: 42501

Description:Internal system error.

Solution:Contact technical support.

GAUSS-05999: "Role '%s' can not be granted across virtual clusters."

SQLSTATE: 0LP01

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06000: "While get super user, invalid role OID: %u"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.
