---
title: DBMind Mode
summary: DBMind Mode
author: Guo Huan
date: 2022-05-06
---

# DBMind Mode

You can run the **gs_dbmind** command to invoke all functions of AI4DB. This command provides the following basic functions:

- Service functions: You can use the **service** subcommand to implement service related functions, including creating and initializing configuration directories, starting background services, and stopping background services.
- Invoking components: You can use the **component** subcommand to invoke components. AI4DB functions (such as index recommendation and parameter tuning) can be invoked in real time in this mode.
- Setting parameters: You can use the **set** subcommand to modify the configuration file in the configuration directory. The configuration file can also be modified by using the text editor.

You can use the **–help** option to obtain the help information about the preceding modes. For example:

```
gs_dbmind --help
```

```
usage: [-h] [--version] {service,set,component} ...

MogDB DBMind: An autonomous platform for MogDB

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit

available subcommands:
  {service,set,component}
                        type '<subcommand> -h' for help on a specific subcommand
    service             send a command to DBMind to change the status of the service
    set                 set a parameter
    component           pass command line arguments to each sub-component.
```

**Table 1** gs_dbmind options

| Parameter  | Description                                     | Value Range |
| :--------- | :---------------------------------------------- | :---------- |
| -h, --help | Help information                                | -           |
| --version  | Version number                                  | -           |
| service    | Subcommand related to service functions         | -           |
| component  | Subcommand for invoking components              | -           |
| set        | Subcommand for modifying the configuration file | -           |