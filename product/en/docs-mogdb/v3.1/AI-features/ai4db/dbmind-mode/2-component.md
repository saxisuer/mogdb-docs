---
title: component
summary: component
author: Guo Huan
date: 2022-05-06
---

# component

This subcommand can be used to start DBMind components, including the exporter for monitoring metrics and other AI functions. It forwards the commands passed by the user through the CLI client to the corresponding components. For details about the commands of different components, see the corresponding sections of the components.

## Command Reference

You can use the **–help** option to obtain the help information about this mode. For example:

```
gs_dbmind component --help
```

```
usage:  component [-h] COMPONENT_NAME ...

positional arguments:
  COMPONENT_NAME  choice a component to start. ['extract_log', 'forecast', 'index_advisor', 'opengauss_exporter', 'reprocessing_exporter', 'slow_query_diagnosis', 'sqldiag', 'xtuner']
  ARGS            arguments for the component to start

optional arguments:
  -h, --help      show this help message and exit
```

**Table 1** Parameters of the gs_dbmind component subcommand

| Parameter      | Description          | Value Range                                                  |
| :------------- | :------------------- | :----------------------------------------------------------- |
| COMPONENT_NAME | Component name       | extract_log, forecast, index_advisor, opengauss_exporter, reprocessing_exporter, slow_query_diagnosis, sqldiag, xtuner |
| ARGS           | Component parameters | Refer to the command description of the corresponding component. |
| -h, –help      | Help information     | -                                                            |