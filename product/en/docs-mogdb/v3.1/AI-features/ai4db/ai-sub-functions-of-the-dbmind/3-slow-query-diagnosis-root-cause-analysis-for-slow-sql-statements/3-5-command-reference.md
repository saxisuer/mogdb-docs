---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2022-05-06
---

# Command Reference

**Table 1** gs_dbmind component slow_query_diagnosis parameters

| Parameter        | Description                                   | Value Range                                            |
| :--------------- | :-------------------------------------------- | :----------------------------------------------------- |
| -h, --help       | Help information                              | -                                                      |
| action           | Action parameter                              | - show: displays results.<br/>- clean: clears results. |
| -c，--conf       | Configuration directory                       | -                                                      |
| --query          | Slow SQL text                                 | *                                                      |
| --start-time     | Timestamp of the start time, in milliseconds. | Non-negative integer                                   |
| --end-time       | Timestamp of the end time, in milliseconds.   | Non-negative integer                                   |
| --retention-days | Number of days retaining results              | Non-negative real number                               |