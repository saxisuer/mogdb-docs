---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2021-05-19
---

# Command Reference

**Table 1** Command-line options

| Parameter       | Description                          | Value Range   |
| :-------------- | :----------------------------------- | :------------ |
| -f              | Training or prediction file location | N/A           |
| –predicted-file | Prediction result location           | N/A           |
| –model          | Model selection                      | template, dnn |
| –model-path     | Location of the training model       | N/A           |
