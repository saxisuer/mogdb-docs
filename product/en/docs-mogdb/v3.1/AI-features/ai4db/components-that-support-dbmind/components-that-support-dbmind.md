---
title: Components that Support DBMind
summary: Components that Support DBMind
author: Guo Huan
date: 2022-05-06
---

# Components that Support DBMind

DBMind provides components to support the deployment and implementation of the entire service or solution. They are not AI functions, but are an important part of the entire service system. They are used to support the quick implementation of the entire autonomous O&M solution. For example, the exporter is used to collect database metrics.
