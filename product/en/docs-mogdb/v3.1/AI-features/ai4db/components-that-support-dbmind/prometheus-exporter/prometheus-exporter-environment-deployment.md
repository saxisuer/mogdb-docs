---
title: 环境部署
summary: 环境部署
author: Guo Huan
date: 2022-05-06
---

# Environment Deployment

1. Start the exporter process and create a listening port number on the local host.
2. Modify the configuration file **prometheus.yml** on the Promethues server and add the information about the started exporter to the file. For example:

   ```
    scrape_configs:
    ...
      - job_name: 'opengauss_exporter'
        static_configs:
        - targets: ['127.0.0.1:9187']
    ...
   ```

The exporter component uses HTTPS for communication by default. Therefore, you need to provide the SSL certificate and key file by default through **–ssl-keyfile** and **–ssl-certfile**. To disable HTTPS, you can run the **–disable-https** command.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> **NOTE:** The default communication protocol used by MogDB is incompatible with PostgreSQL. As a result, the PostgreSQL-based Python driver **psycopg2-binary** installed using the PyPI source cannot connect to the MogDB database. Therefore, you need to compile **psycopg2** or modify GUC parameters for adaptation. You can also download **psycopg2** compiled based on MogDB from the openGauss official website. (Typically, a version compiled for Python 3.6 is provided. User need to check the version number before using it.)
> 
>- Download the openGauss Python driver from the official website:
>   [https://opengauss.org/en/download/](https://opengauss.org/en/download/)
> - For details about the adaptation of the Python driver, see the openGauss operation guide at: 
>   [https://mp.weixin.qq.com/s/2TobUQKtw0N9sBpMZJr6zw](https://mp.weixin.qq.com/s/2TobUQKtw0N9sBpMZJr6zw)
