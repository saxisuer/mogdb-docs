---
title: Overview
summary: Overview
author: Guo Huan
date: 2022-05-06
---

# Overview

Predictor is a query time prediction tool that leverages machine learning and has online learning capabilities. By continuously learning the historical execution information collected in the database, Predictor can predict the execution time of a plan.

To use this tool, you must start the Python process AI Engine for model training and inference.

This function is stored in the *$GAUSSHOME***/bin/dbmind/components/predictor** directory. Some functions of this module involve complex setup. Therefore, you need to switch to the directory to find the corresponding files and deploy the functions according to the description in this section.
