---
title: FAQs
summary: FAQs
author: Guo Huan
date: 2022-05-06
---

# FAQs

## AI Engine Configuration

- **AI Engine failed to be started**: Check whether the IP address and port are available and whether the CA certificate path exists.
- **AI Engine does not respond**: Check whether the CA certificates of the two communication parties are consistent.
- **Training and test failure**: Check whether the path for saving the model files exists and whether the training prediction file is correctly downloaded.

- **Changing the AI Engine IP address**: Regenerate the certificate by following the steps in [Generating Certificates](intelligence-explain-environment-deployment.md#证书生成). Enter the new IP address in Generate a certificate and private key.

## Database Internal Errors

Problem: AI Engine connection failed.

```
ERROR:  AI engine connection failed.
CONTEXT:  referenced column: model_train_opt
```

Solution: Check whether the AI Engine is started or restarted properly. Check whether the CA certificates of the communication parties are consistent. Check whether the IP address and port number in the model configuration match.

Problem: The model does not exist.

```
ERROR:  OPT_Model not found for model name XXX
CONTEXT:  referenced column: track_model_train_opt
```

Solution: Check whether [GS_OPT_MODEL](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md) contains the model specified in the **model_name** column in the statement. If the error is reported when the prediction function is used, check whether the model has been trained.
