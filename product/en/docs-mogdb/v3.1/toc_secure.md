<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 3.1

## Security Guide

+ [Client Access Authentication](/security-guide/security/1-client-access-authentication.md)
+ [Managing Users and Their Permissions](/security-guide/security/2-managing-users-and-their-permissions.md)
+ [Configuring Database Audit](/security-guide/security/3-configuring-database-audit.md)
+ [Setting Encrypted Equality Query](/security-guide/security/4-setting-encrypted-equality-query.md)
+ [Setting a Ledger Database](/security-guide/security/5-setting-a-ledger-database.md)
+ [Transparent Data Encryption](/security-guide/security/6-transparent-data-encryption.md)