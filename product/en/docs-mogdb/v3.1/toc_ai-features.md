<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# MogDB Documentation 3.1

## AI Features Guide

+ [AI Features Overview](/AI-features/1-AI-features-overview.md)
+ [AI4DB: Autonomous Database O&M](/AI-features/ai4db/ai4db-autonomous-database-o&m.md)
  + [DBMind Mode](/AI-features/ai4db/dbmind-mode/dbmind-mode.md)
    + [service](/AI-features/ai4db/dbmind-mode/1-service.md)
    + [component](/AI-features/ai4db/dbmind-mode/2-component.md)
    + [set](/AI-features/ai4db/dbmind-mode/3-set.md)
  + [Components that Support DBMind](/AI-features/ai4db/components-that-support-dbmind/components-that-support-dbmind.md)
    + Prometheus Exporter
      + [Overview](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-overview.md)
      + [Environment Deployment](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-environment-deployment.md)
      + [Usage Guide](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-troubleshooting.md)
  + AI Sub-functions of the DBMind
    + X-Tuner: Parameter Tuning and Diagnosis
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-1-x-tuner-overview.md)
      + [Preparations](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-2-preparations.md)
      + [Examples](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-3-examples.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-4-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-5-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-6-Troubleshooting.md)
    + Index-advisor: Index Recommendation
      + [Single-query Index Recommendation](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-1-single-query-index-recommendation.md)
      + [Virtual Index](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-2-virtual-index.md)
      + [Workload-level Index Recommendation](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-3-workload-level-index-recommendation.md)
    + Slow Query Diagnosis: Root Cause Analysis for Slow SQL Statements
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-1-overview.md)
      + [Environment Deployment](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-2-environment-deployment.md)
      + [Usage Guide](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-3-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-4-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-5-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-6-troubleshooting.md)
    + Forecast: Trend Prediction
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-1-overview.md)
      + [Environment Deployment](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-2-environment-deployment.md)
      + [Usage Guide](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-3-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-4-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-5-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-6-troubleshooting.md)
    + SQLdiag: Slow SQL Discovery
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-1-overview.md)
      + [Usage Guide](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-2-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-3-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-4-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-5-troubleshooting.md)
    + SQL Rewriter
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-1-overview.md)
      + [Usage Guide](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-2-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-3-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-4-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-5-troubleshooting.md)
    + Anomaly Detection
      + [Overview](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-1-overview.md)
      + [Usage Guide](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-2-usage-guide.md)
      + [Obtaining Help Information](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-3-obtaining-help-information.md)
      + [Command Reference](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-4-command-reference.md)
      + [Troubleshooting](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-5-troubleshooting.md)
+ [DB4AI: Database-driven AI](/AI-features/db4ai/db4ai.md)
  + [Native DB4AI Engine](/AI-features/db4ai/native-db4ai-engine.md)
  + [Full-process AI](/AI-features/db4ai/full-process-ai/full-process-ai.md)
    + [PLPython Fenced Mode](/AI-features/db4ai/full-process-ai/plpython-fenced-mode.md)
    + [DB4AI-Snapshots for Data Version Management](/AI-features/db4ai/full-process-ai/db4ai-snapshots-for-data-version-management.md)
+ AI in DB
  + Intelligence Explain: SQL Statement Query Time Prediction
    + [Overview](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-overview.md)
    + [Environment Deployment](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-environment-deployment.md)
    + [User Guide](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-user-guide.md)
    + [Best Practices](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-best-practices.md)
    + [FAQs](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-faqs.md)
+ ABO Optimizer
  + Intelligent Cardinality Estimation
    + [Overview](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-overview.md)
    + [Prerequisites](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-prerequisites.md)
    + [Usage Guide](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-usage-guide.md)
    + [Best Practices](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-best-practices.md)
    + [Troubleshooting](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-troubleshooting.md)
  + Adaptive Plan Selection
    + [Overview](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-overview.md)
    + [Prerequisites](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-prerequisites.md)
    + [Usage Guide](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-usage-guide.md)
    + [Best Practices](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-best-practices.md)
    + [Troubleshooting](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-troubleshooting.md)