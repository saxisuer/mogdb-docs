---
title: Configuration Settings
summary: Configuration Settings
author: Guo Huan
date: 2022-04-29
---

# Configuration Settings

Publication-subscription requires some configuration options to be set.

On the publisher side, **wal_level** must be set to **logical**, and the value of **max_replication_slots** must be at least the number of subscriptions expected to be connected plus the number of connections reserved for table synchronization. Value of **max_wal_senders** ≥ Value of **max_replication_slots** + Number of physical replication slots that are connected at the same time + 1

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** If a subscriber is activated and subscribes to a publication, a temporary connection to the publisher needs to be established to check whether the publication subscribed to by the subscriber exists on the publisher. The publisher creates a temporary WAL sender. After the temporary connection is used up, it is disconnected and released immediately.

**max_replication_slots** must also be set on the subscriber. It must be set to at least the number of subscriptions that will be added to the subscriber. **max_logical_replication_workers** must be set to at least the number of subscriptions plus the number of connections reserved for table synchronization.