---
title: Quick Setup
summary: Quick Setup
author: Guo Huan
date: 2022-04-29
---

# Quick Setup

Set the following configuration items in the **postgresql.conf** file:

```bash
wal_level = logical
```

For a basic setup, retain the default values for the other necessary configuration items.

You need to adjust the **pg_hba.conf** file to allow replication (the value depends on the actual network configuration and the user used for connection).

```bash
host     all     repuser     0.0.0.0/0     sha256
```

In the publisher database:

```sql
CREATE PUBLICATION mypub FOR TABLE users, departments;
```

In the subscriber database:

```sql
CREATE SUBSCRIPTION mysub CONNECTION 'dbname=foo host=bar user=repuser' PUBLICATION mypub;
```

The above statements start the replication process, replicating incremental changes to those tables.
