---
title: Data Types
summary: Data Types
author: Guo Huan
date: 2021-03-04
---

# Data Types

A data type refers to a value set and an operation set defined on the value set. The MogDB database consists of tables, each of which is defined by its own columns. Each column corresponds to a data type. The MogDB uses corresponding functions to perform operations on data based on data types. For example, the MogDB can perform addition, subtraction, multiplication, and division operations on data of numeric values.
