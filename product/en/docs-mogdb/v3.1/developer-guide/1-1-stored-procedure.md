---
title: Stored Procedure
summary: Stored Procedure
author: Guo Huan
date: 2021-03-04
---

# Stored Procedure

In MogDB, business rules and logics are saved as stored procedures.

A stored procedure is a combination of SQL and PL/pgSQL. Stored procedures can move the code that executes business rules from applications to databases. Therefore, the code storage can be used by multiple programs at a time.

For details about how to create and call a stored procedure, see [CREATE PROCEDURE](../reference-guide/sql-syntax/CREATE-PROCEDURE.md).

The application methods for PL/pgSQL functions mentioned in [User-defined Functions](user-defined-functions.md) are similar to those for stored procedures. For details, please refer to [PL/pgSQL-SQL Procedural Language](../developer-guide/plpgsql/1-1-plpgsql-overview.md) section, unless otherwise specified, the contents apply to stored procedures and user-defined functions.
