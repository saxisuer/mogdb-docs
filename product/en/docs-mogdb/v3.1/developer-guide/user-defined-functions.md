---
title: User-defined Functions
summary: User-defined Functions
author: Guo Huan
date: 2021-03-04
---

# User-defined Functions

In the MogDB, in order to achieve specific functions, users can define functions, just like most other languages, in addition to system library functions, programmers will also write many custom functions.

MogDB user-defined functions are a combination of a set of SQL and procedure statements (declaration, allocation, loop, control flow, etc.) that are stored on the database server and can be called using the SQL interface.  In terms of presentation, user-defined functions and stored procedures are very similar, except that stored procedures have no return values but the functions have.

For details about how to create and call a user-defined function, see [CREATE FUNCTION](../reference-guide/sql-syntax/CREATE-FUNCTION.md).

The application methods for stored procedures mentioned in [Stored Procedure](1-1-stored-procedure.md) are similar to those for user-defined functions in this section. For details, please refer to [PL/pgSQL-SQL Procedural Language](../developer-guide/plpgsql/1-1-plpgsql-overview.md) section, unless otherwise specified, the contents apply to stored procedures and user-defined functions.
