---
title: cursor.fetchone()
summary: cursor.fetchone()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.fetchone()

## Function

This method extracts the next row of the query result set and returns a tuple.

## Prototype

```
cursor.fetchone()
```

## Parameter

None

## Return Value

A single tuple is the first result in the result set. If no more data is available, **None** is returned.

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).