---
title: curosr.executemany(query,vars_list)
summary: curosr.executemany(query,vars_list)
author: Zhang Cuiping
date: 2021-10-11
---

# curosr.executemany(query,vars_list)

## Function

This method executes an SQL command against all parameter sequences or mappings found in the sequence SQL.

## Prototype

```
curosr.executemany(query,vars_list)
```

## Parameter

**Table 1** curosr.executemany parameters

| **Keyword** | **Description**                                              |
| :---------- | :----------------------------------------------------------- |
| query       | SQL statement that you want to execute.                      |
| vars_list   | Variable list, which matches the **%s** placeholder in the query. |

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).
