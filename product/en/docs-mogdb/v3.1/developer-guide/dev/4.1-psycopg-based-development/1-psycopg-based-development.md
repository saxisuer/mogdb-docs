---
title: Psycopg-Based Development
summary: Psycopg-Based Development
author: Zhang Cuiping
date: 2021-10-11
---

# Psycopg-Based Development

Psycopg is a Python API used to execute SQL statements and provides a unified access API for PostgreSQL and MogDB. Applications can perform data operations based on psycopg. Psycopg2 is an encapsulation of libpq and is implemented using the C language, which is efficient and secure. It provides cursors on both clients and servers, asynchronous communication and notification, and the COPY TO and COPY FROM functions. Psycopg2 supports multiple types of Python out-of-the-box and adapts to PostgreSQL data types. Through the flexible object adaptation system, you can extend and customize the adaptation. Psycopg2 is compatible with Unicode and Python 3.

MogDB supports the psycopg2 feature and allows psycopg2 to be connected in SSL mode.

**Table 1** Platforms supported by Psycopg

| OS          | Platform |
| :---------- | :------- |
| EulerOS 2.5 | x86_64   |
| EulerOS 2.8 | ARM64    |
