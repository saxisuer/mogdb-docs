---
title: Psycopg Package
summary: Psycopg Package
author: Zhang Cuiping
date: 2021-10-11
---

# Psycopg Package

The psycopg package is obtained from the [openGauss Download](https://opengauss.org/en/download/) page.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/psycopg-package-en.png)

After the decompression, the following folders are generated:

- **psycopg2**: **psycopg2** library file
- **lib**: **lib** library file