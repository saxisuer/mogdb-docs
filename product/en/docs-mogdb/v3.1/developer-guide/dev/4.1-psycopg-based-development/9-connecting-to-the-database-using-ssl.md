---
title: Connecting to the Database
summary: Connecting to the Database
author: Zhang Cuiping
date: 2021-10-11
---

# Connecting to the Database (Using SSL)

When you use psycopy2 to connect to the MogDB server, you can enable SSL to encrypt the communication between the client and server. To enable SSL, you must have the server certificate, client certificate, and private key files. For details on how to obtain these files, see related documents and commands of OpenSSL.

1. Use the .ini file (the **configparser** package of Python can parse this type of configuration file) to save the configuration information about the database connection.
2. Add SSL connection parameters **sslmode**, **sslcert**, **sslkey**, and **sslrootcert** to the connection options.
   1. **sslmode**: [Table 1](#table1.1)
   2. **sslcert**: client certificate path
   3. **sslkey**: client key path
   4. **sslrootcert**: root certificate path
3. Use the **psycopg2.connect** function to obtain the connection object.
4. Use the connection object to create a cursor object.

**Table 1** sslmode options <a id="table1.1"> </a>

| sslmode     | Whether SSL Encryption Is Enabled | Description                                                  |
| :---------- | :-------------------------------- | :----------------------------------------------------------- |
| disable     | No                                | SSL connection is not enabled.                               |
| allow       | Possible                          | If the database server requires SSL connection, SSL connection can be enabled. However, authenticity of the database server will not be verified. |
| prefer      | Possible                          | If the database supports SSL connection, SSL connection is preferred. However, authenticity of the database server will not be verified. |
| require     | Yes                               | SSL connection is required and data is encrypted. However, authenticity of the database server will not be verified. |
| verify-ca   | Yes                               | The SSL connection must be enabled.                          |
| verify-full | Yes                               | The SSL connection must be enabled, which is not supported by MogDB currently. |
