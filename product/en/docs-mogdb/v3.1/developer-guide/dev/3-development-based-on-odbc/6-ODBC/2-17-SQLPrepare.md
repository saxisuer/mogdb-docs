---
title: SQLPrepare
summary: SQLPrepare
author: Guo Huan
date: 2021-05-17
---

# SQLPrepare

## Function

SQLPrepare is used to prepare an SQL statement to be executed.

## Prototype

```
SQLRETURN SQLPrepare(SQLHSTMT      StatementHandle,
                     SQLCHAR       *StatementText,
                     SQLINTEGER    TextLength);
```

## Parameter

**Table 1** SQLPrepare parameters

| **Keyword**     | **Parameter Description**    |
| :-------------- | :--------------------------- |
| StatementHandle | Statement handle.            |
| StatementText   | SQL text string.             |
| TextLength      | Length of **StatementText**. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.
- **SQL_STILL_EXECUTING** indicates that the statement is being executed.

## Precautions

If SQLPrepare returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_STMT** and **StatementHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See [Examples](2-23-Examples.md).
