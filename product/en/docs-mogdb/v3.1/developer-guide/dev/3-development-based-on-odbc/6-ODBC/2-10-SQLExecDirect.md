---
title: SQLExecDirect
summary: SQLExecDirect
author: Guo Huan
date: 2021-05-17
---

# SQLExecDirect

## Function

SQLExecDirect is used to execute a prepared SQL statement specified in this parameter. This is the fastest method for executing only one SQL statement at a time.

## Prototype

```
SQLRETURN SQLExecDirect(SQLHSTMT         StatementHandle,
                        SQLCHAR         *StatementText,
                        SQLINTEGER       TextLength);
```

## Parameter

**Table 1** SQLExecDirect parameters

| **Keyword**     | **Parameter Description**                                    |
| :-------------- | :----------------------------------------------------------- |
| StatementHandle | Statement handle, obtained from SQLAllocHandle.              |
| StatementText   | SQL statement to be executed. One SQL statement can be executed at a time. |
| TextLength      | Length of **StatementText**.                                 |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_NEED_DATA** indicates that parameters provided before executing the SQL statement are insufficient.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.
- **SQL_STILL_EXECUTING** indicates that the statement is being executed.
- **SQL_NO_DATA** indicates that the SQL statement does not return a result set.

## Precautions

If SQLExecDirect returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_STMT** and **StatementHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See [Examples](2-23-Examples.md).
