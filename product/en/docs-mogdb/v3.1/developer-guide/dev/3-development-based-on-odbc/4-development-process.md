---
title: Development Process
summary: Development Process
author: Guo Huan
date: 2021-04-26
---

# Development Process

**Figure 1** ODBC-based application development process

![odbc-based-application-development-process](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-process-4.png)

**APIs Involved in the Development Process**

**Table 1** API description

| **Function**                                              | **API**                                                      |
| :-------------------------------------------------------- | :----------------------------------------------------------- |
| Allocate a handle                                         | SQLAllocHandle is a generic function for allocating a handle. It can replace the following functions:<br />- SQLAllocEnv: allocate an environment handle<br />- SQLAllocConnect: allocate a connection handle<br />- SQLAllocStmt: allocate a statement handle |
| Set environment attributes                                | SQLSetEnvAttr                                                |
| Set connection attributes                                 | SQLSetConnectAttr                                            |
| Set statement attributes                                  | SQLSetStmtAttr                                               |
| Connect to a data source                                  | SQLConnect                                                   |
| Bind a buffer to a column in the result set               | SQLBindCol                                                   |
| Bind the parameter marker of an SQL statement to a buffer | SQLBindParameter                                             |
| Return the error message of the last operation            | SQLGetDiagRec                                                |
| Prepare an SQL statement for execution                    | SQLPrepare                                                   |
| Run a prepared SQL statement                              | SQLExecute                                                   |
| Run an SQL statement directly                             | SQLExecDirect                                                |
| Fetch the next row (or rows) from the result set          | SQLFetch                                                     |
| Return data in a column of the result set                 | SQLGetData                                                   |
| Get the column information from a result set              | SQLColAttribute                                              |
| Disconnect from a data source                             | SQLDisconnect                                                |
| Release a handle                                          | SQLFreeHandle is a generic function for releasing a handle. It can replace the following functions:<br />- SQLFreeEnv: release an environment handle<br />- SQLFreeConnect: release a connection handle<br />- SQLFreeStmt: release a statement handle |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> If an execution request (not in a transaction block) received in the database contains multiple statements, the request is packed into a transaction. If one of the statements fails, the entire request will be rolled back.
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif)**NOTICE:**
>
> ODBC is the central layer of the application program and the database. It is responsible for transmitting the SQL instructions issued by the application program to the database, and does not parse the SQL syntax by itself. Therefore, when an SQL statement with confidential information (such as a plaintext password) is written in an application, the confidential information will be exposed in the driver log.
