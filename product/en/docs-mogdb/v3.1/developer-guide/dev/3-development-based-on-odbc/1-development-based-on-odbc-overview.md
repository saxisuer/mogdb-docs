---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-04-26
---

# Overview

Open Database Connectivity (ODBC) is a Microsoft API for accessing databases based on the X/OPEN CLI. Applications interact with the database through the APIs provided by ODBC, which enhances their portability, scalability, and maintainability.

[Figure 1](#ODBC) shows the system structure of ODBC.

<a id="ODBC">**Figure 1** </a>ODBC system structure

![odbc-system-structure](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-based-on-odbc-overview-2.png)

MogDB supports ODBC 3.5 in the following environments.

**Table 1** OSs Supported by ODBC

| OS                                                 | Platform |
| :------------------------------------------------- | :------- |
| CentOS 6.4/6.5/6.6/6.7/6.8/6.9/7.0/7.1/7.2/7.3/7.4 | x86_64   |
| CentOS 7.6                                         | ARM64    |
| EulerOS 2.0 SP2/SP3                                | x86_64   |
| EulerOS 2.0 SP8                                    | ARM64    |

The ODBC Driver Manager running on UNIX or Linux can be unixODBC or iODBC. unixODBC-2.3.0 is used as the component for connecting the database.

Windows has a native ODBC Driver Manager. You can locate **Data Sources (ODBC)** by choosing **Control Panel** &gt; **Administrative Tools**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> The current database ODBC driver is based on an open-source version and may be incompatible with data types tinyint, smalldatetime, and nvarchar2.
