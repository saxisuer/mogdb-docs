---
title: javax.sql.PooledConnection
summary: javax.sql.PooledConnection
author: Guo Huan
date: 2021-05-17
---

# javax.sql.PooledConnection

This section describes **javax.sql.PooledConnection**, the connection interface created by a connection pool.

**Table 1** Support status for javax.sql.PooledConnection

| Method Name                                                  | Return Type | JDBC 4 Is Supported Or Not |
| :----------------------------------------------------------- | :---------- | :------------------------- |
| addConnectionEventListener (ConnectionEventListener listener) | void        | Yes                        |
| close()                                                      | void        | Yes                        |
| getConnection()                                              | Connection  | Yes                        |
| removeConnectionEventListener (ConnectionEventListener listener) | void        | Yes                        |
