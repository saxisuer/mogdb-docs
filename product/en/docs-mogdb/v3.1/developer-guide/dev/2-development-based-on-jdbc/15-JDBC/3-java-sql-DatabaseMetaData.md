---
title: java.sql.DatabaseMetaData
summary: java.sql.DatabaseMetaData
author: Guo Huan
date: 2021-05-17
---

# java.sql.DatabaseMetaData

This section describes **java.sql.DatabaseMetaData**, the interface for defining database objects.

**Table 1** Support status for java.sql.DatabaseMetaData

| Method Name                                                  | Return Type  | JDBC 4 Is Supported Or Not |
| :----------------------------------------------------------- | :----------- | :------------------------- |
| allProceduresAreCallable()                                   | boolean      | Yes                        |
| allTablesAreSelectable()                                     | boolean      | Yes                        |
| autoCommitFailureClosesAllResultSets()                       | boolean      | Yes                        |
| dataDefinitionCausesTransactionCommit()                      | boolean      | Yes                        |
| dataDefinitionIgnoredInTransactions()                        | boolean      | Yes                        |
| deletesAreDetected(int type)                                 | boolean      | Yes                        |
| doesMaxRowSizeIncludeBlobs()                                 | boolean      | Yes                        |
| generatedKeyAlwaysReturned()                                 | boolean      | Yes                        |
| getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable) | ResultSet    | Yes                        |
| getCatalogs()                                                | ResultSet    | Yes                        |
| getCatalogSeparator()                                        | String       | Yes                        |
| getCatalogTerm()                                             | String       | Yes                        |
| getClientInfoProperties()                                    | ResultSet    | Yes                        |
| getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern) | ResultSet    | Yes                        |
| getConnection()                                              | Connection   | Yes                        |
| getCrossReference(String parentCatalog, String parentSchema, String parentTable, String foreignCatalog, String foreignSchema, String foreignTable) | ResultSet    | Yes                        |
| getDefaultTransactionIsolation()                             | int          | Yes                        |
| getExportedKeys(String catalog, String schema, String table) | ResultSet    | Yes                        |
| getExtraNameCharacters()                                     | String       | Yes                        |
| getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern) | ResultSet    | Yes                        |
| getFunctions(String catalog, String schemaPattern, String functionNamePattern) | ResultSet    | Yes                        |
| getIdentifierQuoteString()                                   | String       | Yes                        |
| getImportedKeys(String catalog, String schema, String table) | ResultSet    | Yes                        |
| getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate) | ResultSet    | Yes                        |
| getMaxBinaryLiteralLength()                                  | int          | Yes                        |
| getMaxCatalogNameLength()                                    | int          | Yes                        |
| getMaxCharLiteralLength()                                    | int          | Yes                        |
| getMaxColumnNameLength()                                     | int          | Yes                        |
| getMaxColumnsInGroupBy()                                     | int          | Yes                        |
| getMaxColumnsInIndex()                                       | int          | Yes                        |
| getMaxColumnsInOrderBy()                                     | int          | Yes                        |
| getMaxColumnsInSelect()                                      | int          | Yes                        |
| getMaxColumnsInTable()                                       | int          | Yes                        |
| getMaxConnections()                                          | int          | Yes                        |
| getMaxCursorNameLength()                                     | int          | Yes                        |
| getMaxIndexLength()                                          | int          | Yes                        |
| getMaxLogicalLobSize()                                       | default long | Yes                        |
| getMaxProcedureNameLength()                                  | int          | Yes                        |
| getMaxRowSize()                                              | int          | Yes                        |
| getMaxSchemaNameLength()                                     | int          | Yes                        |
| getMaxStatementLength()                                      | int          | Yes                        |
| getMaxStatements()                                           | int          | Yes                        |
| getMaxTableNameLength()                                      | int          | Yes                        |
| getMaxTablesInSelect()                                       | int          | Yes                        |
| getMaxUserNameLength()                                       | int          | Yes                        |
| getNumericFunctions()                                        | String       | Yes                        |
| getPrimaryKeys(String catalog, String schema, String table)  | ResultSet    | Yes                        |
| getPartitionTablePrimaryKeys(String catalog, String schema, String table) | ResultSet    | Yes                        |
| getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern) | ResultSet    | Yes                        |
| getProcedures(String catalog, String schemaPattern, String procedureNamePattern) | ResultSet    | Yes                        |
| getProcedureTerm()                                           | String       | Yes                        |
| getSchemas()                                                 | ResultSet    | Yes                        |
| getSchemas(String catalog, String schemaPattern)             | ResultSet    | Yes                        |
| getSchemaTerm()                                              | String       | Yes                        |
| getSearchStringEscape()                                      | String       | Yes                        |
| getSQLKeywords()                                             | String       | Yes                        |
| getSQLStateType()                                            | int          | Yes                        |
| getStringFunctions()                                         | String       | Yes                        |
| getSystemFunctions()                                         | String       | Yes                        |
| getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern) | ResultSet    | Yes                        |
| getTimeDateFunctions()                                       | String       | Yes                        |
| getTypeInfo()                                                | ResultSet    | Yes                        |
| getUDTs(String catalog, String schemaPattern, String typeNamePattern, int[] types) | ResultSet    | Yes                        |
| getURL()                                                     | String       | Yes                        |
| getVersionColumns(String catalog, String schema, String table) | ResultSet    | Yes                        |
| insertsAreDetected(int type)                                 | boolean      | Yes                        |
| locatorsUpdateCopy()                                         | boolean      | Yes                        |
| othersDeletesAreVisible(int type)                            | boolean      | Yes                        |
| othersInsertsAreVisible(int type)                            | boolean      | Yes                        |
| othersUpdatesAreVisible(int type)                            | boolean      | Yes                        |
| ownDeletesAreVisible(int type)                               | boolean      | Yes                        |
| ownInsertsAreVisible(int type)                               | boolean      | Yes                        |
| ownUpdatesAreVisible(int type)                               | boolean      | Yes                        |
| storesLowerCaseIdentifiers()                                 | boolean      | Yes                        |
| storesMixedCaseIdentifiers()                                 | boolean      | Yes                        |
| storesUpperCaseIdentifiers()                                 | boolean      | Yes                        |
| supportsBatchUpdates()                                       | boolean      | Yes                        |
| supportsCatalogsInDataManipulation()                         | boolean      | Yes                        |
| supportsCatalogsInIndexDefinitions()                         | boolean      | Yes                        |
| supportsCatalogsInPrivilegeDefinitions()                     | boolean      | Yes                        |
| supportsCatalogsInProcedureCalls()                           | boolean      | Yes                        |
| supportsCatalogsInTableDefinitions()                         | boolean      | Yes                        |
| supportsCorrelatedSubqueries()                               | boolean      | Yes                        |
| supportsDataDefinitionAndDataManipulationTransactions()      | boolean      | Yes                        |
| supportsDataManipulationTransactionsOnly()                   | boolean      | Yes                        |
| supportsGetGeneratedKeys()                                   | boolean      | Yes                        |
| supportsMixedCaseIdentifiers()                               | boolean      | Yes                        |
| supportsMultipleOpenResults()                                | boolean      | Yes                        |
| supportsNamedParameters()                                    | boolean      | Yes                        |
| supportsOpenCursorsAcrossCommit()                            | boolean      | Yes                        |
| supportsOpenCursorsAcrossRollback()                          | boolean      | Yes                        |
| supportsOpenStatementsAcrossCommit()                         | boolean      | Yes                        |
| supportsOpenStatementsAcrossRollback()                       | boolean      | Yes                        |
| supportsPositionedDelete()                                   | boolean      | Yes                        |
| supportsPositionedUpdate()                                   | boolean      | Yes                        |
| supportsRefCursors()                                         | boolean      | Yes                        |
| supportsResultSetConcurrency(int type, int concurrency)      | boolean      | Yes                        |
| supportsResultSetType(int type)                              | boolean      | Yes                        |
| supportsSchemasInIndexDefinitions()                          | boolean      | Yes                        |
| supportsSchemasInPrivilegeDefinitions()                      | boolean      | Yes                        |
| supportsSchemasInProcedureCalls()                            | boolean      | Yes                        |
| supportsSchemasInTableDefinitions()                          | boolean      | Yes                        |
| supportsSelectForUpdate()                                    | boolean      | Yes                        |
| supportsStatementPooling()                                   | boolean      | Yes                        |
| supportsStoredFunctionsUsingCallSyntax()                     | boolean      | Yes                        |
| supportsStoredProcedures()                                   | boolean      | Yes                        |
| supportsSubqueriesInComparisons()                            | boolean      | Yes                        |
| supportsSubqueriesInExists()                                 | boolean      | Yes                        |
| supportsSubqueriesInIns()                                    | boolean      | Yes                        |
| supportsSubqueriesInQuantifieds()                            | boolean      | Yes                        |
| supportsTransactionIsolationLevel(int level)                 | boolean      | Yes                        |
| supportsTransactions()                                       | boolean      | Yes                        |
| supportsUnion()                                              | boolean      | Yes                        |
| supportsUnionAll()                                           | boolean      | Yes                        |
| updatesAreDetected(int type)                                 | boolean      | Yes                        |
| getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types) | ResultSet    | Yes                        |
| getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) | ResultSet    | Yes                        |
| getTableTypes()                                              | ResultSet    | Yes                        |
| getUserName()                                                | String       | Yes                        |
| isReadOnly()                                                 | boolean      | Yes                        |
| nullsAreSortedHigh()                                         | boolean      | Yes                        |
| nullsAreSortedLow()                                          | boolean      | Yes                        |
| nullsAreSortedAtStart()                                      | boolean      | Yes                        |
| nullsAreSortedAtEnd()                                        | boolean      | Yes                        |
| getDatabaseProductName()                                     | String       | Yes                        |
| getDatabaseProductVersion()                                  | String       | Yes                        |
| getDriverName()                                              | String       | Yes                        |
| getDriverVersion()                                           | String       | Yes                        |
| getDriverMajorVersion()                                      | int          | Yes                        |
| getDriverMinorVersion()                                      | int          | Yes                        |
| usesLocalFiles()                                             | boolean      | Yes                        |
| usesLocalFilePerTable()                                      | boolean      | Yes                        |
| supportsMixedCaseIdentifiers()                               | boolean      | Yes                        |
| storesUpperCaseIdentifiers()                                 | boolean      | Yes                        |
| storesLowerCaseIdentifiers()                                 | boolean      | Yes                        |
| supportsMixedCaseQuotedIdentifiers()                         | boolean      | Yes                        |
| storesUpperCaseQuotedIdentifiers()                           | boolean      | Yes                        |
| storesLowerCaseQuotedIdentifiers()                           | boolean      | Yes                        |
| storesMixedCaseQuotedIdentifiers()                           | boolean      | Yes                        |
| supportsAlterTableWithAddColumn()                            | boolean      | Yes                        |
| supportsAlterTableWithDropColumn()                           | boolean      | Yes                        |
| supportsColumnAliasing()                                     | boolean      | Yes                        |
| nullPlusNonNullIsNull()                                      | boolean      | Yes                        |
| supportsConvert()                                            | boolean      | Yes                        |
| supportsConvert(int fromType, int toType)                    | boolean      | Yes                        |
| supportsTableCorrelationNames()                              | boolean      | Yes                        |
| supportsDifferentTableCorrelationNames()                     | boolean      | Yes                        |
| supportsExpressionsInOrderBy()                               | boolean      | Yes                        |
| supportsOrderByUnrelated()                                   | boolean      | Yes                        |
| supportsGroupBy()                                            | boolean      | Yes                        |
| supportsGroupByUnrelated()                                   | boolean      | Yes                        |
| supportsGroupByBeyondSelect()                                | boolean      | Yes                        |
| supportsLikeEscapeClause()                                   | boolean      | Yes                        |
| supportsMultipleResultSets()                                 | boolean      | Yes                        |
| supportsMultipleTransactions()                               | boolean      | Yes                        |
| supportsNonNullableColumns()                                 | boolean      | Yes                        |
| supportsMinimumSQLGrammar()                                  | boolean      | Yes                        |
| supportsCoreSQLGrammar()                                     | boolean      | Yes                        |
| supportsExtendedSQLGrammar()                                 | boolean      | Yes                        |
| supportsANSI92EntryLevelSQL()                                | boolean      | Yes                        |
| supportsANSI92IntermediateSQL()                              | boolean      | Yes                        |
| supportsANSI92FullSQL()                                      | boolean      | Yes                        |
| supportsIntegrityEnhancementFacility()                       | boolean      | Yes                        |
| supportsOuterJoins()                                         | boolean      | Yes                        |
| supportsFullOuterJoins()                                     | boolean      | Yes                        |
| supportsLimitedOuterJoins()                                  | boolean      | Yes                        |
| isCatalogAtStart()                                           | boolean      | Yes                        |
| supportsSchemasInDataManipulation()                          | boolean      | Yes                        |
| supportsSavepoints()                                         | boolean      | Yes                        |
| supportsResultSetHoldability(int holdability)                | boolean      | Yes                        |
| getResultSetHoldability()                                    | int          | Yes                        |
| getDatabaseMajorVersion()                                    | int          | Yes                        |
| getDatabaseMinorVersion()                                    | int          | Yes                        |
| getJDBCMajorVersion()                                        | int          | Yes                        |
| getJDBCMinorVersion()                                        | int          | Yes                        |

> **NOTE:** If the value of **uppercaseAttributeName** is **true**, the following APIs convert the query result to uppercase letters. The conversion range is the same as that of the **toUpperCase** method in Java. 
>
> - public ResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern) 
> - public ResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern) 
> - public ResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types) 
> - public ResultSet getSchemas(String catalog, String schemaPattern) 
> - public ResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) 
> - public ResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern) 
> - public ResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern) 
> - public ResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable) 
> - public ResultSet getPrimaryKeys(String catalog, String schema, String table) 
> - protected ResultSet getImportedExportedKeys(String primaryCatalog, String primarySchema, String primaryTable, String foreignCatalog, String foreignSchema, String foreignTable) 
> - public ResultSet getIndexInfo(String catalog, String schema, String tableName, boolean unique, boolean approximate) 
> - public ResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern, int[] types) 
> - public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern)

> **CAUTION:** The **getPartitionTablePrimaryKeys(String catalog, String schema, String table)** API is used to obtain the primary key column of a partitioned table that contains global indexes. The following is an example:
>
> ```
> PgDatabaseMetaData dbmd = (PgDatabaseMetaData)conn.getMetaData();
> dbmd.getPartitionTablePrimaryKeys("catalogName", "schemaName", "tableName");
> ```