---
title: JDBC Package, Driver Class, and Environment Class
summary: JDBC Package, Driver Class, and Environment Class
author: Guo Huan
date: 2021-04-26
---

# JDBC Package, Driver Class, and Environment Class

**JDBC Package**

Run **build.sh** in the source code directory on Linux OS to obtain the driver JAR package **postgresql.jar**, which is stored in the source code directory. Obtain the package from the release package named [**openGauss-x.x.x-JDBC.tar.gz**](https://opengauss.org/en/download.html).

The driver package is compatible with PostgreSQL. The class name and structure in the driver are the same as those in the PostgreSQL driver. All applications running on PostgreSQL can be smoothly migrated to the current system.

**Driver Class**

Before establishing a database connection, load the **org.opengauss.Driver** database driver class.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - MogDB is compatible with PostgreSQL in the use of JDBC. Therefore, the org.opengauss.Driver class will conflict when the JDBC driver packages for PostgreSQL and openGauss are loaded in the same JVM virtual machine at the same time.
>
> - Compared with the PostgreSQL driver, the openGauss JDBC driver has the following enhanced features:
>   - The SHA256 encryption mode is supported for login.
>   - The third-party log framework that implements the sf4j API can be connected.
>   - DR failover is supported.

**Environment Class**

JDK 1.8 must be configured on the client. The configuration method is as follows:

1. In the MS-DOS window, run **java -version** to check the JDK version. Ensure that the version is JDK 1.8. If JDK is not installed, download the installation package from the official website and install it. If the system environment JDK version is lower than 1.8, please refer to [Use WebSphere to Configure MogDB Data Sources](../../../quick-start/mogdb-access/use-middleware-to-access-mogdb/websphere-configures-mogdb-data-source-reference.md).

2. Configure system environment variables.

   1. Right-click **My computer** and choose **Properties**.

   2. In the navigation pane, choose **Advanced system settings**.

   3. In the **System Properties** dialog box, click **Environment Variables** on the **Advanced** tab page.

   4. In the **System variables** area of the **Environment Variables** dialog box, click **New** or **Edit** to configure system variables. For details, see [Table 1](#Description).

      **Table 1** Description <a id="Description"> </a>

      | Variable  | Operation                                                    | Variable Value                                               |
      | :-------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
      | JAVA_HOME | - If the variable exists, click **Edit**.<br />- If the variable does not exist, click **New**. | Specifies the Java installation directory.<br />Example: C:\Program Files\Java\jdk1.8.0_131 |
      | Path      | Edit                                                         | - If JAVA_HOME is configured, add **%JAVA_HOME%\bin** before the variable value.<br />- If JAVA_HOME is not configured, add the full Java installation path before the variable value:<br />C:\Program Files\Java\jdk1.8.0_131\bin; |
      | CLASSPATH | New                                                          | .;%JAVA_HOME%\lib;%JAVA_H                                    |
