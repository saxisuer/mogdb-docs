---
title:  Commissioning
summary:  Commissioning
author:  Guo Huan
date: 2021-04-27
---

# Commissioning

To control the output of log files and better understand the operating status of the database, modify specific configuration parameters in the **postgresql.conf** file in the instance data directory.

[Table 1](#Configuration parameters) describes the adjustable configuration parameters.

**Table 1** Configuration parameters<a id="Configuration parameters"></a>

| Parameter                                                    | Description                           | Value Range                           | Remarks                               |
| ------------------------------------------------------------ | ------------------------------------- | ------------------------------------- | ------------------------------------- |
| client_min_messages                | Level of messages to be sent to clients.                     | - DEBUG5<br />- DEBUG4<br />- DEBUG3<br />- DEBUG2<br />- DEBUG1<br />- LOG<br />- NOTICE<br />WARNING<br />- ERROR<br />- FATAL<br />- PANIC<br />Default value: NOTICE | Messages of the set level or lower will be sent to clients. The lower the level is, the fewer the messages will be sent. |
| log_min_messages                   | Level of messages to be recorded in server logs.             | - DEBUG5<br />- DEBUG4<br />- DEBUG3<br />- DEBUG2<br />- DEBUG1<br />- INFO<br />- NOTICE<br />- WARNING<br />- ERROR<br />- LOG<br />- FATAL<br />- PANIC<br />Default value: WARNING | Messages higher than the set level will be recorded in logs. The higher the level is, the fewer the server logs will be recorded. |
| log_min_error_statement            | Level of SQL error statements to be recorded in server logs. | - DEBUG5<br />- DEBUG4<br />- DEBUG3<br />- DEBUG2<br />- DEBUG1<br />- INFO<br />- NOTICE<br />- WARNING<br />- ERROR<br />- FATAL<br />- PANIC<br />Default value: ERROR。 | SQL error statements of the set level or higher will be recorded in server logs.Only a system administrator is allowed to modify this parameter. |
| log_min_duration_statement         | Minimum execution duration of a statement. If the execution duration of a statement is equal to or longer than the set milliseconds, the statement and its duration will be recorded in logs. Enabling this function can help you track the query attempts to be optimized. | INT type<br />Default value: 30min<br />Unit: millisecond    | The default value (-1) indicates that the function is disabled.Only a system administrator is allowed to modify this parameter. |
| log_connections/log_disconnections | Whether to record a server log message when each session is connected or disconnected. | - **on**: The system records a log server when each session is connected or disconnected.<br />- **off**: The system does not record a log server when each session is connected or disconnected.<br />Default value: off | -                                                            |
| log_duration                       | Whether to record the duration of each executed statement.   | - **on**: The system records the duration of each executed statement.<br />- **off**: The system does not record the duration of each executed statement.<br />Default value: on | Only a system administrator is allowed to modify this parameter. |
| log_statement                      | SQL statements to be recorded in logs.                       | - **none**: The system does not record any SQL statements.<br />- **ddl**: The system records data definition statements.<br />- **mod**: The system records data definition statements and data operation statements.<br />- **all**: The system records all statements.<br />Default value: none | Only a system administrator is allowed to modify this parameter. |
| log_hostname                       | Whether to record host names.                                | - **on**: The system records host names.<br />- **off**: The system does not record host names.<br />Default value: off | By default, connection logs only record the IP addresses of connected hosts. With this function, the host names will also be recorded.This parameter affects parameters in **Querying Audit Results**, GS_WLM_SESSION_HISTORY, PG_STAT_ACTIVITY, and **log_line_prefix**. |

[Table 2](#description) describes the preceding parameter levels.

<a id="description">**Table 2**</a> Description of log level parameters

| Level      | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| DEBUG[1-5] | Provides information that can be used by developers. Level 1 is the lowest level whereas level 5 is the highest level. |
| INFO       | Provides information about users' hidden requests, for example, information about the VACUUM VERBOSE process. |
| NOTICE     | Provides information that may be important to users, for example, truncations of long identifiers or indexes created as a part of a primary key. |
| WARNING    | Provides warning information for users, for example, COMMIT out of transaction blocks. |
| ERROR      | Reports an error that causes a command to terminate.         |
| LOG        | Reports information that administrators may be interested in, for example, the activity levels of check points. |
| FATAL      | Reports the reason that causes a session to terminate.       |
| PANIC      | Reports the reason that causes all sessions to terminate.    |
