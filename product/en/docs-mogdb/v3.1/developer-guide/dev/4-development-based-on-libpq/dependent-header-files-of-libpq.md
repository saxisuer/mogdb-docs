---
title: Dependent Header Files of libpq
summary: Dependent Header Files of libpq
author: Guo Huan
date: 2022-04-26
---

# Dependent Header Files of libpq

Client programs that use **libpq** must include the header file **libpq-fe.h** and must link with the libpq library.
