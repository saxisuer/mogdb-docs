---
title: Development Process
summary: Development Process
author: Guo Huan
date: 2022-04-26
---

# Development Process

To compile and connect to a libpq source program, perform the following operations:

1. Decompress the release package (for example, `MogDB-3.0.1-CentOS-64bit-Libpq.tar.gz`). The required header file is stored in the **include** folder, and the **lib** folder contains the required libpq library file.

   > **NOTE:** In addition to **libpq-fe.h**, the **include** folder contains the header files **postgres_ext.h**, **gs_thread.h**, and **gs_threadlocal.h** by default. These three header files are the dependency files of **libpq-fe.h**.

2. Include the **libpq-fe.h** header file.

   ```
   #include <libpq-fe.h>
   ```

3. Provide the **-I** *directory* option to provide the installation location of the header files. (Sometimes the compiler looks for the default directory, so this option can be ignored.) Example:

   ```
   gcc -I (Directory where the header files are located) -L (Directory where the libpq library is located) testprog.c -lpq
   ```

4. If the makefile is used, add the following options to variables *CPPFLAGS*, *LDFLAGS*, and *LIBS*:

   ```
   CPPFLAGS += -I (Directory where the header files are located)
   LDFLAGS += -L (Directory where the libpq library is located)
   LIBS += -lpq
   ```