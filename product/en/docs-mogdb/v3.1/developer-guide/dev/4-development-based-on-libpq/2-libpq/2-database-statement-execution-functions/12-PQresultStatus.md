---
title: PQresultStatus
summary: PQresultStatus
author: Guo Huan
date: 2021-05-17
---

# PQresultStatus

## Function

PQresultStatus is used to return the result status of a command.

## Prototype

```
ExecStatusType PQresultStatus(const PGresult *res);
```

## Parameter

**Table 1** PQresultStatus parameter

| **Keyword** | **Parameter Description**                      |
| :---------- | :--------------------------------------------- |
| res         | Object pointer that contains the query result. |

## Return Value

**PQresultStatus** indicates the command execution status. The enumerated values are as follows:

```
PQresultStatus can return one of the following values:
PGRES_EMPTY_QUERY
The string sent to the server was empty.

PGRES_COMMAND_OK
A command that does not return data was successfully executed.

PGRES_TUPLES_OK
A query (such as SELECT or SHOW) that returns data was successfully executed.

PGRES_COPY_OUT
Copy Out (from the server) data transfer started.

PGRES_COPY_IN
Copy In (to the server) data transfer started.

PGRES_BAD_RESPONSE
The response from the server cannot be understood.

PGRES_NONFATAL_ERROR
A non-fatal error (notification or warning) occurred.

PGRES_FATAL_ERROR
A fatal error occurred.

PGRES_COPY_BOTH
Copy In/Out (to and from the server) data transfer started. This state occurs only in streaming replication.

PGRES_SINGLE_TUPLE
PGresult contains a result tuple from the current command. This state occurs in a single-row query.
```

## Precautions

- Note that the SELECT command that happens to retrieve zero rows still returns **PGRES_TUPLES_OK**. **PGRES_COMMAND_OK** is used for commands that can never return rows (such as INSERT or UPDATE, without return clauses). The result status **PGRES_EMPTY_QUERY** might indicate a bug in the client software.
- The result status **PGRES_NONFATAL_ERROR** will never be returned directly by PQexec or other query execution functions. Instead, such results will be passed to the notice processor.

## Example

For details, see Example.
