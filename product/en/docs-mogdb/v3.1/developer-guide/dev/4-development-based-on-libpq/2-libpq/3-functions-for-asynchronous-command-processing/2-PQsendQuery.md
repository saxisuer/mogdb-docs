---
title: PQsendQuery
summary: PQsendQuery
author: Guo Huan
date: 2021-05-17
---

# PQsendQuery

## Function

PQsendQuery is used to commit a command to the server without waiting for the result. If the query is successful, **1** is returned. Otherwise, **0** is returned.

## Prototype

```c
int PQsendQuery(PGconn *conn, const char *command);
```

## Parameter

**Table 1** PQsendQuery parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |
| command     | Query string to be executed.                                 |

## Return Value

**int** indicates the execution result. **1** indicates successful execution and **0** indicates an execution failure. The failure cause is stored in **conn-&gt;errorMessage**.

## Precautions

After PQsendQuery is successfully called, call PQgetResult one or more times to obtain the results. PQsendQuery cannot be called again (on the same connection) until PQgetResult returns a null pointer, indicating that the command execution is complete.

## Example

For details, see Example.
