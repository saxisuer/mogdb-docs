---
title: PQntuples
summary: PQntuples
author: Guo Huan
date: 2021-05-17
---

# PQntuples

## Function

PQntuples is used to return the number of rows (tuples) in the query result. An overflow may occur if the return value is out of the value range allowed in a 32-bit OS.

## Prototype

```
int PQntuples(const PGresult *res);
```

## Parameter

**Table 1**

| **Keyword** | **Parameter Description** |
| :---------- | :------------------------ |
| res         | Operation result handle.  |

## Return Value

Value of the int type

## Example

For details, see Example.
