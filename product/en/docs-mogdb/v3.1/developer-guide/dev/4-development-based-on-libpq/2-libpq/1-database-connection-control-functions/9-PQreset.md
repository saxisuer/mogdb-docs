---
title: PQreset
summary: PQreset
author: Guo Huan
date: 2021-05-17
---

# PQreset

## Function

PQreset is used to reset the communication port to the server.

## Prototype

```
void PQreset(PGconn *conn);
```

## Parameter

**Table 1** PQreset parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |

## Precautions

This function will close the connection to the server and attempt to establish a new connection to the same server by using all the parameters previously used. This function is applicable to fault recovery after a connection exception occurs.

## Example

For details, see [Example](../../libpq-example.md).
