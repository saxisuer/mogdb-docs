---
title: PQconninfoParse
summary: PQconninfoParse
author: Guo Huan
date: 2021-05-17
---

# PQconninfoParse

## Function

PQconninfoParse is used to return parsed connection options based on the connection.

## Prototype

```
PQconninfoOption* PQconninfoParse(const char* conninfo, char** errmsg);
```

## Parameters

**Table 1**

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conninfo    | Passed string. This parameter can be left empty. In this case, the default value is used. It can contain one or more values separated by spaces or contain a URL. |
| errmsg      | Error information.                                           |

## Return Value

PQconninfoOption pointers
