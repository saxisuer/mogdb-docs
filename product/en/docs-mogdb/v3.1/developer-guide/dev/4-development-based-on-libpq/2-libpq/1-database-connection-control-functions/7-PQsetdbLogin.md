---
title: PQsetdbLogin
summary: PQsetdbLogin
author: Guo Huan
date: 2021-05-17
---

# PQsetdbLogin

## Function

PQsetdbLogin is used to establish a new connection with the database server.

## Prototype

```
PGconn *PQsetdbLogin(const char *pghost,
                     const char *pgport,
                     const char *pgoptions,
                     const char *pgtty,
                     const char *dbName,
                     const char *login,
                     const char *pwd);
```

## Parameter

**Table 1** PQsetdbLogin parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| pghost      | Name of the host to be connected. For details, see the **host** field described in Connection Characters. |
| pgport      | Port number of the host server. For details, see the **port** field described in Connection Characters. |
| pgoptions   | Command-line options to be sent to the server during running. For details, see the **options** field described in Connection Characters. |
| pgtty       | This field can be ignored. (Previously, this field declares the output direction of server logs.) |
| dbName      | Name of the database to be connected. For details, see the **dbname** field described in Connection Characters. |
| login       | Username for connection. For details, see the **user** field described in Connection Characters. |
| pwd         | Password used for authentication during connection. For details, see the **password** field described in Connection Characters. |

## Return Value

**PGconn \*** points to the object pointer that contains a connection. The memory is applied for by the function internally.

## Precautions

- This function is the predecessor of PQconnectdb with a fixed set of parameters. When an undefined parameter is called, its default value is used. Write NULL or an empty string for any one of the fixed parameters that is to be defaulted.
- If the **dbName** value contains an = sign or a valid prefix in the connection URL, it is taken as a conninfo string and passed to PQconnectdb, and the remaining parameters are consistent with PQconnectdbParams parameters.

## Example

For details, see [Example](../../libpq-example.md).
