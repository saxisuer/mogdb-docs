---
title:  Appendices
summary:  Appendices
author:  Guo Huan
date: 2021-04-27
---

# Appendices

## **Table 1** PG_STAT_ACTIVITY Columns <a id="PG_STAT_ACTIVITY"> </a>

| Name             | Type                     | Description                                                  |
| ---------------- | ------------------------ | ------------------------------------------------------------ |
| datid            | oid                      | OID of the database that the user session connects to in the backend |
| datname          | name                     | Name of the database that the user session connects to in the backend |
| pid              | bigint                   | Thread ID of the backend                                     |
| sessionid        | bigint                   | Session ID                                                   |
| usesysid         | oid                      | OID of the user logged in to the backend                     |
| usename          | name                     | Name of the user logged in to the backend                    |
| application_name | text                     | Name of the application connected to the backend             |
| client_addr      | inet                     | IP address of the client connected to the backend. If this column is null, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as autovacuum. |
| client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of `client_addr`. This column will be non-null only for IP connections and only when **log_hostname** is enabled. |
| client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
| backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
| xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active) If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
| query_start      | timestamp with time zone | Time when the currently active query was started, or if **state** is not **active**, when the last query was started |
| state_change     | timestamp with time zone | Time when the **state** was last changed                     |
| waiting          | Boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true**. |
| enqueue          | text                     | Unsupported currently                                        |
| state            | text                     | Overall status of this backend. The value must be one of the following:<br />- **active**: The backend is executing a query.<br />- **idle**: The backend is waiting for a new client command.<br />- **idle in transaction**: The backend is in a transaction, but there is no statement being executed in the transaction.<br />- **idle in transaction** (aborted): The backend is in a transaction, but there are statements failed in the transaction.<br />- **fastpath function call**: The backend is executing a fast-path function.<br />- **disabled**: This state is reported if **track_activities** is disabled in this backend.<br />NOTE:<br />Common users can view their own session status only. The state information of other accounts is empty. For example, after user **judy** is connected to the database, the state information of user **joe** and the initial user omm in **pg_stat_activity** is empty.<br />`SELECT datname, usename, usesysid, state,pid FROM pg_stat_activity;`<br />`datname  | usename | usesysid | state  |       pid ----+---+----+---+------ postgres | omm     |       10 |        | 139968752121616 postgres | omm     |       10 |        | 139968903116560 db_tpcc | judy    |    16398 | active | 139968391403280 postgres | omm     |       10 |        | 139968643069712 postgres | omm     |       10 |        | 139968680818448 postgres | joe     |    16390 |        | 139968563377936 (6 rows)` |
| resource_pool    | name                     | Resource pool used by the user                               |
| query_id         | bigint                   | ID of a query                                                |
| query            | text                     | Text of this backend's most recent query. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
| connection_info  | text                     | A string in JSON format recording the driver type, driver version, driver deployment path, and process owner of the connected database. For details, see **connection_info**. |

## **Table 2** GS_WLM_SESSION_HISTORY Columns <a id="GS_WLM"> </a>

| Name                | Type                     | Description                                                  |
| ------------------------ | ---------------------- | ---------------------- |
| datid               | oid                      | OID of the database that the backend is connected to         |
| dbname              | text                     | Name of the database that the backend is connected to        |
| schemaname          | text                     | Schema name                                                  |
| nodename            | text                     | Name of the database node where the statement is executed    |
| username            | text                     | Username used for connecting to the backend                  |
| application_name    | text                     | Name of the application connected to the backend             |
| client_addr         | inet                     | IP address of the client connected to the backend. If this column is null, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as autovacuum. |
| client_hostname     | text                     | Host name of the connected client, as reported by a reverse DNS lookup of `client_addr`. This column will be non-null only for IP connections and only when **log_hostname** is enabled. |
| client_port         | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
| query_band          | text                     | Job type, which is specified by the GUC parameter **query_band**. The default value is a null string. |
| block_time          | bigint                   | Duration that the statement is blocked before being executed, including the statement parsing and optimization duration (unit: ms) |
| start_time          | timestamp with time zone | Time when the statement starts to be executed                |
| finish_time         | timestamp with time zone | Time when the statement execution ends                       |
| duration            | bigint                   | Execution time of the statement, in ms                       |
| estimate_total_time | bigint                   | Estimated execution time of the statement, in ms             |
| status              | text                     | Final statement execution status, which can be **finished** (normal) or **aborted** (abnormal). |
| abort_info          | text                     | Exception information displayed if the final statement execution status is **aborted** |
| resource_pool       | text                     | Resource pool used by the user                               |
| control_group       | text                     | The function is not supported currently.                     |
| estimate_memory     | integer                  | Estimated memory size of the statement.                      |
| min_peak_memory     | integer                  | Minimum memory peak of the statement across the database nodes, in MB |
| max_peak_memory     | integer                  | Maximum memory peak of the statement across the database nodes, in MB |
| average_peak_memory | integer                  | Average memory usage during statement execution (unit: MB)   |
| memory_skew_percent | integer                  | Memory usage skew of the statement among the database nodes  |
| spill_info          | text                     | Information about statement spill to the database nodes<br />- **None**: The statement has not been spilled to disks on the database nodes.<br />- **All**: The statement has been spilled to disks on the database nodes.<br />- **[a:b]**: The statement has been spilled to disks on *a* of *b* database nodes. |
| min_spill_size      | integer                  | Minimum spilled data among database nodes when a spill occurs, in MB (default value:**0**) |
| max_spill_size      | integer                  | Maximum spilled data among database nodes when a spill occurs, in MB (default value:**0**) |
| average_spill_size  | integer                  | Average spilled data among database nodes when a spill occurs, in MB (default value:**0**) |
| spill_skew_percent  | integer                  | database node spill skew when a spill occurs                 |
| min_dn_time         | bigint                   | Minimum execution time of the statement across the database nodes, in ms |
| max_dn_time         | bigint                   | Maximum execution time of the statement across the database nodes, in ms |
| average_dn_time     | bigint                   | Average execution time of the statement across the database nodes, in ms |
| dntime_skew_percent | integer                  | Execution time skew of the statement among the database nodes |
| min_cpu_time        | bigint                   | Minimum CPU time of the statement across the database nodes, in ms |
| max_cpu_time        | bigint                   | Maximum CPU time of the statement across the database nodes, in ms |
| total_cpu_time      | bigint                   | Total CPU time of the statement across the database nodes, in ms |
| cpu_skew_percent    | integer                  | CPU time skew of the statement among database nodes          |
| min_peak_iops       | integer                  | Minimum IOPS peak of the statement across the database nodes. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| max_peak_iops       | integer                  | Maximum IOPS peak of the statement across the database nodes. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| average_peak_iops   | integer                  | Average IOPS peak of the statement across the database nodes. It is counted by ones in a column-store table and by ten thousands in a row-store table. |
| iops_skew_percent   | integer                  | I/O skew across database nodes                               |
| warning             | text                     | Warning. The following warnings are displayed:<br />- Spill file size large than 256 MB<br />- Broadcast size large than 100 MB<br />- Early spill<br />- Spill times is greater than 3<br />- Spill on memory adaptive<br />- Hash table conflict |
| queryid             | bigint                   | Internal query ID used for statement execution               |
| query               | text                     | Statement executed                                           |
| query_plan          | text                     | Execution plan of the statement                              |
| node_group          | text                     | Unsupported currently                                        |
| cpu_top1_node_name  | text                     | Name of the current database node                            |
| cpu_top2_node_name  | text                     | Unsupported currently                                        |
| cpu_top3_node_name  | text                     | Unsupported currently                                        |
| cpu_top4_node_name  | text                     | Unsupported currently                                        |
| cpu_top5_node_name  | text                     | Unsupported currently                                        |
| mem_top1_node_name  | text                     | Current database name                                        |
| mem_top2_node_name  | text                     | Unsupported currently                                        |
| mem_top3_node_name  | text                     | Unsupported currently                                        |
| mem_top4_node_name  | text                     | Unsupported currently                                        |
| mem_top5_node_name  | text                     | Unsupported currently                                        |
| cpu_top1_value      | bigint                   | CPU usage of the current database node                       |
| cpu_top2_value      | bigint                   | Unsupported currently                                        |
| cpu_top3_value      | bigint                   | Unsupported currently                                        |
| cpu_top4_value      | bigint                   | Unsupported currently                                        |
| cpu_top5_value      | bigint                   | Unsupported currently                                        |
| mem_top1_value      | bigint                   | Memory usage of the current database node                    |
| mem_top2_value      | bigint                   | Unsupported currently                                        |
| mem_top3_value      | bigint                   | Unsupported currently                                        |
| mem_top4_value      | bigint                   | Unsupported currently                                        |
| mem_top5_value      | bigint                   | Unsupported currently                                        |
| top_mem_dn          | text                     | Memory usage information of the current database node        |
| top_cpu_dn          | text                     | CPU usage information of the current database node           |
