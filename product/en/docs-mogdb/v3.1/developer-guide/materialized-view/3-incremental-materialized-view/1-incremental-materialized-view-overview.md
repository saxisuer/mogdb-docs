---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-05-21
---

# Overview

Incremental materialized views can be incrementally refreshed. You need to manually execute statements to incrementally refresh materialized views in a period of time. The difference between the incremental and the full materialized views is that the incremental materialized view supports only a small number of scenarios. Currently, only base table scanning statements or UNION ALL can be used to create materialized views.
