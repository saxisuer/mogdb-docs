---
title: Support and Constraints
summary: Support and Constraints
author: Guo Huan
date: 2021-05-21
---

# Support and Constraints

## Supported Scenarios

- Supports statements for querying a single table.
- Supports UNION ALL for querying multiple single tables.
- Supports index creation in materialized views.
- Supports the Analyze operation in materialized views.

## Unsupported Scenarios

- Multi-table join plans and subquery plans are not supported in materialized views.
- Except for a few ALTER operations, most DDL operations cannot be performed on base tables in materialized views.
- Materialized views cannot be added, deleted, or modified. They support only query statements.
- The temporary table, hashbucket, unlog, or partitioned table cannot be used to create materialized views.
- Materialized views cannot be created in nested mode (that is, a materialized view cannot be created in another materialized view).
- The column-store tables are not supported. Only row-store tables are supported.
- Materialized views of the UNLOGGED type are not supported, and the WITH syntax is not supported.

## Constraints

If the materialized view definition is UNION ALL, each subquery needs to use a different base table.

A high-level lock is added to the base table during the creation, full refresh, and deletion of an incremental materialized view. If the materialized view is defined as a UNION ALL, pay attention to the service logic to avoid deadlocks.