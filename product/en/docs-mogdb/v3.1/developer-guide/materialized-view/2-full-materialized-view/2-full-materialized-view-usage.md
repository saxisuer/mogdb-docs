---
title: Usage
summary: Usage
author: Guo Huan
date: 2021-05-21
---

# Usage

## Syntax

- Create a full materialized view.

  ```
  CREATE MATERIALIZED VIEW [ view_name ] AS { query_block };
  ```

- Fullly refresh a materialized view.

  ```
  REFRESH MATERIALIZED VIEW [ view_name ];
  ```

- Delete a materialized view.

  ```
  DROP MATERIALIZED VIEW [ view_name ];
  ```

- Query a materialized view.

  ```
  SELECT * FROM [ view_name ];
  ```

## Examples

```
-- Prepare data.
MogDB=# CREATE TABLE t1(c1 int, c2 int);
MogDB=# INSERT INTO t1 VALUES(1, 1);
MogDB=# INSERT INTO t1 VALUES(2, 2);

-- Create a full materialized view.
MogDB=# CREATE MATERIALIZED VIEW mv AS select count(*) from t1;

-- Query the materialized view result.
MogDB=# SELECT * FROM mv;
 count
-------
     2
(1 row)

-- Insert data into the base table in the materialized view.
MogDB=# INSERT INTO t1 VALUES(3, 3);

-- Fully refresh a full materialized view.
MogDB=# REFRESH MATERIALIZED VIEW mv;

-- Query the materialized view result.
MogDB=# SELECT * FROM mv;
 count
-------
     3
(1 row)

-- Delete a materialized view.
MogDB=# DROP MATERIALIZED VIEW mv;
```
