---
title: Support and Constraints
summary: Support and Constraints
author: Liuxu
date: 2021-05-21
---

# Support and Constraints

## Supported Scenarios

- Supports the same query scope as the CREATE TABLE AS statement does.
- Supports index creation in full materialized views.
- Supports ANALYZE and EXPLAIN.

## Unsupported Scenarios

Materialized views cannot be added, deleted, or modified. They support only query statements.

## Constraints

A high-level lock is added to the base table during the process of refreshing or deleting a full materialized view. If a materialized view involves multiple tables, pay attention to the service logic to avoid deadlocks.