---
title: Gray Upgrade
summary: Gray Upgrade
author: Guo Huan
date: 2022-05-07
---

# Gray Upgrade

## Availability

This feature is available since MogDB 2.0.0.

## Introduction

Gray upgrade supports full-service operations. All nodes can be upgraded at a time.

## Benefits

Gray upgrade provides an online upgrade mode to ensure that all nodes are upgraded without interrupting services.

## Description

Gray upgrade is an online upgrade mode that upgrades all nodes. If only the binary files of the database need to be replaced during the gray upgrade, to minimize the impact on services, the two sets of binary files exist on the same node at the same time, and the soft connection switchover mode is used to switch the process version (one intermittent disconnection within 10 seconds).

## Enhancements

None.

## Constraints

For details about the gray upgrade constraints, see [Upgrade Guide](../../administrator-guide/upgrade-guide.md) in *Administrator Guide*.

## Dependencies

None.