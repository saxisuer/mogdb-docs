---
title: Global Temporary Table
summary: Global Temporary Table
author: Guo Huan
date: 2022-05-07
---

# Global Temporary Table

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

A temporary table does not guarantee persistency. Its life cycle is usually bound to a session or transaction, which can be used to store temporary data during processing and accelerate query.

## Benefits

This feature improves the expression capability and usability of temporary tables.

## Description

The metadata of the global temporary table is visible to all sessions. After the sessions end, the metadata still exists. The user data, indexes, and statistics of a session are isolated from those of another session. Each session can only view and modify the data submitted by itself.

Global temporary tables have two schemas: ON COMMIT PRESERVE ROWS and ON COMMIT PRESERVE ROWS. In session-based ON COMMIT PRESERVE ROWS schema, user data is automatically cleared when a session ends. In transaction-based ON COMMIT DELETE ROWS schema, user data is automatically cleared when the commit or rollback operation is performed. If the **ON COMMIT** option is not specified during table creation, the session level is used by default. Different from local temporary tables, you can specify a schema that does not start with **pg_temp_** when creating a global temporary table.

## Enhancements

The processing of the global temporary table is added based on the local temporary table.

## Constraints

- Parallel scanning is not supported.
- Temp tablespace is not supported.
- Partitions are not supported.
- GIST indexes are not supported.
- The user-defined statistics **pg_statistic_ext** is not supported.
- ON COMMIT DROP is not supported.
- Hash bucket cluster storage is not supported.
- Row store is not supported.

## Dependencies

None