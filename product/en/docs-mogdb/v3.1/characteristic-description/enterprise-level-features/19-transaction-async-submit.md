---
title: Transaction Asynchronous Submit
summary: Transaction Asynchronous Submit
author: Guo Huan
date: 2022-06-13
---

# Transaction Asynchronous Submit

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

In the thread pool mode of MogDB, a committer thread is added to each thread group, and the session is hung while waiting for the logs to be flushed during the transaction commit process, and the committer thread will continue to commit the transaction after the logs are flushed, while the original worker thread can go to handle other pending sessions after the session is hung.

## Benefits

In TP scenario, there is a high demand on the performance of transaction processing under high concurrency. However, the transaction commit process in MogDB needs to wait for the log to drop synchronously, and the worker threads are idle during this period and cannot be used to process other transactions. Although there is an implementation of "synchronous_commit=off" in MogDB, it does not guarantee the integrity of data after the database crashes in the middle.

This feature can realize the real sense of transaction asynchronous commit, which can make full use of CPU and improve the transaction processing ability in high concurrency scenario, especially in the small query adding, deleting and changing operations.

## Constraints

- This feature is valid only in threadpool mode, non-threadpool mode does not support transaction asynchronous commit. That is, set "enable_thread_pool = on" and "synchronous_commit" not to "off".

## Related Pages

[GS_ASYNC_SUBMIT_SESSIONS_STATUS](../../reference-guide/system-catalogs-and-system-views/system-views/GS_ASYNC_SUBMIT_SESSIONS_STATUS.md), [async_submit](../../reference-guide/guc-parameters/20-MogDB-transaction.md#async_submit), [LOCAL_THREADPOOL_STATUS](../../reference-guide/schema/DBE_PERF/session-thread/LOCAL_THREADPOOL_STATUS.md)