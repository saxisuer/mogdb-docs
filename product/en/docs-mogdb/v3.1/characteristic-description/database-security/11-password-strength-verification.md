---
title: Password Strength Verification
summary: Password Strength Verification
author: Guo Huan
date: 2022-05-07
---

# Password Strength Verification

## Availability

This feature is available as of MogDB 1.1.0.

## Introduction

Verifies the password strength when users access the database.

## Benefits

Users cannot set passwords with low strength to enhance customer data security.

## Description

You need to specify a password when initializing a database, creating a user, or modifying a user. The password must meet the strength requirements. Otherwise, the system prompts you to enter the password again. Password complexity requirements:

- Minimum number of uppercase letters (A-Z) (**password_min_uppercase**)
- Minimum number of lowercase letters (a-z) (**password_min_lowercase**)
- Minimum number of digits (0-9) (**password_min_digital**)
- Minimum number of special characters (**password_min_special**)
- Minimum password length (**password_min_length**)
- Maximum password length (**password_max_length**)
- A password must contain at least three types of the characters (uppercase letters, lowercase letters, digits, and special characters).
- A password is case insensitive and cannot be the username or the username spelled backwards.
- A new password cannot be the current password and the current password spelled backwards.
- It must be a strong password.
  - Weak passwords are weak passwords that are easy to crack. The definition of weak passwords may vary with users or user groups. Users can define their own weak passwords.

If parameter **password_policy** is set to **1**, the default password complexity rule is used to check passwords.

Passwords in the weak password dictionary are stored in the **gs_global_config** system catalog (the record whose name field is **weak_password** is the stored weak password). When a user is created or modified, the password set by the user is compared with the password stored in the weak password dictionary. If the password is matched, a message is displayed, indicating that the password is weak and the password fails to be set.

The weak password dictionary is empty by default. You can add or delete weak passwords using the following syntax:

```sql
CREATE WEAK PASSWORD DICTIONARY WITH VALUES ('password1'), ('password2');

DROP WEAK PASSWORD DICTIONARY;
```

In the preceding statement, **password1** and **password2** are weak passwords prepared by users. After the statement is executed successfully, the passwords are saved to the weak password system catalog.

When a user attempts to run the CREATE WEAK PASSWORD DICTIONARY statement to insert a weak password that already exists in the table, only one weak password is retained in the table.

The DROP WEAK PASSWORD DICTIONARY statement clears weak passwords in the entire system catalog.

The gs_global_config system catalog does not have a unique index. You are not advised to use the COPY FROM statement to copy the same data to the gs_global_config system catalog.

To audit weak password operations, set the third bit of the value of the **audit_system_object** parameter to **1**.

## Enhancements

In MogDB, the weak password dictionary function is implemented.

## Constraints

- Initial users, system administrators, and security administrators can view, add, and delete weak password dictionaries.
- Common users can view but cannot add or delete weak password dictionaries.

## Dependencies

None
