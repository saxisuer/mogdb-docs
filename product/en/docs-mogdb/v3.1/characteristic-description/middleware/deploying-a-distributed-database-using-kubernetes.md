---
title: Deploying a Distributed Database Using Kubernetes
summary: Deploying a Distributed Database Using Kubernetes
author: Guo Huan
date: 2022-05-10
---

# Deploying a Distributed Database Using Kubernetes

## Availability

This feature is available since MogDB 2.1.0.

## Introduction

Deploys a distributed database in one-click mode.

## Benefits

Quickly builds a distributed database, and verifies and uses the distributed capability.

## Description

Patroni is used to implement planned switchover and automatic failover in case of faults. HAProxy is used to implement read and write load balancing between the primary and standby MogDB nodes. ShardingSphere is used to implement distributed capabilities. All functions are packaged into images and one-click deployment scripts are provided.

## Enhancements

None.

## Constraints

Only CentOS and openEuler are supported.

## Dependencies

ShardingSphere, Patroni, HAProxy