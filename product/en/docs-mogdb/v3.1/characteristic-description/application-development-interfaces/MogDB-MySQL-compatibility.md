---
title: MogDB-MySQL Compatibility
summary: MogDB-MySQL Compatibility
author: Zhang Cuiping
date: 2022-06-21
---

# MogDB-MySQL Compatibility

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

This feature mainly enhances the compatibility of MogDB with MySQL in the following three aspects. At the same time, the `insert` syntax is supported, and `insert into` can be abbreviated as `insert`.

User lock allows users to add custom locks through SQL, which allows multiple programs to complete the lock-related interaction process, making the client access from any location to get a consistent lock view.

When data is inserted into a table to be created, the current time is inserted by default. During data update, if the update time is not specified, the time when the data is updated is displayed by default. 

Session-level SQL mode can be set, allowing change in running, global change, and intra-session change.

## Benefits

By setting user locks, data, data structures or certain strings are protected from interfering with each other between sessions, ensuring consistency and security of information. It solves the problem of recording the timestamp of users' operation when their business data is written and modified. By setting SQL mode, it can solve the compatibility between the legacy problems of earlier versions and later versions.

## Related Pages

[CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md), [ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md), [INSERT](../../reference-guide/sql-syntax/INSERT.md), [Advisory Lock Functions](../../reference-guide/functions-and-operators/24-system-management-functions/7-advisory-lock-functions.md), [dolphin](../../reference-guide/oracle-plugins/dolphin.md)