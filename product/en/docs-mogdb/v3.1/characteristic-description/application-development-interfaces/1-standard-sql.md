---
title: Standard SQL
summary: Standard SQL
author: Guo Huan
date: 2022-05-07
---

# Standard SQL

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

SQL is a standard computer language used to control the access to databases and manage data in databases. SQL standards are classified into core features and optional features. Most databases do not fully support SQL standards.

MogDB supports most of the core features of SQL:2011 and some optional features, providing a unified SQL interface for users.

## Benefits

All database vendors can use a unified SQL interface, reducing the costs of learning languages and migrating applications.

## Description

For details, see “SQL Syntax” in the *Reference Guide*.

## Enhancements

None

## Constraints

None

## Dependencies

None