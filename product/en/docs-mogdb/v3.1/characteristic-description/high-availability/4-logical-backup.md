---
title: Logical Backup
summary: Logical Backup
author: Guo Huan
date: 2022-05-07
---

# Logical Backup

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Data in user tables in the database is backed up to a specified storage medium in a general format.

## Benefits

Through logical backup, you can achieve the following purposes:

- Back up user data to a reliable storage medium to secure data.
- Support cross-version recovery and heterogeneous recovery using a general data format.
- Archive cold data.

## Description

MogDB provides the logical backup capability to back up data in user tables to local disk files in text or CSV format and restore the data in homogeneous or heterogeneous databases.

## Enhancements

None

## Constraints

For details about the restrictions on logical backup, see [gs_dump](../../reference-guide/tool-reference/server-tools/5-gs_dump.md) in the *Tool Reference*.

## Dependencies

None