---
title: Delaying Entering the Maximum Availability Mode
summary: Delaying Entering the Maximum Availability Mode
author: Guo Huan
date: 2022-05-10
---

# Delaying Entering the Maximum Availability Mode

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

The primary node can be delayed to enter the maximum availability mode.

## Benefits

When the primary node detects that the standby node exits due to network instability or other reasons and the maximum availability mode is enabled on the primary node, the primary node remains in the maximum protection mode within a specified time window. After the time window expires, the primary node enters the maximum availability mode.

This prevents the primary node from frequently switching between the maximum protection mode and maximum availability mode due to factors such as network jitter and intermittent process disconnection.

## Description

If **most_available_sync** is set to **on**, when synchronous standby nodes are faulty in primary/standby scenarios and the number of configured synchronous standby nodes is insufficient (for details, see the meaning of **synchonous_standby_name**), setting **keep_sync_window** will retain the maximum protection mode within the time window specified by **keep_sync_window**. That is, committing transactions on the primary node is blocked, delaying the primary node to enter the maximum availability mode.

If synchronous standby nodes recover from faults and the number of synchronous standby nodes meets the configuration requirements, transactions are not blocked.

## Enhancements

None.

## Constraints

- This feature takes effect only when the maximum availability mode is enabled.
- Enabling this feature may affect the RPO. If the primary node is faulty within the configured timeout window, its transactions are committed locally but not synchronized to the faulty synchronous standby nodes.
- This feature does not apply to cascaded standby nodes.

## Dependencies

This feature depends on the maximum availability mode.