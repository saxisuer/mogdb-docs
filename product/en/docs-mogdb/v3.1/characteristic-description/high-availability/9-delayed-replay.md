---
title: Delayed Replay
summary: Delayed Replay
author: Guo Huan
date: 2022-05-07
---

# Delayed Replay

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The time for a standby node to replay can be delayed.

## Benefits

By default, the standby server restores the Xlog records from the primary server as soon as possible. This function allows you to delay the time for a standby node to replay Xlog records. In this case, you can query a copy that records data before a period of time, which helps correct errors such as misoperations.

## Description

The GUC parameter **recovery_min_apply_delay** can be used to set the delay time so that a standby server can replay Xlog records from the primary server after a delay time.

Value range: an integer ranging from 0 to INT_MAX. The unit is ms.

Default value: **0** (no delay)

## Enhancements

None

## Constraints

- The **recovery_min_apply_delay** parameter is invalid on the primary node. It must be set on the standby node to be delayed.
- The delay time is calculated based on the timestamp of transaction commit on the primary server and the current time on the standby server. Therefore, ensure that the clocks of the primary and standby servers are the same.
- Operations without transactions are not delayed.
- After the primary/standby switchover, if the original primary server needs to be delayed, you need to manually set this parameter.
- When **synchronous_commit** is set to **remote_apply**, synchronous replication is affected by the delay. Each commit message is returned only after the replay on the standby server is complete.
- Using this feature also delays **hot_standby_feedback**, which may cause the primary server to bloat, so be careful when using both.
- If a DDL operation (such as DROP or TRUNCATE) that holds an AccessExclusive lock is performed on the primary server, the query operation on the operation object on the standby server will be returned only after the lock is released during the delayed replay of the record on the standby server.

## Dependencies

None