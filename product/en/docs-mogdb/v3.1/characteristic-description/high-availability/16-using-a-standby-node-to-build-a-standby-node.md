---
title: Using a Standby Node to Build a Standby Node
summary: Using a Standby Node to Build a Standby Node
author: Guo Huan
date: 2022-05-10
---

# Using a Standby Node to Build a Standby Node

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

A standby node can be built by another standby node to accelerate standby node recovery from faults. The I/O and bandwidth pressure of the primary node can be reduced.

## Benefits

When the service load is heavy, building a standby node by using the primary node increases the resource burden on the primary node. As a result, the performance of the primary node deteriorates and the build becomes slow. Building a standby node by using a standby node does not affect services on the primary node.

## Description

You can run the **gs_ctl** command to specify a standby node to build another standby node to be repaired. For details, see “Tools Used in the Internal System > [gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md)” in *Tool Reference*.

## Enhancements

None.

## Constraints

A standby node can only be used to build another standby node. You can only use a specified IP address and port number to build the standby node. Before building the standby node, ensure that the logs of the standby node to be repaired are later than those of the standby node that sends data.

## Dependencies

None.