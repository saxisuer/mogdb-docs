---
title: Online Node Replacement
summary: Online Node Replacement
author: Guo Huan
date: 2022-05-07
---

# Online Node Replacement

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

If a node in a database is unavailable or the instance status is abnormal due to a hardware fault and the database is not locked, you can replace the node or rectify the instance fault to restore the database. During the restoration, DML operations are supported. DDL operations are supported in limited scenarios only.

## Benefits

Currently, the scale of enterprise data is increasing, the number of nodes increases sharply, and the probability of hardware damage increases accordingly. The traditional offline node replacement mode cannot meet customer requirements for uninterrupted services. During routine O&M, frequent service interruption will bring great loss to customers. However, the current database products in the industry cannot meet the requirements of physical node replacement in large-scale data scenarios without service interruption. Services need to be interrupted, or only some operations are allowed when services are not interrupted.

## Description

If a node in a database is unavailable or the instance status is abnormal due to a hardware fault, you can replace the node or rectify the instance fault to restore the database. During the restoration, DML operations are supported. DDL operations are supported in limited scenarios only.

## Enhancements

None

## Constraints

Currently, online DDL operations are supported during node replacement.

- During node replacement, DML operations are supported and DDL operations are supported in certain scenarios.

## Dependencies

None