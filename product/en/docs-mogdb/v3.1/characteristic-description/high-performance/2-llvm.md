---
title: LLVM
summary: LLVM
author: Guo Huan
date: 2022-05-07
---

# LLVM

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

MogDB provides the Low Level Virtual Machine (LLVM) technology to query dynamic compilation execution.

## Benefits

The requery performance is greatly improved by dynamically building and executing queries.

## Description

Based on the query execution plan tree, with the library functions provided by the LLVM, MogDB moves the process of determining the actual execution path from the executor phase to the execution initialization phase. In this way, problems such as function calling, logic condition branch determination, and a large amount of data reading that are related to the original query execution are avoided, to improve the query performance.

## Enhancements

None

## Constraints

None

## Dependencies

It depends on the LLVM open-source component. Currently, the open-source version 10.0.0 is used.