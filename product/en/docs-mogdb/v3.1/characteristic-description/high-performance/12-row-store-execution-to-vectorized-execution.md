---
title: Row-Store Execution to Vectorized Execution
summary: Row-Store Execution to Vectorized Execution
author: Guo Huan
date: 2022-05-10
---

# Row-Store Execution to Vectorized Execution

## Availability

This feature is available since MogDB 3.0.0.

## Introduction

Row-store table queries are converted into vectorized execution plans for execution, improving the execution performance of complex queries.

## Benefits

When processing row-store table queries, the executor has poor performance when executing complex queries that include many expressions or join operations, while the vectorized executor has excellent performance when executing complex queries. Therefore, query performance of complex queries can be effectively improved by converting row-store table execution into vectorized execution.

## Description

This feature adds a RowToVec operation to scan operators. After the data in the row-store table is converted into the vectorized format in the memory, the upper-layer operators can be converted into vectorized operators. In this way, the vectorized executor can be used for calculation. Scan operators that support this conversion include SeqScan, IndexOnlyscan, IndexScan, BitmapScan, FunctionScan, ValueScan and TidScan.

## Enhancements

None.

## Constraints

- Vectorization is not supported in the following scenarios:
  - The targetList contains a function that returns set.
  - The targetList or qual contains expressions that do not support vectorization, such as array expressions, multi-subquery expressions, field expressions, and system catalog columns.
  - The following types do not support vectorization: POINTOID, LSEGOID, BOXOID, LINEOID, CIRCLEOID, POLYGONOID, PATHOID, and user-defined types.
- MOTs do not support vectorization.

## Dependencies

Vectorized executor