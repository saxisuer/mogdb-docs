---
title: CBO Optimizer
summary: CBO Optimizer
author: Guo Huan
date: 2022-05-07
---

# CBO Optimizer

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The MogDB optimizer is cost-based optimization (CBO).

## Benefits

The MogDB CBO optimizer can select the most efficient execution plan among multiple plans based on the cost to meet customer service requirements to the maximum extent.

## Description

By using CBO, the database calculates the number of tuples and the execution cost for each step under each execution plan based on the number of table tuples, column width, null record ratio, and characteristic values, such as distinct, MCV, and HB values, and certain cost calculation methods. The database then selects the execution plan that takes the lowest cost for the overall execution or for the return of the first tuple.

## Enhancements

None

## Constraints

None

## Dependencies

None