---
title: Slow SQL Statement Discovery
summary: Slow SQL Statement Discovery
author: Guo Huan
date: 2022-05-10
---

# Slow SQL Statement Discovery

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

This feature servers as an SQL statement execution time forecast tool. It forecasts the execution time of SQL statements based on the statement logic similarity and historical execution records without obtaining the SQL statement execution plan using a template.

## Benefits

- This feature does not require users to provide SQL execution plans. Therefore, the database performance is not affected.
- Different from other algorithms in the industry that are limited to OLAP or OLTP, this feature is more widely used.

## Description

The SQLdiag focuses on the historical SQL statements of the database, summarizes the execution performance of the historical SQL statements, and then uses the historical SQL statements to infer unknown services. The execution duration of SQL statements in the database does not differ greatly in a short period of time. SQLdiag can detect the statement result set similar to the executed SQL statements from historical data and predict the execution duration of SQL statements based on the SQL vectorization technology and template-based method.

## Enhancements

None.

## Constraints

- The historical logs and the format of the workload to be predicted meet the requirements. You can use the GUC parameter of the database to enable the collection or use the monitoring tool to collect logs.
- To ensure the prediction accuracy, the historical statement logs provided by users should be as comprehensive and representative as possible.
- The Python environment has been configured as required.

## Dependencies

None.