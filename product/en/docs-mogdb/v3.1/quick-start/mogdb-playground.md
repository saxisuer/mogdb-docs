---
title: MogDB Playground
summary: MogDB Playground
author: Guo Huan
date: 2022-03-18
---

# MogDB Playground

MogDB Playground is an online learning and hands-on training platform for MogDB database provided by [modb](https://www.modb.pro/), which also provides Oracle/MySQL compatible packages and Mogila dataset. You can connect to the database with one click through the command line terminal of your browser, and experience the functions of MogDB conveniently and quickly, without the need to provide your own environment for installation.

<br/>

## Enter the Hands-on Training Environment

Visit [MogDB Playground](https://www.modb.pro/terminal). After entering, the relevant operation documents will be displayed on the left side, and the operation platform will be displayed on the right side. At this time, it is not yet connected, and you need to click **Click to enter the hands-on training environment**. Then register or log in to your modb  account in the pop-up window to use MogDB Playground for free.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-1.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-6.png)

## Start Using

After a successful connection, the official operation starts, and it is time to perform the relevant operations.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-2.png)

## Reference Document

When operating, you can operate according to the operation document on the left side, while the operation document on the left side can be collapsed.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-3.png)

## Shutdown Environment

If you want to quit the database, you can click the close button at the bottom right corner to close it, but the data created before will be cleared after closing, and it will be a new environment when you connect again, so please operate carefully.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-4.png)

## FAQ

**What if I exit the page in the middle?**

Users who close the current page directly, the fact that the database resources are still running, as long as you re-enter within 30 minutes, the previously created data will not be deleted (refresh the current page is the same), and exit the current page in the middle, there will be a relevant prompt to determine whether to leave.

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-5.png)
