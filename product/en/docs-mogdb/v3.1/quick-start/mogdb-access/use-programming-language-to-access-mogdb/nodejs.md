---
title: Use NodeJS to Access MogDB
summary: Use NodeJS to Access MogDB
author: Guo Huan
date: 2022-03-31
---

# Use NodeJS to Access MogDB

openGauss provides a driver for NodeJS based on [node-postgres](https://github.com/brianc/node-postgres), which also works with NodeJS to connect to MogDB.

You can view the source code and operating instructions for this driver by clicking on the following links.

[Source Code](https://gitee.com/opengauss/openGauss-connector-nodejs), [Operating Instructions](https://gitee.com/opengauss/openGauss-connector-nodejs/blob/master/README.md)
