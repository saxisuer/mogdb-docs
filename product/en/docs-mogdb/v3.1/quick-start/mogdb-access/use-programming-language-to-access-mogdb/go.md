---
title: Use Go to Access MogDB
summary: Use Go to Access MogDB
author: Guo Huan
date: 2022-03-31
---

# Use Go to Access MogDB

openGauss provides Go openGauss driver for Go‘s database/sql package based on [lib/pq](https://github.com/lib/pq), which is also suitable for Go connection to MogDB.

You can view the source code and operating instructions for this driver by clicking on the following links.

[Source Code](https://gitee.com/opengauss/openGauss-connector-go-pq), [Operating Instructions](https://gitee.com/opengauss/openGauss-connector-go-pq/blob/master/README.cn.md)
