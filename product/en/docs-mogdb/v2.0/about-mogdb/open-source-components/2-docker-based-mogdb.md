---
title: Container-based MogDB
summary: Container-based MogDB
author: Liuxu
date: 2021-06-09
---

# Container-based MogDB

<br/>

## Features

- As the version of MogDB changes, release the image of the new version as soon as possible.
- The container version of the database image has built-in configuration of initialization parameters for best practices.
- The container version database supports both x86 and ARM CPU architectures.

**Currently, x86-64 and ARM64 architectures are supported. Please get the corresponding container image according to the machine architecture of your host.**

Starting from version 2.0 (including version 2.0)

- MogDB of the x86-64 architecture is run on the [Ubuntu 18.04 operating system](https://ubuntu.com/).
- MogDB of the ARM64 architecture is run on the [Debian 10 operating system](https://www.debian.org/).

Before version 1.1.0 (including version 1.1.0)

- MogDB of the x86-64 architecture is run on the [CentOS7.6 operating system](https://www.centos.org/).
- MogDB of the ARM64 architecture is run on the [openEuler 20.03 LTS operating system](https://openeuler.org/en/).

<br/>

## How to Use an Image?

For details, visit the following website:

[Installation Guide - Container-based Installation](../../installation-guide/docker-installation/docker-installation.md)
