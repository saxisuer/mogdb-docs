---
title: Roadmap
summary: Roadmap
author: Guo Huan
date: 2021-06-15
---

# Roadmap

This document introduces the roadmap for MogDB 2.0.1.

<table>
    <tr>
        <th>Components</th>
        <th>Unmerged content</th>
    </tr>
    <tr>
        <td rowspan="4">MogHA</td>
        <td>Support for automatic restart of primary instance</td>
    </tr>
    <tr>
        <td>Support for database CPU usage limit setting</td>
    </tr>
    <tr>
        <td>Support for customization of HA switching</td>
    </tr>
    <tr>
        <td>Support for WEB interface, API and so on, facilitating small-scale deployment (EA phase)</td>
    </tr>
    <tr>
        <td rowspan="10">MogDB Server</td>
        <td>Support setting the primary node to global read-only, which can prevent brain-split in the case of network isolation</td>
    </tr>
    <tr>
        <td>Support for pg trgm plugin (full-text indexing plugin)</td>
    </tr>
    <tr>
        <td>Support for custom operators and custom operator classes</td>
    </tr>
    <tr>
        <td>Reduce resource consumption for database metrics collection</td>
    </tr>
    <tr>
        <td>Optimize partition creation syntax</td>
    </tr>
     <tr>
        <td>Support sorting of CJK (East Asian character set), with better performance for sorting large data volumes</td>
    </tr>
     <tr>
        <td>Support for xid-based flashback queries</td>
    </tr>
     <tr>
        <td>Support for sub partitions</td>
    </tr>
     <tr>
        <td>Support for citus plug-in, enable scale-out to enhance distributed capacity</td>
    </tr>
     <tr>
        <td>Support for online reindex without locking tables</td>
    </tr>
    <tr>
        <td rowspan="2">MogDB Client</td>
        <td>Go client: support for the SHA256 encryption algorithm</td>
    </tr>
    <tr>
        <td>Python client: support for the SHA256 encryption algorithm and support for automatic identification of the primary node</td>
    </tr>
    <tr>
        <td rowspan="4">MogDB plugin</td>
        <td>Compatibility support for wal2json plug-in used for the export and heterogeneous replication of the current logical log </td>
    </tr>
    <tr>
        <td>Support for pg_dirtyread plug-in used for data recovery and query under special circumstances</td>
    </tr>
    <tr>
        <td>Support for walminer plug-in used for online WAL log parsing (no reliance on logical logs)</td>
    </tr>
    <tr>
        <td>Support for db_link plug-in used for connecting to PostgreSQL databases from MogDB</td>
    </tr>
</table>
