---
title: MogDB Service Exception Testing
summary: MogDB Service Exception Testing
author: Guo Huan
date: 2021-04-25
---

# MogDB Service Exception Testing

## Test Scope

1. Database process exception for the standby node

2. Monitor script exception for the standby node

3. File system exception for the primary node (affecting HA process)

## Test Environment

| Category    | Server Configuration  | Client Configuration | Quantity |
| ----------- | :-------------------: | :------------------: | :------: |
| CPU         |      Kunpeng 920      |     Kunpeng 920      |   128    |
| Memory      |     DDR4,2933MT/s     |    DDR4,2933MT/s     |  2048G   |
| Hard Disk   |       Nvme 3.5T       |       Nvme 3T        |    4     |
| File system |          Xfs          |         Xfs          |    4     |
| OS          | openEuler 20.03 (LTS) |      Kylin V10       |          |
| Database    |      MogDB1.1.0       |                      |          |
| Tool        |        pgbench        |                      |          |

## Database process exception for the standby node

Test procedure:

Kill the database process.

Test result:

1. Observe the cluster status.

2. The script of the standby database node shows the heartbeat exception.

3. No switchover occurs.

4. Run the command on the standby database node.

    ```
    gs_ctl start -D /gaussdata/openGauss/db1 -M standby
    ```

5. The cluster is restored to normal and no switchover has occurred since then.

6. When the standby database process is killed, TPS of the primary database node rises from 9000 to 13000.

7. No primary/standby switchover occurs.

## Monitor script exception for the standby node

Test procedure:

Kill the monitor script of the primary database node.

Test result:

1. Observe the cluster status.
2. No switchover occurs.
3. The monitor script of the primary database node reports script exception of the standby database node.
4. Restore the monitor script of the standby database node.
5. TPS maintains at 9000.

## File system exception for the primary node (affecting HA process)

Test procedure:

Modify the permission of the primary database script called by HA, such as gs_ctl.

Test result:

1. After modifying the **rwx** permission of gs_ctl, the monitoring script of the primary database node reports heartbeat exception.

2. The instance status cannot be detected.

3. Query the current cluster status.

4. After waiting about two minutes, no switchover occurs.
