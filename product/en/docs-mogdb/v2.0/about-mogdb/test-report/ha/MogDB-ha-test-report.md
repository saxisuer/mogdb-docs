---
title: HA Test
summary: HA Test
author: Zhang Cuiping
date: 2021-06-07
---

# HA Test

## Test Objective

Database HA (high availability) test aims at providing users with high-quality services, avoiding service interruption caused by such faults as server crash. Its HA lies in not only whether a database can provide services consistently but whether a database can ensure data consistency.

## Test Environment

| Category    |  Server Configuration   | Client Configuration | Quantity |
| ----------- | :---------------------: | :------------------: | :------: |
| CPU         |       Kunpeng 920       |     Kunpeng 920      |   128    |
| Memory      |     DDR4, 2933MT/s      |    DDR4, 2933MT/s    | 2048 GB  |
| Hard disk   |       NVME 3.5 TB       |      NVME 3 TB       |    4     |
| File system |           Xfs           |         Xfs          |    4     |
| OS          |  openEuler 20.03 (LTS)  |      Kylin V10       |          |
| Database    | MogDB  software package |                      |          |
| Test tool   |         pgbench         |                      |          |

## HA and Scalability Test

| No.  | Test Item                             | Description                                                  |
| ---- | ------------------------------------- | ------------------------------------------------------------ |
| 1    | Cluster with read-write separation    | Supports separate routing of read and write requests and separate distribution of read and write tasks. |
| 2    | Capacity expansion/capacity reduction | In load scenarios, adding or reducing a physical device does not make a front-end application service interrupted. |
| 3    | Shared storage cluster                | Supports two nodes sharing a storage cluster, automatic failover, and load balancing of concurrent transactions. |
| 4    | Service exception test                | Guarantees the availability of an application in scenarios where the process of the standby database, the monitoring script of the standby database, and the file system of the primary database are abnormal. |
| 5    | Routine maintenance test              | Supports the switchover feature provided by gs_ctl (database service control tool) and the failover feature provided by MogHA (HA component). |
| 6    | Database server exception test        | Maximizes the availability and stability of an application when the primary node, standby node, or arbitration node crashes, or both the primary and standby nodes, or both the primary and arbitration nodes crash. |
| 7    | Network exception test                | Maximizes the availability of an application when the service or heartbeat NIC of the primary node is abnormal, both the service and heartbeat NICs of the primary node are abnormal, the service NIC of the standby node is abnormal, or the VIP of the host is abnormal. |
