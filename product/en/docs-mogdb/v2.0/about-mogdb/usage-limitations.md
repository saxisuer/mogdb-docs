---
title: Usage Limitations
summary: Usage Limitations
author: Guo Huan
date: 2021-06-01
---

# Usage Limitations

This document describes the common usage limitations of MogDB.

| Item                                      | Upper limit                                          |
| ----------------------------------------- | ---------------------------------------------------- |
| Database capacity                         | Depend on operating systems and hardware             |
| Size of a single table                    | 32 TB                                                |
| Size of a single row                      | 1 GB                                                 |
| Size of a single field in a row           | 1 GB                                                 |
| Number of rows in a single table          | 281474976710656 (2<sup>48</sup>)                     |
| Number of columns in a single table       | 250~1600 (Varies depending on different field types) |
| Number of indexes in a single table       | Unlimited                                            |
| Number of columns in a compound index     | 32                                                   |
| Number of constraints in a single table   | Unlimited                                            |
| Number of concurrent connections          | 10000                                                |
| Number of partitions in a partition table | 32768 (Range partition)/64 (HASH/List partition)     |
| Size of a single partition                | 32 TB                                                |
| Number of rows in a single partition      | 2<sup>55</sup>                                       |
| Maximum length of SQL text                | 1048576 bytes (1MB)                                  |
