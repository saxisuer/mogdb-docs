---
title: Use GUI to Access MogDB
summary: Use GUI to Access MogDB
author: Guo Huan
date: 2021-08-31
---

# Use GUI to Access MogDB

This document introduces how to access MogDB through the commonly used GUI.

<br/>

## Data Studio

### 1. Introduction

**Data Studio** is an Integrated Development Environment (IDE) that helps database developers build the application conveniently. It supports essential features of the database.
This tool allows working with database objects with minimal programming knowledge. Data Studio provides you with various features, such as

- creating and managing database objects
- executing SQL statements or SQL scripts
- editing and executing PL/SQL statements
- viewing the execution plan and cost through the GUI
- exporting table data

The database objects that are created and managed include

- database
- schema
- function
- procedure
- table
- sequence
- index
- view
- tablespace
- synonym

It also provides SQL assistance for various queries/procedures/functions executed in the SQL Terminal and PL/SQL Viewer.

<br/>

### 2. System Requirements

This section provides the minimum system requirements for using Data Studio.

**Hardware Requirements**

Table 1 Hardware Requirements for Data Studio

| **Hardware Requirement** | **Configuration**                                                     |
| ------------ | ------------------------------------------------------------ |
| CPU          | x86 64-bit                                                     |
| Available RAM     | A minimum of 1 GB memory                                                  |
| Available hard disk     | A minimum of 1 GB space reserved for Data Studio installation directory and 100 MB space reserved for user's HOME directory. |
|Network requirements|Gigabit Ethernet|

**Operating System Requirements**

The supported versions of Microsoft Windows running on a universal x86 server include Windows 7 (64-bit), Windows 10 (64-bit), Windows 2012 (64-bit), and Windows 2016 (64-bit).

**Software Requirements**

Java 1.8.0_181 or later

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:**
>
> * Please refer to [https://java.com/en/download/help/path.html](https://java.com/en/download/help/path.html) to set the Java Home path.
> * To ensure the optimal experience, the recommended minimum screen resolution is 1080 x 768. Below this resolution, the interface will be abnormal.

<br/>

### 3. Download and Installation

The download address of Data Studio is as follows: <https://opengauss.org/en/download.html>

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-14.png)

After downloading, unzip the installation package and double-click **Data Studio.exe** to run the software.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-2.png)

<br/>

### 4. Connection String Configuration

When the software is opened for the first time, the **New Database Connection** window will pop up, as shown in the figure below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-15.png)

You can also choose **File &gt; New Connection** to open this window.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-16.png)

Fill in the information on the **General** tab, and click **OK** to establish a database connection.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-17.png)

<br/>

### 5. Interface Display

After a successful connection, the software interface is shown below, with examples of syntax and operation lists on the right.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-18.png)

<br/>

### 6. Follow-Up Procedure

If you want to learn more about the usage instructions of Data Studio, please choose **Help &gt; User Manual**, or visit the following page to browse the official documents online: [Data Studio User Manual](https://opengauss.obs.cn-south-1.myhuaweicloud.com/2.0.0/Data%20Studio%20User%20Manual.pdf).

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-19.png)

<br/>

## DBeaver

### 1. Introduction

**Dbeaver** is a cross-platform database developer tool, including SQL programming, database management and analysis. It supports any database system that adapts to the JDBC driver. At the same time, the tool also supports some non-JDBC data sources, such as MongoDB, Cassandra, Redis, DynamoDB, etc.

* This tool provides many powerful features, such as metadata editor, SQL editor, rich text data editor, ERD, data import/export/migration, SQL execution plan, etc.
* The tool is developed based on the eclipse platform.
* Adapted databases include MySQL/MariaDB, PostgreSQL, Greenplum, Oracle, DB2 LUW, Exasol, SQL Server, Sybase/SAP ASE, SQLite, Firebird, H2, HSQLDB, Derby, Teradata, Vertica, Netezza, Informix, etc.

<br/>

### 2. Download and Installation

Dbeaver is an open source software with the download address as follows: [https://dbeaver.io/download/](https://dbeaver.io/download/)

Select and download the appropriate installation package according to your operating system, and double-click the package to install it after the download is complete.

<br/>

### 3. Connection String Configuration

When the software is opened for the first time, the **Connect to a database** window will pop up, as shown in the figure below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-20.png)

You can also choose **Database &gt; New Database Connection** to open this window.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-21.png)

Choose **Popular &gt; PostgreSQL**, click **Next**, and download the PostgreSQL driver file on the pop-up page.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-22.png)

After the download is complete, the **Connect a database** window will pop up, fill in the database information in this window, and then click **Finish** to establish a database connection.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-23.png)

<br/>

### 4. Interface Display

After a successful connection, the software interface is shown below.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-24.png)

<br/>

### 5. Follow-Up Procedure

If you want to learn more about the usage instructions of DBeaver, please choose **Help &gt; Help Contents**, or visit the following page to browse the official documents online: [https://dbeaver.com/docs/wiki/](https://dbeaver.com/docs/wiki/).

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-25.png)
