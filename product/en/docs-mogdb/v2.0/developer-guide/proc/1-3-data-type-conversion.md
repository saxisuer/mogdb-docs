---
title: Data Type Conversion
summary: Data Type Conversion
author: Guo Huan
date: 2021-03-04
---

# Data Type Conversion

Certain data types in the database support implicit data type conversions, such as assignments and parameters called by functions. For other data types, you can use the type conversion functions provided by MogDB, such as the **CAST** function, to forcibly convert them.

MogDB lists common implicit data type conversions in [Table 1](#Implicit data type conversions).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The valid value range of **DATE** supported by MogDB is from 4713 B.C. to 294276 A.D.

**Table 1** Implicit data type conversions<a id="Implicit data type conversions"> </a>

| Raw Data Type | Target Data Type | **Remarks**                                  |
| :------------ | :--------------- | :------------------------------------------- |
| CHAR          | VARCHAR2         | -                                            |
| CHAR          | NUMBER           | Raw data must consist of digits.             |
| CHAR          | DATE             | Raw data cannot exceed the valid date range. |
| CHAR          | RAW              | -                                            |
| CHAR          | CLOB             | -                                            |
| VARCHAR2      | CHAR             | -                                            |
| VARCHAR2      | NUMBER           | Raw data must consist of digits.             |
| VARCHAR2      | DATE             | Raw data cannot exceed the valid date range. |
| VARCHAR2      | CLOB             | -                                            |
| NUMBER        | CHAR             | -                                            |
| NUMBER        | VARCHAR2         | -                                            |
| DATE          | CHAR             | -                                            |
| DATE          | VARCHAR2         | -                                            |
| RAW           | CHAR             | -                                            |
| RAW           | VARCHAR2         | -                                            |
| CLOB          | CHAR             | -                                            |
| CLOB          | VARCHAR2         | -                                            |
| CLOB          | NUMBER           | Raw data must consist of digits.             |
| INT4          | CHAR             | -                                            |
| INT4          | BOOLEAN          | -                                            |
| BOOLEAN       | INT4             | -                                            |
