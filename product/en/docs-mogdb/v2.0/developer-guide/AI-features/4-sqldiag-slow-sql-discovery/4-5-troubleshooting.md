---
title: Troubleshooting
summary: Troubleshooting
author: Guo Huan
date: 2021-05-19
---

# Troubleshooting

- Failure in the training scenario: Check whether the file path of historical logs is correct and whether the file format meets the requirements.
- Failure in the prediction scenario: Check whether the model path is correct. Ensure that the format of the load file to be predicted is correct.
