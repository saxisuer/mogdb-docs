---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2021-05-19
---

# Command Reference

**Table 1** Command-line options

| Parameter | Description                         | Value Range    |
| :-------- | :---------------------------------- | :------------- |
| -m, -mode | Module selection                    | train, predict |
| -f, -file | File path                           | -              |
| -h, -help | Description of operation parameters | -              |
