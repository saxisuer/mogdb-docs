---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2021-05-19
---

# Command Reference

**Table 1** Command-line parameters

| Parameter         | Description                                                  | Value Range                                                  |
| :---------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| mode              | Specified running mode                                       | start,stop,forecast,show_metrics,deploy                      |
| -user             | Remote server user                                           | -                                                            |
| -host             | IP address of the remote server                              | -                                                            |
| -project-path     | Path of the **anomaly_detection** project on the remote server | -                                                            |
| -role             | Start role                                                   | agent, server, monitor                                       |
| -metric-name      | Metric name                                                  | -                                                            |
| -forecast-periods | Forecast period                                              | Integer + Time unit, for example, '100S', '3H'.<br/>The time unit can be **S** (second), **M** (minute), **H** (hour), **D** (day), and **W** (week). |
| -forecast-method  | Forecasting method                                           | auto_arima, fbprophet                                        |
| -save-path        | Path for storing the forecasting result                      | -                                                            |
| -version, -v      | Current tool version                                         | -                                                            |
