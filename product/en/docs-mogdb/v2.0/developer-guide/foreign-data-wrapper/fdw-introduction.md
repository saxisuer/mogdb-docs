---
title: Introduction
summary: Introduction
author: Zhang Cuiping
date: 2021-05-17
---

# Introduction

The foreign data wrapper (FDW) of the MogDB can implement cross-database operations between MogDB databases and remote databases. Currently, the following remote databases are supported: PostgreSQL/openGauss/MogDB(postgres_fdw). FDW for Oracle and MySQL/MariaDB will be launched in the near future.
