---
title: Overview
summary: Overview
author: Zhang Cuiping
date: 2021-05-10
---

# Overview

## Function

The data replication capabilities supported by MogDB are as follows:

Data is periodically synchronized to heterogeneous databases (such as Oracle databases) using a data migration tool. Real-time data replication is not supported. Therefore, the requirements for real-time data synchronization to heterogeneous databases are not satisfied.

MogDB provides the logical decoding function to generate logical logs by decoding Xlogs. A target database parses logical logs to replicate data in real time. For details, see Figure 1. Logical replication reduces the restrictions on target databases, allowing for data synchronization between heterogeneous databases and homogeneous databases with different forms. It allows data to be read and written during data synchronization on a target database, reducing the data synchronization latency.

**Figure 1** Logical replication

![image-20210512181021060](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/logical-decoding-2.png)

Logical replication consists of logical decoding and data replication. Logical decoding outputs logical logs by transaction. The database service or middleware parses the logical logs to implement data replication. Currently, MogDB supports only logical decoding. Therefore, this section involves only logical decoding.

Logical decoding provides basic transaction decoding capabilities for logical replication. MogDB uses SQL functions for logical decoding. This method features easy function calling, requires no tools to obtain logical logs, and provides specific interfaces for interconnecting with external replay tools, saving the need of additional adaptation.

Logical logs are output only after transactions are committed because they use transactions as the unit and logical decoding is driven by users. Therefore, to prevent Xlogs from being reclaimed by the system when transactions start and prevent required transaction information from being reclaimed by **VACUUM**, MogDB introduces logical replication slots to block Xlog reclaiming.

A logical replication slot means a stream of changes that can be replayed in other databases in the order they were generated in the original database. Each owner of logical logs maintains one logical replication slot.

## Precautions

- Decoding into DDL statements is not supported.
- Decoding for column-store data and data page replication is not supported.
- Logical decoding is not supported between the standby node and the cascaded standby node.
- After a DDL statement (for example, **ALTER TABLE**) is executed, the physical logs that are not decoded before the DDL statement execution may be lost.
- Online database scale-out is not allowed during logical decoding.
- Do not execute **VACUUM FULL** when the logical decoding function is used.
- The size of a single tuple cannot exceed 1 GB, and decoding results may be larger than inserted data. Therefore, it is recommended that the size of a single tuple be less than or equal to 500 MB.
- MogDB supports the following data types for decoding: **INTEGER**, **BIGINT**, **SMALLINT**, **TINYINT**, **SERIAL**, **SMALLSERIAL**, **BIGSERIAL**, **FLOAT**, **DOUBLE PRECISION**, **DATE**, **TIME[WITHOUT TIME ZONE]**, **TIMESTAMP[WITHOUT TIME ZONE]**, **CHAR(*n*)**, **VARCHAR(n)**, **and** **TEXT**.
- Currently, SSL connections are not supported by default. If SSL connections are required, set the GUC parameter **ssl** to **on**.
- If JDBC is used to create a logical replication slot, the slot name must contain less than 64 characters and contain only one or more types of the following characters: letters (a to z), digits (0-9), and underscores (_).
- Currently, logical replication does not support the MOT feature.
- After the database where a logical replication slot resides is deleted, the replication slot becomes unavailable and needs to be manually deleted.
- Only the UTF-8 character set is supported.
- To decode multiple databases, you need to create a stream replication slot in each database and start decoding. Logs need to be scanned for decoding of each database.
- Forcible startup is not supported. After forcible startup, you need to export all data again.
- After the database failure restarts or the logical replication process restarts, the decoded data is duplicated, and the user needs to filter by himself.
- After the computer kernel fails, there are garbled codes in the decoding, which need to be filtered manually or automatically.
- When creating a logical replication slot, do not start a long transaction, otherwise the creation of the logical replication slot will be blocked.
