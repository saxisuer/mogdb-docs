---
title: SQLSetEnvAttr
summary: SQLSetEnvAttr
author: Guo Huan
date: 2021-05-17
---

# SQLSetEnvAttr

## Function

SQLSetEnvAttr is used to set environment attributes.

## Prototype

```
SQLRETURN SQLSetEnvAttr(SQLHENV       EnvironmentHandle
                        SQLINTEGER    Attribute,
                        SQLPOINTER    ValuePtr,
                        SQLINTEGER    StringLength);
```

## Parameter

**Table 1** SQLSetEnvAttr parameters

| **Keyword**       | **Parameter Description**                                    |
| :---------------- | :----------------------------------------------------------- |
| EnvironmentHandle | Environment handle.                                          |
| Attribute         | Environment attribute to be set. The value must be one of the following:<br/>- **SQL_ATTR_ODBC_VERSION**: ODBC version<br/>- **SQL_CONNECTION_POOLING**: connection pool attribute<br/>- **SQL_OUTPUT_NTS**: string type returned by the driver |
| ValuePtr          | Pointer to the **Attribute** value. **ValuePtr** depends on the **Attribute** value, and can be a 32-bit integer value or a null-terminated string. |
| StringLength      | If **ValuePtr** points to a string or a binary buffer, **StringLength** is the length of ***ValuePtr**. If **ValuePtr** points to an integer, **StringLength** is ignored. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLSetEnvAttr returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, set **HandleType** and **Handle** to **SQL_HANDLE_ENV** and **EnvironmentHandle**, and obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
