---
title: Description
summary: Description
author: Guo Huan
date: 2021-05-17
---

# Description

The ODBC interface is a set of API functions provided to users. This chapter describes its common interfaces. For details on other interfaces, see "ODBC Programmer's Reference" at MSDN (<https://msdn.microsoft.com/en-us/library/windows/desktop/ms714177(v=vs.85).aspx>).
