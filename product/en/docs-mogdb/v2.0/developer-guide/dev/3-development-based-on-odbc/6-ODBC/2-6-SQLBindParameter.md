---
title: SQLBindParameter
summary: SQLBindParameter
author: Guo Huan
date: 2021-05-17
---

# SQLBindParameter

## Function

SQLBindParameter is used to bind parameter markers in an SQL statement to a buffer.

## Prototype

```
SQLRETURN SQLBindParameter(SQLHSTMT       StatementHandle,
                           SQLUSMALLINT   ParameterNumber,
                           SQLSMALLINT    InputOutputType,
                           SQLSMALLINT    ValuetType,
                           SQLSMALLINT    ParameterType,
                           SQLULEN    ColumnSize,
                           SQLSMALLINT    DecimalDigits,
                           SQLPOINTER     ParameterValuePtr,
                           SQLLEN     BufferLength,
                           SQLLEN     *StrLen_or_IndPtr);
```

## Parameters

**Table 1** SQLBindParameter

| **Keyword**       | **Parameter Description**                                    |
| :---------------- | :----------------------------------------------------------- |
| StatementHandle   | Statement handle.                                            |
| ParameterNumber   | Parameter marker number, starting with 1 and increasing in ascending order. |
| InputOutputType   | Input/output type of the parameter.                          |
| ValueType         | C data type of the parameter.                                |
| ParameterType     | SQL data type of the parameter.                              |
| ColumnSize        | Size of the column or expression of the corresponding parameter marker. |
| DecimalDigits     | Decimal digit of the column or the expression of the corresponding parameter marker. |
| ParameterValuePtr | Pointer to the storage parameter buffer.                     |
| BufferLength      | Length of the **ParameterValuePtr** buffer in bytes.         |
| StrLen_or_IndPtr  | Pointer to the length or indicator of the buffer. If **StrLen_or_IndPtr** is null, no length or indicator is used. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLBindParameter returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_STMT** and **StatementHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
