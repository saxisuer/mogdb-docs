---
title: SQLDisconnect
summary: SQLDisconnect
author: Guo Huan
date: 2021-05-17
---

# SQLDisconnect

## Function

SQLDisconnect is used to close the connection associated with a database connection handle.

## Prototype

```
SQLRETURN SQLDisconnect(SQLHDBC    ConnectionHandle);
```

## Parameter

**Table 1** SQLDisconnect parameters

| **Keyword**      | **Parameter Description**                        |
| :--------------- | :----------------------------------------------- |
| ConnectionHandle | Connection handle, obtained from SQLAllocHandle. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLDisconnect returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_DBC** and **ConnectionHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
