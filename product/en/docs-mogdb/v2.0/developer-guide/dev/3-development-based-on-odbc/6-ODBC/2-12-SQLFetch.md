---
title: SQLFetch
summary: SQLFetch
author: Guo Huan
date: 2021-05-17
---

# SQLFetch

## Function

SQLFetch is used to advance the cursor to the next row of the result set and retrieve any bound columns.

## Prototype

```
CSQLRETURN SQLFetch(SQLHSTMT    StatementHandle);
```

## Parameter

**Table 1** SQLFetch parameters

| **Keyword**     | **Parameter Description**                       |
| :-------------- | :---------------------------------------------- |
| StatementHandle | Statement handle, obtained from SQLAllocHandle. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_NO_DATA** indicates that the SQL statement does not return a result set.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.
- **SQL_STILL_EXECUTING** indicates that the statement is being executed.

## Precautions

If SQLFetch returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_STMT** and **StatementHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
