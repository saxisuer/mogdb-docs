---
title: SQLColAttribute
summary: SQLColAttribute
author: Guo Huan
date: 2021-05-17
---

# SQLColAttribute

## Function

SQLColAttribute is used to return the descriptor information about a column in the result set.

## Prototype

```
SQLRETURN SQLColAttibute(SQLHSTMT        StatementHandle,
                         SQLUSMALLINT    ColumnNumber,
                         SQLUSMALLINT    FieldIdentifier,
                         SQLPOINTER      CharacterAtrriburePtr,
                         SQLSMALLINT     BufferLength,
                         SQLSMALLINT     *StringLengthPtr,
                         SQLLEN       *NumericAttributePtr);
```

## Parameters

**Table 1** SQLColAttribute parameters

| **Keyword**           | **Parameter Description**                                    |
| :-------------------- | :----------------------------------------------------------- |
| StatementHandle       | Statement handle.                                            |
| ColumnNumber          | Column number of the field to be queried, starting with 1 and increasing in ascending order. |
| FieldIdentifier       | Field identifier of **ColumnNumber** in IRD.                 |
| CharacterAttributePtr | **Output parameter**: pointer to the buffer that returns the **FieldIdentifier** value. |
| BufferLength          | - **BufferLength** indicates the length of the buffer if **FieldIdentifier** is an ODBC-defined field and **CharacterAttributePtr** points to a string or a binary buffer.<br/>- Ignore this parameter if **FieldIdentifier** is an ODBC-defined field and **CharacterAttributePtr** points to an integer. |
| StringLengthPtr       | **Output parameter**: pointer to a buffer in which the total number of valid bytes (for string data) is stored in ***CharacterAttributePtr**. Ignore the value of **BufferLength** if the data is not a string. |
| NumericAttributePtr   | **Output parameter**: pointer to an integer buffer in which the value of **FieldIdentifier** in the **ColumnNumber** row of the IRD is returned. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLColAttribute returns **SQL_ERROR** or **SQL_SUCCESS_WITH_INFO**, the application can call SQLGetDiagRec, with **HandleType** and **Handle** set to **SQL_HANDLE_STMT** and **StatementHandle**, respectively, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
