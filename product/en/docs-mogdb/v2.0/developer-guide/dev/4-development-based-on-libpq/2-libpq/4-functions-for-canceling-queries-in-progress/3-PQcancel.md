---
title: PQcancel
summary: PQcancel
author: Guo Huan
date: 2021-05-17
---

# PQcancel

## Function

PQcancel is used to request the server to abandon processing of the current command.

## Prototype

```
int PQcancel(PGcancel *cancel, char *errbuf, int errbufsize);
```

## Parameter

**Table 1** PQcancel parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| cancel      | Points to the object pointer that contains the cancel information. |
| errbuf      | Buffer for storing error information.                        |
| errbufsize  | Size of the buffer for storing error information.            |

## Return Value

**int** indicates the execution result. **1** indicates successful execution and **0** indicates an execution failure. The failure cause is stored in **errbuf**.

## Precautions

- Successful sending does not guarantee that the request will have any effect. If the cancellation is valid, the current command is terminated early and an error is returned. If the cancellation fails (for example, because the server has processed the command), no result is returned.
- If **errbuf** is a local variable in a signal handler, you can safely call PQcancel from the signal handler. For PQcancel, the PGcancel object is read-only, so it can also be called from a thread that is separate from the thread that is operating the PGconn object.

## Example

For details, see Example.
