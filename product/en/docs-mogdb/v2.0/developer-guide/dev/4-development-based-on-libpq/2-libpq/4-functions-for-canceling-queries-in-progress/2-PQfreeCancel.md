---
title: PQfreeCancel
summary: PQfreeCancel
author: Guo Huan
date: 2021-05-17
---

# PQfreeCancel

## Function

PQfreeCancel is used to release the data structure created by PQgetCancel.

## Prototype

```
void PQfreeCancel(PGcancel *cancel);
```

## Parameter

**Table 1** PQfreeCancel parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| cancel      | Points to the object pointer that contains the cancel information. |

## Precautions

PQfreeCancel releases a data object previously created by PQgetCancel.

## Example

For details, see Example.
