---
title: PQsendQueryPrepared
summary: PQsendQueryPrepared
author: Guo Huan
date: 2021-05-17
---

# PQsendQueryPrepared

## Function

PQsendQueryPrepared is used to send a request to execute a prepared statement with given parameters, without waiting for the result.

## Prototype

```
int PQsendQueryPrepared(PGconn *conn,
                        const char *stmtName,
                        int nParams,
                        const char * const *paramValues,
                        const int *paramLengths,
                        const int *paramFormats,
                        int resultFormat);
```

## Parameters

**Table 1** PQsendQueryPrepared parameters

| **Keyword**  | **Parameter Description**                                    |
| :----------- | :----------------------------------------------------------- |
| conn         | Points to the object pointer that contains the connection information. |
| stmtName     | Name of **stmt** to be executed.                             |
| nParams      | Parameter quantity.                                          |
| paramValues  | Parameter value.                                             |
| paramLengths | Parameter length.                                            |
| paramFormats | Parameter format.                                            |
| resultFormat | Result format.                                               |

## Return Value

**int** indicates the execution result. **1** indicates successful execution and **0** indicates an execution failure. The failure cause is stored in **conn-&gt;errorMessage**.

## Precautions

PQsendQueryPrepared is similar to PQsendQueryParams, but the command to be executed is specified by naming a previously-prepared statement, instead of providing a query string. PQsendQueryPrepared parameters are handled in the same way as PQexecPrepared parameters. Like PQexecPrepared, PQsendQueryPrepared cannot work on connections using protocol 2.0.

## Example

For details, see Example.
