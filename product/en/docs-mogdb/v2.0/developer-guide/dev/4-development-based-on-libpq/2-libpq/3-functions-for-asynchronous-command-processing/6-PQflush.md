---
title: PQflush
summary: PQflush
author: Guo Huan
date: 2021-05-17
---

# PQflush

## Function

PQflush is used to try to flush any queued output data to the server.

## Prototype

```
Cint PQflush(PGconn *conn);
```

## Parameter

**Table 1** PQflush parameter

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |

## Return Value

**int** indicates the execution result. If the operation is successful (or the send queue is empty), **0** is returned. If the operation fails, **-1** is returned. If all data in the send queue fails to be sent, **1** is returned. (This case occurs only when the connection is non-blocking.) The failure cause is stored in **conn-&gt;error_message**.

## Precautions

Call PQflush after sending any command or data over a non-blocking connection. If **1** is returned, wait for the socket to become read- or write-ready. If the socket becomes write-ready, call PQflush again. If the socket becomes read-ready, call PQconsumeInput and then call PQflush again. Repeat the operation until the value **0** is returned for PQflush. (It is necessary to check for read-ready and drain the input using PQconsumeInput. This is because the server can block trying to send us data, for example, notification messages, and will not read our data until we read it.) Once PQflush returns **0**, wait for the socket to be read-ready and then read the response as described above.

## Example

For details, see Example.
