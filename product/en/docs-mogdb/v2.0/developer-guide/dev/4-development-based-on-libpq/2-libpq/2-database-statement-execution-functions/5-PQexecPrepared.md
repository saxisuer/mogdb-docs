---
title: PQexecPrepared
summary: PQexecPrepared
author: Guo Huan
date: 2021-05-17
---

# PQexecPrepared

## Function

PQexecPrepared is used to send a request to execute a prepared statement with given parameters and wait for the result.

## Prototype

```
PGresult* PQexecPrepared(PGconn* conn,
                         const char* stmtName,
                         int nParams,
                         const char* const* paramValues,
                         const int* paramLengths,
                         const int* paramFormats,
                         int resultFormat);
```

## Parameter

**Table 1**

| **Keyword**  | **Parameter Description**                                    |
| :----------- | :----------------------------------------------------------- |
| conn         | Connection handle.                                           |
| stmtName     | **stmt** name, which can be set to "" or NULL to reference an unnamed statement. Otherwise, it must be the name of an existing prepared statement. |
| nParams      | Parameter quantity.                                          |
| paramValues  | Actual values of parameters.                                 |
| paramLengths | Actual data lengths of parameters.                           |
| paramFormats | Parameter formats (text or binary).                          |
| resultFormat | Return result format (text or binary).                       |

## Return Value

PGresult pointers
