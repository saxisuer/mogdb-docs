---
title: PQfname
summary: PQfname
author: Guo Huan
date: 2021-05-17
---

# PQfname

## Function

PQfname is used to return the column name associated with the given column number. Column numbers start from 0. The caller should not release the result directly. The result will be released when the associated PGresult handle is passed to PQclear.

## Prototype

```
char *PQfname(const PGresult *res,
              int column_number);
```

## Parameter

**Table 1**

| **Keyword**   | **Parameter Description** |
| :------------ | :------------------------ |
| res           | Operation result handle.  |
| column_number | Number of columns.        |

## Return Value

char pointers

## Example

For details, see Example.
