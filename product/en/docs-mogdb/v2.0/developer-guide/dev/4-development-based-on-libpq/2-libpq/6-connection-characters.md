---
title: Connection Characters
summary: Connection Characters
author: Guo Huan
date: 2021-05-17
---

# Connection Characters

**Table 1** Connection strings

| Character String    | Description                                                  |
| :------------------ | :----------------------------------------------------------- |
| host                | Name of the host to connect to. If the host name starts with a slash (/), Unix-domain socket communications instead of TCP/IP communications are used. The value is the directory where the socket file is stored. If **host** is not specified, the default behavior is to connect to the Unix-domain socket in the **/tmp** directory (or the socket directory specified during MogDB installation). On a machine without a Unix-domain socket, the default behavior is to connect to **localhost**. |
| hostaddr            | IP address of the host to connect to. The value is in standard IPv4 address format, for example, 172.28.40.9. If your machine supports IPv6, IPv6 address can also be used. If a non-null string is specified, TCP/IP communications are used.<br/>Replacing **host** with **hostaddr** can prevent applications from querying host names, which may be important for applications with time constraints. However, a host name is required for GSSAPI or SSPI authentication methods. Therefore, the following rules are used:<br/>1. If **host** is specified but **hostaddr** is not, a query for the host name will be executed.<br/>2. If **hostaddr** is specified but **host** is not, the value of **hostaddr** is the server network address. If the host name is required by authentication, the connection attempt fails.<br/>3. If both **host** and **hostaddr** are specified, the value of **hostaddr** is the server network address. The value of **host** is ignored unless it is required by authentication, in which case it is used as the host name.<br/>NOTICE:<br/>- If **host** is not the server name in the network address specified by **hostaddr**, the authentication may fail.<br/>- If neither **host** nor **hostaddr** is specified, libpq will use a local Unix-domain socket for connection. If the machine does not have a Unix-domain socket, it will attempt to connect to **localhost**. |
| port                | Port number of the host server, or the socket file name extension for Unix-domain connections. |
| user                | Name of the user to connect as. By default, the username is the same as the operating system name of the user running the application. |
| dbname              | Database name. The default value is the same as the username. |
| password            | Password to be used if the server requires password authentication. |
| connect_timeout     | Maximum timeout period of the connection, in seconds (in decimal integer string). The value **0** or null indicates infinity. You are not advised to set the connection timeout period to a value less than 2 seconds. |
| client_encoding     | Client encoding for the connection. In addition to the values accepted by the corresponding server options, you can use **auto** to determine the correct encoding from the current environment in the client (the LC_CTYPE environment variable in the Unix system). |
| options             | Adds command-line options to send to the server at runtime.  |
| application_name    | Current user identity.                                       |
| keepalives          | Whether TCP keepalive is enabled on the client side. The default value is **1**, indicating that the function is enabled. The value **0** indicates that the function is disabled. Ignore this parameter for Unix-domain connections. |
| keepalives_idle     | The number of seconds of inactivity after which TCP should send a keepalive message to the server. The value **0** indicates that the default value is used. Ignore this parameter for Unix-domain connections or if keep-alive is disabled. |
| keepalives_interval | The number of seconds after which a TCP keepalive message that is not acknowledged by the server should be retransmitted. The value **0** indicates that the default value is used. Ignore this parameter for Unix-domain connections or if keep-alive is disabled. |
| keepalives_count    | Adds command-line options to send to the server at runtime. For example, adding **-c comm_debug_mode=off** to set the value of the GUC parameter **comm_debug_mode** to **off**. |
