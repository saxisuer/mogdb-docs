---
title: PQsendQueryParams
summary: PQsendQueryParams
author: Guo Huan
date: 2021-05-17
---

# PQsendQueryParams

## Function

PQsendQueryParams is used to commit a command and separate parameters to the server without waiting for the result.

## Prototype

```
int PQsendQueryParams(PGconn *conn,
                      const char *command,
                      int nParams,
                      const Oid *paramTypes,
                      const char * const *paramValues,
                      const int *paramLengths,
                      const int *paramFormats,
                      int resultFormat);
```

## Parameter

**Table 1** PQsendQueryParams parameters

| **Keyword**  | **Parameter Description**                                    |
| :----------- | :----------------------------------------------------------- |
| conn         | Points to the object pointer that contains the connection information. |
| command      | Query string to be executed.                                 |
| nParams      | Parameter quantity.                                          |
| paramTypes   | Parameter type.                                              |
| paramValues  | Parameter value.                                             |
| paramLengths | Parameter length.                                            |
| paramFormats | Parameter format.                                            |
| resultFormat | Result format.                                               |

## Return Value

**int** indicates the execution result. **1** indicates successful execution and **0** indicates an execution failure. The failure cause is stored in **conn-&gt;errorMessage**.

## Precautions

PQsendQueryParams is equivalent to PQsendQuery. The only difference is that query parameters can be specified separately from the query string. PQsendQueryParams parameters are handled in the same way as PQexecParams parameters. Like PQexecParams, PQsendQueryParams cannot work on connections using protocol 2.0 and it allows only one command in the query string.

## Example

For details, see Example.
