---
title: PQconnectdbParams
summary: PQconnectdbParams
author: Guo Huan
date: 2021-05-17
---

# PQconnectdbParams

## Function

PQconnectdbParams is used to establish a new connection with the database server.

## Prototype

```
PGconn *PQconnectdbParams(const char * const *keywords,
                          const char * const *values,
                          int expand_dbname);
```

## Parameter

**Table 1** PQconnectdbParams parameters

| **Keyword**   | **Parameter Description**                                    |
| :------------ | :----------------------------------------------------------- |
| keywords      | An array of strings, each of which is a keyword.             |
| values        | Value assigned to each keyword.                              |
| expand_dbname | When **expand\_dbname** is non-zero, the **dbname** keyword value can be recognized as a connection string. Only **dbname** that first appears is expanded in this way, and any subsequent **dbname** value is treated as a database name. |

## Return Value

**PGconn \*** points to the object pointer that contains a connection. The memory is applied for by the function internally.

## Precautions

This function establishes a new database connection using the parameters taken from two NULL-terminated arrays. Unlike PQsetdbLogin, the parameter set can be extended without changing the function signature. Therefore, use of this function (or its non-blocking analogs PQconnectStartParams and PQconnectPoll) is preferred for new application programming.

## Example

For details, see Example.
