---
title: PQconnectStart
summary: PQconnectStart
author: Guo Huan
date: 2021-05-17
---

# PQconnectStart

## Function

PQconnectStart is used to establish a non-blocking connection with the database server.

## Prototype

```
PGconn* PQconnectStart(const char* conninfo);
```

## Parameters

**Table 1**

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conninfo    | String of connection information. This parameter can be left empty. In this case, the default value is used. It can contain one or more values separated by spaces or contain a URL. |

## Return Value

PGconn pointers
