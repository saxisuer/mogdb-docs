---
title: Development Process
summary: Development Process
author: Guo Huan
date: 2021-04-26
---

# Development Process

**Figure 1** Application development process based on JDBC

![application-development-process-based-on-jdbc](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-process-2.png)
