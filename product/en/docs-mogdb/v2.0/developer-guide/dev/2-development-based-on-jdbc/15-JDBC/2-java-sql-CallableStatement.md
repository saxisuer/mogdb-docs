---
title: java.sql.CallableStatement
summary: java.sql.CallableStatement
author: Guo Huan
date: 2021-05-17
---

# java.sql.CallableStatement

This section describes **java.sql.CallableStatement**, the interface for executing the stored procedure.

**Table 1** Support status for java.sql.CallableStatement

| Method Name                                        | Return Type | Support JDBC 4 |
| :------------------------------------------------- | :---------- | :------------- |
| registerOutParameter(int parameterIndex, int type) | void        | Yes            |
| wasNull()                                          | Boolean     | Yes            |
| getString(int parameterIndex)                      | String      | Yes            |
| getBoolean(int parameterIndex)                     | Boolean     | Yes            |
| getByte(int parameterIndex)                        | byte        | Yes            |
| getShort(int parameterIndex)                       | short       | Yes            |
| getInt(int parameterIndex)                         | int         | Yes            |
| getLong(int parameterIndex)                        | long        | Yes            |
| getFloat(int parameterIndex)                       | float       | Yes            |
| getDouble(int parameterIndex)                      | double      | Yes            |
| getBigDecimal(int parameterIndex)                  | BigDecimal  | Yes            |
| getBytes(int parameterIndex)                       | byte[]      | Yes            |
| getDate(int parameterIndex)                        | Date        | Yes            |
| getTime(int parameterIndex)                        | Time        | Yes            |
| getTimestamp(int parameterIndex)                   | Timestamp   | Yes            |
| getObject(int parameterIndex)                      | Object      | Yes            |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - The batch operation of statements containing OUT parameter is not allowed.
> - The following methods are inherited from java.sql.Statement: close, execute, executeQuery, executeUpdate, getConnection, getResultSet, getUpdateCount, isClosed, setMaxRows, and setFetchSize.
> - The following methods are inherited from java.sql.PreparedStatement: addBatch, clearParameters, execute, executeQuery, executeUpdate, getMetaData, setBigDecimal, setBoolean, setByte, setBytes, setDate, setDouble, setFloat, setInt, setLong, setNull, setObject, setString, setTime, and setTimestamp.
