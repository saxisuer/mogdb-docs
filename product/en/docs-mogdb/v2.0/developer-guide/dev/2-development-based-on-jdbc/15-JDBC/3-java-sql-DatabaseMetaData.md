---
title: java.sql.DatabaseMetaData
summary: java.sql.DatabaseMetaData
author: Guo Huan
date: 2021-05-17
---

# java.sql.DatabaseMetaData

This section describes **java.sql.DatabaseMetaData**, the interface for defining database objects.

**Table 1** Support status for java.sql.DatabaseMetaData

| Method Name                                                  | Return Type | Support JDBC 4 |
| :----------------------------------------------------------- | :---------- | :------------- |
| getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types) | ResultSet   | Yes            |
| getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) | ResultSet   | Yes            |
| getTableTypes()                                              | ResultSet   | Yes            |
| getUserName()                                                | String      | Yes            |
| isReadOnly()                                                 | Boolean     | Yes            |
| nullsAreSortedHigh()                                         | Boolean     | Yes            |
| nullsAreSortedLow()                                          | Boolean     | Yes            |
| nullsAreSortedAtStart()                                      | Boolean     | Yes            |
| nullsAreSortedAtEnd()                                        | Boolean     | Yes            |
| getDatabaseProductName()                                     | String      | Yes            |
| getDatabaseProductVersion()                                  | String      | Yes            |
| getDriverName()                                              | String      | Yes            |
| getDriverVersion()                                           | String      | Yes            |
| getDriverMajorVersion()                                      | int         | Yes            |
| getDriverMinorVersion()                                      | int         | Yes            |
| usesLocalFiles()                                             | Boolean     | Yes            |
| usesLocalFilePerTable()                                      | Boolean     | Yes            |
| supportsMixedCaseIdentifiers()                               | Boolean     | Yes            |
| storesUpperCaseIdentifiers()                                 | Boolean     | Yes            |
| storesLowerCaseIdentifiers()                                 | Boolean     | Yes            |
| supportsMixedCaseQuotedIdentifiers()                         | Boolean     | Yes            |
| storesUpperCaseQuotedIdentifiers()                           | Boolean     | Yes            |
| storesLowerCaseQuotedIdentifiers()                           | Boolean     | Yes            |
| storesMixedCaseQuotedIdentifiers()                           | Boolean     | Yes            |
| supportsAlterTableWithAddColumn()                            | Boolean     | Yes            |
| supportsAlterTableWithDropColumn()                           | Boolean     | Yes            |
| supportsColumnAliasing()                                     | Boolean     | Yes            |
| nullPlusNonNullIsNull()                                      | Boolean     | Yes            |
| supportsConvert()                                            | Boolean     | Yes            |
| supportsConvert(int fromType, int toType)                    | Boolean     | Yes            |
| supportsTableCorrelationNames()                              | Boolean     | Yes            |
| supportsDifferentTableCorrelationNames()                     | Boolean     | Yes            |
| supportsExpressionsInOrderBy()                               | Boolean     | Yes            |
| supportsOrderByUnrelated()                                   | Boolean     | Yes            |
| supportsGroupBy()                                            | Boolean     | Yes            |
| supportsGroupByUnrelated()                                   | Boolean     | Yes            |
| supportsGroupByBeyondSelect()                                | Boolean     | Yes            |
| supportsLikeEscapeClause()                                   | Boolean     | Yes            |
| supportsMultipleResultSets()                                 | Boolean     | Yes            |
| supportsMultipleTransactions()                               | Boolean     | Yes            |
| supportsNonNullableColumns()                                 | Boolean     | Yes            |
| supportsMinimumSQLGrammar()                                  | Boolean     | Yes            |
| supportsCoreSQLGrammar()                                     | Boolean     | Yes            |
| supportsExtendedSQLGrammar()                                 | Boolean     | Yes            |
| supportsANSI92EntryLevelSQL()                                | Boolean     | Yes            |
| supportsANSI92IntermediateSQL()                              | Boolean     | Yes            |
| supportsANSI92FullSQL()                                      | Boolean     | Yes            |
| supportsIntegrityEnhancementFacility()                       | Boolean     | Yes            |
| supportsOuterJoins()                                         | Boolean     | Yes            |
| supportsFullOuterJoins()                                     | Boolean     | Yes            |
| supportsLimitedOuterJoins()                                  | Boolean     | Yes            |
| isCatalogAtStart()                                           | Boolean     | Yes            |
| supportsSchemasInDataManipulation()                          | Boolean     | Yes            |
| supportsSavepoints()                                         | Boolean     | Yes            |
| supportsResultSetHoldability(int holdability)                | Boolean     | Yes            |
| getResultSetHoldability()                                    | int         | Yes            |
| getDatabaseMajorVersion()                                    | int         | Yes            |
| getDatabaseMinorVersion()                                    | int         | Yes            |
| getJDBCMajorVersion()                                        | int         | Yes            |
| getJDBCMinorVersion()                                        | int         | Yes            |
