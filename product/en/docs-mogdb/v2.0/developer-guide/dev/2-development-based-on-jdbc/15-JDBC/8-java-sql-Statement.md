---
title: java.sql.Statement
summary: java.sql.Statement
author: Guo Huan
date: 2021-05-17
---

# java.sql.Statement

This section describes **java.sql.Statement**, the interface for executing SQL statements.

**Table 1** Support status for java.sql.Statement

| Method Name                  | Return Type | Support JDBC 4 |
| :--------------------------- | :---------- | :------------- |
| close()                      | void        | Yes            |
| execute(String sql)          | Boolean     | Yes            |
| executeQuery(String sql)     | ResultSet   | Yes            |
| executeUpdate(String sql)    | int         | Yes            |
| getConnection()              | Connection  | Yes            |
| getResultSet()               | ResultSet   | Yes            |
| getQueryTimeout()            | int         | Yes            |
| getUpdateCount()             | int         | Yes            |
| isClosed()                   | Boolean     | Yes            |
| setQueryTimeout(int seconds) | void        | Yes            |
| setFetchSize(int rows)       | void        | Yes            |
| cancel()                     | void        | Yes            |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Using setFetchSize can reduce the memory occupied by result sets on the client. Result sets are packaged into cursors and segmented for processing, which will increase the communication traffic between the database and the client, affecting performance.
> - Database cursors are valid only within their transactions. If **setFetchSize** is set, set **setAutoCommit(false)** and commit transactions on the connection to flush service data to a database.
