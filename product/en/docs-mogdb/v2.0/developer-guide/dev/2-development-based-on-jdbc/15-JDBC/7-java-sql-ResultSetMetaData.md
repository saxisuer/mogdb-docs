---
title: java.sql.ResultSetMetaData
summary: java.sql.ResultSetMetaData
author: Guo Huan
date: 2021-05-17
---

# java.sql.ResultSetMetaData

This section describes **java.sql.ResultSetMetaData**, which provides details about ResultSet object information.

**Table 1** Support status for java.sql.ResultSetMetaData

| Method Name                   | Return Type | Support JDBC 4 |
| :---------------------------- | :---------- | :------------- |
| getColumnCount()              | int         | Yes            |
| getColumnName(int column)     | String      | Yes            |
| getColumnType(int column)     | int         | Yes            |
| getColumnTypeName(int column) | String      | Yes            |
