---
title: java.sql.Connection
summary: java.sql.Connection
author: Guo Huan
date: 2021-05-17
---

# java.sql.Connection

This section describes **java.sql.Connection**, the interface for connecting to a database.

**Table 1** Support status for java.sql.Connection

| Method Name                             | Return Type       | Support JDBC 4 |
| :-------------------------------------- | :---------------- | :------------- |
| close()                                 | void              | Yes            |
| commit()                                | void              | Yes            |
| createStatement()                       | Statement         | Yes            |
| getAutoCommit()                         | Boolean           | Yes            |
| getClientInfo()                         | Properties        | Yes            |
| getClientInfo(String name)              | String            | Yes            |
| getTransactionIsolation()               | int               | Yes            |
| isClosed()                              | Boolean           | Yes            |
| isReadOnly()                            | Boolean           | Yes            |
| prepareStatement(String sql)            | PreparedStatement | Yes            |
| rollback()                              | void              | Yes            |
| setAutoCommit(boolean autoCommit)       | void              | Yes            |
| setClientInfo(Properties properties)    | void              | Yes            |
| setClientInfo(String name,String value) | void              | Yes            |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The AutoCommit mode is used by default within the interface. If you disable it by running **setAutoCommit(false)**, all the statements executed later will be packaged in explicit transactions, and you cannot execute statements that cannot be executed within transactions.
