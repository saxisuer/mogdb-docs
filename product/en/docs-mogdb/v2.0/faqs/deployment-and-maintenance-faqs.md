---
title: Deployment and Maintenance FAQs
summary: Deployment and Maintenance FAQs
author: Guo Huan
date: 2021-09-26
---

# Deployment and Maintenance FAQs

## Environment Initialization FAQs

**Q1: What parameter values need to be changed in `clusterconfig.xml` sample configuration file?**

The node name and IP address need to be changed.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- Overall information about MogDB -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="mogdb-kernel-0002" />
        <PARAM name="backIp1s" value="172.16.0.245"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/opt/mogdb/mogdb_log/omm" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tool" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>

<!-- Information about node deployment on each server -->
    <DEVICELIST>
        <!-- Information about the node deployment on node1 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="mogdb-kernel-0002"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address. -->
            <PARAM name="backIp1" value="172.16.0.245"/>
            <PARAM name="sshIp1" value="172.16.0.245"/>

            <!--dbnode-->
            <PARAM name="dataNum" value="1"/>
            <PARAM name="dataPortBase" value="26000"/>
            <PARAM name="dataNode1" value="/mogdb/data/db1"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

<br/>

## Script Initialization FAQs

**Q2: What should I do if it is prompted that the parameter value of the configuration file does not match the system environment variable value?**

```bash
Failed to check the preinstall repeat condition.gaussdbToolPath [/opt/enmo/wisery] is not same with environment [/opt/mogdb/tool]
```

You can change the value of `gaussdbToolPath` in `clusterconfig.xml` to `/opt/mogdb/tool`.

> Note:
>
> - This problem occurs mainly because the system has installed MogDB before and the environment may not be fully cleared, so there will be a situation that the parameter value in the configuration file does not match the system environment variable value, which will lead to the failure of execution. You can change the value of the parameter that reports an error in `clusterconfig.xml` configuration file to the same value as that of the parameter in the environment variable file.
> - You can run `vi ~/.bashrc` as user `omm` to view the environment variables.

**Q3: What should I do if it is prompted that I do not have permission to run the command?**

```bash
Fixing server package owner.
[GAUSS-51400] : Failed to execute the command: su - omm -c 'cd '/opt/software/mogdb/''. Error:
-bash: line 0: cd: /opt/software/mogdb/: Permission denied
```

Run `chmod 755 -R /opt/software` to grant permissions to the directory, and then run the script initialization command again.

**Q4: What should I do if the symbolic link creation fails?**

```bash
In:failed to create symbolic link 'usr/bin/python3': File exists
```

Delete `python3` from the `usr/bin/` directory and run the `In` command again.

**Q5: What should I do if it is prompted that the host name is not in the cluster?**

```bash
GAUSS-51619: "The host name [host-172-16-0-195] is not in the cluster."
```

This is because the actual host name is `host-172-16-0-195`, not `mogdb-kernel-0006`, so you need to change the node name in `clusterconfig.xml` to `host-172-16-0-195`.

**Q6: What should I do if it is prompted the following error?**

```bash
[GAUSS-50202]:The /opt/mogdb must be empty. Or user [omm] has write permission to directory /opt/mogdb. Because it will create symbolic link [/opt/mogdb/app] to install path [/opt/mogdb/app_e0e9f58d] in gs_install process with this user.
```

Run `vi ~/.bashrc` as user `omm` to clear the environment variables and execute the initialization script again.

**Q7: What should I do if an error related to certain dependency software is prompted?**

```
/opt/software/mogdb/bin/mogdb: error while loading shared libraries: libnsl.so.1: cannot open shared object file: No such file or directory
no data was returned by command ""/opt/software/mogdb/bin/mogdb" -V"
The program "mogdb" is needed by gs_initdb but was not found in the
same directory as "/opt/software/mogdb/bin/gs_initdb".
Check your installation.
```

Reinstall the dependency software libnsl*.

<br/>

## Installation FAQs

**Q8: What should I do if it is prompted that the cluster has been installed?**

```bash
[GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc;python3 '/opt/mogdb/tool/script/local/CheckInstall.py' -U omm:dbgrp -R /opt/mogdb/app -l /opt/mogdb/mogdb_log/omm/omm/om/gs_local.log -X /opt/software/mogdb/clusterconfig.xml.Error:
Checking old installation.
[GAUSS-51806] : The cluster has been installed.
```

Run `vi ~/.bashrc` to view the environment variables, change `GAUSS_ENV=2` to `GAUSS_ENV=1`, and run the install command again.

**Q9: What should I do if the following error is prompted?**

```bash
[GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc;python3 '/opt/mogdb/tool/script/local/CheckInstall.py' -U omm:dbgrp -R /opt/mogdb/app -l /opt/mogdb/mogdb_log/omm/omm/om/gs_local.log -X /opt/software/mogdb/clusterconfig.xml.Error:
Checking old installation.
Successfully checked old installation.
Checking SHA256.
Successfully checked SHA256.
Checking kernel parameters.
Successfully checked kernel parameters.
Checking directory.
[GAUSS-50202] : The /opt/mogdb/app_b75b585a must be empty.
```

Switch to `/opt/mogdb/app_b75b585a` directory as user **root**, run `rm -rf *` to delete all files in the directory, and run the install command again.

**Q10: What should I do if it is prompted that the socket file already exists?**

```bash
[GAUSS-50200] : The socket file already exists. Port:26000.
```

You can change the `dataPortBase` value in `clusterconfig.xml` to a port that is available.

<br/>

## Post-Deployment FAQs

**Failed to start database via `gs_om -t start`: Changed the host name via `hostnamectl set` after standard installation**

**Applicable to:**

* MogDB 1.1.0

* MogDB 2.0.0

* MogDB 2.0.1

* MogDB single node

The solution in this document is for the x86 architecture.

**Symptom:**

After the standard installation is finished (single node), when `hostnamectl set` is run to change the host name, and `gs_om -t start` is run to start the database, the start fails.

For example:

1. Change the host name from `mogdb-kernel-0005` to `www.mogdb.com` by running `hostnamectl set` as user `root`.

   ```bash
   [root@mogdb-kernel-0005 ~]# hostnamectl
   Static hostname: mogdb-kernel-0005
         Icon name: computer-vm
           Chassis: vm
        Machine ID: de03a5f3bf5a4b7e9df939c9dc5b428d
           Boot ID: 828bfbe948ed4de7aff2a059103a0eaa
    Virtualization: kvm
    Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 3.10.0-1160.15.2.el7.x86_64
      Architecture: x86-64
   [root@mogdb-kernel-0005 ~]# id
   uid=0(root) gid=0(root) groups=0(root)
   [root@mogdb-kernel-0005 ~]# hostname
   mogdb-kernel-0005
   [root@mogdb-kernel-0005 ~]# hostnamectl set-hostname www.mogdb.com
   ```

2. Run the following command as any user to see if the host name is successfully changed.

   ```bash
   [root@mogdb-kernel-0005 ~]# hostname
   www.mogdb.com
   ```

3. Run the following command as user `omm` to stop the database.

   ```bash
   [root@mogdb-kernel-0005 ~]# su - omm
   Last login: Tue Jul 27 11:18:16 CST 2021 on pts/1
   [omm@www ~]$ gs_ctl stop -D /mogdb/data/db1
   [2021-07-27 11:19:20.283][32166][][gs_ctl]: gs_ctl stopped ,datadir is /mogdb/data/db1
   waiting for server to shut down.... done
   server stopped
   ```

4. Run the following command as user `omm` to start the database.

   ```bash
   [omm@www ~]$ gs_om -t start

   Starting cluster.
   =========================================

   =========================================
   [GAUSS-53600]: Can not start the database, the cmd is source /home/omm/.bashrc; python3 '/opt/enmo/wisequery/script/local/StartInstance.py' -U omm -R /opt/mogdb/app -t 300 --security-mode=off,  Error:
   [GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc; python3 '/opt/enmo/wisequery/script/local/StartInstance.py' -U omm -R /opt/mogdb/app -t 300 --security-mode=off. Error:
   [FAILURE] mogdb-kernel-0005
   ```

**Purpose:**

After the standard installation (single node) is finished , when the host name is changed through `modip.py`, and `gs_om -t start` is run to start the command, the database starts successfully.

**Resolution:**

1. Create `modip.py` as user `omm`.

   ```python
   [omm@www ~]$ cd
   [omm@www ~]$ pwd
   /home/omm
   [omm@www ~]$ vi modip.py

   # -*- coding:utf-8 -*-

   import sys
   ##sys.path.append(r'$GPHOME/script/')
   sys.path.append(r'/opt/enmo/wisequery/script/')
   from gspylib.common.DbClusterInfo import dbClusterInfo

   oldIP = '172.16.0.176'
   newIP = '172.16.0.176'
   oldnodename = 'mogdb-kernel-0005'
   newnodename = 'www.mogdb.com'
   ##Output file directory
   tmpDir = r'/home/omm'
   dbuser = 'omm'

   c1=dbClusterInfo()
   c1.initFromStaticConfig(dbuser)

   for node in c1.dbNodes:
       if node.backIps[0] == oldIP:
           node.backIps[0] = newIP
       if len(node.virtualIp):
           if node.virtualIp[0] == oldIP:
               node.virtualIp[0] = newIP
       if node.name == oldnodename:
           node.name = newnodename
       if node.sshIps[0] == oldIP:
           node.sshIps[0] = newIP
       for dn in node.datanodes:
           if dn.listenIps[0] == oldIP:
               dn.listenIps[0] = newIP
           if dn.haIps[0] == oldIP:
               dn.haIps[0] = newIP
           for peerdn in dn.peerInstanceInfos:
               if len(peerdn.peerHAIPs):
                   if peerdn.peerHAIPs[0] == oldIP:
                       peerdn.peerHAIPs[0] = newIP
   for node in c1.dbNodes:
       staticConfigPath_dn = "%s/cluster_static_config_%s" % (tmpDir, node.name)
       c1.saveToStaticConfig(staticConfigPath_dn, node.id)
       print(staticConfigPath_dn)
   ```

   > **Note：**You need to modify the IP address and host name according to the actual situation. In this example, the IP address is `172.16.0.176`, the old host name is `mogdb-kernel-0005`, and the new host name is `www.mogdb.com`.

2. Run the following command to generate the static configuration file of the cluster.

   ```bash
   [omm@www ~]$ python3 modip.py
   ```

   The following output indicates that the command is run successfully.

   ```bash
   /home/omm/cluster_static_config_www.mogdb.com
   ```

3. Run the following command to replace the original file.

   ```bash
   [omm@www ~]$ mv /opt/mogdb/app/bin/cluster_static_config /opt/mogdb/app/bin/cluster_static_config_mogdb.com

   [omm@www ~]$ cp /home/omm/cluster_static_config_www.mogdb.com /opt/mogdb/app/bin/cluster_static_config
   ```

4. Run the following command to start the database.

   ```bash
   [omm@www ~]$ gs_om -t start

   Starting cluster.
   =========================================

   [SUCCESS] www.mogdb.com

   2021-07-26 16:20:42.938 60fe7056.1 [unknown] 139667408496384 [unknown] 0 dn_6001 01000  0 [BACKEND] WARNING: could not create any HA TCP/IP sockets
   =========================================

   Successfully started.
   ```
