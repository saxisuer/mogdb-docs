---
title: Updating Statistics
summary: Updating Statistics
author: Guo Huan
date: 2021-03-16
---

# Updating Statistics

In a database, statistics indicate the source data of a plan generated by a planner. If no statistics are available or out of date, the execution plan may seriously deteriorate, leading to low performance.

## Background

The **ANALYZE** statement collects statistic about table contents in databases, which will be stored in the **PG_STATISTIC** system catalog. Then, the query optimizer uses the statistics to work out the most efficient execution plan.

After executing batch insertions and deletions, you are advised to run the **ANALYZE** statement on the table or the entire library to update statistics. By default, 30,000 rows of statistics are sampled. That is, the default value of the GUC parameter **default_statistics_target** is **100**. If the total number of rows in the table exceeds 1,600,000, you are advised to set **default_statistics_target** to **-2**, indicating that 2% of the statistics are collected.

For an intermediate table generated during the execution of a batch script or stored procedure, you also need to run the **ANALYZE** statement.

If there are multiple inter-related columns in a table and the conditions or grouping operations based on these columns are involved in the query, collect statistics about these columns so that the query optimizer can accurately estimate the number of rows and generate an effective execution plan.

## Procedure

Run the following commands to update the statistics about a table or the entire database:

```sql
--Update statistics about a table.
ANALYZE tablename;
---Update statistics about the entire database.
ANALYZE;
```

Run the following statements to perform statistics-related operations on multiple columns:

```sql
--Collect statistics about column_1 and column_2 of tablename.
ANALYZE tablename ((column_1, column_2));

--Declare statistics about column_1 and column_2 of tablename.
ALTER TABLE tablename ADD STATISTICS ((column_1, column_2));

--Collect statistics about one or more columns.
ANALYZE tablename;

--Delete statistics about column_1 and column_2 of tablename or their statistics declaration.
ALTER TABLE tablename DELETE STATISTICS ((column_1, column_2));
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> After the statistics are declared for multiple columns by running the **ALTER TABLE** **tablename** **ADD STATISTICS** statement, the system collects the statistics about these columns next time **ANALYZE** is performed on the table or the entire database.
> To collect the statistics, run the **ANALYZE** statement.
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> Use **EXPLAIN** to show the execution plan of each SQL statement. If **rows=10** (the default value, probably indicating that the table has not been analyzed) is displayed in the **SEQ SCAN** output of a table, run the **ANALYZE** statement for this table.
