---
title: Description of SQL Error Codes
summary: Description of SQL Error Codes
author: Zhang Cuiping
date: 2021-03-11
---

# Description of SQL Error Codes

<br/>

As defined by the X/Open and SQL Access Group SQL CAE specification (1992), SQLERROR returns SQLSTATE values. A SQLSTATE value is a five-character string consisting of a two-character SQL error class and a three-character subclass. Five characters containing numeric or uppercase letters indicate the code for various errors or warning conditions. The successful status is identified by 00000. SQLSTATE codes are mostly defined in the SQL standard.

MogDB also follows the SQL standard to return the SQLSTATE value of the error code, as detailed in [Table 1](#table).

**<a id="table">Table 1 </a>**SQLSTATE values for MogDB error codes

| SQLSTATE Value                                               | Description                                                  |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| Class 00 - Successful completion                             |                                                              |
| 00000                                                        | Successful completion (SUCCESSFUL_COMPLETION)                |
| Class 01 - Warning                                           |                                                              |
| 01000                                                        | Warning (WARNING)                                            |
| 0100C                                                        | Dynamic result set returned (DYNAMIC_RESULT_SETS_RETURNED)   |
| 01008                                                        | Implicit zero bit padding (IMPLICIT_ZERO_BIT_PADDING)        |
| 01003                                                        | Null value eliminated in set function (NULL_VALUE_ELIMINATED_IN_SET_FUNCTION) |
| 01007                                                        | Privilege not granted (PRIVILEGE_NOT_GRANTED)                |
| 01006                                                        | Privilege not revoked (PRIVILEGE_NOT_REVOKED)                |
| 01004                                                        | String data right truncation (STRING_DATA_RIGHT_TRUNCATION)  |
| 01P01                                                        | Deprecated feature (DEPRECATED_FEATURE)                      |
| Class 02 - No data (This is a warning class according to the SQL standard.) |                                                              |
| 02000                                                        | No data (NO_DATA)                                            |
| 02001                                                        | No additional dynamic result sets returned (NO_ADDITIONAL_DYNAMIC_RESULT_SETS_RETURNED) |
| Class 03 - SQL statement not completed                       |                                                              |
| 03000                                                        | SQL statement not completed (SQL_STATEMENT_NOT_YET_COMPLETE) |
| Class 08 - Connection exception                              |                                                              |
| 08000                                                        | Connection exception (CONNECTION_EXCEPTION)                  |
| 08003                                                        | Connection does not exist (CONNECTION_DOES_NOT_EXIST)        |
| 08006                                                        | Connection failure (CONNECTION_FAILURE)                      |
| 08001                                                        | SQL connections cannot be established on the  SQL client (SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION) |
| 08004                                                        | SQL server rejects establishment of SQL connections (SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION) |
| 08007                                                        | Unknown transaction resolution (TRANSACTION_RESOLUTION_UNKNOWN) |
| 08P01                                                        | Violation of the protocol (PROTOCOL_VIOLATION)               |
| Class 09 - Triggering action exception                       |                                                              |
| 09000                                                        | Triggering action exception (TRIGGERED_ACTION_EXCEPTION)     |
| Class 0A - Feature not supported                             |                                                              |
| 0A000                                                        | Feature not supported (FEATURE_NOT_SUPPORTED)                |
| 0A100                                                        | Stream not supported (STREAM_NOT_SUPPORTED)                  |
| Class 0B - Invalid transaction initiation                    |                                                              |
| 0B000                                                        | Invalid transaction initiation (INVALID_TRANSACTION_INITIATION) |
| Class 0F - Locator exception                                 |                                                              |
| 0F000                                                        | Locator exception (LOCATOR_EXCEPTION)                        |
| 0F001                                                        | Invalid locator specification (INVALID_LOCATOR_SPECIFICATION) |
| Class 0L - Invalid grantor                                   |                                                              |
| 0L000                                                        | Invalid grantor (INVALID_GRANTOR)                            |
| 0LP01                                                        | Invalid grant operation (INVALID_GRANT_OPERATION)            |
| Class 0P - Invalid role specification                        |                                                              |
| 0P000                                                        | Invalid role specification (INVALID_ROLE_SPECIFICATION)      |
| Class 0Z - Diagnostics exception                             |                                                              |
| 0Z000                                                        | Diagnostics exception (DIAGNOSTICS_EXCEPTION)                |
| 0Z002                                                        | Stacked diagnostics accessed without active handler (STACKED_DIAGNOSTICS_ACCESSED_WITHOUT_ACTIVE_HANDLER) |
| Class 20 - Case not found                                    |                                                              |
| 20000                                                        | Case not found (CASE_NOT_FOUND)                              |
| Class 21 - Cardinality violation                             |                                                              |
| 21000                                                        | Cardinality violation (CARDINALITY_VIOLATION)                |
| Class 22 - Data exception                                    |                                                              |
| 22000                                                        | Data exception (DATA_EXCEPTION)                              |
| 2202E                                                        | Array subscript error (ARRAY_SUBSCRIPT_ERROR)                |
| 22021                                                        | Character not in repertoire (CHARACTER_NOT_IN_REPERTOIRE)    |
| 22008                                                        | Datetime field overflow (DATETIME_FIELD_OVERFLOW)            |
| 22012                                                        | Division by zero (DIVISION_BY_ZERO)                          |
| 22005                                                        | Error in assignment (ERROR_IN_ASSIGNMENT)                    |
| 2200B                                                        | Escape character conflict (ESCAPE_CHARACTER_CONFLICT)        |
| 22022                                                        | Indicator overflow (INDICATOR_OVERFLOW)                      |
| 22015                                                        | Interval field overflow (INTERVAL_FIELD_OVERFLOW)            |
| 2201E                                                        | Invalid argument for logarithm (INVALID_ARGUMENT_FOR_LOGARITHM) |
| 22014                                                        | Invalid argument for NTILE function (INVALID_ARGUMENT_FOR_NTILE_FUNCTION) |
| 22016                                                        | Invalid argument for N value function (INVALID_ARGUMENT_FOR_NTH_VALUE_FUNCTION) |
| 2201F                                                        | Invalid argument for power function (INVALID_ARGUMENT_FOR_POWER_FUNCTION) |
| 2201G                                                        | Invalid argument for width bucket function (INVALID_ARGUMENT_FOR_WIDTH_BUCKET_FUNCTION) |
| 22018                                                        | Invalid character value for CAST (INVALID_CHARACTER_VALUE_FOR_CAST) |
| 22007                                                        | Invalid datetime format (INVALID_DATETIME_FORMAT)            |
| 22019                                                        | Invalid escape character (INVALID_ESCAPE_CHARACTER)          |
| 2200D                                                        | Invalid escape octet (INVALID_ESCAPE_OCTET)                  |
| 22025                                                        | Invalid escape sequence (INVALID_ESCAPE_SEQUENCE)            |
| 22P06                                                        | Nonstandard use of escape character (NONSTANDARD_USE_OF_ESCAPE_CHARACTER) |
| 22010                                                        | Invalid indicator parameter value (INVALID_INDICATOR_PARAMETER_VALUE) |
| 22023                                                        | Invalid parameter value (INVALID_PARAMETER_VALUE)            |
| 2201B                                                        | Invalid regular expression (INVALID_REGULAR_EXPRESSION)      |
| 2201W                                                        | Invalid row count in LIMIT clause (INVALID_ROW_COUNT_IN_LIMIT_CLAUSE) |
| 2201X                                                        | Invalid row count in result set clause (INVALID_ROW_COUNT_IN_RESULT_OFFSET_CLAUSE) |
| 2202H                                                        | Invalid sample parameter (ERRCODE_INVALID_TABLESAMPLE_ARGUMENT) |
| 2202G                                                        | Invalid sample repeat (ERRCODE_INVALID_TABLESAMPLE_REPEAT)   |
| 22009                                                        | Invalid time zone displacement value (INVALID_TIME_ZONE_DISPLACEMENT_VALUE) |
| 2200C                                                        | Invalid use of escape character (INVALID_USE_OF_ESCAPE_CHARACTER) |
| 2200G                                                        | Most specific type mismatch (MOST_SPECIFIC_TYPE_MISMATCH)    |
| 22004                                                        | Null value not allowed (NULL_VALUE_NOT_ALLOWED)              |
| 22002                                                        | Null value cannot be used as the indicator parameter(NULL_VALUE_NO_INDICATOR_PARAMETER) |
| 22003                                                        | Numeric value out of range (NUMERIC_VALUE_OUT_OF_RANGE)      |
| 22017                                                        | DOP value out of range (ERRCODE_DOP_VALUE_OUT_OF_RANGE)      |
| 22026                                                        | String data length mismatch (STRING_DATA_LENGTH_MISMATCH)    |
| 22028                                                        | Regular expression mismatch (ERRCODE_REGEXP_MISMATCH)        |
| 22001                                                        | String data right truncation (STRING_DATA_RIGHT_TRUNCATION)  |
| 22011                                                        | Substring error (SUBSTRING_ERROR)                            |
| 22027                                                        | Trim error (TRIM_ERROR)                                      |
| 22024                                                        | Unterminated C string (UNTERMINATED_C_STRING)                |
| 2200F                                                        | Zero-length character string (ZERO_LENGTH_CHARACTER_STRING)  |
| 22P01                                                        | Floating point exception (FLOATING_POINT_EXCEPTION)          |
| 22P02                                                        | Invalid text representation (INVALID_TEXT_REPRESENTATION)    |
| 22P03                                                        | Invalid binary representation (INVALID_BINARY_REPRESENTATION) |
| 22P04                                                        | Incorrect copy file format (BAD_COPY_FILE_FORMAT)            |
| 22P05                                                        | Untranslatable character (UNTRANSLATABLE_CHARACTER)          |
| 22P06                                                        | Memory query failure (ERRCODE_CACHE_LOOKUP_FAILED)           |
| 22P07                                                        | Failed to read the file (ERRCODE_FILE_READ_FAILED)           |
| 22P08                                                        | Failed to obtain data (ERRCODE_FETCH_DATA_FAILED)            |
| 2200L                                                        | Not XML file (NOT_AN_XML_DOCUMENT)                           |
| 2200M                                                        | Invalid XML file (INVALID_XML_DOCUMENT)                      |
| 2200N                                                        | Invalid XML content (INVALID_XML_CONTENT)                    |
| 2200O                                                        | Invalid XML error information (ERRCODE_INVALID_XML_ERROR_CONTEXT) |
| 2200S                                                        | Invalid XML comment (INVALID_XML_COMMENT)                    |
| 2200T                                                        | Invalid XML processing instruction (INVALID_XML_PROCESSING_INSTRUCTION) |
| 2200X                                                        | RELATION close error (RELATION_CLOSE_ERROR)                  |
| Class 23 - Integrity constraint violation                    |                                                              |
| 23000                                                        | Integrity constraint violation (INTEGRITY_CONSTRAINT_VIOLATION) |
| 23001                                                        | Restrict violation (RESTRICT_VIOLATION)                      |
| 23502                                                        | Not null violation (NOT_NULL_VIOLATION)                      |
| 23503                                                        | Foreign key violation (FOREIGN_KEY_VIOLATION)                |
| 23505                                                        | Uniqueness violation (UNIQUE_VIOLATION)                      |
| 23514                                                        | CHECK violation (CHECK_VIOLATION)                            |
| 23P01                                                        | Exclusion violation (EXCLUSION_VIOLATION)                    |
| Class 24 - Invalid cursor state                              |                                                              |
| 24000                                                        | Invalid cursor state (INVALID_CURSOR_STATE)                  |
| Class 25 - Invalid transaction state                         |                                                              |
| 25000                                                        | Invalid transaction state (INVALID_TRANSACTION_STATE)        |
| 25001                                                        | Active SQL transaction (ACTIVE_SQL_TRANSACTION)              |
| 25002                                                        | Branch transaction already activated (BRANCH_TRANSACTION_ALREADY_ACTIVE) |
| 25008                                                        | Held cursor requires the same isolation level (HELD_CURSOR_REQUIRES_SAME_ISOLATION_LEVEL) |
| 25003                                                        | Inappropriate access mode for branch transaction (INAPPROPRIATE_ACCESS_MODE_FOR_BRANCH_TRANSACTION) |
| 25004                                                        | Inappropriate isolation level for branch transaction (INAPPROPRIATE_ISOLATION_LEVEL_FOR_BRANCH_TRANSACTION) |
| 25005                                                        | No active SQL transaction for branch transaction (NO_ACTIVE_SQL_TRANSACTION_FOR_BRANCH_TRANSACTION) |
| 25006                                                        | Read-only SQL transaction (READ_ONLY_SQL_TRANSACTION)        |
| 25007                                                        | Schema and data statement mixing not supported (SCHEMA_AND_DATA_STATEMENT_MIXING_NOT_SUPPORTED) |
| 25009                                                        | Transaction cannot be started during recovery (ERRCODE_RUN_TRANSACTION_DURING_RECOVERY) |
| 25010                                                        | Transaction ID does not exist (ERRCODE_GXID_DOES_NOT_EXIST)  |
| 25P01                                                        | No active SQL transaction (NO_ACTIVE_SQL_TRANSACTION)        |
| 25P02                                                        | In failed SQL transaction (IN_FAILED_SQL_TRANSACTION)        |
| Class 26 - Invalid SQL statement name                        |                                                              |
| 26000                                                        | Invalid SQL statement name (INVALID_SQL_STATEMENT_NAME)      |
| Class 27 - Triggered data change violation                   |                                                              |
| 27000                                                        | Triggered data change violation (TRIGGERED_DATA_CHANGE_VIOLATION) |
| 27001                                                        | Invalid triggered tuple (ERRCODE_TRIGGERED_INVALID_TUPLE)    |
| Class 28 - Invalid authorization specification               |                                                              |
| 28000                                                        | Invalid authorization specification (INVALID_AUTHORIZATION_SPECIFICATION) |
| 28P01                                                        | Invalid password (INVALID_PASSWORD)                          |
| 28P02                                                        | Initial password not changed (INITIAL_PASSWORD_NOT_MODIFIED) |
| Class 29 - Invalid or unexpected state                       |                                                              |
| 29P06                                                        | Invalid cache plan (INVALID_CACHE_PLAN)                      |
| Class 2B - The dependent privilege descriptor still exists   |                                                              |
| 2B000                                                        | The dependent privilege descriptor still exists (DEPENDENT_PRIVILEGE_DESCRIPTORS_STILL_EXIST) |
| 2BP01                                                        | The dependent object still exists (DEPENDENT_OBJECTS_STILL_EXIST) |
| Class 2D - Invalid transaction termination                   |                                                              |
| 2D000                                                        | Invalid transaction termination (INVALID_TRANSACTION_TERMINATION) |
| Class 2F - SQL process exception                             |                                                              |
| 2F000                                                        | SQL process exception (SQL_ROUTINE_EXCEPTION)                |
| 2F005                                                        | No return statement during function execution (FUNCTION_EXECUTED_NO_RETURN_STATEMENT) |
| 2F002                                                        | Modifying SQL data not permitted (MODIFYING_SQL_DATA_NOT_PERMITTED) |
| 2F003                                                        | Prohibited SQL statement attempted (PROHIBITED_SQL_STATEMENT_ATTEMPTED) |
| 2F004                                                        | Reading SQL data not permitted (READING_SQL_DATA_NOT_PERMITTED) |
| Class 34 - Invalid cursor name                               |                                                              |
| 34000                                                        | Invalid cursor name (INVALID_CURSOR_NAME)                    |
| Class 38 - External process exception                        |                                                              |
| 38000                                                        | External process exception (EXTERNAL_ROUTINE_EXCEPTION)      |
| 38001                                                        | Containing SQL not permitted (CONTAINING_SQL_NOT_PERMITTED)  |
| 38002                                                        | Modifying SQL data not permitted (MODIFYING_SQL_DATA_NOT_PERMITTED) |
| 38003                                                        | Prohibited SQL statement attempted (PROHIBITED_SQL_STATEMENT_ATTEMPTED) |
| 38004                                                        | SQL data read not permitted (READING_SQL_DATA_NOT_PERMITTED) |
| Class 39 - External process invocation exception             |                                                              |
| 39000                                                        | External process invocation exception (EXTERNAL_ROUTINE_INVOCATION_EXCEPTION) |
| 39001                                                        | Invalid SQLSTATE value returned (INVALID_SQLSTATE_RETURNED)  |
| 39004                                                        | Null value not allowed (NULL_VALUE_NOT_ALLOWED)              |
| 39P01                                                        | Trigger protocol violated (TRIGGER_PROTOCOL_VIOLATED)        |
| 39P02                                                        | SRF protocol violated (SRF_PROTOCOL_VIOLATED)                |
| Class 3B - Savepoint exception                               |                                                              |
| 3B000                                                        | Savepoint exception (SAVEPOINT_EXCEPTION)                    |
| 3B001                                                        | Invalid savepoint specification (INVALID_SAVEPOINT_SPECIFICATION) |
| Class 3D - Invalid database name                             |                                                              |
| 3D000                                                        | Invalid database name (INVALID_CATALOG_NAME)                 |
| Class 3F - Invalid schema name                               |                                                              |
| 3F000                                                        | Invalid schema name (INVALID_SCHEMA_NAME)                    |
| Class 40 - Transaction rollback                              |                                                              |
| 40000                                                        | Transaction rollback (TRANSACTION_ROLLBACK)                  |
| 40002                                                        | Transaction integrity constraint violation (TRANSACTION_INTEGRITY_CONSTRAINT_VIOLATION) |
| 40001                                                        | Serialization failure (SERIALIZATION_FAILURE)                |
| 40003                                                        | Statement completion unknown (STATEMENT_COMPLETION_UNKNOWN)  |
| 40P01                                                        | Deadlock detected (DEADLOCK_DETECTED)                        |
| Class 42 - Syntax error or access rule violation             |                                                              |
| 42000                                                        | Syntax error or access rule violation (SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION) |
| 42601                                                        | syntax error (SYNTAX_ERROR)                                  |
| 42501                                                        | Insufficient privilege (INSUFFICIENT_PRIVILEGE)              |
| 42846                                                        | Type conversion cannot be performed 无法进行类型转换(CANNOT_COERCE) |
| 42803                                                        | Grouping error (GROUPING_ERROR)                              |
| 42P20                                                        | Windowing error (WINDOWING_ERROR)                            |
| 42P19                                                        | Invalid recursion (INVALID_RECURSION)                        |
| 42830                                                        | Invalid foreign key (INVALID_FOREIGN_KEY)                    |
| 42602                                                        | Invalid name (INVALID_NAME)                                  |
| 42622                                                        | Too long name (NAME_TOO_LONG)                                |
| 42939                                                        | Reserved name (RESERVED_NAME)                                |
| 42804                                                        | Data type mismatch (DATATYPE_MISMATCH)                       |
| 42P18                                                        | Indeterminate data type (INDETERMINATE_DATATYPE)             |
| 42P21                                                        | Collation mismatch (COLLATION_MISMATCH)                      |
| 42P22                                                        | Indeterminate collation (INDETERMINATE_COLLATION)            |
| 42P23                                                        | Partition error (ERRCODE_PARTITION_ERROR)                    |
| 42P24                                                        | Invalid attribute value (ERRCODE_INVALID_ATTRIBUTE)          |
| 42P25                                                        | Invalid aggregation function (ERRCODE_INVALID_AGG)           |
| 42P26                                                        | Resource pool error (ERRCODE_RESOURCE_POOL_ERROR)            |
| 42P27                                                        | Parent plan not found (ERRCODE_PLAN_PARENT_NOT_FOUND)        |
| 42P28                                                        | Update conflict (ERRCODE_MODIFY_CONFLICTS)                   |
| 42P29                                                        | Distribution error (ERRCODE_DISTRIBUTION_ERROR)              |
| 42809                                                        | Incorrect object type (WRONG_OBJECT_TYPE)                    |
| 42703                                                        | Undefined column (UNDEFINED_COLUMN)                          |
| 42883                                                        | Undefined function (UNDEFINED_FUNCTION)                      |
| 42P01                                                        | Undefined table (UNDEFINED_TABLE)                            |
| 42P02                                                        | Undefined parameter (UNDEFINED_PARAMETER)                    |
| 42704                                                        | Undefined object (UNDEFINED_OBJECT)                          |
| 42701                                                        | Duplicate column (DUPLICATE_COLUMN)                          |
| 42P03                                                        | Duplicate cursor (DUPLICATE_CURSOR)                          |
| 42P04                                                        | Duplicate database (DUPLICATE_DATABASE)                      |
| 42723                                                        | Duplicate function (DUPLICATE_FUNCTION)                      |
| 42P05                                                        | Duplicate prepared statement (DUPLICATE_PREPARED_STATEMENT)  |
| 42P06                                                        | Duplicate schema (DUPLICATE_SCHEMA)                          |
| 42P07                                                        | Duplicate table (DUPLICATE_TABLE)                            |
| 42712                                                        | Duplicate alias (DUPLICATE_ALIAS)                            |
| 42710                                                        | Duplicate object (DUPLICATE_OBJECT)                          |
| 42702                                                        | Ambiguous column (AMBIGUOUS_COLUMN)                          |
| 42725                                                        | Ambiguous function (AMBIGUOUS_FUNCTION)                      |
| 42P08                                                        | Ambiguous parameter (AMBIGUOUS_PARAMETER)                    |
| 42P09                                                        | Ambiguous alias (AMBIGUOUS_ALIAS)                            |
| 42P10                                                        | Invalid column reference (INVALID_COLUMN_REFERENCE)          |
| 42611                                                        | Invalid column definition (INVALID_COLUMN_DEFINITION)        |
| 42P11                                                        | Invalid cursor definition (INVALID_CURSOR_DEFINITION)        |
| 42P12                                                        | Invalid database definition (INVALID_DATABASE_DEFINITION)    |
| 42P13                                                        | Invalid function definition (INVALID_FUNCTION_DEFINITION)    |
| 42P14                                                        | Invalid prepared statement definition (INVALID_PREPARED_STATEMENT_DEFINITION) |
| 42P15                                                        | Invalid schema definition (INVALID_SCHEMA_DEFINITION)        |
| 42P16                                                        | Invalid table definition (INVALID_TABLE_DEFINITION)          |
| 42P17                                                        | Invalid object definition (INVALID_OBJECT_DEFINITION)        |
| Class 44 - WITH CHECK option violation                       |                                                              |
| 44000                                                        | WITH CHECK option violation (WITH_CHECK_OPTION_VIOLATION)    |
| Class 53 - Insufficient resources                            |                                                              |
| 53000                                                        | Insufficient resources (INSUFFICIENT_RESOURCES)              |
| 53100                                                        | The disk space is fully occupied (DISK_FULL)                 |
| 53200                                                        | Out of memory (OUT_OF_MEMORY)                                |
| 53300                                                        | Too many connections (TOO_MANY_CONNECTIONS)                  |
| 53400                                                        | Configuration limit exceeded (CONFIGURATION_LIMIT_EXCEEDED)  |
| Class 54 - Program limit exceeded                            |                                                              |
| 54000                                                        | Program limit exceeded (PROGRAM_LIMIT_EXCEEDED)              |
| 54001                                                        | Too complex statement (STATEMENT_TOO_COMPLEX)                |
| 54011                                                        | Too many columns (TOO_MANY_COLUMNS)                          |
| 54023                                                        | Too many parameters (TOO_MANY_ARGUMENTS)                     |
| Class 55 - The object is not in the prerequisite state       |                                                              |
| 55000                                                        | The object is not in the prerequisite state (OBJECT_NOT_IN_PREREQUISITE_STATE) |
| 55006                                                        | The object is in use (OBJECT_IN_USE)                         |
| 55P02                                                        | Runtime parameter cannot be modified (CANT_CHANGE_RUNTIME_PARAM) |
| 55P03                                                        | Lock unavailable (LOCK_NOT_AVAILABLE)                        |
| Class 57 - Operator intervention                             |                                                              |
| 57000                                                        | Operator intervention (OPERATOR_INTERVENTION)                |
| 57014                                                        | Query canceled (QUERY_CANCELED)                              |
| 57015                                                        | Internal query canceled (QUERY_INTERNAL_CANCEL)              |
| 57P01                                                        | System closed by the admin (ADMIN_SHUTDOWN)                  |
| 57P02                                                        | Crash shutdown (CRASH_SHUTDOWN)                              |
| 57P03                                                        | Connection not allowed (CANNOT_CONNECT_NOW)                  |
| 57P04                                                        | Dropped database (DATABASE_DROPPED)                          |
| Class 58 - System error (MogDB internal error)               |                                                              |
| 58000                                                        | System error (SYSTEM_ERROR)                                  |
| 58030                                                        | IO error (IO_ERROR)                                          |
| 58P01                                                        | Undefined file (UNDEFINED_FILE)                              |
| 58P02                                                        | Duplicate file (DUPLICATE_FILE)                              |
| Class F0 - Configuration file error                          |                                                              |
| F0000                                                        | Configuration file error (CONFIG_FILE_ERROR)                 |
| F0001                                                        | Lock file exists (LOCK_FILE_EXISTS)                          |
| Class HV - External data error (SQL/MED)                     |                                                              |
| HV000                                                        | External data error (FDW_ERROR)                              |
| HV005                                                        | No name found (FDW_COLUMN_NAME_NOT_FOUND)                    |
| HV002                                                        | Dynamic parameter value required (FDW_DYNAMIC_PARAMETER_VALUE_NEEDED) |
| HV010                                                        | Function sequence error (FDW_FUNCTION_SEQUENCE_ERROR)        |
| HV021                                                        | Inconsistent description information (FDW_INCONSISTENT_DESCRIPTOR_INFORMATION) |
| HV024                                                        | Invalid attribute value (FDW_INVALID_ATTRIBUTE_VALUE)        |
| HV007                                                        | Invalid column name (FDW_INVALID_COLUMN_NAME)                |
| HV008                                                        | Invalid column number (FDW_INVALID_COLUMN_NUMBER)            |
| HV004                                                        | Invalid data type (FDW_INVALID_DATA_TYPE)                    |
| HV006                                                        | Invalid data type descriptor (FDW_INVALID_DATA_TYPE_DESCRIPTORS) |
| HV091                                                        | Invalid filed identifier (FDW_INVALID_DESCRIPTOR_FIELD_IDENTIFIER) |
| HV00B                                                        | Invalid handling (FDW_INVALID_HANDLE)                        |
| HV00C                                                        | Invalid index option (FDW_INVALID_OPTION_INDEX)              |
| HV00D                                                        | Invalid option name (FDW_INVALID_OPTION_NAME)                |
| HV090                                                        | Invalid string length or buffer length (FDW_INVALID_STRING_LENGTH_OR_BUFFER_LENGTH) |
| HV00A                                                        | Invalid string format (FDW_INVALID_STRING_FORMAT)            |
| HV009                                                        | Invalid use of null pointer (FDW_INVALID_USE_OF_NULL_POINTER) |
| HV014                                                        | Too many handles (FDW_TOO_MANY_HANDLES)                      |
| HV001                                                        | Out of memory (FDW_OUT_OF_MEMORY)                            |
| HV00P                                                        | No schema (FDW_NO_SCHEMAS)                                   |
| HV00J                                                        | No option name found (FDW_OPTION_NAME_NOT_FOUND)             |
| HV00K                                                        | Reply handle (FDW_REPLY_HANDLE)                              |
| HV00Q                                                        | No schema found (FDW_SCHEMA_NOT_FOUND)                       |
| HV00R                                                        | No table found (FDW_TABLE_NOT_FOUND)                         |
| HV00S                                                        | Incorrect server type (FDW_INVALID_SERVER_TYPE)              |
| HV00L                                                        | Unable to establish execution (FDW_UNABLE_TO_CREATE_EXECUTION) |
| HV00M                                                        | Unable to create reply (FDW_UNABLE_TO_CREATE_REPLY)          |
| HV00N                                                        | Unable to establish connection (FDW_UNABLE_TO_ESTABLISH_CONNECTION) |
| Class P0 - PL/pgSQL error                                    |                                                              |
| P0000                                                        | PLPGSQL error (PLPGSQL_ERROR)                                |
| P0001                                                        | Raise exception (RAISE_EXCEPTION)                            |
| P0002                                                        | No data found (NO_DATA_FOUND)                                |
| P0003                                                        | Too many rows (TOO_MANY_ROWS)                                |
| P0004                                                        | FORALL requires DML operation (FORALL_NEED_DML)              |
| Class XX - Interval error                                    |                                                              |
| XX000                                                        | Interval error (INTERNAL_ERROR)                              |
| XX001                                                        | Data corrupted (DATA_CORRUPTED)                              |
| XX002                                                        | Index corrupted (INDEX_CORRUPTED)                            |
| XX003                                                        | Remote stream socket closed (STREAM_REMOTE_CLOSE_SOCKET)     |
| XX004                                                        | Unrecognized node type (ERRCODE_UNRECOGNIZED_NODE_TYPE)      |
| XX005                                                        | Unexpected null value (ERRCODE_UNEXPECTED_NULL_VALUE)        |
| XX006                                                        | Unexpected node state (ERRCODE_UNEXPECTED_NODE_STATE)        |
| XX007                                                        | Null JUNK attribute (ERRCODE_NULL_JUNK_ATTRIBUTE)            |
| XX008                                                        | Inconsistent optimizer state (ERRCODE_OPTIMIZER_INCONSISTENT_STATE) |
| XX009                                                        | Duplicate query ID (ERRCODE_STREAM_DUPLICATE_QUERY_ID)       |
| XX010                                                        | Invalid buffer (INVALID_BUFFER)                              |
| XX011                                                        | Invalid buffer reference (INVALID_BUFFER_REFERENCE)          |
| XX012                                                        | Node ID mismatch (ERRCODE_NODE_ID_MISSMATCH)                 |
| XX013                                                        | XID base cannot be modified (CANNOT_MODIFY_XIDBASE)          |
| XX014                                                        | Unexpected TOAST table data damage (UNEXPECTED_CHUNK_VALUE)  |
| Class YY - SQL retry error                                   |                                                              |
| YY001                                                        | Connection reset by peer (CONNECTION_RESET_BY_PEER)          |
| YY002                                                        | Stream connection reset by peer (STREAM_CONNECTION_RESET_BY_PEER) |
| YY003                                                        | Lock wait timeout (LOCK_WAIT_TIMEOUT)                        |
| YY004                                                        | Connection timed out (CONNECTION_TIMED_OUT)                  |
| YY005                                                        | Set query error (SET_QUERY_ERROR)                            |
| YY006                                                        | Out of logical memory (OUT_OF_LOGICAL_MEMORY)                |
| YY007                                                        | SCTP memory allocated (SCTP_MEMORY_ALLOC)                    |
| YY008                                                        | SCTP no data in buffer (SCTP_NO_DATA_IN_BUFFER)              |
| YY009                                                        | SCTP release memory close (SCTP_RELEASE_MEMORY_CLOSE)        |
| YY010                                                        | SCTP and TCP disconnected (SCTP_TCP_DISCONNECT)              |
| YY011                                                        | SCTP disconnected (SCTP_DISCONNECT)                          |
| YY012                                                        | SCTP remote close (SCTP_REMOTE_CLOSE)                        |
| YY013                                                        | SCTP wait poll unknown (SCTP_WAIT_POLL_UNKNOW)               |
| YY014                                                        | Invalid snapshot (SNAPSHOT_INVALID)                          |
| YY015                                                        | Connection receive error (ERRCODE_CONNECTION_RECEIVE_WRONG)  |
| Class SI - SPI interface error                               |                                                              |
| SP000                                                        | SPI interface error (ERRCODE_SPI_ERROR)                      |
| SP001                                                        | SPI connection failure (ERRCODE_SPI_CONNECTION_FAILURE)      |
| SP002                                                        | SPI finish failure (ERRCODE_SPI_FINISH_FAILURE)              |
| SP003                                                        | SPI preparation failure (ERRCODE_SPI_PREPARE_FAILURE)        |
| SP004                                                        | SPI cursor open failure (ERRCODE_SPI_CURSOR_OPEN_FAILURE)    |
| SP005                                                        | SPI execution failure (ERRCODE_SPI_EXECUTE_FAILURE)          |
| SP006                                                        | Improper call of SPI (ERRORCODE_SPI_IMPROPER_CALL)           |
| RB class - RBTree error                                      |                                                              |
| RB001                                                        | RBTree invalid node state (RBTREE_INVALID_NODE_STATE)        |
| RB002                                                        | RBTree invalid iterator order (RBTREE_INVALID_ITERATOR_ORDER) |
| Class PD - PL debug server internal error                    |                                                              |
| D0000                                                        | PL debug server internal error (PLDEBUGGER_INTERNAL_ERROR)   |
| D0001                                                        | Duplicate breakpoint (DUPLICATE_BREAKPOINT)                  |
| D0002                                                        | Hash function is not initialized (FUNCTION_HASH_IS_NOT_INITIALIZED) |
| D0003                                                        | Breakpoint is not present (BREAKPOINT_IS_NOT_PRESENT)        |
| D0004                                                        | Debug server already attached (DEBUG_SERVER_ALREADY_IS_ATTACHED) |
| D0005                                                        | Debug server not attached (DEBUG_SERVER_NOT_ATTACHED)        |
| D0006                                                        | Debug server already in synchronization (DEBUG_SERVER_ALREADY_IN_SYNC) |
| D0007                                                        | Debug target server not in synchronization (DEBUG_TARGET_SERVERS_NOT_IN_SYNC) |
| D0008                                                        | Target server already in synchronization (TARGET_SERVER_ALREADY_IN_SYNC) |
| D0009                                                        | The variable does not exist (NON_EXISTANT_VARIABLE)          |
| D0010                                                        | Invalid target session ID (INVALID_TARGET_SESSION_ID)        |
| D0011                                                        | Invalid operation (INVALID_OPERATION)                        |
| D0012                                                        | Maximum number of debug sessions reached (MAXIMUM_NUMBER_OF_DEBUG_SESSIONS_REACHED) |
| D0013                                                        | Maximum number of breakpoints reached (MAXIMUM_NUMBER_OF_BREAKPOINTS_REACHED) |
| LL - Logical decoding error                                  |                                                              |
| LL001                                                        | Logical decoding error (LOGICAL_DECODE_ERROR)                |
| LL002                                                        | Hash table query error (RELFILENODEMAP)                      |
