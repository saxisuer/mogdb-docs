---
title: GAUSS-05201- GAUSS-05300
summary: GAUSS-05201- GAUSS-05300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05201- GAUSS-05300

<br/>

## GAUSS-05201- GAUSS-05210

<br/>

GAUSS-05201: "unrecognize LOCKMODE type."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05202: "invalid address for the compute pool: %s"

SQLSTATE: XX005

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05203: "invalid message format, please update to the same version"

SQLSTATE: 08P01

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05204: "cooperation analysis: relation does not exist."

SQLSTATE: 42704

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05205: "cooperation analysis: relation kind is not supported."

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05206: "cooperation analysis: colomn information does not match."

SQLSTATE: 42809

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05207: "cooperation analysis: unrecognized check result type: %d"

SQLSTATE: 38000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05208: "cooperation analysis: invalid size of snapshot"

SQLSTATE: 22000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05209: "Failed to send queryid to %s before COMMIT command(1PC)"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05210: "failed to notify node %u to commit"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05211- GAUSS-05220

<br/>

GAUSS-05211: "failed to receice response from node %u after notify commit"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05212: "failed to send commit csn to node %u"

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05213: "Failed to get valid node id from node definitions"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05214: "invalid node_list, coor_num:%d, datanode_num:%d"

SQLSTATE: 22026

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05215: "Failed to send COMMIT/ROLLBACK on nodes: %s.Failed to COMMIT/ROLLBACK the transaction on nodes: %s."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05216: "unsupported proc in single node mode."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05217: "pooler: Failed to create agent, number of agent reaches MaxAgentCount: %d"

SQLSTATE: 53300

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05218: "agent cannot alloc memory."

SQLSTATE: 53200

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05219: "pooler: connect failed, invaild argument."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05220: "pooler: Failed to init agent, number of agent reaches MaxAgentCount: %d"

SQLSTATE: 53300

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05221- GAUSS-05230

<br/>

GAUSS-05221: "pooler: could not find node of node[%d], oid[%u], needCreateArrayLen[%d], loopIndex[%d], i[%d], j[%d], isNull[%d]"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05222: "pooler: Failed to get_nodeInfo_from_matric: needCreateNodeArray is null."

SQLSTATE: 01000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05223: "pooler: Failed to get_slave_datanode_oid: can't find slave nodes."

SQLSTATE: 01000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05224: "invalid %s node number: %d, max_value is: %d."

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05225: "pooler: The node(oid:%u) has no available slot, the number of slot in use reaches upper limit!"

SQLSTATE: 53300

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05226: "pooler: Node(oid:%u) has been removed or altered"

SQLSTATE: 01000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05227: "pooler: Communication failure, failed to send session commands or invalid incoming data, error count: %d."

SQLSTATE: 08000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05228: "pooler: failed to create connections in parallel mode, due to failover, pending"

SQLSTATE: 08006

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05229: "pooler: failed to create connections in parallel mode, Error Message: %s"

SQLSTATE: 08006

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05230: "pooler: agent_release_connections: agent is null"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05231- GAUSS-05240

<br/>

GAUSS-05231: "malloc memory failed in dynamic loader."

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05232: "latch already owned by %lu, my pid %lu"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05233: "could not reattach to shared memory (key=%d): %m"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05234: "reattaching to shared memory returned unexpected address"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05235: "failed to release reserved memory region : error code %lu"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05236: "could not reattach to shared memory : error code %lu"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05237: "Init dictionary failed for invalid inputs"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05238: "multiple FilePath parameters"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05239: "Text search for Danish is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05240: "Text search for Dutch is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05241- GAUSS-05250

<br/>

GAUSS-05241: "Text search for Finnish is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05242: "Text search for French is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05243: "Text search for German is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05244: "Text search for Hungarian is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05245: "Text search for Italian is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05246: "Text search for Norwegian is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05247: "Text search for Porter is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05248: "Text search for Portuguese is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05249: "Text search for Spanish is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05250: "Text search for Swedish is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05251- GAUSS-05260

<br/>

GAUSS-05251: "Text search for Romanian is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05252: "Text search for Russian is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05253: "Text search for Turkish is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05254: "FilePath parameter should be with DictFile/AffFile/StopWords"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05255: "multiple Synonyms parameters"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05256: "multiple CaseSensitive parameters"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05257: "too many lexemes in thesaurus entry"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05258: "Chinese is not supported by ASCII!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05259: "pound parser only support UTF8/GBK/ASCII encoding"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05260: "Zhparser is not supported!"

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05261- GAUSS-05270

<br/>

GAUSS-05261: "the parameter 'len' should not be negative."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05262: "the parameter 'len' is out of limit."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05263: "The name of internal dictionary file is too long"

SQLSTATE: 42622

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05264: "FilePath must set an absolute path followed by 'file://'."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05265: "FilePath must set an absolute path followed by 'file:///'."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05266: "FilePath must set an obs path info followed by 'obs://'."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05267: "FilePath must start with 'file://' or 'obs://': %s"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05268: "The name of dictionary file is too long"

SQLSTATE: 42622

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05269: "Dictionary file '%s' does not exist or cannot access the directory."

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05270: "Dictionary File '%s' does not have READ permission."

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05271- GAUSS-05280

<br/>

GAUSS-05271: "TEXT SEARCH is not yet supported."

SQLSTATE: 0A000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05272: "Dictionary file name value '%s' contain no %c"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05273: "Copy dictionary file '%s' failed: %m"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05274: "Send dictionary file to node fail: %m, command %s"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05275: "Send dictionary file to backup fail: %m, command %s"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05276: "Delete internal dictionary files failed for invalid inputs"

SQLSTATE: 58000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05277: "unrecognized regis node type: %u"

SQLSTATE: XX004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05278: "affix flag '%s' is out of range"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05279: "invalid affix flag '%s'"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05280: "invalid character in affix flag '%s'"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05281- GAUSS-05290

<br/>

GAUSS-05281: "unrecognized type of Conf->flagMode: %d"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05282: "invalid affix flag '%s' with 'long' flag value"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05283: "unrecognized state in parse_ooaffentry: %d"

SQLSTATE: XX000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05284: "invalid affix alias '%s'"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05285: "Ispell dictionary supports only 'default', 'long', and 'num' flag value"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05286: "number of aliases exceeds specified number %d"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05287: "affix file contains both old-style and new-style commands"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05288: "invalid affix alias '%s', max affix value:%d"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05289: "invalid len: %d"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05290: "localized string format value too long"

SQLSTATE: 22008

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-05291- GAUSS-05300

<br/>

GAUSS-05291: "The input NUMCacheEntry is invalid, which is Null."

SQLSTATE: 22004

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05292: "input format of numeric shouldn't bigger than length of buffer."

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05293: "invalid data."

SQLSTATE: D0011

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05294: "invalid data for 'year = 0' ,value must be between -4712 and 9999, and not be 0"

SQLSTATE: 22008

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05295: "value '%s' is out of range for type %s"

SQLSTATE: 22003

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05296: "could not format inet value"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05297: "invalid input syntax for %s: '%s'"

SQLSTATE: 22P02

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05298: "could not convert format string from UTF-8: error code %lu"

SQLSTATE: 22021

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05299: "strftime(%s) failed: %m"

SQLSTATE: 22023

Description: Internal system error.

Solution:Contact technical support.

GAUSS-05300: "function '%s' does not exist xxxx"

SQLSTATE: 42883

Description: Internal system error.

Solution:Contact technical support.
