---
title: GAUSS-02801 - GAUSS-02900
summary: GAUSS-02801 - GAUSS-02900
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-02801 - GAUSS-02900

<br/>

## GAUSS-02801 - GAUSS-02810

<br/>

GAUSS-02801: "string is too long for tsvector (%d bytes, max %d bytes)"

SQLSTATE: 54000

Description: The string is too long for the tsvector type.

Solution: Shorten the length of the string to a proper one.

GAUSS-02802: "unrecognized operator: %d"

SQLSTATE: XX000

Description: Failed to recognize the **tsvector** operator.

Solution: Use only !, |, or & as the input operator.

GAUSS-02803: "SPI_prepare('%s') failed"

SQLSTATE: XX000

Description: A **NULL** value is returned after **SPI_prepare** is called.

Solution: Internal error. Contact technical support.

GAUSS-02804: "SPI_cursor_open('%s') failed"

SQLSTATE: XX000

Description: A **NULL** value is returned after **SPI_cursor_open** is called.

Solution: Internal error. Contact technical support.

GAUSS-02806: "tsvector_update_trigger: not fired by trigger manager"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02807: "tsvector_update_trigger: must be fired for row"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02808: "tsvector_update_trigger: must be fired BEFORE event"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02809: "tsvector_update_trigger: must be fired for INSERT or UPDATE"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02810: "tsvector_update_trigger: arguments must be tsvector_field, ts_config, text_field1, …)"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02811 - GAUSS-02820

<br/>

GAUSS-02811: “tsvector column '%s' does not exist”

SQLSTATE: 42703

Description: The selected column does not exist.

Solution: Check whether the selected column exists.

GAUSS-02812: “column '%s' is not of tsvector type”

SQLSTATE: 42804

Description: The column is not of the tsvector type.

Solution: Check whether the column type is tsvector.

GAUSS-02813: “configuration column '%s' does not exist”

SQLSTATE: 42703

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02814: “column '%s' is not of regconfig type”

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02815: “configuration column '%s' must not be null”

SQLSTATE: 22004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02816: “text search configuration name '%s' must be schema-qualified”

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02817: “column '%s' is not of a character type”

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02818: “tsvector_update_trigger: %d returned by SPI_modifytuple”

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02819: “gtsvector_in not implemented”

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02820: “invalid input syntax for type numeric: '%s'”

SQLSTATE: 22P02

Description: The numeric input is invalid.

Solution: Check whether the data contains invalid numeric input.

<br/>

## GAUSS-02821 - GAUSS-02830

<br/>

GAUSS-02821: "invalid length in external 'numeric' value"

SQLSTATE: 22P03

Description: The numeric input is invalid.

Solution: Check whether the data contains invalid numeric input. For the valid format of the numeric type, see en-us_topic_0237121926.html.

GAUSS-02822: "invalid sign in external 'numeric' value"

SQLSTATE: 22P03

Description: The numeric input is invalid.

Solution: Check whether the data contains invalid numeric input. For the valid format of the numeric type, see en-us_topic_0237121926.html.

GAUSS-02823: "invalid digit in external 'numeric' value"

SQLSTATE: 22P03

Description: The numeric input is invalid.

Solution: Check whether the data contains invalid numeric input. For the valid format of the numeric type, see en-us_topic_0237121926.html.

GAUSS-02824: "NUMERIC precision %d must be between 1 and %d"

SQLSTATE: 22023

Description: The **numeric** precision is beyond the range.

Solution: Modify the input value according to the precision range defined in the error message.

GAUSS-02825: "NUMERIC scale %d must be between 0 and precision %d"

SQLSTATE: 22023

Description: The numeric scale is beyond the range.

Solution: Modify the input value according to the precision range defined in the error message.

GAUSS-02826: "invalid NUMERIC type modifier"

SQLSTATE: 22023

Description: The numeric input is invalid.

Solution: Check whether the data contains invalid numeric input. For the valid format of the numeric type, see en-us_topic_0237121926.html.

GAUSS-02827: "count must be greater than zero"

SQLSTATE: 2201G

Description: The value of parameter **count** of the **width_bucket** function must be greater than zero.

Solution: Ensure that the value of parameter **count** of the **width_bucket** function is greater than zero.

GAUSS-02828: "operand, lower bound, and upper bound cannot be NaN"

SQLSTATE: 2201G

Description: The operand and upper and lower limits for the **width_bucket** function cannot be **NaN**.

Solution: Ensure that operand and upper and lower limits for the **width_bucket** function are not **NaN**.

GAUSS-02829: "lower bound cannot equal upper bound"

SQLSTATE: 2201G

Description: The upper and lower limits of the **width_bucket** function must be different.

Solution: Ensure that the upper and lower limits for the **width_bucket** function are different.

GAUSS-02830: "value overflows numeric format"

SQLSTATE: 22003

Description: The numeric factorial overflows.

Solution: Check whether the data input for the program is too large. If not, take measures to solve the problem at the application layer.

<br/>

## GAUSS-02831 - GAUSS-02840

<br/>

GAUSS-02831: "zero raised to a negative power is undefined"

SQLSTATE: 2201F

Description: Failed to calculate zero raised to the negative power.

Solution: Check whether the data input contains calculations of zero raised to the negative power.

GAUSS-02832: "a negative number raised to a non-integer power yields a complex result"

SQLSTATE: 2201F

Description:Internal system error.

Solution: Contact technical support. Alternatively, check the statement or data.

GAUSS-02833: "cannot convert NaN to integer"

SQLSTATE: 0A000

Description: Failed to convert the numeric NaN to the integer type.

Solution: Do not convert NaN to the integer type.

GAUSS-02834: "integer out of range"

SQLSTATE: 22003

Description: An integer type is out of range.

Solution: Check whether the integer type is out of range in the statement. Alternatively, convert the integer type to the numeric type.

GAUSS-02835: "cannot convert NaN to bigint"

SQLSTATE: 0A000

Description: Failed to convert the numeric NaN to the bigint type.

Solution: Do not convert NaN to the bigint type.

GAUSS-02836: "bigint out of range"

SQLSTATE: 22003

Description: A bigint type is out of range.

Solution: Check whether the bigint type is out of range in the statement. Alternatively, convert the bigint type to the numeric type.

GAUSS-02837: "cannot convert NaN to smallint"

SQLSTATE: 0A000

Description: A conversion error occurs.

Solution: Check the original data.

GAUSS-02838: "smallint out of range"

SQLSTATE: 22003

Description: Data is out of range.

Solution: Contact technical support. Alternatively, check the statement or data.

GAUSS-02839: "cannot convert NaN to tinyint"

SQLSTATE: 0A000

Description: A conversion error occurs.

Solution: Check the original data.

GAUSS-02840: "tinyint out of range"

SQLSTATE: 22003

Description: Data is out of range.

Solution: Check the original data volume.

<br/>

## GAUSS-02841 - GAUSS-02850

<br/>

GAUSS-02841: "expected 3-element numeric array"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02842: "expected 2-element numeric array"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02843: "expected 2-element int8 array"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02844: "numeric field overflow"

SQLSTATE: 22003

Description: Data is out of range.

Solution: Check the original data volume.

GAUSS-02845: "invalid input syntax for type double precision: '%s'"

SQLSTATE: 22P02

Description: The data type is incorrect.

Solution: Contact technical support. Alternatively, check the statement or data.

GAUSS-02846: "division by zero"

SQLSTATE: 22012

Description: The divisor is 0.

Solution: Check the SQL statement. Modify it if its divisor may be zero.

GAUSS-02847: "cannot take square root of a negative number"

SQLSTATE: 2201F

Description: Calculating the square root of a negative number is not allowed.

Solution: Contact technical support. Alternatively, check the statement or data.

GAUSS-02849: "cannot take logarithm of zero"

SQLSTATE: 2201E

Description: The logarithm parameter cannot be set to zero.

Solution: Contact technical support. Alternatively, check the statement or data.

<br/>

## GAUSS-02851 - GAUSS-02860

<br/>

GAUSS-02851: "invalid input syntax for integer: '%s'"

SQLSTATE: 22P02

Description: Incorrect source string.

Solution: Check the data source.

GAUSS-02853: "OID out of range"

SQLSTATE: 22003

Description: Data is out of range.

Solution: Check the metadata.

GAUSS-02854: "step size cannot equal zero"

SQLSTATE: 22023

Description: Incorrect statement.

Solution: Modify the SQL statement. The interval cannot be zero.

GAUSS-02855: "argument of ntile must be greater than zero"

SQLSTATE: 22014

Description: Internal system error. The value of the parameter expression in the obtained frame must be greater than zero.

Solution: Contact technical support.

GAUSS-02856: "argument of nth_value must be greater than zero"

SQLSTATE: 22016

Description: Internal system error. For a frame, its values, excluding its first value, should be greater than zero.

Solution: Contact technical support.

GAUSS-02857: "type %u is not a range type"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02858: "range constructor flags argument must not be NULL"

SQLSTATE: 22000

Description: Incorrect SQL statement.

Solution: Modify the statement.

GAUSS-02859: "range types do not match"

SQLSTATE: 42804

Description: Incorrect SQL statement.

Solution: Modify the statement.

GAUSS-02860: "result of range difference would not be contiguous"

SQLSTATE: 22000

Description: Incorrect SQL statement.

Solution: Modify the statement.

<br/>

## GAUSS-02861 - GAUSS-02870

<br/>

GAUSS-02861: "unexpected case in range_minus"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02862: "result of range union would not be contiguous"

SQLSTATE: 22000

Description: Incorrect SQL statement.

Solution: Modify the statement.

GAUSS-02863: "could not identify a hash function for type %s"

SQLSTATE: 42883

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02864: "range lower bound must be less than or equal to range upper bound"

SQLSTATE: 22000

Description: Incorrect SQL statement.

Solution: Modify the statement.

GAUSS-02865: "invalid range bound flags"

SQLSTATE: 42601

Description: Incorrect syntax.

Solution: Modify the SQL statement.

GAUSS-02866: "malformed range literal: '%s'"

SQLSTATE: 22P02

Description: Incorrect syntax.

Solution: Modify the SQL statement.

GAUSS-02867: "cannot store a toast pointer inside a range"

SQLSTATE: 22000

Description: Incorrect syntax.

Solution: Modify the SQL statement.

GAUSS-02868: "invalid input syntax for type macaddr: '%s'"

SQLSTATE: 22P02

Description: Incorrect syntax.

Solution: Modify the SQL statement.

GAUSS-02869: "invalid octet value in 'macaddr' value: '%s'"

SQLSTATE: 22003

Description: Incorrect syntax.

Solution: Modify the SQL statement.

GAUSS-02870: "unrecognized QueryItem type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02871 - GAUSS-02880

<br/>

GAUSS-02871: "syntax error in tsquery: '%s'"

SQLSTATE: 42601

Description: The syntax of the tsvector variable is incorrect.

Solution: Correct the syntax that used for the tsvector variable.

GAUSS-02872: "there is no escaped character: '%s'"

SQLSTATE: 42601

Description: In the tsvector variable string, no character follows the escape character.

Solution: Ensure that the escape character is followed by a character to be escaped.

GAUSS-02873: "wrong position info in tsvector: '%s'"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02874: "unrecognized state in gettoken_tsvector: %d"

SQLSTATE: XX000

Description: The status of internal tsvector variable parsing is incorrect.

Solution: Contact technical support. Alternatively, check the syntax of the string.

GAUSS-02875: "identifier too long"

SQLSTATE: 42622

Description: The data is too long.

Solution: Contact technical support. Alternatively, check the statement or data.

GAUSS-02879: "unexpected json parse state: %d"

SQLSTATE: XX004

Description: The syntax is incorrect.

Solution: Modify the SQL statement.

GAUSS-02880: "invalid input syntax for type json"

SQLSTATE: 22P02

Description: The syntax is incorrect.

Solution: Modify the SQL statement.

<br/>

## GAUSS-02881 - GAUSS-02890

<br/>

GAUSS-02881: "invalid type modifier"

SQLSTATE: 22023

Description: Syntax error.

Solution: Modify the SQL statement.

GAUSS-02882: "length for type %s must be at least 1"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02883: "length for type %s cannot exceed %d"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02884: "bit string length %d does not match type bit(%d)"

SQLSTATE: 22026

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02885: "'%c' is not a valid binary digit"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02886: "'%c' is not a valid hexadecimal digit"

SQLSTATE: 22P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02888: "bit string too long for type bit varying(%d)"

SQLSTATE: 22001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02889: "negative substring length not allowed"

SQLSTATE: 22011

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-02891 - GAUSS-02900

<br/>

GAUSS-02893: "bit index %d out of valid range (0..%d)"

SQLSTATE: 2202E

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02894: "new bit must be 0 or 1"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-02895: "invalid input syntax for type %s: '%s'"

SQLSTATE: 22P02

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-02896: "invalid cidr value: '%s'"

SQLSTATE: 22P02

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-02897: "could not format inet value: %m"

SQLSTATE: 22P03

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-02898: "invalid address family in external '%s' value"

SQLSTATE: 22P03

Description: The network address family is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-02899: "invalid bits in external '%s' value"

SQLSTATE: 22P03

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.

GAUSS-02900: "invalid length in external '%s' value"

SQLSTATE: 22P03

Description: The network address is invalid.

Solution: Check whether the format of the entered network address is correct.
