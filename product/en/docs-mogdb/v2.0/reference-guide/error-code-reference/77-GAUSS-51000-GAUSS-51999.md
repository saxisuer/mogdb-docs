---
title: GAUSS-51000 - GAUSS-51999
summary: GAUSS-51000 - GAUSS-51999
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-51000 - GAUSS-51999

<br/>

## GAUSS-51000 - GAUSS-51099

<br/>

GAUSS-51000: "THP services must be shut down."

SQLSTATE: None

Description: The THP service is not disabled.

Solution: Disable the THP service.

GAUSS-51001: "Failed to obtain THP service."

SQLSTATE: None

Description: Failed to obtain THP service information.

Solution: Check whether the THP file exists.

GAUSS-51002: "Failed to close THP service."

SQLSTATE: None

Description: Failed to disable the THP service.

Solution: Check whether the THP file exists.

<br/>

## GAUSS-51100 - GAUSS-51199

<br/>

GAUSS-51100: "Failed to verify SSH trust on these nodes: %s."

SQLSTATE: None

Description: Failed to verify the SSH mutual trust on each node.

Solution: Verify that the mutual trust between nodes is built.

GAUSS-51101: "SSH exception: \n%s"

SQLSTATE: None

Description: The remote connection is abnormal.

Solution: Ensure that the network connection is normal and that the remote server IP address, login username, and password are correct.

GAUSS-51102:"Failed to exchange SSH keys for user [%s] performing the %s operation."

SQLSTATE: None

Description: Failed to exchange the trust key.

Solution: Verify that the network connection is normal and the server IP address configuration is correct.

GAUSS-51103: "Failed to execute the PSSH command [%s]."

SQLSTATE: None

Description: Failed to run the PSSH command.

Solution: Verify that the PSSH tool is complete and the executed command is correct.

GAUSS-51104: "Failed to obtain SSH status."

SQLSTATE: None

Description: Failed to obtain the SSH status and result.

Solution: Verify that the network connection is normal and the connected server exists.

GAUSS-51105: "Failed to parse SSH output: %s."

SQLSTATE: None

Description: Failed to analyze the SSH result.

Solution: Verify that the SSH tool is complete and the parsed command is correct.

GAUSS-51106: "The SSH tool does not exist."

SQLSTATE: None

Description: The SSH tool does not exist.

Solution: Verify that the SSH tool exists.

GAUSS-51107: "Ssh Paramiko failed."

SQLSTATE: None

Description: The Paramiko mutual trust failed.

Solution:

1. Check whether the network connection is normal.
2. Check whether the Paramiko package exists.
3. Check the following options in the **/etc/ssh/sshd_config** file:
   - PasswordAuthentication=yes;
   - MaxStartups=1000;
   - UseDNS=no;
   - **ClientAliveInterval** is greater than 10800 or equal to 0.
   - Check whether the value of **MaxSessions** is too small when there are a large number of hosts in the cluster.
4. After the modification is complete, restart the sshd service.

GAUSS-51108: "Ssh-keygen failed."

SQLSTATE: None

Description: Failed to run the **Ssh-keygen** command.

Solution: Verify that the ssh-keygen tool exists and that the network is normal.

GAUSS-51109: "Failed to check authentication."

SQLSTATE: None

Description: Identity authentication failed.

Solution: View the logs and check the cause of the identity authentication failure. Then, try the authentication again.

GAUSS-51110: "Failed to obtain RSA host key for local host."

SQLSTATE: None

Description: Failed to obtain the RSA host key of the local host.

Solution: Check the cause of the failure and try again.

GAUSS-51111: "Failed to append local ID to authorized_keys on remote node."

SQLSTATE: None

Description: Failed to add the local ID to the **authorized_keys** of the remote node.

Solution: Check the cause of the failure and try again.

GAUSS-51112: "Failed to exchange SSH keys for user[%s] using hostname."

SQLSTATE: None

Description: Failed to exchange the user name using the SSH private key.

Solution: Check whether the network is normal.

<br/>

## GAUSS-51200 - GAUSS-51299

<br/>

GAUSS-51200: "The parameter [%s] in the XML file does not exist."

SQLSTATE: None

Description: Failed to find the *%s* parameter in the XML.

Solution: Check the configuration parameters in the XML.

GAUSS-51201: "Node names must be configured."

SQLSTATE: None

Description: The node name needs to be configured.

Solution: Configure the node name for the node.

GAUSS-51202: "Failed to add the %s instance."

SQLSTATE: None

Description: Failed to add the instance.

Solution: Ensure that the instance exists and the configuration is correct.

GAUSS-51203: "Failed to obtain the %s information from static configuration files."

SQLSTATE: None

Description: Failed to obtain the cluster information from the static configuration file.

Solution: Ensure that the static configuration file exists, that you have the access permission, and that the system is normal.

GAUSS-51204: " Invalid %s instance type: %d."

SQLSTATE: None

Description: The instance type is invalid.

Solution: Check and modify the instance type to be a valid one.

GAUSS-51205: "Failed to refresh the %s instance ID."

SQLSTATE: None

Description: Failed to update the instance ID.

Solution: Check whether the static configuration file of the current cluster exists or whether it has been broken, and check whether the user has the access permission. Update the CN ID of the new cluster based on this file.

GAUSS-51206: "The MPPRC file path must be an absolute path: %s."

SQLSTATE: None

Description: The MPPRC path must be an absolute path.

Solution: Change the MPPRC path to an absolute path.

GAUSS-51207: "Failed to obtain backIp from node [%s]."

SQLSTATE: None

Description: Failed to obtain the alternate IP address on the node.

Solution: Check and correctly configure the alternate IP address on the node.

GAUSS-51208: "Invalid %s number [%d]."

SQLSTATE: None

Description: The quantity is invalid.

Solution: Ensure that the quantity is valid.

GAUSS-51209: "Failed to obtain %s configuration on the host [%s]."

SQLSTATE: None

Description: Failed to obtain the configuration on this node.

Solution: Ensure that the configuration is correct on the node.

GAUSS-51210: "The obtained number does not match the instance number."

SQLSTATE: None

Description: The number of instances obtained does not match the actual instance number.

Solution: Check and modify the obtained number to match the actual instance number.

GAUSS-51211: "Failed to save a static configuration file."

SQLSTATE: None

Description: Failed to save the static configuration file.

Solution: Ensure that the static configuration file exists, that you have the access permission, and that the system is normal.

GAUSS-51212: "There is no information about %s."

SQLSTATE: None

Description: There is no information about *%s*.

Solution: Configure corresponding information for *%s*.

GAUSS-51213: "The port number of XML [%s] conflicted."

SQLSTATE: None

Description: The port number is occupied.

Solution: Check whether the occupied port process is available. If the process is unavailable, kill it, or change a new port number.

GAUSS-51214: "The number of capacity expansion DN nodes cannot be less than three or CN nodes cannot be less than one."

SQLSTATE: None

Description: The number of nodes before scale-out cannot be less than three.

Solution: Ensure that the number of nodes before scale-out is greater than or equal to three.

GAUSS-51215 : "The capacity expansion node [%s] cannot contain GTM/CM/ETCD."

SQLSTATE: None

Description: The GTM, CM, and ETCD cannot be contained on the scale-out node.

Solution: Delete the GTM, CM, and ETCD on the scale-out node.

GAUSS-51216: "The capacity expansion node [%s] must contain CN or DN."

SQLSTATE: None

Description: The node to be added to the cluster for scale-out does not have any CN or DN.

Solution: Add CN or DN on the scale-out node.

GAUSS-51217: "The cluster's static configuration does not match the new configuration file."

SQLSTATE: None

Description: The cluster's static configuration is inconsistent with the new configuration.

Solution: Modify the new XML configuration file, leaving it consistent with the cluster static configuration.

GAUSS-51218: "Failed to obtain initialized configuration parameter: %s."

SQLSTATE: None

Description: Failed to obtain the initialization configuration parameters.

Solution: Try to obtain the parameters again.

GAUSS-51219: "There is no CN in cluster."

SQLSTATE: None

Description: The CN is not installed in the cluster.

Solution: Reconfigure the XML file and execute the preparation and installation operations.

GAUSS-51220: "The IP address %s is incorrect."

SQLSTATE: None

Description: The IP address is incorrect.

Solution: Check whether the IP address configured in the XML file is correct.

GAUSS-51221: "Failed to configure hosts mapping information."

SQLSTATE: None

Description: Failed to configure the host mapping.

Solution: Check whether the **/etc/hosts** file exists and whether the host name and IP address configured in the file are consistent.

GAUSS-51222: "Failed to check hostname mapping."

SQLSTATE: None

Description: Failed to check the host mapping.

Solution: Check whether the mutual trust between nodes is normal.

GAUSS-51223: "Failed to obtain network inet addr on the node(%s)."

SQLSTATE: None

Description: Failed to obtain the inet address from the node.

Solution: Run the **ifconfig** command in the **root** directory to manually check whether the inet address exists. If it does, try to obtain the address again.

GAUSS-51224: "The ip(%s) has been used on other nodes."

SQLSTATE: None

Description: The IP address has been used on another node.

Solution: Check whether the IP address on each node in the XML file conflicts with each other.

GAUSS-51225: "Failed to set virtual IP."

SQLSTATE: None

Description: Failed to configure the virtual IP address.

Solution: Check whether the virtual IP address is used.

GAUSS-51226: "Virtual IP(s) and Back IP(s) do not have the same network segment."

SQLSTATE: None

Description: The virtual IP address and alternate IP address are not in the same network segment.

Solution: Set the virtual IP address and alternate IP address in the XML file to be in the same network segment.

GAUSS-51227: "The number of %s on all nodes are different."

SQLSTATE: None

Description: The number of alternate IP addresses on all nodes is different from that of the SSH IP addresses.

Solution: Set the number of alternate IP addresses in the XML file to be the same as that of the SSH IP address.

GAUSS-51228: "The number %s does not match %s number."

SQLSTATE: None

Description: The two numbers are different.

Solution: Set the two numbers to be the same.

GAUSS-51229: "The DN listenIp(%s) is not in the virtualIp or backIp on the node(%s)."

SQLSTATE: None

Description: The listen IP address on the DN is not the virtual IP address or alternate IP address.

Solution: Change the listen IP address to the virtual IP address or alternate IP address in the XML file.

GAUSS-51230: "The number of %s must %s."

SQLSTATE: None

Description: The number of CM servers, GTMs, or ETCDs is incorrect.

Solution: Configure correct numbers for CM servers, GTMs, and ETCDs in the XML file.

GAUSS-51231: "Old nodes is less than 2."

SQLSTATE: None

Description: The number of old nodes is less than two.

Solution: Set the number of the configured nodes in the XML file to be greater than or equal to 3.

GAUSS-51232: "XML configuration and static configuration are the same."

SQLSTATE: None

Description: The XML configuration and static configuration files are the same.

Solution: Before the scale-out, scale-in, and CN management operations, ensure that the XML is different from that of the installed clusters.

GAUSS-51233: "The Port(%s) is invalid on the node(%s)."

SQLSTATE: None

Description: The port is invalid.

Solution: Check whether the port number configured in the XML file is correct.

GAUSS-51234 : "The configuration file [%s] contains parsing errors."

SQLSTATE: None

Description: The XML file is analyzed incorrectly.

Solution: Check whether the configuration file is broken. If the file has been broken, copy a file from another node. If the file has not been broken, reinstall the cluster.

GAUSS-51235: "Invalid directory [%s]."

SQLSTATE: None

Description: The directory is invalid.

Solution: Check whether the directory is correct.

GAUSS-51236: "Failed to parsing xml."

SQLSTATE: None

Description: Failed to parse the XML file.

Solution: Check whether the XML file is opened successfully.

GAUSS-51239: "Failed to parse json. gs_collect configuration file (%s) is invalid , check key in json file"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51240: "gs_collect configuration file is invalid, TypeName or content must in config file."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51241: "The parameter %s(%s) formate is wrong, or value is less than 0."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51242: "gs_collect configuration file is invalid: %s, the key: (%s) is invalid."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51243: "content(%s) does not match the typename(%s) in gs_collect configuration file(%s)."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51244: "(%s) doesn't yet support."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51245: "There are duplicate key(%s)."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51246: "%s info only support one time collect."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51247: "These virtual IP(%s) are not accessible after configuring."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51248: "gs_collector does not support '%s' view collection. please check key in json file."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-51300 - GAUSS-51399

<br/>

GAUSS-51300: "Failed to execute SQL: %s."

SQLSTATE: None

Description: Failed to execute the SQL statement.

Solution: Ensure that the SQL statement is correct. Check whether the database status is normal. Check whether the user has the execution permission. Check whether the database object exists.

GAUSS-51301 : "Execute SQL time out. \nSql: %s."

SQLSTATE: None

Description: The execution of the SQL statement times out.

Solution: Ensure that the SQL statement is correct. Check whether the database status is normal. Check whether the user has the execution permission. Check whether the network is normal.

GAUSS-51302: "The table '%s.%s' does not exist or is private table!"

SQLSTATE: None

Description: The table does not exist or is a private table.

Solution: 1. Ensure that the table exists in the database. 2. If the table is a private table, use **gs_dump** to back up the table.

GAUSS-51303: "Query·'%s'·has·no·record!."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51304: "Query '%s' result '%s' is incorrect!."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51305: "The table '%s.%s' exists!"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-51400 - GAUSS-51499

<br/>

GAUSS-51400: "Failed to execute the command: %s."

SQLSTATE: None

Description: Failed to run the command.

Solution: Check whether the command is correct and whether you have the permission to run the command.

GAUSS-51401: "Failed to do %s.sh."

SQLSTATE: None

Description: Failed to execute the shell script.

Solution: Check whether the command is correct and whether you have the permission to run the command. Ensure that the shell script exists.

GAUSS_50402: "The usage of INODE cannot be greater than %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-51500 - GAUSS-51599

<br/>

GAUSS-51500: "Failed to call the interface %s. Exception: %s."

SQLSTATE: None

Description: Failed to invoke the interface.

Solution: Check whether the network connection is normal, whether the path is correct, and whether the invoked interface is open.

<br/>

## GAUSS-51600 - GAUSS-51699

<br/>

GAUSS-51600: "Failed to obtain the cluster status."

SQLSTATE: None

Description: Failed to obtain the cluster status.

Solution: Check whether the cluster has been set up. Check whether the created cluster is started as expected.

GAUSS-51601: "Failed to check %s status."

SQLSTATE: None

Description: Failed to check the cluster or local node status.

Solution: Check whether the cluster or local node status is normal.

GAUSS-51602: "The cluster status is Abnormal."

SQLSTATE: None

Description: The cluster status is abnormal.

Solution: Check whether the cluster status is not restored because the tool script command is not executed.

GAUSS-51603: "Failed to obtain peer %s instance."

SQLSTATE: None

Description: Failed to obtain the peer instance.

Solution: Ensure that the **gtm.conf** path exists if you want to obtain GTM. Check whether the configuration is correct.

GAUSS-51604: "There is no HA status for %s."

SQLSTATE: None

Description: No HA status is available in the instance.

Solution: Ensure that the cluster status and network connection are normal.

GAUSS-51605: "Failed to check whether the %s process exists."

SQLSTATE: None

Description: Failed to check whether the process exists.

Solution: Ensure that the user has the permission to check the process and that the process is correctly checked.

GAUSS-51606: "Failed to kill the %s process.

SQLSTATE: None

Description: Failed to kill the process.

Solution: Ensure that the user has the permission to kill the process and that the execution command is correct.

GAUSS-51607: "Failed to start %s."

Description: Failed to start the cluster, node, or instance.

Solution: Ensure that the network connection is normal and that the configuration file is correct.

GAUSS-51608: "Failed to lock cluster"

SQLSTATE: None

Description: Failed to lock the cluster.

Solution: Do not perform the closure or stop operations before the session is complete.

GAUSS-51609: "Failed to unlock cluster"

SQLSTATE: None

Description: Failed to unlock the cluster.

Solution: Do not perform the closure or stop operations before the session is complete.

GAUSS-51610: "Failed to stop %s."

SQLSTATE: None

Description: Failed to stop the cluster, node, or instance.

Solution: Ensure that the command is correctly executed and that the cluster, node, or instance is normal.

GAUSS-51611: "Failed to create %s instance."

SQLSTATE: None

Description: Failed to create the instance.

Solution: Check whether the CN in the XML file is correctly configured.

GAUSS-51612: "The instance IDs [%s] are not found in %s."

SQLSTATE: None

Description: The ID of an instance is not found in the node information.

Solution: Check whether the CN in the XML file is correctly configured when you add or delete the CN.

GAUSS-51613: "There is no instance in %s to be built."

SQLSTATE: None

Description: No instance needs to be created.

Solution: The XML file is incorrectly configured when the user manages the CN. No instance is configured on the new node in the XML file.

GAUSS-51614: "Received signal[%d]."

SQLSTATE: None

Description: Received the signal *%d*.

Solution: Try to receive the signal again.

GAUSS-51615: "Failed to initialize instance."

SQLSTATE: None

Description: Failed to initialize the instance.

Solution: Ensure that the **gs_initdb** or **gs_initcm** utility exists. Ensure that the **gs_initdb** or **gs_initcm** command is correct. Ensure that the cluster, node, or instance is normal.

GAUSS-51616: "Failed to dump %s schema."

SQLSTATE: None

Description: Failed to dump the CN or DN schema.

Solution: Ensure that the **dumpall** or **dump** tool exists. Ensure that the **dumpall** or **dump** command is correctly executed. Ensure that the cluster, node, or instance is normal.

GAUSS-51617: "Failed to rebuild %s."

SQLSTATE: None

Description: Failed to rebuild.

Solution: Run the command again.

GAUSS-51618: "Failed to get all hostname."

SQLSTATE: None

Description: Failed to obtain all the host names.

Solution: Ensure that the SSH mutual trust is normal.

GAUSS-51619: "The host·name [%s] is not in the cluster."

SQLSTATE: None

Description: The current node is not in the cluster.

Solution: Check whether the node is configured in the XML file.

GAUSS-51620: "Failed to obtain %s instance information."

SQLSTATE: None

Description: Failed to obtain the instance information.

Solution: Ensure that the XML file is correctly configured and that the cluster, host, or instance is normal.

GAUSS-51621: "HA IP is empty."

SQLSTATE: None

Description: The HA IP address is empty.

Solution: Check the configuration information in the XML file. If the HA IP address is configured in the XML file, you need to ensure that the value is correctly configured. If the HA IP address is not configured in the XML file, the alternate IP address will be used by default, and you need to ensure that the alternate IP address is correctly configured in the XML file.

GAUSS-51622: "There is no %s on %s node."

SQLSTATE: None

Description: No CN/DN instance exists on the node.

Solution: Check whether the CN or DN instance is configured on the node in the XML file. Ensure that the current cluster, host, or instance is normal.

GAUSS-51623: "Failed to obtain version."

SQLSTATE: None

Description: Failed to obtain the cluster version.

Solution: Ensure that the *version.cfg* file exists. Check whether the data format of the *version.cfg* file is correct. Check whether the current cluster, host, or instance is normal.

GAUSS-51624: "Failed to get CN connections."

SQLSTATE: None

Description: Failed to obtain the number of CN connections.

Solution: Ensure that the cluster, node, and instance are running properly.

GAUSS-51625: "Cluster is running."

SQLSTATE: None

Description: The cluster is running.

Solution: Stop this cluster.

GAUSS-51626: "Failed to rollback."

SQLSTATE: None

Description: Failed to roll back.

Solution: Ensure that the command is correctly executed. Ensure that the current cluster, host, or instance is normal. View the corresponding log information.

GAUSS-51627: "Configuration failed."

SQLSTATE: None

Description: Configuration failed.

Solution: Perform the configuration again.

GAUSS-51628: "The version number of new cluster is [%s]. It should be float."

SQLSTATE: None

Description: The version number format of the new cluster is incorrect.

Solution: Ensure that the *version.cfg* file exists. Check whether the data format of the *version.cfg* file is correct. Check whether the current cluster, host, or instance is normal.

GAUSS-51629: "The version number of new cluster is [%s]. It should be greater than or equal to the old version."

SQLSTATE: None

Description: The version number of the new cluster is incorrect. It should be later than or equal to the old version number.

Solution: Ensure that the *version.cfg* file exists. Check whether the version number in the *version.cfg* file is earlier than that of the existing cluster. Ensure that the cluster, node, or instance is normal.

GAUSS-51630: "No node named %s."

SQLSTATE: None

Description: The node name does not exist.

Solution: Check whether the node is configured in the XML file. Ensure that the cluster, node, or instance is normal.

GAUSS-51631: "Failed to delete the %s instance."

SQLSTATE: None

Description: Failed to delete the CN.

Solution: Ensure that SSH mutual trust is normal, that the command is correctly executed, and that the cluster, node, or instance is normal.

GAUSS-51632: "Failed to do %s."

SQLSTATE: None

Description: Failed to execute the Python script.

Solution: Ensure that the Python script exists, that the command is correctly executed, and that the cluster, node, or instance is normal.

GAUSS-51633: "The·step·of·upgrade·number·%s·is·incorrect."

SQLSTATE: None

Description: The ID must be composed of digits.

Solution: Ensure that the specified instance exists, that the command is correctly executed, and that the cluster, node, or instance is normal.

GAUSS-51634: "Waiting node synchronizing timeout lead to failure."

SQLSTATE: None

Description: Waiting for the node synchronization results times out.

Solution: Ensure that the command is correctly executed and that the cluster, node, or instance is normal.

GAUSS-51635: "Failed to check SHA256."

SQLSTATE: None

Description: Failed to check SHA-256.

Solution: Ensure that the SHA-256 file exists. Ensure that the command is correctly executed. Ensure that the cluster, node, or instance is normal.

GAUSS-51636: "Failed to obtain %s node information."

SQLSTATE: None

Description: Failed to obtain the node information.

Solution: Ensure that the node is configured in the XML file, that the command is correctly executed, and that the cluster, node, or instance is normal.

GAUSS-51637: "The %s does not match with %s."

SQLSTATE: None

Description: The cluster information does not match.

Solution: Ensure that the two parties to be matched are updated simultaneously. Ensure that the command is correctly executed. Ensure that the cluster, node, or instance is normal.

GAUSS-51638: "Failed to append instance on host [%s]."

SQLSTATE: None

Description: Failed to add an instance on the node.

Solution: Check whether the XML file is correctly configured and whether the path conflict exists. Ensure that the cluster, node, or instance is normal.

GAUSS-51639: "Failed to obtain %s status of local node."

SQLSTATE: None

Description: Failed to obtain the local node status.

Solution: Rectify the fault.

GAUSS-51640: "Can't connect to cm_server, cluster is not running possibly."

SQLSTATE: None

Description:**cm_server** is not connected and the cluster is probably not running.

Solution: Restart the cluster.

GAUSS-51641: "Cluster redistributing status is not accord with expectation."

SQLSTATE: None

Description: The redistribution status does not meet the requirements.

Solution: Check whether the cluster status is normal. Ensure that the cluster, node, or instance is normal.

GAUSS-51642: "Failed to promote peer instances."

SQLSTATE: None

Description: Failed to start the peer instance.

Solution: Ensure that the specified instance exists, that the command is correctly executed, and that the cluster, node, or instance is normal.

GAUSS_51643: "Cluster is in read-only mode."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS_51644: "Failed to set resource control for the cluster."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51645: "Failed to restart %s."

SQLSTATE: None

Description: Failed to restart the cluster or logical cluster.

Solution: Ensure that the network connection is normal and that the configuration file is correct.

GAUSS-51646: "The other OM operation is currently being performed in the cluster node:"" '%s'."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51647: "The operation step of OM components in current cluster nodes do not match"" with each other: %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51648: "Waiting for redistribution process to end timeout."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51649: "Capture exceptions '%s' : %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51650: "Unclassified exceptions: %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51651: "The node '%s' status is Abnormal."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51652: "The package version on some nodes are inconsistent."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51653: "The %s can not exist in %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51654: "The standbyIp %s should different Kerberos server IP %s."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51655: "The %s has already installed."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-51656: "Failed to make server.key.cipher and server.key.rand"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-51700 - GAUSS-51799

<br/>

GAUSS-51700: "There must be only one record in the pgxc_group table."

SQLSTATE: None

Description: Only one record exists in the current **pgxc_group** node group.

Solution: Ensure only one record exists in the current **pgxc_group** node group when the SQL statement is executed.

GAUSS-51701: "The current node group is incorrect."

SQLSTATE: None

Description: The current node group is incorrect.

Solution: Ensure that the XML configuration file is correctly configured. Ensure that the current node group is the required node group.

GAUSS-51702: "Failed to obtain node group information."

SQLSTATE: None

Description: Failed to obtain the node group information.

Solution: Ensure that the group name and number of nodes are correct.

GAUSS-51703: "Failed to drop record from PGXC_NODE."

SQLSTATE: None

Description: Failed to redistribute data across nodes.

Solution: Perform the operation again.

GAUSS-51704: "Failed to set Cgroup."

SQLSTATE: None

Description: Failed to configure cgroups.

Solution: Configure cgroups again.

GAUSS-51705: "Failed to update PGXC_NODE."

SQLSTATE: None

Description: Failed to update PGXC_NODE.

Solution: Check whether the database connection is normal.

GAUSS-51706: "Failed to check Cgroup."

SQLSTATE: None

Description: Failed to check cgroups.

Solution: Check whether the cluster status is normal.

GAUSS-51707: "Failed to install Cgroup."

SQLSTATE: None

Description: Failed to install cgroups.

Solution: Check whether the cgroups component has been mounted to the cluster.

GAUSS-51708: "Failed to uninstall Cgroup."

SQLSTATE: None

Description: Failed to uninstall cgroups.

Solution: Uninstall cgroups again.

GAUSS-51709: "Failed to clean Cgroup configuration file."

SQLSTATE: None

Description: Failed to clear the cgroups configuration file.

Solution: Run the clearance command again.

<br/>

## GAUSS-51800 - GAUSS-51899

<br/>

GAUSS-51800: "The environmental variable %s is empty.·or·variable·has·exceeded·maximum·length"

SQLSTATE: None

Description: Environment variable is null.

Solution: Ensure that the environment variable is successfully written.

GAUSS-51801: "The environment variable %s exists."

SQLSTATE: None

Description: The environment variable exists.

Solution: Check why the environment variable exists in the **.barshrc** file and confirm whether it is because the variable has been added in the file.

GAUSS-51802: "Failed to obtain the environment variable %s."

SQLSTATE: None

Description: Failed to obtain the environment variable.

Solution: Ensure that the environment variable configuration is complete. Check that whether the environment variable path is correct.

GAUSS-51803: "Failed to delete the environment variable %s."

SQLSTATE: None

Description: Failed to delete the environment variable.

Solution: Ensure that the deletion command is correct and that you have the permission to delete objects.

GAUSS-51804: "Failed to set the environment variable %s."

SQLSTATE: None

Description: Failed to set the environment variables.

Solution: Check whether the permission is correct. Check whether the network connection is normal. Check whether the command of setting the environment variable is correct.

GAUSS-51805: "The environmental variable [%s]'s value is invalid."

SQLSTATE: None

Description: The environment variable value is invalid.

Solution: Check whether the network connection is normal. Check whether the command of setting the environment variable is correct. Check whether the value of the *$GAUSS_ENV* environment variable in the ./bashrc file is correct.

GAUSS-51806: "The cluster has been installed."

SQLSTATE: None

Description: The cluster has been installed.

Solution: Check whether the value of the *$GAUSS_ENV* environment variable in the .bashrc file is 2.

GAUSS-51807: "$GAUSSHOME of user is not equal to installation path."

SQLSTATE: None

Description: The GAUSSHOME path is inconsistent with that of the cluster installation path.

Solution: Ensure that you have the path access permission and that the network connection is normal. Ensure that the command used for setting the environment variable is correct and that the value of the *$GAUSSHOME* environment variable in the ./bashrc file is the same as the path configured in the XML file.

GAUSS-51808: "The env file contains errmsg: %s."

SQLSTATE: None

Description: The environment variable file package **/etc/profile** contains error or output flows.

Solution: Check the files mentioned in the error information, run **source** to find the corresponding output and delete the corresponding shell.

GAUSS-51809: "The variable [%s] context [%s] is invalid."

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-51900 - GAUSS-51999

<br/>

GAUSS-51900: "The current OS is not supported."

SQLSTATE: None

Description: The current OS cannot be used.

Solution: Check and switch to the OS that is supported for further operations.

GAUSS-51901: "The OS versions are different among cluster nodes."

SQLSTATE: None

Description: The OSs between cluster nodes are inconsistent.

Solution: Keep all nodes in the cluster use the same version of OS.
