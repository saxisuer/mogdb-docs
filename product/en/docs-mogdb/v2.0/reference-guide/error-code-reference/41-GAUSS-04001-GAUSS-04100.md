---
title: GAUSS-04001 - GAUSS-04100
summary: GAUSS-04001 - GAUSS-04100
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04001 - GAUSS-04100

<br/>

## GAUSS-04001 - GAUSS-04010

<br/>

GAUSS-04001: "invalid option '%s'"

SQLSTATE: HV00D

Description: The parameter option is invalid.

Solution: Check the input parameter and perform the operation again.

GAUSS-04002: "SHARED mode can only be used with TEXT format"

SQLSTATE: XX000

Description: The user attempts to use a format other than TEXT when using a GDS foreign table in SHARED mode.

Solution: Modify the file format in the import specifications. The GDS foreign table where the error occurred can only read the TEXT file flow.

GAUSS-04003: "can't find error record table '%s'"

SQLSTATE: XX000

Description: Failed to load the error record table because the system memory is insufficient.

Solution: Perform the operation again after the OS reclaims partial memory.

GAUSS-04005: "could not read from file: %m"

SQLSTATE: XX000

Description: The file does not exist or the relevant permission is incorrect.

Solution: Ensure that the file exists and the permission is correct. Perform the operation again.

GAUSS-04006: "unable to open URL '%s'"

SQLSTATE: XX000

Description: The file does not exist or the relevant permission is incorrect.

Solution: Ensure that the file exists and the permission is correct. Perform the operation again.

GAUSS-04007: "fill_missing_fields can't be set while '%s' is NOT NULL"

SQLSTATE: 42601

Description: The **fill_missing_fields** cannot be set for a non-null column.

Solution: Check the input parameter and perform the operation again.

GAUSS-04008: "unsupport BINARY format"

SQLSTATE: XX000

Description: Data in the BINARY format is not supported.

Solution: Check the data format and perform the operation again.

GAUSS-04009: "only both text && csv formats are supported for foreign table"

SQLSTATE: 0A000

Description: The foreign table supports only the data in TEXT and CSV formats.

Solution: Check the data format contained in the foreign table and perform the operation again.

GAUSS-04010: "%s doesn't exist, please create it first"

SQLSTATE: XX000

Description: The folder does not exist or the relevant permission is incorrect.

Solution: Ensure that the folder exists and the permission is correct. Perform the operation again.

<br/>

## GAUSS-04011 - GAUSS-04020

<br/>

GAUSS-04011: "%s exists and is a file, please remove it first and create directory"

SQLSTATE: XX000

Description: The folder does not exist or the relevant permission is incorrect.

Solution: Delete and create a folder or use other paths, and perform the operation again.

GAUSS-04012: "location filepath is too long when importing data to foreign table"

SQLSTATE: 22023

Description: The folder path of the foreign table is too long.

Solution: Use an appropriate path and perform the operation again.

GAUSS-04014: "no Snowball stemmer available for language '%s' and encoding '%s'"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04015: "multiple Language parameters"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04016: "unrecognized Snowball parameter: '%s'"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04017: "missing Language parameter"

SQLSTATE: 22023

Description: The **Language** parameter is missing.

Solution: Add the **Language** parameter and perform the operation again.

GAUSS-04018: "could not translate host name '%s' to address: %s"

SQLSTATE: XX000

Description: Failed to convert the host name to an IP address.

Solution: Check the host name configured in **hba.conf** and ensure that the IP address is correct.

GAUSS-04019: "unsupported integer size %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04020: "no data left in message"

SQLSTATE: 08P01

Description: No data is left in the message.

Solution: The internal communication of the database is abnormal. Contact technical support.

<br/>

## GAUSS-04021 - GAUSS-04030

<br/>

GAUSS-04021: "insufficient data left in message"

SQLSTATE: 08P01

Description: The remaining length of the message is smaller than the length obtained from the message.

Solution: Check the current string.

GAUSS-04022: "invalid string in message"

SQLSTATE: 08P01

Description: The message is invalid.

Solution: The internal communication of the database is abnormal. Contact technical support.

GAUSS-04023: "invalid message format"

SQLSTATE: 08P01

Description: The message format is invalid.

Solution: The internal communication of the database is abnormal. Contact technical support.

GAUSS-04024: "Postgres-XC does not support large object yet"

SQLSTATE: 0A000

Description: Large objects are not supported.

Solution: Do not use this feature.

GAUSS-04025: "invalid large-object descriptor: %d"

SQLSTATE: 42704

Description: The large-object descriptor is invalid.

Solution: Do not use this feature.

GAUSS-04026: "permission denied for large object %u"

SQLSTATE: 42501

Description: Large objects are not supported.

Solution: Do not use this feature.

GAUSS-04027: "large object descriptor %d was not opened for writing"

SQLSTATE: 55000

Description: Large objects are not supported.

Solution: Do not use this feature.

GAUSS-04028: "must be system admin to use server-side lo_import()"

SQLSTATE: 42501

Description: Only system administrators can use the **lo_import** function on the server.

Solution: Do not use this feature.

GAUSS-04029: "could not open server file '%s': %m"

SQLSTATE: XX000

Description: Failed to open a server file.

Solution: Do not use this feature.

GAUSS-04030: "could not read server file '%s': %m"

SQLSTATE: XX000

Description: Failed to read the server file.

Solution: Do not use this feature.

<br/>

## GAUSS-04031 - GAUSS-04040

<br/>

GAUSS-04031: "must be system admin to use server-side lo_export()"

SQLSTATE: 42501

Description: Only system administrators can use the **lo_export** function on the server.

Solution: Do not use this function.

GAUSS-04032: "could not create server file '%s': %m"

SQLSTATE: XX000

Description: Failed to create the server file.

Solution: Do not use this function.

GAUSS-04033: "could not chmod server file '%s': %m"

SQLSTATE: XX000

Description: Failed to modify the server file permission.

Solution: Do not use this function.

GAUSS-04034: "could not write server file '%s': %m"

SQLSTATE: XX000

Description: Failed to write the server file.

Solution: Do not use this function.

GAUSS-04035: "connection requires a valid client certificate"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04038: "pg_hba.conf rejects connection for host '%s', user '%s', database '%s', %s"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04039: "pg_hba.conf rejects connection for host '%s', user '%s', database '%s'"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04040: "no pg_hba.conf entry for host '%s', user '%s', database '%s', %s"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04041 - GAUSS-04050

<br/>

GAUSS-04041: "no pg_hba.conf entry for host '%s', user '%s', database '%s'"

SQLSTATE: 28000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04045: "Invalid username/password,login denied."

SQLSTATE: 28000

Description: The username or password is invalid and the login fails.

Solution: Ensure that the username and password are valid.

GAUSS-04047: "SSPI is not supported in protocol version 2"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04048: "out of memory"

SQLSTATE: XX000

Description: Failed to allocate the memory.

Solution: Ensure that the system has sufficient memory.

GAUSS-04050: "malloc failed"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04051 - GAUSS-04060

<br/>

GAUSS-04051: "could not set the cipher list (no valid ciphers available)"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04052: "sslciphers can not be null"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04053: "unrecognized ssl ciphers name: '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04055: "could not load server certificate file '%s': %s"

SQLSTATE: F0000

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04057: "private key file '%s' has group or world access"

SQLSTATE: F0000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04058: "could not load private key file '%s': %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04059: "check of private key '%s'failed: %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04061 - GAUSS-04070

<br/>

GAUSS-04061: "could not load the ca certificate file"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04062: "could not load root certificate file '%s': %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04063: "could not load SSL certificate revocation list file '%s': %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04068: "cipher file '%s' has group or world access"

SQLSTATE: F0000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04069: "rand file '%s' has group or world access"

SQLSTATE: F0000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04070: "spin.c does not support S_LOCK_FREE()"

SQLSTATE: XX000

Description:**S_LOCK_FREE ()** is not supported.

Solution: Check whether the OS is supported.

<br/>

## GAUSS-04071 - GAUSS-04080

<br/>

GAUSS-04071: "not enough elements in RWConflictPool to record a read/write conflict"

SQLSTATE: 53200

Description: The RWConflictPool memory is insufficient and cannot record more read and write conflicts.

Solution: Increase the **max_connections** value or try again in a new transaction.

GAUSS-04072: "not enough elements in RWConflictPool to record a potential read/write conflict"

SQLSTATE: 53200

Description: The RWConflictPool memory is insufficient and cannot record more read and write conflicts.

Solution: Increase the **max_connections** value or try again in a new transaction.

GAUSS-04073: "not enough shared memory for elements of data structure '%s' (%lu bytes requested)"

SQLSTATE: 53200

Description: The memory is insufficient to initialize PredXactList.

Solution: Check whether the system memory is sufficient.

GAUSS-04074: "cannot use serializable mode in a hot standby"

SQLSTATE: 0A000

Description: Failed to set a hot standby to the serializable mode.

Solution: Set **default_transaction_isolation** to **repeatable read**.

GAUSS-04075: "a snapshot-importing transaction must not be READ ONLY DEFERRABLE"

SQLSTATE: 0A000

Description: READ ONLY DEFERRABLE cannot be imported into snapshots.

Solution: Redefine the transaction level.

GAUSS-04076: "could not serialize access due to read/write dependencies among transactions"

SQLSTATE: 40001

Description: The read and write operations conflict in serializable mode.

Solution: Try again.

GAUSS-04077: "unrecognized return value from HeapTupleSatisfiesVacuum: %u"

SQLSTATE: XX000

Description: Failed to recognize the returned value of **HeapTupleSatisfiesVacuum**.

Solution:Contact technical support.

GAUSS-04078: "deadlock seems to have disappeared"

SQLSTATE: XX000

Description: The deadlock abnormally disappears.

Solution:Contact technical support.

GAUSS-04079: "inconsistent results during deadlock check"

SQLSTATE: XX000

Description: The deadlock detection results are inconsistent.

Solution:Contact technical support.

GAUSS-04080: "deadlock detected"

SQLSTATE: 40P01

Description: A deadlock is detected.

Solution: Run **SELECT pg_cancel_backend** to end the deadlock thread and perform the operation again.

<br/>

## GAUSS-04081 - GAUSS-04090

<br/>

GAUSS-04081: "proc header uninitialized"

SQLSTATE: XX000

Description: ProcGlobal is not initialized.

Solution:Contact technical support.

GAUSS-04082: "you already exist"

SQLSTATE: XX000

Description: The process has exited.

Solution:Contact technical support.

GAUSS-04084: "could not set timer for process wakeup"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04085: "Lock wait timeout: thread %lu on node %s waiting for %s on %s after %ld.%03d ms"

SQLSTATE: YY002

Description: The distributed lock waiting times out.

Solution: Check the concurrent execution status.

GAUSS-04086: "could not disable timer for process wakeup"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-04087: "stuck spinlock (%p) detected at %s:%d"

SQLSTATE: XX000

Description: The spinlock has been suspended for a long time.

Solution:Contact technical support.

GAUSS-04089: "unrecognized lock mode: %d"

SQLSTATE: XX000

Description: Failed to recognize the lock mode.

Solution:Contact technical support.

GAUSS-04090: "cannot acquire lock mode %s on database objects while recovery is in progress"

SQLSTATE: 55000

Description: The applied lock level during the restoration is incorrect.

Solution:Contact technical support.

<br/>

## GAUSS-04091 - GAUSS-04100

<br/>

GAUSS-04091: "proclock table corrupted"

SQLSTATE: XX000

Description: The proclock hash table is damaged.

Solution:Contact technical support.

GAUSS-04092: "LockAcquire failed"

SQLSTATE: XX000

Description: Failed to obtain a common lock.

Solution: Check the concurrent execution status.

GAUSS-04093: "lock table corrupted"

SQLSTATE: XX000

Description: The lock hash table is damaged.

Solution:Contact technical support.

GAUSS-04094: "lock %s on object %u/%u/%u is already held"

SQLSTATE: XX000

Description: The object has been locked.

Solution: Check the lock status of the object.

GAUSS-04095: "locallock table corrupted"

SQLSTATE: XX000

Description: The locallock hash table is damaged.

Solution:Contact technical support.

GAUSS-04096: "failed to re-find shared lock object"

SQLSTATE: XX000

Description: Failed to find the shared lock again.

Solution:Contact technical support.

GAUSS-04097: "failed to re-find shared proclock object"

SQLSTATE: XX000

Description: Failed to find the shared proclock again.

Solution:Contact technical support.

GAUSS-04098: "too many conflicting locks found"

SQLSTATE: XX000

Description: The number of common lock conflicts is greater than the **MaxBackends** value.

Solution: Reduce the service concurrency.

GAUSS-04099: "cannot PREPARE while holding both session-level and transaction-level locks on the same object"

SQLSTATE: 0A000

Description: You cannot hold session-level and transaction-level locks for the same object at the same time.

Solution: Check the lock status of the object.

GAUSS-04100: "we seem to have dropped a bit somewhere"

SQLSTATE: XX000

Description: The masks of holding and releasing the lock do not match.

Solution:Contact technical support.
