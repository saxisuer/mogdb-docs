---
title: GAUSS-06901 - GAUSS-07000
summary: GAUSS-06901 - GAUSS-07000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-06901 - GAUSS-07000

<br/>

## GAUSS-06901 - GAUSS-06910

<br/>

GAUSS-06901: "Failed to alloc the space to fileHeader."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06902: "Failed to alloc the space to fileReader."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06903: "Can't find .carbondata file."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06904: "'type' option is not provided."

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06905: "No 'type' option provided."

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06906: "setMinMaxByHeapTuple"

SQLSTATE: 20000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06907: "delete or update failed due to concurrent conflict"

SQLSTATE: P0002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06908: "getting next tuple in an ordered catalog scan failed"

SQLSTATE: P0002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06909: "read handler is not in pending_free_reader_list"

SQLSTATE: D0009

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06910: "write handler is not in pending_free_writer_list"

SQLSTATE: D0009

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06911 - GAUSS-06920

<br/>

GAUSS-06911: "Only ORC/PARQUET/CARBONDATA/CSV/TEXT is supported for now."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06912: "Failed to get currentFileName !"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06913: "setDescByFilePath cannot find filename"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06914: "Rigth operation in constraint expression cannot be NULL."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06915: "Invalid Oid for operator %u."

SQLSTATE: 20000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06916: "Unsupported data type on typeoid:%u when converting string to datum."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06917: "Unsupported data type on typeoid:%u when converting datum to string."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06918: "The leftop is null"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06919: "The rightop is null"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06920: "The arg is null"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06921 - GAUSS-06930

<br/>

GAUSS-06921: "bloomFilter create failed"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06922: "empty partition expression"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06923: "Invalid partition expression:%s"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06924: "argument to option'%s' must be a valid encoding name"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06925: "Column %s is unsupported data type."

SQLSTATE: HV004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06926: "No %s is specified for the foreign table."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06927: "Unsupport any empy %s for the foreign table."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06928: "The first character and the end character of each %s must be a '/' in string '%s'."

SQLSTATE: 42P17

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06929: "No folder path is specified for the foreign table."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06930: "No file path is specified for the foreign table."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06931 - GAUSS-06940

<br/>

GAUSS-06931: "No hdfscfg path is specified for the server."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06932: "Only a folder path is allowed for the foreign table."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06933: "Only a hdfscfg path is allowed for the server."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06934: "No address is specified for the server."

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06935: "The port value is out of range: \'%s\'"

SQLSTATE: 44000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06936: "Please select other functions to parse deciaml data."

SQLSTATE: HV004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06937: "The incorrect numeric format."

SQLSTATE: HV004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06938: "index row size %zu exceeds maximum %zu for index '%s'"

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06939: "number of items mismatch in GIN entry tuple, %d in tuple header, %d decoded"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06940: "Invalid arguments for function ginbuild"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06941 - GAUSS-06950

<br/>

GAUSS-06941: "Invalid arguments for function cginbuild"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06942: "access method 'cgin' does not support multi column index with operator ||"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06943: "access method 'cgin' does not support null column index"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06944: "Invalid arguments for function gininsert"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06945: "invalid item pointer."

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06946: "Invalid arguments for function ginarrayextract"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06947: "Invalid arguments for function ginqueryarrayextract"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06948: "right sibling of GIN page is of different type"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06949: "right sibling of GIN page was deleted"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06950: "invalid return code from GIN placeToPage method: %d"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06951 - GAUSS-06960

<br/>

GAUSS-06951: "'%s' is not a GIN index"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06952: "cannot access temporary indexes of other sessions"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06953: "unexpected GIN leaf action: %hhu"

SQLSTATE: 2200G

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06954: "GIN split record did not contain a full-page image of left page"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06955: "GIN split record did not contain a full-page image of right page"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06956: "GIN split record did not contain a full-page image of root page"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06957: "replay of gin entry tree page vacuum did not restore the page"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06958: "gin_redo: unknown op code %hhu"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06959: "unexpected order by operator: %d"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06960: "Invalid arguments for function ginrescan"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06961 - GAUSS-06970

<br/>

GAUSS-06961: "Invalid arguments for function ginendscan"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06962: "posting list is too long"

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06963: "could not split GIN page; all old items didn't fit"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06964: "could not split GIN page; no new items fit"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06965: "could not fit vacuumed posting list"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06966: "Invalid arguments for function ginbulkdelete"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06967: "Invalid arguments for function ginvacuumcleanup"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06968: "missing GIN support function (%d or %d) for attribute %d of index '%s'"

SQLSTATE: 42883

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06969: "invalid attribute num:%hu"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06970: "could not find parent of block %u in lookup table"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06971 - GAUSS-06980

<br/>

GAUSS-06971: "failed to decompress keys in tuple"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06972: "failed to form tuple from gist"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06973: "could not seek temporary file: %ld"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06974: "could not read temporary file: %ld"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06975: "the leaf keys should have the high point equals to the low point"

SQLSTATE: 42P24

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06976: "gist_redo: unknown op code %hhu"

SQLSTATE: XX002

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06977: "index '%s' already contains data."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06978: "no order by operators allowed."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06979: "hashmerge: unimplemented."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06980: "buckets id %d of table is outsize range [%d,%d]"

SQLSTATE: 22003

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06981 - GAUSS-06990

<br/>

GAUSS-06981: "error format single item string %s"

SQLSTATE: 42602

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06982: "empty merge_list string"

SQLSTATE: 42602

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06983: "find merge_str in pgxc_class and is first set %d"

SQLSTATE: 42602

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06984: "not find merge_str in pgxc_class and not first set %d"

SQLSTATE: 42602

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06985: "could not truncate file '%s' to %u: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06986: "could not seek to the end of file '%s': %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06987: "could not parse filename '%s'"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06988: "wrong VM buffer passed to visibilitymap_set"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06989: "attempt to delete tuple containing indirect datums"

SQLSTATE: 29P02

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06990: "shouldn't be called for indirect tuples"

SQLSTATE: 29P02

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06991 - GAUSS-07000

<br/>

GAUSS-06991: "attempted to kill a tuple inserted by another transaction: %lu, %lu"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06992: "Can't fit xid into page. relation '%s', now xid is %lu, base is %lu, min is %u, max is %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06993: "Can't fit xid into page, now xid is %lu, base is %lu, min is %u, max is %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06994: "heap_delete: invalid tid %hu, max tid %hu, rnode[%u,%u,%u], block %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06995: "attempted to delete self created tuple"

SQLSTATE: 40001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06996: "pg_class entry for relid %u vanished during ExtractReplicaIdentity"

SQLSTATE: 40001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06997: "heap_update: invalid tid %hu, max tid %hu, rnode[%u,%u,%u], block %u"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06998: "attempted to update self created tuple"

SQLSTATE: 40001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06999: "relation '%s' has one big row which is not supported under 64bits XID system. Current xid is %lu"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-07000: "relation '%s' has no free space to upgrade. Current xid is %lu, please VACUUM FULL this relation!!!"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.
