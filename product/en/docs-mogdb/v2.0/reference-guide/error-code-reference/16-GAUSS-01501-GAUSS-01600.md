---
title: GAUSS-01501 - GAUSS-01600
summary: GAUSS-01501 - GAUSS-01600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01501 - GAUSS-01600

<br/>

## GAUSS-01501 - GAUSS-01510

<br/>

GAUSS-01501: "OID %u does not refer to a table"

SQLSTATE: 42P01

Description: *Parameter1* of the **pg_extension_config_dump()** function is not defined in the **pg_class** table.

Solution: Ensure that *parameter1* is defined in the **pg_class** table.

GAUSS-01502: "table '%s' is not a member of the extension being created"

SQLSTATE: 55000

Description: *Parameter1* of the **pg_extension_config_dump()** function is not a member of the extension being created.

Solution: Ensure that *parameter1* is a member of the extension being created.

GAUSS-01503: "extension with oid %u does not exist"

SQLSTATE: XX000

Description: Failed to obtain tuples in the **pg_extension** system catalog from the cache based on the OID of the current extension.

Solution: The system cache is abnormal. Contact technical support.

GAUSS-01504: "extconfig is not a 1-D Oid array"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01505: "extconfig and extcondition arrays do not match"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01507: "extension name cannot be qualified"

SQLSTATE: 42601

Description: During **ALTER EXTENSION SET SCHEMA** execution, a scheme name is added before the extension name.

Solution: Do not add a scheme name.

GAUSS-01508: "cannot move extension '%s' into schema '%s' because the extension contains the schema"

SQLSTATE: 55000

Description: During **ALTER EXTENSION SET SCHEMA** execution, the new schema is a member of the current extension.

Solution: Ensure that the new schema is not a member of the current extension.

GAUSS-01509: "extension '%s' does not support SET SCHEMA"

SQLSTATE: 0A000

Description: During **ALTER EXTENSION SET SCHEMA** execution, **relocatable** of the current extension is false.

Solution: Ensure that **relocatable** of an extension is true.

<br/>

## GAUSS-01511 - GAUSS-01520

<br/>

GAUSS-01511: "nested ALTER EXTENSION is not supported"

SQLSTATE: 0A000

Description:**ALTER EXTENSION UPDATE** is executed for multiple times within a transaction.

Solution: Execute the statement only once within a transaction.

GAUSS-01513: "%s is already a member of extension '%s'"

SQLSTATE: 55000

Description: During **ALTER EXTENSION ADD** execution, the object to be added is already a member of the extension. During **CREATE EXTENSION** execution, a specific object created in the extension SQL script is already a member of other extensions. "

Solution: Add an object that is not a member of the extension. Ensure that all objects created in the extension SQL script are not members of other extensions. "

GAUSS-01514: "cannot add schema '%s' to extension '%s' because the schema contains the extension"

SQLSTATE: 55000

Description: During **ALTER EXTENSION ADD** execution, the added schema is the one that contains the extension.

Solution: Add a schema that does not contain the extension.

GAUSS-01515: "%s is not a member of extension '%s'"

SQLSTATE: 55000

Description: During **ALTER EXTENSION DROP** execution, the object to be deleted is not a member of the extension.

Solution: Delete an object that is a member of the extension.

GAUSS-01517: "cache lookup failed for extension %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01518: "no security label providers have been loaded"

SQLSTATE: 22023

Description: During **SECURITY LABEL** execution, no related extensions are loaded.

Solution: Before executing the statement, load related extensions.

GAUSS-01519: "must specify provider when multiple security label providers have been loaded"

SQLSTATE: 22023

Description: During **SECURITY LABEL** execution, no security labels are specified.

Solution: Before executing the statement, specify security labels.

GAUSS-01520: "security label provider '%s' is not loaded"

SQLSTATE: 22023

Description: During **SECURITY LABEL** execution, the specified security label is not loaded.

Solution: Before executing the statement, only specify security lables that can be loaded.

<br/>

## GAUSS-01521 - GAUSS-01530

<br/>

GAUSS-01521: "can't negate an empty subexpression"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01522: "unrecognized boolop: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01524: "could not find attribute %d in subquery targetlist"

SQLSTATE: XX000

Description: The column returned for the subquery is empty.

Solution: Check whether the query result contains a null value.

GAUSS-01525: "so where are the outer joins?"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01527: "reached base rel"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01528: "could not find join node %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01529: "subquery cannot be result relation"

SQLSTATE: XX000

Description: The subquery is used as the result table.

Solution: Use a non-subquery as the result table.

GAUSS-01530: "unrecognized command_type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01531 - GAUSS-01540

<br/>

GAUSS-01531: "targetlist is not sorted correctly"

SQLSTATE: XX000

Description: The sequence of output columns is incorrect.

Solution: Ensure that the sequence of input columns is the same as insert or update columns.

GAUSS-01532: "unexpected outer reference in set operation subquery"

SQLSTATE: XX000

Description: The setop operation involves cross-reference.

Solution: Ensure that no column involved in the setop operation is referenced by other subqueries.

GAUSS-01533: "only UNION queries can be recursive"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01534: "could not implement recursive UNION"

SQLSTATE: 0A000

Description: The recursive union is not executed.

Solution: Adjust corresponding columns so that all of them can be distributed in hash mode.

GAUSS-01535: "could not implement %s"

SQLSTATE: 0A000

Description:**UNION**, **INTERSECT**, or **EXCEPT** is executed.

Solution: Ensure that all columns related to **UNION**, **INTERSECT**, or **EXCEPT** can be distributed in both hash mode and sort mode.

GAUSS-01536: "could not find inherited attribute '%s' of relation '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01537: "attribute '%s' of relation '%s' does not match parent's type"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01538: "attribute '%s' of relation '%s' does not match parent's collation"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01539: "attribute %d of relation '%s' does not exist"

SQLSTATE: XX000

Description: The column does not exist.

Solution: Query existing columns only.

GAUSS-01540: "failed to join all relations together"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01541 - GAUSS-01550

<br/>

GAUSS-01541: "minimum_count not set"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01542: "neither shared nor minimum number nor random edge found"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01543: "no edge found"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01547: "could not find plan for CTE '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01549: "failed to build any %d-way joins"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01551 - GAUSS-01560

<br/>

GAUSS-01551: "a join rel requires both the left path and right path"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01552: "can not create remote path for ranges of type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01553: "can not create remote path for relation of type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01554: "No node list provided for remote query path"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01557: "operator %u is not a valid ordering operator"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01558: "volatile EquivalenceClass has no sortref"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01561 - GAUSS-01570

<br/>

GAUSS-01563: "wrong number of index expressions"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01564: "unsupported indexqual type: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01566: "could not find member %d(%u,%u) of opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01570: "no = operator for opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01571 - GAUSS-01580

<br/>

GAUSS-01571: "no >= operator for opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01572: "no < operator for opfamily %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01576: "unexpected datatype in string_to_const: %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01577: "left and right pathkeys do not match in mergejoin"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01579: "Unrecognised command type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01580: "Postgres-XC does not support this distribution type yet"

SQLSTATE: 0A000

Description: The redistribution data type is not supported.

Solution: Currently, data of some types, such as float and double, cannot be redistributed. Modify the data types in this column.

<br/>

## GAUSS-01581 - GAUSS-01590

<br/>

GAUSS-01581: "predicate_classify returned a bogus value"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01582: "too late to create a new PlaceHolderInfo"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01583: "rel %d already exists"

SQLSTATE: XX000

Description: The table already exists.

Solution: Rename the table.

GAUSS-01584: "no relation entry for relid %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01585: "unexpected node type in reltargetlist: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01587: "cannot access temporary or unlogged relations during recovery"

SQLSTATE: 0A000

Description: Internal system error. During recovery, the temporary table or unlogged table cannot be accessed.

Solution:Contact technical support.

GAUSS-01588: "unsupported RTE kind %d in build_physical_tlist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01589: "invalid restriction selectivity: %f"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01590: "invalid join selectivity: %f"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01591 - GAUSS-01600

<br/>

GAUSS-01591: "WindowFunc contains out-of-range winref %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01592: "cannot commute non-binary-operator clause"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01593: "could not find commutator for operator %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01595: "unexpected RowCompare type: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01596: "unrecognized nulltesttype: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01598: "function's resolved result type changed during planning"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01599: "null prosrc for function %u"

SQLSTATE: 42P13

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01600: "unexpected paramkind: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.
