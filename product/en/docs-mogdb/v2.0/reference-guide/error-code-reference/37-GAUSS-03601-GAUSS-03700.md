---
title: GAUSS-03601 - GAUSS-03700
summary: GAUSS-03601 - GAUSS-03700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03601 - GAUSS-03700

<br/>

## GAUSS-03601 - GAUSS-03610

<br/>

GAUSS-03603: "number of index columns (%d) exceeds limit (%d)"

SQLSTATE: 54011

Description: The number of index tuples exceeds the upper limit 32.

Solution: Reduce the number of columns that require row-store indexes.

GAUSS-03604: "index row requires %lu bytes, maximum size is %lu"

SQLSTATE: 54000

Description: The length of a single index tuple exceeds the size of a page, which is caused by excessively long data in the index column.

Solution: Do not create indexes for excessively long columns.

GAUSS-03605: "column '%s' cannot be applied %s compress mode"

SQLSTATE: 42P16

Description: The column does not support the prompted compression mode.

Solution: Select the correct compression mode for the column.

GAUSS-03606: "unsupported format code: %d"

SQLSTATE: 22023

Description: The type is not supported.

Solution: Check the column type and perform the operation again.

GAUSS-03607: "invalid attnum: %d"

SQLSTATE: XX000

Description: A system column involved in the operation does not exist.

Solution: If the system column query is initiated by the user, check whether the query of the system column is supported. If the query is supported, this error is an internal error. Contact technical support.

GAUSS-03608: "number of columns (%d) exceeds limit (%d)"

SQLSTATE: 54011

Description: The number of columns exceeds the upper limit.

Solution: Reduce the number of columns and perform the operation again.

GAUSS-03609: "Tuple does not match the descriptor"

SQLSTATE: XX001

Description: The number of columns in the tuple is different from the actual column quantity.

Solution: Runtime error. Contact technical support.

GAUSS-03610: "cannot extract system attribute from virtual tuple"

SQLSTATE: XX000

Description: An internal error occurs. The system column cannot be extracted from the virtual tuple.

Solution: Runtime error. Contact technical support.

<br/>

## GAUSS-03611 - GAUSS-03620

<br/>

GAUSS-03611: "cannot extract system attribute from minimal tuple"

SQLSTATE: XX000

Description: An internal error occurs. The system column cannot be extracted from the virtual tuple.

Solution: Runtime error. Contact technical support.

GAUSS-03613: "user-defined relation parameter types limit exceeded"

SQLSTATE: 54000

Description: The definition of relation parameter types exceeds the upper limit.

Solution: Redefine the relation parameter types.

GAUSS-03614: "unsupported option type"

SQLSTATE: XX000

Description: The table definition options contain unsupported data types. Supported data types include boolean, integer, floating point, and string.

Solution: Specify valid data types.

GAUSS-03615: "RESET must not include values for parameters"

SQLSTATE: 42601

Description: The **RESET** command does not support a specified parameter value.

Solution: Check the **RESET** command and run this command to reset the default value.

GAUSS-03616: "unrecognized parameter namespace '%s'"

SQLSTATE: 22023

Description: Failed to recognize the parameter namespace.

Solution: Ensure that the parameter namespace exists and perform the operation again.

GAUSS-03617: "unrecognized parameter '%s'"

SQLSTATE: 22023

Description: Failed to recognize the parameter.

Solution: Ensure that the parameter exists and perform the operation again.

GAUSS-03618: "parameter '%s' specified more than once"

SQLSTATE: 22023

Description: The parameter is specified more than once.

Solution: Check the parameter usage and perform the operation again.

GAUSS-03619: "invalid value for boolean option '%s': %s"

SQLSTATE: XX000

Description: The data of the boolean type is invalid.

Solution: Ensure that the data of the boolean type is valid and perform the operation again.

GAUSS-03620: "invalid value for integer option '%s': %s"

SQLSTATE: XX000

Description: The data of the INT type is invalid.

Solution: Ensure that the data of the INT type is valid and perform the operation again.

<br/>

## GAUSS-03621 - GAUSS-03630

<br/>

GAUSS-03621: "value %s out of bounds for option '%s'"

SQLSTATE: XX000

Description: The data value is out of range.

Solution: Ensure that the data value is within the range and perform the operation again.

GAUSS-03622: "invalid value for floating point option '%s': %s"

SQLSTATE: XX000

Description: The data of the FLOAT type is invalid.

Solution: Ensure that the data of the FLOAT type is valid and perform the operation again.

GAUSS-03623: "unsupported reloption type %d"

SQLSTATE: XX000

Description: The table definition options contain unsupported data types. Supported data types include boolean, integer, floating point, and string.

Solution: Specify valid data types.

GAUSS-03624: "unrecognized reloption type %c"

SQLSTATE: XX000

Description: The table definition options contain unsupported data types. Supported data types include boolean, integer, floating point, and string.

Solution: Specify valid data types.

GAUSS-03625: "reloption '%s' not found in parse table"

SQLSTATE: XX000

Description: A table option specified during table creation does not exist.

Solution: Specify valid options.

GAUSS-03626: "Invalid string for 'ORIENTATION' option"

SQLSTATE: 22023

Description: The parameter of the ORIENTATION option is invalid.

Solution: Ensure that the parameter of the ORIENTATION option is valid and perform the operation again.

GAUSS-03627: "Invalid string for 'COMPRESSION' option"

SQLSTATE: 22023

Description: The parameter of the COMPRESSION option is invalid.

Solution: Ensure that the parameter of the COMPRESSION option is valid and perform the operation again.

GAUSS-03628: "unrecognized StrategyNumber: %d"

SQLSTATE: XX000

Description: The strategy is incorrect.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03629: "missing oprcode for operator %u"

SQLSTATE: XX000

Description:**oprcode** is invalid.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03630: "btree index keys must be ordered by attribute"

SQLSTATE: XX000

Description: The index attribute column is less than 1.

Solution: Ensure that the B-tree index is correctly created.

<br/>

## GAUSS-03631 - GAUSS-03640

<br/>

GAUSS-03631: "multiple active vacuums for index '%s'"

SQLSTATE: XX000

Description: Multiple VACUUM operations are performed on an index.

Solution: Do not perform multiple VACUUM operations on a B-tree index at the same time.

GAUSS-03632: "out of btvacinfo slots"

SQLSTATE: XX000

Description: The number of VACUUM operations exceeds the maximum value.

Solution: Perform this operation after other VACUUM operations are complete.

GAUSS-03633: "index '%s' is not a btree"

SQLSTATE: XX002

Description: This node is not the root of the B-tree index.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03635: "no live root page found in index '%s'"

SQLSTATE: XX000

Description: The B-tree index has no root node.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03636: "root page %u of index '%s' has level %u, expected %u"

SQLSTATE: XX000

Description: The root page of the index contains incorrect level information.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03637: "failed to re-find parent key in index '%s' for deletion target page %u"

SQLSTATE: XX000

Description: Failed to find the parent node during the deletion.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03638: "not enough stack items"

SQLSTATE: XX000

Description:Internal system error.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03639: "left link changed unexpectedly in block %u of index '%s'"

SQLSTATE: XX000

Description: The page number of the left link is changed.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03640: "right sibling's left-link doesn't match: block %u links to %u instead of expected %u in index '%s'"

SQLSTATE: XX000

Description: The B-tree index structure is incorrect. The left link of the right sibling does not match.

Solution: A preventive error mechanism. Contact technical support.

<br/>

## GAUSS-03641 - GAUSS-03650

<br/>

GAUSS-03641: "failed to delete rightmost child %u of block %u in index '%s'"

SQLSTATE: XX000

Description: Failed to delete a node.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03642: "right sibling %u of block %u is not next child %u of block %u in index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03643: "fell off the end of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03644: "missing support function %d(%u,%u) for attribute %d of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03646: "could not find left sibling of block %u in index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03647: "btree level %u not found in index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03648: "invalid scan direction: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03649: "duplicate key value violates unique constraint '%s'"

SQLSTATE: 23505

Description: The inserted index value conflicts with the existing index values.

Solution: Delete the unique constraint and perform the operation again.

GAUSS-03650: "failed to re-find tuple within index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03651 - GAUSS-03660

<br/>

GAUSS-03651: "failed to add new item to block %u in index '%s'"

SQLSTATE: XX000

Description: Failed to insert the record to the specified index page.

Solution:Contact technical support.

GAUSS-03652: "failed to add hikey to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03653: "failed to add hikey to the left sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03655: "failed to add new item to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03656: "failed to add old item to the left sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03657: "failed to add old item to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03658: "could not find a feasible split point for index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03659: "failed to re-find parent key in index '%s' for split pages %u/%u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03660: "failed to add leftkey to new root page while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03661 - GAUSS-03670

<br/>

GAUSS-03661: "failed to add rightkey to new root page while splitting block %u of index '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03662: "failed to add item to the index page"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03663: "index compare error, both are NULL"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03664: "_bt_restore_page: cannot add item to page"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03665: "btree_insert_redo: failed to add item"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03666: "failed to add new item to left page after split"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03667: "failed to add high key to left page after split"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03670: "btree_xlog_cleanup: left block unfound"

SQLSTATE: XX000

Description: Failed to find the left block.

Solution: A preventive error mechanism. Contact technical support.

<br/>

## GAUSS-03671 - GAUSS-03680

<br/>

GAUSS-03671: "btree_xlog_cleanup: right block unfound"

SQLSTATE: XX000

Description: Failed to find the right block.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03672: "btree_xlog_cleanup: _bt_pagedel failed"

SQLSTATE: XX000

Description: Failed to delete the B-tree page.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03673: "unlogged GiST indexes are not supported"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03674: "concurrent GiST page split was incomplete"

SQLSTATE: XX000

Description: The split is incomplete.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03675: "index '%s' contains an inner tuple marked as invalid"

SQLSTATE: XX000

Description: The internal index contains invalid tuples.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03676: "failed to re-find parent of a page in index '%s', block %u"

SQLSTATE: XX000

Description: Failed to find the parent node page.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03677: "numberOfAttributes %d > %d"

SQLSTATE: XX000

Description: The number of attribute columns in the index table exceeds the maximum value.

Solution: Reduce the number of attribute columns in the index.

GAUSS-03678: "invalid GiST tuple found on leaf page"

SQLSTATE: XX000

Description: The GIST page is incorrect.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03679: "GiST only supports forward scan direction"

SQLSTATE: XX000

Description: Only the forward direction is supported.

Solution: Internal system error. Check the scanning direction and the second parameter.

<br/>

## GAUSS-03681 - GAUSS-03690

<br/>

GAUSS-03683: "invalid value for 'buffering' option"

SQLSTATE: 22023

Description: The option of **BufferingOption** is not **on**, **off**, or **auto**.

Solution: Internal system error. Check the **BufferingOption** option.

GAUSS-03685: "failed to re-find parent for block %u"

SQLSTATE: XX000

Description: Failed to find the parent node page.

Solution: A preventive error mechanism. Contact technical support.

GAUSS-03687: "gistmerge: unimplemented"

SQLSTATE: XX000

Description: Failed to perform the merge operation for GIST indexes.

Solution: Do not perform this operation because the function is not supported.

GAUSS-03688: "failed to add item to GiST index page, size %d bytes"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03690: "GiST does not support mark/restore"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03691 - GAUSS-03700

<br/>

GAUSS-03691: "inconsistent point values"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03692: "unknown strategy number: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03693: "unrecognized strategy number: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03694: "index '%s' is not an SP-GiST index"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03695: "desired SPGiST tuple size is too big"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03696: "SP-GiST inner tuple size %lu exceeds maximum %lu"

SQLSTATE: 54000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03697: "SPGiST inner tuple header field is too small"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03698: "some but not all node labels are null in SPGiST inner tuple"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03699: "failed to add item of size %u to SPGiST index page"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.
