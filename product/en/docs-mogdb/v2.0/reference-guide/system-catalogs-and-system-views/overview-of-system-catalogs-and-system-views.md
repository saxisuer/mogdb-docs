---
title: Overview of System Catalogs and System Views
summary: Overview of System Catalogs and System Views
author: Guo Huan
date: 2021-04-19
---

# Overview of System Catalogs and System Views

System catalogs store structured metadata of MogDB. They are the source of information used by MogDB to control system running and are a core component of the database system.

System views provide ways to query the system catalogs and internal database status.

System catalogs and views are visible to either system administrators or all users. Some system catalogs and views have marked the need of administrator permissions, so they are accessible only to administrators.

You can delete and re-create system catalogs, add columns to them, and insert and update values in them, but doing so may make system information inconsistent and cause system faults. Generally, users should not modify system catalogs or system views, or rename their schemas. They are automatically maintained by the system.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Do not add, delete, or modify system catalogs because doing so will result in exceptions or even MogDB unavailability.
