---
title: PG_SHDESCRIPTION
summary: PG_SHDESCRIPTION
author: Guo Huan
date: 2021-04-19
---

# PG_SHDESCRIPTION

**PG_SHDESCRIPTION** records optional comments for shared database objects. Descriptions can be manipulated with the **COMMENT** command and viewed with psql's **\d** commands.

See also **PG_DESCRIPTION**, which provides a similar function for descriptions involving objects within a single database.

Unlike most system catalogs, **PG_SHDESCRIPTION** is shared across all databases of a MogDB. There is only one copy of **PG_SHDESCRIPTION** per MogDB, not one per database.

**Table 1** PG_SHDESCRIPTION columns

| Name        | Type | Reference      | Description                                                 |
| :---------- | :--- | :------------- | :---------------------------------------------------------- |
| objoid      | oid  | Any OID column | OID of the object that this description pertains to         |
| classoid    | oid  | PG_CLASS.oid   | OID of the system catalog where the object appears          |
| description | text | -              | Arbitrary text that serves as the description of the object |
