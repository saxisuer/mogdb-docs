---
title: PG_INDEX
summary: PG_INDEX
author: Guo Huan
date: 2021-04-19
---

# PG_INDEX

**PG_INDEX** records part of index information. The rest is mostly recorded in **PG_CLASS**.

**Table 1** PG_INDEX columns

| Name           | Type         | Description                                                  |
| :------------- | :----------- | :----------------------------------------------------------- |
| indexrelid     | oid          | OID of the **PG_CLASS** entry for this index                 |
| indrelid       | oid          | OID of the **PG_CLASS** entry for the table that uses this index |
| indnatts       | smallint     | Number of columns in the index                               |
| indisunique    | Boolean      | Unique index if the value is **true**                        |
| indisprimary   | Boolean      | Primary key of the table if the value is **true**. **indisunique** should always be **true** when the value of this column is **true**. |
| indisexclusion | Boolean      | Whether the index supports exclusion constraints             |
| indimmediate   | Boolean      | Whether a uniqueness check is performed upon data insertion  |
| indisclustered | Boolean      | Whether the table was last clustered on this index           |
| indisusable    | Boolean      | Whether the index supports insert/select                     |
| indisvalid     | Boolean      | Whether the index is valid for queries. If this column is **false**, the index is possibly incomplete and must still be modified by **INSERT/UPDATE** operations, but it cannot safely be used for queries. If it is a unique index, the uniqueness property is also not **true**. |
| indcheckxmin   | Boolean      | If the value is **true**, queries must not use the index until the xmin of this row in **PG_INDEX** is below their **TransactionXmin**, because the table may contain broken HOT chains with incompatible rows that they can see. |
| indisready     | Boolean      | The index is available for inserted data if the value is **true**. Otherwise, this index is ignored when data is inserted or modified. |
| indkey         | int2vector   | This is an array of **indnatts** values indicating that this index creates table columns. For example, a value of **1 3** indicates that the first and the third columns make up the index key. The value **0** in this array indicates that the corresponding index attribute is an expression over the table columns, rather than a simple column reference. |
| indcollation   | oidvector    | ID of each column used by the index                          |
| indclass       | oidvector    | For each column in the index key, this contains the OID of the operator class to use. See **PG_OPCLASS** for details. |
| indoption      | int2vector   | Array of values that store per-column flag bits. The meaning of the bits is defined by the index's access method. |
| indexprs       | pg_node_tree | Expression trees (in **nodeToString()** representation) for index attributes that are not simple column references. It is a list with one element for each zero entry in **INDKEY**. The value is **NULL** if all index attributes are simple references. |
| indpred        | pg_node_tree | Expression tree (in **nodeToString()** representation) for partial index predicate. If the index is not a partial index, this column is an empty string. |
| indisreplident | Boolean      | If the value is **true**, the column of this index becomes the decoded column of logical decoding. |
