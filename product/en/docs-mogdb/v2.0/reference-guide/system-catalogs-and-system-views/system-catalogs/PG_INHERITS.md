---
title: PG_INHERITS
summary: PG_INHERITS
author: Guo Huan
date: 2021-04-19
---

# PG_INHERITS

**PG_INHERITS** records information about table inheritance hierarchies. There is one entry for each direct child table in the database. Indirect inheritance can be determined by following chains of entries.

**Table 1** PG_INHERITS columns

| Name      | Type    | Reference    | Description                                                  |
| :-------- | :------ | :----------- | :----------------------------------------------------------- |
| inhrelid  | oid     | PG_CLASS.oid | OID of a child table                                         |
| inhparent | oid     | PG_CLASS.oid | OID of a parent table                                        |
| inhseqno  | integer | -            | If there is more than one direct parent for a child table (multiple inheritances), this number tells the order in which the inherited columns are to be arranged. The count starts at 1. |
