---
title: GS_MATVIEW_DEPENDENCY
summary: GS_MATVIEW_DEPENDENCY
author: Guo Huan
date: 2021-06-07
---

# GS_MATVIEW_DEPENDENCY

**GS_MATVIEW_DEPENDENCY** provides association information about the base table and mlog table of each materialized view in the database.

**Table 1** GS_MATVIEW_DEPENDENCY columns

| Name      | Type | Description                                                  |
| :-------- | :--- | :----------------------------------------------------------- |
| matviewid | oid  | OID of a materialized view.                                  |
| relid     | oid  | OID of a base table of a materialized view.                  |
| mlogid    | oid  | OID of a mlog table which is the log table of a materialized view. Each mlog table corresponds to one base table. |
| mxmin     | int4 | Prevents tuples deleted from mlog from being cleared by the vacuum operation. |
