---
title: PG_AMOP
summary: PG_AMOP
author: Guo Huan
date: 2021-04-19
---

# PG_AMOP

**PG_AMOP** records information about operators associated with access method operator families. There is one row for each operator that is a member of an operator family. A family member can be either a search operator or an ordering operator. An operator can appear in more than one family, but cannot appear in more than one search position nor more than one ordering position within a family.

**Table 1** PG_AMOP columns

| Name           | Type     | Reference       | Description                                                  |
| :------------- | :------- | :-------------- | :----------------------------------------------------------- |
| oid            | oid      | -               | Row identifier (hidden attribute, which must be specified)   |
| amopfamily     | oid      | PG_OPFAMILY.oid | Operator family of this entry                                |
| amoplefttype   | oid      | PG_TYPE.oid     | Left-hand input data type of the operator                    |
| amoprighttype  | oid      | PG_TYPE.oid     | Right-hand input data type of the operator                   |
| amopstrategy   | smallint | -               | Number of operator strategies                                |
| amoppurpose    | "char"   | -               | Operator purpose, either **s** for search or **o** for ordering |
| amopopr        | oid      | PG_OPERATOR.oid | OID of the operator                                          |
| amopmethod     | oid      | PG_AM.oid       | Operator family of the index access method                   |
| amopsortfamily | oid      | PG_OPFAMILY.oid | The B-tree operator family according to which this entry sorts for an ordering operator (**0** for a search operator) |

A search operator entry indicates that an index of this operator family can be searched to find all rows satisfying **WHERE indexed_column operator constant**. Obviously, such an operator must return a Boolean value, and its left-hand input type must match the index's column data type.

An ordering operator entry indicates that an index of this operator family can be scanned to return rows in the order represented by **ORDER BY indexed_column operator constant**. Such an operator could return any sortable data type, though again its left-hand input type must match the index's column data type. The exact semantics of **ORDER BY** are specified by the **amopsortfamily** column, which must reference the B-tree operator family for the operator's result type.
