---
title: PG_FOREIGN_TABLE
summary: PG_FOREIGN_TABLE
author: Guo Huan
date: 2021-04-19
---

# PG_FOREIGN_TABLE

**PG_FOREIGN_TABLE** records auxiliary information about foreign tables.

**Table 1** PG_FOREIGN_TABLE columns

| Name        | Type    | Description                                      |
| :---------- | :------ | :----------------------------------------------- |
| ftrelid     | oid     | ID of a foreign table                            |
| ftserver    | oid     | Server where the foreign table is located        |
| ftwriteonly | Boolean | Whether data can be written in the foreign table |
| ftoptions   | text[]  | Foreign table options                            |
