---
title: GS_AUDITING_POLICY_FILTERS
summary: GS_AUDITING_POLICY_FILTERS
author: Guo Huan
date: 2021-06-15
---

# GS_AUDITING_POLICY_FILTERS

**GS_AUDITING_POLICY_FILTERS** records the filtering policies about the unified audit. Each record corresponds to a design policy. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_AUDITING_POLICY_FILTERS columns

| Name            | Type      | Description                                                  |
| :-------------- | :-------- | :----------------------------------------------------------- |
| filtertype      | name      | Filter type. Currently, the value is **logical_expr**.       |
| labelname       | name      | Name. Currently, the value is **logical_expr**.              |
| policyoid       | oid       | This parameter corresponds to OIDs in the **GS_AUDITING_POLICY** system catalog. |
| modifydate      | timestamp | The latest creation or modification timestamp.               |
| logicaloperator | text      | Logical character string of a filter criterion.              |
