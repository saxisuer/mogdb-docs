---
title: GS_MASKING_POLICY_FILTERS
summary: GS_MASKING_POLICY_FILTERS
author: Guo Huan
date: 2021-06-15
---

# GS_MASKING_POLICY_FILTERS

**GS_MASKING_POLICY_FILTERS** records the user filtering criteria corresponding to the dynamic data masking policies. The corresponding masking policy takes effect only when the user information meets the filter criteria. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_MASKING_POLICY_FILTERS columns

| Name            | Type      | Description                                                  |
| :-------------- | :-------- | :----------------------------------------------------------- |
| filtertype      | name      | Filter type. Currently, the value is **logical_expr**.       |
| filterlabelname | name      | Filtering range. Currently, the value is **logical_expr**.   |
| policyoid       | oid       | OID of a masking policy to which the user filtering criteria belong. |
| modifydate      | timestamp | The latest timestamp when a user filter criterion is created or modified. |
| logicaloperator | text      | Polish notation of the filter criteria.                      |
