---
title: GS_WLM_INSTANCE_HISTORY
summary: GS_WLM_INSTANCE_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_INSTANCE_HISTORY

**GS_WLM_INSTANCE_HISTORY** records information about resource usage related to instances (primary database node or database node). Each record in this system catalog indicates resource usage of an instance at a specific time point, including the memory, number of CPU cores, disk I/O, physical I/O of the process, and logical I/O of the process.

**Table 1** GS_WLM_INSTANCE_HISTORY columns

| Name          | Type                     | Description                                                  |
| :------------ | :----------------------- | :----------------------------------------------------------- |
| instancename  | text                     | Instance name                                                |
| timestamp     | timestamp with time zone | Timestamp                                                    |
| used_cpu      | int                      | CPU usage of the instance                                    |
| free_mem      | int                      | Unused memory of the instance, in MB                         |
| used_mem      | int                      | Used memory of the instance, in MB                           |
| io_await      | real                     | Average wait time for an I/O operation on the disk used by the instance. The average value is within 10 seconds. |
| io_util       | real                     | io_util value of the disk used by the instance. The average value is within 10 seconds. |
| disk_read     | real                     | Disk read rate of the instance, in KB/s. The average value is within 10 seconds. |
| disk_write    | real                     | Disk write rate of the instance, in KB/s. The average value is within 10 seconds. |
| process_read  | bigint                   | Read rate (excluding the number of bytes read from the disk pagecache) of the corresponding instance process that reads data from a disk within 10 seconds, in KB/s |
| process_write | bigint                   | Write rate (excluding the number of bytes written to the disk pagecache) of the corresponding instance process that writes data to a disk within 10 seconds, in KB/s |
| logical_read  | bigint                   | Primary database node instance: not collected<br/>Database node instance: logical read byte rate of the instance within the statistical interval (10 seconds), in KB/s |
| logical_write | bigint                   | Primary database node instance: not collected<br/>Database node instance: logical write byte rate of the instance within the statistical interval (10 seconds), in KB/s |
| read_counts   | bigint                   | Primary database node instance: not collected<br/>Database node instance: total number of logical read operations of the instance within the statistical interval (10 seconds) |
| write_counts  | bigint                   | Primary database node instance: not collected<br/>Database node instance: total number of logical write operations of the instance within the statistical interval (10 seconds) |
