---
title: GS_AUDITING_POLICY
summary: GS_AUDITING_POLICY
author: Guo Huan
date: 2021-06-15
---

# GS_AUDITING_POLICY

**GS_AUDITING_POLICY** records the main information about the unified audit. Each record corresponds to a design policy. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_AUDITING_POLICY columns

| Name        | Type                        | Description                                                  |
| :---------- | :-------------------------- | :----------------------------------------------------------- |
| polname     | name                        | Policy name, which must be unique                            |
| polcomments | name                        | Policy description field, which records policy-related description information and is represented by the **COMMENTS** keyword |
| modifydate  | timestamp without time zone | The latest timestamp when a policy is created or modified    |
| polenabled  | boolean                     | Specifies whether to enable the policy.                      |
