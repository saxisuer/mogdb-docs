---
title: PG_DEPEND
summary: PG_DEPEND
author: Guo Huan
date: 2021-04-19
---

# PG_DEPEND

**PG_DEPEND** records the dependency between database objects. This information allows **DROP** commands to find which other objects must be dropped by **DROP CASCADE** or prevent dropping in the **DROP RESTRICT** case.

See also PG_SHDEPEND, which performs a similar function for dependencies involving objects that are shared across MogDB.

**Table 1** PG_DEPEND columns

| Name        | Type    | Reference      | Description                                                  |
| :---------- | :------ | :------------- | :----------------------------------------------------------- |
| classid     | oid     | PG_CLASS.oid   | OID of the system catalog where a dependent object resides   |
| objid       | oid     | Any OID column | OID of the dependent object                                  |
| objsubid    | integer | -              | Column number for a table column (**objid** and **classid** refer to the table itself); The value is **0** for all other object types. |
| refclassid  | oid     | PG_CLASS.oid   | OID of the system catalog where a referenced object resides  |
| refobjid    | oid     | Any OID column | OID of the referenced object                                 |
| refobjsubid | integer | -              | Column number for a table column (**refobjid** and **refclassid** refer to the table itself); **0** for all other object types |
| deptype     | "char"  | -              | A code defining the specific semantics of this dependency    |

In all cases, a **PG_DEPEND** entry indicates that the referenced object cannot be dropped without also dropping the dependent object. However, there are several subflavors identified by **deptype**:

- DEPENDENCY_NORMAL (n): A normal relationship between separately created objects. The dependent object can be dropped without affecting the referenced object. The referenced object can only be dropped by specifying **CASCADE**, in which case the dependent object is dropped too. Example: a table column has a normal dependency on its data type.
- DEPENDENCY_AUTO (a): The dependent object can be dropped separately from the referenced object, and should be automatically dropped (regardless of RESTRICT or CASCADE mode) if the referenced object is dropped. Example: a named constraint on a table is made autodependent on the table, so that it will go away if the table is dropped.
- DEPENDENCY_INTERNAL (i): The dependent object was created as part of creation of the referenced object, and is only a part of its internal implementation. A DROP of the dependent object will be disallowed outright (We'll tell the user to issue a DROP against the referenced object, instead). A **DROP** of the referenced object will be propagated through to drop the dependent object whether **CASCADE** is specified or not.
- DEPENDENCY_EXTENSION (e): The dependent object is a member of the extension of the referenced object (see PG_EXTENSION). The dependent object can be dropped only via **DROP EXTENSION** on the referenced object. Functionally this dependency type acts the same as an internal dependency, but it is kept separate for clarity and to simplify **GS_DUMP**.
- DEPENDENCY_PIN (p): There is no dependent object; this type of entry is a signal that the system itself depends on the referenced object, and so that object must never be deleted. Entries of this type are created only by **initdb**. The columns for the dependent object contain zeroes.
