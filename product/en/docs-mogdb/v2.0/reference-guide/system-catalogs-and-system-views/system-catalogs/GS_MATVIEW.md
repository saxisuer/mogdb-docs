---
title: GS_MATVIEW
summary: GS_MATVIEW
author: Guo Huan
date: 2021-06-15
---

# GS_MATVIEW

**GS_MATVIEW** provides information about each materialized view in the database.

**Table 1** GS_MATVIEW columns

| Name        | Type      | Description                                                  |
| :---------- | :-------- | :----------------------------------------------------------- |
| matviewid   | oid       | OID of a materialized view.                                  |
| mapid       | oid       | OID of a map table associated with a materialized view. Each map table corresponds to one materialized view. |
| ivm         | boolean   | Type of a materialized view. The value **t** indicates an incremental materialized view, and the value **f** indicates a full materialized view. |
| needrefresh | boolean   | Specifies whether a materialized view needs to be refreshed. The value **t** indicates that incremental data needs to be refreshed. |
| refreshtime | timestamp | Last time when a materialized view is refreshed. If the materialized view is not refreshed, the value is null. |
