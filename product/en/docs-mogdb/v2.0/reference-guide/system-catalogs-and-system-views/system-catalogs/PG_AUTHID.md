---
title: PG_AUTHID
summary: PG_AUTHID
author: Guo Huan
date: 2021-04-19
---

# PG_AUTHID

**PG_AUTHID** records information about database authentication identifiers (roles). The concept of users is contained in that of roles. A user is actually a role whose **rolcanlogin** has been set. Any role, whether its **rolcanlogin** is set or not, can use other roles as members.

For MogDB, only one **PG_AUTHID** exists, which is not available for every database. This system catalog is accessible only to system administrators.

**Table 1** PG_AUTHID columns

| Name           | Type                     | Description                                                  |
| :------------- | :----------------------- | :----------------------------------------------------------- |
| oid            | oid                      | Row identifier (hidden attribute, which must be specified)   |
| rolname        | name                     | Specifies the name of a role.                                |
| rolsuper       | Boolean                  | Whether the role is the initial system administrator with the highest permission |
| rolinherit     | Boolean                  | Whether the role automatically inherits permissions of roles of which it is a member |
| rolcreaterole  | Boolean                  | Whether the role can create more roles                       |
| rolcreatedb    | Boolean                  | Whether the role can create databases                        |
| rolcatupdate   | Boolean                  | Whether the role can directly update system catalogs. Only the initial system administrator whose **usesysid** is **10** has this permission. It is unavailable for other users. |
| rolcanlogin    | Boolean                  | Whether the role can log in (whether this role can be given as the initial session authorization identifier) |
| rolreplication | Boolean                  | Indicates that the role is a replicated one (an adaptation syntax and no actual meaning). |
| rolauditadmin  | Boolean                  | Indicates that the role is an audit user.                    |
| rolsystemadmin | Boolean                  | Username of the administrator                                |
| rolconnlimit   | integer                  | Maximum number of concurrent connections that this role can make (valid for roles that can log in)<br/>The value **-1** means no limit. |
| rolpassword    | text                     | Password (possibly encrypted) (null if no password)          |
| rolvalidbegin  | timestamp with time zone | Account validity start time (null if no start time)          |
| rolvaliduntil  | timestamp with time zone | Password expiry time (null if no expiration)                 |
| rolrespool     | name                     | Resource pool that a user can use                            |
| roluseft       | Boolean                  | Whether the role can perform operations on foreign tables    |
| rolparentid    | oid                      | OID of a group user to which the user belongs                |
| roltabspace    | text                     | Maximum size of a user data table                            |
| rolkind        | char                     | Special user types, including private users and common users |
| rolnodegroup   | oid                      | Unsupported currently                                        |
| roltempspace   | text                     | Storage space of the user temporary table                    |
| rolspillspace  | text                     | Maximum size of data that can be written to disks when a user executes a job |
| rolexcpdata    | text                     | Query rules that can be set by users (reserved)              |
