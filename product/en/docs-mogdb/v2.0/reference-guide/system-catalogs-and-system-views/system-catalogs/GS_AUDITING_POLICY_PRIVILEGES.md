---
title: GS_AUDITING_POLICY_PRIVILEGES
summary: GS_AUDITING_POLICY_PRIVILEGES
author: Guo Huan
date: 2021-06-07
---

# GS_AUDITING_POLICY_PRIVILEGES

**GS_AUDITING_POLICY_PRIVILEGES** records the DDL database operations about the unified audit. Each record corresponds to a design policy. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_AUDITING_POLICY_PRIVI columns

| Name          | Type      | Description                                                  |
| :------------ | :-------- | :----------------------------------------------------------- |
| privilegetype | name      | DDL database operation type. For example, CREATE, ALTER, and DROP. |
| labelname     | name      | Specifies the resource label name. This parameter corresponds to the **polname** column in the **GS_AUDITING_POLICY** system catalog. |
| policyoid     | oid       | This parameter corresponds to OIDs in the **GS_AUDITING_POLICY** system catalog. |
| modifydate    | timestamp | Latest creation or modification timestamp.                   |
