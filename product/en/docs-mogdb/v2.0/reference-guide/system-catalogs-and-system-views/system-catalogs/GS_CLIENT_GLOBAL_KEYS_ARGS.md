---
title: GS_CLIENT_GLOBAL_KEYS_ARGS
summary: GS_CLIENT_GLOBAL_KEYS_ARGS
author: Guo Huan
date: 2021-04-19
---

# GS_CLIENT_GLOBAL_KEYS_ARGS

**GS_CLIENT_GLOBAL_KEYS_ARGS** records the metadata about the CMK in the encrypted equality feature. Each record corresponds to a key-value pair of the CMK.

**Table 1** GS_CLIENT_GLOBAL_KEYS_ARGS columns

| Name          | Type  | Description                    |
| :------------ | :---- | :----------------------------- |
| global_key_id | oid   | CMK OID                        |
| function_name | name  | The value is **encryption**.   |
| key           | name  | CMK metadata name              |
| value         | bytea | Value of the CMK metadata name |
