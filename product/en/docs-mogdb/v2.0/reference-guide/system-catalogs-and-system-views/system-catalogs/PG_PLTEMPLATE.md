---
title: PG_PLTEMPLATE
summary: PG_PLTEMPLATE
author: Guo Huan
date: 2021-04-19
---

# PG_PLTEMPLATE

**PG_PLTEMPLATE** records template information for procedural languages.

**Table 1** PG_PLTEMPLATE columns

| Name          | Type      | Description                                                  |
| :------------ | :-------- | :----------------------------------------------------------- |
| tmplname      | name      | Name of the language for which this template is used         |
| tmpltrusted   | Boolean   | The value is **true** if the language is considered trusted. |
| tmpldbacreate | Boolean   | The value is **true** if the language is created by the owner of the database. |
| tmplhandler   | text      | Name of the call handler function                            |
| tmplinline    | text      | Name of the anonymous block handler (**NULL** if no name of the block handler exists) |
| tmplvalidator | text      | Name of the verification function (**NULL** if no verification function is available) |
| tmpllibrary   | text      | Path of the shared library that implements languages         |
| tmplacl       | aclitem[] | Access permissions for template (not yet used)               |
