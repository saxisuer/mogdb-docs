---
title: PG_JOB_PROC
summary: PG_JOB_PROC
author: Guo Huan
date: 2021-04-19
---

# PG_JOB_PROC

**PG_JOB_PROC** records the content of each job in the **PG_JOB** table, including the PL/SQL code blocks and anonymous blocks. Storing such information in the system catalog **PG_JOB** and loading it to the shared memory will result in excessive memory usage. Therefore, such information is stored in a separate table and is retrieved when needed.

**Table 1** PG_JOB_PROC columns

| Name    | Type    | Description                                                |
| :------ | :------ | :--------------------------------------------------------- |
| oid     | oid     | Row identifier (hidden attribute, which must be specified) |
| job_oid | integer | Associated with job_id in the system catalog **PG_JOB**    |
| what    | text    | PL/SQL blocks or anonymous block that the job executes     |
