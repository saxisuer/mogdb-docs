---
title: GS_MASKING_POLICY_ACTIONS
summary: GS_MASKING_POLICY_ACTIONS
author: Guo Huan
date: 2021-06-15
---

# GS_MASKING_POLICY_ACTIONS

**GS_MASKING_POLICY_ACTIONS** records the masking actions of a masking policy in the dynamic data masking policies. One masking policy corresponds to one or more rows of records in the catalog. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_MASKING_POLICY_ACTIONS columns

| Name         | Type      | Description                                                  |
| :----------- | :-------- | :----------------------------------------------------------- |
| actiontype   | name      | Name of a masking function used by a masking policy          |
| actparams    | name      | Parameter information transferred to a masking function      |
| actlabelname | name      | Name of a masked label                                       |
| policyoid    | oid       | OID of a masking policy to which a record belongs, corresponding to OID in **GS_MASKING_POLICY**. |
| modifydate   | timestamp | The latest timestamp when a record is created or modified    |
