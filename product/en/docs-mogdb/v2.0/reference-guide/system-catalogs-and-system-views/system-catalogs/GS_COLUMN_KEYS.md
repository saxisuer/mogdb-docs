---
title: GS_COLUMN_KEYS
summary: GS_COLUMN_KEYS
author: Guo Huan
date: 2021-04-19
---

# GS_COLUMN_KEYS

**GS_COLUMN_KEYS** records information about the CEK in the encrypted equality feature. Each record corresponds to a CEK.

**Table 1** GS_COLUMN_KEYS columns

| Name                      | Type      | Description                                                  |
| :------------------------ | :-------- | :----------------------------------------------------------- |
| column_key_name           | name      | CEK name                                                     |
| column_key_distributed_id | oid       | ID obtained based on the hash value of the fully qualified domain name (FQDN) of the CEK |
| global_key_id             | oid       | A foreign key, which is the CMK OID.                         |
| key_namespace             | oid       | A namespace OID that contains this CEK                       |
| key_owner                 | oid       | CEK owner                                                    |
| create_date               | timestamp | Time when a CEK is created                                   |
| key_acl                   | aclitem[] | Access permissions that this CEK should have on creation     |
