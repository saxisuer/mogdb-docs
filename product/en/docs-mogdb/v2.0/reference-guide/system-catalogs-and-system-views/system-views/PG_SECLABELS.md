---
title: PG_SECLABELS
summary: PG_SECLABELS
author: Guo Huan
date: 2021-04-19
---

# PG_SECLABELS

**PG_SECLABELS** provides information about security labels.

**Table 1** PG_SECLABELS columns

| Name         | Type    | Reference            | Description                                                  |
| :----------- | :------ | :------------------- | :----------------------------------------------------------- |
| objoid       | oid     | Any OID column       | OID of the object that this security label pertains to       |
| classoid     | oid     | PG_CLASS.oid         | OID of the system catalog where the object appears           |
| objsubid     | integer | -                    | Column number for the security label on a table column (**objoid** and **classoid** refer to the table itself). The value is **0** for all other object types. |
| objtype      | text    | -                    | Type of object to which this label applies, as text          |
| objnamespace | oid     | PG_NAMESPACE.oid     | OID of the namespace for this object, if applicable; otherwise **NULL** |
| objname      | text    | -                    | Text-typed name of the object to which this label applies    |
| provider     | text    | PG_SECLABEL.provider | Label provider associated with the label                     |
| label        | text    | PG_SECLABEL.label    | Security label applied to the object                         |
