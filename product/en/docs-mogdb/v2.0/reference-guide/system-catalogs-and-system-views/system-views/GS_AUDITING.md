---
title: GS_AUDITING
summary: GS_AUDITING
author: Guo Huan
date: 2021-06-15
---

# GS_AUDITING

**GS_AUDITING** displays all audit information about database-related operations. Only the users with system administrator or security policy administrator permission can access this view.

**Table 1** GS_AUDITING columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| polname     | name    | Policy name, which must be unique                            |
| pol_type    | text    | Audit policy type. The value can be **access** or **privilege**. |
| polenabled  | boolean | Specifies whether to enable the policy.                      |
| access_type | bigint  | DML database operation type. For example, SELECT, INSERT, and DELETE. |
| label_name  | name    | Specifies the resource label name. This parameter corresponds to the **polname** column in the **GS_AUDITING_POLICY** system catalog. |
| priv_object | name    | Describes the path of the database asset.                    |
| filter_name | text    | Logical character string of a filter criterion.              |
