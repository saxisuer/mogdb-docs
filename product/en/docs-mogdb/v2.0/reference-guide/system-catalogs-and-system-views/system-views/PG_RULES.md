---
title: PG_RULES
summary: PG_RULES
author: Guo Huan
date: 2021-04-19
---

# PG_RULES

**PG_RULES** provides access to query useful information about rewrite rules.

**Table 1** PG_RULES columns

| Name       | Type | Description                                        |
| :--------- | :--- | :------------------------------------------------- |
| schemaname | name | Name of the schema that contains a table           |
| tablename  | name | Name of the table to which the rule applies        |
| rulename   | name | Name of a rule                                     |
| definition | text | Rule definition (a reconstructed creation command) |
