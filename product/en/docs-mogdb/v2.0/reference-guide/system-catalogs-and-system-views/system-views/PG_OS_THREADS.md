---
title: PG_OS_THREADS
summary: PG_OS_THREADS
author: Guo Huan
date: 2021-04-19
---

# PG_OS_THREADS

**PG_OS_THREADS** provides status information about all the threads under the current node.

**Table 1** PG_OS_THREADS columns

| Name          | Type                     | Description                                              |
| :------------ | :----------------------- | :------------------------------------------------------- |
| node_name     | text                     | Current node name                                        |
| pid           | bigint                   | PID of the thread running under the current node process |
| lwpid         | integer                  | Lightweight thread ID corresponding to the PID           |
| thread_name   | text                     | Thread name corresponding to the PID                     |
| creation_time | timestamp with time zone | Thread creation time corresponding to the PID            |
