---
title: PG_TIMEZONE_NAMES
summary: PG_TIMEZONE_NAMES
author: Guo Huan
date: 2021-04-19
---

# PG_TIMEZONE_NAMES

**PG_TIMEZONE_NAMES** provides all time zone names that can be recognized by **SET TIMEZONE**, along with their abbreviations, UTC offsets, and daylight saving time (DST) statuses.

**Table 1** PG_TIMEZONE_NAMES columns

| Name       | Type     | Description                                                  |
| :--------- | :------- | :----------------------------------------------------------- |
| name       | text     | Name of the time zone                                        |
| abbrev     | text     | Abbreviation of the ime zone name                            |
| utc_offset | interval | Offset from UTC                                              |
| is_dst     | Boolean  | Whether DST is used. If DST is used, the value is **true**. Otherwise, the value is **false**. |
