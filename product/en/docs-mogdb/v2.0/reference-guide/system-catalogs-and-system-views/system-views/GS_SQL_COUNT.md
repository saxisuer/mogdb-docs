---
title: GS_SQL_COUNT
summary: GS_SQL_COUNT
author: Guo Huan
date: 2021-04-19
---

# GS_SQL_COUNT

**GS_SQL_COUNT** displays statistics about five types of running statements (**SELECT**, **INSERT**, **UPDATE**, **DELETE**, and **MERGE INTO**) on the current node of the database.

- When a common user queries the **GS_SQL_COUNT** view, statistics about the current node of the user are displayed. When an administrator queries the **GS_SQL_COUNT** view, statistics about the current node of all users are displayed.
- When MogDB or the node is restarted, the statistics are cleared and will be measured again.
- The system counts when a node receives a query, including a query inside MogDB.

**Table 1** GS_SQL_COUNT columns

| Name                | Type   | Description                                               |
| :------------------ | :----- | :-------------------------------------------------------- |
| node_name           | text   | Node name                                                 |
| user_name           | text   | Username                                                  |
| select_count        | bigint | Statistical result of the **SELECT** statement            |
| update_count        | bigint | Statistical result of the **UPDATE** statement            |
| insert_count        | bigint | Statistical result of the **INSERT** statement            |
| delete_count        | bigint | Statistical result of the **DELETE** statement            |
| mergeinto_count     | bigint | Statistical result of the **MERGE INTO** statement        |
| ddl_count           | bigint | Number of DDL statements                                  |
| dml_count           | bigint | Number of DML statements                                  |
| dcl_count           | bigint | Number of DML statements                                  |
| total_select_elapse | bigint | Total response time of **SELECT** statements (unit: μs)   |
| avg_select_elapse   | bigint | Average response time of **SELECT** statements (unit: μs) |
| max_select_elapse   | bigint | Maximum response time of **SELECT** statements (unit: μs) |
| min_select_elapse   | bigint | Minimum response time of **SELECT** statements (unit: μs) |
| total_update_elapse | bigint | Total response time of **UPDATE** statements (unit: μs)   |
| avg_update_elapse   | bigint | Average response time of **UPDATE** statements (unit: μs) |
| max_update_elapse   | bigint | Maximum response time of **UPDATE** statements (unit: μs) |
| min_update_elapse   | bigint | Minimum response time of **UPDATE** statements (unit: μs) |
| total_insert_elapse | bigint | Total response time of **INSERT** statements (unit: μs)   |
| avg_insert_elapse   | bigint | Average response time of **INSERT** statements (unit: μs) |
| max_insert_elapse   | bigint | Maximum response time of **INSERT** statements (unit: μs) |
| min_insert_elapse   | bigint | Minimum response time of **INSERT** statements (unit: μs) |
| total_delete_elapse | bigint | Total response time of **DELETE** statements (unit: μs)   |
| avg_delete_elapse   | bigint | Average response time of **DELETE** statements (unit: μs) |
| max_delete_elapse   | bigint | Maximum response time of **DELETE** statements (unit: μs) |
| min_delete_elapse   | bigint | Minimum response time of **DELETE** statements (unit: μs) |
