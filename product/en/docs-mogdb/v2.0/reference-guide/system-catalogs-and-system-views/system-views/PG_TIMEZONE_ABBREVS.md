---
title: PG_TIMEZONE_ABBREVS
summary: PG_TIMEZONE_ABBREVS
author: Guo Huan
date: 2021-04-19
---

# PG_TIMEZONE_ABBREVS

**PG_TIMEZONE_ABBREVS** displays information about all available time zones.

**Table 1** PG_TIMEZONE_ABBREVS columns

| Name       | Type     | Description                                                  |
| :--------- | :------- | :----------------------------------------------------------- |
| abbrev     | text     | Time zone name abbreviation                                  |
| utc_offset | interval | Offset from UTC                                              |
| is_dst     | Boolean  | Whether DST is used. If DST is used, the value is **true**. Otherwise, the value is **false**. |
