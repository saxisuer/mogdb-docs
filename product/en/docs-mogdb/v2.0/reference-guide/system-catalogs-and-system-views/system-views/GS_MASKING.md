---
title: GS_MASKING
summary: GS_MASKING
author: Guo Huan
date: 2021-06-15
---

# GS_MASKING

**GS_MASKING** displays all configured dynamic masking policies. Only the users with system administrator or security policy administrator permission can access this view.

| Name           | Type    | Description                                              |
| :------------- | :------ | :------------------------------------------------------- |
| polname        | name    | Name of the masking policy                               |
| polenabled     | boolean | Specifies whether to enable the masking policy.          |
| maskaction     | name    | Masking function                                         |
| labelname      | name    | Name of the label to which the masking function applies. |
| masking_object | text    | Masking database resource object                         |
| filter_name    | text    | Logical expression of a filter criterion                 |
