---
title: PG_TDE_INFO
summary: PG_TDE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_TDE_INFO

**PG_TDE_INFO** provides MogDB encryption information.

**Table 1** PG_TDE_INFO columns

| Name       | Type    | Description                                                  |
| :--------- | :------ | :----------------------------------------------------------- |
| is_encrypt | Boolean | Whether to enable MogDB encryption.<br/>- **f**: Non-encryption MogDB<br/>- **t**: Encryption MogDB |
| g_tde_algo | text    | Encryption algorithm<br/>- SM4-CTR-128<br/>- AES-CTR-128     |
| remain     | text    | This is reserved.                                            |
