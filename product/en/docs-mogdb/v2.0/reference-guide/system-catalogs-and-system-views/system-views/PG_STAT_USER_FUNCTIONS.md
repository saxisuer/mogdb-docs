---
title: PG_STAT_USER_FUNCTIONS
summary: PG_STAT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_USER_FUNCTIONS

**PG_STAT_USER_FUNCTIONS** shows user-defined function status information in the namespace. (The language of the function is non-internal language.)

**Table 1** PG_STAT_USER_FUNCTIONS columns

| Name       | Type             | Description                                                  |
| :--------- | :--------------- | :----------------------------------------------------------- |
| funcid     | oid              | OID of a function                                            |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Function name                                                |
| calls      | bigint           | Number of times the function has been called                 |
| total_time | double precision | Total time spent in the function and all other functions called by it |
| self_time  | double precision | Total time spent in the function itself, excluding other functions called by it |
