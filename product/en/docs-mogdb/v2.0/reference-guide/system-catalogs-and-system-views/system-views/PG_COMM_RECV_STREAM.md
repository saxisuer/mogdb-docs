---
title: PG_COMM_RECV_STREAM
summary: PG_COMM_RECV_STREAM
author: Guo Huan
date: 2021-06-15
---

# PG_COMM_RECV_STREAM

**PG_COMM_RECV_STREAM** displays the receiving stream status of all the communication libraries for a single DN.

**Table 1** PG_COMM_RECV_STREAM columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| node_name   | text    | Node name                                                    |
| local_tid   | bigint  | ID of the thread using this stream                           |
| remote_name | text    | Name of the peer node                                        |
| remote_tid  | bigint  | Peer thread ID                                               |
| idx         | integer | Peer DN ID in the local DN                                   |
| sid         | integer | Stream ID in the physical connection                         |
| tcp_sock    | integer | TCP socket used in the stream                                |
| state       | text    | Status of the stream                                         |
| query_id    | bigint  | **debug_query_id** corresponding to the stream               |
| pn_id       | integer | **plan_node_id** of the query executed by the stream         |
| send_smp    | integer | **smpid** of the sender of the query executed by the stream  |
| recv_smp    | integer | **smpid** of the receiver of the query executed by the stream |
| recv_bytes  | bigint  | Total data volume received from the stream, in byte          |
| time        | bigint  | Current lifecycle service duration of the stream, in ms      |
| speed       | bigint  | Average receiving rate of the stream, in byte/s              |
| quota       | bigint  | Current communication quota value of the stream, in byte     |
| buff_usize  | bigint  | Current size of the data cache of the stream, in byte        |
