---
title: GS_CLUSTER_RESOURCE_INFO
summary: GS_CLUSTER_RESOURCE_INFO
author: Guo Huan
date: 2021-06-07
---

# GS_CLUSTER_RESOURCE_INFO

**GS_CLUSTER_RESOURCE_INFO** displays all DNs resource summaries. This view can be queried only when **enable_dynamic_workload** is set to **on** and the view cannot be executed on DNs. Only the user with sysadmin permission can query this view.

**Table 1** GS_CLUSTER_RESOURCE_INFO columns

| Name          | Type    | Description                             |
| :------------ | :------ | :-------------------------------------- |
| min_mem_util  | integer | Minimum memory usage of a DN            |
| max_mem_util  | integer | Maximum memory usage of a DN            |
| min_cpu_util  | integer | Minimum CPU usage of a DN               |
| max_cpu_util  | integer | Maximum CPU usage of a DN               |
| min_io_util   | integer | Minimum I/O usage of a DN               |
| max_io_util   | integer | Maximum I/O usage of a DN               |
| used_mem_rate | integer | Maximum memory usage of a physical node |
