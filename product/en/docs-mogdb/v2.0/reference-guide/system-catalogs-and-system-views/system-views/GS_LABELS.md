---
title: GS_LABELS
summary: GS_LABELS
author: Guo Huan
date: 2021-06-15
---

# GS_LABELS

**GS_LABELS** displays all configured resource labels. Only the users with system administrator or security policy administrator permission can access this view.

| Name       | Type | Description                                                  |
| :--------- | :--- | :----------------------------------------------------------- |
| labelname  | name | Resource label name                                          |
| labeltype  | name | Resource label type                                          |
| fqdntype   | name | Database resource type                                       |
| schemaname | name | Name of the schema to which the database resource belongs    |
| fqdnname   | name | Database resource name                                       |
| columnname | name | Name of the database resource column. If the marked database resource is not a column, this parameter is left blank. |
