---
title: PG_ROLES
summary: PG_ROLES
author: Guo Huan
date: 2021-04-19
---

# PG_ROLES

**PG_ROLES** provides information about database roles.

**Table 1** PG_ROLES columns

| Name           | Type                     | Reference             | Description                                                  |
| :------------- | :----------------------- | :-------------------- | :----------------------------------------------------------- |
| rolname        | name                     | -                     | Role name                                                    |
| rolsuper       | Boolean                  | -                     | Whether the role is the initial system administrator with the highest permission |
| rolinherit     | Boolean                  | -                     | Whether the role inherits the permissions for this type of roles |
| rolcreaterole  | Boolean                  | -                     | Whether the role can create other roles                      |
| rolcreatedb    | Boolean                  | -                     | Whether the role can create databases                        |
| rolcatupdate   | Boolean                  | -                     | Whether the role can update system tables directly. Only the initial system administrator whose **usesysid** is **10** has this permission. It is unavailable for other users. |
| rolcanlogin    | Boolean                  | -                     | Whether the role can log in to the database                  |
| rolreplication | Boolean                  | -                     | Whether the role can be replicated                           |
| rolauditadmin  | Boolean                  | -                     | Whether the role is an audit system administrator            |
| rolsystemadmin | Boolean                  | -                     | Whether the role is a system administrator                   |
| rolconnlimit   | integer                  | -                     | Sets the maximum number of concurrent connections that this role can initiate if this role can log in. The value **-1** indicates no limit. |
| rolpassword    | text                     | -                     | Not the password (always reads as \*\*\*\*\*\*\*\*)          |
| rolvalidbegin  | timestamp with time zone | -                     | Start time of the account validity period (null if no start time) |
| rolvaliduntil  | timestamp with time zone | -                     | End time for account validity (null if no end time)          |
| rolrespool     | name                     | -                     | Resource pool that a user can use                            |
| rolparentid    | oid                      | PG_AUTHID.rolparentid | OID of a group user to which the user belongs                |
| roltabspace    | text                     | -                     | Storage space of the user permanent table                    |
| roltempspace   | text                     | -                     | Storage space of the user temporary table                    |
| rolspillspace  | text                     | -                     | Operator disk spill space of the user                        |
| rolconfig      | text[]                   | -                     | Session defaults for runtime configuration variables         |
| oid            | oid                      | PG_AUTHID.oid         | Role ID                                                      |
| roluseft       | Boolean                  | PG_AUTHID.roluseft    | Whether the role can perform operations on foreign tables    |
| rolkind        | "char"                   | -                     | Role type                                                    |
| nodegroup      | name                     | -                     | Unsupported currently                                        |
