---
title: PG_STAT_SYS_TABLES
summary: PG_STAT_SYS_TABLES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_SYS_TABLES

**PG_STAT_SYS_TABLES** displays statistics about the system catalogs of all the namespaces in **pg_catalog** and **information_schema** schemas.

**Table 1** PG_STAT_SYS_TABLES columns

| Name              | Type                     | Description                                                  |
| :---------------- | :----------------------- | :----------------------------------------------------------- |
| relid             | oid                      | OID of the table                                             |
| schemaname        | name                     | Name of the schema that the table is in                      |
| relname           | name                     | Table name                                                   |
| seq_scan          | bigint                   | Number of sequential scans initiated on the table            |
| seq_tup_read      | bigint                   | Number of live rows fetched by sequential scans              |
| idx_scan          | bigint                   | Number of index scans initiated on the table                 |
| idx_tup_fetch     | bigint                   | Number of live rows fetched by index scans                   |
| n_tup_ins         | bigint                   | Number of rows inserted                                      |
| n_tup_upd         | bigint                   | Number of rows updated                                       |
| n_tup_del         | bigint                   | Number of rows deleted                                       |
| n_tup_hot_upd     | bigint                   | Number of rows HOT updated (with no separate index update required) |
| n_live_tup        | bigint                   | Estimated number of live rows                                |
| n_dead_tup        | bigint                   | Estimated number of dead rows                                |
| last_vacuum       | timestamp with time zone | Last time at which the table was manually vacuumed (excluding **VACUUM FULL**) |
| last_autovacuum   | timestamp with time zone | Last time at which the table was vacuumed by the autovacuum daemon |
| last_analyze      | timestamp with time zone | Last time at which the table was manually analyzed           |
| last_autoanalyze  | timestamp with time zone | Last time at which the table was analyzed by the autovacuum daemon |
| vacuum_count      | bigint                   | Number of times the table has been manually vacuumed (not counting **VACUUM FULL**) |
| autovacuum_count  | bigint                   | Number of times the table has been vacuumed by the autovacuum daemon |
| analyze_count     | bigint                   | Number of times the table has been manually analyzed         |
| autoanalyze_count | bigint                   | Number of times the table has been analyzed by the autovacuum daemon |
| last_data_changed | timestamp with time zone | Last modification time of the table data                     |
