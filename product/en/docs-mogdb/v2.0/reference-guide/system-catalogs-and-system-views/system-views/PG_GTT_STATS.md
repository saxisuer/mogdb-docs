---
title: PG_GTT_STATS
summary: PG_GTT_STATS
author: Guo Huan
date: 2021-04-19
---

# PG_GTT_STATS

**PG_GTT_STATS** displays statistics about a single column in all global temporary tables of the current session by calling **pg_get_gtt_relstats**.

**Table 1** PG_GTT_STATS columns

| Name                   | Type    | Description                                                  |
| :--------------------- | :------ | :----------------------------------------------------------- |
| schemaname             | name    | Schema name.                                                 |
| tablename              | name    | Name of a global temporary table.                            |
| attname                | name    | Attribute name.                                              |
| inherited              | boolean | Specifies whether to collect statistics for objects that have inheritance relationship. |
| null_frac              | real    | Percentage of column entries that are null.                  |
| avg_width              | integer | Average stored width, in bytes, of non-null entries.         |
| n_distinct             | real    | Number of distinct, non-null data values in the column.      |
| most_common_vals       | text[]  | List of the most common values, which is sorted by occurrence frequency. |
| most_common_freqs      | real[]  | Frequencies of the most common values.                       |
| histogram_bounds       | text[]  | Data distribution (excluding the most common values) in a frequency histogram description column. |
| correlation            | real    | Correlation coefficient.                                     |
| most_common_elems      | text[]  | List of the most common element values, which is used for the array type or some other type. |
| most_common_elem_freqs | real[]  | Frequencies of the most common element values.               |
| elem_count_histogram   | real[]  | Array type histogram.                                        |
