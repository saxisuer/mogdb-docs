---
title: PG_VIEWS
summary: PG_VIEWS
author: Guo Huan
date: 2021-04-19
---

# PG_VIEWS

**PG_VIEWS** provides access to basic information about each view in the database.

**Table 1** PG_VIEWS columns

| Name       | Type | Reference            | Description                               |
| :--------- | :--- | :------------------- | :---------------------------------------- |
| schemaname | name | PG_NAMESPACE.nspname | Name of the schema that contains the view |
| viewname   | name | PG_CLASS.relname     | View name                                 |
| viewowner  | name | PG_AUTHID.Erolname   | Owner of the view                         |
| definition | text | -                    | Definition of the view                    |
