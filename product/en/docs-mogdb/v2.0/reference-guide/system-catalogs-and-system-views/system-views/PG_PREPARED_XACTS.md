---
title: PG_PREPARED_XACTS
summary: PG_PREPARED_XACTS
author: Guo Huan
date: 2021-04-19
---

# PG_PREPARED_XACTS

**PG_PREPARED_XACTS** displays information about transactions that are currently prepared for two-phase commit.

**Table 1** PG_PREPARED_XACTS columns

| Name        | Type                     | Reference           | Description                                                  |
| :---------- | :----------------------- | :------------------ | :----------------------------------------------------------- |
| transaction | xid                      | -                   | Numeric transaction identifier of the prepared transaction   |
| gid         | text                     | -                   | Global transaction identifier that was assigned to the transaction |
| prepared    | timestamp with time zone | -                   | Time at which the transaction is prepared for commit         |
| owner       | name                     | PG_AUTHID.rolname   | Name of the user that executes the transaction               |
| database    | name                     | PG_DATABASE.datname | Name of the database in which the transaction is executed    |
