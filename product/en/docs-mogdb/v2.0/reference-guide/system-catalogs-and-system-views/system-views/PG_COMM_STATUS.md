---
title: PG_COMM_STATUS
summary: PG_COMM_STATUS
author: Guo Huan
date: 2021-06-15
---

# PG_COMM_STATUS

**PG_COMM_STATUS** displays the communication library status for a single DN.

**Table 1** PG_COMM_STATUS columns

| Name          | Type    | Description                                                  |
| :------------ | :------ | :----------------------------------------------------------- |
| node_name     | text    | Node name                                                    |
| rxpck_rate    | integer | Receiving rate of the communication library on the node, in byte/s |
| txpck_rate    | integer | Sending rate of the communication library on the node, in byte/s |
| rxkB_rate     | bigint  | Receiving rate of the communication library on the node, in kbyte/s |
| txkB_rate     | bigint  | Sending rate of the communication library on the node, in kbyte/s |
| buffer        | bigint  | Size of the buffer of the Cmailbox                           |
| memKB_libcomm | bigint  | Communication memory size of the **libcomm** process, in bytes |
| memKB_libpq   | bigint  | Communication memory size of the **libpq** process, in bytes |
| used_PM       | integer | Real-time usage of the **postmaster** thread                 |
| used_sflow    | integer | Real-time usage of the **gs_sender_flow_controller** thread  |
| used_rflow    | integer | Real-time usage of the **gs_receiver_flow_controller** thread |
| used_rloop    | integer | Highest real-time usage among multiple **gs_receivers_loop** threads |
| stream        | integer | Total number of used logical connections.                    |
