---
title: PG_STAT_BGWRITER
summary: PG_STAT_BGWRITER
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_BGWRITER

**PG_STAT_BGWRITER** showing statistics about the background writer process's activity.

**Table 1** PG_STAT_BGWRITER columns

| Name                  | Type                     | Description                                                  |
| :-------------------- | :----------------------- | :----------------------------------------------------------- |
| checkpoints_timed     | bigint                   | Number of scheduled checkpoints that have been performed     |
| checkpoints_req       | bigint                   | Number of requested checkpoints that have been performed     |
| checkpoint_write_time | double precision         | Total time that has been spent in the portion of checkpoint processing where files are written to disk (unit: ms) |
| checkpoint_sync_time  | double precision         | Total time that has been spent in the portion of checkpoint processing where files are synchronized to disk (unit: ms) |
| buffers_checkpoint    | bigint                   | Number of buffers written during checkpoints                 |
| buffers_clean         | bigint                   | Number of buffers written by the background writer           |
| maxwritten_clean      | bigint                   | Number of times the background writer stopped a cleaning scan because it had written too many buffers |
| buffers_backend       | bigint                   | Number of buffers written directly by a backend              |
| buffers_backend_fsync | bigint                   | Number of times a backend had to execute its own fsync call (normally the background writer handles those even when the backend does its own write) |
| buffers_alloc         | bigint                   | Number of buffers allocated                                  |
| stats_reset           | timestamp with time zone | Time at which these statistics were last reset               |
