---
title: PG_NODE_ENV
summary: PG_NODE_ENV
author: Guo Huan
date: 2021-04-19
---

# PG_NODE_ENV

**PG_NODE_ENV** obtains environmental variable information about the current node.

**Table 1** PG_NODE_ENV columns

| Name          | Type    | Description                        |
| :------------ | :------ | :--------------------------------- |
| node_name     | text    | Current node name                  |
| host          | text    | Host name of the node              |
| process       | integer | Number of the node process         |
| port          | integer | Port ID of the node                |
| installpath   | text    | Installation directory of the node |
| datapath      | text    | Data directory of the node         |
| log_directory | text    | Log directory of the node          |
