---
title: PG_TABLES
summary: PG_TABLES
author: Guo Huan
date: 2021-04-19
---

# PG_TABLES

**PG_TABLES** provides access to each table in the database.

**Table 1** PG_TABLES columns

| Name          | Type                     | Reference                          | Description                                                  |
| :------------ | :----------------------- | :--------------------------------- | :----------------------------------------------------------- |
| schemaname    | name                     | PG_NAMESPACE.nspname               | Name of the schema that contains a table                     |
| tablename     | name                     | PG_CLASS.relname                   | Table name                                                   |
| tableowner    | name                     | pg_get_userbyid(PG_CLASS.relowner) | Table owner                                                  |
| tablespace    | name                     | PG_TABLESPACE.spcname              | Tablespace that contains the table (default value:**null**) |
| hasindexes    | Boolean                  | PG_CLASS.relhasindex               | The value is **true** if the table has (or recently had) an index; otherwise, the value is **false**. |
| hasrules      | Boolean                  | PG_CLASS.relhasruls                | The value is **true** if the table has rules; otherwise, the value is **false**. |
| hastriggers   | Boolean                  | PG_CLASS.RELHASTRIGGERS            | The value is **TRUE** if the table has triggers; otherwise, the value is **FALSE**. |
| tablecreator  | name                     | pg_get_userbyid(po.creator)        | Table creator.                                               |
| created       | timestamp with time zone | pg_object.ctime                    | Creation time of the object                                  |
| last_ddl_time | timestamp with time zone | pg_object.mtime                    | Last modification time of the object                         |
