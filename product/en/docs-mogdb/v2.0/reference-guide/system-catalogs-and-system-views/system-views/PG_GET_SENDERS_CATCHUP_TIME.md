---
title: PG_GET_SENDERS_CATCHUP_TIME
summary: PG_GET_SENDERS_CATCHUP_TIME
author: Guo Huan
date: 2021-04-19
---

# PG_GET_SENDERS_CATCHUP_TIME

**PG_GET_SENDERS_CATCHUP_TIME** provides catchup information of the currently active primary/standby instance sender thread on the database node.

**Table 1** PG_GET_SENDERS_CATCHUP_TIME columns

| Name          | Type                     | Description                         |
| :------------ | :----------------------- | :---------------------------------- |
| pid           | bigint                   | Current sender thread ID            |
| lwpid         | integer                  | Current sender lwpid                |
| local_role    | text                     | Local role                          |
| peer_role     | text                     | Peer role                           |
| state         | text                     | Current sender's replication status |
| type          | text                     | Current sender type                 |
| catchup_start | timestamp with time zone | Startup time of a catchup task      |
| catchup_end   | timestamp with time zone | End time of a catchup task          |
