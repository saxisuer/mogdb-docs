---
title: PG_COMM_DELAY
summary: PG_COMM_DELAY
author: Guo Huan
date: 2021-06-15
---

# PG_COMM_DELAY

**PG_COMM_DELAY** displays the communication library delay status for a single DN.

**Table 1** PG_COMM_DELAY columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| node_name   | text    | Node name                                                    |
| remote_name | text    | Name of the peer node                                        |
| remote_host | text    | IP address of the peer node                                  |
| stream_num  | integer | Number of logical stream connections used by the current physical connection |
| min_delay   | integer | Minimum delay of the current physical connection within 1 minute, in microsecond<br/>NOTE:<br/>A negative result is invalid. Wait until the delay status is updated and query again. |
| average     | integer | Average delay of the current physical connection within 1 minute, in microsecond |
| max_delay   | integer | Maximum delay of the current physical connection within 1 minute, in microsecond |
