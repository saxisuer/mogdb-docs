---
title: Extended Syntax
summary: Extended Syntax
author: Zhang Cuiping
date: 2021-05-18
---

# Extended Syntax

MogDB provides extended syntax .

**Table 1** Extended SQL syntax

| Category                                                     | Keywords                                                     | Description                                                  |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| Creating a table (CREATE TABLE)                              | **INHERITS ( parent_table [, … ] )**                         | Specifies whether an inherited table is supported.           |
| table_constraint:<br />**EXCLUDE USING index_method** index_parameters [ WHERE ( predicate ) ] \| **FOREIGN KEY ( column_name [, … ] ) REFERENCES reftable [ ( refcolumn [, … ] ) ]**<br />**[ MATCH FULL \| MATCH PARTIAL \| MATCH SIMPLE ] [ ON DELETE action ] [ ON UPDATE action ]** | Specifies that users cannot use **EXCLUDE USING index_method** to create an excluding constraint for a table. |                                                              |
| Altering a table (ALTER TABLE)                               | **TO { GROUP groupname \| NODE ( nodename [, … ] ) }**       | Modifies the list of DNs where table data is distributed.    |
| **DELETE NODE ( nodename [, … ] )**                          | Deletes the DNs on which table data is distributed.          |                                                              |
| Loading a module                                             | **CREATE EXTENSION**                                         | Loads a new module (such as DBLINK) to the current database. |
| **ALTER EXTENSION**                                          | Modifies the loaded module.                                  |                                                              |
| **DROP EXTENSION**                                           | Deletes the loaded module.                                   |                                                              |
| Aggregate functions                                          | **CREATE AGGREGATE**                                         | Defines a new aggregate function.                            |
| **ALTER AGGREGATE**                                          | Modifies the definition of an aggregate function.            |                                                              |
| **DROP AGGREGATE**                                           | Drops an existing aggregate function.                        |                                                              |
| Operators                                                    | **CREATE OPERATOR**                                          | Defines a new operator.                                      |
| **ALTER OPERATOR**                                           | Modifies the definition of an operator.                      |                                                              |
| **DROP OPERATOR**                                            | Drops an existing operator from the database.                |                                                              |
| Operator classes                                             | **CREATE OPERATOR CLASS**                                    | Defines a new operator class.                                |
| **ALTER OPERATOR CLASS**                                     | Modifies the definition of an operator class.                |                                                              |
| **DROP OPERATOR CLASS**                                      | Drops an existing operator class.                            |                                                              |
| Operator families                                            | **CREATE OPERATOR FAMILY**                                   | Defines a new operator family.                               |
| **ALTER OPERATOR FAMILY**                                    | Modifies the definition of an operator family.               |                                                              |
| **DROP OPERATOR FAMILY**                                     | Deletes an existing operator family.                         |                                                              |
| Text search parsers                                          | **CREATE TEXT SEARCH PARSER**                                | Creates a text search parser.                                |
| **ALTER TEXT SEARCH PARSER**                                 | Modifies a text search parser.                               |                                                              |
| **DROP TEXT SEARCH PARSER**                                  | Deletes an existing text search parser.                      |                                                              |
| Text search templates                                        | **CREATE TEXT SEARCH TEMPLATE**                              | Creates a text search template.                              |
| **ALTER TEXT SEARCH TEMPLATE**                               | Modifies a text search template.                             |                                                              |
| **DROP TEXT SEARCH TEMPLATE**                                | Deletes an existing text search template.                    |                                                              |
| Collation rules                                              | **CREATE COLLATION**                                         | Creates a collation rule.<br />The collation rule allows users to define data in the column-level, or even the collation rule and character class behaviors at the operation level. |
| **ALTER COLLATION**                                          | Modifies a collation rule.                                   |                                                              |
| **DROP COLLATION**                                           | Deletes a collation rule.                                    |                                                              |
| Generating a notification                                    | **NOTIFY**                                                   | The **NOTIFY** command sends a notification together with an optional "payload" string to each client that has previously executed **LISTEN** for a specified channel in the current database. |
| Listening for a notification                                 | **LISTEN**                                                   | Registers a listener for the current session.                |
| Stopping listening for a notification                        | **UNLISTEN**                                                 | Clears all listeners of this session registration.           |
| Loading or reloading a shared library file                   | **LOAD**                                                     | Loads a shared library file to the address space of the database server. |
| Releasing session resources in a database                    | **DISCARD**                                                  | Releases session resources in a database.                    |
| Procedural languages                                         | **CREATE LANGUAGE**                                          | Registers a new language.                                    |
| **ALTER LANGUAGE**                                           | Modifies the definition of a procedural language.            |                                                              |
| **DROP LANGUAGE**                                            | Deletes a procedural language.                               |                                                              |
| Domains                                                      | **CREATE DOMAIN**                                            | Creates a domain.                                            |
| **ALTER DOMAIN**                                             | Modifies the definition of an existing domain.               |                                                              |
| **DROP DOMAIN**                                              | Deletes a domain.                                            |                                                              |
| Code conversion                                              | **CREATE CONVERSION**                                        | Defines the character set conversion.                        |
| **ALTER CONVERSION**                                         | Modifies the definition of a code conversion.                |                                                              |
| **DROP CONVERSION**                                          | Deletes a previously defined code conversion.                |                                                              |
| Type conversion                                              | **CREATE CAST**                                              | Defines a new type conversion. A conversion specifies how to perform a conversion between two data types. |
| **DROP CAST**                                                | Deletes a previously defined type conversion.                |                                                              |
| Creating a cursor                                            | CURSOR name [ BINARY ] [ **INSENSITIVE** ] [ [ NO ] **SCROLL** ] [ **WITH HOLD** ] FOR query | **INSENSITIVE** <br />is used only for being compatible with the SQL standard.<br />**SCROLL** declares that the cursor can be used for reverse search.<br />**WITH HOLD** indicates that a cursor can still be used after the transaction creating the cursor is successfully committed. |
| Moving a cursor                                              | MOVE **BACKWARD**                                            | A reverse mobile cursor can be used only when it is used together with SCROLL. |
