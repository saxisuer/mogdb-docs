---
title: Extended Functions
summary: Extended Functions
author: Zhang Cuiping
date: 2021-05-18
---

# Extended Functions

The following table lists the extended functions supported by MogDB. These functions are for reference only.

| Category                                    | Name                                                         | Description                                                  |
| :------------------------------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| Access privilege inquiry function           | has_sequence_privilege(user, sequence, privilege)            | Queries whether a specified user has privilege for sequences. |
| has_sequence_privilege(sequence, privilege) | Queries whether the current user has privilege for sequence. |                                                              |
| Trigger function                            | pg_get_triggerdef(oid)                                       | Gets **CREATE** [ **CONSTRAINT** ] **TRIGGER** command for triggers. |
| pg_get_triggerdef(oid, boolean)             | Gets **CREATE** [ **CONSTRAINT** ] **TRIGGER** command for triggers. |                                                              |
