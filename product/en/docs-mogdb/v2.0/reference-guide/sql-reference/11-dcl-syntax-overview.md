---
title: DCL Syntax Overview
summary: DCL Syntax Overview
author: Zhang Cuiping
date: 2021-05-17
---

# DCL Syntax Overview

Data control language (DCL) is used to create users and roles and set or modify database users or role rights.

## Defining a Role

A role is used to manage permissions. For database security, management and operation permissions can be granted to different roles. For details about related SQL statements, see [Table 1](#sqlstatement1).

**Table 1** SQL statements for defining a role<a id="sqlstatement1"></a>

| Description              | SQL Statement |
| :----------------------- | :------------ |
| Creating a role          | CREATE ROLE   |
| Altering role attributes | ALTER ROLE    |
| Dropping a role          | DROP ROLE     |

## Defining a User

A user is used to log in to a database. Different permissions can be granted to users for managing data accesses and operations of the users. For details about related SQL statements, see [Table 2](#sqlstatement2).

**Table 2** SQL statements for defining a user<a id="sqlstatement2"></a>

| Description              | SQL Statement |
| :----------------------- | :------------ |
| Creating a User          | CREATE USER   |
| Altering user attributes | ALTER USER    |
| Dropping a user          | DROP USER     |

## Granting Rights

MogDB provides a statement for granting rights to data objects and roles.

## Revoking Rights

MogDB provides a statement for revoking rights.

## Setting Default Rights

MogDB allows users to set rights for objects that will be created.

## Shutting Down The Current Node

MogDB allows users to run the **shutdown** command to shut down the current database node.
