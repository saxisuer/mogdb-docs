---
title: Testing a Dictionary
summary: Testing a Dictionary
author: Zhang Cuiping
date: 2021-05-17
---

# Testing a Dictionary

The **ts_lexize** function facilitates dictionary testing.

**ts_lexize(dict regdictionary, token text) returns text[]** **ts_lexize** returns an array of lexemes if the input **token** is known to the dictionary, or an empty array if the token is known to the dictionary but it is a stop word, or **NULL** if it is an unknown word.

For example:

```
postgres=# SELECT ts_lexize('english_stem', 'stars');
 ts_lexize
-----------
 {star}

postgres=# SELECT ts_lexize('english_stem', 'a');
 ts_lexize
-----------
 {}
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The **ts_lexize** function expects a single **token**, not text.
