---
title: Parser
summary: Parser
author: Zhang Cuiping
date: 2021-05-17
---

# Parser

Text search parsers are responsible for splitting raw document text into tokens and identifying each token's type, where the set of types is defined by the parser itself. Note that a parser does not modify the text at all - it simply identifies plausible word boundaries. Because of this limited scope, there is less need for application-specific custom parsers than there is for custom dictionaries.

Currently, MogDB provides the following built-in parsers: pg_catalog.default for English configuration, and pg_catalog.ngram and pg_catalog.pound for full text search in texts containing Chinese, or both Chinese and English.

The built-in parser is named **pg_catalog.default**. It recognizes 23 token types, shown in [Table 1](#defaultparser).

**Table 1** Default parser's token types <a id="defaultparser"></a>

| Alias           | Description                              | Example                                                  |
| :-------------- | :--------------------------------------- | :------------------------------------------------------- |
| asciiword       | Word, all ASCII letters                  | elephant                                                 |
| word            | Word, all letters                        | mañana                                                   |
| numword         | Word, letters and digits                 | beta1                                                    |
| asciihword      | Hyphenated word, all ASCII               | up-to-date                                               |
| hword           | Hyphenated word, all letters             | lógico-matemática                                        |
| numhword        | Hyphenated word, letters and digits      | postgresql-beta1                                         |
| hword_asciipart | Hyphenated word part, all ASCII          | postgresql in the context postgresql-beta1               |
| hword_part      | Hyphenated word part, all letters        | lógico or matemática in the context lógico-matemática    |
| hword_numpart   | Hyphenated word part, letters and digits | beta1 in the context postgresql-beta1                    |
| email           | Email address                            | foo@example.com                                          |
| protocol        | Protocol head                            | http://                                                  |
| url             | URL                                      | example.com/stuff/index.html                             |
| host            | Host                                     | example.com                                              |
| url_path        | URL path                                 | /stuff/index.html, in the context of a URL               |
| file            | File or path name                        | /usr/local/foo.txt, if not within a URL                  |
| sfloat          | Scientific notation                      | -1.23E+56                                                |
| float           | Decimal notation                         | -1.234                                                   |
| int             | Signed integer                           | -1234                                                    |
| uint            | Unsigned integer                         | 1234                                                     |
| version         | Version number                           | 8.3.0                                                    |
| tag             | XML tag                                  | &lt;a href="dictionaries.html"&gt;                       |
| entity          | XML entity                               | &amp;                                                    |
| blank           | Space symbols                            | (any whitespace or punctuation not otherwise recognized) |

Note: The parser's notion of a "letter" is determined by the database's locale setting, specifically **lc_ctype**. Words containing only the basic ASCII letters are reported as a separate token type, since it is sometimes useful to distinguish them. In most European languages, token types word and asciiword should be treated alike.

**email** does not support all valid email characters as defined by RFC 5322. Specifically, the only non-alphanumeric characters supported for email user names are period, dash, and underscore.

It is possible for the parser to identify overlapping tokens in the same piece of text. As an example, a hyphenated word will be reported both as the entire word and as each component:

```
postgres=# SELECT alias, description, token FROM ts_debug('english','foo-bar-beta1');
      alias      |               description                |     token
-----------------+------------------------------------------+---------------
 numhword        | Hyphenated word, letters and digits      | foo-bar-beta1
 hword_asciipart | Hyphenated word part, all ASCII          | foo
 blank           | Space symbols                            | -
 hword_asciipart | Hyphenated word part, all ASCII          | bar
 blank           | Space symbols                            | -
 hword_numpart   | Hyphenated word part, letters and digits | beta1
```

This behavior is desirable since it allows searches to work for both the whole compound word and for components. Here is another instructive example:

```
postgres=# SELECT alias, description, token FROM ts_debug('english','http://example.com/stuff/index.html');
  alias   |  description  |            token
----------+---------------+------------------------------
 protocol | Protocol head | http://
 url      | URL           | example.com/stuff/index.html
 host     | Host          | example.com
 url_path | URL path      | /stuff/index.html
```

N-gram is a mechanical word segmentation method, and applies to no semantic Chinese segmentation scenarios. The N-gram segmentation method ensures the completeness of the segmentation. However, to cover all the possibilities, it but adds unnecessary words to the index, resulting in a large number of index items. N-gram supports Chinese coding, including GBK and UTF-8. Six built-in token types are shown in [Table 2](#tokentypes).

**Table 2** Token types <a id="tokentypes"></a>

| Alias       | Description     |
| :---------- | :-------------- |
| zh_words    | chinese words   |
| en_word     | english word    |
| numeric     | numeric data    |
| alnum       | alnum string    |
| grapsymbol  | graphic symbol  |
| multisymbol | multiple symbol |

Pound segments words in a fixed format. It is used to segment to-be-parsed nonsense Chinese and English words that are separated by fixed separators. It supports Chinese encoding (including GBK and UTF8) and English encoding (including ASCII). Pound has six pre-configured token types (as listed in [Table 3](#tokentypes3)) and supports five separators (as listed in [Table 4](#separatortypes)). The default, the separator is **#**. Pound The maximum length of a token is 256 characters.

**Table 3** Token types <a id="tokentypes3"></a>

| Alias       | Description     |
| :---------- | :-------------- |
| zh_words    | chinese words   |
| en_word     | english word    |
| numeric     | numeric data    |
| alnum       | alnum string    |
| grapsymbol  | graphic symbol  |
| multisymbol | multiple symbol |

**Table 4** Separator types <a id="separatortypes"></a>

| Separator | Description       |
| :-------- | :---------------- |
| @         | Special character |
| #         | Special character |
| $         | Special character |
| %         | Special character |
| /         | Special character |
