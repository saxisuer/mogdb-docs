---
title: Simple Expressions
summary: Simple Expressions
author: Zhang Cuiping
date: 2021-05-17
---

# Simple Expressions

## Logical Expressions

**Logical Operators** lists the operators and calculation rules of logical expressions.

## Comparative Expressions

Operators lists the common comparative operators.

In addition to comparative operators, you can also use the following sentence structure:

- BETWEEN operator

  **a BETWEEN x AND y** is equivalent to **a &gt;= x AND a &lt;= y**.

  **a NOT BETWEEN x AND y** is equivalent to **a &lt; x OR a &gt; y**.

- To check whether a value is null, use:

  expression IS NULL

  expression IS NOT NULL

  or an equivalent (non-standard) sentence structure:

  expression ISNULL

  expression NOTNULL

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** Do not write **expression=NULL** or **expression&lt;&gt;(!=)NULL**, because **NULL** represents an unknown value, and these expressions cannot determine whether two unknown values are equal.

- is distinct from/is not distinct from

  - is distinct from

    If the data types and values of A and B are different, the value is **true**.

    If the data types and values of A and B are the same, the value is **false**.

    Empty values are considered the same.

  - is not distinct from

    If the data types and values of A and B are different, the value is **false**.

    If the data types and values of A and B are the same, the value is **true**.

    Empty values are considered the same.

## Pseudocolumn

ROWNUM

**ROWNUM** is a pseudocolumn that returns a number indicating the row number of the result obtained from the query. The value of **ROWNUM** in the first row is **1**, the value of **ROWNUM** in the second row is **2**, and so on.

The return type of **ROWNUM** is BIGINT. **ROWNUM** can be used to limit the total number of rows returned by a query. For example, the following statement limits the maximum number of records returned from the table **Students** to 10.

```
select * from Students where rownum <= 10;
```

## Examples

```
postgres=# SELECT 2 BETWEEN 1 AND 3 AS RESULT;
 result
----------
 t
(1 row)

postgres=# SELECT 2 >= 1 AND 2 <= 3 AS RESULT;
 result
----------
 t
(1 row)

postgres=# SELECT 2 NOT BETWEEN 1 AND 3 AS RESULT;
 result
----------
 f
(1 row)

postgres=# SELECT 2 < 1 OR 2 > 3 AS RESULT;
 result
----------
 f
(1 row)

postgres=# SELECT 2+2 IS NULL AS RESULT;
 result
----------
 f
(1 row)

postgres=# SELECT 2+2 IS NOT NULL AS RESULT;
 result
----------
 t
(1 row)

postgres=# SELECT 2+2 ISNULL AS RESULT;
 result
----------
 f
(1 row)

postgres=# SELECT 2+2 NOTNULL AS RESULT;
 result
----------
 t
(1 row)

postgres=# SELECT 2+2 IS DISTINCT FROM NULL AS RESULT;
 result
----------
 t
(1 row)

postgres=# SELECT 2+2 IS NOT DISTINCT FROM NULL AS RESULT;
 result
----------
 f
(1 row)
```
