---
title: System Operation
summary: System Operation
author: Zhang Cuiping
date: 2021-05-17
---

# System Operation

MogDB text runs SQL statements to perform different system operations, such as setting variables, displaying the execution plan, and collecting garbage data.

## Setting Variables

For details about how to set various parameters for a session or transaction, see **SET**.

## Displaying the Execution Plan

For details about how to display the execution plan that MogDB makes for SQL statements, see **EXPLAIN**.

## Specifying a Checkpoint in Transaction Logs

By default, WALs periodically specify checkpoints in a transaction log. **CHECKPOINT** forces an immediate checkpoint when the related command is issued, without waiting for a regular checkpoint scheduled by the system. See **CHECKPOINT**.

## Collecting Unnecessary Data

For details about how to collect garbage data and analyze a database as required, See **VACUUM**.

## Collecting Statistics

For details about how to collect statistics on tables in databases, See **ANALYZE | ANALYSE**.

## Setting the Constraint Check Mode for the Current Transaction

For details about how to set the constraint check mode for the current transaction, See **SET CONSTRAINTS**.

## Shutting Down The Current Database Node

For details about shutting down the current database node, see **SHUTDOWN**.
