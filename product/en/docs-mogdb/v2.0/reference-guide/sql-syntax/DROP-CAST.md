---
title: DROP CAST
summary: DROP CAST
author: Zhang Cuiping
date: 2021-06-07
---

# DROP CAST

## Function

DROP CAST deletes a type conversion.

## Precautions

DROP CAST deletes a previously defined type conversion.

To delete a type conversion, you must have a source or destination data type. This is the same permission as creating a type conversion.

## Syntax

```ebnf+diagram
DropCast ::= DROP CAST [ IF EXISTS ] (source_type AS target_type) [ CASCADE | RESTRICT ]
```

## Parameter Description

- I**F EXISTS**

  Do not throw an error if the specified conversion does not exist. A notice is issued in this case.

- **source_type**

  Source data type in the type conversion.

- **target_type**

  Type of the target data in the type conversion.

- **CASCADE**

  **RESTRICT**

  These keys have no effect because there is no dependency on type conversion.

## Example

Delete the conversion from text to int.

```sql
DROP CAST (text AS int);
```

## Compatibility

DROP CAST complies with the SQL standard.
