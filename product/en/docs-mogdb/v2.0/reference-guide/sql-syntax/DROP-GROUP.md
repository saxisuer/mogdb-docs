---
title: DROP GROUP
summary: DROP GROUP
author: Zhang Cuiping
date: 2021-05-18
---

# DROP GROUP

## Function

**DROP GROUP** deletes a user group.

**DROP GROUP** is an alias for **DROP ROLE**.

## Precautions

**DROP GROUP** is an internal interface of the MogDB management tool. You are not advised to use this interface, because doing so affects MogDB.

## Syntax

```ebnf+diagram
DropGroup ::= DROP GROUP [ IF EXISTS ] group_name [, ...];
```

## Parameter Description

See **Parameter Description** in **DROP ROLE**.
