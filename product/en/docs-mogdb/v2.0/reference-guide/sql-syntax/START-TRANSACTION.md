---
title: START TRANSACTION
summary: START TRANSACTION
author: Zhang Cuiping
date: 2021-05-18
---

# START TRANSACTION

## Function

**START TRANSACTION** starts a transaction. If the isolation level or read/write mode is specified, a new transaction will have those characteristics. You can also specify them using **SET TRANSACTION**.

## Precautions

None

## Syntax

Format 1: START TRANSACTION

```ebnf+diagram
StartTransaction ::= START TRANSACTION
  [
    {
       ISOLATION LEVEL { READ COMMITTED | SERIALIZABLE | REPEATABLE READ }
       | { READ WRITE | READ ONLY }
     } [, ...]
  ];
```

Format 2: BEGIN

```ebnf+diagram
Begin ::= BEGIN [ WORK | TRANSACTION ]
  [
    {
       ISOLATION LEVEL { READ COMMITTED | SERIALIZABLE | REPEATABLE READ }
       | { READ WRITE | READ ONLY }
      } [, ...]
  ];
```

## Parameter Description

- **WORK | TRANSACTION**

  Specifies the optional keyword in BEGIN format without functions.

- **ISOLATION LEVEL**

  Specifies the transaction isolation level that determines the data that a transaction can view if other concurrent transactions exist.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > The isolation level of a transaction cannot be reset after the first clause (**SELECT**, **INSERT**, **DELETE**, **UPDATE**, **FETCH**, **COPY**) for modifying data is executed in the transaction.

  Value range:

  - **READ COMMITTED**: Only submitted data is read. It is the default value.
  - **REPEATABLE READ**: Only the data committed before transaction start is read. Uncommitted data or data committed in other concurrent transactions cannot be read.
  - **SERIALIZABLE**: Currently, this isolation level is not supported in MogDB. It is equivalent to **REPEATABLE READ**.

- **READ WRITE | READ ONLY**

  Specifies the transaction access mode (read/write or read only).

## Examples

```sql
-- Start a transaction in default mode.
mogdb=# START TRANSACTION;
mogdb=# SELECT * FROM tpcds.reason;
mogdb=# END;

-- Start a transaction in default mode.
mogdb=# BEGIN;
mogdb=# SELECT * FROM tpcds.reason;
mogdb=# END;

-- Start a transaction with the isolation level being READ COMMITTED and the access mode being READ WRITE:
mogdb=# START TRANSACTION ISOLATION LEVEL READ COMMITTED READ WRITE;
mogdb=# SELECT * FROM tpcds.reason;
mogdb=# COMMIT;
```
