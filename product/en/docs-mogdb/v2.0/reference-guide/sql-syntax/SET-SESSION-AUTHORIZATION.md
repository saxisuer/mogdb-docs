---
title: SET SESSION AUTHORIZATION
summary: SET SESSION AUTHORIZATION
author: Zhang Cuiping
date: 2021-05-18
---

# SET SESSION AUTHORIZATION

## Function

**SET SESSION AUTHORIZATION** sets the session user identifier and the current user identifier of the current SQL session to a specified user.

## Precautions

The session identifier can be changed only when the initial session user has the system administrator rights. Otherwise, the system supports the statement only when the authenticated user name is specified.

## Syntax

- Set the session user identifier and the current user identifier of the current session.

  ```ebnf+diagram
  SetSessionAuthorization ::= SET [ SESSION | LOCAL ] SESSION AUTHORIZATION role_name PASSWORD 'password';
  ```

- Reset the identifiers of the session and current users to the initially authenticated user names.

  ```ebnf+diagram
  ResetSessionAuthorization ::= {SET [ SESSION | LOCAL ] SESSION AUTHORIZATION DEFAULT
      | RESET SESSION AUTHORIZATION};
  ```

## Parameter Description

- **SESSION**

  Specifies that the specified parameters take effect for the current session.

- **LOCAL**

  Specifies that the specified statement takes effect only for the current transaction.

- **role_name**

  Username

  Value range: a string. It must comply with the naming convention rule.

- **password**

  Specifies the password of a role. It must comply with the password convention.

- **DEFAULT**

  Resets the identifiers of the session and current users to the initially authenticated user names.

## Examples

```sql
-- Create a role paul.
mogdb=# CREATE ROLE paul IDENTIFIED BY 'Bigdata@123';

-- Set the current user to paul.
mogdb=# SET SESSION AUTHORIZATION paul password 'Bigdata@123';

-- View the current session user and the current user.
mogdb=# SELECT SESSION_USER, CURRENT_USER;

-- Reset the current user.
mogdb=# RESET SESSION AUTHORIZATION;

-- Delete the user.
mogdb=# DROP USER paul;
```
