---
title: ALTER GROUP
summary: ALTER GROUP
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER GROUP

## Function

**ALTER GROUP** modifies the attributes of a user group.

## Precautions

**ALTER GROUP** is an alias for **ALTER ROLE**, and it is not a standard SQL syntax and not recommended. Users can use **ALTER ROLE** directly.

## Syntax

- Add users to a group.

  ```sql
  AlterGroup ::= ALTER GROUP group_name
      ADD USER user_name [, ... ];
  ```

- Remove users from a group.

  ```sql
  AlterGroup ::= ALTER GROUP group_name
      DROP USER user_name [, ... ];
  ```

- Change the name of the group.

  ```ebnf+diagram
  AlterGroup ::= ALTER GROUP group_name
      RENAME TO new_name;
  ```

## Parameter Description

See **Parameter Description** in **ALTER ROLE**.

## Example

```sql
-- Add users to a group.
mogdb=# ALTER GROUP super_users ADD USER lche, jim;

-- Remove users from a group.
mogdb=# ALTER GROUP super_users DROP USER jim;

-- Change the name of the group.
mogdb=# ALTER GROUP super_users RENAME TO normal_users;
```
