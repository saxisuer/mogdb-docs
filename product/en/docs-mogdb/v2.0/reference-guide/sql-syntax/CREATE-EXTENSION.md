---
title: CREATE EXTENSION
summary: CREATE EXTENSION
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE EXTENSION

## Function

**CREATE EXTENSION** installs an extension.

## Precautions

- The **CREARE EXTENSION** command installs a new extension to a database. Ensure that no extension with the same name has been installed.
- Installing an extension means executing an extended script file that creates a new SQL entity, such as a function, data type, operator, and index-supported method.
- Installing an extension requires the same permissions as creating its component objects. For most extensions, this means that the superuser or database owner's permissions are required. For subsequent permission checks and entities created by the extension script, the role that runs the **CREARE EXTENSION** command becomes the owner of the extension.

## Syntax

```ebnf+diagram
CreateExtension ::= CREATE EXTENSION [ IF NOT EXISTS ] extension_name
[ WITH ] [ SCHEMA schema_name ]
[ VERSION version ]
[ FROM old_version ]
```

## Parameter Description

- **IF NOT EXISTS**

  If an extension with the same name exists in the system, no error is reported. However, a message is displayed, Note that this parameter does not ensure that the existing extensions of the system are the same as those created by the script.

- **extension_name**

  Name of the extension to be installed.

- **schema_name**

  The extension instanceis installed in this schema, and the extended content can be reinstalled. The specified schema must exist. If it is not specified, the extended control file does not specify a schema either. In this case, the default schema is used.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** Extensions are not considered to be in any schema (no restriction is posed on the name of extensions within the scope of a database), but an extension instance belongs to a schema.

- **version**

  Version of the extension to be installed, which can be written as an identifier or a string. The default version is specified in the extended control file.

- **old_version**

  If you want to upgrade the content that is not contained in the **old style** module, you must specify **FROM old_version**. This option makes **CREATE EXTENSION** run an installation script to install new content into the extension instead of creating a new entity. Note that **SCHEMA** specifies the schema that includes these existing entities.

## Examples

Install the **hstore** extension in the current database.

```
CREATE EXTENSION hstore;
```
