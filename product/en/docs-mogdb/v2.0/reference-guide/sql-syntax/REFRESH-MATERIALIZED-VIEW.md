---
title: REFRESH MATERIALIZED VIEW
summary: REFRESH MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# REFRESH MATERIALIZED VIEW

## Function

**REFRESH MATERIALIZED VIEW** completely replaces the content of a materialized view. Old content will be discarded.

## Precautions

- If WITH DATA is specified (or as the default value), the support query will be executed to provide new data. If WITH NO DATA is specified, no data will be created.
- If you want data to be sorted when it is generated, use the **ORDER BY** clause in the supported query.

## Syntax

```ebnf+diagram
RefreshMaterializedView ::= REFRESH MATERIALIZED VIEW mv_name [ WITH [ NO ] DATA ];
```

## Parameter Description

- **mv_name**

  Specifies the name of the materialized view to be refreshed.

## Examples

```sql
-- Use the query in the definition of the materialized view order_summary to replace the content of the materialized view.
mogdb=# REFRESH MATERIALIZED VIEW order_summary;
```
