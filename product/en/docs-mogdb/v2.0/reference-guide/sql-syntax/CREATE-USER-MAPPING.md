---
title: CREATE USER MAPPING
summary: CREATE USER MAPPING
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE USER MAPPING

## Function

**CREATE USER MAPPING** defines a new mapping from a user to a foreign server.

## Syntax

```ebnf+diagram
CreateUserMapping ::= CREATE USER MAPPING FOR { user_name | USER | CURRENT_USER | PUBLIC }
    SERVER server_name
    [ OPTIONS ( option 'value' [ ,' ... '] ) ];
```

## Parameter Description

- **user_name**

  Specifies the name of an existing user to map to a foreign server.

  CURRENT_USER and USER match the name of the current user. When PUBLIC is specified, a public mapping is created and used when no mapping for a particular user is available.

- **server_name**

  Specifies the name of the existing server for which a user mapping will be created.

- **OPTIONS ( { option_name ' value ' } [, …] )**

  Specifies options for user mapping. These options typically define the actual user name and password for this mapping. The option name must be unique. The allowed option names and values are related to the foreign data wrapper of the server.

  - Options supported by oracle_fdw are as follows:

    - user

      Oracle server user name.

    - password

      Password of the Oracle user.

  - Options supported by mysql_fdw are as follows:

    - username

      User name of the MySQL server or MariaDB.

    - password

      User password of the MySQL server or MariaDB.

  - Options supported by postgres_fdw are as follows:

    - user

      User name of the remote openGauss database.

    - password

      User password of the remote openGauss database.
