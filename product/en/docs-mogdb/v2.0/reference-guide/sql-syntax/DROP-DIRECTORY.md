---
title: DROP DIRECTORY
summary: DROP DIRECTORY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DIRECTORY

## Function

**DROP Directory** deletes a synonym.

## Precautions

By default, only the initial user can perform the drop operation. When **enable_access_server_directory** (**enable_access_server_directory**), users with the **sysadmin** permission can also perform the drop operation.

## Syntax

```ebnf+diagram
DropDirectory ::= DROP DIRECTORY [ IF EXISTS ] directory_name;
```

## Parameter Description

- **directory_name**

  Specifies the name of the directory to be deleted.

  Value range: an existing directory name

## Example

```sql
-- Create a directory.
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

-- Delete a directory.
mogdb=# DROP DIRECTORY dir;
```
