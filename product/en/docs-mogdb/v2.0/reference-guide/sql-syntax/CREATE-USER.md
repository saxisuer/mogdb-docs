---
title: CREATE USER
summary: CREATE USER
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE USER

## Function

**CREATE USER** creates a user.

## Precautions

- A user created using the **CREATE USER** statement has the **LOGIN** permission by default.
- A schema named after the user is automatically created in the database where the statement is executed, but not in other databases. You can run the **CREATE SCHEMA** statement to create such a schema for the user in other databases.
- The owner of an object created by a system administrator in a schema with the same name as a common user is the common user, not the system administrator.

## Syntax

```ebnf+diagram
CreateUser ::= CREATE USER user_name [ [ WITH ] option [ ... ] ] [ ENCRYPTED | UNENCRYPTED ] { PASSWORD | IDENTIFIED BY } { 'password' [EXPIRED] | DISABLE };
```

The **option** clause is used to configure information, including permissions and properties.

```ebnf+diagram
option ::= {SYSADMIN | NOSYSADMIN}
    | {AUDITADMIN | NOAUDITADMIN}
    | {CREATEDB | NOCREATEDB}
    | {USEFT | NOUSEFT}
    | {CREATEROLE | NOCREATEROLE}
    | {INHERIT | NOINHERIT}
    | {LOGIN | NOLOGIN}
    | {REPLICATION | NOREPLICATION}
    | {INDEPENDENT | NOINDEPENDENT}
    | {VCADMIN | NOVCADMIN}
    | CONNECTION LIMIT connlimit
    | VALID BEGIN 'timestamp'
    | VALID UNTIL 'timestamp'
    | RESOURCE POOL 'respool'
    | PERM SPACE 'spacelimit'
    | TEMP SPACE 'tmpspacelimit'
    | SPILL SPACE 'spillspacelimit'
    | IN ROLE role_name [, ...]
    | IN GROUP role_name [, ...]
    | ROLE role_name [, ...]
    | ADMIN role_name [, ...]
    | USER role_name [, ...]
    | SYSID uid
    | DEFAULT TABLESPACE tablespace_name
    | PROFILE DEFAULT
    | PROFILE profile_name
    | PGUSER
```

## Parameter Description

- **user_name**

  Username

  Value range: a string. It must comply with the naming convention. It can contain a maximum of 63 characters.

- **password**

  Specifies the login password.

  The new password must:

  - Contain at least eight characters. This is the default length.
  - Differ from the username or the username spelled backward.
  - Contain at least three types of the following four types of characters: uppercase characters (A to Z), lowercase characters (a to z), digits (0 to 9), and special characters, including: ~!@#$%^&*()-_=+\|[{}];:,&lt;.&gt;/?
  - Be enclosed by single or double quotation marks.

  Value range: a string

For other parameters, see [CREATE ROLE](CREATE-ROLE.md#parameter-description).

## Example

```sql
-- Create user jim whose login password is xxxxxxxx:
mogdb=# CREATE USER jim PASSWORD 'xxxxxxxx';

-- Alternatively, you can run the following statement:
mogdb=# CREATE USER kim IDENTIFIED BY 'xxxxxxxx';

-- To create a user with the CREATEDB permission, add the CREATEDB keyword.
mogdb=# CREATE USER dim CREATEDB PASSWORD 'xxxxxxxx';

-- Change user jim's login password from xxxxxxxx to Abcd@123:
mogdb=# ALTER USER jim IDENTIFIED BY 'Abcd@123' REPLACE 'xxxxxxxx';

-- Add the CREATEROLE permission to jim.
mogdb=# ALTER USER jim CREATEROLE;

-- Set enable_seqscan to on. (The setting will take effect in the next session.)
mogdb=# ALTER USER jim SET enable_seqscan TO on;

-- Reset the enable_seqscan parameter for jim.
mogdb=# ALTER USER jim RESET enable_seqscan;

-- Lock jim.
mogdb=# ALTER USER jim ACCOUNT LOCK;

-- Delete users.
mogdb=# DROP USER kim CASCADE;
mogdb=# DROP USER jim CASCADE;
mogdb=# DROP USER dim CASCADE;
```
