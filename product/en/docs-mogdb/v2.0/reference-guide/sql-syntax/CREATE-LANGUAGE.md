---
title: CREATE LANGUAGE
summary: CREATE LANGUAGE
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE LANGUAGE

## Function

CREATE LANGUAGE defines a new process language.

## Syntax

```ebnf+diagram
CreateLanguage ::= CREATE [ OR REPLACE ] [ PROCEDURAL ] LANGUAGE name;
```

```ebnf+diagram
CREATE [ OR REPLACE ] [ TRUSTED ] [ PROCEDURAL ] LANGUAGE name
    HANDLER call_handler [ INLINE inline_handler ] [ VALIDATOR valfunction ];
```

## Parameter Description

- **TRUSTED**

  TRUSTED indicates that the language does not authorize users who do not have permissions to access data. If this keyword is ignored when the language is registered, only the super user has the permission to use the language.

- **PROCEDURAL**

  It's a useless word.

- **name**

  The name of the new procedural language. This name should be unique across all languages of the database.

  For downward compatibility, the name can be enclosed in single quotes.

- **HANDLER call_handler**

  call_handler is a previously registered function that will be used to execute the procedural language. The call handler of the procedural language must be written in a compiled language (such as C), the call style must be version 1, and the function must be registered as a function that does not accept parameters and returns the language_handler type. language_handler is a placeholder for declaring a function as a call handler.

- **INLINE inline_handler**

  Inline_handler is the name of a previously registered function that executes an anonymous block of code (**DO** command) in the language. If the inline_handler function is not specified, the language does not support anonymous code blocks. The handler function must accept a parameter of the internal type, which will be the internal representation of the **DO** command, and it usually returns void. Ignore the return value of the handler.

- **VALIDATOR valfunction**

  valfunction is a previously registered function name, which is used to verify a new function when it is created in the language. If the verification function is not declared, it will not be checked when a new function is created. The verification function must accept a parameter of type oid, which is the OID of the function to be created and usually returns void.

  A check function usually checks the function body for syntax errors, but it can also check other attributes of the function, for example, whether the language can process a certain parameter type. The verification function should use the ereport() function to report errors. The return value of this function will be ignored.

## Example

A good way to create a standard procedural language:

```sql
CREATE LANGUAGE plperl;
```

For languages that pg_pltemplate does not yet know, the following sequence is needed:

```sql
CREATE FUNCTION plsample_call_handler() RETURNS language_handler
    AS '$libdir/plsample'
    LANGUAGE C;
CREATE LANGUAGE plsample
    HANDLER plsample_call_handler;
```
