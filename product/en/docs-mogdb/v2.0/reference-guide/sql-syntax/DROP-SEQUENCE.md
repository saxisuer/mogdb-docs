---
title: DROP SEQUENCE
summary: DROP SEQUENCE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SEQUENCE

## Function

**DROP SEQUENCE** deletes a sequence from the current database.

## Precautions

Only the sequence owner or a user granted with the DROP permission can run the **DROP SEQUENCE** command. The system administrator has this permission by default.

## Syntax

```ebnf+diagram
DropSequence ::= DROP SEQUENCE [ IF EXISTS ] {[schema'.']sequence_name} [ , ... ] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified sequence does not exist.

- **name**

  Specifies the name of the sequence to be deleted.

- **CASCADE**

  Automatically deletes the objects that depend on the sequence.

- **RESTRICT**

  Refuses to delete the sequence if any objects depend on it. This is the default action.

## Examples

```sql
-- Create an ascending sequence named serial, starting from 101.
mogdb=# CREATE SEQUENCE serial START 101;

-- Delete a sequence.
mogdb=# DROP SEQUENCE serial;
```
