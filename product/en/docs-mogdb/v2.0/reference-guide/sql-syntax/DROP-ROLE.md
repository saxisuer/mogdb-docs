---
title: DROP ROLE
summary: DROP ROLE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP ROLE

## Function

**DROP ROLE** deletes a role.

## Precautions

None

## Syntax

```ebnf+diagram
DropRole ::= DROP ROLE [ IF EXISTS ] role_name [, ...];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified role does not exist.

- **role_name**

  Specifies the name of the role to be deleted.

  Value range: an existing role name

## Examples

See **Example** in **CREATE ROLE**.
