---
title: DROP LANGUAGE
summary: DROP LANGUAGE
author: Zhang Cuiping
date: 2021-06-07
---

# DROP LANGUAGE

## Function

DROP LANGUAGE deletes a procedural language.

## Syntax

```ebnf+diagram
DropLanguage ::= DROP [ PROCEDURAL ] LANGUAGE [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Do not throw an error if the specified procedural language does not exist. A notice is issued in this case.

- **name**

  Name of an existing language. For downward compatibility, the name can be enclosed in single quotes.

- **CASCADE**

  Automatically deletes objects that depend on the language (for example, functions written in the language).

- **RESTRICT**

  Refuses to delete the object if any objects depend on it. This is a default parameter.

## Example

Run the following command to delete the plsample language:

```sql
DROP LANGUAGE plsample;
```

## Compatibility

The SQL standard does not contain the DROP LANGUAGE statement.
