---
title: COMMIT | END
summary: COMMIT | END
author: Zhang Cuiping
date: 2021-05-10
---

# COMMIT | END

## Function

**COMMIT** or **END** commits all operations of a transaction.

## Precautions

Only the creator of a transaction or a system administrator can run the **COMMIT** command. The creation and commit operations must be in different sessions.

## Syntax

```ebnf+diagram
CommitEnd ::= { COMMIT | END } [ WORK | TRANSACTION ] ;
```

## Parameter Description

- **COMMIT | END**

  Commits the current transaction and makes all changes made by the transaction become visible to others.

- **WORK | TRANSACTION**

  Specifies an optional keyword, which has no effect except increasing readability.

## Examples

```sql
-- Create a table.
mogdb=# CREATE TABLE tpcds.customer_demographics_t2
(
    CD_DEMO_SK                INTEGER               NOT NULL,
    CD_GENDER                 CHAR(1)                       ,
    CD_MARITAL_STATUS         CHAR(1)                       ,
    CD_EDUCATION_STATUS       CHAR(20)                      ,
    CD_PURCHASE_ESTIMATE      INTEGER                       ,
    CD_CREDIT_RATING          CHAR(10)                      ,
    CD_DEP_COUNT              INTEGER                       ,
    CD_DEP_EMPLOYED_COUNT     INTEGER                       ,
    CD_DEP_COLLEGE_COUNT      INTEGER
)
WITH (ORIENTATION = COLUMN,COMPRESSION=MIDDLE)
;

-- Start a transaction.
mogdb=# START TRANSACTION;

-- Insert data.
mogdb=# INSERT INTO tpcds.customer_demographics_t2 VALUES(1,'M', 'U', 'DOCTOR DEGREE', 1200, 'GOOD', 1, 0, 0);
mogdb=# INSERT INTO tpcds.customer_demographics_t2 VALUES(2,'F', 'U', 'MASTER DEGREE', 300, 'BAD', 1, 0, 0);

-- Commit the transaction to make all changes permanent.
mogdb=# COMMIT;

-- Query data.
mogdb=# SELECT * FROM tpcds.customer_demographics_t2;

-- Delete the tpcds.customer_demographics_t2 table.
mogdb=# DROP TABLE tpcds.customer_demographics_t2;
```
