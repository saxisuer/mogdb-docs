---
title: CREATE CLIENT MASTER KEY
summary: CREATE CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE CLIENT MASTER KEY

## Function

**CREATE CLIENT MASTER KEY** creates a client master key (CMK).

## Precautions

When using gsql to connect to the database, you need to add the **-C** option to enable the encrypted database function.

Before creating a CMK, you need to use KeyTool to generate a key.

## Syntax

```ebnf+diagram
CreateClientMasterKey ::= CREATE CLIENT MASTER KEY client_master_key_name WITH '(' master_key_params ')';
```

master_key_params:

```ebnf+diagram
KEY_STORE '=' key_store_value ',' KEY_PATH '=' key_path_value ',' ALGORITHM '=' algorithm_value
```

## Parameter Description

- **client_master_key_name**

  Name of the CMK in the same namespace. The value must be unique.

  Value range: a string. It must comply with the naming convention.

- **mater_key_params**

  Parameters involved in creating a CMK, including:

  - KEY_STORE: indicates the type of the key management tool or component.

  - KEY_STORE: indicates the key tool or component that manages CMK. Currently, only **localkms** is supported.

  - KEY_PATH: indicates the key uniquely identified by KEY_PATH in KEY_STORE.

  - KEY_PATH: KEY_STORE can manage multiple CMKs. KEY_PATH is used for identifying the unique CMK in KEY_STORE. The value is **key_path_value**.

  - ALGORITHM: indicates the encrypted algorithm used by CLIENT MASTER KEY. Currently, only **AES_256_CBC** is supported.

  - ALGORITHM: CMK is used for encrypting COLUMN ENCRYPTION KEY. This parameter is used for specifying the type of the encrypted algorithm. Currently, only **RSA_2048** is supported.

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:** **Key storing path:**By default, **localkms** can generate/read/delete the key file in \$LOCALKMS_FILE_PATH which can be manually configured. Additionally, the user does not need to independently configure the path. When ​\$LOCALKMS_FILE_PATH failed to be obtained, localkms will attempt to obtain $GAUSSHOME/etc/localkms/. If this path exists, it can be used as the key storing path. **Key-related file name**: When CREATE CMK is used, localkms will create four key-related files. For example, when KEY_PATH is set to key_path_value, the four files include key_path_value.pub, key_path_value.pub.rand, key_path_value.priv, and key_path_value.priv.rand. Therefore,  to successfully create key-related files, make sure that no file with the name the same as any key-related file exists in the key storing path.

## Examples

```sql
-- Create the dev_ce user.
mogdb=# CREATE USER dev_ce PASSWORD 'dev@1234';
-- Connect to an encrypted database.
gsql -p 57101 mogdb -U dev_ce -r -C
gsql ((MogDB 2.0.1 build f892ccb7) compiled at 2021-07-09 16:12:59 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

--Create an encrypted client master key (CMK)
mogdb=> CREATE CLIENT MASTER KEY ImgCMK WITH ( KEY_STORE = gs_ktool , KEY_PATH = "gs_ktool/1" , ALGORITHM = AES_256_CBC);
mogdb=> CREATE CLIENT MASTER KEY ImgCMK WITH (KEY_STORE = localkms, KEY_PATH = "key_path_value", ALGORITHM = RSA_2048);
```
