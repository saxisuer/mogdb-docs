---
title: DROP DATABASE
summary: DROP DATABASE
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DATABASE

## Function

**DROP DATABASE** deletes a database.

## Precautions

- Only the database owner or a user granted with the DROP permission can run the **DROP DATABASE** command. The system administrator has this permission by default.
- The preinstalled POSTGRES, TEMPLATE0, and TEMPLATE1 databases are protected and therefore cannot be deleted. To check databases in the current service, run the gsql statement **\l**.
- If any users are connected to the database, the database cannot be deleted.
- **DROP DATABASE** cannot be executed within a transaction block.
- If **DROP DATABASE** fails and is rolled back, run **DROP DATABASE IF EXISTS** again.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** **DROP DATABASE** cannot be undone.

## Syntax

```ebnf+diagram
DropDatabase ::= DROP DATABASE [ IF EXISTS ] database_name ;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified database does not exist.

- **database_name**

  Specifies the name of the database to be deleted.

  Value range: an existing database name

## Examples

See **Examples** in **CREATE DATABASE**.

## Suggestions

- drop database

  Do not delete databases during transactions.
