---
title: CREATE AUDIT POLICY
summary: CREATE AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE AUDIT POLICY

## Function

**CREATE AUDIT POLICY** creates a unified audit policy.

## Precautions

Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.

The masking policy takes effect only after the security policy is enabled, that is, **enable_security_policy** is set to **on**. For details, see "Database Configuration &gt; Database Security Management Policies &gt; Unified Auditing" in the *Security Hardening Guide*.

## Syntax

```ebnf+diagram
CreateAuditPolicy ::= CREATE AUDIT POLICY [ IF NOT EXISTS ] policy_name { { privilege_audit_clause | access_audit_clause } [ filter_group_clause ] [ ENABLE | DISABLE ] };
```

- privilege_audit_clause

  ```ebnf+diagram
  privilege_audit_clause ::= PRIVILEGES { DDL | ALL } [ ON LABEL ( resource_label_name [, ... ] ) ]
  ```

- access_audit_clause

  ```ebnf+diagram
  access_audit_clause ::= ACCESS { DML | ALL } [ ON LABEL ( resource_label_name [, ... ] ) ]
  ```

- filter_group_clause

  ```ebnf+diagram
  filter_group_clause ::= FILTER ON { ( FILTER_TYPE ( filter_value [, ... ] ) ) [, ... ] }
  ```

## Parameter Description

- **policy_name**

  Specifies the audit policy name, which must be unique.

  Value range: a string. It must comply with the naming convention.

- **DDL**

  Specifies the operations that are audited within the database: **CREATE**, **ALTER**, **DROP**, **ANALYZE**, **COMMENT**, **GRANT**, **REVOKE**, **SET**, **SHOW**, **LOGIN_ANY**, **LOGIN_FAILURE**, **LOGIN_SUCCESS**, and **LOGOUT**.

- **ALL**

  Indicates all operations supported by the specified DDL statements in the database.

- **resource_label_name**

  Specifies the resource label name.

- **DML**

  Specifies the operations that are audited within the database: **SELECT**, **COPY**, **DEALLOCATE**, **DELETE**, **EXECUTE**, **INSERT**, **PREPARE**, **REINDEX**, **TRUNCATE**, and **UPDATE**.

- **FILTER_TYPE**

  Specifies the types of information to be filtered by the audit, including **IP**, **APP**, and **ROLES**.

- **filter_value**

  Indicates the detailed information to be filtered.

- **ENABLE|DISABLE**

  Enables or disables the unified audit policy. If **ENABLE|DISABLE** is not specified, **ENABLE** is used by default.

## Examples

```sql
-- Create users dev_audit and bob_audit.
mogdb=# CREATE USER dev_audit PASSWORD 'dev@1234';
mogdb=# CREATE USER bob_audit password 'bob@1234';

-- Create table tb_for_audit.
mogdb=# CREATE TABLE tb_for_audit(col1 text, col2 text, col3 text);

-- Create a resource label.
mogdb=# CREATE RESOURCE LABEL adt_lb0 add TABLE(tb_for_audit);

-- Perform the CREATE operation on the database to create an audit policy.
mogdb=# CREATE AUDIT POLICY adt1 PRIVILEGES CREATE;

-- Perform the SELECT operation on the database to create an audit policy.
mogdb=# CREATE AUDIT POLICY adt2 ACCESS SELECT;

-- Create an audit policy to audit only the CREATE operations performed on the adt_lb0 resource by users dev_audit and bob_audit.
mogdb=# CREATE AUDIT POLICY adt3 PRIVILEGES CREATE ON LABEL(adt_lb0) FILTER ON ROLES(dev_audit, bob_audit);

-- Create an audit policy to audit only the SELECT, INSERT, and DELETE operations performed on the adt_lb0 resource by users dev_audit and bob_audit using client tools psql and gsql on the servers whose IP addresses are 10.20.30.40 and 127.0.0.0/24.
mogdb=# CREATE AUDIT POLICY adt4 ACCESS SELECT ON LABEL(adt_lb0), INSERT ON LABEL(adt_lb0), DELETE FILTER ON ROLES(dev_audit, bob_audit), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24');
```
