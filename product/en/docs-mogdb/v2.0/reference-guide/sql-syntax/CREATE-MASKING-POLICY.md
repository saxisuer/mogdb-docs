---
title: CREATE MASKING POLICY
summary: CREATE MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

## Function

**CREATE MASKING POLICY** creates an anonymization policy.

## Precautions

Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.

The masking policy takes effect only after the security policy is enabled, that is, **enable_security_policy** is set to **on**. For details, see "Database Configuration &gt; Database Security Management Policies &gt; Dynamic Data Masking" in the *Security Hardening Guide*.

## Syntax

```ebnf+diagram
CreateMaskingPolicy ::= CREATE MASKING POLICY policy_name masking_clause[, ...]* policy_filter [ENABLE | DISABLE];
```

- masking_clause

  ```ebnf+diagram
  masking_clause ::= masking_function ON LABEL(label_name[, ...]*)
  ```

- masking_function

  ```ebnf+diagram
  masking_function ::= maskall | randommasking | creditcardmasking | basicemailmasking | fullemailmasking | shufflemasking | alldigitsmasking
  ```

- policy_filter:

  ```ebnf+diagram
  policy_filter ::= FILTER ON FILTER_TYPE(filter_value [,...]*)[,...]*
  ```

- FILTER_TYPE:

  ```ebnf+diagram
  filter_IP | APP | ROLES
  ```

## Parameter Description

- **policy_name**

  Specifies the audit policy name, which must be unique.

  Value range: a string. It must comply with the naming convention.

- **label_name**

  Specifies the resource label name.

- **masking_clause**

  Specifies the masking function used to anonymize database resources labeled by **label_name**.

- **policy_filter**

  Specifies the users for which the anonymization policy takes effect. If this parameter is left empty, the anonymization policy takes effect for all users.

- **FILTER_TYPE**

  Specifies the types of information to be filtered by the policy, including **IP**, **APP**, and **ROLES**.

- **filter_value**

  Indicates the detailed information to be filtered, such as the IP address, app name, and username.

- **ENABLE|DISABLE**

  Enables or disables the masking policy. If **ENABLE|DISABLE** is not specified, **ENABLE** is used by default.

## Examples

```sql
-- Create users dev_mask and bob_mask.
mogdb=# CREATE USER dev_mask PASSWORD 'dev@1234';
mogdb=# CREATE USER bob_mask PASSWORD 'bob@1234';

-- Create table tb_for_masking.
mogdb=# CREATE TABLE tb_for_masking(col1 text, col2 text, col3 text);

-- Create a resource label for label sensitive column col1.
mogdb=# CREATE RESOURCE LABEL mask_lb1 ADD COLUMN(tb_for_masking.col1);

-- Create a resource label for label sensitive column col2.
mogdb=# CREATE RESOURCE LABEL mask_lb2 ADD COLUMN(tb_for_masking.col2);

-- Create an anonymization policy for the operation of accessing sensitive column col1.
mogdb=# CREATE MASKING POLICY maskpol1 maskall ON LABEL(mask_lb1);

-- Create an anonymization policy that takes effect only for scenarios where users are dev_mask and bob_mask, client tools are psql and gsql, and IP addresses are 10.20.30.40, and 127.0.0.0/24.
mogdb=# CREATE MASKING POLICY maskpol2 randommasking ON LABEL(mask_lb2) FILTER ON ROLES(dev_mask, bob_mask), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24');
```
