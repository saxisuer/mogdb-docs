---
title: ALTER RESOURCE LABEL
summary: ALTER RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER RESOURCE LABEL

## Function

**ALTER RESOURCE LABEL** modifies resource labels.

## Precautions

Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.

## Syntax

```ebnf+diagram
AlterResourceLabel ::= ALTER RESOURCE LABEL label_name (ADD|REMOVE)
  label_item_list[, ...]*;
```

- label_item_list

  ```ebnf+diagram
  label_item_list ::= resource_type(resource_path[, ...]*)
  ```

- resource_type

  ```ebnf+diagram
  label_item_list ::= TABLE | COLUMN | SCHEMA | VIEW | FUNCTION
  ```

## Parameter Description

- **label_name**

  Specifies the resource label name.

  Value range: a string. It must comply with the naming convention.

- **resource_type**

  Specifies the type of database resources to be labeled.

- **resource_path**

  Specifies the path of database resources.

## Examples

```sql
-- Create basic table table_for_label.
mogdb=# CREATE TABLE table_for_label(col1 int, col2 text);

-- Create resource label table_label.
mogdb=# CREATE RESOURCE LABEL table_label ADD COLUMN(table_for_label.col1);

-- Attach resource label table_label to col2.
mogdb=# ALTER RESOURCE LABEL table_label ADD COLUMN(table_for_label.col2)

-- Remove table_label from an item.
mogdb=# ALTER RESOURCE LABEL table_label REMOVE COLUMN(table_for_label.col1);
```
