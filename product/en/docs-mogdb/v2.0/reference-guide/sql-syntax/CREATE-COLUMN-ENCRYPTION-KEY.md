---
title: CREATE COLUMN ENCRYPTION KEY
summary: CREATE COLUMN ENCRYPTION KEY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE COLUMN ENCRYPTION KEY

## Function

**CREATE COLUMN ENCRYPTION KEY** creates a column encryption key (CEK).

## Precautions

When using gsql to connect to the database, you need to add the **-C** option to enable the encrypted database function.

## Syntax

```ebnf+diagram
CreateColumnEncryptionKey ::= CREATE COLUMN ENCRYPTION KEY column_encryption_key_name WITH '(' column_key_params ')';
```

column_key_params:

```ebnf+diagram
column_key_params ::= {CLIENT_MASTER_KEY '=' client_master_key_name ',' ALGORITHM '=' algorithm_value | [',' ENCRYPTED_VALUE '=' Sconst]};
```

## Parameter Description

- **column_encryption_key_name**

  Name of the CEK in the same namespace. The value must be unique.

  Value range: a string. It must comply with the naming convention.

- **column_key_params**

  Parameters involved in creating a CEK, including:

  - **CLIENT_MASTER_KEY**: The value is a CMK name.
  - **ALGORITHM**: An algorithm used to encrypt the CEK. Currently, only **AEAD_AES_256_CBC_HMAC_SHA256** and **AEAD_AES_128_CBC_HMAC_SHA256** are supported.
  - **ENCRYPTED_VALUE**: (Optional) The value is a key specified by the user. The key length ranges from 28 to 256 characters. If it is not specified, the key is automatically generated.
  - **ENCRYPTED_VALUE**: (Optional) The value is a key specified by the user. The key length ranges from 28 to 256 characters. The key security light of the 28-length key meets AES128. If the user needs to use AES256, the key length must be 39. If it is not specified, a 256-bit key will be automatically generated.

## Examples

```sql
-- Create a CEK.
mogdb=> CREATE COLUMN ENCRYPTION KEY ImgCEK WITH VALUES (CLIENT_MASTER_KEY = ImgCMK, ALGORITHM  = AEAD_AES_256_CBC_HMAC_SHA256);
CREATE COLUMN ENCRYPTION KEY
```
