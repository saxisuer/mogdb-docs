---
title: DROP AUDIT POLICY
summary: DROP AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# DROP AUDIT POLICY

## Function

**ALTER AUDIT POLICY** modifies the unified audit policy.

## Precautions

- Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.
- The unified audit policy takes effect only after **enable_security_policy** is set to **on**. For details, see "Database Configuration &gt; Database Security Management Policies &gt; Unified Auditing" in the *Security Hardening Guide*.

## Syntax

```ebnf+diagram
DropAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ADD | REMOVE } { [ privilege_audit_clause ] [ access_audit_clause ] };
ALTER AUDIT POLICY [ IF EXISTS ] policy_name MODIFY ( filter_group_clause );
ALTER AUDIT POLICY [ IF EXISTS ] policy_name DROP FILTER;
ALTER AUDIT POLICY [ IF EXISTS ] policy_name COMMENTS policy_comments;
ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ENABLE | DISABLE };
```

- privilege_audit_clause

  ```
  PRIVILEGES { DDL | ALL }
  ```

- access_audit_clause

  ```
  ACCESS { DML | ALL }
  ```

- filter_group_clause

  ```
  FILTER ON { ( FILTER_TYPE ( filter_value [, ... ] ) ) [, ... ] }
  ```

## Parameter Description

- **policy_name**

  Specifies the audit policy name, which must be unique.

  Value range: a string. It must comply with the naming convention.

- **DDL**

  Specifies the operations that are audited within the database: **CREATE**, **ALTER**, **DROP**, **ANALYZE**, **COMMENT**, **GRANT**, **REVOKE**, **SET**, **SHOW**, **LOGIN_ANY**, **LOGIN_FAILURE**, **LOGIN_SUCCESS**, and **LOGOUT**.

- **ALL**

  Indicates all operations supported by the specified DDL statements in the database.

- **DML**

  Specifies the operations that are audited within the database: **SELECT**, **COPY**, **DEALLOCATE**, **DELETE**, **EXECUTE**, **INSERT**, **PREPARE**, **REINDEX**, **TRUNCATE**, and **UPDATE**.

- **FILTER_TYPE**

  Specifies the types of information to be filtered by the audit: **IP**, **ROLES**, and **APP**.

- **filter_value**

  Indicates the detailed information to be filtered.

- **policy_comments**

  Records description information of audit policies.

- **ENABLE|DISABLE**

  Enables or disables the unified audit policy. If **ENABLE|DISABLE** is not specified, **ENABLE** is used by default.

## Examples

See **Examples** in **CREATE AUDIT POLICY**.
