---
title: DROP ROW LEVEL SECURITY POLICY
summary: DROP ROW LEVEL SECURITY POLICY
author: Zhang Cuiping
date: 2021-05-18
---

# DROP ROW LEVEL SECURITY POLICY

## Function

**DROP ROW LEVEL SECURITY POLICY** deletes a row-level access control policy from a table.

## Precautions

Only the owner of a table or a system administrator has the **DROP ROW LEVEL SECURITY POLICY** permission.

## Syntax

```ebnf+diagram
DropRowLevelSecurityPolicy ::= DROP [ ROW LEVEL SECURITY ] POLICY [ IF EXISTS ] policy_name ON table_name [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified row-level access control policy does not exist.

- **policy_name**

  Specifies the name of the row-level access control policy to be deleted.

  - table_name

    Specifies the name of the table containing the row-level access control policy.

  - CASCADE/RESTRICT

    Currently, no objects depend on row-level access control policies. Therefore, **CASCADE** is equivalent to **RESTRICT**, and they are reserved to ensure backward compatibility.

## Examples

```sql
-- Create the data table all_data.
mogdb=# CREATE TABLE all_data(id int, role varchar(100), data varchar(100));

-- Create a row-level access control policy.
mogdb=# CREATE ROW LEVEL SECURITY POLICY all_data_rls ON all_data USING(role = CURRENT_USER);

-- Delete a row-level access control policy.
mogdb=# DROP ROW LEVEL SECURITY POLICY all_data_rls ON all_data;
```
