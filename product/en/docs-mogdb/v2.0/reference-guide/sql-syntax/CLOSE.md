---
title: CLOSE
summary: CLOSE
author: Zhang Cuiping
date: 2021-05-10
---

# CLOSE

## Function

**CLOSE** frees the resources associated with an open cursor.

## Precautions

- After a cursor is closed, no subsequent operations are allowed on it.
- A cursor should be closed when it is no longer needed.
- Every non-holdable open cursor is implicitly closed when a transaction is terminated by **COMMIT** or **ROLLBACK**.
- A holdable cursor is implicitly closed if the transaction that created it aborts by **ROLLBACK**.
- If the cursor creation transaction is successfully committed, the holdable cursor remains open until an explicit **CLOSE** operation is executed, or the client disconnects.
- openGauss does not have an explicit **OPEN** cursor statement. A cursor is considered open when it is declared. You can view all available cursors by querying the **pg_cursors** system view.

## Syntax

```ebnf+diagram
Close ::= CLOSE { cursor_name | ALL } ;
```

## Parameter Description

- **cursor_name**

  Specifies the name of a cursor to be closed.

- **ALL**

  Closes all open cursors.

## Examples

See **Examples** in **FETCH**.
