---
title: DROP CLIENT MASTER KEY
summary: DROP CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP CLIENT MASTER KEY

## Function

**DROP CLIENT MASTER KEY** deletes a client master key (CMK).

## Precautions

- Only the CMK owner or a user who has been granted the DROP permission can run this command. By default, the system administrator has the permission.
- This command can only be used to delete the metadata information recorded in the system catalog of the database, but cannot be used to delete the CMK file. You need to use KeyTool to delete the CMK file.

## Syntax

```ebnf+diagram
DropClientMasterKey ::= DROP CLIENT MASTER KEY [ IF EXISTS ] client_master_key_name [CASCADE];
```

## Parameter Description

- **IF EXISTS**

  If a specified CMK does not exist, a notice rather than an error is issued.

- **client_master_key_name**

  Name of a CMK to be deleted.

  Value range: a string. It is the name of an existing CMK.

- **CASCADE**

  - **CASCADE**: automatically deletes objects that depend on the CMK.

## **Examples**

```sql
-- Delete a CMK object.
mogdb=> DROP CLIENT MASTER KEY ImgCMK CASCADE;
NOTICE:  drop cascades to column setting: imgcek
DROP GLOBAL SETTING
```
