---
title: DROP TYPE
summary: DROP TYPE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TYPE

## Function

**DROP TYPE** deletes a user-defined data type. Only the superuser, sysadmin, and owner of a type has the **DROP TYPE** permission.

## Precautions

Only the type owner or a user granted with the DROP permission can run the **DROP TYPE** command. The system administrator has this permission by default.

## Syntax

```ebnf+diagram
DropType ::= DROP TYPE [ IF EXISTS ] name [, ...] [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified type does not exist.

- **name**

  Specifies the name (optionally schema-qualified) of the type to be deleted.

- **CASCADE**

  Automatically deletes the objects (such as fields, functions, and operators) that depend on the type.

  **RESTRICT**

  Refuses to delete the type if any objects depend on it. This is the default action.

## Example

See **Examples** in **CREATE TYPE**.
