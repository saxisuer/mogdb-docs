---
title: DROP MASKING POLICY
summary: DROP MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# DROP MASKING POLICY

## Function

**DROP MASKING POLICY** deletes an anonymization policy.

## Precautions

Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.

## Syntax

```ebnf+diagram
DropMaskingPolicy ::= DROP MASKING POLICY [IF EXISTS] policy_name;
```

## Parameter Description

**policy_name**

Specifies the audit policy name, which must be unique.

Value range: a string. It must comply with the naming convention.

## Examples

```sql
-- Delete an anonymization policy.
mogdb=# DROP MASKING POLICY IF EXISTS maskpol1;

-- Delete a group of anonymization policies.
mogdb=# DROP MASKING POLICY IF EXISTS maskpol1, maskpol2, maskpol3;
```
