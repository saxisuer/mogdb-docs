---
title: ALTER LARGE OBJECT
summary: ALTER LARGE OBJECT
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER LARGE OBJECT

## Function

**ALTER LARGE OBJECT** changes the owner of a large object.

## Precautions

Only a system administrator or the owner of the to-be-modified large object can run **ALTER LARGE OBJECT**.

## Syntax

```ebnf+diagram
AlterLargeObject ::= ALTER LARGE OBJECT large_object_oid
    OWNER TO new_owner;
```

## Parameter Description

- **large_object_oid**

  Specifies the OID of the large object to be modified.

  Value range: an existing large object name

- **OWNER TO new_owner**

  Specifies the new owner of an object.

  Value range: an existing username or role name

## Examples

None
