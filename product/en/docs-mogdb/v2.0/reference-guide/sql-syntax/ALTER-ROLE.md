---
title: ALTER ROLE
summary: ALTER ROLE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER ROLE

## Function

**ALTER ROLE** modifies role attributes.

## Precautions

None

## Syntax

- Modify the permissions of a role.

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name [ [ WITH ] option [ ... ] ];
  ```

  The **option** clause for granting permissions is as follows:

  ```ebnf+diagram
  option ::= {CREATEDB | NOCREATEDB}
      | {CREATEROLE | NOCREATEROLE}
      | {INHERIT | NOINHERIT}
      | {AUDITADMIN | NOAUDITADMIN}
      | {SYSADMIN | NOSYSADMIN}
      | {USEFT | NOUSEFT}
      | {LOGIN | NOLOGIN}
      | {REPLICATION | NOREPLICATION}
      | {INDEPENDENT | NOINDEPENDENT}
      | {VCADMIN | NOVCADMIN}
      | CONNECTION LIMIT connlimit
      | [ ENCRYPTED | UNENCRYPTED ] PASSWORD 'password' [EXPIRED]
      | [ ENCRYPTED | UNENCRYPTED ] IDENTIFIED BY 'password' [ REPLACE 'old_password' | EXPIRED ]
      | [ ENCRYPTED | UNENCRYPTED ] PASSWORD { 'password' | DISABLE | EXPIRED }
      | [ ENCRYPTED | UNENCRYPTED ] IDENTIFIED BY { 'password' [ REPLACE 'old_password' ] | DISABLE }
      | VALID BEGIN 'timestamp'
      | VALID UNTIL 'timestamp'
      | RESOURCE POOL 'respool'
      | PERM SPACE 'spacelimit'
      | ACCOUNT { LOCK | UNLOCK }
      | PGUSER
  ```

- Rename a role.

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name
      RENAME TO new_name;
  ```

- Set parameters for a role.

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name [ IN DATABASE database_name ]
      SET configuration_parameter {{ TO | = } { value | DEFAULT } | FROM CURRENT};
  ```

- Reset parameters for a role.

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name
      [ IN DATABASE database_name ] RESET {configuration_parameter|ALL};
  ```

## Parameter Description

- **role_name**

  Specifies a role name.

  Value range: an existing username

- **IN DATABASE database_name**

  Modifies the parameters of a role in a specified database.

- **SET configuration_parameter**

  Sets parameters for a role. Session parameters modified by **ALTER ROLE** apply to a specified role and take effect in the next session triggered by the role.

  Value range:

  For details about the values of **configuration_parameter** and **value**, see **SET**.

  **DEFAULT**: clears the value of **configuration_parameter**. **configuration_parameter** will inherit the default value of the new session generated for the role.

  **FROM CURRENT**: uses the value of **configuration_parameter** of the current session.

- **RESET configuration_parameter/ALL**

  Clears the value of **configuration_parameter**. The statement has the same effect as that of **SET configuration_parameter TO DEFAULT**.

  Value range: **ALL** indicates that the values of all parameters are cleared.

- **ACCOUNT LOCK | ACCOUNT UNLOCK**

  - **ACCOUNT LOCK**: locks an account to forbid login to databases.
  - **ACCOUNT UNLOCK**: unlocks an account to allow login to databases.

- **PGUSER**

  In the current version, the **PGUSER** attribute of a role cannot be modified.

For details about other parameters, see **Parameter Description** in **CREATE ROLE**.

## Example

See **Example** in **CREATE ROLE**.
