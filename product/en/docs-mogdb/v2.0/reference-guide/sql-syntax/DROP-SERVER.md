---
title: DROP SERVER
summary: DROP SERVER
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SERVER

## Function

**DROP SERVER** drops a data server.

## Precautions

Only the server owner or a user granted with the DROP permission can run the **DROP SERVER** command. The system administrator has this permission by default.

## Syntax

```ebnf+diagram
DropServer ::= DROP SERVER [ IF EXISTS ] server_name [ {CASCADE | RESTRICT} ] ;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified data server does not exist.

- **server_name**

  Specifies the name of the data server to be dropped.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically drops the objects that depend on the data server.
  - **RESTRICT**: refuses to drop the server if any objects depend on it. This is the default action.
