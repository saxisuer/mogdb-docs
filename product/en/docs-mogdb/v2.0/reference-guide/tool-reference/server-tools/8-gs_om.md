---
title: gs_om
summary: gs_om
author: Zhang Cuiping
date: 2021-06-07
---

# gs_om

## Background

**gs_om**, provided by MogDB, is used to maintain MogDB. You can use it to start and stop MogDB, query the MogDB status, query static configurations, query MogDB status details, generate static configuration file and dynamic configuration file, replace SSL certificates, query help information, and query the version number.

## Prerequisites

Log in to the OS as the OS user **omm** to run the **gs_om** command.

## Syntax

- Start MogDB.

  ```
  gs_om -t start [-h HOSTNAME] [-D dataDir] [--time-out=SECS] [--security-mode=MODE] [-l LOGFILE]
  ```

- Stop MogDB.

  ```
  gs_om -t stop [-h HOSTNAME] [-D dataDir]  [--time-out=SECS] [-m MODE] [-l LOGFILE]
  ```

- Restart MogDB.

  ```
  gs_om -t restart [-h HOSTNAME] [-D dataDir] [--time-out=SECS] [--security-mode=MODE] [-l LOGFILE] [-m MODE]
  ```

- Query the MogDB status.

  ```
  gs_om -t status [-h HOSTNAME] [-o OUTPUT] [--detail] [--all] [-l LOGFILE]
  ```

- Generate a static configuration file.

  ```
  gs_om -t generateconf -X XMLFILE [--distribute] [-l LOGFILE]
  ```

- Generate a dynamic configuration file. Perform this operation after the failover or switchover from the standby node to the primary node.

  ```
  gs_om -t refreshconf
  ```

- Display the static configurations.

  ```
  gs_om -t view [-o OUTPUT]
  ```

- Query the MogDB status details.

  ```
  gs_om -t query [-o OUTPUT]
  ```

- Replace SSL certificates.

  ```
  gs_om -t cert --cert-file=CERTFILE [-l LOGFILE]
  gs_om -t cert --rollback
  ```

- Enable or disable the Kerberos authentication in the cluster.

  ```
  gs_om -t kerberos -m [install|uninstall] -U USER [-l LOGFILE] [--krb-client|--krb-server]
  ```

- Display help information.

  ```
  gs_om -? | --help
  ```

- Display version information.

  ```
  gs_om -V | --version
  ```

## Parameter Description

The **gs_om** tool can use the following types of parameters:

- Common parameters

  - -t

    Specifies the command types of **gs_om**.

    Value range: **start**, **stop**, **status**, **generateconf**, **cert**, **view**, **query**, **refreshconf**, and **kerberos**

  - -l

    Specifies a log file and its storage path.

    Default value: **$GAUSSLOG/om/gs_om**-*YYYY*-*MM*-*DD*_*hhmmss***.log** (default value for **virtualip**: **/tmp/gs_virtualip/gs_om**-*YYYY*-*MM*-*DD*_*hhmmss***.log**)

  - -?, -help

    Displays help information.

  - -V, -version

    Displays version information.

  - MogDB startup parameters

  - -h

    Specifies the name of the server to be started. Only one server can be started at a time.

    Value range: a server name

    If no server name is specified, MogDB is started.

  - -D

    Specifies the DN path.

    Value range: a DN path

    If the DN path is not specified, the DN path in the static file is used.

  - -time-out=SECS

    Specifies the timeout threshold. **gs_om** exits when times out. Unit: s

    Value range: a positive integer. The recommended value is **300**.

    Default value: **300**

  - -security-mode

    Specifies whether to start the database in secure mode.

    Value range: **on**: Start the database in secure mode. **off**: Not start the database in secure mode.

    The secure mode is disabled by default.

- MogDB stop parameters

  - -h

    Specifies the name of the server where the instance to be stopped is located. Only one server can be stopped at a time.

    Value range: the name of the server where the instance is located

    If no server name is specified, MogDB is stopped.

  - -m, -mode=MODE

    Sets the stop mode.

    Value range: the following two stop modes are supported:

    - **fast**: The data of the primary and standby instance relationship is ensured to be consistent.
    - **immediate**: The data of the primary and standby instance relationship is not ensured to be consistent.

    Default value: **fast**

- -time-out=SECS

  Specifies the timeout threshold. **gs_om** exits when times out. Unit: s

  Value range: a positive integer. The recommended value is **300**.

  Default value: **300**

- Parameters for checking status

    [Table 1](#status) describes parameters in the query result.

  - -h

    Specifies the name of the server to be queried.

    Value range: a server name

    If no server name is specified, MogDB is queried.

  - -o

    Outputs status information to the specified output file.

    If the parameter is not output to the specified file, it is displayed on the screen.

  - -detail

    Displays detailed information. If the parameter is not specified, only a prompt is displayed indicating whether the server is normal.

    -all

    Displays information about all MogDB nodes.

- Parameters for generating the configuration file:

  - -X

    Selects the path of the MogDB configuration file.

    Value range: path of the **clusterconfig.xml** file

  - -distribute

    Publishes the static configuration file to the installation directory of the MogDB instance.

- Parameters for enabling or disabling the Kerberos authentication in the cluster

  - -U

    Specifies a database cluster user.

    Value range: a database cluster deployment user

  - -m

    Specifies the operations to be performed.

    Value range:

    **install**: Enables the Kerberos authentication in a cluster is enabled.

    **uninstall**: Disables the Kerberos authentication in a cluster is disabled.

  - **-krb-server**: Installs the Kerberos server authentication.

  - **-krb-client**: Installs the Kerberos client authentication.

    Note: You need to use **-krb-server** to install the server authentication first. These two parameters are not required during the uninstallation because the server and client are uninstalled at the same time.

- Parameters for replacing SSL certificates

  - -cert-file

    Specifies the path of the local SSL certificate .zip package.

    -rollback

    Rolls back the certificates to the last backup.

**Table 1** Status description <a id="status"></a>

| Field          | Field Description                                            | Field Value                                                  |
| :------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| cluster_state  | MogDB status, which indicates whether MogDB is running properly. | - **Normal**: MogDB is available and the data has redundancy backup. All the processes are running and the primary/standby relationship is normal.<br />- **Unavailable**: MogDB is unavailable.<br />- **Degraded**: MogDB is available but the data does not have redundancy backup. |
| redistributing | Data redistribution status.                                  | - **Yes**: MogDB is in data redistribution status.<br />- **No**: MogDB is not in data redistribution status. |
| balanced       | Load balancing status, which indicates whether a primary/standby switchover has occurred in the MogDB instance and made the host load unbalanced. | - **Yes**: The host loading is balanced in MogDB.<br />- **No**: The host loading is not balanced in MogDB. |
| node           | Host name.                                                   | Specifies the name of a host where an instance is located. If multiple AZs exist, the AZ IDs will be displayed. |
| node_ip        | Host IP address.                                             | Specifies the IP address of a host where an instance is located. |
| instance       | Instance ID.                                                 | Specifies the instance ID.                                   |
| state          | Instance status.                                             | - **P**: The initial role of the node is **Primary**. After the database is installed, the role does not change and is read from the system static file.<br />- **S**: The initial role of the node is **Standby**. After the database is installed, the role does not change and is read from the system static file.<br />- **C**: The initial role of the node is **Cascade Standby**. After the database is installed, the role does not change and is read from the system static file.<br />- **Primary**: The instance is a primary instance.<br />- **Standby**: The instance is a standby instance.<br />- **Cascade Standby**: The instance is a cascaded standby instance.<br />- **Secondary**: The instance is a secondary instance.<br />- **Pending**: The instance is in arbitration.<br />- **Unknown**: The instance status is unknown.<br />- **Down**: The instance is down. |

**Table 2** Feature IDs

| Feature Name                                               | ID   | MogDB Product                 |
| :--------------------------------------------------------- | :--- | :---------------------------- |
| Multi-value column                                         | 0    | Basic edition                 |
| JSON                                                       | 1    | License control not supported |
| XML                                                        | 2    | Not supported                 |
| Data storage format ORC                                    | 3    | Basic edition                 |
| One primary and multiple readable standbys on single-node  | 5    | Invalid                       |
| Multidimensional collision and analysis (GPU acceleration) | 7    | Advanced feature              |
| Full-text indexing                                         | 8    | Basic edition                 |
| EXTENSION_CONNECTOR                                        | 9    | Basic edition                 |
| EXPRESS_CLUSTER                                            | 12   | Advanced edition              |
| Cross-DC collaboration                                     | 13   | Advanced edition              |
| Figure                                                     | 14   | Not supported                 |
| Time sequence                                              | 15   | Not supported                 |
| PostGIS interconnection                                    | 16   | Basic edition                 |
| HA in MogDB (one primary and multiple standbys)            | 17   | Invalid                       |
| Row-level permission control                               | 18   | Advanced edition              |
| Transparent encryption                                     | 19   | Advanced edition              |
| Private table                                              | 20   | Advanced edition              |

## Example

- Start MogDB.

  ```
  gs_om -t start
  Starting cluster.
  ======================================================================
  .
  ======================================================================

  Successfully started .
  ```

- Stop MogDB.

  ```
  gs_om -t stop
  Stopping cluster.
  =========================================
  Successfully stopped cluster.
  =========================================
  End stop cluster.
  ```

- View MogDB status details, including instance status.

  ```
  gs_om -t status --detail
  [   Cluster State   ]

  cluster_state   : Normal
  redistributing  : No

  [  Datanode State   ]
  node          node_ip         instance                                    state            | node          node_ip         instance                                    state            | node          node_ip         instance                                    state
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  ```

- Run the following commands on MogDB to generate the configuration file:

  ```
  gs_om -t generateconf -X  /opt/software/openGauss/clusterconfig.xml  --distribute
  Generating static configuration files for all nodes.
  Creating temp directory to store static configuration files.
  Successfully created the temp directory.
  Generating static configuration files.
  Successfully generated static configuration files.
  Static configuration files for all nodes are saved in /opt/huawei/Bigdata/gaussdb/wisequery/script/static_config_files.
  Distributing static configuration files to all nodes.
  Successfully distributed static configuration files.
  ```

  Open the generated configuration file directory. You can see three new files.

  ```
  cd /opt/huawei/Bigdata/gaussdb/wisequery/script/static_config_files
  ll
  total 456
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat1
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat2
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat3
  ```

- Roll back SSL certificates.

  ```
  gs_om -t cert --rollback
  [plat1] SSL cert files rollback successfully.
  [plat2] SSL cert files rollback successfully.
  [plat3] SSL cert files rollback successfully.
  ```

- Register a license.

  ```
  gs_om -t license -m register -f MTgsMTkK
  Preparing for the program initialization.
  Lock the OPS operation of OM components.
  Check and make sure the consistency of the license file.
  Backup the license file on all of the cluster hosts.
  Encrypt the product feature information and generate the license file.
  Send message to the database node instances to reload the license file.
  Remove the backup license file on all of the cluster hosts.
  License register successfully.
  ```

- Deregister a license.

  ```
  gs_om -t license -m unregister -f MTgsMTkK
  Preparing for the program initialization.
  Lock the OPS operation of OM components.
  Check and make sure the consistency of the license file.
  Backup the license file on all of the cluster hosts.
  Encrypt the product feature information and generate the license file.
  Send message to the database node instances to reload the license file.
  Remove the backup license file on all of the cluster hosts.
  License unregister successfully.
  ```
