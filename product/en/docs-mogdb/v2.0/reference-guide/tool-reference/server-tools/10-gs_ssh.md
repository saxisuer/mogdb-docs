---
title: gs_ssh
summary: gs_ssh
author: Zhang Cuiping
date: 2021-06-07
---

# gs_ssh

## Background

**gs_ssh**, provided by MogDB, helps users run the same command on multiple nodes in MogDB.

## Precautions

- **gs_ssh** can only run the command which the OS user **omm** has permission
- The command to be run will not affect the current session where **gs_ssh** is executed. For example, the command such as **cd** or **source** will not affect the current session as it will only be run in the environment within the process of **gs_ssh** itself

## Prerequisites

- The trust relationship among the hosts is normal.
- MogDB has been correctly installed and deployed.
- The command to be run can be found by the **which** command, and the current user has the required execution permission.
- Log in to the OS as the OS user **omm** to run the **gs_ssh** command.

## Syntax

- Run commands synchronously.

  ```
  gs_ssh -c cmd
  ```

- Display help information.

  ```
  gs_ssh -? | --help
  ```

- Display version information.

  ```
  gs_ssh -V | --version
  ```

## Parameter Description

- -c

  Specifies the command name of Linux shell on the hosts of MogDB.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Example

Run the same command on each host of MogDB. Take the **hostname** command as an example.

```
gs_ssh -c "hostname"
Successfully execute command on all nodes.
Output:
[SUCCESS] plat1:
plat1
[SUCCESS] plat2:
plat2
[SUCCESS] plat3:
plat3
[SUCCESS] plat4:
plat4
```
