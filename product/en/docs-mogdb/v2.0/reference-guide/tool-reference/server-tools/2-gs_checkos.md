---
title: gs_checkos
summary: gs_checkos
author: Zhang Cuiping
date: 2021-06-07
---

# gs_checkos

## Context

**gs_checkos** helps you check the OS version information, control parameters, and disk configurations, and configure control parameters, I/O parameters, network parameters, and THP services.

## Prerequisites

- The hardware and network are working properly.
- The trust relationship of user **root** among the hosts is normal.
- Only user **root** is authorized to run the **gs_checkos** command.

## Syntax

- Check OS information.

  ```
  gs_checkos -i ITEM [-f HOSTFILE] [-h HOSTNAME] [-X XMLFILE] [--detail] [-o OUTPUT] [-l LOGFILE]
  ```

- Display help information.

  ```
  gs_checkos -? | --help
  ```

- Display version information.

  ```
  gs_checkos -V | --version
  ```

## Parameter Description

- -i

  Specifies the number of a check item. The format is -i A, -i B1, -i A1 -i A2, or - i A1,A2.

  Value range: A1…A14 and B1…B8

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > Items from A1 to A14 indicate that OS parameters need to be checked but not to be set.
  > Items from B1 to B8 indicate that system parameters need to be set to expected values.
  > Items A and items B cannot be simultaneously set.

  For details, see [Table 1](#oscheck).

- -f

  Specifies the file of a host name list.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The **-f** and **-h** parameters cannot be used together.

  Value range: a host name list

- -h

  Specifies the name of a host to be checked. You can specify multiple hosts and separate the hosts with commas (,).

  Value range: a host name of MogDB. If no host is specified, the current host is checked.

- -X

  Specifies the XML configuration file of MogDB.

- -detail

  Displays detailed check results.

- -o

  Specifies the file for saving OS check reports.

  If this parameter is not specified, OS check results are displayed on the screen.

- -l

  Specifies a log file and its save path.

  Default value: **/tmp/gs_checkos/gs_checkos-YYYY-MM-DD_hhmmss.log**

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

**Table 1** OS check items <a id="oscheck"> </a>

| Number | Check Item                           | Description                                                  |
| :----- | :----------------------------------- | :----------------------------------------------------------- |
| A1     | OS version status                    | Check the OS version. Ensure that the host platform supports SUSE11, SUSE12, RHEL, or CentOS. In addition, you need to ensure that the all host platforms of MogDB belong to the same hybrid range. |
| A2     | Kernel version status                | Check the kernel version.                                    |
| A3     | Unicode status                       | Check the character set. Ensure that the character sets of MogDB hosts are consistent. |
| A4     | Time zone status                     | Checks the time zone setting. Ensure that the time zones of MogDB hosts are consistent. |
| A5     | Swap memory status                   | Check the swap partition. Ensure that the swap partition size of each host is less than or equals to the total memory size of the host. |
| A6     | System control parameters status     | Check the kernel parameter. Ensure that the parameter value of the current kernel is consistent with the default value.<br />For details, see **OS Parameters**. |
| A7     | File system configuration status     | Check the system file handle. Ensure that the parameter value of the system file handle is greater than or equal to **1000000**.<br />Check the thread stack size: Ensure that the thread size is greater than or equal to 3072 KB.<br />Check the maximum available virtual memory of processes. Ensure that the maximum available virtual memory of system processes is **unlimited**. |
| A8     | Disk configuration status            | Check the hard disk mounting parameter. Ensure that the disk format is XFS and its mounting mode is "rw,noatime,inode64,allocsize=16m". |
| A9     | Pre-read block size status           | Check the pre-read parameter. Ensure that the expected value of the pre-read parameter is **16384**. |
| A10    | IO scheduler status                  | Check the I/O scheduling policy. Ensure that the I/O scheduling policy is **deadline**. |
| A11    | Network card configuration status    | Check the NIC smaller than 10GE.<br />- Ensure that **mtu** of the NIC is **1500**.<br />Check the NIC larger than 10GE.<br />- When the NIC is in unbinding mode and is a 10GE card, ensure that the value of **mtu** is **1500** and the value of **rx/tx** is greater than or equal to **4096**.<br />- When the NIC is in binding mode, ensure that the type of each binding NIC is 10GE, the value of **mtu** is **1500**, and the value of **rx/tx** is greater than or equal to **4096**.<br />- The NIC of MogDB's peripheral devices (such as switches) should be set to the same as that of the MogDB hosts. |
| A12    | Time consistency status              | Check the time consistency status. Ensure that the NTP service is started and the system time deviation of each MogDB host does not exceed 60s. |
| A13    | Firewall service status              | Check the firewall status. Ensure that the firewall is disabled. |
| A14    | THP service status                   | Check the THP service status. Ensure that the THP service is disabled. |
| B1     | Set system control parameters        | Sets the kernel parameter. When the actual value does not meet check requirements, reset the parameter whose result is **Abnormal**. Do not set the **Warning** parameter here. You can set the value of **Warning** as needed. |
| B2     | Set file system configuration value  | Set the parameter of system file handles. Set the number of system file handles if it is less than **1000000**.<br />Set the thread stack size. Set the thread stack size if it is less than 3072 KB.<br />Set the maximum available virtual memory of processes: If the maximum available virtual memory of system processes is not **unlimited**, change it to **unlimited**. |
| B3     | Set pre-read block size value        | Set the hard disk pre-read parameter: When the actual value is less than **16384**, reset the parameter. |
| B4     | Set IO scheduler value               | Set the I/O configuration item. If the actual value of the system is not **deadline**, reset the parameter. |
| B5     | Set network card configuration value | Set the 10GE NIC parameters. Set the **rx** and **tx** parameters that do not meet 10GE NIC requirements. Do not set the **mtu** parameter. |
| B6     | Set THP service                      | Set the THP service. If the THP service is enabled, disable it. |
| B7     | Set RemoveIPC value                  | Check EulerOS attributes. Set the value of RemoveIPC in files **/usr/lib/systemd/system/systemd-logind.service** and **/etc/systemd/logind.conf** to **no**. |
| B8     | Set Session Process                  | Set a remote device to inherit default system resources. Modify the **/etc/pam.d/sshd** service file and add the **session required pam_limits.so** configuration item to control resources used by users. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Item A6 needs to be checked based on the **[/etc/sysctl.conf]** and **[SUGGEST:/etc/sysctl.conf]** domains in the **check_list.conf** configuration file.
>   If the parameter value in **[/etc/sysctl.conf]** differs from the actual value, an **Abnormal** message is displayed during the check of item A6. You can set the parameter in item B1.
>   If the parameter value in **[SUGGEST:/etc/sysctl.conf]** differs from the actual value, a Warning message is displayed during the check of item A6. You can manually set the parameter in item B1 as needed.
> - Item A7 checks items specified by the **open file** parameter in the **[/etc/security/ limits.conf]** domain in the **check_list.conf** configuration file. You can set the parameter in item B2.
> - The **mtu**, **rx**, and **tx** parameters of item A11 need to be checked based on the **[/sbin/ifconfig]** domain in the **check_list.conf** configuration file. You can set the **rx** and **tx** parameters using item B5 and set the **mtu** parameter manually.
> - Use configured **gs_checkos** kernel parameter value and file handles parameter value to restart the new session.

## OS Parameters

**Table 2** OS parameters

| Parameter                                | Description                                                  | Recommended Value             |
| :--------------------------------------- | :----------------------------------------------------------- | :---------------------------- |
| net.ipv4.tcp_max_tw_buckets              | Specifies the maximum number of TCP/IP connections concurrently remaining in the **TIME\_WAIT** state. If the number of TCP/IP connections concurrently remaining in the **TIME\_WAIT** state exceeds the value of this parameter, the TCP/IP connections in the **TIME\_WAIT** state will be released immediately, and alarm information will be printed. | 10000                         |
| net.ipv4.tcp_tw_reuse                    | Reuses sockets whose status is **TIME-WAIT** for new TCP connections.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_tw_recycle                  | Rapidly reclaims sockets whose status is **TIME-WAIT** in TCP connections.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_keepalive_time              | Specifies how often Keepalived messages are sent through TCP connections when Keepalived is enabled. | 30                            |
| net.ipv4.tcp_keepalive_probes            | Specifies the number of Keepalived detection packets sent through a TCP connection before the connection is regarded invalid. The product of the parameter value multiplied by the value of the **tcp_keepalive_intvl** parameter determines the response timeout duration after a Keepalived message is sent through a connection. | 9                             |
| net.ipv4.tcp_keepalive_intvl             | Specifies how often a detection packet is re-sent when the previous packets are not acknowledged. | 30                            |
| net.ipv4.tcp_retries1                    | Specifies the maximum TCP reattempts during the connection establishment process. | 5                             |
| net.ipv4.tcp_syn_retries                 | Specifies the maximum SYN packet reattempts in the TCP.      | 5                             |
| net.ipv4.tcp_synack_retries              | Specifies the maximum SYN response packet reattempts in the TCP. | 5                             |
| net.sctp.path_max_retrans                | Specifies the maximum SCTP reattempts.                       | 10                            |
| net.sctp.max_init_retransmits            | Specifies the maximum INIT packet reattempts in the SCTP.    | 10                            |
| net.sctp.association_max_retrans         | Specifies the maximum reattempts of a single logical connection in the SCTP. | 10                            |
| net.sctp.hb_interval                     | Specifies the retransmission interval of heartbeat detection packets in the SCTP. | 30000                         |
| net.ipv4.tcp_retries2                    | Specifies the number of times that the kernel re-sends data to a connected remote host. A smaller value leads to earlier detection of an invalid connection to the remote host, and the server can quickly release this connection.<br />If "connection reset by peer" is displayed, increase the value of this parameter to avoid the problem. | 12                            |
| vm.overcommit_memory                     | Specifies the kernel check method during memory allocation.<br />- **0**: The system accurately calculates the current available memory.<br />- **1**: The system returns a success message without a kernel check.<br />- **2**: The system returns a failure message if the memory size you have applied for exceeds the result of the following formula: Total memory size x Value of **vm.overcommit\_ratio**/100 + Total SWAP size.<br />The default value for is **2**, which is too conservative. The recommended value is **0**. If memory usage is high, set this parameter to **1**. | 0                             |
| net.sctp.sndbuf_policy                   | Specifies the buffer allocation policy on the SCTP sender.<br />- **0**: The buffer is allocated by connection.<br />- **1**: The buffer is allocated by association. | 0                             |
| net.sctp.rcvbuf_policy                   | Specifies the buffer allocation policy on the SCTP receiver.<br />- **0**: The buffer is allocated by connection.<br />- **1**: The buffer is allocated by association. | 0                             |
| net.sctp.sctp_mem                        | Specifies the maximum free memory of the kernel SCTP stack. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. If the value is **max**, packet loss occurs. | 94500000 915000000 927000000  |
| net.sctp.sctp_rmem                       | Specifies the total free memory for receiving data in the kernel SCTP stack. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. If the value is **max**, packet loss occurs. | 8192 250000 16777216          |
| net.sctp.sctp_wmem                       | Specifies the total free memory for sending data in the kernel SCTP stack. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. If the value is **max**, packet loss occurs. | 8192 250000 16777216          |
| net.ipv4.tcp_rmem                        | Specifies the free memory in the TCP receiver buffer. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. | 8192 250000 16777216          |
| net.ipv4.tcp_wmem                        | Specifies the free memory in the TCP sender buffer. Three memory size ranges in the unit of page are provided: **min**, **default**, and **max**. | 8192 250000 16777216          |
| net.core.wmem_max                        | Specifies the maximum size of the socket sender buffer.      | 21299200                      |
| net.core.rmem_max                        | Specifies the maximum size of the socket receiver buffer.    | 21299200                      |
| net.core.wmem_default                    | Specifies the default size of the socket sender buffer.      | 21299200                      |
| net.core.rmem_default                    | Specifies the default size of the socket receiver buffer.    | 21299200                      |
| net.ipv4.ip_local_port_range             | Specifies the range of temporary ports that can be used by a physical server. | 26000-65535                   |
| kernel.sem                               | Specifies the kernel semaphore.                              | 250 6400000 1000 25600        |
| vm.min_free_kbytes                       | Specifies the minimum free physical memory reserved for unexpected page breaks. | 5% of the total system memory |
| net.core.somaxconn                       | Specifies the maximum length of the listening queue of each port. This is a global parameter. | 65535                         |
| net.ipv4.tcp_syncookies                  | Specifies whether to enable SYN cookies to guard the OS against SYN attacks when the SYN waiting queue overflows.<br />- **0**: The SYN cookies are disabled.<br />- **1**: The SYN cookies are enabled. | 1                             |
| net.sctp.addip_enable                    | Specifies whether dynamic address reset of the SCTP is enabled.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 0                             |
| net.core.netdev_max_backlog              | Specifies the maximum number of data packets that can be sent to the queue when the rate at which the network device receives data packets is higher than that at which the kernel processes the data packets. | 65535                         |
| net.ipv4.tcp_max_syn_backlog             | Specifies the maximum number of unacknowledged connection requests to be recorded. | 65535                         |
| net.ipv4.tcp_fin_timeout                 | Specifies the default timeout duration.                      | 60                            |
| kernel.shmall                            | Specifies the total shared free memory of the kernel.        | 1152921504606846720           |
| kernel.shmmax                            | Specifies the maximum value of a shared memory segment.      | 18446744073709551615          |
| net.ipv4.tcp_sack                        | Specifies whether selective acknowledgment is enabled. The selective acknowledgment on out-of-order packets can increase system performance. Restricting users to sending only lost packets (for wide area networks) should be enabled, but this will increase CPU usage.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| net.ipv4.tcp_timestamps                  | Specifies whether the TCP timestamp (12 bytes are added in the TCP packet header) enables a more accurate RTT calculation than the retransmission timeout (for details, see RFC 1323) for better performance.<br />- **0**: This function is disabled.<br />- **1**: This function is enabled. | 1                             |
| vm.extfrag_threshold                     | When system memory is insufficient, Linux will score the current system memory fragments. If the score is higher than the value of **vm.extfrag\_threshold**, **kswapd** triggers memory compaction. When the value of this parameter is close to **1000**, the system tends to swap out old pages when processing memory fragments to meet the application requirements. When the value of this parameter is close to **0**, the system tends to do memory compaction when processing memory fragments. | 500                           |
| vm.overcommit_ratio                      | When the system uses the algorithms where memory usage never exceeds the thresholds, the total memory address space of the system cannot exceed the value of **swap+RAM** multiplied by the percentage specified by this parameter. When the value of **vm.overcommit\_memory** is set to **2**, this parameter takes effect. | 90                            |
| /sys/module/sctp/parameters/no_checksums | Specifies whether **checksum** is disabled in SCTP.          | 0                             |
| MTU                                      | Specifies the maximum transmission unit (MTU) for a node NIC. The default value is **1500** in the OS. You can set it to **8192** to improve the performance of sending and receiving data using SCTP. | 8192                          |

## File System Parameters

- soft nofile

  Indicates the soft restriction. The number of file handles used by a user can exceed this setting value, but an alarm message will be sent if the value is exceeded.

  Recommended value: **1000000**

- hard nofile

  Indicates the hard restriction. The number of file handles used by a user cannot exceed this setting value.

  Recommended value: **1000000**

- stack size

  Indicates the thread stack size.

  Recommended value: **3072**

## Examples

Run the following commands to check the OS parameters:

```
gs_checkos -i A -h plat1 -X /opt/software/mogdb/clusterconfig.xml --detail -o /var/log/checkos
Performing operation system check/set. Output the result to the file /var/log/checkos.
Operation system check/set is completed.
Total numbers:14. Abnormal numbers:0. Warning number:1.
```

Query check results.

```
vim /var/log/checkos
Checking items:
    A1. [ OS version status ]                                   : Normal
    A2. [ MogDB version status ]                               : Normal
    A3. [ Unicode status ]                                      : Normal
    A4. [ Time zone status ]                                    : Normal
    A5. [ Swap memory status ]                                  : Normal
    A6. [ System control parameters status ]                    : Normal
    A7. [ File system configuration status ]                    : Normal
    A8. [ Disk configuration status ]                           : Normal
    A9. [ Pre-read block size status ]                          : Normal
    A10.[ IO scheduler status ]                                 : Normal
    A11.[ Network card configuration status ]                   : Normal
    A12.[ Time consistency status ]                             : Warning
    A13.[ Firewall service status ]                             : Normal
    A14.[ THP service status ]                                  : Normal
```
