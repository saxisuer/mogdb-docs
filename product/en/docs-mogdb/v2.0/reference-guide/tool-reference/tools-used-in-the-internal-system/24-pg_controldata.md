---
title: pg_controldata
summary: pg_controldata
author: Zhang Cuiping
date: 2021-06-07
---

# pg_controldata

## Function

The **pg_controldata** tool is used to display information about control files in the database directory.

## Prerequisites

Ensure that the data directory is correct and the **pg_control** file exists in the corresponding data directory.

## Syntax

- Check the control file information.

  ```
  pg_controldata [DATADIR]
  ```

- Display the version number.

  ```
  pg_controldata -V | --version
  ```

- Display help information.

  ```
  pg_controldata -? | --help
  ```

## Parameter Description

- DATADIR

  Parameter used for viewing the control file information. **DATADIR** indicates the database directory corresponding to the instance.

- -V, -version

  Displays version information.

- -?,-help

  Displays help information.
