---
title: kadmin.local
summary: kadmin.local
author: Zhang Cuiping
date: 2021-06-07
---

# kadmin.local

## Function

**kadmin.local**, provided by the cluster authentication service Kerberos, is used to directly access the **kdc** service database and perform operations such as adding, deleting, and modifying Kerberos users.

## Parameter Description

The Kerberos tool is provided by an open-source third party. For details about the parameters, see the Kerberos official document at <https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>.
