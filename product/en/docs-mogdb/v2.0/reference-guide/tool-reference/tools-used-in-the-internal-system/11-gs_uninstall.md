---
title: gs_uninstall
summary: gs_uninstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_uninstall

## Background

gs_uninstall, provided by , can be used to uninstall a cluster.

## Syntax

- Uninstall a cluster.

  ```
  gs_uninstall [--delete-data] [-L] [-l LOGFILE]
  ```

- Display help information.

  ```
  gs_uninstall -? | --help
  ```

- Display the version number.

  ```
  gs_uninstall -V | --version
  ```

## Parameter Description

- -delete-data

  Deletes the data file.

- -L

Uninstalls the local host only. If a host in the cluster is uninstalled, the cluster cannot undergo full uninstallation.

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**

After executing gs_postuninstall, the system will automatically delete the directories related to MogDB (including $GAUSSLOG). You are advised to set the log file path to a path outside the MogDB database.

- -l

Specifies a log file name and its path. A timestamp will be added automatically to the log file name.

When **-l** is not specified and **gaussdbLogPath** is not set in the XML file, the default value of the **-l** parameter is **/var/log/gaussdb/om/gs_local-YYYY-MMDD_hhmmss.log**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**NOTE:** After executing gs_uninstall, the system will automatically delete the directories related to MogDB (including $GAUSSLOG). You are advised to set the log file path to a path outside the MogDB database.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

## Example

Use the gs_uninstall script to uninstall a cluster.

```
gs_uninstall --delete-data
Checking uninstallation.
Successfully checked uninstallation.
Stopping the cluster.
Successfully stopped the cluster.
Successfully deleted instances.
Uninstalling application.
Successfully uninstalled application.
Uninstallation succeeded.
```
