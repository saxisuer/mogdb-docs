---
title: Server Signal Functions
summary: Server Signal Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Server Signal Functions

Server signaling functions send control signals to other server processes. Only system administrators can use these functions.

- pg_cancel_backend(pid int)

  Description: Cancels the current query of a backend.

  Return type: Boolean

  Note:**pg_cancel_backend** sends a query cancellation (SIGINT) signal to the backend process identified by **pid**. The PID of an active backend process can be found in the **pid** column of the **pg_stat_activity** view, or can be found by listing the database process using **ps** on the server.

- pg_reload_conf()

  Description: Causes all server processes to reload their configuration files.

  Return type: Boolean

  Note:**pg_reload_conf** sends a SIGHUP signal to the server. As a result, all server processes reload their configuration files.

- pg_rotate_logfile()

  Description: Rotates the log files of the server.

  Return type: Boolean

  Note:**pg_rotate_logfile** sends a signal to the log file manager, instructing the manager to immediately switch to a new output file. This function works only when **redirect_stderr** is used for log output. Otherwise, no log file manager subprocess exists.

- pg_terminate_backend(pid int)

  Description: Terminates a backend thread.

  Return type: Boolean

  Note: Each of these functions returns **true** if they are successful and **false** otherwise.

  For example:

  ```sql
  mogdb=# SELECT pid from pg_stat_activity;
         pid
  -----------------
   140657876268816
  (1 rows)

  mogdb=# SELECT pg_terminate_backend(140657876268816);
   pg_terminate_backend
  ----------------------
   t
  (1 row)
  ```
