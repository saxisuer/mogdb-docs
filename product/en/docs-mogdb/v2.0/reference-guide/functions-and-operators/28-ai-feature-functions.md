---
title: AI Feature Functions
summary: AI Feature Functions
author: Zhang Cuiping
date: 2021-04-20
---

# AI Feature Functions

- gs_index_advise(text)

  Description: Recommends an index for a single query statement.

  Parameter: SQL statement string

  Return type: record

  Single-query Index Recommendation describes the examples.

- hypopg_create_index(text)

  Description: Creates a virtual index.

  Parameter: character string of the statement for creating an index

  Return type: record

  Virtual Index describes the examples.

- hypopg_display_index()

  Description: Displays information about all created virtual indexes.

  Parameter: none

  Return type: record

  Virtual Index describes the examples.

- hypopg_drop_index(oid)

  Description: Deletes a specified virtual index.

  Parameter: OID of the index

  Return type: Boolean

  Virtual Index describes the examples.

- hypopg_reset_index()

  Description: Clears all virtual indexes.

  Parameter: none

  Return type: none

  Virtual Index describes the examples.

- hypopg_estimate_size(oid)

  Description: Estimates the space required for creating a specified index.

  Parameter: OID of the index

  Return type: int8

  Virtual Index describes the examples.

- check_engine_status(ip text, port text)

  Description: Tests whether a predictor engine provides services on a specified IP address and port.

  Parameter: IP address and port number of the predictor engine.

  Return type: text

  **User Guide** describes the examples.

- encode_plan_node(optname text, orientation text, strategy text, options text, dop int8, quals text, projection text)

  Description: Encodes the plan operator information in the input parameters.

  Parameter: plan operator information.

  Return type: text.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This function is an internal function. You are not advised to use it directly.

- model_train_opt(template text, model text)

  Description: Trains a given query performance prediction model.

  Parameters: template name and model name of the performance prediction model.

  Return type: tartup_time_accuracy FLOAT8, total_time_accuracy FLOAT8, rows_accuracy FLOAT8, peak_memory_accuracy FLOAT8

  **User Guide** describes the examples.

- track_model_train_opt(ip text, port text)

  Description: Returns the training log address of the specified IP address and port predictor engine.

  Parameter: IP address and port number of the predictor engine.

  Return type: text

  **User Guide** describes the examples.

- encode_feature_perf_hist(datname text)

  Description: Encodes historical plan operators collected in the target database.

  Parameter: database name

  Return type: queryid bigint, plan_node_id int, parent_node_id int, left_child_id int, right_child_id int, encode text, startup_time bigint, total_time bigint, rows bigint, peak_memory int

  **User Guide** describes the examples.

- gather_encoding_info(datname text)

  Description: Invokes encode_feature_perf_hist to save the encoded data persistently.

  Parameter: database name

  Return type: int

  **User Guide** describes the examples.
