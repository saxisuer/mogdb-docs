---
title: Other System Functions
summary: Other System Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Other System Functions

The MogDB built-in functions and operators are inherited from the open-source PG. For details about the following functions, see the [official PG documents](https://www.postgresql.org/docs/9.2/functions.html).

| _pg_char_max_length              | _pg_char_octet_length                    | _pg_datetime_precision             | _pg_expandarray                     | _pg_index_position             | _pg_interval_type                  | _pg_numeric_precision                |
| -------------------------------- | ---------------------------------------- | ---------------------------------- | ----------------------------------- | ------------------------------ | ---------------------------------- | ------------------------------------ |
| _pg_numeric_precision_radix      | _pg_numeric_scale                        | _pg_truetypid                      | _pg_truetypmod                      | abbrev                         | abs                                | abstime                              |
| abstimeeq                        | abstimege                                | abstimegt                          | abstimein                           | abstimele                      | abstimelt                          | abstimene                            |
| abstimeout                       | abstimerecv                              | abstimesend                        | aclcontains                         | acldefault                     | aclexplode                         | aclinsert                            |
| aclitemeq                        | aclitemin                                | aclitemout                         | aclremove                           | acos                           | age                                | akeys                                |
| any_in                           | any_out                                  | anyarray_in                        | anyarray_out                        | anyarray_recv                  | anyarray_send                      | anyelement_in                        |
| anyelement_out                   | anyenum_in                               | anyenum_out                        | anynonarray_in                      | anynonarray_out                | anyrange_in                        | anyrange_out                         |
| anytextcat                       | area                                     | areajoinsel                        | areasel                             | array_agg                      | array_agg_finalfn                  | array_agg_transfn                    |
| array_append                     | array_cat                                | array_dims                         | array_eq                            | array_fill                     | array_ge                           | array_gt                             |
| array_in                         | array_larger                             | array_le                           | array_length                        | array_lower                    | array_lt                           | array_ndims                          |
| array_ne                         | array_out                                | array_prepend                      | array_recv                          | array_send                     | array_smaller                      | array_to_json                        |
| array_to_string                  | array_typanalyze                         | array_upper                        | arraycontained                      | arraycontains                  | arraycontjoinsel                   | arraycontsel                         |
| arrayoverlap                     | ascii                                    | asin                               | atan                                | atan2                          | avals                              | avg                                  |
| big5_to_euc_tw                   | big5_to_mic                              | big5_to_utf8                       | bit                                 | bit_and                        | bit_in                             | bit_length                           |
| bit_or                           | bit_out                                  | bit_recv                           | bit_send                            | bitand                         | bitcat                             | bitcmp                               |
| biteq                            | bitge                                    | bitgt                              | bitle                               | bitlt                          | bitne                              | bitnot                               |
| bitor                            | bitshiftleft                             | bitshiftright                      | bittypmodin                         | bittypmodout                   | bitxor                             | bool                                 |
| bool_and                         | bool_or                                  | booland_statefunc                  | booleq                              | boolge                         | boolgt                             | boolin                               |
| boolle                           | boollt                                   | boolne                             | boolor_statefunc                    | boolout                        | boolrecv                           | boolsend                             |
| box                              | box_above                                | box_above_eq                       | box_add                             | box_below                      | box_below_eq                       | box_center                           |
| box_contain                      | box_contain_pt                           | box_contained                      | box_distance                        | box_div                        | box_eq                             | box_ge                               |
| box_gt                           | box_in                                   | box_intersect                      | box_le                              | box_left                       | box_lt                             | box_mul                              |
| box_out                          | box_overabove                            | box_overbelow                      | box_overlap                         | box_overleft                   | box_overright                      | box_recv                             |
| box_right                        | box_same                                 | box_send                           | box_sub                             | bpchar                         | bpchar_larger                      | bpchar_pattern_ge                    |
| bpchar_pattern_gt                | bpchar_pattern_le                        | bpchar_pattern_lt                  | bpchar_smaller                      | bpchar_sortsupport             | bpcharcmp                          | bpchareq                             |
| bpcharge                         | bpchargt                                 | bpchariclike                       | bpcharicnlike                       | bpcharicregexeq                | bpcharicregexne                    | bpcharin                             |
| bpcharle                         | bpcharlike                               | bpcharlt                           | bpcharne                            | bpcharnlike                    | bpcharout                          | bpcharrecv                           |
| bpcharregexeq                    | bpcharregexne                            | bpcharsend                         | bpchartypmodin                      | bpchartypmodout                | broadcast                          | btabstimecmp                         |
| btarraycmp                       | btbeginscan                              | btboolcmp                          | btbpchar_pattern_cmp                | btbuild                        | btbuildempty                       | btbulkdelete                         |
| btcanreturn                      | btcharcmp                                | btcostestimate                     | btendscan                           | btfloat48cmp                   | btfloat4cmp                        | btfloat4sortsupport                  |
| btfloat84cmp                     | btfloat8cmp                              | btfloat8sortsupport                | btgetbitmap                         | btgettuple                     | btinsert                           | btint24cmp                           |
| btint28cmp                       | btint2cmp                                | btint2sortsupport                  | btint42cmp                          | btint48cmp                     | btint4cmp                          | btint4sortsupport                    |
| btint82cmp                       | btint84cmp                               | btint8cmp                          | btint8sortsupport                   | btmarkpos                      | btnamecmp                          | btnamesortsupport                    |
| btoidcmp                         | btoidsortsupport                         | btoidvectorcmp                     | btoptions                           | btrecordcmp                    | btreltimecmp                       | btrescan                             |
| btrestrpos                       | btrim                                    | bttext_pattern_cmp                 | bttextcmp                           | bttextsortsupport              | bttidcmp                           | bttintervalcmp                       |
| btvacuumcleanup                  | bytea_sortsupport                        | bytea_string_agg_finalfn           | bytea_string_agg_transfn            | byteacat                       | byteacmp                           | byteaeq                              |
| byteage                          | byteagt                                  | byteain                            | byteale                             | bytealike                      | bytealt                            | byteane                              |
| byteanlike                       | byteaout                                 | bytearecv                          | byteasend                           | cash_cmp                       | cash_div_cash                      | cash_div_flt4                        |
| cash_div_flt8                    | cash_div_int2                            | cash_div_int4                      | cash_div_int8                       | cash_eq                        | cash_ge                            | cash_gt                              |
| cash_in                          | cash_le                                  | cash_lt                            | cash_mi                             | cash_mul_flt4                  | cash_mul_flt8                      | cash_mul_int2                        |
| cash_mul_int4                    | cash_mul_int8                            | cash_ne                            | cash_out                            | cash_pl                        | cash_recv                          | cash_send                            |
| cashlarger                       | cashsmaller                              | cbrt                               | ceil                                | ceiling                        | center                             | char                                 |
| char_length                      | character_length                         | chareq                             | charge                              | chargt                         | charin                             | charle                               |
| charlt                           | charne                                   | charout                            | charrecv                            | charsend                       | chr                                | cideq                                |
| cidin                            | cidout                                   | cidr                               | cidr_in                             | cidr_out                       | cidr_recv                          | cidr_send                            |
| cidrecv                          | cidsend                                  | circle                             | circle_above                        | circle_add_pt                  | circle_below                       | circle_center                        |
| circle_contain                   | circle_contain_pt                        | circle_contained                   | circle_distance                     | circle_div_pt                  | circle_eq                          | circle_ge                            |
| circle_gt                        | circle_in                                | circle_le                          | circle_left                         | circle_lt                      | circle_mul_pt                      | circle_ne                            |
| circle_out                       | circle_overabove                         | circle_overbelow                   | circle_overlap                      | circle_overleft                | circle_overright                   | circle_recv                          |
| circle_right                     | circle_same                              | circle_send                        | circle_sub_pt                       | clock_timestamp                | close_lb                           | close_ls                             |
| close_lseg                       | close_pb                                 | close_pl                           | close_ps                            | close_sb                       | close_sl                           | col_description                      |
| concat                           | concat_ws                                | contjoinsel                        | contsel                             | convert                        | convert_from                       | convert_to                           |
| corr                             | cos                                      | cot                                | count                               | covar_pop                      | covar_samp                         | cstring_in                           |
| cstring_out                      | cstring_recv                             | cstring_send                       | cume_dist                           | current_database               | current_query                      | current_schema                       |
| xpath_exists                     | current_setting                          | current_user                       | currtid                             | currtid2                       | currval                            | cursor_to_xml                        |
| cursor_to_xmlschema              | database_to_xml                          | database_to_xml_and_xmlschema      | database_to_xmlschema               | date                           | date_cmp                           | date_cmp_timestamp                   |
| date_cmp_timestamptz             | date_eq                                  | date_eq_timestamp                  | date_eq_timestamptz                 | date_ge                        | date_ge_timestamp                  | date_ge_timestamptz                  |
| date_gt                          | date_gt_timestamp                        | date_gt_timestamptz                | date_in                             | date_larger                    | date_le                            | date_le_timestamp                    |
| date_le_timestamptz              | date_lt                                  | date_lt_timestamp                  | date_lt_timestamptz                 | date_mi                        | date_mi_interval                   | date_mii                             |
| date_ne                          | date_ne_timestamp                        | date_ne_timestamptz                | date_out                            | date_pl_interval               | date_pli                           | date_recv                            |
| date_send                        | date_smaller                             | date_sortsupport                   | daterange_canonical                 | daterange_subdiff              | datetime_pl                        | datetimetz_pl                        |
| dcbrt                            | decode                                   | defined                            | degrees                             | delete                         | dense_rank                         | dexp                                 |
| diagonal                         | diameter                                 | dispell_init                       | dispell_lexize                      | dist_cpoly                     | dist_lb                            | dist_pb                              |
| dist_pc                          | dist_pl                                  | dist_ppath                         | dist_ps                             | dist_sb                        | dist_sl                            | div                                  |
| dlog1                            | dlog10                                   | domain_in                          | domain_recv                         | dpow                           | dround                             | dsimple_init                         |
| dsimple_lexize                   | dsnowball_init                           | dsnowball_lexize                   | dsqrt                               | dsynonym_init                  | dsynonym_lexize                    | dtrunc                               |
| each                             | enum_ne                                  | enum_out                           | enum_range                          | enum_recv                      | enum_send                          | enum_smaller                         |
| eqjoinsel                        | eqsel                                    | euc_cn_to_mic                      | euc_cn_to_utf8                      | euc_jis_2004_to_shift_jis_2004 | euc_jis_2004_to_utf8               | euc_jp_to_mic                        |
| euc_jp_to_sjis                   | euc_jp_to_utf8                           | euc_kr_to_mic                      | euc_kr_to_utf8                      | euc_tw_to_big5                 | euc_tw_to_mic                      | euc_tw_to_utf8                       |
| every                            | exist                                    | exists_all                         | exists_any                          | exp                            | factorial                          | family                               |
| fdw_handler_in                   | fdw_handler_out                          | fetchval                           | first_value                         | float4                         | float4_accum                       | float48div                           |
| float48eq                        | float48ge                                | float48gt                          | float48le                           | float48lt                      | float48mi                          | float48mul                           |
| float48ne                        | float48pl                                | float4abs                          | float4div                           | float4eq                       | float4ge                           | float4gt                             |
| float4in                         | float4larger                             | float4le                           | float4lt                            | float4mi                       | float4mul                          | float4ne                             |
| float4out                        | float4pl                                 | float4recv                         | float4send                          | float4smaller                  | float4um                           | float4up                             |
| float8                           | float8_accum                             | float8_avg                         | float8_collect                      | float8_corr                    | float8_covar_pop                   | float8_covar_samp                    |
| float8_regr_accum                | float8_regr_avgx                         | float8_regr_avgy                   | float8_regr_collect                 | float8_regr_intercept          | float8_regr_r2                     | float8_regr_slope                    |
| float8_regr_sxx                  | float8_regr_sxy                          | float8_regr_syy                    | float8_stddev_pop                   | float8_stddev_samp             | float8_var_pop                     | float8_var_samp                      |
| float84div                       | float84eq                                | float84ge                          | float84gt                           | float84le                      | float84lt                          | float84mi                            |
| float84mul                       | float84ne                                | float84pl                          | float8abs                           | float8div                      | float8eq                           | float8ge                             |
| float8gt                         | float8in                                 | float8larger                       | float8le                            | float8lt                       | float8mi                           | float8mul                            |
| float8ne                         | float8out                                | float8pl                           | float8recv                          | float8send                     | float8smaller                      | float8um                             |
| float8up                         | floor                                    | flt4_mul_cash                      | flt8_mul_cash                       | fmgr_c_validator               | fmgr_internal_validator            | fmgr_sql_validator                   |
| format                           | format_type                              | gb18030_to_utf8                    | gbk_to_utf8                         | generate_series                | generate_subscripts                | get_bit                              |
| get_byte                         | get_current_ts_config                    | -                                  | -                                   | gin_clean_pending_list         | gin_cmp_prefix                     | gin_cmp_tslexeme                     |
| gin_extract_tsquery              | gin_extract_tsvector                     | gin_tsquery_consistent             | gin_tsquery_triconsistent           | ginarrayconsistent             | ginarrayextract                    | ginarraytriconsistent                |
| ginbeginscan                     | ginbuild                                 | ginbuildempty                      | ginbulkdelete                       | gincostestimate                | ginendscan                         | gingetbitmap                         |
| gininsert                        | ginmarkpos                               | ginoptions                         | ginqueryarrayextract                | ginrescan                      | ginrestrpos                        | ginvacuumcleanup                     |
| gist_box_compress                | gist_box_consistent                      | gist_box_decompress                | gist_box_penalty                    | gist_box_picksplit             | gist_box_same                      | gist_box_union                       |
| gist_circle_compress             | gist_circle_consistent                   | gist_point_compress                | gist_point_consistent               | gist_point_distance            | gist_poly_compress                 | gist_poly_consistent                 |
| gistbeginscan                    | gistbuild                                | gistbuildempty                     | gistbulkdelete                      | gistcostestimate               | gistendscan                        | gistgetbitmap                        |
| gistgettuple                     | gistinsert                               | gistmarkpos                        | gistoptions                         | gistrescan                     | gistrestrpos                       | gistvacuumcleanup                    |
| gtsquery_compress                | gtsquery_consistent                      | gtsquery_decompress                | gtsquery_penalty                    | gtsquery_picksplit             | gtsquery_same                      | gtsquery_union                       |
| gtsvector_compress               | gtsvector_consistent                     | gtsvector_decompress               | gtsvector_penalty                   | gtsvector_picksplit            | gtsvector_same                     | gtsvector_union                      |
| gtsvectorin                      | gtsvectorout                             | has_tablespace_privilege           | has_type_privilege                  | hash_aclitem                   | hashbeginscan                      | hashbuild                            |
| hashbuildempty                   | hashbulkdelete                           | hashcostestimate                   | hashendscan                         | hashgetbitmap                  | hashgettuple                       | hashinsert                           |
| hashint2vector                   | hashint4                                 | hashint8                           | hashmacaddr                         | hashmarkpos                    | hashname                           | hashoid                              |
| hashoidvector                    | hashoptions                              | hashrescan                         | hashrestrpos                        | hashtext                       | hashvacuumcleanup                  | hashvarlena                          |
| host                             | hostmask                                 | iclikejoinsel                      | iclikesel                           | icnlikejoinsel                 | icnlikesel                         | icregexeqjoinsel                     |
| icregexeqsel                     | icregexnejoinsel                         | icregexnesel                       | inet_client_addr                    | inet_client_port               | inet_in                            | inet_out                             |
| inet_recv                        | inet_send                                | inet_server_addr                   | inet_server_port                    | inetand                        | inetmi                             | inetmi_int8                          |
| inetnot                          | inetor                                   | inetpl                             | initcap                             | int2_accum                     | int2_avg_accum                     | int2_mul_cash                        |
| int2_sum                         | int24div                                 | int24eq                            | int24ge                             | int24gt                        | int24le                            | int24lt                              |
| int24mi                          | int24mul                                 | int24ne                            | int24pl                             | int28div                       | int28eq                            | int28ge                              |
| int28gt                          | int28le                                  | int28lt                            | int28mi                             | int28mul                       | int28ne                            | int28pl                              |
| int2abs                          | int2and                                  | int2div                            | int2eq                              | int2ge                         | int2gt                             | int2in                               |
| int2larger                       | int2le                                   | int2lt                             | int2mi                              | int2mod                        | int2mul                            | int2ne                               |
| int2not                          | int2or                                   | int2out                            | int2pl                              | int2recv                       | int2send                           | int2shl                              |
| int2shr                          | int2smaller                              | int2um                             | int2up                              | int2vectoreq                   | int2vectorin                       | int2vectorout                        |
| int2vectorrecv                   | int2vectorsend                           | int2xor                            | int4_accum                          | int4_avg_accum                 | int4_mul_cash                      | int4_sum                             |
| int42div                         | int42eq                                  | int42ge                            | int42gt                             | int42le                        | int42lt                            | int42mi                              |
| int42mul                         | int42ne                                  | int42pl                            | int48div                            | int48eq                        | int48ge                            | int48gt                              |
| int48le                          | int48lt                                  | int48mi                            | int48mul                            | int48ne                        | int48pl                            | int4abs                              |
| int4and                          | int4div                                  | int4eq                             | int4ge                              | int4gt                         | int4in                             | int4inc                              |
| int4larger                       | int4le                                   | int4lt                             | int4mi                              | int4mod                        | int4mul                            | int4ne                               |
| int4not                          | int4or                                   | int4out                            | int4pl                              | int4range                      | int4range_canonical                | int4range_subdiff                    |
| int4recv                         | int4send                                 | int4shl                            | int4shr                             | int4smaller                    | int4um                             | int4up                               |
| int4xor                          | int8                                     | int8_avg                           | int8_avg_accum                      | int8_avg_collect               | int8_mul_cash                      | int8_sum                             |
| int8_sum_to_int8                 | int8_accum                               | int82div                           | int82eq                             | int82ge                        | int82gt                            | int82le                              |
| int82lt                          | int82mi                                  | int82mul                           | int82ne                             | int82pl                        | int84div                           | int84eq                              |
| int84ge                          | int84gt                                  | int84le                            | int84lt                             | int84mi                        | int84mul                           | int84ne                              |
| int84pl                          | int8abs                                  | int8and                            | int8div                             | int8eq                         | int8ge                             | int8gt                               |
| int8in                           | int8inc                                  | int8inc_any                        | int8inc_float8_float8               | int8larger                     | int8le                             | int8lt                               |
| int8mi                           | int8mod                                  | int8mul                            | int8ne                              | int8not                        | int8or                             | int8out                              |
| int8pl                           | int8pl_inet                              | int8range                          | int8range_canonical                 | int8range_subdiff              | int8recv                           | int8send                             |
| int8shl                          | int8shr                                  | int8smaller                        | int8um                              | int8up                         | int8xor                            | integer_pl_date                      |
| inter_lb                         | inter_sb                                 | inter_sl                           | internal_in                         | internal_out                   | interval                           | interval_accum                       |
| interval_avg                     | interval_cmp                             | interval_collect                   | interval_div                        | interval_eq                    | interval_ge                        | interval_gt                          |
| interval_hash                    | interval_in                              | interval_larger                    | interval_le                         | interval_lt                    | interval_mi                        | interval_mul                         |
| interval_ne                      | interval_out                             | interval_pl                        | interval_pl_date                    | interval_pl_time               | interval_pl_timestamp              | interval_pl_timestamptz              |
| interval_pl_timetz               | interval_recv                            | interval_send                      | interval_smaller                    | interval_transform             | interval_um                        | intervaltypmodin                     |
| intervaltypmodout                | intinterval                              | isexists                           | ishorizontal                        | iso_to_koi8r                   | iso_to_mic                         | iso_to_win1251                       |
| iso_to_win866                    | iso8859_1_to_utf8                        | iso8859_to_utf8                    | isparallel                          | isperp                         | isvertical                         | johab_to_utf8                        |
| json_in                          | json_out                                 | json_recv                          | json_send                           | justify_days                   | justify_hours                      | justify_interval                     |
| koi8r_to_iso                     | koi8r_to_mic                             | koi8r_to_utf8                      | koi8r_to_win1251                    | koi8r_to_win866                | koi8u_to_utf8                      | language_handler_in                  |
| language_handler_out             | latin1_to_mic                            | latin2_to_mic                      | latin2_to_win1250                   | latin3_to_mic                  | latin4_to_mic                      | like_escape                          |
| likejoinsel                      | likesel                                  | line                               | line_distance                       | line_eq                        | line_horizontal                    | line_in                              |
| line_interpt                     | line_intersect                           | line_out                           | line_parallel                       | line_perp                      | line_recv                          | line_send                            |
| line_vertical                    | ln                                       | lo_close                           | lo_creat                            | lo_create                      | lo_export                          | lo_import                            |
| lo_lseek                         | lo_open                                  | lo_tell                            | lo_truncate                         | lo_unlink                      | log                                | loread                               |
| lower                            | lower_inc                                | lower_inf                          | lowrite                             | lpad                           | lseg                               | lseg_center                          |
| lseg_distance                    | lseg_eq                                  | lseg_ge                            | lseg_gt                             | lseg_horizontal                | lseg_in                            | lseg_interpt                         |
| lseg_intersect                   | lseg_le                                  | lseg_length                        | lseg_lt                             | lseg_ne                        | lseg_out                           | lseg_parallel                        |
| lseg_perp                        | lseg_recv                                | lseg_send                          | lseg_vertical                       | ltrim                          | macaddr_and                        | macaddr_cmp                          |
| macaddr_eq                       | macaddr_ge                               | macaddr_gt                         | macaddr_in                          | macaddr_le                     | macaddr_lt                         | macaddr_ne                           |
| macaddr_not                      | macaddr_or                               | macaddr_out                        | macaddr_recv                        | macaddr_send                   | makeaclitem                        | masklen                              |
| max                              | md5                                      | mic_to_big5                        | mic_to_euc_cn                       | mic_to_euc_jp                  | mic_to_euc_kr                      | mic_to_euc_tw                        |
| mic_to_iso                       | mic_to_koi8r                             | mic_to_latin1                      | mic_to_latin2                       | mic_to_latin3                  | mic_to_latin4                      | mic_to_sjis                          |
| mic_to_win1250                   | mic_to_win1251                           | mic_to_win866                      | min                                 | mktinterval                    | money                              | mul_d_interval                       |
| name                             | nameeq                                   | namege                             | namegt                              | nameiclike                     | nameicnlike                        | nameicregexeq                        |
| nameicregexne                    | namein                                   | namele                             | namelike                            | namelt                         | namene                             | namenlike                            |
| nameout                          | namerecv                                 | nameregexeq                        | nameregexne                         | namesend                       | neqjoinsel                         | neqsel                               |
| network_cmp                      | network_eq                               | network_ge                         | network_gt                          | network_le                     | network_lt                         | network_ne                           |
| network_sub                      | network_subeq                            | network_sup                        | network_supeq                       | nlikejoinsel                   | nlikesel                           | numeric                              |
| numeric_abs                      | numeric_accum                            | numeric_add                        | numeric_avg                         | numeric_avg_accum              | numeric_avg_collect                | numeric_cmp                          |
| numeric_collect                  | numeric_div                              | numeric_div_trunc                  | numeric_eq                          | numeric_exp                    | numeric_fac                        | numeric_ge                           |
| numeric_gt                       | numeric_in                               | numeric_inc                        | numeric_larger                      | numeric_le                     | numeric_ln                         | numeric_log                          |
| numeric_lt                       | numeric_mod                              | numeric_mul                        | numeric_ne                          | numeric_out                    | numeric_power                      | numeric_recv                         |
| numeric_send                     | numeric_smaller                          | numeric_sortsupport                | numeric_sqrt                        | numeric_stddev_pop             | numeric_stddev_samp                | numeric_sub                          |
| numeric_transform                | numeric_uminus                           | numeric_uplus                      | numeric_var_pop                     | numeric_var_samp               | numerictypmodin                    | numerictypmodout                     |
| numrange_subdiff                 | oid                                      | oideq                              | oidge                               | oidgt                          | oidin                              | oidlarger                            |
| oidle                            | oidlt                                    | oidne                              | oidout                              | oidrecv                        | oidsend                            | oidsmaller                           |
| oidvectoreq                      | oidvectorge                              | oidvectorgt                        | oidvectorin                         | oidvectorle                    | oidvectorlt                        | oidvectorne                          |
| oidvectorout                     | oidvectorrecv                            | oidvectorsend                      | oidvectortypes                      | on_pb                          | on_pl                              | on_ppath                             |
| on_ps                            | on_sb                                    | on_sl                              | opaque_in                           | opaque_out                     | ordered_set_transition             | overlaps                             |
| overlay                          | path                                     | path_add                           | path_add_pt                         | path_center                    | path_contain_pt                    | path_distance                        |
| path_div_pt                      | path_in                                  | path_inter                         | path_length                         | path_mul_pt                    | path_n_eq                          | path_n_ge                            |
| path_n_gt                        | path_n_le                                | path_n_lt                          | path_npoints                        | path_out                       | path_recv                          | path_send                            |
| path_sub_pt                      | percentile_cont                          | percentile_cont_float8_final       | percentile_cont_interval_final      | pg_char_to_encoding            | pg_cursor                          | pg_encoding_max_length               |
| pg_encoding_to_char              | pg_extension_config_dump                 | pg_logical_slot_get_binary_changes | pg_logical_slot_peek_binary_changes | pg_node_tree_in                | pg_node_tree_out                   | pg_node_tree_recv                    |
| pg_node_tree_send                | pg_prepared_statement                    | pg_prepared_xact                   | -                                   | -                              | pg_show_all_settings               | pg_stat_get_bgwriter_stat_reset_time |
| pg_stat_get_buf_fsync_backend    | pg_stat_get_checkpoint_sync_time         | pg_stat_get_checkpoint_write_time  | pg_stat_get_db_blk_read_time        | pg_stat_get_db_blk_write_time  | pg_stat_get_db_conflict_all        | pg_stat_get_db_conflict_bufferpin    |
| pg_stat_get_db_conflict_snapshot | pg_stat_get_db_conflict_startup_deadlock | pg_switch_xlog                     | xpath                               | pg_timezone_abbrevs            | pg_timezone_names                  | -                                    |
| plpgsql_call_handler             | plpgsql_inline_handler                   | plpgsql_validator                  | point_above                         | point_add                      | point_below                        | point_distance                       |
| point_div                        | point_eq                                 | point_horiz                        | point_in                            | point_left                     | point_mul                          | point_ne                             |
| point_out                        | point_recv                               | point_right                        | point_send                          | point_sub                      | point_vert                         | poly_above                           |
| poly_below                       | poly_center                              | poly_contain                       | poly_contain_pt                     | poly_contained                 | poly_distance                      | poly_in                              |
| poly_left                        | poly_npoints                             | poly_out                           | poly_overabove                      | poly_overbelow                 | poly_overlap                       | poly_overleft                        |
| poly_overright                   | poly_recv                                | poly_right                         | poly_same                           | poly_send                      | polygon                            | position                             |
| positionjoinsel                  | positionsel                              | postgresql_fdw_validator           | pow                                 | power                          | prsd_end                           | prsd_headline                        |
| prsd_lextype                     | prsd_nexttoken                           | prsd_start                         | pt_contained_circle                 | pt_contained_poly              | query_to_xml                       | query_to_xml_and_xmlschema           |
| query_to_xmlschema               | quote_ident                              | quote_literal                      | quote_nullable                      | radians                        | radius                             | random                               |
| range_adjacent                   | range_after                              | range_before                       | range_cmp                           | range_contained_by             | range_contains                     | range_contains_elem                  |
| range_eq                         | range_ge                                 | range_gist_compress                | range_gist_consistent               | range_gist_decompress          | range_gist_penalty                 | range_gist_picksplit                 |
| range_gist_same                  | range_gist_union                         | range_gt                           | range_in                            | range_intersect                | range_le                           | range_lt                             |
| range_minus                      | range_ne                                 | range_out                          | range_overlaps                      | range_overleft                 | range_overright                    | range_recv                           |
| range_send                       | range_typanalyze                         | range_union                        | rank                                | record_eq                      | record_ge                          | record_gt                            |
| record_in                        | record_le                                | record_lt                          | record_ne                           | record_out                     | record_recv                        | record_send                          |
| regclass                         | regclassin                               | regclassout                        | regclassrecv                        | regclasssend                   | regconfigin                        | regconfigout                         |
| regconfigrecv                    | regconfigsend                            | regdictionaryin                    | regdictionaryout                    | regdictionaryrecv              | regdictionarysend                  | regexeqjoinsel                       |
| regexeqsel                       | regexnejoinsel                           | regexnesel                         | regexp_matches                      | regexp_replace                 | regexp_split_to_array              | regexp_split_to_table                |
| regoperatorin                    | regoperatorout                           | regoperatorrecv                    | regoperatorsend                     | regoperin                      | regoperout                         | regoperrecv                          |
| regopersend                      | regprocedurein                           | regprocedureout                    | regprocedurerecv                    | regproceduresend               | regprocin                          | regprocout                           |
| regprocrecv                      | regprocsend                              | regr_avgx                          | regr_avgy                           | regr_count                     | regr_intercept                     | regr_r2                              |
| regr_slope                       | regr_sxx                                 | regr_sxy                           | regr_syy                            | regtypein                      | regtypeout                         | regtyperecv                          |
| regtypesend                      | reltime                                  | reltimeeq                          | reltimege                           | reltimegt                      | reltimein                          | reltimele                            |
| reltimelt                        | reltimene                                | reltimeout                         | reltimerecv                         | reltimesend                    | repeat                             | replace                              |
| reverse                          | RI_FKey_cascade_del                      | RI_FKey_cascade_upd                | RI_FKey_check_ins                   | RI_FKey_check_upd              | RI_FKey_noaction_del               | RI_FKey_noaction_upd                 |
| RI_FKey_restrict_del             | RI_FKey_restrict_upd                     | RI_FKey_setdefault_del             | RI_FKey_setdefault_upd              | RI_FKey_setnull_del            | RI_FKey_setnull_upd                | right                                |
| round                            | row_number                               | row_to_json                        | rpad                                | rtrim                          | scalargtjoinsel                    | scalargtsel                          |
| scalarltjoinsel                  | scalarltsel                              | schema_to_xml                      | schema_to_xml_and_xmlschema         | schema_to_xmlschema            | session_user                       | set_bit                              |
| set_byte                         | set_config                               | set_masklen                        | shift_jis_2004_to_euc_jis_2004      | shift_jis_2004_to_utf8         | sjis_to_euc_jp                     | sjis_to_mic                          |
| sjis_to_utf8                     | smgrin                                   | smgrout                            | spg_kd_choose                       | spg_kd_config                  | spg_kd_inner_consistent            | spg_kd_picksplit                     |
| spg_quad_choose                  | spg_quad_config                          | spg_quad_inner_consistent          | spg_quad_leaf_consistent            | spg_quad_picksplit             | spg_text_choose                    | spg_text_config                      |
| spg_text_inner_consistent        | spg_text_leaf_consistent                 | spg_text_picksplit                 | spgbeginscan                        | spgbuild                       | spgbuildempty                      | spgbulkdelete                        |
| spgcanreturn                     | spgcostestimate                          | spgendscan                         | spggetbitmap                        | spggettuple                    | spginsert                          | spgmarkpos                           |
| spgoptions                       | spgrescan                                | spgrestrpos                        | spgvacuumcleanup                    | stddev                         | stddev_pop                         | stddev_samp                          |
| string_agg                       | string_agg_finalfn                       | string_agg_transfn                 | strip                               | sum                            | suppress_redundant_updates_trigger | table_to_xml                         |
| table_to_xml_and_xmlschema       | table_to_xmlschema                       | tan                                | text                                | text_ge                        | text_gt                            | text_larger                          |
| text_le                          | text_lt                                  | text_pattern_ge                    | text_pattern_gt                     | text_pattern_le                | text_pattern_lt                    | text_smaller                         |
| textanycat                       | textcat                                  | texteq                             | texticlike                          | texticnlike                    | texticregexeq                      | texticregexne                        |
| textin                           | textlike                                 | textne                             | textnlike                           | textout                        | textrecv                           | textregexeq                          |
| textregexne                      | textsend                                 | thesaurus_init                     | thesaurus_lexize                    | tideq                          | tidge                              | tidgt                                |
| tidin                            | tidlarger                                | tidle                              | tidlt                               | tidne                          | tidout                             | tidrecv                              |
| tidsend                          | tidsmaller                               | time                               | time_cmp                            | time_eq                        | time_ge                            | time_gt                              |
| time_hash                        | time_in                                  | time_larger                        | time_le                             | time_lt                        | time_mi_interval                   | time_mi_time                         |
| time_ne                          | time_out                                 | time_pl_interval                   | time_recv                           | time_send                      | time_smaller                       | time_transform                       |
| timedate_pl                      | timemi                                   | timepl                             | timestamp                           | timestamp_cmp                  | timestamp_cmp_date                 | timestamp_cmp_timestamptz            |
| timestamp_eq                     | timestamp_eq_date                        | timestamp_eq_timestamptz           | timestamp_ge                        | timestamp_ge_date              | timestamp_ge_timestamptz           | timestamp_gt                         |
| timestamp_gt_date                | timestamp_gt_timestamptz                 | timestamp_hash                     | timestamp_in                        | timestamp_larger               | timestamp_le                       | timestamp_le_date                    |
| timestamp_le_timestamptz         | timestamp_lt                             | timestamp_lt_date                  | timestamp_lt_timestamptz            | timestamp_mi                   | timestamp_mi_interval              | timestamp_ne                         |
| timestamp_ne_date                | timestamp_ne_timestamptz                 | timestamp_out                      | timestamp_pl_interval               | timestamp_recv                 | timestamp_send                     | timestamp_smaller                    |
| timestamp_sortsupport            | timestamp_transform                      | timestamptypmodin                  | timestamptypmodout                  | timestamptz                    | timestamptz_cmp                    | timestamptz_cmp_date                 |
| timestamptz_cmp_timestamp        | timestamptz_eq                           | timestamptz_eq_date                | timestamptz_eq_timestamp            | timestamptz_ge                 | timestamptz_ge_date                | timestamptz_ge_timestamp             |
| timestamptz_gt                   | timestamptz_gt_date                      | timestamptz_gt_timestamp           | timestamptz_in                      | timestamptz_larger             | timestamptz_le                     | timestamptz_le_date                  |
| timestamptz_le_timestamp         | timestamptz_lt                           | timestamptz_lt_date                | timestamptz_lt_timestamp            | timestamptz_mi                 | timestamptz_mi_interval            | timestamptz_ne                       |
| timestamptz_ne_date              | timestamptz_ne_timestamp                 | timestamptz_out                    | timestamptz_pl_interval             | timestamptz_recv               | timestamptz_send                   | timestamptz_smaller                  |
| timestamptztypmodin              | timestamptztypmodout                     | timetypmodin                       | timetypmodout                       | timetz                         | timetz_cmp                         | timetz_eq                            |
| timetz_ge                        | timetz_gt                                | timetz_hash                        | timetz_in                           | timetz_larger                  | timetz_le                          | timetz_lt                            |
| timetz_mi_interval               | timetz_ne                                | timetz_out                         | timetz_pl_interval                  | timetz_recv                    | timetz_send                        | timetz_smaller                       |
| timetzdate_pl                    | timetztypmodin                           | timetztypmodout                    | timezone (2069)                     | timezone (1159)                | timezone (2037)                    | timezone (2070)                      |
| timezone (1026)                  | timezone (2038)                          | tintervalct                        | tintervaleq                         | tintervalge                    | tintervalgt                        | tintervalin                          |
| tintervalle                      | tintervalleneq                           | tintervallenge                     | tintervallengt                      | tintervallenle                 | tintervallenlt                     | tintervallenne                       |
| tintervallt                      | tintervalne                              | tintervalout                       | tintervalov                         | tintervalrecv                  | tintervalsame                      | tintervalsend                        |
| tintervalstart                   | to_ascii (1845)                          | to_ascii (1847)                    | to_ascii (1846)                     | trigger_in                     | trigger_out                        | ts_match_qv                          |
| ts_match_tq                      | ts_match_tt                              | ts_match_vq                        | ts_rank                             | ts_rank_cd                     | ts_rewrite                         | ts_stat                              |
| ts_token_type                    | ts_typanalyze                            | tsmatchjoinsel                     | tsmatchsel                          | tsq_mcontained                 | tsq_mcontains                      | tsquery_and                          |
| tsquery_cmp                      | tsquery_eq                               | tsquery_ge                         | tsquery_gt                          | tsquery_le                     | tsquery_lt                         | tsquery_ne                           |
| tsquery_not                      | tsquery_or                               | tsqueryin                          | tsqueryout                          | tsqueryrecv                    | tsquerysend                        | tsrange                              |
| tsrange_subdiff                  | tstzrange                                | tstzrange_subdiff                  | tsvector_cmp                        | tsvector_concat                | tsvector_eq                        | tsvector_ge                          |
| tsvector_gt                      | tsvector_le                              | tsvector_lt                        | tsvector_ne                         | tsvector_update_trigger        | tsvector_update_trigger_column     | tsvectorin                           |
| tsvectorout                      | tsvectorrecv                             | tsvectorsend                       | txid_current                        | txid_current_snapshot          | txid_snapshot_in                   | txid_snapshot_out                    |
| txid_snapshot_recv               | txid_snapshot_send                       | txid_snapshot_xip                  | txid_snapshot_xmax                  | txid_snapshot_xmin             | txid_visible_in_snapshot           | uhc_to_utf8                          |
| unique_key_recheck               | unknownin                                | unknownout                         | unknownrecv                         | unknownsend                    | unnest                             | utf8_to_big5                         |
| utf8_to_euc_cn                   | utf8_to_euc_jis_2004                     | utf8_to_euc_jp                     | utf8_to_euc_kr                      | utf8_to_euc_tw                 | utf8_to_gb18030                    | utf8_to_gbk                          |
| utf8_to_iso8859                  | utf8_to_iso8859_1                        | utf8_to_johab                      | utf8_to_koi8r                       | utf8_to_koi8u                  | utf8_to_shift_jis_2004             | utf8_to_sjis                         |
| utf8_to_uhc                      | utf8_to_win                              | uuid_cmp                           | uuid_eq                             | uuid_ge                        | uuid_gt                            | uuid_hash                            |
| uuid_in                          | uuid_le                                  | uuid_lt                            | uuid_ne                             | uuid_out                       | uuid_recv                          | uuid_send                            |
| var_pop                          | var_samp                                 | varbit                             | varbit_in                           | varbit_out                     | varbit_recv                        | varbit_send                          |
| varbit_transform                 | varbitcmp                                | varbiteq                           | varbitge                            | varbitgt                       | varbitle                           | varbitlt                             |
| varbitne                         | varbittypmodin                           | varbittypmodout                    | varchar                             | varchar_transform              | varcharin                          | varcharout                           |
| varcharrecv                      | varcharsend                              | varchartypmodin                    | varchartypmodout                    | variance                       | void_in                            | void_out                             |
| void_recv                        | void_send                                | win_to_utf8                        | win1250_to_latin2                   | win1250_to_mic                 | win1251_to_iso                     | win1251_to_koi8r                     |
| win1251_to_mic                   | win1251_to_win866                        | win866_to_iso                      | win866_to_koi8r                     | win866_to_mic                  | win866_to_win1251                  | xideq                                |
| xideqint4                        | xidin                                    | xidout                             | xidrecv                             | xidsend                        | xml                                | xml_in                               |
| xml_is_well_formed               | xml_is_well_formed_content               | xml_is_well_formed_document        | xml_out                             | xml_recv                       | xml_send                           | xmlagg                               |
| xmlcomment                       | xmlconcat2                               | xmlexists                          | xmlvalidate                         | pg_notify                      | pg_stat_get_wal_receiver           | -                                    |

The following table lists the functions used by MogDB Kernel to implement internal system functions. You are not advised to use these functions. If you need to use them, contact Enmo technical support.

- smgreq(a smgr, b smgr)

  Description: Compares two smgrs to check whether they are the same.

  Parameters: smgr, smgr

  Return type: Boolean

- smgrne(a smgr, b smgr)

  Description: Checks whether the two smgrs are different.

  Parameters: smgr, smgr

  Return type: Boolean

- xidin4

  Description: Inputs a 4-byte xid.

  Parameter: cstring

  Return type: xid32

- set_hashbucket_info

  Description: Sets hash bucket information.

  Parameter: text

  Return type: Boolean

- hs_concat

  Description: Concatenates two pieces of hstore data.

  Parameters: hstore, hstore

  Return type: hstore

- hs_contained

  Description: Determines whether two hstore data records are included. The return value is of the Boolean type.

  Parameters: hstore, hstore

  Return type: Boolean

- hs_contains

  Description: Determines whether two hstore data records are included. The return value is of the Boolean type.

  Parameters: hstore, hstore

  Return type: Boolean

- hstore

  Description: Converts parameters to the hstore type.

  Parameters: text, text

  Return type: hstore

- hstore_in

  Description: Receives hstore data in string format.

  Parameter: cstring

  Return type: hstore

- hstore_out

  Description: Sends hstore data in string format.

  Parameter: hstore

  Return type: cstring

- hstore_send

  Description: Sends hstore data in bytea format.

  Parameter: hstore

  Return type: bytea

- hstore_to_array

  Description: Sends hstore data in text array format.

  Parameter: hstore

  Return type: text[]

- hstore_to_matrix

  Description: Sends hstore data in text array format.

  Parameter: hstore

  Return type: text[]

- hstore_version_diag

  Description: Sends hstore data in integer array format.

  Parameter: hstore

  Return type: integer

- int1send

  Description: Packs unsigned 1-byte integers into the internal data buffer stream.

  Parameter: tinyint

  Return type: bytea

- isdefined

  Description: Checks whether a specified key exists.

  Parameters: hstore, text

  Return type: Boolean

- listagg

  Description: aggregate function of the list type

  Parameters: smallint, text

  Return type: text

- log_fdw_validator

  Description: validate function

  Parameter: text[], oid

  Return type: void

- nvarchar2typmodin

  Description: Obtains the typmod information of varchar.

  Parameter: cstring[]

  Return type: integer

- nvarchar2typmodout

  Description: Obtains the typmod information of varchar, constructs a character string, and returns the character string.

  Parameter: integer

  Return type: cstring

- read_disable_conn_file

  Description: Reads forbidden connection files.

  Parameter: nan

  Return type: disconn_mode text, disconn_host text, disconn_port text, local_host text, local_port text, redo_finished text

- regex_like_m

  Description: Regular expression match, which is used to determine whether a character string complies with a specified regular expression.

  Parameters: text, text

  Return type: Boolean

- update_pgjob

  Description: Updates a job.

  Parameter: bigint, "char", bigint, timestamp without time zone, timestamp without time zone, timestamp without time zone, timestamp without time zone, timestamp without time zone, smallint

  Return type: void

- enum_cmp

  Description: Enumeration comparison function, which is used to determine whether two enumeration classes are equal and determine their relative sizes.

  Parameter: anyenum, anyenum

  Return type: integer

- enum_eq

  Description: Enumeration comparison function, which is used to implement the = symbol.

  Parameter: anyenum, anyenum

  Return type: Boolean

- enum_first

  Description: Returns the first element in the enumeration class.

  Parameter: anyenum

  Return type: anyenum

- enum_ge

  Description: Enumeration comparison function, which is used to implement the >= symbol.

  Parameter: anyenum, anyenum

  Return type: Boolean

- enum_gt

  Description: Enumeration comparison function, which is used to implement the > sign.

  Parameter: anyenum, anyenum

  Return type: Boolean

- enum_in

  Description: Enumeration comparison function, which is used to determine whether an element is in an enumeration class.

  Parameter: cstring, oid

  Return type: anyenum

- enum_larger

  Description: Enumeration comparison function, which is used to implement the > sign.

  Parameter: anyenum, anyenum

  Return type: anyenum

- enum_last

  Description: Returns the last element in the enumeration class.

  Parameter: anyenum

  Return type: anyenum

- enum_le

  Description: Enumeration comparison function, which is used to implement the <= symbol.

  Parameter: anyenum, anyenum

  Return type: Boolean

- enum_lt

  Description: Enumeration comparison function, which is used to implement the < symbol.

  Parameter: anyenum, anyenum

  Return type: Boolean

- enum_smaller

  Description: Enumeration comparison function, which is used to implement the < symbol.

  Parameter: anyenum, anyenum

  Return type: Boolean

- node_oid_name

  Description: Not supported.

  Parameter: oid

  Return type: cstring

- pg_buffercache_pages

  Description: Reads data from the shared buffer.

  Parameter: nan

  Return type: bufferid integer, relfilenode oid, bucketid smallint, reltablespace oid, reldatabase oid, relforknumber smallint, relblocknumber bigint, isdirty boolean, usage_count smallint

- pg_check_xidlimit

  Description: Checks whether nextxid is greater than or equal to xidwarnlimit.

  Parameter: nan

  Return type: boolean

- pg_comm_delay

  Description: Displays the delay status of the communication library of a single DN.

  Parameter: nan

  Return type: text, text, integer, integer, integer, integer

- pg_comm_recv_stream

  Description: Displays the receiving stream status of all communication libraries on a single DN.

  Parameter: nan

  Return type: text, bigint, text, bigint, integer, integer, integer, text, bigint, integer, integer, integer, bigint, bigint, bigint, bigint, bigint

- pg_comm_send_stream

  Description: Displays the sending stream status of all communication libraries on a single DN.

  Parameter: nan

  Return type: text, bigint, text, bigint, integer, integer, integer, text, bigint, integer, integer, integer, bigint, bigint, bigint, bigint, bigint

- pg_comm_status

  Description: Displays the communication status of a single DN.

  Parameter: nan

  Return type: text, integer, integer, bigint, bigint, bigint, bigint, bigint, integer, integer, integer, integer, integer

- pg_log_comm_status

  Description: Prints some logs on the DN.

  Parameter: nan

  Return type: Boolean

- pg_parse_clog

  Description: Parses clog to obtain the status of xid.

  Parameter: nan

  Return type: xid xid, status text

- pg_pool_ping

  Description: Sets PoolerPing.

  Parameter: Boolean

  Return type: SETOF boolean

- pg_resume_bkp_flag

  Description: Obtains the delay xlong flag for backup and restoration.

  Parameter: slot_name name

  Return type: start_backup_flag boolean, to_delay boolean, ddl_delay_recycle_ptr text, rewind_time text

- psortoptions

  Description: Returns the psort attribute.

  Parameter: text[], boolean

  Return type: bytea

- xideq4

  Description: Compares two values of the xid type to check whether they are the same.

  Parameters: xid32, xid32

  Return type: Boolean

- xideqint8

  Description: Compares values of the xid type and int8 type to check whether they are the same.

  Parameter: xid, bigint

  Return type: Boolean

- xidlt

  Description: Returns whether xid1 < xid2 is true.

  Parameter: xid, xid

  Return type: Boolean

- xidlt4

  Description: Returns whether xid1 < xid2 is true.

  Parameters: xid32, xid32

  Return type: Boolean
