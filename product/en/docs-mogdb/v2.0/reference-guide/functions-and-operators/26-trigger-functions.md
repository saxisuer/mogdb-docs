---
title: Trigger Functions
summary: Trigger Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Trigger Functions

- pg_get_triggerdef(oid)

  Description: Obtains the definition information of a trigger.

  Parameter: OID of the trigger to be queried

  Return type: text

  Example:

  ```sql
  mogdb=# select pg_get_triggerdef(oid) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN ((new.a IS NOT NULL)) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)
  ```

- pg_get_triggerdef(oid, boolean)

  Description: Obtains the definition information of a trigger.

  Parameter: OID of the trigger to be queried and whether it is displayed in pretty mode

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Boolean parameters take effect only when the WHEN condition is specified during trigger creation.

  Return type: text

  Example:

  ```sql
  mogdb=# select pg_get_triggerdef(oid,true) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN (new.a IS NOT NULL) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)

  mogdb=# select pg_get_triggerdef(oid,false) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN ((new.a IS NOT NULL)) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)
  ```
