---
title: _PG_FOREIGN_SERVERS
summary: _PG_FOREIGN_SERVERS
author: Guo Huan
date: 2021-06-23
---

# _PG_FOREIGN_SERVERS

**_PG_FOREIGN_SERVERS** displays information about a foreign server. Only the sysadmin user has the permission to view this view.

**Table 1** _PG_FOREIGN_SERVERS columns

| Name                         | Type                              | Description                                                  |
| ---------------------------- | --------------------------------- | ------------------------------------------------------------ |
| oid                          | oid                               | OID of the foreign server                                    |
| srvoptions                   | text[]                            | Foreign server specific options, expressed in a string in the format of *keyword***=***value* |
| foreign_server_catalog       | information_schema.sql_identifier | Name of the database where the foreign server is located (always the current database) |
| foreign_server_name          | information_schema.sql_identifier | Name of the foreign server                                   |
| foreign_data_wrapper_catalog | information_schema.sql_identifier | Name of the database where the foreign-data wrapper is located (always the current database) |
| foreign_data_wrapper_name    | information_schema.sql_identifier | Name of the foreign-data wrapper                             |
| foreign_server_type          | information_schema.character_data | Type of the foreign server                                   |
| foreign_server_version       | information_schema.character_data | Version of the foreign server                                |
| authorization_identifier     | information_schema.sql_identifier | Role of the owner of the foreign server                      |
