---
title: _PG_FOREIGN_TABLES
summary: _PG_FOREIGN_TABLES
author: Guo Huan
date: 2021-06-23
---

# _PG_FOREIGN_TABLES

**_PG_FOREIGN_TABLES** stores information about all foreign tables defined in the current database, whereas displays information about foreign tables accessible to the current user. Only the sysadmin user has the permission to view this view.

**Table 1** _PG_FOREIGN_TABLES columns

| Name                     | Type                              | Description                                                  |
| ------------------------ | --------------------------------- | ------------------------------------------------------------ |
| foreign_table_catalog    | information_schema.sql_identifier | Name of the database where the foreign table is located (always the current database) |
| foreign_table_schema     | name                              | Name of the schema that the foreign table is in              |
| foreign_table_name       | name                              | Name of the foreign table                                    |
| ftoptions                | text[]                            | Foreign table options                                        |
| foreign_server_catalog   | information_schema.sql_identifier | Name of the database where the foreign server is located (always the current database) |
| foreign_server_name      | information_schema.sql_identifier | Name of the foreign server                                   |
| authorization_identifier | information_schema.sql_identifier | Role of the owner                                            |
