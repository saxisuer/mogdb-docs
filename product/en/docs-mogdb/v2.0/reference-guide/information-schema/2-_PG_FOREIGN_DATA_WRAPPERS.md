---
title: _PG_FOREIGN_DATA_WRAPPERS
summary: _PG_FOREIGN_DATA_WRAPPERS
author: Guo Huan
date: 2021-06-23
---

# _PG_FOREIGN_DATA_WRAPPERS

**_PG_FOREIGN_DATA_WRAPPERS** displays information about a foreign-data wrapper. Only the sysadmin user has the permission to view this view.

**Table 1** _PG_FOREIGN_DATA_WRAPPERS columns

| Name                          | Type                              | Description                                                  |
| ----------------------------- | --------------------------------- | ------------------------------------------------------------ |
| oid                           | oid                               | OID of the foreign-data wrapper                              |
| fdwowner                      | oid                               | OID of the owner of the foreign-data wrapper                 |
| fdwoptions                    | text[]                            | Foreign-data wrapper specific option, expressed in a string in the format of *keyword***=***value* |
| foreign_data_wrapper_catalog  | information_schema.sql_identifier | Name of the database where the foreign-data wrapper is located (always the current database) |
| foreign_data_wrapper_name     | information_schema.sql_identifier | Name of the foreign-data wrapper                             |
| authorization_identifier      | information_schema.sql_identifier | Role of the owner of the foreign-data wrapper                |
| foreign_data_wrapper_language | information_schema.character_data | Programming language of the foreign-data wrapper             |
