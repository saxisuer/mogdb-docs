---
title: JSON Types
summary: JSON Types
author: Guo Huan
date: 2021-04-06
---

# JSON Types

JSON data types are for storing JavaScript Object Notation (JSON) data. Such data can also be stored as TEXT, but the JSON data type has the advantage of checking that each stored value is a valid JSON value.

For functions that support the JSON data type, see JSON Functions.
