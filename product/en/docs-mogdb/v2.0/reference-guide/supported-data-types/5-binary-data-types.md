---
title: Binary Data Types
summary: Binary Data Types
author: Guo Huan
date: 2021-04-06
---

# Binary Data Types

Table 1 lists the binary data types supported by MogDB.

**Table 1** Binary data types

| Name  | Description                                                  | Storage Space                                                |
| :---- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| BLOB  | Binary large object<br/>NOTE:<br/>Column storage cannot be used for the BLOB type. | The maximum size is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes. |
| RAW   | Variable-length hexadecimal string<br/>NOTE:<br/>Column storage cannot be used for the raw type. | 4 bytes plus the actual hexadecimal string. The maximum size is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes. |
| BYTEA | Variable-length binary string                                | 4 bytes plus the actual binary string. The maximum size is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In addition to the size limitation on each column, the total size of each tuple is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes.

An example is provided as follows:

```sql
-- Create a table.
mogdb=# CREATE TABLE blob_type_t1
(
    BT_COL1 INTEGER,
    BT_COL2 BLOB,
    BT_COL3 RAW,
    BT_COL4 BYTEA
) ;

-- Insert data.
mogdb=# INSERT INTO blob_type_t1 VALUES(10,empty_blob(),
HEXTORAW('DEADBEEF'),E'\\xDEADBEEF');

-- Query data in the table.
mogdb=# SELECT * FROM blob_type_t1;
 bt_col1 | bt_col2 | bt_col3  |  bt_col4
---------+---------+----------+------------
      10 |         | DEADBEEF | \xdeadbeef
(1 row)

-- Delete the table.
mogdb=# DROP TABLE blob_type_t1;
```
