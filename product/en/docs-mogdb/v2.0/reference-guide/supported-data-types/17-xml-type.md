---
title: XML Type
summary: XML Type
author: Guo Huan
date: 2021-04-06
---

# XML Type

MogDB supports the XML type. The following is an example:

```sql
mogdb= CREATE TABLE xmltest ( id int, data xml );
mogdb= INSERT INTO xmltest VALUES (1, 'one');
mogdb= INSERT INTO xmltest VALUES (2, 'two');
mogdb= SELECT * FROM xmltest ORDER BY 1;
 id | data
----+--------------------
1 | one
2 | two
(2 rows)
mogdb= SELECT xmlconcat('', NULL, '');
xmlconcat
(1 row)
mogdb= SELECT xmlconcat('', NULL, '');
xmlconcat
(1 row)
```

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**

- This function is disabled by default. To use this function, you need to use the **build.sh** script to compile the database again and add the **-with-libxml** parameter to the **./configure** configuration parameter.
- Before compilation, you need to run the **yum install -y libxml2-devel** command. Otherwise, the "configure: error: library 'xml2' (version >= 2.6.23) is required for XML support" error will be reported.
- Before compilation, you need to add the **dependency/operating system environment/libobs/comm/lib** in the binary file of the third-party library to the system environment variable **LD_LIBRARY_PATH**. Otherwise, an error message is displayed, indicating that **libiconv.so** does not exist.
