---
title: Character Data Types
summary: Character Data Types
author: Guo Huan
date: 2021-04-06
---

# Character Data Types

Table 1 lists the character data types supported by MogDB. For string operators and related built-in functions, see Character Processing Functions and Operators.

**Table 1** Character data types

| Name                                  | Description                                                  | Storage Space                                                |
| :------------------------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| CHAR(n)<br/>CHARACTER(n)<br/>NCHAR(n) | Fixed-length character string, blank padded. **n** indicates the string length. If it is not specified, the default precision **1** is used. | The maximum size is 10 MB.                                   |
| VARCHAR(n)<br/>CHARACTER VARYING(n)   | Variable-length string **n** indicates the string length.    | The maximum size is 10 MB.                                   |
| VARCHAR2(n)                           | Variable-length string Alias of the VARCHAR(n) type. **n** indicates the string length. | The maximum size is 10 MB.                                   |
| NVARCHAR2(n)                          | Variable-length string. **n** indicates the string length.   | The maximum size is 10 MB.                                   |
| CLOB                                  | A big text object. Alias of the TEXT type.                   | The maximum size is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes. |
| TEXT                                  | Variable-length string                                       | The maximum size is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> In addition to the size limitation on each column, the total size of each tuple is 8,203 bytes less than 1 GB, namely, 1,073,733,621 bytes.

In MogDB, there are two other fixed-length character types, as shown in Table 2. The **name** type exists only for the storage of identifiers in the internal system catalogs and is not intended for use by general users. Its length is currently defined as 64 bytes (63 usable characters plus terminator). The type **"char"** only uses one byte of storage. It is internally used in the system catalogs as a simplistic enumeration type.

**Table 2** Special character types

| Name   | Description                    | Storage Space |
| :----- | :----------------------------- | :------------ |
| name   | Internal type for object names | 64 bytes      |
| "char" | Single-byte internal type      | 1 byte        |

## Example

```sql
-- Create a table.
mogdb=# CREATE TABLE char_type_t1
(
    CT_COL1 CHARACTER(4)
);

-- Insert data.
mogdb=# INSERT INTO char_type_t1 VALUES ('ok');

-- Query data in the table.
mogdb=# SELECT ct_col1, char_length(ct_col1) FROM char_type_t1;
 ct_col1 | char_length
---------+-------------
 ok      |           4
(1 row)

-- Delete the table.
mogdb=# DROP TABLE char_type_t1;
```

```sql
-- Create a table.
mogdb=# CREATE TABLE char_type_t2
(
    CT_COL1 VARCHAR(5)
);

-- Insert data.
mogdb=# INSERT INTO char_type_t2 VALUES ('ok');

mogdb=# INSERT INTO char_type_t2 VALUES ('good');

-- Specify the type length. An error is reported if an inserted string exceeds this length.
mogdb=# INSERT INTO char_type_t2 VALUES ('too long');
ERROR:  value too long for type character varying(4)
CONTEXT:  referenced column: ct_col1

-- Specify the type length. A string exceeding this length is truncated.
mogdb=# INSERT INTO char_type_t2 VALUES ('too long'::varchar(5));

-- Query data.
mogdb=# SELECT ct_col1, char_length(ct_col1) FROM char_type_t2;
 ct_col1 | char_length
---------+-------------
 ok      |           2
 good    |           4
 too l   |           5
(3 rows)

-- Delete data.
mogdb=# DROP TABLE char_type_t2;
```
