---
title: INSTANCE_TIME
summary: INSTANCE_TIME
author: Guo Huan
date: 2021-04-19
---

# INSTANCE_TIME

**INSTANCE_TIME** records time consumption information on the current cluster node. The time consumption information is classified into the following types:

- DB_TIME: effective time spent by jobs in multi-core scenarios
- CPU_TIME: CPU time cost.
- EXECUTION_TIME: time spent in the executor.
- PARSE_TIME: time spent on parsing SQL statements
- PLAN_TIME: time spent on generating plans
- REWRITE_TIME: time spent on rewriting SQL statements
- PL_COMPILATION_TIME: compilation time of the PL/pgSQL stored procedure.
- PL_COMPILATION_TIME: compilation time of the PL/pgSQL stored procedure
- NET_SEND_TIME: time spent on the network
- DATA_IO_TIME: time spent on I/Os.

**Table 1** INSTANCE_TIME columns

| Name      | Type    | Description           |
| :-------- | :------ | :-------------------- |
| stat_id   | integer | Statistics ID         |
| stat_name | text    | Type name             |
| value     | bigint  | Time value (unit: μs) |
