---
title: GLOBAL_INSTANCE_TIME
summary: GLOBAL_INSTANCE_TIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_INSTANCE_TIME

**GLOBAL_INSTANCE_TIME** provides time consumption information about all normal nodes in MogDB. For details about the time types, see the **INSTANCE_TIME** view.

**Table 1** GLOBAL_INSTANCE_TIME columns

| **Name**  | **Type** | **Description**     |
| :-------- | :------- | :------------------ |
| node_name | name     | node name           |
| stat_id   | integer  | Statistics ID       |
| stat_name | text     | Type name           |
| value     | bigint   | Duration (unit: μs) |
