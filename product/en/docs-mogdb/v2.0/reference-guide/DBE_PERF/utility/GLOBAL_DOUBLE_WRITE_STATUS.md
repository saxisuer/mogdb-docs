---
title: GLOBAL_DOUBLE_WRITE_STATUS
summary: GLOBAL_DOUBLE_WRITE_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_DOUBLE_WRITE_STATUS

**GLOBAL_DOUBLE_WRITE_STATUS** displays doublewrite file status of all instances in MogDB.

**Table 1** GLOBAL_DOUBLE_WRITE_STATUS columns

| Name                  | Type   | Description                                                  |
| :-------------------- | :----- | :----------------------------------------------------------- |
| node_name             | text   | node name                                                    |
| curr_dwn              | bigint | Sequence number of the doublewrite file                      |
| curr_start_page       | bigint | Start page for restoring the doublewrite file                |
| file_trunc_num        | bigint | Number of times that the doublewrite file is reused          |
| file_reset_num        | bigint | Number of reset times after the doublewrite file is full     |
| total_writes          | bigint | Total number of I/Os of the doublewrite file                 |
| low_threshold_writes  | bigint | Number of I/Os for writing doublewrite files with low efficiency (the number of I/O flushing pages at a time is less than 16) |
| high_threshold_writes | bigint | Number of I/Os for writing doublewrite files with high efficiency (the number of I/O flushing pages at a time is more than 421) |
| total_pages           | bigint | Total number of pages that are flushed to the doublewrite file area |
| low_threshold_pages   | bigint | Number of pages that are flushed with low efficiency         |
| high_threshold_pages  | bigint | Number of pages that are flushed with high efficiency        |
