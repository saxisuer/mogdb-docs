---
title: USER_LOGIN
summary: USER_LOGIN
author: Guo Huan
date: 2021-04-19
---

# USER_LOGIN

**USER_LOGIN** records the number of user logins and logouts.

**Table 1** USER_LOGIN columns

| **Name**       | **Type** | **Description**                                              |
| :------------- | :------- | :----------------------------------------------------------- |
| node_name      | text     | Database process name                                        |
| user_name      | text     | Username                                                     |
| user_id        | integer  | User OID (Its value is the same as that of **oid** in **pg_authid**.) |
| login_counter  | bigint   | Number of logins                                             |
| logout_counter | bigint   | Number of logouts                                            |
