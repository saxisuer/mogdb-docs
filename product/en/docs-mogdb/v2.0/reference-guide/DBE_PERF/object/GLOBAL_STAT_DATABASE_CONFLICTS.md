---
title: GLOBAL_STAT_DATABASE_CONFLICTS
summary: GLOBAL_STAT_DATABASE_CONFLICTS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_DATABASE_CONFLICTS

**GLOBAL_STAT_DATABASE_CONFLICTS** displays statistics about database conflicts on each node.

**Table 1** GLOBAL_STAT_DATABASE_CONFLICTS columns

| Name             | Type   | Description                       |
| :--------------- | :----- | :-------------------------------- |
| node_name        | name   | Node name                         |
| datid            | oid    | Database ID                       |
| datname          | name   | Database name                     |
| confl_tablespace | bigint | Number of conflicting tablespaces |
| confl_lock       | bigint | Number of conflicting locks       |
| confl_snapshot   | bigint | Number of conflicting snapshots   |
| confl_bufferpin  | bigint | Number of conflicting buffers     |
| confl_deadlock   | bigint | Number of conflicting deadlocks   |
