---
title: STAT_USER_FUNCTIONS
summary: STAT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# STAT_USER_FUNCTIONS

**STAT_USER_FUNCTIONS** displays user-defined function status information in the current namespace. (The language of the function is non-internal language.)

**Table 1** STAT_USER_FUNCTIONS columns

| Name       | Type             | Description                                                  |
| :--------- | :--------------- | :----------------------------------------------------------- |
| funcid     | oid              | OID of the function                                          |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Rename the customized function.                              |
| calls      | bigint           | Number of times the function has been called                 |
| total_time | double precision | Total time spent in this function, including other functions called by it (unit: ms) |
| self_time  | double precision | Total time spent in this function, excluding other functions called by it (unit: ms) |
