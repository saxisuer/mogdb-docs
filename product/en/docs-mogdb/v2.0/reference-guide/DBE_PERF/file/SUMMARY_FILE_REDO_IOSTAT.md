---
title: SUMMARY_FILE_REDO_IOSTAT
summary: SUMMARY_FILE_REDO_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_FILE_REDO_IOSTAT

**SUMMARY_FILE_REDO_IOSTAT** records statistics about all redo logs and WALs in MogDB.

**Table 1** SUMMARY_FILE_REDO_IOSTAT columns

| **Name**  | **Type** | **Description**                                              |
| :-------- | :------- | :----------------------------------------------------------- |
| phywrts   | numeric  | Number of times writing into the WAL buffer                  |
| phyblkwrt | numeric  | Number of blocks written into the WAL buffer                 |
| writetim  | numeric  | Duration of writing into XLOG files (unit: μs)               |
| avgiotim  | bigint   | Average duration of writing into XLOG files (unit: μs). **avgiotim** = **writetim**/**phywrts** |
| lstiotim  | bigint   | Duration of the last writing into XLOG files (unit: μs)      |
| miniotim  | bigint   | Minimum duration of writing into XLOG files (unit: μs)       |
| maxiowtm  | bigint   | Maximum duration of writing into XLOG files (unit: μs)       |
