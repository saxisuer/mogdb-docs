---
title: SUMMARY_STATEMENT
summary: SUMMARY_STATEMENT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATEMENT

**SUMMARY_STATEMENT** obtains all information about execution statements (normalized SQL statements) on the primary database node and other database nodes.

**Table 1** SUMMARY_STATEMENT columns

| **Name**            | **Type**                 | **Description**                                              |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| node_name           | name                     | Database process name                                        |
| node_id             | integer                  | ID of a node                                                 |
| user_name           | name                     | Username                                                     |
| user_id             | oid                      | OID of the user                                              |
| unique_sql_id       | bigint                   | ID of the normalized SQL statement                           |
| query               | text                     | Normalized SQL statement                                     |
| n_calls             | bigint                   | Number of calls                                              |
| min_elapse_time     | bigint                   | Minimum execution time of the SQL statement in the kernel (unit: μs) |
| max_elapse_time     | bigint                   | Maximum execution time of the SQL statement in the kernel (unit: μs) |
| total_elapse_time   | bigint                   | Total execution time of the SQL statement in the kernel (unit: μs) |
| n_returned_rows     | bigint                   | Number of rows in the result set returned by the **SELECT** statement |
| n_tuples_fetched    | bigint                   | Number of rows randomly scanned                              |
| n_tuples_returned   | bigint                   | Number of rows sequentially scanned                          |
| n_tuples_inserted   | bigint                   | Number of rows inserted                                      |
| n_tuples_updated    | bigint                   | Number of rows updated                                       |
| n_tuples_deleted    | bigint                   | Number of rows deleted                                       |
| n_blocks_fetched    | bigint                   | Number of buffer block access times                          |
| n_blocks_hit        | bigint                   | Number of buffer block hits                                  |
| n_soft_parse        | bigint                   | Number of soft parsing times                                 |
| n_hard_parse        | bigint                   | Number of hard parsing times                                 |
| db_time             | bigint                   | Valid DB time, which is accumulated if multiple threads are involved (unit: μs) |
| cpu_time            | bigint                   | CPU time (unit: μs)                                          |
| execution_time      | bigint                   | Execution time in the executor (unit: μs)                    |
| parse_time          | bigint                   | SQL parsing time (unit: μs)                                  |
| plan_time           | bigint                   | SQL plan generation time (unit: μs)                          |
| rewrite_time        | bigint                   | SQL rewriting time (unit: μs)                                |
| pl_execution_time   | bigint                   | Execution time of PL/pgSQL (unit: μs)                        |
| pl_compilation_time | bigint                   | Compilation time of PL/pgSQL (unit: μs)                      |
| net_send_time       | bigint                   | Network time (unit: μs)                                      |
| data_io_time        | bigint                   | I/O time (unit: μs)                                          |
| last_updated        | timestamp with time zone | Last time when the statement was updated                     |
| sort_count          | bigint                   | Sorting count                                                |
| sort_time           | bigint                   | Sorting duration (unit: μs)                                  |
| sort_mem_used       | bigint                   | Size of work memory used during sorting (unit: KB)           |
| sort_spill_count    | bigint                   | Count of file writing when data is flushed to disks during sorting |
| sort_spill_size     | bigint                   | File size used when data is flushed to disks during sorting (unit: KB) |
| hash_count          | bigint                   | Hashing count                                                |
| hash_time           | bigint                   | Hashing duration (unit: μs)                                  |
| hash_mem_used       | bigint                   | Size of work memory used during hashing (unit: KB)           |
| hash_spill_count    | bigint                   | Count of file writing when data is flushed to disks during hashing |
| hash_spill_size     | bigint                   | File size used when data is flushed to disks during hashing (unit: KB) |
