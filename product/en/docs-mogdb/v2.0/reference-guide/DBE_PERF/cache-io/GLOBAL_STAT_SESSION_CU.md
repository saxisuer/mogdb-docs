---
title: GLOBAL_STAT_SESSION_CU
summary: GLOBAL_STAT_SESSION_CU
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_SESSION_CU

**GLOBAL_STAT_SESSION_CU** is used to query the CU hit rate of running sessions on each node in MogDB. The data about a session is cleared when you exit this session or restart MogDB.

**Table 1** GLOBAL_STAT_SESSION_CU columns

| **Name**      | **Type** | **Description**                        |
| :------------ | :------- | :------------------------------------- |
| mem_hit       | integer  | Number of memory hits                  |
| hdd_sync_read | integer  | Number of synchronous hard disk reads  |
| hdd_asyn_read | integer  | Number of asynchronous hard disk reads |
