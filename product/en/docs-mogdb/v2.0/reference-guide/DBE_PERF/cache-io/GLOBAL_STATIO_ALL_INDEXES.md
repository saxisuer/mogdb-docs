---
title: GLOBAL_STATIO_ALL_INDEXES
summary: GLOBAL_STATIO_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_ALL_INDEXES

**GLOBAL_STATIO_ALL_INDEXES** contains one row for each index in databases on each node, showing I/O statistics about specific indexes.

**Table 1** GLOBAL_STATIO_ALL_INDEXES columns

| **Name**      | **Type** | **Description**                                 |
| :------------ | :------- | :---------------------------------------------- |
| node_name     | name     | Node name                                       |
| relid         | oid      | OID of the table that the index is created for  |
| indexrelid    | oid      | OID of the index                                |
| schemaname    | name     | Name of the schema that the index is in         |
| relname       | name     | Name of the table that the index is created for |
| indexrelname  | name     | Index name                                      |
| idx_blks_read | numeric  | Number of disk blocks read from the index       |
| idx_blks_hit  | numeric  | Number of cache hits in the index               |
