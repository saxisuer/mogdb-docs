---
title: GLOBAL_STATIO_ALL_TABLES
summary: GLOBAL_STATIO_ALL_TABLES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_ALL_TABLES

**GLOBAL_STATIO_ALL_TABLES** contains I/O statistics about each table (including TOAST tables) in databases on each node.

**Table 1** GLOBAL_STATIO_ALL_TABLES columns

| **Name**        | **Type** | **Description**                                              |
| :-------------- | :------- | :----------------------------------------------------------- |
| node_name       | name     | Node name                                                    |
| relid           | oid      | Table OID                                                    |
| schemaname      | name     | Name of the schema that contains the table                   |
| relname         | name     | Table name                                                   |
| heap_blks_read  | bigint   | Number of disk blocks read from the table                    |
| heap_blks_hit   | bigint   | Number of cache hits in the table                            |
| idx_blks_read   | bigint   | Number of disk blocks read from all indexes in the table     |
| idx_blks_hit    | bigint   | Number of cache hits in the table                            |
| toast_blks_read | bigint   | Number of disk blocks read from the TOAST table (if any) in the table |
| toast_blks_hit  | bigint   | Number of buffer hits in the TOAST table (if any) in the table |
| tidx_blks_read  | bigint   | Number of disk blocks read from the TOAST table index (if any) in the table |
| tidx_blks_hit   | bigint   | Number of buffer-hits in the TOAST table index (if any) in the table |
