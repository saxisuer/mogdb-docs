---
title: SUMMARY_STATIO_ALL_TABLES
summary: SUMMARY_STATIO_ALL_TABLES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_TABLES

**SUMMARY_STATIO_ALL_TABLES** contains I/O statistics about each table (including TOAST tables) in databases in MogDB.

**Table 1** SUMMARY_STATIO_ALL_TABLES columns

| **Name**        | **Type** | **Description**                                              |
| :-------------- | :------- | :----------------------------------------------------------- |
| schemaname      | name     | Name of the schema that contains the table                   |
| relname         | name     | Table name                                                   |
| heap_blks_read  | numeric  | Number of disk blocks read from the table                    |
| heap_blks_hit   | numeric  | Number of cache hits in the table                            |
| idx_blks_read   | numeric  | Number of disk blocks read from all indexes in the table     |
| idx_blks_hit    | numeric  | Number of cache hits in the table                            |
| toast_blks_read | numeric  | Number of disk blocks read from the TOAST table (if any) in the table |
| toast_blks_hit  | numeric  | Number of buffer hits in the TOAST table (if any) in the table |
| tidx_blks_read  | numeric  | Number of disk blocks read from the TOAST table index (if any) in the table |
| tidx_blks_hit   | numeric  | Number of buffer-hits in the TOAST table index (if any) in the table |
