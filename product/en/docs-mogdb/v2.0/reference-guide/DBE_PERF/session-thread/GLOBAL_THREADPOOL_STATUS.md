---
title: GLOBAL_THREADPOOL_STATUS
summary: GLOBAL_THREADPOOL_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_THREADPOOL_STATUS

**GLOBAL_THREADPOOL_STATUS** displays the status of worker threads and sessions in thread pools on all nodes. Columns in this view are the same as those in LOCAL_THREADPOOL_STATUS -> Table 1.
