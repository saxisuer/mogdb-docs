---
title: WORKLOAD_SQL_ELAPSE_TIME
summary: WORKLOAD_SQL_ELAPSE_TIME
author: Guo Huan
date: 2021-04-19
---

# WORKLOAD_SQL_ELAPSE_TIME

**WORKLOAD_SQL_ELAPSE_TIME** collects statistics about SUIDs in workloads.

**Table 1** WORKLOAD_SQL_ELAPSE_TIME columns

| Name                | Type   | Description                                               |
| :------------------ | :----- | :-------------------------------------------------------- |
| workload            | name   | Workload name                                             |
| total_select_elapse | bigint | Total response time of **SELECT** statements (unit: μs)   |
| max_select_elapse   | bigint | Maximum response time of **SELECT** statements (unit: μs) |
| min_select_elapse   | bigint | Minimum response time of **SELECT** statements (unit: μs) |
| avg_select_elapse   | bigint | Average response time of **SELECT** statements (unit: μs) |
| total_update_elapse | bigint | Total response time of **UPDATE** statements (unit: μs)   |
| max_update_elapse   | bigint | Maximum response time of **UPDATE** statements (unit: μs) |
| min_update_elapse   | bigint | Minimum response time of **UPDATE** statements (unit: μs) |
| avg_update_elapse   | bigint | Average response time of **UPDATE** statements (unit: μs) |
| total_insert_elapse | bigint | Total response time of **INSERT** statements (unit: μs)   |
| max_insert_elapse   | bigint | Maximum response time of **INSERT** statements (unit: μs) |
| min_insert_elapse   | bigint | Minimum response time of **INSERT** statements (unit: μs) |
| avg_insert_elapse   | bigint | Average response time of **INSERT** statements (unit: μs) |
| total_delete_elapse | bigint | Total response time of **DELETE** statements (unit: μs)   |
| max_delete_elapse   | bigint | Maximum response time of **DELETE** statements (unit: μs) |
| min_delete_elapse   | bigint | Minimum response time of **DELETE** statements (unit: μs) |
| avg_delete_elapse   | bigint | Average response time of **DELETE** statements (unit: μs) |
