---
title: WORKLOAD_SQL_COUNT
summary: WORKLOAD_SQL_COUNT
author: Guo Huan
date: 2021-04-19
---

# WORKLOAD_SQL_COUNT

**WORKLOAD_SQL_COUNT** displays the distribution of SQL statements in workloads on the current node. Common users can view only the distribution of SQL statements executed by themselves in workloads, whereas Initial user can view the overall load status of workloads.

**Table 1** WORKLOAD_SQL_COUNT columns

| **Name**     | **Type** | **Description**                 |
| :----------- | :------- | :------------------------------ |
| workload     | name     | Workload name                   |
| select_count | bigint   | Number of **SELECT** statements |
| update_count | bigint   | Number of **UPDATE** statements |
| insert_count | bigint   | Number of **INSERT** statements |
| delete_count | bigint   | Number of **DELETE** statements |
| ddl_count    | bigint   | Number of **DDL** statements    |
| dml_count    | bigint   | Number of **DML** statements    |
| dcl_count    | bigint   | Number of **DCL** statements    |
