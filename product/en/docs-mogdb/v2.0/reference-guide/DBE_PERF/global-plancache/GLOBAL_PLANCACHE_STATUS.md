---
title: GLOBAL_PLANCACHE_STATUS
summary: GLOBAL_PLANCACHE_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_PLANCACHE_STATUS

**GLOBAL_PLANCACHE_STATUS** displays the GPC status information.

**Table 1** GLOBAL_PLANCACHE_STATUS columns

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| nodename    | text    | Name of the node that the plan cache belongs to              |
| query       | text    | Text of query statements                                     |
| refcount    | integer | Number of times that the plan cache is referenced            |
| valid       | bool    | Whether the plan cache is valid                              |
| databaseid  | oid     | ID of the database that the plan cache belongs to            |
| schema_name | text    | Schema that the plan cache belongs to                        |
| params_num  | integer | Number of parameters                                         |
| func_id     | oid     | OID of the stored procedure where the plan cache is located. If the plancache does not belong to the stored procedure, the value is **0**. |
