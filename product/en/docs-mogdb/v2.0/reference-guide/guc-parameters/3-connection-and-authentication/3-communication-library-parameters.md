---
title: Communication Library Parameters
summary: Communication Library Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Communication Library Parameters

This section describes parameter settings and value ranges for communication libraries.

## tcp_keepalives_idle

**Parameter description**: Specifies the interval for transmitting keepalive signals on an OS that supports the **TCP_KEEPIDLE** socket option. If no keepalive signal is transmitted, the connection is in idle mode.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPIDLE**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.

**Value range:** 0 to 3600. The unit is s.

**Default value**: **60**

## tcp_keepalives_interval

**Parameter description**: Specifies the response time before retransmission on an OS that supports the **TCP_KEEPINTVL** socket option.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: 0 to 180. The unit is s.

**Default value**: **30**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPINTVL**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.

## tcp_keepalives_count

**Parameter description**: Specifies the number of keepalive signals that can be waited before the MogDB server is disconnected from the client on an OS that supports the **TCP_KEEPCNT** socket option.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the OS does not support **TCP_KEEPCNT**, set this parameter to **0**.
> - The parameter is ignored on an OS where connections are established using the Unix domain socket.

**Value range**: 0 to 100. **0** indicates that the connection is immediately broken if MogDB does not receive a keepalived signal from the client.

**Default value:** **20**

## comm_tcp_mode

**Parameter description**: Specifies whether the communication library uses the TCP or SCTP protocol to set up a data channel. The parameter setting takes effect after you restart MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> SCTP is no longer supported. This parameter is provided for compatibility, but its value is fixed at **on**.

**Value range**: Boolean. If this parameter is set to **on**, TCP is used to connect to database nodes.

**Default value**: **on**

## comm_sctp_port

**Parameter description**: Specifies the TCP or SCTP port used to listen for data packet channels by the TCP proxy communication library or SCTP communication library.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This port number is automatically allocated during MogDB deployment. Do not change the parameter value. If the port number is incorrectly configured, the database communication fails.

**Value range**: an integer ranging from 0 to 65535

**Default value**: **7000**

## comm_control_port

**Parameter description**: Specifies the TCP listening port used by the TCP proxy communication library or SCTP communication library.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This port number is automatically allocated during MogDB deployment. Do not change the parameter value. If the port number is incorrectly configured, the database communication fails.

**Value range**: an integer ranging from 0 to 65535

**Default value**: **7001**

## comm_max_receiver

**Parameter description**: Specifies the maximum number of receiving threads for the TCP proxy communication library or SCTP communication library.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 50

**Default value:** **4**

## comm_quota_size

**Parameter description**: Specifies the maximum size of packets that can be consecutively sent by the TCP proxy communication library or SCTP communication library. When you use a 1GE NIC, a small value ranging from 20 KB to 40 KB is recommended.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 2048000. The default unit is KB.

**Default value**: **1MB**

## comm_usable_memory

**Parameter description**: Specifies the maximum memory available for buffering on the TCP proxy communication library or SCTP communication library on a database node.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This parameter must be set based on environment memory and the deployment method. If it is too large, an out-of-memory (OOM) exception may occur. If it is too small, the performance of the TCP proxy communication library or SCTP communication library may deteriorate.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 100 x 1024 to *INT_MAX*/2. The default unit is KB.

**Default value**: **4000MB**

## comm_memory_pool

**Parameter description**: Specifies the size of the memory pool resources that can be used by the TCP proxy communication library or SCTP communication library on a database node.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If the memory used by the communication library is small, set this parameter to a small value. Otherwise, set it to a large value.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 100 x 1024 to *INT_MAX*/2. The default unit is KB.

**Default value**: **2000MB**

## comm_memory_pool_percent

**Parameter description**: Specifies the percentage of the memory pool resources that can be used by the TCP proxy communication library or SCTP communication library on a database node. This parameter is used to adaptively reserve memory used by the communication library.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If the memory used by the communication library is small, set this parameter to a small value. Otherwise, set it to a large value.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 100

**Default value**: **0**

## comm_no_delay

**Parameter description**: Specifies whether to use the **NO_DELAY** attribute of the communication library connection. Restart MogDB for the setting to take effect.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If packet loss occurs in MogDB because a large number of packets are received per second, set this parameter to **off** so that small packets are combined into large packets for transmission to reduce the total number of packets.

**Default value**: **off**

## comm_debug_mode

**Parameter description**: Specifies whether to enable the debug mode of the TCP proxy communication library or SCTP communication library, that is, whether to print logs about the communication layer.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If this parameter is set to **on**, a huge number of logs will be printed, adding extra overhead and reducing database performance. Therefore, set it to **on** only in debugging scenarios.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the debug logs of the communication library are printed.
- **off** indicates that the debug logs of the communication library are not printed.

**Default value**: **off**

## comm_ackchk_time

**Parameter description**: Specifies the duration after which the communication library server automatically triggers ACK when no data packet is received.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 20000. The unit is ms. **0** indicates that automatic ACK triggering is disabled.

**Default value**: **2000**

## comm_timer_mode

**Parameter description**: Specifies whether to enable the timer mode of the TCP proxy communication library or SCTP communication library, that is, whether to print timer logs in each phase of the communication layer.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If this parameter is set to **on**, a huge number of logs will be printed, adding extra overhead and reducing database performance. Therefore, set it to **on** only in debugging scenarios.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the timer logs of the communication library are printed.
- **off** indicates that the timer logs of the communication library are not printed.

**Default value**: **off**

## comm_stat_mode

**Parameter description**: Specifies whether to enable the statistics mode of the TCP proxy communication library or SCTP communication library, that is, whether to print statistics about the communication layer.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> If this parameter is set to **on**, a huge number of logs will be printed, adding extra overhead and reducing database performance. Therefore, set it to **on** only in debugging scenarios.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the statistics logs of the communication library are printed.
- **off** indicates that the statistics logs of the communication library are not printed.

**Default value**: **off**
