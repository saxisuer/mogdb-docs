---
title: Performance Statistics
summary: Performance Statistics
author: Zhang Cuiping
date: 2021-04-20
---

# Performance Statistics

During the running of the database, the lock access, disk I/O operation, and invalid message processing are involved. All these operations are the bottleneck of the database performance. The performance statistics provided by MogDB can facilitate the performance fault location.

## Generating Performance Statistics Logs

**Parameter description**: For each query, the following four parameters record the performance statistics of corresponding modules in the server log:

- The **og_parser_stats** parameter records the performance statistics of a parser in the server log.
- The **log_planner_stats** parameter records the performance statistics of a query optimizer in the server log.
- The **log_executor_stats** parameter records the performance statistics of an executor in the server log.
- The **log_statement_stats** parameter records the performance statistics of the whole statement in the server log.

All these parameters can only provide assistant analysis for administrators, which are similar to the getrusage() of the Linux OS.

These parameters are SUSET parameters. Set them based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - The **log_statement_stats** records the total statement statistics whereas other parameters record statistics only about their corresponding modules.
> - The **log_statement_stats** parameter cannot be enabled together with any parameter recording statistics about a module.

**Value range**: Boolean

- **on** indicates that performance statistics are recorded.
- **off** indicates that performance statistics are not recorded.

**Default value**: **off**
