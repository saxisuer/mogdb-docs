---
title: Query
summary: Query
author: Zhang Cuiping
date: 2021-04-20
---

# Query

## instr_unique_sql_count

**Parameter description**: Specifies the maximum number of Unique SQL records to be collected. The value **0** indicates that the function of collecting Unique SQL information is disabled.

If the value is changed from a larger one to a smaller one, Unique SQL statistics will be reset and re-collected. There is no impact if the value is changed from a smaller one to a larger one.

When the number of Unique SQL records generated in the system is greater than the value of **instr_unique_sql_count**, the extra Unique SQL records are not collected.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*

**Default value**: **100**

## instr_unique_sql_track_type

**Parameter description**: Specifies which SQL statements are recorded in Unique SQL.

This parameter is an INTERNAL parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated values

- **top**: Only top-level SQL statements are recorded.

**Default value**: **top**

## enable_instr_rt_percentile

**Parameter description**: Specifies whether to enable the function of calculating the response time of 80% and 95% SQL statements in the system.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the function of calculating the response time of 80% and 95% SQL statements is enabled.
- **off** indicates that the function of calculating the response time of 80% and 95% SQL statements is disabled.

**Default value**: **on**

## percentile

**Parameter description**: Specifies the percentage of SQL statements whose response time is to be calculated by the background calculation thread.

This parameter is an INTERNAL parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **80,95**

## instr_rt_percentile_interval

**Parameter description**: Specifies the interval at which the background calculation thread calculates the SQL response time.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 3600. The unit is s.

**Default value**: **10s**

## enable_instr_cpu_timer

**Parameter description**: Specifies whether to capture the CPU time consumed during SQL statement execution.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates the CPU time consumed during SQL statement execution is captured.
- **off** indicates the CPU time consumed during SQL statement execution is not captured.

**Default value**: **on**
