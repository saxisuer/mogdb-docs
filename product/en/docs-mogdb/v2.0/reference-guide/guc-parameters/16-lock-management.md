---
title: Lock Management
summary: Lock Management
author: Zhang Cuiping
date: 2021-04-20
---

# Lock Management

In MogDB, a deadlock may occur when concurrently executed transactions compete for resources. This section describes parameters used for managing transaction locks.

## deadlock_timeout

**Parameter description**: Specifies the time, in milliseconds, to wait on a lock before checking whether there is a deadlock condition. When the applied lock exceeds the preset value, the system will check whether a deadlock occurs.

- The check for deadlock is relatively expensive. Therefore, the server does not check it when waiting for a lock every time. Deadlocks do not frequently occur when the system is running. Therefore, the system just needs to wait on the lock for a while before checking for a deadlock. Increasing this value reduces the time wasted in needless deadlock checks, but slows down reporting of real deadlock errors. On a heavily loaded server, you may need to raise it. The value you have set needs to exceed the transaction time. By doing this, the possibility that a lock will be checked for deadlocks before it is released will be reduced.
- If you want to write the lock wait time during query execution to logs by setting **log_lock_waits**, ensure that the value of **log_lock_waits** is less than the specified value (or the default value) of **deadlock_timeout**.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 2147483647. The unit is ms.

**Default value**: **1s**

## lockwait_timeout

**Parameter description**: Specifies the timeout for attempts to acquire a lock. If the time spent in waiting for a lock exceeds the specified time, an error is reported.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is ms.

**Default value**: **20min**

## update_lockwait_timeout

**Parameter description**: Specifies the maximum duration that a lock waits for concurrent updates on a row to complete when the concurrent update feature is enabled. If the time spent in waiting for a lock exceeds the specified time, an error is reported.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is ms.

**Default value**: **2min**

## max_locks_per_transaction

**Parameter description**: Controls the average number of object locks allocated for each transaction.

- The size of the shared lock table is calculated under the condition that a maximum of *N* independent objects need to be locked at any time. *N* = **max_locks_per_transaction** x (**max_connections** + **max_prepared_transactions**). Objects whose amount does not exceed the preset number can be locked simultaneously at any time. You may need to increase this value if many different tables are modified in a single transaction. This parameter can only be set at database start.
- Increasing the value of this parameter may cause MogDB to request more System V-shared memory than the OS's default configuration allows.
- When running a standby server, you must set this parameter to a value that is no less than that on the primary server. Otherwise, queries will not be allowed on the standby server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 10 to *INT_MAX*

**Default value**: **256**

## max_pred_locks_per_transaction

**Parameter description**: Controls the average number of predicate locks allocated for each transaction.

- The size of the shared predicate lock table is calculated under the condition that a maximum of *N* independent objects need to be locked at any time. *N* = **max_pred_locks_per_transaction** x (**max_connections** + **max_prepared_transactions**). Objects whose amount does not exceed the preset number can be locked simultaneously at any time. You may need to increase this value if many different tables are modified in a single transaction. This parameter can only be set at server start.
- Increasing the value of this parameter may cause MogDB to request more System V-shared memory than the OS's default configuration allows.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 10 to *INT_MAX*

**Default value**: **64**

## partition_lock_upgrade_timeout

**Parameter description**: Specifies the timeout for attempts to upgrade an exclusive lock (read allowed) to an access exclusive lock (read/write blocked) on a partitioned table during the execution of some query statements. If there are concurrent read transactions running, the lock upgrade will need to wait. This parameter sets the waiting timeout for lock upgrade attempts.

- When you do MERGE PARTITION and CLUSTER PARTITION on a partitioned table, temporary tables are used for data rearrangement and file exchange. To concurrently perform as many operations as possible on the partitions, exclusive locks are acquired for the partitions during data rearrangement and access exclusive locks are acquired during file exchange.

- Generally, a partition waits until it acquires a lock, or a timeout occurs if the partition waits for a period longer than the value specified by the **[lockwait_timeout](#lockwait_timeout)** parameter.

- When doing MERGE PARTITION or CLUSTER PARTITION on a partitioned table, an access exclusive lock needs to be acquired during file exchange. If the lock fails to be acquired, the acquisition is retried at an interval of 50 ms until timeout occurs. The **partition_lock_upgrade_timeout** parameter specifies the time to wait before the lock acquisition attempt times out.

- If this parameter is set to **-1**, the lock upgrade never times out. The lock upgrade is continuously retried until it succeeds.

  This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from -1 to 3000. The unit is s.

**Default value**: **1800**

## fault_mon_timeout

**Parameter description**: Specifies the period for detecting lightweight deadlocks. This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1440. The unit is minute.

**Default value**: **5min**

## enable_online_ddl_waitlock

**Parameter description**: Specifies whether to block DDL operations to wait for the release of MogDB locks, such as **pg_advisory_lock** and **pgxc_lock_for_backup**. This parameter is mainly used in online OM operations and you are not advised to modify the settings.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that DDL operations will be blocked to wait for the lock release.
- **off** indicates that DDL operations will not be blocked.

**Default value**: **off**

## xloginsert_locks

**Parameter description**: Specifies the number of locks on concurrent write-ahead logging. This parameter is used to improve the efficiency of writing write-ahead logs.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 1000

**Default value**: **8**
