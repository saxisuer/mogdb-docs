---
title: Operation Auditing
summary: Operation Auditing
author: Zhang Cuiping
date: 2021-04-20
---

# Operation Auditing

## audit_system_object

**Parameter description**: Specifies whether to audit the CREATE, DROP, and ALTER operations on MogDB database objects. MogDB database objects include DATABASE, USER, Schema, and TABLE. The operations on the database object can be audited by changing the value of this parameter.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 524287

- **0** indicates that the function of auditing the CREATE, DROP, and ALTER operations on MogDB database objects is disabled.
- Other values indicate that the CREATE, DROP, and ALTER operations on a certain or some MogDB database objects are audited.

**Value description:**

The value of this parameter is calculated by 19 binary bits. The 19 binary bits represent 19 types of MogDB database objects. If the corresponding binary bit is set to **0**, the CREATE, DROP, and ALTER operations on corresponding database objects are not audited. If it is set to **1**, the CREATE, DROP, and ALTER operations are audited. For details about the audit contents represented by these 19 binary bits, see [Table 1](#audit_system_object).

**Default value**: **12295**

**Table 1** Meaning of each value for the **audit_system_object** parameter<a id="audit_system_object"> </a>

| **Binary Bit** | Description                                                  | Value Description                                            |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Bit 0          | Whether to audit the CREATE, DROP, and ALTER operations on databases. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 1          | Whether to audit the CREATE, DROP, and ALTER operations on schemas. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 2          | Whether to audit the CREATE, DROP, and ALTER operations on users. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 3          | Whether to audit the CREATE, DROP, ALTER, and TRUNCATE operations on tables. | **0** indicates that the CREATE, DROP, ALTER, and TRUNCATE operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, ALTER, and TRUNCATE operations on these objects are audited. |
| Bit 4          | Whether to audit the CREATE, DROP, and ALTER operations on indexes. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 5          | Whether to audit the CREATE and DROP operations on views.    | **0** indicates that the CREATE and DROP operations on these objects are not audited.<br />**1** indicates that the CREATE and DROP operations on these objects are audited. |
| Bit 6          | Whether to audit the CREATE, DROP, and ALTER operations on triggers. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 7          | Whether to audit the CREATE, DROP, and ALTER operations on procedures/functions. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 8          | Whether to audit the CREATE, DROP, and ALTER operations on tablespaces. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 9          | Whether to audit the CREATE, DROP, and ALTER operations on resource pools. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 10         | Whether to audit the CREATE, DROP, and ALTER operations on workloads. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 11         | Whether to audit the CREATE, DROP, and ALTER operations on SERVER FOR HADOOP objects. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 12         | Whether to audit the CREATE, DROP, and ALTER operations on data sources. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 13         | Whether to audit the CREATE and DROP operations on Node Group objects. | **0** indicates that the CREATE and DROP operations on these objects are not audited.<br />**1** indicates that the CREATE and DROP operations on these objects are audited. |
| Bit 14         | Whether to audit the CREATE, DROP, and ALTER operations on ROW LEVEL SECURITY objects. | **0** indicates that the CREATE, DROP, and ALTER operations on these objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on these objects are audited. |
| Bit 15         | Whether to audit the CREATE, DROP, and ALTER operations on types. | **0** indicates that the CREATE, DROP, and ALTER operations on types are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on types are audited. |
| Bit 16         | Whether to audit the CREATE, DROP, and ALTER operations on text search objects (CONFIGURATION and DICTIONARY). | **0** indicates that the CREATE, DROP, and ALTER operations on text search objects are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on text search objects are audited. |
| Bit 17         | Whether to audit the CREATE, DROP, and ALTER operations on directories. | **0** indicates that the CREATE, DROP, and ALTER operations on directories are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on directories are audited. |
| Bit 18         | Whether to audit the CREATE, DROP, and ALTER operations on workloads. | **0** indicates that the CREATE, DROP, and ALTER operations on types are not audited.<br />**1** indicates that the CREATE, DROP, and ALTER operations on types are audited. |
| Bit 19         | Whether to audit the CREATE, DROP, and ALTER operations on sequences. | **0** indicates that the operations are not audited.<br />**1** indicates that the operations are audited. |
| Bit 20         | Whether to audit the CREATE and DROP operations on CMK and CEK objects. | **0** indicates that the CREATE and DROP operations on CMK and CEK objects are not audited.<br />**1** indicates that the CREATE and DROP operations on CMK and CEK objects are audited. |

## audit_dml_state

**Parameter description**: Specifies whether to audit the INSERT, UPDATE, and DELETE operations on a specific table.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing the DML operations (except SELECT) is disabled.
- **1** indicates that the function of auditing the DML operations (except SELECT) is enabled.

**Default value**: **0**

## audit_dml_state_select

**Parameter description**: Specifies whether to audit the SELECT operation.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the SELECT auditing function is disabled.
- **1** indicates that the SELECT auditing function is enabled.

**Default value**: **0**

## audit_function_exec

**Parameter description**: Specifies whether to record the audit information during the execution of the stored procedures, anonymous blocks, or user-defined functions (excluding system functions).

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the function of auditing the procedure or function execution is disabled.
- **1** indicates that the function of auditing the procedure or function execution is enabled.

**Default value**: **0**

## audit_copy_exec

**Parameter description**: Specifies whether to audit the COPY operation.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the COPY auditing function is disabled.
- **1** indicates that the COPY auditing function is enabled.

**Default value**: **0**

## audit_set_parameter

**Parameter description**: Specifies whether to audit the SET operation.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** **0** or **1**

- **0** indicates that the SET auditing function is disabled.
- **1** indicates that the SET auditing function is enabled.

**Default value**: **1**

## enableSeparationOfDuty

**Parameter description**: Specifies whether the separation of three duties is enabled.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the separation of three duties is enabled.
- **off** indicates that the separation of three duties is disabled.

**Default value**: **off**

## enable_nonsysadmin_execute_direct

**Parameter description**: Specifies whether non-system administrators are allowed to execute the EXECUTE DIRECT ON statement.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that any user is allowed to execute the EXECUTE DIRECT ON statement.
- **off** indicates that only the system administrator is allowed to execute the EXECUTE DIRECT ON statement.

**Default value**: **off**

## enable_copy_server_files

**Parameter description**: Specifies whether to enable the permission to copy server files.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the permission to copy server files is enabled.
- **off** indicates that the permission to copy server files is disabled.

**Default value**: **off**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** The copy from/to file function can be used only by users with system administrator permissions or initial users. However, when **enableSeparationOfDuty** is set to **on**, system administrators and initial users have different permissions. You can use **enable_copy_server_file** to control the copy permission of the system administrator. By default, the system administrators are not allowed to copy files. They can perform this operation only after this parameter is set to **on**.

## enable_access_server_directory

**Parameter description**: Specifies whether the system administrator has the permissions to create and delete DIRECTORY objects.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the system administrator has the permission to create and delete DIRECTORY objects.
- **off** indicates that the system administrator does not have the permissions to create and delete DIRECTORY objects.

**Default value**: **off**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - For security purposes, only initial users can create and delete DIRECTORY objects by default.
> - If **enable_access_server_directory** is set to **on**, system administrators (including initial users) can create and delete DIRECTORY objects when **enableSeparationOfDuty** is set to **off**. When **enableSeparationOfDuty** is set to **on**, only the initial users can create and delete DIRECTORY objects.
