---
title: Log Replay
summary: Log Replay
author: Zhang Cuiping
date: 2021-04-20
---

# Log Replay

## recovery_time_target

**Parameter description**: Specifies the time for a standby server to write and replay logs.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 3600. The unit is second.

**0** indicates that log flow control is disabled. A value from **1** to **3600** indicates that a standby server can write and replay logs within the period specified by the value, so that the standby server can quickly assume the primary role. If this parameter is set to a small value, the performance of the primary server is affected. If it is set to a large value, the log flow is not effectively controlled.

**Default value**: **0**

## recovery_max_workers

**Parameter description**: Specifies the maximum number of concurrent replay threads.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 20

**Default value**: **1** (it will be set to 4 by installation tool in default for better performance)

## recovery_parse_workers

**Parameter description**: Specifies the number of **ParseRedoRecord** threads for the extreme Recovery Time Objective (RTO) feature.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 16

This parameter can be set to a value greater than 1 only when the extreme RTO feature is enabled. In addition, it must be used together with **recovery_redo_workers**. If both **recovery_parse_workers** and **recovery_max_workers** are enabled, the setting of **recovery_parse_workers** prevails and the concurrent replay function is disabled. The extreme RTO feature does not support the hot standby mode or primary/standby/secondary mode. Therefore, **recovery_parse_workers** can be set to a value greater than 1 only when **hot_standby** is set to **off** and **replication_type** to **1**. This feature does not support column-store tables, either. Therefore, disable this feature in a system where column-store tables are used or are to be used.

**Default value**: **1**

## recovery_redo_workers

**Parameter description**: Specifies the number of **PageRedoWorker** threads corresponding to each **ParseRedoRecord** thread when the ultimate RTO feature is enabled.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 8

This parameter must be used together with **recovery_parse_workers**, and it takes effect only when **recovery_parse_workers** is set to a value greater than 0.

**Default value**: **1**

## recovery_parallelism

**Parameter description**: Specifies the actual number of replay threads. This parameter is read-only.

**Value range**: an integer ranging from 1 to 2147483647

This parameter is affected by the parameter: recovery_max_workers and parameter: recovery_parse_workers. When any of the above parameters is greater than 0, the parameter value will be recalculated.

**Default value**: **1**

## enable_page_lsn_check

**Parameter description**: Specifies whether to enable the data page LSN check. During replay, the current LSN of the data page is checked to see if it is the expected one.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

**Default value**: **on**

## recovery_min_apply_delay

**Parameter description**: Specifies the delay for backing up the database.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - This parameter is invalid on the primary node. It must be set on the standby node to be delayed.
> - The delay time is calculated based on the timestamp of transaction submission on the primary server and the current time on the standby server. Therefore, ensure that the clocks of the primary and standby servers are the same.
> - Operations without transactions are not delayed.
> - After the primary/standby switchover, if the original primary node needs to be delayed, you need to manually set this parameter.
> - When synchronous_commit is set to remote_apply, synchronous replication is affected by the delay. Each commit message is returned only after the replay on the standby node is complete.
> - Using this feature also delays **hot_standby_feedback**, which may cause the primary server to bloat, so be careful when using both.
> - If a DDL operation (such as DROP or TRUNCATE) that holds an AccessExclusive lock is performed on the primary node, the query operation on the operation object on the standby node will be returned only after the lock is released during the delayed replay of the record on the standby node.
> - The MOT table is not supported.

**Value range**: an integer ranging from 0 to INT_MAX. The unit is ms.

**Default value**: 0 (no delay added)
