---
title: Asynchronous I/O Operations
summary: Asynchronous I/O Operations
author: Zhang Cuiping
date: 2021-04-20
---

# Asynchronous I/O Operations

## enable_adio_debug

**Parameter description**: Specifies whether O&M personnel are allowed to generate some ADIO logs to locate ADIO issues. This parameter is used only by developers. Common users are advised not to use it.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** or **true** indicates that generation of ADIO logs is allowed.
- **off** or **false** indicates that generation of ADIO logs is disallowed.

**Default value**: **off**

## enable_adio_function

**Parameter description**: Specifies whether to enable the ADIO function.

The current version does not support enabling the asynchronous IO function. By default, this function is disabled. Please do not modify it yourself.

**Value range**: Boolean

- **on** or **true** indicates that the function is enabled.
- **off** or **false** indicates that the function is disabled.

**Default value**: **off**

## enable_fast_allocate

**Parameter description**: Specifies whether the quick disk space allocation is enabled.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md). This function can be enabled only in the XFS file system.

**Value range**: Boolean

- **on** or **true** indicates that the function is enabled.
- **off** or **false** indicates that the function is disabled.

**Default value**: **off**

## prefetch_quantity

**Parameter description**: Specifies the amount of the I/O that the row-store prefetches using the ADIO.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 128 to 131072. The unit is 8 KB.

**Default value**: **32MB** (4096 x 8 KB)

## backwrite_quantity

**Parameter description**: Specifies the amount of I/O that the row-store writes using the ADIO.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 128 to 131072. The unit is 8 KB.

**Default value**: **8MB** (1024 x 8 KB)

## cstore_prefetch_quantity

**Parameter description**: Specifies the amount of I/O that the column-store prefetches using the ADIO.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1024 to 1048576. The unit is KB.

**Default value**: **32MB**

## cstore_backwrite_quantity

**Parameter description**: Specifies the amount of I/O that the column-store writes using the ADIO.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1024 to 1048576. The unit is KB.

**Default value**: **8MB**

## cstore_backwrite_max_threshold

**Parameter description**: Specifies the maximum amount of buffer I/O that the column-store writes in the database using the ADIO.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 4096 to *INT_MAX*/2. The unit is KB.

**Default value**: **2GB**

## fast_extend_file_size

**Parameter description**: Specifies the disk size that the row-store pre-scales using the ADIO.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1024 to 1048576. The unit is KB.

**Default value**: **8MB**

## effective_io_concurrency

**Parameter description**: Specifies the number of requests that can be simultaneously processed by a disk subsystem. For the RAID array, the parameter value must be the number of disk drive spindles in the array.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 1000

**Default value**: **1**
