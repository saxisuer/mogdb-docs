---
title: Disk Space
summary: Disk Space
author: Zhang Cuiping
date: 2021-04-20
---

# Disk Space

This section describes the disk space parameters, which are used to set limits on the disk space for storing temporary files.

## sql_use_spacelimit

**Parameter description**: Specifies the space size for files to be flushed to disks when a single SQL statement is executed on a single database node. The managed space includes the space occupied by ordinary tables, temporary tables, and intermediate result sets to be flushed to disks.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from -1 to 2147483647. The unit is KB. **-1** indicates no limit.

**Default value:** **-1**

## temp_file_limit

**Parameter description**: Specifies the limit on the size of a temporary file spilled to disk in a session. The temporary file can be a sort or hash temporary file, or the storage file for a held cursor.

This is a session-level setting.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This parameter does not apply to disk space used for temporary tables during the SQL query process.

**Value range**: an integer ranging from -1 to 2147483647. The unit is KB. **-1** indicates no limit.

**Default value**: **-1**
