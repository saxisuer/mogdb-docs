---
title: GUC Parameter Usage
summary: GUC Parameter Usage
author: Zhang Cuiping
date: 2021-04-20
---

# GUC Parameter Usage

A database provides many operation parameters. Configurations of these parameters affect the behavior of the database system. Before modifying these parameters, learn the impact of these parameters on the database. Otherwise, unexpected results may occur.
**Precautions**

- If the value range of a parameter is a string, the string should comply with the naming conventions of the path and file name in the OS running the target database.

- If the maximum value of a parameter is *INT_MAX*, the maximum parameter value varies by OS.

- If the maximum value of a parameter is *DBL_MAX*, the maximum parameter value varies by OS.
