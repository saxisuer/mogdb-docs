---
title: Fault Tolerance
summary: Fault Tolerance
author: Zhang Cuiping
date: 2021-04-20
---

# Fault Tolerance

This section describes parameters used for controlling the methods that the server processes an error occurring in the database system.

## exit_on_error

**Parameter description**: Specifies whether to terminate the current session.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that any error will terminate the current session.
- **off** indicates that only a FATAL error will terminate the current session.

**Default value**: **off**

## restart_after_crash

**Parameter description**: Specifies whether MogDB automatically reinitializes the backend process when a backend process crashes.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that MogDB automatically reinitializes the backend process when a backend process crashes.

  In some circumstances (for example, when a management tool, such as xCAT, is used to manage MogDB), setting this parameter to **on** maximizes the availability of the database.

- **off** indicates a management tool is enabled to obtain control permission and take proper measures when a backend process crashes.

**Default value**: **on**

## omit_encoding_error

**Parameter description**: If this parameter is set to **on** and the client character set of the database is encoded in UTF-8 format, the occurring character encoding conversion errors will be recorded in logs. Additionally, converted characters that have conversion errors will be ignored and replaced with question marks (?).

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that characters that have conversion errors will be ignored and replaced with question marks (?), and error information will be recorded in logs.
- **off** indicates that characters that have conversion errors cannot be converted and error information will be directly displayed.

**Default value**: **off**

## max_query_retry_times

**Parameter description**: Specifies the maximum number of times that an **SQL** statement with errors can be re-executed. **SQL** statements with the following types of errors can be re-executed:**Connection reset by peer**, **Lock wait timeout**, and **Connection timed out**. If this parameter is set to **0**, the re-execution function is disabled.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 20

**Default value**: **0**

## cn_send_buffer_size

**Parameter description**: Specifies the size of the data buffer used for data transmission on the primary database node.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 8 to 128. The unit is KB.

**Default value**: **8KB**

## max_cn_temp_file_size

**Parameter description**: Specifies the maximum number of temporary files that can be used by the the primary node of the database during automatic SQL statement retries. The value **0** indicates that no temporary file is used.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 10485760. The unit is KB.

**Default value**: **5GB**

## retry_ecode_list

**Parameter description**: Specifies the list of SQL error types that support automatic retry.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: **YY001 YY002 YY003 YY004 YY005 YY006 YY007 YY008 YY009 YY010 YY011 YY012 YY013 YY014 YY015 53200 08006 08000 57P01 XX003 XX009 YY016**

## data_sync_retry

**Parameter description**: Specifies whether to keep running the database when updated data fails to be written into disks by using the **fsync** function. In some OSs, no error is reported even if **fsync** has failed for multiple times. As a result, data is lost.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on**: The database keeps running and **fsync** is executed again after **fsync** fails.
- **off**:**PANIC** is reported and the database is stopped after **fsync** fails.

**Default value**: **off**

## remote_read_mode

**Parameter description**: Specifies whether to enable the remote read function. This function allows pages on the standby server to be read when reading pages on the primary server fails.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated values

- **off** indicates that the remote read function is disabled.
- **non_authentication** indicates that the remote read function is enabled but certificate authentication is not required.
- **authentication** indicates that the remote read function is enabled and certificate authentication is required.

**Default value**: **authentication**
