---
title: Platform and Client Compatibility
summary: Platform and Client Compatibility
author: Zhang Cuiping
date: 2021-04-20
---

# Platform and Client Compatibility

Many platforms use the database system. External compatibility of the database system provides a lot of convenience for platforms.

## convert_string_to_digit

**Parameter description:** Specifies the implicit conversion priority, which determines whether to preferentially convert strings into numbers.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that strings are preferentially converted into numbers.
- **off** indicates that strings are not preferentially converted into numbers.

**Default value**: **on**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** Adjusting this parameter will change the internal data type conversion rule and cause unexpected behavior. Exercise caution when performing this operation.

## nls_timestamp_format

**Parameter description:** Specifies the default timestamp format.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value**: **DD-Mon-YYYY HH:MI:SS.FF AM**

## max_function_args

**Parameter description**: Specifies the maximum number of parameters allowed for a function.

This parameter is a fixed INTERNAL parameter and cannot be modified.

**Value range**: an integer.

**Default value**: **666**

## transform_null_equals

**Parameter description**: Specifies whether expressions of the form expr = NULL (or NULL = expr) are treated as expr IS NULL. They return true if expr evaluates to **NULL**, and false otherwise.

- The correct SQL-standard-compliant behavior of expr = NULL is to always return null (unknown).
- Filtered forms in Microsoft Access generate queries that appear to use expr = NULL to test for null values. If you enable this parameter, you can use this interface to access the database.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates expressions of the form expr = NULL (or NULL = expr) are treated as expr IS NULL.
- **off** indicates expr = NULL always returns NULL.

**Default value**: **off**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> New users are always confused about the semantics of expressions involving **NULL** values. Therefore, **off** is used as the default value.

## support_extended_features

**Parameter description**: Specifies whether extended database features are supported.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that extended database features are supported.
- **off** indicates that extended database features are not supported.

**Default value**: **off**

## sql_compatibility

**Parameter description**: Specifies the type of mainstream database with which the SQL syntax and statement behavior of the database is compatible. This parameter is an INTERNAL parameter. It can be viewed but cannot be modified.

**Value range**: enumerated values

- **A** indicates that the database is compatible with the O database.
- **B** indicates that the database is compatible with the MY database.
- **C** indicates that the database is compatible with the TD database.
- **PG** indicates that the database is compatible with the PostgreSQL database.

**Default value**: **A**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - This parameter can be set only when you run the CREATE DATABASE command to create a database.
> - In the database, this parameter must be set to a specific value. It can be set to **A** or **B** and cannot be changed randomly. Otherwise, the setting is not consistent with the database behavior.

## behavior_compat_options

**Parameter description**: Specifies database compatibility behavior. Multiple items are separated by commas (,).

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string

**Default value:** empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Currently, only compatibility configuration items in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md) are supported.
> - Multiple items are separated by commas (,), for example, **set behavior_compat_options='end_month_calculate,display_leading_zero';**.

**Table 1** Compatibility configuration items

| Configuration Item        | Behavior                                                     |
| :------------------------ | :----------------------------------------------------------- |
| display_leading_zero      | Specifies how floating point numbers are displayed.<br />- If this item is not specified, for a decimal number between -1 and 1, the 0 before the decimal point is not displayed. For example, 0.25 is displayed as **.25**.<br />- If this item is specified, for a decimal number between -1 and 1, the 0 before the decimal point is displayed. For example, 0.25 is displayed as **0.25**. |
| end_month_calculate       | Specifies the calculation logic of the add_months function.<br />Assume that the two parameters of the add_months function are **param1** and **param2**, and that the month of **param1** and **param2** is **result**.<br />- If this item is not specified, and the **Day** of **param1** indicates the last day of a month shorter than **result**, the **Day** in the calculation result will equal that in **param1**. For example:<br />`mogdb=# select add_months('2018-02-28',3) from sys_dummy; add_months ---------------------- 2018-05-28 00:00:00 (1 row)`<br />- If this item is specified, and the **Day** of **param1** indicates the last day of a month shorter than **result**, the **Day** in the calculation result will equal that in **result**. For example:<br />`mogdb=# select add_months('2018-02-28',3) from sys_dummy; add_months ---------------------- 2018-05-31 00:00:00 (1 row)` |
| compat_analyze_sample     | Specifies the sampling behavior of the ANALYZE operation.<br />If this item is specified, the sample collected by the ANALYZE operation will be limited to around 30,000 records, DBnode memory consumption and maintaining the stability of ANALYZE. |
| bind_schema_tablespace    | Binds a schema with the tablespace with the same name.<br />If a tablespace name is the same as *sche_name*, **default_tablespace** will also be set to *sche_name* if **search_path** is set to *sche_name*. |
| bind_procedure_searchpath | Specifies the search path of the database object for which no schema name is specified.<br />If no schema name is specified for a stored procedure, the search is performed in the schema to which the stored procedure belongs.<br />If the stored procedure is not found, the following operations are performed:<br />- If this item is not specified, the system reports an error and exits.<br />- If this item is specified, the search continues based on the settings of **search_path**. If the issue persists, the system reports an error and exits. |
| correct_to_number         | Controls the compatibility of the to_number() result.<br />If this item is specified, the result of the to_number() function is the same as that of PG11. Otherwise, the result is the same as that of the O database. |
| unbind_dive_bound         | Controls the range check on the result of integer division.<br />If this item is specified, you do not need to check the range of the division result. For example, the result of INT_MIN/(-1) can be *INT_MAX*+1. If this item is not specified, an out-of-bounds error is reported because the result is greater than*INT_MAX*. |
| merge_update_multi        | Performs an update if multiple rows are matched for **MERGE INTO**.<br />If this item is specified, no error is reported if multiple rows are matched. Otherwise, an error is reported (same as the O database). |
| return_null_string        | Specifies how to display the empty result (empty string ") of the lpad() and rpad() functions.<br />- If this item is not specified, the empty string is displayed as **NULL**.<br />`mogdb=# select length(lpad('123',0,'*')) from sys_dummy; length ---- (1 row)`<br />- If this item is specified, the empty string is displayed as single quotation marks (").<br />`mogdb=# select length(lpad('123',0,'*')) from sys_dummy; length ---- 0 (1 row)` |
| compat_concat_variadic    | Specifies the compatibility of variadic results of the concat() and concat_ws() functions.<br />If this item is specified and a concat function has a parameter of the variadic type, different result formats in O and Teradata are retained. If this item is not specified and a concat function has a parameter of the variadic type, the result format of O is retained for both O and Teradata. This option has no effect on MY because MY has no variadic type. |
| merge_update_multi        | When MERGE INTO… WHEN MATCHED THEN UPDATE (see **MERGE INTO**) and INSERT… ON DUPLICATE KEY UPDATE (see **INSERT**) are used, control the UPDATE behavior if a piece of target data in the target table conflicts with multiple pieces of source data.<br />If this item is specified and the preceding scenario exists, the system performs multiple UPDATE operations on the conflicting row. If this item is not specified and the preceding scenario exists, an error is reported, that is, the MERGE or INSERT operation fails. |

## td_compatible_truncation

**Parameter description:** Specifies whether to enable features compatible with a Teradata database. You can set this parameter to **on** when connecting to a database compatible with the TD database, so that when you perform the INSERT operation, overlong strings are truncated based on the allowed maximum length before being inserted into char- and varchar-type columns in the target table. This ensures all data is inserted into the target table without errors reported.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> The string truncation function cannot be used if the INSERT statement includes a foreign table.
> If inserting multi-byte character data (such as Chinese characters) to database with the character set byte encoding (SQL_ASCII, LATIN1), and the character data crosses the truncation position, the string is truncated based on its bytes instead of characters. Unexpected result will occur in tail after the truncation. If you want correct truncation result, you are advised to adopt encoding set such as UTF8, which has no character data crossing the truncation position.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates overlong strings are truncated.
- **off** indicates overlong strings are not truncated.

**Default value**: **off**
