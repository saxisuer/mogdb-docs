---
title: Logging Time
summary: Logging Time
author: Zhang Cuiping
date: 2021-04-20
---

# Logging Time

## client_min_messages

**Parameter description**: Specifies which level of messages will be sent to the client. Each level covers all the levels following it. The lower the level is, the fewer messages are sent.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> A same value for **client_min_messages** and **[log_min_messages](#log_min_messages)** does not indicate the same level.

**Value range**: enumerated values. Valid values are **debug**, **debug5**, **debug4**, **debug3**, **debug2**, **debug1**, **info**, **log**, **notice**, **warning**, **error**, **fatal**, and **panic**. For details about the parameter, see **Table 1 Message severity levels**. If the configured level is higher than **error**, for example, **fatal** or **panic**, the system changes the level to **error** by default.

**Default value**: **notice**

## log_min_messages

**Parameter description**: Specifies which level of messages will be written into the server log. Each level covers all the levels following it. The lower the level is, the fewer messages will be written into the log.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> A same value for **[client_min_messages](#client_min_messages)** and **log_min_messages** does not indicate the same level.

**Value range**: enumerated values. Valid values are **debug**, **debug5**, **debug4**, **debug3**, **debug2**, **debug1**, **info**, **log**, **notice**, **warning**, **error**, **fatal**, and **panic**. For details about the parameter, see **Table 1 Message severity levels**.

**Default value**: **warning**

## log_min_error_statement

**Parameter description**: Controls which SQL statements that cause an error condition are recorded in the server log.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: enumerated values. Valid values are **debug**, **debug5**, **debug4**, **debug3**, **debug2**, **debug1**, **info**, **log**, **notice**, **warning**, **error**, **fatal**, and **panic**. For details about the parameter, see **Table 1 Message severity levels**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - The default is **error**, indicating that statements causing errors, log messages, fatal errors, or panics will be logged.
> - **panic** indicates that SQL statements that cause an error condition will not be logged.

**Default value**: **error**

## log_min_duration_statement

**Parameter description**: Specifies the threshold for logging the duration of a completed statement. If a statement runs for a period longer than or equal to the specified value, its duration will be logged.

Setting this parameter can be helpful in tracking down unoptimized queries. For clients using extended query protocols, the time required for parsing, binding, and executing steps are logged independently.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> When using this option together with **log_statement**, the text of statements that are logged because of **log_statement** will not be repeated in the duration log message. If you are not using **syslog**, it is recommended that you log the process ID (PID) or session ID using **log_line_prefix** so that you can link the statement message to the later duration message.

**Value range**: an integer ranging from -1 to *INT_MAX*. The unit is ms.

- If this parameter is set to **250**, all SQL statements that run for 250 ms or longer will be logged.
- **0** indicates that the execution durations of all the statements are logged.
- **-1** indicates that the duration logging is disabled.

**Default value**: **30min**

## backtrace_min_messages

**Parameter description**: Prints the function's stack information to the server's log file if the information generated is greater than or equal to the level specified by this parameter.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> This parameter is used to locate problems on-site. Frequent stack printing will affect the system's overhead and stability. Therefore, set the value of this parameter to a rank other than **fatal** or **panic** during problem location.

**Value range**: enumerated values

Valid values are **debug**, **debug5**, **debug4**, **debug3**, **debug2**, **debug1**, **info**, **log**, **notice**, **warning**, **error**, **fatal**, and **panic**. For details about the parameter, see **Table 1 Message severity levels**.

**Default value**: **panic**

[Table 1](#Message severity levels) explains message severities used by MogDB. If logging output is sent to **syslog** or **eventlog**, the severities are translated as shown in the table. Note that **backtrace_min_messages** takes effect only in a Windows environment and is not supported in MogDB.

**Table 1** Message severity levels<a id="Message severity levels"> </a>

| Severity   | Description                                                  | System Log | Event Log   |
| ---------- | ------------------------------------------------------------ | ---------- | ----------- |
| debug[1-5] | Provides detailed debug information.                         | DEBUG      | INFORMATION |
| log        | Reports information of interest to administrators, for example, checkpoint activity. | INFO       | INFORMATION |
| info       | Provides information implicitly requested by users, for example, output from **VACUUM VERBOSE**. | INFO       | INFORMATION |
| notice     | Provides information that might be helpful to users, for example, notice of truncation of long identifiers and index created as part of the primary key. | NOTICE     | INFORMATION |
| warning    | Provides warnings of likely problems, for example, **COMMIT** outside a transaction block. | NOTICE     | WARNING     |
| error      | Reports an error that causes a command to terminate.         | WARNING    | ERROR       |
| fatal      | Reports the reason that causes a session to terminate.       | ERR        | ERROR       |
| panic      | Reports an error that caused all database sessions to terminate. | CRIT       | ERROR       |

## plog_merge_age

**Parameter description**: Specifies the output period of performance log data.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The parameter value is in milliseconds. You are advised to set it to a multiple of 1000. That is, the value is in seconds. The performance log files controlled by this parameter are stored in the **$GAUSSLOG/gs_profile/**&lt;*node_name*&gt; directory in .prf format. *node_name* is the value of **pgxc_node_name** in the **postgres.conf** file.

**Value range**: an integer ranging from 0 to 2147483647. The unit is ms.

**0** indicates that the current session will not log performance data. A value other than 0 indicates that the current session will log performance data based on the period specified by this parameter.

A small value indicates that much data is logged, which seriously affects performance.

**Default value**: **0**
