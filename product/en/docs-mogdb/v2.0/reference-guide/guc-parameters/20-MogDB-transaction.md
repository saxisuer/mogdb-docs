---
title: MogDB Transaction
summary: MogDB Transaction
author: Zhang Cuiping
date: 2021-04-20
---

# MogDB Transaction

This section describes the settings and value ranges of MogDB transaction parameters.

## transaction_isolation

**Parameter description**: Specifies the isolation level of the current transaction.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string of case-sensitive characters. The values include:

- **serializable**: This value is equivalent to REPEATABLE READ in MogDB.
- **read committed**: Only the data in committed transactions will be read.
- **repeatable read**: Only the data committed before transaction start is read. Uncommitted data or data committed in other concurrent transactions cannot be read.
- **default**: The value is the same as that of **default_transaction_isolation**.

**Default value:** **read committed**

## transaction_read_only

**Parameter description:** Specifies that the current transaction is a read-only transaction.

This parameter is fixed to on during the database recovery process or in the standby machine; otherwise, it is fixed to the value of default_transaction_read_only.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the current transaction is a read-only transaction.
- **off** indicates that the current transaction can be a read/write transaction.

**Default value**: **off**

## xc_maintenance_mode

**Parameter description:** Specifies whether the system is in maintenance mode.

This parameter is a SUSET parameter. Set it based on method 3 in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that concurrent update is allowed.
- **off** indicates that concurrent update is disallowed.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> Enable the maintenance mode with caution to avoid MogDB data inconsistencies.

**Default value**: **off**

## allow_concurrent_tuple_update

**Parameter description**: Specifies whether to allow concurrent update.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that concurrent update is allowed.
- **off** indicates that concurrent update is disallowed.

**Default value**: **on**

## transaction_deferrable

**Parameter description:** specifies whether to delay the execution of a read-only serial transaction without incurring an execution failure. Assume this parameter is set to **on**. When the server detects that the tuples read by a read-only transaction are being modified by other transactions, it delays the execution of the read-only transaction until the other transactions finish modifying the tuples. Currently, this parameter is not used in MogDB. Similar to this parameter, the **default_transaction_deferrable** parameter is used to specify whether to allow delayed execution of a transaction.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the execution of a transaction can be delayed.
- **off** indicates that the execution of a transaction cannot be delayed.

**Default value**: **off**

## enforce_two_phase_commit

**Parameter description**: Specifies whether to forcibly use two-phase commit. This parameter is reserved for compatibility with earlier versions and is invalid in the current version.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that two-phase commit is forcibly used.
- **off** indicates that two-phase commit is not forcibly used.

**Default value**: **on**

## enable_show_any_tuples

**Parameter description:** This parameter is available only in a read-only transaction and is used for analysis. When this parameter is set to **on**/**true**, all versions of tuples in the table are displayed.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on/true** indicates that all versions of tuples in the table are displayed.
- **off/false** indicates that no versions of tuples in the table are displayed.

**Default value**: **off**

## replication_type

**Parameter description**: Specifies what nodes are deployed for the HA mode. The nodes can consist of a primary, a standby, and a secondary node; or consist of a primary and multiple standby nodes; or consist of a primary node.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

You are advised not to modify this parameter.

**Value range**: 0 to 2

- **0** indicates that the HA nodes consist of a primary, a standby, and a secondary node, This mode is not currently supported.
- **1** indicates that the HA nodes consist of a primary and multiple standby nodes. Full scene coverage, this value is recommended.
- **2** indicates that single host mode. This mode cannot expand the standby nodes.

**Default value**: **1**

## pgxc_node_name

**Parameter description**: Specifies the name of a node.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: current node name
