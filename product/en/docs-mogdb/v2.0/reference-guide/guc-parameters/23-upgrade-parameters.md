---
title: Upgrade Parameters
summary: Upgrade Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Upgrade Parameters

## IsInplaceUpgrade

**Parameter description**: Specifies whether an upgrade is ongoing. This parameter cannot be modified by users.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates an upgrade is ongoing.
- **off** indicates no upgrade is ongoing.

**Default value**: **off**

## inplace_upgrade_next_system_object_oids

**Parameter description**: Indicates the OID of a new system object during the in-place upgrade. This parameter cannot be modified by users.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

**Default value**: empty

## upgrade_mode

**Parameter description**: Specifies the upgrade mode. You are advised not to modify this parameter.

**Value range**: an integer ranging from 0 to *INT_MAX*

- **0** indicates that no upgrade is ongoing.
- **1** indicates that a local upgrade is ongoing.
- **2** indicates that a grayscale upgrade is ongoing.

**Default value**: **0**
