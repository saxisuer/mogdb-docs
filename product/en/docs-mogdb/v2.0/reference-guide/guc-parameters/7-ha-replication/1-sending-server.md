---
title: Sending Server
summary: Sending Server
author: Zhang Cuiping
date: 2021-04-20
---

# Sending Server

## max_wal_senders

**Parameter description**: Specifies the maximum number of simultaneously running WAL sender processes. The value cannot be greater than or equal to that of **max_connections**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** **wal_level** must be set to **archive** or **hot_standby** to allow the connection from standby servers.

**Value range**: an integer ranging from 0 to 262143

**Default value:** **16**

## wal_keep_segments

**Parameter description**: Specifies the number of Xlog file segments. Specifies the minimum number of transaction log files stored in the **pg_xlog** directory. The standby server obtains log files from the primary server for streaming replication.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 2 to *INT_MAX*

**Default value**: **16**

**Setting suggestions**:

- During WAL archiving or recovery from a checkpoint on the server, the system may retain more log files than the number specified by **wal_keep_segments**.
- If this parameter is set to an excessively small value, a transaction log may have been overwritten by a new transaction before requested by the standby server. As a result, the request fails and the connection between the primary and standby servers is terminated.
- If the HA system uses asynchronous transmission, increase the value of **wal_keep_segments** when data greater than 4 GB is continuously imported in COPY mode. Take T6000 board as an example. If the data to be imported reaches 50 GB, you are advised to set this parameter to **1000**. You can dynamically restore the setting of this parameter after data import is complete and the WAL synchronization is proper.

## wal_sender_timeout

**Parameter description**: Specifies the maximum duration that the sending server waits for the WAL reception in the receiver.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If the data volume on the primary server is huge, the value of this parameter must be increased for the database rebuilding on a standby server. For example, if the data volume on the primary server reaches 500 GB, you are advised to set this parameter to 600 seconds.
> - This parameter cannot be set to a value larger than the value of **wal_receiver_timeout** or the timeout parameter for database rebuilding.

**Value range**: an integer ranging from 0 to *INT_MAX*. The unit is ms.

**Default value**: **6s**

## max_replication_slots

**Parameter description**: Specifies the number of log replication slots in the primary server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 262143.

**Default value**: **8**

**Setting suggestions**:

When using logical replication, you are advised to set this parameter to: Number of current physical replication slots + Number of required logical replication slots.

- Physical replication slots provide an automatic method to ensure that Xlogs are not removed from a primary node before they are received by all the standby nodes and secondary nodes. That is, physical replication slots are used to support primary/standby HA. The number of physical replication slots required by a database is equal to the ratio of standby and secondary nodes to the primary node. For example, if an HA database has 1 primary node, 1 standby node, and 1 secondary node, the number of required physical replication slots will be 2. If an HA database has 1 primary node and 3 standby nodes, the number of required physical replication slots will be 3.
- Plan the number of logical replication slots as follows:
  - A logical replication slot can carry changes of only one database for decoding. If multiple databases are involved, create multiple logical replication slots.
  - If logical replication is needed by multiple target databases, create multiple logical replication slots in the source database. Each logical replication slot corresponds to one logical replication link.

## enable_slot_log

**Parameter description**: Specifies whether to enable primary/standby synchronization for logical replication slots.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that primary/standby synchronization is enabled for logical replication slots.
- **off** indicates that primary/standby synchronization is disabled for logical replication slots.

**Default value**: **off**

## max_changes_in_memory

**Parameter description**: Specifies the maximum size (bytes) of a single transaction buffered in the memory during logical decoding.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 2147483647

**Default value**: **4096**

## max_cached_tuplebufs

**Parameter description**: Specifies the maximum size (bytes) of the total tuple information buffered in the memory during logic decoding. You are advised to set this parameter to a value greater than or equal to twice of **[max_changes_in_memory](#max_changes_in_memory)**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to 2147483647

**Default value**: **8192**

## replconninfo1

**Parameter description**: Specifies the information about the first node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the first node is configured.

**Default value**: empty

## replconninfo2

**Parameter description**: Specifies the information about the second node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the second node is configured.

**Default value**: empty

## replconninfo3

**Parameter description**: Specifies the information about the third node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the third node is configured.

**Default value**: empty

## replconninfo4

**Parameter description**: Specifies the information about the fourth node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the fourth node is configured.

**Default value**: empty

## replconninfo5

**Parameter description**: Specifies the information about the fifth node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the fifth node is configured.

**Default value**: empty

## replconninfo6

**Parameter description**: Specifies the information about the sixth node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the sixth node is configured.

**Default value**: empty

## replconninfo7

**Parameter description**: Specifies the information about the seventh node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the seventh node is configured.

**Default value**: empty

## replconninfo8

**Parameter description**: Specifies the information about the eighth node to be listened to and authenticated by the current server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the eighth node is configured.

**Default value**: an empty string

## available_zone

**Parameter description**: Specifies the region where the local node is located.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. An empty string indicates that no information about the node is configured.

**Default value**: empty
