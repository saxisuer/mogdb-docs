---
title: Insufficient Memory
summary: Insufficient Memory
author: Guo Huan
date: 2021-05-24
---

# Insufficient Memory

## Symptom

The client or logs contain the error message **memory usage reach the max_dynamic_memory**.

## Cause Analysis

The possible cause is that the value of the GUC parameter **max_process_memory** is too small. This parameter limits the maximum memory that can be used by an MogDB instance.

## Procedure

Use the **gs_guc** tool to adjust the value of **max_process_memory**. Note that you need to restart the instance for the modification to take effect.
