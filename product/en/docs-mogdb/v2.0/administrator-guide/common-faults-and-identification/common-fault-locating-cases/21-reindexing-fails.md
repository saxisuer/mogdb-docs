---
title: Reindexing Fails
summary: Reindexing Fails
author: Guo Huan
date: 2021-05-24
---

# Reindexing Fails

## Symptom

When an index of the desc table is damaged, a series of operations cannot be performed. The error information may be as follows:

```
index \"%s\" contains corrupted page at block
 %u" ,RelationGetRelationName(rel),BufferGetBlockNumber(buf), please reindex it.
```

## Cause Analysis

In actual operations, indexes may break down due to software or hardware faults. For example, if disk space is insufficient or pages are damaged after indexes are split, the indexes may be damaged.

## Procedure

If the table is a column-store table named **pg_cudesc_xxxxx_index**, the desc index table is damaged. Find the OID and table corresponding to the primary table based on the desc index table name, and run the following statement to recreate the cudesc index.

```
REINDEX INTERNAL TABLE name;
```
