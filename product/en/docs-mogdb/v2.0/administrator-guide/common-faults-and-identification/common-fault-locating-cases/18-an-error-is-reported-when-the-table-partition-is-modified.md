---
title: An Error Is Reported When the Table Partition Is Modified
summary: An Error Is Reported When the Table Partition Is Modified
author: Guo Huan
date: 2021-05-24
---

# An Error Is Reported When the Table Partition Is Modified

## Symptom

When **ALTER TABLE PARTITION** is performed, the following error message is displayed:

```
ERROR:start value of partition "XX" NOT EQUAL up-boundary of last partition.
```

## Cause Analysis

If the **ALTER TABLE PARTITION** statement involves both the DROP PARTITION operation and the ADD PARTITION operation, MogDB always performs the DROP PARTITION operation before the ADD PARTITION operation regardless of their orders. However, performing DROP PARTITION before ADD PARTITION causes a partition gap. As a result, an error is reported.

## Procedure

To prevent partition gaps, set **END** in DROP PARTITION to the value of **START** in ADD PARTITION. The following is an example:

```
-- Create a partitioned table partitiontest.
mogdb=#  CREATE TABLE partitiontest
(
c_int integer,
c_time TIMESTAMP WITHOUT TIME ZONE
)
PARTITION BY range (c_int)
(
partition p1 start(100)end(108),
partition p2 start(108)end(120)
);
-- An error is reported when the following statements are used:
mogdb=#  ALTER TABLE partitiontest ADD PARTITION p3 start(120)end(130), DROP PARTITION p2;
ERROR:  start value of partition "p3" NOT EQUAL up-boundary of last partition.
mogdb=#  ALTER TABLE partitiontest DROP PARTITION p2,ADD PARTITION p3 start(120)end(130);
ERROR:  start value of partition "p3" NOT EQUAL up-boundary of last partition.
-- Change them as follows:
mogdb=#  ALTER TABLE partitiontest ADD PARTITION p3 start(108)end(130), DROP PARTITION p2;
mogdb=#  ALTER TABLE partitiontest DROP PARTITION p2,ADD PARTITION p3 start(108)end(130);
```
