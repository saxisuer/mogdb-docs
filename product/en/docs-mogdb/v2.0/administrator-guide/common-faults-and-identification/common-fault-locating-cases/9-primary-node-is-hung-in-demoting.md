---
title: Primary Node Is Hung in Demoting During a Switchover
summary: Primary Node Is Hung in Demoting During a Switchover
author: Guo Huan
date: 2021-05-24
---

# Primary Node Is Hung in Demoting During a Switchover

## Symptom

In a cluster deployed with one primary and multiple standby DNs, if system resources are insufficient and a switchover occurs, a node is hung in demoting.

## Cause Analysis

If system resources are insufficient, the third-party management thread cannot be created. As a result, the managed sub-threads cannot exit and the primary node is hung in demoting.

## Procedure

Run the following command to stop the process of the primary node so that the standby node can be promoted to primary: Perform the following operations only in the preceding scenario.

```
 kill -9 PID
```
