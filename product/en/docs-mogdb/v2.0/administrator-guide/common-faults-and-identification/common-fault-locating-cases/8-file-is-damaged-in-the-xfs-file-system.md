---
title: File Is Damaged in the XFS File System
summary: File Is Damaged in the XFS File System
author: Guo Huan
date: 2021-05-24
---

# File Is Damaged in the XFS File System

## Symptom

When a cluster is in use, error reports such as an input/output error or the structure needs cleaning generally do not occur in the XFS file system.

## Cause Analysis

The XFS file system is abnormal.

## Procedure

Try to mount or unmount the file system to check whether the problem can be solved.

If the problem recurs, refer to the file system document, such as **xfs_repair**, and ask the system administrator to restore the file system. After the file system is repaired, run the **gs_ctl build** command to restore the damaged DNs.
