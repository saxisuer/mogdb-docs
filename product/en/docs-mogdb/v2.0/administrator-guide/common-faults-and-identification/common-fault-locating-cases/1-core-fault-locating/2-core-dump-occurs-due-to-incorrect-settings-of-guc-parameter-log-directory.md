---
title: Core Dump Occurs Due to Incorrect Settings of GUC Parameter log_directory
summary: Core Dump Occurs Due to Incorrect Settings of GUC Parameter log_directory
author: Guo Huan
date: 2021-05-24
---

# Core Dump Occurs Due to Incorrect Settings of GUC Parameter log_directory

## Symptom

After the database process is started, a core dump occurs and no log is recorded.

## Cause Analysis

The directory specified by GUC parameter **log_directory** cannot be read or you do not have permissions to access this directory. As a result, the verification fails during the database startup, and the program exits through the panic log.

## Procedure

Set **log_directory** to a valid directory. For details, see [log_directory](../../../../reference-guide/guc-parameters/10-error-reporting-and-logging/1-logging-destination.md#log_directory).
