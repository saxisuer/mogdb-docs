---
title: Running the INSERT Statement to Insert Data
summary: Running the INSERT Statement to Insert Data
author: Guo Huan
date: 2021-03-04
---

# Running the INSERT Statement to Insert Data

Run the **INSERT** statement to write data into the MogDB database in either of the following ways:

- Use the client tool provided by the MogDB database to write data into MogDB.

  For details, see Inserting Data to Tables.

- Connect to the database using the JDBC or ODBC driver and run the **INSERT** statement to write data into the MogDB database.

  For details, see Connecting to a Database.

You can add, modify, and delete database transactions for the MogDB database. **INSERT** is the simplest way to write data and applies to scenarios with small data volume and low concurrency.
