---
title: Container-based Installation
summary: Container-based Installation
author: Guo Huan
date: 2021-06-01
---

# Container-based Installation

This document introduces the installation of single instance and primary/standby instances in a MogDB docker container. MogDB container does not support the MogHA and OM tools, and currently supports at most one primary node and eight standby nodes.<br/>

In consideration of narrowing the container image size for quick download and deployment, it is recommended that the x86 and ARM-based OSs depended by MogDB running in a docker container will be Ubuntu and Debian since MogDB 2.0.0. <br/>

A MogDB container based on the x86-64 architecture runs in Ubuntu 18.04.<br/>

A MogDB container based on the ARM64 architecture runs in Debian 10.

<br/>

## Single Instance Installation

### Before You Start

The installation of a MogDB container is independent of the host OS. All OSs that can run a container enginer are supported, such as Linux, Windows, and macOS.

Before installing the MogDB container, you need to prepare the container runnning environment, such as Docker.

Docker is an open source application container engine based on Go and compliant with the Apache 2.0 protocol. Docker allows developers to package their applications and dependency packages into a lightweight, portable container that can then be distributed to any popular Linux machine and virtualized.

Docker Desktop download address: <https://www.docker.com/products/docker-desktop>

### Installation Procedures

1. Run Docker.

2. Run one of the following commands according to your system architecture to obtain the latest version of the MogDB image file.

   x86-64:

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```

   ARM64:

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```

   Take the x86-64 machine as an example:

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   2.0.1_amd: Pulling from enmotech/mogdb
   feac53061382: Pull complete
   3d93ed1381ed: Pull complete
   ff71768be531: Pull complete
   f0e5522ade34: Pull complete
   5bcc54da2fc0: Pull complete
   615274e4a1ad: Pull complete
   14de03ed7a5f: Pull complete
   ec2e6c8ece85: Pull complete
   b5884cae3461: Pull complete
   865b32103ae9: Pull complete
   b183a90e4bf8: Pull complete
   Digest: sha256:75f89b9254d8d6f3e7e95f08b473704f2ba159d44e7a3036286ca1ff5eb5421c
   Status: Downloaded newer image for swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```

3. Run the following command to create a running directory for MogDB, using **mogdb** as an example below.

   ```bash
   mkdir /mogdb
   ```

4. Continue with the following command to create a new container, and name it **mogdb** to run a MogDB instance:

   ```bash
   docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123  -v /mogdb:/var/lib/opengauss  -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```

   For Windows OS:
   - If Docker uses Hyper-V as the engine, run the following commands in sequence to create the logical object **mogdata** of the volume in Hyper-V, and then create the container:

      ```bash
      docker volume create mogdata

      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v mogdata:/var/lib/opengauss -p 15453:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
      ```

   - If Docker uses WSL 2 as the engine, run the following command to create the container:

      ```bash
      docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v C:\mogdb:/var/lib/opengauss -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
      ```

   > Note:
   >
   > - MogDB uses port 5432 in the container for listening by default. If you want to access the database from outside the container, you need to specify the `-p` parameter in `docker run` command. For example, the above command will allow access to the container database using port 15432.
   > - Once the container is deleted, all the data and configuration in the container will be lost. After re-running a container from the image, all the data is presented in the initialized state. So for the database container, in order to prevent the loss of data due to the demise of the container or corruption, the operation of persistent storage data is required. This is achieved by specifying the `-v` parameter in `docker run` command. For example, the above command will specify that all data files of MogDB will be stored under **/mogdb** of the host. In addition, If you use Podman, there will be a target path check. Therefore you need to create the target path in advance (step 4).

5. Open the docker terminal:

   ```bash
   docker exec -it mogdb bash
   ```

   Now, the single instance installation in a MogDB container is complete.

### Using MogDB

After the installation is complete and enter the container, switch to omm user by running "su - omm", you can access the database via gsql and use MogDB properly.

```bash
root@384ac97543bd:/# su - omm
omm@384ac97543bd:~$ gsql -d postgres
gsql ((MogDB 2.0.1 build f892ccb7) compiled at 2021-07-09 16:12:59 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

postgres=#
```

<br/>

### Environment Variables

To flexibly use an MogDB image, you can set additional parameters. In the future, more control parameters will be added. The current version supports the setting of the following variables.

**GS_PASSWORD**

This parameter is mandatory when the MogDB image is used. The value cannot be empty or undefined. This parameter specifies the passwords of superuser **omm** and test user **mogdb** of the MogDB database. During the MogDB installation, the superuser **omm** is created by default. This username cannot be changed. The test user **mogdb** is created in  **entrypoint.sh**.

The local trust mechanism is configured for the MogDB image. Therefore, no password is required for connecting to the database in the container. However, if you want to connect to the database from other hosts or containers, you need to enter the password.

The password must contain at least eight characters, including uppercase letters, lowercase letters, digits, and special characters ( \# ? ! @ $ % ^ & * \- ). **!$&**must be escaped using a backslash ().

**GS_NODENAME**

Specifies the database node name. The default value is  **mogdb**.

**GS_USERNAME**

Specifies the username for connecting to the database. The default value is  **mogdb**.

**GS_PORT**

Specifies the database port. The default value is  **5432**.

<br/>

## Primary/Standby Installation

### Before You Begin

To implement primary/standby installation in a MogDB container, please complete the steps in the previous document **Single Instance Installation** first.

### Procedures

1. Switch to user **root**.

   ```bash
   su - root
   ```

2. Create custom networks to create fixed IPs that are easy for containers to use.

   ```bash
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. Create a primary database container.

   ```bash
   docker run --name mogdb_master \
   --network myNetwork --ip 172.18.0.10 --privileged=true \
   --hostname mogdb_master --detach \
   --env GS_PORT=6432 \
   --env OG_SUBNET=172.18.0.0/16 \
   --env GS_PASSWORD=Enmo@1234 \
   --env NODE_NAME=mogdb_master \
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" \
   swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3 -M primary
   ```

   ![docker-installation-1](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-1.png)

4. Create a standby database container.

   ```bash
    docker run --name mogdb_slave_one \
    --network myNetwork --ip 172.18.0.11 --privileged=true \
    --hostname mogdb_slave_one --detach \
    --env GS_PORT=6432 \
    --env OG_SUBNET=172.18.0.0/16 \
    --env GS_PASSWORD=Enmotech@1234 \
    --env NODE_NAME=mogdb_slave_one \
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" \
    swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3 -M standby
   ```

    ![docker-installation-2](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-2.png)

5. Query the status of the primary database.

   ```bash
   docker exec -it mogdb_master bash
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-3](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-3.png)

6. Query the status of the standby database.

   ```bash
   docker exec -it mogdb_slave_one bash
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-4](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-4.png)

   > Note: As shown in the figures above, the senders info and the receiver info indicate that the status of the primary and standby databases is normal.

### Read/Write Testing

Primary database write test:

```bash
gsql -p6432
create table test(ID int);
insert into test values(1);
```

![docker-installation-5](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-5.png)

Standby database read test:

```bash
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-6](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-6.png)

> Note: The result shows that the primary database is writable and the standby database is readable but not writable.

### Switchover Testing

Switch **mogdb_slave_one** as the primary database and **mogdb_master** as the standby database.

Run the switchover command in **mogdb_slave_one**.

```bash
gs_ctl switchover -D /var/lib/opengauss/data/
```

![docker-installation-7](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-7.png)

Query the status of **mogdb_slave_one**.

```bash
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-8](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-8.png)

Query the status of **mogdb_master**.

```bash
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-9](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-9.png)

You can find that **mogdb_master** becomes the standby database and **mogdb_slave_one** becomes the primary database. The switchover is successful.

### Read/Write Verification

Perform write validation on the primary database **mogdb_slave_one**.

```bash
gsql -p6432
select * from test;
insert into test values(2);
```

![docker-installation-10](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-10.png)

Perform read validation on the standby database **mogdb_master**.

```bash
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-11](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-11.png)

As you can see, the original standby database is writable after being switched as the primary database, and the original primary database is readable but not writable after being switched as the standby database.

<br/>

## What's Next

MogDB container does not support MogHA and OM tools. It is only used for testing, and currently supports at most one primary node and eight standby nodes. MogDB Enterprise Edition includes MogHA component. The basic functions of the MogDB container and Enterprise Edition are identical. It is recommended that MogDB Enterprise Edition is used in the production environment.
