---
title: Environment Requirement
summary: Environment Requirement
author: Zhang Cuiping
date: 2021-04-2
---

# Environment Requirement

## Hardware Requirements

The following table describes the minimum hardware requirements of MogDB. When planning the hardware configuration of a product, consider the data scale and expected database response speed. Plan hardware as required.

| Item                 | Description                                                  |
| -------------------- | ------------------------------------------------------------ |
| Minimum memory       | Minimum 32 GB of memory is required for function debugging. <br />In performance tests and commercial deployment, the single-instance deployment is performed. It is recommended that the memory be greater than 128 GB.  <br />Complex queries require much memory but the memory may be insufficient in high-concurrency scenarios. In this case, it is recommended that a large-memory server or load management be used to restrict concurrences on the system. |
| CPU                  | Minimum one 8-core 2.0 GHz CPU is required for function debugging.  <br />In performance tests and commercial deployment, the single-instance deployment is performed. It is recommended that one 16-core 2.0 GHz CPU be used.  <br />You can set CPUs to hyper-threading or non-hyper-threading mode, but ensure the setting is consistent across all the MogDB nodes. |
| Hard disk            | Hard disks used for installing the MogDB must meet the following requirements: <br />-  At least 1 GB is used to install the MogDB application. <br />- About 300 MB is used for each host to store metadata.  <br />- More than 70% of available disk space is reserved to store data.  <br />You are advised to configure the system disk to RAID 1 and data disk to RAID 5 and plan four groups of RAID 5 data disks for installing MogDB. RAID configuration is not described in this document. You can configure RAID by following instructions in the hardware vendors' manuals or using common methods found on the Internet. Set **Disk Cache Policy** to **Disabled** to avoid data loss in an unexpected power-off. <br />MogDB supports using an SSD with the SAS interface and NVMe protocol deployed in RAID mode as the primary storage device of the database. |
| Network requirements | Minimum 300 Mbit/s Ethernet is required.  <br />You are advised to bond two NICs for redundancy. The configuration is not described in this document. You can configure it by following instructions in the manual provided by the hardware manufacturer or using the methods provided on the Internet.<br />If bonds are configured for the MogDB, ensure the consistency among these bond modes, because inconsistent bond modes may cause MogDB running exceptions. |

## Software Requirements

| Software          | Description                                                  |
| ----------------- | ------------------------------------------------------------ |
| Linux OS          | - Arm:<br />openEuler 20.3LTS / CentOS 7 / Redhat Linux 7 / Kylin OS V10 (Currently not compatible with CentOS 8/Redhat Linux 8 series)<br />-x86:<br />openEuler 20.3LTS<br />CentOS 7.6 |
| rdtscp Instruction Set (x86)  | Run the `lscpu | grep rdtscp` command to see if the rdtscp instruction set is supported.            |
| Tool              | Huawei JDK 1.8.0, psmisc, and bzip2                          |
| Python            | - openEuler: Python 3.7.X is supported.<br />- CentOS: Python 3.6.X is supported.<br />- Kirin: Python 3.7.X is supported.<br />NOTE:<br />Python needs to be compiled in -enable-shared mode. |

## Software Dependency Requirements

You are advised to use the default installation packages of the following dependent software in the listed OS installation CD-ROMs or sources. If the following software does not exist, refer to the recommended versions of the software.

| Software       | Recommended Version |
| :------------- | :------------------ |
| libaio-devel   | 0.3.109-13          |
| flex           | 2.5.31 or later     |
| bison          | 2.7-4               |
| ncurses-devel  | 5.9-13.20130511     |
| glibc-devel    | 2.17-111            |
| patch          | 2.7.1-10            |
| lsb_release    | 4.1                 |
| readline_devel | 7.0-13              |
| openSSH        | 8.4p1               |

Note: Installation is supported even if there is only 4 GB memory in actual test. If the memory size is greater than 4 GB, it does not need to be modified.
