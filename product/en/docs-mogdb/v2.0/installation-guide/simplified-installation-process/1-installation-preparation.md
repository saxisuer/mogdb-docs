---
title: Preparing for Installation
summary: Preparing for Installation
author: Zhang Cuiping
date: 2021-06-11
---

# Preparing for Installation

This section describes how to prepare and configure an environment for simplified MogDB installation. The simplified installation can be performed on a single node or on primary/standby nodes. Read this section carefully before installation. If the configuration in this section has been completed, skip this section.

## Obtaining the Installation Package

You can obtain the installation package from the MogDB website.

MogDB website: <https://www.mogdb.io/en/downloads/mogdb/>

**Procedure**

This section takes the openEuler OS as an example to describe how to obtain the target installation package.

1. Upload the downloaded standard installation package and decompress it to obtain the single-node installation package.

   ```bash
   tar -xvf MogDB-2.0.3-openEuler-arm64.tar
   ```

   > Note:
   >
   > + The MogDB website provides the standard installation package **MogDB-2.0.3-openEuler-arm64.tar**. You need to decompress it to obtain the single-node installation package **MogDB-2.0.3-openEuler-64bit.tar.bz2**.
   > + In environments that allow access to external networks, the standard installation package can be downloaded directly via the wget command.
   >
   > ```bash
   > wget -c https://cdn-mogdb.enmotech.com/mogdb-media/2.0.3/MogDB-2.0.3-openEuler-arm64.tar
   > ```
   >
   > Then you can run the **tar** command to decompress it.

2. Check the installation package.

   Decompress the installation package and check whether the installation directory and files are complete. Run the following commands in the directory where the installation package is stored:

   ```bash
   tar -jxf MogDB-2.0.3-openEuler-64bit.tar.bz2
   ls -1b
   ```

   Run the **ls** command to display information similar to the following:

   ```bash
   total 90296
   drwx------ 3 root root     4096 Mar 31 21:18 bin
   drwx------ 3 root root     4096 Mar 31 21:18 etc
   drwx------ 3 root root     4096 Mar 31 21:18 include
   drwx------ 4 root root     4096 Mar 31 21:18 jre
   drwx------ 5 root root     4096 Mar 31 21:18 lib
   -rw------- 1 root root 92427499 Apr  1 09:43 MogDB-2.0.3-openEuler-64bit.tar.bz2
   drwx------ 5 root root     4096 Mar 31 21:18 share
   drwx------ 2 root root     4096 Mar 31 21:18 simpleInstall
   -rw------- 1 root root       32 Mar 31 21:18 version.cfg
   ```

## Preparing the Software and Hardware Installation Environment

This section describes the preparations for installation.

### Software and Hardware Requirements

This section describes hardware and software requirements of MogDB. It is recommended that servers to be deployed on MogDB have the same software and hardware configurations.

**Hardware Requirements**

[Table 1](#table1) lists the minimum hardware requirements of the MogDB server. When planning the hardware configuration of a product, consider the data scale and expected database response speed. Plan hardware as required.

**Table 1** Hardware requirements <a id="table1"> </a>

| Item    | Configuration Description                                    |
| :------ | :----------------------------------------------------------- |
| Memory  | It is recommended that the memory be 32 GB or above for function debugging.<br />In performance tests and commercial deployment, it is recommended that the memory be 128 GB or above for single-instance deployment.<br />Complex queries require much more memory, and therefore, the memory may be insufficient in high-concurrency scenarios. In this case, it is recommended that a large-memory server or load management be used to restrict concurrency on the system. |
| CPU     | It is recommended at least one 8-core 2.0 GHz CPU be used for function debugging.<br />In performance tests and commercial deployment, it is recommended that one 16-core 2.0 GHz CPU be used.<br />You can set CPUs to hyper-threading or non-hyper-threading mode.<br />NOTE:<br />For individual developers, the minimum configuration is 2 cores and 4 GB memory, and the recommended configuration is 4 cores and 8 GB memory.<br />Currently, MogDB supports Kunpeng 920 servers, Hygon 3000, 5000, 7000 series servers, ZHAOXIN KH30000、KH20000、ZX-C+ series servers and x86_64-based universal PC servers. |
| Disk    | Hard disks used for installing MogDB must meet the following requirements:<br />- At least 1 GB is used to install the MogDB applications.<br />- About 300 MB is used for each host to store metadata.<br />- More than 70% of available disk space is reserved to store data.<br />You are advised to configure the system disk to RAID 1 and data disk to RAID 5 and plan four groups of RAID 5 data disks for installing MogDB. For details about RAID configuration, see the instructions in the hardware vendors' manuals. Set **Disk Cache Policy** to **Disabled** to avoid data loss upon unexpected power-off.<br />MogDB supports using an SSD with the SAS interface and NVMe protocol deployed in RAID mode as the primary storage device of the database. |
| Network | Minimum 300 Mbit/s Ethernet is required.<br />You are advised to bond two NICs for redundancy. For details about NIC redundancy bond configuration, see the instructions in the hardware vendors' manuals. |

**Software Requirements**

**Table 2** Software requirements

| Software          | Configuration Description                                    |
| :---------------- | :----------------------------------------------------------- |
| Linux OS          | ARM:<br />- openEuler 20.3LTS (recommended)<br />- Kirin V10<br />x86:<br />openEuler 20.3LTS<br />- CentOS 7.6 |
| rdtscp Instruction Set (x86)  | Run the `lscpu | grep rdtscp` command to see if the rdtscp instruction set is supported.            |
| Tool              | bzip2                                                        |

**Software Dependency Requirements**

[Table 3](#table3) describes the MogDB software dependency requirements.

You are advised to use the default installation packages of the following dependent software in the listed OS installation CD-ROMs or sources. If the following software does not exist, refer to the recommended versions of the software.

**Table 3** Software dependency requirements <a id="table3"></a>

| Software                                    | Recommended Version |
| :------------------------------------------ | :------------------ |
| libaio-devel                                | 0.3.109-13          |
| flex                                        | 2.5.31 or later     |
| bison                                       | 2.7-4               |
| ncurses-devel                               | 5.9-13.20130511     |
| glibc-devel                                 | 2.17-111            |
| patch                                       | 2.7.1-10            |
| redhat-lsb-core                             | 4.1                 |
| readline-devel                              | 7.0-13              |
| libnsl (in the openEuler + x86 environment) | 2.28-36             |
| openSSH                                     | 8.4p1               |

### Modifying OS Configuration

**Disabling the OS Firewall**

To ensure that MogDB can work properly when the firewall is enabled, MogDB-related services, protocols, IP addresses, and ports need to be added to the firewall whitelist of the MogDB host.

Take openEuler OS as an example. Assume that the MogDB information is listed in [Table 4](#mogdbinfo).

**Table 4** MogDB information <a id="mogdbinfo"></a>

| Host Name          | Internal IP Address | External IP Address |
| :----------------- | :------------------ | :------------------ |
| plat1              | 192.168.0.11        | 10.10.0.11          |
| Management network | -                   | 10.10.64.236        |

Currently, the installation can be performed only when the firewall is disabled.

1. Modify the SELinux operating mode to temporarily disable the firewall.<a id="config file"></a>

    ```bash
    setenforce 0
    ```

2. Set the value of **SELINUX** in the **/etc/selinux/config** file to **disabled**.

   a. Run the **vim** command to open the **config** file.

   ```bash
   vim /etc/selinux/config
   ```

   b. Change the value of **SELINUX** to **disabled** and run the **:wq** command to save the change and exit.

   ```bash
   SELINUX=disabled
   ```

3. (Optional)Restart the OS.

   ```bash
   reboot
   ```

   Perform the following operations to permanently disable the firewall.

4. Check whether the firewall is disabled.

   ```bash
   systemctl status firewalld
   ```

   If the firewall status is **active (running)**, the firewall is not disabled. Go to step [5](#disablethefirewall).

   If the firewall status is **inactive (dead)**, you do not need to disable the firewall.

5. Disable the firewall service. <a id="disablethefirewall"></a>

   ```bash
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   ```

6. Repeat steps [1](#config file) to [5](#disablethefirewall) on other hosts.

**Setting Character Set Parameters**

Set the same character set for all database nodes. You can add **export LANG=Unicode** to the **/etc/profile** file.

```bash
vim /etc/profile
```

**Setting the Time Zone and Time**

Ensure that the time zone and time on each database node are consistent.

1. Run the following command to check whether the time and time zone of each database node are consistent: If the time and time zone of each database node are inconsistent, perform steps [2](#2) to [3](#3).

   ```bash
   date
   ```

2. Run the following command to copy the **/etc/localtime** file to the **/usr/share/zoneinfo/** directory of each database node: <a id="2"> </a>

   ```bash
   cp /usr/share/zoneinfo/$Locale/$Time zone /etc/localtime
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)  **NOTE:** *$Locale/$Time zone* indicates the locale and time zone to be set, for example, **Asia/Shanghai**.

3. Run the **date -s** command to set the time of each database node to the same time. For example: <a id="3"></a>

   ```bash
   date -s "Sat Sep 27 16:00:07 CST 2020"
   ```

**(Optional) Disabling the Swap Memory**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)  **NOTE:** Disabling the swap memory ensures the access performance of the database and prevents the buffer memory of the database from being evicted to the disk. If the server memory is small and the memory is overloaded, you can enable the swap memory to ensure normal running.

Run the **swapoff -a** command on each database node to disable the swap memory.

```bash
swapoff -a
```

**Disabling RemoveIPC**

On each database node, disable RemoveIPC. For CentOS, skip this step.

1. Change the value of **RemoveIPC** in the **/etc/systemd/logind.conf** file to **no**. <a id="1"></a>.

   a. Run the **vim** command to open the **logind.conf** file.

   ```bash
   vim  /etc/systemd/logind.conf
   ```

   b. Change the value of **RemoveIPC** to **no**.

   ```bash
   RemoveIPC=no
   ```

2. Change the value of **RemoveIPC** in the **/usr/lib/systemd/system/systemd-logind.service** file to **no**.

   a. Run the **vim** command to open the **systemd-logind.service** file.

   ```bash
   vim /usr/lib/systemd/system/systemd-logind.service
   ```

   b. Change the value of **RemoveIPC** to **no**.

   ```bash
   RemoveIPC=no
   ```

3. Reload configuration parameters.

   ```bash
   systemctl daemon-reload
   systemctl restart systemd-logind
   ```

4. Check whether the modification takes effect.

   ```bash
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

5. Open the **limits.conf** and **sysctl.conf** configuration files and add the following content to the corresponding files, respectively.<a id="5"> </a>

   ```bash
   vim /etc/security/limits.conf
   #Add the following content to the limits.conf configuration file：
   omm            soft    nofile          1000000
   omm            hard    nofile          1000000
   vim /etc/sysctl.conf
   #Add the following content to the sysctl.conf configuration file：
   kernel.sem = 250 32000 100 999
   ```

6. Repeat steps [1](#1) to [5](#5) on other hosts.
