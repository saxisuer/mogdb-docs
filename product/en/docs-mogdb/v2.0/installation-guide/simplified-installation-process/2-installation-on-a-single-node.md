---
title: Installation on a Single Node
summary: Installation on a Single Node
author: Zhang Cuiping
date: 2021-06-11
---

# Installation on a Single Node

## Prerequisites

- A user group and a common user have been created.
- All the server OSs and networks are functioning properly.
- A common user must have the read, write, and execute permissions on the database package decompression path and installation path, and the installation path must be empty.
- A common user has the execution permission on the downloaded MogDB package.
- Before the installation, check whether the specified MogDB port is occupied. If the port number is occupied, change the port or stop the process that uses the port.

## Procedure

1. Log in to the host where the MogDB package is installed as a common user and decompress the MogDB package to the installation directory.

   ```
   tar -jxf MogDB-2.0.3-openEuler-64bit.tar.bz2 -C /opt/software/mogdb
   ```

2. Assume that the decompressed package is stored in the **/opt/software/mogdb** directory. Go to the **simpleInstall** directory.

   ```bash
   cd /opt/software/mogdb/simpleInstall
   ```

3. Run the **sh install.sh -w** &lt;*login password*&gt; command to install MogDB. For example, **sh install.sh -w Aqz@179**.

   ```bash
   sh install.sh  -w xxxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   >
   > - **-w**: initializes the database password (specified by **gs_initdb**). This parameter is mandatory for security purposes. Make sure that the password complexity requirement is met, including uppercase and lower case letters, characters, and digits.
   > - **-p**: specifies the MogDB port number. If the port number is not specified, the default value **5432** is used.
   > - **-h|-help**: displays usage instructions.
   > - After the installation, the database name is **sgnode**.
   > - The installation path of the database directory is **/opt/software/mogdb/data/single_node**, in which **/opt/software/mogdb** is the decompression path and **data/single_node** is the newly created database node directory.

4. After the installation is complete, check whether the process is normal by using **ps** and **gs_ctl**.

   ```
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/single_node
   ```

   Run the **ps** command to display information similar to the following:

   ```
   omm      24209 11.9  1.0 1852000 355816 pts/0  Sl   01:54   0:33 /opt/software/mogdb/bin/gaussdb -D /opt/software/mogdb/single_node
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   Run the **gs_ctl** command to display information similar to the following:

   ```
   gs_ctl query ,datadir is /opt/software/mogdb/data/single_node
   HA state:
       local_role                     : Normal
       static_connections             : 0
       db_state                       : Normal
       detail_information             : Normal

   Senders info:
       No information

    Receiver info:
   No information
   ```
