<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## Administrator Guide

+ Routine Maintenance
  + [Starting and Stopping MogDB](/administrator-guide/routine-maintenance/0-starting-and-stopping-mogdb.md)
  + [Using the gsql Client for Connection](/administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md)
  + [Routine Maintenance](/administrator-guide/routine-maintenance/1-routine-maintenance-check-items.md)
  + [Checking OS Parameters](/administrator-guide/routine-maintenance/2-checking-os-parameters.md)
  + [Checking MogDB Health Status](/administrator-guide/routine-maintenance/3-checking-mogdb-health-status.md)
  + [Checking Database Performance](/administrator-guide/routine-maintenance/4-checking-database-performance.md)
  + [Checking and Deleting Logs](/administrator-guide/routine-maintenance/5-checking-and-deleting-logs.md)
  + [Checking Time Consistency](/administrator-guide/routine-maintenance/6-checking-time-consistency.md)
  + [Checking The Number of Application Connections](/administrator-guide/routine-maintenance/7-checking-the-number-of-application-connections.md)
  + [Routinely Maintaining Tables](/administrator-guide/routine-maintenance/8-routinely-maintaining-tables.md)
  + [Routinely Recreating an Index](/administrator-guide/routine-maintenance/9-routinely-recreating-an-index.md)
  + [Data Security Maintenance Suggestions](/administrator-guide/routine-maintenance/10-data-security-maintenance-suggestions.md)
  + [Log Reference](/administrator-guide/routine-maintenance/11-log-reference.md)
+ [Primary and Standby Management](/administrator-guide/primary-and-standby-management.md)
+ MOT Engine
  + Introducing MOT
    + [MOT Introduction](/administrator-guide/mot-engine/1-introducing-mot/1-mot-introduction.md)
    + [MOT Features and Benefits](/administrator-guide/mot-engine/1-introducing-mot/2-mot-features-and-benefits.md)
    + [MOT Key Technologies](/administrator-guide/mot-engine/1-introducing-mot/3-mot-key-technologies.md)
    + [MOT Usage Scenarios](/administrator-guide/mot-engine/1-introducing-mot/4-mot-usage-scenarios.md)
    + [MOT Performance Benchmarks](/administrator-guide/mot-engine/1-introducing-mot/5-mot-performance-benchmarks.md)
  + Using MOT
    + [Using MOT Overview](/administrator-guide/mot-engine/2-using-mot/1-using-mot-overview.md)
    + [MOT Preparation](/administrator-guide/mot-engine/2-using-mot/2-mot-preparation.md)
    + [MOT Deployment](/administrator-guide/mot-engine/2-using-mot/3-mot-deployment.md)
    + [MOT Usage](/administrator-guide/mot-engine/2-using-mot/4-mot-usage.md)
    + [MOT Administration](/administrator-guide/mot-engine/2-using-mot/5-mot-administration.md)
    + [MOT Sample TPC-C Benchmark](/administrator-guide/mot-engine/2-using-mot/6-mot-sample-tpcc-benchmark.md)
  + Concepts of MOT
    + [MOT Scale-up Architecture](/administrator-guide/mot-engine/3-concepts-of-mot/3-1.md)
    + [MOT Concurrency Control Mechanism](/administrator-guide/mot-engine/3-concepts-of-mot/3-2.md)
    + [Extended FDW and Other MogDB Features](/administrator-guide/mot-engine/3-concepts-of-mot/3-3.md)
    + [NUMA Awareness Allocation and Affinity](/administrator-guide/mot-engine/3-concepts-of-mot/3-4.md)
    + [MOT Indexes](/administrator-guide/mot-engine/3-concepts-of-mot/3-5.md)
    + [MOT Durability Concepts](/administrator-guide/mot-engine/3-concepts-of-mot/3-6.md)
    + [MOT Recovery Concepts](/administrator-guide/mot-engine/3-concepts-of-mot/3-7.md)
    + [MOT Query Native Compilation (JIT)](/administrator-guide/mot-engine/3-concepts-of-mot/3-8.md)
    + [Comparison - Disk vs. MOT](/administrator-guide/mot-engine/3-concepts-of-mot/3-9.md)
  + Appendix
    + [References](/administrator-guide/mot-engine/4-appendix/1-references.md)
    + [Glossary](/administrator-guide/mot-engine/4-appendix/2-glossary.md)
+ [Column-store Tables Management](/administrator-guide/column-store-tables-management.md)
+ Backup and Restoration
  + [Overview](/administrator-guide/br/1-1-br.md)
  + [Physical Backup and Restoration](/administrator-guide/br/1-2-br.md)
  + [Logical Backup and Restoration](/administrator-guide/br/1-3-br.md)
  + [Flashback Restoration](/administrator-guide/br/1-4-br.md)
+ Importing and Exporting Data
  + Importing Data
    + [Import Modes](/administrator-guide/importing-and-exporting-data/importing-data/1-import-modes.md)
    + [Running the INSERT Statement to Insert Data](/administrator-guide/importing-and-exporting-data/importing-data/2-running-the-INSERT-statement-to-insert-data.md)
    + [Running the COPY FROM STDIN Statement to Import Data](/administrator-guide/importing-and-exporting-data/importing-data/3-running-the-COPY-FROM-STDIN-statement-to-import-data.md)
    + [Using a gsql Meta-Command to Import Data](/administrator-guide/importing-and-exporting-data/importing-data/4-using-a-gsql-meta-command-to-import-data.md)
    + [Using gs_restore to Import Data](/administrator-guide/importing-and-exporting-data/importing-data/5-using-gs_restore-to-import-data.md)
    + [Updating Data in a Table](/administrator-guide/importing-and-exporting-data/importing-data/6-updating-data-in-a-table.md)
    + [Deep Copy](/administrator-guide/importing-and-exporting-data/importing-data/7-deep-copy.md)
    + [ANALYZE Table](/administrator-guide/importing-and-exporting-data/importing-data/8-ANALYZE-table.md)
    + [Doing VACUUM to a Table](/administrator-guide/importing-and-exporting-data/importing-data/9-doing-VACUUM-to-a-table.md)
    + [Managing Concurrent Write Operations](/administrator-guide/importing-and-exporting-data/importing-data/10-managing-concurrent-write-operations.md)
  + Exporting Data
    + [Using gs_dump and gs_dumpall to Export Data Overview](/administrator-guide/importing-and-exporting-data/exporting-data/1-using-gs_dump-and-gs_dumpall-to-export-data-overview.md)
    + [Exporting a Single Database](/administrator-guide/importing-and-exporting-data/exporting-data/2-exporting-a-single-database.md)
    + [Exporting All Databases](/administrator-guide/importing-and-exporting-data/exporting-data/3-exporting-all-databases.md)
    + [Data Export By a User Without Required Permissions](/administrator-guide/importing-and-exporting-data/exporting-data/4-data-export-by-a-user-without-required-permissions.md)
+ [Upgrade Guide](/administrator-guide/upgrade-guide.md)
