---
title: Starting and Stopping MogDB
summary: Starting and Stopping MogDB
author: Guo Huan
date: 2021-06-24
---

# Starting and Stopping MogDB

## Starting MogDB

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to start MogDB:

   ```bash
   gs_om -t start
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** An HA cluster must be started in HA mode. If the cluster is started in standalone mode, you need to restore the HA relationship by running the **gs_ctl build** command. For details about how to use the **gs_ctl** tool, see [gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md).

## Stopping MogDB

1. Log in as the OS user **omm** to the primary node of the database.

2. Run the following command to stop MogDB:

   ```bash
   gs_om -t stop
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** For details about how to start and stop nodes and availability zones (AZs), see [gs_om](../../reference-guide/tool-reference/server-tools/8-gs_om.md).

## Examples

Start MogDB:

```bash
gs_om -t start
Starting cluster.
=========================================
=========================================
Successfully started.
```

Stop MogDB:

```bash
gs_om -t stop
Stopping cluster.
=========================================
Successfully stopped cluster.
=========================================
End stop cluster.
```

## Troubleshooting

If starting or stopping MogDB fails, troubleshoot the problem based on log information. For details, see [Log Reference](11-log-reference.md).

If the startup fails due to timeout, you can run the following command to set the startup timeout interval, which is 300s by default:

```bash
gs_om -t start --time-out=300
```
