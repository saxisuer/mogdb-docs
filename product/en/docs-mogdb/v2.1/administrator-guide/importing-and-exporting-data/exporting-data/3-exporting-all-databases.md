---
title: Exporting All Databases
summary: Exporting All Databases
author: Guo Huan
date: 2021-03-04
---

# Exporting All Databases

## Exporting All Databases

You can use **gs_dumpall** to export full information of all databases in MogDB, including information about each database and global objects in MogDB. You can specify the information to export as follows:

- Export full information of all databases, including information about each database and global objects (such as roles and tablespaces) in MogDB.

  You can use the exported information to create a host environment containing the same databases, global objects, and data as the current one.

- Export data of all databases, excluding all object definitions and global objects.

- Export all object definitions of all databases, including the definitions of tablespaces, databases, functions, schemas, tables, indexes, and stored procedures.

  You can use the exported object definitions to quickly create a host environment that is the same as the current one, containing the same databases and tablespaces but no data.

### Procedure

1. Log in as the OS user **omm** to the primary node of the database.

2. Run **gs_dumpall** to export full information of all databases.

   ```bash
   gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_backup.sql -p 8000
   ```

   **Table 1** Common parameters

   | Parameter | Description                                                  | Example Value                        |
   | :-------- | :----------------------------------------------------------- | :----------------------------------- |
   | -U        | Username for database connection. The user must be an MogDB administrator. | -U omm                               |
   | -W        | User password for database connection.<br/>- This parameter is not required for database administrators if the trust policy is used for authentication.<br/>- If you connect to the database without specifying this parameter and you are not a database administrator, you will be prompted to enter the password. | -W Bigdata@123                       |
   | -f        | Folder to store exported files. If this parameter is not specified, the exported files are stored in the standard output. | -f /home/omm/backup/MPPDB_backup.sql |
   | -p        | TCP port or local Unix-domain socket file extension on which the server is listening for connections. | -p 8000                              |

   For details about other parameters, see "Tool Reference > Server Tools > [gs_dumpall](../../../reference-guide/tool-reference/server-tools/6-gs_dumpall.md)" in the **Reference Guide**.

### Examples

Example 1: Run **gs_dumpall** as the cluster administrator **omm** to export full information of all databases in a cluster. After the command is executed, a large amount of output information will be displayed. **total time** will be displayed at the end of the information, indicating that the backup is successful. In this example, only relative output information is included.

```bash
gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_backup.sql -p 8000
gs_dumpall[port='8000'][2017-07-21 15:57:31]: dumpall operation successful
gs_dumpall[port='8000'][2017-07-21 15:57:31]: total time: 9627  ms
```

Example 2: Run **gs_dumpall** as the cluster administrator **omm** to export object definitions of all databases in a cluster. The exported files are in text format. After the command is executed, a large amount of output information will be displayed. **total time** will be displayed at the end of the information, indicating that the backup is successful. In this example, only relative output information is included.

```bash
gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_backup.sql -p 8000 -s
gs_dumpall[port='8000'][2018-11-14 11:28:14]: dumpall operation successful
gs_dumpall[port='8000'][2018-11-14 11:28:14]: total time: 4147  ms
```

Example 3: Run **gs_dumpall** to export data of all databases in a cluster, encrypt the exported files, and store them in text format. After the command is executed, a large amount of output information will be displayed. **total time** will be displayed at the end of the information, indicating that the backup is successful. In this example, only relative output information is included.

```bash
gs_dumpall -f /home/omm/backup/MPPDB_backup.sql -p 8000 -a --with-encryption AES128 --with-key 1234567812345678
gs_dumpall[port='8000'][2018-11-14 11:32:26]: dumpall operation successful
gs_dumpall[port='8000'][2018-11-14 11:23:26]: total time: 4147  ms
```

## Exporting Global Objects

You can use **gs_dumpall** to export global objects, including database users, user groups, tablespaces, and attributes (for example, global access permissions), from MogDB.

### Procedure

1. Log in as the OS user **omm** to the primary node of the database.

2. Run **gs_dumpall** to export global tablespaces.

   ```bash
   gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_tablespace.sql -p 8000 -t
   ```

   **Table 1** Common parameters

   | Parameter | Description                                                  | Example Value                                |
   | :-------- | :----------------------------------------------------------- | :------------------------------------------- |
   | -U        | Username for database connection. The user must be an MogDB administrator. | -U omm                                       |
   | -W        | User password for database connection.<br/>- This parameter is not required for database administrators if the trust policy is used for authentication.<br/>- If you connect to the database without specifying this parameter and you are not a database administrator, you will be prompted to enter the password. | -W Bigdata@123                               |
   | -f        | Folder to store exported files. If this parameter is not specified, the exported files are stored in the standard output. | -f /home/omm/backup/**MPPDB_tablespace**.sql |
   | -p        | TCP port or local Unix-domain socket file extension on which the server is listening for connections. | -p 8000                                      |
   | -t        | Dumps only tablespaces. You can also use **-tablespaces-only** alternatively. | -                                            |

   For details about other parameters, see "Tool Reference > Server Tools > [gs_dumpall](../../../reference-guide/tool-reference/server-tools/6-gs_dumpall.md)" in the **Reference Guide**.

### Examples

Example 1: Run **gs_dumpall** as the cluster administrator **omm** to export global tablespaces and users of all databases. The exported files are in text format.

```bash
gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_globals.sql -p 8000 -g
gs_dumpall[port='8000'][2018-11-14 19:06:24]: dumpall operation successful
gs_dumpall[port='8000'][2018-11-14 19:06:24]: total time: 1150  ms
```

Example 2: Run **gs_dumpall** as the cluster administrator **omm** to export global tablespaces of all databases, encrypt the exported files, and store them in text format.

```bash
gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_tablespace.sql -p 8000 -t --with-encryption AES128 --with-key 1212121212121212
gs_dumpall[port='8000'][2018-11-14 19:00:58]: dumpall operation successful
gs_dumpall[port='8000'][2018-11-14 19:00:58]: total time: 186  ms
```

Example 3: Run **gs_dumpall** as the cluster administrator **omm** to export global users of all databases. The exported files are in text format.

```bash
gs_dumpall -W Bigdata@123 -U omm -f /home/omm/backup/MPPDB_user.sql -p 8000 -r
gs_dumpall[port='8000'][2018-11-14 19:03:18]: dumpall operation successful
gs_dumpall[port='8000'][2018-11-14 19:03:18]: total time: 162  ms
```
