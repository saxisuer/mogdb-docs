---
title: Logical Replication
summary: Logical Replication
author: Guo Huan
date: 2022-05-09
---

# Logical Replication

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

MogDB provides the logical decoding function to reversely parse physical logs to logical logs. Logical replication tools such as DRS convert logical logs to SQL statements and replay the SQL statements in the peer database. In this way, data can be synchronized between heterogeneous databases. Currently, unidirectional and bidirectional logical replication between the MogDB database and the MySQL or Oracle database is supported.

## Benefits

Logical replication is applicable to real-time database data migration, dual-database active-active system, and rolling upgrades.

## Description

DNs reversely parse physical logs to logical logs. Logical replication tools such as DRS extract logical logs from DNs, convert the logs to SQL statements, and replay the SQL statements in MySQL. Logical replication tools also extract logical logs from a MySQL database, reversely parse the logs to SQL statements, and replay the SQL statements in MogDB. In this way, data can be synchronized between heterogeneous databases.

## Enhancements

- MogDB 1.1.0 logic decoding supports the extraction of logs from full and incremental logs.
- MogDB 1.1.0 supports logical decoding on a standby node.

## Constraints

Column-store replication and DDL replication are not supported.

## Dependencies

It depends on logical replication tools that decode logical logs.