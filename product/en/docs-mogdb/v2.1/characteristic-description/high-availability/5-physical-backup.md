---
title: Physical Backup
summary: Physical Backup
author: Guo Huan
date: 2022-05-09
---

# Physical Backup

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Data in the entire database is backed up to a specified storage medium in an internal format.

## Benefits

Through physical backup, you can achieve the following purposes:

- Back up data of the entire database to a reliable storage medium, improving system reliability.
- Improve backup and restoration performance using an internal data format.
- Archive cold data.

The typical physical backup policy and application scenario are as follows:

- On Monday, perform a full backup of the database.
- On Tuesday, perform an incremental backup based on the full backup on Monday.
- On Wednesday, perform an incremental backup based on the incremental backup on Tuesday.
- …
- On Sunday, perform an incremental backup based on the incremental backup on Saturday.

The preceding backup operations are executed every week.

## Description

MogDB 1.1.0 provides the physical backup capability to back up data of the entire database to local disk files, OBS objects, NBU objects, or EISOO objects in the internal database format, and restore data of the entire database in a homogeneous database. In addition to the preceding functions, it also provides advanced functions such as compression, flow control, and resumable backup.

Physical backup is classified into full backup and incremental backup. The difference is as follows: Full backup includes the full data of the database at the backup time point. The time required for full backup is long (in direct proportion to the total data volume of the database), and a complete database can be restored. An incremental backup involves only incremental data modified after a specified time point. It takes a short period of time (in direct proportion to the incremental data volume and irrelevant to the total data volume). However, a complete database can be restored only after the incremental backup and full backup are performed.

## Enhancements

Supports full backup and incremental backup simultaneously.

## Constraints

For details about constraints on physical backup, see “[Physical Backup and Restoration](../../administrator-guide/br/1-2-br.md)” in the *Administrator Guide*.

## Dependencies

None