---
title: DeepSQL AI Algorithm in the Database
summary: DeepSQL AI Algorithm in the Database
author: Guo Huan
date: 2022-05-09
---

# DeepSQL: AI Algorithm in the Database

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The database DeepSQL feature implements the DB4AI function, that is, the AI algorithm is implemented in the database to better support quick analysis and computing of big data. A complete set of SQL-based machine learning, data mining, and statistics algorithms is provided. Users can directly use SQL statements to perform machine learning. DeepSQL can abstract the end-to-end R&D process from data to models. With the bottom-layer engine and automatic optimization, technical personnel with basic SQL knowledge can complete most machine learning model training and prediction tasks. The entire analysis and processing are running in the database engine. Users can directly analyze and process data in the database without transferring data between the database and other platforms. This avoids unnecessary data movement between multiple environments.

## Benefits

Users can directly use AI algorithms in the database to avoid extra costs caused by migration of a large amount of data. In addition, models can be centrally managed by the database, which is easy to use.

## Description

DeepSQL is an enhancement to MogDB DB4AI. DeepSQL encapsulates common machine learning algorithms into UDF and supports more than 60 general algorithms, including regression algorithms (such as linear regression, logistic regression, and random forest), classification algorithms (such as KNN), and clustering algorithms (such as K-means). In addition to basic machine learning algorithms, graph-related algorithms are also included, such as algorithms about the shortest path and graph diameter. Also, it supports data processing (such as PCA), sparse vectors, common statistical algorithms (such as covariance and Pearson coefficient calculation), training set and test set segmentation, and cross validation.

## Enhancements

None

## Constraints

- Python 2.7.12 or later has been installed.
- The database supports the PL/Python stored procedure.
- You have the administrator permission to install the algorithm database.

## Dependencies

None