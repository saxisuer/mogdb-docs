---
title: Index-advisor Index Recommendation
summary: Index-advisor Index Recommendation
author: Guo Huan
date: 2022-05-09
---

# Index-advisor: Index Recommendation

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Index-advisor is an intelligent database index recommendation tool that covers multiple task levels and application scenarios. It provides the single-query index recommendation function, virtual index function, and workload-level index recommendation function to provide reliable index suggestions for users.

## Benefits

This feature provides the quick and reliable index recommendation function, greatly simplifying the work of O&M personnel.

## Description

The single-query index recommendation function allows users to directly perform operations in the database. This function generates recommended indexes for a single query statement entered by users based on the semantic information of the query statement and the statistics of the database. The virtual index function allows users to directly perform operations in the database. This function simulates the creation of a real index to avoid the time and space overhead required for creating a real index. Based on the virtual index, users can evaluate the impact of the index on the specified query statement by using the optimizer. The workload-level index recommendation can be used by running scripts outside the database. This function uses the workload of multiple DML statements as the input to generate a batch of indexes that can optimize the overall workload execution performance.

## Enhancements

None

## Constraints

The database is normal, and the client can be connected properly.

The gsql tool has been installed by the current user, and the tool path has been added to the _PATH_environment variable.

The Python 3.6+ environment is available.

## Dependencies

None