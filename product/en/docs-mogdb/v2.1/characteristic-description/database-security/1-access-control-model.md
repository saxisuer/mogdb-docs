---
title: Access Control Model
summary: Access Control Model
author: Guo Huan
date: 2022-05-09
---

# Access Control Model

## Availability

This feature is available as of MogDB 1.1.0.

## Introduction

The access control model can be used to manage users' access permissions and grant them the minimum permissions required for completing a task.

## Benefits

You can create users and grant permissions to them as needed to minimize risks.

## Description

The database provides a role-based access control model and an access control model based on the separation of duties. In the role-based access control model, database roles are classified into system administrator, monitoring administrator, O&M administrator, security policy administrator, and common user. The security administrator creates roles or user groups and grant permissions to roles. The monitoring administrator views the monitoring views or functions in **dbe_perf** mode. The O&M administrator uses the Roach tool to back up and restore the database. The security policy administrator creates resource labels, anonymization policies, and unified audit policies. A user who is assigned a role has the role's permissions.

In the access control model based on the separation of duties, database roles are classified into system administrator, security administrator, audit administrator, monitoring administrator, O&M administrator, security policy administrator, and common user. The security administrator creates users, the system administrator grants permissions to users, and the audit administrator audits all user behavior.

By default, the role-based access control model is used. To switch to another mode, set the GUC parameter **enableSeparationOfDuty** to **on**.

## Enhancements

None

## Constraints

The permissions of the system administrator are controlled by the GUC parameter **enableSeparationOfDuty**.

The database needs to be restarted when the separation of duties is enabled, disabled or switched. In addition, improper user permissions in the new model cannot be automatically identified. The database administrator needs to manually identify and rectify the fault.

## Dependencies

None