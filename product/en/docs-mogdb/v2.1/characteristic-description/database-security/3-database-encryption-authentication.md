---
title: Database Encryption Authentication
summary: Database Encryption Authentication
author: Guo Huan
date: 2022-05-09
---

# Database Encryption Authentication

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The client/server (C/S) mode-based client connection authentication mechanism is provided.

## Benefits

The unidirectional, irreversible hash encryption algorithm PBKDF2 is used for encryption and authentication, effectively defending against rainbow attacks.

## Description

MogDB uses a basic client connection authentication mechanism. After a client initiates a connection request, the server verifies the information and sends the information required for authentication to the client based on the verification result. The authentication information includes the salt, token, and server signature. The client responds to the request and sends the authentication information to the server. The server calls the authentication module to authenticate the client authentication information. The user password is encrypted and stored in the memory. During the entire authentication process, passwords are encrypted for storage and transmission. When the user logs in to the system next time, the hash value is calculated and compared with the key value stored on the server to verify the correctness.

## Enhancements

The message processing flow in the unified encryption and authentication process effectively prevents attackers from cracking the username or password by capturing packets.

## Constraints

None

## Dependencies

None