---
title: High Concurrency of Thread Pools
summary: High Concurrency of Thread Pools
author: Guo Huan
date: 2022-05-09
---

# High Concurrency of Thread Pools

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The thread pooling technology is used to support stable running of databases at high concurrency.

## Benefits

The overall system throughput is stable in case of a large number of concurrent requests.

## Description

The overall design idea of the thread pool technology is to pool thread resources and reuse them among different connections. After the system is started, a fixed number of working threads are started based on the current number of cores or user configuration. A working thread serves one or more connection sessions. In this way, the session and thread are decoupled. The number of worker threads is fixed. Therefore, frequent thread switchover does not occur in case of high concurrency. The database layer schedules and manages sessions.

## Enhancements

This feature is available since MogDB 1.1.0.

In MogDB 1.1.0, thread pools can be dynamically scaled in or out.

## Constraints

None

## Dependencies

None