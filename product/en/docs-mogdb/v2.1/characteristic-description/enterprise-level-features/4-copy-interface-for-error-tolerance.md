---
title: Copy Interface for Error Tolerance
summary: Copy Interface for Error Tolerance
author: Guo Huan
date: 2022-05-09
---

# Copy Interface for Error Tolerance

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Certain errors that occur during the copy process are imported to a specified error table without interrupting the process.

## Benefits

Refine the copy function and improve the tolerance and robustness to common errors such as invalid formats.

## Description

MogDB provides the encapsulated copy error tables for creating functions and allows users to specify error tolerance options when using the **Copy From** statement. In this way, errors related to parsing, data format, and character set during the execution of the **Copy From** statement are recorded in the error table instead of being reported and interrupted. Even if a small amount of data in the target file of **Copy From** is incorrect, the data can be imported to the database. You can locate and rectify the fault in the error table later.

## Enhancements

None

## Constraints

For details, see “Importing Data > Running the COPY FROM STDIN Statement to Import Data > [Handling Import Errors](../../administrator-guide/importing-and-exporting-data/importing-data/3-running-the-COPY-FROM-STDIN-statement-to-import-data.md#handling-import-errors)” in the *Administrator Guide*.

## Dependencies

None