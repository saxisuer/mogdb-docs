---
title: Support for Functions and Stored Procedures
summary: Support for Functions and Stored Procedures
author: Guo Huan
date: 2022-05-09
---

# Support for Functions and Stored Procedures

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

Functions and stored procedures are important database objects. They encapsulate SQL statement sets used for certain functions so that the statements can be easily invoked.

## Benefits

1. Allows customers to modularize program design and encapsulate SQL statement sets, easy to invoke.
2. Caches the compilation results of stored procedures to accelerate SQL statement set execution.
3. Allows system administrators to restrict the permission for executing a specific stored procedure and controls access to the corresponding type of data. This prevents access from unauthorized users and ensures data security.

## Description

MogDB supports functions and stored procedures compliant with the SQL standard. The stored procedures are compatible with certain mainstream stored procedure syntax, improving their usability.

## Enhancements

PL/pgSQL single-step debugging is supported.

## Constraints

None.

## Dependencies

None.