---
title: Stored Procedure Debugging
summary: Stored Procedure Debugging
author: Guo Huan
date: 2022-05-09
---

# Stored Procedure Debugging

## Availability

This feature was introduced in MogDB 1.1.0. After the third-party database code directory structure was adjusted, this feature was temporarily deleted and is now available since MogDB 1.1.0.

## Introduction

This feature provides a group of APIs for debugging stored procedures, such as breakpoint debugging and variable printing.

## Benefits

This feature improves user experience in developing stored procedures based on MogDB.

## Description

Stored procedures are important database objects. They encapsulate SQL statement sets used for certain functions so that the statements can be easily invoked. A stored procedure usually contains many SQL statements and procedural execution structures, depending on the service scale. However, writing a large stored procedure is usually accompanied by logic bugs. It is difficult or even impossible to find the bugs by only executing the stored procedure. Therefore, a debugging tool is required.

The stored procedure debugging tool provides a group of debugging APIs to enable the stored procedure to be executed step by step. During the execution, you can set breakpoints and print variables so that SQL developers can detect and correct errors in time and develop functions more efficiently and with high quality.

## Enhancements

None

## Constraints

None

## Dependencies

None