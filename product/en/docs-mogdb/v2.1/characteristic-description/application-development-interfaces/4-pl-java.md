---
title: PL/Java
summary: PL/Java
author: Guo Huan
date: 2022-05-09
---

# PL/Java

## Availability

This feature is available since MogDB 1.1.0.

## Introduction

The Java user-defined field (UDF) is supported.

## Benefits

A development environment is provided for multiple functions.

## Description

With the MogDB PL/Java function, you can choose your favorite Java IDE to write Java methods and install the JAR files containing these methods into MogDB before invoking them. MogDB PL/Java is developed based on open-source tada PL/Java 1.5.2. PL/Java uses Huawei JDK V100R001C00SPC190B003-b09.

## Enhancements

None

## Constraints

- Java UDFs can implement simple Java computing. However, do not encapsulate services in Java UDFs.

- Do not connect to a database in any way (for example, JDBC) in Java functions.

- Use Huawei JDK V100R001C00SPC190B003-b09 to compile Java methods and JAR files.

- Currently, only data types listed in Table 1 are supported. Other data types, such as user-defined data types and complex data types (for example, Java array and its derived types) are not supported.

- Currently, UDAF and UDTF are not supported.

  **Table 1** PL/Java mapping for default data types

  | MogDB   | **Java**                                           |
  | :---------- | :------------------------------------------------- |
  | BOOLEAN     | boolean                                            |
  | “char”      | byte                                               |
  | bytea       | byte[]                                             |
  | SMALLINT    | short                                              |
  | INTEGER     | int                                                |
  | BIGINT      | long                                               |
  | FLOAT4      | float                                              |
  | FLOAT8      | double                                             |
  | CHAR        | java.lang.String                                   |
  | VARCHAR     | java.lang.String                                   |
  | TEXT        | java.lang.String                                   |
  | name        | java.lang.String                                   |
  | DATE        | java.sql.Timestamp                                 |
  | TIME        | java.sql.Time (stored value treated as local time) |
  | TIMETZ      | java.sql.Time                                      |
  | TIMESTAMP   | java.sql.Timestamp                                 |
  | TIMESTAMPTZ | java.sql.Timestamp                                 |

## Dependencies

PL/Java depends on the Java Development Kit (JDK) environment. Currently, JDK is included in MogDB and installation is not required. If you have installed the same or different versions of JDK, no conflict will occur. MogDB uses Huawei JDK V100R001C00SPC190B003-b09 to run PL/Java.