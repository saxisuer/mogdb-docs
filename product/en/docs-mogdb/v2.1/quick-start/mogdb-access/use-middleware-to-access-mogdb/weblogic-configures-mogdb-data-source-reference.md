---
title: WebLogic Used for Configuring MogDB (PostgreSQL) Data Sources
summary: WebLogic Used for Configuring MogDB (PostgreSQL) Data Sources
author: Guo Huan
date: 2021-08-11
---

# WebLogic Used for Configuring MogDB (PostgreSQL) Data Sources

## Overview

WebLogic can be used for configuring MogDB data sources. So does it for configuring PostgreSQL data sources. For details, see the following sections.

<br/>

## Prerequisites

### Confirmation of the WebLogic JDK Version

```bash
$ /home/jdk1.7.0_80/bin/java -version
java version "1.7.0_80"
Java(TM) SE Runtime Environment (build 1.7.0_80-b15)
Java HotSpot(TM) 64-Bit Server VM (build 24.80-b11, mixed mode)
```

Find the target JDK and check its version.

The JAR packages of the JDBC driver vary depending on the JDK version. Only when you are clear about the JDK version can you download the corresponding driver.

The current environment takes JDK 1.7.0 as an example.

### Downloading of the JDBC Driver Package

```
The current version is JDBC 42.2.23.

This JDBC is the target driver to be downloaded unless you have special requirements (run the old application programs or JVM). It supports PostgreSQL 8.2 or later and requires Java 6 or a higher version. It also supports the SSL and javax.sql packages.

JDBC 4.2 applies when you use Java 8.
JDBC 4.1 applies when you use Java 7.
JDBC 4.0 applies when you use Java 6.
```

The links for downloading JDBC of certain versions corresponding to the JDK of specific versions are as follows:

| JDK Version | Website of the Corresponding JDBC Driver                     |
| ----------- | ------------------------------------------------------------ |
| 1.6         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre6.jar> |
| 1.7         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre7.jar> |
| 1.8         | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jar>  |

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-1.png)

The best choice is to use JDBC  42.2.23 because it supports PostgreSQL 8.2 or a higher version and is the most stable version.

Download website: [https://jdbc.postgresql.org/download/](https://jdbc.postgresql.org/download/)

<br/>

## Configuration Process

### Confirmation of Related Database Information

| Database Name | User  | Password | Port  | IP Address |
| ------------- | ----- | -------- | ----- | ---------- |
| postgres      | mogdb | 123456   | 25431 |            |

### Confirmation of Port Connection

On the middleware server, access the IP port using Telnet and make sure that the database port can be accessed.

### Configuration of Data Sources on the Console

#### Adding a Driver to CLASSPATH

a. Back up the startup file.

```
cd ${DOMAIN_HOME}/bin
cp startWebLogic.sh startWebLogic.sh.bak
```

b. Upload the driver to the WebLogic server.

For example, the JDK version is 1.7. Upload the JDK driver to the following directory:

/home/postgresql-42.2.23.jre7.jar

c. Modify the startup file to add the driver package.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-2.png)

Change CLASSPATH="${SAVE_CLASSPATH}"

to:

CLASSPATH="${SAVE_CLASSPATH}:/home/postgresql-42.2.23.jre7.jar"

d. Restart the server to make the change take effect.

#### Configuring Data Sources

a. Create new data sources.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-3.png)

b. Fill JNDI with related information.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-4.png)

c. Select the default driver.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-5.png)

d. Select the default option.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-6.png)

e. Fill the database information.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-7.png)

f. Test the connection.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-8.png)

g. Make sure that the test is successful.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-9.png)

h. Select the server as required.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-10.png)

i. Finish configurations and activate changes.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-11.png)

<br/>

## MogDB Connected Using the openGauss Driver

openGauss JDBC provides only JDK 1.8. Applications developed using JDK 1.8 can connect MogDB using the openGauss driver.

Applications developed using JDK 1.6 or 1.7 can connect MogDB using only the JDBC driver from the PostgreSQL official website.

Website for downloading the openGauss driver:

[https://opengauss.org/en/download/](https://opengauss.org/en/download/)

<br/>

## Common Questions

When the driver version is higher than the JDK version, the following error occurs once the server is started:

```
<xxx 10, 2021 10:43:53 PM CST> <Error> <Deployer> <BEA-149205> <Failed to initialize the application 'pg JDBC Data Source' due to error java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0.
java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0
        at java.lang.ClassLoader.defineClass1(Native Method)
        at java.lang.ClassLoader.defineClass(ClassLoader.java:800)
        at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
        at java.net.URLClassLoader.defineClass(URLClassLoader.java:449)
        at java.net.URLClassLoader.access$100(URLClassLoader.java:71)
        Truncated. see log file for complete stacktrace
Caused By: java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0
        at java.lang.ClassLoader.defineClass1(Native Method)
        at java.lang.ClassLoader.defineClass(ClassLoader.java:800)
        at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
        at java.net.URLClassLoader.defineClass(URLClassLoader.java:449)
        at java.net.URLClassLoader.access$100(URLClassLoader.java:71)
        Truncated. see log file for complete stacktrace
>
```
