---
title: Use Java to Access MogDB
summary: Use Java to Access MogDB
author: Guo Huan
date: 2022-03-25
---



# Use Java to Access MogDB

Java Database Connectivity (JDBC) is a Java API for running SQL statements. It provides unified access interfaces for different relational databases, based on which applications process data. MogDB supports JDBC 4.0.

For detailed instructions on using Java to access MogDB database, please refer to [Development Based on JDBC](../../../developer-guide/dev/2-development-based-on-jdbc/1-development-based-on-jdbc-overview.md) chapter in "Developer's Guide".
