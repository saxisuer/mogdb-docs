---
title: Use Rust to Access MogDB
summary: Use Rust to Access MogDB
author: Guo Huan
date: 2022-03-31
---



# Use Rust to Access MogDB

openGauss provides a driver for Rust based on [rust-postgres](https://github.com/sfackler/rust-postgres), which is also applicable to the Rust language for connecting to MogDB.

You can view the source code and operating instructions for this driver by clicking on the following links.

[Source Code](https://gitee.com/opengauss/openGauss-connector-rust), [Operating Instructions](https://gitee.com/opengauss/openGauss-connector-rust/blob/master/README.md)
