---
title: Installation on a Single Node
summary: Installation on a Single Node
author: Zhang Cuiping
date: 2021-04-2
---

# Installation on a Single Node

This article introduces the installation process of MogDB Simplified version, which is mainly for universities and personal test environments, and is simpler and faster than the enterprise installation process. The package does not contain OM tools, and the script can be used to achieve one-click installation and deployment.

<br/>

## Preparing the Software and Hardware Installation Environment

**Software Environment:** Visit [MogDB download page](https://www.mogdb.io/en/downloads/mogdb/) to download the installation package according to your OS and CPU architecture. The `MogDB-x.x.x-openEuler-64bit.tar.bz2` obtained after unpacking the standard installation package is the simplified installation package.

**Hardware Environment:** The minimum configuration for individual developers is 2 cores/4G, and the recommended configuration is 4 cores/8G.

> Note:
>
> In order to accommodate small memory machines, the simplified version sets some important memory parameters lower during deployment, such as `shared_buffers = 32MB`, `cstore_buffers = 512MB`. In addition, the database character set of the simplified version is changed from the original default `SQL_ACSII` character set to `en_US.UTF-8`, and the initial user password is not forced to be changed, i.e. `modify_initial_password = false`.

<br/>

## Running Environment Configuration

1. Disable the firewall.

   ```
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   setenforce=0
   sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
   ```

2. Install dependency packages.

   ```
   yum install libaio-devel flex bison ncurses-devel glibc-devel patch redhat-lsb-core readline-devel -y
   ```

3. Set the time zone and time.

   ```
   rm -fr /etc/localtime
   ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
   ll /etc/localtime
   ```

4. For openEuler OS, you need to disable RemoveIPC. For CentOS, skip this step.

   ```
   # Modify the value of "RemoveIPC" in the /etc/systemd/logind.conf file to "no".
   vim  /etc/systemd/logind.conf
   RemoveIPC=no
   
   # Modify the value of "RemoveIPC" in the /usr/lib/systemd/system/systemd-logind.service file to "no".
   vim /usr/lib/systemd/system/systemd-logind.service
   RemoveIPC=no
   
   # Reload the configuration parameters.
   systemctl daemon-reload
   systemctl restart systemd-logind
   
   # Check if the changes take effect.
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

<br/>

## Installation Procedure

1. Create directory, user, group, and authorize them.

   ```
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. Decompress the MogDB package to the installation directory.

   ```
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.bz2 -C /opt/software/mogdb/
   ```

3. Go to the **simpleInstall** directory.

   ```
   cd /opt/software/mogdb/simpleInstall
   ```

4. Run the **install.sh -w** command to install MogDB.

   ```
   sh install.sh  -w xxxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   >
   > - **-w**: initializes the database password (specified by **gs_initdb**). This parameter is mandatory for security purposes.
   > - **-p**: specifies the MogDB port number. If the port number is not specified, the default value **5432** is used.
   > - **-h|-help**: displays usage instructions.
   > - After the installation, the database name is **sgnode**.
   > - The installation path of the database directory is **/opt/software/mogdb/data/single_node**, in which **/opt/software/mogdb** is the decompression path and **data/single_node** is the newly created database node directory.

<br/>

## Check and Use

```
## Configure PATH
echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
source ~/.bash_profile
-bash: ulimit: open files: cannot modify limit: Operation not permitted
(The default setting fd available size exceeds the system setting, you can ignore the setting)

## Login to MogDB
gsql -d postgres -p 5432 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

mogdb=#
```