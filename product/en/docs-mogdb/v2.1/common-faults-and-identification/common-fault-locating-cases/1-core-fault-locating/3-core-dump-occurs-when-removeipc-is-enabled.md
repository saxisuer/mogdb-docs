---
title: Core Dump Occurs when RemoveIPC Is Enabled
summary: Core Dump Occurs when RemoveIPC Is Enabled
author: Guo Huan
date: 2021-05-24
---

# Core Dump Occurs when RemoveIPC Is Enabled

## Symptom

The **RemoveIPC** parameter in the OS configuration is set to **yes**. The database breaks down during running, and the following log information is displayed:

```
FATAL: semctl(1463124609, 3, SETVAL, 0) failed: Invalid argument
```

## Cause Analysis

If **RemoveIPC** is set to **yes**, the OS deletes the IPC resources (shared memory and semaphore) when the corresponding user exits. As a result, the IPC resources used by the MogDB server are cleared, causing the database to break down.

## Procedure

Set **RemoveIPC** to **no**. For details, see [Modifying OS Configuration](../../../installation-guide/standard-installation/3-modifying-os-configuration.md) in the *Installation Guide*.
