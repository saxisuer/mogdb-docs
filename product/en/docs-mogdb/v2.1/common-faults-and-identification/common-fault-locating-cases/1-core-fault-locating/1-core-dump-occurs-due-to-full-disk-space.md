---
title: Core Dump Occurs due to Full Disk Space
summary: Core Dump Occurs due to Full Disk Space
author: Guo Huan
date: 2021-05-24
---

# Core Dump Occurs due to Full Disk Space

## Symptom

When TPC-C is running, the disk space is full during injection. As a result, a core dump occurs on the MogDB process, as shown in the following figure.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-due-to-full-disk-space.png)

## Cause Analysis

When the disk is full, Xlog logs cannot be written. The program exits through the panic log.

## Procedure

Externally monitor the disk usage and periodically clean up the disk.
