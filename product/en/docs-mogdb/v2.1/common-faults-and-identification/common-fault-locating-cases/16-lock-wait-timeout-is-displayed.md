---
title: Lock wait timeout Is Displayed When a User Executes an SQL Statement
summary: Lock wait timeout Is Displayed When a User Executes an SQL Statement
author: Guo Huan
date: 2021-05-24
---

# "Lock wait timeout" Is Displayed When a User Executes an SQL Statement

## Symptom

"Lock wait timeout" is displayed when a user executes an SQL statement.

```
ERROR:  Lock wait timeout: thread 140533638080272 waiting for ShareLock on relation 16409 of database 13218 after 1200000.122 ms ERROR:  Lock wait timeout: thread 140533638080272 waiting for AccessExclusiveLock on relation 16409 of database 13218 after 1200000.193 ms
```

## Cause Analysis

Lock waiting times out in the database.

## Procedure

- After detecting such errors, the database automatically retries the SQL statements. The number of retries is controlled by **max_query_retry_times**.
- To analyze the cause of the lock wait timeout, find the SQL statements that time out in the **pg_locks** and **pg_stat_activity** system catalogs.
