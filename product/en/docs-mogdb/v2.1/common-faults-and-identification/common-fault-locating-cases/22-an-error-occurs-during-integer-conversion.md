---
title: An Error Occurs During Integer Conversion
summary: An Error Occurs During Integer Conversion
author: Guo Huan
date: 2021-05-24
---

# An Error Occurs During Integer Conversion

## Symptom

The following error is reported during integer conversion:

```
Invalid input syntax for integer: "13."
```

## Cause Analysis

Some data types cannot be converted to the target data type.

## Procedure

Gradually narrow down the range of SQL statements to determine the data types that cannot be converted.
