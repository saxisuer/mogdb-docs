---
title: Undo
summary: Undo
author: Zhang Cuiping
date: 2021-11-08
---

# Undo

## undo_space_limit_size

**Parameter description**: Specifies the undo forcible reclamation threshold. If 80% of the threshold is reached, forcible reclamation is triggered.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 800 MB to 32 TB

**Default value**: **32 GB**

## undo_limit_size_per_transaction

**Parameter description**: Specifies the undo space threshold of a single transaction. If the threshold is reached, the transaction is rolled back due to an error.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 100 MB to 32 TB

**Default value**: **32 GB**

## undo_zone_count

**Parameter description**: Specifies the number of undo zones that can be allocated in the memory. If the number reaches the threshold or the value set this time is less than the value set last time, an error is reported. After a proper value is set based on the rule, the number of undo zones becomes normal. If this parameter is set to **0**, the undo function is disabled and Ustore tables cannot be created.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1048576

Value constraint:

1. The value set this time must be greater than or equal to the value set last time. If the value is incorrect, the database cannot be restored. You need to reset the value and restart the database to restore the database.
2. The value of **undo_zone_count** needs to be adjusted based on the value of **max_connections**. After changing the value of **max_connections**, you need to change the value of **undo_zone_count**, which must be four times that of **max_connections**.

**Default value**: **0**
