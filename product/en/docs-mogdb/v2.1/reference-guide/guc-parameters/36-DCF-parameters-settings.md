---
title: DCF Parameters Settings
summary: DCF Parameters Settings
author: Zhang Cuiping
date: 2021-11-08
---

# DCF Parameters Settings

## enable_dcf

**Parameter description**: Specifies whether to enable the DCF mode. This parameter cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean. The value can be **on** or **off**. The value **on** indicates that the current installation mode is DCF, and the value **off** indicates that the current installation mode is not DCF.

**Default value**: **off**

## dcf_ssl

**Parameter description**: Specifies whether to enable SSL. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean. The value can be **on** or **off**. The value **on** indicates that SSL is used, and the value **off** indicates that SSL is not used.

**Default value**: **on**

## dcf_config

**Parameter description**: Specifies the customized configuration information during installation. This parameter cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Default value**: a string, which is specified by users during installation

## dcf_data_path

**Parameter description**: Specifies the DCF data path. This parameter cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Default value**: a string, which is the **dcf_data** directory under the data directory of the DN

## dcf_log_path

**Parameter description**: Specifies the DCF log path. This parameter cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Default value**: a string, which is the **dcf_log** directory under the data directory of the DN.

## dcf_node_id

**Parameter description**: Specifies the ID of the DN where the DCF is located. This parameter is defined by users during installation and cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Default value**: an integer, which is specified by users during installation

## dcf_max_workers

**Parameter description**: Specifies the number of DCF callback function threads.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10 to 262143

**Default value**: **10**

## dcf_truncate_threshold

**Parameter description**: Specifies the threshold for a DN to truncate DCF logs.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 2147483647

**Default value**: **100000**

## dcf_election_timeout

**Parameter description**: Specifies the timeout interval for selecting the DCF leader and follower. The election timeout interval depends on the status of the network between DNs. If the timeout interval is short and the network quality is poor, timeout occurs. After the network recovers, the election becomes normal. You are advised to set a proper timeout interval based on the current network status.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 600, in seconds

**Default value**: **3**

## dcf_run_mode

**Parameter description**: Specifies the DCF election mode. The value **0** indicates that the automatic DCF election mode is enabled, and the value **2** indicates that the DCF election mode is disabled.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Enumerated type. The value can be **0** or **2**.

**Default value**: **0**

## dcf_log_level

**Parameter description**: Specifies the DCF log level.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

- Disable the log function: **NONE**, indicating that the log function is disabled and cannot be used for the following log levels:

- Enable the log function: **RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER**

  You can select a string from the preceding strings and use vertical bars (|) to combine the strings. The log level cannot be left blank.

**Default value**: **RUN_ERR|RUN_WAR|DEBUG_ERR|OPER|RUN_INF|PROFILE**

## dcf_log_backup_file_count

**Parameter description**: Specifies the number of DCF run log backups.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 100

**Default value**: **10**

## dcf_max_log_file_size

**Parameter description**: Specifies the maximum size of a DCF run log file.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 1000, in MB

**Default value**: **10**

## dcf_socket_timeout

**Parameter description**: Specifies the timeout interval for the DCF communication module to connect to the socket. This parameter takes effect upon the system restart. In an environment where the network quality is poor, if the timeout interval is set to a small value, a connection may fail to be set up. In this case, you need to increase the value.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10 to 600000, in ms

**Default value**: **5000**

## dcf_connect_timeout

**Parameter description**: Specifies the timeout interval for the DCF communication module to set up a connection. This parameter takes effect upon the system restart. In an environment where the network quality is poor, if the timeout interval is set to a small value, the connection may fail to be set up. In this case, you need to increase the value.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 10 to 600000, in ms

**Default value:** **60000**

## dcf_mec_fragment_size

**Parameter description**: Specifies the fragment size of the DCF communication module. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 10240, in KB

**Default value**: **64**

## dcf_stg_pool_max_size

**Parameter description**: Specifies the maximum size of the memory pool of the DCF storage module. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 2147483647, in MB

**Default value**: **2048**

## dcf_stg_pool_init_size

**Parameter description**: Specifies the minimum size of the memory pool of the DCF storage module. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 2147483647, in MB

**Default value**: **32**

## dcf_mec_pool_max_size

**Parameter description**: Specifies the maximum size of the memory pool of the DCF communication module. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 2147483647, in MB

**Default value**: **200**

## dcf_flow_control_disk_rawait_threshold

**Parameter description**: Specifies the disk waiting threshold for DCF flow control.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 2147483647, in μs

**Default value**: **100000**

## dcf_flow_control_net_queue_message_num_threshold

**Parameter description**: Specifies the threshold for the number of messages in a network queue for DCF flow control.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 2147483647

**Default value:** **1024**

## dcf_flow_control_cpu_threshold

**Parameter description**: Specifies the threshold for DCF CPU flow control.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 2147483647, in percentage (%)

**Default value**: **100**

## dcf_mec_batch_size

**Parameter description**: Specifies the number of batch messages for DCF communication. When the value is **0**, the DCF automatically adjusts the value based on the network and the amount of data to be written. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 1024

**Default value**: **0**

## dcf_mem_pool_max_size

**Parameter description**: Specifies the maximum DCF memory. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 2147483647, in MB

**Default value**: **2048**

## dcf_mem_pool_init_size

**Parameter description**: Specifies the initial size of the DCF memory. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 32 to 2147483647, in MB

**Default value**: **32**

## dcf_compress_algorithm

**Parameter description**: Specifies the compression algorithm for DCF run log transmission. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer

- **0** indicates no compression.
- **1** indicates the ZSTD compression algorithm.
- **2** indicates the LZ4 compression algorithm.

**Default value**: **0**

## dcf_compress_level

**Parameter description**: Specifies the compression level for DCF log transmission. This parameter takes effect upon the system restart. Before this parameter takes effect, a valid compression algorithm must be configured, that is, the **dcf_compress_algorithm** parameter is set.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer

- If no compression algorithm is used, the value does not take effect.
- If the ZSTD compression algorithm is used, the value ranges from 1 to 22.
- If the LZ4 compression algorithm is used, the value ranges from 1 to 9.

**Default value**: **1**

## dcf_mec_channel_num

**Parameter description**: Specifies the number of DCF communication channels. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 64

**Default value**: **1**

## dcf_rep_append_thread_num

**Parameter description**: Specifies the number of DCF log replication threads. This parameter takes effect upon the system restart.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 1000

**Default value**: **2**

## dcf_mec_agent_thread_num

**Parameter description**: Specifies the number of DCF communication working threads. This parameter takes effect upon the system restart. It is recommended that the value of **dcf_mec_agent_thread_num** be greater than or equal to 2 x Number of nodes x Value of **dcf_mec_channel_num**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 1000

**Default value**: **10**

## dcf_mec_reactor_thread_num

**Parameter description**: Specifies the number of reactor threads used by the DCF. This parameter takes effect upon the system restart. It is recommended that the ratio of the value of **dcf_mec_reactor_thread_num** to the value of **dcf_mec_agent_thread_num** be 1:40.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 100

**Default value**: **1**

## dcf_log_file_permission

**Parameter description**: Specifies the attribute of a DCF run log file. This parameter takes effect upon the system restart. To allow other users in the same group to access logs, ensure that all parent directories can be accessed by other users in the same group.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated type. The value can be **600** or **640**.

**Default value**: **600**

## dcf_log_path_permission

**Parameter description**: Specifies the attribute of the DCF run log directory. This parameter takes effect upon the system restart and cannot be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated type. The value can be **700** or **750**.

**Default value**: **700**
