---
title: Equality Query in a Fully-encrypted Database
summary: Equality Query in a Fully-encrypted Database
author: Zhang Cuiping
date: 2021-04-20
---

# Equality Query in a Fully-encrypted Database

## enable_full_encryption

**Parameter description**: Specifies whether the fully-encrypted database takes effect.

This parameter is a BACKEND parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

After enable_full_encryption is set to on, you need to enter the "-C" parameter or "-enable-client-encryption" when using gsql to connect to the database, otherwise the connection will not succeed.

**Value range**: Boolean

- **on**: The fully-encrypted database is enabled.
- **off**: The fully-encrypted database is disabled.

**Default value**: **off**
