---
title: User-defined Functions
summary: User-defined Functions
author: Zhang Cuiping
date: 2021-11-08
---

# User-defined Functions

## udf_memory_limit

**Parameter description**: Controls the maximum physical memory that can be used when each database node executes UDFs. This parameter does not take effect in the current version. Use **FencedUDFMemoryLimit** and **UDFWorkerMemHardLimit** to control virtual memory used by fenced udf worker.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer. The value range is from 200 x 1024 to *max_process_memory* and the unit is KB.

**Default value**: **200 MB**

## FencedUDFMemoryLimit

**Parameter description**: Specifies the virtual memory used by each fenced udf worker process.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 KB to 2147483647 KB. The unit can also be MB or GB. **0** indicates that the memory is not limited.

**Default value**: **0**

## UDFWorkerMemHardLimit

**Parameter description**: Specifies the maximum value of **fencedUDFMemoryLimit**.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

Value range: an integer ranging from 0KB to 2147483647KB. The unit can also be MB or GB.

**Default value**: **1GB**

## pljava_vmoptions

**Parameter description**: Specifies the startup parameters for JVMs used by the PL/Java function. Only the sysadmin user can access this parameter.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, supporting:

- JDK8 JVM startup parameters. For details, see JDK [official](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html)descriptions.
- JDK8 JVM system attributes (starting with **-D**, for example, **-Djava.ext.dirs**). For details, see JDK [official](https://docs.oracle.com/javase/tutorial/deployment/doingMoreWithRIA/properties.html)descriptions.
- User-defined parameters (starting with **-D**, for example, **-Duser.defined.option**).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** If **pljava_vmoptions** is set to a value beyond the value range, an error will be reported when PL/Java functions are used.

**Default value:** empty
