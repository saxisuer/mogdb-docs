---
title: Connection Settings
summary: Connection Settings
author: Zhang Cuiping
date: 2021-04-20
---

# Connection Settings

This section describes parameters related to client-server connection modes.

## listen_addresses

**Parameter description**: Specifies the TCP/IP addresses that a server listens to for connections from the client. This parameter specifies the IP address used by the MogDB server to listen on, such as IPv4 address or IPv6 address(if supported). A server may have multiple NICs, and each NIC can be bound with multiple IP address. This parameter specifies the IP addresses which the MogDB server is bound. And the client can use the IP address specified in this parameter to connect to MogDB or send requests to MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**:

- Host name or IP address. Multiple values are separated with commas (,).
- Asterisk `*` or `0.0.0.0`, indicating that all IP addresses will be listened to, which is not recommended due to potential security risks. This parameter must be used together with valid addresses (for example, the local IP address). Otherwise, the build may fail. At the same time, when the configuration is `*` or `0.0.0.0` in the active/standby environment, the localport port number in the postgresql.conf file under the database path of the active node cannot be the database dataPortBase+1, otherwise the database cannot be started.
- If the parameter is not specified, the server does not listen to any IP address. In this case, only Unix domain sockets can be used for database connections.

**Default value**: After the database instance is installed, the default value is configured according to the IP address of different instances in the XML configuration file. The default value for the DN instance is **'x.x.x.x'**.

## local_bind_address

**Parameter description**: Specifies the host IP address bound to the current node for connecting to other nodes in MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Default value**: After the database instance is installed, the default value is configured according to the IP address of different instances in the XML configuration file. The default value for the DN instance is **'x.x.x.x'**.

## port

**Parameter description**: Specifies the TCP port listened on by the MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This parameter is specified in the configuration file during installation. Do not modify this parameter unless absolutely necessary. Otherwise, database communication will be affected.

**Value range**: an integer ranging from 1 to 65535

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - When setting the port number, ensure that the port number is not in use. When setting the port numbers of multiple instances, ensure that the port numbers do not conflict.
> - Ports 1 to 1023 are reserved for the operating system. Do not use them.
> - When the database instance is installed using the configuration file, pay attention to the ports reserved in the communication matrix in the configuration file. For example, *dataPortBase* + 1 needs to be reserved as the port used by internal tools, and *dataPortBase* + 6 needs to be reserved as the communication port of the flow engine message queue. Therefore, during database instance installation, the maximum port number is **65529** for DNs. Ensure that the port number does not conflict with each other.

**Default value**: **5432** (The actual value is specified in the configuration file during installation.)

## max_connections

**Parameter description**: Specifies the maximum number of concurrent connections to the database. This parameter influences the concurrent processing capability of MogDB.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer. The minimum value is **10** (greater than *max_wal_senders*). The theoretical maximum value is **262143**. The actual maximum value is a dynamic value, which is calculated using the formula 262143 -_job_queue_processes_ - *autovacuum_max_workers* - *AUXILIARY_BACKENDS* - *AV_LAUNCHER_PROCS*. The values of job_queue_processes, autovacuum_max_workers, and max_inner_tool_connections depend on the settings of the corresponding GUC parameters. *AUXILIARY_BACKENDS*indicates the number of reserved auxiliary threads, which is fixed to 20.*AV_LAUNCHER_PROCS* indicates the number of reserved lancher threads for autovacuum, which is fixed to 2.

**Default value**: **200**

**Setting suggestions**:

Retain the default value of this parameter on the primary node of the databases.

**Impact of incorrect configuration:**

- If the value of *max_connections* is too large and exceeds the dynamic maximum value described in the formula, the node fails to be started and the error message " invalid value for parameter "max_connections"" is displayed.
- If only the value of *max_connections* is increased while the memory parameter is not adjusted in proportion according to the external egress specifications, when the service load is heavy, the memory may be insufficient, and the error message "memory is temporarily unavailable" is displayed.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - If the number of connections of the administrator exceeds the value of *max_connections*, the administrator can still connect to the database after the connections are used up by common users. If the number of connections exceeds the value of *sysadmin_reserved_connections*, an error is reported. That is, the maximum number of connections of the administrator is equal to the value of *max_connections* + *sysadmin_reserved_connections*.
> - For common users, internal jobs use some connections. Therefore, the value of this parameter is slightly less than that of *max_connections*. The value depends on the number of internal connections.

## max_inner_tool_connections

**Parameter description**: Specifies the maximum number of concurrent connections of a tool which is allowed to connect to the database. This parameter influences the concurrent connection capability of the MogDB tool.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *MIN* (which takes the smaller value between **262143** and *max_connections*). For details about how to calculate the value of *max_connection*, see the preceding description.

**Default value**: **50** for each database node. If the default value is greater than the maximum value supported by the kernel (determined when the **gs_initdb** command is executed), an error message is displayed.

**Setting suggestions**:

Retain the default value of this parameter on the primary node of the databases.

If this parameter is set to a large value, MogDB requires more System V shared memories or semaphores, which may exceed the default maximum configuration of the OS. In this case, modify the value as needed.

## sysadmin_reserved_connections

**Parameter description**: Specifies the minimum number of connections reserved for administrators. You are advised not to set this parameter to a large value. This parameter is used together with the *max_connections* parameter. The maximum number of connections of the administrator is equal to the value of *max_connections* + *sysadmin_reserved_connections*.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to *MIN* (which takes the smaller value between **262143** and *max_connections*). For details about how to calculate the value of *max_connection*, see the preceding description.

**Default value**: **3**

## unix_socket_directory

**Parameter description**: Specifies the Unix domain socket directory that the MogDB server listens to for connections from the client.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

The parameter length limit varies by OS. If the length is exceeded, the error "Unix-domain socket path xxx is too long" will be reported.

**Value range**: a string

**Default value**: empty. The actual value is specified by the configuration file during installation.

## unix_socket_group

**Parameter description**: Specifies the group of the Unix domain socket (the user of a socket is the user that starts the server). This parameter can work with **[unix_socket_permissions](#unix_socket_permissions)** to control socket access.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: a string. If this parameter is set to an empty string, the default group of the current user is used.

**Default value**: an empty string

## unix_socket_permissions

**Parameter description**: Specifies access permissions for the Unix domain socket.

The Unix domain socket uses the usual permission set of the Unix file system. The value of this parameter should be a number (acceptable for the **chmod** and **umask** commands). If a user-defined octal format is used, the number must start with 0.

You are advised to set it to **0770** (only allowing access from users connecting to the database and users in the same group as them) or **0700** (only allowing access from users connecting to the database).

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: 0000 to 0777

**Default value**: 0777

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In the Linux OS, a document has one document attribute and nine permission attributes, which consists of the read (r), write (w), and execute (x) permissions of the Owner, Group, and Others groups. The r, w, and x permissions are represented by the following numbers:
>
> r: 4
>
> w: 2
>
> x: 1
>
> -: 0
>
> The three attributes in a group are accumulative.
>
> For example, **-rwxrwx-** indicates the following permissions:
>
> owner = rwx = 4+2+1 = 7
>
> group = rwx = 4+2+1 = 7
>
> others = - = 0+0+0 = 0
>
> The permission of the file is 0770.

## application_name

**Parameter description**: Specifies the client name used in the current connection request.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range:** a string

**Default value**: empty (The actual value is the name of the application connected to the backend.)

## connection_info

**Parameter description**: Specifies the database connection information, including the driver type, driver version, driver deployment path, and process owner.

This parameter is a USERSET parameter used for O&M. You are advised not to change the parameter value.

**Value range:** a string

**Default value**: empty

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - An empty string indicates that the driver connected to the database does not support automatic setting of the **connection_info** parameter or the parameter is not set by users in applications.
> - The following is an example of the concatenated value of **connection_info**:
>
> ```
> {"driver_name":"ODBC","driver_version": "(MogDB 2.1.1 build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr","driver_path":"/usr/local/lib/psqlodbcw.so","os_user":"omm"}
> ```
>
> **driver_name** and **driver_version** are displayed by default. Whether **driver_path** and **os_user** are displayed is determined by users.
