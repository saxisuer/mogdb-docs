---
title: Backup and Restoration
summary: Backup and Restoration
author: Zhang Cuiping
date: 2021-11-08
---

# Backup and Restoration

## operation_mode

**Parameter description**: Specifies whether the system enters the backup and restoration mode.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the system is in the backup and restoration mode.
- **off** indicates that the system is not in the backup and restoration mode.

**Default value**: **off**

## enable_cbm_tracking

**Parameter description:** This parameter must be enabled when Roach is used to perform full and incremental backups. If this parameter is disabled, the backup will fail.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on**: The cbm tracking is enabled.
- **off**: The cbm tracking is disabled.

**Default value**: **off**

## hadr_max_size_for_xlog_receiver

**Parameter description**: Specifies the maximum difference between the OBS logs obtained by instances in the DR database instance and the local playback logs. If the difference is greater than the value of this parameter, the instances stop obtaining OBS logs.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Handling suggestion**: The value of this parameter is related to the local disk size. You are advised to set this parameter to 50% of the local disk size.

**Value range**: an integer ranging from 0 to 2147483647

**Default value**: **256 GB**
