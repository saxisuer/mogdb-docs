---
title: Primary Server
summary: Primary Server
author: Zhang Cuiping
date: 2021-04-20
---

# Primary Server

## synchronous_standby_names

**Parameter description**: Specifies a comma-separated list of names of potential standby servers that support synchronous replication.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - The current synchronous standby server is on the top of the list. If the current synchronous standby server is disconnected, it will be replaced immediately with the next-highest-priority standby server. Name of the next-highest-priority standby server is added to the list.
> - The standby server name can be specified by setting the environment variable **PGAPPNAME**.

**Value range**: a string. If this parameter is set to *****, the name of any standby server that provides synchronous replication is matched. The value can be configured in the following format:

- ANY *num_sync* (*standby_name* [, …])

- [FIRST] *num_sync* (*standby_name* [, …])

- *standby_name* [, …]

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - In the preceding command, *num_sync* indicates the number of standby nodes that need to wait for responses from the transaction, *standby_name* indicates the name of the standby node, and FIRST and ANY specify the policies for selecting standby nodes for synchronous replication from the listed servers.
  > - **ANY N (dn_instanceId1, dn_instanceId2,…)** indicates that any *N* host names in the brackets are selected as the name list of standby nodes for synchronous replication. For example, ANY 1(dn_instanceId1, dn_instanceId2) indicates that any one of dn_instanceId1 and dn_instanceId2 is used as the standby node for synchronous replication.
  > - FIRST N (dn_instanceId1, dn_instanceId2, …) indicates that the first N primary node names in the brackets are selected as the standby node name list for synchronous replication based on the priority. For example, FIRST 1 (dn_instanceId1, dn_instanceId2) indicates that dn_instanceId1 is selected as the standby node for synchronous replication. - The meanings of dn_instanceId1, dn_instanceId2, … are the same as those of FIRST 1 (dn_instanceId1, dn_instanceId2, …).

If you use the gs_guc tool to set this parameter, perform the following operations:

```
gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY NODE 1(dn_instanceId1, dn_instanceId2)'";
```

or

```
gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY 1(AZ1, AZ2)'";
```

**Default value**: *****

## most_available_sync

**Parameter description**: Specifies whether to block the primary server when the primary-standby synchronization fails.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the primary server is not blocked when the synchronization fails.
- **off** indicates that the primary server is blocked when the synchronization fails.

**Default value**: **off**

## enable_stream_replication

**Parameter description**: Specifies whether data and logs are synchronized between primary and standby servers, and between primary and secondary servers.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - This parameter is used for performance testing in scenarios where data synchronization to standby server is enabled and where it is disabled. If this parameter is set to **off**, tests on abnormal scenarios, such as switchover and faults, cannot be performed to prevent inconsistency between the primary, standby, and secondary servers.
> - This parameter is a restricted parameter, and you are advised not to set it to **off** in normal service scenarios.

**Value range**: Boolean

- **on** indicates that data and log synchronization is enabled.
- **off** indicates that data and log synchronization is disabled.

**Default value**: **on**

## enable_mix_replication

**Parameter description**: Specifies how WAL files and data are replicated between primary and standby servers, and between primary and secondary servers.

This parameter is an INTERNAL parameter. Its default value is **off** and cannot be modified.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> This parameter cannot be modified in normal service scenarios. That is, the WAL file and data page mixed replication mode is disabled.

**Value range**: Boolean

- **on** indicates that the WAL file and data page mixed replication mode is enabled.
- **off** indicates that the WAL file and data page mixed replication mode is disabled.

**Default value**: **off**

## vacuum_defer_cleanup_age

**Parameter description**: Specifies the number of transactions by which **VACUUM** will defer the cleanup of invalid row-store table records, so that **VACUUM** and **VACUUM FULL** do not clean up deleted tuples immediately.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000. **0** means no delay.

**Default value**: **0**

## data_replicate_buffer_size

**Parameter description**: Specifies the amount of memory used by queues when the sender sends data pages to the receiver. The value of this parameter affects the buffer size used during the replication from the primary server to the standby server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 4096 to 1072693248. The unit is KB.

**Default value**: **16MB** (16448 KB)

## walsender_max_send_size

**Parameter description**: Specifies the size of the WAL or Sender buffers on the primary server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 8 to *INT_MAX*. The unit is KB.

**Default value**: **8MB** (8192 KB)

## enable_data_replicate

**Parameter description**: Specifies how data is synchronized between primary and standby servers when the data is imported to a row-store table.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the primary and standby servers synchronize data using data pages when the data is imported to a row-store table. When **replication_type** is set to **1**, this parameter cannot be set to **on**. If this parameter is set to **on** using the GUC tool, its value will be forcibly changed to **off**.
- **off** indicates that the primary and standby servers synchronize data using Xlogs when the data is imported to a row-store table.

**Default value**: **off**

## ha_module_debug

**Parameter description**: Specifies the replication status log of a specific data block during data replication.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the status of each data block is recorded in logs during data replication.
- **off** indicates that the status of each data block is not recorded in logs during data replication.

**Default value**: **off**

## enable_incremental_catchup

**Parameter description**: Specifies the data catchup mode between the primary and standby servers.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: Boolean

- **on** indicates that the standby server uses the incremental catchup mode. That is, the standby server scans local data files on the standby server to obtain the list of differential data files between the primary and standby servers and then performs catchup between the primary and standby servers.
- **off** indicates that the standby server uses the full catchup mode. That is, the standby server scans all local data files on the primary server to obtain the list of differential data files between the primary and standby servers and performs catchup between the primary and standby servers.

**Default value**: **on**

## wait_dummy_time

**Parameter description**: Specifies the maximum duration for the primary server to wait for the standby and secondary servers to start and send the scanning lists when incremental data catchup is enabled in MogDB.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from 1 to *INT_MAX*. The unit is second.

**Default value**: **300**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> The unit can only be second.

## catchup2normal_wait_time

**Parameter description**: Specifies the maximum duration that the primary server is blocked during the data catchup on the standby server in the case of a single synchronous standby server.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: an integer ranging from -1 to 10000. The unit is ms.

- The value **-1** indicates that the primary server is blocked until the data catchup on the standby server is complete.
- The value **0** indicates that the primary server is not blocked during the data catchup on the standby server.
- Other values indicate the maximum duration that the primary server is blocked during the data catchup on the standby server. For example, if this parameter is set to **5000**, the primary server is blocked until the data catchup on the standby server is complete in 5s.

**Default value**: **-1**

## sync_config_strategy

**Parameter description**: Specifies the strategy for synchronizing configuration files between the primary and standby servers, and the standby and cascaded standby servers.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](../../../reference-guide/guc-parameters/30-appendix.md).

**Value range**: enumerated values

- **all_node**: If this parameter is set to **all_node** on a primary server, configuration files on the primary server can be automatically synchronized to all standby servers. If this parameter is set to **all_node** on a standby server, the current standby server is allowed to send a synchronization request to the primary server and configuration files on the current standby server can be automatically synchronized to all cascaded standby servers. If this parameter is set to **all_node** on a cascaded standby server, the current cascaded standby server is allowed to send a synchronization request to its standby server.
- **only_sync_node**: If this parameter is set to **only_sync_node** on a primary server, configuration files on the primary server can be automatically synchronized to only all standby servers. If this parameter is set to **only_sync_node** on a standby server, the current standby server is allowed to send a synchronization request to the primary server and configuration files on the current standby server cannot be automatically synchronized to all cascaded standby servers. If this parameter is set to **only_sync_node** on a cascaded standby server, the current cascaded standby server is allowed to send a synchronization request to its standby server.
- **none_node**: If this parameter is set to **none_node** on a primary server, configuration files on the primary server cannot be automatically synchronized to only all standby servers. If this parameter is set to **none_node** on a standby server, the current standby server is not allowed to send a synchronization request to the primary server and configuration files on the current standby server cannot be automatically synchronized to all cascaded standby servers. If this parameter is set to **none_node** on a cascaded standby server, the current cascaded standby server is not allowed to send a synchronization request to its standby server.

**Default value**: **all_node**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> That the sender initiatively synchronizes the configuration file to the receiver and the receiver requests the sender to synchronize the configuration file is two separate events. Both of them will trigger configuration file synchronization. If you do not want to synchronize the configuration file, you need to configure the receiver as **none_node**. If the sender is a standby server, it can only be configured as **none_node**. If the sender is a primary server and configured as **none_node**, it will not synchronize with all the standby servers. If the sender is a primary server and configured as **only_sync_node**, it will synchronize with the standby servers and not synchronize with the asynchronous standby servers.
