---
title: Thread Pool
summary: Thread Pool
author: Zhang Cuiping
date: 2021-06-15
---

# Thread Pool

## enable_thread_pool

**Parameter description**: Specifies whether to enable the thread pool function. This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the thread pool function is enabled.
- **off** indicates that the thread pool function is disabled.

**Default value**: off

## thread_pool_attr

**Parameter description**: Specifies the detailed attributes of the thread pool function. This parameter is valid only when **enable_thread_pool** is set to **on**. This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string, consisting of one or more characters.

This parameter consists of three parts: thread_num, group_num, and cpubind_info. The meanings of the three parts are as follows:

- **thread_num** indicates the total number of threads in the thread pool. The value ranges from 0 to 4096. The value **0** indicates that the database automatically configures the number of threads in the thread pool based on the number of CPU cores. If the value is greater than **0**, the number of threads in the thread pool is the same as the value of **thread_num**.
- **group_num** indicates the number of thread groups in the thread pool. The value ranges from 0 to 64. The value **0** indicates that the database automatically configures the number of thread groups in the thread pool based on the number of NUMA groups. If the value is greater than **0**, the number of thread groups in the thread pool is the same as the value of **group_num**.
- **cpubind_info** indicates whether the thread pool is bound to a core. The available configuration modes are as follows:
  1. '(nobind)': The thread is not bound to a core.
  2. '(allbind)': Use all CPU cores that can be queried in the current system to bind threads.
  3. '(nodebind: 1, 2)': Use the CPU cores in NUMA groups 1 and 2 to bind threads.
  4. '(cpubind: 0-30)': Use the CPU cores 0 to 30 to bind threads. This parameter is case-insensitive.

**Default value**: **'16, 2, (nobind)'**
