---
title: Connection Pool Parameters
summary: Connection Pool Parameters
author: Zhang Cuiping
date: 2021-04-20
---

# Connection Pool Parameters

When a connection pool is used to access the database, database connections are established and then stored in the memory as objects during system running. When you need to access the database, no new connection is established. Instead, an existing idle connection is selected from the connection pool. After you finish accessing the database, the database does not disable the connection but puts it back into the connection pool. The connection can be used for the next access request.

## cache_connection

**Parameter description**: Specifies whether to reclaim the connections of a connection pool.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 2 [Methods for setting GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the connections of a connection pool will be reclaimed.
- **off** indicates that the connections of a connection pool will not be reclaimed.

**Default value**: **on**
