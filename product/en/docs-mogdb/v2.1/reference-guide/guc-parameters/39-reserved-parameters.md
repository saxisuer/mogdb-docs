---
title: Reserved Parameters
summary: Reserved Parameters
author: Zhang Cuiping
date: 2021-11-08
---

# Reserved Parameters

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The following parameters are reserved and do not take effect in this version.

acce_min_datasize_per_thread

cstore_insert_mode

enable_constraint_optimization

enable_hadoop_env

enable_hdfs_predicate_pushdown

enable_orc_cache

schedule_splits_threshold

backend_version