---
title: Rollback Parameters
summary: Rollback Parameters
author: Zhang Cuiping
date: 2021-11-08
---

# Rollback Parameters

## max_undo_workers

**Parameter description**: Specifies the number of undo worker threads invoked during asynchronous rollback. The parameter setting takes effect after the system is restarted.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 1 to 100

**Default value**: **5**
