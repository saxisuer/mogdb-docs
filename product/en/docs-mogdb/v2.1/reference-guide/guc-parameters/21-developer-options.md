---
title: Developer Options
summary: Developer Options
author: Zhang Cuiping
date: 2021-04-20
---

# Developer Options

## allow_system_table_mods

**Parameter description**: Specifies whether the structures of system tables can be modified.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

It is not recommended to modify the default value of this parameter. If it is set to on, the system tables may be damaged and the database may not start.

**Value range**: Boolean

- **on** indicates that the structures of system tables can be modified.
- **off** indicates that the structures of system tables cannot be modified.

**Default value**: **off**

## debug_assertions

**Parameter description:** Specifies whether to enable various assertion checks. This parameter assists in debugging. If you are experiencing strange problems or crashes, set this parameter to **on** to identify programming defects. To use this parameter, the macro USE_ASSERT_CHECKING must be defined (through the configure option **-enable-cassert**) during the MogDB compilation.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that various assertion checks are enabled.
- **off** indicates that various assertion checks are disabled.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This parameter is set to **on** by default if MogDB is compiled with various assertion checks enabled.

**Default value**: **off**

## ignore_checksum_failure

**Parameter description**: Specifies whether to ignore check failures (but still generates an alarm) and continues reading data. Continuing reading data may result in breakdown, damaged data being transferred or hidden, failure of data recovery from remote nodes, or other serious problems. You are not advised to modify the settings.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that data check errors are ignored.
- **off** indicates that data check errors are reported.

**Default value**: **off**

## ignore_system_indexes

**Parameter description**: Specifies whether to ignore system indexes when reading system tables (but still update the indexes when modifying the tables).

This parameter is a BACKEND parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** This parameter is useful for recovering data from tables whose system indexes are damaged.

**Value range**: Boolean

- **on** indicates that system indexes are ignored.
- **off** indicates that system indexes are not ignored.

**Default value**: **off**

## post_auth_delay

**Parameter description**: Specifies the delay in the connection to the server after a successful authentication. Developers can attach a debugger to the server startup process.

This parameter is a BACKEND parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range:** an integer ranging from 0 to 2147. The unit is s.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This parameter is used only for commissioning and fault locating. To prevent impact on service running, ensure that the default value **0** is used in the production environment. If this parameter is set to a value other than 0, the cluster status may be abnormal due to a long authentication delay.

## pre_auth_delay

**Parameter description**: Specifies the period of delaying authentication after the connection to the server is started. Developers can attach a debugger to the authentication procedure.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 60. The unit is s.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This parameter is used only for commissioning and fault locating. To prevent impact on service running, ensure that the default value 0 is used in the production environment. If this parameter is set to a value other than 0, the cluster status may be abnormal due to a long authentication delay.

## trace_notify

**Parameter description**: Specifies whether to enable the function of generating debugging output for the **LISTEN** and **NOTIFY** commands. The level of client_min_messages or log_min_messages must be **debug1** or lower so that debugging output can be recorded in the client or server logs, respectively.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the function is enabled.
- **off** indicates that the function is disabled.

**Default value**: **off**

## trace_recovery_messages

**Parameter description**: Specifies whether to enable logging of recovery-related debugging output. This parameter allows users to overwrite the normal setting of log_min_messages, but only for specific messages. This is intended for the use in debugging the standby server.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated values. Valid values include **debug5**, **debug4**, **debug3**, **debug2**, **debug1**, and **log**. For details about the parameter values, see log_min_messages.

**Default value**: **log**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - **log** indicates that recovery-related debugging information will not be logged.
> - Except the default value **log**, each of the other values indicates that recovery-related debugging information at the specified level will also be logged. Common settings of **log_min_messages** enables logs to be unconditionally recorded into server logs.

## trace_sort

**Parameter description**: Specifies whether to print information about resource usage during sorting operations. This parameter is available only when the macro TRACE_SORT is defined during the MogDB compilation. However, TRACE_SORT is currently defined by default.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the function is enabled.
- **off** indicates that the function is disabled.

**Default value**: **off**

## zero_damaged_pages

**Parameter description**: Specifies whether to detect a damaged page header that causes MogDB to report an error, aborting the current transaction.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

Setting this parameter to **on** causes the system to report a warning, zero out the damaged page, and continue processing. This behavior will destroy data, including all the rows on the damaged page. However, it allows you to bypass the error and retrieve rows from any undamaged pages that may be present in the table. Therefore, it is useful for restoring data if corruption has occurred due to a hardware or software error. In most cases, you are advised not to set this parameter to **on** if you want to restore data from damaged pages.

**Default value**: **off**

## remotetype

**Parameter description**: Specifies the remote connection type.

This parameter cannot be changed.

**Value range**: enumerated values. Valid values are **application**, **datanode**, and **internaltool**.

**Default value**: **application**

## max_user_defined_exception

**Parameter description**: Specifies the maximum number of exceptions. The default value cannot be changed.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

Value range: an integer. Currently, only the fixed value **1000** is supported.

**Default value**: **1000**

## enable_fast_numeric

**Parameter description**: Specifies whether to enable optimization for numeric data calculation. Calculation of numeric data is time-consuming. Numeric data is converted into int64- or int128-type data to improve numeric data calculation performance.

This parameter is a SUSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** or **true** indicates that optimization for numeric data calculation is enabled.
- **off** or **false** indicates that optimization for numeric data calculation is disabled.

**Default value**: **on**

## enable_compress_spill

**Parameter description**: Specifies whether to enable the compression function of writing data to disk.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** or **true** indicates that optimization for writing data to disk is enabled.
- **off** or **false** indicates that optimization for writing data to a disk is disabled.

**Default value**: **on**

## resource_track_log

**Parameter description**: Specifies the log level of self-diagnosis. Currently, this parameter takes effect only in multi-column statistics.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string

- **summary**: Brief diagnosis information is displayed.
- **detail**: Detailed diagnosis information is displayed.

Currently, the two parameter values differ only when there is an alarm about multi-column statistics not collected. If the parameter is set to **summary**, such an alarm will not be displayed. If it is set to **detail**, such an alarm will be displayed.

**Default value**: **summary**

## show_acce_estimate_detail

**Parameter description**: The evaluation information is generally used by O&M personnel during maintenance, and it may affect the output display of the **EXPLAIN** statement. Therefore, this parameter is disabled by default. The evaluation information is displayed only if the **verbose** option of the **EXPLAIN** statement is enabled.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the evaluation information is displayed in the output of the **EXPLAIN** statement.
- **off** indicates that the evaluation information is not displayed in the output of the **EXPLAIN** statement.

**Default value**: **off**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The current version does not support database acceleration. Therefore, this parameter does not take effect after being set.

## support_batch_bind

**Parameter description**: Specifies whether to batch bind and execute PBE statements through interfaces such as JDBC, ODBC, and libpq.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that batch binding and execution are used.
- **off** indicates that batch binding and execution are not used.

**Default value**: **on**

## numa_distribute_mode

**Parameter description**: Specifies the distribution of some shared data and threads among NUMA nodes. This parameter is used to optimize the performance of large-scale ARM servers with multiple NUMA nodes. Generally, you do not need to set this parameter.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: a string. The valid values are **none** and **all**.

- **none** indicates that this function is disabled.
- **all** indicates that some shared data and threads are distributed to different NUMA nodes to reduce the number of remote access times and improve performance. Currently, this function applies only to ARM servers with multiple NUMA nodes. All NUMA nodes must be available for database processes. You cannot select only some NUMA nodes.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** In the current version, **numa_distribute_mode** cannot be set to **all** on the x86 platform.

**Default value**: **none**

## log_pagewriter

**Parameter description**: Specifies whether to display the page refresh information of a thread and details about an incremental check point after the incremental check point is enabled. You are not advised to set this parameter to **true** because a large amount of information will be generated.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **on**

## advance_xlog_file_num

**Parameter description**: Specifies the number of Xlog files that are periodically initialized in advance in the background. This parameter is used to prevent the Xlog file initialization from affecting the performance during transaction submission. However, such a fault may occur only when the system is overloaded. Therefore, you do not need to set this parameter.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000. The value **0** indicates that initialization is not performed in advance. For example, the value **10** indicates that the background thread periodically initializes 10 Xlog files in advance based on the write location of the current xlog.

**Default value**: **0**

## enable_beta_opfusion

**Parameter description**: Specifies whether to accelerate the execution of SQL statements, such as aggregate functions and sorting in TPC-C when **enable_opfusion** is set to **on**.

This parameter is a USERSET parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the functions are enabled.
- **off** indicates that the function are disabled.

**Default value**: **off**

## string_hash_compatible

**Parameter description**: Specifies whether to use the same method to calculate char-type hash values and varchar- or text-type hash values. Based on the setting of this parameter, you can determine whether a redistribution is required when a distribution column is converted from a char-type data distribution into a varchar- or text-type data distribution.

This parameter is a **POSTMASTER** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

- **on** indicates that the same calculation method is used and a redistribution is not required.
- **off** indicates that different calculation methods are used and a redistribution is required.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** Calculation methods differ in the length of input strings used for calculating hash values. (For a char-type hash value, spaces following a string are not counted as the length. For a text- or varchar-type hash value, the spaces are counted.) The hash value affects the calculation result of queries. To avoid query errors, do not modify this parameter during database running once it is set.

**Default value**: **off**
