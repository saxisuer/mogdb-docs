---
title: Flashback
summary: Flashback
author: Zhang Cuiping
date: 2021-11-08
---

# Flashback

This section describes parameters related to the flashback function.

## enable_recyclebin

**Parameter description**: Specifies whether the recycle bin is enabled or disabled in real time.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: Boolean

**Default value**: **off**

## timecapsule_mode

**Parameter description**: Specifies whether to enable the flashback function.

This parameter is a **SIGHUP** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: enumerated values

- **none** indicates that the flashback function is disabled.
- **heap** indicates that only the flashback function of the Heap engine is enabled.
- **ustore** indicates that only the flashback function of the UStore engine is enabled.
- **all** indicates that the flashback function is enabled for both the Heap and UStore engines.

**Default value**: **none**

## recyclebin_retention_time

**Parameter description**: Specifies the retention period of objects in the recycle bin. The objects will be automatically deleted after the retention period expires.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 1 to 2147483647. The unit is s.

Default value: **15 min** (900s)

## version_retention_age

**Parameter description**: Specifies the number of transactions retained in the old version. If the number of transactions exceeds the value of this parameter, the old version will be recycled and cleared.

This parameter is a **SIGHUP** parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 576460752303423487. **0** means no delay.

**Default value**: **0**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** System catalogs use the Heap engine. To prevent unnecessary system catalog bloating (for example, only the Uheap flashback function is used in services), you need to set **version_retention_age** and **vacuum_defer_cleanup_age** to the same value so that the old version can be retained.

## vacuum_defer_cleanup_age

**Parameter description**: Specifies the number of transactions by which **VACUUM** will defer the cleanup of invalid row-store table records, so that **VACUUM** and **VACUUM FULL** do not clean up deleted tuples immediately. You can also set this parameter to configure the retention period of the flashback function in the old version.

This parameter is a SIGHUP parameter. Set it based on instructions provided in Table 1 [GUC parameters](30-appendix.md).

**Value range**: an integer ranging from 0 to 1000000. **0** means no delay. The value range needs to be extended to 100 million.

**Default value**: **0**
