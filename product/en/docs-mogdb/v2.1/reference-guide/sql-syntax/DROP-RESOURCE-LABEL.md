---
title: DROP RESOURCE LABEL
summary: DROP RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# DROP RESOURCE LABEL

## Function

**DROP RESOURCE LABEL** deletes a resource label.

## Precautions

Only user **poladmin**, user **sysadmin**, or the initial user can perform this operation.

## Syntax

```ebnf+diagram
DropResourceLabel ::= DROP RESOURCE LABEL [IF EXISTS] policy_name[, ...]*;
```

## Parameter Description

**label_name**

Specifies the resource label name.

Value range: a string. It must comply with the naming convention.

## Examples

```sql
-- Delete a resource label.
mogdb=# DROP RESOURCE LABEL IF EXISTS res_label1;

-- Delete a group resource label.
mogdb=# DROP RESOURCE LABEL IF EXISTS res_label1, res_label2, res_label3;
```

## Helpful Links

[ALTER RESOURCE LABEL](ALTER-RESOURCE-LABEL.md) and [CREATE RESOURCE LABEL](CREATE-RESOURCE-LABEL.md)
