---
title: DROP TEXT SEARCH CONFIGURATION
summary: DROP TEXT SEARCH CONFIGURATION
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TEXT SEARCH CONFIGURATION

## Function

**DROP TEXT SEARCH CONFIGURATION** deletes a text search configuration.

## Precautions

Only the owner of a text search configuration has the **DROP TEXT SEARCH CONFIGURATION** permission.

## Syntax

```ebnf+diagram
DropTextSearchConfiguration ::= DROP TEXT SEARCH CONFIGURATION [ IF EXISTS ] name [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified text search configuration does not exist.

- **name**

  Specifies the name (optionally schema-qualified) of the text search configuration to be deleted.

- **CASCADE**

  Automatically deletes the objects that depend on the text search configuration.

- **RESTRICT**

  Refuses to delete the text search configuration if any objects depend on it. This is the default action.

## Examples

See **Examples** in **CREATE TEXT SEARCH CONFIGURATION**.

## Helpful Links

[ALTER TEXT SEARCH CONFIGURATION](ALTER-TEXT-SEARCH-CONFIGURATION.md) and [CREATE TEXT SEARCH CONFIGURATION](CREATE-TEXT-SEARCH-CONFIGURATION.md)
