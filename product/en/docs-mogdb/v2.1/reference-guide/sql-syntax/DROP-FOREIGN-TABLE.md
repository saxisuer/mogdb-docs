---
title: DROP FOREIGN TABLE
summary: DROP FOREIGN TABLE
author: Zhang Cuiping
date: 2021-05-10
---

# DROP FOREIGN TABLE

## Function

**DROP FOREIGN TABLE** drops a foreign table.

## Precautions

**DROP FOREIGN TABLE** forcibly drops the specified table and the indexes depending on the table. After the table is dropped, the functions and stored procedures that need to use this table cannot be executed.

## Syntax

```ebnf+diagram
DropForeignTable ::= DROP FOREIGN TABLE [ IF EXISTS ]
    table_name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the specified table does not exist.

- **table_name**

  Table name

  Value range: an existing table name

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically drops the objects (such as views) that depend on the table.
  - **RESTRICT**: refuses to drop the table if any objects depend on it. This is the default action.

## Helpful Links

[ALTER FOREIGN TABLE](ALTER-FOREIGN-TABLE.md) and [CREATE FOREIGN TABLE](CREATE-FOREIGN-TABLE.md)
