---
title: DROP WEAK PASSWORD DICTIONARY
summary: DROP WEAK PASSWORD DICTIONARY
author: Zhang Cuiping
date: 2021-11-01
---

# DROP WEAK PASSWORD DICTIONARY

## Function

**DROP WEAK PASSWORD DICTIONARY** clears all weak passwords in **gs_global_config**.

## Precautions

Only the initial user, system administrator, and security administrator have the permission to execute this syntax.

## Syntax

```ebnf+diagram
DropWeakPasswordDictionary ::= DROP WEAK PASSWORD DICTIONARY;
```

## Parameter Description

None

## Examples

See the examples in **CREATE WEAK PASSWORD DICTIONARY**.

## Helpful Links

[CREATE WEAK PASSWORD DICTIONARY](CREATE-WEAK-PASSWORD-DICTIONARY.md)