---
title: ALTER DIRECTORY
summary: ALTER DIRECTORY
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER DIRECTORY

## Function

**ALTER DIRECTORY** modifies a directory.

## Precautions

- Currently, only the directory owner can be changed.
- When enable_access_server_directory=off, only the initial user is allowed to modify the owner of the directory; when enable_access_server_directory=on, users with SYSADMIN authority and the owner of the directory object can modify the directory, and the user is required to be a member of the new owner.

## Syntax

```ebnf+diagram
AlterDirectory ::= ALTER DIRECTORY directory_name
    OWNER TO new_owner;
```

## Parameter Description

**directory_name**

Specifies the name of a directory to be modified. The value must be an existing directory name.

## Examples

```sql
-- Create a directory.
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

-- Change the owner of the directory.
mogdb=# ALTER DIRECTORY dir OWNER TO system;

-- Delete a directory.
mogdb=# DROP DIRECTORY dir;
```

## Helpful Links

[CREATE DIRECTORY](CREATE-DIRECTORY.md)，[DROP DIRECTORY](DROP-DIRECTORY.md)
