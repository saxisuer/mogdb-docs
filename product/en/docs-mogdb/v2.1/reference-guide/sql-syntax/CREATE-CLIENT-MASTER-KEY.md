---
title: CREATE CLIENT MASTER KEY
summary: CREATE CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE CLIENT MASTER KEY

## Function

**CREATE CLIENT MASTER KEY** creates a CMK object that can be used to encrypt a CEK object.

## Precautions

This syntax is specific to a fully-encrypted database.

When using **gsql** to connect to a database server, you need to use the **-C** parameter to enable the fully-encrypted database.

In the CMK object created using this syntax, only the method for reading keys from independent key management tools, services, or components is stored. The key itself is not stored.

## Syntax

```ebnf+diagram
CreateClientMasterKey ::= CREATE CLIENT MASTER KEY client_master_key_name WITH (KEY_STORE = key_store_name, KEY_PATH = "key_path_value", ALGORITHM = algorithm_type)
```

## Parameter Description

- **client_master_key_name**

  This parameter is used as the name of a key object. In the same namespace, the value of this parameter must be unique.

  Value range: a string. It must comply with the identifier naming convention.

- **KEY_STORE**

  Specifies the key tool or component that manages CMKs. Currently, only **localkms** is supported.

- **KEY_PATH**

  **KEY_STORE** manages multiple CMKs. The **KEY_PATH** option is used to uniquely identify a CMK in **KEY_STORE**. The value is similar to that of **key_path_value**.

- **ALGORITHM**

  Type of the encryption algorithm used to encrypt CEKs. Value range: **RSA_2048**, **RSA3072**, and **SM2**.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** **Key storage path**: By default, **localkms** generates, reads, or deletes a key file in the `$LOCALKMS_FILE_PATH` path. You can manually configure this environment variable. However, you do not need to configure this environment variable separately. When failing to obtain `$LOCALKMS_FILE_PATH`, **localkms** attempts to obtain the *$GAUSSHOME***/etc/localkms/** path. If the path exists, it is used as the key storage path. **Key-related file name**: When the **CREATE CMK** syntax is used, **localkms** creates four files related to key paths. For example, when **KEY_PATH** is set to **key_path_value**, the names of the four files are **key_path_value.pub**, **key_path_value.pub.rand**, **key_path_value.priv**, and **key_path_value.priv.rand**. Therefore, to successfully create key-related files, ensure that no file with the same name as the key-related files exists in the key path.

## Examples

```sql
-- (1) Use the common account alice to connect to the fully-encrypted database.
[cmd] gsql -U alice -h $host -p $port -d $database -C -r

-- (2) Use this syntax to create a CMK object.
mogdb=> CREATE CLIENT MASTER KEY a_cmk WITH (KEY_STORE = localkms, KEY_PATH = "key_path_value", ALGORITHM = RSA_2048);
mogdb=> CREATE CLIENT MASTER KEY another_cmk WITH (KEY_STORE = localkms, KEY_PATH = "another_path_value", ALGORITHM = SM2);
```
