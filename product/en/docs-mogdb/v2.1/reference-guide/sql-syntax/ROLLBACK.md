---
title: ROLLBACK
summary: ROLLBACK
author: Zhang Cuiping
date: 2021-05-18
---

# ROLLBACK

## Function

**ROLLBACK** rolls back the current transaction and backs out all updates in the transaction.

**ROLLBACK** backs out of all changes that a transaction makes to a database if the transaction fails to be executed due to a fault.

## Precautions

If a **ROLLBACK** statement is executed out of a transaction, no error occurs, but a notice is displayed.

## Syntax

```ebnf+diagram
Rollback ::= ROLLBACK [ WORK | TRANSACTION ];
```

## Parameter Description

**WORK | TRANSACTION**

Specifies the optional keyword. that more clearly illustrates the syntax.

## Example

```sql
-- Start a transaction.
mogdb=# START TRANSACTION;

-- Back out all changes.
mogdb=# ROLLBACK;
```

## Helpful Links

[COMMIT | END](COMMIT-END.md)
