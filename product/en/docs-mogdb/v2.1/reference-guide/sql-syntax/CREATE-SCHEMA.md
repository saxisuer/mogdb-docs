---
title: CREATE SCHEMA
summary: CREATE SCHEMA
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SCHEMA

## Function

**CREATE SCHEMA** creates a schema.

Named objects are accessed either by "qualifying" their names with the schema name as a prefix, or by setting a search path that includes the desired schema. When creating named objects, you can also use the schema name as a prefix.

Optionally, **CREATE SCHEMA** can include sub-commands to create objects within the new schema. The sub-commands are treated essentially the same as separate commands issued after creating the schema. If the **AUTHORIZATION** clause is used, all the created objects are owned by this user.

## Precautions

- Only a user with the **CREATE** permission on the current database can perform this operation.
- The owner of an object created by a system administrator in a schema with the same name as a common user is the common user, not the system administrator.

## Syntax

- Create a schema based on a specified name.

  ```ebnf+diagram
  CreateSchema ::= CREATE SCHEMA schema_name
      [ AUTHORIZATION user_name ] [WITH BLOCKCHAIN] [ schema_element [ ... ] ];
  ```

- Create a schema based on a username.

  ```ebnf+diagram
  CreateSchema ::= CREATE SCHEMA AUTHORIZATION user_name [ schema_element [ ... ] ];
  ```

## Parameter Description

- **schema_name**

  Specifies the schema name.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  > The name must be unique.
  > The schema name cannot start with **pg_**.

  Value range: a string. It must comply with the naming convention rule.

- **AUTHORIZATION user_name**

  Specifies the owner of a schema. If **schema_name** is not specified, **user_name** will be used as the schema name. In this case, **user_name** can only be a role name.

  Value range: an existing username or role name

- **WITH BLOCKCHAIN**

    Specifies the tamper-proof attribute of a schema. In this mode, a row-store common user table is automatically extended to tamper-proof user table.

- **schema_element**

  Specifies an SQL statement defining an object to be created within the schema. Currently, only the  **CREATE TABLE**,  **CREATE VIEW**,  **CREATE INDEX**,  **CREATE PARTITION**,  **CREATE SEQUENCE**,  **CREATE TRIGGER**  and  **GRANT**  clauses are supported.

  Objects created by sub-commands are owned by the user specified by **AUTHORIZATION**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> If objects in the schema on the current search path are with the same name, specify the schemas for different objects. You can run **SHOW SEARCH_PATH** to check the schemas on the current search path.

## Examples

```sql
-- Create the role1 role.
mogdb=# CREATE ROLE role1 IDENTIFIED BY 'xxxxxxxxx';

-- Create a schema named role1 for the role1 role. The owner of the films and winners tables created by the clause is role1.
mogdb=# CREATE SCHEMA AUTHORIZATION role1
     CREATE TABLE films (title text, release date, awards text[])
     CREATE VIEW winners AS
     SELECT title, release FROM films WHERE awards IS NOT NULL;

-- Delete the schema.
mogdb=# DROP SCHEMA role1 CASCADE;
-- Delete the user.
mogdb=# DROP USER role1 CASCADE;
```

## Helpful Links

[ALTER SCHEMA](ALTER-SCHEMA.md)，[DROP SCHEMA](DROP-SCHEMA.md)
