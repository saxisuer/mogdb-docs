---
title: ALTER MASKING POLICY
summary: ALTER MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER MASKING POLICY

## Function

**ALTER MASKING POLICY** modifies masking policies.

## Precautions

- Only users with the  **poladmin**  or  **sysadmin**  permission, or the initial user can perform this operation.
- The masking policy takes effect only after **enable_security_policy** is set to **on**.

## Syntax

- Modify the policy description.

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name COMMENTS policy_comments;
  ```

- Modify the masking method.

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name [ADD | REMOVE | MODIFY] masking_actions[, ...]*;
  ```

  The syntax of masking_action.

  ```ebnf+diagram
  masking_action ::= masking_function ON LABEL(label_name[, ...]*)
  ```

- Modify the scenarios where the masking policies take effect.

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name MODIFY(FILTER ON FILTER_TYPE(filter_value[, ...]*)[, ...]*);
  ```

- Removes the filters of the masking policies.

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name DROP FILTER;
  ```

- Enable or disable the masking policies.

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name [ENABLE | DISABLE];
  ```

## Parameter Description

- **policy_name**

  Specifies the masking policy name, which must be unique.

  Value range: a string. It must comply with the naming convention.

- **policy_comments**

  Adds or modifies description of masking policies.

- **masking_function**

  Specifies eight preset masking methods or user-defined functions. Schemas are supported.

  **maskall**  is not a preset function. It is hard-coded and cannot be displayed by running  **\\df**.

  The masking methods during presetting are as follows:

  ```bash
  maskall | randommasking | creditcardmasking | basicemailmasking | fullemailmasking | shufflemasking | alldigitsmasking | regexpmasking
  ```

- **label_name**

  Specifies the resource label name.

- **FILTER_TYPE**

  Specifies the types of information to be filtered by the policies: **IP**, **ROLES**, and **APP**.

- **filter_value**

  Indicates the detailed information to be filtered, such as the IP address, app name, and username.

- **ENABLE|DISABLE**

  Enables or disables the masking policy. If **ENABLE|DISABLE** is not specified, **ENABLE** is used by default.

## Examples

```sql
-- Create users dev_mask and bob_mask.
mogdb=# CREATE USER dev_mask PASSWORD 'dev@1234';
mogdb=# CREATE USER bob_mask PASSWORD 'bob@1234';

-- Create table tb_for_masking.
mogdb=# CREATE TABLE tb_for_masking(col1 text, col2 text, col3 text);

-- Create a resource label for label sensitive column col1.
mogdb=# CREATE RESOURCE LABEL mask_lb1 ADD COLUMN(tb_for_masking.col1);

-- Create a resource label for label sensitive column col2.
mogdb=# CREATE RESOURCE LABEL mask_lb2 ADD COLUMN(tb_for_masking.col2);

-- Create a masking policy for the operation of accessing sensitive column col1.
mogdb=# CREATE MASKING POLICY maskpol1 maskall ON LABEL(mask_lb1);

-- Add description for masking policy maskpol1.
mogdb=# ALTER MASKING POLICY maskpol1 COMMENTS 'masking policy for tb_for_masking.col1';

-- Modify masking policy maskpol1 to add a masking method.
mogdb=# ALTER MASKING POLICY maskpol1 ADD randommasking ON LABEL(mask_lb2);

-- Modify masking policy maskpol1 to remove a masking method.
mogdb=# ALTER MASKING POLICY maskpol1 REMOVE randommasking ON LABEL(mask_lb2);

-- Modify masking policy maskpol1 to modify a masking method.
mogdb=# ALTER MASKING POLICY maskpol1 MODIFY randommasking ON LABEL(mask_lb1);

-- Modify masking policy maskpol1 so that it takes effect only for scenarios where users are dev_mask and bob_mask, client tools are psql and gsql, and the IP addresses are 10.20.30.40 and 127.0.0.0/24.
mogdb=# ALTER MASKING POLICY maskpol1 MODIFY (FILTER ON ROLES(dev_mask, bob_mask), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24'));

-- Modify masking policy maskpol1 so that it takes effect for all user scenarios.
mogdb=# ALTER MASKING POLICY maskpol1 DROP FILTER;

-- Disable masking policy maskpol1.
mogdb=# ALTER MASKING POLICY maskpol1 DISABLE;
```

## Helpful Links

[CREATE MASKING POLICY](CREATE-MASKING-POLICY.md), [DROP MASKING POLICY](DROP-MASKING-POLICY.md).
