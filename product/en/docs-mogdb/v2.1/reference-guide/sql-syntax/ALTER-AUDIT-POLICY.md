---
title: ALTER AUDIT POLICY
summary: ALTER AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER AUDIT POLICY

## Function

**ALTER AUDIT POLICY** modifies the unified audit policy.

## Precautions

- Only users with the  **poladmin**  or  **sysadmin**  permission, or the initial user can perform this operation.
- The unified audit policy takes effect only after **enable_security_policy** is set to **on**.

## Syntax

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ADD | REMOVE } { [ privilege_audit_clause ] [ access_audit_clause ] };
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name MODIFY ( filter_group_clause );
ALTER AUDIT POLICY [ IF EXISTS ] policy_name DROP FILTER;
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name COMMENTS policy_comments;
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ENABLE | DISABLE };
```

- privilege_audit_clause：

  ```ebnf+diagram
  privilege_audit_clause ::= PRIVILEGES { DDL | ALL };
  ```

- access_audit_clause：

  ```ebnf+diagram
  access_audit_clause ::= ACCESS { DML | ALL };
  ```

- filter_group_clause

  ```ebnf+diagram
  filter_group_clause ::= FILTER ON {(FILTER_TYPE (filter_value [, ...])) [, ...]};
  ```

## Parameter Description

- **policy_name**

  Specifies the audit policy name, which must be unique.

  Value range: a string. It must comply with the identifier naming convention.

- **DDL**

  Specifies the operations that are audited in the database: **CREATE**, **ALTER**, **DROP**, **ANALYZE**, **COMMENT**, **GRANT**, **REVOKE**, **SET**, **SHOW**, **LOGIN_ANY**, **LOGIN_FAILURE**, **LOGIN_SUCCESS**, and **LOGOUT**.

- **ALL**

  Specifies all operations supported by the specified DDL statements in the database.

- **DML**

  Specifies the operations that are audited in the database: **SELECT**, **COPY**, **DEALLOCATE**, **DELETE**, **EXECUTE**, **INSERT**, **PREPARE**, **REINDEX**, **TRUNCATE**, and **UPDATE**.

- **FILTER_TYPE**

  Specifies the types of information to be filtered by the policy:  **IP**,  **ROLES**, and  **APP**.

- **filter_value**

  Specifies the detailed information to be filtered.

- **policy_comments**

  Records description information of the audit policy.

- **ENABLE|DISABLE**

  Enables or disables the unified audit policy. If **ENABLE|DISABLE** is not specified, **ENABLE** is used by default.

## Examples

See **Examples** in [CREATE AUDIT POLICY](CREATE-AUDIT-POLICY.md).

## Helpful Links

[CREATE AUDIT POLICY](CREATE-AUDIT-POLICY.md), [DROP AUDIT POLICY](DROP-AUDIT-POLICY.md).
