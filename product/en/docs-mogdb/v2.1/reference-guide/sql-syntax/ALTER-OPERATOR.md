---
title: ALTER OPERATOR
summary: ALTER OPERATOR
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER OPERATOR

## Function

ALTER OPERATOR modifies the definition of an operator.

## Precautions

ALTER OPERATOR changes the definition of an operator. Currently, the only function available is to change the owner of the operator.

To use ALTER OPERATOR, you must be the owner of the operator. To modify the owner, you must also be a direct or indirect member of the new owning role, and that member must have CREATE permission on the operator's schema. (These restrictions force the owner to do nothing that cannot be done by deleting and recreating the operator. However, the super user can modify the ownership of any operator in any way.)

## Syntax

```ebnf+diagram
AlterOperator ::= ALTER OPERATOR name ( { left_type | NONE } , { right_type | NONE } ) OWNER TO new_owner;
```

```ebnf+diagram
AlterOperator ::= ALTER OPERATOR name ( { left_type | NONE } , { right_type | NONE } ) SET SCHEMA new_schema;
```

## Parameter Description

- **name**

  Name of an existing operator.

- **left_type**

  Data type of the left operand for the operator; if there is no left operand, write NONE.

- **right_type**

  Data type of the right operand for the operator; if there is no right operand, write NONE.

- **new_owner**

  New owner of the operator.

- **new_schema**

  New schema name of the operator.

## Example

Change a user-defined operator for text a @@ b:

```sql
ALTER OPERATOR @@ (text, text) OWNER TO joe;
```

## Compatibility

The SQL standard does not contain the ALTER OPERATOR statement.
