---
title: DROP DIRECTORY
summary: DROP DIRECTORY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DIRECTORY

## Function

**DROP Directory** deletes a synonym.

## Precautions

When enable_access_server_directory=off, only the initial user is allowed to delete the directory object; when enable_access_server_directory=on, the user with SYSADMIN permission, the owner of the directory object, the user who has been granted the DROP permission of the directory, or the user who has inherited the built-in role gs_rloe_directory_drop permission The user can delete the directory.

## Syntax

```ebnf+diagram
DropDirectory ::= DROP DIRECTORY [ IF EXISTS ] directory_name;
```

## Parameter Description

- **directory_name**

  Specifies the name of the directory to be deleted.

  Value range: an existing directory name

## Example

```sql
-- Create a directory.
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

-- Delete a directory.
mogdb=# DROP DIRECTORY dir;
```

## Helpful Links

[CREATE DIRECTORY](CREATE-DIRECTORY.md) and [ALTER DIRECTORY](ALTER-DIRECTORY.md)
