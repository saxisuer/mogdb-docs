---
title: SHOW
summary: SHOW
author: Zhang Cuiping
date: 2021-05-18
---

# SHOW

## Function

**SHOW** shows the current value of a run-time parameter.

## Precautions

None

## Syntax

```ebnf+diagram
Show ::= SHOW
  {
    [VARIABLES LIKE] configuration_parameter |
    CURRENT_SCHEMA |
    TIME ZONE |
    TRANSACTION ISOLATION LEVEL |
    SESSION AUTHORIZATION |
    ALL
  };
```

## Parameter Description

See **Parameter Description** in **RESET**.

## Examples

```sql
-- Show the value of timezone.
mogdb=# SHOW timezone;

-- Show all parameters.
mogdb=# SHOW ALL;

-- Show all parameters whose names contain var.
mogdb=# SHOW VARIABLES LIKE var;
```

## Helpful Links

[SET](SET.md) and [RESET](RESET.md)
