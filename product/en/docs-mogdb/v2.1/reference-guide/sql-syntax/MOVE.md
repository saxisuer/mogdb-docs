---
title: MOVE
summary: MOVE
author: Zhang Cuiping
date: 2021-05-18
---

# MOVE

## Function

**MOVE** repositions a cursor without retrieving any data. **MOVE** works exactly like the **FETCH** statement, except it only repositions the cursor and does not return rows.

## Precautions

None

## Syntax

```ebnf+diagram
Move ::= MOVE [ direction [ FROM | IN ] ] cursor_name;
```

The **direction** clause specifies optional parameters.

```ebnf+diagram
direction ::= NEXT
   | PRIOR
   | FIRST
   | LAST
   | ABSOLUTE count
   | RELATIVE count
   | count
   | ALL
   | FORWARD
   | FORWARD count
   | FORWARD ALL
   | BACKWARD
   | BACKWARD count
   | BACKWARD ALL
```

## Parameter Description

The parameters of **MOVE** and **FETCH** are the same. For details, see **Parameter Description** in **FETCH**.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> On successful completion, a **MOVE** statement returns a tag of the form **MOVE count**. The **count** is the number of rows that a **FETCH** statement with the same parameters would have returned (possibly zero).

## Examples

```sql
-- Start a transaction.
mogdb=# START TRANSACTION;

-- Define a cursor cursor1.
mogdb=# CURSOR cursor1 FOR SELECT * FROM tpcds.reason;

-- Skip the first three rows of cursor1.
mogdb=# MOVE FORWARD 3 FROM cursor1;

-- Fetch the first four rows from cursor1.
mogdb=# FETCH 4 FROM cursor1;
 r_reason_sk |   r_reason_id    |                                            r_reason_desc
-------------+------------------+------------------------------------------------------------------------------------------------------
           4 | AAAAAAAAEAAAAAAA | Not the product that was ordred
           5 | AAAAAAAAFAAAAAAA | Parts missing
           6 | AAAAAAAAGAAAAAAA | Does not work with a product that I have
           7 | AAAAAAAAHAAAAAAA | Gift exchange
(4 rows)

-- Close the cursor.
mogdb=# CLOSE cursor1;

-- End the transaction.
mogdb=# END;
```

## Helpful Links

[CLOSE](CLOSE.md) and [FETCH](FETCH.md)
