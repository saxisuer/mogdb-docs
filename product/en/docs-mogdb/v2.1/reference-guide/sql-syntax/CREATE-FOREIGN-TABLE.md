---
title: CREATE FOREIGN TABLE
summary: CREATE FOREIGN TABLE
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE FOREIGN TABLE

## Function

**CREATE FOREIGN TABLE** creates a foreign table.

## Precautions

- System columns (such as  **tableoid**  and  **ctid**) cannot be used in foreign tables.
- Among them, the appearance of Private and Shares mode requires the initial user and operation and maintenance administrator authority in operation mode (operation_mode).

## Syntax

```ebnf+diagram
CreateForeignTable ::= CREATE FOREIGN TABLE [ IF NOT EXISTS ] table_name ( [
    column_name type_name [ OPTIONS ( option 'value') ] [ COLLATE collation ] [ column_constraint [ ... ] ]
    [, ... ]
] )
  SERVER server_name
[ OPTIONS ( option 'value' ) ]
```

The column_constraint can be:

```ebnf+diagram
column_constraint ::= [ CONSTRAINT constraint_name ]
{ NOT NULL |
  NULL |
  DEFAULT default_expr }
```

## Parameter Description

- **IF NOT EXISTS**

  Sends a notice, but does not throw an error, if a table with the same name exists.

- **table_name**

  Specifies the name of a foreign table.

  Value range: a string. It must comply with the naming convention.

- **column_name**

  Specifies the name of a column in the foreign table.

  Value range: a string. It must comply with the naming convention.

- **type_name**

  Specifies the data type of the column.

- **SERVER server_name**

  Specifies the server name of the foreign table. The default value is **mot_server**.

  If you need to create an external table of Oracle, MySQL, PostgreSQL, or File type, you need to create the corresponding server manually first, see [CREATE SERVER](CREATE-SERVER.md) for the statement of creating server. Here the server_name should be the same as the server_name defined in the create server statement.

- **OPTIONS ( option 'value' [, … ] )**

  Options are related to the new foreign table or the columns in the foreign table. The allowed option names and values are specified by each foreign data wrapper, and are also verified by the verification function of the foreign data wrapper. The option name must be unique (although table options and table column options can share the same name).

  - Options supported by **oracle_fdw** are as follows:

    - **table**

        Name of a table on the Oracle server. The value must be the same as the table name recorded in the Oracle system catalog. Generally, the value consists of uppercase letters.

    - **schema**

        Schema (or owner) corresponding to the table. The value must be the same as the table name recorded in the Oracle system catalog. Generally, the value consists of uppercase letters.

  - Options supported by **mysql_fdw** are as follows:

    - **dbname**

        Name of the MySQL database.

    - **table_name**

        Name of a table in the MySQL database.

  - Options supported by **postgres_fdw** are as follows:

    - **schema_name**

        Schema name of a remote server. If this option is not specified, the schema name of the foreign table is used as the schema name of the remote server.

    - **table_name**

        Table name of a remote server. If this option is not specified, the name of the foreign table is used as the table name of the remote server.

    - **column_name**

        Column name of a table on a remote server. If this option is not specified, the column name of the foreign table is used as the column name of a table on a remote server.

  - Options supported by **file_fdw** are as follows:

    - filename

        File to be read. This parameter is mandatory and must be an absolute path.

    - format

        File format of the remote server, which is the same as the **FORMAT** option in the **COPY** statement. The value can be **text**, **csv**, **binary**, or **fixed**.

    - header

        Specifies whether a specified file has a header, which is the same as the **HEADER** option of the **COPY** statement.

    - delimiter

        File delimiter, which is the same as the **DELIMITER** option of the **COPY** statement.

    - quote

        Quote character of a file, which is the same as the **QUOTE** option of the **COPY** statement.

    - escape

        Escape character of a file, which is the same as the **ESCAPE** option of the **COPY** statement.

    - null

        Null string of a file, which is the same as the **NULL** option of the **COPY** statement.

    - encoding

        Encoding of a file, which is the same as the **ENCODING** option of the **COPY** statement.

    - force_not_null

        This is a Boolean option. If it is true, the value of the declared field cannot be an empty string. This option is the same as the **FORCE_NOT_NULL** option of the **COPY** statement.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** For details, please see [file_fdw](../../developer-guide/foreign-data-wrapper/file_fdw.md), [oracle_fdw](../../developer-guide/foreign-data-wrapper/1-oracle_fdw.md), [mysql_fdw](../../developer-guide/foreign-data-wrapper/2-mysql_fdw.md), [postgres_fdw](../../developer-guide/foreign-data-wrapper/3-postgres_fdw.md).

## Helpful Links

[ALTER FOREIGN TABLE](ALTER-FOREIGN-TABLE.md) and [DROP FOREIGN TABLE](DROP-FOREIGN-TABLE.md)
