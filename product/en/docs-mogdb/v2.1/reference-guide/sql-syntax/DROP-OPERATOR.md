---
title: DROP OPERATOR
summary: DROP OPERATOR
author: Zhang Cuiping
date: 2021-06-07
---

# DROP OPERATOR

## Function

DROP OPERATOR deletes an operator.

## Precautions

The DROP OPERATOR statement deletes an existing operator from the database. To execute this command, you must be the owner of the operator.

## Syntax

```ebnf+diagram
DropOperator ::= DROP OPERATOR [ IF EXISTS ] name ( { left_type | NONE } , { right_type | NONE } ) [ CASCADE | RESTRICT ]
```

## Parameter Description

- **IF EXISTS**

  Do not throw an error if the specified operator does not exist. A notice is issued in this case.

- **name**

  Name (optionally schema-qualified) of an existing operator.

- **left_type**

  Type of the left operand for the operator. If no, set this parameter to **NONE**.

- **right_type**

  Type of the right operand for the operator. If no, set this parameter to **NONE**.

- **CASCADE**

  Automatically drops all objects that depend on this operator.

- **RESTRICT**

  Refuses to delete the operator if any objects depend on it. This is a default parameter.

## Example

Delete the power operator a^b used for integer.

```sql
DROP OPERATOR ^ (integer, integer);
```

Delete the left unary bit operator ~b for the bit type.

```sql
DROP OPERATOR ~ (none, bit);
```

Delete the factorial x used for bigint.:

```sql
DROP OPERATOR ! (bigint, none);
```

## Compatibility

The SQL standard does not contain the DROP OPERATOR statement.
