---
title: CREATE WEAK PASSWORD DICTIONARY
summary: CREATE WEAK PASSWORD DICTIONARY
author: Zhang Cuiping
date: 2021-11-01
---

# CREATE WEAK PASSWORD DICTIONARY

## Function

**CREATE WEAK PASSWORD DICTIONARY** inserts one or more weak passwords into the **gs_global_config** table.

## Precautions

- Only the initial user, system administrator, and security administrator have the permission to execute this syntax.
- Passwords in the weak password dictionary are stored in the **gs_global_config** system catalog.
- The weak password dictionary is empty by default. You can use this syntax to add one or more weak passwords.
- When a user attempts to execute this syntax to insert a weak password that already exists in the **gs_global_config** table, only one weak password is retained in the table.

## Syntax

```ebnf+diagram
CreateWeakPasswordDictionary ::= CREATE WEAK PASSWORD DICTIONARY
       [WITH VALUES] ( {'weak_password'} [, ...] );
```

## Parameter Description

weak_password

Specifies a weak password.

Value range: a character string

## Examples

```sql
-- Insert a single weak password into the gs_global_config system catalog.
mogdb=# CREATE WEAK PASSWORD DICTIONARY WITH VALUES ('password1');

-- Insert multiple weak passwords into the gs_global_config system catalog.
mogdb=# CREATE WEAK PASSWORD DICTIONARY WITH VALUES ('password2'),('password3');

-- Clear all weak passwords in the gs_global_config system catalog.
mogdb=# DROP WEAK PASSWORD DICTIONARY;

-- View existing weak passwords.
mogdb=# SELECT * FROM gs_global_config WHERE NAME LIKE 'weak_password';
```

## Helpful Links

[DROP WEAK PASSWORD DICTIONARY](DROP-WEAK-PASSWORD-DICTIONARY.md)
