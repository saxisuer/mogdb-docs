---
title: ABORT
summary: ABORT
author: Zhang Cuiping
date: 2021-05-17
---

# ABORT

## Function

**ABORT** rolls back the current transaction and cancels the changes in the transaction.

This command is equivalent to **ROLLBACK**, and is present only for historical reasons. Now **ROLLBACK** is recommended.

## Precautions

**ABORT**  has no impact outside a transaction, but will throw a NOTICE message.

## Syntax

```ebnf+diagram
Abort ::= ABORT [ WORK | TRANSACTION ];
```

## Parameter Description

**WORK | TRANSACTION**

Specifies an optional keyword, which has no effect except increasing readability.

## Examples

```sql
-- Create the customer_demographics_t1 table.
mogdb=# CREATE TABLE customer_demographics_t1
(
    CD_DEMO_SK                INTEGER               NOT NULL,
    CD_GENDER                 CHAR(1)                       ,
    CD_MARITAL_STATUS         CHAR(1)                       ,
    CD_EDUCATION_STATUS       CHAR(20)                      ,
    CD_PURCHASE_ESTIMATE      INTEGER                       ,
    CD_CREDIT_RATING          CHAR(10)                      ,
    CD_DEP_COUNT              INTEGER                       ,
    CD_DEP_EMPLOYED_COUNT     INTEGER                       ,
    CD_DEP_COLLEGE_COUNT      INTEGER
)
WITH (ORIENTATION = COLUMN,COMPRESSION=MIDDLE)
;

-- Insert data.
mogdb=# INSERT INTO customer_demographics_t1 VALUES(1920801,'M', 'U', 'DOCTOR DEGREE', 200, 'GOOD', 1, 0,0);

-- Start a transaction.
mogdb=# START TRANSACTION;

-- Update the column.
mogdb=# UPDATE customer_demographics_t1 SET cd_education_status= 'Unknown';

-- Abort the transaction. All updates are rolled back.
mogdb=# ABORT;

-- Query data.
mogdb=# SELECT * FROM customer_demographics_t1 WHERE cd_demo_sk = 1920801;
cd_demo_sk | cd_gender | cd_marital_status | cd_education_status  | cd_purchase_estimate | cd_credit_rating | cd_dep_count | cd_dep_employed_count | cd_dep_college_count
------------+-----------+-------------------+----------------------+----------------------+------------------+--------------+-----------------------+----------------------
    1920801 | M         | U                 | DOCTOR DEGREE        |                  200 | GOOD             |            1 |                     0 |                    0
(1 row)

-- Delete the table.
mogdb=# DROP TABLE customer_demographics_t1;
```

## Helpful Links

[SET TRANSACTION](SET-TRANSACTION.md), [COMMIT | END](COMMIT-END.md), [ROLLBACK](ROLLBACK.md)
