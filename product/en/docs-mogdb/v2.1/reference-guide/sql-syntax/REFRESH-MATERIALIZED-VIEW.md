---
title: REFRESH MATERIALIZED VIEW
summary: REFRESH MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# REFRESH MATERIALIZED VIEW

## Function

**REFRESH MATERIALIZED VIEW** refreshes materialized views in full refresh mode.

## Precautions

- Full refreshing can be performed on both full and incremental materialized views.
- To refresh a materialized view, you must have the SELECT permission on the base table.

## Syntax

```
REFRESH MATERIALIZED VIEW mv_name;
```

## Parameter Description

- **mv_name**

  Name of the materialized view to be refreshed.

## Examples

```
-- Create an ordinary table.
mogdb=# CREATE TABLE my_table (c1 int, c2 int);
-- Create a full materialized view.
mogdb=# CREATE MATERIALIZED VIEW my_mv AS SELECT * FROM my_table;
-- Create an incremental materialized view.
mogdb=# CREATE INCREMENTAL MATERIALIZED VIEW my_imv AS SELECT * FROM my_table;
-- Write data to the base table.
mogdb=# INSERT INTO my_table VALUES(1,1),(2,2);
-- Refresh the full materialized view my_mv.
mogdb=# REFRESH MATERIALIZED VIEW my_mv;
-- Fully refresh the incremental materialized view my_imv.
mogdb=# REFRESH MATERIALIZED VIEW my_imv;
```

## Helpful Links

[ALTER MATERIALIZED VIEW](ALTER-MATERIALIZED-VIEW.md), [CREATE INCREMENTAL MATERIALIZED VIEW](CREATE-INCREMENTAL-MATERIALIZED-VIEW.md), [CREATE MATERIALIZED VIEW](CREATE-MATERIALIZED-VIEW.md), [CREATE TABLE](CREATE-TABLE.md), [DROP MATERIALIZED VIEW](DROP-MATERIALIZED-VIEW.md), and [REFRESH INCREMENTAL MATERIALIZED VIEW](REFRESH-INCREMENTAL-MATERIALIZED-VIEW.md)
