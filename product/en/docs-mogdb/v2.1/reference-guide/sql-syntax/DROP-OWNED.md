---
title: DROP OWNED
summary: DROP OWNED
author: Zhang Cuiping
date: 2021-05-18
---

# DROP OWNED

## Function

**DROP OWNED** deletes the database objects owned by a database role.

## Precautions

- This interface will revoke the role's permissions on all objects in the current database and shared objects (databases and tablespaces).
- **DROP OWNED** is often used to prepare for removing one or more roles. Because **DROP OWNED** affects only the objects in the current database, you need to run this statement in each database that contains the objects owned by the role to be removed.
- Using the **CASCADE** option may cause this statement to recursively remove objects owned by other users.
- The databases and tablespaces owned by the role will not be removed.

## Syntax

```ebnf+diagram
DropOwned ::= DROP OWNED BY name [, ...] [ CASCADE | RESTRICT ];
```

## Parameter Description

- **name**

  Specifies the role name.

- **CASCADE | RESTRICT**

  - **CASCADE**: automatically deletes the objects that depend on the objects to be deleted.
  - **RESTRICT**: refuses to delete the objects if other objects depend on them. This is the default action.

## Helpful Links

[REASSIGN OWNED](REASSIGN-OWNED.md) and [DROP ROLE](DROP-ROLE.md)
