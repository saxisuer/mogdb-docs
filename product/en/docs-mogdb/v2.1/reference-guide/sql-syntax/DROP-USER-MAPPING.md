---
title: DROP USER MAPPING
summary: DROP USER MAPPING
author: Zhang Cuiping
date: 2021-05-18
---

# DROP USER MAPPING

## Function

**DROP USER MAPPING** drops a user mapping for a foreign server.

## Syntax

```ebnf+diagram
DropUserMapping ::= DROP USER MAPPING [ IF EXISTS ] FOR { user_name | USER | CURRENT_USER | PUBLIC } SERVER server_name;
```

## Parameter Description

- **IF EXISTS**

  Reports a notice instead of an error if the user mapping does not exist.

- **user_name**

  Specifies user name of the mapping.

  CURRENT_USER and USER match the name of the current user. PUBLIC is used to match all current and future user names in the system.

- **server_name**

  Specifies name of the server to which the user is mapped.

## Helpful Links

[ALTER USER MAPPING](ALTER-USER-MAPPING.md) and [CREATE USER MAPPING](CREATE-USER-MAPPING.md)
