---
title: COMMIT PREPARED
summary: COMMIT PREPARED
author: Zhang Cuiping
date: 2021-05-10
---

# COMMIT PREPARED

## Function

**COMMIT PREPARED** commits a prepared two-phase transaction.

## Precautions

- The function is only available in maintenance mode (when the GUC parameter **xc_maintenance_mode** is **on**). Exercise caution when enabling the mode. It is used by maintenance engineers for troubleshooting. Common users should not use the mode.
- Only the transaction creators or system administrators can run the **COMMIT PREPARED** command. The creation and commit operations must be in different sessions.
- The transaction function is maintained automatically by the database, and should be not visible to users.

## Syntax

```ebnf+diagram
CommitPrepared ::= COMMIT PREPARED transaction_id ;
```

```ebnf+diagram
CommitPrepared ::= COMMIT PREPARED transaction_id WITH CSN;
```

## Parameter Description

- **transaction_id**

  Specifies the identifier of the transaction to be committed. The identifier must be different from those for current prepared transactions.

- **CSN(commit sequence number)**

  Specifies the sequence number of the transaction to be committed. It is a 64-bit, incremental, unsigned number.

## Example

```sql
-COMMIT PREPARED commits a transaction whose identifier is trans_test.
mogdb=# COMMIT PREPARED 'trans_test';
```

## Helpful Links

[PREPARE TRANSACTION](PREPARE-TRANSACTION.md), [ROLLBACK PREPARED](ROLLBACK-PREPARED.md).
