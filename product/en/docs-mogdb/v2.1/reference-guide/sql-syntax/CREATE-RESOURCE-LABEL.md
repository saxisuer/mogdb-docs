---
title: CREATE RESOURCE LABEL
summary: CREATE RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE RESOURCE LABEL

## Function

**CREATE RESOURCE LABEL** creates a resource label.

## Precautions

Only users with the  **poladmin**  or  **sysadmin**  permission, or the initial user can perform this operation.

## Syntax

```ebnf+diagram
CreateResourceLabel ::= CREATE RESOURCE LABEL [IF NOT EXISTS] label_name ADD label_item_list[, ...]*;
```

- label_item_list

  ```ebnf+diagram
  label_item_list ::= resource_type(resource_path[, ...]*)
  ```

- resource_type

  ```ebnf+diagram
  resource_type ::= TABLE | COLUMN | SCHEMA | VIEW | FUNCTION
  ```

## Parameter Description

- **label_name**

  Specifies the resource label name, which must be unique.

  Value range: a string. It must comply with the naming convention.

- **resource_type**

  Specifies the type of database resources to be labeled.

- **resource_path**

  Specifies the path of database resources.

## Examples

```sql
-- Create table tb_for_label.
mogdb=# CREATE TABLE tb_for_label(col1 text, col2 text, col3 text);

-- Create schema schema_for_label.
mogdb=# CREATE SCHEMA schema_for_label;

-- Create view view_for_label.
mogdb=# CREATE VIEW view_for_label AS SELECT 1;

-- Create function func_for_label.
mogdb=# CREATE FUNCTION func_for_label RETURNS TEXT AS $$ SELECT col1 FROM tb_for_label; $$ LANGUAGE SQL;

-- Create a resource label based on the table.
mogdb=# CREATE RESOURCE LABEL IF NOT EXISTS table_label add TABLE(public.tb_for_label);

-- Create a resource label based on the columns.
mogdb=# CREATE RESOURCE LABEL IF NOT EXISTS column_label add COLUMN(public.tb_for_label.col1);

-- Create a resource label based on the schema.
mogdb=# CREATE RESOURCE LABEL IF NOT EXISTS schema_label add SCHEMA(schema_for_label);

-- Create a resource label based on the view.
mogdb=# CREATE RESOURCE LABEL IF NOT EXISTS view_label add VIEW(view_for_label);

-- Create a resource label based on the function.
mogdb=# CREATE RESOURCE LABEL IF NOT EXISTS func_label add FUNCTION(func_for_label);
```

## Helpful Links

[ALTER RESOURCE LABEL](ALTER-RESOURCE-LABEL.md)，[DROP RESOURCE LABEL](DROP-RESOURCE-LABEL.md)
