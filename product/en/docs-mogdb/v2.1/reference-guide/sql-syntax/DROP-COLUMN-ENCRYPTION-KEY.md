---
title: DROP COLUMN ENCRYPTION KEY
summary: DROP COLUMN ENCRYPTION KEY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP COLUMN ENCRYPTION KEY

## Function

**CREATE COLUMN ENCRYPTION KEY** deletes a column encryption key (CEK).

## Precautions

Only the CEK owner or a user who has been granted the DROP permission can run this command. By default, the system administrator has the permission.

## Syntax

```ebnf+diagram
DropColumnEncryptionKey ::= DROP COLUMN ENCRYPTION KEY [ IF EXISTS ] column_encryption_key_name [CASCADE];
```

## Parameter Description

- **IF EXISTS**

  If a specified CEK does not exist, a notice rather than an error is issued.

- **column_encryption_key_name**

  Name of a CEK to be deleted.

  Value range: a string. It is the name of an existing CEK.

## Examples

```sql
-- Delete a CEK.
mogdb=# DROP COLUMN ENCRYPTION KEY ImgCEK CASCADE;
ERROR:  cannot drop column setting: imgcek cascadely because encrypted column depend on it.
HINT:  we have to drop encrypted column: name, ... before drop column setting: imgcek cascadely.
```
