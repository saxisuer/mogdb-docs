---
title: REFRESH INCREMENTAL MATERIALIZED VIEW
summary: REFRESH INCREMENTAL MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-06-07
---

# REFRESH INCREMENTAL MATERIALIZED VIEW

## Function

REFRESH INCREMENTAL MATERIALIZED VIEW refreshes the materialized view in materialized mode.

## Precautions

- Incremental refresh supports only incremental materialized views.
- To refresh a materialized view, you must have the SELECT permission on the base table.

## Syntax

```ebnf+diagram
RefreshIncrementalMaterializedView ::= REFRESH INCREMENTAL MATERIALIZED VIEW mv_name;
```

## Parameter Description

- **mv_name**

  Name of the materialized view to be refreshed.

## Example

```sql
-- Create an ordinary table.
mogdb=# CREATE TABLE my_table (c1 int, c2 int);
-- Create an incremental materialized view.
mogdb=# CREATE INCREMENTAL MATERIALIZED VIEW my_imv AS SELECT * FROM my_table;
-- Write data to the base table.
mogdb=# INSERT INTO my_table VALUES(1,1),(2,2);
-- Incrementally refresh the incremental materialized view my_imv.
mogdb=# REFRESH INCREMENTAL MATERIALIZED VIEW my_imv;
```

## Helpful Links

[ALTER MATERIALIZED VIEW](ALTER-MATERIALIZED-VIEW.md), [CREATE INCREMENTAL MATERIALIZED VIEW](CREATE-INCREMENTAL-MATERIALIZED-VIEW.md), [CREATE MATERIALIZED VIEW](CREATE-MATERIALIZED-VIEW.md), [CREATE TABLE](CREATE-TABLE.md), [DROP MATERIALIZED VIEW](DROP-MATERIALIZED-VIEW.md), and [REFRESH MATERIALIZED VIEW](REFRESH-MATERIALIZED-VIEW.md)
