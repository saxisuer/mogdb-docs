---
title: SET ROLE
summary: SET ROLE
author: Zhang Cuiping
date: 2021-05-18
---

# SET ROLE

## Function

**SET ROLE** sets the current user identifier of the current session.

## Precautions

- Users of the current session must be members of specified **rolename**, but the system administrator can choose any roles.
- Executing this statement may add rights of a user or restrict rights of a user. If the role of a session user has the **INHERITS** attribute, it automatically has all rights of roles that **SET ROLE** enables the role to be. In this case, **SET ROLE** physically deletes all rights directly granted to session users and rights of its belonging roles and only leaves rights of the specified roles. If the role of the session user has the **NOINHERITS** attribute, **SET ROLE** deletes rights directly granted to the session user and obtains rights of the specified role.

## Syntax

- Set the current user identifier of the current session.

  ```ebnf+diagram
  SetRole ::= SET [ SESSION | LOCAL ] ROLE role_name PASSWORD 'password';
  ```

- Reset the current user identifier to that of the current session.

  ```ebnf+diagram
  ResetRole ::= RESET ROLE;
  ```

## Parameter Description

- **SESSION**

  Specifies that the statement takes effect only for the current session. This parameter is used by default.

- **LOCAL**

  Specifies that the specified statement takes effect only for the current transaction.

- **role_name**

  Indicates the role name.

  Value range: a string. It must comply with the naming convention rule.

- **password**

  Specifies the password of a role. It must comply with the password convention.

- **RESET ROLE**

  Resets the current user identifier.

## Examples

```sql
-- Create a role paul.
mogdb=# CREATE ROLE paul IDENTIFIED BY 'Bigdata@123';

-- Set the current user to paul.
mogdb=# SET ROLE paul PASSWORD 'Bigdata@123';

-- View the current session user and the current user.
mogdb=# SELECT SESSION_USER, CURRENT_USER;

-- Reset the current user.
mogdb=# RESET role;

-- Delete the user.
mogdb=# DROP USER paul;
```
