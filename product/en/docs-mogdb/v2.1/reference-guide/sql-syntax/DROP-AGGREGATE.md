---
title: DROP AGGREGATE
summary: DROP AGGREGATE
author: Zhang Cuiping
date: 2021-06-07
---

# DROP AGGREGATE

## Function

**DROP AGGREGATE** deletes an aggregate function.

## Precautions

**DROP AGGREGATE** deletes an existing aggregate function. Only the owner of the aggregate function can run this command.

## Syntax

```ebnf+diagram
DropAggregate ::= DROP AGGREGATE [ IF EXISTS ] name ( argtype [ , ... ] ) [ CASCADE | RESTRICT ];
```

## Parameter Description

- **IF EXISTS**

  Do not throw an error if the specified aggregation does not exist. A notice is issued in this case.

- **name**

  Existing aggregate function name (optionally schema-qualified).

- **argtype**

  Input data type of the aggregate function. To reference a zero-parameter aggregate function, use * to replace the input data type list.

- **CASCADE**

  Cascade deletes objects that depend on the aggregate function.

- **RESTRICT**

  Refuses to delete the aggregate function if any objects depend on it. This is a default processing.

## Examples

Delete the aggregate function **myavg** of the integer type:

```sql
DROP AGGREGATE myavg(integer);
```

## Compatibility

The SQL standard does not provide the **DROP AGGREGATE** statement.
