---
title: gaussdb
summary: gaussdb
author: Zhang Cuiping
date: 2021-06-07
---

# gaussdb

## Background

**gaussdb** is a major process of MogDB and also an executable command. A database process can be started using gaussdb.

## Syntax

```
gaussdb [OPTION]...
```

## Description

To access the database, the client application connects (over a local network or the Internet) to a running gaussdb process. Then the process starts an independent thread to handle the connection.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> When starting the database using **gaussdb**, you also need to open another window to connect to the database or use & to enable the application to run in the background.

A **gaussdb** process manages all data in a database. A system can operate multiple **gaussdb** processes using different data directories and port numbers. When **gaussdb** is started, the data directory must be specified using the **-data-dir** option. Typically, **-D** points directly to the database directory created by **gs_initdb**.

By default, **gaussdb** starts in the foreground and prints log messages to the standard error stream. In practical applications, **gaussdb** should be automatically started as a background process at boot time in most cases.

The **gaussdb** command can also be called in single-user mode. The primary use for this mode is during the initialization of gs_initdb. Sometimes, it is used for debugging or disaster recovery. However, running a single-user server is not truly suitable for debugging the server, because no realistic inter-process communication and locking happens. When the **gaussdb** command is called in single-user mode from the shell, you can enter queries, and the results will be printed to the screen, but in a form that is more useful for developers rather than common users. In single user mode, the session user ID is set to **1** and given the administrator rights. The user does not need to be existent. Therefore, servers in single user mode can be used to manually recover damaged system catalogs.

## Options

**gaussdb** supports command-line parameters listed in [Table 1](#parameter).

**Table 1** Parameter description <a id="parameter"></a>

| Parameter          | Parameter Description                                        | Value Range                                                  |
| :----------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -B NBUFFERS        | Sets the number of shared buffers to be used by the server processes. | -                                                            |
| -b BINARY UPGRADES | Specifies binary upgrade.                                    | -                                                            |
| -c NAME=VALUE      | Assigns a value to a running parameter. **-c** can appear multiple times to set multiple parameters. | -                                                            |
| -C NAME            | Prints the value of a named run-time parameter and then exits. It can be used on a running server, and returns values from **postgresql.conf**. | The value can be the value of any parameter in the **postgresql.conf** file. |
| -d 1-5             | Sets the debug level. The value ranges from **1** to **5** indicates debug level information recorded. The higher this value is set, the more debugging output is written to the server logs. | Value range: 1 to 5<br />NOTE:<br />If the value is less than or equal to **0**, only notice-level information is recorded. |
| -D DATADIR         | Specifies the data directory or location of configuration files. | User-defined value.                                          |
| -e                 | Sets the default date style to **European**, that is DMY ordering of input date fields. It also prints the day before the month in certain date output formats. | -                                                            |
| -F                 | Disables fsync calls for improved performance, at the risk of data corruption in the event of a system crash. Specifying this option is equivalent to disabling the fsync parameter. | -                                                            |
| -h HOSTNAME        | Specifies the host name or IP address on which **gaussdb** listens to TCP/IP connections from client applications. | Existing host name or IP address at the front-end.           |
| -i                 | Allows remote clients to communicate with the server through TCP/IP (Internet domain socket) connections. | -                                                            |
| -k DIRECTORY       | Specifies the directory of the Unix domain socket on which gaussdb listens to connections from client applications. | The default value is normally **/tmp**, but it can be changed during compilation. |
| -l                 | Allows remote clients to connect to the server via secure socket layer (SSL). | -                                                            |
| -N MAX-CONNECT     | Sets the maximum number of client connections that this server will accept. The default value of this parameter is automatically chosen by gs_initdb. Specifying this option is equivalent to setting the max_connections configuration parameter. | Value range: a positive integer.                             |
| -M SERVERMODE      | Specifies the startup mode.                                  | Values:<br />- **primary**: The server starts in primary mode.<br />- **standby**: The server starts in standby mode.<br />- **cascade_standby**: The local server starts as a cascaded standby server.<br />- **pending**: The server is in pending mode to be promoted to a primary server or a standby server. |
| -p PORT            | Specifies the TCP/IP port or local Unix domain socket file extension on which gaussdb is to listen for connections from client applications. The default port ID is 5432. | A positive integer within the port range supported by the OS. |
| -s                 | Prints time information and other statistics at the end of each command. | -                                                            |
| -S WORK-MEM        | Specifies the size of memory to be used by internal sorts and hashes before resorting to temporary disk files. | The unit is KB.                                              |
| -single_node       | Start a single-node database.                                | -                                                            |
| -V, -version       | Prints the **gaussdb** version and exits.                    | -                                                            |
| -NAME=VALUE        | Assigns a value to a running parameter.                      | -                                                            |
| -describe-config   | Describes configuration parameters and then exits.           | -                                                            |
| -?, -help          | Displays help about the **gaussdb** command line arguments and then exits. | -                                                            |

Parameters described in [Table 2](#developer) are used for debugging purposes, and in some cases for assisting in recovering severely damaged databases. Do not use these parameters for debugging if databases are providing services for applications.

**Table 2** Developer options <a id="developer"></a>

| Parameter        | Parameter Description                                        | Value Range                                                  |
| :--------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -f s\|i\|n\|m\|h | Forbids the use of particular scan and join methods.         | Value range:<br />- **s**: stop sequence<br />- **i**: index scanning<br />- **n**: nested loop is disabled.<br />- **m**: merge join<br />- **h**: hash join |
| -n               | Debugs problems that cause a server process breakdown. The ordinary strategy in this situation is to instruct all other server processes to terminate and then reinitialize the shared memory and semaphores. This option specifies that **gaussdb** will not reinitialize shared memory. | -                                                            |
| -P               | Ignores system indexes when reading system tables, but still updates the indexes when modifying the tables. | -                                                            |
| -t pa\|pl\|ex    | Prints timing statistics for each query relating to each of the major system modules. | -                                                            |
| -T               | Debugs problems that cause a server process breakdown. This option specifies that gaussdb will stop all other server processes by sending the signal SIGSTOP, but will not cause them to exit. This permits system programmers to manually collect core dumps from all server processes. | -                                                            |
| -W NUM           | Specifies the number of seconds to wait before a new server process is started. | Unit: second                                                 |
| -localxid        | Uses the local transaction ID instead of the global transaction ID.<br />NOTICE:<br />This option is used only for **gs_initdb**. Using this option may cause database inconsistency. | Existing local transaction ID.                               |

Options listed in [Table 3](#options) apply only to the single user mode.

**Table 3** Options for the single-user mode <a id="options"></a>

| Parameter   | Parameter Description                                        | Value Range                                             |
| :---------- | :----------------------------------------------------------- | :------------------------------------------------------ |
| -single     | Selects the single user mode. This must be the first argument on the command line. | -                                                       |
| DBNAME      | Specifies the name of the database to be accessed.This must be the last argument on the command line. | A character string. The default value is the user name. |
| -d 0-5      | Re-specifies the debug level.                                | 0-5                                                     |
| -E          | Echoes all commands.                                         | -                                                       |
| -j          | Disables use of newline as a statement delimiter.            | -                                                       |
| -r FILENAME | Saves all server standard output and standard errors to **filename**. | -                                                       |

**Table 4** Automatic startup mode options

| Parameter   | Parameter Description                                        | Value Range |
| :---------- | :----------------------------------------------------------- | :---------- |
| -boot       | Enables the automatic startup mode. This must be the first argument on the command line. This parameter initializes parameter variables and related configurations. It is usually used in the database installation process and is unperceivable. | -           |
| -r FILENAME | Saves all server standard output and standard errors to **filename**. | -           |
| -x NUM      | Specifies a new server thread type. The automatic startup mode can be used during database initialization. This parameter starts various threads to execute different logics. Generally, this parameter is not used because the automatic startup mode is seldom used. | -           |

**Table 5** Upgrade mode options

| Parameter | Parameter Description                                     | Value Range |
| :-------- | :-------------------------------------------------------- | :---------- |
| -u NUM    | Specifies the database kernel version before the upgrade. | -           |

In addition to the preceding parameters, all parameters in postgres.conf can be transferred to **gaussdb** using -**guc_name=guc_value** when **gaussdb** is started. For example: **-enable_fast_query_shipping=off**

## Environment Variables

- **PGLINETENCODING**: default character encoding used by clients. Clients can overwrite it individually. This value can also be set in the configuration file.
- **PGDATESTYLE**: default value of the run-time **DateStyle** parameter. The use of this environment variable is deprecated.
- **TZ**: server time zone.

## Error Handling

A failure message mentioning **semget** or **shmget** probably indicates that you need to reconfigure your kernel to provide adequate shared memory and semaphores. You can decrease **shared_buffers** to reduce the shared memory consumption of MogDB, or decrease **max_connections** to reduce the semaphore consumption of MogDB.

If a failure message suggesting that another server is running is reported, check the system using the following command based on your system:

```
ps ax | grep gaussdb
```

or

```
ps -ef | grep gaussdb
```

If you are certain that no conflicting server is running, you can remove the lock file mentioned in the message and try again.

A failure message indicating inability to bind to a port might indicate that the port is already in use by a non-MogDB process. This message may also be reported if you terminate **gaussdb** and immediately restart it using the same port. In this case, you must simply wait a few seconds until the operating system closes the port before trying again. Finally, you may get this error if you specify a port number that your operating system considers it to be reserved. For example, the Unix considers ports whose IDs are under 1024 to be "trusted" and permits only Unix administrators to access the Unix.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - If possible, do not use SIGKILL to stop the main process. because the operation prevents **gaussdb** from releasing system resources (such as shared memory and semaphores) that it holds before terminating. This might cause problems for starting a fresh process.
> - To properly terminate the server process, signals **SIGTERM**, **SIGINT**, and **SIGQUIT** can be used. **SIGTERM** will wait for all clients to terminate before exit. **SIGINT** will forcibly disconnect all clients, and **SIGQUIT** will exit immediately without proper shutdown, resulting in a recovery run during restart.
> - The SIGHUP signal will reload the server configuration files. It is also possible to send **SIGHUP** to an individual server process, but that is usually not sensible.
> - To cancel a running query, send the **SIGINT** signal to the process running that command.
> - The main server process uses **SIGTERM** to tell its subordinate server processes to quit properly and SIGQUIT to terminate without a cleanup. These signals should not be used by users. It is also unwise to send SIGKILL to a server process: The main process will interpret this as a crash and will force all the subordinate processes to quit as part of its standard crash-recovery pocedure.

## **Usage**

Run the following command to start a server in single-user mode:

```
gaussdb --single -D /usr/local/pgsql/data other-options my_database
```

Use **-D** to provide the correct path of the database directory for the server. Besides, specify the name of the existing particular database.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - In most cases, the single-user mode server treats newline as the command entry terminator. To continue a command across multiple lines, you must type a backslash just before each newline except the last one.
> - If you use the -j command line option, then newline does not terminate command entry. In this case, the server will read the standard input until the end-of-file (EOF) marker, then process the input as a single command string. Backslashes and newlines are treated as common characters in this case.
> - To quit the session, type EOF (Control+D). If you have used -j, two consecutive EOFs are needed to exit.
> - The single-user mode server does not provide complex line-editing functions (no command history, for example). The single-user mode also does not do any background processing, like automatic checkpoints.

## Example

1. Run the following command to start gaussdb in the background using default values:

   ```
   nohup gaussdb >logfile 2>&1 </dev/null  &
   ```

2. Run the following command to start gaussdb with a specified port, for example, 1234:

   ```
   gaussdb --single_node -p -D data_dir
   ```

   To connect to this server using gsql, specify this port with the -p option:

   ```
   gsql -d postgres -p 1234
   ```
