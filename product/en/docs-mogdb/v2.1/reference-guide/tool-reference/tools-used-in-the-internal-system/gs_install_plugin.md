---
title: gs_install_plugin
summary: gs_install_plugin
author: Guo Huan
date: 2021-11-29
---

# gs_install_plugin

## Background

MogDB provides gs_install_plugin for installing plugins. Currently, the supported plugins include pg_repack, pg_trgm, dblink, wal2json, orafce, pg_bulkload, and pg_prewarm. This tool is suitable for MogDB deployed with one primary node and multiple standby nodes.

&nbsp;

## Prerequisites

The deployment of MogDB with one primary node and multiple standby nodes has been completed.

> **release patch notes**:
>
> There is a known bug in version 2.1 of this tool that causes installation failure due to unrecognized plug-in packages.
> Please do the following to fix the patch before using this tool and then proceed with the subsequent plugin installation (for 2.1.x).
>
> - When the extranet is accessible.
>
> 1. switch to the user who installed the database (e.g.: omm): `su - omm`
> 2. perform online repair: `cd $GPHOME && curl https://cdn-mogdb.enmotech.com/mogdb-media/2.1.1/om_plugin_install_v2.1.1.patch | patch -p1`
>
> - When unable to access the extranet.
>
> 1. Please download the patch file [om_plugin_install_v2.1.1.patch](https://cdn-mogdb.enmotech.com/mogdb-media/2.1.1/om_plugin_install_v2.1.1.patch) from a server with internet access first, and then copy the patch file to the server to be repaired.
> 2. Switch to the user who installed the database (e.g.: omm): `su - omm`
> 3. Execute `cd $GPHOME && patch -p1 < [replace Patch file path here]`

&nbsp;

## Syntax

```
gs_install_plugin [-h] [-X XML] [--all] [--dblink] [--pg_trgm] [--wal2json] [--pg_repack] [--orafce] [--pg_bulkload] [--pg_prewarm] [--plugins PLUGINS [PLUGINS ...]]
```

&nbsp;

## Parameter Description

- -h

  show help message and exit

- -X

  Add the location of the MogDB clusterconfig.xml file.

- --all

  Install all plugins.

- --plugins

  Multiple parameters can be accepted, for example: --plugins dblink pg_repack wal2json

- --dblink

  Install dblink plugin.

- --pg_trgm

  Install pg_trgm plugin.

- --wal2json

  Install wal2json plugin.

- --pg_repack

  Install pg_repack plugin.

- --orafce

  Install orafce plugin.

- --pg_bulkload

  Install pg_bulkload plugin.

&nbsp;

## Example

1. Go to the **script** folder of the database installation directory.

   ```bash
   cd /opt/software/mogdb/script/
   ```

2. Create the **static** folder.

   ```bash
   mkdir -p static
   ```

    > Note: Please do not change the folder name.

3. Visit [MogDB official website download page](https://www.mogdb.io/en/downloads/mogdb/), download the corresponding version of the plugin according to your operating system and CPU architecture, and put the plugin in the **static** folder.

4. Set permissions.

    ```
    chmod -R 755 /opt/software/mogdb/script
    chown -R omm:dbgrp /opt/software/mogdb/script
    ```

5. Configure XML File. The XML file configuration uses one primary and one standby configuration file as an example. `backIp1s` is the IP address of a node, and `dataNode1` is the data directory. The data directory obtained by this program by default is the data directory of the first node.

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <ROOT>
        <!-- Overall information about MogDB -->
        <CLUSTER>
            <PARAM name="clusterName" value="dbCluster" />
            <PARAM name="nodeNames" value="node1,node2" />
            <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12"/>
            <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
            <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
            <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
            <PARAM name="corePath" value="/opt/mogdb/corefile"/>
            <PARAM name="clusterType" value="single-inst"/>
        </CLUSTER>
        <!-- Information about node deployment on each server -->
        <DEVICELIST>
            <!-- Information about the node deployment on node1 -->
            <DEVICE sn="1000001">
                <PARAM name="name" value="node1"/>
                <PARAM name="azName" value="AZ1"/>
                <PARAM name="azPriority" value="1"/>
                <!-- If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address. -->
                <PARAM name="backIp1" value="192.168.0.11"/>
                <PARAM name="sshIp1" value="192.168.0.11"/>

                <!--dbnode-->
                <PARAM name="dataNum" value="1"/>
                <PARAM name="dataPortBase" value="26000"/>
                <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1"/>
            </DEVICE>

            <!-- Node deployment information on node2. The value of name is the host name.  -->
            <DEVICE sn="1000002">
                <PARAM name="name" value="node2"/>
                <PARAM name="azName" value="AZ1"/>
                <PARAM name="azPriority" value="1"/>
                <!-- If only one NIC is available for the server, set backIP1 and sshIP1 to the same IP address. -->
                <PARAM name="backIp1" value="192.168.0.12"/>
                <PARAM name="sshIp1" value="192.168.0.12"/>
            </DEVICE>
        </DEVICELIST>
    </ROOT>
    ```

6. Switch to omm user.

    ```
    su - omm
    ```

7. Install all plugins.

    ```bash
    /opt/software/mogdb/script/gs_install_plugin -X /opt/software/mogdb/clusterconfig.xml --all
    # When the installation is successful, the output is as follows:
    SUCCESS: dblink.
    SUCCESS: pg_trgm.
    SUCCESS: pg_repack.
    SUCCESS: wal2json.
    SUCCESS: orafce.
    SUCCESS: pg_bulkload.
    SUCCESS: pg_prewarm.
    ```

## Related Pages

[orafce](../../../reference-guide/oracle-plugins/orafce-user-guide.md), [dblink](../../../reference-guide/oracle-plugins/dblink-user-guide.md), [pg_repack](../../../reference-guide/oracle-plugins/pg_repack-user-guide.md), [pg_trgm](../../../reference-guide/oracle-plugins/pg_trgm-user-guide.md), [wal2json](../../../reference-guide/oracle-plugins/wal2json-user-guide.md), [pg_bulkload](../../../reference-guide/oracle-plugins/pg_bulkload-user-guide.md), [pg_prewarm](../../../reference-guide/oracle-plugins/pg_prewarm-user-guide.md).