---
title: gstrace
summary: gstrace
author: Zhang Cuiping
date: 2021-06-07
---

# gstrace

## Function Description

**gstrace**, provided by MogDB, is used to trace the kernel code execution path, record kernel data structure, and analyze code performance. The number of trace points and data are fixed in the version and cannot be dynamically added or deleted.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **WARNING:**
>
> 1. The specified memory variables dumped from the kernel are used only for diagnosis and analysis. Registers or memory addresses will not be randomly and directly specified. The read memory addresses are hard-coded in the development, and no register or memory is read or modified randomly.
> 2. Trace points may involve sensitive data. Before collecting information to be traced, you need to obtain authorization and permission from the customer.
> 3. MogDB does not point on sensitive information or trace user-related data.
> 4. The trace is used only for diagnosis. Enabling the trace function affects the performance, and the impact depends on the load and the modules to be traced.
> 5. The permission of the trace tool is **0700**, which indicates that only database users can read, write, and execute the trace tool.
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** If the process is stopped abnormally, the **gstrace_trace_cfg_*** file will exist in the **/dev/shm** directory. You can manually delete the file.

## Syntax

```
gstrace [start|stop|config|dump|detailcodepath|analyze] [-p <port>][-s <BUFFER_SIZE>] [-f <DUMP_FILE>] [-o <OUTPUT_FILE>] [-t <STEP_SIZE>]
```

## Parameter Description

**Table 1** gs_trace parameters

| Parameter      | Description                                                  |
| :------------- | :----------------------------------------------------------- |
| start          | Starts tracing.                                              |
| stop           | Stops tracing and releases the shared memory trace buffer. Note: This will cause the traced information in the trace buffer to be lost. |
| config         | Displays configurations of the tracing tool.                 |
| dump           | Writes the traced information frm the shared memory to a specified file. If the tracing is not started, an error message will be displayed. |
| detail         | Parses the dumped binary file to a text file, so that traced threads and time information are displayed. |
| codepath       | Extracts the function calling information from the dumped file and displays the information in the call stack mode. |
| analyze        | Collects statistics on the number of execution times, total duration, average duration, maximum duration, and minimum duration of each function. |
| -p PORT        | Specifies the listening port number of an instance on which the trace function is enabled. |
| -f DUMP_FILE   | Specifies the trace file to be dumped.                       |
| -o OUTPUT_FILE | Specifies the file to be written.                            |
| -t STEP_SIZE   | Specifies the time span for segment analysis, in seconds. This parameter is optional. A separate *{OUTPUT_FILE}***.step** file will be generated. |
| -m MASK        | Specifies the modules and functions to be traced. If this parameter is left blank, all functions of all modules with the **trace** interface added are traced.<br />Format: [comp…][ALL].[func…][ALL]<br />Description:<br />- **comp** indicates the module list, which is separated by commas (,). For example, **executer,opengauss**. You can also use a sequence number, for example, **executer,2**.<br />- **func** indicates a function list, which is separated by commas (,). For example, **sql_execute_ddl, sql_execute_dcl**. You can also use a sequence number to indicate the sequence number of a function in a module, for example, **2**.<br />- The value **ALL** indicates that all modules or functions are traced.<br />Examples:<br />- **executer,opengauss.ALL**: All functions defined in the executer and opengauss modules are traced.<br />- **executer,opengauss.sql_execute_ddl**: The **sql_execute_ddl** function defined in the executer is traced. The opengauss module is ignored because this function does not exist.<br />- **executer,opengauss.1**: The first functions in the executer and opengauss modules are traced.<br />**ALL.1,2**: The first and second functions in all modules are traced. |
| -s BUFFER_SIZE | Specifies the size of the shared memory used for the trace function. The default value is 1 GB. If the specified value of BUFFER_SIZE is less than the minimum value 2048, the system automatically adjusts the value to the minimum value. If the value specified by BUFFER_SIZE is not 2^N (2 to the power of N), 2^N is aligned downwards. For example, if BUFFER_SIZE is set to 3072, the system changes the value to 2048 because 2^11 < 3072 < 2^12. |

## Example

1. Start the tracing.

   ```bash
   gstrace start -p 8000
   ```

2. Stop the tracing.

   ```bash
   gstrace stop -p 8000
   ```

3. Check the configurations for tracing.

   ```bash
   gstrace config -p 8000
   ```

4. dump trace

   ```bash
   gstrace dump -p 8000 -o /data/8000.dump
   ```

5. Parses dumped details.

   ```bash
   gstrace detail -f /data/8000.dump -o /data/8000.detail
   ```

6. Parse dumped codepath.

   ```bash
   gstrace codepath -f /data/8000.dump -o /data/8000.codepath
   ```

7. Analyze the global performance.

   ```bash
   gstrace analyze -f /data/8000.dump -o /data/8000.perf
   ```

8. Analyze the segment performance.

   ```bash
   gstrace analyze -f /data/8000.dump -o /data/8000.perf -t 1
   ```
