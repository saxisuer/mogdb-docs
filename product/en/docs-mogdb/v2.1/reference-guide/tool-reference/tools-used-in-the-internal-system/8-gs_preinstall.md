---
title: gs_preinstall
summary: gs_preinstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_preinstall

## Background

**gs_preinstall**, provided by MogDB, is used to configure the MogDB environment so that MogDB can be properly installed.

## Precautions

- You need to check the upper-layer directory permissions to ensure that the user has the read, write, and execution permissions on the installation package and configuration file directory.

- The mapping between each host name and IP address in the XML configuration file must be correct.

- Only user **root** is authorized to run the **gs_preinstall** command.

- This product supports hybrid deployment of minor versions under major versions in the same OS. The supported list is as follows:

  ```
  CentOS 6.4/6.5/6.6/6.7/6.8/6.9
  CentOS 7.0/7.1/7.2/7.3/7.4
  openEuler
  ```

## Syntax

- Prepare the MogDB environment.

  ```
  gs_preinstall -U USER -G GROUP -X XMLFILE [-L] [--skip-os-set] [--env-var="ENVVAR" [...]] [--sep-env-file=ENVFILE] [--skip-hostname-set] [-l LOGFILE] [--non-interactive]
  ```

- Display help information.

  ```
  gs_preinstall -? | --help
  ```

- Display version information.

  ```
  gs_preinstall -V | --version
  ```

## Parameter Description

- -U

  Specifies the name of the OS user running MogDB.

  Value range: a string. It must comply with the naming convention.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The value of **-U** cannot contain double backslashes () or the following characters: |;&$&lt;&gt;`'\"{}()[]~*?"

- -G

  Specifies the name of the OS user group running MogDB.

  Value range: a string. It must comply with the naming convention.

- -X

  Specifies the path of the MogDB configuration file.

  Value range: storage paths of XML files

- -L

  Prepares MogDB installation environment for the current nodes only. This parameter is used in the scenarios where the trust relationship of user **root** is not allowed and where the security hardening parameter **PermitRootLogin** is set to **no**. The following prerequisites must be met:

  1. The installation package and MogDB configuration file have been uploaded to all the hosts.
  2. The MogDB installation user has been created and mutual trust relationships have been established.
  3. All the commands executed on all the hosts are consistent.
  4. The mapping relationships between hostnames and IP addresses of all nodes need to be manually written to /etc/hosts on each host, and the comment content **#Gauss OM IP Hosts Mapping** must be added.

- -skip-os-set

  Specifies whether to set the parameter for the OS. By default, the parameter is set for the OS. If the parameter is specified, the configuration is dropped.

- -env-var="ENVVAR"

  Sets the environment variable for common users. You can specify multiple environment variables.

  Value range: a string.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This parameter cannot be set to the environment variable created by MogDB by default. Otherwise, the environment variables of the MogDB will be overwritten. [Table 1](#environment) lists the environment variables created by MogDB by default.

- -sep-env-file=ENVFILE

  Saves environment variable files. If this parameter is specified, the split version of environment variable is used.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The environment variable file name cannot be specified the same as that of the corresponding MogDB directories or those directories, or the directory of the MogDB user.

- -skip-hostname-set

  Specifies whether to write the mapping relationship between the host name and IP address of the XML configuration file to the **/etc/hosts** file. By default, the relationship is written to the file. If this parameter is specified, the relationship is not written to the file.

- -l

Specifies a log file name and its path. A timestamp will be added automatically to the log file name.

Default value: **/var/log/gaussdb/\*user name\*/om/gs_preinstall.log**

When **-l** is not specified but **gaussdbLogPath** is set in the XML file, the default value of the **-l** parameter is a combination of **gaussdbLogPath** value, the user name, and **om/gs_preinstall-YYYY-MM-DD_hhmmss.log**.

- -non-interactive

  Specifies the preinstallation execution mode.

  - When this parameter is not specified, the secure interactive mode is used where you need to enter the password for man-machine interaction.
  - If this parameter is specified, the non-interactive mode is used and man-machine interaction is not required.

- -?, -help

  Displays help information.

- -V, -version

  Displays version information.

  **Table 1** Environment variables created by MogDB by default <a id="environment"></a>

  | Environment Variable    | Description                                           |
  | :---------------------- | :---------------------------------------------------- |
  | MPPDB_ENV_SEPARATE_PATH | Split file path of the MogDB environment variable     |
  | GPHOME                  | MogDB tool directory                                  |
  | PATH                    | MogDB tool script directory                           |
  | LD_LIBRARY_PATH         | Third-party dynamic database path referenced by MogDB |
  | PYTHONPATH              | Python software path                                  |
  | GAUSS_WARNING_TYPE      | Alarm type                                            |
  | GAUSSHOME               | MogDB installation path                               |
  | GAUSS_VERSION           | MogDB version                                         |
  | PGHOST                  | Temporary directory of the user in MogDB              |
  | GS_CLUSTER_NAME         | MogDB name                                            |
  | GAUSSLOG                | MogDB log path                                        |
  | GAUSS_ENV               | Environment variable flag of MogDB                    |

## Manually Setting Syslog on the Server

Add the following content to the **/etc/syslog-ng/syslog-ng.conf** file:

```bash
template t_gaussdb {template("$DATE $SOURCEIP $MSGONLY\n");template_escape(no);};
source s_gaussdb{ udp(); };
filter f_gaussdb    { level(err,  crit) and match('GaussDB'); };
destination d_gaussdb { file("/var/log/syslog_MPPDB", template(t_gaussdb)); };
log { source(s_gaussdb); filter(f_gaussdb); destination(d_gaussdb); };
```

Add the following content to the **/etc/sysconfig/syslog** file:

```bash
SYSLOGD_OPTIONS="-r -m 0"
KLOGD_OPTIONS="-x"
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** You need to perform the operations on every host in MogDB.

## Example

Prepare MogDB environment in non-interactive mode.

```bash
./gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml --non-interactive
Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Distributing package.
Successfully distributed package.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Warning: Installation environment contains some warning messages.
Please get more details by "/package_r8/sudo/gs_checkos -i A -h host179174,host179175,host179176".
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```

Prepare MogDB environment in interactive mode.

```bash
./gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml
Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Are you sure you want to create trust for root (yes/no)? yes
Please enter password for root.
Password:
Creating SSH trust for the root permission user.
Please enter password for current user[root].
Password:
Checking network information.
All nodes in the network are Normal.
Successfully checked network information.
Creating SSH trust.
Creating the local key file.
Successfully created the local key files.
Appending local ID to authorized_keys.
Successfully appended local ID to authorized_keys.
Updating the known_hosts file.
Successfully updated the known_hosts file.
Appending authorized_key on the remote node.
Successfully appended authorized_key on all remote node.
Checking common authentication file content.
Successfully checked common authentication content.
Distributing SSH trust file to all node.
Successfully distributed SSH trust file to all node.
Verifying SSH trust on all hosts.
Successfully verified SSH trust on all hosts.
Successfully created SSH trust.
Successfully created SSH trust for the root permission user.
Distributing package.
Successfully distributed package.
Are you sure you want to create the user[omm] and create trust for it (yes/no)? yes
Please enter password for cluster user.
Password:
Please enter password for cluster user again.
Password:
Creating [omm] user on all nodes.
Successfully created [omm] user on all nodes.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Creating SSH trust for [omm] user.
Please enter password for current user[omm].
Password:
Checking network information.
All nodes in the network are Normal.
Successfully checked network information.
Creating SSH trust.
Creating the local key file.
Successfully created the local key files.
Appending local ID to authorized_keys.
Successfully appended local ID to authorized_keys.
Updating the known_hosts file.
Successfully updated the known_hosts file.
Appending authorized_key on the remote node.
Successfully appended authorized_key on all remote node.
Checking common authentication file content.
Successfully checked common authentication content.
Distributing SSH trust file to all node.
Successfully distributed SSH trust file to all node.
Verifying SSH trust on all hosts.
Successfully verified SSH trust on all hosts.
Successfully created SSH trust.
Successfully created SSH trust for [omm] user.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Warning: Installation environment contains some warning messages.
Please get more details by "/package_r8/sudo/gs_checkos -i A -h host179174,host179175,host179176".
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```
