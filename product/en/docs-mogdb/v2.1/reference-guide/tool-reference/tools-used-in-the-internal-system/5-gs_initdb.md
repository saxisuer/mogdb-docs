---
title: gs_initdb
summary: gs_initdb
author: Zhang Cuiping
date: 2021-06-07
---

# gs_initdb

## Overview

**Background**

When a database is initialized using **gs_initdb**, a database directory is created, a system catalog is generated, and a default database and a template database are created.

**System catalog**

A large number of system catalogs and views are generated when a database is initialized, many of which allow access from any database user.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> Permissions on **pg_user_status** and **pg_auth_history** system catalogs are granted only to initialized users and **sysadmin** users.

**Generated database**

- template1: a template database. When you create a database, all content in the template1 database is copied. The settings of template1 are determined by the parameters of **gs_initdb**.
- template0: Is the initial backup database provided by MogDB. template0 can be used to generate an empty database if necessary.
- postgres: a default database provided for users, tools, and third-party applications.

## Usage Guide

**Background**

During installation, you are advised to use the **-D** parameter to call **gs_initdb** to initialize a database. If a database needs to be initialized to rectify a fault, run **gs_initdb**.

- Although **gs_initdb** attempts to create the corresponding data directory, it may not have the permission to do so, because in most cases, the parent directory is owned by user **root**. To create a data directory, create an empty data directory as user **root** first and deliver the ownership of this directory to the database user using **chown**.
- **gs_initdb** is used to set the template1 database and the setting becomes the default setting of other databases.
- **gs_initdb** initializes the default locale and character set encoding of a database. The character set encoding, character encoding sequence (LC_COLLATE), and character set class (LC_CTYPE, such as uppercase letters, lowercase letters, and digits) can be separately set for databases when you create them.

**syntax**

```
gs_initdb [OPTION]... [DATADIR]
```

**Procedure**

1. Log in as the OS user **omm** to the primary database node.

2. Plan the database directory.

   a. Switch to user **root**.

   ```bash
    su - root
   ```

   b. Enter the username and password as prompted.

3. Go to the **/opt/gaussdb** directory and create the **data1** directory.

   ```bash
   cd /opt/gaussdb
   mkdir data1
   ```

   a. Allocate the ownership of the **data1** directory to the database user omm. dbgrp is the user group that the omm belongs to.

   ```bash
   chown omm:dbgrp data1
   ```

   b. Exit user **root**.

   ```sql
   exit
   ```

4. Run the **gs_initdb** command to initialize the database.

   ```bash
   gs_initdb -D /opt/gaussdb/data1 -w "Gauss@123" --nodename='data1'
   ```

## Command Reference

For details about common parameters and uncommon parameters supported by **gs_initdb**, see [Table 1](#parameters) and [Table 2](#uncommon).

**Table 1** Common parameters <a id="parameters"></a>

| Parameter                                                    | Description                                                  | Value Range                                                  |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -A, --auth=METHOD                                            | Specifies the authentication method for local users used in **pg_hba.conf** (host and local rows).Do not use **trust**, which is the default value, unless you trust all local users on your system.<br />NOTICE:<br />If the value is **md5**, manually change the value of **password_encryption_type** in the **postgresql.conf.sample** file to **0** and comment out the parameter for the change to take effect. **gs_initdb** must be used in conjunction with **-W**. | Values of **METHOD**:<br />- trust<br />- reject<br />- **md5** (an insecure algorithm, used for compatibility with earlier versions)<br />- sha256<br />- sm3<br />Default value: **trust** |
| --auth-host=METHOD                                           | Specifies the authentication method for local users over TCP/IP used in **pg_hba.conf** (host rows).<br />The value specified in the **-A** parameter is overwritten when this parameter is specified. | Values of **METHOD**:<br />- trust<br />- reject<br />- **md5** (an insecure algorithm, used for compatibility with earlier versions)<br />- sha256<br />- sm3<br />Default value: **trust** |
| --auth-local=METHOD                                          | Specifies the authentication method for local users using Unix-domain socket connections used in **pg_hba.conf** (local rows).<br />The value specified in the **-A** parameter is overwritten when this parameter is specified. | Values of **METHOD**:<br />- trust<br />- reject<br />- **md5** (an insecure algorithm, used for compatibility with earlier versions)<br />- sha256<br />- sm3<br />Default value: **trust** |
| [-D, --pgdata=]DATADIR                                       | Specifies the data directory.                                | Set **DATADIR** as required.                                 |
| --nodename=NODENAME                                          | Specifies the name of an initialized node.                   | The naming rules for nodes are as follows:<br />- The node name can only be lowercase letters (a-z), underscores (_), number signs (#), digits (0-9), and dollar signs ($).<br />- The node name must start with a lowercase letter (a-z), or underscore (_).<br />- The nodename cannot be an empty string, and cannot exceed 64 characters. |
| -E, --encoding=ENCODING                                      | Specifies the encoding format for a new database.            | If this parameter is specified, the **--locale** option must be added to specify the locale that supports the encoding format. If the **--locale** option is not added, the system default locale is used. If the encoding format in the system default locale is inconsistent with the encoding format specified in this parameter, the database initialization failed.<br />If this parameter is not specified, the encoding format of the system default locale is used. The system default locale and encoding format can be checked using the **locale** command as follows:<br />`omm@linux:~> locale|grep LC_CTYPE LC_CTYPE="en_US.UTF-8"`<br />**UTF-8** indicates the encoding format of the system default locale. |
| --locale=LOCALE                                              | Specifies the default locale for the new database. You can run the **locale -a** command to view available locales, for example, `zh_CN.gbk`. If you do not want to specify a locale, set the parameter to **C**.<br />NOTICE:<br />If the encoding format of the database is set, the encoding format of the user specified area must be consistent with the encoding format set by the user. Otherwise, database initialization fails. | For example, to set the database encoding format to GBK, perform the following steps:<br />1. Run the `locale -a |grep gbk` command to check the locale that supports GBK encoding:<br />`omm@linux:~>  locale -a|grep gbk zh_CN.gbk zh_SG.gbk`<br />2. Add the `--locale=zh_CN.gbk` option when the database is initialized. |
| --dbcompatibility=DBCOMPATIBILITY                            | Specifies the type of the compatible database.               | Value range: A, B, C, and PG, indicating **O**, **MY**, **TD**, and **POSTGRES** databases, respectively. |
| --lc-collate=LOCALE--lc-ctype=LOCALE--lc-messages=LOCALE--lc-monetary=LOCALE--lc-numeric=LOCALE--lc-time=LOCALE | Sets the specified locale for a new database.                | The parameter values must be supported by the OS.<br />NOTE:<br />If the **--lc-collate** parameter is not specified when the database is installed, the default value of **--lc-collate** is **C**. |
| --no-locale                                                  | Equivalent to **--locale=C**.                                | -                                                            |
| --pwfile=FILE                                                | Reads the password for the database administrator from **FILE** during the running of **gs_initdb**. The first row of the file is taken as the password. | The format of **FILE** can be "a relative path+file" or "an absolute path+file". The relative path is relative to the current path. |
| -T, --text-search-config=CFG                                 | Specifies the default text search mode. The value of this parameter is not verified. After the configuration is successful, a log is recorded to notify you of the value of this parameter. | Values of **text-search-config**:<br />- **english**: full-document search<br />- **simple**: common text search<br />Default value: **simple** |
| -U, --username=NAME                                          | Selects the username of the database administrator.          | Value range: normal database users<br />Default value: OS user who runs **gs_initdb** |
| -W, --pwprompt                                               | Prompts users to enter the password for the database administrator during the running of **gs_initdb**. | -                                                            |
| -w, --pwpasswd=PASSWD                                        | Specifies the password of the database administrator by running commands during the running of **gs_initdb** instead of interactive input. | The password must meet the following complexity requirements:<br />- Contain at least eight characters.<br />- Cannot be the same as the username, the current password (ALTER), or the current password in an inverted sequence.<br />- Contain at least three of the following: uppercase characters (A to Z), lowercase characters (a to z), digits (0 to 9), and other characters |
| -C, --enpwdfiledir=DIR                                       | Specifies the directory where the password encrypted is located, using the AES128 encryption algorithm during the running of **gs_initdb**. **gs_initdb** decrypts the password file in this directory and performs the password complexity check on the decrypted password. The password is used as the user's password if the check passes.<br />NOTE:<br />- The decrypted password file must use **gs_guc** to generate **gs_guc encrypt -K Gauss@123 -D Dir**.<br />- If multiple **-w** and **-C** parameters are specified, **gs_initdb** regards the last **-w** or **-C** parameter as the user's requirement (entering a plain-text password or a password encrypted using AES128). | -                                                            |
| -X, --xlogdir=XLOGDIR                                        | Specifies the directory where the transaction logs are stored.<br />The directory must be a directory where an MogDB user has the read and write permissions. | The value must be an absolute path.                          |
| -S, --security                                               | Initializes a database in a secure mode.                     | After the database is initialized using **-S**, the created database user permissions are restricted, and the **public schema** permission cannot be used any more by default. |

**Table 2** Uncommon parameters <a id="uncommon"></a>

| Parameter    | Description                                                  | Value Range                                               |
| :----------- | :----------------------------------------------------------- | :-------------------------------------------------------- |
| -d, -debug   | Prints debugging output from the bootstrap backend. The bootstrap backend is used by **gs_initdb** to create system catalogs. | -                                                         |
| -L DIRECTORY | Specifies the input file path where the database is initialized using **gs_initdb**. This is unnecessary. You will be told if you need to specify their location explicitly. This parameter is used to create a database with specified configuration information. You are advised to copy all subdirectories and files related to startup in the **share/postgresql** directory to avoid impact of other factors. | Path of input files required by the initialized database. |
| -n, -noclean | If this parameter is not specified and **gs_initdb** determines that an error prevents it from creating a database, it removes any files it might have created before discovering that it cannot finish the job. This option inhibits tidying-up and is useful for debugging. | -                                                         |
| -s, -show    | Displays internal settings.                                  | -                                                         |
| -V, -version | Prints the **gs_initdb** version and exits.                  | -                                                         |
| -?, -help    | Displays help about **gs_initdb** command line parameters and exits. | -                                                         |
