---
title: gs_ctl
summary: gs_ctl
author: Zhang Cuiping
date: 2021-06-07
---

# gs_ctl

## Background

**gs_ctl**, provided by MogDB, is used to control the database service. It can be used to start and stop the database service and to query the database status. **gs_ctl** is used by the MogDB management module.

**gs_ctl** is executed by OS user **omm**.

- Start, stop, and restart the MogDB node.
- Reload the configuration files (**postgresql.conf** and **pg_hba.conf**) without stopping the database.
- Perform primary/standby switchover, query primary/standby status, rebuild the database instance, and query the rebuilding status.

## Description

The **gs_ctl** tool can use the following types of parameters:

- Parameters for **option**. For details, see [Table 1](#1).

- Common parameters. For details, see [Table 2](#2).

- Parameters for **start** and **restart**. For details, see [Table 3](#3).

- Parameters for **stop** and **restart**. For details, see [Table 4](#4).

- Parameters for **switchover**. For details, see [Table 5](#5).

- Parameters for **build**. For details, see [Table 6](#6).
**Table 1** Parameters for option <a id="1"></a>

| Parameter  | Description                                                  |
| :--------- | :----------------------------------------------------------- |
| init       | Creates a database.                                          |
| start      | Starts the database.                                         |
| restart    | Restarts the database.                                       |
| build      | Rebuilds the standby or cascaded standby server on the primary server. |
| stop       | Stops the database.                                          |
| reload     | Reloads the configuration files (**postgresql.conf** and **pg_hba.conf**). |
| status     | Displays the running status of the database services.        |
| promote    | Exits the faulty restoration when the running standby server is instructed in the specified data directory, and starts to perform the read and write operations. |
| finishredo | Specifies that the standby server stops playback and is forcibly switched to the primary when the primary server is faulty. |
| failover   | When the primary server is abnormal, switches the standby server to a primary server.<br />After the switchover is successful, run the **gs_om -t refreshconf** command to record the information about the primary and standby servers. Ensure that the **gs_om -t refreshconf** command is executed successfully. Otherwise, the database status will be affected when the database is restarted. |
| switchover | When the primary/standby server relationship is normal, switches the standby server to primary for maintenance purposes. Data will not be lost during the switchover.<br />The **switchover** command takes effect only when it is executed on the standby server to be switched to the primary server. If the **switchover** command is executed on the primary server, it is used only for query.<br />After the switchover is successful, run the **gs_om -t refreshconf** command to record the information about the primary and standby servers. Ensure that the **gs_om -t refreshconf** command is executed successfully. Otherwise, the database status will be affected when the database is restarted.<br />After the **switchover** command is delivered, if the command execution times out, the execution status of the background process may be uncertain. |
| query      | Queries the status between the primary and standby server.   |
| notify     | Specifies the primary and standby servers after startup.     |
| kill       | Sends semaphore to a specified process.                      |
| querybuild | Checks the rebuilding process of the database.               |

**Table 2** Common parameters <a id="2"></a>

| Parameter           | Description                                                  | Value Range                                                  |
| :------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -D, -pgdata=DATADIR | Specifies the data directory.                                | The value of **DATADIR** must be a valid data directory.     |
| -s, -silent         | Displays partial prompt information only for the **promote**, **reload**, **restart**, and **stop** commands. For example, information such as "server promoting" and "server shutting down" is displayed while "waiting for server to shut down" or "server stopped" is not displayed. | -                                                            |
| -t, -timeout=SECS   | Maximum seconds for waiting the completion of database startup, shutdown, or switchover. If the waiting times out, the command ends and exits with a notification that the waiting is not required anymore. Then, you can run the gs_om -t status -detail command or other commands to check whether the database is started, shut down, or switched over successfully. | Value range: an integer (second).<br />Default value: 60 seconds (waiting the completion of database startup, shutdown, or switchover) |
| -V , -version       | Prints the **gs_ctl** version information and exits.         | -                                                            |
| -w                  | Waits until the operation is complete.                       | By default, you need to wait until the operation is complete when the database is started, stopped, or restarted. |
| -W                  | Specifies that you do not need to wait for the operation to complete. | -                                                            |
| -M                  | **-M** must be followed by the **SERVERMODE** parameter, which specifies the startup mode of a database upon startup. | Value range of **SERVERMODE**:<br />- **primary**: The server starts in primary mode.<br />- **standby**: The server starts in standby mode.<br />- **cascade_standby**: The local server starts as a cascaded standby server.<br />- **pending**: The server is in pending mode to be promoted to a primary server or a standby server. |
| -T                  | **-T** is followed by a term. The term is used as the host term when the command for switching the standby server to primary is executed. During the rebuilding, hosts greater than the term will be connected. | Value range of term: an unsigned integer.                    |
| -d                  | Prints more debugging information.                           | None                                                         |
| -q                  | After the generation is completed, it does not start automatically, and needs to be started by the caller. | None                                                         |
| -P PASSWORD         | Specifies the password of the user connecting to the local database. It is used in conjunction with the **-U** parameter.<br />NOTE:<br />When **-U** is not specified, and the authentication mode is **trust**, the setting of the **-P** parameter is invalid. | -                                                            |
| -U USERNAME         | Specifies the user that connects to the database. This parameter can be used only in conjunction with the parameter **notify**, **query**, **failover**, **switchover**, or **querybuild**.<br />For domain users, use the DOMAIN\username format. | Value range: users existing in MogDB.<br />Default value: If this parameter is not specified, the current OS user is used. |
| -?, -help           | Displays the help information related to the **gs_ctl** command line parameters. | -                                                            |

**Table 3** Common parameters for **start** and **restart**<a id="3"></a>

| Parameter           | Description                                                  | Value Range                                                  |
| :------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -c, -core-file      | Allows the **gaussdb** process to generate core files.       | -                                                            |
| -l, -log=FILENAME   | Appends the server log output to the **FILENAME** file.      | The value of **FILENAME** is a file that the user who starts the database service has the permission to operate.<br />For example, the value is a file in the **data** directory. |
| -o OPTIONS          | Specifies options to be passed from MogDB directly to the **gaussdb** command.<br />The options should usually be surrounded by single or double quotation marks to ensure that they are transferred as a whole. | Parameters supported in **gaussdb**.                         |
| -p PATH-TO-POSTGRES | Specifies the location of the executable file of the **gaussdb**. | By default, the gaussdb executable file is taken from the same directory as **gs_ctl**. |

**Table 4** Common parameters for **stop** and **restart**<a id="4"></a>

| Parameter      | Description                  | Value Range                                                  |
| :------------- | :--------------------------- | :----------------------------------------------------------- |
| -m, -mode=MODE | Specifies the disabled mode. | Values of **mode**:<br />- **fast**: All active transactions are rolled back and all clients are disconnected forcibly, and then the server is disabled.<br />- **immediate**: The server is shut down forcibly and fault recovery occurs on the next startup.<br />Default value: **fast** |

**Table 5** Parameters for switchover<a id="5"></a>

| Parameter          | Description                                                  | Value Range                                                  |
| :----------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -m SWITCHOVER-MODE | Specifies the HA switchover mode when the primary and standby servers are normal. | Values of **mode**:<br />- **fast**: All active transactions are rolled back and all clients are disconnected forcibly, and then the server is switched.<br />- **smart**: This mode is not supported in this version.<br />Default value: **fast** |

**Table 6** Parameters for build<a id="6"></a>

| Parameter                 | Description                                                  | Value Range                                                  |
| :------------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -b MODE                   | Specifies the mode of the standby server to be rebuilt.      | Values of **mode**:<br />- **full**: Fully resynchronizes the data directory of the primary server using an image.<br />- **incremental**: Incrementally restores the standby server based on the differences between the data of primary and standby DNs found in Xlogs.<br />NOTE:<br />Incremental rebuilding applies to scenarios where logs are inconsistent between the primary and standby nodes.<br />Incremental rebuilding does not apply to the scenario where one primary and one standby are deployed and the maximum HA is not enabled. In this scenario, you need to perform full rebuilding or enable the maximum HA before performing the incremental rebuilding.<br />The server cannot be restored using the incremental rebuilding method if the standby server data file is damaged or the data directory is lost. Restore the standby server using the incremental rebuilding method.<br />- **auto** (not specified): Selects **incremental** first and then **full** after **incremental** fails for three times.<br />Default value: **auto**<br />NOTE:<br />To rebuild a cascaded standby server, add the **-M cascade_standby** parameter. |
| -r, -recvtimeout=INTERVAL | Specifies the timeout duration that the log of the standby server receives the host response during the rebuilding process. | Value range: an integer (second).<br />Default value: **120s**<br />Note: <br />The timeout interval needs to be set based on the service load. If the service load is heavy, the timeout interval for waiting for a response from the host needs to be prolonged. It is recommended that the timeout interval be equal to the checkpoint execution time plus 1 minute. |
| -q                        | Specifies whether to automatically restart after the rebuilding is complete.<br />If this parameter is specified, the system does not automatically restart.<br />If no scenario is specified, the system automatically restarts. The default maximum waiting time is 60 seconds. If the database fails to be started within this period, the system displays a message indicating that the waiting is not required and the command exits. Then, you can run the gs_om -t status -detail command or other commands to check whether the database is started successfully. | None                                                         |

**Table 7** Parameters for query

| Parameter | Description                                      | Value Range |
| :-------- | :----------------------------------------------- | :---------- |
| -L        | Queries the LSN and displays the maximum length. | None        |
