---
title: GAUSS-01001 - GAUSS-01100
summary: GAUSS-01001 - GAUSS-01100
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01001 - GAUSS-01100

<br/>

## GAUSS-01001 - GAUSS-01010

<br/>

GAUSS-01001: "invalid type category '%s': must be simple ASCII"

SQLSTATE: 22023

Description: The type category is invalid.

Solution: Ensure that the category name consists of simple ASCII characters (ACII value from 22 to 126).

GAUSS-01002: "array element type cannot be %s"

SQLSTATE: 42804

Description: The array element type cannot be the pseudo type.

Solution: Do not use the pseudo type as the array element type.

GAUSS-01003: "alignment '%s' not recognized"

SQLSTATE: 22023

Description: Invalid alignment parameter.

Solution: Ensure that the alignment parameters in the data type definition are valid.

GAUSS-01004: "storage '%s' not recognized"

SQLSTATE: 22023

Description: Invalid storage parameter.

Solution: Ensure that the storage parameters in the data type definition are valid.

GAUSS-01005: "type input function must be specified"

SQLSTATE: 42P17

Description: No input function is specified for the data type.

Solution: Specify an input function.

GAUSS-01006: "type output function must be specified"

SQLSTATE: 42P17

Description: No output function is specified for the data type.

Solution: Specify an output function.

GAUSS-01007: "type modifier output function is useless without a type modifier input function"

SQLSTATE: 42P17

Description: Only a typmod_out function is specified for the data type.

Solution: Specify both typmod_in and typmod_out functions.

GAUSS-01008: "type input function %s must return type %s"

SQLSTATE: 42P17

Description: The return type of the input function is not specified.

Solution: Specify the return type of the input function.

GAUSS-01009: "type output function %s must return type 'cstring'"

SQLSTATE: 42P17

Description: The return type of the output function is not specified.

Solution: Specify the return type of the output function.

GAUSS-01010: "type receive function %s must return type %s"

SQLSTATE: 42P17

Description: The return type of the receive function is not specified.

Solution: Specify the return type of the receive function.

<br/>

## GAUSS-01011 - GAUSS-01020

<br/>

GAUSS-01011: "type send function %s must return type 'bytea'"

SQLSTATE: 42P17

Description: The return type of the send function is not specified.

Solution: Specify the return type of the send function.

GAUSS-01012: "'%s' is not a valid base type for a domain"

SQLSTATE: 42804

Description: The base type of the domain is invalid.

Solution: Ensure that the base type of the domain is b, d, e, or r.

GAUSS-01013: "multiple default expressions"

SQLSTATE: 42601

Description: Multiple default expressions are specified.

Solution: Define only one default expression.

GAUSS-01014: "conflicting NULL/NOT NULL constraints"

SQLSTATE: 42601

Description: NULL/NOT NULL constraints conflict.

Solution: Ensure that NULL/NOTNULL constraints do not conflict.

GAUSS-01015: "CHECK constraints for domains cannot be marked NO INHERIT"

SQLSTATE: 42P17

Description: The check constraint of a domain is set to **NO INHERIT**.

Solution: Do not set the check constraint to **NO INHERIT**.

GAUSS-01016: "unique constraints not possible for domains"

SQLSTATE: 42000

Description: The domain definition contains a unique constraint.

Solution: Check the domain definition statement to ensure that no unique constraint exists.

GAUSS-01017: "primary key constraints not possible for domains"

SQLSTATE: 42601

Description: The domain definition contains a primary key constraint.

Solution: Check the domain definition statement to ensure that no primary key constraint exists.

GAUSS-01018: "exclusion constraints not possible for domains"

SQLSTATE: 42601

Description: The domain definition contains an exclusive constraint.

Solution: Check the domain definition statement to ensure that no exclusive constraint exists.

GAUSS-01019: "foreign key constraints not possible for domains"

SQLSTATE: 42601

Description: The domain definition contains a foreign key constraint.

Solution: Check the domain definition statement to ensure that no foreign key constraint exists.

GAUSS-01020: "specifying constraint deferrability not supported for domains"

SQLSTATE: 0A000

Description: The domain contains the deferrability attribute of a constraint.

Solution: Delete the deferrability attribute from the domain definition statement.

<br/>

## GAUSS-01021 - GAUSS-01030

<br/>

GAUSS-01021: "unrecognized constraint subtype: %d"

SQLSTATE: XX000

Description: The constraint type is invalid.

Solution: Check whether the constraint type defined in the SQL statement is correct.

GAUSS-01022: "%s is not an enum"

SQLSTATE: 42809

Description: The object is not an enum type.

Solution: Ensure that the object is an enum type.

GAUSS-01024: "type attribute 'subtype' is required"

SQLSTATE: 42601

Description: The subtype attribute of the type is not specified.

Solution: Add the subtype attribute to the SQL statement.

GAUSS-01025: "range subtype cannot be %s"

SQLSTATE: 42804

Description: Pseudo type.

Solution: Ensure that range subtype is not the pseudo type.

GAUSS-01026: "range collation specified but subtype does not support collation"

SQLSTATE: 42809

Description: Range collation is specified, but the subtype does not support collation.

Solution: If collation is specified, ensure that the subtype supports collation.

GAUSS-01027: "typmod_in function %s must return type 'integer'"

SQLSTATE: 42P17

Description: The return type of the typmod_in function is not integer.

Solution: Check the tymod_in function and ensure that the return type is integer.

GAUSS-01028: "typmod_out function %s must return type 'cstring'"

SQLSTATE: 42P17

Description: The return type of the typmod_out function is not cstring.

Solution: Check the typmod_out function and ensure that the return type is cstring.

GAUSS-01029: "type analyze function %s must return type 'Boolean'"

SQLSTATE: 42P17

Description: The return type of the analyze function is not Boolean.

Solution: Check the analyze function and ensure that the return type is Boolean.

GAUSS-01030: "operator class '%s' does not accept data type %s"

SQLSTATE: 42804

Description: The operator class does not support this data type.

Solution: Ensure that the subtype matches the operator class.

<br/>

## GAUSS-01031 - GAUSS-01040

<br/>

GAUSS-01031: "data type %s has no default operator class for access method '%s'"

SQLSTATE: 42704

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01032: "range canonical function %s must return range type"

SQLSTATE: 42P17

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01033: "range canonical function %s must be immutable"

SQLSTATE: 42P17

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01034: "range subtype diff function %s must return type double precision"

SQLSTATE: 42P17

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01035: "range subtype diff function %s must be immutable"

SQLSTATE: 42P17

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01036: "column '%s' of table '%s' contains null values"

SQLSTATE: 23502

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01037: "constraint '%s' of domain '%s' does not exist"

SQLSTATE: 42704

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01038: "constraint '%s' of domain '%s' is not a check constraint"

SQLSTATE: 42809

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01039: "column '%s' of table '%s' contains values that violate the new constraint"

SQLSTATE: 23514

Description: Syntax blacklisted.

Solution: Do not use the syntax.

GAUSS-01040: "%s is not a domain"

SQLSTATE: 42809

Description: Syntax blacklisted.

Solution: Do not use the syntax.

<br/>

## GAUSS-01041 - GAUSS-01050

<br/>

GAUSS-01041: "constraint '%s' for domain '%s' already exists"

SQLSTATE: 42710

Description: Syntax blacklisted.

Solution: Syntax blacklisted.

GAUSS-01042: "cannot use table references in domain check constraint"

SQLSTATE: 42P10

Description: Syntax blacklisted.

Solution: Syntax blacklisted.

GAUSS-01043: "cannot use subquery in check constraint"

SQLSTATE: 0A000

Description: Syntax blacklisted.

Solution: Syntax blacklisted.

GAUSS-01044: "cannot use aggregate function in check constraint"

SQLSTATE: 42803

Description: Syntax blacklisted.

Solution: Syntax blacklisted.

GAUSS-01045: "cannot use window function in check constraint"

SQLSTATE: 42P20

Description: Syntax blacklisted.

Solution: Syntax blacklisted.

GAUSS-01046: "domain '%s' constraint '%s' has NULL conbin"

SQLSTATE: XX000

Description: The constraint attribute is null.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01047: "'%s' is not a domain"

SQLSTATE: 42809

Description: The object is not a type.

Solution: Ensure that the object to be processed is a type.

GAUSS-01048: "%s is a table's row type"

SQLSTATE: 42809

Description: This object is a row type of a table and cannot use the **ALTER TYPE** syntax.

Solution: Replace **ALTER TYPE** with **ALTER TABLE**.

GAUSS-01049: "cannot alter array type %s"

SQLSTATE: 42809

Description: The array type cannot be altered.

Solution: The current operation can alter the array element type and automatically alter the array type.

GAUSS-01050: "type '%s' already exists in schema '%s'"

SQLSTATE: 42710

Description: The type already exists in the schema.

Solution: Check whether the type already exists in the schema.

<br/>

## GAUSS-01051 - GAUSS-01060

<br/>

GAUSS-01051: "failed to change schema dependency for type %s"

SQLSTATE: XX000

Description: Failed to change the schema dependency of the type.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01052: "channel name cannot be empty"

SQLSTATE: 22023

Description: The channel name is empty.

Solution: Check the SQL statement and specify a channel name.

GAUSS-01053: "channel name too long"

SQLSTATE: 22023

Description: The channel name is excessively long.

Solution: Check the SQL statement and ensure that the channel name length is valid.

GAUSS-01054: "payload string too long"

SQLSTATE: 22023

Description: The string is too long for the payload type.

Solution: Check the SQL statement and ensure that the payload string length is valid.

GAUSS-01055: "cannot PREPARE a transaction that has executed LISTEN, UNLISTEN, or NOTIFY"

SQLSTATE: 0A000

Description: A transaction that has executed LISTEN, UNLISTEN, or NOTIFY operation cannot be prepared.

Solution: Check the SQL statement and ensure that the prepared transaction has not executed LISTEN, UNLISTEN, or NOTIFY operation.

GAUSS-01056: "too many notifications in the NOTIFY queue"

SQLSTATE: 54000

Description: Excessive notifications exist in the notify queue.

Solution: Retry the failed statement.

GAUSS-01057: "unexpected rewrite result for CREATE TABLE AS SELECT"

SQLSTATE: XX000

Description: The rewriting result of **CREATE TABLE AS SELECT** is incorrect.

Solution: Simplify the **SELECT** statement following **CREATE TABLE AS**.

GAUSS-01058: "no collation was derived for column '%s' with collatable type %s"

SQLSTATE: 42P22

Description: Creating a heap table failed because the attribute column supports attcollation but the attcollation is invalid.

Solution: Ensure that the collation definition in the attribute column is valid when the table is created.

GAUSS-01059: "CREATE TABLE AS specifies too many column names"

SQLSTATE: 42601

Description:**CREATE TABLE AS** specifies too many columns.

Solution: Check the SQL statement and ensure that the number of specified columns is appropriate.

GAUSS-01060: "option '%s' not found"

SQLSTATE: 42704

Description: Option not found.

Solution: Check the SQL statement and ensure that the corresponding option is specified.

<br/>

## GAUSS-01061 - GAUSS-01070

<br/>

GAUSS-01061: "option '%s' provided more than once"

SQLSTATE: 42710

Description: The option is provided more than once.

Solution: Check the SQL statement and ensure that the option is provided only once.

GAUSS-01062: "unrecognized action %d on option '%s'"

SQLSTATE: XX000

Description: The action of the required option is set to an invalid value.

Solution: Check whether the SQL statement parameters are correct.

GAUSS-01063: "foreign-data wrapper '%s' does not exist"

SQLSTATE: 42704

Description: The foreign-data wrapper does not exist.

Solution: Ensure that the foreign-data wrapper has been created.

GAUSS-01064: "foreign-data wrapper '%s' already exists"

SQLSTATE: 42710

Description: The foreign-data wrapper already exists.

Solution: Do not create the foreign-data wrapper again.

GAUSS-01065: "server '%s' does not exist"

SQLSTATE: 42704

Description: The server does not exist.

Solution: Ensure that the server has been created.

GAUSS-01066: "server '%s' already exists"

SQLSTATE: 42710

Description: The server already exists.

Solution: Do not create the server again.

GAUSS-01067: "permission denied to change owner of foreign-data wrapper '%s'"

SQLSTATE: 42501

Description: You do not have permission to change the owner of foreign-data wrapper.

Solution: Change the owner of foreign-data wrapper as a system administrator.

GAUSS-01068: "foreign-data wrapper with OID %u does not exist"

SQLSTATE: 42704

Description: The foreign-data wrapper does not exist.

Solution: Ensure that the foreign-data wrapper has been created.

GAUSS-01069: "foreign server with OID %u does not exist"

SQLSTATE: 42704

Description: The foreign server does not exist.

Solution: Ensure that the foreign server has been created.

GAUSS-01070: "function %s must return type 'fdw_handler'"

SQLSTATE: 42809

Description: The function does not return the fdw_handler type.

Solution: Check the function definition and ensure that the function returns the fdw_handler type.

<br/>

## GAUSS-01071 - GAUSS-01080

<br/>

GAUSS-01071: "permission denied to create foreign-data wrapper '%s'"

SQLSTATE: 42501

Description: You do not have permission to create a foreign-data wrapper.

Solution: Create the foreign-data wrapper as a system administrator.

GAUSS-01072: "permission denied to alter foreign-data wrapper '%s'"

SQLSTATE: 42501

Description: You do not have permission to alter a foreign-data wrapper.

Solution: Alter the foreign-data wrapper as a system administrator.

GAUSS-01073: "cache lookup failed for foreign-data wrapper %u"

SQLSTATE: XX000

Description: Failed to find the required foreign-data wrapper.

Solution: Check whether the settings of GDS, HDFS foreign tables, Roach, and OBS are correct.

GAUSS-01074: "cache lookup failed for foreign server %u"

SQLSTATE: XX000

Description: Failed to find the required foreign-data wrapper.

Solution: Check whether the settings of GDS, HDFS foreign tables, Roach, and OBS are correct.

GAUSS-01075: "user mapping '%s' already exists for server %s"

SQLSTATE: 42710

Description: The user mapping of the server already exists.

Solution: Do not create the user mapping of the server again.

GAUSS-01076: "user mapping '%s' does not exist for the server"

SQLSTATE: 42704

Description: The user mapping of the server does not exist.

Solution: Ensure that the user mapping of the server has been created before using the server.

GAUSS-01077: "cache lookup failed for user mapping %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-01081 - GAUSS-01090

<br/>

GAUSS-01083: "get search path failed"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01084: "get namespace failed"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01086: "unacceptable schema name '%s'"

SQLSTATE: 42939

Description: The schema name is invalid.

Solution: Avoid using the **pg_** prefix. It is reserved for the system catalog.

GAUSS-01087: "cache lookup failed for namespace %u"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01088: "schema '%s' does not exist"

SQLSTATE: 3F000

Description: The schema does not exist.

Solution: Ensure that the schema already exists before using it.

GAUSS-01089: "schema '%s' already exists"

SQLSTATE: 42P06

Description: The schema already exists.

Solution: Do not create the schema.

GAUSS-01090: "cache lookup failed for schema %u"

SQLSTATE: XX000

Description: Failed to find the schema.

Solution: The system catalog is abnormal. Contact technical support.

<br/>

## GAUSS-01091 - GAUSS-01100

<br/>

GAUSS-01091: "cstore.%s is a internal table"

SQLSTATE: XX000

Description: An internal table starting with **cstore.** is vacuumed.

Solution: Do not vacuum internal tables.

GAUSS-01092: "pg_class entry for relid %u vanished during vacuuming"

SQLSTATE: XX000

Description: Failed to find the required table object after the VACUUM operation is complete.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01093: "could not find tuple for database %u"

SQLSTATE: XX000

Description: Failed to find the database object.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01094: "pg_partition entry for partid %u vanished during vacuuming"

SQLSTATE: XX000

Description: Failed to find the required partition object after the VACUUM operation is complete.

Solution: The system catalog is abnormal. Contact technical support.

GAUSS-01095: "unexpected relkind!"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-01096: "%s requires a parameter"

SQLSTATE: 42601

Description: A parameter is missing.

Solution: Ensure that the number of parameters for the SQL statement is correct.

GAUSS-01097: "%s requires a numeric value"

SQLSTATE: 42601

Description: The parameter provided is not of the numeric type.

Solution: Check the SQL statement and provide the correct parameter.

GAUSS-01098: "%s requires a Boolean value"

SQLSTATE: 42601

Description: The parameter provided is not of the Boolean type.

Solution: Check the SQL statement and provide the correct parameter.

GAUSS-01099: "argument of %s must be a name"

SQLSTATE: 42601

Description: The parameter name must comply with the naming conventions.

Solution: Check the parameter and change the parameter value.

GAUSS-01100: "argument of %s must be a type name"

SQLSTATE: 42601

Description: The parameter must be a type name.

Solution: Ensure that a type name is used as the parameter value.
