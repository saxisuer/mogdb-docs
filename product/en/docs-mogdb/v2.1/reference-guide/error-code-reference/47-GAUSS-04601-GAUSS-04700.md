---
title: GAUSS-04601 - GAUSS-04700
summary: GAUSS-04601 - GAUSS-04700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04601 - GAUSS-04700

<br/>

## GAUSS-04601 - GAUSS-04610

<br/>

GAUSS-04601: "Failed on getting IR function : LLVMIRrtrim1!\n"

SQLSTATE: XX000

Description: The ll file corresponding to the IR file contains the function.

Solution: Exclude this function from the ll file corresponding to the IR file or set **enable_codegen** to **off**.

GAUSS-04602: "Failed on getting IR function : LLVMIRbtrim1!\n"

SQLSTATE: XX000

Description: The **LLVMIRbtrim1** function is not found in the current LLVM module. An error is reported to avoid subsequent invalid calling.

Solution: Exclude this function from the ll file corresponding to the IR file or set **enable_codegen** to **off**.

GAUSS-04603: "Failed on getting IR function : LLVMIRbpchareq!\n"

SQLSTATE: XX000

Description: The **LLVMIRbpchareq** function is not found in the current LLVM module. An error is reported to avoid subsequent invalid calling.

Solution: Exclude this function from the ll file corresponding to the IR file or set **enable_codegen** to **off**.

GAUSS-04604: "Cannot get the llvm::Intrinsic::sadd_with_overflow function!\n"

SQLSTATE: XX000

Description: The addition function in the LLVM database needs to be called. If this function is declared failed, an error is reported.

Solution: Contact technical support. Alternatively, check whether the LLVM database and the LLVM storage path are correct.

GAUSS-04605: "Cannot get the llvm::Intrinsic::ssub_with_overflow function!\n"

SQLSTATE: XX000

Description: The subtraction function in the LLVM database needs to be called. If this function is declared failed, an error is reported.

Solution: Contact technical support. Alternatively, check whether the LLVM database and the LLVM storage path are correct.

GAUSS-04606: "Cannot get the llvm::Intrinsic::smul_with_overflow function!\n"

SQLSTATE: XX000

Description: The multiplication function in the LLVM database needs to be called. If this function is declared failed, an error is reported.

Solution: Contact technical support. Alternatively, check whether the LLVM database and the LLVM storage path are correct.

GAUSS-04607: "Unsupport address, cfgpath, storepath options when the filesystem is not HDFS."

SQLSTATE: XX000

Description: When the tablespace is created, only when the **filesystem** option is set to **HDFS**, the **address**, **cfgpath** and **storepath** options are supported.

Solution: Ensure that the values of the **address**, **cfgpath**, and **storepath** options in **CREATE TABLESPACE..LOCATION** indicate the HDFS file system.

GAUSS-04608: "Failed to get storepath from tablespace options."

SQLSTATE: XX000

Description: Failed to obtain a valid **storepath** parameter from the created **tablespace** option.

Solution: Ensure that the **CREATE TABLESPACE..LOCATION** syntax contains the **storepath** option.

GAUSS-04609: "Failed to get cfgpath from tablespace options."

SQLSTATE: XX000

Description: Failed to find the **cfgpath** parameter during HDFS tablespace creation.

Solution: Specify **cfgpath** during HDFS tablespace creation.

GAUSS-04610: "Failed to create directory '%s' on HDFS."

SQLSTATE: 42P17

Description: Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-04611 - GAUSS-04620

<br/>

GAUSS-04611: "tablespace HDFS path '%s' is too long."

SQLSTATE: 42P17

Description: The **cfgpath** path of the HDFS tablespace is too long.

Solution: The configured path cannot exceed 1024 bytes.

GAUSS-04612: "Directory '%s' already in use as a tablespace on HDFS."

SQLSTATE: 55006

Description: The directory where the error is reported exists. Failed to create a tablespace.

Solution: Select another **storepath** parameter value during tablespace creation.

GAUSS-04613: "Failed to drop external directory, because the cfgpath option has not been found from pg_tablespace."

SQLSTATE: XX000

Description: The **cfgpath** parameter is not found in the option of the deleted tablespace when you delete the tablespace.

Solution: Internal system error. In the **pg_tablespace** system catalog, the existing tablespace names are modified abnormally. Contact technical support.

GAUSS-04619: "Resource Pool '%s': object not defined."

SQLSTATE: 42704

Description: The resource pool is not defined.

Solution: Check whether the resource pool is defined.

<br/>

## GAUSS-04621 - GAUSS-04630

<br/>

GAUSS-04622: "Password can't contain more than %d characters."

SQLSTATE: 28P01

Description: The account password cannot contain over *%d* characters.

Solution: Change the password format to ensure that the number of characters does not exceed *%d*.

GAUSS-04623: "invalid GDS command: %d"

SQLSTATE: XX000

Description: Invalid GDS interaction is received.

Solution: Check whether invalid data exists in the imported file of GDS foreign tables, clean the file, and import it again.

GAUSS-04624: "Failed to deserialize command, which type is %d"

SQLSTATE: XX000

Description: Invalid GDS interaction is received.

Solution: Check whether invalid data exists in the imported file of GDS foreign tables, clean the file, and import it again.

GAUSS-04626: "Invalid schema oid: %u"

SQLSTATE: 3F000

Description: Failed to find the schema entered by the user.

Solution:Contact technical support.

GAUSS-04627: "deltamerge: This relation doesn't support vacuum deltamerge operation"

SQLSTATE: 42P01

Description: Failed to merge the table by using the **VACUUM DELTAMERGE** *[table_name]* syntax.

Solution: Ensure that *[table_name]* specifies a delta table in the **VACUUM DELTAMERGE** *[table_name]* syntax.

GAUSS-04629: "gs_switch_relfilenode can be only used by redistribution tool."

SQLSTATE: XX000

Description: The metainformation exchange function is only used for the redistribution tool and not recommended for users.

Solution: The database does not support the function. Contact technical support.

GAUSS-04630: "DFS tablespace can not be used as default tablespace."

SQLSTATE: 22023

Description: The DFS tablespace cannot be used as the default tablespace.

Solution: Do not use the DFS tablespace as the default tablespace.

<br/>

## GAUSS-04631 - GAUSS-04640

<br/>

GAUSS-04631: "It is unsupported to rename database '%s' on DFS tablespace '%s'."

SQLSTATE: 0A000

Description: If the DFS tablespace is used in the database, the database cannot be renamed.

Solution: Ensure that no table in the database uses DFS tablespaces.

GAUSS-04634: "cannot alter text search configuration '%s' because other objects depend on it"

SQLSTATE: 2BP01

Description: This configuration information cannot be changed because the context index configuration items have dependent relationship with other tables or indexes.

Solution: Cancel the dependency and then perform this operation.

GAUSS-04637: "It is not allowed to assign version option for non-dfs table."

SQLSTATE: XX000

Description: An error is reported when the **version** parameter is specified for creating row-store tables or column-store tables in CU format.

Solution: Do not use the **version** parameter when creating row-store tables or column-store tables in CU format.

GAUSS-04638: "Unsupport '%s' option"

SQLSTATE: None

Description: Internal system error.

Solution:Contact technical support.

GAUSS-04639: "Invalid string for 'VERSION' option"

SQLSTATE: XX000

Description: The **version** parameter is invalid.

Solution: Set this parameter to **0.12**.

GAUSS-04640: "Num of partition keys in value-partitioned table should not be zeror"

SQLSTATE: XX000

Description: The partition column is not specified when the HDFS table partitioned by value is created.

Solution: Specify 1 to 4 columns when creating the value-partitioned HDFS table.

<br/>

## GAUSS-04641 - GAUSS-04650

<br/>

GAUSS-04641: "Num of partition keys in value-partitioned table exceeds max allowed num:%d"

SQLSTATE: XX000

Description: The number of partition columns exceeds 4 when the HDFS table partitioned by value is created.

Solution: Specify 1 to 4 columns when creating the value-partitioned HDFS table.

GAUSS-04642: "Unsupport partition strategy '%s' feature for dfs table."

SQLSTATE: XX000

Description: Only the HDFS table partitioned by value is supported. Otherwise, an error is reported.

Solution: Specify the value partition when creating HDFS tables.

GAUSS-04644: "Value partitioned table can only be created on DFS tablespace."

SQLSTATE: 0A000

Description: The value partition can only be created in the DFS table.

Solution: Do not create the value partition for other types of tables except the DFS table.

GAUSS-04645: "It is not supported to truncate foreign table '%s'."

SQLSTATE: 42809

Description: The truncate operation cannot be performed on foreign tables.

Solution: Do not perform the truncate operation on foreign tables.

GAUSS-04646: "It is not supported to truncate non-table '%s'"

SQLSTATE: 42809

Description: The truncate operation is not supported on database objects in abnormal tables.

Solution: Do not perform the truncate operation on database objects in abnormal tables.

GAUSS-04648: "Storage type '%s' is meaningless for column relation"

SQLSTATE: 0A000

Description: The storage type of row-store tables is meaningless for column-store tables.

Solution: Do not set the storage type for the column-store tables.

GAUSS-04649: "column '%s' cannot be served as a value-partitioning column because of its datatype [%s]"

SQLSTATE: 42804

Description: Some types are not supported in value partition.

Solution: Check the documentation and determine the data type supported by the value partition.

GAUSS-04650: "can not set tablespace for partitioned relation"

SQLSTATE: XX000

Description: The tablespace of the partitioned table is changed.

Solution: Do not change the tablespace for partitioned tables.

<br/>

## GAUSS-04651 - GAUSS-04660

<br/>

GAUSS-04651: "It is not supported to rename schema '%s' which includes DFS table '%s'."

SQLSTATE: 42939

Description: Currently, a schema containing the DFS table cannot be renamed using the `ALTER SCHEMA schema_name RENAME TO new_schema_name` syntax.

Solution: Ensure that the `ALTER SCHEMA schema_name RENAME TO new_schema_name` syntax does not contain the DFS table. Delete the DFS table and rename the schema again.

GAUSS-04652: "COPY_ROACH does not implement in CopySendEndOfRow"

SQLSTATE: XX000

Description: Do not use ROACH for COPY TO export.

Solution: Do not use ROACH for COPY TO export.

GAUSS-04653: "the bulkload compatible illegal chars flag is already set before bulkload starts"

SQLSTATE: XX000

Description: Internal system error. The parameter for specifying invalid character compatibility that was imported before has not been cleaned.

Solution:Contact technical support.

GAUSS-04655: "cannot specify bulkload compatibility options in BINARY mode"

SQLSTATE: 42601

Description: Internal system error. The imported CopyState object is not cleared.

Solution:Contact technical support.

GAUSS-04656: "IGNORE_EXTRA_DATA specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42601

Description: The **ignore_extra_data** parameter can only be used to import data.

Solution: Remove the **ignore_extra_data** parameter when the export function is enabled.

GAUSS-04657: "COMPATIBLE_ILLEGAL_CHARS specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42601

Description: The **compatible_illegal_chars** parameter can only be used to import data.

Solution: Remove the **compatible_illegal_chars** parameter when the export function is enabled.

GAUSS-04658: "DATE_FORMAT specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42601

Description: The **date_format** parameter can only be used to import data.

Solution: Remove the **date_format** parameter when the export function is enabled.

GAUSS-04659: "TIME_FORMAT specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42601

Description: The **time_format** parameter can only be used to import data.

Solution: Remove the **time_format** parameter when the export function is enabled.

GAUSS-04660: "TIMESTAMP_FORMAT specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42601

Description: The **timestamp_format** parameter can only be used to import data.

Solution: Remove the **timestamp_format** parameter when the export function is enabled.

<br/>

## GAUSS-04661 - GAUSS-04670

<br/>

GAUSS-04662: "illegal chars conversion may confuse COPY null 0x%x"

SQLSTATE: 42601

Description: The **null** parameter is set to a space or question mark (?), and **compatible_illegal_chars** is set to **true**, indicating the invalid characters are compatible. The imported invalid characters are converted to spaces or question marks, introducing confusion with the **null** parameter, which results in potential import errors.

Solution: Set **null** to other characters except a space or question mark to avoid confusion with invalid characters after they are converted.

GAUSS-04663: "illegal chars conversion may confuse COPY delimiter 0x%x"

SQLSTATE: 42601

Description: The **delimiter** parameter is set to a space or question mark (?), and **compatible_illegal_chars** is set to **true**, indicating the invalid characters are compatible. The imported invalid characters are converted to spaces or question marks, introducing confusion with the **delimiter** parameter, which results in potential import errors.

Solution: Set **delimiter** to other characters except a space or question mark to avoid confusion with invalid characters after they are converted.

GAUSS-04664: "illegal chars conversion may confuse COPY quote 0x%x"

SQLSTATE: XX000

Description: The **quote** parameter is set to a space or question mark (?), and **compatible_illegal_chars** is set to **true**, indicating the invalid characters are compatible. The imported invalid characters are converted to spaces or question marks, introducing confusion with the **quote** parameter, which results in potential import errors.

Solution: Set **quote** to other characters except a space or question mark to avoid confusion with invalid characters after they are converted.

GAUSS-04665: "illegal chars conversion may confuse COPY escape 0x%x"

SQLSTATE: 42601

Description: The **escape** parameter is set to a space or question mark (?), and **compatible_illegal_chars** is set to **true**, indicating the imported invalid characters are compatible. The imported invalid characters are converted to spaces or question marks, introducing confusion with the **escape** parameter, which results in potential import errors.

Solution: Set **escape** to other characters except a space or question mark to avoid confusion with invalid characters after they are converted.

GAUSS-04666: "the bulkload state isn't accordant"

SQLSTATE: XX000

Description: Internal system error. The cached global variable CopyState is inconsistent with the imported CopyState.

Solution:Contact technical support.

GAUSS-04667: "Failed to get import task for dn:%s"

SQLSTATE: XX000

Description: A DN is not assigned any import tasks when you use GDS to import data.

Solution: Try again.

GAUSS-04668: "Receive wrong message %d from GDS."

SQLSTATE: XX000

Description: A DN receives an error message from GDS.

Solution: Ensure the current network is normal and try again.

GAUSS-04669: "Receive incomplete message from GDS."

SQLSTATE: XX000

Description: The DN receives a currently unsupported message from GDS.

Solution: Ensure the current network is normal and try again.

<br/>

## GAUSS-04681 - GAUSS-04690

<br/>

GAUSS-04681: "CUBE is limited to 12 elements"

SQLSTATE: 54011

Description: The number of **CUBE** parameters is less than or equal to 12.

Solution: Modify the SQL statement.

GAUSS-04682: "type %s is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Modify the SQL statement.

GAUSS-04683: "REFERENCES constraint is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use it.

GAUSS-04684: "EXCLUDE constraint is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

GAUSS-04685: "FOREIGN KEY … REFERENCES constraint is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

GAUSS-04686: "CREATE TABLE … INHERITS is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

GAUSS-04687: "CREATE TABLE … WITH OIDS is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

GAUSS-04689: "CURSOR … WITH HOLD is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

GAUSS-04690: "SCROLL CURSOR is not yet supported."

SQLSTATE: 0A000

Description: The feature is not supported.

Solution: Do not use this feature.

<br/>

## GAUSS-04691 - GAUSS-04700

<br/>

GAUSS-04691: "INSENSITIVE CURSOR is not yet supported."

SQLSTATE: 0A000

Description: Feature not supported.

Solution: Do not use this feature.

GAUSS-04693: "For foreign table ROUNDROBIN distribution type is built-in support."

SQLSTATE: 0A000

Description: The ROUNDROBIN distribution information is supported when you create a GDS foreign table. Displaying the added distribution information when you create a foreign table is not supported.

Solution: Delete the specified distribution information.

GAUSS-04695: "Value-based partition table should have one column at least"

SQLSTATE: 42601

Description: The value-based partitioned table has at least one column.

Solution:Contact technical support.

GAUSS-04696: "Value-Based partition table creation encounters unexpected data in unnecessary fields"

SQLSTATE: XX000

Description: Unexpected data is generated when the value-based partitioned table is created.

Solution:Contact technical support.

GAUSS-04697: "too many grouping sets present (max 4096)"

SQLSTATE: 54001

Description: The number of groups cannot exceed 4096.

Solution: Modify the SQL statement.

GAUSS-04698: "arguments to GROUPING must be grouping expressions of the associated query level"

SQLSTATE: 42803

Description: Failed to find the **grouping** parameter in the **GROUP BY** clause.

Solution: Modify the SQL statement.

GAUSS-04699: "GROUPING must have fewer than 32 arguments"

SQLSTATE: 54023

Description: The number of **grouping** parameters is less than 32.

Solution: Modify the SQL statement.

GAUSS-04700: "ngram parser only support UTF8/GBK encoding"

SQLSTATE: XX000

Description: The **NGRAM** segmentation algorithm supports only the UTF8 or GBK database encoding format.

Solution:**NGRAM** is a segmentation algorithm to support Chinese full text retrieval. Ensure that the database encoding is UTF8 or GBK format when the **NGRAM** is used.
