---
title: GAUSS-03801 - GAUSS-03900
summary: GAUSS-03801 - GAUSS-03900
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03801 - GAUSS-03900

<br/>

## GAUSS-03801 - GAUSS-03810

<br/>

GAUSS-03801: "cannot perform DELETE RETURNING on relation '%s'"

SQLSTATE: 0A000

Description: An error occurs when the delete returning operation is performed on a table because the INSTEAD rule contains **RETURNING** but the original statement does not.

Solution: Use the unconditional ON DELETE DO INSTEAD rule that contains the **RETURNING** clause.

GAUSS-03802: "WITH cannot be used in a query that is rewritten by rules into multiple queries"

SQLSTATE: 0A000

Description: Using **WITH** in multiple queries is not allowed.

Solution: Check the usage of rules.

GAUSS-03803: "Unexpected commandType or intoClause is not set properly"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03804: "cannot push down CurrentOfExpr"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03805: "expected to find SELECT subquery"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03806: "could not find rule placeholders"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03807: "conditional utility statements are not implemented"

SQLSTATE: 0A000

Description: The conditional utility statements cannot be used.

Solution: Do not perform this operation because the function is not implemented.

GAUSS-03808: "replace_rte_variables inserted a SubLink, but has noplace to record it"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03809: "WHERE CURRENT OF on a view is not implemented"

SQLSTATE: 0A000

Description: The **WHERE CURRENT OF** clause cannot be used in a view.

Solution: Do not perform this operation because the function is not implemented.

GAUSS-03810: "unexpected varattno %d in expression to be mapped"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03811 - GAUSS-03820

<br/>

GAUSS-03811: "rule '%s' for relation '%s' already exists"

SQLSTATE: 42710

Description: The rule to be inserted into a table exists.

Solution: Do not insert the rule if it exists.

GAUSS-03812: "rule actions on OLD are not implemented"

SQLSTATE: 0A000

Description: The OLD value rule is not implemented.

Solution: Use a view or trigger to avoid the problem.

GAUSS-03813: "rule actions on NEW are not implemented"

SQLSTATE: 0A000

Description: The NEW value rule is not implemented.

Solution: Use a trigger to avoid the problem.

GAUSS-03814: "INSTEAD NOTHING rules on SELECT are not implemented"

SQLSTATE: 0A000

Description: The INSTEAD NOTHING rule ON SELECT is not supported.

Solution: Check the usage of rules.

GAUSS-03815: "multiple actions for rules on SELECT are not implemented"

SQLSTATE: 0A000

Description: The rule has more than one action.

Solution: Simplify the rule actions and retain only one.

GAUSS-03816: "rules on SELECT must have action INSTEAD SELECT"

SQLSTATE: 0A000

Description: The rule action must be INSTEAD SELECT.

Solution: Correct the defined rule action.

GAUSS-03817: "rules on SELECT must not contain data-modifying statements in WITH"

SQLSTATE: 0A000

Description: The required rule action cannot contain data modification operations in the **WITH** statement.

Solution: Correct the defined rule action.

GAUSS-03818: "event qualifications are not implemented for rules on SELECT"

SQLSTATE: 0A000

Description: The execution of rule actions cannot be restricted by conditions.

Solution: Correct the defined rule action.

GAUSS-03819: "'%s' is already a view"

SQLSTATE: 55000

Description: A view can have only the ON SELECT rule action.

Solution: Correct the defined rule action.

GAUSS-03820: "view rule for '%s' must be named '%s'"

SQLSTATE: 42P17

Description: The rule action of a view must be defined as _RETURN.

Solution: Correct the defined rule action.

<br/>

## GAUSS-03821 - GAUSS-03830

<br/>

GAUSS-03821: "could not convert table '%s' to a view because it is a partitioned table"

SQLSTATE: 55000

Description: Failed to convert a partitioned table to a view.

Solution: Ensure that the table to be converted to a view is a non-partitioned table.

GAUSS-03822: "could not convert table '%s' to a view because it is not empty"

SQLSTATE: 55000

Description: Failed to convert a non-empty table to a view.

Solution: Ensure that the table to be converted to a view is an empty table.

GAUSS-03823: "could not convert table '%s' to a view because it has triggers"

SQLSTATE: 55000

Description: Failed to convert a table containing triggers to a view.

Solution: Ensure that the table to be converted to a view does not have triggers.

GAUSS-03824: "could not convert table '%s' to a view because it has indexes"

SQLSTATE: 55000

Description: Failed to convert a table containing indexes to a view.

Solution: Ensure that the table to be converted to a view does not have indexes.

GAUSS-03825: "could not convert table '%s' to a view because it has child tables"

SQLSTATE: 55000

Description: Failed to convert a table containing sub-tables to a view.

Solution: Ensure that the table to be converted to a view does not have sub-tables.

GAUSS-03826: "cannot have multiple RETURNING lists in a rule"

SQLSTATE: 0A000

Description: A rule cannot contain multiple RETURNING lists.

Solution: Check the number of RETURNING lists.

GAUSS-03827: "RETURNING lists are not supported in conditional rules"

SQLSTATE: 0A000

Description: The RETURNING lists are used in the conditional rules.

Solution: Delete the RETURNING lists.

GAUSS-03828: "RETURNING lists are not supported in non-INSTEAD rules"

SQLSTATE: 0A000

Description: The RETURNING lists are used in the non-INSTEAD rules.

Solution: Delete the RETURNING lists.

GAUSS-03829: "SELECT rule's target list has too many entries"

SQLSTATE: 42P17

Description: The SELECT rules target list has more entries than the required number of columns.

Solution: Check the number of columns and delete the unnecessary columns.

GAUSS-03830: "cannot convert relation containing dropped columns to view"

SQLSTATE: 0A000

Description: Failed to convert a table containing dropped columns to a view.

Solution: Check the structure of the table to be converted to a view.

<br/>

## GAUSS-03831 - GAUSS-03840

<br/>

GAUSS-03831: "SELECT rule's target entry %d has different column name from '%s'"

SQLSTATE: 42P17

Description: The column names in the SELECT rule are different.

Solution: Check the column name in the SELECT rule target entry.

GAUSS-03832: "SELECT rule's target entry %d has different type from column '%s'"

SQLSTATE: 42P17

Description: The column types in the SELECT rule are different.

Solution: Check the column type in the SELECT rule target entry.

GAUSS-03833: "SELECT rule's target entry %d has different size from column '%s'"

SQLSTATE: 42P17

Description: The sizes of columns in the SELECT rule are different.

Solution: Check the column size in the SELECT rule target entry.

GAUSS-03834: "SELECT rule's target list has too few entries"

SQLSTATE: 42P17

Description: The number of columns entered in the SELECT rule is too small.

Solution: Check the number of columns entered in the SELECT rule.

GAUSS-03835: "rule '%s' for relation '%s' does not exist"

SQLSTATE: 42704

Description: The rules to be disabled or enabled does not exist in the corresponding table.

Solution: Check the table rules.

GAUSS-03836: "rule '%s' does not exist"

SQLSTATE: 42704

Description: The rules to be obtained do not exist in the corresponding table.

Solution: Check the table rules.

GAUSS-03837: "there are multiple rules named '%s'"

SQLSTATE: 42710

Description: Multiple rules with the same name are found.

Solution: Check the table rules.

GAUSS-03838: "Column store don't support this system column"

SQLSTATE: XX000

Description: Column storage does not support this system column.

Solution: Check the involved system column.

<br/>

## GAUSS-03841 - GAUSS-03850

<br/>

GAUSS-03841: "vector scan for VecMarkPos is not yet implemented "

SQLSTATE: XX000

Description: Internal system error. The MarkPos function for the column scanning node is not implemented.

Solution: Internal system error. Contact technical support.

GAUSS-03842: "VecResult for VecMarkPos is not yet implemented "

SQLSTATE: XX000

Description: Internal system error. The MarkPos function for the column-store result node is not implemented.

Solution: Internal system error. Contact technical support.

GAUSS-03844: "vector scan for VecRestrPos is not yet implemented "

SQLSTATE: XX000

Description: Internal system error. The RestrPos function for the column scanning node is not implemented.

Solution: Internal system error. Contact technical support.

GAUSS-03845: "VecResult for VecRestrPos is not yet implemented "

SQLSTATE: XX000

Description: Internal system error. The RestrPos function for the column-store result node is not implemented.

Solution: Internal system error. Contact technical support.

GAUSS-03848: "Not support pushing predicate with none-const external param"

SQLSTATE: 0A000

Description: Non-constant external parameters cannot be pushed down.

Solution: Check parameters.

GAUSS-03849: "could not find tuple for operator %u"

SQLSTATE: XX000

Description: Failed to obtain the required corresponding information from the **pg_operator** system catalog based on the ID.

Solution: Check the **pg_operator** system catalog for confirmation.

<br/>

## GAUSS-03851 - GAUSS-03860

<br/>

GAUSS-03851: "UnSupported vector aggregation function %u"

SQLSTATE: XX000

Description: An incorrect execution plan is generated.

Solution: Retain the plan. Contact technical support.

GAUSS-03853: "unsupported aggregation type"

SQLSTATE: XX000

Description: Generated plan error.

Solution: Retain the plan. Contact technical support.

GAUSS-03854: "VecRemoteQuery ReScan is not yet implemented"

SQLSTATE: 0A000

Description: VecRemoteQuery rescanning cannot be implemented.

Solution: Check the scan statement.

GAUSS-03857: "Unsupported window function %s in vector engine"

SQLSTATE: XX000

Description: Usage not supported.

Solution: Modify the SQL statement.

GAUSS-03860: "Un-support CHECK constraint"

SQLSTATE: XX000

Description: Column storage does not support the CHECK constraint.

Solution: Check the syntax and remove the CHECK constraint.

<br/>

## GAUSS-03861 - GAUSS-03870

<br/>

GAUSS-03862: "unsupport join type %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03864: "unrecognized running state: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03865: "ExecReScanVecModifyTable is not implemented"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03866: "could not write into hash-join temporary file: %m"

SQLSTATE: XX000

Description: An error occurs when the **hash-join** temporary file is written.

Solution: Check the file and user permissions.

GAUSS-03867: "could not write to hashAgg temporary file: %m"

SQLSTATE: XX000

Description: An error occurs when the **hashAgg** temporary file is written.

Solution: Check the file and user permissions.

GAUSS-03868: "could not read from hashAgg temporary file: %m"

SQLSTATE: XX000

Description: An error occurs when the **hashAgg** temporary file is read.

Solution: Check the file and user permissions.

GAUSS-03869: "UnSupported vector function %s"

SQLSTATE: XX000

Description: Internal system error. Failed to vectorize the function.

Solution: Contact technical support.

GAUSS-03870: "Unsupported array reference expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The array reference expression is not vectorized.

Solution: Contact technical support.

<br/>

## GAUSS-03871 - GAUSS-03880

<br/>

GAUSS-03873: "Unsupported field expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The field select expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03874: "Unsupported field store expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The field store expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03875: "Unsupported convert row type expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The convert row type expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03876: "Unsupported array expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The array expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03877: "Unsupported xml expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The XML expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03878: "Unsupported coerce to domain expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The coerce to domain expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03879: "Unsupported coerce to domain value expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The coerce to domain value expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03880: "Unsupported current of expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The **CURRENT OF** expression function is not vectorized.

Solution: Internal system error. Contact technical support.

<br/>

## GAUSS-03881 - GAUSS-03890

<br/>

GAUSS-03882: "Unsupported array coerce expression in vector engine"

SQLSTATE: XX000

Description: Internal system error. The array coerce expression function is not vectorized.

Solution: Internal system error. Contact technical support.

GAUSS-03888: "redundant options"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03891 - GAUSS-03900

<br/>

GAUSS-03891: "Failed to initialize Cgroup. Please check if workload manager is enabled and Cgroups have been created!"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03893: "invalid control group: %s"

SQLSTATE: XX000

Description: The cgroup name is invalid.

Solution: Check the cgroup name.

GAUSS-03897: "Resource Pool '%s': object not defined"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03899: "read workload_gpname failed."

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03900: "Workload Group '%s': object not defined"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.
