---
title: GAUSS-03501 - GAUSS-03600
summary: GAUSS-03501 - GAUSS-03600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03501 - GAUSS-03600

<br/>

## GAUSS-03501 - GAUSS-03510

<br/>

GAUSS-03501: "value too long for restore point (maximum %d characters)"

SQLSTATE: 22023

Description: The length of the restore point name exceeds 63 bytes.

Solution: Recreate the restore point with a short name.

GAUSS-03502: "could not parse transaction log location '%s'"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03504: "recovery is not in progress"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03505: "invalid input syntax for transaction log location: '%s'"

SQLSTATE: 22P02

Description: The input parameter is invalid.

Solution: Ensure that the function parameter is correct. Then, perform the operation again.

GAUSS-03507: "could not access status of transaction %lu"

SQLSTATE: XX000

Description: Failed to obtain the file status.

Solution: Check whether the file path and permission are correct.

GAUSS-03508: "unrecognized SimpleLru error Description: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03509: "cannot make new WAL entries during recovery"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03511 - GAUSS-03520

<br/>

GAUSS-03513: "xlog write request %X/%X is past end of log %X/%X"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03514: "xlog flush request %X/%X is not satisfied - flushed only to %X/%X"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03515: "not enough data in file '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03516: "InstallXLogFileSegment should not have failed"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03517: "invalid XLogFileRead source %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03518: "could not open transaction log directory '%s': %m"

SQLSTATE: XX000

Description: Failed to open the **pg_xlog** folder.

Solution: Check whether the file path and permission are correct.

GAUSS-03519: "required WAL directory '%s' does not exist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03520: "could not create missing directory '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03521 - GAUSS-03530

<br/>

GAUSS-03523: "syntax error in history file: %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03524: "invalid data in history file: %s"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03525: "invalid data in history file '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03527: "sizeof(ControlFileData) is larger than PG_CONTROL_SIZE; fix either one"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03529: "could not write to control file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03530: "could not fsync control file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03531 - GAUSS-03540

<br/>

GAUSS-03531: "could not close control file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03532: "could not open control file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03533: "could not read from control file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03534: "database files are incompatible with server"

SQLSTATE: XX000

Description: Database files are incompatible with the software version.

Solution: Use the correct software version to rebuild the database.

GAUSS-03535: "incorrect checksum in control file"

SQLSTATE: XX000

Description: The content of the **pg_control** file is abnormal.

Solution: Use the backup data to restore the **pg_control** file or rebuild the database.

GAUSS-03536: "could not write bootstrap transaction log file: %m"

SQLSTATE: XX000

Description: The disk space is insufficient.

Solution: Ensure that the disk space is sufficient and restart the database.

GAUSS-03537: "could not fsync bootstrap transaction log file: %m"

SQLSTATE: XX000

Description: The file permission is incorrect.

Solution: Check whether the file permission is correct and restart the database.

GAUSS-03538: "could not close bootstrap transaction log file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03539: "could not open recovery command file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03540: "recovery_target_timeline is not a valid number: '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03541 - GAUSS-03550

<br/>

GAUSS-03541: "recovery_target_xid is not a valid number: '%s'"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03542: "recovery_target_name is too long (maximum %d characters)"

SQLSTATE: 22023

Description: The recovery target name is too long.

Solution: Shorten the recovery target name and restore the database.

GAUSS-03543: "unrecognized recovery parameter '%s'"

SQLSTATE: XX000

Description: Failed to configure the recovery parameter.

Solution: Configure the correct recovery parameter and restore the database.

GAUSS-03544: "recovery command file '%s' must specify restore_command when standby mode is not enabled"

SQLSTATE: XX000

Description: The recovery command to be configured is empty.

Solution: Configure the correct recovery command and restore the database.

GAUSS-03545: "recovery target timeline %u does not exist"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03546: "hot standby is not possible because %s = %d is a lower setting than on the master server (its value was %d)"

SQLSTATE: 22023

Description: The configured parameter of the standby server is less than that of the primary server.

Solution: Modify the configuration and restart the database.

GAUSS-03548: "control file contains invalid data"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03549: "requested timeline %u is not a child of database system timeline %u"

SQLSTATE: XX000

Description: The backup WAL file is inconsistent with that in the database.

Solution: Ensure that the WAL file is consistent with that in the database, and restore the database.

GAUSS-03550: "could not find redo location referenced by checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03551 - GAUSS-03560

<br/>

GAUSS-03551: "could not locate required checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03552: "could not locate a valid checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03553: "invalid next transaction ID"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03554: "invalid redo in checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03555: "invalid redo record in shutdown checkpoint"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03556: "backup_label contains data inconsistent with control file"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03558: "requested recovery stop point is before consistent recovery point"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03559: "WAL ends before end of online backup"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03560: "WAL ends before consistent recovery point"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03561 - GAUSS-03570

<br/>

GAUSS-03562: "concurrent transaction log activity while database system is shutting down"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03563: "online backup was canceled, recovery cannot continue"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03564: "unexpected timeline ID %u (after %u) in checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03565: "unexpected timeline ID %u (should be %u) in checkpoint record"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03566: "unrecognized wal_sync_method: %d"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03571 - GAUSS-03580

<br/>

GAUSS-03571: "WAL level not sufficient for making an online backup"

SQLSTATE: 55000

Description: The WAL level is insufficient.

Solution: Set the WAL level to **archive** or **hot_standby** and perform a backup operation after the database is restarted.

GAUSS-03572: "backup label too long (max %d bytes)"

SQLSTATE: 22023

Description: The length of the backup path name exceeds the limit.

Solution: Change the backup path name and back up again.

GAUSS-03573: "a backup is already in progress"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03574: "WAL generated with full_page_writes=off was replayed since last restartpoint"

SQLSTATE: 55000

Description: The configuration item **full_page_writes** is set to **off**.

Solution: Set the configuration item **full_page_writes** to **on**, perform **CHECKPOINT** on the host, and back up again.

GAUSS-03575: "could not write file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03576: "a backup is not in progress"

SQLSTATE: 55000

Description: No backup is in process.

Solution: The **SELECT pg_start_backup** and **SELECT pg_stop_backup** functions are called in pairs. Call the **SELECT pg_start_backup** and **SELECT pg_stop_backup** in sequence. Check whether related backup is started.

GAUSS-03577: "invalid data in file '%s'"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03578: "the standby was promoted during online backup"

SQLSTATE: 55000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03579: "WAL generated with full_page_writes=off was replayed during online backup"

SQLSTATE: 55000

Description: The configuration item **full_page_writes** is set to **off**.

Solution: Set the configuration item **full_page_writes** to **on**, perform **CHECKPOINT** on the host, and back up again.

GAUSS-03580: "invalid record offset at %X/%X."

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03581 - GAUSS-03590

<br/>

GAUSS-03581: "not enough shared memory for pg_lsnxlogflushchk share memory"

SQLSTATE: 53200

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03582: "transaction identifier '%s' is too long"

SQLSTATE: 22023

Description: The transaction identifier parameter is too long.

Solution: Check the input parameter and perform the prepare transaction operation again.

GAUSS-03583: "prepared transactions are disabled"

SQLSTATE: 55000

Description: The configuration item **max_prepared_transactions** is set to **0**.

Solution: Set the configuration item **max_prepared_transactions** to a value other than **0**.

GAUSS-03584: "transaction identifier '%s' is already in use"

SQLSTATE: 42710

Description: The transaction identifier parameter is in use.

Solution: Check the input parameter and perform the prepare transaction operation again.

GAUSS-03585: "maximum number of prepared transactions reached"

SQLSTATE: 53200

Description: The value of the configuration item **max_prepared_transactions** is small.

Solution: Set the configuration item **max_prepared_transactions** to a rational value or submit and roll back some transactions.

GAUSS-03586: "prepared transaction with identifier '%s' is busy"

SQLSTATE: 55000

Description: The transaction identifier parameter is in use.

Solution: Perform the operation again.

GAUSS-03587: "permission denied to finish prepared transaction"

SQLSTATE: 42501

Description: The permission is incorrect.

Solution: Terminate the prepared transactions as a transaction initiator or an administrator.

GAUSS-03588: "prepared transaction belongs to another database"

SQLSTATE: 0A000

Description: Failed to connect the database.

Solution: Ensure that the database is correct and connect to the database again.

GAUSS-03589: "prepared transaction with identifier '%s' does not exist"

SQLSTATE: 42704

Description: The transaction identifier parameter does not exist.

Solution: Check the parameter input and perform the operation again.

GAUSS-03590: "failed to find %p in GlobalTransaction array"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-03591 - GAUSS-03600

<br/>

GAUSS-03591: "failed to find GlobalTransaction for xid %lu"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03594: "could not write two-phase state file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03596: "could not close two-phase state file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03598: "could not recreate two-phase state file '%s': %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-03599: "could not fsync two-phase state file: %m"

SQLSTATE: XX000

Description:Internal system error.

Solution:Contact technical support.
