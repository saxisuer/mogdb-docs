---
title: GAUSS-06801 - GAUSS-06900
summary: GAUSS-06801 - GAUSS-06900
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-06801 - GAUSS-06900

<br/>

## GAUSS-06801 - GAUSS-06810

<br/>

GAUSS-06801: "invalid datum int array size %d"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06802: "[SonicHash] Unsupport sonic data desc size %d without tuple desc"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06803: "[SonicHash] Unrecognize data type %u with tuple desc"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06804: "[VecSonicHashJoin: write numeric atom occurs error.]"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06805: "m_curFlag should not be NULL"

SQLSTATE: 22004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06806: "[VecSonicHashJoin: load numeric atom occurs error.]"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06807: "Unrecognized vector sonic hashjoin run status."

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06808: "Unrecognize data type %u when choosing match functions from inner hash keys."

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06809: "Unrecognize data type %u when choosing match functions from outer hash keys."

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06810: "SonicHashJoin reportSorthashinfo: Unsupport report type %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06811 - GAUSS-06820

<br/>

GAUSS-06811: "[SonicHash] Unrecognize desc type %d with type oid %u"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06812: "[SonicHash] The number of rows is greater than SONIC_MAX_ROWS %u, which is not supported for hash table construction."

SQLSTATE: P0003

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06813: "[SonicHash] Unrecognized datetype %u, attrlen %d when init hash functions."

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06814: "Unrecognized vector sonic hashagg run status."

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06815: "Unrecognized vector sonic hashagg data status."

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06816: "Unrecognized vector sonic hash aggregation status."

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06817: "[SonicHash] could not rewind sonic hash-join temporary file: %m"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06818: "[SonicHash] Int has unknown type size of %d"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06819: "[VecSonicHashJoin: reading numeric occurs error.]"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06820: "Unimplemented vector node %s"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06821 - GAUSS-06830

<br/>

GAUSS-06821: "unrecognized node type: %s in function ExecVecMarkPos"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06822: "unrecognized node type: %s in ExecRestrPos"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06823: "Type of parameter %d (%s) does not match that when preparing the plan (%s)"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06824: "No value found for parameter %d"

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06825: "Unrecognized RowCompareType: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06826: "Unrecognized nulltesttype: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06827: "nvalues: %d is invalid"

SQLSTATE: 22015

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06828: "udfInfo->allocRows: %d is not enough"

SQLSTATE: 22015

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06829: "UnSupported vector function %u"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06830: "unrecognized paramtype: %d"

SQLSTATE: XX004

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06831 - GAUSS-06840

<br/>

GAUSS-06831: "Unaligned rows for batches need to be expression evaluation"

SQLSTATE: 23514

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06832: "LZ4 decompress failed when deserializing message, return %d, compressed length %d, original length %d"

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06833: "[%s] no such label found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06834: "Unsupported policy filter values"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06835: "%s policy already exists, create failed"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06836: "Unsupported policy type"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06837: "%s no such policy found, alter failed"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06838: "%s policy does not exist, drop failed"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06839: "Cache lookup failed for relation %u"

SQLSTATE: 29P01

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06840: "Do not support policy label on temp table '%s'"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06841 - GAUSS-06850

<br/>

GAUSS-06841: "[%s] no such schema found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06842: "[%s.%s] no such relation found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06843: "[%s.%s] no such view found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06844: "[%s.%s.%s] no such relation column found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06845: "[%s.%s] no such function found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06846: "Column %s already in other label"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06847: "Column name without table"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06848: "%s label already defined"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06849: "Resource %s %s already exists in label %s"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06850: "No such resource %s %s found in label %s"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06851 - GAUSS-06860

<br/>

GAUSS-06851: "Could not find tuple for label %lld"

SQLSTATE: None

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06852: "%s no such label found"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06853: "One or more labels bound to policies - cannot drop"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06854: "Masking policy can only operate on column object."

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06855: "Masking policy can only operate on column of ordinary table."

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06856: "current policy is conflict with exist policy: %s"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06857: "unsupported policy filter values"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06858: "policy %s not exists."

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06859: "app: [%s] is invalid"

SQLSTATE: 42601

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06860: "ip range: [%s] is invalid, please identify"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06861 - GAUSS-06870

<br/>

GAUSS-06861: "role: [%s] is invalid"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06862: "filter: [%s] is invalid"

SQLSTATE: 42809

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06863: "length of session ip buffer should more than 128"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06864: "invalid value for 64-bit integer option '%s': %s"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06865: "Value '%s' of option 'compression' is invalid for timeseries table"

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06866: "Invalid interval string for 'ttl' option"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06867: "Invalid interval range for 'ttl' option"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06868: "Invalid interval string for 'period' option"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06869: "Invalid interval range for 'period' option"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06870: "invalid value for 'append_mode' option"

SQLSTATE: 22023

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06871 - GAUSS-06880

<br/>

GAUSS-06871: "The split flag should exactly be one character and can not be NULL."

SQLSTATE: 2200F

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06872: "%s is not supported by pound parser."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06873: "number of columns (%u) exceeds limit (%d)"

SQLSTATE: 54011

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06874: "index is not correct"

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06875: "data length is not correct"

SQLSTATE: XX001

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06876: "unrecognized batch compress type"

SQLSTATE: 42804

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06877: "the attribute column of user defined CompositeType does not support view type '%s' "

SQLSTATE: 42704

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06878: "unsupport connector type %d"

SQLSTATE: HV00N

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06879: "Invalid file size is found on file: %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06880: "Failed to parse the postscript from string"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06881 - GAUSS-06890

<br/>

GAUSS-06881: "Failed to parse the filefooter from string"

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06882: "Error occurred while reading column %d: ORC and mpp types do not match, ORC type is %s and mpp type is %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06883: "Error occurs while reading orc file %s, detail can be found in dn log of %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06884: "The type of decimal(precision>38) doesn't support predicate pushdown."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06885: "Unsupported var type %u from data type %s(%u)."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06886: "Unsupported data type : %s."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06887: "Error occurs while reading parquet file %s, detail can be found in dn log of %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06888: "Error occurred while reading column %d: PARQUET and mpp types do not match, PARQUET type is %s(%s) and mpp type is %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06889: "Unsupported parquet type : %u."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06890: "Before encoding, the length of data as partition directory name must be less than dfs_partition_directory_length(%d)/3."

SQLSTATE: 53400

Description:Internal system error.

Solution:Contact technical support.

<br/>

## GAUSS-06891 - GAUSS-06900

<br/>

GAUSS-06891: "line is larger than 1GB."

SQLSTATE: 22P04

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06892: "Indicates the restriction's level is undefined."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06893: "get min max statistics false."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06894: "Unsupported data type for min-max filter: %s(%u)."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06895: "Error occurred while reading column %d: carbondata and kernel types do not match, carbondata type is %s and kernel type is %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06896: "Unsupported var type %s from data type %s."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06897: "Error occurred while reading column %d: carbondata and kernel types do not match, carbondata type is %s(%d,%d) and kernel type is %s."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06898: "Unsupported data type : %s for predicate filter."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06899: "Unsupported carbondata type : %s."

SQLSTATE: 0A000

Description:Internal system error.

Solution:Contact technical support.

GAUSS-06900: "Failed to alloc the space to fileFooter."

SQLSTATE: 22000

Description:Internal system error.

Solution:Contact technical support.
