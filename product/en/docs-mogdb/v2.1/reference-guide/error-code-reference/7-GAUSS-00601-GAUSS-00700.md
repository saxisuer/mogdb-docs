---
title: GAUSS-00601 - GAUSS-00700
summary: GAUSS-00601 - GAUSS-00700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00601 - GAUSS-00700

<br/>

## GAUSS-00601 - GAUSS-00610

<br/>

GAUSS-00601: "Permission denied."

SQLSTATE: 42501

Description: You have no permission to run **CREATE/ALTER/DROP ROLE**.

Solution: Ensure that you have permission to run **CREATE/ALTER/DROP ROLE**.

GAUSS-00602: "Separation of privileges is used,user can't be altered because of too many privileges."

SQLSTATE: 42501

Description: The permissions specified for users exceed the limit during privilege separation.

Solution: Ensure that the permissions specified for users do not exceed the limit during privilege separation.

GAUSS-00603: "The account has been locked."

SQLSTATE: 28000

Description: An account is locked due to invalid operations.

Solution: Contact the system administrator to unlock the account. The system administrator will unlock an account after confirming that no invalid operations exist.

GAUSS-00606: "The role's password is null in pg_authid, REPLACE should not be specified."

SQLSTATE: XX000

Description: The user password is not specified, and the value REPLACE is specified.

Solution: Specify the password.

GAUSS-00610: "Permission denied to drop role."

SQLSTATE: 42501

Description: You have no permission to run **DROP ROLE**.

Solution: Ensure that you have permission to run **DROP ROLE**.

<br/>

## GAUSS-00611 - GAUSS-00620

<br/>

GAUSS-00611: "fail to drop the current schema"

SQLSTATE: 55006

Description:**DROP ROLE** cannot be used to delete the schema.

Solution: Do not delete the schema.

GAUSS-00612: "current user cannot be dropped"

SQLSTATE: 55006

Description:**DROP ROLE** cannot be used to delete the user.

Solution: Do not delete the user.

GAUSS-00613: "session user cannot be dropped"

SQLSTATE: 55006

Description:**DROP ROLE** cannot be used to delete the session user.

Solution: Do not delete the session user.

GAUSS-00615: "role '%s' cannot be dropped because some objects depend on it"

SQLSTATE: 2BP01

Description:**DROP ROLE** cannot be used to delete the user that objects depend on.

Solution: Clear the dependency and try again.

GAUSS-00616: "session user cannot be renamed"

SQLSTATE: 0A000

Description: Failed to rename the session user.

Solution: Rename the session user as an authorized user.

GAUSS-00617: "current user cannot be renamed"

SQLSTATE: 0A000

Description: Failed to rename the user.

Solution: Do not rename the user.

GAUSS-00620: "Permission denied to rename role."

SQLSTATE: 42501

Description: You have no permission to rename users.

Solution: Ensure that you have permission to rename users.

<br/>

## GAUSS-00621 - GAUSS-00630

<br/>

GAUSS-00621: "column names cannot be included in GRANT/REVOKE ROLE"

SQLSTATE: 0LP01

Description: The **GRANT/REVOKE ROLE** statement contains column names.

Solution: Ensure that the **GRANT/REVOKE ROLE** statement does not contain column names.

GAUSS-00622: "Permission denied to drop objects."

SQLSTATE: 42501

Description: You have no permission to delete objects.

Solution: Ensure that you have permission to delete objects.

GAUSS-00623: "Permission denied to reassign objects."

SQLSTATE: 42501

Description: You have no permission to reassign objects.

Solution: Ensure that you have permission to reassign objects.

GAUSS-00625: "must have admin option on role '%s'"

SQLSTATE: 42501

Description: System administrators do not have the **SYSADMIN** attribute.

Solution: Specify the **SYSADMIN** attribute for system administrators when creating them.

GAUSS-00626: "must be system admin to set grantor"

SQLSTATE: 42501

Description: You have no permission to set authorizers.

Solution: Set authorizers as a system administrator.

GAUSS-00627: "role '%s' is a member of role '%s'"

SQLSTATE: 0LP01

Description: The target user is a member of a user.

Solution: Check whether the target user is a member of a user.

GAUSS-00628: "schema '%s' doesnot exist"

SQLSTATE: 3F000

Description: The schema does not exist.

Solution: Check whether the schema exists.

GAUSS-00629: "cannot cancel current session's query"

SQLSTATE: 42601

Description: The session query cannot be canceled.

Solution: Check whether the session has locked the objects owned by users.

GAUSS-00630: "md5-password encryption failed."

SQLSTATE: XX000

Description: MD5 fails to encrypt passwords.

Solution:Contact technical support.

<br/>

## GAUSS-00631 - GAUSS-00640

<br/>

GAUSS-00631: "sha256-password encryption failed."

SQLSTATE: XX000

Description: SHA256 failed to be encrypted.

Solution: Contact technical support.

GAUSS-00633: "Password must contain at least %d upper characters."

SQLSTATE: 28P01

Description: The password must contain a certain number of uppercase letters specified by the GUC parameter password_min_uppercase.

Solution: Add uppercase letters to the password to meet the parameter requirement.

GAUSS-00634: "Password must contain at least %d lower characters."

SQLSTATE: 28P01

Description: The password must contain a certain number of lowercase letters specified by the GUC parameter password_min_lowercase.

Solution: Add lowercase letters to the password to meet the parameter requirement.

GAUSS-00635: "Password must contain at least %d digital characters."

SQLSTATE: 28P01

Description: The password must contain a certain number of digits specified by the GUC parameter password_min_digital.

Solution: Add digits to the password to meet the parameter requirement.

GAUSS-00636: "Password must contain at least %d special characters."

SQLSTATE: 28P01

Description: The password must contain a certain number of special characters specified by the GUC parameter password_min_special.

Solution: Add special characters to the password to meet the parameter requirement.

GAUSS-00637: "Password must contain at least three kinds of characters."

SQLSTATE: 28P01

Description: Less than three character types are contained in the password.

Solution: 1. The password contains at least eight characters. 2. The password cannot be the same as the username. 3. The password contains at least three of the following character types: uppercase characters, lowercase characters, digits, and special characters (limited to including: ~!@#$ %^&*()-_=+\|[{}];:,<.>/?. "

GAUSS-00638: "The parameter roleID of CheckPasswordComplexity is NULL"

SQLSTATE: XX000

Description: **roleid** is null. Password complexity checking fails.

Solution: Ensure that **roleid** is not null.

GAUSS-00639: "The parameter newPasswd of CheckPasswordComplexity is NULL"

SQLSTATE: XX000

Description: The new password is null. Password complexity checking fails.

Solution: Enter the password again.

GAUSS-00640: "Password should not equal to the rolname."

SQLSTATE: XX000

Description: The new password and the username are the same.

Solution: Ensure that the password is not the same as the username. Reset the password.

<br/>

## GAUSS-00641 - GAUSS-00650

<br/>

GAUSS-00641: "reverse_string failed, possibility out of memory"

SQLSTATE: XX000

Description: The password string failed to be reversed.

Solution: Check whether the OOM problem has occurred.

GAUSS-00644: "New password should not equal to the reverse of old ones."

SQLSTATE: XX000

Description: The new password and the reversed old password are the same.

Solution: Ensure that the new password differs from the reversed old password.

GAUSS-00645: "the parameter passwd of AddAuthHistory is null"

SQLSTATE: XX000

Description: The password is null when you add the record.

Solution: Ensure that the password is not null.

GAUSS-00646: "sha256-password encryption failed"

SQLSTATE: XX000

Description: SHA256 failed to be encrypted.

Solution: Internal system error.

GAUSS-00647: "The password cannot be reused."

SQLSTATE: XX000

Description: The password cannot be reused.

Solution: Check whether the new password is the same as the old password and meets the reuse requirements.

GAUSS-00648: "TryLockAccount(): roleid is not valid."

SQLSTATE: XX000

Description:**roleid** is invalid.

Solution: Ensure that **roleid** is valid.

GAUSS-00649: "Permission denied."

SQLSTATE: XX000

Description: You have no permission to run **CREATE/ALTER/DROP ROLE**.

Solution: Ensure that you have permission to run **CREATE/ALTER/DROP ROLE**.

GAUSS-00650: "TryLockAccount(): parameter extrafails is less than zero."

SQLSTATE: XX000

Description: The value of **extrafails** is smaller than 0.

Solution: Ensure that the value of **extrafails** is not smaller than 0.

<br/>

## GAUSS-00651 - GAUSS-00660

<br/>

GAUSS-00651: "The tuple of pg_user_status not found"

SQLSTATE: XX000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-00652: "TryUnlockAccount(): roleid is not valid."

SQLSTATE: XX000

Description: **roleid** is invalid.

Solution: Ensure that **roleid** is valid.

GAUSS-00653: "IsAccountLocked(): roleid is not valid."

SQLSTATE: XX000

Description:**roleid** is invalid.

Solution: Ensure that **roleid** is valid.

GAUSS-00654: "getAccountLockedStyle: roleid is not valid."

SQLSTATE: XX000

Description: Internal system error.

Solution: Contact technical support.

GAUSS-00655: "DropUserStatus(): roleid is not valid."

SQLSTATE: XX000

Description: **roleid** is invalid.

Solution: Ensure that **roleid** is valid.

GAUSS-00656: "Invalid username/password,login denied."

SQLSTATE: 42704

Description: The username or password is invalid and the login fails.

Solution: Ensure that the username and password are valid.

GAUSS-00657: "User's passwordtime in pg_auth_history is 0."

SQLSTATE: 28P01

Description:**passwordtime** of the corresponding user in **pg_auth_history** is empty.

Solution: Theoretically, this column is not empty. Contact technical support.

GAUSS-00658: "aggregate stype must be specified"

SQLSTATE: 42P13

Description: When an aggregate function is defined, **stype** is not specified.

Solution: Specify **stype** when defining an aggregate function.

GAUSS-00659: "aggregate sfunc must be specified"

SQLSTATE: 42P13

Description: When an aggregate function is defined, **sfunc** is not specified.

Solution: Specify **sfunc** when defining an aggregate function.

GAUSS-00660: "aggregate input type must be specified"

SQLSTATE: 42P13

Description: When an aggregate function is defined, **input type** is not specified.

Solution: Specify **input type** when defining an aggregate function.

<br/>

## GAUSS-00661 - GAUSS-00670

<br/>

GAUSS-00661: "basetype is redundant with aggregate input type specification"

SQLSTATE: 42P13

Description: When an aggregate function is defined, **basetype** is redundant.

Solution: If the aggregate function is defined using version pg8.2 or later, do not use **basetype** to describe the input type.

GAUSS-00662: "aggregate transition data type cannot be %s"

SQLSTATE: 42P13

Description: The type of the data to be converted by an aggregate function is incorrect.

Solution: Ensure that the type of the data to be converted by the aggregate function is not pseudo. The initial user type to be converted can be internal.

GAUSS-00663: "function %s already exists in schema '%s'"

SQLSTATE: 42723

Description: A function already exists in the schema.

Solution: Ensure that the function name is unique.

GAUSS-00664: "Unsupport feature"

SQLSTATE: XX000

Description: Feature not supported.

Solution: Check the table definition statement.

GAUSS-00665: "column store unsupport constraint '%s'"

SQLSTATE: XX000

Description: Constraints unsupported by the column-store table are defined.

Solution: Use constraints supported by the column-store table, such as NULL, NOT NULL, DEFAULT, and CLUSTER constraints.

GAUSS-00666: "PARTIAL_CLUSTER_ROWS cannot be less than MAX_BATCHROW."

SQLSTATE: 42P16

Description: The value of **PARTIAL_CLUSTER_ROWS** is smaller than that of **MAX_BATCHROW**.

Solution: Ensure that the value of **PARTIAL_CLUSTER_ROWS** is greater than or equal to that of **MAX_BATCHROW**.

GAUSS-00667: "ON COMMIT can only be used on temporary tables"

SQLSTATE: 42P16

Description:**ON COMMIT** is used for non-temporary tables.

Solution: Use **ON COMMIT** for temporary tables only.

GAUSS-00668: "constraints on foreign tables are not supported"

SQLSTATE: 42809

Description: Constraints on foreign tables are defined.

Solution: Do not define constraints on foreign tables.

GAUSS-00669: "cannot create temporary table within security-restricted operation"

SQLSTATE: 42501

Description: Temporary tables are created under security restrictions.

Solution: Do not create temporary tables under security restrictions.

GAUSS-00670: "only shared relations can be placed in pg_global tablespace"

SQLSTATE: 22023

Description: User-defined relations are added to the tablespace of the **pg_global** table.

Solution: Add only shared relations to the tablespace of the **pg_global** table.

<br/>

## GAUSS-00671 - GAUSS-00680

<br/>

GAUSS-00673: "default values on foreign tables are not supported"

SQLSTATE: 42809

Description: When foreign tables are defined, default values are set for columns.

Solution: Do not set default values for columns of foreign tables.

GAUSS-00674: "No Datanode defined in cluster"

SQLSTATE: 42704

Description: No DN is defined.

Solution: Create DNs for the cluster.

GAUSS-00675: "DROP INDEX CONCURRENTLY does not support dropping multiple objects"

SQLSTATE: 0A000

Description: The **DROP INDEX CONCURRENTLY** statement is used to delete multiple objects.

Solution: Do not delete multiple objects.

GAUSS-00676: "DROP INDEX CONCURRENTLY does not support CASCADE"

SQLSTATE: 0A000

Description:**CASCADE** is specified for the **DROP INDEX CONCURRENTLY** statement.

Solution: Do not specify **CASCADE** for the **DROP INDEX CONCURRENTLY** statement.

GAUSS-00677: "unrecognized drop object type: %d"

SQLSTATE: XX000

Description: The object type cannot be dropped.

Solution: Ensure that the object type can be dropped.

GAUSS-00678: "permission denied: '%s' is a system catalog"

SQLSTATE: 42501

Description: You have no permission to delete a system catalog.

Solution: Do not delete a system catalog.

GAUSS-00679: "PGXC does not support RESTART IDENTITY yet"

SQLSTATE: 0A000

Description: **RESTART IDENTITY** is specified for the **TRUNCATE** statement.

Solution: Do not specify **RESTART IDENTITY** for the **TRUNCATE** statement.

<br/>

## GAUSS-00681 - GAUSS-00690

<br/>

GAUSS-00681: "'%s' is not a table"

SQLSTATE: 42809

Description: The object to be truncated is not a table.

Solution: Ensure that the object to be truncated is a table.

GAUSS-00682: "cannot truncate temporary tables of other sessions"

SQLSTATE: 0A000

Description: Temporary tables of other sessions are truncated.

Solution: Do not truncate temporary tables of other sessions.

GAUSS-00683: "tables can have at most %d columns"

SQLSTATE: 54011

Description: The number of columns of a table is greater than 1600.

Solution: Ensure that the number of columns of a table is not greater than 1600.

GAUSS-00684: "cannot inherit from temporary relation '%s'"

SQLSTATE: 42809

Description: A temporary table is inherited.

Solution: Do not inherit temporary tables. Only temporary tables of the session are displayed.

GAUSS-00685: "cannot inherit from temporary relation of another session"

SQLSTATE: 42809

Description: A temporary table is inherited.

Solution: Do not inherit temporary tables. Only temporary tables of the session are displayed.

GAUSS-00686: "relation '%s' would be inherited from more than once"

SQLSTATE: 42P07

Description: A table has been inherited multiple times.

Solution: Check the table creation statement to ensure that no duplicate tables exist in the inherited table.

GAUSS-00687: "inherited column '%s' has a type conflict"

SQLSTATE: 42804

Description: The type of a column inherited is inconsistent with those of other columns.

Solution: Ensure that the type of the column inherited is consistent with those of other columns.

GAUSS-00688: "inherited column '%s' has a collation conflict"

SQLSTATE: 42P21

Description: The collation of the column inherited is inconsistent with those of other columns.

Solution: Ensure that the collation of the column inherited is consistent with those of other columns.

GAUSS-00689: "inherited column '%s' has a storage parameter conflict"

SQLSTATE: 42804

Description: The storage parameter of the column inherited is inconsistent with those of other columns.

Solution: Ensure that the storage parameter of the column inherited is consistent with those of other columns.

GAUSS-00690: "column '%s' has a type conflict"

SQLSTATE: 42804

Description: The type of a column is inconsistent with those of other columns.

Solution: Ensure that the type of the column inherited is consistent with those of other columns.

<br/>

## GAUSS-00691 - GAUSS-00700

<br/>

GAUSS-00691: "column '%s' has a collation conflict"

SQLSTATE: 42P21

Description: The collation of a column is inconsistent with those of other columns.

Solution: Ensure that the collation of the column inherited is consistent with those of other columns.

GAUSS-00692: "column '%s' has a storage parameter conflict"

SQLSTATE: 42804

Description: The storage parameter of the column is inconsistent with those of other columns.

Solution: Ensure that the storage parameter of the column inherited is consistent with those of other columns.

GAUSS-00693: "column '%s' inherits conflicting default values"

SQLSTATE: 42611

Description: The value a column inherits is inconsistent with the default value.

Solution: Ensure that the value a column inherits is consistent with the default value.

GAUSS-00694: "check constraint name '%s' appears multiple times but with different expressions"

SQLSTATE: 42710

Description: The names of check constraints on different expressions occur multiple times.

Solution: Ensure that the names of check constraints on different expressions appear once.

GAUSS-00695: "cannot rename column of typed table"

SQLSTATE: 42809

Description: The column of a typed table is renamed.

Solution: Do not rename the column of a typed table.

GAUSS-00696: "'%s' is not a table, view, composite type, index, or foreign table"

SQLSTATE: 42809

Description: The object to be renamed is not a table.

Solution: Do not rename the column of a view, type, index, or foreign table.

GAUSS-00697: "inherited column '%s' must be renamed in child tables too"

SQLSTATE: 42P16

Description: A renamed inherited column is not synchronized to child tables.

Solution: Synchronize the renamed inherited column to child tables.

GAUSS-00698: "cannot rename system column '%s'"

SQLSTATE: 0A000

Description: The name of a column reserved for the system is modified.

Solution: Do not modify the name of a column reserved for the system.

GAUSS-00699: "cannot rename inherited column '%s'"

SQLSTATE: 42P16

Description: The name of an inherited column is modified.

Solution: Do not modify the name of an inherited column.

GAUSS-00700: "inherited constraint '%s' must be renamed in child tables too"

SQLSTATE: 42P16

Description: A renamed inherited constraint is not synchronized to child tables.

Solution: Synchronize the renamed inherited constraint to child tables.
