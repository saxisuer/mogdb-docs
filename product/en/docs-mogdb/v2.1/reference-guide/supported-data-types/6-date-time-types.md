---
title: Date/Time
summary: Date/Time
author: Guo Huan
date: 2021-04-06
---

# Date/Time

Table 1 lists the date/time types supported by MogDB. For the operators and built-in functions of the types, see Date and Time Processing Functions and Operators.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** If the time format of another database is different from that of MogDB, modify the value of the **DateStyle** parameter to keep them consistent.

**Table 1** Date/Time types

| Name                               | Description                                                  | Storage Space                                  |
| :--------------------------------- | :----------------------------------------------------------- | :--------------------------------------------- |
| DATE                               | Date and time.                                               | 4 bytes (The actual storage space is 8 bytes.) |
| TIME [(p)] [WITHOUT TIME ZONE]     | Time within one day.<br/>**p** indicates the precision after the decimal point. The value ranges from 0 to 6. | 8 bytes                                        |
| TIME [(p)] [WITH TIME ZONE]        | Time within one day (with time zone).<br/>**p** indicates the precision after the decimal point. The value ranges from 0 to 6. | 12 bytes                                       |
| TIMESTAMP[(p)] [WITHOUT TIME ZONE] | Date and time.<br/>**p** indicates the precision after the decimal point. The value ranges from 0 to 6. | 8 bytes                                        |
| TIMESTAMP[(p)][WITH TIME ZONE]    | Date and time (with time zone). TIMESTAMP is also called TIMESTAMPTZ.<br/>**p** indicates the precision after the decimal point. The value ranges from 0 to 6. | 8 bytes                                        |
| SMALLDATETIME                      | Date and time (without time zone).<br/>The precision level is minute. 31s to 59s are rounded into 1 minute. | 8 bytes                                        |
| INTERVAL DAY (l) TO SECOND (p)     | Time interval (X days X hours X minutes X seconds).<br/>- **l**: indicates the precision of days. The value ranges from 0 to 6. For compatibility, the precision functions are not supported.<br/>- **p**: indicates the precision of seconds. The value ranges from 0 to 6. The digit 0 at the end of a decimal number is not displayed. | 16 bytes                                       |
| INTERVAL [FIELDS] [ (p) ]          | Time interval.<br/>- fields:**YEAR**, **MONTH**, **DAY**, **HOUR**, **MINUTE**, **SECOND**, **DAY TO HOUR**, **DAY TO MINUTE**, **DAY TO SECOND**, **HOUR TO MINUTE**, **HOUR TO SECOND**, and **MINUTE TO SECOND**.<br/>- **p**: indicates the precision of seconds. The value ranges from 0 to 6. **p** takes effect only when fields are **SECOND**, **DAY TO SECOND**, **HOUR TO SECOND**, or **MINUTE TO SECOND**. The digit 0 at the end of a decimal number is not displayed. | 12 bytes                                       |
| reltime                            | Relative time interval. The format is as follows:<br/>**X** years **X** mons **X** days **XX:XX:XX**<br/>The Julian calendar is used. It specifies that a year has 365.25 days and a month has 30 days. The relative time interval needs to be calculated based on the input value. The output format is POSTGRES. | 4 bytes                                        |
| abstime                            | Date and time. The format is as follows:<br/>YYYY-MM-DD hh:mm:ss+timezone<br/>The value range is from 1901-12-13 20:45:53 GMT to 2038-01-18 23:59:59 GMT. The precision level is second. | 4 bytes                                        |

Examples

```sql
-- Create a table.
mogdb=# CREATE TABLE date_type_tab(coll date);

-- Insert data.
mogdb=# INSERT INTO date_type_tab VALUES (date '12-10-2010');

-- View data.
mogdb=# SELECT * FROM date_type_tab;
        coll
---------------------
 2010-12-10 00:00:00
(1 row)

-- Drop the table.
mogdb=# DROP TABLE date_type_tab;

-- Create a table.
mogdb=# CREATE TABLE time_type_tab (da time without time zone ,dai time with time zone,dfgh timestamp without time zone,dfga timestamp with time zone, vbg smalldatetime);

-- Insert data.
mogdb=# INSERT INTO time_type_tab VALUES ('21:21:21','21:21:21 pst','2010-12-12','2013-12-11 pst','2003-04-12 04:05:06');

-- View data.
mogdb=# SELECT * FROM time_type_tab;
    da    |     dai     |        dfgh         |          dfga          |         vbg
----------+-------------+---------------------+------------------------+---------------------
 21:21:21 | 21:21:21-08 | 2010-12-12 00:00:00 | 2013-12-11 16:00:00+08 | 2003-04-12 04:05:00
(1 row)

-- Drop the table.
mogdb=# DROP TABLE time_type_tab;

-- Create a table.
mogdb=# CREATE TABLE day_type_tab (a int,b INTERVAL DAY(3) TO SECOND (4));

-- Insert data.
mogdb=# INSERT INTO day_type_tab VALUES (1, INTERVAL '3' DAY);

-- View data.
mogdb=# SELECT * FROM day_type_tab;
 a |   b
---+--------
 1 | 3 days
(1 row)

-- Drop the table.
mogdb=# DROP TABLE day_type_tab;

-- Create a table.
mogdb=# CREATE TABLE year_type_tab(a int, b interval year (6));

-- Insert data.
mogdb=# INSERT INTO year_type_tab VALUES(1,interval '2' year);

-- View data.
mogdb=# SELECT * FROM year_type_tab;
 a |    b
---+---------
 1 | 2 years
(1 row)

-- Drop the table.
mogdb=# DROP TABLE year_type_tab;
```

## Date Input

Date and time input is accepted in almost any reasonable formats, including ISO 8601, SQL-compatible, and traditional POSTGRES. The system allows you to customize the sequence of day, month, and year in the date input. Set the **DateStyle** parameter to **MDY** to select month-day-year interpretation, **DMY** to select day-month-year interpretation, or **YMD** to select year-month-day interpretation.

Remember that any date or time literal input needs to be enclosed with single quotes, and the syntax is as follows:

type [ ( p ) ] 'value'

The **p** that can be selected in the precision statement is an integer, indicating the number of fractional digits in the **seconds** column. Table 2 shows some possible inputs for the **date** type.

**Table 2** Date input

| Example          | Description                                                  |
| :--------------- | :----------------------------------------------------------- |
| 1999-01-08       | ISO 8601 (recommended format). January 8, 1999 in any mode   |
| January 8, 1999  | Unambiguous in any **datestyle** input mode                  |
| 1/8/1999         | January 8 in **MDY** mode. August 1 in **DMY** mode          |
| 1/18/1999        | January 18 in **MDY** mode, rejected in other modes          |
| 01/02/03         | - January 2, 2003 in **MDY** mode<br/>- February 1, 2003 in **DMY** mode<br/>- February 3, 2001 in **YMD** mode |
| 1999-Jan-08      | January 8 in any mode                                        |
| Jan-08-1999      | January 8 in any mode                                        |
| 08-Jan-1999      | January 8 in any mode                                        |
| 99-Jan-08        | January 8 in **YMD** mode, else error                        |
| 08-Jan-99        | January 8, except error in **YMD** mode                      |
| Jan-08-99        | January 8, except error in **YMD** mode                      |
| 19990108         | ISO 8601. January 8, 1999 in any mode                        |
| 990108           | ISO 8601. January 8, 1999 in any mode                        |
| 1999.008         | Year and day of year                                         |
| J2451187         | Julian date                                                  |
| January 8, 99 BC | Year 99 BC                                                   |

Examples

```sql
-- Create a table.
mogdb=# CREATE TABLE date_type_tab(coll date);

-- Insert data.
mogdb=# INSERT INTO date_type_tab VALUES (date '12-10-2010');

-- View data.
mogdb=# SELECT * FROM date_type_tab;
        coll
---------------------
 2010-12-10 00:00:00
(1 row)

-- View the date format.
mogdb=# SHOW datestyle;
 DateStyle
-----------
 ISO, MDY
(1 row)

-- Set the date format.
mogdb=# SET datestyle='YMD';
SET

-- Insert data.
mogdb=# INSERT INTO date_type_tab VALUES(date '2010-12-11');

-- View data.
mogdb=# SELECT * FROM date_type_tab;
        coll
---------------------
 2010-12-10 00:00:00
 2010-12-11 00:00:00
(2 rows)

-- Drop the table.
mogdb=# DROP TABLE date_type_tab;
```

## Time

The time-of-day types are **TIME [(p)] [WITHOUT TIME ZONE]** and **TIME [(p)] [WITH TIME ZONE]**. **TIME** alone is equivalent to **TIME WITHOUT TIME ZONE**.

If a time zone is specified in the input for **TIME WITHOUT TIME ZONE**, it is silently ignored.

For details about the time input types, see Table 3. For details about time zone input types, see Table 4.

**Table 3** Time input types

| Example                              | Description                             |
| :----------------------------------- | :-------------------------------------- |
| 05:06.8                              | ISO 8601                                |
| 4:05:06                              | ISO 8601                                |
| 4:05                                 | ISO 8601                                |
| 40506                                | ISO 8601                                |
| 4:05 AM                              | Same as 04:05. AM does not affect value |
| 4:05 PM                              | Same as 16:05. Input hour must be <= 12 |
| 04:05:06.789-8                       | ISO 8601                                |
| 04:05:06-08:00                       | ISO 8601                                |
| 04:05-08:00                          | ISO 8601                                |
| 040506-08                            | ISO 8601                                |
| 04:05:06 PST                         | Time zone specified by abbreviation     |
| 2003-04-12 04:05:06 America/New_York | Time zone specified by full name        |

**Table 4** Time zone input types

| Example          | Description                              |
| :--------------- | :--------------------------------------- |
| PST              | Abbreviation (for Pacific Standard Time) |
| America/New_York | Full time zone name                      |
| -8:00            | ISO-8601 offset for PST                  |
| -800             | ISO-8601 offset for PST                  |
| -8               | ISO-8601 offset for PST                  |

Examples

```sql
mogdb=# SELECT time '04:05:06';
   time
----------
 04:05:06
(1 row)

mogdb=# SELECT time '04:05:06 PST';
   time
----------
 04:05:06
(1 row)

mogdb=# SELECT time with time zone '04:05:06 PST';
   timetz
-------------
 04:05:06-08
(1 row)
```

## Special Values

The special values supported by MogDB are converted to common date/time values when being read. For details, see Table 5.

**Table 5** Special values

| Input String | Applicable Type       | Description                                    |
| :----------- | :-------------------- | :--------------------------------------------- |
| epoch        | date, timestamp       | 1970-01-01 00:00:00+00 (Unix system time zero) |
| infinity     | timestamp             | Later than any other timestamps                |
| -infinity    | timestamp             | Earlier than any other timestamps              |
| now          | date, time, timestamp | Start time of the current transaction          |
| today        | date, timestamp       | Midnight today                                 |
| tomorrow     | date, timestamp       | Midnight tomorrow                              |
| yesterday    | date, timestamp       | Midnight yesterday                             |
| allballs     | time                  | 00:00:00.00 UTC                                |

## Interval Input

The input of **reltime** can be any valid interval in text format. It can be a number (negative numbers and decimals are also allowed) or a specific time, which must be in SQL standard format, ISO-8601 format, or POSTGRES format. In addition, the text input needs to be enclosed with single quotation marks (").

For details, see Table 6 Interval input.

**Table 6** Interval input

<table>
    <tr>
        <th>Input</th>
        <th>Output</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>60</td>
        <td>2 mons</td>
        <td rowspan="3">Numbers are used to indicate intervals. The default unit is day. Decimals and negative numbers are allowed. Particularly, a negative interval syntactically means how long before.</td>
    </tr>
    <tr>
        <td>31.25</td>
        <td>1 mons 1 days 06:00:00</td>
    </tr>
    <tr>
        <td>-365</td>
        <td>-12 mons -5 days</td>
    </tr>
    <tr>
        <td>1 years 1 mons 8 days 12:00:00</td>
        <td>1 years 1 mons 8 days 12:00:00</td>
        <td rowspan="3">Intervals are in POSTGRES format. They can contain both positive and negative numbers and are case-insensitive. Output is a simplified POSTGRES interval converted from the input.</td>
    </tr>
    <tr>
        <td>-13 months -10 hours</td>
        <td>-1 years -25 days -04:00:00</td>
    </tr>
    <tr>
        <td>-2 YEARS +5 MONTHS 10 DAYS</td>
        <td>-1 years -6 mons -25 days -06:00:00</td>
    </tr>
    <tr>
        <td>P-1.1Y10M</td>
        <td>-3 mons -5 days -06:00:00</td>
        <td rowspan="2">Intervals are in ISO-8601 format. They can contain both positive and negative numbers and are case-insensitive. Output is a simplified POSTGRES interval converted from the input.</td>
    </tr>
    <tr>
        <td>-12H</td>
        <td>-12:00:00</td>
    </tr>
</table>

Examples

```sql
-- Create a table.
mogdb=# CREATE TABLE reltime_type_tab(col1 character(30), col2 reltime);

-- Insert data.
mogdb=# INSERT INTO reltime_type_tab VALUES ('90', '90');
mogdb=# INSERT INTO reltime_type_tab VALUES ('-366', '-366');
mogdb=# INSERT INTO reltime_type_tab VALUES ('1975.25', '1975.25');
mogdb=# INSERT INTO reltime_type_tab VALUES ('-2 YEARS +5 MONTHS 10 DAYS', '-2 YEARS +5 MONTHS 10 DAYS');
mogdb=# INSERT INTO reltime_type_tab VALUES ('30 DAYS 12:00:00', '30 DAYS 12:00:00');
mogdb=# INSERT INTO reltime_type_tab VALUES ('P-1.1Y10M', 'P-1.1Y10M');

-- View data.
mogdb=# SELECT * FROM reltime_type_tab;
              col1              |                col2
--------------------------------+-------------------------------------
 1975.25                        | 5 years 4 mons 29 days
 -2 YEARS +5 MONTHS 10 DAYS     | -1 years -6 mons -25 days -06:00:00
 P-1.1Y10M                      | -3 mons -5 days -06:00:00
 -366                           | -1 years -18:00:00
 90                             | 3 mons
 30 DAYS 12:00:00               | 1 mon 12:00:00
(6 rows)

-- Drop the table.
mogdb=# DROP TABLE reltime_type_tab;
```
