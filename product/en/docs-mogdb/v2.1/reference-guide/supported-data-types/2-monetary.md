---
title: Monetary Types
summary: Monetary Types
author: Guo Huan
date: 2021-04-06
---

# Monetary Types

The money type stores a currency amount with fixed fractional precision.

The range shown in Table 1 assumes there are two fractional digits. Input is accepted in a variety of formats, including integer and floating-point literals, as well as typical currency formatting, such as "$1,000.00". Output is generally in the last format but depends on the locale.

**Table 1** Monetary type

| Name  | Storage Space | Description     | Range                                          |
| :---- | :------------ | :-------------- | :--------------------------------------------- |
| money | 8 bytes       | Currency amount | -92233720368547758.08 to +92233720368547758.07 |

Values of the numeric, int, and bigint data types can be cast to money. Conversion from the real and double precision data types can be done by casting to numeric first, for example:

```sql
mogdb=# SELECT '12.34'::float8::numeric::money;
```

However, this is not recommended. Floating point numbers should not be used to handle money due to the potential for rounding errors.

A money value can be cast to numeric without loss of precision. Conversion to other types could potentially lose precision, and must also be done in two stages:

```sql
mogdb=# SELECT '52093.89'::money::numeric::float8;
```

When a money value is divided by another money value, the result is double precision (that is, a pure number, not money); the currency units cancel each other out in the division.
