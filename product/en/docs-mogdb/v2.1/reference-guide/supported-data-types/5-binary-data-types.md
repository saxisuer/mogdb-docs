---
title: Binary Data Types
summary: Binary Data Types
author: Guo Huan
date: 2021-04-06
---

# Binary Data Types

Table 1 lists the binary data types supported by MogDB.

**Table 1** Binary data types

| Name                           | Description                | Storage Space              |
| :----------------------------- | :------------------------- | :------------------------- |
| BLOB                           | Binary large object (BLOB).<br />NOTE:<br />Column storage cannot be used for the BLOB type. | Its maximum length is 1073733621 bytes (1 GB - 8203 bytes).  |
| RAW                            | Variable-length hexadecimal string.<br />NOTE:<br />Column storage cannot be used for the raw type. | 4 bytes plus the actual hexadecimal string. Its maximum length is 1073733621 bytes (1 GB - 8203 bytes). |
| BYTEA                          | Variable-length binary string.                               | 4 bytes plus the actual binary string. Its maximum length is 1073733621 bytes (1 GB - 8203 bytes). |
| BYTEAWITHOUTORDERWITHEQUALCOL  | Variable-length binary character string (new type for the encryption feature. If the encryption type of the encrypted column is specified as deterministic encryption, the column type is BYTEAWITHOUTORDERWITHEQUALCOL). The original data type is displayed when the encrypted table is printed by running the meta command. | 4 bytes plus the actual binary string. The maximum value is 1073741771 bytes (1 GB - 53 bytes). |
| BYTEAWITHOUTORDERCOL           | Variable-length binary character string (new type for the encryption feature. If the encryption type of the encrypted column is specified as random encryption, the column type is BYTEAWITHOUTORDERCOL). The original data type is displayed when the encrypted table is printed by running the meta command. | 4 bytes plus the actual binary string. The maximum value is 1073741771 bytes (1 GB - 53 bytes). |
| _BYTEAWITHOUTORDERWITHEQUALCOL | Variable-length binary character string, which is a new type for the encryption feature. | 4 bytes plus the actual binary string. The maximum value is 1073741771 bytes (1 GB - 53 bytes). |
| _BYTEAWITHOUTORDERCOL          | Variable-length binary character string, which is a new type for the encryption feature. | 4 bytes plus the actual binary string. The maximum value is 1073741771 bytes (1 GB - 53 bytes). |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - In addition to the size limitation on each column, the total size of each tuple is 1073733621 bytes (1 GB to 8203 bytes).
> - BYTEAWITHOUTORDERWITHEQUALCOL, BYTEAWITHOUTORDERCOL, BYTEAWITHOUTORDERWITHEQUALCOL, and BYTEAWITHOUTORDERCOL cannot be directly used to create a table.

An example is provided as follows:

```sql
-- Create a table.
mogdb=# CREATE TABLE blob_type_t1
(
    BT_COL1 INTEGER,
    BT_COL2 BLOB,
    BT_COL3 RAW,
    BT_COL4 BYTEA
) ;

-- Insert data.
mogdb=# INSERT INTO blob_type_t1 VALUES(10,empty_blob(),
HEXTORAW('DEADBEEF'),E'\\xDEADBEEF');

-- Query data in the table.
mogdb=# SELECT * FROM blob_type_t1;
 bt_col1 | bt_col2 | bt_col3  |  bt_col4
---------+---------+----------+------------
      10 |         | DEADBEEF | \xdeadbeef
(1 row)

-- Delete the table.
mogdb=# DROP TABLE blob_type_t1;
```
