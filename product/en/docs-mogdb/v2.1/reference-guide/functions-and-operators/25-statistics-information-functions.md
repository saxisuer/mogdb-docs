---
title: Statistics Information Functions
summary: Statistics Information Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Statistics Information Functions

Statistics information functions are divided into the following two categories: functions that access databases, using the OID of each table or index in a database to mark the database for which statistics are generated; functions that access servers, identified by the server process ID, whose value ranges from 1 to the number of currently active servers.

- global_comm_get_status()

  Description: Obtains the communication library status of all DNs.

  Return type: record

- global_comm_get_send_stream()

  Description: Obtains the sending stream status of all communication libraries on all DNs.

  Return type: record

- global_comm_get_recv_stream()

  Description: Obtains the receiving stream status of all communication libraries on all DNs.

  Return type: record

- global_comm_get_client_info()

  Description: Obtains information about client connections of global nodes.

  Return type: record

- pg_stat_get_db_conflict_tablespace(oid)

  Description: Specifies the number of queries canceled due to a conflict between the restored tablespace and the deleted tablespace in the database.

  Return type: bigint

- pg_control_group_config

  Description: Prints Cgroup configurations on the current node.

  Return type: record

- pg_stat_get_db_stat_reset_time(oid)

  Description: Specifies the most recent time when database statistics were reset. It is initialized to the system time during the first connection to each database. The reset time is updated when you call **pg_stat_reset** on the database and execute **pg_stat_reset_single_table_counters** against any table or index in it.

  Return type: timestamptz

- pg_stat_get_function_total_time(oid)

  Description: Specifies the total wall clock time spent in the function, in microseconds. The time spent on this function calling other functions is included.

  Return type: bigint

- pg_stat_get_xact_tuples_returned(oid)

  Description: Specifies the number of rows read through sequential scans when the parameter is a table in the current transaction or the number of index entries returned when the parameter is an index.

  Return type: bigint

- pg_lock_status()

  Description: Queries information about locks held by open transactions. All users can execute this function.

  Return type: For details, see **PG_LOCKS** which is obtained by querying this function.

- pg_stat_get_xact_numscans(oid)

  Description: Specifies the number of sequential scans performed when the parameter is a table in the current transaction or the number of index scans performed when the parameter is an index.

  Return type: bigint

- pg_stat_get_xact_blocks_fetched(oid)

  Description: Specifies the number of disk block fetch requests for a table or an index in the current transaction.

  Return type: bigint

- pg_stat_get_xact_blocks_hit(oid)

  Description: Specifies the number of disk block fetch requests for tables or indexes found in cache in the current transaction.

  Return type: bigint

- pg_stat_get_xact_function_calls(oid)

  Description: Specifies the number of times the function is called in the current transaction.

  Return type: bigint

- pg_stat_get_xact_function_self_time(oid)

  Description: Specifies the time spent on this function in the current transaction, excluding the time spent on this function internally calling other functions.

  Return type: bigint

- pg_stat_get_xact_function_total_time(oid)

  Description: Specifies the total wall clock time (in microseconds) spent on the function in the current transaction, including the time spent on this function internally calling other functions.

  Return type: bigint

- pg_stat_get_wal_senders()

  Description: Queries walsender information on the primary server.

  Return type: setofrecord

  The following table describes return columns.

  **Table 1** Return column description

  | Column                     | Type                     | Description                             |
  | :------------------------- | :----------------------- | :-------------------------------------- |
  | pid                        | bigint                   | Thread ID of the WAL sender             |
  | sender_pid                 | integer                  | Lightweight thread ID of the WAL sender |
  | local_role                 | text                     | Type of the primary node                |
  | peer_role                  | text                     | Type of the standby node                |
  | peer_state                 | text                     | Status of the standby node              |
  | state                      | text                     | Status of the WAL sender                |
  | catchup_start              | timestamp with time zone | Startup time of a catchup task          |
  | catchup_end                | timestamp with time zone | End time of a catchup task              |
  | sender_sent_location       | text                     | Sending position of the primary node    |
  | sender_write_location      | text                     | Writing position of the primary node    |
  | sender_flush_location      | text                     | Flushing position of the primary node   |
  | sender_replay_location     | text                     | Redo position of the primary node       |
  | receiver_received_location | text                     | Receiving position of the standby node  |
  | receiver_write_location    | text                     | Writing position of the standby node    |
  | receiver_flush_location    | text                     | Flushing position of the standby node   |
  | receiver_replay_location   | text                     | Redo position of the standby node       |
  | sync_percent               | text                     | Synchronization percentage              |
  | sync_state                 | text                     | Synchronization status                  |
  | sync_priority              | text                     | Priority of synchronous replication     |
  | sync_most_available        | text                     | Maximum availability mode               |
  | channel                    | text                     | Channel information of the WAL sender   |

- get_paxos_replication_info()

  Description: Queries the primary/standby replication status in Paxos mode.

  Return type: setofrecord

  The following table describes return columns.

  **Table 2** Return column description

  | Column                | Type | Description                                                  |
  | :-------------------- | :--- | :----------------------------------------------------------- |
  | paxos_write_location  | text | Location of the Xlog that has been written to the Distribute Consensus Framework (DCF) |
  | paxos_commit_location | text | Location of the Xlog agreed in the DCF                       |
  | local_write_location  | text | Writing position of a node                                   |
  | local_flush_location  | text | Flushing position of a node                                  |
  | local_replay_location | text | Redo position of a node                                      |
  | dcf_replication_info  | text | DCF module information of a node                             |

- pg_stat_get_stream_replications()

  Description: Queries the primary/standby replication status.

  Return type: setofrecord

  The following table describes return values.

  **Table 3** Return value description

  | Return Parameter   | Type    | Description           |
  | :----------------- | :------ | :-------------------- |
  | local_role         | text    | Local role            |
  | static_connections | integer | Connection statistics |
  | db_state           | text    | Database status       |
  | detail_information | text    | Detailed information  |

- pg_stat_get_db_numbackends(oid)

  Description: Specifies the number of active server processes for a database.

  Return type: integer

- pg_stat_get_db_xact_commit(oid)

  Description: Specifies the number of transactions committed in a database.

  Return type: bigint

- pg_stat_get_db_xact_rollback(oid)

  Description: Specifies the number of transactions rolled back in a database.

  Return type: bigint

- pg_stat_get_db_blocks_fetched(oid)

  Description: Specifies the number of disk blocks fetch requests for a database.

  Return type: bigint

- pg_stat_get_db_blocks_hit(oid)

  Description: Specifies the number of disk block fetch requests found in cache for a database.

  Return type: bigint

- pg_stat_get_db_tuples_returned(oid)

  Description: Specifies the number of tuples returned for a database.

  Return type: bigint

- pg_stat_get_db_tuples_fetched(oid)

  Description: Specifies the number of tuples fetched for a database.

  Return type: bigint

- pg_stat_get_db_tuples_inserted(oid)

  Description: Specifies the number of tuples inserted in a database.

  Return type: bigint

- pg_stat_get_db_tuples_updated(oid)

  Description: Specifies the number of tuples updated in a database.

  Return type: bigint

- pg_stat_get_db_tuples_deleted(oid)

  Description: Specifies the number of tuples deleted in a database.

  Return type: bigint

- pg_stat_get_db_conflict_lock(oid)

  Description: Specifies the number of lock conflicts in a database.

  Return type: bigint

- pg_stat_get_db_deadlocks(oid)

  Description: Specifies the number of deadlocks in a database.

  Return type: bigint

- pg_stat_get_numscans(oid)

  Description: Specifies the number of sequential row scans done if parameters are in a table or the number of index scans done if parameters are in an index.

  Return type: bigint

- pg_stat_get_role_name(oid)

  Description: Obtains the username based on the user OID. Only users with the **sysadmin** or **monitor admin** permission can access the information.

  Return type: text

  Example:

  ```sql
  mogdb=# select pg_stat_get_role_name(10);
   pg_stat_get_role_name
  -----------------------
   aabbcc
  (1 row)
  ```

- pg_stat_get_tuples_returned(oid)

  Description: Specifies the number of sequential row scans done if parameters are in a table or the number of index scans done if parameters are in an index.

  Return type: bigint

- pg_stat_get_tuples_fetched(oid)

  Description: Specifies the number of table rows fetched by bitmap scans if parameters are in a table or the number of table rows fetched by simple index scans using the index if parameters are in an index.

  Return type: bigint

- pg_stat_get_tuples_inserted(oid)

  Description: Specifies the number of rows inserted into a table.

  Return type: bigint

- pg_stat_get_tuples_updated(oid)

  Description: Specifies the number of rows updated in a table.

  Return type: bigint

- pg_stat_get_tuples_deleted(oid)

  Description: Specifies the number of rows deleted from a table.

  Return type: bigint

- pg_stat_get_tuples_changed(oid)

  Description: Specifies the total number of inserted, updated, and deleted rows after a table was last analyzed or autoanalyzed.

  Return type: bigint

- pg_stat_get_tuples_hot_updated(oid)

  Description: Specifies the number of rows hot updated in a table.

  Return type: bigint

- pg_stat_get_live_tuples(oid)

  Description: Specifies the number of live rows in a table.

  Return type: bigint

- pg_stat_get_dead_tuples(oid)

  Description: Specifies the number of dead rows in a table.

  Return type: bigint

- pg_stat_get_blocks_fetched(oid)

  Description: Specifies the number of disk block fetch requests for a table or an index.

  Return type: bigint

- pg_stat_get_blocks_hit(oid)

  Description: Specifies the number of disk block requests found in cache for a table or an index.

  Return type: bigint

- pg_stat_get_partition_tuples_inserted(oid)

  Description: Specifies the number of rows in the corresponding table partition.

  Return type: bigint

- pg_stat_get_partition_tuples_updated(oid)

  Description: Specifies the number of rows that have been updated in the corresponding table partition.

  Return type: bigint

- pg_stat_get_partition_tuples_deleted(oid)

  Description: Specifies the number of rows deleted from the corresponding table partition.

  Return type: bigint

- pg_stat_get_partition_tuples_changed(oid)

  Description: Specifies the total number of inserted, updated, and deleted rows after a table partition was last analyzed or autoanalyzed.

  Return type: bigint

- pg_stat_get_partition_live_tuples(oid)

  Description: Specifies the number of live rows in a partitioned table.

  Return type: bigint

- pg_stat_get_partition_dead_tuples(oid)

  Description: Specifies the number of dead rows in a partitioned table.

  Return type: bigint

- pg_stat_get_xact_tuples_fetched(oid)

  Description: Specifies the number of tuple rows scanned in a transaction.

  Return type: bigint

- pg_stat_get_xact_tuples_inserted(oid)

  Description: Specifies the number of tuple inserted into the active subtransactions related to a table.

  Return type: bigint

- pg_stat_get_xact_tuples_deleted(oid)

  Description: Specifies the number of deleted tuples in the active subtransactions related to a table.

  Return type: bigint

- pg_stat_get_xact_tuples_hot_updated(oid)

  Description: Specifies the number of hot updated tuples in the active subtransactions related to a table.

  Return type: bigint

- pg_stat_get_xact_tuples_updated(oid)

  Description: Specifies the number of updated tuples in the active subtransactions related to a table.

  Return type: bigint

- pg_stat_get_xact_partition_tuples_inserted(oid)

  Description: Specifies the number of inserted tuples in the active subtransactions related to a table partition.

  Return type: bigint

- pg_stat_get_xact_partition_tuples_deleted(oid)

  Description: Specifies the number of deleted tuples in the active subtransactions related to a table partition.

  Return type: bigint

- pg_stat_get_xact_partition_tuples_hot_updated(oid)

  Description: Specifies the number of hot updated tuples in the active subtransactions related to a table partition.

  Return type: bigint

- pg_stat_get_xact_partition_tuples_updated(oid)

  Description: Specifies the number of updated tuples in the active subtransactions related to a table partition.

  Return type: bigint

- pg_stat_get_last_vacuum_time(oid)

  Description: Specifies the most recent time when the autovacuum thread is manually started to clear a table.

  Return type: timestamptz

- pg_stat_get_last_autovacuum_time(oid)

  Description: Specifies the time of the last vacuum initiated by the autovacuum daemon on a table.

  Return type: timestamptz

- pg_stat_get_vacuum_count(oid)

  Description: Specifies the number of times a table is manually cleared.

  Return type: bigint

- pg_stat_get_autovacuum_count(oid)

  Description: Specifies the number of times the autovacuum daemon is started to clear a table.

  Return type: bigint

- pg_stat_get_last_analyze_time(oid)

  Description: Specifies the last time when a table starts to be analyzed manually or by the autovacuum thread.

  Return type: timestamptz

- pg_stat_get_last_autoanalyze_time(oid)

  Description: Specifies the time when the last analysis initiated by the autovacuum daemon on a table.

  Return type: timestamptz

- pg_stat_get_analyze_count(oid)

  Description: Specifies the number of times a table is manually analyzed.

  Return type: bigint

- pg_stat_get_autoanalyze_count(oid)

  Description: Specifies the number of times the autovacuum daemon analyzes a table.

  Return type: bigint

- pg_total_autovac_tuples(bool,bool)

  Description: Returns tuple records related to the total autovac, such as **nodename**, **nspname**, **relname**, and tuple IUDs. The input parameters specify whether to query **relation** and **local** information, respectively.

  Return type: setofrecord

  The following table describes return parameters.

  **Table 4** Return parameter description

  | Return Parameter      | Type   | Description                                             |
  | :-------------------- | :----- | :------------------------------------------------------ |
  | nodename              | name   | Node name                                               |
  | nspname               | name   | Name of a namespace                                     |
  | relname               | name   | Name of an object, such as a table, an index, or a view |
  | partname              | name   | Partition name                                          |
  | n_dead_tuples         | bigint | Number of dead rows in a table partition                |
  | n_live_tuples         | bigint | Number of live rows in a table partition                |
  | changes_since_analyze | bigint | Number of changes generated by ANALYZE                  |

- pg_autovac_status(oid)

  Description: Returns autovac information, such as **nodename**, **nspname**, **relname**, **analyze**, **vacuum**, thresholds of **analyze** and **vacuum**, and the number of analyzed or vacuumed tuples. Only users with the **sysadmin** permission can use this function.

  Return type: setofrecord

  The following table describes return parameters.

  **Table 5** Return parameter description

  | Return Parameter | Type    | Description                                             |
  | :--------------- | :------ | :------------------------------------------------------ |
  | nspname          | text    | Name of a namespace                                     |
  | relname          | text    | Name of an object, such as a table, an index, or a view |
  | nodename         | text    | Node name                                               |
  | doanalyze        | Boolean | Whether to execute **ANALYZE**                          |
  | anltuples        | bigint  | Number of ANALYZE tuples                                |
  | anlthresh        | bigint  | ANALYZE threshold                                       |
  | dovacuum         | Boolean | Whether to execute **VACUUM**                           |
  | vactuples        | bigint  | Number of VACUUM tuples                                 |
  | vacthresh        | bigint  | VACUUM threshold                                        |

- pg_autovac_timeout(oid)

  Description: Returns the number of consecutive timeouts during the autovac operation on a table. If the table information is invalid or the node information is abnormal, **NULL** will be returned.

  Return type: bigint

- pg_stat_get_last_data_changed_time(oid)

  Description: Returns the time when **INSERT**, **UPDATE**, **DELETE**, or **EXCHANGE**/**TRUNCATE**/**DROP** **PARTITION** was last performed on a table. The data in the **last_data_changed** column of the PG_STAT_ALL_TABLES view is calculated by using this function. The performance of obtaining the last modification time by using the view is poor when the table has a large amount of data. In this case, you are advised to use the function.

  Return type: timestamptz

- pg_stat_set_last_data_changed_time(oid)

  Description: Manually changes the time when **INSERT**, **UPDATE**, **DELETE**, or **EXCHANGE**/**TRUNCATE**/**DROP** **PARTITION** was last performed.

  Return type: void

- pg_backend_pid()

  Description: Specifies the thread ID of the server thread attached to the current session.

  Return type: integer

- pg_stat_get_activity(integer)

  Description: Returns a record about the backend with the specified PID. A record for each active backend in the system is returned if **NULL** is specified. The return results are a subnet of those (excluding the **connection_info** column) in the PG_STAT_ACTIVITY view.

  Example:

  ```sql
  mogdb=# select * from pg_stat_get_activity(139754904483584);
   datid |       pid       |    sessionid    | usesysid | application_name | state |                    query                     | waiting | xact_start |          query_start          |         backend_start         |         state_cha
  nge          | client_addr | client_hostname | client_port | enqueue | query_id | srespool | global_sessionid
  -------+-----------------+-----------------+----------+------------------+-------+----------------------------------------------+---------+------------+-------------------------------+-------------------------------+------------------
  -------------+-------------+-----------------+-------------+---------+----------+----------+------------------
   16099 | 139754904483584 | 139754904483584 |       10 | coordinator2     | idle  | SET SESSION AUTHORIZATION DEFAULT;RESET ALL; | f       |            | 2021-06-09 16:40:58.391727+08 | 2021-06-09 16:03:52.040929+08 | 2021-06-09 16:40:
  58.392143+08 | ::1         |                 |       35210 |         |        0 | unknown  | 0#0#0
  (1 row)
  ```

  Return type: setofrecord

  The following table describes return parameters.

  **Table 6** Return parameter description

  | Return Parameter | Type                     | Description                                                  |
  | :--------------- | :----------------------- | :----------------------------------------------------------- |
  | datid            | oid                      | OID of the database that the user session connects to in the backend |
  | pid              | bigint                   | Backend thread ID                                            |
  | sessionid        | bigint                   | Session ID                                                   |
  | usesysid         | oid                      | Name of the user logged in to the backend                    |
  | application_name | text                     | Name of the application connected to the backend             |
  | state            | text                     | Overall status of the backend                                |
  | query            | text                     | Latest query at the backend. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
  | waiting          | Boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true**. |
  | xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active). If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
  | query_start      | timestamp with time zone | Time when the currently active query was started, or time when the last query was started if **state** is not **active** |
  | backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
  | state_change     | timestamp with time zone | Time when **state** was last modified                        |
  | client_addr      | inet                     | IP address of the client connected to the backend. If this column is **NULL**, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as **AUTOVACUUM**. |
  | client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of **client\_addr**. This column will be non-null only for IP connections and only when**log_hostname** is enabled. |
  | client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used). |
  | enqueue          | text                     | Unsupported currently                                        |
  | query_id         | bigint                   | ID of a query                                                |
  | global_sessionid | text                     | Global session ID                                            |

- pg_stat_get_activity_with_conninfo(integer)

  Description: Returns a record about the backend with the specified PID. A record for each active backend in the system is returned if **NULL** is specified. PG_STAT_ACTIVITY is a subset of the function.

  Return type: setofrecord

  The following table describes return values.

  **Table 7** Return value description

  | Return Value     | Return Type              | Description                                                  |
  | :--------------- | :----------------------- | :----------------------------------------------------------- |
  | datid            | oid                      | OID of the database that the user session connects to in the backend |
  | pid              | bigint                   | Backend thread ID                                            |
  | sessionid        | bigint                   | Session ID                                                   |
  | usesysid         | oid                      | Name of the user logged in to the backend                    |
  | application_name | text                     | Name of the application connected to the backend             |
  | state            | text                     | Overall status of the backend                                |
  | query            | text                     | Latest query at the backend. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed. |
  | waiting          | Boolean                  | Whether the backend is currently waiting on a lock. If yes, the value is **true** |
  | xact_start       | timestamp with time zone | Time when current transaction was started (null if no transaction is active). If the current query is the first of its transaction, the value of this column is the same as that of the **query_start** column. |
  | query_start      | timestamp with time zone | Time when the currently active query was started, or time when the last query was started if **state** is not **active** |
  | backend_start    | timestamp with time zone | Time when this process was started, that is, when the client connected to the server |
  | state_change     | timestamp with time zone | Time when **state** was last modified                        |
  | client_addr      | inet                     | IP address of the client connected to the backend If this column is **NULL**, it indicates either the client is connected via a Unix socket on the server or this is an internal process, such as **AUTOVACUUM**. |
  | client_hostname  | text                     | Host name of the connected client, as reported by a reverse DNS lookup of **client\_addr**. This column will be non-null only for IP connections and only when**log_hostname** is enabled. |
  | client_port      | integer                  | TCP port number that the client uses for communication with this backend (**-1** if a Unix socket is used) |
  | enqueue          | text                     | Unsupported currently                                        |
  | query_id         | bigint                   | ID of a query                                                |
  | connection_info  | text                     | A string in JSON format recording the driver type, driver version, driver deployment path, and process owner of the connected database |
  | global_sessionid | text                     | Global session ID                                            |

- pg_user_iostat(text)

  Description: Displays the I/O load management information about the job currently executed by the user.

  Return type: record

  The following table describes return fields.

  | Name           | Type | Description                                                  |
  | :------------- | :--- | :----------------------------------------------------------- |
  | userid         | oid  | User ID                                                      |
  | min_curr_iops  | int4 | Minimum I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | max_curr_iops  | int4 | Maximum I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | min_peak_iops  | int4 | Minimum peak I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | max_peak_iops  | int4 | Maximum peak I/O of the current user across database nodes. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | io_limits      | int4 | **io_limits** set for the resource pool specified by the user. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | io_priority    | text | **io_priority** set for the user. The IOPS is counted by ones for column storage and by ten thousands for row storage. |
  | curr_io_limits | int4 | Real-time **io_limits** value when **io_priority** is used to control I/Os |

- pg_stat_get_function_calls(oid)

  Description: Specifies the number of times the function has been called.

  Return type: bigint

- pg_stat_get_function_self_time(oid)

  Description: Specifies the time spent in only this function. The time spent on this function calling other functions is excluded.

  Return type: bigint

- pg_stat_get_backend_idset()

  Description: Sets the number of currently active server processes (from 1 to the number of active server processes).

  Return type: setofinteger

- pg_stat_get_backend_pid(integer)

  Description: Specifies the ID of the given server thread.

  Return type: bigint

- pg_stat_get_backend_dbid(integer)

  Description: Specifies the ID of the database connected to the given server process.

  Return type: oid

- pg_stat_get_backend_userid(integer)

  Description: Specifies the user ID of the given server process.

  Return type: oid

- pg_stat_get_backend_activity(integer)

  Description: Active command of the given server process, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: text

- pg_stat_get_backend_waiting(integer)

  Description: True if the given server process is waiting for a lock, but only if the current user is a system administrator or the same user as that of the session being queried and **track_activities** is on

  Return type: Boolean

- pg_stat_get_backend_activity_start(integer)

  Description: Specifies the time when the given server process's currently executing query is started only if the current user is the system administrator or the user of the session being queried and **track_activities** is enabled.

  Return type: timestamp with time zone

- pg_stat_get_backend_xact_start(integer)

  Description: Specifies the time when the given server process's currently executing transaction is started only if the current user is the system administrator or the user of the session being queried and **track_activities** is enabled.

  Return type: timestamp with time zone

- pg_stat_get_backend_start(integer)

  Description: Specifies the time when the given server process is started. If the current user is neither the system administrator nor the user of the session being queried, NULL is **returned**.

  Return type: timestamp with time zone

- pg_stat_get_backend_client_addr(integer)

  Description: Specifies the IP address of the client connected to the given server process. If the connection is over a Unix domain socket, or if the current user is neither a system administrator nor the same user as that of the session being queried, **NULL** will be returned.

  Return type: inet

- pg_stat_get_backend_client_port(integer)

  Description: Specifies the TCP port number of the client connected to the given server process. If the connection is over a Unix domain socket, **-1** will be returned. If the current user is neither a system administrator nor the same user as that of the session being queried, **NULL** will be returned.

  Return type: integer

- pg_stat_get_bgwriter_timed_checkpoints()

  Description: Specifies the time when the background writer starts scheduled checkpoints (because the **checkpoint_timeout** time has expired).

  Return type: bigint

- pg_stat_get_bgwriter_requested_checkpoints()

  Description: Specifies the time when the background writer starts checkpoints based on requests from the backend because **checkpoint_segments** has been exceeded or the **CHECKPOINT** command has been executed.

  Return type: bigint

- pg_stat_get_bgwriter_buf_written_checkpoints()

  Description: Specifies the number of buffers written by the background writer during checkpoints.

  Return type: bigint

- pg_stat_get_bgwriter_buf_written_clean()

  Description: Specifies the number of buffers written by the background writer for routine cleaning of dirty pages.

  Return type: bigint

- pg_stat_get_bgwriter_maxwritten_clean()

  Description: Specifies the time when the background writer stops its cleaning scan because it has written more buffers than specified in the **bgwriter_lru_maxpages** parameter.

  Return type: bigint

- pg_stat_get_buf_written_backend()

  Description: Specifies the number of buffers written by the backend because they need to allocate a new buffer.

  Return type: bigint

- pg_stat_get_buf_alloc()

  Description: Specifies the total number of the allocated buffers.

  Return type: bigint

- pg_stat_clear_snapshot()

  Description: Discards the current statistics snapshot.

  Return type: void

- pg_stat_reset()

  Description: Resets all statistics counters for the current database to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_shared(text)

  Description: Resets all statistics counters for the current database in each node in a shared cluster to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_single_table_counters(oid)

  Description: Resets statistics for a single table or index in the current database to zero (requires system administrator permissions).

  Return type: void

- pg_stat_reset_single_function_counters(oid)

  Description: Resets statistics for a single function in the current database to zero (requires system administrator permissions).

  Return type: void

- pg_stat_session_cu(int, int, int)

  Description: Obtains the compression unit (CU) hit statistics of sessions running on the current node.

  Return type: record

- pg_stat_get_cu_mem_hit(oid)

  Description: Obtains the number of CU memory hits of a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_cu_hdd_sync(oid)

  Description: Obtains the times CU is synchronously read from a disk by a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_cu_hdd_asyn(oid)

  Description: Obtains the times CU is asynchronously read from a disk by a column storage table in the current database of the current node.

  Return type: bigint

- pg_stat_get_db_cu_mem_hit(oid)

  Description: Obtains the CU memory hit in a database of the current node.

  Return type: bigint

- pg_stat_get_db_cu_hdd_sync(oid)

  Description: Obtains the times CU is synchronously read from a disk by a database of the current node.

  Return type: bigint

- fenced_udf_process()

  Description: Shows the number of local UDF Master and Work processes.

  Return type: record

- total_cpu()

  Description: Obtains the CPU time used by the current node, in jiffies.

  Return type: bigint

- mot_global_memory_detail()

  Description: Checks the size of the MOT global memory, including data and indexes.

  Return type: record

- mot_local_memory_detail()

  Description: Checks the size of the MOT local memory, including data and indexes.

  Return type: record

- mot_session_memory_detail()

  Description: Checks the MOT memory usage of all sessions.

  Return type: record

- total_memory()

  Description: Obtains the size of the virtual memory used by the current node, in KB.

  Return type: bigint

- pg_stat_get_db_cu_hdd_asyn(oid)

  Description: Obtains the times CU is asynchronously read from a disk by a database of the current node.

  Return type: bigint

- pg_stat_bad_block(text, int, int, int, int, int, timestamp with time zone, timestamp with time zone)

  Description: Obtains damage information about pages or CUs after the current node is started.

  Example: select * from pg_stat_bad_block();

  Return type: record

- pg_stat_bad_block_clear()

  Description: Deletes the page and CU damage information that is read and recorded on the node (requires system administrator permissions).

  Return type: void

- gs_respool_exception_info(pool text)

  Description: Queries the query rule of a specified resource pool.

  Return type: record

- gs_control_group_info(pool text)

  Description: Queries information about Cgroups associated with a resource pool.

  Return type: record

  The command output is as follows:

  | Attribute | Value               | Description                                             |
  | :-------- | :------------------ | :------------------------------------------------------ |
  | name      | class_a:workload_a1 | Class name and workload name                            |
  | class     | class_a             | Class Cgroup name                                       |
  | workload  | workload_a1         | Workload Cgroup name                                    |
  | type      | DEFWD               | Cgroup type (Top, CLASS, BAKWD, DEFWD, and TSWD)        |
  | gid       | 87                  | Cgroup ID                                               |
  | shares    | 30                  | Percentage of CPU resources to those on the parent node |
  | limits    | 0                   | Percentage of CPU cores to those on the parent node     |
  | rate      | 0                   | Allocation ratio in Timeshare                           |
  | cpucores  | 0-3                 | Number of CPU cores                                     |

- gs_all_control_group_info()

  Description: Collects information about all Cgroups in the database.

  Return type: record

- gs_get_control_group_info()

  Description: Collects information about all Cgroups.

  Return type: record

- get_instr_workload_info(integer)

  Description: Obtains the transaction volume and time information on the primary database node.

  Return type: record

  | Attribute           | Value        | Description                                                  |
  | :------------------ | :----------- | :----------------------------------------------------------- |
  | resourcepool_oid    | 10           | OID of the resource pool (the logic is equivalent to the load) |
  | commit_counter      | 4            | Number of frontend transactions that were committed          |
  | rollback_counter    | 1            | Number of frontend transactions that were rolled back        |
  | resp_min            | 949          | Minimum response time of frontend transactions (unit: μs)    |
  | resp_max            | 201891       | Maximum response time of frontend transactions (unit: μs)    |
  | resp_avg            | 43564        | Average response time of frontend transactions (unit: μs)    |
  | resp_total          | 217822       | Total response time of frontend transactions (unit: μs)      |
  | bg_commit_counter   | 910          | Number of backend transactions that were committed           |
  | bg_rollback_counter | 0            | Number of backend transactions that were rolled back         |
  | bg_resp_min         | 97           | Minimum response time of backend transactions (unit: μs)     |
  | bg_resp_max         | 678080687    | Maximum response time of backend transactions (unit: μs)     |
  | bg_resp_avg         | 327847884    | Average response time of backend transactions (unit: μs)     |
  | bg_resp_total       | 298341575300 | Total response time of backend transactions (unit: μs)       |

- pv_instance_time()

  Description: Obtains the time consumed in each execution phase on the current node.

  Return type: record

  | Stat_name Attribute | Value   | Description                                                  |
  | :------------------ | :------ | :----------------------------------------------------------- |
  | DB_TIME             | 1062385 | Total end-to-end wall time consumed by all threads (unit: μs) |
  | CPU_TIME            | 311777  | Total CPU time consumed by all threads (unit: μs)            |
  | EXECUTION_TIME      | 380037  | Total time consumed on the executor (unit: μs)               |
  | PARSE_TIME          | 6033    | Total time consumed for parsing SQL statements (unit: μs)    |
  | PLAN_TIME           | 173356  | Total time consumed for generating an execution plan (unit: μs) |
  | REWRITE_TIME        | 2274    | Total time consumed on query rewriting (unit: μs)            |
  | PL_EXECUTION_TIME   | 0       | Total time consumed for executing PL/SQL statements (unit: μs) |
  | PL_COMPILATION_TIME | 557     | Total time consumed for SQL compilation (unit: μs)           |
  | NET_SEND_TIME       | 1673    | Total time consumed for sending data over network (unit: μs) |
  | DATA_IO_TIME        | 426622  | Total time consumed for data read and write (unit: μs)       |

- DBE_PERF.get_global_instance_time()

  Description: Provides the time consumed in each key phase in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- get_instr_unique_sql()

  Description: Obtains information about execution statements (normalized SQL statements) on the current node as a user with the **sysadmin** permission.

  Return type: record

- reset_unique_sql(text, text, bigint)

  Description: Resets information about system execution statements (normalized SQL statements) information as a user with the **sysadmin** permission. The value of the first parameter can be **global** or **local**. **global** indicates that information on all nodes is cleared, and **local** indicates that only information on the current node is cleared. The value of the second parameter can be **ALL**, **BY_USERID**, or **BY_CNID**. **ALL** indicates that all information is cleared. **BY_USERID** indicates that the SQL information of the user specified by **USERID** is cleared. **BY_CNID** indicates that the SQL information related to the primary node of the database in the system is cleared. The third parameter indicates **CNID** and **USERID**. If the second parameter is set to **ALL**, the third parameter does not take effect and can be set to any value.

  Return type: Boolean

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** This function involves distributed nodes. Currently, MogDB is a centralized database, for which the function of the value **global** is the same as that of the value **local** and the second parameter cannot set to be **BY_CNID**.

- get_instr_wait_event(NULL)

  Description: Obtains the statistics on wait events of the current node.

  Return type: record

- get_instr_user_login()

  Description: Obtains the number of user login and logout times on the current node. Only users with the **sysadmin** or **monitor admin** permission can execute this function.

  Return type: record

- get_instr_rt_percentile(integer)

  Description: Obtains the SQL response time P80 and P95 distribution information of the database.

  Return type: record

- get_node_stat_reset_time()

  Description: Obtains statistics about reset (restart, primary/standby switchover, and database deletion) time of the current node.

  Return type: record

- DBE_PERF.get_global_os_runtime()

  Description: Displays the running status of the current operating system. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_os_threads()

  Description: Provides information about the threads under all normal nodes of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_workload_sql_count()

  Description: Provides statistics about the number of SELECT, UPDATE, INSERT, DELETE, DDL, DML, and DCL statements of different service loads in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_workload_sql_elapse_time()

  Description: Provides statistics about the number of SELECT, UPDATE, INSERT, and DELETE statements and response time information (TOTAL, AVG, MIN, and MAX) for different loads in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_workload_transaction()

  Description: Obtains the transaction volume and time information on all nodes of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_stat()

  Description: Obtains the session status information on all nodes of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The status information contains the following 17 items: **commit**, **rollback**, **sql**, **table_scan**, **blocks_fetched**, **physical_read_operation**, **shared_blocks_dirtied**, **local_blocks_dirtied**, **shared_blocks_read**, **local_blocks_read**, **blocks_read_time**, **blocks_write_time, sort_imemory**, **sort_idisk**, **cu_mem_hit**, **cu_hdd_sync_read**, and **cu_hdd_asyread**.

- DBE_PERF.get_global_session_time()

  Description: Provides the time consumed in each key phase of each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_memory()

  Description: Displays statistics about memory usage at the session level on each node in the unit of MB, including all the memory allocated to Postgres and stream threads on DNs for jobs currently executed by users. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_session_memory_detail()

  Description: Displays statistics about thread memory usage on each node by MemoryContext node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- create_wlm_session_info(int flag)

  Description: Clears top SQL query statement-level statistics recorded in the current memory. Only the administrator can execute this function.

  Return type: int

- pg_stat_get_wlm_session_info(int flag)

  Description: Obtains top SQL query statement-level statistics recorded in the current memory. If the input parameter is not 0, the information is cleared from the memory. Only users with the **system admin** or **monitor admin** permission can execute this function.

  Return type: record

- gs_paxos_stat_replication()

  Description: Queries the standby server information on the primary server. Currently, only the centralized DCF mode is supported.

  Return type: setofrecord

  The following table describes return columns.

  | Column                   | Type | Description                                                  |
  | ------------------------ | ---- | ------------------------------------------------------------ |
  | local_role               | text | Role of the node that sends logs                             |
  | peer_role                | text | Role of the node that receives logs                          |
  | local_dcf_role           | text | DCF role of the node that sends logs                         |
  | peer_dcf_role            | text | DCF role of the node that receives logs                      |
  | peer_state               | text | Status of the node that receives logs                        |
  | sender_write_location    | text | Location in the Xlog buffer where the node that sends logs is written |
  | sender_commit_location   | text | Consistency point reached for the DCF logs of the node that sends logs |
  | sender_flush_location    | text | Location in the Xlog disk where the node that sends logs is written |
  | sender_replay_location   | text | Location where the node that sends logs replays logs         |
  | receiver_write_location  | text | Location in the Xlog buffer where the node that receives logs is written |
  | receiver_commit_location | text | Consistency point reached for the DCF logs of the node that receives logs |
  | receiver_flush_location  | text | Location in the Xlog disk where the node that receives logs is written |
  | receiver_replay_location | text | Location where the node that receives logs replays Xlogs     |
  | sync_percent             | text | Synchronization percentage                                   |
  | dcf_run_mode             | int4 | DCF synchronization mode                                     |
  | channel                  | text | Channel information                                          |

- gs_wlm_get_resource_pool_info(int)

  Description: Obtains resource usage statistics of all users. The input parameter can be any value of the INT type or be null.

  Return type: record

- gs_wlm_get_all_user_resource_info()

  Description: Obtains the resource usage statistics of all users.

  Return type: record

- gs_wlm_get_user_info(int)

  Description: Obtains information about all users. The input parameter can be any value of the INT type or be null. Only users with the **sysadmin** permission can execute this function.

  Return type: record

- gs_wlm_get_workload_records()

  Description: Obtains all job information in dynamic load management. This function is valid only when dynamic load management is enabled.

  Return type: record

- gs_wlm_readjust_user_space()

  Description: Corrects the storage space usage of all users. Only the administrator can execute this function.

  Return type: record

- gs_wlm_readjust_user_space_through_username(text name)

  Description: Corrects the storage space usage of a specified user. Common users can use this function to modify only their own usage. Only the administrator can modify the usage of all users. If the value of **name** is **0000**, the usage of all users needs to be modified.

  Return type: record

- gs_wlm_readjust_user_space_with_reset_flag(text name, boolean isfirst)

  Description: Corrects the storage space usage of a specified user. If the input parameter **isfirst** is set to **true**, statistics are collected from 0. Otherwise, statistics are collected from the previous result. Common users can use this function to modify only their own usage. Only the administrator can modify the usage of all users. If the value of **name** is **0000**, the usage of all users needs to be modified.

  Return type: record

- gs_wlm_session_respool(bigint)

  Description: Obtains the session resource pool information about all backend threads. The input parameter can be any value of the bigint type or can be null.

  Return type: record

- gs_wlm_get_session_info()

  Description: This API has been discarded and is unavailable currently.

- gs_wlm_get_user_session_info()

  Description: This API has been discarded and is unavailable currently.

- gs_io_wait_status()

  Description: This API does not support single-node systems or centralized systems and is unavailable currently.

- global_stat_get_hotkeys_info()

  Description: Obtains the statistics of hot keys in the entire cluster. This API does not support single-node systems or centralized systems and is unavailable currently.

- global_stat_clean_hotkeys()

  Description: Clears statistics on hot keys in the entire cluster. This API does not support single-node systems or centralized systems and is unavailable currently.

- DBE_PERF.get_global_session_stat_activity()

  Description: Displays information about threads that are running on each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_thread_wait_status()

  Description: Displays the block waiting status of backend threads and auxiliary threads on all nodes. To query this function, you must have the **sysadmin** or **monitoradmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_history_table()

  Description: Displays the operator-related records (persistent) generated after jobs are executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_history()

  Description: Displays the operator-related records generated after jobs are executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_operator_runtime()

  Description: Displays real-time operator-related records of jobs executed on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_history()

  Description: Displays the historical records of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_history_table()

  Description: Displays the historical records (persistent) of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_complex_runtime()

  Description: Displays the real-time information of complex queries on the primary database node of the current user. To query this function, you must have the **sysadmin** or **monadmin** permission.

  Return type: record

- DBE_PERF.get_global_memory_node_detail()

  Description: Displays the memory usage of a certain database on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_shared_memory_detail()

  Description: Displays the usage information about all the shared memory contexts of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_indexes()

  Description: Displays statistics about each index displayed in a row in the current database, showing I/O statistics about accesses to that specific index. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_all_tables()

  Description: Displays statistics about a row in each table (including the TOAST table) on each node.

  Return type: record

- DBE_PERF.get_global_stat_all_tables()

  Description: Displays statistics about a row in each table (including the TOAST table) on each node.

  Return type: record

- DBE_PERF.get_local_toastname_and_toastindexname()

  Description: Provides the mapping between the name and index of the local TOAST table and its associated table. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_all_indexes()

  Description: Collects statistics about each index displayed in a row in the current databases of all nodes and displays the I/O statistics of a specific index. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_sequences()

  Description: Provides I/O status information about all sequences in the namespace. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_all_tables()

  Description: Displays the I/O statistics about each table in databases on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_all_tables()

  Description: Collects I/O statistics about each table in databases in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_local_toast_relation()

  Description: Provides the mapping between the name of the local TOAST table and its associated table. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_indexes()

  Description: Displays the I/O status information about all system catalog indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_sys_indexes()

  Description: Collects the I/O status information about all system catalog indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_sequences()

  Description: Provides the I/O status information about all the system sequences in the namespace. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_sys_tables()

  Description: Provides I/O status information about all system catalogs in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_sys_tables()

  Description: Displays the I/O status information of all system catalogs in the namespace in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_indexes()

  Description: Displays the I/O status information about all user relationship table indexes in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_user_indexes()

  Description: Displays the I/O status information about all user relationship table indexes in namespaces in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_sequences()

  Description: Displays the I/O status information about all user sequences in the namespace of each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_statio_user_tables()

  Description: Displays the I/O status information about all user relationship tables in namespaces on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statio_user_tables()

  Description: Displays the I/O status information about all user relationship tables in namespaces in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_stat_db_cu()

  Description: Queries CU hits in a database and in each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_all_indexes()

  Description: Displays statistics of each index in databases on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_all_indexes()

  Description: Collects statistics of each index in all databases on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_sys_tables()

  Description: Displays statistics about the system catalogs of all the namespaces in **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_sys_tables()

  Description: Collects statistics about the system catalogs of all the namespaces in **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_sys_indexes()

  Description: Displays index status information about all the system catalogs in the **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_sys_indexes()

  Description: Collects statistics about index status information about all the system catalogs in the **pg_catalog** and **information_schema** schemas on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_tables()

  Description: Displays the status information about customized ordinary tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_user_tables()

  Description: Collects statistics about the status information about customized ordinary tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_indexes()

  Description: Displays the status information about the index of customized ordinary tables in all databases. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_user_indexes()

  Description: Collects statistics about the status information about the index of customized ordinary tables in all databases. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_database()

  Description: Displays database statistics of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_database_conflicts()

  Description: Collects statistics on the database of all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_all_tables()

  Description: Displays transaction status information about all ordinary tables and TOAST tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_all_tables()

  Description: Collects statistics about transaction status information about all ordinary tables and TOAST tables in all namespaces. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_sys_tables()

  Description: Displays transaction status information about all system catalogs in namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_sys_tables()

  Description: Collects statistics about transaction status information about all system catalogs in namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_user_tables()

  Description: Displays the transaction status information of the user tables in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_stat_xact_user_tables()

  Description: Collects statistics about the transaction status information of the user tables in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_user_functions()

  Description: Displays the transaction status information of customized functions in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_xact_user_functions()

  Description: Collects statistics about the transaction status information of customized functions in the namespaces on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_stat_bad_block()

  Description: Displays information about table and index read failures on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_file_redo_iostat()

  Description: Collects statistics on information about table and index read failures on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_file_iostat()

  Description: Displays statistics about data file I/Os on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_locks()

  Description: Displays lock information of all nodes. To query this function, you must have the **sysadmin** or **monadmin** permission.

  Return type: record

- DBE_PERF.get_global_replication_slots()

  Description: Displays logical replication information on all nodes. To query this function, you must have the **sysadmin** or **monadmin** permission.

  Return type: record

- DBE_PERF.get_global_bgwriter_stat()

  Description: Displays statistics about the background writer process's activities on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_replication_stat()

  Description: Displays information about log synchronization status on each node, such as the locations where the sender sends logs and where the receiver receives logs. To query this function, you must have the **sysadmin** or **monadmin** permission.

  Return type: record

- DBE_PERF.get_global_transactions_running_xacts()

  Description: Displays information about running transactions on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_transactions_running_xacts()

  Description: Collects statistics of information about running transactions on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_transactions_prepared_xacts()

  Description: Displays information about transactions that are currently prepared for two-phase commit on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_transactions_prepared_xacts()

  Description: Collects statistics information about transactions that are currently prepared for two-phase commit on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_statement()

  Description: Displays the status information of the historically-executed statements on each node. To query this function, you must have the **sysadmin** or **monitoradmin** permission.

  Return type: record

- DBE_PERF.get_global_statement_count()

  Description: Displays the number of **SELECT**, **UPDATE**, **INSERT**, and **DELETE** statements and response time information (TOTAL, AVG, MIN, and MAX) on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_config_settings()

  Description: Displays GUC parameter configuration information on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_wait_events()

  Description: Displays the wait event status information on each node. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_statement_responsetime_percentile()

  Description: Obtains the response time distribution for 80% and 95% SQL statements of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_summary_user_login()

  Description: Collects statistics about number of user login and logout times on each node in MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.get_global_record_reset_time()

  Description: Displays the statistics about reset (restart, primary/standby switchover, and database deletion) time of MogDB. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.track_memory_context(context_list text)

  Description: Sets the memory context whose memory application details need to be collected. The input parameter is the memory context names, which are separated by commas (,), for example, **ThreadTopMemoryContext, SessionCacheMemoryContext**. Note that the memory context names are context-sensitive. In addition, the length of a single memory context is 63, and the excess part is truncated. The maximum number of memory contexts that can be collected at a time is 16. If the number of memory contexts exceeds 16, the setting fails. Each time this function is called, the previous statistics result is cleared. When the input parameter is set to **""**, the statistics function is disabled. Only the initial user (super user) or a user with the **monadmin**permission can execute this function.

  Return type: Boolean

- DBE_PERF.track_memory_context_detail()

  Description: Obtains the memory application details of the memory context specified by the **DBE_PERF.track_memory_context** function. For details, see the **DBE_PERF.track_memory_context_detail** view. Only the initial user (super user) or a user with the **monadmin**permission can execute this function.

  Return type: record

- pg_stat_get_mem_mbytes_reserved(tid)

  Description: Collects statistics on variables related to resource management, which is used only for fault locating.

  Parameter: thread ID

  Return type: text

- gs_wlm_user_resource_info(name text)

  Description: Queries a user's resource quota and resource usage.

  Return type: record

- pg_stat_get_file_stat()

  Description: Rrecords statistics about data file I/Os to indicate I/O performance and detect performance problems such as abnormal I/O operations.

  Return type: record

- pg_stat_get_redo_stat()

  Description: Displays statistics on the replay of session thread logs.

  Return type: record

- pg_stat_get_status(int8)

  Description: Tests the block waiting status about the backend thread and auxiliary thread of the current instance.

  Return type: record

- get_local_rel_iostat()

  Description: Queries the accumulated I/O status of data files on the current node.

  Return type: record

- DBE_PERF.get_global_rel_iostat()

  Description: Displays statistics about data file I/Os on all nodes. To query this function, you must have the **sysadmin** permission.

  Return type: record

- DBE_PERF.global_threadpool_status()

  Description: Displays the status of worker threads and sessions in thread pools on all nodes. For details about the columns returned by the function, see GLOBAL_THREADPOOL_STATUS.

  Return type: record

- remote_bgwriter_stat()

  Description: Displays the information about pages flushed by the bgwriter threads of all instances in the database, number of pages in the candidate buffer chain, and buffer elimination information (except for the local node and not available on the DN).

  Return type: record

- pv_os_run_info

  Description: Displays the running status of the current OS. For details about the columns, see GS_OS_RUN_INFO.

  Parameter: nan

  Return type: SETOF record

- pv_session_stat

  Description: Collects session status information by session thread or AutoVacuum thread. For details about the columns, see GS_SESSION_STAT.

  Parameter: **nan**

  Return type: SETOF record

- pv_session_time

  Description: Collects statistics on the running time of session threads and the time consumed in each execution phase. For details about the columns, see GS_SESSION_TIME.

  Parameter: **nan**

  Return type: SETOF record

- pg_stat_get_db_temp_bytes

  Description: Collects statistics on the total amount of data written to temporary files through database query. All temporary files are counted, regardless of why the temporary file was created, and regardless of the **log_temp_files** setting.

  Parameter: **oid**

  Return type: bigint

- pg_stat_get_db_temp_files

  Description: Queries the number of temporary files created in the database. All temporary files are counted, regardless of why the temporary file was created (for example, sorting or hashing), and regardless of the **log_temp_files** setting.

  Parameter: **oid**

  Return type: bigint

- remote_candidate_stat()

  Description: Displays the checkpoint information and log flushing information about all instances in the database (except the current node). Centralized systems are not supported.

  Return type: record

- dbe_perf.gs_stat_activity_timeout(int)

  Description: Obtains information about query jobs whose execution time exceeds the timeout threshold on the current node. The correct result can be returned only when the GUC parameter **track_activities** is set to **on**. The timeout threshold ranges from 0 to 2147483.

  Return type: SETOF record

  | Name             | Type        | Description                                               |
  | :--------------- | :---------- | :-------------------------------------------------------- |
  | database         | name        | Name of the database to which a user session is connected |
  | pid              | bigint      | Backend thread ID                                         |
  | sessionid        | bigint      | Session ID                                                |
  | usesysid         | oid         | OID of the user logged in to the backend                  |
  | application_name | text        | Name of the application connected to the backend          |
  | query            | text        | Query that is being executed on the backend               |
  | xact_start       | timestamptz | Time when the current transaction is started              |
  | query_start      | timestamptz | Time when the current query starts                        |
  | query_id         | bigint      | Query statement ID                                        |

- gs_wlm_user_resource_info(name text)

  Description: Queries a user's resource quota and resource usage. Common users can query only their own information. Administrators can query information about all users.

  Return type: record

- create_wlm_instance_statistics_info

  Description: Saves the historical monitoring data of the current instance persistently.

  Parameter: **nan**

  Return type: integer

- gs_session_memory

  Description: Collects statistics about memory usage at the session level in the unit of MB, including all the memory allocated to Postgres and Stream threads on DNs for tasks currently executed by users.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** If **enable_memory_limit** is set to **off**, this function cannot be used.

  Return type: record

  **Table 8** Return value description

  | Name     | Type    | Description                                                  |
  | :------- | :------ | :----------------------------------------------------------- |
  | sessid   | text    | Thread start time and ID                                     |
  | init_mem | integer | Memory allocated to the currently executed jobs before they enter the executor, in MB |
  | used_mem | integer | Memory allocated to the currently executed jobs, in MB       |
  | peak_mem | integer | Peak memory allocated to the currently executed jobs, in MB  |

- gs_wlm_persistent_user_resource_info()

  Description: Archives all user resource usage statistics to the **gs_wlm_user_resource_history** system catalog. To query this function, you must have the **sysadmin** permission.

  Return type: record

- create_wlm_operator_info(int flag)

  Description: Clears top SQL operator-level statistics recorded in the current memory. If the input parameter is greater than 0, the information is archived to **gs_wlm_operator_info** and **gs_wlm_ec_operator_info**. Otherwise, the information is not archived. Only users with the **sysadmin** permission can execute this function.

  Return type: int

- GS_ALL_NODEGROUP_CONTROL_GROUP_INFO(text)

  Description: Provides Cgroup information for all logical database instances. Before invoking this function, you need to specify the name of a logical database instance to be queried. For example, to query the Cgroup information for the installation logical database instance, run the following command:

  ```sql
  SELECT * FROM GS_ALL_NODEGROUP_CONTROL_GROUP_INFO('installation')
  ```

  Return type: record

  The following table describes return columns.

  | Name     | Type   | Description                                                  |
  | :------- | :----- | :----------------------------------------------------------- |
  | name     | text   | Cgroup name.                                                 |
  | type     | text   | Cgroup type.                                                 |
  | gid      | bigint | Cgroup ID.                                                   |
  | classgid | bigint | ID of the **Class** cgroup where a **Workload** cgroup belongs. |
  | class    | text   | **Class** cgroup.                                            |
  | workload | text   | **Workload** cgroup.                                         |
  | shares   | bigint | CPU quota allocated to the cgroup.                           |
  | limits   | bigint | Limit of CPUs allocated to a cgroup.                         |
  | wdlevel  | bigint | **Workload** cgroup level.                                   |
  | cpucores | text   | Usage of CPU cores in a cgroup.                              |

- gs_total_nodegroup_memory_detail

  Description: Returns information about the memory used by the current logical database, in MB.

  Return type: SETOF record
