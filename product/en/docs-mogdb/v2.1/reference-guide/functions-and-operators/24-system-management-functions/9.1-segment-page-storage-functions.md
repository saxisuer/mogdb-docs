---
title: Segment-Page Storage Functions
summary: Segment-Page Storage Functions
author: Guo Huan
date: 2021-10-28
---

# Segment-Page Storage Functions

- local_segment_space_info(tablespacename TEXT, databasename TEXT)

  Description: Generates usage information about all extent groups in the tablespace.

  Return type:

  | node_name        | Node name                                                    |
  | ---------------- | ------------------------------------------------------------ |
  | extent_size      | Extent specifications of an extent group. The unit is the number of blocks. |
  | forknum          | Fork number                                                  |
  | total_blocks     | Total number of extents in a physical file                   |
  | meta_data_blocks | Number of blocks occupied by the metadata managed in a tablespace, including the space header and map page but excluding the segment head |
  | used_data_blocks | Number of extents used for storing data, including the segment head |
  | utilization      | Percentage of the number of used blocks to the total number of blocks, that is, (the value of **used_data_blocks** + the value of **meta_data_block**)/the value of **total_blocks** |
  | high_water_mark  | High-water mark, indicating the number of allocated extents and maximum physical page number. Blocks that exceed the high-water mark are not used and can be directly recycled. |

  Example:

  ```sql
  select * from local_segment_space_info('pg_default', 'postgres');
       node_name     | extent_size | forknum | total_blocks | meta_data_blocks | used_data_blocks | utilization | high_water_mark
  -------------------+-------------+---------+--------------+------------------+------------------+-------------+-----------------
   dn_6001_6002_6003 |           1 |       0 |        16384 |             4157 |                1 |     .253784 |            4158
   dn_6001_6002_6003 |           8 |       0 |        16384 |             4157 |                8 |     .254211 |            4165
  (2 rows)
  ```

- pg_stat_segment_extent_usage(int4 tablespace oid, int4 database oid, int4 extent_type, int4 forknum)

  Description: Specifies the usage information of each allocated extent in an extent group returned each time. **extent_type** indicates the type of the extent group. The value is an integer ranging from 1 to 5. If the value is not within the range, an error is reported. **forknum** indicates the fork number. The value is an integer ranging from 0 to 4. Currently, only the following values are valid: **0** for data files, **1** for FSM files, and **2** for visibility map files.

  Return type:

  | Name          | Description                                                  |
  | :------------ | :----------------------------------------------------------- |
  | start_block   | Start physical page number of an extent                      |
  | extent_size   | Size of an extent                                            |
  | usage_type    | Usage type of an extent, for example, **segment head** and **data extent** |
  | ower_location | Object location of an extent to which a pointer points. For example, the owner of a data extent is the head of the segment to which the data extent belongs. |
  | special_data  | Position of an extent in its owner. The value of this field is related to the usage type. For example, special data of a data extent is the extent ID in the segment to which the data extent belongs. |

  The value of **usage_type** is enumerated. The meaning of each value is as follows:

  - **Non-bucket table segment head**: data segment head of a non-hash bucket table
  - **Non-bucket table fork head**: fork segment header of a non-segment-page table
  - **Data extent**: data block

  Example:

  ```sql
  select * from pg_stat_segment_extent_usage((select oid::int4 from pg_tablespace where spcname='pg_default'), (select oid::int4 from pg_database where datname='postgres'), 1, 0);
  start_block | extent_size |       usage_type       | ower_location | special_data
  -------------+-------------+------------------------+---------------+--------------
  4157 |           1 | Data extent |    4294967295 |            0
  4158 |           1 | Data extent |          4157 |            0
  ```

- local_space_shrink(tablespacename TEXT, databasename TEXT)

  Description: Compresses specified physical segment-page space on the current node. Only the currently connected database can be shrank.

  Return value: empty

- gs_space_shrink(int4 tablespace, int4 database, int4 extent_type, int4 forknum)

  Description: Works similar to **local_space_shrink**, that is, compresses specified physical segment-page space. However, the parameters are different. The input parameters are the OIDs of the tablespace and database, and the value of **extent_type** is an integer ranging from 2 to 5. Note: The value **1** of **extent_type** indicates segment-page metadata. Currently, the physical file that contains the metadata cannot be compressed. This function is used only by tools. You are not advised to use it directly.

  Return value: empty

- pg_stat_remain_segment_info()

  Description: Displays residual extents on the current node due to faults. Residual extents are classified into two types: segments that are allocated but not used and extents that are allocated but not used. The main difference is that a segment contains multiple extents. During reclamation, all extents in the segment need to be recycled.

  Return type:

  | Name     | Description                                                  |
  | -------- | ------------------------------------------------------------ |
  | space_id | Tablespace ID                                                |
  | db_id    | Database ID                                                  |
  | block_id | Extent ID                                                    |
  | type     | Extent type. The options are as follows: **ALLOC_SEGMENT**, **DROP_SEGMENT**, and **SHRINK_EXTENT**. |

  The values of **type** are described as follows:

  - **ALLOC_SEGMENT**: When a user creates a segment-page table and the segment is just allocated but the transaction of creating a table is not committed, the node is faulty. As a result, the segment is not used after being allocated.

  - **DROP_SEGMENT**: When a user deletes a segment-page table and the transaction is successfully committed, the bit corresponding to the segment page of the table is not reset and a fault, such as power failure, occurs. As a result, the segment is not used or released.

  - **SHRINK_EXTENT**: When a user shrinks a segment-page table and does not release the idle extent, a fault, such as power failure, occurs. As a result, the extent remains and cannot be reused.

    Example:

    ```sql
    select * from pg_stat_remain_segment_info();
    space_id | db_id | block_id | type
    ----------+-------+----------+------
    1663       |   16385|        4156| ALLOC_SEGMENT
    ```

- pg_free_remain_segment(int4 spaceId, int4 dbId, int4 segmentId)

  Description: Releases a specified residual extent. The value must be obtained from the **pg_stat_remain_segment_info** function. The function verifies input values. If the specified extent is not among the recorded residual extents, an error message is returned. If the specified extent is a single extent, the extent is released independently. If it is a segment, the segment and all extents in the segment are released.

  Return value: empty
