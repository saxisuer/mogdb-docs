---
title: Database Object Functions
summary: Database Object Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Database Object Functions

## Database Object Size Functions

Database object size functions calculate the actual disk space used by database objects.

- pg_column_size(any)

  Description: Specifies the number of bytes used to store a particular value (possibly compressed)

  Return type: int

  Note: **pg_column_size** displays the space for storing an independent data value.

  ```sql
  mogdb=# SELECT pg_column_size(1);
   pg_column_size
  ----------------
                4
  (1 row)
  ```

- pg_database_size(oid)

  Description: Specifies the disk space used by the database with the specified OID.

  Return type: bigint

- pg_database_size(name)

  Description: Specifies the disk space used by the database with the specified name.

  Return type: bigint

  Note: **pg_database_size** receives the OID or name of a database and returns the disk space used by the corresponding object.

  Example:

  ```sql
  mogdb=# SELECT pg_database_size('postgres');
   pg_database_size
  ------------------
           51590112
  (1 row)
  ```

- pg_relation_size(oid)

  Description: Specifies the disk space used by the table with a specified OID or index.

  Return type: bigint

- get_db_source_datasize()

  Description: Estimates the total size of non-compressed data in the current database.

  Return type: bigint

  Remarks: (1) Perform an analysis before this function is called. (2) Calculate the total data capacity in the non-compressed state by estimating the compression rate of the column-store tables.

  Example:

  ```sql
  mogdb=# analyze;
  ANALYZE
  openGauss=# select get_db_source_datasize();
   get_db_source_datasize
  ------------------------
              35384925667
  (1 row)
  ```

- pg_relation_size(text)

  Description: Specifies the disk space used by the table with a specified name or index. The table name can be schema-qualified.

  Return type: bigint

- pg_relation_size(relation regclass, fork text)

  Description: Specifies the disk space used by the specified bifurcating tree ('main', 'fsm', or 'vm') of a certain table or index.

  Return type: bigint

- pg_relation_size(relation regclass)

  Description: Is an abbreviation of **pg_relation_size(…, 'main')**.

  Return type: bigint

  Note: **pg_relation_size** receives the OID or name of a table, an index, or a compressed table, and returns the size.

- pg_partition_size(oid,oid)

  Description: Specifies the disk space used by the partition with a specified OID. The first **oid** is the OID of the table and the second **oid** is the OID of the partition.

  Return type: bigint

- pg_partition_size(text, text)

  Description: Specifies the disk space used by the partition with a specified name. The first **text** is the table name and the second **text** is the partition name.

  Return type: bigint

- pg_partition_indexes_size(oid,oid)

  Description: Specifies the disk space used by the index of the partition with a specified OID. The first **oid** is the OID of the table and the second **oid** is the OID of the partition.

  Return type: bigint

- pg_partition_indexes_size(text,text)

  Description: Specifies the disk space used by the index of the partition with a specified name. The first **text** is the table name and the second **text** is the partition name.

  Return type: bigint

- pg_indexes_size(regclass)

  Description: Specifies the total disk space used by the index appended to the specified table.

  Return type: bigint

- pg_size_pretty(bigint)

  Description: Converts a size in bytes expressed as a 64-bit integer into a human-readable format with size units.

  Return type: text

- pg_size_pretty(numeric)

  Description: Converts a size in bytes expressed as a numeric value into a human-readable format with size units.

  Return type: text

  Note: **pg_size_pretty** formats the results of other functions into a human-readable format. KB/MB/GB/TB can be used.

- pg_table_size(regclass)

  Description: Specifies the disk space used by the specified table, excluding indexes (but including TOAST, free space mapping, and visibility mapping).

  Return type: bigint

- pg_tablespace_size(oid)

  Description: Specifies the disk space used by the tablespace with a specified OID.

  Return type: bigint

- pg_tablespace_size(name)

  Description: Specifies the disk space used by the tablespace with a specified name.

  Return type: bigint

  Note:

  **pg_tablespace_size** receives the OID or name of a database and returns the disk space used by the corresponding object.

- pg_total_relation_size(oid)

  Description: Specifies the disk space used by the table with a specified OID, including the index and the compressed data.

  Return type: bigint

- pg_total_relation_size(regclass)

  Description: Specifies the total disk space used by the specified table, including all indexes and TOAST data.

  Return type: bigint

- pg_total_relation_size(text)

  Description: Specifies the disk space used by the table with a specified name, including the index and the compressed data. The table name can be schema-qualified.

  Return type: bigint

  Note: **pg_total_relation_size** receives the OID or name of a table or a compressed table, and returns the sizes of the data, related indexes, and the compressed table in bytes.

- datalength(any)

  Description: Specifies the number of bytes used by an expression of a specified data type (data management space, data compression, or data type conversion is not considered).

  Return type: int

  Note: **datalength** is used to calculate the space of an independent data value.

  Example:

  ```sql
  mogdb=# SELECT datalength(1);
  datalength
  ------------
  4
  (1 row)
  ```

  The following table lists the supported data types and calculation methods.

  <table>
      <tr>
          <th colspan=3>Data Type</th>
          <th>Storage Space</th>
      </tr>
      <tr>
          <td rowspan=18>Numeric data types</td>
          <td rowspan=5>Integer types</td>
          <td>TINYINT</td>
          <td>1</td>
      </tr>
      <tr>
          <td>SMALLINT</td>
          <td>2</td>
      </tr>
      <tr>
          <td>INTEGER</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BINARY_INTEGER</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BIGINT</td>
          <td>8</td>
      </tr>
      <tr>
          <td rowspan=3>Arbitrary precision types</td>
          <td>DECIMAL</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td>NUMERIC</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td>NUMBER</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td rowspan=3>Sequence integer</td>
          <td>SMALLSERIAL</td>
          <td>2</td>
      </tr>
      <tr>
          <td>SERIAL</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BIGSERIAL</td>
          <td>8</td>
      </tr>
      <tr>
          <td rowspan=7>Floating point types</td>
          <td>FLOAT4</td>
          <td>4</td>
      </tr>
      <tr>
          <td>DOUBLE PRECISION</td>
          <td>8</td>
      </tr>
      <tr>
          <td>FLOAT8</td>
          <td>8</td>
      </tr>
      <tr>
          <td>BINARY_DOUBLE</td>
          <td>8</td>
      </tr>
      <tr>
          <td>FLOAT[(p)]</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td>DEC[(p[,s])]</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td>INTEGER[(p[,s])]</td>
          <td>Every four decimal digits occupy two bytes. The digits before and after the decimal point are calculated separately.</td>
      </tr>
      <tr>
          <td>Boolean data types</td>
          <td>Boolean types</td>
          <td>BOOLEAN</td>
          <td>1</td>
      </tr>
      <tr>
          <td rowspan=11>Character data types</td>
          <td rowspan=11>Character types</td>
          <td>CHAR</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHARACTER(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>NCHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>VARCHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHARACTER</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td>VARYING(n)</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td>VARCHAR2(n)</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td>NVARCHAR2(n)</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td>TEXT</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td>CLOB</td>
          <td>Actual number of bytes of a character</td>
      </tr>
      <tr>
          <td rowspan=11>Time data types</td>
          <td rowspan=11>Time types</td>
          <td>DATE</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIME</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIMEZ</td>
          <td>12</td>
      </tr>
      <tr>
          <td>TIMESTAMP</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIMESTAMPZ</td>
          <td>8</td>
      </tr>
      <tr>
          <td>SMALLDATETIME</td>
          <td>8</td>
      </tr>
      <tr>
          <td>INTERVAL DAY TO SECOND</td>
          <td>16</td>
      </tr>
      <tr>
          <td>INTERVAL</td>
          <td>16</td>
      </tr>
      <tr>
          <td>RELTIME</td>
          <td>4</td>
      </tr>
      <tr>
          <td>ABSTIME</td>
          <td>4</td>
      </tr>
      <tr>
          <td>TINTERVAL</td>
          <td>12</td>
      </tr>
  </table>

## Database Object Position Functions

- pg_relation_filenode(relation regclass)

  Description: Specifies the ID of a filenode with the specified relationship.

  Return type: oid

  Description: **pg_relation_filenode** receives the OID or name of a table, an index, a sequence, or a compressed table, and returns the number of **filenode** allocated to it. **filenode** is the basic component of the file name used by the relationship. For most tables, the result is the same as that of **pg_class.relfilenode**. For a specified system directory, **relfilenode** is set to **0** and this function must be used to obtain the correct value. If a relationship that is not stored is transmitted, such as a view, this function returns **NULL**.

- pg_relation_filepath(relation regclass)

  Description: Specifies the name of a file path with the specified relationship.

  Return type: text

  Description: **pg_relation_filepath** is similar to **pg_relation_filenode**, except that **pg_relation_filepath** returns the whole file path name for the relationship (relative to the data directory **PGDATA** of openGauss).

- pg_filenode_relation(tablespace oid, filenode oid)

  Description: Obtains the table names corresponding to the tablespace and relfilenode.

  Return type: regclass

- pg_partition_filenode(partition_oid)

  Description: Obtains **filenode** corresponding to the OID lock of a specified partitioned table.

  Return type: oid

- pg_partition_filepath(partition_oid)

  Description: Specifies the file path name of a partition.

  Return type: text

## Recycle Bin Object Functions

- gs_is_recycle_object(classid, objid, objname)

  Description: Determines whether an object is in the recycle bin.

  Return type: Boolean
