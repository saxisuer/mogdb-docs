---
title: Logical Replication Functions
summary: Logical Replication Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Logical Replication Functions

- pg_create_logical_replication_slot('slot_name', 'plugin_name')

  Description: Creates a logical replication slot.

  Parameter description:

  - slot_name

    Indicates the name of the streaming replication slot.

    Value range: a string, supporting only letters, digits, and the following special characters: _?-.

  - plugin_name

    Indicates the name of the plugin.

    Value range: a string, supporting **mppdb_decoding**

  Return type: name, text

  Note: The first return value is the slot name, and the second is the start LSN position for decoding in the logical replication slot.

- pg_create_physical_replication_slot('slot_name', 'isDummyStandby')

  Description: Creates a physical replication slot.

  Parameter description:

  - slot_name

    Indicates the name of the streaming replication slot.

    Value range: a string, supporting only letters, digits, and the following special characters: _?-.

  - isDummyStandby

    Specifies whether the replication slot is created by connecting the secondary server to the primary server.

    Type: bool

    Return type: name, text

- pg_drop_replication_slot('slot_name')

  Description: Deletes a streaming replication slot.

  Parameter description:

  - slot_name

    Indicates the name of the streaming replication slot.

    Value range: a string, supporting only letters, digits, and the following special characters: _?-.

  Return type: void

- pg_logical_slot_peek_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value') <a id="pg_logical"> </a>

  Description: Performs decoding but does not go to the next streaming replication slot. (The decoding result will be returned again on future calls.)

  Parameter description:

  - slot_name

    Indicates the name of the streaming replication slot.

    Value range: a string, supporting only letters, digits, and the following special characters: _?-.

  - LSN

    Indicates a target LSN. Decoding is performed only when an LSN is less than or equal to this value.

    Value range: a string, in the format of xlogid/xrecoff, for example, '1/2AAFC60' (If this parameter is set to **NULL**, the target LSN indicating the end position of decoding is not specified.)

  - upto_nchanges

    Indicates the number of decoded records (including the **begin** and **commit** timestamps). Assume that there are three transactions, which involve 3, 5, and 7 records, respectively. If **upto_nchanges** is **4**, 8 records of the first two transactions will be decoded. Specifically, decoding is stopped when the number of decoded records exceeds 4 after decoding in the first two transactions is finished.

    Value range: a non-negative integer

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** If any of the **LSN** and **upto_nchanges** values are reached, decoding ends.

  - options (optional)

    - include-xids

    Indicates whether the decoded **data** column contains XID information.

    Valid value:**0** and **1**. The default value is **1**.

    - **0**: The decoded **data** column does not contain XID information.
    - **1**: The decoded **data** column contains XID information.

    - skip-empty-xacts

      Indicates whether to ignore empty transaction information during decoding.

      Valid value:**0** and **1**. The default value is **0**.

      - **0**: The empty transaction information is not ignored during decoding.
      - **1**: The empty transaction information is ignored during decoding.

    - include-timestamp

        Indicates whether decoding information contains the **commit** timestamp.

        Valid value:**0** and **1**. The default value is **0**.

    - **0**: The decoding information does not contain the **commit** timestamp.
    - **1**: The decoding information contains the **commit** timestamp.

  Return type: text, uint, text

  Note: The function returns the decoding result. Each decoding result contains three columns, corresponding to the above return types and indicating the LSN position, XID, and decoded content, respectively.

- pg_logical_slot_get_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  Description: Performs decoding and goes to the next streaming replication slot.

  Parameter: This function has the same parameters as **pg_logical_slot_peek_changes**. For details, see [pg_logical_slot_peek_ch…](#pg_logical).

- pg_replication_slot_advance ('slot_name', 'LSN')

  Description: Directly goes to the streaming replication slot for a specified LSN, without outputting any decoding result.

  Parameter description:

  - slot_name

    Indicates the name of the streaming replication slot.

    Value range: a string, supporting only letters, digits, and the following special characters: _?-.

  - LSN

    Indicates a target LSN. Next decoding will be performed only in transactions whose commission position is greater than this value. If an input LSN is smaller than the position recorded in the current streaming replication slot, the function directly returns. If the input LSN is greater than the LSN of the current physical log, the latter LSN will be directly used for decoding.

    Value range: a string, in the format of xlogid/xrecoff

  Return type: name, text

  Note: A return result contains the slot name and LSN that is actually used for decoding.

- pg_get_replication_slots

  Description: Obtains the replication slot list.

  Return type: text, text, text, oid, boolean, xid, xid, text, boolean

  Example:

  ```sql
  mogdb=# select * from pg_get_replication_slots();
   slot_name |     plugin     | slot_type | datoid | active | xmin | catalog_xmin | restart_lsn | dummy_standby
  -----------+----------------+-----------+--------+--------+------+--------------+-------------+---------------
   wkl001    | mppdb_decoding | logical   |  15914 | f      |      |      2079556 | 4/1B81D920  | f
   dn_6002   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   dn_6004   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   dn_6003   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   gfslot001 | mppdb_decoding | logical   |  15914 | f      |      |      2412553 | 4/A54B2428  | f
  (5 rows)
  ```
