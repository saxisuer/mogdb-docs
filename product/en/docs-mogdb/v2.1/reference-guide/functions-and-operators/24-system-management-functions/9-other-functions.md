---
title: Other Functions
summary: Other Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Other Functions

- plan_seed()

  Description: Obtains the seed value of the previous query statement (internal use).

  Return type: int

- pg_stat_get_env()

  Description: Obtains the environment variable information of the current node. Only users with the **sysadmin** or **monitor admin** permission can access the environment variable information.

  Return type: record

  Example:

  ```sql
  mogdb=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/GaussDB_Kernel_TRUNK/install,/data1/GaussDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  Description: Clears the global plan cache that is not used on nodes.

  Return type: Boolean

- pg_catalog.plancache_status()

  Description: Displays information about the global plan cache on nodes. The information returned by the function is the same as that in GLOBAL_PLANCACHE_STATUS.

  Return type: record

- textlen(text)

  Description: Provides the method of querying the logical length of text.

  Return type: int

- threadpool_status()

  Description: Displays the status of worker threads and sessions in the thread pool.

  Return type: record

- get_local_active_session()

  Description: Provides sampling records of the historical active sessions stored in the memory of the current node.

  Return type: record

- pg_stat_get_thread()

  Description: Provides status information about all threads on the current node. Users with the **sysadmin** or **monitor admin** permission can view information about all threads, and common users can view only their own thread information.

  Return type: record

- pg_stat_get_sql_count()

  Description: Provides the counts of the **SELECT**, **UPDATE**, **INSERT**, **DELETE**, and **MERGE INTO** statements executed on the current node. Users with the **sysadmin** or **monitor admin** permission can view information about all users, and common users can view only their own statistics.

  Return type: record

- pg_stat_get_data_senders()

  Description: Provides detailed information about the data-copy sending thread active at the moment.

  Return type: record

- get_wait_event_info()

  Description: Provides detailed information about the wait event.

  Return type: record

- generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring)

  Description: Generates system diagnosis reports based on two snapshots. You need to run the command in the **postgres** database. By default, the initial user or the user with the **monadmin** permission can access the database. In V500R001C20SPC002 and earlier versions, the initial user or the user with the **sysadmin** permission can access the database. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 1** generate_wdr_report parameter description

  | Parameter     | Description                                                  | Range                                                        |
  | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | begin_snap_id | Snapshot ID that starts the diagnosis report period.         | -                                                            |
  | end_snap_id   | Snapshot ID that ends the diagnosis report period. By default, the value of **end_snap_id** is greater than that of **begin_snap_id**. | -                                                            |
  | report_type   | Specifies the type of the generated report.                  | **summary****detail****all**: Both **summary** and **detail** types are included. |
  | report_scope  | Specifies the scope for a report to be generated.            | **cluster**: database-level information**node**: node-level information |
  | node_name     | When **report\_scope** is set to **node**, set this parameter to the name of the corresponding node. (You can run the **select \* from pg_node_env;** command to query the node name.)If **report\_scope** is set to **cluster**, this parameter can be omitted, left blank, or set to **NULL**. | **cluster**: This value is omitted, left blank or set to **NULL**.**node**: indicates the node name in MogDB. |

- create_wdr_snapshot()

  Description: Manually generates system diagnosis snapshots. This function requires the **sysadmin** permission.

  Return type: text

- kill_snapshot()

  Description: Kills the WDR snapshot backend thread. Users who invoke this function must have the **SYSADMIN** permission, the **REPLICATION** permission, or inherit the **gs_role_replication** permission of the built-in role.

  Return type: void

- capture_view_to_json(text,integer)

  Description: Saves the view result to the directory specified by GUC: **perf_directory**. If **is_crossdb** is set to **1**, the view is accessed once for all databases. If the value of **is_crossdb** is **0**, the current database is accessed only once. Only users with the **sysadmin** or **monitor admin** permission can execute this function.

  Return type: int

- reset_unique_sql

  Description: Clears the unique SQL statements in the memory of the database node. (The **sysadmin** permission is required.)

  Return type: Boolean

  **Table 2** reset_unique_sql parameter description

  | Parameter   | Type | Description                                                  |
  | :---------- | :--- | :----------------------------------------------------------- |
  | scope       | text | Clearance scope type. The options are as follows:**GLOBAL**: Clears all nodes. If the value is **GLOBAL**, this function can be executed only on the primary node.**LOCAL**: Clears the current node. |
  | clean_type  | text | **BY_USERID**: Unique SQL statements are cleared based on user IDs.**BY_CNID**: Unique SQL statements are cleared based on primary node IDs.**ALL**: All data is cleared. |
  | clean_value | int8 | Clearance value corresponding to the clearance type.         |

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  >
  > - The values **GLOBAL** and **LOCAL** of **scope** apply to distributed databases. For MogDB, the values have the same meaning, indicating that the local node is cleared.
  > - The value **BY_CNID** of **clean_type** applies only to distributed databases and is invalid for MogDB.

- wdr_xdb_query(db_name_str text, query text)

  Description: Provides the capability of executing local cross-database queries. For example, when connecting to the Postgres database, access tables in the **test** database.

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  Return type: record

- pg_wlm_jump_queue(pid int)

    Description: Moves a task to the top of the queue of the primary node of the database.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- gs_wlm_switch_cgroup(pid int, cgroup text)

    Description: Moves a job to another Cgroup to change the job priority.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

    Description: Records information about the memory context **MemoryContextName** of the thread **tid** into the *threadid***_timestamp.log** file in the *$GAUSSLOG***/pg_log/***${node_name}***/dumpmem** directory. *threadid* can be obtained from *sessid* in the **GS_SESSION_MEMORY_DETAIL** view. In the officially released version, only the **MemoryContextName** that is an empty string (two single quotation marks indicate that the input is an empty string) is accepted. In this case, all memory context information is recorded. Otherwise, no operation is performed. For the DEBUG version for internal development and test personnel to debug, you can specify the **MemoryContextName** to be counted. In this case, all the memory usage of the context is recorded in the specified file. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- pg_shared_memctx_detail(MemoryContextName text)

    Description: Records information about the memory context **MemoryContextName** into the *threadid***_timestamp.log** file in the *$GAUSSLOG***/pg_log/***${node_name}***/dumpmem** directory. This function is provided only for internal development and test personnel to debug in the DEBUG version. Calling this function in the officially released version does not involve any operation. Only the administrator can execute this function.

    Return type: Boolean

  - **true**: success
  - **false**: failure

- local_bgwriter_stat()

  Description: Displays the information about pages flushed by the bgwriter thread of this instance, number of pages in the candidate buffer chain, and buffer elimination information.

  Return type: record

- local_candidate_stat()

  Description: Displays the number of pages in the candidate buffer chain of this instance and buffer elimination information, including the normal buffer pool and segment buffer pool.

  Return type: record

- local_ckpt_stat()

  Description: Displays the information about checkpoints and flushing pages of the current instance.

  Return type: record

- local_double_write_stat()

  Description: Displays the doublewrite file status of the current instance.

  Return type: record

- local_single_flush_dw_stat()

  Description: Displays the elimination of dual-write files on a single page in the instance.

  Return type: record

- local_pagewriter_stat()

  Description: Displays the page flushing information and checkpoint information of the current instance.

  Return type: record

- local_redo_stat()

  Description: Displays the replay status of the current standby instance.

  Return type: record

  Note: The returned replay status includes the current replay position and the replay position of the minimum restoration point.

- local_recovery_status()

  Description: Displays log flow control information about the primary and standby nodes.

  Return type: record

- gs_wlm_node_recover(boolean isForce)

  Description: Obtains top SQL query statement-level statistics recorded in the current memory. If the input parameter is not **0**, the information is cleared from the memory.

  Return type: record

- gs_wlm_node_clean(cstring nodename)

  Description: Clears data after the dynamic load management node is faulty. Only administrators can execute this function. This function is called by the cluster management module. You are not advised to directly call this function. This view is not supported in a centralized or standalone system.

  Return type: Boolean

- gs_cgroup_map_ng_conf(group name)

  Description: Reads the Cgroup configuration file of a specified logical database.

  Return type: record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  Description: Switches the Cgroup of a specified session.

  Return type: record

- hdfs_fdw_handler()

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- hdfs_fdw_validator(text[], oid)

  Description: Rewrites a foreign table. It is a function to be defined when a foreign table is defined.

  Return type: record

- comm_client_info()

  Description: Queries information about active client connections of a single node.

  Return type: SETOF record

- pg_sync_cstore_delta(text)

  Description: Synchronizes the delta table structure of a specified column-store table with that of the column-store primary table.

  Return type: bigint

- pg_sync_cstore_delta()

  Description: Synchronizes the delta table structure of all column-store tables with that of the column-store primary table.

  Return type: bigint

- pg_get_flush_lsn()

  Description: Returns the location of the Xlog flushed from the current node.

  Return type: text

- pg_get_sync_flush_lsn()

  Description: Returns the location of the Xlog flushed by the majority on the current node.

  Return type: text

- gs_create_log_tables()

  Description: Creates foreign tables and views for run logs and performance logs.

  Return type: void

  Example:

  ```
  mogdb=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains full SQL information at the database level. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 3** dbe_perf.get_global_full_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | :-------------- | :----------------------- | :--------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  Description: Obtains slow SQL information at the database level. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: record

  **Table 4** dbe_perf.get_global_slow_sql_by_timestamp parameter description

  | Parameter       | Type                     | Description                              |
  | :-------------- | :----------------------- | :--------------------------------------- |
  | start_timestamp | timestamp with time zone | Start point of the SQL start time range. |
  | end_timestamp   | timestamp with time zone | End point of the SQL start time range.   |

- statement_detail_decode(detail text, format text, pretty boolean)

  Description: Parses the **details** column in a full or slow SQL statement. The result can be queried only in the system database but cannot be queried in the user database.

  Return type: text

  **Table 5** statement_detail_decode parameter description

  | Parameter  | Type    | Description                                                  |
  | :--------- | :------ | :----------------------------------------------------------- |
  | **detail** | text    | Set of events generated by the SQL statement (unreadable).   |
  | format     | text    | Parsing output format. The value is **plaintext**.           |
  | pretty     | boolean | Whether to display the text in pretty format when **format** is set to **plaintext**. The options are as follows:The value **true** indicates that events are separated by **\n**.The value **false** indicates that events are separated by commas (,). |

- get_prepared_pending_xid

  Description: Returns nextxid when restoration is complete.

  Parameter: nan

  Return type: text

- pg_clean_region_info

  Description: Clears the regionmap.

  Parameter: nan

  Return type: character varying

- pg_get_delta_info

  Description: Obtains delta information from a single DN.

  Parameter: rel text, schema_name text

  Return type: part_name text, live_tuple bigint, data_size bigint, and blocknum bigint

- pg_get_replication_slot_name

  Description: Obtains the slot name.

  Parameter: nan

  Return type: text

- pg_get_running_xacts

  Description: Obtains running transactions.

  Parameter: **nan**

  Return type: handle integer, gxid xid, state tinyint, node text, xmin xid, vacuum boolean, timeline bigint, prepare_xid xid, pid bigint, and next_xid xid

- pg_get_variable_info

  Description: Obtains the shared memory variable *cache*.

  Parameter: nan

  Return type: node_name text, nextOid oid, nextXid xid, oldestXid xid, xidVacLimit xid, oldestXidDB oid, lastExtendCSNLogpage xid, startExtendCSNLogpage xid, nextCommitSeqNo xid, latestCompletedXid xid, and startupMaxXid xid

- pg_get_xidlimit

  Description: Obtains transaction ID information from the shared memory.

  Parameter: nan

  Return type: nextXid xid, oldestXid xid, xidVacLimit xid, xidWarnLimit xid, xidStopLimit xid, xidWrapLimit xid, and oldestXidDB oid

- get_global_user_transaction()

  Description: Returns transaction information about each user on all nodes.

  Return type: node_name name, usename name, commit_counter bigint, rollback_counter bigint, resp_min bigint, resp_max bigint, resp_avg bigint, resp_total bigint, bg_commit_counter bigint, bg_rollback_counter bigint, bg_resp_min bigint, bg_resp_max bigint, bg_resp_avg bigint, and bg_resp_total bigint

- pg_collation_for

  Description: Returns the sorting rule corresponding to the input parameter string.

  Parameter: any (Explicit type conversion is required for constants.)

  Return type: text

- pgxc_unlock_for_sp_database(name Name)

  Description: Releases a specified database lock.

  Parameter: database name

  Return type: Boolean

- pgxc_lock_for_sp_database(name Name)

  Description: Locks a specified database.

  Parameter: database name

  Return type: Boolean

- copy_error_log_create()

  Description: Creates the error table (**public.pgxc_copy_error_log**) required for creating the **COPY FROM** error tolerance mechanism.

  Return type: Boolean

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  >
  > - This function attempts to create the **public.pgxc_copy_error_log** table. For details about the table, see Table 6.
  > - Create the B-tree index on the **relname** column and execute **REVOKE ALL on public.pgxc_copy_error_log FROM public** to manage permissions for the error table (the permissions are the same as those of the **COPY** statement).
  > - **public.pgxc_copy_error_log** is a row-store table. Therefore, this function can be executed and **COPY** error tolerance is available only when row-store tables can be created in the database instance. Note that after the GUC parameter **enable_hadoop_env** is enabled, row-store tables cannot be created in the database instance (the default value is **off** for MogDB).
  > - Same as the error table and the **COPY** statement, the function requires **sysadmin** or higher permissions.
  > - If the **public.pgxc_copy_error_log** table or the **copy_error_log_relname_idx** index exists before the function creates it, the function will report an error and roll back.

  **Table 6** Error table public.pgxc_copy_error_log

  | Column     | Type                     | Description                                                  |
  | :--------- | :----------------------- | :----------------------------------------------------------- |
  | relname    | character varying        | Table name in the form of *Schema name***.***Table name*     |
  | begintime  | timestamp with time zone | Time when a data format error was reported                   |
  | filename   | character varying        | Name of the source data file where a data format error occurs |
  | lineno     | bigint                   | Number of the row where a data format error occurs in a source data file |
  | rawrecord  | text                     | Raw record of a data format error in the source data file    |
  | **detail** | text                     | Error details                                                |

- gs_comm_proxy_thread_status()

  Description: Collects statistics on data packets sent and received by the proxy communication library **comm_proxy** when a user-mode network is configured for the cluster.

  Parameter: nan

  Return type: record

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:** The query result of this function is displayed only when the user-mode network is deployed in a centralized environment and **enable_dfx in comm_proxy_attr** is set to **true**. In other scenarios, an error message is displayed, indicating that queries are not supported.
