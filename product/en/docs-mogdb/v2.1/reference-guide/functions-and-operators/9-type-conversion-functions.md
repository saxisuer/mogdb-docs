---
title: Type Conversion Functions
summary: Type Conversion Functions
author: Zhang Cuiping
date: 2021-04-20
---

# Type Conversion Functions

- cash_words(money)

    Description: Type conversion function, which converts money into text.

    Example:

    ```sql
    mogdb=# SELECT cash_words('1.23');
                cash_words
    -----------------------------------
     One dollar and twenty three cents
    (1 row)
    ```

- cast(x as y)

  Description: Converts x into the type specified by y.

  Example:

  ```sql
  mogdb=# SELECT cast('22-oct-1997' as timestamp);
        timestamp
  ---------------------
   1997-10-22 00:00:00
  (1 row)
  ```

- hextoraw(raw)

  Description: Converts a string in hexadecimal format into the raw type.

  Return type: raw

  Example:

  ```sql
  mogdb=# SELECT hextoraw('7D');
   hextoraw
  ----------
   7D
  (1 row)
  ```

- numtoday(numeric)

  Description: Converts values of the number type into the timestamp of the specified type.

  Return type: timestamp

  Example:

  ```sql
  mogdb=# SELECT numtoday(2);
   numtoday
  ----------
   2 days
  (1 row)
  ```

- pg_systimestamp()

  Description: Obtains the system timestamp.

  Return type: timestamp with time zone

  Example:

  ```sql
  mogdb=# SELECT pg_systimestamp();
          pg_systimestamp
  -------------------------------
   2015-10-14 11:21:28.317367+08
  (1 row)
  ```

- rawtohex(string)

  Description: Converts a string in binary format into hexadecimal format.

  The result is the ACSII code of the input characters in hexadecimal format.

  Return type: varchar

  Example:

  ```sql
  mogdb=# SELECT rawtohex('1234567');
      rawtohex
  ----------------
   31323334353637
  (1 row)
  ```

- to_bigint(varchar)

    Description: Converts the character type to the bigint type.

    Return type: bigint

    Example:

    ```sql
    mogdb=# SELECT to_bigint('123364545554455');
        to_bigint
    ----------------
     123364545554455
    (1 row)
    ```

- to_char (datetime/interval [, fmt]) <a id = 'to_char'> </a>

  Description: Converts a DATETIME or INTERVAL value of the DATE/TIMESTAMP/TIMESTAMP WITH TIME ZONE/TIMESTAMP WITH LOCAL TIME ZONE type into the VARCHAR type according to the format specified by **fmt**.

  - The optional parameter **fmt** allows for the following types: date, time, week, quarter, and century. Each type has a unique template. The templates can be combined together. Common templates include: HH, MM, SS, YYYY, MM, and DD.
  - A template may have a modification word. FM is a common modification word and is used to suppress the preceding zero or the following blank spaces.
  - For TIMESTAMP WITH TIME ZONE, you can use TZH, TZM, TZD, TZR parameters in TO_CHAR to output time zone information.
  
  Return type: varchar
  
  Example:
  
  ```sql
  mogdb=# SELECT to_char(current_timestamp,'HH12:MI:SS');
   to_char
  ----------
   10:19:26
  (1 row)
  ```
  
  ```sql
  mogdb=# SELECT to_char(current_timestamp,'FMHH12:FMMI:FMSS');
   to_char
  ----------
   10:19:46
  (1 row)
  ```
  
  ```sql
  mogdb=# select to_char(systimestamp, 'TZH'); 
  to_char
  --------- 
  +08
  (1 row)
  
  mogdb=# select to_char(systimestamp, 'TZM');
  to_char
  ---------
  00
  (1 row)
  
  mogdb=# select to_char(systimestamp, 'TZD');
  to_char
  ---------
  CST
  (1 row)
  
  mogdb=# select to_char(systimestamp, 'TZR');
  to_char
  ---------------
  Asia/Shanghai
  (1 row)
  ```
  
  **Table 1** Template Patterns for Date/Time Formatting
  
  | Pattern                          | Description                                                  |
  | :------------------------------- | :----------------------------------------------------------- |
  | `HH`                             | hour of day (01-12)                                          |
  | `HH12`                           | hour of day (01-12)                                          |
  | `HH24`                           | hour of day (00-23)                                          |
  | `MI`                             | minute (00-59)                                               |
  | `SS`                             | second (00-59)                                               |
  | `MS`                             | millisecond (000-999)                                        |
  | `US`                             | microsecond (000000-999999)                                  |
  | `SSSS`                           | seconds past midnight (0-86399)                              |
  | `AM`, `am`, `PM` or `pm`         | meridiem indicator (without periods)                         |
  | `A.M.`, `a.m.`, `P.M.` or `p.m.` | meridiem indicator (with periods)                            |
  | `Y,YYY`                          | year (4 or more digits) with comma                           |
  | `YYYY`                           | year (4 or more digits)                                      |
  | `YYY`                            | last 3 digits of year                                        |
  | `YY`                             | last 2 digits of year                                        |
  | `Y`                              | last digit of year                                           |
  | `IYYY`                           | ISO 8601 week-numbering year (4 or more digits)              |
  | `IYY`                            | last 3 digits of ISO 8601 week-numbering year                |
  | `IY`                             | last 2 digits of ISO 8601 week-numbering year                |
  | `I`                              | last digit of ISO 8601 week-numbering year                   |
  | `BC`, `bc`, `AD` or `ad`         | era indicator (without periods)                              |
  | `B.C.`, `b.c.`, `A.D.` or `a.d.` | era indicator (with periods)                                 |
  | `MONTH`                          | full upper case month name (blank-padded to 9 chars)         |
  | `Month`                          | full capitalized month name (blank-padded to 9 chars)        |
  | `month`                          | full lower case month name (blank-padded to 9 chars)         |
  | `MON`                            | abbreviated upper case month name (3 chars in English, localized lengths vary) |
  | `Mon`                            | abbreviated capitalized month name (3 chars in English, localized lengths vary) |
  | `mon`                            | abbreviated lower case month name (3 chars in English, localized lengths vary) |
  | `MM`                             | month number (01-12)                                         |
  | `DAY`                            | full upper case day name (blank-padded to 9 chars)           |
  | `Day`                            | full capitalized day name (blank-padded to 9 chars)          |
  | `day`                            | full lower case day name (blank-padded to 9 chars)           |
  | `DY`                             | abbreviated upper case day name (3 chars in English, localized lengths vary) |
  | `Dy`                             | abbreviated capitalized day name (3 chars in English, localized lengths vary) |
  | `dy`                             | abbreviated lower case day name (3 chars in English, localized lengths vary) |
  | `DDD`                            | day of year (001-366)                                        |
  | `IDDD`                           | day of ISO 8601 week-numbering year (001-371; day 1 of the year is Monday of the first ISO week) |
  | `DD`                             | day of month (01-31)                                         |
  | `D`                              | day of the week, Sunday (`1`) to Saturday (`7`)              |
  | `ID`                             | ISO 8601 day of the week, Monday (`1`) to Sunday (`7`)       |
  | `W`                              | week of month (1-5) (the first week starts on the first day of the month) |
  | `WW`                             | week number of year (1-53) (the first week starts on the first day of the year) |
  | `IW`                             | week number of ISO 8601 week-numbering year (01-53; the first Thursday of the year is in week 1) |
  | `CC`                             | century (2 digits) (the twenty-first century starts on 2001-01-01) |
  | `J`                              | Julian Day (integer days since November 24, 4714 BC at midnight UTC) |
  | `Q`                              | quarter                                                      |
  | `RM`                             | month in upper case Roman numerals (I-XII; I=January)        |
  | `rm`                             | month in lower case Roman numerals (i-xii; i=January)        |
  | `TZ`                             | upper case time-zone abbreviation (only supported in `to_char`) |
  | `tz`                             | lower case time-zone abbreviation (only supported in `to_char`) |
  | `TZH`                            | Time zone hour                                               |
  | `TZM`                            | Time zone minute                                             |
  | `TZD`                            | Time zone daylight saving time                               |
  | `TZR`                            | Time zone region                                             |

- to_char(double precision/real, text)

  Description: Converts the values of the floating point type into the strings in the specified format.

  Return type: text

  Example:

  ```sql
  mogdb=# SELECT to_char(125.8::real, '999D99');
   to_char
  ---------
    125.80
  (1 row)
  ```

- to_char (numeric/smallint/integer/bigint/double precision/real[, fmt])

  Descriptions: Converts an integer or a value in floating point format into a string in specified format.

  - The optional parameter **fmt** allows for the following types: decimal characters, grouping characters, positive/negative sign and currency sign. Each type has a unique template. The templates can be combined together. Common templates include: 9, 0, millesimal sign (,), and decimal point (.).
  - A template can have a modification word, similar to FM. However, FM does not suppress 0 which is output according to the template.
  - Use the template X or x to convert an integer value into a string in hexadecimal format.

  Return type: varchar

  Example:

  ```sql
  mogdb=# SELECT to_char(1485,'9,999');
   to_char
  ---------
    1,485
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char( 1148.5,'9,999.999');
    to_char
  ------------
    1,148.500
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char(148.5,'990999.909');
     to_char
  -------------
      0148.500
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char(123,'XXX');
   to_char
  ---------
     7B
  (1 row)
  ```

- to_char(interval, text)

  Description: Converts the values of the time interval type into the strings in the specified format.

  Return type: text

  Example:

  ```sql
  mogdb=# SELECT to_char(interval '15h 2m 12s', 'HH24:MI:SS');
   to_char
  ----------
   15:02:12
  (1 row)
  ```

- to_char(int, text)

  Description: Converts the values of the integer type into the strings in the specified format.

  Return type: text

  Example:

  ```sql
  mogdb=# SELECT to_char(125, '999');
   to_char
  ---------
    125
  (1 row)
  ```

- to_char(numeric, text)

  Description: Converts the values of the numeric type into the strings in the specified format.

  Return type: text

  Example:

  ```sql
  mogdb=# SELECT to_char(-125.8, '999D99S');
   to_char
  ---------
   125.80-
  (1 row)
  ```

- to_char (string)

  Description: Converts the CHAR/VARCHAR/VARCHAR2/CLOB type into the VARCHAR type.

  If this function is used to convert data of the CLOB type, and the value to be converted exceeds the value range of the target type, an error is returned.

  Return type: varchar

  Example:

  ```sql
  mogdb=# SELECT to_char('01110');
   to_char
  ---------
   01110
  (1 row)
  ```

- to_char(timestamp, text)

  Description: Converts the values of the timestamp type into the strings in the specified format.

  Return type: text

  Example:

  ```sql
  mogdb=# SELECT to_char(current_timestamp, 'HH12:MI:SS');
   to_char
  ----------
   10:55:59
  (1 row)
  ```

- to_clob(char/nchar/varchar/varchar2/nvarchar2/text/raw)

  Description: Converts the RAW type or text character set type CHAR/NCHAR/VARCHAR/VARCHAR2/NVARCHAR2/TEXT into the CLOB type.

  Return type: clob

  Example:

  ```sql
  mogdb=# SELECT to_clob('ABCDEF'::RAW(10));
   to_clob
  ---------
   ABCDEF
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('hello111'::CHAR(15));
   to_clob
  ----------
   hello111
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss123'::NCHAR(10));
   to_clob
  ----------
   gauss123
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss234'::VARCHAR(10));
   to_clob
  ----------
   gauss234
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss345'::VARCHAR2(10));
   to_clob
  ----------
   gauss345
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss456'::NVARCHAR2(10));
   to_clob
  ----------
   gauss456
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('World222!'::TEXT);
    to_clob
  -----------
   World222!
  (1 row)
  ```

- to_date(text)

  Description: Converts values of the text type into the timestamp in the specified format. Currently, only the following two formats are supported:

  - Format 1: Date without separators, for example, 20150814. The value must contain the complete year, month, and day.
  - Format 2: Date with separators, for example, 2014-08-14. The separator can be any non-digit character.

  Return type: timestamp without time zone

  Example:

  ```sql
    mogdb=# SELECT to_date('2015-08-14');
           to_date
    ---------------------
     2015-08-14 00:00:00
    (1 row)
  ```

- to_date(text, text)

  Description: Converts the values of the string type into the dates in the specified format.

  Return type: timestamp without time zone

  Example:

  ```sql
  mogdb=# SELECT to_date('05 Dec 2000', 'DD Mon YYYY');
         to_date
  ---------------------
   2000-12-05 00:00:00
  (1 row)
  ```

- to_number ( expr [, fmt])

  Description: Converts **expr** into a value of the NUMBER type according to the specified format.

  For details about the type conversion formats, see [Table 2](#template-patterns).

  If a hexadecimal string is converted into a decimal number, the hexadecimal string can include a maximum of 16 bytes if it is to be converted into a sign-free number.

  During the conversion from a hexadecimal string to a decimal digit, the format string cannot have a character other than x or X. Otherwise, an error is reported.

  Return type: number

  Example:

  ```sql
  mogdb=# SELECT to_number('12,454.8-', '99G999D9S');
   to_number
  -----------
    -12454.8
  (1 row)
  ```

- to_number(text, text)

  Description: Converts the values of the string type into the numbers in the specified format.

  Return type: numeric

  Example:

  ```sql
  mogdb=# SELECT to_number('12,454.8-', '99G999D9S');
   to_number
  -----------
    -12454.8
  (1 row)
  ```

- to_timestamp(double precision)

  Description: Converts a UNIX century into a timestamp.

  Return type: timestamp with time zone

  Example:

  ```sql
  mogdb=# SELECT to_timestamp(1284352323);
        to_timestamp
  ------------------------
   2010-09-13 12:32:03+08
  (1 row)
  ```

- to_timestamp(string [,fmt])

  Description: Converts a string into a value of the timestamp type according to the format specified by **fmt**. When **fmt** is not specified, perform the conversion according to the format specified by **nls_timestamp_format**.

  In **to_timestamp** in MogDB,

  - If the input year *YYYY* is 0, an error will be reported.
  - If the input year *YYYY* is less than 0, specify *SYYYY* in **fmt**. The year with the value of n (an absolute value) BC will be output correctly.

  Characters in the **fmt** must match the schema for formatting the data and time. Otherwise, an error is reported.

  Return type: timestamp without time zone

  Example:

  ```sql
  mogdb=# SHOW nls_timestamp_format;
      nls_timestamp_format
  ----------------------------
   DD-Mon-YYYY HH:MI:SS.FF AM
  (1 row)

  mogdb=# SELECT to_timestamp('12-sep-2014');
      to_timestamp
  ---------------------
   2014-09-12 00:00:00
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('12-Sep-10 14:10:10.123000','DD-Mon-YY HH24:MI:SS.FF');
        to_timestamp
  -------------------------
   2010-09-12 14:10:10.123
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('-1','SYYYY');
        to_timestamp
  ------------------------
   0001-01-01 00:00:00 BC
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('98','RR');
      to_timestamp
  ---------------------
   1998-01-01 00:00:00
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('01','RR');
      to_timestamp
  ---------------------
   2001-01-01 00:00:00
  (1 row)
  ```

- to_timestamp(text, text)

  Description: Converts values of the string type into the timestamp of the specified type.

  Return type: timestamp

  Example:

  ```sql
  mogdb=# SELECT to_timestamp('05 Dec 2000', 'DD Mon YYYY');
      to_timestamp
  ---------------------
   2000-12-05 00:00:00
  (1 row)
  ```

**Table 2** Template patterns for numeric formatting<a id="template-patterns"> </a>

| Pattern    | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| 9          | Value with specified digits                                  |
| 0          | Values with leading zeros                                    |
| Period (.) | Decimal point                                                |
| Comma (,)  | Group (thousand) separator                                   |
| PR         | Negative values in angle brackets                            |
| S          | Sign anchored to number (uses locale)                        |
| L          | Currency symbol (uses locale)                                |
| D          | Decimal point (uses locale)                                  |
| G          | Group separator (uses locale)                                |
| MI         | Minus sign in the specified position (if the number is less than 0) |
| PL         | Plus sign in the specified position (if the number is greater than 0) |
| SG         | Plus or minus sign in the specified position                 |
| RN         | Roman numerals (the input values are between 1 and 3999)     |
| TH or th   | Ordinal number suffix                                        |
| V          | Shifts specified number of digits (decimal)                  |

- abstime_text

    Description: Converts abstime to text.

    Parameter: abstime

    Return type: text

- abstime_to_smalldatetime

    Description: Converts abstime to smalldatatime.

    Parameter: abstime

    Return type: smalldatetime

- bigint_tid

    Description: Converts bigint to tid.

    Parameter: bigint

    Return type: tid

- bool_int1

    Description: Converts bool to int1.

    Parameter: Boolean

    Return type: tinyint

- bool_int2

    Description: Converts bool to int2.

    Parameter: Boolean

    Return type: smallint

- bool_int8

    Description: Converts bool to int8.

    Parameter: Boolean

    Return type: bigint

- bpchar_date

    Description: Converts a string to a date.

    Parameter: character

    Return type: date

- bpchar_float4

    Description: Converts a string to float4.

    Parameter: character

    Return type: real

- bpchar_float8

    Description: Converts a string to float8.

    Parameter: character

    Return type: double precision

- bpchar_int4

    Description: Converts a string to int4.

    Parameter: character

    Return type: integer

- bpchar_int8

    Description: Converts a string to int8.

    Parameter: character

    Return type: bigint

- bpchar_numeric

    Description: Converts a string to numeric.

    Parameter: character

    Return type: numeric

- bpchar_timestamp

    Description: Converts a string to a timestamp.

    Parameter: character

    Return type: timestamp without time zone

- bpchar_to_smalldatetime

    Description: Converts a string to smalldatetime.

    Parameter: character

    Return type: smalldatetime

- cupointer_bigint

    Description: Converts the column-store CU pointer type to the bigint type.

    Parameter: text

    Return type: bigint

- date_bpchar

    Description: Converts the date type to bpchar.

    Parameter: date

    Return type: character

- date_text

    Description: Converts date to text.

    Parameter: date

    Return type: text

- date_varchar

    Description: Converts date to varchar.

    Parameter: date

    Return type: character varying

- f4toi1

    Description: Forcibly converts float4 to uint8.

    Parameter: real

    Return type: tinyint

- f8toi1

    Description: Forcibly converts float8 to uint8.

    Parameter: double precision

    Return type: tinyint

- float4_bpchar

    Description: Converts float4 to bpchar.

    Parameter: real

    Return type: character

- float4_text

    Description: Converts float4 to text.

    Parameter: real

    Return type: text

- float4_varchar

    Description: Converts float4 to varchar.

    Parameter: real

    Return type: character varying

- float8_bpchar

    Description: Converts float4 to bpchar.

    Parameter: double precision

    Return type: character

- float8_interval

    Description: Converts float4 to interval.

    Parameter: double precision

    Return type: interval

- float8_text

    Description: Converts float8 to text.

    Parameter: double precision

    Return type: text

- float8_varchar

    Description: Converts float8 to varchar.

    Parameter: double precision

    Return type: character varying

- i1tof4

    Description: Converts uint8 to float4.

    Parameter: tinyint

    Return type: real

- i1tof8

    Description: Converts uint8 to float8.

    Parameter: tinyint

    Return type: double precision

- i1toi2

    Description: Converts uint8 to int16.

    Parameter: tinyint

    Return type: smallint

- i1toi4

    Description: Converts uint8 to int32.

    Parameter: tinyint

    Return type: integer

- i1toi8

    Description: Converts uint8 to int64.

    Parameter: tinyint

    Return type: bigint

- i2toi1

    Description: Converts int16 to uint8.

    Parameter: smallint

    Return type: tinyint

- i4toi1

    Description: Converts int32 to uint8.

    Parameter: integer

    Return type: tinyint

- i8toi1

    Description: Converts int64 to uint8.

    Parameter: bigint

    Return type: tinyint

- int1_avg_accum

    Description: Adds the second parameter of the uint8 type to the first parameter. The first parameter is an array of the bigint type.

    Parameter: bigint[], tinyint

    Return type: bigint[]

- int1_bool

    Description: Converts uint8 to bool.

    Parameter: tinyint

    Return type: Boolean

- int1_bpchar

    Description: Converts uint8 to bpchar.

    Parameter: tinyint

    Return type: character

- int1_mul_cash

    Description: Returns the product of a parameter of the int8 type and a parameter of the cash type. The return type is cash.

    Parameter: tinyint, money

    Return type: money

- int1_numeric

    Description: Converts uint8 to numeric.

    Parameter: tinyint

    Return type: numeric

- int1_nvarchar2

    Description: Converts uint8 to nvarchar2.

    Parameter: tinyint

    Return type: nvarchar2

- int1_text

    Description: Converts uint8 to text.

    Parameter: tinyint

    Return type: text

- int1_varchar

    Description: Converts uint8 to varchar.

    Parameter: tinyint

    Return type: character varying

- int1in

    Description: Converts a string into an unsigned 1-byte integer.

    Parameter: cstring

    Return type: tinyint

- int1out

    Description: Converts an unsigned 1-byte integer into a string.

    Parameter: tinyint

    Return type: cstring

- int1up

    Description: Converts an input integer to an unsigned 1-byte integer.

    Parameter: tinyint

    Return type: tinyint

- int2_bool

    Description: Converts a signed two-byte integer to the bool type.

    Parameter: smallint

    Return type: Boolean

- int2_bpchar

    Description: Converts a signed two-byte integer to the bpchar type.

    Parameter: smallint

    Return type: character

- int2_text

    Description: Converts a signed two-byte integer to the text type.

    Parameter: smallint

    Return type: text

- int2_varchar

    Description: Converts a signed two-byte integer to the varchar type.

    Parameter: smallint

    Return type: character varying

- int8_text

    Description: Converts an eight-byte signed integer to the text type.

    Parameter: bigint

    Return type: text

- int8_varchar

    Description: Converts an eight-byte signed integer to varchar.

    Parameter: bigint

    Return type: character varying

- intervaltonum

    Description: Converts the internal dats type date to numeric.

    Parameter: interval

    Return type: numeric

- numeric_bpchar

    Description: Converts numeric to bpchar.

    Parameter: numeric

    Return type: character

- numeric_int1

    Description: Converts numeric to a signed one-byte integer.

    Parameter: numeric

    Return type: tinyint

- numeric_text

    Description: Converts numeric to text.

    Parameter: numeric

    Return type: text

- numeric_varchar

    Description: Converts numeric to varchar.

    Parameter: numeric

    Return type: character varying

- nvarchar2in

    Description: Converts c string to varchar.

    Parameter: cstring, oid, integer

    Return type: nvarchar2

- nvarchar2out

    Description: Converts text into a c string.

    Parameter: nvarchar2

    Return type: cstring

- nvarchar2send

    Description: Converts varchar to binary.

    Parameter: nvarchar2

    Return type: bytea

- oidvectorin_extend

    Description: Converts a string to oidvector.

    Parameter: cstring

    Return type: oidvector_extend

- oidvectorout_extend

    Description: Converts oidvector to a string.

    Parameter: oidvector_extend

    Return type: cstring

- oidvectorsend_extend

    Description: Converts oidvector to a string.

    Parameter: oidvector_extend

    Return type: bytea

- reltime_text

    Description: Converts reltime to text.

    Parameter: reltime

    Return type: text

- text_date

    Description: Converts the text type to the date type.

    Parameter: text

    Return type: date

- text_float4

    Description: Converts text to float4.

    Parameter: text

    Return type: real

- text_float8

    Description: Converts the text type to float8.

    Parameter: text

    Return type: double precision

- text_int1

    Description: Converts the text type to int1.

    Parameter: text

    Return type: tinyint

- text_int2

    Description: Converts the text type to the int2 type.

    Parameter: text

    Return type: smallint

- text_int4

    Description: Converts the text type to int4.

    Parameter: text

    Return type: integer

- text_int8

    Description: Converts the text type to the int8 type.

    Parameter: text

    Return type: bigint

- text_numeric

    Description: Converts the text type to the numeric type.

    Parameter: text

    Return type: numeric

- text_timestamp

    Description: Converts the text type to the timestamp type.

    Parameter: text

    Return type: timestamp without time zone

- time_text

    Description: Converts the time type to the text type.

    Parameter: time without time zone

    Return type: text

- timestamp_text

    Description: Converts the timestamp type to the text type.

    Parameter: timestamp without time zone

    Return type: text

- timestamp_to_smalldatetime

    Description: Converts the timestamp type to the smalldatetime type.

    Parameter: timestamp without time zone

    Return type: smalldatetime

- timestamp_varchar

    Description: Converts the timestamp type to varchar.

    Parameter: timestamp without time zone

    Return type: character varying

- timestamptz_to_smalldatetime

    Description: Converts timestamptz to smalldatetime.

    Parameter: timestamp with time zone

    Return type: smalldatetime

- timestampzone_text

    Description: Converts the timestampzone type to the text type.

    Parameter: timestamp with time zone

    Return type: text

- timetz_text

    Description: Converts the timetz type to the text type.

    Parameter: time with time zone

    Return type: text

- to_integer

    Description: Converts data to the integer type.

    Parameter: character varying

    Return type: integer

- to_interval

    Description: Converts to the interval type.

    Parameter: character varying

    Return type: interval

- to_numeric

    Description: Converts to the numeric type.

    Parameter: character varying

    Return type: numeric

- to_nvarchar2

    Description: Converts to the nvarchar2 type.

    Parameter: numeric

    Return type: nvarchar2

- to_text

    Description: Converts to the text type.

    Parameter: smallint

    Return type: text

- to_ts

    Description: Converts to the ts type.

    Parameter: character varying

    Return type: timestamp without time zone

- to_varchar2

    Description: Converts to the varchar2 type.

    Parameter: timestamp without time zone

    Return type: character varying

- varchar_date

    Description: Converts varchar to date.

    Parameter: character varying

    Return type: date

- varchar_float4

    Description: Converts varchar to float4.

    Parameter: character varying

    Return type: real

- varchar_float8

    Description: Converts the varchar type to the float8 type.

    Parameter: character varying

    Return type: double precision

- varchar_int4

    Description: Converts the type from varchar to int4.

    Parameter: character varying

    Return type: integer

- varchar_int8

    Description: Converts the varchar type to the int8 type.

    Parameter: character varying

    Return type: bigint

- varchar_numeric

    Description: Converts varchar to numeric.

    Parameter: character varying

    Return type: numeric

- varchar_timestamp

    Description: Converts varchar to timestamp.

    Parameter: character varying

    Return type: timestamp without time zone

- varchar2_to_smlldatetime

    Description: Converts varchar2 to smlldatetime.

    Parameter: character varying

    Return type: smalldatetime

- xidout4

    Description: The xid output is a four-byte number.

    Parameter: xid32

    Return type: cstring

- xidsend4

    Description: Converts xid to the binary format.

    Parameter: xid32

    Return type: bytea

## Encoding Type Conversion

- convert_to_nocase(text, text)

    Description: Converts a string into a specified encoding type.

    Return type: bytea

    Example:

    ```sql
    mogdb=# SELECT convert_to_nocase('12345', 'GBK');
     convert_to_nocase
    -------------------
     \x3132333435
    (1 row)
    ```
