---
title: INFORMATION_SCHEMA_CATALOG_NAME
summary: INFORMATION_SCHEMA_CATALOG_NAME
author: Guo Huan
date: 2021-06-23
---

# INFORMATION_SCHEMA_CATALOG_NAME

**INFORMATION_SCHEMA_CATALOG_NAME** displays the name of the current database.

**Table 1** INFORMATION_SCHEMA_CATALOG_NAME columns

| Name         | Type                              | Description           |
| :----------- | :-------------------------------- | :-------------------- |
| catalog_name | information_schema.sql_identifier | Current database name |
