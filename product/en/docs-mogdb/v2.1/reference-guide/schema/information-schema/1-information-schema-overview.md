---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-06-23
---

# Overview

An information schema named **INFORMATION_SCHEMA** automatically exists in all databases. An information schema consists of a group of views that contain information about objects defined in the current database. The owner of this schema is the initial database user, and the user naturally has all the privileges on this schema, including the ability to delete the schema.

Information schemas are inherited from the open-source PGXC and PG. For details, visit the following links to see the official PGXC and PG documents:

<http://postgres-xc.sourceforge.net/docs/1_1/information-schema.html>

<https://www.postgresql.org/docs/9.2/information-schema.html>

The following sections display only the views that are not listed in the preceding links.
