---
title: DBE_PLDEBUGGER.info_locals
summary: DBE_PLDEBUGGER.info_locals
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEBUGGER.info_locals

During debugging on the debug end, **info_locals** is invoked to print the variables in the current stored procedure. The input parameter **frameno** of this function indicates the stack layer to be traversed. This function can be invoked without input parameters. By default, the top-layer stack variable is queried.

**Table 1** info_locals input parameters and return values

| Name         | Type                  | Description                                                  |
| :----------- | :-------------------- | :----------------------------------------------------------- |
| frameno      | IN integer (optional) | Specified stack layer. The default value is the top layer.   |
| varname      | OUT text              | Variable name                                                |
| vartype      | OUT text              | Variable type                                                |
| value        | OUT text              | Variable value                                               |
| package_name | OUT text              | Name of the package corresponding to the variable. If the variable is not a package, the value is null. |
| isconst      | OUT boolean           | Whether it is a constant                                     |