---
title: DBE_PLDEBUGGER.add_breakpoint
summary: DBE_PLDEBUGGER.add_breakpoint
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.add_breakpoint

During debugging on the debug end, call **add_breakpoint** to add a breakpoint. If **-1** is returned, the specified breakpoint is invalid. Determine the proper position of the breakpoint based on the **canbreak** column in [DBE_PLDEBUGGER.info_code](DBE_PLDEBUGGER.info_code.md).

**Table 1** add_breakpoint input parameters and return values

| Name         | Type        | Description       |
| :----------- | :---------- | :---------------- |
| funcoid      | IN text     | Function ID       |
| lineno       | IN integer  | Line number       |
| breakpointno | OUT integer | Breakpoint number |
