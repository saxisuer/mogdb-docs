---
title: DBE_PLDEBUGGER.backtrace
summary: DBE_PLDEBUGGER.backtrace
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.backtrace

During debugging on the debug end, call **backtrace** to view the current call stack.

**Table 1** backtrace input parameters and return values

| Name     | Type        | Description        |
| :------- | :---------- | :----------------- |
| frameno  | OUT integer | Call stack ID      |
| funcname | OUT oid     | Function name      |
| lineno   | OUT integer | Line number        |
| query    | OUT text    | Breakpoint content |
| funcoid  | OUT oid     | Function OID       |
