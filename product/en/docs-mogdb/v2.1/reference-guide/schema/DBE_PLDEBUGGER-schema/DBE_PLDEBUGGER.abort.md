---
title: DBE_PLDEBUGGER.abort
summary: DBE_PLDEBUGGER.abort
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.abort

This function is used to abort the stored procedure executed on the server and report an error. The return value indicates whether the abort message is successfully sent.

The function prototype is as follows:

```sql
DBE_PLDEBUGGER.abort()
RETURN boolean;
```

**Table 1** abort input parameters and return values

| Name  | Type        | Description        |
| :---- | :---------- | :----------------- |
| abort | OUT boolean | Success or failure |
