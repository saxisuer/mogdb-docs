---
title: DBE_PLDEBUGGER.delete_breakpoint
summary: DBE_PLDEBUGGER.delete_breakpoint
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.delete_breakpoint

During debugging on the debug end, call **delete_breakpoint** to delete the existing breakpoint.

**Table 1** delete_breakpoint input parameters and return values

| Name         | Type       | Description                                     |
| :----------- | :--------- | :---------------------------------------------- |
| breakpointno | IN integer | Breakpoint number                               |
| result       | OUT bool   | Specifies whether this operation is successful. |
