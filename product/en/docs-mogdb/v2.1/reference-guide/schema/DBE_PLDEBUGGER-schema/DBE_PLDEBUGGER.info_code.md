---
title: DBE_PLDEBUGGER.info_code
summary: DBE_PLDEBUGGER.info_code
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.info_code

During debugging on the debug end, **info_code** is invoked to view the source statement of the specified stored procedure and the line number corresponding to each line. The line number starts from the function body, and the line number in the function header is empty.

**Table 1** info_code input parameters and return values

| Name     | Type        | Description                                              |
| :------- | :---------- | :------------------------------------------------------- |
| funcoid  | IN oid      | Function ID                                              |
| lineno   | OUT integer | Line number                                              |
| query    | OUT text    | Source statement                                         |
| canbreak | OUT bool    | Specifies whether the current line supports breakpoints. |
