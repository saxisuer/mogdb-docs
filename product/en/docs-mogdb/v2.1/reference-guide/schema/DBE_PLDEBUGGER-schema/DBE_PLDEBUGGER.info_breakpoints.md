---
title: DBE_PLDEBUGGER.info_breakpoints
summary: DBE_PLDEBUGGER.info_breakpoints
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.info_breakpoints

During debugging on the debug end, call **info_breakpoints** to view the current function breakpoint.

**Table 1** info_breakpoints input parameters and return values

| Name         | Type        | Description        |
| :----------- | :---------- | :----------------- |
| breakpointno | OUT integer | Breakpoint number  |
| funcoid      | OUT oid     | Function ID        |
| lineno       | OUT integer | Line number        |
| query        | OUT text    | Breakpoint content |
