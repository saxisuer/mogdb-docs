---
title: DBE_PLDEBUGGER.next
summary: DBE_PLDEBUGGER.next
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.next

This function is used to execute the current SQL statement in a stored procedure and return the number of the next SQL statement and the corresponding query.

**Table 1** next input parameters and return values

| Name     | Type        | Description                                                  |
| :------- | :---------- | :----------------------------------------------------------- |
| funcoid  | OUT oid     | Function ID                                                  |
| funcname | OUT text    | Function name                                                |
| lineno   | OUT integer | Number of the next line in the current debugging process     |
| query    | OUT text    | Source code of the next line of the function that is being debugged |
