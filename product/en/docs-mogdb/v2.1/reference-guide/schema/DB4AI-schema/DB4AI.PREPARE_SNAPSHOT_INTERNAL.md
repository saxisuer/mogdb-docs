---
title: DB4AI.PREPARE_SNAPSHOT_INTERNAL
summary: DB4AI.PREPARE_SNAPSHOT_INTERNAL
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PREPARE_SNAPSHOT_INTERNAL

**PREPARE_SNAPSHOT_INTERNAL** is a built-in execution function of the DB4AI.PREPARE_SNAPSHOT function. The function involves information verification and cannot be directly invoked.

**Table 1** DB4AI.PREPARE_SNAPSHOT_INTERNAL input parameters and return values

| Parameter   | Type         | Description                                                  |
| :---------- | :----------- | :----------------------------------------------------------- |
| s_id        | IN BIGINT    | Snapshot ID                                                  |
| p_id        | IN BIGINT    | Parent snapshot ID                                           |
| m_id        | IN BIGINT    | Matrix ID                                                    |
| r_id        | IN BIGINT    | Root snapshot ID                                             |
| i_schema    | IN NAME      | Snapshot schema                                              |
| i_name      | IN NAME      | Snapshot name                                                |
| i_commands  | IN TEXT[]    | DDL and DML commands for modifying snapshots                 |
| i_comment   | IN TEXT      | Snapshot description                                         |
| i_owner     | IN NAME      | Snapshot owner                                               |
| i_idx       | INOUT INT    | Command index                                                |
| i_exec_cmds | INOUT TEXT[] | DDL and DML commands for execution                           |
| i_mapping   | IN NAME[]    | Maps user columns to backup columns. A rule is generated if the value is not **NULL**. |
