---
title: DB4AI.MANAGE_SNAPSHOT_INTERNAL
summary: DB4AI.MANAGE_SNAPSHOT_INTERNAL
author: Guo Huan
date: 2021-11-15
---

# DB4AI.MANAGE_SNAPSHOT_INTERNAL

**MANAGE_SNAPSHOT_INTERNAL** is a built-in execution function of the **DB4AI.PUBLISH_SNAPSHOT** and **DB4AI.ARCHIVE_SNAPSHOT** functions. The function involves information verification and cannot be directly invoked.

**Table 1** DB4AI.MANAGE_SNAPSHOT_INTERNAL input parameters and return values

| Parameter | Type                    | Description                          |
| :-------- | :---------------------- | :----------------------------------- |
| i_schema  | IN NAME                 | Name of the schema storing snapshots |
| i_name    | IN NAME                 | Snapshot name                        |
| publish   | IN BOOLEN               | Whether the snapshot is published    |
| res       | OUT db4ai.snapshot_name | Result                               |
