---
title: DB4AI.CREATE_SNAPSHOT
summary: DB4AI.CREATE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.CREATE_SNAPSHOT

**CREATE_SNAPSHOT** creates snapshots for the DB4AI feature. You can invoke **CREATE SNAPSHOT** to implement this function.

**Table 1** DB4AI.CREATE_SNAPSHOT input parameters and return values

| Parameter  | Type                    | Description                                                  |
| :--------- | :---------------------- | :----------------------------------------------------------- |
| i_schema   | IN NAME                 | Name of the schema storing snapshots. The default value is the current user or **PUBLIC**. |
| i_name     | IN NAME                 | Snapshot name                                                |
| i_commands | IN TEXT[]               | SQL commands for obtaining data                              |
| i_vers     | IN NAME                 | Version suffix                                               |
| i_comment  | IN TEXT                 | Snapshot description                                         |
| res        | OUT db4ai.snapshot_name | Result                                                       |
