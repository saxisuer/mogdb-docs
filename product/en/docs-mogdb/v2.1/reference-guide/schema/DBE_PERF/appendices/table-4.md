---
title: Table 4
summary: Table 4
author: Guo Huan
date: 2021-04-19
---

**Table 4** List of wait events corresponding to transaction locks<a id="Table4"> </a>

| wait_event       | Description                                        |
| :--------------- | :------------------------------------------------- |
| relation         | Adds a lock to a table.                            |
| extend           | Adds a lock to a table being scaled out.           |
| partition        | Adds a lock to a partitioned table.                |
| partition_seq    | Adds a lock to a partition of a partitioned table. |
| page             | Adds a lock to a table page.                       |
| tuple            | Adds a lock to a tuple on a page.                  |
| transactionid    | Adds a lock to a transaction ID.                   |
| virtualxid       | Adds a lock to a virtual transaction ID.           |
| object           | Adds a lock to an object.                          |
| cstore_freespace | Adds a lock to idle column-store space.            |
| userlock         | Adds a lock to a user.                             |
| advisory         | Adds an advisory lock.                             |
