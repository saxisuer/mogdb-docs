---
title: OS_THREADS
summary: OS_THREADS
author: Guo Huan
date: 2021-04-19
---

# OS_THREADS

**OS_THREADS** provides status information about all threads on the current node.

**Table 1** OS_THREADS columns

| **Name**      | **Type**                 | **Description**                                             |
| :------------ | :----------------------- | :---------------------------------------------------------- |
| node_name     | text                     | Database process name                                       |
| pid           | bigint                   | ID of the thread running under the current database process |
| lwpid         | integer                  | Lightweight thread ID corresponding to **pid**              |
| thread_name   | text                     | Name of the thread corresponding to **pid**                 |
| creation_time | timestamp with time zone | Creation time of the thread corresponding to **pid**        |
