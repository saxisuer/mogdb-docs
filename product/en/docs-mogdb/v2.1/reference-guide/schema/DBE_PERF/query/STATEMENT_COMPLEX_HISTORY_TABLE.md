---
title: STATEMENT_COMPLEX_HISTORY_TABLE
summary: STATEMENT_COMPLEX_HISTORY_TABLE
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COMPLEX_HISTORY_TABLE

**STATEMENT_COMPLEX_HISTORY_TABLE** displays load management information about completed jobs executed on the current primary database node. Data is dumped from the kernel to this system catalog. Columns in this view are the same as those in **Appendices -> Table 5**.
