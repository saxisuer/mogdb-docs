---
title: GLOBAL_STATEMENT_COMPLEX_HISTORY_TABLE
summary: GLOBAL_STATEMENT_COMPLEX_HISTORY_TABLE
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATEMENT_COMPLEX_HISTORY_TABLE

**GLOBAL_STATEMENT_COMPLEX_HISTORY_TABLE** displays load management information about completed jobs executed on each node. Data is dumped from the kernel to this system catalog. For details about the columns, see GLOBAL_STATEMENT_COMPLEX_HISTORY.
