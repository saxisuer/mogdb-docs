---
title: GLOBAL_STATEMENT_COUNT
summary: GLOBAL_STATEMENT_COUNT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATEMENT_COUNT

**GLOBAL_STATEMENT_COUNT** displays statistics about five types of running statements (**SELECT**, **INSERT**, **UPDATE**, **DELETE**, and **MERGE INTO**) as well as DDL, DML, and DCL statements on each node of the database.

**Table 1** GLOBAL_STATEMENT_COUNT columns

| **Name**            | **Type** | **Description**                                           |
| :------------------ | :------- | :-------------------------------------------------------- |
| node_name           | text     | Node name                                                 |
| user_name           | text     | Username                                                  |
| select_count        | bigint   | Statistical result of the **SELECT** statement            |
| update_count        | bigint   | Statistical result of the **UPDATE** statement            |
| insert_count        | bigint   | Statistical result of the **INSERT** statement            |
| delete_count        | bigint   | Statistical result of the **DELETE** statement            |
| mergeinto_count     | bigint   | Statistical result of the **MERGE INTO** statement        |
| ddl_count           | bigint   | Number of DDL statements                                  |
| dml_count           | bigint   | Number of DML statements                                  |
| dcl_count           | bigint   | Number of DCL statements                                  |
| total_select_elapse | bigint   | Total response time of **SELECT** statements (unit: μs)   |
| avg_select_elapse   | bigint   | Average response time of **SELECT** statements (unit: μs) |
| max_select_elapse   | bigint   | Maximum response time of **SELECT** statements (unit: μs) |
| min_select_elapse   | bigint   | Minimum response time of **SELECT** statements (unit: μs) |
| total_update_elapse | bigint   | Total response time of **UPDATE** statements (unit: μs)   |
| avg_update_elapse   | bigint   | Average response time of **UPDATE** statements (unit: μs) |
| max_update_elapse   | bigint   | Maximum response time of **UPDATE** statements (unit: μs) |
| min_update_elapse   | bigint   | Minimum response time of **UPDATE** statements (unit: μs) |
| total_insert_elapse | bigint   | Total response time of **INSERT** statements (unit: μs)   |
| avg_insert_elapse   | bigint   | Average response time of **INSERT** statements (unit: μs) |
| max_insert_elapse   | bigint   | Maximum response time of **INSERT** statements (unit: μs) |
| min_insert_elapse   | bigint   | Minimum response time of **INSERT** statements (unit: μs) |
| total_delete_elapse | bigint   | Total response time of **DELETE** statements (unit: μs)   |
| avg_delete_elapse   | bigint   | Average response time of **DELETE** statements (unit: μs) |
| max_delete_elapse   | bigint   | Maximum response time of **DELETE** statements (unit: μs) |
| min_delete_elapse   | bigint   | Minimum response time of **DELETE** statements (unit: μs) |
