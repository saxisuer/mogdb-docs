---
title: STATEMENT_WLMSTAT_COMPLEX_RUNTIME
summary: STATEMENT_WLMSTAT_COMPLEX_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_WLMSTAT_COMPLEX_RUNTIME

**STATEMENT_WLMSTAT_COMPLEX_RUNTIME** displays load management information about ongoing jobs executed by the current user.

**Table 1** STATEMENT_WLMSTAT_COMPLEX_RUNTIME columns

| Name             | Type    | Description                                                  |
| :--------------- | :------ | :----------------------------------------------------------- |
| datid            | oid     | OID of the database that the backend is connected to         |
| datname          | name    | Name of the database that the backend is connected to        |
| threadid         | bigint  | Thread ID of the backend                                     |
| processid        | integer | Process ID of the backend                                    |
| usesysid         | oid     | OID of the user logged in to the backend                     |
| appname          | text    | Name of the application connected to the backend             |
| usename          | name    | Name of the user logged in to the backend                    |
| priority         | bigint  | Priority of Cgroup where the statement is located            |
| attribute        | text    | Attributes of the statement:<br/>- **Ordinary**: default attribute of a statement before it is parsed by the database<br/>- **Simple**: simple statement<br/>- **Complicated**: complicated statement<br/>- **Internal**: internal statement of the database |
| block_time       | bigint  | Pending duration of the statement by now (unit: s)           |
| elapsed_time     | bigint  | Actual execution duration of the statement by now (unit: s)  |
| total_cpu_time   | bigint  | Total CPU usage duration of the statement on the database nodes in the last period (unit: s) |
| cpu_skew_percent | integer | CPU usage skew percentage of the statement on the database nodes in the last period |
| statement_mem    | integer | **statement_mem** used for executing the statement (reserved column) |
| active_points    | integer | Number of concurrently active points occupied by the statement in the resource pool |
| dop_value        | integer | DOP value obtained by the statement from the resource pool   |
| control_group    | text    | Unsupported currently                                        |
| status           | text    | Unsupported currently                                        |
| enqueue          | text    | Queuing status of the statement, including:<br/>- **Global**: queuing in the global queue<br/>- **Respool**: queuing in the resource pool queue<br/>- **CentralQueue**: queuing on the CCN<br/>- **Transaction**: being in a transaction block<br/>- **StoredProc**: being in a stored procedure<br/>- **None**: not in the queue<br/>- **Forced None**: being forcibly executed (transaction block statement or stored procedure statement are) because the statement waiting time exceeds the specified value |
| resource_pool    | name    | Current resource pool where the statements are located       |
| query            | text    | Latest query at the backend. If **state** is **active**, this column shows the ongoing query. In all other states, it shows the last query that was executed |
| is_plana         | boolean | Whether a statement occupies the resources of other logical MogDB in logical MogDB mode. The default value is **f** (does not occupy resources) |
| node_group       | text    | Logical MogDB of the user running the statement              |
