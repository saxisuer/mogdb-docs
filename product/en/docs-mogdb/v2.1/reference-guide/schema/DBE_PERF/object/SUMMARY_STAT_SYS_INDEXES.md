---
title: SUMMARY_STAT_SYS_INDEXES
summary: SUMMARY_STAT_SYS_INDEXES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_SYS_INDEXES

**SUMMARY_STAT_SYS_INDEXES** displays index status information about all the system catalogs in the **pg_catalog**, **information_schema**, and **pg_toast** schemas in MogDB.

**Table 1** SUMMARY_STAT_SYS_INDEXES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| schemaname    | name     | Name of the schema that the index is in                      |
| relname       | name     | Name of the table for the index                              |
| indexrelname  | name     | Index name                                                   |
| idx_scan      | numeric  | Number of index scans initiated on the index                 |
| idx_tup_read  | numeric  | Number of index entries returned by scans on the index       |
| idx_tup_fetch | numeric  | Number of live table rows fetched by simple index scans using the index |
