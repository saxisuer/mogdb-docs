---
title: GLOBAL_STAT_USER_INDEXES
summary: GLOBAL_STAT_USER_INDEXES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_USER_INDEXES

**GLOBAL_STAT_USER_INDEXES** displays the status information about the index of user-defined ordinary tables in all databases of each node.

**Table 1** GLOBAL_STAT_USER_INDEXES columns

| **Name**      | **Type** | **Description**                                              |
| :------------ | :------- | :----------------------------------------------------------- |
| node_name     | name     | Node name                                                    |
| relid         | oid      | OID of the table for this index                              |
| indexrelid    | oid      | OID of the index                                             |
| schemaname    | name     | Name of the schema that the index is in                      |
| relname       | name     | Name of the table for the index                              |
| indexrelname  | name     | Index name                                                   |
| idx_scan      | bigint   | Number of index scans initiated on the index                 |
| idx_tup_read  | bigint   | Number of index entries returned by scans on the index       |
| idx_tup_fetch | bigint   | Number of live table rows fetched by simple index scans using the index |
