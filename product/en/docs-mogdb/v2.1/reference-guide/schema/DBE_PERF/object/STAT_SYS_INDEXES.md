---
title: STAT_SYS_INDEXES
summary: STAT_SYS_INDEXES
author: Guo Huan
date: 2021-04-19
---

# STAT_SYS_INDEXES

**STAT_SYS_INDEXES** displays index status information about all the system catalogs in the **pg_catalog**, **information_schema**, and **pg_toast** schemas.

**Table 1** STAT_SYS_INDEXES columns

| Name          | Type   | Description                                                  |
| :------------ | :----- | :----------------------------------------------------------- |
| relid         | oid    | OID of the table for this index                              |
| indexrelid    | oid    | OID of the index                                             |
| schemaname    | name   | Name of the schema that the index is in                      |
| relname       | name   | Name of the table that the index is created for              |
| indexrelname  | name   | Index name                                                   |
| idx_scan      | bigint | Number of index scans initiated on the index                 |
| idx_tup_read  | bigint | Number of index entries returned by scans on the index       |
| idx_tup_fetch | bigint | Number of live table rows fetched by simple index scans using the index |
