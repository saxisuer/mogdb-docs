---
title: GLOBAL_STAT_USER_FUNCTIONS
summary: GLOBAL_STAT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_USER_FUNCTIONS

**GLOBAL_STAT_USER_FUNCTIONS** displays statistics about the status of functions created by users on each node in MogDB.

**Table 1** GLOBAL_STAT_USER_FUNCTIONS columns

| **Name**   | **Type**         | **Description**                                              |
| :--------- | :--------------- | :----------------------------------------------------------- |
| node_name  | name             | Node name                                                    |
| funcid     | oid              | ID of the function                                           |
| schemaname | name             | Schema name                                                  |
| funcname   | name             | Function name                                                |
| calls      | bigint           | Number of times the function has been called                 |
| total_time | double precision | Total time spent in this function and all other functions called by it (unit: ms) |
| self_time  | double precision | Total time spent in this function, excluding other functions called by it (unit: ms) |
