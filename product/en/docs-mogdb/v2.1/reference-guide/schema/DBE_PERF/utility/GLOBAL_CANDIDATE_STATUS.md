---
title: GLOBAL_CANDIDATE_STATUS
summary: GLOBAL_CANDIDATE_STATUS
author: Guo Huan
date: 2021-11-15
---

# GLOBAL_CANDIDATE_STATUS

**GLOBAL_CANDIDATE_STATUS** displays the number of candidate buffers and buffer eviction information of all instances in the database.

**Table 1** GLOBAL_GET_BGWRITER_STATUS columns

| Name                    | Type    | Description                                                  |
| :---------------------- | :------ | :----------------------------------------------------------- |
| node_name               | text    | Node name                                                    |
| candidate_slots         | integer | Number of pages in the candidate buffer chain of the current normal buffer pool |
| get_buf_from_list       | bigint  | Number of times that pages are obtained from the candidate buffer chain during buffer eviction in the current normal buffer pool |
| get_buf_clock_sweep     | bigint  | Number of times that pages are obtained from the original eviction solution during buffer eviction in the current normal buffer pool |
| seg_candidate_slots     | integer | Number of pages in the candidate buffer chain of the current segment buffer pool |
| seg_get_buf_from_list   | bigint  | Number of times that pages are obtained from the candidate buffer chain during buffer eviction in the current segment buffer pool |
| seg_get_buf_clock_sweep | bigint  | Number of times that pages are obtained from the original eviction solution during buffer eviction in the current segment buffer pool |
