---
title: TRANSACTIONS_RUNNING_XACTS
summary: TRANSACTIONS_RUNNING_XACTS
author: Guo Huan
date: 2021-11-15
---

# TRANSACTIONS_RUNNING_XACTS

**TRANSACTIONS_RUNNING_XACTS** displays information about running transactions on the current node.

**Table 1** TRANSACTIONS_RUNNING_XACTS columns

| **Name**    | **Type** | **Description**                                              |
| :---------- | :------- | :----------------------------------------------------------- |
| handle      | integer  | Handle corresponding to the transaction in GTM               |
| gxid        | xid      | Transaction ID                                               |
| state       | tinyint  | Transaction status (**3**: prepared or **0**: starting)      |
| node        | text     | Node name                                                    |
| xmin        | xid      | Minimum transaction ID on the node                           |
| vacuum      | boolean  | Whether the current transaction is lazy vacuum               |
| timeline    | bigint   | Number of database restarts                                  |
| prepare_xid | xid      | Transaction ID in the **prepared** state. If the status is not **prepared**, the value is **0**. |
| pid         | bigint   | Thread ID corresponding to the transaction                   |
| next_xid    | xid      | Transaction ID sent from a CN to a DN                        |
