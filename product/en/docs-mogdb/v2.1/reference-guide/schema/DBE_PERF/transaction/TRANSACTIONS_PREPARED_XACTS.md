---
title: TRANSACTIONS_PREPARED_XACTS
summary: TRANSACTIONS_PREPARED_XACTS
author: Guo Huan
date: 2021-04-19
---

# TRANSACTIONS_PREPARED_XACTS

**TRANSACTIONS_PREPARED_XACTS** displays information about transactions that are currently prepared for two-phase commit.

**Table 1** TRANSACTIONS_PREPARED_XACTS columns

| Name        | Type                     | Description                                                  |
| :---------- | :----------------------- | :----------------------------------------------------------- |
| transaction | xid                      | Numeric transaction identifier of the prepared transaction   |
| gid         | text                     | Global transaction identifier that was assigned to the transaction |
| prepared    | timestamp with time zone | Time at which the transaction is prepared for commit         |
| owner       | name                     | Name of the user that executes the transaction               |
| database    | name                     | Name of the database in which the transaction is executed    |
