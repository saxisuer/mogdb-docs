---
title: GLOBAL_STATIO_SYS_INDEXES
summary: GLOBAL_STATIO_SYS_INDEXES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_SYS_INDEXES

**GLOBAL_STATIO_SYS_INDEXES** displays I/O status information about all system catalog indexes in namespaces on each node.

**Table 1** GLOBAL_STATIO_SYS_INDEXES columns

| **Name**      | **Type** | **Description**                                 |
| :------------ | :------- | :---------------------------------------------- |
| node_name     | name     | Node name                                       |
| relid         | oid      | OID of the table that the index is created for  |
| indexrelid    | oid      | OID of the index                                |
| schemaname    | name     | Name of the schema that the index is in         |
| relname       | name     | Name of the table that the index is created for |
| indexrelname  | name     | Index name                                      |
| idx_blks_read | numeric  | Number of disk blocks read from the index       |
| idx_blks_hit  | numeric  | Number of cache hits in the index               |
