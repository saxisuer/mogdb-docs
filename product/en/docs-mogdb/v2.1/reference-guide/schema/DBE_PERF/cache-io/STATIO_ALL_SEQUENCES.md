---
title: STATIO_ALL_SEQUENCES
summary: STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_ALL_SEQUENCES

**STATIO_ALL_SEQUENCES** contains one row for each sequence in the current database, showing I/O statistics about specific sequences.

**Table 1** STATIO_ALL_SEQUENCES columns

| Name       | Type   | Description                                  |
| :--------- | :----- | :------------------------------------------- |
| relid      | oid    | OID of this sequence                         |
| schemaname | name   | Name of the schema where the sequence is in  |
| relname    | name   | Sequence name                                |
| blks_read  | bigint | Number of disk blocks read from the sequence |
| blks_hit   | bigint | Cache hits in the sequence                   |
