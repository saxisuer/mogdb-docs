---
title: STATIO_USER_SEQUENCES
summary: STATIO_USER_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_USER_SEQUENCES

**STATIO_USER_SEQUENCE** displays I/O status information about all user relationship table sequences in namespaces on the current node.

**Table 1** STATIO_USER_SEQUENCE columns

| Name       | Type   | Description                                  |
| :--------- | :----- | :------------------------------------------- |
| relid      | oid    | OID of the sequence                          |
| schemaname | name   | Name of the schema that the sequence is in   |
| relname    | name   | Sequence name                                |
| blks_read  | bigint | Number of disk blocks read from the sequence |
| blks_hit   | bigint | Number of cache hits in the sequence         |
