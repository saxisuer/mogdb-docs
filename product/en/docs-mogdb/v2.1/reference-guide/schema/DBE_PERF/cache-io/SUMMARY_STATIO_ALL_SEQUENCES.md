---
title: SUMMARY_STATIO_ALL_SEQUENCES
summary: SUMMARY_STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_SEQUENCES

**SUMMARY_STATIO_ALL_SEQUENCES** contains one row for each sequence in databases in MogDB, showing I/O statistics about specific sequences.

**Table 1** SUMMARY_STATIO_ALL_SEQUENCES columns

| **Name**   | **Type** | **Description**                              |
| :--------- | :------- | :------------------------------------------- |
| schemaname | name     | Name of the schema that the sequence is in   |
| relname    | name     | Sequence name                                |
| blks_read  | numeric  | Number of disk blocks read from the sequence |
| blks_hit   | numeric  | Number of cache hits in the sequence         |
