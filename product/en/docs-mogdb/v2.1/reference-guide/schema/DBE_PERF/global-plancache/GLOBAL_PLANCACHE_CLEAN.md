---
title: GLOBAL_PLANCACHE_CLEAN
summary: GLOBAL_PLANCACHE_CLEAN
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_PLANCACHE_CLEAN

**GLOBAL_PLANCACHE_CLEAN** clears the global plan cache that is not used on all nodes. The return value is of the Boolean type.
