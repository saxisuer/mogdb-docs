---
title: GLOBAL_SHARED_MEMORY_DETAIL
summary: GLOBAL_SHARED_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_SHARED_MEMORY_DETAIL

**GLOBAL_SHARED_MEMORY_DETAIL** is used to query the usage of shared memory contexts on all normal nodes in MogDB.

**Table 1** GLOBAL_SHARED_MEMORY_DETAIL columns

| **Name**    | **Type** | **Description**                                  |
| :---------- | :------- | :----------------------------------------------- |
| node_name   | name     | Node name                                        |
| contextname | text     | Name of the memory context                       |
| level       | smallint | Level of the memory context                      |
| parent      | text     | Name of the parent memory context                |
| totalsize   | bigint   | Total size of the shared memory (unit: byte)     |
| freesize    | bigint   | Remaining size of the shared memory (unit: byte) |
| usedsize    | bigint   | Used size of the shared memory (unit: byte)      |
