---
title: WLM_USER_RESOURCE_CONFIG
summary: WLM_USER_RESOURCE_CONFIG
author: Guo Huan
date: 2021-04-19
---

# WLM_USER_RESOURCE_CONFIG

**WLM_USER_RESOURCE_CONFIG** displays the resource configuration information of a user.

**Table 1** WLM_USER_RESOURCE_CONFIG columns

| Name       | Type    | Description                                      |
| :--------- | :------ | :----------------------------------------------- |
| userid     | oid     | OID of the user                                  |
| username   | name    | Username                                         |
| sysadmin   | boolean | Whether the user has the **sysadmin** permission |
| rpoid      | oid     | OID of the resource pool                         |
| respool    | name    | Name of a resource pool                          |
| parentid   | oid     | OID of the parent user                           |
| totalspace | bigint  | Size of the occupied space                       |
| spacelimit | bigint  | Upper limit of the space size                    |
| childcount | integer | Number of child users                            |
| childlist  | text    | Child user list                                  |
