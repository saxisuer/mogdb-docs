---
title: GLOBAL_SESSION_MEMORY
summary: GLOBAL_SESSION_MEMORY
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_SESSION_MEMORY

**GLOBAL_SESSION_MEMORY** collects statistics about memory usage at the session level on each node in the unit of MB, including all the memory allocated to MogDB and stream threads on DNs for jobs currently executed by users.

**Table 1** GLOBAL_SESSION_MEMORY columns

| **Name**  | **Type** | **Description**                                              |
| :-------- | :------- | :----------------------------------------------------------- |
| node_name | name     | Node name                                                    |
| sessid    | text     | Thread start time and ID                                     |
| init_mem  | integer  | Memory allocated to the currently executed job before the job enters the executor |
| used_mem  | integer  | Memory allocated to the currently executed job               |
| peak_mem  | integer  | Peak memory allocated to the currently executed job          |
