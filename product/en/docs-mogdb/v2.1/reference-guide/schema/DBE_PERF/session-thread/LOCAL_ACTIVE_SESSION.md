---
title: LOCAL_ACTIVE_SESSION
summary: LOCAL_ACTIVE_SESSION
author: Guo Huan
date: 2021-11-15
---

# LOCAL_ACTIVE_SESSION

The **LOCAL_ACTIVE_SESSION** view displays the samples in the **ACTIVE SESSION PROFILE** memory on this node.

**表 1** LOCAL_ACTIVE_SESSION field

| Name                  | Type                     | Description                                                  |
| :-------------------- | :----------------------- | :----------------------------------------------------------- |
| sampleid              | bigint                   | Sample ID.                                                   |
| sample_time           | timestamp with time zone | The time of sampling.                                        |
| need_flush_sample     | boolean                  | Whether the sample needs to be refreshed on disk.            |
| databaseid            | oid                      | Database ID.                                                 |
| thread_id             | bigint                   | Thread ID.                                                   |
| sessionid             | bigint                   | Session ID.                                                  |
| start_time            | timestamp with time zone | The start time of the session.                               |
| event                 | text                     | The specific event name.                                     |
| lwtid                 | integer                  | The lightweight thread ID of the current thread.             |
| psessionid            | bigint                   | The parent thread of the streaming thread.                   |
| tlevel                | integer                  | The level of streaming threads. Correspond to the level (ID) of the execution plan. |
| smpid                 | integer                  | The parallel ID of the parallel thread in the smp execution mode. |
| userid                | oid                      | Session user ID。                                            |
| application_name      | text                     | The name of the application.                                 |
| client_addr           | inet                     | The address of the client.                                   |
| client_hostname       | text                     | The name of the client.                                      |
| client_port           | integer                  | The TCP port number used by the client to communicate with the backend. |
| query_id              | bigint                   | debug query id。                                             |
| unique_query_id       | bigint                   | unique query id。                                            |
| user_id               | oid                      | The user_id in the key of the unique query.                  |
| cn_id                 | integer                  | cn id, on the DN, it means the ID of the node that issued the unique sql, and the cn_id in the key of the unique query. |
| unique_query          | text                     | UniqueSQL text string after normalization.                   |
| locktag               | text                     | The session waiting lock information can be parsed by locktag_decode. |
| lockmode              | text                     | The session waits for the lock mode.                         |
| block_sessionid       | bigint                   | If the session is waiting for a lock, block the session ID from acquiring the lock. |
| final_block_sessionid | bigint                   | Indicates the source blocking session ID.                    |
| wait_status           | text                     | Describe more detailed information about the event column.   |
| global_sessionid      | text                     | Global session ID.                                           |
