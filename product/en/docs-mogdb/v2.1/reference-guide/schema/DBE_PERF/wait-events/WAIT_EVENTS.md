---
title: WAIT_EVENTS
summary: WAIT_EVENTS
author: Guo Huan
date: 2021-04-19
---

# WAIT_EVENTS

**WAIT_EVENTS** displays statistics about wait events on the current node. For details about events, see **[Appendices-Table 1](../../../../reference-guide/schema/DBE_PERF/appendices/table-1.md)**, **[Appendices-Table 2](../../../../reference-guide/schema/DBE_PERF/appendices/table-2.md)**, **[Appendices-Table 3](../../../../reference-guide/schema/DBE_PERF/appendices/table-3.md)**, and **[Appendices-Table 4](../../../../reference-guide/schema/DBE_PERF/appendices/table-4.md)**. For details about the extent to which each transaction lock affects the services, see [LOCK](../../../../reference-guide/sql-syntax/LOCK.md).

**Table 1** WAIT_EVENTS columns

| **Name**        | **Type**                 | **Description**                      |
| :-------------- | :----------------------- | :----------------------------------- |
| nodename        | text                     | Database process name                |
| type            | text                     | Event type                           |
| event           | text                     | Event name                           |
| wait            | bigint                   | Number of waiting times              |
| failed_wait     | bigint                   | Number of waiting failures           |
| total_wait_time | bigint                   | Total waiting time (unit: μs)        |
| avg_wait_time   | bigint                   | Average waiting time (unit: μs)      |
| max_wait_time   | bigint                   | Maximum waiting time (unit: μs)      |
| min_wait_time   | bigint                   | Minimum waiting time (unit: μs)      |
| last_updated    | timestamp with time zone | Last time when the event was updated |
