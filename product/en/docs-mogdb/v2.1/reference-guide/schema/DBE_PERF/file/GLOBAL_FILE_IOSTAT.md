---
title: GLOBAL_FILE_IOSTAT
summary: GLOBAL_FILE_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_FILE_IOSTAT

**GLOBAL_FILE_IOSTAT** records statistics about data file I/Os on all nodes.

**Table 1** GLOBAL_FILE_IOSTAT columns

| **Name**  | **Type** | **Description**                                      |
| :-------- | :------- | :--------------------------------------------------- |
| node_name | name     | Node name                                            |
| filenum   | oid      | File ID                                              |
| dbid      | oid      | Database ID                                          |
| spcid     | oid      | Tablespace ID                                        |
| phyrds    | bigint   | Number of times of reading physical files            |
| phywrts   | bigint   | Number of times of writing into physical files       |
| phyblkrd  | bigint   | Number of times of reading physical file blocks      |
| phyblkwrt | bigint   | Number of times of writing into physical file blocks |
| readtim   | bigint   | Total duration of reading (unit: μs)                 |
| writetim  | bigint   | Total duration of writing (unit: μs)                 |
| avgiotim  | bigint   | Average duration of reading and writing (unit: μs)   |
| lstiotim  | bigint   | Duration of the last file reading (unit: μs)         |
| miniotim  | bigint   | Minimum duration of reading and writing (unit: μs)   |
| maxiowtm  | bigint   | Maximum duration of reading and writing (unit: μs)   |
