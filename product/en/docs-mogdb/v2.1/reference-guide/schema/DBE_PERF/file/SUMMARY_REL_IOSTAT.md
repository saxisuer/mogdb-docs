---
title: SUMMARY_REL_IOSTAT
summary: SUMMARY_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_REL_IOSTAT

**SUMMARY_REL_IOSTAT** obtains statistics about data file I/Os on all nodes.

**Table 1** SUMMARY_REL_IOSTAT columns

| **Name**  | **Type** | **Description**                                      |
| :-------- | :------- | :--------------------------------------------------- |
| phyrds    | numeric  | Number of times of reading physical files            |
| phywrts   | numeric  | Number of times of writing into physical files       |
| phyblkrd  | numeric  | Number of times of reading physical file blocks      |
| phyblkwrt | numeric  | Number of times of writing into physical file blocks |
