---
title: FILE_IOSTAT
summary: FILE_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# FILE_IOSTAT

**FILE_IOSTAT** records statistics about data file I/Os to indicate I/O performance and detect performance problems such as abnormal I/O operations.

**Table 1** FILE_IOSTAT columns

| Name      | Type   | Description                                          |
| :-------- | :----- | :--------------------------------------------------- |
| filenum   | oid    | File identifier                                      |
| dbid      | oid    | Database ID                                          |
| spcid     | oid    | Tablespace ID                                        |
| phyrds    | bigint | Number of times of reading physical files            |
| phywrts   | bigint | Number of times of writing into physical files       |
| phyblkrd  | bigint | Number of times of reading physical file blocks      |
| phyblkwrt | bigint | Number of times of writing into physical file blocks |
| readtim   | bigint | Total duration of reading (unit: μs)                 |
| writetim  | bigint | Total duration of writing (unit: μs)                 |
| avgiotim  | bigint | Average duration of reading and writing (unit: μs)   |
| lstiotim  | bigint | Duration of the last file reading (unit: μs)         |
| miniotim  | bigint | Minimum duration of reading and writing (unit: μs)   |
| maxiowtm  | bigint | Maximum duration of reading and writing (unit: μs)   |
