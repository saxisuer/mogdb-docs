---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-04-19
---

# Overview

In the **DBE_PERF** schema, views are used to diagnose performance issues and are also the data source of WDR snapshots. After the database is installed, only the initial user has the permission for the **DBE_PERF** schema by default. If the database is upgraded from an earlier version, the permission for the **DBE_PERF** schema needs to be the same as that of the earlier version to ensure forward compatibility. Organization views are divided based on multiple dimensions, such as OS, instance, and memory. These views need to comply with the following naming rules:

- Views starting with **GLOBAL_**: Request data from database nodes and return the data without processing them.
- Views starting with **SUMMARY_**: Summarize data in MogDB. In most cases, data from database nodes (sometimes only the primary database node) is processed, collected, and returned.
- Views that do not start with **GLOBAL_** or **SUMMARY_**: Local views that do not request data from other database nodes.

<br/>

## Grant DBE_PERF Access Permission to Other Users

If the following error occurs during the query of the views in the **DBE_PERF** schema, it means that the permission is not granted:

```sql
mogdb=> select * from dbe_perf.statement;
permission denied for schema dbe_perf
LINE 1: select * from dbe_perf.statement
                      ^
mogdb=> select * from dbe_perf.statement;
ERROR:  only system/monitor admin can query unique sql view
```

The solution is as follows:

Use the omm user to change the user who needs to access the **DBE_PERF** schema to the monitoring administrator. For example:

```sql
mogdb=# ALTER USER mogdb_usr MONADMIN;
ALTER ROLE
```

A monitoring administrator is an account with the **MONADMIN** attribute that has the permission to view views and functions in the **DBE_PERF** schema and to grant or revoke the permission to or from objects in **DBE_PERF** schema. For more information about users and permissions, please refer to [Managing Users and Their Permissions](../../../security-guide/security/2-managing-users-and-their-permissions.md).
