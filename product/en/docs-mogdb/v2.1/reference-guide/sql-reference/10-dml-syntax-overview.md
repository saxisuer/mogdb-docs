---
title: DML Syntax Overview
summary: DML Syntax Overview
author: Zhang Cuiping
date: 2021-05-17
---

# DML Syntax Overview

Data manipulation language (DML) is used to perform operations on data in database tables, such as inserting, updating, querying, or deleting data.

## Inserting Data

Inserting data refers to adding one or multiple records to a database table. For details, see [INSERT](../../reference-guide/sql-syntax/INSERT.md).

## Updating Data

Updating data refers to modifying one or multiple records in a database table. For details, see [UPDATE](../../reference-guide/sql-syntax/UPDATE.md).

## Querying Data

The database query statement **SELECT** is used to search required information in a database. For details, see [SELECT](../../reference-guide/sql-syntax/SELECT.md).

## Deleting Data

MogDB provides two statements for deleting data from database tables. To delete data meeting specified conditions from a database table, see [DELETE](../../reference-guide/sql-syntax/DELETE.md). To delete all data from a database table, see [TRUNCATE](../../reference-guide/sql-syntax/TRUNCATE.md).

**TRUNCATE** can quickly delete all data from a database table, which achieves the effect same as that running **DELETE** to delete data without specifying conditions from each table. Deletion efficiency using **TRUNCATE** is faster because **TRUNCATE** does not scan tables. Therefore, **TRUNCATE** is useful in large tables.

## Copying Data

MogDB provides a statement for copying data between tables and files. For details, see [COPY](../../reference-guide/sql-syntax/COPY.md).

## Locking a Table

MogDB provides multiple lock modes to control concurrent accesses to table data. For details, see [LOCK](../../reference-guide/sql-syntax/LOCK.md).

## Calling a Function

MogDB provides three statements for calling functions. These statements are the same in the syntax structure. For details, see [CALL](../../reference-guide/sql-syntax/CALL.md).

## Session Management

A session is a connection established between the user and the database. [Table 1](#sqlstatement) lists the related SQL statements.

**Table 1** SQL statements related to sessions <a id="sqlstatement"></a>

| Function           | SQL Statement                                             |
| :----------------- | :-------------------------------------------------------- |
| Altering a session | [ALTER SESSION](../../reference-guide/sql-syntax/ALTER-SESSION.md)                         |
| Killing a session  | [ALTER SYSTEM KILL SESSION](../../reference-guide/sql-syntax/ALTER-SYSTEM-KILL-SESSION.md) |
