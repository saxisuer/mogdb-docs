---
title: Extended Syntax
summary: Extended Syntax
author: Zhang Cuiping
date: 2021-05-18
---

# Extended Syntax

MogDB provides extended syntax .

**Table 1** Extended SQL syntax

<table>
    <tr>
        <th>Category</th>
        <th>Keywords</th>
        <th>Description</th>
    </tr>
    <tr>
        <td rowspan=2>Creating a table</td>
        <td>INHERITS ( parent_table [, … ] )</td>
        <td>Specifies whether an inherited table is supported.</td>
    </tr>
    <tr>
        <td>column_constraint：<br/>REFERENCES reftable<br/>[ ( refcolumn ) ] [ MATCH<br/>
FULL | MATCH PARTIAL |<br/>MATCH SIMPLE ][ ON<br/>DELETE action ] [ ON<br/>UPDATE action ]</td>
        <td>You can run REFERENCES reftable[(refcolumn)] [MATCH FULL |MATCH PARTIAL | MATCH SIMPLE] [ON DELETE action] [ON UPDATE action] to create foreign key constraints for tables.</td>
    </tr>
    <tr>
        <td rowspan=2>Loading a module</td>
        <td>CREATE EXTENSION</td>
        <td>Loads a new module (such as DBLINK) to the current database.</td>
    </tr>
    <tr>
        <td>DROP EXTENSION</td>
        <td>Deletes the loaded module.</td>
    </tr>
    <tr>
        <td rowspan=3>Aggregate functions</td>
        <td>CREATE AGGREGATE</td>
        <td>Defines a new aggregation function.</td>
    </tr>
    <tr>
        <td>ALTER AGGREGATE</td>
        <td>Modifies the definition of an aggregate function.</td>
    </tr>
    <tr>
        <td>DROP AGGREGATE</td>
        <td>Drops an existing function.</td>
    </tr>
</table>
