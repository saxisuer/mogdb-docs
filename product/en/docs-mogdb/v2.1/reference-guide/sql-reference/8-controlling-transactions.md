---
title: Controlling Transactions
summary: Controlling Transactions
author: Zhang Cuiping
date: 2021-05-17
---

# Controlling Transactions

A transaction is a user-defined sequence of database operations, which form an integral unit of work.

## Starting a Transaction

MogDB starts a transaction using START TRANSACTION and **BEGIN**. For details, see [START TRANSACTION](../../reference-guide/sql-syntax/START-TRANSACTION.md) and [BEGIN](../../reference-guide/sql-syntax/BEGIN.md).

## Setting a Transaction

MogDB sets a transaction using **SET TRANSACTION** or **SET LOCAL TRANSACTION**. For details, see [SET TRANSACTION](../../reference-guide/sql-syntax/SET-TRANSACTION.md).

## Committing a Transaction

MogDB commits all operations of a transaction using **COMMIT** or **END**. For details, see [COMMIT | END](../../reference-guide/sql-syntax/COMMIT-END.md).

## Rolling Back a Transaction

If a fault occurs during a transaction and the transaction cannot proceed, the system performs rollback to cancel all the completed database operations related to the transaction. See [ROLLBACK](../../reference-guide/sql-syntax/ROLLBACK.md).

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
> If an execution request (not in a transaction block) received in the database contains multiple statements, the request is packed into a transaction. If one of the statements fails, the entire request will be rolled back.
