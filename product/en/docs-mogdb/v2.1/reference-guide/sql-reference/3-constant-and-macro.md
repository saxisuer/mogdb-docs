---
title: Constant and Macro
summary: Constant and Macro
author: Zhang Cuiping
date: 2021-05-17
---

# Constant and Macro

[Table 1](#constantandmacro) lists the constants and macros that can be used in openGauss.

**Table 1** Constant and macro <a id="constantandmacro"> </a>

| Parameter       | Description                                               | Example                                                      |
| :------------- | :----------- | :--------------------------------|
| CURRENT_CATALOG | Specifies the current database.                           | `postgres=# SELECT CURRENT_CATALOG; current_database ------------------- postgres (1 row)` |
| CURRENT_ROLE    | Specifies the current user.                               | `postgres=# SELECT CURRENT_ROLE; current_user -------------------- omm (1 row)` |
| CURRENT_SCHEMA  | Specifies the current database mode.                      | `postgres=# SELECT CURRENT_SCHEMA; current_schema ------------------- public (1 row)` |
| CURRENT_USER    | Specifies the current user.                               | `postgres=# SELECT CURRENT_USER; current_user -------------------- omm (1 row)` |
| LOCALTIMESTAMP  | Specifies the current session time (without time zone).   | `postgres=# SELECT LOCALTIMESTAMP;         timestamp ------------------- 2015-10-10 15:37:30.968538 (1 row)` |
| NULL            | This parameter is left blank.                             | N/A                                                          |
| SESSION_USER    | Specifies the current system user.                        | `postgres=# SELECT SESSION_USER; session_user -------------------- omm (1 row)` |
| SYSDATE         | Specifies the current system date.                        | `postgres=# SELECT SYSDATE; sysdate ------------------- 2015-10-10 15:48:53 (1 row)` |
| USER            | Specifies the current user, also called **CURRENT_USER**. | `postgres=# SELECT USER; current_user -------------------- omm (1 row)`    |
