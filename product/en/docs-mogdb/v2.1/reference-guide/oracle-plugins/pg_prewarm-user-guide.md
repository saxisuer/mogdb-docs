---
title: pg_prewarm User Guide
summary: pg_prewarm User Guide
author: Guo Huan
date: 2021-11-29
---

# pg_prewarm

## pg_prewarm Overview

The pg_prewarm module provides a convenient way to load relational data into the operating system buffer cache or MogDB buffer cache.

<br/>

## Install pg_prewarm

For details, please refer to [gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md) or [gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md).

<br/>

## pg_prewarm loading mode

- mode: The loading mode, the options are 'prefetch', 'read', 'buffer', the default is 'buffer'

- prefetch: Asynchronously preload data into OS cache

- read: The end result is the same as prefetch, but it is synchronous and supports all platforms

- buffer: Preload data into database cache

## Create and Use pg_prewarm

1. Create extension pg_prewarm.

    ```sql
   create extension pg_prewarm; 
   ```

2. Create a test table. 

   ```sql
   create table test_pre (id int4,name character varying(64),creat_time timestamp(6) without time zone); 
   ```

3. Insert data into the test table.

   ```sql
   insert into test_pre select generate_series(1,100000),generate_series(1,100000)||  '_pre',clock_timestamp(); 
   ```

4. Check the table size. 

   ```sql
   mogdb=# select pg_size_pretty(pg_relation_size('test_pre'));
   pg_size_pretty
   ----------------
   5136 kB
   (1 row)
   ```

5. Load data to the database cache. The result shows that pg_prewarm divides data into 642 data blocks. 

   ```sql
   mogdb=# select pg_prewarm('test_pre','buffer');
    pg_prewarm
   ------------
        642
   (1 row)
   ```

6. Check the block size. 

   ```sql
   mogdb=# select current_setting('block_size');
    current_setting
   -----------------
   8192
   (1 row)
   ```

   The size of each data block is 8 KB in MogDB by default.  

   ```sql
   mogdb=# select 642*8; 
    ?column?
   ----------
     5136
   (1 row)
   ```