---
title: PG_GTT_ATTACHED_PIDS
summary: PG_GTT_ATTACHED_PIDS
author: Guo Huan
date: 2021-04-19
---

# PG_GTT_ATTACHED_PIDS

**PG_GTT_ATTACHED_PIDS** checks which sessions are using global temporary tables by calling the **pg_get_attached_pid** function.

**Table 1** PG_GTT_ATTACHED_PIDS columns

| Name       | Type     | Description                       |
| :--------- | :------- | :-------------------------------- |
| schemaname | name     | Schema name.                      |
| tablename  | name     | Name of a global temporary table. |
| relid      | oid      | OID of a global temporary table.  |
| pids       | bigint[] | Thread PID list.                  |
