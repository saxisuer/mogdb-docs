---
title: GS_THREAD_MEMORY_CONTEXT
summary: GS_THREAD_MEMORY_CONTEXT
author: Guo Huan
date: 2021-06-15
---

# GS_THREAD_MEMORY_CONTEXT

**GS_THREAD_MEMORY_CONTEXT** displays statistics about memory usage of all threads based on MemoryContext nodes. This view is equivalent to the **GS_SESSION_MEMORY_CONTEXT** view when **enable_thread_pool** is set to **off**. If the GUC parameter **enable_memory_limit** is set to **off**, this view is unavailable.

The memory context **TempSmallContextGroup** collects information about all memory contexts whose value in the **totalsize** column is less than 8192 bytes in the current thread, and the number of the collected memory contexts is recorded in the **usedsize** column. Therefore, the **totalsize** and **freesize** columns for **TempSmallContextGroup** in the view display the corresponding information about all the memory contexts whose value in the **totalsize** column is less than 8192 bytes in the current thread, and the **usedsize** column displays the number of these memory contexts.

**Table 1** GS_THREAD_MEMORY_CONTEXT columns

| Name        | Type     | Description                                                  |
| :---------- | :------- | :----------------------------------------------------------- |
| threadid    | text     | Thread start time + thread ID (string: **timestamp.tsessionid**) |
| tid         | bigint   | Thread ID                                                    |
| thrdtype    | text     | Thread type                                                  |
| contextname | text     | Name of the memory context                                   |
| level       | smallint | Hierarchy of the memory context                              |
| parent      | text     | Name of the parent memory context                            |
| totalsize   | bigint   | Total size of the memory context, in bytes                   |
| freesize    | bigint   | Total size of released memory in the current memory context, in bytes |
| usedsize    | bigint   | Size of used memory in the memory context, in bytes. For **TempSmallContextGroup**, this parameter specifies the number of collected memory contexts. |
