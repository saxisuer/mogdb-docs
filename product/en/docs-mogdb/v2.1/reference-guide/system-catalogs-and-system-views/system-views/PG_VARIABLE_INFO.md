---
title: PG_VARIABLE_INFO
summary: PG_VARIABLE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_VARIABLE_INFO

**PGXC_VARIABLE_INFO** records information about transaction IDs and OIDs of the current node in openGauss.

**Table 1** PG_VARIABLE_INFO columns

| Name                     | Type | Description                                                  |
| :----------------------- | :--- | :----------------------------------------------------------- |
| node_name                | text | Node name                                                    |
| next_oid                 | oid  | OID generated next time for the node                         |
| next_xid                 | xid  | Transaction ID generated next time for the node              |
| oldest_xid               | xid  | Oldest transaction ID on the node                            |
| xid_vac_limit            | xid  | Critical point (transaction ID) that triggers forcible autovacuum |
| oldest_xid_db            | oid  | OID of the database that has the minimum datafrozenxid on the node |
| last_extend_csn_logpage  | xid  | Number of the last extended csnlog page                      |
| start_extend_csn_logpage | xid  | Number of the page from which csnlog extending starts        |
| next_commit_seqno        | xid  | CSN generated next time for the node                         |
| latest_completed_xid     | xid  | Latest transaction ID on the node after the transaction commission or rollback |
| startup_max_xid          | xid  | Last transaction ID before the node is powered off           |
