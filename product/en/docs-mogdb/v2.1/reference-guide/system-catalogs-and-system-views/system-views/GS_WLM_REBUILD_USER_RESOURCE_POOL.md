---
title: GS_WLM_REBUILD_USER_RESOURCE_POOL
summary: GS_WLM_REBUILD_USER_RESOURCE_POOL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_REBUILD_USER_RESOURCE_POOL

**GS_WLM_REBUILD_USER_RESOURCE_POOL** is used to rebuild a user's resource pool information in memory on the current connection node, with no output. This view is only used as a remedy when resource pool information is missing or misplaced. Only the user with sysadmin permission can query this view.

**Table 1** **GS_WLM_REBUILD_USER_RESOURCE_POOL** columns

| Name                              | Type    | Description                                                  |
| :-------------------------------- | :------ | :----------------------------------------------------------- |
| gs_wlm_rebuild_user_resource_pool | Boolean | Rebuilds information about the user resource pool in the memory. **t** indicates success, and **f** indicates failure. |
