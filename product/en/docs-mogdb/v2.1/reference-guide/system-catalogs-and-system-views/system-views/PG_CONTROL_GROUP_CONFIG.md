---
title: PG_CONTROL_GROUP_CONFIG
summary: PG_CONTROL_GROUP_CONFIG
author: Guo Huan
date: 2021-06-15
---

# PG_CONTROL_GROUP_CONFIG

**PG_CONTROL_GROUP_CONFIG** stores Cgroup configuration information in the system. Only the user with sysadmin permission can query this view.

**Table 1** PG_CONTROL_GROUP_CONFIG columns

| Name                    | Type | Description                             |
| :---------------------- | :--- | :-------------------------------------- |
| pg_control_group_config | text | Configuration information of the Cgroup |
