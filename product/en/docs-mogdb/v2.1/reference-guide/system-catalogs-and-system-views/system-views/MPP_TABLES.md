---
title: MPP_TABLES
summary: MPP_TABLES
author: Guo Huan
date: 2021-04-19
---

# MPP_TABLES

The following information is displayed in the **MPP_TABLES** view.

**Table 1** MPP_TABLES columns

| Name       | Type             | Description                              |
| :--------- | :--------------- | :--------------------------------------- |
| schemaname | name             | Name of the schema that contains a table |
| tablename  | name             | Table name                               |
| tableowner | name             | Table owner                              |
| tablespace | name             | Tablespace containing the table          |
| pgroup     | name             | Name of a node cluster                   |
| nodeoids   | oidvector_extend | List of distributed table node OIDs      |
