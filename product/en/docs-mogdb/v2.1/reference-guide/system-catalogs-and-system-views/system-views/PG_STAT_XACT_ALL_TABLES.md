---
title: PG_STAT_XACT_ALL_TABLES
summary: PG_STAT_XACT_ALL_TABLES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_XACT_ALL_TABLES

**PG_STAT_XACT_ALL_TABLES** displays transaction status information about all ordinary tables and TOAST tables in the namespaces.

**Table 1** PG_STAT_XACT_ALL_TABLES columns

| Name          | Type   | Description                                                  |
| :------------ | :----- | :----------------------------------------------------------- |
| relid         | oid    | OID of the table                                             |
| schemaname    | name   | Name of the schema that the table is in                      |
| relname       | name   | Table name                                                   |
| seq_scan      | bigint | Number of sequential scans initiated on the table            |
| seq_tup_read  | bigint | Number of live rows fetched by sequential scans              |
| idx_scan      | bigint | Number of index scans initiated on the table                 |
| idx_tup_fetch | bigint | Number of live rows fetched by index scans                   |
| n_tup_ins     | bigint | Number of rows inserted                                      |
| n_tup_upd     | bigint | Number of rows updated                                       |
| n_tup_del     | bigint | Number of rows deleted                                       |
| n_tup_hot_upd | bigint | Number of rows HOT updated (with no separate index update required) |
