---
title: PG_STAT_REPLICATION
summary: PG_STAT_REPLICATION
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_REPLICATION

**PG_STAT_REPLICATION** displays information about log synchronization status, such as the locations of the sender sending logs and the receiver receiving logs.

**Table 1** PG_STAT_REPLICATION columns

| Name                     | Type                     | Description                                                  |
| :----------------------- | :----------------------- | :----------------------------------------------------------- |
| pid                      | bigint                   | Process ID of the thread                                     |
| usesysid                 | oid                      | User system ID                                               |
| usename                  | name                     | Username                                                     |
| application_name         | text                     | Program name                                                 |
| client_addr              | inet                     | Client address                                               |
| client_hostname          | text                     | Client name                                                  |
| client_port              | integer                  | Port of the client                                           |
| backend_start            | timestamp with time zone | Start time of the program                                    |
| state                    | text                     | Log replication state (catch-up or consistent streaming)     |
| sender_sent_location     | text                     | Location where the sender sends logs                         |
| receiver_write_location  | text                     | Location where the receiver writes logs                      |
| receiver_flush_location  | text                     | Location where the receive end flushes logs                  |
| receiver_replay_location | text                     | Location where the receive end replays logs                  |
| sync_priority            | integer                  | Priority of synchronous duplication (**0** indicates asynchronization.) |
| sync_state               | text                     | Synchronization state (asynchronous duplication, synchronous duplication, or potential synchronization) |
