---
title: GS_AUDITING_PRIVILEGE
summary: GS_AUDITING_PRIVILEGE
author: Guo Huan
date: 2021-06-15
---

# GS_AUDITING_PRIVILEGE

**GS_AUDITING_PRIVILEGE** displays all audit information about database DDL-related operations. Only the users with system administrator or security policy administrator permission can access this view.

| Name        | Type    | Description                                                  |
| :---------- | :------ | :----------------------------------------------------------- |
| polname     | name    | Policy name, which must be unique                            |
| pol_type    | text    | Audit policy type. The value **privilege** indicates that DDL operations are audited. |
| polenabled  | boolean | Specifies whether to enable the policy.                      |
| access_type | name    | DDL database operation type. For example, CREATE, ALTER, and DROP. |
| label_name  | name    | Specifies the resource label name. This parameter corresponds to the **polname** column in the **GS_AUDITING_POLICY** system catalog. |
| priv_object | text    | Full domain name of a database object.                       |
| filter_name | text    | Logical character string of a filter criterion.              |
