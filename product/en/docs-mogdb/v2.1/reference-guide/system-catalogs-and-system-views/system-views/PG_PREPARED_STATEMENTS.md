---
title: PG_PREPARED_STATEMENTS
summary: PG_PREPARED_STATEMENTS
author: Guo Huan
date: 2021-04-19
---

# PG_PREPARED_STATEMENTS

**PG_PREPARED_STATEMENTS** displays all prepared statements that are available in the current session.

**Table 1** PG_PREPARED_STATEMENTS columns

| Name            | Type                     | Description                                                  |
| :-------------- | :----------------------- | :----------------------------------------------------------- |
| name            | text                     | Identifier of the prepared statement                         |
| statement       | text                     | Query string for creating this prepared statement. For prepared statements created through SQL, this is the PREPARE statement submitted by the client. For prepared statements created through the frontend/backend protocol, this is the text of the prepared statement itself. |
| prepare_time    | timestamp with time zone | Timestamp when the prepared statement is created             |
| parameter_types | regtype[]                | Expected parameter types for the prepared statement in the form of an array of **regtype**. The OID corresponding to an element of this array can be obtained by converting the **regtype** value to **oid**. |
| from_sql        | Boolean                  | - **True** if the prepared statement was created through the PREPARE statement<br/>- **False** if the statement was prepared through the frontend/backend protocol |
