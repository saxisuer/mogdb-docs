---
title: PG_STAT_SYS_INDEXES
summary: PG_STAT_SYS_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_SYS_INDEXES

**PG_STAT_SYS_INDEXES** displays index status information about all the system catalogs in the **pg_catalog** and **information_schema** schemas.

**Table 1** PG_STAT_SYS_INDEXES columns

| Name          | Type   | Description                                                  |
| :------------ | :----- | :----------------------------------------------------------- |
| relid         | oid    | OID of the table for this index                              |
| indexrelid    | oid    | OID of the index                                             |
| schemaname    | name   | Name of the schema that the index is in                      |
| relname       | name   | Name of the table for the index                              |
| indexrelname  | name   | Index name                                                   |
| idx_scan      | bigint | Number of index scans initiated on the index                 |
| idx_tup_read  | bigint | Number of index entries returned by scans on the index       |
| idx_tup_fetch | bigint | Number of live table rows fetched by simple index scans using the index |
