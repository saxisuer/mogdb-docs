---
title: PG_OPFAMILY
summary: PG_OPFAMILY
author: Guo Huan
date: 2021-04-19
---

# PG_OPFAMILY

**PG_OPFAMILY** defines operator families.

Each operator family is a collection of operators and associated support routines that implement semantics specified for a particular index access method. Furthermore, the operators in a family are all compatible, in a way that is specified by the access method. The operator family allows cross-data-type operators to be used with indexes and to be reasoned about using knowledge of access method semantics.

**Table 1** PG_OPFAMILY columns

| Name         | Type | Reference        | Description                                                |
| :----------- | :--- | :--------------- | :--------------------------------------------------------- |
| oid          | oid  | -                | Row identifier (hidden attribute, which must be specified) |
| opfmethod    | oid  | PG_AM.oid        | Index access method used by an operator family             |
| opfname      | name | -                | Name of the operator family                                |
| opfnamespace | oid  | PG_NAMESPACE.oid | Namespace of the operator family                           |
| opfowner     | oid  | PG_AUTHID.oid    | Owner of the operator family                               |

The majority of the information defining an operator family is not in its **PG_OPFAMILY** row, but in the associated rows in PG_AMOP, PG_AMPROC, and PG_OPCLASS.
