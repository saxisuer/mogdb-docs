---
title: PG_TS_TEMPLATE
summary: PG_TS_TEMPLATE
author: Guo Huan
date: 2021-04-19
---

# PG_TS_TEMPLATE

**PG_TS_TEMPLATE** contains entries defining text search templates. A template provides a framework for text search dictionaries. Since a template must be implemented by C-language-level functions, templates can be created only by database administrators.

**Table 1** PG_TS_TEMPLATE columns

| Name          | Type    | Reference        | Description                                                |
| :------------ | :------ | :--------------- | :--------------------------------------------------------- |
| oid           | oid     | -                | Row identifier (hidden attribute, which must be specified) |
| tmplname      | name    | -                | Text search template name                                  |
| tmplnamespace | oid     | PG_NAMESPACE.oid | OID of the namespace that contains the template            |
| tmplinit      | regproc | PG_PROC.proname  | Name of the template's initialization function             |
| tmpllexize    | regproc | PG_PROC.proname  | Name of the template's lexize function                     |
