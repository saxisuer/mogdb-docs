---
title: PG_OBJECT
summary: PG_OBJECT
author: Guo Huan
date: 2021-04-19
---

# PG_OBJECT

**PG_OBJECT** records the creator, creation time, and last modification time of objects of specified types (ordinary tables, indexes, sequences, views, stored procedures, and functions).

**Table 1** PG_OBJECT columns

| Name        | Type                     | Description                                                  |
| :---------- | :----------------------- | :----------------------------------------------------------- |
| object_oid  | oid                      | Object identifier                                            |
| object_type | "char"                   | Object type<br/>- **r**: ordinary table<br/>- **i**: index<br/>- **s**: sequence<br/>- **v**: view<br/>- **p**: stored procedure and function |
| creator     | oid                      | ID of the creator                                            |
| ctime       | timestamp with time zone | Creation time of the object                                  |
| mtime       | timestamp with time zone | Last modification time of the object. The modification operations include **ALTER**, **GRANT**, and **REVOKE**. |
| createcsn   | int8                     | CSN when an object is created                                |
| changecsn   | int8                     | CSN when DDL operations are performed on a table or an index |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - Objects created or modified during database initialization (initdb) cannot be recorded. **PG_OBJECT** does not contain these object records.
> - When an object created before the upgrade is modified again, the modification time (specified by **mtime**) is recorded. When DDL operations are performed on a table or an index, the transaction commit sequence number (specified by **changecsn**) of the transaction to which the table or index belongs is recorded. Because the creation time of the object cannot be obtained, **ctime** and **createcsn** are empty.
> - The time recorded by **ctime** and **mtime** is the start time of the transaction to which the current operation belongs.
> - The time of object modification due to capacity expansion is also recorded.
> - **createcsn** and **changecsn** record the transaction commit sequence number of the transaction to which the current operation belongs.
