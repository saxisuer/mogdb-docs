---
title: PG_TYPE
summary: PG_TYPE
author: Guo Huan
date: 2021-04-19
---

# PG_TYPE

**PG_TYPE** stores information about data types.

**Table 1** PG_TYPE columns

| Name           | Type         | Description                                                  |
| :------------- | :----------- | :----------------------------------------------------------- |
| oid            | oid          | Row identifier (hidden attribute, which must be specified)   |
| typname        | name         | Data type name                                               |
| typnamespace   | oid          | OID of the namespace that contains the type                  |
| typowner       | oid          | Owner of the type                                            |
| typlen         | smallint     | Number of bytes in the internal representation of the type for a fixed-size type. It is a negative number for a variable-length type.<br/>- The value **-1** indicates a "varlena" type (one that has a length word).<br/>- The value **-2** indicates a null-terminated C string. |
| typbyval       | Boolean      | Whether the value of this type is passed by a parameter or reference of this column. **typbyval** is **false** if the type of **typlen** is not **1**, **2**, **4**, or **8**, because values of this type are always passed by reference of this column. **typbyval** can be **false** even if the **typlen** is passed by a parameter of this column. |
| typtype        | "char"       | - **b**: base type.<br/>- **c**: composite type (for example, a table's row type)<br/>- **d**: domain<br/>- **p**: pseudo<br/>For details, see **typrelid** and **typbasetype**. |
| typcategory    | "char"       | **typcategory** is an arbitrary classification of data types that is used by the parser to determine which implicit casts should be preferred. |
| typispreferred | Boolean      | The value is **true** if conversion is performed when data meets conversion rules specified by **typcategory**. |
| typisdefined   | Boolean      | Whether a type has been defined. It is **true** if the type is defined, and **false** if this is a placeholder entry for a not-yet-defined type. When it is **false**, nothing except the type name, namespace, and OID can be relied on. |
| typdelim       | "char"       | Character that separates two values of this type when parsing an array input. Note that the delimiter is associated with the array element data type, not the array data type. |
| typrelid       | oid          | If this is a composite type (see **typtype**), then this column points to the **PG_CLASS** entry that defines the corresponding table. For a free-standing composite type, the **PG_CLASS** entry does not represent a table, but it is required for the type's **PG_ATTRIBUTE** entries to link to. It is **0** for non-composite type. |
| typelem        | oid          | If **typelem** is not **0**, it identifies another row in **PG_TYPE**. The current type can be described as an array yielding values of type **typelem**. A "true" array type has a variable length (**typlen** = **-1**), but some fixed-length types (**typlen** > **0**) also have non-zero **typelem**, for example **name** and **point**. If a fixed-length type has a **typelem**, its internal representation must be a number of values of the **typelem** data type with no other data. Variable-length array types have a header defined by the array subroutines. |
| typarray       | oid          | If the value is not **0**, the corresponding type record is available in **PG_TYPE**. |
| typinput       | regproc      | Input conversion function (text format)                      |
| typoutput      | regproc      | Output conversion function (text format)                     |
| typreceive     | regproc      | Input conversion function (binary format); **0** for non-input conversion function |
| typsend        | regproc      | output conversion function (binary format); **0** for non-output conversion function |
| typmodin       | regproc      | Type modifier input function; **0** if the type does not support modifiers |
| typmodout      | regproc      | Type modifier output function; **0** if the type does not support modifiers |
| typanalyze     | regproc      | Custom **ANALYZE** function; **0** if the standard function is used |
| typalign       | "char"       | Alignment required when storing a value of this type. It applies to storage on disks as well as most representations of the value inside PostgreSQL. When multiple values are stored consecutively, such as in the representation of a complete row on disk, padding is inserted before a data of this type so that it begins on the specified boundary. The alignment reference is the beginning of the first datum in the sequence. Possible values are:<br/>- **c**: char alignment, that is, no alignment needed<br/>- **s**: short alignment (2 bytes on most machines)<br/>- **i**: integer alignment (4 bytes on most machines)<br/>- **d**: double alignment (8 bytes on many machines, but by no means all)<br/>NOTICE:<br/>For types used in system tables, the size and alignment defined in **PG_TYPE** must agree with the way that the compiler lays out the column in a structure representing a table row. |
| typstorage     | "char"       | **typstorage** tells for varlena types (those with **typlen = -1**) if the type is prepared for toasting and what the default strategy for attributes of this type should be. Possible values are:<br/>- **p**: Values are always stored plain.<br/>- **e**: Value can be stored in a secondary relationship (if the relation has one, see **pg_class.reltoastrelid**).<br/>- **m**: Values can be stored compressed inline.<br/>- **x**: Values can be stored compressed inline or stored in secondary storage.<br/>NOTICE:<br/>**m** domains can also be moved out to secondary storage, but only as a last resort (**e** and **x** domains are moved first). |
| typenotnull    | Boolean      | Whether the type has a NOTNULL constraint. Currently, it is used for domains only. |
| typbasetype    | oid          | If this is a domain (see **typtype**), then **typbasetype** identifies the type that this one is based on. The value is **0** if this type is not a derived type. |
| typtypmod      | integer      | Records the **typtypmod** to be applied to domains' base types by domains (the value is **-1** if the base type does not use **typmod**). This is **-1** if this type is not a domain. |
| typndims       | integer      | Number of array dimensions for a domain that is an array (**typbasetype** is an array type; the domain's **typelem** matches the base type's **typelem**). This is **0** for types other than domains over array types. |
| typcollation   | oid          | Sequence rule for specified types (**0** if sequencing is not supported) |
| typdefaultbin  | pg_node_tree | **nodeToString()** representation of a default expression for the type if the value is non-null. Currently, this column is only used for domains. |
| typdefault     | text         | The value is **NULL** if a type has no associated default value.<br/>- If **typdefaultbin** is not **NULL**, **typdefault** must contain a default expression represented by **typdefaultbin**.<br/>- If **typdefaultbin** is **NULL** and **typdefault** is not, then **typdefault** is the external representation of the type's default value, which can be fed to the type's input converter to produce a constant. |
| typacl         | aclitem[]    | Access permissions                                           |
