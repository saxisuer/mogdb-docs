---
title: PG_ATTRDEF
summary: PG_ATTRDEF
author: Guo Huan
date: 2021-04-19
---

# PG_ATTRDEF

**PG_ATTRDEF** records default values of columns.

**Table 1** PG_ATTRDEF columns

| Name     | Type         | Description                                                  |
| :------- | :----------- | :----------------------------------------------------------- |
| oid      | oid          | Row identifier (hidden attribute, which must be specified)   |
| adrelid  | oid          | Table to which a column belongs                              |
| adnum    | smallint     | Number of columns                                            |
| adbin    | pg_node_tree | Internal representation of the default value of a column or of a generated expression |
| adsrc    | text         | Internal representation of a readable default value or of a generated expression |
| adgencol | char         | Specifies whether a column is a generated column. The value **s** indicates that the column is a generated column, and the value **'\0** indicates that the column is a common column. The default value is **'\0**. |
