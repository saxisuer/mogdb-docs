---
title: PG_AUTH_MEMBERS
summary: PG_AUTH_MEMBERS
author: Guo Huan
date: 2021-04-19
---

# PG_AUTH_MEMBERS

**PG_AUTH_MEMBERS** records the membership between roles.

**Table 1** PG_AUTH_MEMBERS columns

| Name         | Type    | Description                                               |
| :----------- | :------ | :-------------------------------------------------------- |
| roleid       | oid     | ID of a role that has a member                            |
| member       | oid     | ID of a role that is a member of ROLEID                   |
| grantor      | oid     | ID of a role that grants this membership                  |
| admin_option | Boolean | Whether a member can grant membership in ROLEID to others |
