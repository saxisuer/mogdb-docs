---
title: PG_LANGUAGE
summary: PG_LANGUAGE
author: Guo Huan
date: 2021-04-19
---

# PG_LANGUAGE

**PG_LANGUAGE** registers programming languages. You can use them and interfaces to write functions or stored procedures.

**Table 1** PG_LANGUAGE columns

| Name          | Type      | Reference     | Description                                                  |
| :------------ | :-------- | :------------ | :----------------------------------------------------------- |
| oid           | oid       | -             | Row identifier (hidden attribute, which must be specified)   |
| lanname       | name      | -             | Language name                                                |
| lanowner      | oid       | PG_AUTHID.oid | Owner of the language                                        |
| lanispl       | Boolean   | -             | The value is **false** for internal languages (such as SQL) and **true** for user-defined languages. Currently, **GS_DUMP** still uses this column to determine which languages need to be dumped, but this might be replaced by a different mechanism in the future. |
| lanpltrusted  | Boolean   | -             | The value is **true** if this is a trusted language, which means that it is believed not to grant access to anything outside the normal SQL execution environment. Only the initial user can create functions in untrusted languages. |
| lanplcallfoid | oid       | PG_PROC.oid   | For non-internal languages, this column references the language handler, which is a special function responsible for executing all functions that are written in the particular language. |
| laninline     | oid       | PG_PROC.oid   | This column references a function responsible for executing "inline" anonymous code blocks (DO blocks). The value is **0** if inline blocks are not supported. |
| lanvalidator  | oid       | PG_PROC.oid   | This column references a language validator function responsible for checking the syntax and validity of new functions when they are created. The value is **0** if no validator is provided. |
| lanacl        | aclitem[] | -             | Access permissions                                           |
