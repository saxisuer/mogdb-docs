---
title: GS_POLICY_LABEL
summary: GS_POLICY_LABEL
author: Guo Huan
date: 2021-06-15
---

# GS_POLICY_LABEL

**GS_POLICY_LABEL** records the resource label configuration information. One resource label corresponds to one or more records, and each record identifies the resource label to which a database resource belongs. Only the system administrator or security policy administrator can access this system catalog.

Fully Qualified Domain Name (FQDN) identifies an absolute path of a database resource.

**Table 1** GS_POLICY_LABEL columns

| Name          | Type | Description                                                  |
| :------------ | :--- | :----------------------------------------------------------- |
| oid           | oid  | Row identifier (hidden attribute, which must be specified)   |
| labelname     | name | Resource label name                                          |
| labeltype     | name | Resource tag type. Currently, the value is **RESOURCE**.     |
| fqdnnamespace | oid  | OID of a namespace to which an identified database resource belongs |
| fqdnid        | oid  | OID of an identified database resource. If the database resource is a column, this column is the OID of the catalog. |
| relcolumn     | name | Column name. If the identified database resource is a column, this column indicates the column name. Otherwise, this column is empty. |
| fqdntype      | name | Type of the identified database resource, for example, schema, table, column, or view |
