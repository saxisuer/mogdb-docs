---
title: PG_REWRITE
summary: PG_REWRITE
author: Guo Huan
date: 2021-04-19
---

# PG_REWRITE

**PG_REWRITE** records rewrite rules defined for tables and views.

**Table 1** PG_REWRITE columns

| Name       | Type         | Description                                                  |
| :--------- | :----------- | :----------------------------------------------------------- |
| oid        | oid          | Row identifier (hidden attribute, which must be specified)   |
| rulename   | name         | Name of a rule                                               |
| ev_class   | oid          | Name of the table that uses the rule                         |
| ev_attr    | smallint     | Column to which this rule applies (always **0** to indicate the entire table) |
| ev_type    | "char"       | Event type for this rule<br/>- 1 = SELECT<br/>- 2 = UPDATE<br/>- 3 = INSERT<br/>- 4 = DELETE |
| ev_enabled | "char"       | Controls the mode in which the rule is triggered.<br/>- **O**: The rule is triggered in origin and local modes.<br/>- **D**: The rule is disabled.<br/>- **R**: The rule is triggered in replica mode.<br/>- **A**: The rule is always triggered. |
| is_instead | Boolean      | The value is **true** if the rule is of the **INSTEAD** type. |
| ev_qual    | pg_node_tree | Expression tree (in the form of a **nodeToString**() representation) for the rule's qualifying condition |
| ev_action  | pg_node_tree | Query tree (in the form of a **nodeToString**() representation) for the rule's action |
