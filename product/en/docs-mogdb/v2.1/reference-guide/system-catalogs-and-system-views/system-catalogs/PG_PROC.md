---
title: PG_PROC
summary: PG_PROC
author: Guo Huan
date: 2021-04-19
---

# PG_PROC

**PG_PROC** records information about functions or procedures.

**Table 1** PG_PROC columns

| Name                | Type              | Description                                                  |
| :------------------ | :---------------- | :----------------------------------------------------------- |
| oid                 | oid               | Row identifier (hidden attribute, which must be specified)   |
| proname             | name              | Function name                                                |
| pronamespace        | oid               | OID of the namespace that contains the function              |
| proowner            | oid               | Owner of the function                                        |
| prolang             | oid               | Implementation language or call interface of the function    |
| procost             | real              | Estimated execution cost                                     |
| prorows             | real              | Estimated number of rows that are influenced                 |
| provariadic         | oid               | Data type of parameter element                               |
| protransform        | regproc           | Simplified call method for the function                      |
| proisagg            | Boolean           | Whether the function is an aggregate function<br />- **t** (true): yes<br />- **f** (false): no |
| proiswindow         | Boolean           | Whether the function is a window function<br />- **t** (true): yes<br />- **f** (false): no |
| prosecdef           | Boolean           | Whether the function is a security definer (or a "setuid" function)<br />- **t** (true): yes<br />- **f** (false): no |
| proleakproof        | Boolean           | This function has no side effect. If no leakproof treatment is provided for parameters, the function throws errors.<br />- **t** (true): There is no side effect.<br />- **f** (false): There are side effects. |
| proisstrict         | Boolean           | The function returns null if any call parameter is null. In that case, the function is actually not called at all. Functions that are not "strict" must be prepared to process null inputs. |
| proretset           | Boolean           | The function returns a set (multiple values of a specified data type). |
| provolatile         | "char"            | Whether the function's result depends only on its input parameters, or is affected by outside factors.<br />- **i**: for "immutable" functions, which always deliver the same result for the same inputs.<br />- **s**: for "stable" functions, whose results (for fixed inputs) do not change within a scan.<br />- **v**: for "volatile" functions, whose results may change at any time. Use **v** also for functions with side-effects, so that the engine cannot get optimized if volatile functions are called. |
| pronargs            | smallint          | Number of parameters                                         |
| pronargdefaults     | smallint          | Number of parameters that have default values                |
| prorettype          | oid               | Data type of return values                                   |
| proargtypes         | oidvector         | Array that stores the data types of function parameters. This array includes only input parameters (including **INOUT** parameters), and indicates the call signature (interface) of the function. |
| proallargtypes      | oid[]             | Array that contains the data types of function parameters. This array includes all parameter types (including **OUT** and **INOUT** parameters); however, if all the parameters are **IN** parameters, this column is null. Note that array subscripting is 1-based, whereas for historical reasons, **proargtypes** is subscripted from 0. |
| proargmodes         | "char"[]          | Array with the modes of the function parameters, encoded as follows:<br />- **i** indicates the IN parameter.<br />- **o** indicates the OUT parameter.<br />- **b** indicates the INOUT parameter.<br />- **v** indicates the VARIADIC parameter.<br />If all the parameters are **IN** parameters, this column is null. Note that subscripts correspond to positions of **proallargtypes**, not **proargtypes**. |
| proargnames         | text[]            | Array that stores the names of the function parameters. Parameters without a name are set to empty strings in the array. If none of the parameters have a name, this column is null. Note that subscripts correspond to positions of **proallargtypes**, not **proargtypes**. |
| proargdefaults      | pg_node_tree      | Expression tree of the default value. This is the list of **pronargdefaults** elements. |
| prosrc              | text              | A definition that describes a function or stored procedure. In an interpreting language, it is the function source code, a link symbol, a file name, or any body content specified when a function or stored procedure is created, depending on how a language or call is used. |
| probin              | text              | Additional information about how to call the function. Again, the interpretation is language-specific. |
| proconfig           | text[]            | Function's local settings for run-time configuration variables. |
| proacl              | aclitem[]         | Access permissions. For details, see GRANT and REVOKE.       |
| prodefaultargpos    | int2vector        | Position of the input parameter of a function with a default value. |
| fencedmode          | Boolean           | Execution mode of a function, indicating whether the function is executed in fence or not fence mode. If the execution mode is **fence,** the function is executed in the fork process that is reworked.<br />In the C function created by the user, the default value of fencedmode is **true**, indicating the fence mode. For built-in functions in the system, the fencedmode field is set to **false**, indicating the not fence mode. |
| proshippable        | Boolean           | Whether the function can be pushed down to database nodes. The default value is **false**.<br />- Functions of the IMMUTABLE type can always be pushed down to the database nodes.<br />- A STABLE or VOLATILE function can be pushed down to the database nodes only if SHIPPABLE is specified for it. |
| propackage          | Boolean           | Whether the function supports overloading. The default value is **false**.<br />- **t** (true): supported.<br />- **f** (false): not supported. |
| prokind             | "char"            | Whether the object is a function or a stored procedure.<br />- **f** indicates that the object is a function.<br />- **p** indicates that the object is a stored procedure. |
| proargsrc           | text              | Describes the parameter input strings of functions or stored procedures that are compatible with Oracle syntax, including parameter comments. The default value is **NULL**. |
| proisprivate        | Boolean           | Whether a function is a private function in the package. The default value is **false**. |
| propackageid        | oid               | OID of the package to which the function belongs. If the function is not in the package, the value is **0**. |
| proargtypesext      | oidvector_extend  | Data type array used to store function parameters when there are a large number of function parameters. This array includes only input parameters (including **INOUT** parameters), and indicates the call signature (interface) of the function. |
| prodefaultargposext | int2vector_extend | Position of the input parameter with a default value when the function has a large number of parameters. |
