---
title: GS_AUDITING_POLICY_ACCESS
summary: GS_AUDITING_POLICY_ACCESS
author: Guo Huan
date: 2021-06-15
---

# GS_AUDITING_POLICY_ACCESS

**GS_AUDITING_POLICY_ACCESS** records the DML database operations about the unified audit. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_AUDITING_POLICY_ACCESS columns

| Name       | Type                        | Description                                                  |
| :--------- | :-------------------------- | :----------------------------------------------------------- |
| oid        | oid                         | Row identifier (hidden attribute, which must be specified)   |
| accesstype | name                        | DML database operation type. For example, SELECT, INSERT, and DELETE. |
| labelname  | name                        | Specifies the resource label name. This parameter corresponds to the **polname** column in the **GS_AUDITING_POLICY** system catalog. |
| policyoid  | oid                         | This parameter corresponds to OIDs in the **GS_AUDITING_POLICY** system catalog. |
| modifydate | timestamp without time zone | The latest creation or modification timestamp.               |
