---
title: GS_MASKING_POLICY
summary: GS_MASKING_POLICY
author: Guo Huan
date: 2021-06-15
---

# GS_MASKING_POLICY

**GS_MASKING_POLICY** records the main information about dynamic data masking policies. Each record corresponds to a masking policy. Only the system administrator or security policy administrator can access this system catalog.

**Table 1** GS_MASKING_POLICY columns

| Name        | Type      | Description                                                  |
| :---------- | :-------- | :----------------------------------------------------------- |
| oid         | oid       | Row identifier (hidden attribute, which must be specified)   |
| polname     | name      | Policy name, which must be unique                            |
| polcomments | name      | Policy description field, which records policy-related description information and is represented by the **COMMENTS** keyword |
| modifydate  | timestamp | Latest timestamp when a policy is created or modified.       |
| polenabled  | Boolean   | Specifies whether to enable the policy.                      |
