---
title: GS_COLUMN_KEYS_ARGS
summary: GS_COLUMN_KEYS_ARGS
author: Guo Huan
date: 2021-04-19
---

# GS_COLUMN_KEYS_ARGS

**GS_COLUMN_KEYS_ARGS** records the metadata about the CMK in the encrypted equality feature. Each record corresponds to a key-value pair of the CMK.

**Table 1** GS_COLUMN_KEYS_ARGS columns

| Name          | Type  | Description                    |
| :------------ | :---- | :----------------------------- |
| oid           | oid   | Row identifier (hidden column) |
| column_key_id | oid   | CEK OID                        |
| function_name | name  | The value is **encryption**.   |
| key           | name  | CEK metadata name              |
| value         | bytea | Value of the CEK metadata name |
