---
title: GS_MATVIEW
summary: GS_MATVIEW
author: Guo Huan
date: 2021-06-15
---

# GS_MATVIEW

**GS_MATVIEW** provides information about each materialized view in the database.

**Table 1** GS_MATVIEW columns

| Name        | Type      | Description                                                  |
| :---------- | :-------- | :----------------------------------------------------------- |
| oid         | oid       | Row identifier (hidden attribute, which must be specified)   |
| matviewid   | oid       | OID of a materialized view.                                  |
| mapid       | oid       | OID of a map table associated with a materialized view. Each map table corresponds to one materialized view. If a full materialized view does not correspond to a map table, the value of this column is **0**. |
| ivm         | boolean   | Type of a materialized view. The value **t** indicates an incremental materialized view, and the value **f** indicates a full materialized view. |
| needrefresh | boolean   | Reserved column                                              |
| refreshtime | timestamp | Last time when a materialized view was refreshed. If the materialized view is not refreshed, the value is null. This column is maintained only for incremental materialized views. For full materialized views, the value is null. |
