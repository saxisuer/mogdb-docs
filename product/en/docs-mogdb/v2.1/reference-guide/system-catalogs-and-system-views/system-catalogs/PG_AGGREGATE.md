---
title: PG_AGGREGATE
summary: PG_AGGREGATE
author: Guo Huan
date: 2021-04-19
---

# PG_AGGREGATE

PG_AGGREGATE records information about aggregate functions. Each entry in **PG\_AGGREGATE** is an extension of an entry in **PG\_PROC**. The **PG_PROC** entry carries the aggregate's name, input and output data types, and other information that is similar to ordinary functions.

**Table 1** PG_AGGREGATE columns

| Name             | Type     | Reference       | Description                                                  |
| :--------------- | :------- | :-------------- | :----------------------------------------------------------- |
| aggfnoid         | regproc  | PG_PROC.proname | PG_PROC proname of the aggregate function                    |
| aggtransfn       | regproc  | PG_PROC.proname | Transition function                                          |
| aggcollectfn     | regproc  | PG_PROC.proname | Collect function                                             |
| aggfinalfn       | regproc  | PG_PROC.proname | Final function (**0** if none)                               |
| aggsortop        | oid      | PG_OPERATOR.oid | Associated sort operator (**0** if none)                     |
| aggtranstype     | oid      | PG_TYPE.oid     | Data type of the aggregate function's internal transition (state) data |
| agginitval       | text     | -               | Initial value of the transition state. This is a text column containing the initial value in its external string representation. If this column is null, the transition state value starts from null. |
| agginitcollect   | text     | -               | Initial value of the collection state. This is a text column containing the initial value in its external string representation. If this column is null, the collection state value starts from null. |
| aggkind          | "char"   | -               | Type of the aggregate function:<br/>- **n**: normal aggregate<br/>- **o**: ordered set aggregate |
| aggnumdirectargs | smallint | -               | Number of direct parameters (non-aggregation-related parameters) of the aggregate function of the ordered set aggregate type. For an aggregate function of the normal aggregate type, the value is **0**. |
