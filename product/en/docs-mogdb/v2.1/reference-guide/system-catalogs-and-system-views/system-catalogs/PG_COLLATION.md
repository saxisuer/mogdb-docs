---
title: PG_COLLATION
summary: PG_COLLATION
author: Guo Huan
date: 2021-04-19
---

# PG_COLLATION

**PG_COLLATION** describes available collations, which are essentially mappings from an SQL name to operating system locale categories.

**Table 1** PG_COLLATION columns

| Name          | Type    | Reference        | Description                                                  |
| :------------ | :------ | :--------------- | :----------------------------------------------------------- |
| oid           | oid     | -                | Row identifier (hidden attribute, which must be specified)   |
| collname      | name    | -                | Collation name (unique per namespace and encoding)           |
| collnamespace | oid     | PG_NAMESPACE.oid | OID of the namespace that contains this collation            |
| collowner     | oid     | PG_AUTHID.oid    | Owner of the collation                                       |
| collencoding  | integer | -                | Encoding in which the collation is applicable, or **-1** if it works for any encoding. It is compatible with PostgreSQL. |
| collcollate   | name    | -                | **LC_COLLATE** for this collation object                     |
| collctype     | name    | -                | **LC_CTYPE** for this collation object                       |
