---
title: PG_APP_WORKLOADGROUP_MAPPING
summary: PG_APP_WORKLOADGROUP_MAPPING
author: Guo Huan
date: 2021-04-19
---

# PG_APP_WORKLOADGROUP_MAPPING

**PG_APP_WORKLOADGROUP_MAPPING** provides load mapping group information in the database.

**Table 1** PG_APP_WORKLOADGROUP_MAPPING columns

| Name            | Type | Description                                                |
| :-------------- | :--- | :--------------------------------------------------------- |
| oid             | oid  | Row identifier (hidden attribute, which must be specified) |
| appname         | name | Application name                                           |
| workload_gpname | name | Mapped workload group name                                 |
