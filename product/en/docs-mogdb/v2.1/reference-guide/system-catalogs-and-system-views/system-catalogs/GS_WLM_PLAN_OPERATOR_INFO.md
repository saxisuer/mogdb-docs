---
title: GS_WLM_PLAN_OPERATOR_INFO
summary: GS_WLM_PLAN_OPERATOR_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_PLAN_OPERATOR_INFO

**GS_WLM_PLAN_OPERATOR_INFO** displays records about plan operator levels of completed jobs. Data is dumped from the kernel to this system catalog.

**Table 1** GS_WLM_PLAN_OPERATOR_INFO columns<a id="GS_WLM_PLAN_OPERATOR_INFO columns"> </a>

| Name            | Type    | Description                                                  |
| :-------------- | :------ | :----------------------------------------------------------- |
| datname         | name    | Name of the database where the collected plan information is located |
| queryid         | bigint  | Internal query ID used for statement execution               |
| plan_node_id    | integer | Plan node ID of the execution plan                           |
| startup_time    | bigint  | Time when the operator starts to process the first data record |
| total_time      | bigint  | Total execution time of the operator, in ms                  |
| actual_rows     | bigint  | Number of rows that are actually executed                    |
| max_peak_memory | integer | Maximum peak memory used by the operator on database nodes, in MB |
| query_dop       | integer | DOP of the operator                                          |
| parent_node_id  | integer | Parent node ID of the operator                               |
| left_child_id   | integer | Left child node ID of the operator                           |
| right_child_id  | integer | Right child node ID of the operator                          |
| operation       | text    | Name of the operation performed by the operator              |
| orientation     | text    | Alignment mode of the operator                               |
| strategy        | text    | Implementation method of the operator                        |
| options         | text    | Selection mode of the operator                               |
| condition       | text    | Filter criteria of the operator                              |
| projection      | text    | Mapping relationship of the operator                         |
