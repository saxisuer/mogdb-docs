---
title: GS_RECYCLEBIN
summary: GS_RECYCLEBIN
author: Zhang Cuiping
date: 2021-10-11
---

# GS_RECYCLEBIN

**gs_recyclebin** describes details about objects in the recycle bin.

**Table 1** gs_recyclebin columns

| Name           | Type        | Description                             |
| :------------- | :---------- | :---------------------------------------|
| oid            | oid         | System column                                                |
| rcybaseid      | oid         | Base table object ID, which references **gs_recyclebin.oid** |
| rcydbid        | oid         | OID of the database to which the current object belongs      |
| rcyrelid       | oid         | OID of the current object                                    |
| rcyname        | name        | Name of the object in the recycle bin. The format is `BIN$unique_id$oid$0`. **unique_id** indicates the unique identifier with a maximum of 16 characters, and **oid** indicates the OID. |
| rcyoriginname  | name        | Original object name                                         |
| rcyoperation   | "char"      | Operation type<br />- **d**: drop<br />- **t**: truncate     |
| rcytype        | int         | Object type<br />- **0**: table<br />- **1**: index<br />- **2**: TOAST table<br />- **3**: TOAST index<br />- **4**: sequence, indicating the sequence object that is automatically associated with the serial and bigserial types. |
| rcyrecyclecsn  | bigint      | CSN when an object is dropped or truncated                   |
| rcyrecycletime | timestamptz | Time when an object is dropped or truncated                  |
| rcycreatecsn   | bigint      | CSN when an object is created                                |
| rcychangecsn   | bigint      | SCN when an object definition is modified                    |
| rcynamespace   | oid         | OID of the namespace that contains this relationship         |
| rcyowner       | oid         | Relationship owner                                           |
| rcytablespace  | oid         | Tablespace where the relationship is stored. If the value is **0**, the default tablespace of the database is used. This column is meaningless if the relationship has no on-disk file. |
| rcyrelfilenode | oid         | File name of the recycle bin object on a disk, or **0** if none, used to restore the texture file when the **TRUNCATE** object is restored. |
| rcycanrestore  | bool        | Whether flashback can be performed separately                |
| rcycanpurge    | bool        | Whether the purge operation can be performed independently   |
| rcyfrozenxid   | xid32       | All transaction IDs before this one have been replaced with a permanent (frozen) transaction ID in the table. |
| rcyfrozenxid64 | xid         | All transaction IDs before this one have been replaced with a permanent (frozen) transaction ID in the table. |
