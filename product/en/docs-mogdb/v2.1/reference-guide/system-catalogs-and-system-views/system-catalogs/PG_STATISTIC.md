---
title: PG_STATISTIC
summary: PG_STATISTIC
author: Guo Huan
date: 2021-04-19
---

# PG_STATISTIC

**PG_STATISTIC** records statistics about tables and index columns in a database. By default, only the system administrator can access the system catalog. Common users can access the system catalog only after being authorized.

**Table 1** PG_STATISTIC columns

| Name          | Type     | Description                                                  |
| :------------ | :------- | :----------------------------------------------------------- |
| starelid      | oid      | Table or index that the described column belongs to          |
| starelkind    | "char"   | Type of an object                                            |
| staattnum     | smallint | Number of the described column in the table, starting from 1 |
| stainherit    | Boolean  | Whether to collect statistics for objects that have inheritance relationship |
| stanullfrac   | real     | Percentage of column entries that are null                   |
| stawidth      | integer  | Average stored width, in bytes, of non-null entries          |
| stadistinct   | real     | Number of distinct, non-**NULL** data values in the column for database nodes<br/>- A value greater than 0 is the actual number of distinct values.<br/>- A value less than 0 is the negative of a multiplier for the number of rows in the table. (For example, **stadistinct=-0.5** indicates that values in a column appear twice on average.)<br/>- The value **0** indicates that the number of distinct values is unknown. |
| stakindN      | smallint | Code number stating that the type of statistics is stored in slot N of the **pg_statistic** row<br/>Value range: 1 to 5 |
| staopN        | oid      | Operator used to generate the statistics stored in slot N. For example, a histogram slot shows the < operator that defines the sort order of the data.<br/>Value range: 1 to 5 |
| stanumbersN   | real[]   | Numerical statistics of the appropriate type for slot N. The value is **NULL** if the slot does not involve numerical values.<br/>Value range: 1 to 5 |
| stavaluesN    | anyarray | Column data values of the appropriate type for slot N. The value is **NULL** if the slot type does not store any data values. Each array's element values are actually of the specific column's data type so there is no way to define these columns' type more specifically than anyarray.<br/>Value range: 1 to 5 |
| stadndistinct | real     | Number of unique non-null data values in the **dn1** column<br/>- A value greater than 0 is the actual number of distinct values.<br/>- A value less than 0 is the negative of a multiplier for the number of rows in the table. (For example, **stadistinct=-0.5** indicates that values in a column appear twice on average.)<br/>- The value **0** indicates that the number of distinct values is unknown. |
| staextinfo    | text     | Information about extension statistics. This is reserved.    |
