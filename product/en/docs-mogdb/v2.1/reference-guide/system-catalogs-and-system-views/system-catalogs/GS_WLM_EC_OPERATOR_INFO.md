---
title: GS_WLM_EC_OPERATOR_INFO
summary: GS_WLM_EC_OPERATOR_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_EC_OPERATOR_INFO

**GS_WLM_EC_OPERATOR_INFO** records operator information after an Extension Connector job ends. If **enable_resource_record** is set to **on**, the system imports records from **GS_WLM_EC_OPERATOR_HISTORY** to this system catalog every 3 minutes. This operation occupies storage space and affects performance. Only users with the **sysadmin** permission can query this system catalog.

**Table 1** GS_WLM_EC_OPERATOR_INFO columns

| Name                | Type                     | Description                                                  |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| queryid             | bigint                   | Internal query ID used for Extension Connector statement execution |
| plan_node_id        | integer                  | Plan node ID of the execution plan of an Extension Connector operator |
| start_time          | timestamp with time zone | Time when the Extension Connector operator starts to process the first data record |
| duration            | bigint                   | Total execution time of the Extension Connector operator (unit: ms) |
| tuple_processed     | bigint                   | Number of elements returned by the Extension Connector operator |
| min_peak_memory     | integer                  | Minimum peak memory used by the Extension Connector operator on all DNs (unit: MB) |
| max_peak_memory     | integer                  | Maximum peak memory used by the Extension Connector operator on all DNs (unit: MB) |
| average_peak_memory | integer                  | Average peak memory used by the Extension Connector operator on all DNs (unit: MB) |
| ec_status           | text                     | Status of the Extension Connector job                        |
| ec_execute_datanode | text                     | Name of the DN executing the Extension Connector job         |
| ec_dsn              | text                     | DSN used by the Extension Connector job                      |
| ec_username         | text                     | Username used by the Extension Connector job to access a remote database instance (the value is null if the remote database instance is SPARK.) |
| ec_query            | text                     | Statement sent by the Extension Connector job to a remote database instance |
| ec_libodbc_type     | text                     | Type of the unixODBC driver used by the Extension Connector job |
