---
title: PG_LARGEOBJECT_METADATA
summary: PG_LARGEOBJECT_METADATA
author: Guo Huan
date: 2021-04-19
---

# PG_LARGEOBJECT_METADATA

**PG_LARGEOBJECT_METADATA** records metadata associated with large objects. The actual large object data is stored in **PG_LARGEOBJECT**.

**Table 1** PG_LARGEOBJECT_METADATA columns

| Name     | Type      | Reference     | Description                                                |
| :------- | :-------- | :------------ | :--------------------------------------------------------- |
| oid      | oid       | -             | Row identifier (hidden attribute, which must be specified) |
| lomowner | oid       | PG_AUTHID.oid | Owner of the large object                                  |
| lomacl   | aclitem[] | -             | Access permissions                                         |
