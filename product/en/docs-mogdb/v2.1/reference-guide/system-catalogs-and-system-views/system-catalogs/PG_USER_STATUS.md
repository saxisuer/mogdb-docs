---
title: PG_USER_STATUS
summary: PG_USER_STATUS
author: Guo Huan
date: 2021-04-19
---

# PG_USER_STATUS

**PG_USER_STATUS** provides the states of users who access the database. This system catalog is accessible only to system administrators.

**Table 1** PG_USER_STATUS columns

| Name            | Type                     | Description                                                  |
| :-------------- | :----------------------- | :----------------------------------------------------------- |
| oid             | oid                      | Row identifier (hidden attribute, which must be specified)   |
| roloid          | oid                      | ID of a role                                                 |
| failcount       | integer                  | Number of failed attempts                                    |
| locktime        | timestamp with time zone | Time at which the role is locked                             |
| rolstatus       | smallint                 | Role state<br/>- **0**: normal<br/>- **1**: The role is locked for a specific period of time because the failed login attempts exceed the threshold.<br/>- **2**: The role is locked by the administrator. |
| permspace       | bigint                   | Size of the permanent table storage space used by a role     |
| tempspace       | bigint                   | Size of the temporary table storage space used by a role     |
| passwordexpired | smallint                 | Whether a password is valid.<br/>- **0**: The password is valid.<br/>- **1**: The password is invalid. |
