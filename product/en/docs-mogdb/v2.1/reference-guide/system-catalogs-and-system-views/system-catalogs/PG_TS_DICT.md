---
title: PG_TS_DICT
summary: PG_TS_DICT
author: Guo Huan
date: 2021-04-19
---

# PG_TS_DICT

**PG_TS_DICT** contains entries that define text search dictionaries. A dictionary depends on a text search template, which specifies all the implementation functions needed; the dictionary itself provides values for the user-settable parameters supported by the template.

This division of labor allows dictionaries to be created by unprivileged users. The parameters are specified by a text string **dictinitoption**, whose format and meaning vary depending on the template.

**Table 1** PG_TS_DICT columns

| Name           | Type | Reference          | Description                                                |
| :------------- | :--- | :----------------- | :--------------------------------------------------------- |
| oid            | oid  | -                  | Row identifier (hidden attribute, which must be specified) |
| dictname       | name | -                  | Text search dictionary name                                |
| dictnamespace  | oid  | PG_NAMESPACE.oid   | OID of the namespace that contains the dictionary          |
| dictowner      | oid  | PG_AUTHID.oid      | Owner of the dictionary                                    |
| dicttemplate   | oid  | PG_TS_TEMPLATE.oid | OID of the text search template for the dictionary         |
| dictinitoption | text | -                  | Initialization option string for the template              |
