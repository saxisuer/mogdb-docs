---
title: GS_WLM_PLAN_ENCODING_TABLE
summary: GS_WLM_PLAN_ENCODING_TABLE
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_PLAN_ENCODING_TABLE

**GS_WLM_PLAN_ENCODING_TABLE** displays encoding information of the plan operator level and provides the training and prediction sets of label values such as startup time, total time, peak memory, and rows for machine learning models.

**Table 1** GS_WLM_PLAN_ENCODING_TABLE columns

| Name           | Type    | Description                                                  |
| :------------- | :------ | :----------------------------------------------------------- |
| queryid        | bigint  | Internal query ID used for statement execution               |
| plan_node_id   | integer | Plan node ID of the execution plan                           |
| parent_node_id | integer | Parent node ID of the operator                               |
| startup_time   | bignit  | Time when the operator starts to process the first data record |
| total_time     | bigint  | Total execution time of the operator, in ms                  |
| rows           | bigint  | Number of rows executed by the operator                      |
| peak_memory    | integer | Maximum peak memory used by the operator on database nodes, in MB |
| encode         | text    | Encoding information of the operator                         |
