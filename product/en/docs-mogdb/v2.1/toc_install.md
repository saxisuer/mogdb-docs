<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## Installation Guide

+ [Container Installation](/installation-guide/docker-installation/docker-installation.md)
+ Simplified Installation Process
  + [Preparing for Installation](/installation-guide/simplified-installation-process/1-installation-preparation.md)
  + [Installation on a Single Node](/installation-guide/simplified-installation-process/2-installation-on-a-single-node.md)
  + [Installation on Primary/Standby Nodes](/installation-guide/simplified-installation-process/3-installation-on-primary-standby-nodes.md)
+ Standard Installation
  + [Installation Overview](/installation-guide/standard-installation/1-installation-overview.md)
  + Preparing for Installation
    + [Environment Requirement](/installation-guide/standard-installation/2-environment-requirement.md)
    + [Modifying OS Configuration](/installation-guide/standard-installation/3-modifying-os-configuration.md)
  + [Installing MogDB](/installation-guide/standard-installation/4-installing-mogdb.md)
  + [Verifying the Installation](/installation-guide/standard-installation/5-verifying-installation.md)
  + [Recommended Parameter Settings](/installation-guide/standard-installation/7-recommended-parameter-settings.md)
  + [Uninstalling MogDB](/installation-guide/standard-installation/6-uninstallation.md)
+ [Manual Installation](/installation-guide/manual-installation.md)
