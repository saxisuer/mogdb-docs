---
title: MogDB - Safe and Easy-to-Use Commercial Database
summary: MogDB - Safe and Easy-to-Use Commercial Database
author: Liu Xu
date: 2021-03-04
---

# MogDB - Safe and Easy-to-Use Commercial Database

MogDB (En**Mo**tech **o**pen**G**auss **D**ata**B**ase Enterprise Edition) is a commercial database developed by ENMOTECH based on openGauss open source database.

It aims at enterprise users and features high availability, security, automatic operation and maintenance, database appliance, and SQL audit optimization. Its core values lie in such advantages as ease of use, high performance, high availability, and 24/7 enterprise support.

## Core Technologies of MogDB

### Basic Functions Oriented to Application Development

- **Standard SQL**

  MogDB supports standard SQL statements. The SQL standard is an international standard and updated periodically.

- **Standard Development Interfaces**

  Standard ODBC and JDBC interfaces are provided to ensure quick migration of user services to MogDB.

- **Transaction Support**

  Transaction support refers to the system capability of ensuring atomicity, consistency, isolation, and durability (ACID) features of global transactions.

- **Support for Functions and Stored Procedures**

  Functions and stored procedures are important database objects. They encapsulate SQL statement sets used for certain functions so that the statements can be easily invoked.

- **PG Interface Compatibility**

  Compatible with PostgreSQL clients and interfaces.

- **SQL Hints**

  SQL hints are supported, which can override any execution plan and thus improve SQL query performance. In plan hints, you can specify a join order, join, stream, and scan operations, and the number of rows in a result of tuning an execution plan, thereby improving query performance.

- **Copy Interface for Error Tolerance**

  MogDB provides the encapsulated copy error tables for creating functions and allows users to specify error tolerance options when the Copy From statement is used. In this way, errors related to parsing, data format, and character set during the execution of the Copy From statement are recorded in the error table instead of being reported and interrupted. Even if a small amount of data in the target file of Copy From is incorrect, the data can be imported to the database. You can locate and rectify the fault in the error table later.

### Database Security

- **Access Control**

  Access control is to manage users' database access control permissions, including database system permissions and object permissions.

- **Separation of Control and Access Permissions**

  For the system administrator, the control and access permissions on table objects are separated to improve data security of common users and restrict the object access permissions of administrators.

- **Database Encryption Authentication**

  The password encryption method based on the RFC5802 mechanism is used for authentication.

- **Database Audit**

  The password encryption method based on the RFC5802 mechanism is used for authentication.

- **Network Communication Security**

  Audit logs record user operations performed on database startup and stopping, connection, and DDL, DML, and DCL operations. The audit log mechanism enhances the database capability of tracing illegal operations and collecting evidence.

- **Row-Level Access Control**

  The row-level access control feature enables database access control to be accurate to each row of data tables. When different users perform the same SQL query operation, the read results may be different according to the row-level access control policy.

### High Performance

- **CBO Optimizer**

  The MogDB optimizer is a typical optimizer developed from Cost-based Optimization (CBO). By using CBO, the database calculates the number of tuples and the execution cost for each step under each execution plan based on the number of table tuples, column width, null record ratio, and characteristic values, such as distinct, MCV, and HB values, and certain cost calculation methods.

- **Hybrid Row-Column Storage**

  MogDB supports both row-store and column-store models. You can choose a row-store or column-store table as needed.

  Column-store is recommended if a table contains many columns (called a wide table) but its query involves only a few columns. Row-store is recommended if a table contains only a few columns and a query involves most of the columns.

- **Adaptive Compression**

  Currently, mainstream databases usually use the data compression technology. Various compression algorithms are used for different data types. If pieces of data of the same type have different characteristics, their compression algorithms and results will also be different. Adaptive compression chooses the suitable compression algorithm for data based on the data type and characteristics, achieving high performance in compression ratio, import, and query.

- **Partitioning**

  In the MogDB system, data is partitioned horizontally on an instance using a specified policy. This operation splits a table into multiple partitions that are not overlapped.

- **SQL Bypass**

  To accelerate such query, the SQL bypass framework is proposed. After simple mode judgment is performed on such query at the parse layer, the query enters a special execution path and skips the classic execution framework, including operator initialization and execution, expression, and projection. Instead, it directly rewrites a set of simple execution paths and directly invokes storage interfaces, greatly accelerating the execution of simple queries.

- **Kunpeng NUMA Architecture Optimization**

  - Based on the multi-core NUMA architecture of the Kunpeng processor, MogDB optimizes the NUMA architecture to reduce the cross-core memory access latency and maximize the multi-core Kunpeng computing capability. The key technologies include redo log batch insertion, NUMA distribution of hotspot data, and Clog partitions, greatly improving the processing performance of the TP system.
  - Based on the ARMv8.1 architecture used by the Kunpeng chip, MogDB uses the LSE instruction set to implement efficient atomic operations, effectively improving the CPU usage, multi-thread synchronization performance, and XLog write performance.
  - Based on the wider L3 cacheline provided by the Kunpeng chip, MogDB optimizes hotspot data access, effectively improving the cache access hit ratio, reducing the cache consistency maintenance overhead, and greatly improving the overall data access performance of the system.

### High Scalability

**High Concurrency of the Thread Pool**

In the OLTP field, a database needs to process a large quantity of client connections. Therefore, the processing capability in high-concurrency scenarios is one of the important capabilities of the database.

The simplest processing mode for external connections is the per-thread-per-connection mode, in which a user connection generates a thread. This mode features simple processing thanks to its architecture. However, in high-concurrency scenarios, there are too many threads, causing heavy workload in thread switchover and large conflict between the lightweight lock areas of the database. As a result, the performance (throughput) deteriorates sharply and the SLA of user performance cannot be met.

Therefore, a thread resource pooling and reuse technology needs to be used to resolve this problem. The overall design idea of the thread pool technology is to pool thread resources and reuse them among different connections. After the system is started, a fixed number of working threads are started based on the current number of cores or user configuration. A working thread serves one or more connection sessions. In this way, the session and thread are decoupled. The number of worker threads is fixed. Therefore, frequent thread switchover does not occur in case of high concurrency. The database layer schedules and manages sessions.

### HA

- **Primary/Standby**

  To ensure that a fault can be rectified, data needs to be written into multiple copies. Multiple copies are configured for the primary and standby nodes, and logs are used for data synchronization. In this way, MogDB has no data lost when a node is faulty or the system restarts after a stop, meeting the ACID feature requirements. The primary/standby environment supports two modes: primary/standby, and one primary and multiple standbys. In primary/standby mode, if the standby node needs to redo logs, it can be promoted to primary. In the one primary and multiple standbys mode, all standby nodes need to redo logs and can be promoted to primary. The primary/standby mode is mainly used for OLTP systems with general reliability to save storage resources. The one primary and multiple standbys mode provides higher DR capabilities and is suitable for the OLTP system with higher availability transaction processing.

- **Logical Backup**

  MogDB provides the logical backup capability to back up data in user tables to local disk files in text or CSV format and restore the data in homogeneous or heterogeneous databases.

- **Physical backup**

  MogDB provides the physical backup capability to back up data of the entire instance to local disk files in the internal database format, and restore data of the entire instance in a homogeneous database.

- **Point-In-Time Recovery (PITR)**

  PITR uses basic hot backup, write-ahead logs (WALs), and archived WALs for backup and recovery. When replaying a WAL record, you can stop at any point in time, so that there is a snapshot of the consistent database at any point in time. That is, you can restore the database to the state at any time since the backup starts. During recovery, MogDB supports specifying the recovery stop point as TID, time, and LSN.

### Maintainability

**Workload Diagnosis Report**

The workload diagnosis report (WDR) generates a performance report between two different time points based on the system performance snapshot data at two different time points. The report is used to diagnose database kernel performance faults.

WDR depends on the following two components:

- SNAPSHOT: The performance snapshot can be configured to collect a certain amount of performance data from the kernel at a specified interval and store the data in the user tablespace. Any snapshot can be used as a performance baseline for comparison with other snapshots.
- WDR Reporter: This tool analyzes the overall system performance based on two snapshots, calculates the changes of more specific performance indicators between the two time periods, and generates summarized and detailed performance data.

## MogDB Manager

MogDB Manager provides a whole set of enterprise-level database management suite, including installation and deployment, high availability monitoring, troubleshooting, performance monitoring, database migration, backup and restoration management, data synchronization between heterogeneous databases, SQL compatibility analysis, comparative analysis of performance pressure tests,  ultimate data recovery. It helps customers improve database management efficiency in multiple O&M dimensions.
