---
title: Migration FAQs
summary: Migration FAQs
author: Guo Huan
date: 2021-09-26
---

**Q1: Which databases can be migrated to MogDB currently?**

With the heterogeneous database migration tool MTK developed by ENMOTECH, you can migrate data structures and full data of the following databases to MogDB at a high speed. 

* Oracle
* MySQL
* DB2 
* openGauss
* PostgreSQL (Not supported for stored procedures)
* SQL Server (Not supported for partitioned tables)

For more information, visit [MTK official document](http://docs.mogdb.io:8000/zh/mtk/v2.0/overview).