---
title: Product FAQs
summary: Product FAQs
author: Guo Huan
date: 2021-09-26
---

# Product FAQs

**Q1. What is the relationship between ultimate RTO, parallel recovery, and standby node readability?**

The configurations of ultimate RTO, parallel recovery, and standby node readability are as follows:

- Ultimate RTO: **recovery_parse_workers** indicates the number of Xlog threads parsed during parallel recovery. **recovery_parallelism** indicates the actual number of Xlog threads during parallel recovery. When **recovery_parse_workers** is greater than 1, ultimate RTO recovery is implemented. Only when both **recovery_parse_workers** and **recovery_parallelism** are set to a value greater than 1, can parallel recovery be implemented.
- Parallel recovery: **recovery_max_workers** specifies the maximum number of threads for parallel recovery. **recovery_parallelism** specifies the actual number of threads for parallel recovery. If the value of **recovery_max_workers** is greater than 1, parallel recovery is configured. However, only when **recovery_max_workers** and **recovery_parallelism** are set to a value greater than 1, can parallel recovery be implemented.
- Standby node readability: **hot_standby** indicates that a hot standby node can be read during recovery. If the value of **hot_standby** is **true**, the standby node can be read.

Parallel recovery is a file-level parallel redo, and the ultimate RTO is a data block-level parallel recovery. Parallel recovery is compatible with standby node readability, but ultimate RTO is incompatible with standby node readability. During configuration of preceding parameters, code check (with **CheckExtremeRtoGUCConflicts**) is performed. If **recovery_parse_workers** is set to a value greater than 1 and **hot_standby** is set to **true**, an error will be reported.

**Q2. How can I select a candidate primary node for primary/standby switchover when the standby node cannot be read in the ultimate RTO scenario?**

The standby node cannot be read only when the RTO is ultimate. In serial recovery and parallel recovery, the standby node can be read. If the ultimate RTO is used, only the synchronous mode can be configured, and a node is selected randomly as the primary. After the synchronous mode is used, the data on all nodes is the same.

**Q3. What is the function of the template database? Which tables are contained in the template database?**

The template database provides a method of quickly creating a database. When creating a database, you can specify the **TEMPLATE** parameter to create a database by copying a template database.

The template database does not contain any user table. You can view the attributes of the template database in the **PG_DATABASE** system catalog.

**Q4. When MogDB supports physical replication, does the standby node replay logs by page in parallel mode?**

MogDB supports two parallel recovery modes: file-level recovery and page-level recovery.

**Q5: Is MogDB a distributed database?**

No. MogDB supports only primary/standby deployment in standalone mode currently.

MogDB uses the distributed middleware pgpool-II or Shardingsphere to deal with sharding and non-native distributed requests.

**Q6: What are MogDB application scenarios?**

MogDB application scenarios are similar to those of postgreSQL. MogDB can be applied to e-commerce, finance, O2O, and telecom CRM/billing which feature high concurrency and large data volume.

Application scenarios include transactional application scenarios which focus on online transaction processing and IoT application scenarios which focus on industrial monitoring, remote control, smart cities, smart homes, and IoV.

MogDB provides not only basic functions of relational databases but enhanced HA and monitoring functions for enterprise application scenarios and features.

**Q7: Can MogDB be deployed on x86 servers? What are the recommended hardware configurations?**

MogDB can be installed on x86 servers (AMD64 architecture). For details, see [Hardware Requirements](../installation-guide/standard-installation/2-environment-requirement.md)。

**Q8: Can MogDB be deployed on KUNPENG servers? What are the recommended hardware configurations?**

MogDB can be deployed on KUNPENG servers (ARM architecture) with the recommended hardware configurations the same as those of x86 servers.

Actually, the performance of MogDB on KUNPENG servers is better than that on x86 servers. Therefore, you are advised to choose domestic chip servers.

**Q9: Is MogDB free? How is MogDB charged?**

MogDB is not free (openGauss is free). MogDB is designed for enterprise users and is not open to individuals. For details about the pricing rules, please contact the sales personnel for consultation.

**Q10: What does the database compatibility setting mean during MogDB database creation? What is done to data types for being compatible with different databases?**

During MogDB database creation, you can set the DBCOMPATIBILITY parameter to make MogDB compatible with different databases. However, compatibility here refers to only data storage structure compatibility and does not include SQL syntax compatibility.

Currently, you can configure MogDB compatible with Oracle, MySQL, or PostgreSQL. The default setting is that MogDB is compatible with Oracle.

- For the compatibility type of Oracle, the database considers an empty string as null and replace the data type **DATE** with **TIMESTAMP(0) WITHOUT TIME ZONE**.

- For the compatibility type of MySQL, when a string is converted to an integer type, if an illegal value is entered, it will be converted to 0. However, for other compatibility types, an error will be reported.

- For the compatibility type of PostgreSQL, CHAR and VARCHAR use character as the string unit. For other compatibility types, byte is used as the string unit.

  For example, as for UTF-8 character set, CHAR(3) can hold 3 Chinese characters in the compatibility type of PG but only one Chinese character in the other compatibility types.

If you need to create a database compatible with PostgreSQL, the syntax format is "CREATE DATABASE dbname DBCOMPATIBILITY='PG';."

Parameter description:

DBCOMPATIBILITY [ = ] compatibility_type

Specifies the type of the database to be compatible with.

Value range: **A**, **B**, **C**, and **PG**

**A** indicates that MogDB is compatible with Oracle. **B** indicates that MogDB is compatible with MySQL. **C** indicates that MogDB is compatible with Teradata. **PG** indicates that MogDB is compatible with PostgreSQL. However, **C** is not supported currently. Therefore, the values **A**, **B**, and **PG** are commonly used.

**Q11: What are major improvement of MogDB based on traditional PostgreSQL? What are advantages of MogDB?**

For technical implementation:

- most_available_sync (If the synchronous standby node fails, the services of the primary node will not be interrupted.)
- XIDs that cannot be exhausted
- Physical replication slot automatically created in the stream replication environment
- Incremental checkpoints
- Double write

For ecological consideration:

- Modified based on openGauss and dependent on the Huawei R&D team

- Completely autonomous and controllable

- Integrated ecological tools, including PTK, MTK, RWT, MIT, SCA, and BRM

  Additionally, part of tools are being developed.

- Active community and rich materials

**Q12: What is the MogDB performance in terms of the Intel processor?**

| **Test Item**                                            | **Data Volume** | Number of Concurrent Requests | **Average CPU Usage** | **IOPS** | **IO** Delay | **Number of WALs** | **tpmC** | **Test Duration** |
| -------------------------------------------------------- | --------------- | ----------------------------- | --------------------- | -------- | ------------ | ------------------ | -------- | ----------------- |
| Single node                                              | 100 GB          | 500                           | 29.39%                | 6.50K    | 1.94 ms      | 3974               | 520896.3 | 10 minutes        |
| One primary, one synchronous, and one asynchronous nodes | 100 GB          | 500                           | 30.4%                 | 5.31K    | 453.2 us     | 3944               | 519993.5 | 10 minutes        |
| One primary and two synchronous nodes                    | 100 GB          | 500                           | 26.35%                | 7.66K    | 531.9 us     | 3535               | 480842.2 | 10 minutes        |

**Q13: Which OSs can MogDB be deployed on? Is there any compatibility problems?**

MogDB can be deployed on the following OSs:

- ARM:
  - openEuler 20.3LTS (recommended)
  - Kylin OS V10
- x86:
  - openEuler 20.3LTS
  - CentOS 7.6
  - Asianux 7.6

Different OSs and CPU architectures have compatibility problems.

**Q14: What are distinctions between MogDB and Oracle in implementation of MVCC?**

Oracle is based on SCN, stores data in data blocks, uses undo segments in a circular fashion, and supports the flashback function. However, there exists a large number of transaction rollbacks and too old snapshots (ORA-01555).
MogDB is based on transaction ID (TXID), supports row-level storage, does not need undo segments, and supports instantaneous rollback of large transactions. However, there exists block page space and performance overhead problems.

**Q15: What are advantages of MogDB incremental checkpoints?**

Incremental checkpoints can refresh dirty pages in small batches, in stages, or rolling mode, update LSN information, and reclaim unwanted Xlogs, thereby preventing the performance from being affected by the impact on disks caused by fully refreshing dirty pages.

**Q16: How does MogDB deal with XID exhaustion occurred in native PostgreSQL?**

MogDB changes transaction ID from int32 to int64. 64-bit XIDs cannot be exhausted for ever. Expired XIDs also need to be frozen but the risk of XID rollback failure will never occur.

**Q17: What are distinctions between MogDB AND Oracle in user permission?**

Oracle schema and user are the same concept. But they are not the same in MogDB. In MogDB, all objects created by a user are under a specified schema (or namespace). Other users may have or may not have permission to access these objects or even they cannot create objects in the schema. User (or role) are global objects and not defined in a database but in an instance. Schema is created in a specified database which includes database objects.

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/faqs/product-faqs-1.png)

**Q18: What is the difference between double write in MogDB and second commit in MySQL?**

The implementation principle of double write is the same as that of second commit in MySQL. It aims to maintain data consistency and avoid data loss due to damaged data blocks so that data can be recovered.

**Q19: What are distinctions between MogDB primary/standby and Oracle Data Guard?**

The standby database can receive query requests to share the pressure with the primary database in both MogDB and Oracle.

**Q20: Is concurrent physical backup supported? Can physical backup be performed remotely? How can data files be transmitted to the target machine during remote physical backup?**

gs_probackup supports concurrent physical backup. Physical backup can be performed remotely to transmit data files to the target machine by using the archive file or stream protocol.

**Q21: What is the difference when xlog-method is set to stream or fetch during physical backup?**

**xlog-method** indicates the Xlog transmission mode. If you do not set the parameter, the default value is **stream**. The **stream** mode includes all WALs generated during backup. The **fetch** mode collects WALs at the end of backup. Therefore, the value of **wal_keep_segments** needs to be set high enough so that logs will not be removed by the end of the backup. If logs have been removed during log transmission, the backup will fail and become unavailable. The **stream** mode transmits WALs during backup creation, which will start a second connection to a server and transmit WALs during backup. Therefore, the **stream** mode uses a maximum of two connections determined by the **max_wal_senders** parameter. The **stream** mode does not need to store additional WALs only when the client can receive WALs constantly.