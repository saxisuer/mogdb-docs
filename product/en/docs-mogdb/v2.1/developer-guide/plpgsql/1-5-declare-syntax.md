---
title: DECLARE Syntax
summary: DECLARE Syntax
author: Guo Huan
date: 2021-03-04
---

# DECLARE Syntax

## Basic Structure

**Structure**

A PL/SQL block can contain a sub-block which can be placed in any section. The following describes the architecture of a PL/SQL block:

- **DECLARE**: declares variables, types, cursors, and regional stored procedures and functions used in the PL/SQL block.

  ```sql
  DECLARE
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
  > This part is optional if no variable needs to be declared.
  >
  > - An anonymous block may omit the **DECLARE** keyword if no variable needs to be declared.
  > - For a stored procedure, **AS** is used, which is equivalent to **DECLARE**. The **AS** keyword must be reserved even if there is no variable declaration part.

- **EXECUTION**: specifies procedure and SQL statements. It is the main part of a program. Mandatory.

  ```sql
  BEGIN
  ```

- Exception part: processes errors. Optional.

  ```sql
  EXCEPTION
  ```

- End

  ```
  END;
  /
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  > You are not allowed to use consecutive tabs in the PL/SQL block because they may result in an exception when the **gsql** tool is executed with the **-r** parameter specified.

**Category**

PL/SQL blocks are classified into the following types:

- Anonymous block: a dynamic block that can be executed only for once. For details about the syntax, see [Figure 1](#anonymous_block::=).
- Subprogram: a stored procedure, function, operator, or packages stored in a database. A subprogram created in a database can be called by other programs.

## Anonymous Blocks

An anonymous block applies to a script infrequently executed or a one-off activity. An anonymous block is executed in a session and is not stored.

**Syntax**

[Figure 1](#anonymous_block::=) shows the syntax diagrams for an anonymous block.

**Figure 1** anonymous_block::=<a id="anonymous_block::="> </a>

![anonymous_block](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/declare-syntax-1.png)

Details about the syntax diagram are as follows:

- The execute part of an anonymous block starts with a **BEGIN** statement, has a break with an **END** statement, and ends with a semicolon (;). Type a slash (/) and press **Enter** to execute the statement.

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
  > The terminator "/" must be written in an independent row.

- The declaration section includes the variable definition, type, and cursor definition.

- A simplest anonymous block does not execute any commands. At least one statement, even a **NULL** statement, must be presented in any implementation blocks.

## Subprogram

A subprogram stores stored procedures, functions, operators, and advanced packages. A subprogram created in a database can be called by other programs.
