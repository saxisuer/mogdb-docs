---
title: dblink
summary: dblink
author: Guo Huan
date: 2021-11-22
---

# dblink

dblink is a tool that can connect to other MogDB databases in an MogDB database session. The connection parameters supported by dblink are the same as those supported by libpq. For details, see [Connection Characters](../../developer-guide/dev/4-development-based-on-libpq/2-libpq/6-connection-characters.md). By default, MogDB does not compile dblink. The following describes how to compile and use dblink.

## Compiling dblink

Currently, the source code of dblink is stored in the [contrib/dblink](https://gitee.com/opengauss/openGauss-server/tree/master/contrib/dblink) directory. After the MogDB database is compiled and installed, if you need to use the dblink, go to the preceding directory and run the following command to compile and install the dblink:

```bash
make
make install
```

## Common dblink Functions

- Load the dblink extension.

  ```sql
  CREATE EXTENSION dblink;
  ```

- Open a persistent connection to a remote database.

  ```sql
  SELECT dblink_connect(text connstr);
  ```

- Close a persistent connection to a remote database.

  ```sql
  SELECT dblink_disconnect();
  ```

- Query data in a remote database.

  ```sql
  SELECT * FROM dblink(text connstr, text sql);
  ```

- Execute commands in a remote database.

  ```sql
  SELECT dblink_exec(text connstr, text sql);
  ```

- Return the names of all opened dblinks.

  ```sql
  SELECT dblink_get_connections();
  ```

- Send an asynchronous query to a remote database.

  ```sql
  SELECT dblink_send_query(text connname, text sql);
  ```

- Check whether the connection is busy with an asynchronous query.

  ```sql
  SELECT dblink_is_busy(text connname);
  ```

- Delete the extension.

  ```sql
  DROP EXTENSION dblink;
  ```

## Precautions

- Currently, dblink allows only the MogDB database to access another MogDB database and does not allow the MogDB database to access a PostgreSQL database.
- Currently, dblink does not support the thread pool mode.
