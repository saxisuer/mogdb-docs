---
title: Creating a Resource Pool
summary: Creating a Resource Pool
author: Guo Huan
date: 2021-10-22
---

# Creating a Resource Pool

## Background

MogDB creates resource pools to divide host resources. After resource load management is enabled, the default resource pool alone is insufficient to address the resource load management requirements of services. Therefore, new resource pools must be used to reallocate system resources for granular control purposes. Table 1 describes the features of a common resource pool.

**Table 1** Features of a common resource pool

| Resource Pool                          | Feature                                                      |
| :------------------------------------- | :----------------------------------------------------------- |
| Common resource pool (common scenario) | A common resource pool is associated with a Workload Cgroup, and its group resource pool must not be associated with the corresponding sub-Class Cgroup. For example, to create a service resource pool and associate it with **class1:wd**, ensure that its group resource pool has not been associated with **class1**.The default value of **mem_percent** is **0%**. The sum of **mem_percent** for all common resource groups under the same group resource pool can be greater than or equal to 100%. |

After resource load management is enabled, the system automatically creates **default_pool**. If no resource pool is specified for a session or user, they will be automatically associated with **default_pool**. By default, **default_pool** is associated with the **DefaultClass:Medium** Cgroup and does not limit the number of concurrent services. Table 2 describes the attributes of **default_pool**.

**Table 2** default_pool attributes

| Attribute         | Value               | Description                                                  |
| :---------------- | :------------------ | :----------------------------------------------------------- |
| respool_name      | default_pool        | Name of the resource pool                                    |
| mem_percent       | 100                 | Maximum percentage of used memory                            |
| cpu_affinity      | -1                  | CPU affinity (reserved)                                      |
| control_group     | DefaultClass:Medium | Cgroup associated with the resource pool                     |
| active_statements | -1                  | Maximum number of concurrent queries allowed by the resource pool. The value **-1** indicates that the number of concurrent queries is not limited. |
| max_dop           | 1                   | Concurrency level of execution operators after the SMP is enabled (reserved) |
| memory_limit      | 8GB                 | Upper memory usage limit (reserved)                          |
| parentid          | 0                   | OID of the parent resource pool                              |
| io_limits         | 0                   | Upper limit of IOPS. It is counted by ones for column storage and by 10 thousands for row storage. The value **0** indicates there is no limit. |
| io_priority       | None                | I/O priority set for jobs that consume many I/O resources. It takes effect when the I/O usage reaches 90%. **None** indicates there is no control. |
| nodegroup         | installation        | Name of the logical cluster to which the resource pool belongs |
| is_foreign        | f                   | Specifies that the resource pool is not used for users outside the logical cluster. |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** **default_pool** cannot be modified in MogDB.

## Prerequisites

You are familiar with the [CREATE RESOURCE POOL](../../../reference-guide/sql-syntax/CREATE-RESOURCE-POOL.md), [ALTER RESOURCE POOL](../../../reference-guide/sql-syntax/ALTER-RESOURCE-POOL.md), and [DROP RESOURCE POOL](../../../reference-guide/sql-syntax/DROP-RESOURCE-POOL.md) syntax.

## **Procedure**

**Creating a resource pool**

1. Perform the steps in [Using the gsql Client for Connection](../../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md).

2. Create a group resource pool and associate it with the specified sub-Class Cgroup. In the following example, the group resource pool named **resource_pool_a** is associated with the **class_a** Cgroup.

   ```sql
   mogdb=# CREATE RESOURCE POOL resource_pool_a WITH (control_group='class_a');
   mogdb=# CREATE RESOURCE POOL resource_pool_b WITH (control_group='class_b');
   CREATE RESOURCE POOL
   ```

3. Create a service resource pool and associate it with the specified Workload Cgroup. In the following example, the service resource pool named **resource_pool_a1** is associated with the **workload_a1** Cgroup.

   ```sql
   mogdb=# CREATE RESOURCE POOL resource_pool_a1 WITH (control_group='class_a:workload_a1');
   mogdb=# CREATE RESOURCE POOL resource_pool_a2 WITH (control_group='class_a:workload_a2');
   mogdb=# CREATE RESOURCE POOL resource_pool_b1 WITH (control_group='class_b:workload_b1');
   mogdb=# CREATE RESOURCE POOL resource_pool_b2 WITH (control_group='class_b:workload_b2');
   CREATE RESOURCE POOL
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   >
   > - If you do not specify an associated Cgroup when creating a resource pool, the resource pool will be associated with the default Cgroup, which is the Timeshare Cgroup **Medium** under the **DefaultClass** Cgroup.
   > - The value of **control_group** is case-sensitive and must be contained in single quotation marks (").
   > - If a database user specifies the Timeshare string (**Rush**, **High**, **Medium**, or **Low**) in the syntax, for example, **control_group** is set to **High**, the resource pool will be associated with the **High** Timeshare Cgroup under **DefaultClass**.
   > - **control_group** allows you to create a Workload Cgroup, for example, **class1:wd** whose Cgroup level can also be appended, such as **class1:wd:2**. The Cgroup level must be within 1 to 10, but it is not used for Cgroup differentiation. In earlier versions, you can create Workload Cgroups with the same name and differentiate them by their levels. In the latest version, Cgroup names must be unique. If you have created duplicate Workload Cgroups in an earlier version, delete them to avoid confusion.

**Managing resource pools**

Modify resource pool attributes. In the following example, the Cgroup associated with the resource pool **resource_pool_a2** is changed to **class_a:workload_a1** (assuming that **class_a:workload_a1** is not associated with any other resource pools).

```sql
mogdb=# ALTER RESOURCE POOL resource_pool_a2 WITH (control_group="class_a:workload_a1");
ALTER RESOURCE POOL
```

**Deleting a resource pool**

Delete a resource pool. For example, run the following command to delete the resource pool **resource_pool_a2**:

```sql
mogdb=# DROP RESOURCE POOL resource_pool_a2;
DROP RESOURCE POOL
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - The resource pool cannot be deleted if it is associated with a role.
> - In a multi-tenant scenario, deleting a group resource pool also deletes the related service resource pools. A resource pool can be deleted only when it is not associated with any users.

## Viewing Resource Pool Information

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
>
> - Do not use the INSERT, UPDATE, DELETE, and TRUNCATE statements in the **pg_resource_pool** system catalog that manages resource load.
> - Do not modify the **memory_limit** and **cpu_affinity** attributes of a resource pool.

- Run the following command to view the information of all the resource pools of the current cluster:

  ```sql
  mogdb=# SELECT * FROM PG_RESOURCE_POOL;
  ```

  ```sql
     respool_name   | mem_percent | cpu_affinity |    control_group    | active_statements | max_dop | memory_limit | parentid | io_limits | io_priority |  nodegroup   | is_foreign  | max_worker
  ------------------+-------------+--------------+---------------------+-------------------+---------+--------------+----------+-----------+--------------+--------------+------------
  default_pool     |         100 |           -1 | DefaultClass:Medium |                -1 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_a  |          20 |           -1 | class_a             |                10 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_b  |          20 |           -1 | class_b             |                10 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_a1 |          20 |           -1 | class_a:workload_a1 |                10 |       1 | 8GB          |    16970 |         0 | None        | installation | f  |
  resource_pool_a2 |          20 |           -1 | class_a:workload_a2 |                10 |       1 | 8GB          |    16970 |         0 | None        | installation | f  |
  resource_pool_b1 |          20 |           -1 | class_b:workload_b1 |                10 |       1 | 8GB          |    16971 |         0 | None        | installation | f  |
  resource_pool_b2 |          20 |           -1 | class_b:workload_b2 |                10 |       1 | 8GB          |    16971 |         0 | None        | installation | f  |
  (7 rows)
  ```

- View information about Cgroups associated with a resource pool. For details, see **gs_control_group_info(pool text)** in [Statistics Information Functions](../../../reference-guide/functions-and-operators/25-statistics-information-functions.md).

  In the following example, **resource_pool_a1** is the name of the resource pool.

  ```sql
  mogdb=# SELECT * FROM gs_control_group_info('resource_pool_a1');
  ```

  ```sql
          name         |  class  |  workload   | type  | gid | shares | limits | rate | cpucores
  ---------------------+---------+-------------+-------+-----+--------+--------+------+----------
  class_a:workload_a1 | class_a | workload_a1 | DEFWD |  87 |     30 |      0 |    0 | 0-3
  (1 row)
  ```

  **Table 3** gs_control_group_info attributes

  | Attribute | Value               | Description                                             |
  | :-------- | :------------------ | :------------------------------------------------------ |
  | name      | class_a:workload_a1 | Class name and workload name                            |
  | class     | class_a             | Class Cgroup name                                       |
  | workload  | workload_a1         | Workload Cgroup name                                    |
  | type      | DEFWD               | Cgroup type (Top, CLASS, BAKWD, DEFWD, and TSWD)        |
  | gid       | 87                  | Cgroup ID                                               |
  | shares    | 30                  | Percentage of CPU resources to those on the parent node |
  | limits    | 0                   | Percentage of CPU cores to those on the parent node     |
  | rate      | 0                   | Allocation ratio in Timeshare                           |
  | cpucores  | 0-3                 | CPU cores                                               |
