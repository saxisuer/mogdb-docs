---
title: Enabling Resource Load Management
summary: Enabling Resource Load Management
author: Guo Huan
date: 2021-10-22
---

# Enabling Resource Load Management

## Background

This section describes how to configure parameters for resource load management.

## Prerequisites

- In MogDB, you can manage system resources only as a database administrator. Run the following statement to query user permissions:

  ```sql
  mogdb=# SELECT rolname FROM pg_roles WHERE rolsystemadmin = 't';
   rolname
  ---------
   omm
   Jack
  (2 rows)
  ```

- Resource load management can be applied only to users with the login permission. Run the following statement to query user permissions:

  ```sql
  mogdb=# SELECT rolname FROM pg_roles WHERE rolcanlogin = 't';
   rolname
  ---------
   omm
  (1 row)
  ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** If a user's login permission is revoked, the user's resource pool will be changed to **default_pool**. For details about **default_pool**, see Table 2 in [Creating a Resource Pool](creating-a-resource-pool.md).

## Procedure

You can perform the following steps only as a database administrator to enable load management based on the resource pool. The following uses user **omm** as an example.

1. Log in as the OS user **omm** to the primary node of MogDB.

2. Enable the Cgroup function.

   ```bash
   gs_guc reload -Z coordinator -Z datanode -N all -I all -c "enable_control_group=on"
   ```

3. Enable resource pool-based load management.

   ```bash
   gs_guc set -Z coordinator -Z datanode -N all -I all -c "use_workload_manager=on"
   ```

4. Enable resident backend thread control for the database.

   ```bash
   gs_guc set -Z coordinator -Z datanode -N all -I all -c "enable_backend_control=on"
   ```

5. Enable control over the autoVacuumWorker thread among the resident backend threads for the database.

   ```bash
   gs_guc set -Z coordinator -Z datanode -N all -I all -c "enable_vacuum_control=on"
   ```

6. Restart the database for the parameter settings to take effect.

   ```bash
   gs_om -t stop && gs_om -t start
   ```
