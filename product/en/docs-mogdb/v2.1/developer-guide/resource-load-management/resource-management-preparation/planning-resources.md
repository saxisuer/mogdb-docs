---
title: Planning Resources
summary: Planning Resources
author: Guo Huan
date: 2021-10-22
---

# Planning Resources

Before configuring resource load management, plan tenant resources based on service models. After services run for a period of time, you can adjust the configurations based on resource usage.

Assume that two departments in a large enterprise use the same cluster. MogDB puts system resources used by the same department in a group to isolate resources for different departments. Table 1 describes the resource plan.

**Table 1** Tenant resource plan

<table>
    <tr>
        <th>Tenant Name</th>
        <th>Parameter</th>
        <th>Example Value</th>
    </tr>
    <tr>
        <td rowspan=6>Tenant A</td>
        <td>Sub-Class Cgroup</td>
        <td>class_a</td>
    </tr>
    <tr>
        <td>Workload Cgroup</td>
        <td>- workload_a1<br/>- workload_a2</td>
    </tr>
    <tr>
        <td>Group resource pool</td>
        <td>resource_pool_a</td>
    </tr>
    <tr>
        <td>Service resource pool</td>
        <td>- resource_pool_a1<br/>- resource_pool_a2</td>
    </tr>
    <tr>
        <td>Group user</td>
        <td>tenant_a</td>
    </tr>
    <tr>
        <td>Service user</td>
        <td>- tenant_a1<br/>- tenant_a2</td>
    </tr>

</table>

<table>
    <tr>
        <th>Tenant Name</th>
        <th>Parameter</th>
        <th>Example Value</th>
    </tr>
    <tr>
        <td rowspan=6>Tenant B</td>
        <td>Sub-Class Cgroup</td>
        <td>class_b</td>
    </tr>
    <tr>
        <td>Workload Cgroup</td>
        <td>- workload_b1<br/>- workload_b2</td>
    </tr>
    <tr>
        <td>Group resource pool</td>
        <td>resource_pool_b</td>
    </tr>
    <tr>
        <td>Service resource pool</td>
        <td>- resource_pool_b1<br/>- resource_pool_b2</td>
    </tr>
    <tr>
        <td>Group user</td>
        <td>tenant_b</td>
    </tr>
    <tr>
        <td>Service user</td>
        <td>- tenant_b1<br/>- tenant_b2</td>
    </tr>
</table>
