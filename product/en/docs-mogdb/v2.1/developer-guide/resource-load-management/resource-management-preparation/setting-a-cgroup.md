---
title: Setting a Cgroup
summary: Setting a Cgroup
author: Guo Huan
date: 2021-03-04
---

# Setting a Cgroup

## Background

The core of MogDB resource load management is resource pools. The first step of configuring a resource pool is to configure Cgroups in the environment. For details about Cgroup principles, see the product manual corresponding to your OS. For details about MogDB Cgroups, see [Viewing Cgroup Information](#Viewing-Cgroup-Information).

The Class Cgroup is a top-layer Cgroup for database service running. **DefaultClass** is a sub-category of the Class Cgroup and is automatically created when a cluster is deployed. The **Medium** Cgroup under **DefaultClass** contains running jobs that are triggered by the system. Resource configurations of **Medium** cannot be modified, and the jobs running on it are not controlled by resource management. Therefore, you are advised to create sub-Class and Workload Cgroups to control resource allocation.

## Prerequisites

You are familiar with "Server Tools > [gs_cgroup](../../../reference-guide/tool-reference/server-tools/0-gs_cgroup.md)" and "Server Tools > [gs_ssh](../../../reference-guide/tool-reference/server-tools/10-gs_ssh.md)" in Tool Reference.

## Procedure

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - To control all the resources in MogDB, you need to create, update, and delete Cgroups on each node. Use **gs_ssh** (see "Server Tools > gs_ssh" in *Tool Reference*) to run commands in the steps below.
> - A Cgroup must be named as follows:
>   - The names of sub-Class Cgroups and Workload Cgroups cannot contain columns (:).
>   - Cgroups having the same name cannot be created.

**Creating sub-Class and Workload Cgroups**

1. Log in as the OS user **omm** to the primary node of MogDB.

2. Create sub-Class Cgroups **class_a** and **class_b**, and allocate 40% and 20% of Class CPU resources to them, respectively.

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_a -s 40"
   ```

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_b -s 20"
   ```

3. Create Workload Cgroups **workload_a1** and **workload_a2** under **class_a**, and allocate 20% and 60% of **class_a** CPU resources to them, respectively.

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_a -G workload_a1 -g 20 "
   ```

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_a -G workload_a2 -g 60 "
   ```

4. Create Workload Cgroups **workload_b1** and **workload_b2** under **class_b**, and allocate 50% and 40% of **class_b** CPU resources to them, respectively.

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_b -G workload_b1 -g 50 "
   ```

   ```bash
   gs_ssh -c "gs_cgroup -c -S class_b -G workload_b2 -g 40 "
   ```

**Adjusting resource quotas for Cgroups**

1. Change the CPU resource quota for **class_a** to 30%.

   ```bash
   gs_ssh -c "gs_cgroup -u -S class_a -s 30"
   ```

2. Change the CPU resource quota for **workload_a1** under **class_a** to 30% of **class_a** CPU resources.

   ```bash
   gs_ssh -c "gs_cgroup -u -S class_a -G workload_a1 -g 30"
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** After adjustment, CPU resources allocated to **workload_a1** should not be greater than those allocated to **class_a**. The name of a Workload Cgroup cannot be a default name of the Timeshare Cgroup, that is, **Low**, **Medium**, **High**, or **Rush**.

**Deleting a Cgroup**

1. Delete the Cgroup **class_a**.

   ```bash
   gs_ssh -c "gs_cgroup -d  -S class_a"
   ```

   After the command is executed, the Cgroup **class_a** is deleted.

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:** User **root** or a user with the **root** permission can delete the default Cgroups that can be accessed by a common user **username** by specifying **-d** and **-U** **username**. A common user can delete existing Class Cgroups by specifying **-d** and **-S** **classname**.

## Viewing Cgroup Information

1. View Cgroup information in configuration files.

   ```bash
   gs_cgroup -p
   ```

   Cgroup configuration

   ```bash
   gs_cgroup -p

   Top Group information is listed:
   GID:   0 Type: Top    Percent(%): 1000( 50) Name: Root                  Cores: 0-47
   GID:   1 Type: Top    Percent(%):  833( 83) Name: Gaussdb:omm           Cores: 0-20
   GID:   2 Type: Top    Percent(%):  333( 40) Name: Backend               Cores: 0-20
   GID:   3 Type: Top    Percent(%):  499( 60) Name: Class                 Cores: 0-20

   Backend Group information is listed:
   GID:   4 Type: BAKWD  Name: DefaultBackend   TopGID:   2 Percent(%): 266(80) Cores: 0-20
   GID:   5 Type: BAKWD  Name: Vacuum           TopGID:   2 Percent(%):  66(20) Cores: 0-20

   Class Group information is listed:
   GID:  20 Type: CLASS  Name: DefaultClass     TopGID:   3 Percent(%): 166(20) MaxLevel: 1 RemPCT: 100 Cores: 0-20
   GID:  21 Type: CLASS  Name: class1           TopGID:   3 Percent(%): 332(40) MaxLevel: 2 RemPCT:  70 Cores: 0-20

   Workload Group information is listed:
   GID:  86 Type: DEFWD  Name: grp1:2           ClsGID:  21 Percent(%):  99(30) WDLevel:  2 Quota(%): 30 Cores: 0-5

   Timeshare Group information is listed:
   GID: 724 Type: TSWD   Name: Low              Rate: 1
   GID: 725 Type: TSWD   Name: Medium           Rate: 2
   GID: 726 Type: TSWD   Name: High             Rate: 4
   GID: 727 Type: TSWD   Name: Rush             Rate: 8

   Group Exception information is listed:
   GID:  20 Type: EXCEPTION Class: DefaultClass
   PENALTY: QualificationTime=1800 CPUSkewPercent=30

   GID:  21 Type: EXCEPTION Class: class1
   PENALTY: AllCpuTime=100 QualificationTime=2400 CPUSkewPercent=90

   GID:  86 Type: EXCEPTION Group: class1:grp1:2
   ABORT: BlockTime=1200 ElapsedTime=2400
   ```

   Table 1 lists the Cgroup configuration shown in the above example.

   **Table 1** Cgroup configuration

   <table>
       <tr>
           <th>GID</th>
           <th>Type</th>
           <th>Name</th>
           <th>Percentage (%)</th>
           <th>Remarks</th>
       </tr>
       <tr>
           <td>0</td>
           <td rowspan=4>Top Cgroup</td>
           <td>Root</td>
           <td>The number 1000 indicates that the total system resources are divided into 1000 pieces.<br/>The number 50 in the parentheses indicates 50% of I/O resources.<br/>MogDB does not control I/O resources through Cgroups. Therefore, the following Cgroup information is only about CPU.</td>
           <td>-</td>
       </tr>
       <tr>
           <td>1</td>
           <td>Gaussdb:omm</td>
           <td>Only one database program runs in a cluster. The default quota of the Gaussdb:omm Cgroup is 833. That is, the ratio of database programs to non-database programs is 5:1 (833:167).</td>
           <td>-</td>
       </tr>
       <tr>
           <td>2</td>
           <td>Backend</td>
           <td rowspan=2>The number 40 in the parentheses indicates that the Backend Cgroup takes up 40% of the resources of the Gaussdb:dbuser Cgroup. The number 60 in the parentheses indicates that the Class Cgroup takes up 60% of the resources of the Gaussdb:dbuser Cgroup.</td>
           <td>-</td>
       </tr>
       <tr>
           <td>3</td>
           <td>Class</td>
           <td>-</td>
       </tr>
       <tr>
           <td>4</td>
           <td rowspan=2>Backend Cgroup</td>
           <td>DefaultBackend</td>
           <td rowspan=2>The numbers 80 and 20 in the parentheses indicate the percentages of Backend Cgroup resources taken by the DefaultBackend and Vacuum groups, respectively.</td>
           <td rowspan=2>TopGID: GID (2) of the Backend Cgroup in a Top Cgroup</td>
       </tr>
       <tr>
           <td>5</td>
           <td>Vacuum</td>
       </tr>
       <tr>
           <td>20</td>
           <td rowspan=2>Class Cgroup</td>
           <td>DefaultClass</td>
           <td rowspan=2>The number 20 in the parentheses indicates that the DefaultClass Cgroup takes up 20% of the Class Cgroup resources. The number 40 in the parentheses indicates that the class1 Cgroup takes up 40% of the Class Cgroup resources. There are only two Class Cgroups currently. Therefore, the system resource quotas for the Class Cgroups (499) are allocated in the ratio of 20:40 (166:332).</td>
           <td rowspan=2>- TopGID: GID (3) of the Class Cgroups in a Top Cgroup to which the DefaultClass and class1 Cgroups belong.<br/>-
   MaxLevel: maximum number of levels for Workload Cgroups in a Class Cgroup. This parameter is set to 1 for DefaultClass because it has no Workload Cgroups.<br/>-
   RemPCT: percentage of remaining resources in a Class Cgroup after its resources are allocated to Workload Cgroups. For example, the percentage of remaining resources in the class1 Cgroup is 70%.</td>
       </tr>
       <tr>
           <td>21</td>
           <td>class1</td>
       </tr>
       <tr>
           <td>86</td>
           <td>Workload Cgroup</td>
           <td>grp1:2<br/>(This name is composed of the name of a Workload Cgroup and its level in the Class Cgroup. This grp1:2 Cgroup is the first Workload Cgroup under the class1 Cgroup, and its level is 2. Each Class Cgroup contains a maximum of 10 levels of Workload Cgroups.)</td>
           <td>In this example, this Workload Cgroup takes up 30% of class1 Cgroup resources (332 x 30% = 99).</td>
           <td>- ClsGID: GID of the class1 Cgroup to which the Workload Cgroup belongs.<br/>- WDLevel: level of the Workload Cgroup in the corresponding Class Cgroup.</td>
       </tr>
       <tr>
           <td>724</td>
           <td rowspan=4>Timeshare Cgroup</td>
           <td>Low</td>
           <td>-</td>
           <td rowspan=4>Rate: rate of resources allocated to a Timeshare Cgroup. The Low Cgroup has the minimum rate 1 and the Rush Cgroup has the maximum rate 8. The resource rate for Rush:High:Medium:Low is 8:4:2:1 under a Timeshare Cgroup.</td>
       </tr>
       <tr>
           <td>725</td>
           <td>Medium</td>
           <td>-</td>
       </tr>
       <tr>
           <td>726</td>
           <td>High</td>
           <td>-</td>
       </tr>
       <tr>
           <td>727</td>
           <td>Rush</td>
           <td>-</td>
       </tr>
   </table>

2. View the Cgroup tree in the OS.

   Run the following command to query the structure of the Cgroup tree:

   ```bash
   gs_cgroup -P
   ```

   In the command output, **shares** indicates the value of **cpu.shares**, which specifies the dynamic quota of CPU resources in the OS, and **cpus** indicates the value of **cpuset.cpus**, which specifies the dynamic quota of CPUSET resources in the OS (number of cores that a Cgroup can use).

   ```
   Mount Information:
   cpu:/dev/cgroup/cpu
   blkio:/dev/cgroup/blkio
   cpuset:/dev/cgroup/cpuset
   cpuacct:/dev/cgroup/cpuacct

   Group Tree Information:
   - Gaussdb:wangrui (shares: 5120, cpus: 0-20, weight: 1000)
           - Backend (shares: 4096, cpus: 0-20, weight: 400)
                   - Vacuum (shares: 2048, cpus: 0-20, weight: 200)
                   - DefaultBackend (shares: 8192, cpus: 0-20, weight: 800)
           - Class (shares: 6144, cpus: 0-20, weight: 600)
                   - class1 (shares: 4096, cpus: 0-20, weight: 400)
                           - RemainWD:1 (shares: 1000, cpus: 0-20, weight: 100)
                                   - RemainWD:2 (shares: 7000, cpus: 0-20, weight: 700)
                                           - Timeshare (shares: 1024, cpus: 0-20, weight: 500)
                                                   - Rush (shares: 8192, cpus: 0-20, weight: 800)
                                                   - High (shares: 4096, cpus: 0-20, weight: 400)
                                                   - Medium (shares: 2048, cpus: 0-20, weight: 200)
                                                   - Low (shares: 1024, cpus: 0-20, weight: 100)
                                   - grp1:2 (shares: 3000, cpus: 0-5, weight: 300)
                           - TopWD:1 (shares: 9000, cpus: 0-20, weight: 900)
                   - DefaultClass (shares: 2048, cpus: 0-20, weight: 200)
                           - RemainWD:1 (shares: 1000, cpus: 0-20, weight: 100)
                                   - Timeshare (shares: 1024, cpus: 0-20, weight: 500)
                                           - Rush (shares: 8192, cpus: 0-20, weight: 800)
                                           - High (shares: 4096, cpus: 0-20, weight: 400)
                                           - Medium (shares: 2048, cpus: 0-20, weight: 200)
                                           - Low (shares: 1024, cpus: 0-20, weight: 100)
                           - TopWD:1 (shares: 9000, cpus: 0-20, weight: 900)
   ```

3. Obtain the Cgroup configuration in a system view.

   1. Perform the steps in [Using the gsql Client for Connection](../../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md).

   2. Obtain the configuration about all Cgroups in the system.

      ```sql
      mogdb=# SELECT * FROM gs_all_control_group_info;
      ```
