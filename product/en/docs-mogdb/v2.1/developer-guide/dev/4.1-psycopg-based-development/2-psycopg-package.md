---
title: Psycopg Package
summary: Psycopg Package
author: Zhang Cuiping
date: 2021-10-11
---

# Psycopg Package

The psycopg package is obtained from the release package. Its name is **GaussDB-Kernel-VxxxRxxxCxx-OS version number-64bit-Python.tar.gz**.

After the decompression, the following folders are generated:

- **psycopg2**: **psycopg2** library file
- **lib**: **lib** library file