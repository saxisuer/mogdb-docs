---
title: cursor.fetchall()
summary: cursor.fetchall()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.fetchall()

## Function

This method obtains all the (remaining) rows of the query result and returns them as a list of tuples.

## Prototype

```
cursor.fetchall()
```

## Parameter

None

## Return Value

Tuple list, which contains all results of the result set. An empty list is returned when no rows are available.

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).