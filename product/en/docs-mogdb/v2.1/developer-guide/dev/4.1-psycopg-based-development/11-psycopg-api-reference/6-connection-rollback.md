---
title: connection.rollback()
summary: connection.rollback()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.rollback()

## Function

This method rolls back the current pending transaction.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** If you close the connection using **close()** but do not commit the change using **commit()**, an implicit rollback will be performed.

## Prototype

```
connection.rollback()
```

## Parameter

None

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).