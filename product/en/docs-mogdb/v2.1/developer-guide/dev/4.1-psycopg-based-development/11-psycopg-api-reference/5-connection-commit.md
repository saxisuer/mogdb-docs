---
title: connection.commit()
summary: connection.commit()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.commit()

## Function

This method commits the currently pending transaction to the database.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** By default, Psycopg opens a transaction before executing the first command. If **commit()** is not called, the effect of any data operation will be lost.

## Prototype

```
connection.commit()
```

## Parameter

None

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).