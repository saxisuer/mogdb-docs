---
title: connection.close()
summary: connection.close()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.close()

## Function

This method closes the database connection.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **CAUTION:** This method closes the database connection and does not automatically call **commit()**. If you just close the database connection without calling **commit()** first, changes will be lost.

## Prototype

```
connection.close()
```

## Parameter

None

## Return Value

None

## Examples

For details, see [Example: Common Operations](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md).