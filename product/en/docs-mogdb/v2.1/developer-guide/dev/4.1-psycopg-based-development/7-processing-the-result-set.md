---
title: Processing the Result Set
summary: Processing the Result Set
author: Zhang Cuiping
date: 2021-10-11
---

# Processing the Result Set

1. **cursor.fetchone()**: Fetches the next row in a query result set and returns a sequence. If no data is available, null is returned.
2. **cursor.fetchall()**: Fetches all remaining rows in a query result and returns a list. An empty list is returned when no rows are available.