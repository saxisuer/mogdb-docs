---
title: Development Based on libpq
summary: Development Based on libpq
author: Guo Huan
date: 2021-04-27
---

# Development Based on libpq

MogDB does not verify the use of libpq interfaces in application development. You are not advised to use this set of interfaces for application development, because underlying risks probably exist. You can use the ODBC or JDBC interface instead.
