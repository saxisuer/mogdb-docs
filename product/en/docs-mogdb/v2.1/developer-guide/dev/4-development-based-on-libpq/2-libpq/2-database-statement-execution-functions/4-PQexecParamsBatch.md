---
title: PQexecParamsBatch
summary: PQexecParamsBatch
author: Guo Huan
date: 2021-05-17
---

# PQexecParamsBatch

## Function

PQexecParamsBatch is used to run a command to bind batches of parameters.

## Prototype

```
PGresult* PQexecParamsBatch(PGconn* conn,
                            const char* command,
                            int nParams,
                            int nBatch,
                            const Oid* paramTypes,
                            const char* const* paramValues,
                            const int* paramLengths,
                            const int* paramFormats,
                            int resultFormat);
```

## Parameter

**Table 1**

| **Keyword**  | **Parameter Description**           |
| :----------- | :---------------------------------- |
| conn         | Connection handle.                  |
| command      | SQL text string.                    |
| nParams      | Number of parameters to be bound.   |
| nBatch       | Number of batch operations.         |
| paramTypes   | Types of parameters to be bound.    |
| paramValues  | Values of parameters to be bound.   |
| paramLengths | Parameter lengths.                  |
| paramFormats | Parameter formats (text or binary). |
| resultFormat | Result format (text or binary).     |

## Return Value

PGresult pointers
