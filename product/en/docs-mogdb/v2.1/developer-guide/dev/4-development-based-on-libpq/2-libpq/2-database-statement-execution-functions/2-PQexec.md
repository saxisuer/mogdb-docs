---
title: PQexec
summary: PQexec
author: Guo Huan
date: 2021-05-17
---

# PQexec

## Function

PQexec is used to commit a command to the server and wait for the result.

## Prototype

```
PGresult *PQexec(PGconn *conn, const char *command);
```

## Parameter

**Table 1** PQexec parameters

| **Keyword** | **Parameter Description**                                    |
| :---------- | :----------------------------------------------------------- |
| conn        | Points to the object pointer that contains the connection information. |
| command     | Query string to be executed.                                 |

## Return Value

**PGresult** indicates the object pointer that contains the query result.

## Precautions

The PQresultStatus function should be called to check the return value for any errors (including the value of a null pointer, in which **PGRES_FATAL_ERROR** will be returned). The PQerrorMessage function can be called to obtain more information about such errors.

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **NOTICE:**
> The command string can contain multiple SQL commands separated by semicolons (;). Multiple queries sent in a PQexec call are processed in one transaction, unless there are specific BEGIN/COMMIT commands in the query string to divide the string into multiple transactions. Note that the returned PGresult structure describes only the result of the last command executed from the string. If a command fails, the string processing stops and the returned PGresult describes the error condition.

## Example

For details, see Example.
