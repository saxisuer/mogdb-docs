---
title: PQgetvalue
summary: PQgetvalue
author: Guo Huan
date: 2021-05-17
---

# PQgetvalue

## Function

PQgetvalue is used to return a single field value of one row of a PGresult. Row and column numbers start from 0. The caller should not release the result directly. The result will be released when the associated PGresult handle is passed to PQclear.

## Prototype

```
char *PQgetvalue(const PGresult *res,
                 int row_number,
                 int column_number);
```

## Parameter

**Table 1**

| **Keyword**   | **Parameter Description** |
| :------------ | :------------------------ |
| res           | Operation result handle.  |
| row_number    | Number of rows.           |
| column_number | Number of columns.        |

## Return Value

For data in text format, the value returned by PQgetvalue is a null-terminated string representation of the field value.

For binary data, the value is a binary representation determined by the typsend and typreceive functions of the data type.

If this field is left blank, an empty string is returned.

## Example

For details, see Example.
