---
title: java.sql.Driver
summary: java.sql.Driver
author: Guo Huan
date: 2021-05-17
---

# java.sql.Driver

This section describes **java.sql.Driver**, the database driver interface.

**Table 1** Support status for java.sql.Driver

| Method Name                                  | Return Type          | JDBC 4 Is Supported Or Not |
| :------------------------------------------- | :------------------- | :------------------------- |
| acceptsURL(String url)                       | Boolean              | Yes                        |
| connect(String url, Properties info)         | Connection           | Yes                        |
| jdbcCompliant()                              | Boolean              | Yes                        |
| getMajorVersion()                            | int                  | Yes                        |
| getMinorVersion()                            | int                  | Yes                        |
| getParentLogger()                            | Logger               | Yes                        |
| getPropertyInfo(String url, Properties info) | DriverPropertyInfo[] | Yes                        |
