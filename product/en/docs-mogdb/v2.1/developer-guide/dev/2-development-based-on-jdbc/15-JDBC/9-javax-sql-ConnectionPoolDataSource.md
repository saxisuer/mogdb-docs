---
title: javax.sql.ConnectionPoolDataSource
summary: javax.sql.ConnectionPoolDataSource
author: Guo Huan
date: 2021-05-17
---

# javax.sql.ConnectionPoolDataSource

This section describes **javax.sql.ConnectionPoolDataSource**, the interface for data source connection pools.

**Table 1** Support status for javax.sql.ConnectionPoolDataSource

| Method Name                                      | Return Type      | JDBC 4 Is Supported Or Not |
| :----------------------------------------------- | :--------------- | :------------------------- |
| getPooledConnection()                            | PooledConnection | Yes                        |
| getPooledConnection(String user,String password) | PooledConnection | Yes                        |
