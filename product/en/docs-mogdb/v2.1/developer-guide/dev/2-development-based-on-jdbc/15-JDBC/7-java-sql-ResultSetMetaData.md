---
title: java.sql.ResultSetMetaData
summary: java.sql.ResultSetMetaData
author: Guo Huan
date: 2021-05-17
---

# java.sql.ResultSetMetaData

This section describes **java.sql.ResultSetMetaData**, which provides details about ResultSet object information.

**Table 1** Support status for java.sql.ResultSetMetaData

| Method Name                      | Return Type | JDBC 4 Is Supported Or Not |
| :------------------------------- | :---------- | :------------------------- |
| getCatalogName(int column)       | String      | Yes                        |
| getColumnClassName(int column)   | String      | Yes                        |
| getColumnCount()                 | int         | Yes                        |
| getColumnDisplaySize(int column) | int         | Yes                        |
| getColumnLabel(int column)       | String      | Yes                        |
| getColumnName(int column)        | String      | Yes                        |
| getColumnType(int column)        | int         | Yes                        |
| getColumnTypeName(int column)    | String      | Yes                        |
| getPrecision(int column)         | int         | Yes                        |
| getScale(int column)             | int         | Yes                        |
| getSchemaName(int column)        | String      | Yes                        |
| getTableName(int column)         | String      | Yes                        |
| isAutoIncrement(int column)      | boolean     | Yes                        |
| isCaseSensitive(int column)      | boolean     | Yes                        |
| isCurrency(int column)           | boolean     | Yes                        |
| isDefinitelyWritable(int column) | boolean     | Yes                        |
| isNullable(int column)           | int         | Yes                        |
| isReadOnly(int column)           | boolean     | Yes                        |
| isSearchable(int column)         | boolean     | Yes                        |
| isSigned(int column)             | boolean     | Yes                        |
| isWritable(int column)           | boolean     | Yes                        |
