---
title: java.sql.PreparedStatement
summary: java.sql.PreparedStatement
author: Guo Huan
date: 2021-05-17
---

# java.sql.PreparedStatement

This section describes **java.sql.PreparedStatement**, the interface for preparing statements.

**Table 1** Support status for java.sql.PreparedStatement

| Method Name                                                  | Return Type       | JDBC 4 Is Supported Or Not |
| :----------------------------------------------------------- | :---------------- | :------------------------- |
| clearParameters()                                            | void              | Yes                        |
| execute()                                                    | Boolean           | Yes                        |
| executeQuery()                                               | ResultSet         | Yes                        |
| excuteUpdate()                                               | int               | Yes                        |
| executeLargeUpdate()                                         | long              | No                         |
| getMetaData()                                                | ResultSetMetaData | Yes                        |
| getParameterMetaData()                                       | ParameterMetaData | Yes                        |
| setArray(int parameterIndex, Array x)                        | void              | Yes                        |
| setAsciiStream(int parameterIndex, InputStream x, int length) | void              | Yes                        |
| setBinaryStream(int parameterIndex, InputStream x)           | void              | Yes                        |
| setBinaryStream(int parameterIndex, InputStream x, int length) | void              | Yes                        |
| setBinaryStream(int parameterIndex, InputStream x, long length) | void              | Yes                        |
| setBlob(int parameterIndex, InputStream inputStream)         | void              | Yes                        |
| setBlob(int parameterIndex, InputStream inputStream, long length) | void              | Yes                        |
| setBlob(int parameterIndex, Blob x)                          | void              | Yes                        |
| setCharacterStream(int parameterIndex, Reader reader)        | void              | Yes                        |
| setCharacterStream(int parameterIndex, Reader reader, int length) | void              | Yes                        |
| setClob(int parameterIndex, Reader reader)                   | void              | Yes                        |
| setClob(int parameterIndex, Reader reader, long length)      | void              | Yes                        |
| setClob(int parameterIndex, Clob x)                          | void              | Yes                        |
| setDate(int parameterIndex, Date x, Calendar cal)            | void              | Yes                        |
| setNull(int parameterIndex, int sqlType)                     | void              | Yes                        |
| setNull(int parameterIndex, int sqlType, String typeName)    | void              | Yes                        |
| setObject(int parameterIndex, Object x)                      | void              | Yes                        |
| setObject(int parameterIndex, Object x, int targetSqlType)   | void              | Yes                        |
| setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) | void              | Yes                        |
| setSQLXML(int parameterIndex, SQLXML xmlObject)              | void              | Yes                        |
| setTime(int parameterIndex, Time x)                          | void              | Yes                        |
| setTime(int parameterIndex, Time x, Calendar cal)            | void              | Yes                        |
| setTimestamp(int parameterIndex, Timestamp x)                | void              | Yes                        |
| setTimestamp(int parameterIndex, Timestamp x, Calendar cal)  | void              | Yes                        |
| setUnicodeStream(int parameterIndex, InputStream x, int length) | void              | Yes                        |
| setURL(int parameterIndex, URL x)                            | void              | Yes                        |
| setBoolean(int parameterIndex, boolean x)                    | void              | Yes                        |
| setBigDecimal(int parameterIndex, BigDecimal x)              | void              | Yes                        |
| setByte(int parameterIndex, byte x)                          | void              | Yes                        |
| setBytes(int parameterIndex, byte[] x)                       | void              | Yes                        |
| setDate(int parameterIndex, Date x)                          | void              | Yes                        |
| setDouble(int parameterIndex, double x)                      | void              | Yes                        |
| setFloat(int parameterIndex, float x)                        | void              | Yes                        |
| setInt(int parameterIndex, int x)                            | void              | Yes                        |
| setLong(int parameterIndex, long x)                          | void              | Yes                        |
| setShort(int parameterIndex, short x)                        | void              | Yes                        |
| setString(int parameterIndex, String x)                      | void              | Yes                        |
| setNString(int parameterIndex, String x)                     | void              | Yes                        |
| addBatch()                                                   | void              | Yes                        |
| executeBatch()                                               | int[]             | Yes                        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
>
> - Execute addBatch() and execute() only after running clearBatch().
> - Batch is not cleared by calling executeBatch(). Clear batch by explicitly calling clearBatch().
> - After bounded variables of a batch are added, if you want to reuse these values (add a batch again), set*() is not necessary.
> - The following methods are inherited from java.sql.Statement: close, execute, executeQuery, executeUpdate, getConnection, getResultSet, getUpdateCount, isClosed, setMaxRows, and setFetchSize.
> - The **executeLargeUpdate()** method can only be used in JDBC 4.2 or later.
