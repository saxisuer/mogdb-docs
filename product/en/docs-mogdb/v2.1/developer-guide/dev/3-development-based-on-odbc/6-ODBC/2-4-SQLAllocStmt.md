---
title: SQLAllocStmt
summary: SQLAllocStmt
author: Guo Huan
date: 2021-05-17
---

# SQLAllocStmt

In ODBC 3.x, SQLAllocStmt was deprecated and replaced by SQLAllocHandle. For details, see SQLAllocHandle.
