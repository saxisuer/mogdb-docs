---
title: SQLFreeConnect
summary: SQLFreeConnect
author: Guo Huan
date: 2021-05-17
---

# SQLFreeConnect

In ODBC 3.x, SQLFreeConnect (an ODBC 2.x function) was deprecated and replaced by SQLFreeHandle. For details, see SQLFreeHandle.
