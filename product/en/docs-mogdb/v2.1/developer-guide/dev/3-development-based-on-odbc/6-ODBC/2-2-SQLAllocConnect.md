---
title: SQLAllocConnect
summary: SQLAllocConnect
author: Guo Huan
date: 2021-05-17
---

# SQLAllocConnect

In ODBC 3.x, SQLAllocConnect (an ODBC 2.x function) was deprecated and replaced by SQLAllocHandle. For details, see SQLAllocHandle.
