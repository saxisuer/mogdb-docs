---
title: SQLAllocHandle
summary: SQLAllocHandle
author: Guo Huan
date: 2021-05-17
---

# SQLAllocHandle

## Function

SQLAllocHandle is used to allocate environment, connection, statement, or descriptor handles. This function replaces the deprecated ODBC 2.x functions SQLAllocEnv, SQLAllocConnect, and SQLAllocStmt.

## Prototype

```
SQLRETURN SQLAllocHandle(SQLSMALLINT   HandleType,
                         SQLHANDLE     InputHandle,
                         SQLHANDLE     *OutputHandlePtr);
```

## Parameter

**Table 1** SQLAllocHandle parameters

| **Keyword**     | **Parameter Description**                                    |
| :-------------- | :----------------------------------------------------------- |
| HandleType      | Type of handle to be allocated by SQLAllocHandle. The value must be one of the following:<br/>- SQL_HANDLE_ENV (environment handle)<br/>- SQL_HANDLE_DBC (connection handle)<br/>- SQL_HANDLE_STMT (statement handle)<br/>- SQL_HANDLE_DESC (descriptor handle)<br/>The handle application sequence is: **SQL_HANDLE_ENV** &gt; **SQL_HANDLE_DBC** &gt; **SQL_HANDLE_STMT**. The handle applied later depends on the handle applied prior to it. |
| InputHandle     | Existing handle to use as a context for the new handle being allocated.<br/>- If **HandleType** is **SQL_HANDLE_ENV**, this parameter is set to **SQL_NULL_HANDLE**.<br/>- If **HandleType** is **SQL_HANDLE_DBC**, this parameter value must be an environment handle.<br/>- If **HandleType** is **SQL_HANDLE_STMT** or **SQL_HANDLE_DESC**, this parameter value must be a connection handle. |
| OutputHandlePtr | **Output parameter**: Pointer to a buffer that stores the returned handle in the newly allocated data structure. |

## Return Value

- **SQL_SUCCESS** indicates that the call succeeded.
- **SQL_SUCCESS_WITH_INFO** indicates that some warning information is displayed.
- **SQL_ERROR** indicates major errors, such as memory allocation and connection failures.
- **SQL_INVALID_HANDLE** indicates that invalid handles were called. This value may also be returned by other APIs.

## Precautions

If SQLAllocHandle returns **SQL_ERROR** when it is used to allocate a non-environment handle, it sets **OutputHandlePtr** to **SQL_NULL_HDBC**, **SQL_NULL_HSTMT**, or **SQL_NULL_HDESC**. The application can then call SQLGetDiagRec, with **HandleType** and **Handle** set to the value of **IntputHandle**, to obtain the **SQLSTATE** value. The **SQLSTATE** value provides the detailed function calling information.

## Example

See ODBC - Examples.
