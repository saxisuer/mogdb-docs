---
title: ODBC Packages, Dependent Libraries, and Header Files
summary: ODBC Packages, Dependent Libraries, and Header Files
author: Guo Huan
date: 2021-04-26
---

# ODBC Packages, Dependent Libraries, and Header Files

**ODBC Packages for the Linux OS**

Obtain the [**openGauss-x.x.x-ODBC.tar.gz**](https://opengauss.org/en/download.html) package from the release package. In the Linux OS, header files (including **sql.h** and **sqlext.h**) and library (**libodbc.so**) are required in application development. These header files and library can be obtained from the **unixODBC-2.3.0** installation package.
