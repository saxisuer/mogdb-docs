<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## AI Features Guide

+ [Overview](/AI-features/1-AI-features-overview.md)
+ Predictor: AI Query Time Forecasting
  + [Overview](/AI-features/2-predictor-ai-query-time-forecasting/2-1-ai-query-time-forecasting-overview.md)
  + [Environment Deployment](/AI-features/2-predictor-ai-query-time-forecasting/2-2-environment-deployment.md)
  + [Usage Guide](/AI-features/2-predictor-ai-query-time-forecasting/2-3-usage-guide.md)
  + [Best Practices](/AI-features/2-predictor-ai-query-time-forecasting/2-4-best-practices.md)
  + [FAQs](/AI-features/2-predictor-ai-query-time-forecasting/2-5-faqs.md)
+ X-Tuner: Parameter Optimization and Diagnosis
  + [Overview](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-1-x-tuner-overview.md)
  + [Preparations](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-2-preparations.md)
  + [Examples](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-3-examples.md)
  + [Obtaining Help Information](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-4-obtaining-help-information.md)
  + [Command Reference](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-5-command-reference.md)
  + [Troubleshooting](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-6-Troubleshooting.md)
+ SQLdiag: Slow SQL Discovery
  + [Overview](/AI-features/4-sqldiag-slow-sql-discovery/4-1-overview.md)
  + [Usage Guide](/AI-features/4-sqldiag-slow-sql-discovery/4-2-usage-guide.md)
  + [Obtaining Help Information](/AI-features/4-sqldiag-slow-sql-discovery/4-3-obtaining-help-information.md)
  + [Command Reference](/AI-features/4-sqldiag-slow-sql-discovery/4-4-command-reference.md)
  + [Troubleshooting](/AI-features/4-sqldiag-slow-sql-discovery/4-5-troubleshooting.md)
+ A-Detection: Status Monitoring
  + [Overview](/AI-features/5-a-detection-status-monitoring/5-1-overview.md)
  + [Preparations](/AI-features/5-a-detection-status-monitoring/5-2-preparations.md)
  + [Adding Monitoring Parameters](/AI-features/5-a-detection-status-monitoring/5-3-adding-monitoring-parameters.md)
  + [Obtaining Help Information](/AI-features/5-a-detection-status-monitoring/5-4-obtaining-help-information.md)
  + [Examples](/AI-features/5-a-detection-status-monitoring/5-6-examples.md)
  + [Command Reference](/AI-features/5-a-detection-status-monitoring/5-5-command-reference.md)
  + [AI_SERVER](/AI-features/5-a-detection-status-monitoring/5-7-ai-server.md)
  + [AI_MANAGER](/AI-features/5-a-detection-status-monitoring/5-8-ai-manager.md)
+ Index-advisor: Index Recommendation
  + [Single-query Index Recommendation](/AI-features/6-index-advisor-index-recommendation/6-1-single-query-index-recommendation.md)
  + [Virtual Index](/AI-features/6-index-advisor-index-recommendation/6-2-virtual-index.md)
  + [Workload-level Index Recommendation](/AI-features/6-index-advisor-index-recommendation/6-3-workload-level-index-recommendation.md)
+ DeepSQL
  + [Overview](/AI-features/7-deepsql/7-1-overview.md)
  + [Environment Deployment](/AI-features/7-deepsql/7-2-environment-deployment.md)
  + [Usage Guide](/AI-features/7-deepsql/7-3-usage-guide.md)
  + [Best Practices](/AI-features/7-deepsql/7-4-best-practices.md)
  + [Troubleshooting](/AI-features/7-deepsql/7-5-troubleshooting.md)
+ AI-Native Database (DB4AI)
  + [Overview](/AI-features/8-db4ai/8-1-overview.md)
  + [DB4AI-Snapshots for Data Version Management](/AI-features/8-db4ai/8-2-db4ai-snapshots-for-data-version-management.md)
  + [DB4AI-Query for Model Training and Prediction](/AI-features/8-db4ai/8-3-db4ai-query-for-model-training-and-prediction.md)
  + [PL/Python Fenced Mode](/AI-features/8-db4ai/8-4-pl-python-fenced-mode.md)