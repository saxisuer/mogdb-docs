---
title: Command Reference
summary: Command Reference
author: Guo Huan
date: 2021-05-19
---

# Command Reference

**Table 1** Command-line Parameter

| Parameter              | Description                                                  | Value Range            |
| :--------------------- | :----------------------------------------------------------- | :--------------------- |
| mode                   | Specifies the running mode of the tuning program.            | train, tune, recommend |
| -tuner-config-file, -x | Path of the core parameter configuration file of X-Tuner. The default path is **xtuner.conf** under the installation directory. | -                      |
| -db-config-file, -f    | Path of the connection information configuration file used by the optimization program to log in to the database host. If the database connection information is configured in this file, the following database connection information can be omitted. | -                      |
| -db-name               | Specifies the name of a database to be tuned.                | -                      |
| -db-user               | Specifies the user account used to log in to the tuned database. | -                      |
| -port                  | Specifies the database listening port.                       | -                      |
| -host                  | Specifies the host IP address of the database instance.      | -                      |
| -host-user             | Specifies the username for logging in to the host where the database instance is located. The database O&M tools, such as **gsql** and **gs_ctl**, can be found in the environment variables of the username. | -                      |
| -host-ssh-port         | Specifies the SSH port number of the host where the database instance is located. This parameter is optional. The default value is **22**. | -                      |
| -help, -h              | Returns the help information.                                | -                      |
| -version, -v           | Returns the current tool version.                            | -                      |

**Table 2** Parameters in the configuration file

| Parameter             | Description        | Value Range          |
| :-------------------- | :----------------- | :------------------- |
| logfile               | Path for storing generated logs.                             | -                             |
| output_tuning_result  | (Optional) Specifies the path for saving the tuning result.  | -                             |
| verbose               | Whether to print details.                                    | on, off                       |
| recorder_file         | Path for storing logs that record intermediate tuning information. | -                             |
| tune_strategy         | Specifies a strategy used in tune mode.                      | rl, gop, auto                 |
| drop_cache            | Whether to perform drop cache in each iteration. Drop cache can make the benchmark score more stable. If this parameter is enabled, add the login system user to the **/etc/sudoers** list and grant the NOPASSWD permission to the user. (You are advised to enable the NOPASSWD permission temporarily and disable it after the tuning is complete.) | on, off                       |
| used_mem_penalty_term | Penalty coefficient of the total memory used by the database. This parameter is used to prevent performance deterioration caused by unlimited memory usage. The greater the value is, the greater the penalty is. | Recommended value: 0 ~ 1      |
| rl_algorithm          | Specifies the RL algorithm.                                  | ddpg                          |
| rl_model_path         | Path for saving or reading the RL model, including the save directory name and file name prefix. In train mode, this path is used to save the model. In tune mode, this path is used to read the model file. | -                             |
| rl_steps              | Number of training steps of the deep reinforcement learning algorithm | -                             |
| max_episode_steps     | Maximum number of training steps in each episode             | -                             |
| test_episode          | Number of episodes when the RL algorithm is used for optimization | -                             |
| gop_algorithm         | Specifies a global optimization algorithm.                   | bayes, pso, auto              |
| max_iterations        | Maximum number of iterations of the global search algorithm. (The value is not fixed. Multiple iterations may be performed based on the actual requirements.) | -                             |
| particle_nums         | Number of particles when the PSO algorithm is used           | -                             |
| benchmark_script      | Benchmark driver script. This parameter specifies the file with the same name in the benchmark path to be loaded. Typical benchmarks, such as TPC-C and TPC-H, are supported by default. | tpcc, tpch, tpcds, sysbench … |
| benchmark_path        | Path for saving the benchmark script. If this parameter is not configured, the configuration in the benchmark drive script is used. | -                             |
| benchmark_cmd         | Command for starting the benchmark script. If this parameter is not configured, the configuration in the benchmark drive script is used. | -                             |
| benchmark_period      | This parameter is valid only for **period benchmark**. It indicates the test period of the entire benchmark. The unit is second. | -                             |
| scenario              | Type of the workload specified by the user.                  | tp, ap, htap                  |
| tuning_list           | List of parameters to be tuned. For details, see the **share/knobs.json.template** file. | -                             |
