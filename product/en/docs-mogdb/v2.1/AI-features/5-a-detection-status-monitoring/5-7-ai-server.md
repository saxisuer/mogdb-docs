---
title: AI_SERVER
summary: AI_SERVER
author: Guo Huan
date: 2021-10-21
---

# AI_SERVER

AI_SERVER is an independent from the anomaly_detection feature. In addition to the data collection function of the anomaly_detection feature, the collection type, collection item, and data storage mode are added to the AI_SERVER feature. The AI_SERVER feature is used only for data collection and will be integrated into the anomaly_detection feature in the future. This feature includes the server and agent components. The agent must be deployed on the database node for data collection, while the server is deployed on an independent node for data collection and storage.

The data storage modes include **sqlite**, **mongodb**, and **influxdb**.

Table 1 describes the collection items.

**Table 1** Collection items

<table>
    <tr>
        <th>Collection Type</th>
        <th>Collection Item</th>
        <th>Description</th>
    </tr>
    <tr>
        <td rowspan=5>database</td>
        <td>work_mem</td>
        <td>GUC parameter related to the database memory. This parameter is used to check whether the allocated space is sufficient for SQL statements involving sorting tasks.</td>
    </tr>
    <tr>
        <td>shared_buffers</td>
        <td>GUC parameter related to the database memory. Improper setting of shared_buffer will deteriorate the database performance.</td>
    </tr>
    <tr>
        <td>max_connections</td>
        <td>Maximum number of database connections.</td>
    </tr>
    <tr>
        <td>current connections</td>
        <td>Number of current database connections.</td>
    </tr>
    <tr>
        <td>qps</td>
        <td>Database performance metrics:</td>
    </tr>
    <tr>
        <td rowspan=6>os</td>
        <td>cpu usage</td>
        <td>CPU usage.</td>
    </tr>
    <tr>
        <td>memory usage</td>
        <td>Memory usage.</td>
    </tr>
    <tr>
        <td>io wait</td>
        <td>I/O wait event.</td>
    </tr>
    <tr>
        <td>io write</td>
        <td>Data disk write throughput.</td>
    </tr>
    <tr>
        <td>io read</td>
        <td>Data disk read throughput.</td>
    </tr>
    <tr>
        <td>disk used</td>
        <td>Size of the used disk space.</td>
    </tr>
</table>

For details about the deployment mode, see [AI_MANAGER](5-8-ai-manager.md).
