---
title: Overview
summary: Overview
author: Guo Huan
date: 2021-05-19
---

# Overview

anomaly_detection is an AI tool integrated into MogDB. It can be used to collect database metrics, forecast metric trend changes, analyze root causes of slow SQL statements, and detect and diagnose exceptions. It is a component in the DBMind suite.

The information that can be collected consists of **os_exporter**,**database_exporter**, and **wdr**.

- **os_exporter**includes I/O read, I/O write, I/O wait, CPU usage, memory usage, and disk space occupied by the database data directories.

- **database_exporter** includes QPS, some key GUC parameters (**work_mem**,**shared_buffers**, and **max_connections**), temporary database files, external processes, and external connections.

- **wdr** includes the slow SQL text, SQL execution start time, and SQL execution end time.

In terms of exception detection, anomaly_detection can forecast the change trend of multiple metrics such as `IO_Read`, `IO_Write`, `IO_Wait`, `CPU_Usage`, `Memory_Usage`, and `Disk_Space`. When detecting that a metric exceeds the manually set threshold in a certain period or at a certain moment in the future, the tool generates an alarm through logs.

In terms of slow SQL root cause analysis, the tool periodically obtains slow SQL information from WDR reports, diagnoses the root causes of slow SQL statements, and saves the diagnosis result to log files. In addition, the tool supports interactive slow SQL diagnosis, that is, the tool analyzes the root causes of slow SQL statements entered by users, and sends the result to the user.

anomaly_detection consists of the agent and detector modules.

The agent and MogDB database are deployed on the same server. The agent module provides the following functions: 1. Periodically collect database metric data and store the collected data in the buffer queue. 2. Periodically send the data in the buffer queue to the collector submodule of the detector module.

The detector module consists of collector and monitor.

- The collector submodule communicates with the agent module through HTTP or HTTPS, receives data pushed by the agent module, and stores the data locally.
- The monitor submodule forecasts the metric change trend and generates alarms based on the local data. In addition, the monitor submodule analyzes the root cause of slow SQL statements based on related information such as the system and WDR reports.