<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## Performance Tuning

+ System Optimization
  + [Optimizing OS Parameters](/performance-tuning/1-system/1-optimizing-os-parameters.md)
  + [Optimizing Database Parameters](/performance-tuning/1-system/2-optimizing-database-parameters.md)
  + [Configuring SMP](/performance-tuning/1-system/4-configuring-smp.md)
  + [Configuring LLVM](/performance-tuning/1-system/3-configuring-llvm.md)
+ SQL Optimization
  + [Query Execution Process](/performance-tuning/2-sql/1-query-execution-process.md)
  + [Introduction to the SQL Execution Plan](/performance-tuning/2-sql/2-introduction-to-the-sql-execution-plan.md)
  + [Tuning Process](/performance-tuning/2-sql/3-tuning-process.md)
  + [Updating Statistics](/performance-tuning/2-sql/4-updating-statistics.md)
  + [Reviewing and Modifying a Table Definition](/performance-tuning/2-sql/5-reviewing-and-modifying-a-table-definition.md)
  + [Typical SQL Optimization Methods](/performance-tuning/2-sql/6-typical-sql-optimization-methods.md)
  + [Experience in Rewriting SQL Statements](/performance-tuning/2-sql/7-experience-in-rewriting-sql-statements.md)
  + [Resetting Key Parameters During SQL Tuning](/performance-tuning/2-sql/8-resetting-key-parameters-during-sql-tuning.md)
  + [Hint-based Tuning](/performance-tuning/2-sql/9-hint-based-tuning.md)
+ [WDR Snapshot Schema](/performance-tuning/wdr-snapshot-schema.md)
+ [TPCC Performance Tuning Guide](/performance-tuning/TPCC-performance-tuning-guide.md)
