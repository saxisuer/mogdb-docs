---
title: compat-tools
summary: compat-tools
author: Zhang Cuiping
date: 2021-07-14
---

# compat-tools

This project is a set of compatibility tools. It aims to provide compatibility for necessary functions and system views created for OSs migrated from other asynchronous databases to MogDB, thereby facilitating the follow-up system maintenance and application modification.

The script is executed based on the version information. When you execute the script, it will be executed in terms of the following three situations:

1. If the object to be created does not exist in the target database, it will be directly created.
2. If the version of the object to be created is later than that of the object in the target database, the target database will be upgraded and has the object re-created.
3. If the version of the object to be created is earlier than that of the object in the target database, the creation operation will be skipped.

Please refer to [compat-tools repository page](https://gitee.com/enmotech/compat-tools) for details on how to obtain and use component.
