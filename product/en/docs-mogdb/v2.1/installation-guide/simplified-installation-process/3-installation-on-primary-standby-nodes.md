---
title: Installation on Primary/Standby Nodes
summary: Installation on Primary/Standby Nodes
author: Zhang Cuiping
date: 2021-06-15
---

# Installation on Primary/Standby Nodes

This section takes openEuler system as an example to introduce how to install MogDB on primary/standby  nodes.

1. Create directory, user, group, and authorize them.

   ```
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. Decompress the MogDB package to the installation directory.

   ```
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.bz2 -C /opt/software/mogdb/
   ```

3. Go to the **simpleInstall** directory.

   ```
   cd /opt/software/mogdb/simpleInstall
   ```

4. Run the **install.sh -w** command to install MogDB.

   ```
   sh install.sh  -w xxxx  --multinode
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)  **NOTE:**
   >
   > - **-w**: initializes the database password (specified by **gs_initdb**). This parameter is mandatory for security purposes. Make sure that the password complexity requirement is met, including uppercase and lower case letters, characters, and digits.
   > - **-p**: specifies the port number of the primary MogDB node. The default value is **5432**. The port number of the standby node is that of the primary node plus 200. The default port number is **5632**.
   > - **-multinode**: determines whether the installation is performed on a single node or on primary/standby nodes.
   > - **-h|-help**: displays usage instructions.
   > - After the installation, the primary database node name is **nodename1** and the standby database node name is **nodename2**.
   > - The installation path of the primary database node directory is **/opt/software/mogdb/data/master**, and that of the standby node directory is **/opt/software/mogdb/data/slave**, in which **/opt/software/mogdb** is the decompression path, and **data/master(slave)** is the newly created database node directory.

5. After the installation is complete, check whether the process is normal by using **ps** and **gs_ctl**.

   ```
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/master
   ```

   Run the **ps** command to display information similar to the following:

   ```
   omm  4879 11.8 1.1 2082452 373832 pts/0  Sl   14:26   8:29 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/data/master -M primary
   omm  5083  1.1  0.9 1819988 327200 pts/0  Sl   14:26   0:49 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/data/slave -M standby
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```
   
   Run the **gs_ctl** command to display information similar to the following:

   ```
   gs_ctl query ,datadir is /opt/software/mogdb/data/master
   HA state:
       local_role                     : Primary
       static_connections             : 1
       db_state                       : Normal
       detail_information             : Normal
   
   Senders info:
       sender_pid                     : 5165
       local_role                     : Primary
       peer_role                      : Standby
       peer_state                     : Normal
       state                          : Streaming
       sender_sent_location           : 0/4005148
       sender_write_location          : 0/4005148
       sender_flush_location          : 0/4005148
       sender_replay_location         : 0/4005148
       receiver_received_location     : 0/4005148
       receiver_write_location        : 0/4005148
       receiver_flush_location        : 0/4005148
       receiver_replay_location       : 0/4005148
       sync_percent                   : 100%
       sync_state                     : Sync
       sync_priority                  : 1
       sync_most_available            : Off
       channel                        : 10.244.44.52:27001-->10.244.44.52:35912
   
    Receiver info:
   No information
   ```

6. Use MogDB

```
-- Configure PATH
echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
source ~/.bash_profile
-bash: ulimit: open files: cannot modify limit: Operation not permitted
(The default setting fd available size exceeds the system setting, you can ignore the setting)

-- Login to MogDB
gsql -d postgres -p 5432 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

mogdb=#
```