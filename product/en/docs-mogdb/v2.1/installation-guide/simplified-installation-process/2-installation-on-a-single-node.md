---
title: Installation on a Single Node
summary: Installation on a Single Node
author: Zhang Cuiping
date: 2021-06-11

---

# Installation on a Single Node

This section takes openEuler system as an example to introduce how to install MogDB on a single node.

1. Create directory, user, group, and authorize them.

   ```
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. Decompress the MogDB package to the installation directory.

   ```
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.bz2 -C /opt/software/mogdb/
   ```

3. Go to the **simpleInstall** directory.

   ```
   cd /opt/software/mogdb/simpleInstall
   ```

4. Run the **install.sh -w** command to install MogDB.

   ```
   sh install.sh  -w xxxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   >
   > - **-w**: initializes the database password (specified by **gs_initdb**). This parameter is mandatory for security purposes. Make sure that the password complexity requirement is met, including uppercase and lower case letters, characters, and digits.
   > - **-p**: specifies the MogDB port number. If the port number is not specified, the default value **5432** is used.
   > - **-h|-help**: displays usage instructions.
   > - After the installation, the database name is **sgnode**.
   > - The installation path of the database directory is **/opt/software/mogdb/data/single_node**, in which **/opt/software/mogdb** is the decompression path and **data/single_node** is the newly created database node directory.

5. After the installation is complete, check whether the process is normal by using **ps** and **gs_ctl**.

   ```
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/single_node
   ```

   Run the **ps** command to display information similar to the following:

   ```
   omm      24209 11.9  1.0 1852000 355816 pts/0  Sl   01:54   0:33 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/single_node
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   Run the **gs_ctl** command to display information similar to the following:

   ```
   gs_ctl query ,datadir is /opt/software/mogdb/data/single_node
   HA state:
       local_role                     : Normal
       static_connections             : 0
       db_state                       : Normal
       detail_information             : Normal
   
   Senders info:
       No information
   
    Receiver info:
   No information
   ```

6. Use MogDB

```
-- Configure PATH
echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
source ~/.bash_profile
-bash: ulimit: open files: cannot modify limit: Operation not permitted
(The default setting fd available size exceeds the system setting, you can ignore the setting)

-- Login to MogDB
gsql -d postgres -p 5432 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

mogdb=#
```