---
title: Installation Overview
summary: Installation Overview
author: Zhang Cuiping
date: 2021-04-02
---

# Installation Overview

MogDB can be deployed in standalone or in standalone HA mode. In standalone mode, multiple database instances can be deployed on a single host. However, this mode is not recommended for data security purposes. In the standalone HA deployment mode, one primary server and at least one standby server are supported, and a maximum of eight standby servers are supported.
