---
title: Modifying OS Configuration
summary: Modifying OS Configuration
author: Zhang Cuiping
date: 2021-04-2
---

# Modifying OS Configuration

## Disabling the OS Firewall

To ensure that the MogDB can work properly when the firewall is enabled, related services, protocols, IP addresses, and ports need to be added to the firewall whitelist of each host in the MogDB.

Take openEuler OS as an example. Assume that the MogDB information is listed in [Table 1](#mogdb).

**Table 1** Information of MogDB <a id="mogdb"></a>

| Host Name          | Internal IP Address | External IP Address |
| :----------------- | :------------------ | :------------------ |
| plat1              | 192.168.0.11        | 10.10.0.11          |
| plat2              | 192.168.0.12        | 10.10.0.12          |
| plat3              | 192.168.0.13        | 10.10.0.13          |
| plat4              | 192.168.0.14        | 10.10.0.14          |
| Management network | -                   | 10.10.64.236        |

**Procedure**

Currently, EulerOS can be installed only when the firewall is disabled.

1. Set the value of **SELINUX** in the **/etc/selinux/config** file to **disabled**. <a id="1"> </a>
   a. Run the **vim** command to open the **config** file.

   ```bash
   vim /etc/selinux/config
   ```

   b. Change the value of **SELINUX** to **disabled** and run the **:wq** command to save the change and exit.

   ```bash
   SELINUX=disabled
   ```

2. Restart the OS.

   ```bash
   reboot
   ```

3. Check whether the firewall is disabled.

   ```bash
   systemctl status firewalld
   ```

   If the firewall status is **active (running)**, the firewall is not disabled. Go to [4](#4).

   If the firewall status is **inactive (dead)**, you do not need to disable the firewall.

4. Disable the firewall. <a id="4"> </a>

   ```bash
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   ```

5. Repeat steps [1](#1) to [4](#4) on other hosts.

## Setting Character Set Parameters

Set the same character set for all database nodes. You can add **export LANG=Unicode** in the **/etc/profile** file.

```bash
vim /etc/profile
```

## Setting the Time Zone and Time

Set the same time zone for all database nodes by copying the **/etc/localtime** time zone file to the **/usr/share/zoneinfo/** directory.

```bash
cp /usr/share/zoneinfo/$Locale/$Time zone /etc/localtime
```

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
*$Locale/$Time zone* indicates the locale and time zone to be set, for example, **Asia/Shanghai**.

Run the **date -s** command to set the time of each host to the same time. For example:

```bash
date -s "Sat Sep 27 16:00:07 CST 2020"
```

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
You can run the **date** command to query the time zone of the host.

## (Optional) Disabling the Swap Memory

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **Note:** Disabling the swap memory ensures the access performance of the database and prevents the buffer memory of the database from being evicted to the disk. If the server memory is small and the memory is overloaded, you can enable the swap memory to ensure normal running.

Run the **swapoff -a** command on each database node to disable the swap memory.

```bash
swapoff -a
```

## Setting the NIC MTU

Set the NIC MTU value on each database node to the same value. For x86, the recommended MTU value is 1500. For ARM, the recommended MTU value is 8192.

```bash
ifconfig NIC ID mtu Value
```

## Disabling RemoveIPC

On each database node, disable RemoveIPC. For CentOS, skip this step.

**Procedure**

1. Change the value of **RemoveIPC** in the **/etc/systemd/logind.conf** file to **no**. <a id="01"></a>.

   a. Run the **vim** command to open the **logind.conf** file.

   ```bash
   vim  /etc/systemd/logind.conf
   ```

   b. Change the value of **RemoveIPC** to **no**.

   ```bash
   RemoveIPC=no
   ```

2. Change the value of **RemoveIPC** in the **/usr/lib/systemd/system/systemd-logind.service** file to **no**.

   a. Run the **vim** command to open the **systemd-logind.service** file.

   ```bash
   vim /usr/lib/systemd/system/systemd-logind.service
   ```

   b. Change the value of **RemoveIPC** to **no**.

   ```bash
   RemoveIPC=no
   ```

3. Reload configuration parameters.

   ```bash
   systemctl daemon-reload
   systemctl restart systemd-logind
   ```

4. Check whether the modification takes effect. <a id="04"> </a>

   ```bash
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

5. Repeat steps [1](#01) to [4](#04) on other hosts.

## Setting Remote Login of User root

During the MogDB installation, the user **root** is required for remote login. This section describes how to set the user **root** for remote login.

1. Modify the **PermitRootLogin** configuration to enable remote login of user **root**.

   a. Open the **sshd_config** file.

   ```bash
   vim /etc/ssh/sshd_config
   ```

   b. Modify permissions of user **root** using either of the following methods:

   - Comment out **PermitRootLogin no**.

     ```bash
     #PermitRootLogin no
     ```

   - Set the value of **PermitRootLogin** to **yes**.

     ```bash
     PermitRootLogin yes
     ```

   c. Run the **:wq** command to save the modification and exit.

2. Modify the **Banner** configuration to delete the welcome information displayed when you connect to the system. The welcome information affects the return result of remote operations during the installation.

   a. Open the **sshd_config** file.

   ```bash
   vim /etc/ssh/sshd_config
   ```

   b. Comment out the line where **Banner** is located.

   ```bash
   #Banner XXXX
   ```

   c. Run the **:wq** command to save the modification and exit.

3. Run the following command to validate the settings:

   ```bash
   systemctl restart sshd.service
   ```

4. Re-log in to the system as user **root**.

   ```bash
   ssh xxx.xxx.xxx.xxx
   ```

   ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   *xxx.xxx.xxx.xxx* indicates the IP address of the MogDB installation environment.

## Dynamically Disabling Transparent Huge Page

```bash
echo never > /sys/kernel/mm/transparent_hugepage/enabled
```

For example: openEuler

```bash
/root~ # cat /etc/system-release
openEuler release 20.03 (LTS)
/etc/default~ # echo never > /sys/kernel/mm/transparent_hugepage/enabled
/etc/default~ # cat /sys/kernel/mm/transparent_hugepage/enabled
always madvise [never]
```

If the transparent huge page function is successfully disabled, [never] will be displayed.

Disabling the transparent huge page function can reduce the overhead of the host OS to achieve more stable performance.
