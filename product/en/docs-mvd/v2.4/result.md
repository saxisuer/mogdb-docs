---
title: MVD Result
summary: MVD Result
author: hongyedba
date: 2021-09-02
---

# MVD Result

The MVD result is directly shown in the readable text format in a terminal by default. The user can output the MVD result in the JSON or Plain format and output the MVD result in a specified result file. The following sections will use the text format to introduce the meaning of each part in the MVD result.

## Eigenvalue Comparison

Run the following command:

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*'
```

The comparison result is as follows:

```
 >>>> =============================================
>>>> 1. Environments:
>>>> =============================================
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| OPTION             | VALUE                                                                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| SOURCE_DB          | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']                                                                                               |
| TARGET_DB          | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']                                                                                    |
| WORKERS            | 8                                                                                                                                                     |
| INCLUDE            | [['%', 'MTK', '%']]                                                                                                                                   |
| RESULT_FORMAT      | json                                                                                                                                                  |
| FUNCTION_DIMENSION | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}} |
| CATAGORY           | ALL                                                                                                                                                   |
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
+-------------+-------------------+------------------+
| OBJECT_TYPE | SOURCE_EXTRA      | TARGET_EXTRA     |
+-------------+-------------------+------------------+
| TABLE       | MTK.MTK_TEST      |                  |
|             | MTK.MTK_TEST_1    |                  |
|             | MTK.MTK_TEST_2    |                  |
|             | MTK.MTK_TEST_3    |                  |
|             | MTK.MTK_TEST_4    |                  |
| TRIGGER     | MTK.USER_LOG      |                  |
| SEQUENCE    |                   | MTK.SEQ_LOGS_ID  |
|             |                   | MTK.SEQ_USERS_ID |
| PROCEDURE   | MTK.TEST_MTK_DATA |                  |
|             | MTK.TEST_MTK_TEST |                  |
+-------------+-------------------+------------------+

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
+-------+---------------+-----------------+---------+--------+--------+
| OWNER | TABLE         | TYPE            | NAME    | SOURCE | TARGET |
+-------+---------------+-----------------+---------+--------+--------+
| MTK   | TEST_VARCHAR3 | COLUMN_NULLABLE | ID      | Y      | N      |
| MTK   | TEST_VARCHAR3 | CONSTRAINT      | PRIMARY |        | ID     |
| MTK   | TEST_VARCHAR3 | INDEX           |         |        | ID     |
+-------+---------------+-----------------+---------+--------+--------+

>>>> =============================================
>>>> 4. Table Data Differences (Statistics Analysis):
>>>> =============================================
+-------+---------------+--------------+-----------+----------------+----------------+
| OWNER | TABLE         | COLUMN       | DIMENSION |         SOURCE |         TARGET |
+-------+---------------+--------------+-----------+----------------+----------------+
| MTK   | TABLE_NULL    | COL_NOT_NULL | AVG       |              1 | 1.666666666667 |
| MTK   | TABLE_NULL    | COL_NOT_NULL | MIN       |              0 |              1 |
| MTK   | TABLE_NULL    | COL_NULL     | AVG       |            1.5 |              3 |
| MTK   | TABLE_NULL    | COL_NULL     | MIN       |              0 |              3 |
| MTK   | TEST_VARCHAR3 | NAME         | AVG       | 5.166666666667 |              5 |
| MTK   | TEST_VARCHAR3 | NAME         | MIN       |              4 |              3 |
| MTK   | TEST_VARCHAR3 | NAME         | MAX       |              7 |              9 |
+-------+---------------+--------------+-----------+----------------+----------------+

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## MD5 Row-By-Row Comparison

Run the following command:

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*' -R './diff'
```

The comparison result is as follows:

```
 >>>> =============================================
>>>> 1. Environments:
>>>> =============================================
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| OPTION             | VALUE                                                                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| SOURCE_DB          | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']                                                                                               |
| TARGET_DB          | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']                                                                                    |
| WORKERS            | 8                                                                                                                                                     |
| INCLUDE            | [['%', 'MTK', '%']]                                                                                                                                   |
| RESULT_FORMAT      | json                                                                                                                                                  |
| ROW_DIR            | ./diff                                                                                                                                                |
| FUNCTION_DIMENSION | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}} |
| CATAGORY           | ALL                                                                                                                                                   |
+--------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
+-------------+-------------------+------------------+
| OBJECT_TYPE | SOURCE_EXTRA      | TARGET_EXTRA     |
+-------------+-------------------+------------------+
| TABLE       | MTK.MTK_TEST      |                  |
|             | MTK.MTK_TEST_1    |                  |
|             | MTK.MTK_TEST_2    |                  |
|             | MTK.MTK_TEST_3    |                  |
|             | MTK.MTK_TEST_4    |                  |
| TRIGGER     | MTK.USER_LOG      |                  |
| PROCEDURE   | MTK.TEST_MTK_DATA |                  |
|             | MTK.TEST_MTK_TEST |                  |
| SEQUENCE    |                   | MTK.SEQ_LOGS_ID  |
|             |                   | MTK.SEQ_USERS_ID |
+-------------+-------------------+------------------+

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
+-------+---------------+-----------------+---------+--------+--------+
| OWNER | TABLE         | TYPE            | NAME    | SOURCE | TARGET |
+-------+---------------+-----------------+---------+--------+--------+
| MTK   | TEST_VARCHAR3 | COLUMN_NULLABLE | ID      | Y      | N      |
| MTK   | TEST_VARCHAR3 | CONSTRAINT      | PRIMARY |        | ID     |
| MTK   | TEST_VARCHAR3 | INDEX           |         |        | ID     |
+-------+---------------+-----------------+---------+--------+--------+

>>>> =============================================
>>>> 4. Table Data Differences (MD5 ROW-BY-ROW):
>>>> =============================================
+-------+---------------+-------------+-------------+-------------+-------------+
| OWNER | TABLE         | SOURCE_ROWS | TARGET_ROWS | SOURCE_DIFF | TARGET_DIFF |
+-------+---------------+-------------+-------------+-------------+-------------+
| MTK   | TABLE_NULL    |           3 |           3 |           2 |           2 |
| MTK   | TEST_VARCHAR3 |           6 |           6 |           1 |           1 |
+-------+---------------+-------------+-------------+-------------+-------------+

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## Environment

Environment parameters and variables show the program parameters and variables during running.

## Object Difference

The object difference result obtained based on the source and target databases can be categorized into three types shown in three columns.

- **OBJECT_TYPE**: indicates the type of a difference object.
- **SOURCE_EXTRA**: indicates the extra objects of a source database, which are displayed in a list. Each row contains an object, and the list is ordered by **NAME**.
- **TARGET_EXTRA**: indicates the extra objects of a target database, which are displayed in a list. Each row contains an object, and the list is ordered by **NAME**.

## Table Structure Difference

The table structure difference table shows each difference item. The table structure difference result can be categorized into five types shown in five columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **TYPE**: indicates the difference type, including **COLUMN_COUNT**, **COLUMN**, **COLUMN_POSITION**, **COLUMN_DATATYPE**, **COLUMN_NULLABLE**, **CONSTRAINT**, and **INDEX**.
- **NAME**: indicates the name of a difference. The name of a difference varies depending on the difference category.
- **SOURCE**: indicates the difference value of a source database. Difference values vary from type to type.
- **TARGET**: indicates the difference value of a target database. Difference values vary from type to type.

**Note**：

For constraints with different names, it is not considered that they are different from each other. Two constraints can be considered the same only when their field names, positions, and sequences are consistent or the information about whether to use a function is consistent. This rule also applies to indexes.

For example,  the following two indexes are considered the same.

```sql
-- Oracle
create index idx_test_pk_oracle on hongye.test(id);
-- MogDB
create index idx_test_pk_mogdb on hongye.test(id);
```

## Table Data Difference

Only data with difference is displayed. Data difference can be grouped into two types according to the comparison mode, including eigenvalue comparison and MD5 row-by-row comparison.

- Eigenvalue comparison (statistics analysis)

- MD5 row-by-row comparison

The eigenvalue comparison result can be categorized into six types shown in six columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **COLUMN_NAME**: indicates the name of a field.
- **DIMENSION**: indicates the statistical dimension. By default, the statistical dimension is COUNT + AVG + CORR for primary key tables. The statistical dimension is COUNT + AVG + MIN + MAX + MEDIAN for non-primary key tables.
- **SOURCE_VALUE**: indicates the statistical value in the source database.
- **TARGET_VALUE**: indicates the statistical value in the target database.

The MD5 row-by-row comparison result can be categorized into six types shown in four columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **SOURCE_ROWS**: indicates the number of total rows of a source database.
- **TARGET_ROWS**: indicates the number of total rows of a target database.
- **SOURCE_DIFF**: indicates the number of difference rows in a source database (It refers to the number of rows that exist in a source database and are different from those in a target database.)
- **TARGET_DIFF**: indicates the number of difference rows in a target database (It refers to the number of rows that exist in a target database and are different from those in a source database.)
