---
title: Release Notes
summary: Release Notes
author: Zhang Cuiping
date: 2021-08-30
---

# Release Notes

## v3.3.1 (2022-11-25)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_macos_x86_64)

### Features

- Support customize precision of float,double and fraction, max fraction in informix adjust to 5

### Improvements

- Optimizing MySQL Connections: try to connect in two method, default auth-plugin and 'mysql_native_password' auth-plugin
- Optimizing the logical of packing scripts, reduce the amount of external lib files and the size of executable program
- Consider float/double precision in column statistics comparasion

### Bugfixs

- Fix problem: missing one column in md5 hash calculation when compare primary-key table in Informix
- Fix problem: data type of money direct convert to string with dollar sign
- Fix problem: cannot recognize varchar and tinyint data type in MySQL data comparasion
- Fix problem: concat null returns null in openGauss with dbcompatibility=B
- Fix problem: coalesce(null::int, '') returns 0 in openGauss with dbcompatibility=B

## v3.3.0 (2022-11-21)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_macos_x86_64)

### Features

- Support Informix: basic object comparasion and md5 comparasion for table row data

### Improvements

- Support boolean column type in data comparasion for openGauss

### Bugfixs

- Fix row value assignment error in data comparasion for openGauss
- Repair the problem that openGauss data comparison does not recognize the text field

## v3.2.0 (2022-11-03)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_macos_x86_64)

### Features

- Support SQL SERVER: basic object comparasion and md5 comparasion for table row data
- Support receive process info and result by callback interface

### Improvements

- Tuning Oracle library initial logical in macOS
- Consider customer column name mapping in data structure comparasion
- Support large sequence objects in openGauss

### Bugfixs

- Fix error in data handling when column data type is money
- Fix error in statistics comparasion when column-list options does not set
- Fix MySQL driver error: ImportError: No localization support for language 'eng'

## v3.1.1 (2022-10-12)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_macos_x86_64)

### Bugfixs

- Fix the problem caused by splicing empty strings in MogDB/openGauss/MySQL, compatible with mariadb

## v3.1.0 (2022-10-10)

### Improvements

- License version upgrade to v31, older license file does not supported

### Bugfixs

- Fix the problem caused by splicing empty strings in MogDB/openGauss when comparing primary key tables

## v3.0.1 (2022-09-23)

### Features

- Add "--upgrade" option to support command self-upgrade
- Support comparison for PostgreSQL
- Support remap for schema and table name in single table mode (with "-T" option used)
- Support remap and filter for columns in single table mode (with "-T" option used)

### Improvements

- Query optimization for Oracle table without primary key
- Data query sorting rules adjustment (with primary key sorted by primary key, without primary key sorted by MD5 value)

### Bugfixs

- Repair the query error caused by multi-byte characters in Oracle fields under some scenarios

## v3.0.0 (2022-06-09)

### Features

- Add options: "-L", "--license", "--apply-licnese", used for apply and verify license
- Add option: "--column-list", support partial column comparion

### Improvements

- Use "--debug-md5" option can output primary key in source and target database
- When comparing with statistics method, filter columns before query
- Adjust the display of empty characters in object names (use double quotes to wrap them)
- Support for MySQL with object names without space compared to other databases with space in name
- Add debug information for openGauss SQL executions

### Bugfixs

- Fix "NULL" (None in python) value returns when comparision with primary key
- Remove "public" schema out of system schema list in openGauss

## v2.4.8 (2022-04-22)

### Feature

- When comparing objects, MogDB adds Package objects (2.1 and later versions) and SYNONYM objects comparison
- Add recognition of OS local variables (TMPDIR, TMP) for storing temporary data during program runtime when comparing Oracle/DB2 databases
- Add -Z option to manually specify the time zone used for data comparison in different databases at runtime

### Function Optimization

- Fine-tuned final results presentation for better readability
- Optimize the output of the --debug-md5 option during data comparison to provide a more detailed view of the bilateral data comparison process
- Optimize -T mode to support -r specified Schema mapping even in single table comparison mode

### Bugfix

- Fix the issue of abnormal comparison results caused by different sorting rules of primary key tables in openGauss/MogDB when querying
- Fix the issue of program error caused by the failure of primary key identification in the comparison of non-primary key table.
- Fix the issue of inconsistent sorting data due to mixed case of primary key field names in different databases ("TID").
- Fix the issue of inconsistent data in the unilateral database which cannot be correctly identified.
- Fix the issue that the comparison result of primary key table is incorrect due to the space at the end of the primary key field.
- Fix the issue that the query field appears several times in Oracle/DB2 (primary key table), which leads to the query error.
- Fix the issue of incorrect data comparison caused by trailing space of char type in MySQL database.

## v2.4.0 (2021-12-20)

### Feature

- Add the category option (-C/--catagory): supports object structure comparison only and data comparison only.
- Add the debug option (--debug-md5): outputs the original value of the Python MD5 computing result during data comparison.
- Add the schema remapping option (--remap-schema): supports data comparison even if the name of a schema in a source or target database is changed.

### Function Optimization

- Optimize the data comparison logic: For tables with primary keys, comparison is implemented by primary key. Additionally, the values of the primary keys and the MD5 computing results of the source and target databases will be output in the result.
- Optimize the output result: The display of the object structure comparison result and data comparison result is rewritten, thereby facilitating reading and understanding.
- Optimize the DB2 connection mode: During DB2 database connection, more authentication methods can be used (SERVER_ENCRYPT, SERVER, CLIENT, and default).

### Bugfix

- Fix the data comparison failure caused by invisible characters chr(0), and add the -z/--zero-char option for specifying the conversion character of chr(0) during migration.
- Fix the issue that the numeral modulo operator is unavailable in some versions during restoration of the DB2 database.

## v2.3.2 (2021-11-08)

### Bugfix

- Resolve the table constraint comparison error in MySQL earlier than 8.0. Therefore, MySQL 5.5, 5.7, and 8.0 are supported.
- Resolve the problem that the data types of return values in the Python driver for the fields in MySQL of different versions are different.

## v2.3.1

### Bugfix

- Resolve the error that the program is abnormal because the return value of the driver is **str/byte** for the MySQL query field type in some versions.
- Resolve the error that the return value of the **lsb_release -a** command is incorrect when the MySQL driver is to obtain the OS version in SUSUE.

## v2.3.0

### Feature

- Support the sha256 authentication method for openGauss/MogDB in Linux.

## v2.2.14
