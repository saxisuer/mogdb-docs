---
title: Introduction to MVD
summary: Introduction to MVD
author: hongyedba
date: 2021-09-02
---

# Introduction to MVD

MVD is a tool used for comparing heterogeneous databases. After data migration is complete between heterogeneous databases or data synchronization is finished, MVD can check difference between the source and target databases.

MVD mainly supports the following features:

1. **Object structure comparison**: Compare the difference between objects, table structures (including the field type and name), constraints, and indexes in the source and target databases.
2. **Data feature comparison**: Compare the statistical eigenvalues of each field in tables, including relevance, average value, maximum value, minimum value, and median.
3. **Precise recognition of difference**: Precisely recognize table data difference and record the rows that involve data inconsistency between the source and target databases.

## Supported Operating Systems and Platforms

MVD supports the following operating systems and platforms:

1. Linux x86_64
2. Linux arm64 (Because the ARM platform does not have a DB2 client, it does not support DB2 used as a source or target database.)
3. MacOS

## Supported Databases

MVD supports the following source or target database types:

1. Oracle &gt;= 11.2
2. MogDB/openGauss &gt;= 1.0
3. MySQL &gt;= 5.5
4. DB2 &gt;= 11.5
5. Informix &gt;= 14.10
6. SQL Server &gt;= 16.0

**Note**:  Whether MVD supports a platform does not have relevance to whether MVD supports a database. A database running on other platforms, such as Windows/AIX can be supported by MVD only when the the host network and port are connected between the database and MVD.
