---
title: Release Notes
summary: Release Notes
author: Zhang Cuiping
date: 2021-08-30
---

# Release Notes

## v2.3.2 (2021-11-08)

### Bugfix

- Resolve the table constraint comparison error in MySQL earlier than 8.0. Therefore, MySQL 5.5, 5.7, and 8.0 are supported.
- Resolve the problem that the data types of return values in the Python driver for the fields in MySQL of different versions are different.

## v2.3.1

### Bugfix

- Resolve the error that the program is abnormal because the return value of the driver is **str/byte** for the MySQL query field type in some versions.
- Resolve the error that the return value of the **lsb_release -a** command is incorrect when the MySQL driver is to obtain the OS version in SUSUE.

## v2.3.0

### Feature

- Support the sha256 authentication method for openGauss/MogDB in Linux.

## v2.2.14
