---
title: MVD Result
summary: MVD Result
author: hongyedba
date: 2021-09-02
---

# MVD Result

The MVD result is directly shown in the readable text format in a terminal by default. The user can output the MVD result in the JSON or Plain format and output the MVD result in a specified result file. The following sections will use the text format to introduce the meaning of each part in the MVD result.

## Eigenvalue Comparison

Run the following command:

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*'
```

The comparison result is as follows:

```
>>>> =============================================
>>>> 1. Environments:
>>>> =============================================
 OPTION               | VALUE
 ---------------------+--------------------------------------------------------
 SOURCE_DB            | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']
 TARGET_DB            | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']
 WORKERS              | 8
 INCLUDE              | [['%', 'mtk', '%']]
 RESULT_FORMAT        | json
 FUNCTION_DIMENSION   | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}}

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
 OBJECT_TYPE               | EXTRA_TYPE      | DIFFERENCE
 --------------------------+-----------------+------------------------------------------------------------------
 TABLE                     | SOURCE_EXTRA    | ['MTK.TAB_PART_RANGE_HASH', 'MTK.TEST_PK_UK', 'MTK.TAB_PART_KEY_2', 'MTK.TAB_PART_HASH', 'MTK.TAB_PART_RANGE']
 PROCEDURE                 | SOURCE_EXTRA    | ['MTK.TEST_MTK_DATA', 'MTK.TEST_MTK_TEST']
 TRIGGER                   | SOURCE_EXTRA    | ['MTK.USER_LOG']
 SEQUENCE                  | TARGET_EXTRA    | ['MTK.LOGS_ID_SEQ', 'MTK.USERS_ID_SEQ']

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
 OWNER | TABLE_NAME         | TYPE             | NAME                      | DIFFERENCE
 ------+--------------------+------------------+---------------------------+------------------------------------------------------------------
 MTK   | TAB_PART_KEY_1     | CONSTRAINT       | PRIMARY                   | COMMON: []
 MTK   | TAB_PART_KEY_1     |                  |                           | SOURCE_EXTRA: []
 MTK   | TAB_PART_KEY_1     |                  |                           | TARGET_EXTRA: ['ID']

>>>> =============================================
>>>> 4. Table Data Differences (Statistics Analysis):
>>>> =============================================
 OWNER | TABLE_NAME | COLUMN_NAME  | DIMENSION |      SOURCE_VALUE |       TARGET_VALUE
 ------+------------+--------------+-----------+-------------------+-------------------
 MTK   | TABLE_NULL | CNT          | ROW       |                 3 |                  0
 MTK   | MTK_TEST_2 | COL_DOUBLE_2 | AVG       | 501.2637835427314 | 501.26378354273165
 MTK   | MTK_TEST_2 | COL_DOUBLE   | AVG       | 499.1979752563913 |  499.1979752563916
 MTK   | MTK_TEST_1 | COL_DOUBLE   | AVG       | 498.8954055279028 |  498.8954055279032
 MTK   | MTK_TEST_4 | COL_DOUBLE_2 | AVG       | 501.5950313959741 |  501.5950313959745
 MTK   | MTK_TEST_4 | COL_DOUBLE   | AVG       | 503.2131632758452 |  503.2131632758458

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## MD5 Row-By-Row Comparison

Run the following command:

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*' -R './diff'
```

The comparison result is as follows:

```
>>>> =============================================
>>>> 1. Environments:
>>>> =============================================
 OPTION               | VALUE
 ---------------------+--------------------------------------------------------
 SOURCE_DB            | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']
 TARGET_DB            | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']
 WORKERS              | 8
 INCLUDE              | [['%', 'mtk', '%']]
 RESULT_FORMAT        | json
 ROW_DIR              | /Users/hongyedba/Desktop/mysql
 FUNCTION_DIMENSION   | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}}

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
 OBJECT_TYPE               | EXTRA_TYPE      | DIFFERENCE
 --------------------------+-----------------+------------------------------------------------------------------
 PROCEDURE                 | SOURCE_EXTRA    | ['MTK.TEST_MTK_DATA', 'MTK.TEST_MTK_TEST']
 TRIGGER                   | SOURCE_EXTRA    | ['MTK.USER_LOG']
 TABLE                     | SOURCE_EXTRA    | ['MTK.TAB_PART_RANGE', 'MTK.TAB_PART_RANGE_HASH', 'MTK.TAB_PART_KEY_2', 'MTK.TEST_PK_UK', 'MTK.TAB_PART_HASH']
 SEQUENCE                  | TARGET_EXTRA    | ['MTK.LOGS_ID_SEQ', 'MTK.USERS_ID_SEQ']

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
 OWNER | TABLE_NAME         | TYPE             | NAME                      | DIFFERENCE
 ------+--------------------+------------------+---------------------------+------------------------------------------------------------------
 MTK   | TAB_PART_KEY_1     | CONSTRAINT       | PRIMARY                   | COMMON: []
 MTK   | TAB_PART_KEY_1     |                  |                           | SOURCE_EXTRA: []
 MTK   | TAB_PART_KEY_1     |                  |                           | TARGET_EXTRA: ['ID']

>>>> =============================================
>>>> 4. Table Data Differences (MD5 ROW-BY-ROW):
>>>> =============================================
 OWNER | TABLE_NAME | SOURCE_EXTRA | TARGET_EXTRA
 ------+------------+--------------+-------------
 MTK   | TABLE_NULL |       3 of 3 |       0 of 0
 MTK   | MTK_TEST_3 | 158 of 20000 | 158 of 20000
 MTK   | MTK_TEST_1 | 153 of 20000 | 153 of 20000
 MTK   | MTK_TEST_4 | 165 of 20000 | 165 of 20000
 MTK   | MTK_TEST_2 | 166 of 20000 | 166 of 20000

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## Environment

Environment parameters and variables show the program parameters and variables during running.

## Object Difference

The object difference result obtained based on the source and target databases can be categorized into three types shown in three columns.

- **OBJECT_TYPE**: indicates the type of a difference object.
- **EXTRA_TYPE**: indicates the difference type, including `TARGET_EXTRA` and `SOURCE_EXTRA`. `TARGET_EXTRA` indicates the extra objects in the target database. `SOURCE_EXTRA` indicates the extra objects in the source database.
- **DIFFERENCE**: indicates the list of difference objects. Each element in the list is in the &lt;SCHEMA&gt;.&lt;OBJECT_NAME &gt; format.

## Table Structure Difference

The table structure difference table shows each difference item. The table structure difference result can be categorized into five types shown in five columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **TYPE**: indicates the difference type, including **COLUMN_COUNT**, **COLUMN**, **COLUMN_POSITION**, **COLUMN_DATATYPE**, **COLUMN_NULLABLE**, **CONSTRAINT**, and **INDEX**.
- **NAME**: indicates the name of a difference. The name of a difference varies depending on the difference category.
- **DIFFERENCE**: indicates the difference item. The format of a difference item varies depending on the difference category.

The meanings of difference names and items under each difference category are as follows.

| Difference Category | Difference Name | Difference Item                                              |
| ------------------- | --------------- | ------------------------------------------------------------ |
| COLUMN_COUNT        | None            | Format: SOURCE: {n}, TARGET: {m}<br />n and m indicate the number of fields in the source and target databases, respectively. |
| COLUMN              | None            | Format: COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />x indicates the field that exists in both the source and target databases. y indicates the extra field in the source database. z indicates the extra field in the target database. |
| COLUMN_POSITION     | Field name      | Format: SOURCE: {n}, TARGET: {m}<br />n and m indicate the positions of a field in the source and target databases, respectively. |
| COLUMN_DATATYPE     | Field name      | Format: SOURCE: {n}, TARGET: {m}<br />n and m indicate the data types of a field in the source and target databases, respectively. |
| COLUMN_NULLABLE     | Field name      | Format: SOURCE: {n}, TARGET: {m}<br />n and m indicate whether the values of a field are null in the source and target databases, respectively. |
| CONSTRAINT          | Constraint type | Format: COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />x indicates the constraint that exists in both the source and target databases. y indicates the extra constraint in the source database. z indicates the extra constraint in the target database. |
| INDEX               | None            | Format: COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />x indicates the index that exists in both the source and target databases. y indicates the extra index in the source database. z indicates the extra index in the target database. |

**Note**：

- For constraints with different names, it is not considered that they are different from each other. Two constraints can be considered the same only when their field names, positions, and sequences are consistent or the information about whether to use a function is consistent. This rule also applies to indexes.

  For example,  the following two indexes are considered the same.

```sql
-- Oracle
create index idx_test_pk_oracle on hongye.test(id);
-- MogDB
create index idx_test_pk_mogdb on hongye.test(id);
```

## Table Data Difference

Only data with difference is displayed. Data difference can be grouped into two types according to the comparison mode, including eigenvalue comparison and MD5 row-by-row comparison.

- Eigenvalue comparison (statistics analysis)

- MD5 row-by-row comparison

The eigenvalue comparison result can be categorized into six types shown in six columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **COLUMN_NAME**: indicates the name of a field.
- **DIMENSION**: indicates the statistical dimension. By default, the statistical dimension is COUNT + AVG + CORR for primary key tables. The statistical dimension is COUNT + AVG + MIN + MAX + MEDIAN for non-primary key tables.
- **SOURCE_VALUE**: indicates the statistical value in the source database.
- **TARGET_VALUE**: indicates the statistical value in the target database.

The MD5 row-by-row comparison result can be categorized into four types shown in four columns.

- **OWNER**: indicates the table schema.
- **TABLE_NAME**: indicates the name of a table.
- **SOURCE_EXTRA**: indicates the number of rows owned by only the source database and the total number of rows in the source database.
- **TARGET_EXTRA**: indicates the number of rows owned by only the target database and the total number of rows in the target database.
