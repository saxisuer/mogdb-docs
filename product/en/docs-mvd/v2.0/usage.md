---
title: MVD Usage
summary: MVD Usage
author: hongyedba
date: 2021-09-02
---

# MVD Usage

MVD is command line tool can be run only in the Shell interface. You can query the command help information by running the **-h** command.

```shell
Introduction:
    MVD is a data verification tool for Heterogeneous Databases.

Options:
    -h, --help           : Show help message
    -v, --version        : Show tool version [2.2.14]
    -x, --debug          : Run in debug mode, means more output logs
    -c, --config-file    : Using a config file with format json
        --mtk-config     : Using a config file from MTK tool with format json
    -d, --func-dimension : [Advanced Option] Functions used in data comparison
                         : Default: avg:a,min:np,max:np,median:np
                         : Format: <name>:<primary_type>    -- Same function name among all database
                         :         <primary_type> := a|p|np, a = all, p = primary table, np = not [p]
                         :                           can be ignored, then use default [a]
                         : Format: <name>:<function_list>:<primary_type>
                         :         <function_list> := <db_type> = <function_name> | <db_type> = <function_name>
                         :         <db_type> := ORACLE|DB2|MYSQL|POSTGRESQL|MOGDB|OPENGAUSS
                         : Example 'testmin:oracle=min|mogdb=min|mysql=min|db2=min:p'
    -s, --source-db      : Source database to be verified
                         : Format: <db_type>:<ip>:<port>:<name>:<user>:<password>
                         :         <db_type> := ORACLE|DB2|MYSQL|POSTGRESQL|MOGDB|OPENGAUSS
    -t, --target-db      : Target database to be verified, format see also '-s'
    -w, --workers        : Parallel workers (1~32), default: 8
    -T, --table          : Check a single table
                         : Format: <owner>.<table_name>
                         :   Tips: if this option was specified, then '-i' and '-e' will be ignored
    -i, --include        : White list, patterns used for object filter, all patterns combined with comma
                         : Format: <type>:<owner>.<object_name>,...
                         :         <type>:<object_name>
                         :         <owner>.<object_name>
                         :         <object_name>
                         : Notes: 1. <type> can use: */%/TABLE/VIEW/SEQUENCE/PROCEDURE/FUNCTION/OTHERS
                         :        2. Can use */% in <owner> and <object_name> field, means to match all
    -e, --exclude        : Black list, patterns used for object filter, format see also '-i'
    -f, --result-file    : Result file, used to save result, default to print result to screen
    -F, --result-format  : Result file data format: json (default), plain
    -R, --row-dir        : Row directory for differences data (MD5 & KEY)
        --row-feedback   : Query row data when differences found, otherwise just key condition listed
        --ignore-float   : Ignore float data type in comparison
    -l, --logfile        : Write output information to logfile

Usage:
    1. Verify a single table  (Using Statistics Analysis)
       ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -T 'HONGYE.TEST'
    2. Verify a single schema  (Using MD5 ROW-BY-ROW)
       ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -i 'HONGYE.*' -R './diff'
```

## Command Line Option Description

| Command Option       | Description                                                  |
| -------------------- | ------------------------------------------------------------ |
| -h, --help           | Shows the help information of the tool.                      |
| -v, --version        | Shows the current version of the tool.                       |
| -x, --debug          | Enables the debug mode and outputs more detailed log content. <br />This option is used only in the debugging process. |
| -c, --config-file    | Specifies configuration parameters (JSON format) used during program execution. Configuration parameters can replace the specific command line options. |
| --mtk-config         | Specifies the configuration file (JSON format) used during MTK execution. It is used for verifying objects and table data involved in MTK synchronization. |
| -d, --func-dimension | This is an advanced option. If you are not very clear about its meaning, do not use it. <br />Specifies the dimension of the statistical function during data comparison. The input format is as follows:  <br />   1. &lt;name&gt;:&lt;primary_type&gt;<br />   2. &lt;name&gt;:&lt;function_list&gt;:&lt;primary_type&gt;<br />Where: <br />The value of `primary_type` an be a (all tables), p (primary key tables), and np (non-key value tables).<br />**name** indicates the function name. If the function name is the actual function used in a database, the name does not need to be specified. Otherwise, the name needs to be specified. <br />**function_list** indicates the format of the function in different databases. The format is &lt;db_type&gt;=&lt;function_name&gt; \| &lt;db_type&gt;=&lt;function_name&gt; \| ...<br />The value of **db_type** can be **ORACLE**, **DB2**, **MYSQL**, **POSTGRESQL**, **MOGDB**, and **OPENGAUSS**. |
| -s, --source-db      | Specifies the source database. The input format is &lt;db_type&gt;:&lt;ip&gt;:&lt;port&gt;:&lt;name&gt;:&lt;user&gt;:&lt;password&gt;.<br />The value of `db_type` can be **ORACLE** , **DB2**, **MYSQL**, **POSTGRESQL**, **MOGDB**, and **OPENGAUSS**. |
| -t, --target-db      | Specifies the target database. The input format is the same as that of `-s`. |
| -w, --workers        | Specifies the number of concurrent processes during data comparison. The value ranges from 1 to 32. The default value is **8**. |
| -T, --table          | Specifies a single table to be compared. The input format is &lt;owner&gt;.&lt;table_name&gt;. Wildcard characters are not allowed to be used because they will conflict with `-i` and `-e`. |
| -i, --include        | Specifies a list of objects to be included for comparison. You can specify multiple matching modes and separate them with commas. <br />The matching format includes &lt;type&gt;:&lt;owner&gt; .&lt;object_name&gt;, &lt;owner&gt;.&lt;object_name&gt;, &lt;object_name&gt;, ....<br />``*`` or ``%`` can be used in OWNER and OBJECT_NAME to mark wildcard characters.<br />TYPE includes \*/%/TABLE/VIEW/SEQUENCE/PROCEDURE/FUNCTION/OTHERS. |
| -e, --exclude        | Specifies the list of objects to be excluded. The format is the same as that of `-i`. |
| -f, --result-file    | Specifies the comparison result file. By default, no file will be generated, and the result is shown in the command line window. |
| -F, --result-format  | Specifies the comparison result format, including **json** and **plain**. The default value is **plain**, indicating the text format which is convenient for the user to read. |
| -R, --row-dir        | Specifies the folder where the row difference result is generated upon the execution of the MD5 row-by-row comparison mode. For each table with a difference, a difference file is created in that directory. |
| --row-feedback       | Specifies whether to show the full data of a difference row. Only KEY of a difference row is shown by default. (The KEY indicates ROWID in Oracle and CTID in PostgreSQL.) |
| --ignore-float       | Specifies whether to ignore the floating-point type (float, double, real, and other non-precise types) during data comparison. The floating-point type is not ignored by default. |
| -l, --logfile        | Specifies the log file for tool running.                     |

## Examples of Common Commands

The following lists command examples in common scenarios (using the MacOS 2.0 as an example).

Note: The method of comparing statistical eigenvalues is inefficient. It is recommended to use the MD5 row-by-row comparison method.

1. Compare the structures and data between Oracle and MogDB.

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:hongye:pwd' -t 'MOGDB:127.0.0.1:5432:omm:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

2. Compare the structures and data between DB2 to MogDB.

   ```shell
   ./mvd_macos_x86_64 -s 'DB2:127.0.0.1:50000:HONGYE:db2inst1:pwd' -t 'MOGDB:127.0.0.1:5432:db2_mtk1:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

3. Compare the structures and data between MySQL to MogDB.

   ```shell
   ./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306:hongye:root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

4. Compare data in a table (eigenvalue comparison).

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -T 'HONGYE.TEST'
   ```

5. Compare data and structures and precisely recognize rows that involve difference.

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

6. Perform data verification after migration according to the MTK configuration file.

   ```shell
   ./mvd_macos_x86_64 --mtk-config oracle2opengauss.json
   ```
