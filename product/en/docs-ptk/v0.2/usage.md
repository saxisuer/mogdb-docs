---
title: Usage Guide
summary: Use Guide
author: Yao Qian
date: 2022-05-30
---

> Note:
> To operate with PTK as the non-root user, make sure that the user has the sudo permission. You can add the following content to the /etc/sudoers file (replace USERNAME as required):
> `[USERNAME]  ALL=(ALL)  NOPASSWD:ALL`

## Viewing Help Information

After PTK is successfully installed, you can run `ptk -h` to view the help information. For details about command parameters, see [PTK Command Reference](command.md).

## Viewing Version

You can run `ptk -v` to view PTK version. 

```shell
$ ptk -v
PTK Version: v0.1.0
Go Version: go1.17.1
Build Date: 2022-05-27T09:32:14Z
Git Hash: 6aba9ea
```

The output includes the PTK version, Golang version in compilation, build time, and git hash value. 

## Creating a Topology Configuration File

Before installing the database, PTK can be used to create a configuration file for defining the expected database cluster topology.

To install a single-instance database, run the following command to generate a configuration file.

```shell
ptk template -l > config.yaml
```

To install a multi-instance cluster, run the following command to generate a configuration file.

```shell
ptk template > config.yaml
```

After PTK is run, a `config.yaml` file is generated in the current folder. You need to replace the value of the HOST field with the actual IP address. If there is the password field, you need to run [ptk encrypt](commands/ptk-encrypt.md) to encrypt the password and then add the ciphertext to the configuration file.

If you are not sure about the meaning of each field, run the following command to create a configuration file. PTK supports configuration file creation in an interactive way.

```shell
ptk template create
```

## System Check

Before installing the database, you need to check the system parameters and software dependencies of the server where the database is to be installed to ensure smooth installation.

You can run `checkos` to perform system check. During the check, PTK checks the following modules.

| No. | Category  | Check Item               | Description       |
| -------- | ------------ | ----------------------------- | -------------------- |
| A1       | System version | Check_OS_Version              | Checks the system version. |
| A2       | Kernel version        | Check_Kernel_Version          | Checks the kernel version. |
| A3       | Character set | Check_Unicode                 | Check the character set. |
| A4       | Time zone | Check_TimeZone                | Checks the time zone. |
| A5       | Swap memory | Check_Swap_Memory_Configure   | Checks the swap memory configuration. |
| A6       | sysctl parameter | Check_SysCtl_Parameter        | Checks the sysctl parameter. |
| A7       | File system | Check_FileSystem_Configure    | Checks the file system configuration. |
| A8       | Disk | Check_Disk_Configure          | Checks the disk configuration. |
| A9       | Preread block | Check_BlockDev_Configure      | Checks the block configuration. |
|          |  | Check_Logical_Block           | Checks the logic block. |
| A10      | IO scheduling | Check_IO_Request              | Checks the IO request parameters. |
|          |  | Check_Asynchronous_IO_Request | Checks the asynchronous IO request parameters. |
|          |              | Check_IO_Configure            | Checks the IO configuration. |
| A11      | Network | Check_Network_Configure       | Checks the network configuration. |
| A12      | Clock consistency | Check_Time_Consistency        | Checks the clock consistency. |
| A13      | Firewall | Check_Firewall_Service        | Checks the firewall configuration. |
| A14      | Transparent huge page | Check_THP_Service             | Checks the transparent huge page configuration. |
| A15    | Dependencies          | Check_Dependent_Package        | Checks the database system installation dependencies. |
| A16      | CPU command set | Check_CPU_Instruction_Set        | Checks the CPU command set. |

To perform system check using PTK, you need to specify the cluster configuration file using`-f` and content to be checked using `-i`. If the content to be checked is unknown, all check items listed in the above table will be checked by default.

To check individual check items, specify the check items by separating their item numbers with commas.

Example:

```shell
ptk checkos -i A1,A2,A3   # To check multiple check items, enter the item number in the -i A1,A2,A3 format.
ptk checkos -i A          # Check all check items.
ptk checkos -i A --detail # Display details. 
```

The PTK check result includes four levels.

- **OK**: The check result reaches expectations and meets the installation requirement. 
- **Warning**: The check result does not reach expectations but meets the installation requirement.
- **Abnormal**: The check result does not meet the installation requirement. The installation may fail. The exception needs to be handled manually based on the suggested script by PTK.
- **ExecuteError**: PTK failed to execute commands during system check. The reason may be that some tools are not installed in the environment or there is an internal bug. This error needs to be handled based on the actual prompt information. 

Before installing the database, make sure that the check result of all check items is `OK` or `Warning`. If the check result displays `Abnormal`, an error may occur during installation and installation exits. 

## Database Cluster Installation

After a configuration file is created and the system check is successful, you can  start to install a database. 

You can run the following command to install a database.

```shell
$ ptk install -f config.yaml -y
```

`-f` specifies the configuration file path. `-y` specifies that yes is replied automatically for all interactive questions. (Note: If the initial password is not configured in the configuration file, `-y` cannot skip the password setting process.)

MogDB of the version corresponding to PTK release will be installed by default. You can run `ptk install -h` to query the default value of `--db-version`.

PTK also supports installation of a database of a different version or a database compiled by yourself.

Parameter `--db-version` allows you to install MogDB of a specified version. For example, run `ptk install -f config.yaml --db-version 2.0.1 -y` to install MogDB 2.0.1. For details about the supported MogDB versions, see [MogDB Download](https://www.mogdb.io/downloads/mogdb/).

To install a database using an installation package compiled by yourself or an openGauss package from the openGauss community, use parameter `-p|--pkg` to specify the local path for storing the installation package or network address. PTK distributes the installation package from the server where it is located to other servers.

More options are as follows:

- `-e|--env` : adds environment variables to the database system user environment after database installation.
- `--launch-db-timeout`: specifies the database start timeout duration. The default value is 5 minutes. 
- `--pre-run`: adds a shell script path and executes the specified script on all servers in a cluster before cluster installation.
- `--post-run`: adds a shell script path and executes the specified script on all servers in a cluster after cluster installation.
- `--skip-check-os`: skips the system check. 
- `--skip-check-distro`: skips system release check. 
- `--skip-create-user`: PTK automatically creates a user by default. This option skips user creation. When this option is specified, make sure that a user must exist in the server. 
- `--skip-launch-db`: does not start database after installation. 
- `--skip-rollback`: PTK rolls back all operations to clear environment by default once an error occurs. This option does not allow rollback operations.
- `--install-cm`: installs the CM component during database installation (applied to MogDB 3.0.0 or later or scenarios where at least three instances are installed in a cluster).
- `--no-cache`: does not use an 
- installation package in the local cache and downloads the installation package from the specified network.

## Database Cluster Uninstallation

> Note: Once a database is uninstalled, it cannot be recovered. Please                        execute operation with caution. 
>

You can run `ptk uninstall (-f CONFIG.YAML|--name CLUSTER_NAME)` to uninstall a database cluster on the server accommodating PTK.

You can specify a database cluster to be uninstalled by config.yaml or the cluster name. The prerequisite of specifying a database cluster by the cluster name is that you can find the cluster by running `ptk ls`.

Before uninstallation, PTK questions you in an interactive way to confirm the topology information of the cluster to be deleted, whether to delete the system user,   and whether to delete the database data. Please confirm your answer to each question to avoid data loss due to incorrect operations. 

Before uninstallation, if you confirm deletion of the data directory, PTK will delete only the data directory and will not delete its parent directory. You need to manually delete the parent directory.

## Database Cluster Management

### Viewing the Cluster List

After the database clusters is installed, you can run `ptk ls` to query the list of clusters installed by the current user (Because PTK metadata is stored in the `$HOME/.ptk` directory, you can query the list of the clusters installed under the current user).

```shell
$ ptk ls
  cluster_name  |     instances      |  user  |     data_dir
----------------+--------------------+--------+-------------------
  single        | 172.16.0.183:21000 | single | /opt/mogdb_single/data
  cluster       | 172.16.0.114:23000 | cluster| /opt/mogdb_cluster/data
                | 172.16.0.190:23000 |        |
```

You can find the cluster name, the instance list, the system user, and the data directory of the instances from the output table. 

After the database cluster is installed, PTK can use `-f` to specify the configuration file or `--name` to specify the cluster name to uninstall or manage the cluster. 

### Starting/Stopping/Restarting a Cluster

PTK supports starting and stopping of the whole cluster and also single instance in the cluster. 

To start or stop a cluster, you need to specify the cluster name or configuration file. To manage a cluster by the cluster name, the prerequisite is that the cluster must be installed using PTK and you can find the cluster by running `ptk ls`.

### Viewing the Cluster Status

To check whether the database cluster status is normal, run the `status` subcommand. The following shows an example.

```shell
$ ptk cluster status --name CLUSTER_NAME

[   Cluster State   ]

database_version  : MogDB-3.0.0
cluster_name      : test
cluster_state     : Normal
current_az        : AZ_ALL

[  Datanode State   ]

   id  |      ip      | port  | user  | instance | db_role |  state
-------+--------------+-------+-------+----------+---------+---------
  6001 | 172.16.0.100 | 26000 |  omm  | dn_6001  | primary | Normal
  6002 | 172.16.0.101 | 26000 |  omm  | dn_6002  | standby | Normal
  6003 | 172.16.0.102 | 26000 |  omm  | dn_6003  | standby | Normal
```

To check details about primary/standby replication status, add the `--detail` parameter to the command.

## Database Installation Example

> **CLUSTER_NAME** in the following example needs to be replaced with the actual cluster name. 

### Creating a Configuration File

```shell
ptk template create -o config.yaml
```

### Checking a Server

```shell
ptk checkos -f config.yaml -i A
```

### Installing a Cluster

```shell
ptk install -f config.yaml
```

### Querying the Cluster Status

```shell
ptk cluster status --name CLUSTER_NAME

# View cluster details.
ptk cluster status --name CLUSTER_NAME --detail
```

### Starting or Stopping a Cluster

```shell
# stop
ptk cluster stop --name CLUSTER_NAME

# stop a node
ptk cluster stop --name CLUSTER_NAME -H HOST_IP
# stop a node with mode(f:fast, i:immediate)
ptk cluster stop --name CLUSTER_NAME -H HOST_IP --mode f

# start
ptk cluster start --name CLUSTER_NAME

# start a node
ptk cluster start --name CLUSTER_NAME -H HOST_IP
# start a node with security-mode
ptk cluster start  --name CLUSTER_NAME -H HOST_IP --security-mode on
```

### Uninstalling a Cluster

```shell
ptk uninstall --name CLUSTER_NAME
```