---
title: PTK Overview
summary: PTK Overview
author: Yao Qian
date: 2022-05-30
---

## PTK Overview

PTK (Provisioning Toolkit) is an installation and operation and maintenance tool for MogDB. It aims at quick installation of MogDB. 

If you need to run MogDB or its related components, only one command needs to be run. 

## Application Scenarios

- Developers need to quickly enable multiple local MogDB processes.  
- Users need to install MogDB quickly using PTK.
- Daily operation and maintenance by DBAs
- A third-party operation and maintenance platform needs to be integrated.

## Operating Systems Supported by PTK for Installing MogDB

You can run `ptk candidate os`  to check the latest OSs supported. 

| OS       | CPU      |
| ----------- | -------------- |
| CentOS 7 |x86_64|
| CentOS 8 |x86_64|
| CentOS 8 |arm64|
| EulerOS 2 |arm64|
| EulerOS 2 |x86_64|
| Kylin V10 |arm64|
| Kylin V10 |x86_64|
| NeoKylin V7 |x86_64|
| Oracle Linux Server 7 |x86_64|
| Oracle Linux Server 8 |x86_64|
| openEuler 20 |arm64|
| openEuler 20 |x86_64|
| openEuler 22 |arm64|
| openEuler 22 |x86_64|
| RedHat 7 |x86_64|
| Rocky Linux 7 |x86_64|
| Rocky Linux 8 |x86_64|
| SLES 12 |arm64|
| Ubuntu 18 |x86_64|
| UOS 20 | arm64|
| UOS 20 |x86_64|

*For other OSs, the adaptation tests are in progress.*