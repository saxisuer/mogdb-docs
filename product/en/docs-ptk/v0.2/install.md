---
title: Installation
summary: Installation
author: Yao Qian
date: 2022-05-30
---

## Installation

PTK is a provisioning toolkit. It does not need to be installed on the same server together with a database instance. It is recommended that a management server is used to install PTK and deploy database instances on other servers. 

### Online Installation

> **Note**: Command line installation does not support PTK installed on Windows. 

You can run the following command to install PTK on macOS or Linux operating systems. 

```shell
curl --proto '=https' --tlsv1.2 -sSf https://cdn-mogdb.enmotech.com/ptk/install.sh | sh
```

After running the command, PTK is automatically installed in the `$HOME/.ptk` directory where cache files, data files, cluster configuration files, and backup files are stored. Additionally, the `$HOME/.ptk/bin` directory will be automatically updated to the environment variable of the corresponding Shell profile file. After login, you can run the ptk-related commands.

### Offline Installation

If the server where PTK is to be installed cannot be connected to the external network or you need to install PTK on Windows, you can manually install PTK.  

Perform the following operations to install PTK manually:

**Step 1** Download the installation package corresponding to your server.  The following lists installation packages of the latest versions corresponding to different system architectures. 

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

**Step 2** Copy the installation package to the target server on the intranet. 

**Step 3** Decompress the package to obtain a binary file named `ptk` (executable file of PTK) and move the file to a directory (`$HOME/.ptk/bin/` is recommended and it needs to be manually created) as you need. Update the directory to the PTK PATH environment variable. 

### PTK Upgrade

For offline installation, download the latest installation package to cover the binary file on the server to upgrade PTK. 

For online installation, run the following command to upgrade PTK. 

```shell
ptk self upgrade
```