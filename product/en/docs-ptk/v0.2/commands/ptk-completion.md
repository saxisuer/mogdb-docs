---
title: ptk completion
summary: ptk completion
author: ptk
date: 2022-06-28
---

> Note: This command is not necessary. It is used for reference. 

## ptk completion

ptk completion generates automatic PTK completion script for the specified Shell. For details about how to use the generated script, see the subcommands. 

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Subcommand

- [ptk completion bash](ptk-completion-bash.md): Generates the automatic completion script of bash. 
- [ptk completion fish](ptk-completion-fish.md): Generates the automatic completion script of fish. 
- [ptk completion powershell](ptk-completion-powershell.md): Generates the automatic completion script of powershell. 
- [ptk completion zsh](ptk-completion-zsh.md): Generates the automatic completion script of zsh. 
