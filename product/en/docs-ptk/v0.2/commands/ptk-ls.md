---
title: ptk ls
summary: ptk ls
author: ptk
date: 2022-06-28
---

## ptk ls

ptk ls lists all databases installed. Different users can see only their own databases.

## Syntax

```shell
ptk ls [flags]
```

## Option

### -n, --only-name

- Lists only the cluster name. 
- Data type: Boolean
- This option is disabled by default. It displays the static data of a cluster. It takes effect by being added to the command line.
- Default value: **false**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs a cluster list, which includes the following fields:

- cluster_name: Cluster name
- instances: Instances in a cluster
- user: Operating system user
- data_dir: Data directory of an instance
- db_version: Database version
