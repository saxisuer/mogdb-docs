---
title: ptk candidate mogdb-server
summary: ptk candidate mogdb-server
author: ptk
date: 2022-6-28
---

## ptk candidate mogdb-server

ptk candidate mogdb-server prints the list of the MogDB database versions supported by PTK.

## Syntax

```shell
ptk candidate mogdb-server [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs a table including the `software` and `version` fields.
