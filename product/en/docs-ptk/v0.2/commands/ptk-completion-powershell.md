---
title: ptk completion powershell
summary: ptk completion powershell
author: ptk
date: 2022-06-28
---

## ptk completion powershell

ptk completion powershell generates the automatic completion script of powershell.

Make the script take effect by running the command in the shell session. 

```powershell
ptk completion powershell | Out-String | Invoke-Expression
```

Run the command in each session to make the script take effect. Add the output of the above command to your powershell profile.

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
