---
title: ptk candidate
summary: ptk candidate
author: ptk
date: 2022-6-28
---

## ptk candidate

ptk candidate prints the list of software versions supported by PTK and prints the list of the supported MogDB server versions by default. The function of ptk candidate is the same as that of [ptk candidate mogdb-server](ptk-candidate-mogdb-server.md).

## Syntax

```shell
ptk candidate [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs a table including the `software` and `version` fields.

## Subcommand

- [ptk candidate mogdb-server](ptk-candidate-mogdb-server.md) - Lists the supported MogDB database versions. 
- [ptk candidate os](ptk-candidate-os.md) - Lists the OSs that support installation of MogDB. 
