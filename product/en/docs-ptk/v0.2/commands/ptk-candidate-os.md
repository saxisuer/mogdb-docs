---
title: ptk candidate os
summary: ptk candidate os
author: ptk
date: 2022-6-28
---

## ptk candidate os

Lists the operating systems supported for installing MogDB. 

## Syntax

```shell
ptk candidate os [flags]
```

## Option

### -h, --help

- Outputs the help information. 
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command. 

## Output

Outputs a table including the `software` and `version` fields.