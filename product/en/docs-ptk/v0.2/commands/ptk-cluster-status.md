---
title: ptk cluster status
summary: ptk cluster status
author: ptk
date: 2022-06-28
---

## ptk cluster status

ptk cluster status queries the running status of a database cluster or instance. 

## Syntax

```shell
ptk cluster status -n string [flags]
```

## Option

### -n, --name string

- Specifies the name of the cluster to be operated with.
- Data type: String

### -H, --host string

- Specifies the IP address of the instance to be operated with. 
- Data type: String
- This option supports querying the status of a single instance. It is used for querying the status of all instances in a cluster by default.

### --detail

- Specifies whether to display details. 
- Data type: Boolean
- Default value: **false**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs the status by module. 

- Cluster State: specifies the cluster status. 
- Datanode State: specifies the status of each instance node.
- (Datanode Detail): specifies the node details which is not output by default and displayed when you specify the `--detail` option. 
