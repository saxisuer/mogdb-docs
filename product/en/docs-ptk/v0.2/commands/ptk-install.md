---
title: ptk install
summary: ptk install
author: ptk
date: 2022-6-28
---

## ptk install

ptk install deploys a MogDB database cluster based on the given topology configuration. 

## Syntax

```shell
ptk install -f CONFIG_YAML [flags]
```

## Option

### -y, --assumeyes

- Automatically replies yes for all questions. 
- Data type: Boolean
- This option is disabled by default. It takes effect by being added to the command line. 
- Default value: **false**

### --db-version string

- Specifies the database version (only support for online installation)
- Data type: String
- You can run the `ptk candidate mogdb-server` command to query the supported MogDB server version list.
- Default value: **3.0.0**

### -e, --env strings

- Specifies the environment variable string in the key-value format. All values are added to environment variables of the system user after installation. 
- Data type: String list
- This option can be specified for multiple times and its value varies each time. 

### -p, --pkg string

- Specifies the file path or download address of a database installation package. 
- Data type: String
- This option is left blank by default. The installation package corresponding to the default value of `--db-version` is used. The option supports local disk file path and also network download path (including `http://` or `https://`).

### --no-cache

- Specifies that the installation package in the local cache is not used.
- Data type: Boolean
- This option is disabled by default. PTK checks whether there is a cached installation package in the local metadata. If there is an installation package, it will be used for installation.
- Default value: **false**

### --launch-db-timeout duration

- Specifies the database start timeout duration.
- Data type: Duration
- If the database start time exceeds the specified timeout duration, a timeout error is reported. You need to manually check the database start status or manually start the database. 
- The value unit can be h (hour), m (minute), s (second), and ms (millisecond).
- Default value: **1m**

### --pre-run string

- Specifies the path for storing the bash script to be executed before installing a database. 
- Data type: String
- This option provides the pre-hook function, which allows you to provide a bash script. PTK will run the script on each server in a cluster before database deployment. 

### --post-run string

- Specifies the path for storing the bash script to be executed after starting a database. 
- Data type: String
- This option provides the post-hook function, which allows you to provide a bash script. PTK will run the script on each server in a cluster after database start. 

### --skip-check-distro

- Skips the check of operating system release version.
- Data type: Boolean
- This option is disabled by default and takes effect after being added to the command line. It is usually used in test scenarios. It can be used for testing a database on an operating system that PTK does not support.
- Default value: **false**

### --skip-check-os

- Skips the check of system environment. 
- Data type: Boolean
- This option is disabled by default and takes effect after being added to the command line. When you cannot modify system parameters due to some reasons, PTK supports skipping this check item to install a database directly. However, once the check result is abnormal, the installation will terminate. 
- Default value: **false**

### --skip-create-user

- Skips the creation of a system user. 
- Data type: Boolean
- This option is disabled by default and takes effect after being added to the command line. When this option is specified, make sure that the user of the server where a database is installed exists. Otherwise, the installation will terminate.
- Default value: **false**

### --install-cm

- Installs CM-related components (this option takes effect in the scenario where at least 3 nodes are deployed).

- Data type: Boolean

- This option is disabled by default and takes effect after being added to the command line. The CM component provides HA and operation and maintenance capabilities. When you choose other HA components, PTK supports CM component that is not installed by default and starts the primary and standby databases using gs_ctl. 

  **Note**: PTK manages database instances using gs_ctl, therefore the start and stop functions are not supported temporarily when the CM component is installed.

- Default value: **false**

### --skip-launch-db

- Skips database start upon completion of installation. 
- Data type: Boolean
- This option is disabled by default and takes effect after being added to the command line. When this option is specified for installation and you manually start the database again, you need to change the initial password. Otherwise, error `ERROR:  Please use "ALTER ROLE user_name PASSWORD 'password';" to set the password of user xx before other operation!` will be reported when you check the cluster status by running [ptk cluster status](ptk-cluster-status.md).
- Default value: **false**

### --skip-rollback

- Skips rollback once an error occurs during installation. 
- Data type: Boolean
- This option is disabled by default and takes effect after being added to the command line. PTK will roll back all operations once an error occurs during installation by default to make sure that no file or configuration is left in the system. This operation depends on the network, therefore make sure that the SSH connection is normal. If the network is disconnected or other error occurs, rollback will terminate and you need to manually clear the system environment.
- Default value: **false**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Displays installation log. The default log level is INFO. You can modify the log level by specifying the global parameter `--log-level`.

Outputs installation status table, including the following fields: 

- host: server IP address
- stage: installation stage
- status: installation status
- message: installation result information

Installation status：

- precheck_success: Precheck is successful.
- precheck_failed: Precheck failed.
- initial_db_success: Database initialization is successful.
- initial_db_failed: Database initialization failed.
- initial_cm_success: CM component initialization is successful.
- initial_cm_failed: CM component initialization failed.
- rollback_success: Rollback is successful.
- need_rollback_manually: You need to manually perform rollback operations.
- start_success: The database is started successfully.
- start_failed: The database failed to be started.
- need_start_manually: You need to manually start the database. 
