---
title: ptk completion bash
summary: ptk completion bash
author: ptk
date: 2022-06-28
---

## ptk completion bash

ptk completion bash generates the automatic completion script of bash shell.

The script depends on the bash-completion package.

You can install the bash-completion package using the  package manager of the operating system. 

Make the script take effect by running the command in the shell session. 

```shell
Source <(ptk completion bash)
````

Run the command in each session to make the script take effect. 

**Linux:**

```shell
ptk completion bash > /etc/bash_completion.d/ptk
````

**macOS:**

```shell
ptk completion bash > /usr/local/etc/bash_completion.d/ ptk
```

You need to start new shell to make the setting take effect. 

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
