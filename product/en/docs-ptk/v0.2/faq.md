---
title: FAQ
summary: FAQ
author: Yao Qian
date: 2022-05-30
---

## FAQ

### Can Multiple Instances Be Deployed on a Single Server?

One server supports deployment of multiple database instances. The database depends on environment variables, therefore different system users need to be defined during database configuration to isolate environment variables. 

### Is gs_om Supported After PTK Is Installed?

Yes. The database cluster installed using PTK is compatible with gs_om because the `cluster_static_config` and `upgrade_version` files required by gs_om are generated during automatic installation using PTK.

> **Note**: `gs_om` requires user mutual-trust which needs to be configured.

### How Can I Query the Supported Versions of MogDB Using PTK?

You can run the `ptk candidate mogdb-server` command.

### What Should I Do If Error `FATAL:  semctl(578486307, 7, SETVAL, 0) failed: Invalid argument` Is Reported During `gs_ctl query`

Check the value of **RemoveIPC** in the `/etc/systemd/logind.conf` file. Make sure that the value is **no** and then restart the server. 

### How Can I Query the Supported Operating Systems for Installing MogDB Using PTK?

You can run the `ptk candidate os` command.