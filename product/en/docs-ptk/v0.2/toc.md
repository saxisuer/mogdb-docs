<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# Documentation

## PTK Documentation

+ [About PTK](/overview.md)
+ [Quick Start](/quick-start.md)
+ [Installation](/install.md)
+ [Usage](/usage.md)
+ [Configuration](/config.md)
+ PTK Commands
  + [ptk](/command.md)
  + [ptk candidate](/commands/ptk-candidate.md)
    + [ptk candidate mogdb-server](/commands/ptk-candidate-mogdb-server.md)
    + [ptk candidate os](/commands/ptk-candidate-os.md)
  + [ptk checkos](/commands/ptk-checkos.md)
  + [ptk encrypt](/commands/ptk-encrypt.md)
  + [ptk env](/commands/ptk-env.md)
  + [ptk template](/commands/ptk-template.md)
    + [ptk template create](/commands/ptk-template-create.md)
  + [ptk install](/commands/ptk-install.md)
  + [ptk ls](/commands/ptk-ls.md)
  + [ptk cluster](/commands/ptk-cluster.md)
    + [ptk cluster start](/commands/ptk-cluster-start.md)
    + [ptk cluster stop](/commands/ptk-cluster-stop.md)
    + [ptk cluster status](/commands/ptk-cluster-status.md)
    + [ptk cluster restart](/commands/ptk-cluster-restart.md)
  + [ptk self](/commands/ptk-self.md)
    + [ptk self upgrade](/commands/ptk-self-upgrade.md)
  + [ptk uninstall](/commands/ptk-uninstall.md)
  + [ptk gen-om-xml](/commands/ptk-gen-om-xml.md)
  + [ptk version](/commands/ptk-version.md)
  + [ptk completion](/commands/ptk-completion.md)
+ [Troubleshooting](/debug.md)
+ [FAQ](/faq.md)
+ [Release Note](/release.md)
