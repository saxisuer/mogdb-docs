---
title: Release Note
summary: Release Note
author: Yao Qian
date: 2022-06-01
---

## PTK of the Latest Versions 

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## Release Note

### v0.2.3 (2022.7.4)

#### Change Log

- Fix the issue that the status of the three-node cluster is incorrectly displayed.
- Support Oracle Linux, Rocky Linux, and Neokylin
- Add the cluster name, user, and port fields in the installation result table.

#### Download Address

- [ptk_0.2.3_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_linux_x86_64.tar.gz)
- [ptk_0.2.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_linux_arm64.tar.gz)
- [ptk_0.2.3_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_darwin_x86_64.tar.gz)
- [ptk_0.2.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_darwin_arm64.tar.gz)
- [ptk_0.2.3_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_windows_x86_64.tar.gz)

### v0.2.0 (2022.6.30)

#### Change Log

- Support installation of MogDB 3.0.0.
- Support installation of CM components
- Optimize the system check logic. 
- Forcibly encrypt the password field in the configuration file. 

#### Download Address

- [ptk_0.2.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_linux_x86_64.tar.gz)
- [ptk_0.2.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_linux_arm64.tar.gz)
- [ptk_0.2.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_darwin_x86_64.tar.gz)
- [ptk_0.2.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_darwin_arm64.tar.gz)
- [ptk_0.2.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_windows_x86_64.tar.gz)

### v0.1.0 (2022.6.1)

- [ptk_0.1.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_x86_64.tar.gz)
- [ptk_0.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_arm64.tar.gz)
- [ptk_0.1.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_x86_64.tar.gz)
- [ptk_0.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_arm64.tar.gz)
- [ptk_0.1.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_windows_x86_64.tar.gz)
