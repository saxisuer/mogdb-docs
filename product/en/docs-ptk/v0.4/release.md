---
title: Release Note
summary: Release Note
author: Yao Qian
date: 2022-06-01
---

## PTK of the Latest Version

The PTK of the revised version will be released aperiodically based on user feedback. It is recommended that users download the latest version for use. 

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## Release Note

### v0.4

#### New Function

- Add the failover/switchover function. 
- Add the show-hba/show-guc/show-config function so that users can query the cluster configuration information conveniently. 
- Add the MogHA installation function.
- Add the shell command that can make the shell command or the script run quickly. 
- Add the refresh command so that users can update cluster configurations conveniently. Currently, the IP address modification function is available. 
- Add the register command that allows users to register the system type. 
- Add the CPU command to allow users to check the CPU architectures supported by PTK. 
- Add the copy mode in cluster scale-out.
- Support the operation (start, stop, and query) on all the clusters. 
- Support installation of CM two-node in MogDB 3.0.1 or later. 
- Support Phytium CPU check. 
- Be compatible with openGauss installation. 

#### Function Optimization and Bug Fixing

- Optimize that check and processing of the interrupt signal in execution. 
- Offer suggestions once an error occurs due to insufficient semaphore. 
- Fix the issue that CM fails to start because the AZ priority configuration is incorrect. 
- Fix other known issues. 

### v0.3

#### New Function

- Support scale-in. 
- Support extension installation. 
- Automatically fix the dynamic library dependency based on the environment. 
- Add the version to the cluster list result. 
- Add the MD5 verification of an installation package. 
- Support the download command. 
- Add the cluster comment function. 

#### Function Optimization and Bug Fixing

- Fix the network check error when there is an IB NIC. 
- Fix the SUSE script error.
- Fix other known issues.

### v0.2

#### New Function

- Add the candidate command.
- Support CM installation. 
- Generate a fix script of the system check automatically.
- Add the default configuration of recommended database parameters.

#### Bug Fixing

- Optimize the installation process. 
- Fix the multi-check item problem in OS check. 
- Automatically detect the package format.
- Fix other known issues. 

### v0.1

#### New Function

- Support system check. 
- Support database installation and uninstallation.
- Support cluster management, including start, stop, query, and restart.
- Support password encryption.
- Support template generation.
- Adapt to different OS for installation.
- Support PTK self-upgrade.