---
title: Troubleshooting
summary: Troubleshooting
author: Yao Qian
date: 2022-05-30
---

## Troubleshooting

### Error Code Starting with 40xx

Error code starting with 40XX, such as `PTK-4030` indicates that data in the configuration file is incorrect. You need to modify related information according to the prompt information in the error code and then run the command.

### Error Code Starting with 50xxx

Error code starting with 50XX, such as `PTK-50000` indicates that the error occurs during PTK running. 

The specific categories are as follows.

```wiki
- 500XX Program error or unknown error
- 501XX Cluster and metadata error
- 502XX File or folder error
- 503XX System user or group error
- 504XX Disk error
- 505XX Memory error
- 506XX Network error
- 507XX Firewall error
- 508XX Other system error
- 509XX gs_ctl operation error
- 510XX Environment variable error
```

You can query the related system or running parameters as required. 

If a program crashes or there are bugs, report them to PTK developers for fixing or upgrade.