---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# Configuration File

The PTK configuration file is in the YAML format. 

This document introduces all configuration parameters required for cluster installation.

To install a database cluster, you need to provide a configuration file used for describing the cluster topology. It will be used in most PTK subcommands.

PTK provides an interactive way of creating a configuration file. You can run the following command to enter the interactive process.

```shell
ptk template create
```

After finishing configuration according to the terminal prompts, a `config.yaml` file will generate in the current folder. 

Additionally, if you do not need to create a configuration file in the interactive way, run the following command to generate a default configuration file `config.yaml`. You need to configure parameters based on actual requirements.

```shell
ptk template > config.yaml
```

When you do not create the configuration file in an interactive way, you are advised to encrypt the system user password, database password, SSH password, or key password by running the PTK encryption command before writing the password into the configuration file.

```shell
ptk encrypt PASSWORD
```

---

## Configuration

The following shows how to configure database cluster topology information. 

**Example**:

- Simple configuration

```yaml
# Cluster configuration information, in which db_port and ssh_option are reusable fields
global:
    cluster_name: DefaultCluster # Cluster name that is the unique identifier of a cluster
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts. 
# Configuration information of database instance servers in a cluster
db_servers:
    - host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
    - host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
```

- Complex configuration

```yaml
# Cluster configuration information, in which db_port and ssh_option are reusable fields
global:
    cluster_name: CustomCluster # Cluster name that is the unique identifier of a cluster
    user: omm # System username for running a database
    group: omm # System user group for running a database
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts. 
    base_dir: /opt/mogdb # Base directory of database installation. If it is not configured in app_dir, data_dir, log_dir, and tool_dir, PTK will create these directories in the current directory.
# Configuration information of database instance servers in a cluster
db_servers:
    - host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
      ha_ips:
        - 10.1.1.200
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: primary # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      az_name: BJ # AZ name
      az_priority: 1 # AZ priority. The lower the value, the higher the priority. 
      # SSH login information. The login user should be root or a user who has the sudo permission. 
      ssh_option:
        port: 22 # SSH login user
        user: root # Password of the SSH login user
        password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH login key file path
        conn_timeout: 5m0s # Timeout for performing a single command in SSH. 
        exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection. 
    - host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
      ha_ips:
        - 10.1.1.201
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      az_name: BJ # AZ name
      az_priority: 1 # AZ priority. The lower the value, the higher the priority. 
       # SSH login information. The login user should be root or a user who has the sudo permission. 
      ssh_option:
        port: 22 # SSH login user
        user: root # Password of the SSH login user
        key_file: ~/.ssh/id_rsa # SSH login key file password
        conn_timeout: 5m0s # Timeout for performing a single command in SSH. 
        exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection. 
    - host: 10.1.1.102 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
      ha_ips:
        - 10.1.1.202
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: cascade_standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      upstream_host: 10.1.1.101 # When the instance role is  cascade_standby, this field indicates the IP address of the upstream standby server.
      az_name: SH # AZ name
      az_priority: 2 # AZ priority. The lower the value, the higher the priority. 
```

### Global

**Data type**: <a href="#global">Global</a>

**Description**: Cluster configuration information, in which `db_port` and `ssh_option` are **reusable fields**.<br /><br />**Reusable fields**

- If this field is set in instance-level configuration, the instance-level configuration prevails.
- If this field is not set in instance-level configuration, the global-level configuration prevails.

**Example**:

- Simple global example

```yaml
global:
    cluster_name: DefaultCluster # Cluster name that is the unique identifier of a cluster
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts.
```

- Complex global example

```yaml
global:
    cluster_name: CustomCluster # Cluster name that is the unique identifier of a cluster
    user: omm # System username for running a database
    group: omm # System user group for running a database
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts.
    base_dir: /opt/mogdb # Base directory of database installation. If it is not configured in app_dir, data_dir, log_dir, and tool_dir, PTK will create these directories in the current directory.
```

**Added in**: v0.1

### db_servers

**Data type**: <a href="#dbserver">DBServer</a>

**Description**: Configuration information of database instance servers in a cluster

**Example**:

- Simple database server example

```yaml
db_servers:
    - host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
    - host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
```

- Complex database server example

```yaml
db_servers:
    - host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
      ha_ips:
        - 10.1.1.200
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: primary # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      az_name: BJ # AZ name
      az_priority: 1 # AZ priority. The lower the value, the higher the priority.
      # SSH login information. The login user should be root or a user who has the sudo permission.
      ssh_option:
        port: 22 # SSH login user
        user: root # Password of the SSH login user
        password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH login key file path
        conn_timeout: 5m0s # Timeout for performing a single command in SSH.
        exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection. 
    - host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
      ha_ips:
        - 10.1.1.201
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      az_name: BJ # AZ name
      az_priority: 1 # AZ priority. The lower the value, the higher the priority.
      # SSH login information. The login user should be root or a user who has the sudo permission. 
      ssh_option:
        port: 22 # SSH login user
        user: root # Password of the SSH login user
        key_file: ~/.ssh/id_rsa # SSH login key file password
        conn_timeout: 5m0s # Timeout for performing a single command in SSH.
        exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection.
    - host: 10.1.1.102 # IP address of the database instance server (only supporting IPv4)
      db_port: 27000 # Database port
      # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster)
      ha_ips:
        - 10.1.1.202
      ha_port: 27001 # Log transfer port of primary/standby database instances
      role: cascade_standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
      upstream_host: 10.1.1.101 # When the instance role is  cascade_standby, this field indicates the IP address of the upstream standby server.
      az_name: SH # AZ name
      az_priority: 2 # AZ priority. The lower the value, the higher the priority.
```

**Added in**: v0.1

---

## Global

Global defines global parameters in a configuration file. 

Appears in:

- <code><a href="#config">Config</a>.global</code>

**Example**:

- Simple global example

```yaml
cluster_name: DefaultCluster # Cluster name that is the unique identifier of a cluster
db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts.
```

- Complex global example

```yaml
cluster_name: CustomCluster # Cluster name that is the unique identifier of a cluster
user: omm # System username for running a database
group: omm # System user group for running a database
db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts.
base_dir: /opt/mogdb # Base directory of database installation. If it is not configured in app_dir, data_dir, log_dir, and tool_dir, PTK will create these directories in the current directory.
```

### cluster_name

**Data type**: String

**Description**: Cluster name that is the unique identifier of a cluster

**Example**:

- Cluster name example

```yaml
cluster_name: cluster1
```

**Added in**: v0.1

### user

**Data type**: String

**Description**: System username for running a database<br />The default value is **omm**. 

**Example**:

- OS user example

```yaml
user: omm
```

**Added in**: v0.1

### group

**Data type**: String

**Description**: System user group for running a database<br />The value is the same as the system username by default. 

**Example**:

- OS user group example

```yaml
group: omm
```

**Added in**: v0.1

### user_password

**Data type**: String

**Description**: System user password for running a database<br />The value can be displayed in plain text and ciphertext. It is recommended that the value is encrypted by running `ptk encrypt`. <br />This field is left blank by default. 

**Added in**: v0.1

### db_password

**Data type**: String

**Description**: Initial database user password. If this password is not specified in the configuration file, it needs to be entered during database installation according to the terminal prompts.<br />The value can be displayed in plain text and ciphertext. It is recommended that the value is encrypted by running `ptk encrypt`.

**Added in**: v0.1

### db_port

**Data type**: Int

**Description**: Database port<br />The default value is **26000**.

**Example**:

- Database port example

```yaml
db_port: 26000
```

**Added in**: v0.1

### base_dir

**Data type**: String

**Description**: Base directory of database installation. If it is not configured in app_dir, data_dir, log_dir, and tool_dir, PTK will create these directories in the current directory.<br />The default value is **/opt/mogdb**.

**Example**:

- Database base directory example

```yaml
base_dir: /opt/mogdb
```

**Added in**: v0.1

### app_dir

**Data type**: String

**Description**: Directory for storing database deployment file, startup script, and configuration file<br />The default value is **/opt/mogdb/app**.

**Example**:

- Database app directory example

```yaml
app_dir: /opt/mogdb/app
```

**Added in**: v0.1

### data_dir

**Data type**: String

**Description**: Data directory of a database<br />The default value is **/opt/mogdb/data**.

**Example**:

- Database data directory example

```yaml
data_dir: /opt/mogdb/data
```

**Added in**: v0.1

### log_dir

**Data type**: String

**Description**: Directory for storing database run logs and operation logs<br />The default value is **/opt/mogdb/log**.

**Example**:

- Database log directory example

```yaml
log_dir: /opt/mogdb/log
```

**Added in**: v0.1

### tool_dir

**Data type**: String

**Description**: Database tool directory<br />The default value is **/opt/mogdb/tool**.

**Example**:

- Database tool directory example

```yaml
tool_dir: /opt/mogdb/tool
```

**Added in**: v0.1

### tmp_dir

**Data type**: String

**Description**: Temporary directory of a database<br />The default value is **/tmp**.

**Example**:

- Temporary directory example

```yaml
tmp_dir: /tmp
```

**Added in**: v0.1

### core_file_dir

**Data type**: String

**Description**: Directory for storing error log files when core dump occurs. After this field is configured, system configuration parameter `kernel.core_pattern` will be modified accordingly. <br />This field is left blank by default. 

**Added in**: v0.1

### cm_dir

**Data type**: String

**Description**: [This field is not used in v0.4  or later. Please use the CMOption field] Directory for storing database CM data (This field takes effect in MogDB 3.0 or later)

**Example**:

- Database CM directory example

```yaml
cm_dir: /opt/mogdb/cm
```

**Added in**: v0.2

### cm_server_port

**Data type**: Int

**Description**: [This field is not used in v0.4  or later. Please use the CMOption field] CM server port (This field takes effect in MogDB 3.0 or later)

**Example**:

- cm server port

```yaml
cm_server_port: 15300
```

**Added in**: v0.2

### ssh_option

**Data type**: <a href="#sshoption">SSHOption</a>

**Description**: SSH login information. The login user needs to be **root** or has sudo permission. <br />If this field is not configured. The database is installed on the local server where PTK is located. 

**Added in**: v0.1

### gs_initdb_opts

**Data type**: []string

**Description**: Used for the user to initialize the database. <br />If the user does not configure the `locale` parameter. PTK will automatically configure no-locale by default. <br />For details about the supported parameter list, see [gs_initdb](https://docs.mogdb.io/en/mogdb/v3.0/5-gs_initdb)

**Example**:

- gs_initdb options example

```yaml
gs_initdb_opts:
    - --locale=LOCALE
    - -E=UTF-8
```

**Added in**: v0.3

### cm_option

**Data type**: CMOption

**Description**: (optional) MogDB CM configuration information. If CM is not used, this field can be left blank. <br />This field takes effect in MogDB 3.0 or later.

**Added in**: v0.4

## CMOption

Appears in:

- <code><a href="#global">Global</a>.cm_option</code>

### dir

**Data type**: string

**Description**: CM installation directory

**Example**:

- db cm dir example

```yaml
dir: /opt/mogdb/cm
```

**Added in**: v0.4

### cm_server_port

**Data type**: int

**Description**: Listening port of the CM server

**Example**:

- cm server port

```yaml
cm_server_port: 15300
```

**Added in**: v0.4

### db_service_vip

**Data type**: string

**Description**: Virtual IP address for the database to provide services 

**Added in**: v0.4

### cm_server_conf

**Data type**: map[string]string

**Description**: Configuration parameters supported in cm_server.conf. PTK does not verify the accuracy and validity of the parameter. 

**Example**:

- cm server conf

```yaml
cm_server_conf:
    key: value
```

**Added in**: v0.4

### cm_agent_conf

**Data type**: map[string]string

**Description**: Configuration parameters supported in cm_agent.conf. PTK does not verify the accuracy and validity of the parameter. 

**Example**:

- cm agent conf

```yaml
cm_agent_conf:
    key: value
```

**Added in**: v0.4

## DBServer

Configuration information of database instance servers in the DBServer cluster

Appears in:

- <code><a href="#config">Config</a>.db_servers</code>

**Example**:

- Simple database server example

```yaml
- host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
- host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
```

- Complex database server example

```yaml
- host: 10.1.1.100 # IP address of the database instance server (only supporting IPv4)
  db_port: 27000 # Database port
  # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster)
  ha_ips:
    - 10.1.1.200
  ha_port: 27001 # Log transfer port of primary/standby database instances
  role: primary # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
  az_name: BJ # AZ name
  az_priority: 1 # AZ priority. The lower the value, the higher the priority. 
  # SSH login information. The login user should be root or a user who has the sudo permission.
  ssh_option:
    port: 22 # SSH login user
    user: root # Password of the SSH login user
    password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH login key file path
    conn_timeout: 5m0s # Timeout for performing a single command in SSH. 
    exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection.
- host: 10.1.1.101 # IP address of the database instance server (only supporting IPv4)
  db_port: 27000 # Database port
  # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster)
  ha_ips:
    - 10.1.1.201
  ha_port: 27001 # Log transfer port of primary/standby database instances
  role: standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
  az_name: BJ # AZ name
  az_priority: 1 # AZ priority. The lower the value, the higher the priority.
  # SSH login information. The login user should be root or a user who has the sudo permission. 
  ssh_option:
    port: 22 # SSH login user
    user: root # Password of the SSH login user
    key_file: ~/.ssh/id_rsa # SSH login key file password
    conn_timeout: 5m0s # Timeout for performing a single command in SSH. 
    exec_timeout: 5m0s # Login information of a jump server. If the target server cannot be directly connected, a jump server can be used for connection.
- host: 10.1.1.102 # IP address of the database instance server (only supporting IPv4)
  db_port: 27000 # Database port
  # IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 
  ha_ips:
    - 10.1.1.202
  ha_port: 27001 # Log transfer port of primary/standby database instances
  role: cascade_standby # Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.
  upstream_host: 10.1.1.101 # When the instance role is  cascade_standby, this field indicates the IP address of the upstream standby server.
  az_name: SH # AZ name
  az_priority: 2 # AZ priority. The lower the value, the higher the priority.
```

### host

**Data type**: String

**Description**: IP address of the database instance server (only supporting IPv4)

**Example**:

- Database server IP address example

```yaml
host: 10.1.1.100
```

**Added in**: v0.1

### db_port

**Data type**: Int

**Description**: Database port<br />The field is left blank by default. The global port 26000 is reused. 

**Example**:

- Database port example

```yaml
db_port: 26000
```

**Added in**: v0.1

### ha_ips

**Data type**: String

**Description**: IP addresses of the standby database servers (Note: it does not include the IP addresses of other nodes in the cluster) 

**Example**:

- Database HA IP address list example

```yaml
ha_ips:
    - 10.1.1.200
```

**Added in**: v0.1

### ha_port

**Data type**: Int

**Description**: Log transfer port of primary/standby database instances<br />The HA port is the same as the database port by default.

**Added in**: v0.1

### role

**Data type**: String

**Description**: Database instance role. If it is not specified, PTK will choose an instance randomly to make it a primary instance and the others will be standby instances.

**Optional values**: 

- primary
- standby
- cascade_standby

**Example**:

- Database role example

```yaml
role: primary
```

**Added in**: v0.1

### upstream_host

**Data type**: String

**Description**: When the instance role is  cascade_standby, this field indicates the IP address of the upstream standby server.

**Example**:

- Cascaded database upstream server example

```yaml
upstream_host: 10.1.1.101
```

**Added in**: v0.1

### az_name

**Data type**: String

**Description**: AZ name<br />The default value is **AZ1**.

**Example**:

- Database AZ example

```yaml
az_name: AZ1
```

**Added in**: v0.1

### az_priority

**Data type**: Int

**Description**: AZ priority. The lower the value, the higher the priority.<br />The default value is **1**.

**Added in**: v0.1

### xlog_dir

**Data type**: String

**Description**: Directory for storing database  Xlog <br />This field is left blank by default. If this field is configured, the value cannot be the subdirectory of the data directory. 

**Added in**: v0.1

### db_conf

**Data type**: map[string]string

**Description**: You can configure the database parameters as you need. This field configuration information will be written into the postgresql.conf file before the database is started.

**Example**:

- Database configuration example

```yaml
db_conf:
    key: value
```

**Added in**: v0.1

### ssh_option

**Data type**: <a href="#sshoption">SSHOption</a>

**Description**: SSH login information. The login user needs to be **root** or has sudo permission. <br />If this field is not configured. The database is installed on the local server where PTK is located. 

**Added in**: v0.1

---

## SSHOption

SSHOption SSH login supports password and key authentication. You need to choose at least one.  

Appears in:

- <code><a href="#global">Global</a>.ssh_option</code>
- <code><a href="#dbserver">DBServer</a>.ssh_option</code>
- <code><a href="#sshoption">SSHOption</a>.proxy</code>

**Example**:

```yaml
host: 10.1.1.100
port: 22
user: root
key_file: ~/.ssh/id_rsa
```

### host

**Data type**: String

**Description**: IP address of the target server to be connected<br />This field is mandatory when the host works as a jump server. <br />When the server has the database installed, this field can be left blank. The IP address of db_server will be used. <br />This field is left blank by default. 

**Added in**: v0.1

### port

**Data type**: Int

**Description**: SSH service port<br />The default value is **22**. 

**Added in**: v0.1

### user

**Data type**: String

**Description**: SSH login user<br />The default value is **root**. 

**Example**:

- SSH user example

```yaml
user: root
```

**Added in**: v0.1

### password

**Data type**: String

**Description**: SSH login user password<br />The value can be displayed in plain text and ciphertext. It is recommended that the value is encrypted by running `ptk encrypt`.

**Example**:

- SSH password example

```yaml
password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U=
```

**Added in**: v0.1

### key_file

**Data type**: String

**Description**: Directory for storing SSH login key files<br />This field is left blank by default. 

**Example**:

- SSH key file example

```yaml
key_file: ~/.ssh/id_rsa
```

**Added in**: v0.1

### passphrase

**Data type**: String

**Description**: SSH login key file password<br />The value can be displayed in plain text and ciphertext. It is recommended that the value is encrypted by running `ptk encrypt`.<br />This field is left blank by default. 

**Added in**: v0.1

### conn_timeout

**Data type**: Duration

**Description**: SSH login connection timeout duration. The value unit can be minute or second. <br />The default value is **1m**. 

**Example**:

- SSH connection timeout example

```yaml
conn_timeout: 1m
```

```yaml
conn_timeout: 30s
```

**Added in**: v0.1

### exec_timeout

**Data type**: Duration

**Description**: Timeout duration for executing a single command in SSH mode<br />The default value is **10m**.

**Example**:

- SSH execution timeout example

```yaml
exec_timeout: 1m
```

```yaml
exec_timeout: 30s
```

**Added in**: v0.1

### proxy

**Data type**: <a href="#sshoption">SSHOption</a>

**Description**: Jump server login information. If the target server cannot be connected directly, you can use a jump server for connection. q

**Added in**: v0.1
