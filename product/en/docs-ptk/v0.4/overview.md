---
title: PTK Overview
summary: PTK Overview
author: Yao Qian
date: 2022-05-30
---

## PTK Overview

PTK (Provisioning Toolkit) is an installation and operation and maintenance tool for MogDB. It aims at quick installation of MogDB. 

If you need to run MogDB or its related components, only one command needs to be run. 

## Application Scenarios

- Developers need to quickly enable multiple local MogDB processes.  
- Users need to install MogDB quickly using PTK.
- Daily operation and maintenance by DBAs
- A third-party operation and maintenance platform needs to be integrated.

## Recommended Deployment Architecture

PTK is multi-cluster management software. As a centrally-controlling machine, it manages multiple database clusters remotely using SSH. Therefore, it is recommended that PTK is independently deployed a control server and the database is another server, as shown in the following figure. Certainly, PTK can be also deployed on the local server where the database is located. You can choose either of the deployment methods as required. 

```
                           +-----------+
                           |    PTK    |
                           +-----------+
                 /---ssh-----/   |    \---ssh----\
                /               ssh               \
               /                 |                 \
        +-----------+      +-----------+      +-----------+
        |   MogDB   |      |   MogDB   |      |   MogDB   |
        +-----------+      +-----------+      +-----------+
```

## Operating Systems Supported by PTK for Installing MogDB

You can run `ptk candidate os`  to check the latest OSs supported. 

| ID      | OS                                  | Tested Version            |
| ------- | ----------------------------------- | ------------------------- |
| 1007010 | CentOS Linux 7 (Core) (x86_64)      | 7.6.1810 (Core)           |
| 1008010 | Centos 8 (x86_64)                   | 8.0.1905 (Core)           |
| 1008020 | Centos 8 (arm64)                    | 8.0.1905 (Core)           |
| 1120010 | openEuler 20 (x86_64)               | 20.03 LTS                 |
| 1120020 | openEuler 20 (arm64)                | 20.03 LTS                 |
| 1122010 | openEuler 22 (x86_64)               | 22.03 LTS                 |
| 1122020 | openEuler 22 (arm64)                | 22.03 LTS                 |
| 1210010 | Kylin V10 (x86_64)                  | V10 (Tercel)              |
| 1210020 | Kylin V10 (arm64)                   | V10 (Tercel)              |
| 1320010 | UOS 20 A (x86_64)                   | 1002a/1020a/1050a         |
| 1320020 | UOS 20 A (arm64)                    | 1050a (kongzi)            |
| 1420010 | UOS 20 D/E (x86_64)                 | 1040d (fou)               |
| 1420020 | UOS 20 D/E (arm64)                  | 1040d (fou)               |
| 1520010 | Ubuntu 20 (x86_64)                  | 20.04.3 LTS (Focal Fossa) |
| 1522010 | Ubuntu 22 (x86_64)                  | 22.04 (Jammy Jellyfish)   |
| 1607010 | Red Hat Enterprise Linux 7 (x86_64) | 7.5 (Maipo)               |
| 1608010 | Red Hat Enterprise Linux 8 (x86_64) | 8.5 (Ootpa)               |
| 1702010 | EulerOS 2 (x86_64)                  | 2.0 (SP3)                 |
| 1702020 | EulerOS 2 (arm64)                   | 2.0 (SP3)                 |
| 1812010 | SLES 12SP5 (x86_64)                 | 12SP5                     |
| 1907010 | Oracle Linux 7 (x86_64)             | 7.9 (Maipo)               |
| 1908010 | Oracle Linux 8 (x86_64)             | 8.6 (Ootpa)               |
| 2008010 | Rocky Linux 8 (x86_64)              | 8.5 (Green Obsidian)      |
| 2107010 | NeoKylin V7 (x86_64)                | V7Update6                 |
| 2222010 | FusionOS 22 (x86_64)                | 22.0.2                    |
| 2222020 | FusionOS 22 (arm64)                 | 22.0.2                    |

*For other OSs, the adaptation tests are in progress.*