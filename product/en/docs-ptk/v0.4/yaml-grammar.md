---
title: Appendix-YAML Syntax
summary: Appendix-YAML Syntax
author: Yao Qian
date: 2022-09-16
---

# Appendix: YAML Syntax

PTK uses the configuration file in the YAML format because YAML is simple in syntax, intuitive in data structure, and convenient in comment addition. 

> **Note**: Spaces are used to show the hierarchical structure in YAML. Therefore, during configuration editing, pay attention to indentation. 

This document describes the YAML syntax to help users understand the PTK configuration file format easily. 

## Scalar and Collection

According to [YAML specification](https://yaml.org/spec/1.2/spec.html), there are two types of collections and multiple types of scalars. 

The two types of collections include map and sequence.

```yaml
map:
  one: 1
  two: 2
  three: 3

sequence:
  - one
  - two
  - three
```

The scalar value is a single value (which is contrary to the collection).

## Scalar Type in YAML

An integer or floating-point number is usually considered numeric if it is not quoted.  

```yaml
count: 1
size: 2.34
```

If An integer or floating-point number is quoted, it is considered a string. 

```yaml
count: "1" # <-- string, not int
size: '2.34' # <-- string, not float
```

Boolean is the same.

```yaml
isGood: true   # bool
answer: "true" # string
```

An empty string is called `null`  not `nil`.

**Note**: `port: "80"` is legal. It is possible to pass values through the template engine and YAML interpreter, but if PTK expects the port to be an integer, it will fail.

In some cases, YAML node tags can be used to force  inference of a specific type.

```yaml
coffee: "yes, please"
age: !!str 21
port: !!int "80"
```

As shown above, the `!!str` tells the interpreter that `age` is a string, even though it looks like an integer. Even if `port` is enclosed in quotes, it is treated as an integer.

## String in YAML

The data type of most data in YAML is string. There are multiple string methods in YAML. This section describes these methods and introduces how to use some of the methods. 

The following shows three single-row methods to declare a string. 

```yaml
way1: bare words
way2: "double-quoted strings"
way3: 'single-quoted strings'
```

Single row indicates that all characters must be put in one line. 

- Bare words are not quoted and escaped. Therefore, use characters with caution. 

- A double-quoted string  can have specified characters escaped with a backslash. For example, you can use `\n` to wrap lines for `"\"Hello\", she said"`.
- A single-quoted string is not escaped, and only the single quotes need to be escaped with `'`.

Additionally, you can declare a multi-row string. 

```yaml
coffee: |
  Latte
  Cappuccino
  Espresso
```

The above refers to the string value of `coffee`, which is equivalent to `Latte\nCappuccino\nEspresso\n`.

The second line following `|` must be correctly indented. 

```yaml
coffee: |
         Latte
  Cappuccino
  Espresso

```

As shown in the above example, `Latte` is indented incorrectly. As a result, the following error is reported:

```shell
Error parsing file: error converting YAML to JSON: yaml: line 7: did not find expected key
```

In a template, adding a line similar to "# commented first line" in multi-row text can avoid this error. 

```yaml
coffee: |
  # Commented first line
         Latte
  Cappuccino
  Espresso

```

No matter what the first line is, it will be saved and output to a string. If you need to output the file text to a configuration mapping, the comment should be the type required for reading this item.

### Spaces in a Multi-Row String

In the above example, `|` is used to express a multi-row string. Note that the string is followed by `\n`. If you need to remove the line break using the YAML processor, add `-` behind `|`.

```yaml
coffee: |-
  Latte
  Cappuccino
  Espresso
```

Here, the value of `coffee` is `Latte\nCappuccino\nEspresso` without `\n` at the end.

In some cases, you may hope to reserve the spaces behind `|`, use `|+`.

```yaml
coffee: |+
  Latte
  Cappuccino
  Espresso


another: value
```

Here, the value of `coffee` is `Latte\nCappuccino\nEspresso\n\n\n`.

The indentation and line break will be reserved in the text.

```yaml
coffee: |-
  Latte
    12 oz
    16 oz
  Cappuccino
  Espresso
```

In the above example, the value of `coffee` is `Latte\n  12 oz\n  16 oz\nCappuccino\nEspresso`.

### Collapsing a Multi-Row String

Sometimes you need to show a string in multiple rows, but it can be interpreted as a long row. This is called collapse. To express a collapsed block, replace `|` with `>`.

```yaml
coffee: >
  Latte
  Cappuccino
  Espresso


```

In the above example, the value of `coffee` is `Latte Cappuccino Espresso\n`. Note that all line breaks will be converted to spaces except the last one. 

You can use `>-` to combine the space symbol and collapsing symbol to replace or cancel all new rows.

In the collapsing syntax, indenting a text will reserve a symbol.

```yaml
coffee: >-
  Latte
    12 oz
    16 oz
  Cappuccino
  Espresso
```

The value of `coffee` is `Latte\n  12 oz\n  16 oz\nCappuccino Espresso`. Both the space and line break are reserved as part of the string.

## YAML Is the Superset of JSON

YAML is the superset of JSON, any legal JSON file should be a legal YAML file.

```json
{
  "coffee": "yes, please",
  "coffees": [
    "Latte", "Cappuccino", "Espresso"
  ]
}
```

Another expression method of the above JSON is as follows:

```yaml
coffee: yes, please
coffees:
- Latte
- Cappuccino
- Espresso
```

The two expression methods can be together used, therefore, use them with caution. 

```yaml
coffee: "yes, please"
coffees: [ "Latte", "Cappuccino", "Espresso"]
```

All three types can be interpreted into the same meaning.