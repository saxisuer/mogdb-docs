---
title: Viewing Help Information
summary: Viewing Help Information
author: Yao Qian
date: 2022-05-30
---

## Viewing Help Information

After PTK is successfully installed, you can run `ptk -h` to view the help information. For details about command parameters, see [PTK Command Reference}](../command.md)

```
PTK is a command line tool for deploying and managing the MogDB database cluster. 

Usage:
  ptk [flags] <command> [args...]
  ptk [command]

Available Commands:
  env         Prints PTK-loaded environment variable values.
  gen-om-xml  Generates the gs_om XML configuration file.
  self        Operates with the PTK self installation package.
  version     Prints the PTK version.
  help        Prints the PTK help information.
  completion  Generate the autocompletion script for the specified shell

Pre Install Commands:
  candidate   Prints the PTK-supported software version list. 
  download    Downloads the MogDB installation package online. 
  checkos     Checks whether the cluster server system meets database installation requirements. 
  encrypt     Provides a convenient method of encrypting your text or password. 
  template    Prints the configuration template.

Install Commands:
  install     Configures and deploys the MogDB database cluster based on the given topology. 
  uninstall   Uninstalls the MogDB database cluster.

Post Install Commands:
  ls          Lists all MogDB clusters.
  cluster     Manages the database cluster.
Experimental Commands:
  register    注册PTK内部类型满足特定需求
Flags:
  -h, --help                Prints the PTK help information.
      --log-file string     Specifies a path of the run log file
      --log-format string   Specifies the output format of the run log. The value can be text or json. The default value is text. 
      --log-level string    Specifies the run log level. The value can be debug, info, warning, error, and panic. The default value is info.
  -v, --version             Prints the PTK version.

Use "ptk [command] --help" for more information about a command.
```

## Viewing Version

You can run `ptk -v` to view PTK version. 

```shell
$ ptk -v
PTK Version: v0.4.0
Go Version: go1.17.1
Build Date: 2022-09-14T09:32:14Z
Git Hash: a9xa9e
```

The output includes the PTK version, Golang version in compilation, build time, and git hash value. 

## More

- [Checking the System](./usage-chekos.md)
- [Creating a Configuration File](./usage-config.md)
- [Installing a Database](./usage-install.md)
- [Operating with a Database](./usage-operation.md)
- [Scale-In/Out](./usage-scale.md)
- [Installing Extensions](./usage-install-plugin.md)
- [Uninstalling a Database Cluster](./usage-uninstall.md)
- [Commenting a Cluster](./usage-comment.md)
- [Upgrading PTK](./usage-self-upgrade.md)
