---
title: Creating a Configuration File
summary: Creating a Configuration File
author: Yao Qian
date: 2022-07-30
---

# Creating a Configuration File

## Manual Configuration

Before installing the database, PTK can be used to create a configuration file for defining the expected database cluster topology.

To install a single-instance database, run the following command to generate a configuration file.

```shell
ptk template -l > config.yaml
```

To install a multi-instance cluster, run the following command to generate a configuration file.

```shell
ptk template > config.yaml
```

After PTK is run, a `config.yaml` file is generated in the current folder.  You need to modify db_server-related fields. 

If there is the password field, you need to run [ptk encrypt](../commands/ptk-encrypt.md) to encrypt the password and then add the ciphertext to the configuration file.

> **Note**: 
> If you hope to use a non-root user to connect the target server, make sure that the user has the sudo permission on the target server. 
>
> Configuration method: Add the following content (replacing `USERNAME` with SSH connection user name) to the  `sudoers` files in the `/etc` directory: 
> `[USERNAME]  ALL=(ALL)  NOPASSWD:ALL`

## Interactive Creation

PTK also supports interactive creation of a configuration file. You can perform the following command to perform interactive creation:

```shell
ptk template create
```

After the command is run, the interactive terminal is displayed. You need to only reply the questions in sequence. 

**Note**: During db_server configuration, if the SSH connection information is the same, it can be used repeatedly. 

After the interactive terminal exits, a `config.yaml` file will be automatically created in the current folder. 

### Example

The default values prevail for most fields by default. The password field value needs to be entered. 

The following is an example of a configuration file where one primary instance and one standby instance are deployed. Assume that the primary instance is deployed on the local server and the IP address of the standby instance is  `192.168.1.100`.

```shell
$ ptk template create
Please enter the cluster name: (default: cluster_igiltry)
Please enter the system user name: (default: omm)
Please enter the user group name: (default: omm)
Please enter the initial password of the database (8 to 16 characters):
Please enter your password again: 
Please enter the listening port of the database (value range: 1024 to 65535 ): (default: 26000)
Please enter the listening port of the CM sever (value range: 1024 to 65535): (default: 15300)
Please enter the directory for database installation (make sure the directory is empty): (default: /opt/mogdb)
Next, let us add some database instance servers
================db server 1================
Please enter the IP address of the server (support for only IPv4): 127.0.0.1
Please choose a database role: 
  1: primary
  2: standby
  3: cascade_standby
Please enter the number: 1
Please enter the name of the database Az (available zone): (default: AZ1)
================ end ================
Do you want to add another db server?[Y|Yes](default=N) y
================db server 2================
Please enter the IP address of the server (support for only IPv4):  192.168.1.100
Please choose a database role:
  1: primary
  2: standby
  3: cascade_standby
Please enter the number:  2
Please enter the name of the database Az (available zone): (default: AZ1)
[SSH] Please enter the SSH login user name: (default: root)
[SSH] Please enter the SSH port: (default: 22)
[SSH] Please choose an verification mode: 
  1: Password
  2: KeyFile
Please enter the number:  1
[SSH] Please enter the SSH login password: 
================ end ================
Do you want to add another db server?[Y|Yes](default=N) n
Generate /Users/vimiix/Home/enmotech/ptk/ptk_repo/config.yaml successfully
```

## Configuration File Field Description

The following is an example of a completed configuration file:

```yaml
global:
  # # cluster name
  cluster_name:
  # # system user for running db
  user: omm
  # # system user group, same as username if not given
  group: omm
  # # system user password, use 'ptk encrypt' to encrypt it if not empty
  # user_password: ""
  # # database password, use 'ptk encrypt' to encrypt it if not empty
  # db_password: ""
  # # database port
  db_port: 26000
  # # cm_server port (Effective after mogdb3.0)
  # cm_server_port: 15300
  # # base directory for install MogDB server,
  # # if any of app_dir，data_dir，log_dir and tool_dir not config，
  # # PTK will create corresponding directory under base_dir
  base_dir: /opt/mogdb
  # # application directory
  # app_dir: /opt/mogdb/app
  # # log directory
  # log_dir: /opt/mogdb/log
  # # data directory
  # data_dir: /opt/mogdb/data
  # # tool directory
  # tool_dir: /opt/mogdb/tool
  # # temporary directory
  tmp_dir: /tmp
  # # corefile directory
  # core_file_dir: 
  # # gs_initdb custom options
  gs_initdb_opts:
  - --encoding=UTF-8
  - --dbcompatibility=A
  cm_option:
    dir: /opt/mogdb/cm
    cm_server_port: 15300
    db_service_vip: ""
    cm_server_conf:
      # # support all fields in cm_server.conf
      # instance_heartbeat_timeout: 2
      # ...
    cm_agent_conf:
      # # support all fields in cm_agent.conf
      # log_max_size: 10240
      # ...

db_servers:
  - host: 10.0.1.100
    # # database port
    db_port: 26000
    # # ip list for database replication, use host default
    # ha_ips:
    # - 10.0.2.100
    # # port for database replication, use db_port plus 1 default
    ha_port: 26001
    # # database role, options: primary, standby, cascade_standby
    role: primary
    # # available zone name
    az_name: AZ1
    # available zone priority
    az_priority: 1
    # # upstream host ip for cascade_standby instance
    upstream_host:
    # # parameters in postgresql.conf
    db_conf:
      # replication_type: 1
    # # SSH connection config，use global.ssh_option if not config
    # ssh_option:
    #    port: 22
    #    user: root
    #    password: 
    #    key_file: 
    #    passphrase: 
    #    conn_timeout: 5s
    #    exec_timeout: 1m
    #    proxy:
    #      host: 
    #      user: 
    #      password: 
    #      key_file: 
    #      passphrase: 
    #      conn_timeout: 5s
    #      exec_timeout: 1m
```

The configuration file includes two parts, including `global` and `db_servers`.

- **global**: describes the common information of all instances in a cluster. 
- **db_servers**: shows the instance list. Each element represents an instance. 

### Global Configuration

Field Name | Description | Value Type | Default Value | Mandatory or Not | Remarks 
------|-------|-----|------|------ | ------
cluster_name | Cluster name | String | `cluster_` is the prefix of a cluster name by default. | Yes |
user | Operating system user | String | omm | No |
group | Operating system user group | String | omm | No |
user_password | Operating system user password | String | Null | No | The value needs to be encrypted by running the `encrypt` command. 
db_password | Database password | String | Null | No | The value needs to be encrypted by running the encrypt command. If the field is not set, it can be set during installation. 
db_port | Listening port of a database | Integer | 26000 | No |
base_dir | Base directory for database installation | String | /opt/mogdb | No | If this field is set, the app_dir, log_dir, data_dir, tool_dir, cm_dir, and tmp_dir fields can be optional and automatically put in the specified directory of this field. 
app_dir | Application directory of a database | String | /opt/mogdb/app | No |
log_dir | Database log directory | String | /opt/mogdb/log | No |
data_dir | Database data directory | String | /opt/mogdb/data | No |
tool_dir | Database tool directory | String | /opt/mogdb/tool | No |
tmp_dir | Directory of the temporary database file | String | /tmp | No |
core_file_dir | Directory of the system core file | String | Null | No | If this field is set, the `kernel.core_pattern` value will be changed. 
gs_initdb_opts | gs_initdb parameters for database initialization | String list | no | An element maps a parameter. If the `--locale` parameter is not set, PTK will set the element to `--no-locale` by default. |
ssh_option | SSH connection authentication   information | [ssh_option](#ssh_option) | no | If an instance needs to be  connected remotely but the instance-level ssh_option is not configured, the field in the global will prevail. |
cm_option | CM configuration parameter | [cm_option](#cm_option) | no                                                     | The field takes effect when `--install-cm` is specified during installation. |

### db_servers Configuration

Field Name | Description | Value Type | Default Value | Mandatory or Not | Remarks 
------|-------|-----|------|------ | ------
host | IP address of an instance | String | Null | Yes |
db_port | Instance port | Integer | 26000 | No | If this field is not configured, the field in the global will prevail. 
ha_ips | HA IP address list for instance log replication | String list | Null | No |
ha_port | HA port for log replication | Integer | 26001 | No |
role | Database instance role | String | Null | No | The value can be `primary`, `standby`, and `cascade_standby`. If this field is not configured, an instance in a cluster will be chosen as the primary instance randomly. Other instances will become standby instances. 
upstream_host | IP address of the upstream standby of the cascaded standby | String | Null | Mandatory for a cascaded standby |
az_name | AZ name | String                    | AZ1 | No |
az_priority | AZ priority                                                | Integer | 1 | The smaller the value, the higher the priority. |
xlog_dir | xlog log directory | String | Null | The xlog will be put in the pg_log of the `log_dir` directory. This field cannot be configured under the data directory. Otherwise, an error will occur during database starting. |
db_conf | Database GUC parameter configuration | Dictionary | Null |The dictionary key is a parameter in postgresql.conf.|
ssh_option | SSH connection authentication for connecting an instance | [ssh_option](#ssh_option) | no | If this field is not configured, the field in the global will prevail. |

### cm_option

| Field Name     | Description                                            | Value Type | Default Value | Mandatory  or Not | Remarks                                                      |
| -------------- | ------------------------------------------------------ | ---------- | ------------- | ----------------- | ------------------------------------------------------------ |
| dir            | CM installation directory                              | String     | $base_dir/cm  | Yes               |                                                              |
| cm_server_port | Listening port of the CM server                        | Integer    | 15300         | No                |                                                              |
| db_service_vip | Virtual IP address (which needs to be supported by CM) | String     | Null          | No                |                                                              |
| cm_server_conf | CM sever configuration parameter                       | Dictionary | Null          | No                | The dictionary key of the field supports any parameter of cm_server. PTK will set this value in cm_server.conf . |
| cm_agent_conf  | CM agent configuration parameter                       | Dictionary | Null          | No                | The dictionary key of the field supports any parameter of cm_agent. PTK will set this value in cm_agent.conf . |

### ssh_option

Field Name | Description | Value Type | Default Value | Mandatory  or Not | Remarks 
------|-------|-----|------|------ | ------
host | IP address of a host | String | IP address of an instance | No | If a host is to be configured as a proxy jump server, this field is mandatory. 
port | SSH connection port | Integer | 22 | No |
user | SSH connection user | String | root | No | If the connection user is not root, make sure that the user has the sudo permission without the code. 
password | SSH connection password | String | Null | No | The value needs to be encrypted by running the `encrypt` command. 
key_file | SSH connection key file | String | Null | No |
passphrase | Password of the SSH connection key file | String     | Null | No |
conn_timeout | SSH connection timeout duration | Duration | 1m | No |
exec_timeout | Command execution timeout duration | Duration | 10m | No |
proxy | Jump server configuration | ssh_option | Null | No | The jump server can be connected for only once. 
