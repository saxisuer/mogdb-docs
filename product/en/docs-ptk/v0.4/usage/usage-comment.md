---
title: Cluster Comment
summary: Cluster Comment
author: Yao Qian
date: 2022-09-15
---

# Commenting a Cluster 

PTK support the `--comment` parameter for commenting a cluster during database cluster installation. 

After successful installation, the comment information can be displayed in the cluster list by running `ls`.

## Comment Modification

Run the following command to modify the comment: 

```shell
ptk cluster -n CLUSTER_NAME modify-comment --comment "new comment"
```
