---
title: Cluster Install MogHA
summary: Cluster Install MogHA
author: Yao Qian
date: 2022-09-15
---

# Installing MogHA

PTK supports the MogHA installation process in prerequisite that the user provides a legal MogHA configuration file. 

> In the current version, PTK does not verify the MogHA configuration file but deploys the file committed by the user to the cluster server directly. Therefore, the user needs to guarantee the configuration file accuracy. 

MogHA supports the lite and non-lite modes. The lite mode supports deployment of MogHA on the primary server and synchronous standby servers. The non-lite mode supports deployment of MogHA on all nodes in a cluster. 

## Generation of Configuration File Template 

PTK provides a template command to help users automatically generate a MogHA configuration file. 

```shell
ptk template mogha -n CLUSTER_NAME [-o node.conf] [--port 8081]
```

## Installation Mode

```shell
ptk cluster -n CLUSTER_NAME install-mogha -d INSTALL_DIR -c node.conf [-p MOGHA_PACKAGE]
```
