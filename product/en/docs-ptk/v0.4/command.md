---
title: PTK Command Reference
summary: PTK Command Reference
author: Yao Qian
date: 2022-06-28
---

## PTK

PTK is a command line tool for deploying and managing MogDB database clusters. 

## Syntax

```shell
ptk [flags] <command> [args...]
```

## Global Options

Global options can be applied to any subcommand of PTK. 

### -f, --file string

- Specifies the cluster configuration file. 
- Data type: String
- During database installation, you need to specify the configuration file path by running the [ptk template](commands/ptk-template.md) command. 

### --log-file string

- Specifies the file path to which the run log is stored.
- Data type: String
- The parameter is left blank by default. If this parameter is specified, PTK will export logs to a specified file.

### --log-format string

- Specifies the log export format. 
- Data type: String
- The PTK log export format supports only `text` and `json`.
- Default value: **text**

### --log-level string

- Specifies the log export level. 
- Data type: String
- The supported log level includes debug, info, warning, error, and panic.
- Default value: **info**

### -v, --version

- Displays the PTK version. 
- Data type: Boolean
- Default value: **false**

### -h, --help

- Outputs the help information. 
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command. 

## More Subcommands

* [ptk candidate](./commands/ptk-candidate.md) - Prints the list of software versions supported by PTK. 
* [ptk checkos](./commands/ptk-checkos.md) - Checks whether the cluster server dependencies meet the database installation requirements. 
* [ptk encrypt](./commands/ptk-encrypt.md) - Provides a quick method of encrypting your text or password. 
* [ptk env](./commands/ptk-env.md) - Prints PTK environment variables. 
* [ptk install](./commands/ptk-install.md) - Deploys the MogDB database cluster based on the given topology configuration. 
* [ptk ls](./commands/ptk-ls.md) - Lists all MogDB database clusters. 
* [ptk self](./commands/ptk-self.md) - Operates with the PTK installation package. 
* [ptk cluster](./commands/ptk-cluster.md) - Operates with the database cluster. 
* [ptk template](./commands/ptk-template.md) - Prints the configuration template. 
* [ptk uninstall](./commands/ptk-uninstall.md) - Uninstalls the MogDB database cluster. 
* [ptk gen-om-xml](./commands/ptk-gen-om-xml.md) - Generates the gs_om XML configuration file. 
* [ptk version](./commands/ptk-version.md) - Prints the PTK version. 
* [ptk register](./commands/ptk-register.md) - Registers the system built-in type with PTK. 
