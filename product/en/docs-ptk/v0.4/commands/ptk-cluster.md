---
title: ptk cluster
summary: ptk cluster
author: ptk
date: 2022-06-28
---

## ptk cluster

ptk cluster operates with database cluster management. 

## Syntax

```shell
ptk cluster [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
