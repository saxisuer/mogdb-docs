---
title: ptk cluster refresh
summary: ptk cluster refresh
author: ptk
date: 2022-08-01
---

# ptk cluster refresh

ptk cluster refresh refreshes cluster configuration. 

## Syntax

```shell
ptk cluster refresh [flags]
```

## Option

### -n, --name string

- Specifies the cluster name.
- Data tｙｐｅ：　Sｔｒｉｎｇ

### --replace-ip stringArray

- Replaces the db_server IP address mapping string. The format is old_ip=new_ip.
- Data type: String
- This parameter can be specified multiple times.
