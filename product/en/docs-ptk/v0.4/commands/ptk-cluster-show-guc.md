---
title: ptk cluster show-guc
summary: ptk cluster show-guc
author: ptk
date: 2022-08-01
---

# ptk cluster show-guc

ptk cluster show-guc queries database parameters. 

## Syntax

```shell
ptk cluster show-guc [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -k, --key strings

- Specifies the name of the parameter to be queried. 
- Data type: String
- The different parameters can be specified multiple times. 
