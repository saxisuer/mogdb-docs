---
title: ptk candidate cpu
summary: ptk candidate cpu
author: ptk
date: 2022-9-14
---

## ptk candidate cpu

ptk candidate cpu lists the supported CPU models. 

## Syntax

```shell
ptk candidate cpu [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs a table containing the `cpu model` field. 
