---
title: ptk gen-om-xml
summary: ptk  gen-om-xml
author: ptk
date: 2022-06-28
---

## ptk  gen-om-xml

ptk  gen-om-xml generates an XML configuration file used by gs_om.

## Syntax

```shell
ptk gen-om-xml -f CONFIG_FILE [flags]
```

## Option

### -o, --output string

- Specifies the file path. 
- Data type: String
- Default value: `config_[timestamp].xml` 

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
