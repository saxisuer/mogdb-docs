---
title: ptk checkos
summary: ptk checkos
author: ptk
date: 2022-6-28
---

## ptk checkos

ptk checkos checks whether the system parameters or dependencies of servers in a cluster meet database installation requirements. 

When the check result includes warnings or exceptions, PTK will automatically generate a shell repair script `root_fix_os.sh` in the current folder. The script includes suggested repair code for abnormal check items. You can copy the code to the terminal to execute it.

**Note**: If multiple servers are deployed, the script includes repair suggestions of all servers. You need to copy the suggested code to the corresponding server for execution. 

PTK supports the following check items: 

- A1: Operating system version
- A2: Kernel version
- A3: Character set encoding
- A4: Time zone
- A5: Memory information
- A6: System control parameter
- A7: File system configuration
- A8: Disk configuration
- A9: Preread block size
- A10: IO scheduling configuration
- A11: NIC setting
- A12: Time clock consistency
- A13: Firewall status
- A14: Transparent huge page configuration
- A15: System dependencies
- A16: CPU command set

All check items are checked by default. If you need to check certain check item, use `-i` to specify the specific item number. To specify multiple items, separate their item numbers with commas.

## Syntax

```shell
ptk checkos [flags]
```

## Option

### -i, --item string

- Specifies the check item. To specify multiple items, separate their item numbers with commas.
- Data type: String
- This option checks all check items by default. 

### -o, --output string

- Specifies the file path for storing the check result. 
- Data type: String
- This option is left blank by default. The check result is printed in the terminal window.

### --detail

- Displays details of each check item. 
- Data type: Boolean
- This option is disabled by default and enabled by being added to the command line.

### --gen-warning-fix

- Outputs repair suggestions for abnormal and warning errors.
- Data type: Boolean
- The option is disabled and outputs repair suggestions for only abnormal errors by default.

### --only-abnormal-detail

- Displays only abnormal errors.
- Data type: Boolean
- The option is disabled by default, includes warning and abnormal-related errors, and is enabled by being added to the command line.

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs check process logs and the check result table.

The check result is categorized into the following:

- `Normal`: No operation is needed. 
- `Warning`: needs to be processed as required. No processing does not influence installation and  use. But it is not best practice. 
- `Abnormal`: needs to be fixed according to the suggested script. Otherwise, the database may fail to be started.
- `ExecuteError`: Execution errors. It is recommended that it is reported to engineers for processing. 

The check result table includes the following fields:

- Item: Check item name
- Level: Check result level 
- (Message): This field is not displayed by default and is displayed by adding the `--detail` option. It displays detailed exceptions of different servers.
