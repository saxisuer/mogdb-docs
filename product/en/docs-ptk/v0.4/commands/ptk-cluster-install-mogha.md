---
title: ptk cluster install-mogha
summary: ptk cluster install-mogha
author: ptk
date: 2022-08-01
---

## ptk cluster install-mogha

ptk cluster install-mogha installs the MogDB plugin. 

## Syntax

```shell
ptk cluster -n CLUSTER_NAME install-mogha [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -d, --install-dir string

- Specifies the MogHA installation directory. 
- Data type: String

### -p, --mogha-pkg string

- Specifies the local path of the MogHA installation package. 
- Data type: String

### -c, --node-conf string

- Specifies the path of the MogHA configuration file. 
- Data type: String

### --skip-start

- Skips the MogHA start-up operation. 
- Data type: Boolean
