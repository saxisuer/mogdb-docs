---
title: ptk completion zsh
summary: ptk completion zsh
author: ptk
date: 2022-06-28
---

## ptk completion zsh

ptk completion zsh generates the automatic completion script of zsh shell.

If the shell completion function is not enabled, you can run the following command to enable it.

```shell
echo "autoload -U compinit;compinit" >> ~/.zshrc
```

Make the script take effect by running the command in the shell session. 

```shell
source <(ptk completion zsh); compdef _ptk ptk
```

Run the command in each session to make the script take effect. 

**Linux:**

```shell
ptk completion zsh > "${fpath[1]}/_ptk"
```

**macOS:**

```shell
ptk completion zsh > /usr/local/share/zsh/site-functions/_ptk
```

You need to start new shell to make the setting take effect. 

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
