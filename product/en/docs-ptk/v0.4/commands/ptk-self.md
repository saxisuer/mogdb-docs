---
title: ptk self
summary: ptk self
author: ptk
date: 2022-06-28
---

## ptk self

ptk self prints PTK environment variables.

## Syntax

```shell
ptk self [command]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Subcommand

[ptk self upgrade](ptk-self-upgrade.md): PTK upgrade