---
title: ptk uninstall
summary: ptk uninstall
author: ptk
date: 2022-06-28
---

## ptk uninstall

ptk uninstall uninstalls a MogDB database cluster. 

## Syntax

```shell
ptk uninstall [flags]
```

## Option

### -n, --name string

- Specifies the name of the cluster to be uninstalled. 
- Data type: String

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Example

```shell
# Uninstall a database by specifying a cluster name
ptk uninstall --name CLUSTER_NAME
```

## Output

Outputs the uninstallation operation log.
