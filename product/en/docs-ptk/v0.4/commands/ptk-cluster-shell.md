---
title: ptk cluster shell
summary: ptk cluster shell
author: ptk
date: 2022-08-01
---

# ptk cluster shell

ptk cluster shell performs the shell command or script. It no parameter is specified, the terminal mode is opened. 

## Syntax

```shell
ptk cluster shell [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -c, --command string

- Specifies the shell command to be performed. 
- Data type: String

### -H, --host stringArray

- Specifies the IP address of the target server. The operation is performed on all servers in a cluster by default. 
- Data type: String
- This parameter can be performed multiple times. 

### -s, --script string

- Specifies the path of the shell script to be executed. 
- Data type: String
