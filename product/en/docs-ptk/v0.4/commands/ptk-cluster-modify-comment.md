---
title: ptk cluster modify-comment
summary: ptk cluster modify-comment
author: ptk
date: 2022-08-01
---

## ptk cluster modify-comment

ptk cluster modify-comment modifies the cluster comment. 

## Syntax

```shell
ptk cluster -n CLUSTER_NAME modify-comment [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### --comment string

- Specifies the new comment. 
- Data type: String
