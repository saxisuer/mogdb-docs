---
title: ptk template mogha
summary: ptk template mogha
author: ptk
date: 2022-08-01
---

# ptk template mogha

ptk template mogha generates the MogHA configurations. 

## Syntax

```shell
ptk template mogha [flags]
```

## Option

### -n, --cluster-name string

- Specifies the cluster name. 
- Data type: String

### -o, --output string

- Specifies the output file. 
- Data type: String

### --port int

- Specifies the port to be listened by MogHA. 
- Data type: Integer
- Default value: **8081**
