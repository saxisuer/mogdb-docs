---
title: ptk env
summary: ptk env
author: ptk
date: 2022-06-28
---

## ptk env

ptk env prints PTK environment variables. 

## Syntax

```shell
ptk env [key1 key2 ... keyN] [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs values of environment variables in the key-value format.