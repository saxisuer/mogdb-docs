---
title: ptk cluster show-hba
summary: ptk cluster show-hba
author: ptk
date: 2022-08-01
---

# ptk cluster show-hba

ptk cluster show-hba queries the database pg_hba information. 

## Syntax

```shell
ptk cluster show-hba [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -H, --host string

- Specifies the IP address of the instance to be queried. 
- Data type: String
- The configuration of the primary database is queried by default. 
