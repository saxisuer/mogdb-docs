---
title: ptk cluster scale-out
summary: ptk cluster scale-out
author: ptk
date: 2022-08-01
---

## ptk cluster scale-out

ptk cluster scale-out performs the scale-out operation on a MogDB cluster. 

## Syntax

```shell
ptk cluster -n CLUSTER_NAME scale-out -c add.yaml [--force] [--skip-check-distro] [--skip-check-os] [--skip-create-user]

```

## Option

### -n, --name string

- Specifies the name of a cluster to be operated with. 
- Data type: String

### -c, --config string

- Specifies the scale-out configuration file path. 
- Data type: String

### --default-guc

- Uses the default GUC parameters, which are not optimized automatically. 
- Data type: Boolean

### --force

- Performs the scale-out operation again meanwhile PTK clears old data if scale-out fails or is interrupted.  
- Data type: Boolean

### --gen-template

- Generates a scale-out template. 
- Data type: Boolean

### --skip-check-distro

- Skips system distro check. 
- Data type: Boolean

### --skip-check-os

- Skips system check. 
- Data type: Boolean

### --skip-create-user

- Skips user creation. 
- Data type: Boolean

### -t, --timeout duration

- Specifies the execution timeout duration.
- Data type: Duration
- The unit supported by the duration type includes h, m, s, and ms. 
- Default value: **10m**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs an operation log.
