---
title: ptk template scale-out
summary: ptk template scale-out
author: ptk
date: 2022-08-01
---

# ptk template scale-out

ptk template scale-out generates cluster scale-out configuration. 

## Syntax

```shell
ptk template scale-out [flags]
```

## Option

### -o, --output string

- Specifies the output file. 
- Data type: String
