---
title: ptk cluster failover
summary: ptk cluster failover
author: ptk
date: 2022-08-01
---

# ptk cluster failover

ptk cluster failover queries database parameter information. 

## Syntax

```shell
ptk cluster failover [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -H, --host string

- Specifies the IP address of the instance on which the failover is to be performed. 
- Data type: String
