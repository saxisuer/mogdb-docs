---
title: ptk completion fish
summary: ptk completion fish
author: ptk
date: 2022-06-28
---

## ptk completion fish

ptk completion fish generates the automatic completion script of fish.

Make the script take effect by running the command in the shell session. 

```shell
ptk completion fish | source
```

Run the command in each session to make the script take effect.

```shell
ptk completion fish > ~/.config/fish/completions/ptk.fish
```

You need to start new shell to make the setting take effect.

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
