---
title: ptk cluster show-config
summary: ptk cluster show-config
author: ptk
date: 2022-08-01
---

# ptk cluster show-config

ptk cluster show-config shows the topology configuration of a specified cluster. 

## Syntax

```shell
ptk cluster show-hba [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String 
