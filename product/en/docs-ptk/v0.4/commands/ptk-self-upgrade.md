---
title: ptk self upgrade
summary: ptk self upgrade
author: ptk
date: 2022-06-28
---

## ptk self upgrade

ptk self upgrade downloads and automatically installs PTK of the latest version. 

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs the upgrade operation log, including the package of the latest version and performs the upgrade. When `upgrade ptk successfully` is displayed, the upgrade is successful. 

You can run the [ptk version](ptk-version.md) command to query the version after the upgrade. 