---
title: ptk cluster switchover
summary: ptk cluster switchover
author: ptk
date: 2022-08-01
---

# ptk cluster switchover

ptk cluster switchover queries database parameters. 

## Syntax

```shell
ptk cluster switchover [flags]
```

## Option

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### -H, --host string

- Specifies the IP address of the instance to be worked as the primary database. 
- Data type: String
