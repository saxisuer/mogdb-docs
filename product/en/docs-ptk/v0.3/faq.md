---
title: FAQ
summary: FAQ
author: Yao Qian
date: 2022-05-30
---

## FAQ

### What Can I Do If the Error "can not combine with 'abrt-hook-ccpp'" Is Reported?

You can use the abrt(automatic bug report too) service agent to dump the core file. This may lead to core file loss or database downtime. 

*[What is the abrt service](https://abrt.readthedocs.io/en/latest/howitworks.html)*

You can fix this bug by modifying the `kernel.core_pattern` parameter.

```shell
# The value can be customized based on the actual requirement.
sysctl -w kernel.core_pattern=/var/log/coredump/%e.%p.%u.%t.core
```

### Do I Need to Pre-Install Python3?

Yes. 

Python 3.6 or Python 3.7 needs to be pre-installed because the psutil (python system and process utilities) dynamic library in the OM package supports only this two versions.

> PTK and MogDB themselves do not depend on Python3 but gs_om and gs_guc do. Therefore, Python needs to be pre-installed to make sure that the environment meets the requirement. 

### Can I Install a Database on an Operating System that Is Not Supported by PTK? 

Yes.

PTK itself provides a completed and general installation process. It automatically judges the database installation package to be downloaded based on the customer system by default. However, PTK cannot determine the installation package that is really needed when the operating system or operating system version is not supported by PTK. In this case, you can use the `-p` parameter in the `ptk install` command to specify an installation package. In view of the fact that the installation is not tested, it may fail. This is normal.

When you try to choose an installation package to adapt to your system, you can comply with the following rules:

1. Make judgment based on the CPU command set architecture. 

    ```shell
    uname -m
    ```

    For `arm64/aarch64`, use the openEuler arm64 installation package.

    For `x86_64`, perform the following operation make judgment. 

2. Make judgment based on the glibc version.

    ```shell
    getconf GNU_LIBC_VERSION
    ```

    If the glibc version is 2.17 or earlier than 2.17, use the centos7 x86_64 installation package. Otherwise, use the openEuler x86_64 installation package.

### Is gs_om Still Supported After a Database Is Installed Using PTK?

Yes.

The cluster installed using PTK is compatible with gs_om. After the database is installed, gs_om dependent static files generate automatically. 

> **Note**: `gs_om` needs user SSH mutual-trust between nodes. After the database is installed using PTK, user SSH mutual-trust is generally established. If it is not established, you need to manually perform the establishment. 

### What Should I Do If the Error `FATAL: semctl(578486307, 7, SETVAL, 0) failed: Invalid argument` Is Reported?

You can check whether the value of `RemoveIPC` in the `/etc/systemd/logind.conf` directory is `no`. If it is, restart the server. 

### How Can I Query the MogDB Versions Supported by PTK?

You can run the `ptk candidate db` command to query the MogDB versions supported by PTK.

### How Can I Query the Operating Systems Supported by PTK?

You can run the `ptk candidate os` command to query the operating systems supported by PTK.

### Can the CM Component Be Installed?

Yes.

> **Note**: To use CM, at least 3 instances are required. 

`ptk install` supports the `--install-cm` parameter. When this parameter is specified, and all nodes in a cluster are initialized, cm_ctl can be used to start the cluster (if CM is not installed, use gs_ctl to start the cluster). During the starting, the primary instance needs to be chosen and the primary/standby building process is required. Therefore, the starting time is relatively long. 

If the command times out during the PTK starting, you can log in to the database server to check the cluster status. In the starting phase, exit of PTK does not interrupt the execution of cm_ctl. 

### Should PTK and the Database Be Installed on the Same Server?

No.

PTK is designed as a controller. PTK is able to manage multiple database clusters, therefore it is recommended that PTK is installed on a management server independently and the database is installed on a database-dedicated server in the cluster. 

Certainly, PTK supports installation on the same server as the database. This is called local installation. 