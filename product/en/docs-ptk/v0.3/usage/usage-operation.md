---
title: Operating with a Database
summary: Operating with a Database
author: Yao Qian
date: 2022-07-30
---

# Operating with a Database

## View the Cluster List

After the database clusters is installed, you can run `ptk ls` to query the list of clusters installed by the current user (Because PTK metadata is stored in the `$HOME/.ptk` directory, you can query the list of the clusters installed under the current user).

Example:

```shell
$ ptk ls
   cluster_name   |       instances       | user |     data_dir     | db_version  |     create_time
------------------+-----------------------+------+------------------+-------------+----------------------
  cluster_slirist | 192.168.122.101:26000 | vmx  | /opt/mogdb/data  | MogDB 3.0.0 (build 62408a0f) | 2022-08-01 08:37:25
                  | 192.168.122.102:26000 |      |                  |             |
  cluster_lviful  | 192.168.122.101:34500 | bkk  | /opt/bkmog1/data | MogDB 3.0.0 (build 62408a0f) | 2022-07-29 17:27:54
                  | 192.168.122.102:34500 |      |                  |             |
```

The output list describes the following information: 

- cluster_name: specifies the cluster name. 
- instances: specifies all instances in the cluster. 
- user: specifies the system user. 
- data_dir: specifies the data directory of the instances. 
- db_version: specifies the database version. 
- create_time: specifies the cluster creation time. 

After the database cluster is installed, it is recommended that the `-n` parameter (specifying the cluster name) is used to manage the cluster. 

## View the Cluster Status

You can run the `cluster status` command to view the cluster status. 

```shell
# ptk cluster -n cluster_slirist status

[   Cluster State   ]
database_version            : MogDB-MogDB
cluster_name                : cluster_slirist
cluster_state               : Normal
current_az                  : AZ_ALL

[  Datanode State   ]
   id  |       ip        | port  | user | instance | db_role | state
-------+-----------------+-------+------+----------+---------+---------
  6001 | 192.168.122.101 | 26000 | vmx  | dn_6001  | primary | Normal
  6002 | 192.168.122.102 | 26000 | vmx  | dn_6002  | standby | Normal
```

## Start the Cluster

> The following uses the `cluster_slirist` cluster as an example. 

After the database cluster is installed, PTK will start the database cluster by default. 

If the `--skip-launch-db` parameter is specified during installation, the database is in the stopped status. 

You can run the `cluster start` command to start the database cluster. You need to specify the cluster name in the command. 

Example:

```shell
# ptk cluster -n cluster_slirist start
INFO[2022-08-02T11:40:48.728] Operating: Starting.
INFO[2022-08-02T11:40:48.728] =========================================
INFO[2022-08-02T11:40:48.784] starting host 192.168.122.101
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.102
INFO[2022-08-02T11:40:56.329] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:40:56.613] waiting for check cluster state...
INFO[2022-08-02T11:41:01.861] =========================================
INFO[2022-08-02T11:41:01.861] Successfully started.
INFO[2022-08-02T11:41:01.861] Operation succeeded: Start.
```

PTK starts all instances in the cluster by default. It also supports the starting of a single instance. 

```shell
# ptk cluster -n cluster_slirist start -H 192.168.122.101
INFO[2022-08-02T11:50:04.442] Operating: Starting.
INFO[2022-08-02T11:50:04.442] =========================================
INFO[2022-08-02T11:50:06.692] starting host 192.168.122.101 successfully
```

For more parameters, see the help information. 

```shell
# ptk cluster start -h
Start a database instance or cluster.

Usage:
  ptk cluster start [flags]

Flags:
  -h, --help                   help for start
  -H, --host string            Specifies the IP address of an instance. 
  -n, --name string            Specifies the cluster name. 
      --security-mode string   Specifies whether to start a database in safe mode. 
                               The value can be on and off. 
      --time-out duration      Specifies the start timeout duration. The default value is 10 minutes.
```

## Stop the Cluster

> The following uses the `cluster_slirist` cluster as an example. 

You can run the `cluster stop` command to stop a database cluster.  PTK will stop all instances in a cluster by default. 

```shell
# ptk cluster -n cluster_slirist stop
INFO[2022-08-02T11:49:40.685] Operating: Stopping.
INFO[2022-08-02T11:49:40.685] =========================================
INFO[2022-08-02T11:49:40.891] stopping host 192.168.122.102
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.101
INFO[2022-08-02T11:49:43.004] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:49:43.004] =========================================
INFO[2022-08-02T11:49:43.004] Successfully stoped.
INFO[2022-08-02T11:49:43.004] Operation succeeded: Stop.
```

You can use the `-H` parameter to specify a instance to stop it. 

```shell
# ptk cluster -n cluster_slirist stop -H 192.168.122.101
INFO[2022-08-02T11:56:32.880] Operating: Stopping.
INFO[2022-08-02T11:56:32.881] =========================================
INFO[2022-08-02T11:56:34.154] stopping host 192.168.122.101 successfully
```

For more parameters, see the help information.

```shell
# ptk cluster stop -h
Stop a database instance or cluster. 

Usage:
  ptk cluster stop [flags]

Flags:
  -h, --help                help for stop
  -H, --host string         Specifies the IP address of an instance.
  -n, --name string         Specifies the cluster name. 
      --time-out duration   Specifies the stop timeout duration. The default value is 10 minutes. 
```

## Restart the Cluster

> The following uses the `cluster_slirist` cluster as an example. 

Restarting a cluster is actually to stop the database first and then start the database.

You can run the `cluster restart` command to restart the cluster.

```shell
# ptk cluster -n cluster_slirist restart
INFO[2022-08-02T11:59:31.037] Operating: Stopping.
INFO[2022-08-02T11:59:31.037] =========================================
INFO[2022-08-02T11:59:31.217] stopping host 192.168.122.102
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.101
INFO[2022-08-02T11:59:33.309] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:59:33.309] =========================================
INFO[2022-08-02T11:59:33.309] Successfully stoped.
INFO[2022-08-02T11:59:33.309] Operation succeeded: Stop.

INFO[2022-08-02T11:59:33.310] Operating: Starting.
INFO[2022-08-02T11:59:33.310] =========================================
INFO[2022-08-02T11:59:33.376] starting host 192.168.122.101
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.102
INFO[2022-08-02T11:59:36.787] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:59:36.995] waiting for check cluster state...
INFO[2022-08-02T11:59:42.247] =========================================
INFO[2022-08-02T11:59:42.247] Successfully started.
INFO[2022-08-02T11:59:42.247] Operation succeeded: Start.
```

For more parameters, see the help information.

```shell
# ptk cluster restart -h
Restart a database instance or cluster. 

Usage:
  ptk cluster restart [flags]

Flags:
  -h, --help                   help for restart
  -H, --host string            Specifies the IP address of an instance.
  -n, --name string            Specifies the cluster name. 
      --security-mode string   Specifies whether to start a database in safe mode. 
                               The value can be on and off. 
      --time-out duration      Specifies the start timeout duration. The default value is 10 minutes.
```
