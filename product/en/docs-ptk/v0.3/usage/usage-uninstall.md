---
title: Uninstalling a Database Cluster
summary: Uninstalling a Database Cluster
author: Yao Qian
date: 2022-07-30
---

# Uninstalling a Database Cluster

During uninstallation, PTK will check whether the cluster is in the running status. A cluster in the running status cannot be uninstalled. You need to stop the cluster and then uninstall the cluster. 

> **Note**:
> Once a database is uninstalled, it cannot be recovered. Please execute operation with caution.

You can run the `ptk uninstall -n CLUSTER_NAME` command on the server where PTK is installed to uninstall a cluster. 

To uninstall a cluster, you need to specify the cluster name, you can run the `ptk ls` command to query the cluster name. 

During uninstallation, PTK will ask you whether to delete the cluster topology information, system user, and database data. 

> If you are sure to delete the data directory, PTK delete only the data directory and will not delete its parent directory. You need to manually delete the parent directory. 
