---
title: Checking the System
summary: Checking the System
author: Yao Qian
date: 2022-07-30
---

# Checking the System

Before installing the database, you need to check the system parameters and software dependencies of the server where the database is to be installed to ensure smooth installation.

You can run `checkos` to perform system check. During the check, PTK checks the following modules.

| No.  | Category              | Check Item                    | Description                                           |
| ---- | --------------------- | ----------------------------- | ----------------------------------------------------- |
| A1   | System version        | Check_OS_Version              | Checks the system version.                            |
| A2   | Kernel version        | Check_Kernel_Version          | Checks the kernel version.                            |
| A3   | Character set         | Check_Unicode                 | Check the character set.                              |
| A4   | Time zone             | Check_TimeZone                | Checks the time zone.                                 |
| A5   | Swap memory           | Check_Swap_Memory_Configure   | Checks the swap memory configuration.                 |
| A6   | sysctl parameter      | Check_SysCtl_Parameter        | Checks the sysctl parameter.                          |
| A7   | File system           | Check_FileSystem_Configure    | Checks the file system configuration.                 |
| A8   | Disk                  | Check_Disk_Configure          | Checks the disk configuration.                        |
| A9   | Preread block         | Check_BlockDev_Configure      | Checks the block configuration.                       |
|      |                       | Check_Logical_Block           | Checks the logic block.                               |
| A10  | IO scheduling         | Check_IO_Request              | Checks the IO request parameters.                     |
|      |                       | Check_Asynchronous_IO_Request | Checks the asynchronous IO request parameters.        |
|      |                       | Check_IO_Configure            | Checks the IO configuration.                          |
| A11  | Network               | Check_Network_Configure       | Checks the network configuration.                     |
| A12  | Clock consistency     | Check_Time_Consistency        | Checks the clock consistency.                         |
| A13  | Firewall              | Check_Firewall_Service        | Checks the firewall configuration.                    |
| A14  | Transparent huge page | Check_THP_Service             | Checks the transparent huge page configuration.       |
| A15  | Dependencies          | Check_Dependent_Package       | Checks the database system installation dependencies. |
| A16  | CPU command set       | Check_CPU_Instruction_Set     | Checks the CPU command set.                           |
| A17  | Port status           | Check_Port                    | Checks whether the database port is occupied.         |

To perform system check using PTK, you need to specify the cluster configuration file using`-f` and content to be checked using `-i`. If the content to be checked is unknown, all check items listed in the above table will be checked by default.

To check individual check items, specify the check items by separating their item numbers with commas.

Example:

```shell
ptk checkos -i A1,A2,A3   # To check multiple check items, enter the item number in the -i A1,A2,A3 format.
ptk checkos -i A          # Check all check items.
ptk checkos -i A --detail # Display details. 
```

The PTK check result includes four levels.

- **OK**: The check result reaches expectations and meets the installation requirement. 
- **Warning**: The check result does not reach expectations but meets the installation requirement.
- **Abnormal**: The check result does not meet the installation requirement. The installation may fail. The exception needs to be handled manually based on the suggested script by PTK.
- **ExecuteError**: PTK failed to execute commands during system check. The reason may be that some tools are not installed in the environment or there is an internal bug. This error needs to be handled based on the actual prompt information. 

Before installing the database, make sure that the check result of all check items is `OK` or `Warning`. If the check result displays `Abnormal`, an error may occur during installation and installation exits. 