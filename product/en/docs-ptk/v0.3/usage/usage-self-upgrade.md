---
title: Upgrading PTK
summary: Upgrading PTK
author: Yao Qian
date: 2022-07-30
---

# Upgrading PTK

## View  the Help Information

```
$ ptk self -h

Operate with the PTK installation package. 

Usage:
  ptk self [command]

Available Commands:
  upgrade     Download and automatically install PTK

Flags:
  -h, --help   help for self
```

### Online Upgrade

You can run the `self upgrade` command to upgrade PTK to the latest version. After the upgrade, you can run the `ptk -v` command to check the PTK version.

Example: 

```shell
$ ptk self upgrade 

INFO[2022-07-30T10:26:36.197] downloading ptk_darwin_arm64.tar.gz...
> ptk_darwin_arm64.tar.gz: 4.86 MiB / 4.86 MiB [-------------------------------------------------------------------] 100.00% 7.48 MiB p/s 900ms
INFO[2022-07-30T10:26:37.701] download successfully
INFO[2022-07-30T10:26:37.831] upgrade ptk successfully
```

### Offline Upgrade

Firstly, you need to download a package based on your operating system (the following link always shows you the packages of the latest versions):

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86: [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86: [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

Secondly, you need to put the PTK package in the PTK binary directory of the server where PTK is installed and decompress the package to override the old binary file. 

```shell
# which ptk
/root/.ptk/bin/ptk

# cd /root/.ptk/bin/
tar -xvf ptk_[os]_[arch]_.tar.gz
```

If the PTK version is later than 0.3, you can specify the `-p` parameter to perform the local upgrade. 

```
# ptk self upgrade -p /path/to/ptk_[os]_[arch]_.tar.gz
```
