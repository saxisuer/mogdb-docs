---
title: Install
summary: Install
author: Yao Qian
date: 2022-07-30
---

# Installing a Database

## View the Help Information

After a configuration file is created and the system check is successful, you can  start to install a database. 

Before installing the database, you are advised to familiarize yourself with some common parameters. You can run the `ptk install -h` command to view these parameters.

```shell
Configure and deploy a MogDB database cluster based on the given topology.

Usage:
  ptk install [flags]

Flags:
  -y, --assumeyes                    Automatically applies Yes to all questions. 
      --db-version string            Specifies the database package version (used for only online installation)
                                     You can run 'ptk candidate db' to query the supported MogDB server version (default "3.0.0").
      --default-guc                  Does not automatically optimize GUC parameters. The default database parameters are used. 
  -e, --env strings                  Adds environment variables to the configuration file of the system user. 
  -h, --help                         Shows the help information for installation.
      --install-cm                   Installs the CM component. 
      --launch-db-timeout duration   Specifies the database start timeout duration (default 10 min).
      --no-cache                     Does not use the installation package cached on the local server. 
  -p, --pkg string                   Specifies the path of the database package files or URL. 
      --post-run string              Specifies the path of the Bash script, which will be distributed to each server after the database is deployed.
      --pre-run string               Specifies the path of the Bash script, which will be distributed to each server before the database is deployed. 
      --skip-check-distro            Skips the system release check and perform installation directly. 
      --skip-check-os                Skips system environment check and perform installation directly. 
      --skip-create-user             Skips system user creation. 
      --skip-launch-db               Skips database starting. 
      --skip-rollback                Skips the rollback operation once the database fails to be installed. 
```

### Install MogDB

You can run the following command to install the MogDB database. 

```shell
$ ptk install -f config.yaml -y
```

`-f` specifies the configuration file path. `-y` specifies that yes is replied automatically for all interactive questions.

> **Note**: If the initial password is not configured in the configuration file, `-y` cannot skip the password setting process.

PTK will automatically download MogDB of the latest version for installation by default. 

PTK also supports installation of a database of a different version or a database compiled by yourself.

Parameter `--db-version` provided by PTK allows you to install MogDB of a specified version.

For example, run the following command to install MogDB 2.1.1:

```shell
ptk install -f config.yaml --db-version 2.1.1 -y
```

> You can run the `ptk candidate db` command to query the database versions supported by PTK. 

To install a database using an installation package compiled by yourself, use parameter `-p|--pkg` to specify the local path for storing the installation package or network address. 

More options are as follows:

- `-e|--env` : adds environment variables to the database system user environment after database installation.
- `--launch-db-timeout`: specifies the database start timeout duration. The default value is 5 minutes. 
- `--pre-run`: adds a shell script path and executes the specified script on all servers in a cluster before cluster installation.
- `--post-run`: adds a shell script path and executes the specified script on all servers in a cluster after cluster installation.
- `--skip-check-os`: skips the system check. 
- `--skip-check-distro`: skips system release check. 
- `--skip-create-user`: PTK automatically creates a user by default. This option skips user creation. When this option is specified, make sure that a user must exist in the server. 
- `--skip-launch-db`: does not start database after installation. 
- `--skip-rollback`: PTK rolls back all operations to clear environment by default once an error occurs. This option does not allow rollback operations.
- `--install-cm`: installs the CM component during database installation (applied to MogDB 3.0.0 or later or scenarios where at least three instances are installed in a cluster).
- `--no-cache`: does not use an installation package in the local cache and downloads the installation package from the specified network.