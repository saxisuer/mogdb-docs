---
title: Install Extensions
summary: Install Extensions
author: Yao Qian
date: 2022-07-30
---

# Installing Extensions

> For details about MogDB extension usage, see the related document, such as [dblink](https://docs.mogdb.io/en/mogdb/v3.0/dblink-user-guide).

PTK automatically judges the version of the current cluster to download the corresponding extension package for installation. 

After successful installation, PTK will not create extensions in the database. You need to manually create extensions based on the related document. 

## View the Help Document

```shell
# ptk cluster install-plugin -h
Installl the MogDB extension.

Usage:
  ptk cluster install-plugin [flags]

Flags:
  -h, --help             Specifies the help information for installing extensions. 
  -H, --host strings     Specifies the IP address of the host where an extension is to be installed. The extension is installed on all hosts in a cluster by default.
  -n, --name string      Specifies the cluster name.
      --override         Specifies whether to override the current extension file. 
  -p, --pkg string       Specifies the extension package path. 
  -P, --plugin strings   Specifies the name of the extension to be installed. All extensions will be installed by default. 
```

## Install All Extensions 

PTK downloads all the extension packages of the corresponding version to install them by default. If you need to specify an extension for installation, use `-P` or `--plugin`  to specify it. 

Example:

```bash
# ptk cluster -n cluster_slirist install-plugin
INFO[2022-08-02T16:15:11.786] downloading Plugins-3.0.0-CentOS-x86_64.tar.gz...
> Plugins-3.0.0-CentOS-x86_64...: 70.71 MiB / 70.71 MiB [-----------------------------------------------] 100.00% 14.33 MiB p/s 5.1s
INFO[2022-08-02T16:15:17.629] download successfully
INFO[2022-08-02T16:15:17.651] scp file from /tmp/2451478859/Plugins-3.0.0-CentOS-x86_64.tar.gz to 192.168.122.101:/opt/mogdb/tool/Plugins-3.0.0-CentOS-x86_64.tar.gz  host=192.168.122.101
INFO[2022-08-02T16:15:17.651] scp file from /tmp/2451478859/Plugins-3.0.0-CentOS-x86_64.tar.gz to 192.168.122.102:/opt/mogdb/tool/Plugins-3.0.0-CentOS-x86_64.tar.gz  host=192.168.122.102
INFO[2022-08-02T16:15:17.781] mkdir /opt/mogdb/tool/plugins                 host=192.168.122.101
> upload Plugins-3.0.0-CentOS...: 70.71 MiB / 70.71 MiB [--------------------------------------------------------] 100.00% 124.92 MiB p/s 800ms
INFO[2022-08-02T16:15:18.437] mkdir /opt/mogdb/tool/plugins                 host=192.168.122.102
INFO[2022-08-02T16:15:18.606] decompress Plugins-3.0.0-CentOS-x86_64.tar.gz to dir /opt/mogdb/tool/plugins  host=192.168.122.102
INFO[2022-08-02T16:15:19.816] change /opt/mogdb/tool/plugins owner to vmx   host=192.168.122.101
INFO[2022-08-02T16:15:20.542] change /opt/mogdb/tool/plugins owner to vmx   host=192.168.122.102
INFO[2022-08-02T16:15:20.562] installing plugin [dblink] ...                host=192.168.122.101
INFO[2022-08-02T16:15:20.562] installing plugin [dolphin] ...               host=192.168.122.102
INFO[2022-08-02T16:15:20.627] plugin [dblink] installed successfully        host=192.168.122.101
INFO[2022-08-02T16:15:20.627] installing plugin [wal2json] ...              host=192.168.122.101
INFO[2022-08-02T16:15:20.668] plugin [wal2json] installed successfully      host=192.168.122.101
INFO[2022-08-02T16:15:20.668] installing plugin [pg_bulkload] ...           host=192.168.122.101
INFO[2022-08-02T16:15:20.725] plugin [dolphin] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:20.725] installing plugin [postgis] ...               host=192.168.122.102
INFO[2022-08-02T16:15:20.734] plugin [pg_bulkload] installed successfully   host=192.168.122.101
INFO[2022-08-02T16:15:20.734] installing plugin [pg_prewarm] ...            host=192.168.122.101
INFO[2022-08-02T16:15:20.771] plugin [pg_prewarm] installed successfully    host=192.168.122.101
INFO[2022-08-02T16:15:20.771] installing plugin [dolphin] ...               host=192.168.122.101
INFO[2022-08-02T16:15:20.881] plugin [dolphin] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:20.881] installing plugin [postgis] ...               host=192.168.122.101
INFO[2022-08-02T16:15:20.985] plugin [postgis] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:20.985] installing plugin [dblink] ...                host=192.168.122.102
INFO[2022-08-02T16:15:21.042] plugin [dblink] installed successfully        host=192.168.122.102
INFO[2022-08-02T16:15:21.042] installing plugin [wal2json] ...              host=192.168.122.102
INFO[2022-08-02T16:15:21.075] plugin [postgis] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:21.075] installing plugin [pg_trgm] ...               host=192.168.122.101
INFO[2022-08-02T16:15:21.112] plugin [wal2json] installed successfully      host=192.168.122.102
INFO[2022-08-02T16:15:21.112] installing plugin [pg_bulkload] ...           host=192.168.122.102
INFO[2022-08-02T16:15:21.117] plugin [pg_trgm] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:21.117] installing plugin [orafce] ...                host=192.168.122.101
INFO[2022-08-02T16:15:21.160] plugin [orafce] installed successfully        host=192.168.122.101
INFO[2022-08-02T16:15:21.160] installing plugin [pg_repack] ...             host=192.168.122.101
INFO[2022-08-02T16:15:21.200] plugin [pg_repack] installed successfully     host=192.168.122.101
INFO[2022-08-02T16:15:21.200] installing plugin [whale] ...                 host=192.168.122.101
INFO[2022-08-02T16:15:21.208] plugin [pg_bulkload] installed successfully   host=192.168.122.102
INFO[2022-08-02T16:15:21.208] installing plugin [pg_prewarm] ...            host=192.168.122.102
INFO[2022-08-02T16:15:21.243] plugin [whale] installed successfully         host=192.168.122.101
INFO[2022-08-02T16:15:21.266] plugin [pg_prewarm] installed successfully    host=192.168.122.102
INFO[2022-08-02T16:15:21.266] installing plugin [pg_trgm] ...               host=192.168.122.102
INFO[2022-08-02T16:15:21.321] plugin [pg_trgm] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:21.321] installing plugin [orafce] ...                host=192.168.122.102
INFO[2022-08-02T16:15:21.384] plugin [orafce] installed successfully        host=192.168.122.102
INFO[2022-08-02T16:15:21.384] installing plugin [pg_repack] ...             host=192.168.122.102
INFO[2022-08-02T16:15:21.444] plugin [pg_repack] installed successfully     host=192.168.122.102
INFO[2022-08-02T16:15:21.444] installing plugin [whale] ...                 host=192.168.122.102
INFO[2022-08-02T16:15:21.510] plugin [whale] installed successfully         host=192.168.122.102
INFO[2022-08-02T16:15:26.949] All done
INFO[2022-08-02T16:15:26.949] Time elapsed: 15s
```
