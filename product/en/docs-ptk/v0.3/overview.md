---
title: PTK Overview
summary: PTK Overview
author: Yao Qian
date: 2022-05-30
---

## PTK Overview

PTK (Provisioning Toolkit) is an installation and operation and maintenance tool for MogDB. It aims at quick installation of MogDB. 

If you need to run MogDB or its related components, only one command needs to be run. 

## Application Scenarios

- Developers need to quickly enable multiple local MogDB processes.  
- Users need to install MogDB quickly using PTK.
- Daily operation and maintenance by DBAs
- A third-party operation and maintenance platform needs to be integrated.

## Operating Systems Supported by PTK for Installing MogDB

You can run `ptk candidate os`  to check the latest OSs supported. 

| id   | os                                  | tested version(s)         |
| ---- | ----------------------------------- | ------------------------- |
| 1    | CentOS 7 (x86_64)                   | 7.6.1810 (Core)           |
| 2    | openEuler 20 (x86_64)               | 20.03 LTS                 |
| 3    | openEuler 20 (arm64)                | 20.03 LTS                 |
| 4    | openEuler 22 (x86_64)               | 22.03 LTS                 |
| 5    | openEuler 22 (arm64)                | 22.03 LTS                 |
| 6    | Kylin V10 (x86_64)                  | V10 (Tercel)              |
| 7    | Kylin V10 (arm64)                   | V10 (Tercel)              |
| 8    | UOS 20 A (x86_64)                   | 1002a/1020a/1050a         |
| 9    | UOS 20 A (arm64)                    | 1050a (kongzi)            |
| 10   | Ubuntu 20 (x86_64)                  | 20.04.3 LTS (Focal Fossa) |
| 11   | CentOS 8 (arm64)                    | 8.0.1905 (Core)           |
| 12   | CentOS 8 (x86_64)                   | 8.0.1905 (Core)           |
| 13   | Red Hat Enterprise Linux 7 (x86_64) | 7.5 (Maipo)               |
| 14   | Red Hat Enterprise Linux 8 (x86_64) | 8.5 (Ootpa)               |
| 15   | EulerOS 2 (x86_64)                  | 2.0 (SP3)                 |
| 16   | EulerOS 2 (arm64)                   | 2.0 (SP3)                 |
| 18   | SLES 12SP5 (x86_64)                 | 12SP5                     |
| 19   | Oracle Linux 7 (x86_64)             | 7.9 (Maipo)               |
| 20   | Oracle Linux 8 (x86_64)             | 8.6 (Ootpa)               |
| 21   | Rocky Linux 8 (x86_64)              | 8.5 (Green Obsidian)      |
| 22   | NeoKylin V7 (x86_64)                | V7Update6                 |
| 23   | UOS 20 D/E (x86_64)                 | 1040d (fou)               |
| 24   | UOS 20 D/E (arm64)                  | 1040d (fou)               |
| 25   | Ubuntu 22 (x86_64)                  | 22.04 (Jammy Jellyfish)   |

*For other OSs, the adaptation tests are in progress.*