---
title: Release Note
summary: Release Note
author: Yao Qian
date: 2022-06-01
---

## PTK of the Latest Version

The PTK of the revised version will be released aperiodically based on user feedback. 

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## Release Note

### v 0.3.0

#### Change Log

- Support scaling-up and scaling-down. 
- Support extension installation.
- Optimize partial functions and fix some bugs. 

#### Download Address

- [ptk_0.3.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_linux_x86_64.tar.gz)
- [ptk_0.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_linux_arm64.tar.gz)
- [ptk_0.3.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_darwin_x86_64.tar.gz)
- [ptk_0.3.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_darwin_arm64.tar.gz)
- [ptk_0.3.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_windows_x86_64.tar.gz)

### v 0.2.21

#### Download Address

- [ptk_0.2.21_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_linux_x86_64.tar.gz)
- [ptk_0.2.21_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_linux_arm64.tar.gz)
- [ptk_0.2.21_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_darwin_x86_64.tar.gz)
- [ptk_0.2.21_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_darwin_arm64.tar.gz)
- [ptk_0.2.21_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_windows_x86_64.tar.gz)

### v 0.1.0

- [ptk_0.1.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_x86_64.tar.gz)
- [ptk_0.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_arm64.tar.gz)
- [ptk_0.1.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_x86_64.tar.gz)
- [ptk_0.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_arm64.tar.gz)
- [ptk_0.1.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_windows_x86_64.tar.gz)