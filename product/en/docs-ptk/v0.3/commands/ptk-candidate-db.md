---
title: ptk candidate db
summary: ptk candidate db
author: ptk
date: 2022-6-28
---

## ptk candidate db

ptk candidate db lists the MogDB versions that are supported for installation.

## Syntax

```shell
ptk candidate db [flags]
```

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs a table containing the `software` and `version` fields.