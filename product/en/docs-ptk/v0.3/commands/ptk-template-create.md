---
title: ptk template create
summary: ptk template create
author: ptk
date: 2022-6-28
---

## ptk template create

ptk template create creates a configuration template in an interactive way. 

## Syntax

```shell
ptk template create [flags]
```

## Option

### -o, --output string

- Specifies the file path for storing a template. 
- Data type: String
- Default value: **config.yaml**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.
