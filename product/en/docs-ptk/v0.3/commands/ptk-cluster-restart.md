---
title: ptk cluster restart
summary: ptk cluster restart
author: ptk
date: 2022-06-28
---

## ptk cluster restart

ptk cluster restart restarts a database cluster or instance. 

## Syntax

```shell
ptk cluster restart -n string [flags]
```

## Option

### -n, --name string

- Specifies the name of the cluster to be operated with.
- Data type: String

### -H, --host string

- Specifies the IP address of the instance to be operated with. 
- Data type: String
- This option supports querying the status of a single instance. It is used for querying the status of all instances in a cluster by default. 

### --security-mode string

- Specifies whether to start the database in safe mode. 
- Data type: String
- Values: **on** and **off**
- Default value: **off**

### --time-out duration

- Specifies the execution timeout duration. 
- Data type: Duration
- The value unit can be h (hour), m (minute), s (second), and ms (millisecond).
- Default value: **2m**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs the run log. 
