---
title: ptk encrypt
summary: ptk encrypt
author: ptk
date: 2022-6-28
---

## ptk encrypt

ptk encrypt encrypts text or password. It is mainly used in encrypting the password field in a configuration file. It is recommended that a configuration file should store ciphertext instead of plain text.

## Syntax

```shell
ptk encrypt TEXT1 [TEXT2 TEXT3...] [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs original content and ciphertext in the key-value format. If multiple passwords are encrypted, they are output in multiple lines.
