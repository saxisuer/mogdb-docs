---
title: ptk template
summary: ptk template
author: ptk
date: 2022-6-28
---

## ptk template

ptk template prints a configuration template, and outputs a configuration template of multiple instances. 

## Syntax

```shell
ptk template [flags]
```

## Option

### -l, --local

- Generates the configuration file of the local single-instance database.
- Data type: Boolean
- The option is disabled by default. It takes effect by being added to the command line. 
- Default value: **false**

### -d, --base-dir string

- Specifies the root directory of the database to be installed. 
- Data type: String
- PTK automatically creates data, tool, and application directories for the database to be installed in this root directory.
- Default value: **/opt/mogdb**

### -n, --cluster-name string

- Specifies the cluster name. 
- Data type: String
- If this parameter is not specified, PTK will generate a cluster name in random by default.

### -u, --user string

- Specifies the system user name for running a database. 
- Data type: String
- Default value: **omm**

### -g, --group string

- Specifies the system user group name for running a database. 
- Data type: String
- Default value: The default value is the same as the system user name specified by `-u`.

### -p, --port int

- Specifies the database port. 
- Data type: Integer
- Default value: **26000**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs original content and ciphertext in the key-value format. If multiple passwords are encrypted, they are output in multiple lines.

## Subcommand

- [ptk template create][ptk-template-create.md]