---
title: ptk cluster scale-in
summary: ptk cluster scale-in
author: ptk
date: 2022-08-01
---

## ptk cluster scale-in

ptk cluster scale-in performs the scale-in operation on a MogDB cluster. 

## Syntax

```shell
ptk cluster -n CLUSTER_NAME scale-in  -H HOST_IP [--stop-db] [--clear-user] [--clear-dir] [--clear-env] [-t 120]

```

## Option

### -n, --name string

- Specifies the name of a cluster to be operated with. 
- Data type: String

### -H, --host strings

- Specifies the IP address of a server to be removed. 
- Data type: String
- This parameter can be specified for multiple times. 

### --stop-db

- Stops the database. 
- Data type: Boolean

### --clear-user

- Deletes the system user. 
- Data type: Boolean

### --clear-env

- Clears environment variables. 
- Data type: Boolean

### --clear-dir

- Clears the relative directory of a database. 
- Data type: Boolean

### -t, --timeout duration

- Specifies the execution timeout duration. 
- Data type: Duration
- The unit supported by the duration type includes h, m, s, and ms. 
- Default value: **5m**

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs an operation log.
