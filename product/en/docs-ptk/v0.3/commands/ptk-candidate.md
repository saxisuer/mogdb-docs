---
title: ptk candidate
summary: ptk candidate
author: ptk
date: 2022-6-28
---

## ptk candidate

ptk candidate shows the PTK support reference.

## Syntax

```shell
ptk candidate [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs the candidate help information.

## Subcommand

- [ptk candidate db](ptk-candidate-db.md) - Lists the supported MogDB database versions.
- [ptk candidate os](ptk-candidate-os.md) - Lists the OSs that support installation of MogDB.
