---
title: ptk candidate
summary: ptk candidate
author: ptk
date: 2022-6-28
---

## ptk download

ptk download downloads the MogDB installation package online. 

## Syntax

```shell
ptk candidate [flags]
```

## Option

### -O，--os

- Specifies the system ID (you can run [ptk candidate os](ptk-candidate-os.md) to query the ID).
- Data type: Integer
- If this option is not specified, the ID of the current system will be detected by default. 

### -V，--db-version

- Specifies the MogDB version (You can run [ptk candidate db](ptk-candidate-db.md) to query the supported MogDB versions).
- Data type: String
- Default value: the latest version

### --output

- Specifies the file path in which the package is to be downloaded. 
- Data type: String
- This parameter is left blank by default.

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs the file downloading progress log. 
