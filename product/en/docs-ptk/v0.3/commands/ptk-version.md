---
title: ptk version
summary: ptk version
author: ptk
date: 2022-06-28
---

## ptk Version

ptk version prints the PTK version. 

## Syntax

```shell
ptk version [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

The version information is similar to the following.

```text
PTK Version: v0.2.0
Go Version: go1.17.1
Build Date: 2022-06-28T07:28:33Z
Git Hash: df42c22
```

The output includes the PTK version, Golang version in compilation, build time, and git commit information.
