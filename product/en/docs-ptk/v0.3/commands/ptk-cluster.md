---
title: ptk cluster
summary: ptk cluster
author: ptk
date: 2022-06-28
---

## ptk cluster

ptk cluster operates with database cluster management. 

## Syntax

```shell
ptk cluster [flags]
```

## Option

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Subcommand

- [ptk cluster status](ptk-cluster-status.md): Queries the cluster or instance status. 
- [ptk cluster start](ptk-cluster-start.md): Starts a database cluster or instance. 
- [ptk cluster stop](ptk-cluster-stop.md): Stops a database cluster or instance. 
- [ptk cluster restart](ptk-cluster-restart.md): Restarts a database cluster or instance. 
- [ptk cluster scale-out](ptk-cluster-scale-out.md): Cluster scaling up
- [ptk cluster scale-in](ptk-cluster-scale-in.md): Cluster scaling down
- [ptk cluster install-plugin](ptk-cluster-install-plugin.md): installs an extension. 
