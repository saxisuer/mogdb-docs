---
title: ptk cluster install-plugin
summary: ptk cluster install-plugin
author: ptk
date: 2022-08-01
---

## ptk cluster install-plugin

ptk cluster install-plugin installs the MogDB plugins.

## Syntax

```
ptk cluster install-plugin [flags]
```

## Option

### -H, --host strings

- Specifies the IP address of the server where the plugin is to be installed. 
- Data type: String
- The plugin is installed on all servers in a cluster by default. 
- This parameter can be specified multiple times. 

### -n, --name string

- Specifies the cluster name. 
- Data type: String

### --override

- Specifies whether to overwrite the current plugin file. 
- Data type: Boolean

### -p, --pkg string

- Specifies the local path of the plugin package. This is applicable to offline installation. 
- Data type: String

### -P, --plugin strings

- Specifies the name of the plugin to be installed. All plugins will be installed by default. 
- Data type: String
- This parameter can be specified multiple times. 

### -h, --help

- Outputs the help information.
- Data type: Boolean
- This option is disabled by default. The default value is **false**. This function can be enabled by adding the option to the command or adding the option and its value **true** to the command.

## Output

Outputs an operation log. 
