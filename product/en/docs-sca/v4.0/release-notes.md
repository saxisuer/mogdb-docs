---
title: Release Notes
summary: Release Notes
author: hongyedba
date: 2021-09-30
---

# Release Notes

## v4.0.0

### Feature

- Integrate a newly designed offline analysis report.
- Support data collection, compatibility analysis, and performance assessment of a single executable program.
- Support the SHA256 encryption authentication mode of openGauss/MogDB in the Linux OS.
