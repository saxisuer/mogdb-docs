---
title: SCA Usage
summary: SCA Usage
author: hongyedba
date: 2021-09-30
---

# SCA Usage

## Deployment Procedure

SCA uses the Python3-compiled script program, which has been packaged into an executable program and does not require deployment.

SCA can be directly used only when the database can be connected through the network after the program file is uploaded.

Before using SCA, you are advised to read SCA Usage and Command Options carefully. Make sure that all options are used appropriately.

## Common Commands

**Note**: The following uses the Linux OS of the x86-64 architecture as an example. For other ARM platforms or the MacOS system, the commands need to be modified accordingly.

* Initialize the repository and perform the compatibility assessment (generally used in the first time to run, use the automatically created target database by SCA)

```shell
./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password> -d <unzipped data directory>
```

* Initialize only the repository (run only one time for a repository)

```shell
./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password>
```

* Perform only the compatibility assessment (data analysis is performed usually when an initialized repository is connected)

```shell
./sca_linux_x86_64 -d <unzipped data directory>
```

* Simulate only SQLs (after the repository is initialized)

```shell
./sca_linux_x86_64 -T S -d <unzipped data directory>
```

* Perform both SQL compatibility and performance assessment (after the repository is initialized)

```shell
./sca_linux_x86_64 -T IS -H <host> -P <port> -U <super-user> -E <super-password> -d <unzipped data directory> -t <target_db>
```
