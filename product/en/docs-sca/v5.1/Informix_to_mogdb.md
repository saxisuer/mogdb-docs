---
title: Informix to MogDB evaluation
summary: Informix to MogDB evaluation
author: Hong Rihua
date: 2022-03-07
---

# Informix to MogDB Evaluation

## MogDB Environment Preparation

You need to prepare your own MogDB/openGauss database environment for the repository and the target database, and the same environment is used by default for the repository and the target database.

## Object Structure Migration

- **Structure Migration**: You need to create the target database in the prepared MogDB/openGauss environment and migrate the data structures from the source database to the newly created target database.
- **Data Migration**: If you want to perform SQL performance comparison evaluation, then in addition to migrating the database structure, you also need to migrate the table data to ensure that the source Informix and the target MogDB/openGauss database data size is the same for the performance comparison to be meaningful.

Related Tool:

- **MTK**: Structure and data migration can be done by MTK, for details please refer to [https://mogdb.io/mtk](https://mogdb.io/mtk)

## Informix Data Collection

Currently Informix's data collection is a continuous collection of SQL from the cache. By default, the collection process is performed periodically (depends on data range in the cache) for a total of one week's worth of cached SQL data.

You must enable "global sql trace" feature before Informix SQL collection。for detail command options please refer to [Set-global-sql-tracing](https://www.ibm.com/docs/en/informix-servers/12.10?topic=saaf-set-sql-tracing-argument-set-global-sql-tracing-sql-administration-api)

### Required Permissions

The permissions required for the collection user are as follows.

> dba (system user)

### Related Commands

```shell
./sca_linux_x86_64 -T IC -h <host> -p <port> -n <source-informix-db> -u <informix-user> -e <informix-password>

# Command options:
# -h/p/t/u/e specify the connection method of the source Informix database
# Considering that the SQL content in the shared cache may not be comprehensive, the collection will be continuous and incremental, with one week of data collected by default. If you are only doing functional validation, you can use the -q 0.0001 option, i.e. only collect SQL data once.
```

### Collection Results

When the collection is complete, a zip packet is generated which can be copied to the target database and unpacked into a data directory.

Subsequent compatibility analysis relies on the data in this packet and does not require another connection to the source Informix database.

## Repository Initialization

```shell
# Use the MogDB/openGauss user with administrator privileges for repository initialization
# Repository initialization will create a database with the name sca_db by default
# You can specify the repository name by -N, the repository user name by -U, and the repository user password by -E

./sca_linux_x86_64 -T i -H <host> -P <port> -N sca_db -U sca_repo -E 'SCA@password' --user <super-user> --password <super-password>
```

## Perform Analysis Tasks

### Compatibility Analysis

Informix database currently only supports SQL compatibility analysis with the following command.

```shell
# If the repository name, user name, password are not the default, you need to use the -N, -U, -E options to specify
# If the target database information is different from the repository, use the -h, -p, -n, -u, -e options to specify

./sca_linux_x86_64 -T II -H <host> -P <port> -n <target-db> -d <unzipped data directory>
```

When the analysis is complete, a report will be generated in the directory specified by **-d**, which can be downloaded offline for viewing.
