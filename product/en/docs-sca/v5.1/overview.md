---
title: Overview of SCA
summary: Overview of SCA
author: hongyedba
date: 2021-09-30
---

# Overview of SCA

SCA (SQL Compatibility Analyzer) is an SQL compatibility and performance evaluation tool for heterogeneous databases prior to migration.

SCA is used to assess the compatibility of heterogeneous data before migration, to assess whether the actual business SQL in the source database has syntax problems in the target database, and to assess the performance differences between the actual execution of SQL in the two heterogeneous databases.

SCA currently supports three source databases, including: Oracle, MySQL, DB2, PostgreSQL, Informix, SQL Server.

**Applicable to**: MogDB and other openGauss-based databases

SCA service program can run the following 14 types of tasks:

* Runtime tasks

  1. `I [Initialize]`: **Initialize**, used to initialize the SCA repository
  2. `L [Apply License]`:  **Apply license**, task for license application

* Collection tasks

  1. `OC [Oracle Collection]`:  **Oracle collection**, used to collect the business SQL executed in Oracle database, need long time to collect
  2. `MC [MySQL Collection]`:  **MySQL collection**, used to collect the business SQL executed in MySQL database, need to configure the slow log in advance, and then collect at once
  3. `DC [DB2 Collection]`:  **DB2 collection**, used to collect the business SQL executed in DB2 database, need long time to collect
  4. `PC [PostgreSQL Collection]`:  **PostgreSQL collection**, used to collect the business SQL executed in PostgreSQL database (using plugin pg_stat_statements), need long time to collect
  5. `IC [Informix Collection]`:  **Informix collection**, used to collect the business SQL executed in Informix database (need enable global sql tracing manually), need long time to collect
  6. `SC [SQL Server Collection]`:  **SQL Server collection**, used to collect the business SQL executed in SQL Server database, need long time to collect

* Analysis tasks

  1. `OI [Oracle Inspection]`:  **Oracle compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in Oracle on the source side in MogDB on the target side
  2. `MI [MySQL Inspection]`:  **MySQL compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in MySQL on the source side in MogDB on the target side
  3. `DI [DB2 Inspection]`: **DB2 compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in DB2 on the source side in MogDB on the target side
  4. `PI [PostgreSQL Inspection]`: **PostgreSQL compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in PostgreSQL on the source side in MogDB on the target side
  5. `II [Informix Inspection]`: **Informix compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in Informix on the source side in MogDB on the target side
  6. `SI [SQL Server Inspection]`: **SQL Server compatibility evaluation**, used to evaluate the actual compatibility of business SQL collected in SQL Server on the source side in MogDB on the target side
  7. `OS [Oracle Simulation]`: **Oracle performance evaluation**, used to evaluate the execution performance of business SQL collected in Oracle on the source side in MogDB on the target side
  8. `OIS [Oracle Inspection & Simulation]`: **Oracle compatibility and performance evaluation**, equivalent to OI + OS two tasks at the same time

Note:

- Compatibility assessment requires a complete source database structure. It is recommended that MTK is used for migration of the source database structure. SCA can also be used to automatically create a target test database for test purposes.
- SQL simulation requires a complete source database data and data. It is recommended that MTK is used for migration of the source database structure and data.

## Supported OSs and Platforms

SCA supports the following OSs and platforms currently:

1. Linux x86_64
2. Linux arm64 (ARM platform does not support DB2 data collection because it does not have a DB2 client)
3. MacOS (MacOS does not support the SHA256 encryption authentication mode of openGauss/MogDB.)

## Supported Databases

SCA supports the following source and target databases currently:

1. Source database:
   - Oracle &gt;= 10.2
   - MySQL &gt;= 5.5
   - DB2 &gt;= 11.5
   - PostgreSQL &gt;= 9
   - Informix &gt;= 14.10 (lower version does not verified)
   - SQL Server &gt;= 16.0 (lower version does not verified)
   - File: MySQL slow-log
2. Target database:
   - MogDB/openGauss &gt;= 2.0
   - PostgreSQL &gt;= 13.0

**Note**: Whether SCA supports a platform does not have relevance to whether SCA supports a database. A database running on other platforms, such as Windows/AIX can be supported by SCA only when the the host network and port are connected between the database and SCA.
