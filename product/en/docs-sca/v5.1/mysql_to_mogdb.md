---
title: MySQL to MogDB Evaluation
summary: MySQL to MogDB Evaluation
author: Hong Rihua
date: 2022-03-07
---

# MySQL to MogDB Evaluation

## MogDB Environment Preparation

You need to prepare your own MogDB/openGauss database environment for the repository and the target database, and the same environment is used by default for the repository and the target database.

## Object Structure Migration

- **Structure Migration**: You need to create the target database in the prepared MogDB/openGauss environment and migrate the data structures from the source database to the newly created target database.
- **Data Migration**: If you want to perform SQL performance comparison evaluation, then in addition to migrating the database structure, you also need to migrate the table data to ensure that the source MySQL and the target MogDB/openGauss database data size is the same for the performance comparison to be meaningful.

Related Tool:

- **MTK**: Structure and data migration can be done by MTK, for details please refer to [https://mogdb.io/mtk](https://mogdb.io/mtk)

## MySQL Data Collection

Currently MySQL source database SQL collection relies on slow logs (either to log files, or to database tables) to collect all SQL recorded in the slow logs at once.

To avoid SQL misses, you must ensure that the entire business cycle has been tested and executed or covered.

### Required Permissions

The permissions required for the collection user are as follows.

> root

### Enable Slow SQL Logging

- You need to enable slow logging in advance to ensure that the complete business cycle data is recorded in the slow log, it is recommended to enable slow logging one week in advance
- Set slow SQL threshold interval to 0

```sql
SET GLOBAL slow_query_log=ON;
SET GLOBAL long_query_time=0.001;
set global log_output='FILE';      -- log_output='TABLE' is also supported, but the content is relatively small
```

### Data Collection

It is recommended to execute the data collection command after running a full business cycle with slow logging enabled.

It is recommended to execute the data collection command after running a full business cycle and full process business is done with slow logging enabled.

```shell
./sca_linux_x86_64 -T MC -h <host> -p <port> -n <source-mysql-db> -u <mysql-user> -e <mysql-password>

# Command options:
# -h/p/t/u/e specify the connection method of the source MySQL database
```

### Collection Results

When the collection is complete, a zip packet is generated which can be copied to the target database and unpacked into a data directory.

Subsequent compatibility analysis relies on the data in this packet and does not require another connection to the source MySQL database.

## Repository Initialization

```shell
# Use the MogDB/openGauss user with administrator privileges for repository initialization
# Repository initialization will create a database with the name sca_db by default
# You can specify the repository name by -N, the repository user name by -U, and the repository user password by -E

./sca_linux_x86_64 -T i -H <host> -P <port> -N sca_db -U sca_repo -E 'SCA@password' --user <super-user> --password <super-password>
```

## Perform Analysis Tasks

### Compatibility Analysis

MySQL database currently only supports SQL compatibility analysis with the following command.

```shell
# If the repository name, username, password are not the default, you need to use the -N, -U, -E options to specify
# If the target database information is different from the repository, use the -h, -p, -n, -u, -e options to specify

./sca_linux_x86_64 -T OI -H <host> -P <port> -n <target-db> -d <unzipped data directory>
```

When the analysis is complete, a report will be generated in the directory specified by **-d**, which can be downloaded offline for viewing.
