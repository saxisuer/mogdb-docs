---
title: Oracle to MogDB Evaluation
summary: Oracle to MogDB Evaluation
author: Hong Rihua
date: 2022-03-07
---

# Oracle to MogDB Evaluation

## MogDB Environment Preparation

You need to prepare your own MogDB/openGauss database environment for the repository and the target database, and the same environment is used by default for the repository and the target database.

## Object Structure Migration

- **Structure Migration**: You need to create the target database in the prepared MogDB/openGauss environment and migrate the data structures from the source database to the newly created target database.
- **Data Migration**: If you want to perform SQL performance comparison evaluation, then in addition to migrating the database structure, you also need to migrate the table data to ensure that the source Oracle and the target MogDB/openGauss database data size is the same for the performance comparison to be meaningful.

Related Tool:

- **MTK**: Structure and data migration can be done by MTK, for details please refer to  [https://mogdb.io/mtk](https://mogdb.io/mtk)

## Oracle Data Collection

### Required Permissions

The permissions required for the collection user are as follows.

> SELECT ANY DICTIONARY
> 
> CREATE PROCEDURE
> 
> EXECUTE ON DBMS_LOB
> 
> SELECT ON GV_$SQL_PLAN

### Related Commands

```shell
./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n <target-db> -u <oracle-user> -e <oracle-password>

# Command options:
# -s specify the list of Schema to be collected
# -h/p/t/u/e specify the connection method of the source Oracle database
# Considering that the SQL content in the shared cache may not be comprehensive, the collection will be continuous and incremental, with one week of data collected by default. If you are only doing functional validation, you can use the -q 0.001 -Q 60 option, i.e. only collect SQL data once.
```

### Collection Results

When the collection is complete, a zip packet is generated which can be copied to the target database and unpacked into a data directory.

Subsequent compatibility analysis relies on the data in this packet and does not require another connection to the source Oracle database.

## Repository Initialization

```shell
# Use the MogDB/openGauss user with administrator privileges for repository initialization
# Repository initialization will create a database with the name sca_db by default
# You can specify the repository name by -N, the repository user name by -U, and the repository user password by -E

./sca_linux_x86_64 -T i -H <host> -P <port> -N sca_db -U sca_repo -E 'SCA@password' --user <super-user> --password <super-password>
```

## Perform Analysis Tasks

### Compatibility Analysis

To do SQL compatibility analysis only, use the following command.

```shell
# If the repository name, user name, password are not the default, you need to use the -N, -U, -E options to specify

./sca_linux_x86_64 -T OI -H <host> -P <port> -d <unzipped data directory>
```

When the analysis is complete, a report will be generated in the directory specified by **-d**, which can be downloaded offline for viewing.

### Performance Comparison Analysis

To do SQL performance comparison analysis only, use the following command.

```shell
# If the repository name, user name, password are not the default, you need to use the -N, -U, -E options to specify

./sca_linux_x86_64 -T OS -H <host> -P <port> -d <unzipped data directory>
```

When the analysis is complete, a report will be generated in the directory specified by **-d**, which can be downloaded offline for viewing.

### Compatibility Analysis + Performance Comparison Analysis

If you need to do both SQL compatibility analysis and SQL performance comparison, use the following command.

```shell
# If the repository name, user name, password are not the default, you need to use the -N, -U, -E options to specify

./sca_linux_x86_64 -T OIS -H <host> -P <port> -d <unzipped data directory>
```

When the analysis is complete, a report will be generated in the directory specified by **-d**, which can be downloaded offline for viewing.
