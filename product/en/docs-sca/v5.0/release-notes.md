---
title: Release Notes
summary: Release Notes
author: hongyedba
date: 2021-09-30
---

# Release Notes

## v5.0.0

### Feature

- Code logic refactoring, command options and usage have changed considerably
- Support MySQL connecting database collection, through the database table of slow log information, or through the server-side local slow log
- Support DB2 data collection, collection mode is similar to Oracle, need to run for a long time, the default collection time is one week
- Supports DB2 SQL compatibility analysis, provided that the target MogDB database needs to be prepared in advance
- Support for export of Oracle SQL detail list files (sql_detail_list.csv)
- Adjust Oracle data collection logic, remove unnecessary object information and system configuration information
- Adjust the repository structure, only keep the basic information of the source database, SQL compatibility audit, SQL performance comparison and other related structures

## v4.1.0

### Feature

- Support SQL compatibility analysis of the MySQL database based on the slow log and general log.
- Support customization of SQL conversion rules.

## v4.0.0

### Feature

- Integrate a newly designed offline analysis report.
- Support data collection, compatibility analysis, and performance assessment of a single executable program.
- Support the SHA256 encryption authentication mode of openGauss/MogDB in the Linux OS.
