---
title: SCA Usage
summary: SCA Usage
author: hongyedba
date: 2021-09-30
---

# SCA Usage

## Deployment Procedure

SCA is already packaged as a binary executable, no additional deployment operations are required.

SCA can be directly used only when the database can be connected through the network after the program file is uploaded.

## Common Commands

**Note**: The following uses the Linux OS of the x86-64 architecture as an example. For other ARM platforms or the MacOS system, the commands need to be modified accordingly.

* Apply for a License (when the software is run for the first time, you need to apply for a license online first)

```shell
# You need to enter the user email during the run
# The requested license data will be sent to the entered email address.
# Copy the license data and write it to the license.json file in the same directory as the SCA
./sca_linux_x86_64 -T L
```

* Initialize the repository (for the same target MogDB database, you only need to initialize the repository on the first run)

```shell
./sca_linux_x86_64 -T i -H <host> -P <port> -N <repo-database> -U <repo-user> -E <repo-password> --user <super_user> --password <super_password>
```

* Oracle data collection (no repository required)

```shell
# For test, you can add the following parameters to speed up SQL collection: -q 0.001 -Q 60 -m off
./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n <target_db> -u <Oracle_user> -e <Oracle_password>
```

* Slow log collection from specified MySQL

```shell
./sca_linux_x86_64 -T MC -d <data_directory> --slow-log=<slow_logfile>
```

* Automatic collection from a specified MySQL server

```shell
./sca_linux_x86_64 -T MC -d <data_directory> -h <host> -p <port> -n <target_db> -u <MySQL_user> -e <MySQL_password>
```

* Perform Oracle compatibility evaluation (repository is initialized and repository login information is default)

```shell
./sca_linux_x86_64 -T OI -d <unzipped data directory> -n <target_db>
```

* SQL simulation only (repository is initialized and repository user and password information is default)

```shell
./sca_linux_x86_64 -T OS -h <host> -p <port> -n <target_db> -d <unzipped data directory>
```

* Simultaneous SQL compatibility and performance evaluation (repository initialized)

```shell
./sca_linux_x86_64 -T OIS -h <host> -p <port> -n <target_db> -u <repo_user> -e <repo_password> -d <unzipped data directory>
```
