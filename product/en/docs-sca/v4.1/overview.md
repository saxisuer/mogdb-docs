---
title: Overview of SCA
summary: Overview of SCA
author: hongyedba
date: 2021-09-30
---

# Overview of SCA

SCA (SQL Compatible Analysis) is compiled using Python (Version 3).
SCA is a compatibility assessment tool used before migration of a heterogeneous database. It can evaluate whether the actual service SQL in the source database is executed in the target database and SQL performance difference in the two heterogeneous databases.
**Applicable to**: MogDB and other openGauss-based databases

An SCA service program can run the following three types of tasks:

1. `I [Inspection]`: **Compatibility assessment**

   Evaluates the compatibility of MogDB objects and service SQLs from Oracle to MogDB.

2. `S [Simulation]`: **SQL simulation**

   Simulates the execution of Oracle service SQLs in the MogDB fidelity environment.

3. `C [Collection]`: **Data collection**

   Collects Oracle metadata, including structure data of objects, SQL execution data, and others.

4. `M [MySQL]`: **MySQL evaluation**

   Evaluates the compatibility of SQLs from the slow query log collected from MySQL.

Note:

- Compatibility assessment requires a complete source database structure. It is recommended that MTK is used for migration of the source database structure. SCA can also be used to automatically create a target test database but its compatibility is relatively poor.
- SQL simulation requires a complete source database data. It is recommended that MTK is used for migration of the source database structure and data.

## Supported OSs and Platforms

SCA supports the following OSs and platforms currently:

1. Linux x86_64
2. Linux arm64
3. MacOS (MacOS does not support the SHA256 encryption authentication mode of openGauss/MogDB.)

## Supported Databases

SCA supports the following source and target databases currently:

1. Source database: Oracle &gt;= 10.2
2. Target database: MogDB/openGauss &gt;= 1.0 (MacOS does not support the SHA-256 authentication mode temporarily.)

**Note**: Whether SCA supports a platform does not have relevance to whether SCA supports a database. A database running on other platforms, such as Windows/AIX can be supported by SCA only when the the host network and port are connected between the database and SCA.
