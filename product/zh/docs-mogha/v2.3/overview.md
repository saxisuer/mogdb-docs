---
title: MogHA简介
summary: MogHA简介
author: Yao Qian
date: 2021-09-14
---

# 关于MogHA

MogHA 是[云和恩墨](https://enmotech.com/)基于 [MogDB](https://www.mogdb.io/) 同步异步流复制技术自研的一款保障数据库主备集群高可用的企业级软件系统

(适用于 MogDB 和 openGauss 数据库)

MogHA 能够自主探测故障实现故障转移，虚拟IP自动漂移等特性，使得数据库的故障持续时间从分钟级降到秒级（RPO=0，RTO<30s），确保数据库集群的高可用服务。

<br/>

## 为什么数据库支持主备，还需要 MogHA

首先我们需要理解一下什么是高可用，高可用的目的是为了让数据库尽可能提供连续服务，以保证上层业务的稳定运行。数据库虽然支持主备库的部署结构，其目的是防止单点故障。但数据库并不提供故障检测以及自动化切换主备的功能，这也不属于数据库的处理范畴。所以需要有 MogHA 这样的一套高可用系统，来保证数据库服务的连续性。

<br/>

## 功能特性

- 自主发现数据库实例角色
- 自主故障转移
- 支持网络故障检测
- 支持磁盘故障检测
- 虚拟IP自动漂移
- 感知双主脑裂，自动选主
- 数据库进程和CPU绑定
- HA自身进程高可用
- 支持单机并行部署多套 MogHA
- 支持 x86_64 和 aarch64

<br/>

## 系统架构

![image](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-deploy-arch.png)

<br/>

## 支持的模式

### Lite 模式（推荐）

Lite 模式，顾名思义即轻量级模式，该模式仅需在主库和一台**同步备**机器上启动 MogHA 服务，此时 MogHA 服务可以保证这两台机器上数据库实例的高可用，当主库发生不可修复的问题或者网络隔离时，MogHA 可以自主地进行故障切换和虚拟IP漂移。

![lite mode](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-lite-mote.png)

### Full 模式

Full模式相较于 lite 模式，需要在所有实例机器上运行 MogHA 服务，且所有的实例有由 MogHA 来自动管理，当出现主库故障时，会优先选择本机房同步备进行切换，如果本机房同步备也是故障的情况，会选择同城备机房的同步备进行切换。为了达到RPO=0，MogHA 不会选择异步备库进行切换，以防止数据丢失。该模式会在主备切换时，会自动修改数据库的复制连接及同步备列表配置。

举例：两地三中心【1主6备】

![full mode](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/overview-full-mode.png)
