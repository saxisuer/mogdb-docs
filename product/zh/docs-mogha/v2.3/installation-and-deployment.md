---
title: Installation and deployment
summary: Installation and deployment
author: Yao Qian
date: 2021-09-14
---

# 安装部署

## 安装包下载

请前往[发布记录](release-notes.md)页面下载 `mogha-2.3.0` 对应平台的安装包

## 安装包文件说明

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v23/installation-dir-tree.jpg)

## 系统环境要求

### gsql, gs_ctl 免密执行

请确认服务器上用数据库安装用户登录时，执行 gsql 或 gs_ctl 时无需输入密码。可以在数据库数据目录中，找到的 pg_hba.conf 认证文件，确认本地连接的校验方式为 trust。
检验方式：切换到 `omm` 用户下，执行下面的命令不需要输入密码即可查询到集群状态：

```bash
gs_ctl -D [PGDATA] query
```

### 数据库安装用户的 sudo 权限

由于 MogHA 需要自动挂虚拟IP的操作，内部需要通过 `ifconfig` 指令来操作网卡，MogHA 是通过数据库安装用户进行启动的，要想执行网卡操作就需要 sudo 权限，在安装期间脚本会检查 /etc/sudoers 配置中是否配置了运行用户的权限，如果存在就跳过配置，如果没有，会尝试自动的将 `omm` 用户添加到 `/etc/sudoers` 中，并赋予 `ifconfig` 的权限。

所以建议在部署 MogHA 服务之前，先检查一下 `/etc/sudoers` 中是否成功配置了 运行用户的 sudo 权限。
配置方式参考：

```bash
# 追加下列 1 行到文件末尾
omm     ALL=(ALL)       NOPASSWD: /usr/sbin/ifconfig
```

### MogHA通信端口互通

MogHA 需要一个固定的端口（默认为 8081，可配置，修改配置文件中的 `agent_port`）用于不同节点的 MogHA 之间通信，所以需确认要配置的端口可以在节点直接互相访问。

### 校对时间

使用 ntp 或 chronyd 校对主库和备库的时间。

## 安装步骤

> ! 注意：安装过程请使用 `root` 用户进行操作，涉及到给数据库安装用户sudo权限以及注册系统服务。

将安装包拷贝到要部署 MogHA 的服务器上，存放位置建议选择 `/opt` 或 `/home/[USER]` 下，以 `omm` 用户为例，存放目录为 `/home/omm/`，**为了便于演示，后面不再声明，默认使用该目录进行演示**。

### 解压缩安装包

```bash
cd /home/omm
tar -zxf mogha-2.x.x.tar.gz
```

解压后会在当前目录下得到一个 `mogha` 的文件夹。

### 安装 MogHA 服务

进入 mogha 文件夹，执行安装脚本：`sudo ./install.sh USER PGDATA [SERVICE_NAME]`（注意替换参数）

- `USER` 是指数据库安装用户，在本例中指 `omm` 用户
- `PGDATA` 是指数据库数据目录，本例中假设 MogDB 的数据目录为 `/opt/mogdb/data`
- `SERVICE_NAME` [可选参数] 注册 systemd 时的服务名，默认：mogha"

以下是安装过程示例（如果安装过程出错会终止后面的安装步骤）：

```bash
# 进入刚刚解压后的文件夹
cd /home/omm/mogha
# 执行安装脚本
sudo ./install.sh omm /opt/mogdb/data

# 安装过程，输出类似以下内容
[2021-10-30 14:29:36]: MogHA installation directory：/home/omm/mogha
[2021-10-30 14:29:36]: runtime user：omm, user group：dbgrp
[2021-10-30 14:29:36]: PGDATA=/opt/mogdb/data
[2021-10-30 14:29:36]: LD_LIBRARY_PATH=/opt/mogdb/app/lib:/opt/mogdb/tool/lib:/opt/huawei/install/om/lib:/opt/huawei/install/om/script/gspylib/clib:
[2021-10-30 14:29:36]: GAUSSHOME=/opt/mogdb/app
[2021-10-30 14:29:36]: database port: 26000
[2021-10-30 14:29:36]: architecture：aarch64
[2021-10-30 14:29:36]: modify owner for installation dir...
[2021-10-30 14:29:36]: generate mogha.service file...
[2021-10-30 14:29:36]: copy mogha.service to /usr/lib/systemd/system/
[2021-10-30 14:29:36]: reload systemd
[2021-10-30 14:29:37]: mogha service register successful
[2021-10-30 14:29:37]: add sudo permissions to omm
[2021-10-30 14:29:37]: add successful: NOPASSWD:/usr/sbin/ifconfig
[2021-10-30 14:29:37]: not found node.conf in current directory, generate it
node.conf generated，location：/home/omm/mogha/node.conf
[2021-10-30 14:29:37]: MogHA install successful!

Please edit /home/omm/mogha/node.conf first before start MogHA service!!!

Manage MogHA service by systemctl command:
    Start service：  sudo systemctl start mogha
    Stop service：   sudo systemctl stop mogha
    Restart service：sudo systemctl restart mogha

Uninstall MogHA service command：
    sudo ./uninstall.sh mogha

```

从安装脚本执行的输出结果可以大概看出安装的流程，安装成功的标志是看到输出的最后打印出了『**MogHA 安装成功!』**语句。其他情况，需根据实际情况，检查安装步骤哪里出错。

如果正常执行完安装脚本后，期间生成一个新文件：

- node.conf： HA 配置文件（如已存在则不会生成），需要用户自行补充填写实例和机房信息

### 完善 node.conf 配置

执行完安装脚本后，不要立即启动 MogHA 服务，我们需要先完善一下配置文件再启动服务。

当第一次安装的时候，`node.conf` 是没有的，安装脚本会根据 `node.conf.tmpl` 模板文件自动生成一份 `node.conf`， 得到该文件后，参考[配置文件](configuration-file.md)的介绍，完善您的机器和机房配置信息。

### 管理 MogHA 服务

请确认您已经配置完成 `node.conf` 以后，就可以通过 systemctl 指令来管理 MogHA 服务了。

**如果执行安装脚本时指定了 `SERVICE_NAME` , 后续操作的服务名需替换为自定义的服务名。**

#### 启动服务

```bash
sudo systemctl start mogha
```

#### 查看服务状态

```bash
sudo systemctl status mogha
● mogha.service - MogHA High Available Service
   Loaded: loaded (/usr/lib/systemd/system/mogha.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-18 22:48:51 CST; 5 days ago
     Docs: https://docs.mogdb.io/zh/mogha/v2.0/installation-and-depolyment
 Main PID: 193849 (mogha-2.2.3-dev)
    Tasks: 55
   Memory: 1.5G
   CGroup: /system.slice/mogha.service
           ├─ 193849 /home/omm/mogha/mogha-2.3.0-aarch64 -c /home/omm/mogha/node.conf
           ├─ 193862 mogha: watchdog
           ├─ 193863 mogha: http-server
           └─ 193864 mogha: heartbeat
```

#### 设置开机自启

```shell
sudo systemctl enable mogha
```

#### 重启 MogHA 服务

```bash
sudo systemctl restart mogha
```

#### 停止 MogHA 服务

```bash
sudo systemctl stop mogha
```

## 卸载 MogHA 软件

进入安装目录，执行卸载脚本即可：

```bash
cd /home/omm/mogha
sudo ./uninstall.sh
```

## 日志文件

MogHA 会生成两个日志文件：

- mogha_heartbeat.log
这个日志文件会存放在安装目录中，在本示例中为 `/home/omm/mogha`，这个日志主要记录HA的心跳日志，也是排查问题主要的优先检查的日志文件。
- mogha_web.log
这个日志文件存放目录和心跳日志相同。这个日志主要记录 web api 接口的请求日志。
