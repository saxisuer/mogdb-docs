---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# 配置文件

> !! 注意：MogHA 的配置文件采用 init 格式，所有的参数名前面不能包含空格或其他字符

MogHA 配置文件会在执行 install.sh 脚本时根据模板（node.conf.tmpl）自动生成，配置文件名为：node.conf，如果安装目录中已存在 node.conf 文件，则会跳过自动生成。配置文件格式采用 [ini 格式](https://baike.baidu.com/item/INI/9212321) 规则编写

ini文件由节、键、值组成。

  **节**  ：[section]

  **参数**（键=值） ：name=value

  **注解** ：注解使用分号(;)或井号(#)表示，在分号或井号后面的文字，直到该行结尾都全部为注解。

## 参数的键和值

所有的参数键均采用小写的蛇形命名规则定义，每个参数根据不同用途，可以接受布尔、字符串、整数这三种类型之一：

- 布尔：值可以写 `on`, `off`, `true`, `false`, `yes`, `no`, `1`, `0` （都是大小写不敏感的）
- 字符串：值即为字符串字面量，无需使用引号包裹
- 整数：直接填写数字，无需使用引号包裹

## 基础配置项

- **db_port** （整数）

  数据库端口，用于 MogHA 连接数据库

- **db_user** （字符串）

  数据库用户，用于 MogHA 连接和操作管理数据库，默认使用安装时执行 install.sh 脚本指定的系统用户名

- **db_datadir** （字符串）

  数据库的数据目录

- **primary_info** （字符串）

  MogHA 主备元数据的存储路径，默认值为安装目录下 primary_info 文件，支持 json 和 二进制存储，通过 `meta_file_type` 配置

- **standby_info** （字符串）

  MogHA 备元数据的存储路径，默认值为安装目录下 standby_info 文件，支持 json 和 二进制存储，通过 `meta_file_type` 配置

- **meta_file_type**（字符串）

  元数据存储格式，默认 json，支持两种格式json（值为：json）和二进制（值为：bin）。

- **lite_mode** （布尔）

    是否为 lite 模式，默认为 `True`。MogHA 通过该参数来区分

  - **lite 模式**：顾名思义，代表轻量级调度模式，lite 模式仅需要在主库和同步备库所在的服务器上启动 MogHA 服务。该模式下适用于1主1从部署结构或1主多从情况下仅在主和同步备部署MogHA的场景。

  - **非 lite 模式**：该模式下，需要运维人员在 node.conf 配置文件中，配置所有期望管理的服务器信息。同时，需要在所有配置的实例机器上运行 MogHA 服务。该模式下，在主库故障需要切换时，会在zone1/zone2 （同城）机房内选择最优备库进行切换，且自动修改主备实例的 repliconninfo 配置信息。

    切换时备库选择策略：

    - 如果主机房（`zone1`）内有同步备，优先切换到同机房同步备；
    - 如果同机房没有同步备，同城机房(`zone2`) 内有同步备，会切到同城的同步备；

- **agent_port** （整数）

  MogHA节点之间通信端口，如果有防火墙，需要配置互通。默认：8081

- **http_req_timeout** （整数）

  MogHA 节点间通信请求超时时间（单位：秒），默认：3

- **heartbeat_interval** （整数）

  MogHA 心跳间隔（单位：秒），默认：3

- **primary_lost_timeout** （整数）

  备库确认主库丢失的探测超时时间（单位：秒），默认：10。

  当备库无法 ping 通主库，但是可以 ping 通网关或者其他节点时，认为主库丢失（离线），当持续时间超过超时时间，备库就会发起故障切换，提升为主库。

- **primary_lonely_timeout** （整数）

  主库服务器上 MogHA 判断当前机器是否处于孤单（网络隔离）的超时时间（单位：秒），当无法 ping 通任何其他机器时，认为当前机器网络故障。当持续时间超过超时时间，MogHA 会下掉虚拟IP（如果有），关闭当前实例。

- **double_primary_timeout** （整数）

  主库服务器上 MogHA 在每轮检测心跳期间，都会监控当前集群中是否存在另外一个主库实例。发现后，表示当前集群存在双主的异常情况。如果双主状态持续时间超过该超时时间，进入自主选主。

- **taskset** （布尔）

  是否为数据库实例进程设置 CPU 亲和性，默认：False。

  当该字段设置为 True 时，会遵循如下规则进行CPU限制：

  获取当前机器的CPU核数，假设为 **N**

  >  当 **N >= 16** 时，设置数据库进程可用 CPU 数为  N-4 个
  >
  >  当 **8 <= N <16**时，设置数据库进程可用 CPU 数为  N-2 个
  >
  >  当 **1 < N < 8** 时，设置数据库进程可用 CPU 数为  N-1 个
  >
  >  当 **N = 1**时，不做限制
- **logger_format** （字符串）

  日志格式，默认：`%(asctime)s %(levelname)s [%(filename)s:%(lineno)d]: %(message)s`

  更多字段请参考 [python3 logging logrecord-attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes)

- **log_dir** （字符串）

  日志存储目录，默认为安装目录

- **log_max_size** （字符串）

  单个日志文件最大字节数，接近该值时，将发生日志滚动。默认：512MB，支持的单位：KB, MB, GB （忽略大小写）

- **log_backup_count** （整数）

  日志留存的文件个数，默认：10

- **allow_ips** （字符串）

  允许外部可请求 MogHA 集群 HTTP API 的IP列表，英文逗号分割填写

- **handle_down_primary** （布尔）

  检测到主库进程宕机时，MogHA 是否需要处理（重启或切换），默认：True。

  该参数是 `primary_down_handle_method` 参数生效的前提

- **handle_down_standby** （布尔）

  检测到备库进程宕机时，MogHA 是否需要拉起

- **primary_down_handle_method** （字符串）

  主库进程未启动时，MogHA 的处理方式，默认：restart。支持重启（值：restart）和切换（值：failover）

  当该参数的值为 restart 时，需配置 `restart_strategy` 参数使用。

- **restart_strategy** （字符串）

  重启实例进程最大尝试边界，格式为 `最大次数/超时分钟` ，其中两个值均可限定尝试重启的终止。默认：10/3

  假设该参数为 10/3 ，则表示最多尝试重启数据库10次或者重启超时时间为3分钟，任何一个条件先满足就终止尝试重启，进入故障切换。

- **uce_error_detection** （布尔）

  UCE（uncorrected error）故障感知功能，默认：True

  *UCE错误：数据库内核进程在收到系统内核的一些特定的 sigbus 信号错误时，比如内存坏块等，内核会进行优雅停止，将错误码写入数据库日志中后退出进程。而坏的内存块会被系统内核标记，以后就不会被再次分配给应用程序，所以当出现该错误时，直接重启数据库进程即可恢复服务。*

- **uce_detect_max_lines** （整数）

  UCE错误检查时，从后向前读取数据库日志的行数，默认：200

- **debug_mode** （布尔）

  调试模式，开启后，MogHA 打印日志的级别将降为 DEBUG，输入更多运行日志。默认：False

## 元数据库（可选）

默认无需配置

MogHA 保留元数据库存储的功能，便于第三方监控系统读取数据库集群状态。但 MogHA 不依赖元数据库做逻辑判断，如果配置了该部分字段，MogHA 在运行期间会同步输出元数据信息到元数据库中。

### 配置项

- **ha_name** （字符串）

  MogHA集群名，全局唯一，禁止两套HA集群共用一个名字

- **host** （字符串）

  元数据库的机器IP

- **port** （整数）

  元数据库连接端口

- **db** （字符串）

  元数据库数据库名

- **user** （字符串）

  元数据库连接用户名

- **password** （字符串）

  元数据库连接用户密码

- **connect_timeout** （整数）

  元数据库连接超时时间（单位：秒），默认：3

## 机器配置

host1 到 host9 分别代表不同的数据库实例所在的机器。

下面以 host1 为例介绍参数字段，其他相同。

```ini
[host1]
ip=
heartbeat_ips=
```

- **ip** （字符串）

  必填，HA节点间通信IP (**确保和数据库日志复制IP一致**)

- **heartbeat_ips** （字符串）

  选填，HA节点间通信的冗余 ip，该字段为选填项，作为 `ip` 请求失败时的备用IP。

  如果有多个备用IP，以英文逗号分隔连接填写即可

## 机房配置

zone1 到 zone3 分别代表主机房，同城备机房，异地机房。

下面以 zone1 为例介绍参数字段，其他相同。

```ini
[zone1]
hosts=
ping_list=
vip=
cascades=
arping=
vip_bind_nic=
vip_netmask=
```

- **vip** （字符串）

  选填，当前机房的虚拟IP，如果存在，MogHA 会自动将该虚拟挂载到本机房内主库所在的机器上，没有则留空。

- **hosts** （字符串）

  必填，当前机房内的机器列表，值的内容为对应机器的节（ section ）名，多个机器以英文逗号分隔。

  比如：zone1 中有 host1 机器和 host2 机器，则该字段配置为：`hosts=host1,host2`

- **ping_list** （字符串）

  必填，当前机房内用户判断机器网络是否正常的仲裁节点，一般可配置为机房的网关地址。允许配置多个仲裁IP，以英文逗号分隔。

- **cascades** （字符串）

  选填，当前机房内的级联备库机器列表，值得内容和 `hosts` 相同，没有则留空。

- **arping** （字符串）

  选填，广播地址，用于上线虚拟IP后的广播，没有则留空。

- **vip_bind_nic** (字符串)

  [2.4.0新增] 选填，虚拟IP要绑定的网卡名, 默认使用当前 host 配置中的 ip 的网卡

- **vip_netmask** (字符串)

  [2.4.0新增] 选填，虚拟ip的子网掩码, 默认和当前 host 配置中 ip 同一网段
