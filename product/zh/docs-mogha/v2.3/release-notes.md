---
title: Release Notes
summary: Release Notes
author: Yao Qian
date: 2021-09-14
---

# 发布记录

## mogha-2.4.2 (2022.12.02)

- openEuler-arm64: [mogha-2.4.2-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.2-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.2-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.2/mogha-2.4.2-CentOS-x86_64.tar.gz)

Change Log:

- 修复对于未启动的实例角色的判断

## mogha-2.4.1 (2022.11.28)

- openEuler-arm64: [mogha-2.4.1-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.1-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.1-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.1/mogha-2.4.1-CentOS-x86_64.tar.gz)

Change Log:

- 支持对于实例 Starting/Building/Catchup 状态的检测
- 修复启动实例时针对 Starting 状态重复启动的问题

## mogha-2.4.0 (2022.11.24)

- openEuler-arm64: [mogha-2.4.0-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.4.0-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.4.0-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.4.0/mogha-2.4.0-CentOS-x86_64.tar.gz)

Change Log:

- node.conf 中 Zone 配置中新增 `vip_bind_nic`, `vip_netmask` 配置
- HA节点之间 api 通信优先使用 `ip`，请求失败使用冗余IP `heartbeat_ips` 

## mogha-2.3.8 (2022.11.2)

- openEuler-arm64: [mogha-2.3.8-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.8-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.8-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.8/mogha-2.3.8-CentOS-x86_64.tar.gz)

Change Log:

- 修复 Full 模式下主机房没有同步备时，不切换的问题

## mogha-2.3.7 (2022.9.8)

- openEuler-arm64: [mogha-2.3.7-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.7/mogha-2.3.7-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.7-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.7/mogha-2.3.7-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.7-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.7/mogha-2.3.7-CentOS-x86_64.tar.gz)

Change Log:

- 动态感知集群实例数量，full 模式下，自动切换 lite 和 full
- full 模式双冗余来接管主库

## mogha-2.3.6 (2022.8.14)

- openEuler-arm64: [mogha-2.3.6-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.6-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.6-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.6/mogha-2.3.6-CentOS-x86_64.tar.gz)

Change Log:

- 增加心跳错误类型，已知的心跳错误由抛异常改为输出错误日志

## mogha-2.3.5 (2022.5.10)

- openEuler-arm64: [mogha-2.3.5-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.5-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.5-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.5/mogha-2.3.5-CentOS-x86_64.tar.gz)

Change Log:

- 切换时增加虚拟IP冲突检测

## mogha-2.3.4 (2022.4.24)

- openEuler-arm64: [mogha-2.3.4-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.4-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.4-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.4/mogha-2.3.4-CentOS-x86_64.tar.gz)

Change Log:

- 优化心跳请求，使用长连接减少 socket 占用数量和通信时延
- 优化心跳逻辑，请求多个IP时改为并发请求，缩短时延
- 优化日志磁盘无法写入的情况处理，避免进程阻塞
- 调整心跳使用的IP优先级，优先使用心跳IP，失败后使用业务IP

## mogha-2.3.2 (2022.3.15)

- openEuler-arm64: [mogha-2.3.2-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.2-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.2-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.2/mogha-2.3.2-CentOS-x86_64.tar.gz)

Change Log:

- Fix: 切换后路由缓存刷新失败的bug
- Feat: 关闭数据库采用 `immediate` 模式
- Fix(Full模式): 切换后集群 `replconninfo` 配置错误

## mogha-2.3.0 (2021.11.30)

- openEuler-arm64: [mogha-2.3.0-openEuler-arm64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-openEuler-arm64.tar.gz)
- openEuler-x86_64: [mogha-2.3.0-openEuler-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-openEuler-x86_64.tar.gz)
- Centos-x86_64: [mogha-2.3.0-CentOS-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.3.0/mogha-2.3.0-CentOS-x86_64.tar.gz)

Change Log:

- 调整系统架构，去除python依赖
- 支持单机并行部署多套HA
- lite模式: failover前，开启最大可用模式
- 动态获取业务网卡子网掩码用作虚拟IP子网掩码
- 备库状态为Need Repair 时，自动尝试增量重建一次
- 定义人性化进程cmdline，便于区分
- 更新 mogha.service.tmpl 模板
- 优化代码，修复部分Bug

## mogha-2.2.2  (2021.10.14)

- arm: [mogha-2.2.2-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-aarch64.tar.gz)
- x86: [mogha-2.2.2-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-x86_64.tar.gz)

Change Log:

- 新增备库切换前最大可用检查
- 新增备库切换后主库角色检查
- 优化切换流程
- 优化备库重启流程

## mogha-2.2.1  (2021.9.10)

- arm: [mogha-2.2.1-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-aarch64.tar.gz)
- x86: [mogha-2.2.1-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-x86_64.tar.gz)

Change Log:

- 增加元数据库连接超时控制为3s
- 增加 http 接口请求超时控制为3s
- 修复http接口鉴权时 allow_ips 未包含虚拟ip的错误
- 优化代码结构

## mogha-2.2.0  (2021.9.7)

- arm : [mogha-2.2.0-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-aarch64.tar.gz)
- x86：[mogha-2.2.0-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-x86_64.tar.gz)

Change Log:

- 修改版本管理方式
- 元数据库表结构变更
- 支持自动发现数据库配置

## mogha-2.1.1 (2021.7.14)

- [mogha-2.1.1.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.1.1/mogha-2.1.1.tar.gz) (支持 x86_64, aarch64)

Change Log:

- 汉化脚本输出
- 新增MogHA 卸载脚本
- 新增支持UCE故障检测
- 新增磁盘故障检查
