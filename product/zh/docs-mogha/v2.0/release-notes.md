---
title: Release Notes
summary: Release Notes
author: Yao Qian
date: 2021-09-14
---

# 发布记录

## mogha-2.2.2  (2021.10.14)

- arm: [mogha-2.2.2-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-aarch64.tar.gz)
- x86: [mogha-2.2.2-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.2/mogha-2.2.2-x86_64.tar.gz)

Change Log:

- 新增备库切换前最大可用检查
- 新增备库切换后主库角色检查
- 优化切换流程
- 优化备库重启流程

## mogha-2.2.1  (2021.9.10)

- arm: [mogha-2.2.1-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-aarch64.tar.gz)
- x86: [mogha-2.2.1-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.1/mogha-2.2.1-x86_64.tar.gz)

Change Log:

- 增加元数据库连接超时控制为3s
- 增加 http 接口请求超时控制为3s
- 修复http接口鉴权时 allow_ips 未包含虚拟ip的错误
- 优化代码结构

## mogha-2.2.0  (2021.9.7)

- arm : [mogha-2.2.0-aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-aarch64.tar.gz)
- x86：[mogha-2.2.0-x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.2.0/mogha-2.2.0-x86_64.tar.gz)

Change Log:

- 修改版本管理方式
- 元数据库表结构变更
- 支持自动发现数据库配置

## mogha-2.1.1 (2021.7.14)

- [mogha-2.1.1.tar.gz](https://cdn-mogdb.enmotech.com/mogha/2.1.1/mogha-2.1.1.tar.gz) (支持 x86_64, aarch64)

Change Log:

- 汉化脚本输出
- 新增MogHA 卸载脚本
- 新增支持UCE故障检测
- 新增磁盘故障检查
