---
title: Installation and deployment
summary: Installation and deployment
author: Yao Qian
date: 2021-09-14
---

# 安装部署

<br/>

## 安装包下载

请前往[发布记录](release-notes.md)页面下载安装包，下载后得到一个类似 `mogha-x.x.x.tar.gz` 的压缩包（`x` 代表版本号）

<br/>

## 安装包文件说明

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/installation-dir-tree.png)

<br/>

## 系统环境要求

### python 3.6+

由于 MogHA 下层组件 supervisord 在运行时依赖 python3.6 （或更高版本也可）版本的环境，所以请在安装前确认当前服务器能够正常执行 python3 指令，同时检查 python3 的版本。
检查方式：

```bash
command -v python3
# 输出 /usr/bin/python3 路径不一定一样，确保存在即可
python3 -V
# 输出的版本在 Python 3.6 或更高版本
```

### gsql, gs_ctl 免密执行

请确认服务器上用数据库安装用户登录时，执行 gsql 或 gs_ctl 时无需输入密码。可以在数据库数据目录中，找到的 pg_hba.conf 认证文件，确认本地连接的校验方式为 trust。
检验方式：切换到 `omm` 用户下，执行下面的命令不需要输入密码即可查询到集群状态：

```bash
gs_ctl -D [PGDATA] query
```

### 数据库安装用户的 sudo 权限

由于 MogHA 需要自动挂虚拟IP的操作，内部会通过 `ifconfig` 指令来操作网卡，MogHA 会通过数据库安装用户进行启动的，要想执行网卡操作就需要 sudo 权限，在安装期间 `install.sh` 脚本中会尝试自动的将 `omm` 用户添加到 `/etc/sudoers` 中，并赋予 `ifconfig` 和 `systemctl` 的权限，但不能完全确保操作成功。所以建议在部署 MogHA 服务之前，先检查一下 `/etc/sudoers` 中是否成功配置了 omm 用户的 sudo 权限。
配置方式参考：

```bash
配置用户的sudo权限
chmod +w /etc/sudoers

which systemctl
# /usr/bin/systemctl
which ifconfig
# /usr/sbin/ifconfig

vi /etc/sudoers
# 追加下列 2 行到文件末尾
omm     ALL=(ALL)       NOPASSWD: /usr/sbin/ifconfig
omm     ALL=(ALL)       NOPASSWD: /usr/sbin/systemctl
# 保存退出
chmod -w /etc/sudoers
```

### 开放通信端口

MogHA 需要一个固定的端口（默认为 8081，可配置，修改配置文件中的 agent_port）用于不同节点的 MogHA 之间通信，所以需确认要配置的端口可以在节点直接互相访问。

### 校对时间

使用 ntp 或 chronyd 校对主库和备库的时间。

<br/>

## 安装步骤

> ！注意：安装过程请使用 `root` 用户进行操作，涉及到给数据库安装用户sudo权限以及注册系统服务。

将安装包拷贝到要部署 MogHA 的服务器上，存放目录建议选择数据库安装用户的家目录，以 `omm` 为例，则存放目录为 `/home/omm/`，**为了便于演示，后面不再声明，默认使用该目录进行演示**。

### 解压缩安装包

```bash
cd /home/omm
tar -zxf mogha-2.x.x.tar.gz
```

解压后会在当前目录下得到一个 `mogha` 的文件夹。

### 安装 MogHA 服务

进入 mogha 文件夹，执行安装脚本：`./install.sh USER PGDATA`（注意替换参数）

- `USER` 是指数据库安装用户，在本例中指 `omm` 用户
- `PGDATA` 是指数据库数据目录，本例中假设 MogDB 的数据目录为 `/opt/mogdb/data`

以下是安装过程示例：

```bash
# 进入刚刚解压后的文件夹
cd /home/omm/mogha
# 执行安装脚本
./install.sh omm /opt/mogdb/data

# 安装过程，输出类似以下内容
[2021-07-01 10:43:24]: MogHA 安装目录为：/home/omm/mogha
[2021-07-01 10:43:24]: 数据库安装用户为：omm, 所在用户组为：dbgrp
[2021-07-01 10:43:24]: 数据库数据目录为：/opt/mogdb/data
[2021-07-01 10:43:24]: LD_LIBRARY_PATH=/opt/mogdb/app/lib:/opt/mogdb/tool/lib:
[2021-07-01 10:43:24]: GAUSSHOME=/opt/mogdb/app
[2021-07-01 10:43:24]: 当前系统架构为：aarch64
[2021-07-01 10:43:24]: 修改安装目录及其子目录或文件的用户和用户组...
[2021-07-01 10:43:24]: 链接系统的 python3 解释器到虚拟环境...
[2021-07-01 10:43:24]: 当前系统中 Python3 的路径为：/usr/bin/python3
[2021-07-01 10:43:24]: Python3 的版本为：3.7
[2021-07-01 10:43:24]: 虚拟环境中 python 解释器链接成功
[2021-07-01 10:43:24]: 生成 mogha.service 文件...
[2021-07-01 10:43:24]: 复制 mogha.service 文件到 /usr/lib/systemd/system/
[2021-07-01 10:43:24]: 重载配置文件
[2021-07-01 10:43:24]: mogha 服务注册成功
[2021-07-01 10:43:24]: 用户 omm 已经存在于 /etc/sudoers 文件中，请自行确认权限!
[2021-07-01 10:43:24]: 生成 supervisord.conf 配置文件...
[2021-07-01 10:43:24]: supervisord.conf 生成成功
[2021-07-01 10:43:24]: 发现 node.conf 已经存在，不再自动生成
[2021-07-01 10:43:24]: MogHA 安装成功!

请修改 /home/omm/mogha/node.conf 配置文件后再启动服务 !!!

启动 MogHA 的命令:
    systemctl start mogha
```

从安装脚本执行的输出结果可以大概看出安装的流程，安装成功的标志是看到输出的最后打印出了『**MogHA 安装成功!』**语句。其他情况，需根据实际情况，检查安装步骤哪里出错。

如果正常执行完安装脚本后，期间会生成 3 个新的文件：

- node.conf： HA 配置文件（如已存在则不会生成），需要我们完善实例和机房信息
- supervisord.conf： supervisord 的配置文件 （supervisord 是进程高可用组件）
- mogha.service：用于注册 systemd 的服务描述文件，安装过程已经将该文件注册到了 systemd 的服务中，可以通过 systemctl 工具来管理 MogHA 服务

#### 完善 node.conf 配置

执行完安装脚本后，不要立即启动服务，MogHA 的运行依赖安装目录中 `node.conf` 的配置，我们需要先正确的配置了这个文件再启动服务。当第一次安装的时候，`node.conf` 一般是没有的，脚本会根据 `node.conf.tmpl` 模板文件自动生成一份 `node.conf`， 所以可以看到执行的输出最后重点强调了: `请根据您的集群信息修改 node.conf 中的配置后再启动服务`，下一节将介绍如何修改配置文件 `node.conf` 。

### 启动 MogHA 服务

确认配置完成 `node.conf` 以后，就可以启动 HA 服务，启动方式为：

```bash
sudo systemctl start mogha
```

启动后检查 mogha 的运行状态:

```bash
sudo systemctl status mogha
● mogha.service - MogDB/openGuass HA manager service
   Loaded: loaded (/usr/lib/systemd/system/mogha.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-06-22 10:17:19 CST; 2s ago
 Main PID: 832531 (supervisord)
    Tasks: 16
   Memory: 144.1M
   CGroup: /system.slice/mogha.service
           ├─832531 /home/omm/mogha/prod_venv/bin/python /home/omm/mogha/prod_venv/bin/supervisord -c /home/omm/mogha/supervisord.conf
           ├─832548 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --heartbeat
           ├─832549 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --web
           ├─832550 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --heartbeat
           └─832551 /home/omm/mogha/mogha_2_1_aarch64 --config /home/omm/mogha/node.conf --web

Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: mogha.service: Succeeded.
Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: Stopped MogDB/openGuass HA manager service.
Jun 22 10:17:19 mogdb-manager-0004 systemd[1]: Started MogDB/openGuass HA manager service.
Jun 22 10:17:19 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:19,967 INFO supervisord started with pid 832531
Jun 22 10:17:20 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:20,969 INFO spawned: 'heartbeat' with pid 832548
Jun 22 10:17:20 mogdb-manager-0004 supervisord[832531]: 2021-06-22 10:17:20,971 INFO spawned: 'web' with pid 832549
```

### 设置开机自启

```shell
sudo systemctl enable mogha
```

### 重启 MogHA 服务

```bash
sudo systemctl restart mogha
```

### 停止 MogHA 服务

```bash
sudo systemctl stop mogha
```

<br/>

## 卸载 MogHA 软件

进入安装目录，执行卸载脚本即可：

```bash
cd /home/omm/mogha./uninstall.sh
# 过程输出如下
[2021-07-01 11:09:07]: 尝试停止 mogha (systemctl stop mogha)
[2021-07-01 11:09:08]: 删除注册文件[2021-07-01 11:09:08]: 卸载成功
```

<br/>

## 日志文件

MogHA 会生成两个日志文件：

- mogha_heartbeat.log
这个日志文件会存放在安装目录中，在本示例中为 `/home/omm/mogha`，这个日志主要记录HA的心跳日志，也是排查问题主要的优先检查的日志文件。
- mogha_web.log
这个日志文件存放目录和心跳日志相同。这个日志主要记录 web api 接口的请求日志。
