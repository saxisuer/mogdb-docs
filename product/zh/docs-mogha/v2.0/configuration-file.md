---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# 配置文件

## node.conf 的配置

### 必须用户确认配置的字段

- 数据库连接端口

    ```ini
    # 数据库端口，用于连接数据库
    db_port=0
    ```

- HA通信端口

    ```ini
    # ha节点之间通讯用的端口，如果有防火墙，需要打开这个端口的访问
    agent_port=8081
    ```

- host 主机列表（仅选取 host1 做演示，其他相同）
  主机列表需要用户将自己的数据库集群中的所有主备实例所在的服务器的业务IP填写到 `ip` 字段。
  如果没有心跳IP的话，可以不填写置空，如果有则填写，如果有多个心跳IP，则以英文半角逗号分隔填写

    ```ini
    # 这部分从host1-8，每个代表一个机器
    [host1]
    # 业务网络ip
    ip=192.168.1.111
    # 心跳网络ip，允许配置多个心跳网络，以逗号隔开
    heartbeat_ips=192.168.1.112,192.168.1.113
    ```

- zone 机房列表（仅选取 zone1 做演示，其他相同）

    zone 在这里代表的是机房的概念

  - `vip`: 不同的机房拥有各自的虚拟IP（VIP）,虚拟IP用于业务访问，解耦服务器的私有IP，便于主备切换
  - `arping`: 可以留空，系统会采用广播地址
  - `ping_list`: 用于检查网络的中间节点IP，一般可以选择网关地址
  - `hosts`: 本机房中的主机列表，特别强调，这里**不是**填写主机名或主机IP，这里是用于**归类**上一步中填写的 host 列表，所以这个字段应该填写的是本配置文件中定义的 `host1`，`host2` ... 等等这种 section 名，举例：假如 `host1`, `host2` 这两个机器同属于 `zone1` 这个机房，那么这里的 `hosts` 应该填写为： - `hosts=host1,host2` （以英文半角逗号分隔）
  - `cascade`:  本机房中的级联备列表，配置方式同 `hosts`

    ```ini
    # 这部分主要用于区分机房，不同机房会有自己的VIP
    [zone1]
    # 本机房的VIP
    vip=
    # 本机房的arping地址，如果不知道，可以留空，会采用arp广播模式
    arping=
    # 检查网络的中间节点，一般是可以ping通的网关地址
    ping_list=
    # 本机房的机器列表，例如 host1,host2,host3
    hosts=
    # 本机房的级联备库列表
    cascade=
    ```

### 其他可选项配置

```ini
[config]
# 心跳间隔时间，也是主流程检查的间隔时间，一般设置为3-5s
heartbeat_interval=3

# 主库丢失的探测时间，当主库宕机无法ping到，认为持续到这个时长之后，备库可以切换为主库
primary_lost_timeout=10

# 主库的孤单时间，当主库无法连接到其他机器，ping不到其他任何机器，认为主库网络出现问题
# 当持续超过这个时长，认为主库实例需要关闭(mogdb中会设置为readonly)
primary_lonely_timeout=10

# 当双主状态持续指定时长之后，认为需要做双主判断，
# 因为在维护性操作中，会有可能存在极短时间的双主状态（此时事实上数据库不允许写入）
double_primary_timeout=10

# 数据库的操作系统用户，通常为omm或者postgres
db_user=__USER__

# 数据库的数据目录，用于信号发送等本地通讯行为
db_datadir=__PGDATA__

# 本地元数据文件类型:
#   - "json" : json 格式
#   - "bin" : 二进制格式 (如果该参数没有配置，默认使用 bin)
meta_file_type=json

# 本地元数据存储位置，自动维护无需人工干预，当元数据库不可用时候，用这个数据作为主库判断
primary_info=__INSTALLDIR__/primary_info

# 本地元数据存储位置，自动维护无需人工干预，当主库丢失，备库依赖这个去判断自身同步状态
standby_info=__INSTALLDIR__/standby_info

# 是否为数据库实例设置cpu限制，True的话，会保留至少一个cpu出来不分配给数据库用
taskset=True

# 是否使用 lite 模式，可选值：True / False
# lite 模式下，即便是一主多备，也只需在主和同步备启动服务。
# 该值为 False 的话，即为 full 模式，需要在所有节点启动高可用服务。
# 二者的区别：lite 模式，HA服务不会修改数据库的主备相关配置，而 full 模式下会自动修改
lite_mode=False

# 是否使用元数据库。
# 如果值为 True，需在下面的 [meta] 部分，配置元数据库连接参数，
# 如果值为 False，[meta] 部分可以置空
use_meta=False

# 设置输出的日志格式
logger_format=%(asctime)s - %(name)s - %(levelname)s [%(filename)s:%(lineno)d]: %(message)s

# 设置除了主备相关的机器，允许可以访问到web接口的IP列表, 多个IP时英文半角逗号分隔
allow_ips=

# [v2.1新增] 主库实例进程未启动时，如何处理
# 支持两种处理方式：
#   restart: 尝试重启，尝试次数在 restart_instance_limit 参数中设定
#   failover: 备库升为主，主库尝试重建数据库后启动为备
primary_down_handle_method=failover

# [v2.1新增] 重启实例最大尝试条件: times/minutes
# 例如： 10/3 最多尝试10次或者3分钟，任何一个条件先满足就不再尝试。
restart_strategy=10/3

# 这部分都是元数据库的连接参数（opengauss数据库），
# 不强依赖，配置错误会在日志中提示，但不影响 HA 调度
[meta]
ha_name=    # ha主备的名称，全局必须唯一，禁止两套主备共用一个名字
host=       # 数据库机器
port=       # 数据库端口
db=         # 数据库的database
user=       # 用户名
password=   # 密码
schema=     # 使用的schema
```
