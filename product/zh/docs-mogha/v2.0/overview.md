---
title: MogHA简介
summary: MogHA简介
author: Yao Qian
date: 2021-09-14
---

# 关于MogHA

MogHA是[云和恩墨](https://enmotech.com/)基于 [MogDB](https://www.mogdb.io/) 同步和异步流复制技术自研的一款企业级高可用软件系统，适用于 MogDB 和 openGauss 数据库的高可用需求。MogHA 主要针对服务器宕机、实例宕机等多种情况，实现主备自动切换和虚拟IP的自动漂移，使数据库的故障持续时间从分钟级降到秒级，确保业务系统的持续运行。

<br/>

## 为什么数据库支持主备，还需要 MogHA

首先我们需要理解一下什么是高可用，高可用的目的是为了让数据库尽可能提供不间断的服务，以保证上层业务的稳定运行。数据库虽然支持主备库的部署结构，其目的是防止单点故障。但数据库并不提供故障检测以及自动化切换主备的功能，这也不属于数据库的业务范畴。所以说，需要有 MogHA 这样的一套系统，用来监控实例的状态，监控主备的状态，以及在探测到故障时能够立即做出合理的主备切换操作。

<br/>

## 功能特性

- 自主发现数据库实例角色
- 实时监控数据库实例进程健康状态
- 支持数据库实例故障处理或 Failover
- 支持网络故障检测
- 支持磁盘故障检测
- 支持虚拟IP自动漂移
- 双主脑裂处理，自动选主
- 支持数据库进程和CPU的亲和性绑定
- HA自身进程高可用
- 支持 x86_64 和 aarch64

<br/>

## MogHA 部署结构

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-mogha-arch.png)

MogHA 在安装包中实现了一键部署的功能，大大简化了部署过程，自动将 MogHA 的 web 进程和 heartbeat 进程托管在 supervisord 中（supervisord 已集成在安装包中），实现 MogHA 自身的高可用。并将 MogHA 服务自动注册到 systemd 中，便于运维者通过 systemctl 指令快速的管理 MogHA 服务。

<br/>

## MogHA 通信模型

MogHA 的通信基于 HTTP 协议，内部实现了一套不同节点间 MogHA 的交互接口。通过配置文件中用户定义的方式实现节点间的互信机制。集群外的请求会被有效的拒绝，以保证 MogHA 集群的安全性。

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-communicate-strategy.png)

<br/>

## MogHA 支持的模式

### Lite 模式

Lite 模式，顾名思义即轻量级模式，该模式仅需在主库和一台同步备机器上启动 MogHA 服务，此时 MogHA 服务可以保证这两台机器对于应用的连续性服务，当主库发生不可修复的问题或者网络隔离时，MogHA 可以实现即时数据库的 Failover 切主和虚拟IP的漂移操作。

该模式适用于数据库数量仅有两台或限制切换到其他机器等场景

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-lite-mode.png)

### Full 模式（Beta）

该模式需要在启动 MogHA 服务前，在配置文件中配置所有数据库实例的信息（最多9个实例[1主8备]），然后在所有实例机器上运行 MogHA 服务。该模式会在主备切换时，修改数据库的复制连接配置（replconninfo ）。

举例：两地三中心【1主6备】

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogha/v20/overview-full-mode.png)

<br/>

## Lite 模式和 Full 模式的区别

### **数据库配置 repliconninfo**

lite 模式切主后不会修改复制关系配置，full 模式会。
修改方式：主库配置为所有实例的复制关系，而备库只保留一个和主库的复制关系。

### **MogHA配置 ping_list**

lite 模式时必需在 MogHA 的配置文件（node.conf）中配置机器所在 zone 的 ping_list 字段，full 模式下
不强制要求填写。因为 Lite 模式时 MogHA 维护的集群内仅存在两台机器，当出现其中一台出现网络故障
或网络隔离时，两节点被分隔后，单节点无法判断自己的网络是否处于正常状态，所以需要用户配置一个或
多个额外的 IP （也就是 ping_list）来用于网络判断， ping_list 字段可以填写两台机器共同的网关地址，
或者两台机器都可以 ping 通的第三台机器的 IP。
