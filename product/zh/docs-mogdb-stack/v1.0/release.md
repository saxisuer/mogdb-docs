---
title: 发布记录
summary: 发布记录
author: Wang Dong
date: 2022-06-30
---

# 发布记录

## v1.0.0

本次发布 mgo客户端 v1.0.0 和 MogDB operator v1.0.0

[MogDB operator](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar)

mgo 客户端

+ [mgo_linux_x86_64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_linux_x86_64)
+ [mgo_linux_arm64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_linux_arm64)
+ [mgo_darwin_x86_64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_darwin_x86_64)
+ [mgo_darwin_arm64](https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mgo_darwin_arm64)