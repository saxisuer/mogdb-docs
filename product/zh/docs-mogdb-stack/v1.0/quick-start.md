---
title: 快速上手
summary: 快速上手
author: Ji Yabin
date: 2022-06-16
---

# 快速上手

本文介绍了如何创建一个简单的 Kubernetes 集群，部署 MogDB Operator，并使用 Mogdb Operator 部署 MogDB 集群。

> 本文中的部署说明仅用于测试目的，不要直接用于生产环境。如果要在生产环境部署，请参阅部署 > 部署 MogDB 集群章节。

基本步骤如下：

[1. 创建Kubernetes测试集群](#创建kubernetes测试集群)

[2. 部署MogDB Operator](#部署mogdb-operator)

[3. 部署MogDB集群](#部署mogdb集群)

[4. 连接MogDB集群](#连接mogdb集群)

[5. 销毁MogDB集群](#销毁mogdb集群)

<br/>

## 创建Kubernetes测试集群

本节介绍如何使用 minikube 部署 Kubernetes 集群。

minikube 可以在虚拟机中创建一个 Kubernetes 集群，可在 macOS, Linux 和 Windows 上运行。

部署前，请确保满足以下要求：

- minikube：版本 1.18.1+

- minikube 需要安装一个兼容的 hypervisor，详情见官方安装教程。

- kubectl: 版本 >= 1.18.1

安装完 minikube 后，可以执行下面命令启动一个 Kubernetes 集群：

```shell
minikube start
```

如果一切运行正常，会看到类似下面的输出，根据操作系统和使用的 hypervisor 会有些许差异。

```shell
😄  minikube v1.10.1 on Darwin 10.15.4
✨  Automatically selected the hyperkit driver. Other choices: docker, vmwarefusion
💾  Downloading driver docker-machine-driver-hyperkit:
    > docker-machine-driver-hyperkit.sha256: 65 B / 65 B [---] 100.00% ? p/s 0s
    > docker-machine-driver-hyperkit: 10.90 MiB / 10.90 MiB  100.00% 1.76 MiB p
🔑  The 'hyperkit' driver requires elevated permissions. The following commands will be executed:

    $ sudo chown root:wheel /Users/user/.minikube/bin/docker-machine-driver-hyperkit
    $ sudo chmod u+s /Users/user/.minikube/bin/docker-machine-driver-hyperkit


💿  Downloading VM boot image ...
    > minikube-v1.10.0.iso.sha256: 65 B / 65 B [-------------] 100.00% ? p/s 0s
    > minikube-v1.10.0.iso: 174.99 MiB / 174.99 MiB [] 100.00% 6.63 MiB p/s 27s
👍  Starting control plane node minikube in cluster minikube
💾  Downloading Kubernetes v1.18.2 preload ...
    > preloaded-images-k8s-v3-v1.18.2-docker-overlay2-amd64.tar.lz4: 525.43 MiB
🔥  Creating hyperkit VM (CPUs=2, Memory=4000MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.18.2 on Docker 19.03.8 ...
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
```

对于中国大陆用户，可以使用国内 gcr.io mirror 仓库，例如 registry.cn-hangzhou.aliyuncs.com/google_containers。

```shell
minikube start --image-repository registry.cn-hangzhou.aliyuncs.com/google_containers
```

或者给 Docker 配置 HTTP/HTTPS 代理。

将下面命令中的 127.0.0.1:1086 替换为您自己的 HTTP/HTTPS 代理地址：

```shell
minikube start --docker-env https_proxy=http://127.0.0.1:1086 \
      --docker-env http_proxy=http://127.0.0.1:1086
```

> **注意:** </br>
> 由于 minikube（默认）通过虚拟机运行，127.0.0.1 指向虚拟机本身，所以在有些情况下可能需要将代理修改为您的主机的实际 IP。

参考 minikube setup 查看配置虚拟机和 Kubernetes 集群的更多选项。

你可以使用 minikube 的子命令 kubectl 来进行集群操作。要使 kubectl 命令生效，你需要在 shell 配置文件中添加以下别名设置命令，或者在打开一个新的 shell 后执行以下别名设置命令。

```shell
alias kubectl='minikube kubectl --'
```

执行以下命令检查集群状态，并确保可以通过 kubectl 访问集群:

```shell
kubectl cluster-info
```

期望输出：

```shell
Kubernetes master is running at https://192.168.64.2:8443
KubeDNS is running at https://192.168.64.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

现在就可以开始部署 MogDB Operator 了！

测试完成后，执行下面命令来销毁集群：

```shell
minikube delete
```

<br/>

## 部署MogDB Operator

开始之前，确保以下要求已满足：

- 可以使用 kubectl 访问的 Kubernetes 集群
- 已安装 Kustomize v3+

**步骤1: 下载样例**

```shell
wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar
tar xf mogdb-operator-examples.tar
cd mogdb-operator-examples
```

**步骤2: 安装MogDB Operator**

```shell
kustomize build ./kustomize/install/default | kubectl apply -f -
```

期望输出:

```shell
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
secret/mogdb-operator-huawei-registry created
deployment.apps/mogdb-operator-controller-manager created
```

<br/>

## 部署MogDB集群

```shell
kubectl apply -f https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-cluster.yaml
```

期望输出:

```text
mogdbcluster.mogdb.enmotech.io/cluster1 created
```

### 查看pod状态

```shell
kubectl get pods -n mogdb-operator-system
```

期望输出:

```shell
NAME                          READY   STATUS            RESTARTS   AGE
cluster1-jl2kn                3/3     Running           0          4m18s
cluster1-zuveg                0/3     PodInitializing   0          108s
```

等待所有pod都处于running状态，然后进行下一步，连接到MogDB集群。

<br/>

## 连接MogDB集群

### 安装gsql命令行工具

要连接到MogDB集群，您需要在使用kubectl的主机上安装MogDB客户端工具[gsql](../../docs-mogdb/v3.0/quick-start/mogdb-access/use-cli-to-access-mogdb/gsql.md#安装gsql客户端)安装与物理机匹配的命令行工具。

<br/>

### 查看MogDB服务端口

首先，将端口从本地主机转发到 Kubernetes 中的 MogDB Service。 我们先获取 mogdb-operator-system 命名空间中的服务列表：

```shell
kubectl get svc -n mogdb-operator-system
```

期望输出：

```text
NAME                       TYPE       CLUSTER-IP     EXTERNAL-IP        PORT(S)          AGE
cluster1-svc-master        NodePort   10.1.115.245   <none>       5432:30013/TCP   38m
cluster1-svc-replicas      NodePort   10.1.41.187    <none>       5432:30012/TCP   38m
```

输出结果显示了，k8s集群内部通过5432端口，集群外部通过30013端口访问实际的MogDB集群。

<br/>

### 连接集群

在连接之前，需要修改默认用户的密码。可以通过kubectl命令连接到容器，修改密码。

```shell
kubectl exec -it cluster1-xxx -c ha-sidecar -- /bin/bash
gsql -dpostgres
```

```sql
ALTER USER mogdb WITH PASSWORD 'new_password';
```

```shell
gsql -h 10.1.115.245 -p 5432 -U mogdb -W
```

期望输出：

```shell
gsql ((MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

以下是一些可以用来验证集群功能的命令。

- 查看MogDB版本

```sql
MogDB=#select version();
                                                                      version
---------------------------------------------------------------------------------------------------------------------------------------------------
 (MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr   on aarch64-unknown-linux-gnu, compiled by g++ (GCC) 7.3.0, 64-bit
(1 row)
```

- 创建t1表

```sql
MogDB=#create table t1(id int);
CREATE TABLE
```

- 插入数据

```sql
MogDB=#insert into t1(id) values(1),(2),(3);
INSERT 0 3
```

- 查看数据

```sql
MogDB=#select * from t1;
 id
----
  1
  2
  3
(3 rows)
```

<br/>

## 销毁MogDB集群

完成测试后，您可能希望销毁MogDB集群。

### 删除MogDB集群

```shell
kubectl delete mogdbcluster cluster1
```

期望输出:

```text
mogdbcluster.mogdb.enmotech.io "cluster1" deleted
```

<br/>

## 卸载MogDB Operator

```shell
kustomize build ./kustomize/install/default | kubectl delete -f -
```

期望输出:

```shell
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-apiserver created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
configmap/mogdb-operator-mgo-config created
secret/mgorole-admin created
secret/mgouser-admin created
secret/mogdb-operator-huawei-registry created
service/mogdb-apiserver created
deployment.apps/mogdb-apiserver created
deployment.apps/mogdb-operator-controller-manager created
```
