---
title: kustomize方式安装
summary: kustomize方式安装
author: Ji Yabin
date: 2022-06-16
---

## 前置条件

- 安装Kustomize v3+

  ```shell
  go get sigs.k8s.io/kustomize/kustomize/v3@v3.8.7
  ~/go/bin/kustomize version
  ```

- 下载MogDB Operator样例

  包含了MogDB Operator Kustomize安装器。

  <https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar>

  ```shell
  wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar
  tar xf mogdb-operator-examples.tar
  cd mogdb-operator-examples
  ```

  MogDB Operator安装项目在kustomize/install目录下。

- 安装的组件
  - mogdb-operator-controller-manager
  
  - mogdb-apiserver
  
  - mgo-client
  
    其中，mgo-client运行在物理机下，其余组件都运行在k8s下
  
- 生成的k8s对象
  - mogdb-operator-manager-config
  - mogdb-operator-mgo-config
  - mogdb-operator-huawei-registry

## 配置

默认的Kustomize可以工作在大部分Kubernetes环境中，也可以根据您的特殊需求定制。

例如，为MogDB Operator自定义镜像路径，该镜像在kustomize/install/default/kustomization.yaml文件中，可以修改为:

```yaml
images:
- name: controller
  newName: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-operator
  newTag: v1.0.0
```

如果需要更改namespace名称，需要在kustomize/install/default/kustomization.yaml文件中修改如下配置。

```yaml
namespace: custom-namespace
```

## 安装

### 安装 MogDB Operator

```shell
kustomize build ./kustomize/install/default | kubectl apply -f -
```

期望输出:

```text
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-apiserver created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mgo-cluster-role created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
configmap/mogdb-operator-mgo-config created
secret/mgorole-admin created
secret/mgouser-admin created
secret/mogdb-operator-huawei-registry created
service/mogdb-apiserver created
deployment.apps/mogdb-apiserver created
deployment.apps/mogdb-operator-controller-manager created
```

### 安装 mgo 客户端

```shell
wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/client-setup.sh
chmod +x client-setup.sh
./client-setup.sh
```

这将下载mgo客户端并且提示您添加一些环境变量以供您在会话中设置，您可以使用以下命令执行此操作：

```shell
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
```

如果您希望将这些变量永久添加到您的环境中，您可以运行以下命令：

```shell
cat <<EOF >> ~/.bashrc
export PATH="${HOME?}/.mgo:$PATH"
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
EOF

source ~/.bashrc
```

> **注意：** 对于 macOS 用户，您使用文件是\~/.bash_profile而不是\~/.bashrc。

## 卸载

```shell
kustomize build config/default | kubectl delete -f -
```

期望输出:

```shell
namespace "mogdb-operator-system" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbbackups.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbclusters.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbrecoveries.mogdb.enmotech.io" deleted
serviceaccount "mogdb-operator-controller-manager" deleted
role.rbac.authorization.k8s.io "mogdb-operator-leader-election-role" deleted
clusterrole.rbac.authorization.k8s.io "mogdb-operator-manager-role" deleted
rolebinding.rbac.authorization.k8s.io "mogdb-operator-leader-election-rolebinding" deleted
clusterrolebinding.rbac.authorization.k8s.io "mogdb-operator-manager-rolebinding" deleted
configmap "mogdb-operator-manager-config" deleted
secret "mogdb-operator-huawei-registry" deleted
deployment.apps "mogdb-operator-controller-manager" deleted
```

> **注意**:
>
> 卸载之前，请确保系统所有的集群已经删除完整，否则无法卸载。
