---
title: kubectl方式安装
summary: kubectl方式安装
author: Ji Yabin
date: 2022-06-16
---

## 前置条件

- 下载MogDB Operator样例

  包含了MogDB Operator Kustomize安装器。

  ```shell
  wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/mogdb-operator-examples.tar
  tar xf mogdb-operator-examples.tar
  cd mogdb-operator-examples
  ```

  MogDB Operator安装项目在kustomize/install目录下。

## 配置

默认的Kustomize可以工作在大部分Kubernetes环境中，也可以根据您的特殊需求定制。

## 安装

### 安装 MogDB Operator

```shell
kubectl apply -f ./kustomize/install/kubectl/mogdb-operator.yml
```

期望输出:

```shell
namespace/mogdb-operator-system created
customresourcedefinition.apiextensions.k8s.io/mogdbbackups.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbclusters.mogdb.enmotech.io created
customresourcedefinition.apiextensions.k8s.io/mogdbrecoveries.mogdb.enmotech.io created
serviceaccount/mogdb-operator-controller-manager created
role.rbac.authorization.k8s.io/mogdb-operator-leader-election-role created
clusterrole.rbac.authorization.k8s.io/mogdb-operator-manager-role created
rolebinding.rbac.authorization.k8s.io/mogdb-operator-leader-election-rolebinding created
clusterrolebinding.rbac.authorization.k8s.io/mogdb-operator-manager-rolebinding created
configmap/mogdb-operator-manager-config created
secret/mogdb-operator-huawei-registry created
deployment.apps/mogdb-operator-controller-manager created
```

### 安装 mgo 客户端

```shell
wget https://cdn-mogdb.enmotech.com/mogdb-stack/v1.0.0/client-setup.sh
chmod +x client-setup.sh
./client-setup.sh
```

这将下载mgo客户端并且提示您添加一些环境变量以供您在会话中设置，您可以使用以下命令执行此操作：

```shell
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
```

如果您希望将这些变量永久添加到您的环境中，您可以运行以下命令：

```shell
cat <<EOF >> ~/.bashrc
export PATH="${HOME?}/.mgo:$PATH"
export MGOUSER="${HOME?}/.mgo/mgouser"
export MGO_CA_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_CERT="${HOME?}/.mgo/client.crt"
export MGO_CLIENT_KEY="${HOME?}/.mgo/client.key"
export MGO_APISERVER_URL='https://127.0.0.1:32444'
export MGO_NAMESPACE=mogdb-operator-system
EOF

source ~/.bashrc
```

> **注意：** 对于 macOS 用户，您使用文件是\~/.bash_profile而不是\~/.bashrc。

## 卸载

```shell
kubectl delete -f ./kustomize/install/kubectl/mogdb-operator.yml
```

```shell
namespace "mogdb-operator-system" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbbackups.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbclusters.mogdb.enmotech.io" deleted
customresourcedefinition.apiextensions.k8s.io "mogdbrecoveries.mogdb.enmotech.io" deleted
serviceaccount "mogdb-operator-controller-manager" deleted
role.rbac.authorization.k8s.io "mogdb-operator-leader-election-role" deleted
clusterrole.rbac.authorization.k8s.io "mogdb-operator-manager-role" deleted
rolebinding.rbac.authorization.k8s.io "mogdb-operator-leader-election-rolebinding" deleted
clusterrolebinding.rbac.authorization.k8s.io "mogdb-operator-manager-rolebinding" deleted
configmap "mogdb-operator-manager-config" deleted
secret "mogdb-operator-huawei-registry" deleted
deployment.apps "mogdb-operator-controller-manager" deleted
```

> **注意:**
>
> 卸载之前，请确保系统所有的集群已经删除完整，否则无法卸载。
