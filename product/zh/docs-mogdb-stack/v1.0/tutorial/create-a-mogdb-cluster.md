---
title: 创建MogDB集群
summary: 创建MogDB集群
author: Wang Dong
date: 2022-06-30
---

# 创建 MogDB 集群

## 使用 mgo 命令创建集群

参考[mgo create cluster](../references/client/mgo-create-cluster.md)命令创建集群，例如：

```shell
mgo create cluster cluster1
```

期望输出：

```text
created cluster: cluster1
```

这将创建一个名为 cluster1 的新 MogDB 集群，此操作可能需要一些时间才能完成。

您可以使用 [mgo show cluster](../references/client/mgo-show-cluster.md) 命令检查集群创建的状态。该命令查看 MogDB 集群的详细信息。

例如，当 **cluster1** 集群准备好时，运行以下命令：

```shell
mgo show cluster cluster1
```

期望输出：

```text
cluster : cluster1
 pod : cluster1-ib7zq (Running) on mogdb-k8s-001 (3/3) (primary)
 pod : cluster1-rtwdz (Running) on mogdb-k8s-002 (3/3) (standby)
 service : cluster1-svc-master - ClusterIP (10.1.149.4) - Ports (5432:30013/TCP)
 service : cluster1-svc-replicas - ClusterIP (10.1.175.46) - Ports (5432:30012/TCP)
```

<br/>

## 创建集群过程

首先，mgo客户端在自定义资源mogdbcluster中创建一个条目，其中包含创建集群所需的属性。
在上面的例子中，cluster1 集群利用了 MogDB Operator 配置中的许多默认值。例如，默认两个从节点(replicas:2)。

我们将在本教程后面讨论有关 MogDB Operator 配置的更多信息。