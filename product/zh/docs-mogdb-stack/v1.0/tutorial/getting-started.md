---
title: 开始
summary: 开始
author: Wang Dong
date: 2022-06-30
---

# 快速开始

## 安装

如果您尚未安装 MogDB operator 和 mgo, 请参考 [快速上手](../quick-start.md) 安装 MogDB operator 和 [安装 mgo 客户端](../installation/kustomize.md#安装-mgo-客户端)。

<br/>

## 设置mgo客户端

MogDB Operator 和mgo客户端设计为在多命名空间部署环境中工作，许多mgo命令要求将命名空间标志 ( -n) 传递给其中。您可以使用MGO_NAMESPACE环境变量来设置pgo命令可以使用的命名空间。例如：

```shell
export MGO_NAMESPACE=mogdb-operator-system
mgo show cluster --all
```

这将显示部署到mogdb-operator-system命名空间的所有 MogDB 集群。这相当于：

```shell
mgo show cluster -n mogdb-operator-system --all
```

（注：**-n**优先于**MGO_NAMESPACE**。）

<br/>

## 下一步

[mgo version](../references/client/mgo-version.md) 命令是检查 MogDB Operator 连接性的好方法，因为它是一个非常简单、安全的操作。试试看：

```shell
mgo version
```

如果它正常工作，您应该会看到类似于以下内容的结果：

```shell
mgo client version v1.0.0
mgo-apiserver version v1.0.0
```

请注意，mgo客户端的版本**必须**与 MogDB Operator 的版本匹配。
