---
title: 扩缩容
summary: 扩缩容
author: Wang Dong
date: 2022-06-30
---

# 扩缩容 MogDB 集群

## 创建高可用 MogDB 集群

只要您拥有多个副本，MogDB Operator 中就会默认启用高可用性。要创建高可用性 MogDB 集群，可以执行以下命令：

```shell
mgo create cluster cluster1 --replica=1
```

<br/>

## 扩容 MogDB 集群

您可以使用以下命令[mgo scale](../references/client/mgo-scale.md)扩展现有 MogDB 集群以向其添加副节点：

```shell
mgo scale cluster1
```

<br/>

## 缩容 MogDB 集群

要缩容 MogDB 集群，您必须提供要缩减的实例的目标。您可以使用以下[mgo scaledown](../references/client/mgo-scaledown.md)命令执行此操作：

```shell
mgo scaledown cluster1 --query
```

期望输出：

```text
PODNAME              STATUS     NODE
cluster1-rtwdz       Running    mogdb-k8s-002
```

确定要缩容的实例后，可以运行以下命令：

```shell
mgo scaledown cluster1 --target=cluster1-rtwdz
```
