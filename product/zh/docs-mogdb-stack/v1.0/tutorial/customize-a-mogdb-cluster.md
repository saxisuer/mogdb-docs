---
title: 定制MogDB集群
summary: 定制MogDB集群
author: Wang Dong
date: 2022-06-30
---

# 定制 MogDB 集群

## 自定义 MogDB 集群

+ 资源分配（例如内存、CPU、PVC 大小）
+ 高可用性（例如添加副本）
+ 指定特定的 MogDB 镜像
+ 自定义 MogDB 配置

自定义 MogDB 集群的配置有很多种。 您可以在[mgo create cluster](../references/client/mgo-create-cluster.md)中了解所有这些选项。

<br/>

### 自定义 PVC 尺寸

数据库有各种不同的大小，而且这些大小肯定会随着时间而改变。因此，能够指定要存储 MogDB 数据的 PVC 大小是很有帮助的。

<br/>

#### 为 MogDB 的数据库自定义 PVC 大小

MogDB Operator允许您使用--data-storage-size标志选择“MogDB 数据目录”的大小。PVC 大小应该使用标准的 Kubernetes 资源单元来选择，例如128Mi。

例如，要创建一个具有128Mi大小数据目录的 MogDB 集群：

```shell
mgo create cluster cluster1 --data-storage-size=20Gi
```

<br/>

#### 自定义 backup 的 PVC 大小

您还可以使用--backup-storage-size. backup用于存储您的所有备份，因此您希望调整它的大小以满足您的备份保留策略。

例如，要创建一个 backup 存储库，它的 PVC 大小为1Gi：

```shell
mgo create cluster cluster1 --backup-storage-size=1Gi
```

<br/>

#### 自定义数据库日志的 PVC 大小

您还可以使用--log-storage-size设置日志数据卷大小，因此您希望调整它的大小以满足您的日志保留策略。

例如，对于存储日志数据卷，它的 PVC 大小为128Mi：

```shell
mgo create cluster cluster1 --log-storage-size=1Gi
```

<br/>

### 自定义 CPU / 内存

数据库具有不同的 CPU 和内存要求，这通常取决于工作集中的数据量（即主动访问的数据）。Kubernetes 为 Pod 提供了几种管理 CPU 和内存资源的方法：

+ CPU Requests 和 Memory Requests
+ CPU Limits 和 Memory Limits

CPU Requests 和 Memory Requests告诉 Kubernetes 确保节点上至少有足够的资源可用于调度 Pod。

CPU Limits 告诉 Kubernetes 不要让 Pod 使用超过该数量的 CPU。仅允许 Pod 使用最大数量的 CPU。类似地，Memory Limits告诉 Kubernetes 不要让 Pod 超过一定的内存量。在这种情况下，如果 Kubernetes 检测到 Pod 超出了内存限制，它将尝试终止任何导致超出限制的进程。内存限制可能会影响 MogDB 的可用性，我们建议谨慎使用它们。

下面介绍如何自定义可用于 MogDB 集群的核心部署 Pod 的 CPU 和内存资源。自定义 CPU 和内存确实会为您的 MogDB 集群添加更多资源，但要充分利用额外资源，您将需要自定义 MogDB 配置并调整参数shared_buffers等。

#### 为 MogDB 定制 CPU / 内存

MogDB Operator 提供了几个标志[mgo create cluster](../references/client/mgo-create-cluster.md)来帮助管理 MogDB 实例的资源：

+ --cpu：指定 MogDB 实例的 CPU 请求
+ --cpu-limit：指定 MogDB 实例的 CPU 限制
+ --memory：指定 MogDB 实例的内存请求
+ --memory-limit: 指定 MogDB 实例的内存限制

例如，要创建一个 MogDB 集群，其 CPU 请求为 1.0，CPU 限制为 2.0，内存请求为 1Gi，内存限制为 2Gi：

```shell
mgo create cluster cluster1 \
--cpu=1.0 --cpu-limit=2.0 \
--memory=1Gi --memory-limit=2Gi
```

<br/>

## 创建高可用性 MogDB 集群

高可用性允许您部署具有冗余的 MogDB 集群，即使您的主实例发生停机事件，您的应用程序也可以访问它们。MogDB 集群使用 Kubernetes 附带的分布式共识存储系统，因此可用性与 Kubernetes 集群的可用性相关联。

要创建具有一个副节点的高可用性 MogDB 集群，您可以运行以下命令：

```shell
mgo create cluster cluster1 --replicas=1
```

您可以使用[mgo scale](../references/client/mgo-scale.md)和[mgo scaledown](../references/client/mgo-scaledown.md)命令扩展和缩容 MogDB 集群。