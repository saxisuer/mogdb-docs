---
title: 连接MogDB集群
summary: 连接MogDB集群
author: Wang Dong
date: 2022-06-30
---

# 连接到 MogDB 集群

在连接之前，需要修改默认用户的密码。可以通过kubectl命令连接到容器，修改密码。

```shell
kubectl exec -it cluster1-xxx -c ha-sidecar -- /bin/bash
gsql -dpostgres
```

```sql
ALTER USER mogdb WITH PASSWORD 'new_password';
```

## 连接方式gsql

让我们看看如何连接到 cluster1 使用 gsql 的命令行工具。确保您已安装 gsql 客户端。

MogDB Operator 创建两个有集群名称前缀的服务, 一个是主节点的service，一个是从节点的service。获取 mogdb-operator-system 命名空间中所有可用服务的列表：

执行命令：

```shell
kubectl get svc -n mogdb-operator-system
```

期望输出：

```text
NAME                    TYPE       CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
cluster1-svc-master     NodePort   10.1.149.4     <none>         5432:30013/TCP   57m
cluster1-svc-replicas   NodePort   10.1.175.46    <none>         5432:30012/TCP   57m
mogdb-apiserver         NodePort   10.1.254.248   <none>         8444:32444/TCP   25h
```

您可以使用以下命令连接到数据库：

```shell
gsql -h 127.0.0.1 -p 30013 -U mogdb -W
```

然后应该会看到 mogdb 提示符：

```text
gsql ((MogDB 3.0.0 build b5f25b20) compiled at 2022-06-30 14:41:25 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

<br/>

## 在 Kubernetes 集群之外

由 MogDB Operator 创建两个带有集群名称前缀的service，service的类型type设置为NodePort，可将service端口和主机端口映射起来。

只需要连接“Host:Port”即可。