---
title: mgo create mgorole
summary: mgo创建角色
author: Ji Yabin
date: 2021-07-09
---

# 创建 mgo 权限角色

## 概览

创建 mgo 权限角色。例如：

```shell
mgo create mgorole somerole --permissions="Cat,Ls"
```

```shell
mgo create mgorole [flags]
```

<br/>

## 选项

```text
  -h, --help                 help for mgorole
      --permissions string   指定mgo 权限角色的权限列表，用逗号隔开
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo create](mgo-create.md)  - 创建 MogDB Operator 资源