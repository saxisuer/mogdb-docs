---
title: mgo-delete
desription: mgo 删除命令
author: Wang Dong
date: 2022-06-30
---

# 删除 MogDB Operator 资源

## 概览

delete 命令允许你删除一个 MogDB Operator 资源。例如：

```shell
mgo delete cluster mycluster
mgo delete mgouser someuser
mgo delete mgorole somerole
mgo delete user --username=testuser --selector=name=mycluster
```

```shell
mgo delete [flags]
```

<br/>

## 选项

```text
  -h, --help   delete 的帮助信息
```

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo](mgo.md)  - mgo 命令行工具
* [mgo delete cluster](mgo-delete-cluster.md)  - 删除 MogDB 集群
* [mgo delete mgorole](mgo-delete-mgorole.md)  - 删除 mgo 权限角色
* [mgo delete mgouser](mgo-delete-mgouser.md)  - 删除 mgo 用户