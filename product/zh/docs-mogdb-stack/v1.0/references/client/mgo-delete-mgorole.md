---
title: mgo delete mgorole
desription: mgo 删除角色
author: Ji Yabin
date: 2022-06-30
---

# 删除 mgo 权限角色

## 概览

删除指定的角色或全部角色，例如：

```shell
mgo delete mgorole somerole
```

```shell
mgo delete mgorole [flags]
```

<br/>

## 选项

```text
      --all         删除所有的 mgo 权限角色
  -h, --help        delete mgorole 的帮助信息
      --no-prompt   删除前没有命令行确认
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo delete](mgo-delete.md)  - 删除 MogDB Operator 资源