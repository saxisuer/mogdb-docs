---
title: mgo-scaledown
desription: mgo 缩容集群命令
author: Wang Dong
date: 2022-06-30
---

# 缩容 MogDB 集群

## 概览

scaledown 命令允许您缩小集群的副本配置。例如：

 列出目标副节点：

 ```shell
 mgo scaledown mycluster --query
 ```

 缩容特定副节点：

 ```shell
 mgo scaledown mycluster --target=mycluster-replica-xxxx
 ```

```shell
mgo scaledown [flags]
```

<br/>

## 选项

```text
  -h, --help            scaledown 的帮助信息
      --no-prompt       无命令行确认
      --query           打印目标副本候选列表
      --target string   缩容指定以逗号分隔的副节点列表
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```