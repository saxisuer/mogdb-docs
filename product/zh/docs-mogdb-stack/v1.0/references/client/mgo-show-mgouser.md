---
title: mgo show mgouser
summary: mgo查看用户信息
author: Ji Yabin
date: 2021-07-09
---

# 显示 mgo 用户信息

## 概览

显示指定的 mgo 用户信息或全部用户信息。例如：

```shell
mgo show mgouser someuser
```

```shell
mgo show mgouser [flags]
```

<br/>

## 选项

```text
      --all    查看所有的 mgo 用户信息
  -h, --help   help for mgouser
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo show](mgo-show.md) - 显示 MogDB Operator 资源