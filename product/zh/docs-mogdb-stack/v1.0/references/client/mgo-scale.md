---
title: mgo-scale
desription: mgo 扩容集群命令
author: Wang Dong
date: 2022-06-30
---

# 扩容 MogDB 集群

## 概览

scale命令允许您添加集群的副本配置。例如：

```shell
mgo scale mycluster --replica-count=1
```

```shell
mgo scale [flags]
```

<br/>

## 选项

```text
  -h, --help                scale 的帮助信息
      --no-prompt           无命令行确认
      --replica-count int   要添加于群集的副本计数。(默认 1)
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```