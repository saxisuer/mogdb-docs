---
title: mgo-switch
desription: mgo 切换用户命令
author: Wang Dong
date: 2022-06-30
---

# 切换 mgo 用户

## 概览

switch 允许你切换 mgo 用户。例如：

```shell
mgo switch mgouser someuser
```

```shell
mgo switch mgouser [flags]
```

<br/>

## 选项

```text
  -h, --help   switch 的帮助信息
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```