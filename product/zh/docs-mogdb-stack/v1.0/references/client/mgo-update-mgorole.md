---
title: mgo update mgorole
desription: mgo 修改角色
author: Ji Yabin
date: 2022-06-30
---

# 修改 mgo 权限角色

## 概览

update mgorole 命令允许修改 mgo 权限角色。例如：

```shell
mgo update mgorole somerole  --permissions="version,show,create"
```

```shell
mgo update mgorole [flags]
```

<br/>

## 选项

```text
  -h, --help                 update mgorole 的帮助信息
      --no-prompt            没有命令行提示
      --permissions string   为mgorole指定以逗号分隔的权限列表
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo update](mgo-update.md) - 修改 MogDB Operator 资源