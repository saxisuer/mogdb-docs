---
title: mgo-update
desription: mgo 修改命令
author: Wang Dong
date: 2022-06-30
---

# 修改 MogDB Operator 资源

## 概览

update 命令允许您修改 mgouser、mgorole 或者 cluster。例如：

```shell
mgo update cluster --selector=name=mycluster --disable-autofail
mgo update cluster --all --enable-autofail
mgo update mgorole somerole --mgorole-permission="version"
mgo update mgouser someuser --mgouser-password=somenewpassword
mgo update mgouser someuser --mgouser-roles="role1,role2"
mgo update mgouser someuser --mgouser-namespaces="mgouser2"
```

```shell
mgo update [flags]
```

<br/>

## 选项

```text
  -h, --help   update 的帮助信息
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo](mgo.md) - mgo 命令行工具
* [mgo update cluster](mgo-update-cluster.md) - 修改 MogDB 集群
* [mgo update mgorole](mgo-update-mgorole.md) - 修改 mgo 权限角色
* [mgo update mgouser](mgo-update-mgouser.md) - 修改 mgo 用户