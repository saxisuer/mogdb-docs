---
title: mgo-create
desription: mgo 创建命令
author: Wang Dong
date: 2022-06-30
---

## 创建 MogDB Operator resource

### 概览

CREATE 命令允许您创建一个新的 Operator 资源。例如：

```shell
mgo create cluster
mgo create mgouser
mgo create mgorole
```

```shell
mgo create [flags]
```

<br/>

### 选项

```text
  -h, --help   create 的帮助信息
```

<br/>

### 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

### 另请参阅

* [mgo](mgo.md)  - mgo 命令行工具
* [mgo create cluster](mgo-create-cluster.md)  - 创建 MogDB 集群
* [mgo create mgorole](mgo-create-mgorole.md)  - 创建 mgo 权限角色
* [mgo create mgouser](mgo-create-mgouser.md)  - 创建 mgo 用户