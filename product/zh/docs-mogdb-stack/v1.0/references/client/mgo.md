---
title: mgo 
desription: mgo 命令行工具参考
author: Wang Dong
date: 2022-06-30
---

# mgo命令

## 概览

mgo 命令行工具帮助您创建和管理MogDB集群。

<br/>

## 选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
  -h, --help                      mgo 的帮助信息
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo version](mgo-version.md) - 打印 MogDB Operator 版本信息
* [mgo show](mgo-show.md) - 显示 MogDB Operator 资源
* [mgo show cluster](mgo-show-cluster.md) - 显示 MogDB Operator 集群
* [mgo show mgorole](mgo-show-mgorole.md) - 显示 MogDB Operator 角色
* [mgo show mgouser](mgo-show-mgouser.md) - 显示 MogDB Operator 用户
* [mgo create](mgo-create.md) - 创建 MogDB Operator 资源
* [mgo create cluster](mgo-create-cluster.md) - 创建 MogDB Operator 集群
* [mgo create mgorole](mgo-create-mgorole.md) - 创建 MogDB Operator 角色
* [mgo create mgouser](mgo-create-mgouser.md) - 创建 MogDB Operator 用户
* [mgo update](mgo-update.md) - 修改 MogDB Operator 资源
* [mgo update cluster](mgo-update-cluster.md) - 更新 MogDB Operator 集群
* [mgo update mgorole](mgo-update-mgorole.md) - 更新 MogDB Operator 角色
* [mgo update mgouser](mgo-update-mgouser.md) - 更新 MogDB Operator 用户
* [mgo delete](mgo-delete.md) - 删除 MogDB Operator 资源
* [mgo delete cluster](mgo-delete-cluster.md) - 删除 MogDB Operator 集群
* [mgo delete mgorole](mgo-delete-mgorole.md) - 删除 MogDB Operator 角色
* [mgo delete mgouser](mgo-create-mgouser.md) - 删除 MogDB Operator 用户
* [mgo scale](mgo-scale.md) - 扩容 MogDB 集群
* [mgo scaledown](mgo-scaledown.md) - 缩容 MogDB 集群
* [mgo switch](mgo-switch.md) - 切换 MogDB operator 用户