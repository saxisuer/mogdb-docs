---
title: mgo create mgouser
summary: mgo创建用户
author: Ji Yabin
date: 2021-07-09
---

# 创建 mgo 用户

## 概览

创建一个 mgo 用户。例如：

```shell
mgo create mgouser someuser
```

```shell
mgo create mgouser [flags]
```

<br/>

## 选项

```text
      --all-namespaces               指定此用户将有权访问所有命名空间
  -h, --help                         help for mgouser
      --mgouser-namespaces string   为mgouser指定以逗号分隔的名称空间列表
      --mgouser-password string     指定mgouser的密码
      --mgouser-roles string        为mgouser指定以逗号分隔的角色列表
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo create](mgo-create.md)  - 创建 MogDB Operator 资源