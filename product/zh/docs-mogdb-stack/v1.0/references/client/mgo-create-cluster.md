---
title: mgo create cluster
summary: mgo创建集群
author: Ji Yabin
date: 2021-07-09
---

# 创建 MogDB 集群

## 概览

后台创建由一个主节点和几个从节点组成的 MogDB 集群, 例：

```shell
mgo create cluster mycluster
```

```shell
mgo create cluster [flags]
```

<br/>

## 选项

```text
      --access-modes int             设置数据卷的访问方式。1: ReadWriteOnce, 2: ReadOnlyMany, 3: ReadWriteMany. 默认值ReadWriteOnce
      --backup-storage-size string   设置备份数据卷大小。默认值128Mi
      --cluster-role string          The role in clusters, There are only three legal roles: primary, standby, cascade
      --cpu string                   设置要请求CPU的核数，e.g. "100m" or "0.1"
      --cpu-limit string             为CPU设置要限制的核数，e.g. "100m" or "0.1"
      --data-storage-size string     设置数据库数据卷大小。默认值128Mi
      --exporter-image string        将用于MogDB exporter sidecar 容器的镜像。如果指定，将替代默认值
      --ha-image string              将用于MogDB ha sidecar 容器的镜像。如果指定，将替代默认值
  -h, --help                         help for cluster
      --image string                 将用于MogDB server 容器的镜像。如果指定，将替代默认值
      --init-image string            将用于MogDB 初始化容器的镜像。如果指定，将替代默认值
      --log-storage-size string      设置日志数据卷大小。默认值128Mi
      --memory string                设置要请求的内存数量，e.g. 1GiB
      --memory-limit string          设置限制的内存数量，e.g. 1GiB
      --read-port int                设置 service 的读取端口
      --replicas int                 设置创建集群的从节点数量，默认值为2
      --sidecar-image string         将用于MogDB sidecar 容器的镜像。如果指定，将替代默认值
      --storage-class string         设置声明所需的StorageClass的名称。默认值是 local-path
      --write-port int               设置 service 的写端口
```

<br/>

## 从父命令继承的选项

```text
      --apiserver-url string      将处理来自mgo客户端的请求的MogDB Operator apiserver的URL。请注意，URL不应以“/”结尾
      --debug                     启用debug输出以进行调试
      --disable-tls               关闭 MogDB Operator 的 TLS 认证
      --exclude-os-trust          从操作系统默认信任存储中排除CA证书
      --mgo-ca-cert string       连接 MogDB Operator apiserver CA 证书文件路径.
      --mgo-client-cert string   用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径
      --mgo-client-key string    用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径
  -n, --namespace string          用于mgo请求的命名空间
```

<br/>

## 另请参阅

* [mgo create](mgo-create.md)  - 创建 MogDB Operator 资源