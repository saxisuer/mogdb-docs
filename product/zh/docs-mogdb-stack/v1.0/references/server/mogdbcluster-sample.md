---
title: MogDB Cluster CRD 样例
summary: MogDB Cluster CRD 样例
author: Ji Yabin
date: 2022-06-16
---

# MogDB Cluster CR 样例

如果您希望通过原生的kubectl命令创建一个MogDB Cluster CR，可以通过编写简单的yaml文件，提交到k8s系统，如:

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbCluster
metadata:
  name: cluster1
  namespace: mogdb-operator-system
spec:
  # Add fields here
  podSpec:
    volumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    logVolumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    backupVolumeSpec:
      persistentVolumeClaim:
        accessModes:
        - ReadWriteOnce
        storageClassName: local-path
        resources:
          requests:
            storage: 128Mi
    initImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-init:v3.0.0
    sidecarImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-sidecar:v3.0.0
    exporterImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-exporter:v3.0.0
    sidecarHaImage: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-ha:v3.0.0
  readPort: 30012
  writePort: 30013
  replicas: 2
  backupSpec:
    backupSchedule: "0 */1 * * * ?"
    jobSpec:
      image: swr.cn-north-4.myhuaweicloud.com/mogdb-cloud/mogdb-sidecar:v3.0.0
```

上述配置，分别制定了数据卷、日志卷、备份卷以及基本的镜像配置，还有副本数等，提交到系统将会创建一个MogDB 集群。