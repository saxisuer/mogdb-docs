---
title: CRD 参考
summary: CRD 参考
author: Ji Yabin
date: 2022-06-16
---

# CRD 参考

## 资源类型

+ MogdbCluster
+ MogdbBackup
+ MogdbRecovery

<br/>

## MogdbCluster

MogdbCluster是MogdbCluster API的简单概要

| 名称         | 类型    | 描述                           | 必选项    |
|------------|--------|------------------------------|----------|
| apiVersion | string | mogdb.enmotech.io/v1         |  true    |
| kind       | string | MogdbCluster                 |  true    |
| metadata   | object | 相关字段参考 Kubernetes API文档      |  true |
| spec       | object | 定义MogdbCluster期望的状态      |  false   |
| status     | object | 定义MogdbCluster观测到的状态     |  false   |

<br/>

### MogdbCluster.spec

MogdbClusterSpec 定义MogdbCluster期望的状态

| 名称             | 类型      | 描述                           | 必选项    |
|----------------|---------|------------------------------|----------|
| replicas       | integer | MogdbCluster期望的副本数  |  true  |
| readPort       | integer | 只读service端口          |  true  |
| writePort      | integer | 读写service端口          |  true  |
| postgresConf   | object  | MogDB配置文件配置项       |  false |
| podSpec        | object  | 集群pod期望的状态         |  false |
| backupSpec     | object  | 备份期望的状态            | false  |

<br/>

### MogdbCluster.spec.podSpec

| 名称                 | 类型                | 描述             | 必选项    |
|--------------------|-------------------|----------------|--------|
| mogdbVersion       | string            | MogDB数据库的版本    | false  |
| image              | string            | MogDB镜像        | false  |
| sidecarImage       | string            | sidecar镜像      | false  |
| initImage          | string            | init镜像         | false  |
| volumeSpec         | object            | volume类型配置     | false  |
| logVolumeSpec      | object            | 日志volume类型配置   | false  |
| backupVolumeSpec   | object            | 备份volume类型配置   | false  |
| imagePullPolicy    | string            | 镜像拉取策略         | false  |
| imagePullSecrets   | []string          | 镜像拉取秘钥         | false  |
| labels             | map[string]string | 自定义label       | false  |
| annotations        | map[string]string | 自定义注释          | false  |
| resources          | object            | 资源限额           | false  |
| affinity           | object            | 亲和性设置          | false  |
| nodeSelector       | map[string]string | 节点选择器          | false  |
| priorityClassName  | string            | 优先级类名设置        | false  |
| tolerations        | []object          | 容忍性设置          | false  |
| serviceAccountName | string            | 服务账户名称         | false  |
| volumes            | []object          | 额外的volume设置    | false  |
| volumeMounts       | []object          | volume挂载设置     | false  |
| initContainers     | []object          | 额外的初始化容器设置     | false  |
| containers         | []object          | 额外的容器设置        | false  |
| sidecarResources   | object            | sidecar容器资源限额  | false  |

<br/>

### MogdbCluster.spec.podSpec.volumeSpec

| 名称                     | 类型      | 描述           | 必选项    |
|------------------------|---------|--------------|--------|
| emptyDir               | object  | emptyDir类型的卷 | false  |
| hostPath               | object  | hostPath类型的卷 | false  |
| persistentVolumeClaim  | object  | pvc卷声明       | false  |

<br/>

### MogdbCluster.spec.backupSpec

| 名称                             | 类型       | 描述               | 必选项    |
|--------------------------------|----------|------------------|--------|
| backupSchedule                 | string   | 备份调度cron设置       | false  |
| backupScheduleJobsHistoryLimit | integer  | 备份历史保留天数         | false  |
| backupURL                      | string   | 备份远程存储路径         | false  |
| backupSecretName               | string   | 远程存储秘钥           | false  |
| backupCompressCommand          | []string | 备份压缩命令           | false  |
| backupDecompressCommand        | []string | 备份解压缩命令          | false  |
| rcloneExtraArgs                | []string | rclone命令额外参数     | false  |
| gsAllDumpExtraArgs             | []string | gs_alldump命令额外参数 | false  |
| gsqlExtraArgs                  | []string | gsql命令额外参数       | false  |
| jobSpec                        | object   | 任务期望状态           | false  |

<br/>

### MogdbCluster.spec.backupSpec.jobSpec

| 名称                 | 类型                 | 描述         | 必选项    |
|--------------------|--------------------|------------|--------|
| image              | string             | 任务镜像       | false  |
| imagePullPolicy    | string             | 镜像拉取策略     | false  |
| imagePullSecrets   | []string           | 镜像拉取秘钥     | false  |
| serviceAccountName | string             | 服务账户名      | false  |
| affinity           | object             | 亲和性        | false  |
| nodeSelector       | map[string]string  | 节点选择器      | false  |
| priorityClassName  | string             | 优先类名称      | false  |
| tolerations        | []object           | 容忍性设置      | false  |
| labels             | map[string]string  | 自定义labels  | false  |

<br/>

### MogdbCluster.status

| 名称         | 类型        | 描述           | 必选项   |
|------------|-----------|--------------|-------|
| readyNodes | integer   | 处于ready节点的个数 | true  |
| conditions | []object  | 集群状况         | true  |
| nodes      | []object  | 节点状况         | true  |

<br/>

### MogdbCluster.status.conditions

| 名称                 | 类型      | 描述      | 必选项   |
|--------------------|---------|---------|-------|
| type               | string  | 状况类型    | true  |
| status             | string  | 状况的状态   | true  |
| lastTransitionTime | time    | 状况发生的时间 | true  |
| reason             | string  | 状况的原因   | true  |
| message            | string  | 状况信息    | true  |

<br/>

### MogdbCluster.status.nodes

| 名称          | 类型        | 描述        | 必选项   |
|-------------|-----------|-----------|-------|
| name        | string    | 集群节点状态的名称 | true  |
| conditions  | []object  | 节点状况集合    | true  |

<br/>

### MogdbCluster.status.nodes.conditions

| 名称                  | 类型     | 描述       | 必选项   |
|---------------------|--------|----------|-------|
| type                | string | 状况类型     | true  |
| status              | string | 状况的状态    | true  |
| lastTransitionTime  | time   | 状况发生的时间  | true  |

<br/>

## MogdbBackup

MogdbBackup是MogdbBackup API的简单概要

| 名称         | 类型      | 描述                      | 必选项    |
|------------|---------|-------------------------|--------|
| apiVersion | string  | mogdb.enmotech.io/v1    | true   |
| kind       | string  | MogdbBackup             | true   |
| metadata   | object  | 相关字段参考 Kubernetes API文档 | true   |
| spec       | object  | 定义MogdbBackup期望的状态      | false  |
| status     | object  | 定义MogdbBackup观测到的状态     | false  |

<br/>

### MogdbBackup.spec

| 名称                             | 类型        | 描述                | 必选项    |
|--------------------------------|-----------|-------------------|--------|
| clusterName                    | string    | MogDB cluster集群名称 | true   |
| backupScheduleJobsHistoryLimit | integer   | 备份历史保留天数          | false  |
| backupURL                      | string    | 备份保存在远程存储的路径      | false  |
| backupSecretName               | string    | 远程存储的秘钥           | false  |
| remoteDeletePolicy             | string    | 备份历史删除策略          | false  |
| dbName                         | string    | 备份库名              | false  |
| tbNames                        | []string  | 备份的表名列表           | false  |

<br/>

### MogdbBackup.status

| 名称                 | 类型        | 描述      | 必选项   |
|--------------------|-----------|---------|-------|
| completed          | bool      | 备份是否完成  | true  |
| completedTimestamp | time      | 备份完成的时间 | true  |
| conditions         | []object  | 备份状况列表  | true  |

<br/>

### MogdbBackup.status.conditions

| 名称                 | 类型      | 描述      | 必选项   |
|--------------------|---------|---------|-------|
| type               | string  | 状况类型    | true  |
| status             | string  | 状况的状态   | true  |
| lastTransitionTime | time    | 状况发生的时间 | true  |
| reason             | string  | 状况发生的原因 | true  |
| message            | string  | 状况详情    | true  |

<br/>

## MogdbRecovery

MogdbRecovery是MogdbRecovery API的简单概要

| 名称         | 类型      | 描述                      | 必选项    |
|------------|---------|-------------------------|--------|
| apiVersion | string  | mogdb.enmotech.io/v1    | true   |
| kind       | string  | MogdbRecovery           | true   |
| metadata   | object  | 相关字段参考 Kubernetes API文档 | true   |
| spec       | object  | 定义MogdbRecovery期望的状态    | false  |
| status     | object  | 定义MogdbRecovery观测到的状态   | false  |

<br/>

### MogdbRecovery.spec

| 名称                | 类型      | 描述                | 必选项   |
|-------------------|---------|-------------------|-------|
| clusterName       | string  | MogDB cluster集群名称 | true  |
| recoveryTimePoint | string  | 备份恢复的时间节点         | true  |
| backupSecretName  | string  | 远程存储秘钥            | true  |

<br/>

### MogdbRecovery.status

| 名称          | 类型        | 描述      | 必选项   |
|-------------|-----------|---------|-------|
| completed   | bool      | 恢复是否完成  | true  |
| conditions  | []object  | 恢复状况列表  | true  |

<br/>

### MogdbRecovery.status.conditions

| 名称               | 类型   | 描述           | 必选项 |
| ------------------ | ------ | -------------- | ------ |
| type               | string | 状况类型       | true   |
| status             | string | 状况状态       | true   |
| lastTransitionTime | time   | 状况发生的时间 | true   |
| reason             | string | 状况发生的原因 | true   |
| message            | string | 状况详情       | true   |