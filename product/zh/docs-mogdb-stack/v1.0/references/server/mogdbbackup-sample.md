---
title: MogDB Backup CRD 样例
summary: MogDB Backup CRD 样例
author: Ji Yabin
date: 2022-06-16
---

# MogDB Backup CR 样例

如果您希望通过原生的kubectl命令创建一个MogDB Backup CR，可以通过编写简单的yaml文件，提交到k8s系统，如:

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbBackup
metadata:
  name: sample1-002
  labels:
    cluster: sample1
spec:
  # Add fields here
  clusterName: cluster1
  dbName: db1
```

上述配置，指定了需要备份的集群，以及特定的库。