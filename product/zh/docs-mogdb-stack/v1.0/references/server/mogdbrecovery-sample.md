---
title: MogDB Recovery CRD 样例
summary: MogDB Recovery CRD 样例
author: Ji Yabin
date: 2022-06-16
---

# MogDB Recovery CR 样例

如果您希望手动通过kubectl命令创建一个MogDB Recovery CR 完成一次备份，可以通过编写简单的yaml文件，提交到k8s系统，如:

```yaml
apiVersion: mogdb.enmotech.io/v1
kind: MogdbRecovery
metadata:
  name: sample1-003
spec:
  # Add fields here
  clusterName: cluster1
  recoveryTimePoint: "2022-07-06 12:47:59"
```

上述配置，指定了需要恢复的集群，以及恢复的point-in-time。