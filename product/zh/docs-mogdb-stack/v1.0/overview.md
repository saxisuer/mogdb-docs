---
title: MogDB Stack 简介
summary: MogDB Stack 简介
author: Ji Yabin
date: 2021-06-16
---

# MogDB Stack 简介

MogDB Stack是Kubernetes上的MogDB集群自动运维系统，提供包括部署、高可用、扩缩容、监控、备份恢复的MogDB全生命周期管理。借助MogDB Stack，MogDB可以无缝运行在公有云或私有部署的Kubernetes集群上。

<br/>

## MogDB Stack特性

- **简单便捷的自动部署/回收**

  以Kubernetes作为原生的容器编排系统，以扩展资源的方式，轻松便捷地创建整套MogDB集群，并且根据用户的期望任意扩缩容。

- **稳定可靠的备份恢复能力**

  支持基于SQL的备份方式，并且存储在远端分布式存储中，多副本保证备份的可靠性，指定point-in-time的恢复方式，恢复到指定的时间点。

- **企业级的高可用性**

  自动故障探测、切换，并自动化的拉取数据快照恢复故障节点或重新调度新节点，实现系统的自愈能力，保证用户期望的副本数。

- **完善可靠的监控能力**

  基于Prometheus实现多维度的统一监控，囊括了系统层、容器层、数据库层的完整的监控指标。

- **精细化的资源管控**

  针对CPU、内存、存储等资源的限额，保证容器层的独立性，不会互相干扰，支持容器的亲和性、反亲和性调度。

<br/>

## 系统架构

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/overview-arch.png)

<br/>

## 操作系统与平台支持

MogDB Stack 目前支持在如下操作系统和平台架构下运行：

1. Linux x86_64
2. Linux arm64