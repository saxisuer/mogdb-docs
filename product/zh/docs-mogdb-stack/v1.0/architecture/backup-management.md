---
title: 备份
summary: 备份
author: Ji Yabin
date: 2022-06-16
---

# 备份管理

MogDB Operator使用MogDB原生的dump命令执行备份，因此，备份内容为可移殖的sql形式，这样的好处是可以跨版本恢复。

<br/>

## backup CR通用字段介绍

为了对Kubernetes上的MogDB集群进行备份，用户可以创建一个自定义的backup Custom Resource(CR)对象来描述一次备份，以下介绍 Backup CR 各个字段的具体含义。

**具体含义请查阅参考[CRD References](../references/server/crd-references.md)章节**

<br/>

## 备份配置

当您使用MogDB Operator时，可通过以下期望设置，完成备份的配置功能。

```yaml
backupSpec:
  backupSchedule: "0 */1 * * * ?"
```

另外需要注意的是，在MogDB Operator 安装包中，修改相关的rclone配置。

具体路径:

```text
./kustomize/install/manager/configmap.yaml
```

修改的内容:

```text
[backup]
type = s3
provider = Minio
env_auth = false
access_key_id = root   // 连接用户
secret_access_key = root@123   // 连接密码
endpoint = http://10.105.68.26:9000  // s3的服务地址
```

<br/>

## 架构

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/backup-management.png)
