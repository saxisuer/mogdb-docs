---
title: 客户端
summary: 客户端
author: Wang Dong
date: 2022-06-30
---

# mgo客户端

MogDB Operator Client，又叫做mgo，是与 MogDB Operator 交互的最便捷方式。mgo通过一系列简单的命令，为创建、管理和删除 MogDB 集群提供了许多便捷的方法。
MogDB Operator 提供了于 mgo 客户端连接的接口，并通过 RBAC 和 TLS 进行验证管理。

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/single-idc-operator-arch.png)

mgo客户端可用于 Linux、macOS，您可以从[发布版本](../release.md#v100)中选择您所需的mgo客户端。

<br/>

## 使用mgo客户端的一般注意事项

如果您使用[快速入门](../quick-start.md) 指南安装 MogDB Operator，您会将 MogDB Operator 安装到名为 mogdb-operator-system 命名空间下。

为方便起见，我们建议设置 mogdb-operator-system 为 环境变量MGO_NAMESPACE的值。在您将执行 mgo 命令的 shell 中，运行以下命令：

```shell
export MGO_NAMESPACE=mogdb-operator-system
```

如果您不想设置此环境变量，或者处于无法使用环境变量的环境中，则必须对大多数命令使用 --namespace(or -n) 标志，例如：

```shell
mgo version -n mogdb-operator-system
```

<br/>

## 语法

mgo 的语法类似于您使用 kubectl 一样简单，MogDB Operator 项目的目标之一是允许在支持 Kubernetes 的环境中无缝管理 MogDB 集群，并且通过遵循用户熟悉的命令模式，使学习曲线变得更加容易。

要了解 mgo 命令的顶层可用内容，请执行：

```shell
mgo
```

mgo 命令的语法通常遵循以下格式：

```shell
pgo [command] ([TYPE] [NAME]) [flags]
```

其中command是一个动词，例如：

- create
- show
- delete

type是一种资源类型，例如：

- cluster
- mgorole
- mgouser

name是资源类型的名称，例如：

- cluster1
- admin

有些全局flag作用于每个 mgo 命令，有些特殊flag作用于指定 mgo 命令，要获取命令可用的所有选项和flag的列表，您可以使用--help flag。例如，要查看mgo create cluster 命令可用的所有选项，您可以运行以下命令：

```shell
mgo create cluster --help
```

<br/>

## 命令概述

下表概述了 mgo 客户端提供的命令。

| 命令         | 语法                                 | 描述                                                 |
|-------------|--------------------------------------|-----------------------------------------------------|
| create      | mgo create cluster cluster1          | 创建 Operator 资源类型（例如cluster、mgouser、mgorole） |
| delete      | mgo delete cluster cluster1          | 删除 Operator 资源类型（例如cluster、mgouser、mgorole） |
| update      | mgo update cluster cluster1 --cpu=1  | 更新 MogDB 集群、mgouser、mgorole                     |
| show        | mgo show cluster cluster1            | 显示 Operator 资源类型（例如cluster、mgouser、mgorole） |
| scale       | mgo scale cluster1                   | 为给定的 MogDB 集群创建一个 MogDB 副本                  |
| scaledown   | mgo scaledown cluster1 --query       | 从 MogDB 集群中删除副本。                              |
| version     | mgo version                          | 显示 Operator 版本信息。                              |

<br/>

## 全局flag

适用于所有 mgo 命令的全局flag。

**注意：** flag优先于环境变量

| flag         | 描述                                                                                      | 
|---------------------------|-----------------------------------------------------------------------------|
| --apiserver-url string    | 将处理来自mgo客户端的请求的mogDB Operator apiserver的URL。请注意，URL不应以“/”结尾  | 
| --debug                   | 启用debug输出以进行调试                                                        | 
| --disable-tls             | 关闭 MogDB Operator 的 TLS 认证                                               | 
| --exclude-os-trust        | 从操作系统默认信任存储中排除CA证书                                                | 
| -h, --help                | mgo 的帮助信息                                                                | 
| --mgo-ca-cert string      | 连接 MogDB Operator apiserver CA 证书文件路径.                                 | 
| --mgo-client-cert string  | 用于向 MogDB Operator apiserver 进行身份验证的客户端证书文件路径                   |    
| --mgo-client-key string   | 用于向 MogDB Operator apiserver 进行身份验证的客户端密钥文件路径                   |    
| -n, --namespace string    | 用于mgo请求的命名空间                                                          |    