---
title: 恢复管理
summary: 恢复管理
author: Ji Yabin
date: 2022-06-16
---

# 恢复管理

MogDB Operator通过在s3存储上，查找合适的备份数据，恢复数据库到指定的时间点。

您可以通过设置如下的期望值，来恢复指定的数据库:

```yaml
kind: MogDBRecovery
metadata:
  name: sample1-recovery
spec:
  # Add fields here
  clusterName: sample1
  recoveryTimePoint: 2022-02-28 07:15:59
```

<br/>

## 架构

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/recovery-management.png)
