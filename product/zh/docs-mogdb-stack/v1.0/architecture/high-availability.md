---
title: 高可用
summary: 高可用
author: Ji Yabin
date: 2022-06-16
---

# 高可用

高可用是MogDB Stack体系中的，一个功能组件，以sidecar模式运行在MogDB所在的pod中，实时的监测MogDB的运行状态，当primary出现不可用状态时，触发切换逻辑。

发生切换的场景如下:

- 数据库磁盘故障或者某些硬件故障
- 主库的网络不可达
- 数据库发生故障宕机
- 机架掉电

<br/>

## 特性

- 通过dcs保证ha自身的高可用
- ha具备leader、follower角色，leader具有决策权
- 实时修复MogDB集群的复制状态
- 维护MogDB的角色

<br/>

## 架构

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/ha.png)
