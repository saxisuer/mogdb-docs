---
title: 监控
summary: 监控
author: Ji Yabin
date: 2022-06-16
---

# Kubernetes监控与告警

本文介绍如何对 Kubernetes 集群进行监控。在MogDB集群运行的过程中，需要对容器资源、宿主机、Kubernetes 组件等进行监控。对于这些组件或资源的监控，需要在整个 Kubernetes 集群维度部署监控系统来实现。

<br/>

## 宿主机监控

通过node exporter对物理机指标进行采集，统一抓取到prometheus中。Node Exporter 是 Prometheus 官方提供的一个节点资源采集组件，可以用于收集服务器节点的数据，如 CPU频率信息、磁盘IO统计、剩余可用内存 等等。Node Exporter 会将收集到的信息转换为 Prometheus 可识别的 Metrics 数据。Prometheus 可以从 Node Exporter 中对这些指标进行收集与存储，并且可以根据这些数据的实时变化进行服务器节点资源监控。

<br/>

## 容器监控

cAdvisor是Google开源的容器资源监控和性能分析工具，它是专门为容器而生，在Kubernetes中，我们不需要单独去安装，cAdvisor作为kubelet内置的一部分程序可以直接使用，也就是我们可以直接使用cadvisor采集数据，可以采集到和容器运行相关的所有指标。

cadvisor中获取到的典型监控指标如下：

| 指标名称                                    | 类型       | 含义                        |
|-----------------------------------------|----------|---------------------------|
| container_cpu_load_average_10s          | gauge    | 过去10秒容器CPU的平均负载           |
| container_cpu_usage_seconds_total       | counter  | 容器在每个CPU内核上的累积占用时间 (单位：秒) |
| container_cpu_system_seconds_total      | counter  | System CPU累积占用时间（单位：秒）    |
| container_cpu_user_seconds_total        | counter  | User CPU累积占用时间（单位：秒）      |
| container_fs_usage_bytes                | gauge    | 容器中文件系统的使用量(单位：字节)        |
| container_fs_limit_bytes                | gauge    | 容器可以使用的文件系统总量(单位：字节)      |
| container_fs_reads_bytes_total          | counter  | 容器累积读取数据的总量(单位：字节)        |
| container_fs_writes_bytes_total         | counter  | 容器累积写入数据的总量(单位：字节)        |
| container_memory_max_usage_bytes        | gauge    | 容器的最大内存使用量（单位：字节）         |
| container_memory_usage_bytes            | gauge    | 容器当前的内存使用量（单位：字节）         |
| container_spec_memory_limit_bytes       | gauge    | 容器的内存使用量限制                |
| machine_memory_bytes                    | gauge    | 当前主机的内存总量                 |
| container_network_receive_bytes_total   | counter  | 容器网络累积接收数据总量（单位：字节）       |
| container_network_transmit_bytes_total  | counter  | 容器网络累积传输数据总量（单位：字节）       |

<br/>

## 资源对象监控

kube-state-metrics提供资源对象本身的监控，如pod运行状态、有多少job在运行中等等，它基于client-go开发，轮询Kubernetes API，并将Kubernetes的结构化信息转换为metrics。

指标类别包括：

- CronJob Metrics

- DaemonSet Metrics

- Deployment Metrics

- Job Metrics

- LimitRange Metrics

- Node Metrics

- PersistentVolume Metrics

- PersistentVolumeClaim Metrics

- Pod Metrics

- Pod Disruption Budget Metrics

- ReplicaSet Metrics

- ReplicationController Metrics

- ResourceQuota Metrics

- Service Metrics

- StatefulSet Metrics

- Namespace Metrics

- Horizontal Pod Autoscaler Metrics

- Endpoint Metrics

- Secret Metrics

- ConfigMap Metrics

<br/>

## MogDB监控

为了获取MogDB自身的监控指标，需要配套的MogDB exporter采集相应的数据。

<br/>

## 架构

![deploy-arch](https://cdn-mogdb.enmotech.com/docs-media/mogdb-stack/v1.0.0/monitor.png)
