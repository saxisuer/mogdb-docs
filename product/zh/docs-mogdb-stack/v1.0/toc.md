<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# 文档

## MogDB Stack 文档目录

+ [关于MogDB Stack](/overview.md)
+ [快速上手](/quick-start.md)
+ 软件安装
  + [kustomize](installation/kustomize.md)
  + [kubectl](installation/kubectl.md)
+ 使用手册
  + [开始](tutorial/getting-started.md)
  + [创建MogDB集群](tutorial/create-a-mogdb-cluster.md)
  + [连接MogDB集群](tutorial/conntect-to-mogdb-cluster.md)
  + [扩缩容](tutorial/resize-a-mogdb-cluster.md)
  + [定制MogDB集群](tutorial/customize-a-mogdb-cluster.md)
+ 架构
  + [高可用](architecture/high-availability.md)
  + [监控](architecture/monitor.md)
  + [备份](architecture/backup-management.md)
  + [恢复](architecture/recovery-management.md)
  + [客户端](architecture/client.md)
+ 参考
  + 命令行
    + [mgo](references/client/mgo.md)
    + [mgo create](references/client/mgo-create.md)
    + [mgo create mgorole](references/client/mgo-create-mgorole.md)
    + [mgo create mgouser](references/client/mgo-create-mgouser.md)
    + [mgo create cluster](references/client/mgo-create-cluster.md)
    + [mgo show](references/client/mgo-show.md)
    + [mgo show mgorole](references/client/mgo-show-mgorole.md)
    + [mgo show mgouser](references/client/mgo-show-mgouser.md)
    + [mgo show cluster](references/client/mgo-show-cluster.md)
    + [mgo delete](references/client/mgo-delete.md)
    + [mgo delete mgorole](references/client/mgo-delete-mgorole.md)
    + [mgo delete mgouser](references/client/mgo-delete-mgouser.md)
    + [mgo delete cluster](references/client/mgo-delete-cluster.md)
    + [mgo scale](references/client/mgo-scale.md)
    + [mgo scaledown](references/client/mgo-scaledown.md)
    + [mgo switch](references/client/mgo-switch.md)
    + [mgo update](references/client/mgo-update.md)
    + [mgo update mgorole](references/client/mgo-update-mgorole.md)
    + [mgo update mgouser](references/client/mgo-update-mgouser.md)
    + [mgo update cluster](references/client/mgo-update-cluster.md)
    + [mgo version](references/client/mgo-version.md)
  + 服务端
    + [CRD参考](references/server/crd-references.md)
    + [MogDB集群参考](references/server/mogdbcluster-sample.md)
    + [MogDB备份参考](references/server/mogdbbackup-sample.md)
    + [MogDB恢复参考](references/server/mogdbrecovery-sample.md)
+ [FAQ](faq.md)
+ [发布记录](release.md)