---
title: 备份恢复
summary: 备份恢复
author: Guo Huan
date: 2022-11-24
---

# 备份恢复

时序数据库提供数据备份恢复的工具，通过数据备份达到容灾的目的。

Uqbar继承MogDB能力，支持物理备份及恢复；物理备份支持全量备份和增量备份两种模式。时序表支持物理备份。

## gs_basebackup

### 背景信息

Uqbar部署成功后，在数据库运行的过程中，会遇到各种问题及异常状态。Uqbar提供了gs_basebackup工具做基础的物理备份。gs_basebackup的实现目标是对服务器数据库文件的二进制进行拷贝，其实现原理使用了复制协议。远程执行gs_basebackup时，需要使用系统管理员账户。gs_basebackup当前支持热备份模式和压缩格式备份模式。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - gs_basebackup仅支持主机和备机的全量备份，不支持增量。
> - gs_basebackup当前支持热备份模式和压缩格式备份模式。
> - 若打开增量检测点功能且打开双写，gs_basebackup也会备份双写文件。
> - gs_basebackup在备份包含绝对路径的表空间时，如果在同一台机器上进行备份，可以通过tablespace-mapping重定向表空间路径，或使用归档模式进行备份。
> - 若pg_xlog目录为软链接，备份时将不会建立软链接，会直接将数据备份到目的路径的pg_xlog目录下。
> - 备份过程中收回用户备份权限，可能导致备份失败，或者备份数据不可用。
> - 如果因为网络临时故障等原因导致Server端无法应答，gs_basebackup将在最长等待120秒后退出。

### 前提条件

- 可以正常连接Uqbar数据库。
- 备份过程中用户权限没有被回收。
- pg_hba.conf中需要配置允许复制链接，且该连接必须由一个系统管理员建立。
- 如果xlog传输模式为stream模式，需要配置max_wal_senders的数量，至少有一个可用。
- 如果xlog传输模式为fetch模式，有必要把wal_keep_segments参数设置得足够高，这样在备份末尾之前日志不会被移除。
- 在进行还原时，需要保证各节点备份目录中存在备份文件，若备份文件丢失，则需要从其他节点进行拷贝。

### 语法

- 显示帮助信息

  ```bash
  gs_basebackup -? | --help
  ```

- 显示版本号信息

  ```bash
  gs_basebackup -V | --version
  ```

### 参数说明

gs_basebackup参数可以分为如下几类：

- -D directory

  备份文件输出的目录，必选项。

- 常用参数：

  - -c，--checkpoint=fast|spread

    设置检查点模式为fast或者spread（默认）。

  - -l，--label=LABEL

    为备份设置标签。

  - -P，--progress

    启用进展报告。

  - -v， --verbose

    启用冗长模式。

  - -V， --version

    打印版本后退出。

  - -?，--help

    显示gs_basebackup命令行参数。

  - -T，--tablespace-mapping=olddir=newdir

    在备份期间将目录olddir中的表空间重定位到newdir中。为使之有效，olddir必须正好匹配表空间所在的路径（但如果备份中没有包含olddir中的表空间也不是错误）。olddir和newdir必须是绝对路径。如果一个路径凑巧包含了一个=符号，可用反斜线对它转义。对于多个表空间可以多次使用这个选项。

  - -F，--format=plain|tar

    设置输出格式为plain（默认）或者tar。没有设置该参数的情况下，默认--format=plain。plain格式把输出写成平面文件，使用和当前数据目录和表空间相同的布局。当集簇没有额外表空间时，整个数据库将被放在目标目录中。如果集簇包含额外的表空间，主数据目录将被放置在目标目录中，但是所有其他表空间将被放在它们位于服务器上的相同的绝对路径中。tar模式将输出写成目标目录中的 tar 文件。主数据目录将被写入到一个名为base.tar的文件中，并且其他表空间将被以其 OID 命名。生成的tar包，需要用gs_tar命令解压。

  - -X， --xlog-method=fetch|stream

    设置xlog传输方式。没有设置该参数的情况下，默认--xlog-method=stream。在备份中包括所需的预写式日志文件（WAL文件）。这包括所有在备份期间产生的预写式日志。fetch方式在备份末尾收集预写式日志文件。因此，有必要把wal_keep_segments参数设置得足够高，这样在备份末尾之前日志不会被移除。如果在要传输日志时它已经被轮转，备份将失败并且是不可用的。stream方式在备份被创建时流传送预写式日志。这将开启一个到服务器的第二连接并且在运行备份时并行开始流传输预写式日志。因此，它将使用最多两个由max_wal_senders参数配置的连接。只要客户端能保持接收预写式日志，使用这种模式不需要在主控机上保存额外的预写式日志。

  - -x，--xlog 使用这个选项等效于和方法fetch一起使用-X。

  - -Z --compress=level

    启用对 tar 文件输出的 gzip 压缩，并且制定压缩级别（0 到 9，0 是不压缩，9 是最佳压缩）。只有使用 tar 格式时压缩才可用，并且会在所有tar文件名后面自动加上后缀.gz。

  - -z

    启用对 tar 文件输出的 gzip 压缩，使用默认的压缩级别。只有使用 tar 格式时压缩才可用，并且会在所有tar文件名后面自动加上后缀.gz。

  - -t，--rw-timeout

    设置备份期间checkpoint的时间限制，默认限制时间为120s。当数据库全量checkpoint耗时较长时，可以适当增大rw-timeout限制时间。

- 连接参数

  - -h, --host=HOSTNAME

    指定正在运行服务器的主机名或者Unix域套接字的路径。

  - -p，--port=PORT

    指定数据库服务器的端口号。

    可以通过port参数修改默认端口号。

  - -U，--username=USERNAME

    指定连接数据库的用户。

  - -s, --status-interval=INTERVAL

    发送到服务器的状态包的时间（以秒为单位）。

  - -w,--no-password

    不出现输入密码提示。

  - -W, --password

    当使用-U参数连接本地数据库或者连接远端数据库时，可通过指定该选项出现输入密码提示。

### 示例

```bash
gs_basebackup -D /home/test/trunk/install/data/backup -h 127.0.0.1 -p 21233
INFO:  The starting position of the xlog copy of the full build is: 0/1B800000. The slot minimum LSN is: 0/1B800000.
```

### 从备份文件恢复数据

当数据库发生故障时需要从备份文件进行恢复。因为gs_basebackup是对数据库按二进制进行备份，因此恢复时可以直接拷贝替换原有的文件，或者直接在备份的库上启动数据库。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 若当前数据库实例正在运行，直接从备份文件启动数据库可能会存在端口冲突，这时需要修改配置文件的port参数，或者在启动数据库时指定一下端口。
> - 若当前备份文件为主备数据库，可能需要修改一下主备之间的复制连接。即配置文件postgresql.conf中的replconninfo1，replconninfo2等。
> - 若配置文件postgresql.conf的参数data_directory打开且有配置，当使用备份目录启动数据库时候，data_directory和备份目录不同会导致启动失败。可以修改data_directory的值为新的数据目录，或者注释掉该参数。

若要在原库的地方恢复数据库，参考步骤如下：

1. 停止数据库服务器，具体操作请参见《管理指南》。
2. 将原数据库和所有表空间复制到另外一个位置, 以备后面需要。
3. 清理原库中的所有或部分文件。
4. 使用数据库系统用户权限从备份中还原需要的数据库文件。
5. 若数据库中存在链接文件，需要修改使其链接到正确的文件。
6. 重启数据库服务器，并检查数据库内容，确保数据库已经恢复到所需的状态。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 暂不支持备份文件增量恢复。
> - 恢复后需要检查数据库中的链接文件是否链接到正确的文件。

<br/>

## gs_probackup

### 背景信息

gs_probackup是一个用于管理Uqbar数据库备份和恢复的工具。它对Uqbar实例进行定期备份，以便在数据库出现故障时能够恢复服务器。

- 可用于备份单机数据库，也可对主机或者主节点数据库备机进行备份，为物理备份。
- 可备份外部目录的内容，如脚本文件、配置文件、日志文件、dump文件等。
- 支持增量备份、定期备份和远程备份。
- 可设置备份的留存策略。

### 前提条件

- 可以正常连接Uqbar数据库。
- 若要使用PTRACK增量备份，需在postgresql.conf中手动添加参数“enable_cbm_tracking = on”。
- 为了防止xlog在传输结束前被清理，请适当调高postgresql.conf文件中wal_keep_segments的值。

### 限制说明

- 备份必须由运行数据库服务器的用户执行。
- 备份和恢复的数据库服务器的主版本号必须相同。
- 如果要通过ssh在远程模式下备份数据库，需要在本地和远程主机安装相同主版本的数据库，并通过ssh-copy-id remote_user@remote_host命令设置本地主机备份用户和远程主机数据库用户的无密码ssh连接。
- 远程模式下只能执行add-instance、backup、restore子命令。
- 使用restore子命令前，应先停止uqbar进程。
- 当存在用户自定义表空间时，备份的时候要加上--external-dirs 参数，否则，该表空间不会被备份。
- 当备份的规模比较大时，为了防止备份过程中timeout发生，请适当调整postgresql.conf文件的参数 session_timeout、wal_sender_timeout。并且在备份的命令行参数中适当调整参数--rw-timeout的值。
- 恢复时，使用-T选项把备份中的外部目录重定向到新目录时，请同时指定参数--external-mapping。
- 当使用远程备份时，请确保远程机器和备份机器的时钟同步，以防止使用--recovery-target-time恢复的场合,启动uqbar时有可能会失败。
- 当远程备份有效时\(remote-proto=ssh\)，请确保-h和--remote-host指定的是同一台机器。当远程备份无效时，如果指定了-h选项，请确保-h指定的是本机地址或本机主机名。
- 当前暂不支持备份逻辑复制槽。

### 命令说明

- 打印gs_probackup版本。

  ```bash
  gs_probackup -V|--version
  gs_probackup version
  ```

- 显示gs_probackup命令的摘要信息。如果指定了gs_probackup的子命令，则显示可用于此子命令的参数的详细信息。

  ```bash
  gs_probackup -?|--help
  gs_probackup help [command]
  ```

- 初始化备份路径_backup-path_中的备份目录，该目录将存储已备份的内容。如果备份路径_backup-path_已存在，则_backup-path_必须为空目录。

  ```bash
  gs_probackup init -B backup-path [--help]
  ```

- 在备份路径_backup-path_内初始化一个新的备份实例，并生成pg_probackup.conf配置文件，该文件保存了指定数据目录_pgdata-path_的gs_probackup设置。

  ```bash
  gs_probackup add-instance -B backup-path -D pgdata-path --instance=instance_name
  [-E external-directories-paths]
  [remote_options]
  [--help]
  ```

- 在备份路径_backup-path_内删除指定实例相关的备份内容。

  ```bash
  gs_probackup del-instance -B backup-path --instance=instance_name
  [--help]
  ```

- 将指定的连接、压缩、日志等相关设置添加到pg_probackup.conf配置文件中，或修改已设置的值。不推荐手动编辑pg_probackup.conf配置文件。

  ```bash
  gs_probackup set-config -B backup-path --instance=instance_name
  [-D pgdata-path] [-E external-directories-paths] [--archive-timeout=timeout]
  [--retention-redundancy=retention-redundancy] [--retention-window=retention-window] [--wal-depth=wal-depth]
  [--compress-algorithm=compress-algorithm] [--compress-level=compress-level]
  [-d dbname] [-h hostname] [-p port] [-U username]
  [logging_options] [remote_options]
  [--help]
  ```

- 将备份相关设置添加到backup.control配置文件中，或修改已设置的值。

  ```bash
  gs_probackup set-backup -B backup-path --instance=instance_name -i backup-id
  [--note=text] [pinning_options]
  [--help]
  ```

- 显示位于备份目录中的pg_probackup.conf配置文件的内容。可以通过指定--format=json选项，以json格式显示。默认情况下，显示为纯文本格式。

  ```bash
  gs_probackup show-config -B backup-path --instance=instance_name
  [--format=plain|json]
  [--help]
  ```

- 显示备份目录的内容。如果指定了instance_name和backup_id，则显示该备份的详细信息。可以通过指定--format=json选项，以json格式显示。默认情况下，备份目录的内容显示为纯文本格式。

  ```bash
  gs_probackup show -B backup-path
  [--instance=instance_name [-i backup-id]] [--archive] [--format=plain|json]
  [--help]
  ```

- 创建指定实例的备份。

  ```bash
  gs_probackup backup -B backup-path --instance=instance_name -b backup-mode
  [-D pgdata-path] [-C] [-S slot-name] [--temp-slot] [--backup-pg-log] [-j threads_num] [--progress]
  [--no-validate] [--skip-block-validation] [-E external-directories-paths] [--no-sync] [--note=text]
  [--archive-timeout=timeout] [-t rwtimeout]
  [logging_options] [retention_options] [compression_options]
  [connection_options] [remote_options] [pinning_options]
  [--help]
  ```

- 从备份目录_backup-path_中的备份副本恢复指定实例。如果指定了恢复目标选项，gs_probackup将查找最近的备份并将其还原到指定的恢复目标。否则，使用最近一次备份。

  ```bash
  gs_probackup restore -B backup-path --instance=instance_name
  [-D pgdata-path] [-i backup_id] [-j threads_num] [--progress] [--force] [--no-sync] [--no-validate] [--skip-block-validation]
  [--external-mapping=OLDDIR=NEWDIR] [-T OLDDIR=NEWDIR] [--skip-external-dirs] [-I incremental_mode]
  [recovery_options] [remote_options] [logging_options]
  [--help]
  ```

- 将指定的增量备份与其父完全备份之间的所有增量备份合并到父完全备份。父完全备份将接收所有合并的数据，而已合并的增量备份将作为冗余被删除。

  ```bash
  gs_probackup merge -B backup-path --instance=instance_name -i backup_id
  [-j threads_num] [--progress] [logging_options]
  [--help]
  ```

- 删除指定备份，或删除不满足当前保留策略的备份。

  ```bash
  gs_probackup delete -B backup-path --instance=instance_name
  [-i backup-id | --delete-expired | --merge-expired | --status=backup_status]
  [--delete-wal] [-j threads_num] [--progress]
  [--retention-redundancy=retention-redundancy] [--retention-window=retention-window]
  [--wal-depth=wal-depth] [--dry-run]
  [logging_options]
  [--help]
  ```

- 验证恢复数据库所需的所有文件是否存在且未损坏。如果未指定_instance_name_，gs_probackup将验证备份目录中的所有可用备份。如果指定_instance_name_而不指定任何附加选项，gs_probackup将验证此备份实例的所有可用备份。如果指定了_instance_name_并且指定_backup-id_或恢复目标相关选项，gs_probackup将检查是否可以使用这些选项恢复数据库。

  ```bash
  gs_probackup validate -B backup-path
  [--instance=instance_name] [-i backup-id]
  [-j threads_num] [--progress] [--skip-block-validation]
  [--recovery-target-time=time | --recovery-target-xid=xid | --recovery-target-lsn=lsn | --recovery-target-name=target-name]
  [--recovery-target-inclusive=boolean]
  [logging_options]
  [--help]
  ```

### 参数说明

#### 通用参数

- command

  gs_probackup除version和help以外的子命令：init、add-instance、del-instance、set-config、set-backup、show-config、show、backup、restore、merge、delete、validate。

- -?, --help

  显示gs_probackup命令行参数的帮助信息，然后退出。

  子命令中只能使用-help，不能使用-?。

- -V, --version

  打印gs_probackup版本，然后退出。

- -B *backup-path*, --backup-path=*backup-path*

  备份的路径。

  系统环境变量：$BACKUP_PATH

- -D *pgdata-path*, --pgdata=*pgdata-path*

  数据目录的路径。

  系统环境变量：$PGDATA

- --instance=*instance_name*

  实例名。

- -i *backup-id*, --backup-id=*backup-id*

  备份的唯一标识。

- --format=*format*

  指定显示备份信息的格式，支持plain和json格式。

  默认值：plain

- --status=*backup_status*

  删除指定状态的所有备份，包含以下状态：

  - OK：备份已完成且有效。
  - DONE：备份已完成但未经过验证。
  - RUNNING：备份正在进行中。
  - MERGING：备份正在合并中。
  - DELETING：备份正在删除中。
  - CORRUPT：部分备份文件已损坏。
  - ERROR：由于意外错误，备份失败。
  - ORPHAN：由于其父备份之一已损坏或丢失，备份无效。

- -j *threads_num*, --threads=*threads_num*

  设置备份、还原、合并进程的并行线程数。

- --archive

  显示WAL归档信息。

- --progress

  显示进度。

- --note=*text*

  给备份添加note。

#### 备份相关参数

- -b *backup-mode*, --backup-mode=*backup-mode*

  指定备份模式，支持FULL和PTRACK。

  FULL：创建全量备份，全量备份包含所有数据文件。

  PTRACK：创建PTRACK增量备份。

- -C, --smooth-checkpoint

  将检查点在一段时间内展开。默认情况下，gs_probackup会尝试尽快完成检查点。

- -S *slot-name*, --slot=*slot-name*

  指定WAL流处理的复制slot。

- --temp-slot

  在备份的实例中为WAL流处理创建一个临时物理复制slot，它确保在备份过程中，所有所需的WAL段仍然是可用的。

  默认的slot名为pg_probackup_slot，可通过选项--slot/-S更改。

- --backup-pg-log

  将日志目录包含到备份中。此目录通常包含日志消息。默认情况下包含日志目录，但不包含日志文件。如果修改了默认的日志路径，需要备份日志文件时可使用-E参数进行备份，使用方法见下文。

- -E *external-directories-paths*, --external-dirs=*external-directories-paths*

  将指定的目录包含到备份中。此选项对于备份位于数据目录外部的脚本、sql转储和配置文件很有用。如果要备份多个外部目录，请在Unix上用冒号分隔它们的路径。

  例如：-E /tmp/dir1:/tmp/dir2

- --skip-block-validation

  关闭块级校验，加快备份速度。

- --no-validate

  在完成备份后跳过自动验证。

- --no-sync

  不将备份文件同步到磁盘。

- --archive-timeout=*timeout*

  以秒为单位设置流式处理的超时时间。

  默认值：300

- -t rwtimeout

  以秒为单位的连接的超时时间。

  默认值: 120

#### 恢复相关参数

- -I, --incremental-mode=none|checksum|lsn

  若PGDATA中可用的有效页没有修改，则重新使用它们。

  默认值：none

- --external-mapping=*OLDDIR=NEWDIR*

  在恢复时，将包含在备份中的外部目录从_OLDDIR_重新定位到_NEWDIR_目录。_OLDDIR_和_NEWDIR_都必须是绝对路径。如果路径中包含“=”，则使用反斜杠转义。此选项可为多个目录多次指定。

- -T *OLDDIR=NEWDIR*, --tablespace-mapping=*OLDDIR=NEWDIR*

  在恢复时，将表空间从_OLDDIR_重新定位到_NEWDIR_目录。_OLDDIR_和_NEWDIR_必须都是绝对路径。如果路径中包含“=”，则使用反斜杠转义。多个表空间可以多次指定此选项。此选项必须和--external-mapping一起使用。

- --skip-external-dirs

  跳过备份中包含的使用-external-dirs选项指定的外部目录。这些目录的内容将不会被恢复。

- --skip-block-validation

  跳过块级校验，以加快验证速度。在恢复之前的自动验证期间，将仅做文件级别的校验。

- --no-validate

  跳过备份验证。

- --force

  允许忽略备份的无效状态。如果出于某种原因需要从损坏的或无效的备份中恢复数据，可以使用此标志。请谨慎使用。

#### 恢复目标相关参数(recovery_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前不支持配置连续的WAL归档的PITR，因而使用这些参数会有一定限制，具体如下描述。 如果需要使用持续归档的WAL日志进行PITR恢复，请按照下面描述的步骤： 
>
> 1. 将物理备份的文件替换目标数据库目录。 
> 2. 删除数据库目录下pg_xlog/中的所有文件。 
> 3. 将归档的WAL日志文件复制到pg_xlog文件中（此步骤可以省略，通过配置recovery.conf恢复命令文件中的restore_command项替代）。 
> 4. 在数据库目录下创建恢复命令文件recovery.conf，指定数据库恢复的程度。 
> 5. 启动数据库。 
> 6. 连接数据库，查看是否恢复到希望预期的状态。若已经恢复到预期状态，通过pg_xlog_replay_resume()指令使主节点对外提供服务。

- --recovery-target-lsn=*lsn*

  指定要恢复到的lsn，当前只能指定备份的stop lsn。

- --recovery-target-name=*target-name*

  指定要将数据恢复到的已命名的保存点，保存点可以通过查看备份中recovery-name字段得到。

- --recovery-target-time=*time*

  指定要恢复到的时间，当前只能指定备份中的recovery-time。

- --recovery-target-xid=*xid*

  指定要恢复到的事务ID，当前只能指定备份中的recovery-xid。

- --recovery-target-inclusive=*boolean*

  当该参数指定为true时，恢复目标将包括指定的内容。

  当该参数指定为false时，恢复目标将不包括指定的内容。

  该参数必须和--recovery-target-name、--recovery-target-time、--recovery-target-lsn或--recovery-target-xid一起使用。

#### 留存相关参数(retention_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  可以和backup和delete命令一起使用这些参数。

- --retention-redundancy=*retention-redundancy*

  指定在数据目录中留存的完整备份数。必须为正整数。0表示禁用此设置。

  默认值：0

- --retention-window=*retention-window*

  指定留存的天数。必须为正整数。0表示禁用此设置。

  默认值：0

- --wal-depth=*wal-depth*

  每个时间轴上必须留存的执行PITR能力的最新有效备份数。必须为正整数。0表示禁用此设置。

  默认值：0

- --delete-wal

  从任何现有的备份中删除不需要的WAL文件。

- --delete-expired

  删除不符合pg_probackup.conf配置文件中定义的留存策略的备份。

- --merge-expired

  将满足留存策略要求的最旧的增量备份与其已过期的父备份合并。

- --dry-run

  显示所有可用备份的当前状态，不删除或合并过期备份。

#### 固定备份相关参数(pinning_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  如果要将某些备份从已建立的留存策略中排除，可以和backup和set-backup命令一起使用这些参数。

- --ttl=*interval*

  指定从恢复时间开始计算，备份要固定的时间量。必须为正整数。0表示取消备份固定。

  支持的单位：ms、 s、 min、 h、 d（默认为s）。

  例如：-ttl=30d。

- --expire-time=*time*

  指定备份固定失效的时间戳。必须是ISO-8601标准的时间戳。

  例如：--expire-time='2020-01-01 00:00:00+03'

#### 日志相关参数(logging_options)

日志级别：verbose、log、info、warning、error和off。

- --log-level-console=*log-level-console*

  设置要发送到控制台的日志级别。每个级别都包含其后的所有级别。级别越高，发送的消息越少。指定off级别表示禁用控制台日志记录。

  默认值：info

- --log-level-file=*log-level-file*

  设置要发送到日志文件的日志级别。每个级别都包含其后的所有级别。级别越高，发送的消息越少。指定off级别表示禁用日志文件记录。

  默认值：off

- --log-filename=*log-filename*

  指定要创建的日志文件的文件名。文件名可以使用strftime模式，因此可以使用%-escapes指定随时间变化的文件名。

  例如，如果指定了“pg_probackup-%u.log”模式，则pg_probackup为每周的每一天生成单独的日志文件，其中%u替换为相应的十进制数字，即pg_probackup-1.log表示星期一；pg_probackup-2.log表示星期二，以此类推。

  如果指定了--log-level-file参数启用日志文件记录，则该参数有效。

  默认值：“pg_probackup.log”

- --error-log-filename=*error-log-filename*

  指定仅用于error日志的日志文件名。指定方式与--log-filename参数相同。

  此参数用于故障排除和监视。

- --log-directory=*log-directory*

  指定创建日志文件的目录。必须是绝对路径。此目录会在写入第一条日志时创建。

  默认值：$BACKUP_PATH/log

- --log-rotation-size=*log-rotation-size*

  指定单个日志文件的最大大小。如果达到此值，则启动gs_probackup命令后，日志文件将循环，但help和version命令除外。0表示禁用基于文件大小的循环。

  支持的单位：KB、MB、GB、TB（默认为KB）。

  默认值：0

- --log-rotation-age=*log-rotation-age*

  单个日志文件的最大生命周期。如果达到此值，则启动gs_probackup命令后，日志文件将循环，但help和version命令除外。$BACKUP_PATH/log/log_rotation目录下保存最后一次创建日志文件的时间。0表示禁用基于时间的循环。

  支持的单位：ms、 s、 min、 h、 d（默认为min）。

  默认值：0

#### 连接相关参数(connection_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  可以和backup命令一起使用这些参数。

- -d *dbname*, --pgdatabase=*dbname*

  指定要连接的数据库名称。该连接仅用于管理备份进程，因此您可以连接到任何现有的数据库。如果命令行、PGDATABASE环境变量或pg_probackup.conf配置文件中没有指定此参数，则gs_probackup会尝试从PGUSER环境变量中获取该值。如果未设置PGUSER变量，则从当前用户名获取。

  系统环境变量：$PGDATABASE

- -h *hostname*, --pghost=*hostname*

  指定运行服务器的系统的主机名。如果该值以斜杠开头，则被用作到Unix域套接字的路径。

  系统环境变量：$PGHOST

  默认值：local socket

- -p *port*, --pgport=_p_*ort*

  指定服务器正在侦听连接的TCP端口或本地Unix域套接字文件扩展名。

  系统环境变量：$PGPORT

  默认值：5432

- -U *username*, --pguser=*username*

  指定所连接主机的用户名。

  系统环境变量：$PGUSER

- -w, --no-password

  不出现输入密码提示。如果主机要求密码认证并且密码没有通过其它形式给出，则连接尝试将会失败。 该选项在批量工作和不存在用户输入密码的脚本中很有帮助。

- -W *password*, --password=*password*

  指定用户连接的密码。如果主机的认证策略是trust，则不会对系统管理员进行密码验证，即无需输入-W选项；如果没有-W选项，并且不是系统管理员，则会提示用户输入密码。

#### 压缩相关参数(compression_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  可以和backup命令一起使用这些参数。

- --compress-algorithm=*compress-algorithm*

  指定用于压缩数据文件的算法。

  取值包括zlib、pglz和none。如果设置为zlib或pglz，此选项将启用压缩。默认情况下，压缩功能处于关闭状态。

  默认值：none

- --compress-level=*compress-level*

  指定压缩级别。取值范围: 0~9

  - 0表示无压缩。
  - 1表示压缩比最小，处理速度最快。
  - 9表示压缩比最大，处理速度最慢。
  - 可与--compress-algorithm选项一起使用。

  默认值：1

- --compress

  以--compress-algorithm=zlib和--compress-level=1进行压缩。

#### 远程模式相关参数(remote_options)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  通过SSH远程运行gs_probackup操作的相关参数。可以和add-instance、set-config、backup、restore命令一起使用这些参数。

- --remote-proto=*protocol*

  指定用于远程操作的协议。目前只支持SSH协议。取值包括：

  ssh：通过SSH启用远程备份模式。这是默认值。

  none：显式禁用远程模式。

  如果指定了-remote-host参数，可以省略此参数。

- --remote-host=*destination*

  指定要连接的远程主机的IP地址或主机名。

- --remote-port=*port*

  指定要连接的远程主机的端口号。

  默认值：22

- --remote-user=*username*

  指定SSH连接的远程主机用户。如果省略此参数，则使用当前发起SSH连接的用户。

  默认值：当前用户

- --remote-path=*path*

  指定gs_probackup在远程系统的安装目录。

  默认值：当前路径

- --remote-libpath=*libpath*

  指定gs_probackup在远程系统安装的lib库目录。

- --ssh-options=*ssh_options*

  指定SSH命令行参数的字符串。

  例如：--ssh-options='-c cipher_spec -F configfile'

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 如果因为网络临时故障等原因导致server端无应答，gs_probackup将在等待archive-timeout（默认300秒）后退出。
  >
  > - 如果备机lsn与主机有差别时，数据库会不停地刷以下log信息，此时应重新build备机。
  >
  >   ```bash
  >   LOG: walsender thread shut down
  >   LOG: walsender thread started
  >   LOG: received wal replication command: IDENTIFY_VERSION
  >   LOG: received wal replication command: IDENTIFY_MODE
  >   LOG: received wal replication command: IDENTIFY_SYSTEM
  >   LOG: received wal replication command: IDENTIFY_CONSISTENCE 0/D0002D8
  >   LOG: remote request lsn/crc: [xxxxx] local max lsn/crc: [xxxxx]
  >   ```

### 备份流程

1. 初始化备份目录。在指定的目录下创建backups/和wal/子目录，分别用于存放备份文件和WAL文件。

   ```bash
   gs_probackup init -B backup_dir
   ```

2. 添加一个新的备份实例。gs_probackup可以在同一个备份目录下存放多个数据库实例的备份。

   ```bash
   gs_probackup add-instance -B backup_dir -D data_dir --instance instance_name
   ```

3. 创建指定实例的备份。在进行增量备份之前，必须至少创建一次全量备份。

   ```bash
   gs_probackup backup -B backup_dir --instance instance_name -b backup_mode
   ```

4. 从指定实例的备份中恢复数据。

   ```bash
   gs_probackup restore -B backup_dir --instance instance_name -D pgdata-path -i backup_id
   ```

### 故障处理

| 问题描述                                                     | 原因和解决方案                                               |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| ERROR: query failed: ERROR: canceling statement due to conflict with recovery（错误：查询失败：由于与恢复操作冲突，正在取消语句命令） | 原因：在备机上执行的操作正在访问存储行，主机上更改或者删除了对应的行，并将xlog在备机上重放，迫使备机上操作取消。<br />解决方案：<br />1. 适当增加如下配置参数的值<br />max_standby_archive_delay<br />max_standby_streaming_delay<br />2. 增加如下配置<br />hot_standby_feedback = on |

### 示例

以下为全量备份和增量备份示例，备份目录为/opt/uqbar/backup_dir。

1. 初始化备份目录。在指定的备份目录/opt/uqbar/backup_dir下创建backups/和wal/子目录，分别用于存放备份文件和WAL文件。

   ```bash
   [ommdoc@hostname]$ gs_probackup init -B /opt/uqbar/backup_dir
   INFO: Backup catalog '/opt/uqbar/backup_dir' successfully inited
   ```

2. 添加一个新的备份实例instance1（用户需自定义实例名称）。gs_probackup可以在同一个备份目录下存放多个数据库实例的备份。

   ```bash
   [ommdoc@hostname]$ gs_probackup add-instance -B /opt/uqbar/backup_dir -D /opt/uqbar/data --instance instance1
   INFO: Instance 'instance1' successfully inited
   ```

3. 创建指定实例的全量备份。

   **注意**：如果提示数据库无法连接，请执行如下命令建立数据库连接后再执行全量备份。例如执行如下命令连接到postgres数据库，且端口号为28000。

   ```bash
   [ommdoc@hostname]$ gs_probackup set-config --instance=instance1 -B /opt/uqbar/backup_dir -d postgres -p 28000
   ```

   ```bash
   [ommdoc@hostname]$ gs_probackup backup -B /opt/uqbar/backup_dir --instance instance1 -b FULL
   INFO: Backup start, gs_probackup version: 2.4.2, instance: instance1, backup ID: RGOZF6, backup mode: FULL, wal mode: STREAM, remote: false, compress-algorithm: none, compress-level: 1
   LOG: Backup destination is initialized
   LOG: This openGauss instance was initialized with data block checksums. Data block corruption will be detected
   LOG: Database backup start
   LOG: started streaming WAL at 0/CA000000 (timeline 1)
   INFO: Cannot parse path "base"
                                                                          [2022-08-16 13:04:18]: check identify system success
                                                                          [2022-08-16 13:04:18]: send START_REPLICATION 0/CA000000 success
                                                                          [2022-08-16 13:04:18]: keepalive message is received
                                                                          [2022-08-16 13:04:18]: keepalive message is received
   INFO: PGDATA size: 1320MB
   INFO: Start transferring data files
   LOG: Creating page header map "/opt/uqbar/backup_dir/backups/instance1/RGOZF6/page_header_map"
   INFO: Data files are transferred, time elapsed: 2s
   INFO: wait for pg_stop_backup()
   INFO: pg_stop backup() successfully executed
   LOG: stop_lsn: 0/CA0001E8
   LOG: Looking for LSN 0/CA0001E8 in segment: 0000000100000000000000CA
   LOG: Found WAL segment: /opt/uqbar/backup_dir/backups/instance1/RGOZF6/database/pg_xlog/0000000100000000000000CA
   LOG: Thread [0]: Opening WAL segment "/opt/uqbar/backup_dir/backups/instance1/RGOZF6/database/pg_xlog/0000000100000000000000CA"
   LOG: Found LSN: 0/CA0001E8
   LOG: finished streaming WAL at 0/CB000000 (timeline 1)
   LOG: Getting the Recovery Time from WAL
   LOG: Thread [0]: Opening WAL segment "/opt/uqbar/backup_dir/backups/instance1/RGOZF6/database/pg_xlog/0000000100000000000000CA"
   INFO: Syncing backup files to disk
   INFO: Backup files are synced, time elapsed: 1s
   INFO: Validating backup RGOZF6
   INFO: Backup RGOZF6 data files are valid
   INFO: Backup RGOZF6 resident size: 1337MB
   INFO: Backup RGOZF6 completed
   ```

4. 登录数据库，创建表warehouse_t1。

   ```sql
   [ommdoc@hostname]$ gsql -d postgres -p 28000
   gsql ((Uqbar 1.1.0 build 62408a0f) compiled at 2022-06-30 15:06:56 commit 0 last mr  )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.
   
   Uqbar=# CREATE TABLE warehouse_t1
   (
       W_WAREHOUSE_SK            INTEGER               NOT NULL,
       W_WAREHOUSE_ID            CHAR(16)              NOT NULL,
       W_WAREHOUSE_NAME          VARCHAR(20)                   ,
       W_WAREHOUSE_SQ_FT         INTEGER                    Uqbar-#    ,
       W_STREET_NUMBER           CHAR(10)                      ,
       W_STREET_NAME             VARCHAR(60)                   ,
       W_STREET_TYPE             CHAR(15)                      ,
       W_Uqbar(# SUITE_NUMBER            CHAR(10)                      ,
       W_CITY                    VARCHAR(60)                   ,
       W_COUNTY                  VARCHAR(30)                   ,
       W_STATE                   CHAR(2)     Uqbar(#                   ,
       W_ZIP                     CHAR(10)                      ,
       W_COUNTRY                 VARCHAR(20)                 Uqbar(#   ,
       W_GMT_OFFSET              DECIMAL(5,2)
   );
   CREATE TABLE
   Uqbar=# 
   ```

5. 创建指定实例的增量备份。

   **注意**：创建增量备份前，需在postgresql.conf中手动添加参数“enable_cbm_tracking = on”，并重启数据库确保参数生效。

   ```bash
   [ommdoc@hostname]$ gs_probackup backup -B /opt/uqbar/backup_dir --instance instance1 -b PTRACK
   INFO: Backup start, gs_probackup version: 2.4.2, instance: instance1, backup ID: RGOZKI, backup mode: PTRACK, wal mode: STREAM, remote: false, compress-algorithm: none, compress-level: 1
   LOG: Backup destination is initialized
   LOG: This openGauss instance was initialized with data block checksums. Data block corruption will be detected
   LOG: Database backup start
   LOG: Latest valid FULL backup: RGOZF6
   INFO: Parent backup: RGOZF6
   LOG: started streaming WAL at 0/CC000000 (timeline 1)
   INFO: Cannot parse path "base"
                                                                          [2022-08-16 13:07:30]: check identify system success
                                                                          [2022-08-16 13:07:30]: send START_REPLICATION 0/CC000000 success
                                                                          [2022-08-16 13:07:30]: keepalive message is received
                                                                          [2022-08-16 13:07:30]: keepalive message is received
   INFO: PGDATA size: 1320MB
   LOG: Current tli: 1
   LOG: Parent start_lsn: 0/CA000028
   LOG: start_lsn: 0/CC000028
   INFO: Extracting pagemap of changed blocks
   INFO: change bitmap start lsn location is 0/CA000028
   INFO: change bitmap end lsn location is 00000000/CC000028
   INFO: Pagemap successfully extracted, time elapsed: 0 sec
   INFO: Start transferring data files
   LOG: Creating page header map "/opt/uqbar/backup_dir/backups/instance1/RGOZKI/page_header_map"
   INFO: Data files are transferred, time elapsed: 0
   INFO: wait for pg_stop_backup()
   INFO: pg_stop backup() successfully executed
   LOG: stop_lsn: 0/CC0001E8
   LOG: Looking for LSN 0/CC0001E8 in segment: 0000000100000000000000CC
   LOG: Found WAL segment: /opt/uqbar/backup_dir/backups/instance1/RGOZKI/database/pg_xlog/0000000100000000000000CC
   LOG: Thread [0]: Opening WAL segment "/opt/uqbar/backup_dir/backups/instance1/RGOZKI/database/pg_xlog/0000000100000000000000CC"
   LOG: Found LSN: 0/CC0001E8
   LOG: finished streaming WAL at 0/CD000000 (timeline 1)
   LOG: Getting the Recovery Time from WAL
   LOG: Thread [0]: Opening WAL segment "/opt/uqbar/backup_dir/backups/instance1/RGOZKI/database/pg_xlog/0000000100000000000000CC"
   INFO: Syncing backup files to disk
   INFO: Backup files are synced, time elapsed: 0
   INFO: Validating backup RGOZKI
   INFO: Backup RGOZKI data files are valid
   INFO: Backup RGOZKI resident size: 273MB
   INFO: Backup RGOZKI completed
   ```

6. 删除数据库目录。

   ```bash
   [ommdoc@hostname]$ gs_ctl stop -D /opt/uqbar/data
   [2022-08-16 13:27:04.642][766772][][gs_ctl]: gs_ctl stopped ,datadir is /opt/uqbar/data 
   waiting for server to shut down.............. done
   server stopped
   [ommdoc@hostname]$ rm -rf /opt/uqbar/data
   ```

7. 恢复数据库。

   ```bash
   [ommdoc@uqbar-kernel-0003 data]$ gs_probackup restore -B /opt/uqbar/backup_dir --instance instance1 -D /opt/uqbar/data -i RGOZF6 
   LOG: Restore begin.
   LOG: there is no file tablespace_map
   LOG: check tablespace directories of backup RGOZF6
   LOG: check external directories of backup RGOZF6
   WARNING: Process 719280 which used backup RGOZF6 no longer exists
   INFO: Validating backup RGOZF6
   INFO: Backup RGOZF6 data files are valid
   LOG: Thread [1]: Opening WAL segment "/opt/uqbar/backup_dir/backups/instance1/RGOZF6/database/pg_xlog/0000000100000000000000CA"
   INFO: Backup RGOZF6 WAL segments are valid
   INFO: Backup RGOZF6 is valid.
   INFO: Restoring the database from backup at 2022-08-16 13:04:18+08
   LOG: there is no file tablespace_map
   LOG: Restore directories and symlinks...
   INFO: Start restoring backup files. PGDATA size: 1336MB
   LOG: Start thread 1
   INFO: Backup files are restored. Transfered bytes: 1336MB, time elapsed: 0
   INFO: Restore incremental ratio (less is better): 100% (1336MB/1336MB)
   INFO: Syncing restored files to disk
   INFO: Restored backup files are synced, time elapsed: 8s
   INFO: Restore of backup RGOZF6 completed.
   [ommdoc@hostname]$ gs_om -t restart
   Stopping cluster.
   =========================================
   Successfully stopped cluster.
   =========================================
   End stop cluster.
   Starting cluster.
   =========================================
   [SUCCESS] uqbar-kernel-0003
   2022-08-16 13:34:10.277 [unknown] [unknown] localhost 70371578937360 0[0:0#0] 0 [BACKEND] WARNING:  could not create any HA TCP/IP sockets
   2022-08-16 13:34:10.277 [unknown] [unknown] localhost 70371578937360 0[0:0#0] 0 [BACKEND] WARNING:  could not create any HA TCP/IP sockets
   2022-08-16 13:34:10.282 [unknown] [unknown] localhost 70371578937360 0[0:0#0] 0 [BACKEND] WARNING:  No explicit IP is configured for listen_addresses GUC.
   =========================================
   Successfully started.
   ```