---
title: 时序数据压缩
summary: 时序数据压缩
author: Zhang Cuiping
date: 2022-11-22
---

# 时序数据压缩

时序数据的价值密度低，对存储成本较敏感；且时序数据前后点的变化往往很小，比较易于压缩。因此，时序数据库需要支持时序数据的高压缩率。

相同数据集下，压缩率超越TimescaleDB至少20%。

## 压缩方式

Uqbar支持自动压缩和手动压缩两种方式。

### 自动压缩

系统会按照设定的压缩延迟时间，自动对时序数据执行行存转列存操作，列存数据存储时会对数据进行压缩，以节省存储空间。

压缩延迟时间可通过修改配置文件中的 GUC 参数 uqbar.timeseries_compression_delay 设置。该参数为实例级 GUC 参数，只可以由管理员修改，修改完成后执行 reload 参数生效。

#### 注意事项

- 压缩以分片组（ChunkGroup）为粒度进行，即一个 ChunkGroup 内的所有 chunk 一起执行压缩操作。如果有一个 chunk 执行转列失败或者不满足转列的条件，不执行本次压缩，ChunkGroup 内所有 chunk 保持为行存。
- 当达到设置的自动压缩间隔后，执行数据压缩还需要 ChunkGroup 内的 chunk 满足以下条件才可执行自动压缩：
  - chunk 所属的 ChunkGroup 的结束时间早于数据库系统当前时间。
  - 上次触发压缩以来 ChunkGroup 超过一个uqbar.timeseries_compression_delay 的间隔没有数据写入。
  - uqbar.timeseries_compression_delay 是一个取值为正的 interval 类型，不支持负值和零值，默认值是 2 hours。
  - 由于当前 OpenGauss 后台 job 触发机制的限制，设置的值小于 0.1s 时，触发的间隔不会精确到实际设置的值。后台 job 触发的间隔最小为 0.1s 。
  - 压缩成功后的 ChunkGroup 不支持写入数据。

#### 示例

假设前置条件：chunkGroupDuration = 1 week, 假设当前时间为2022-7-7 09:55:00

1. 设置自动压缩的延迟间隔为1 min：即 timeseries_compression_delay = '1 min';

2. 插入数据建立chunk（分片），保证分片的结束时间小于当前时间，例如

   ```sql
   INSERT INTO weather SELECT '2022-07-01 00:00:00'::timestamp, 'beijing', 'park', generate_series(1, 2000); 
   ```

3. 查看chunk属性，找到建立的分片。

   ```sql
   SELECT * FROM timeseries_catalog.tschunk;
   ```

4. 两分钟以后再次查看chunk属性，确认其是否已经被压缩。

### 手动压缩

Uqbar支持用户手动触发压缩。当自动压缩的策略还未触发时，可以通过手动压缩命令强制触发数据压缩，以提早节省存储空间。

#### 注意事项

- Uqbar 暂不支持分片压缩后写入数据，压缩后的分片无法执行写入操作。
- 不指定分区时会对时序表所有的 chunkgroup 执行强制压缩，指定分区时会对指定的 chunkgroup 执行强制压缩。

#### 语法格式

```sql
COMPRESS TIMESERIES table_name [ PARTITION chunkgroupname ]
```

#### 示例

```sql
MogDB=# COMPRESS TIMESERIES weather PARTITION chunkgroup_1;
COMPRESSED
MogDB=# COMPRESS TIMESERIES weather;
COMPRESSED
```

## 压缩率视图

Uqbar 提供压缩率视图，供用户查看时序表压缩前后的数据量、压缩率，以及表下每个chunkgroup的压缩前后数据量，压缩率。压缩率的计算方法是压缩前数据大小除以压缩后数据大小。

### 语法格式

```sql
SELECT * FROM timeseries_views.comression_table;
SELECT * FROM timeseries_views.compression_chunkgroup;
```

### 视图属性

timeseries_views 模式下的compression_table视图，供用户查看所有时序表的压缩率。after_compression_size和compression_ratio仅在时序表存在至少一个被压缩的chunkgroup时非空。当存在至少一个chunkGroup被压缩后，after_compression_size为未压缩的chunkGroup的before_compression_size与被压缩的chunkGroup的after_compression_size之和。

**表1**  comression_table 视图属性

| 属性                    | 类型   | 描述                           |
| ----------------------- | ------ | ------------------------------ |
| schemaname              | name   | 时序表的模式名。               |
| tablename               | name   | 时序表的表名。                 |
| before_compression_size | bigint | 时序表压缩前大小, 单位为KB。   |
| after_compression_size  | bigint | 时序表压缩后大小, 单位为KB。   |
| compression_ratio       | float  | 时序表压缩率（保留两位小数）。 |

timeseries_views模式下的compression_chunkgroup视图，供用户查看所有时序表分片的压缩率。after_compression_size和compression_ratio仅在chunkgroup被压缩后非空。

**表2** compression_chunkgroup 视图属性

| 属性                    | 类型   | 描述                               |
| ----------------------- | ------ | ---------------------------------- |
| schemaname              | name   | 时序表的模式名。                   |
| tablename               | name   | 时序表的表名。                     |
| chunkgroupname          | name   | 时序表的分片名。                   |
| before_compression_size | bigint | 时序表分片组压缩前大小，单位为KB。 |
| after_compression_size  | bigint | 时序表分片组压缩后大小，单位为KB。 |
| compression_ratio       | float  | 时序表分片组的压缩率。             |

### 示例

```sql
--查看时序表weather的压缩率
MogDB=# SELECT tablename, before_compression_size, after_compression_size, compression_rate FROM timeseries_views.compression_table WHERE tablename = 'weather';
 tablename | before_compression_size | after_compression_size | compression_ratio 
--------------+--------------------------------+------------------------------+----------------------
weather   |  1746534             |  1483457             |  1.17

(1 rows)

--查看时序表weather的分片压缩率
MogDB=# SELECT tablename, chunkname, before_compression_size, after_compression_size, compression_rate FROM timeseries_views.compression_tables WHERE tablename = 'weather';
 tablename | chunkname | before_compression_size | after_compression_size | compression_rate 
--------------+----------------+-------------------------------+------------------------------+----------------------
weather   |  p_1_1     | 387564              |  328576            |  1.17
weather   |  p_1_2     | 417829              |  358273            |  1.16

(2 rows)
```