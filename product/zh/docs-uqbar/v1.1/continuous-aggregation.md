---
title: 持续聚合
summary: 持续聚合
author: zhang cuiping
date: 2022-11-24
---

# 持续聚合

## 背景介绍

随着时间流逝，时序数据的价值越来越低，并且历史数据的分析往往只需要粗粒度的数据。为了降低存储成本，以及提高历史数据的查询效率，时序数据库需要支持持续聚合CA（Continuous Aggregation）能力，系统自动对历史数据进行降采样，同时降低存储成本。

## 创建持续聚合

用户可以在指定数据库下创建持续聚合任务，指定持续聚合的SQL语句，定期执行时间，以及生命周期策略。系统自动按照时间点周期执行持续聚合，对原始时序数据进行各类降采样聚合运算，并将结果保存到持续聚合表中。

### 注意事项

- 持续聚合的group by子句中只能使用time_bucket，不能使用。time_bucket_gapfill函数。

- 在GROUP BY 子句中必须存在time_bucket函数。

- select_query子句的from语句中只能包含一个时序表。

- resample_interval 最小为 1s。

### 语法格式

```sql
CREATE CONTINUOUS aggregation [ IF NOT EXISTS ] conagg_name 
[ (column_name..) ] 
INTERVAL resample_interval
[ POLICY policy_name ]
AS <select_query> ;
```

- resample_interval 指持续聚合自动重新采样的时间间隔。
- 其中POLICY与数据表使用相同体系的策略，如果不指定会使用默认策略。

- 其中*<select_query>*格式为：

  ```sql
  SELECT <grouping_exprs>, <aggregate_functions>
  FROM timeseries_table_name
  [WHERE ... ]
  GROUP BY time_bucket( <const_value>, <time_col> ),
           [ optional grouping exprs>]
  [HAVING ...]
  ```

### 示例

```sql
MogDB=# CREATE CONTINUOUS AGGRETATION conagg_test(time, city, avgtemp)
INTERVAL '2 hours'
POLICY default_ca_policy
AS
SELECT time_bucket('1 days',  time) AS timeb, city, avg(temperature )
FROM table_name
GROUP BY timeb, city;
CREATE CONTINUOUS AGGRETATION
```

## 修改持续聚合

用户可以修改已创建的持续聚合，定期执行时间、数据保留策略等。变更对已存在的持续聚合数据结果不产生影响，但后续按照新的数据保留策略进行持续聚合。

### 注意事项

- 只有对持续聚合当前操作，例如查询、刷新、清理等完成后 ，才可变更持续聚合。
- 修改的参数满足创建持续聚合时的约束，如 resample_interval 大于等于 1s。

### 语法格式

```sql
ALTER CONTINUOUS aggregation [ IF EXISTS ] conagg_name action
```

其中action包含：

```sql
| RENAME TO new_conagg_name
| ALTER INTERVAL TO resample_interval
| ALTER POLICY TO policy_name
```

### 示例

```sql
MogDB=# ALTER CONTINUOUS AGGRETATION conagg_test ALTER INTERVAL TO '2 hours';
ALTER CONTINUOUS AGGRETATION
```

## 删除持续聚合

用户可以删除已创建的持续聚合，删除后其job也会同步删除。

### 语法格式

```sql
DROP CONTINUOUS aggregation [ IF EXISTS ] conagg_name
```

### 示例

```sql
MogDB=# DROP CONTINUOUS AGGRETATION conagg_test;
DROP CONTINUOUS AGGRETATION
```

## 手动执行持续聚合

### 语法格式

```sql
RESAMPLE CONTINUOUS aggregation <conagg_name>
```

### 示例

```sql
MogDB=# RESAMPLE CONTINUOUS AGGRETATION conagg_test;
RESAMPLE CONTINUOUS AGGRETATION
```

## 持续聚合视图

Uqbar 提供持续聚合视图，可以查看所有持续聚合任务。

### 语法格式

```
SELECT * FROM timeseries_views.continuous_aggregation;
```

### 视图属性

timeseries_views模式下包含continuous_aggregation视图，供用户查看所有持续聚合。

| 属性              | 类型      | 描述                            |
| ----------------- | --------- | ------------------------------- |
| schemaname        | name      | 持续聚合的模式名。              |
| caname            | name      | 持续聚合的名字。                |
| resample_interval | interval  | 持续聚合自动重采样的间隔 。     |
| resampletime      | timestamp | 持续聚合的上次刷新时间。        |
| query             | text      | 持续聚合采样时使用的query语句。 |

### 示例

```sql
MogDB=# SELECT * FROM timeseries_views.continuous_aggregation;
     schemaname      |   caname  |  resample_interval|  resampletime      | query
--------------------------------+----------------+-----------------------+----------------------------+-----------------
 public                |  CA_1    |  7 days          | '2022-07-10 00:00:00' |  SELECT time_bucket('1 days',  time) AS timeb, city, avg(temperature ) FROM table_name GROUP BY timeb,  city;

(1 row)
```