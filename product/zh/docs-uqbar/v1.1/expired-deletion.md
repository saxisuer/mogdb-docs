---
title: 时序数据过期删除
summary: 时序数据过期删除
author: Guo Huan
date: 2022-11-23
---

# 时序数据过期删除

时序数据具有一定的时效性，早期的数据基本没有价值，时序数据库需要支持对时序数据进行生命周期管理，能够高效地删除过期历史数据。

<br/>

## 自动删除过期数据

当时序数据的时间早于保留策略中设置的过期删除时间（即chunkGroup的boundaries<=当前时间-duration），系统会自动删除过期数据。时序数据的时间以时序数据的Time属性列为准，以ChunkGroup为单位进行删除，也就是说，需要整个ChunkGroup中所有数据都过期后才会删除，有一定的滞后性。自动删除过期分片组的job会以chunkGroupDuration为间隔启动运行。

当创建时序表时，如保留策略的duration非0，将会创建一个自动删除过期数据的job。对于duration为0的时序表，更换保留策略或更新保留策略后duration如果非0，将会创建一个自动删除过期数据的job。如存在自动删除过期数据的job，其执行间隔为时序表所使用的保留策略的chunkGroupDuration。如果时序表使用的保留策略的duration由非0转为0，则会删除该时序表的自动删除过期数据job。

### 约束

删除分片组的判断依靠于分片组的结束时间和当前服务器时间的比较，当分片组的结束时间<=当前服务器时间-duration时，满足删除条件（当duration为0时，该分片组不会过期，不满足删除条件），删除分片组的操作为对整个分片组的drop操作。

<br/>

## 自动删除持续聚合数据

由持续聚合产生的数据，可以指定和原表不一样的保留策略，持续聚合按照自己的策略进行过期删除。持续聚合本身数据也是通过另一张时序表存储，只是保留策略可能和原表不一样。

### 约束

与自动删除过期数据一致。

<br/>

## 手动删除分片组

通过手动删除过期数据函数可删除时序表分片组。

### 示例

```sql
SELECT * FROM timeseries_catalog.drop_ts_outdate_cg(‘weather’, now());
SELECT * FROM timeseries_catalog.drop_ts_outdate_cg(‘public.weather’, now()+ ‘1 h’::interval);
NOTICE:  timeseries table weather outdate boundary is 2022-10-31 12:58:01.641916+08
NOTICE:  chunkgroup 24773 boundary is 2022-07-27 04:00:00+08
NOTICE:  chunkgroup 24773 with boundary(2022-07-27 04:00:00+08) is out of date.
NOTICE:  drop chunkgroup 24773 of rel 24760
NOTICE:  chunkgroup 24780 boundary is 2025-10-18 09:00:00+08
NOTICE:  chunkgroup 24787 boundary is 2025-10-18 11:00:00+08
NOTICE:  chunkgroup 24794 boundary is 2025-10-18 12:00:00+08
NOTICE:  chunkgroup 24801 boundary is 2030-07-28 06:00:00+08
```

对于duration非0的时序表，首先通过NOTICE信息输出以带时区时间表示的时序表的过期边界，此边界为函数的第二个参数duration。随后NOTICE信息将输出每个分片组的边界，如该分片组的boundary<=时序表的过期边界，将通过NOTICE提示该分片组已过期，删除成功将显示NOTICE信息提示该分区已删除。

### 约束

此函数仅能用于删除时序表分片组。此函数接收两个参数，第一个参数传入时序表的表名，第二个参数接收一个带时区的时间，此时间将用于判断时序表分片组是否过期。对于时序表下每个分片组，如果此时间-duration>=boundary且duration不等于0，将会删除该分片组，在任何时间都不会删除duration为0的时序表分片组。
