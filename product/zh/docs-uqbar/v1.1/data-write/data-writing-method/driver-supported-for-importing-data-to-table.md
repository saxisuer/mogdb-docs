---
title: 支持驱动写入时序表
summary: 支持驱动写入时序表
author: Zhang Cuiping
date: 2022-11-zh
---

# 支持驱动写入时序表

传感器或监控数据支持使用 Libpq、JDBC、ODBC、GO 以及 Python 驱动写入时序表。一次可插入单条或多条数据。写入数据前请先创建时序表，或确保系统已存在时序表。

## 注意事项

- 支持Libpq、JDBC 和 GO的prepare和COPY语义，向时序表插入数据。
- 支持ODBC的prepare语义，向时序表插入数据。

## 语法格式

- Libpq 驱动

  ```
  PQprepare
  PQexecPrepared
  PQputCopyData
  ```

- JDBC 驱动

  ```sql
  java.sql.PreparedStatement
  CopyManager
  ```

- ODBC 驱动

  ```
  SQLPrepare
  SQLExecute
  --不支持COPY
  ```

- GO 驱动

  ```
  Prepare()
  Exec()
  ```

## 示例

- JDBC prepare接口：[https://docs.mogdb.io/zh/mogdb/v3.0/5-java-sql-PreparedStatement](https://docs.mogdb.io/zh/mogdb/v3.0/5-java-sql-PreparedStatement)

  JDBC prepare示例：[https://docs.mogdb.io/zh/mogdb/v3.0/7-running-sql-statements](https://docs.mogdb.io/zh/mogdb/v3.0/7-running-sql-statements)

- JDBC COPY接口：[https://docs.mogdb.io/zh/mogdb/v3.0/14-CopyManager](https://docs.mogdb.io/zh/mogdb/v3.0/14-CopyManager)

  JDBC COPY示例(非官方)：[https://blog.csdn.net/ifenggege/article/details/108905808](https://blog.csdn.net/ifenggege/article/details/108905808)

- Libpq prepare接口：[https://docs.mogdb.io/zh/mogdb/v3.0/11-PQprepare](https://docs.mogdb.io/zh/mogdb/v3.0/11-PQprepare)

  Libpq prepare示例(非官方)：[https://www.cnblogs.com/gaojian/p/3140491.html](https://www.cnblogs.com/gaojian/p/3140491.html)

- Libpq COPY接口(非官方)：[http://www.postgres.cn/docs/10/libpq-copy.html](http://www.postgres.cn/docs/10/libpq-copy.html)

  Libpq COPY示例(非官方)：[https://blog.csdn.net/liufeng1980423/article/details/108254469](https://blog.csdn.net/liufeng1980423/article/details/108254469)

- GO prepare示例(非官方)：[https://vimsky.com/examples/usage/golang_database_sql_DB_Prepare.html](https://vimsky.com/examples/usage/golang_database_sql_DB_Prepare.html)

- GO COPY示例(非官方)：[https://pkg.go.dev/github.com/lib/pq#section-readme](https://pkg.go.dev/github.com/lib/pq#section-readme)