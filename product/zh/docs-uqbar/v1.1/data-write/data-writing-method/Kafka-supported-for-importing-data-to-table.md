---
title: 支持从Kafka写入时序表
summary: 支持从Kafka写入时序表
author: Zhang Cuiping
date: 2022-11-zh
---

# 支持从Kafka写入时序表

传感器或监控数据支持从Kafka写入时序表。Kafka和Uqbar可通过Uqbar Kafka Connector进行连接。该连接器负责从Kafka读取数据，然后将消息中的SQL发送到Uqbar执行并记录失败消费结果，维护消息消费位置。向数据库插入数据需要使用批量插入模式。
