---
title: 支持标准SQL写入时序表
summary: 支持标准SQL写入时序表
author: Zhang Cuiping
date: 2022-11-zh
---

# 支持标准SQL写入时序表

传感器或监控数据支持使用标准SQL写入时序表。一次可插入单条或多条数据。写入数据前请先创建时序表，或确保系统已存在时序表。

## 注意事项

- 支持不带时间值插入。
- 支持乱序插入（本版本支持未压缩 chunk 的乱序写入）。
- tstime 列不能写入 NULL 值。
- tstag 不能写入 NULL 值。

## 语法格式

- 插入单条数据

  ```sql
  INSERT INTO table_name[(targelist)] VALUES(value_list);
  ```

- 插入多条数据

  ```sql
  INSERT INTO table_name[(targelist)]  VALUES(value_list)[, (value_list)]...;
  ```

## 示例

```sql
MogDB=# INSERT INTO weather(time, city, location, temperature) VALUES('2022-07-06 00:00:00','beijing', 'gongyuan', 29.1);
INSERT 0 1
MogDB=# INSERT INTO weather(time, city, location, temperature) VALUES('2022-07-06 00:00:00','beijing', 'chaoyang', 29.1), ('2022-7-6 00:00:00','shanghao', 'gongyuan', 29.2);
INSERT 0 2
```