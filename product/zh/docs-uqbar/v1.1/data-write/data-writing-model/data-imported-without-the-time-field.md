---
title: 无时间值写入
summary: 无时间值写入
author: Zhang Cuiping
date: 2021-11-23
---

# 无时间值写入

Uqbar支持无时间值的时序数据写入，对无时间值的时序数据以服务端系统时间作为 time 列的值。当插入语法中没有指定插入时间时，数据库使用插入时的数据库时间补录time列。

默认 timestamp 的精度为ms（秒级单位小数点后6位），用户也可以使用如 now()::timestamp(x) 这样的语法指定小数点后保留几位。（timestamp(3) 表示小数点后保留3位）。

## 语法格式

```
INSERT INTO table VALUES [, ...];
```

## 示例

```sql
MogDB=# INSERT INTO weather(city, location, temperature) VALUES('beijing', 'chaoyang', 29.1);
INSERT 0 1
```