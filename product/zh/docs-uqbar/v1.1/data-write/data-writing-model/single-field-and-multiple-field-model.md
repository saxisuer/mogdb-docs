---
title: 单值模型和多值模型
summary: 单值模型和多值模型
author: Zhang Cuiping
date: 2021-11-23
---

# 单值模型和多值模型

Uqbar支持单值和多值模型写入。单值模型指创建时序表时，只有一个非TSTIME非TSTAG的字段列，多值模型指创建时序表时有多个非TSTIME非TSTAG的字段列。

## 语法格式

语法格式支持 insert 和 copy。

```
INSERT INTO table_name[(targelist)]  VALUES(value_list)[, (value_list)]...;
```

```
COPY table_name [ ( column_name [, ...] ) ] 
    FROM { 'filename' | STDIN }
    [ [ USING ] DELIMITERS 'delimiters' ]
    [ WITHOUT ESCAPING ]
    [ LOG ERRORS ]
    [ REJECT LIMIT 'limit' ]
    [ WITH ( option [, ...] ) ]
    | copy_option
    | TRANSFORM  ( { column_name [ data_type ] [ AS transform_expr ] } [, ...] )
    | FIXED FORMATTER ( { column_name( offset, length ) } [, ...] ) [ ( option [, ...] ) | copy_option [  ...] ] ];
```

## 示例

- 单值模型表创建和插入

  ```sql
  MogDB=# CREATE TIMESERIES TABLE weather(time timestamp TSTIME, city text TSTAG, location text TSTAG, temperature float) POLICY default_policy;
  CREATE TIMESERIES TABLE
  MogDB=# INSERT INTO weather VALUES('2022-06-18 00:00:00', 'beijing', 'park', 36);
  INSERT 0 1
  ```

- 多值模型表创建和插入

  ```sql
  MogDB=# CREATE TIMESERIES TABLE weather(time timestamp TSTIME, city text TSTAG, location text TSTAG, temperature float, humidity float) POLICY default_policy;
  CREATE TIMESERIES TABLE
  MogDB=# INSERT INTO weather VALUES('2022-06-18 00:00:00', 'beijing', 'park', 36, 2.1);
  INSERT 0 1
  ```