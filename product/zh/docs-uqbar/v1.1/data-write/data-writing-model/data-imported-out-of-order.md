---
title: 乱序写入
summary: 乱序写入
author: Zhang Cuiping
date: 2021-11-23
---

# 乱序写入

Uqbar支持数据乱序写入。通常情况下上报到 Uqbar 的数据是按照时间顺序有序排列的，也就是后写入的数据 time 列的值大于先写入的数据。但是由于各种原因，例如网络或者消息队列的问题，可能导致后写入的数据 time 列的值小于先写入的数据，这种现象称为乱序。时序数据库中的乱序，通常是针对同一个采集器而言，即相同标签集的数据。

说明：Uqbar 支持未压缩数据的乱序写入，暂不支持压缩后数据的乱序写入。

## 语法格式

```
INSERT INTO table VALUES [, ...];
```

## 示例

```sql
MogDB=# CREATE TIMESERIES TABLE weather(time timestamp TSTIME, city text TSTAG, location text TSTAG, temperature float) POLICY default_policy;
CREATE TIMESERIES TABLE
MogDB=# INSERT INTO weather VALUES('2022-06-18 00:00:00', 'beijing', 'park', 36);
INSERT 0 1
MogDB=# INSERT INTO weather VALUES('2022-06-17 12:00:00', 'beijing', 'park', 30);
INSERT 0 1
```