---
title: 时序数据保留策略
summary: 时序数据保留策略
author: Guo Huan
date: 2022-11-23
---

# 时序数据保留策略

Uqbar支持用户自定义时序数据保留策略 (Retention Policy)，如过期时间、时间分片粒度。时序数据库保留策略归属于某个数据库，一个数据库下可以定义多个保留策略并指定其中一个为数据库的默认策略。如果创建时序表时未指定保留策略，则使用所属数据库的默认保留策略。

<br/>

## 创建时序数据保留策略

Uqbar支持在数据库下创建时序数据保留策略，策略内容包括：

1. 数据保存时长，超出保留时长的数据会自动删除；

2. 时序数据按时间分片粒度，时序数据按照分片进行删除；

3. 保留策略的chunkGroupDuartion需要对齐。

### 语法描述

```sql
CREATE TIMESERIES POLICY [ IF NOT EXISTS ] policy_name WITH (option = value, [...] )
```

其中option包含：

- duration：时序数据保留时间，超出这个时间的历史数据自动删除，删除以Chunk Group粒度为单位。取值范围 >= 1h。特殊值0表示数据永久保留，此时时间单位支持second、minute、hour、day、week、month、year。

- chunkGroupDuration：指定时序表的Chunk Group时间跨度，不支持设为特殊值0。

  如果未指定本参数，系统自动根据duration参数设置合适的Chunk Group Duration参数，规则如下

  - duration < 2天，chunkGroupDuration设置为1小时。chunkGroup 时间对齐到一小时，即chunkGroup的起止时间应该是整点，如 '2020-07-10 00:00:00'、'2020-07-10 01:00:00'，而不会出现 '2020-07-10 00:10:00'、'2020-07-10 00:10:10' 这样的时间点。
  - duration <= 6个月，chunkGroupDuration设置为1天。chunkGroup对齐到天，如 '2020-07-10 00:00:00'、'2020-07-11 00:00:00';
  - duration > 6个月，chunkGroupDuration设置为7天。chunkGroup对齐到周，即chunkGroup的起始时间对齐到当周的周一。
  - duration > 3 年，chunkGroupDuration设置为 1个月。chunkGroup对齐到月，即chunkGroup的起始时间对齐到当月的一号。

duration和chunkGroupDuration数字部分的上限为2<sup>31</sup>。

数据库初始化时会创建默认策略，默认数据表的策略名为'infinity'，其设置为chunkGroupDuration='1 month',  duration='0s'。Interval为0时，查询展示结果显示为'00:00:00'，表示永久保存。

### 约束

- 策略名不能与已存在的策略名相同。

- duration不能小于chunkGroupDuration。

- duration必须指定，chunkGroupDuration如未指定则按照规则生成。

- duration和chunkGroupDuration只能使用'数字+时间单位'的方式。如果用户输入的数字不是整数，会对其向上取整，例如1.2 week会取整为2 weeks。

- duration和chunkGroupDuration不能是负数，duration和chunkGroupDuration支持设置的最小时间长度为1 hour（非0且小于1 hour的时间向上取整后为1 hour）。

- duration和chunkGroupDuration支持设置的单位包括：小时(hour)、天(day)、周(week)、月(month)、年(year)，不支持其他时间单位。

- duration和chunkGroupDuration只支持到double类型能表示的精度。

- 每种时间单位的取值范围限制如下：

  | 单位 | 下限 | 上限       |
  | ---- | ---- | ---------- |
  | 年   | 0    | 296533     |
  | 月   | 0    | 3558399    |
  | 周   | 0    | 15250284   |
  | 天   | 0    | 106751991  |
  | 小时 | 0    | 2147483647 |

### 示例

```sql
MogDB=# CREATE TIMESERIES POLICY policy_test WITH (duration = '2 years', chunkGroupDuration='7 days');
CREATE POLICY
```

<br/>

## 修改时序数据保留策略

修改已创建的时序数据保留策略。

### 语法描述

```sql
ALTER TIMESERIES POLICY [IF EXISTS] policy_name actions
```

其中action包含：

- RENAME TO policy_name

- SET ( option = value, [...] ) 

其中option包含：

- { duration, chunkGroupDuration }

### 约束

- 默认策略可以改名，也可以改option；

- 被时序表使用的策略也可以被修改；

- duration和chunkGroupDuration需要满足创建时序保留策略时的约束。

### 示例

```sql
MogDB=# ALTER TIMESERIES POLICY policy_test SET( chunkGroupDuration='1 month');
ALTER POLICY
MogDB=# ALTER TIMESERIES POLICY policy_test RENAME TO policy_1;
ALTER POLICY
```

<br/>

## 删除时序数据保留策略

删除已创建的保留策略，删除时如果时序策略正在被时序表使用，那么删除操作失败。

### 语法描述

```sql
DROP TIMESERIES POLICY [IF EXISTS] policy_name
```

可以用于删除一个或多个timeseries policy。

如果没有IF EXISTS选项，且被删除的策略列表中中有不存在的策略，则执行报错，给出对应的错误提示，不执行任何策略的删除。

如果含有IF EXISTS选项，且被删除的策略列表中有不存在的策略，则执行跳过不存在的策略，并给出对应的提示信息，存在的策略执行删除操作。

### 约束

- 如果对应的保留策略被时序表使用，无法删除。

- 不能删除默认策略。

### 示例

```sql
MogDB=# DROP TIMESERIES POLICY policy1;
DROP POLICY
```

<br/>

## 设置默认策略

系统为每个数据库提供一个默认策略，用户也可以重新设置某个策略为默认策略，创建时序表时如果未指定具体策略，则使用所属数据库的默认保留策略。

### 语法描述

```sql
ALTER TIMESERIES POLICY policy_name DEFAULT;
```

### 示例

```sql
MogDB=# ALTER TIMESERIES POLICY policy1 DEFAULT;
ALTER TIMESERIES POLICY
```

<br/>

## 查看所有策略

支持查看指定数据库下的所有保留策略。timeseries_views这个schema下提供了policies视图，供用户查看所有时序表策略。

timeseries_views.policies视图属性：

| 属性               | 类型     | 描述                                                       |
| ------------------ | -------- | ---------------------------------------------------------- |
| policyname         | name     | 时序策略名                                                 |
| duration           | interval | 时序表数据的保留时长，特殊值00:00:00表示时序表数据永久保留 |
| chunkGroupDuration | interval | 时序表的时间分组跨度                                       |
| default            | bool     | 时序策略是否为默认策略                                     |

### 语法描述

```sql
SELECT * FROM timeseries_views.policies;
```

### 示例

```sql
MogDB=# SELECT * FROM timeseries_views.policies;
     policyname       |   ttl       |  duration  |  default
--------------------------------+----------------+----------------+--------------
 default_policy       |  infinity   |  7 days    |   t
 policy_test          |   2 years   |  7 days    |   f

(2 rows)
```

<br/>

## 后台任务视图

Uqbar提供后台任务视图，供用户查看定期删除、压缩等后台任务的运行状况。timeseries_views这个schema下提供了bgw_job图，供用户查看所有时序表策略。

timeseries_views.bgw_job视图属性：

| 属性          | 类型      | 描述                                                         |
| ------------- | --------- | ------------------------------------------------------------ |
| objname       | name      | 任务所属的时序表的名字                                       |
| type          | name      | 任务的类型(r = retension, c = compression, s = continuous aggregation) |
| status        | name      | 当前任务的执行状态。(running, finished, failed, disable)     |
| interval      | text      | 用来计算下次作业运行时间的时间表达式，可以是interval表达式，也可以是sysdate加上一个numeric值（例如：sysdate+1.0/24）。如果为空值或字符串"null"表示只执行一次，执行后Job状态STATUS变成’d’不再执行。 |
| next_run_date | timestamp | 任务下次执行的时间，精确到毫秒                               |
| last_run_date | timestamp | 任务上次执行的时间，精确到毫秒                               |
| last_end_date | timestamp | 任务上次结束的时间，精确到毫秒                               |
| failure_count | smallint  | 失败计数，任务连续失败16次后不再执行                         |

### 语法描述

```sql
SELECT * FROM timeseries_views.bgw_job;
```

### 示例

```sql
MogDB=# SELECT objname, type, status, interval, next_run_date, last_run_date FROM timeseries_views.bgw_job;
  objname  |   type      |   status   |  interval |   next_run_date    |  last_run_date
--------------------------------+----------------+----------------
  weather  | compression |  finished  |  2 hours  | 2022-6-16 14:00:05 |  2022-6-16 12:00:00 
  weather  | retension   |  finished  |  2 hours  | 2022-6-16 13:00:04 |  2022-6-16 11:00:00
(2 rows)
```
