<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# Uqbar Documentation 1.1.0

## Documentation List

+ 关于Uqbar
  + [什么是时序数据](/about-uqbar/what-is-time-series-data.md)
  + [Uqbar简介](/overview.md)
  + [发行说明](/about-uqbar/release-note.md)
+ [Uqbar安装](/ptk-based-installation.md)
+ [Uqbar管理](/uqbar-management.md)
+ [时序数据保留策略](/retention-policy.md)
+ [时序表管理](/time-series-table-management.md)
+ 时序数据写入
  + [简介](/data-write/data-write-introduction.md)
  + 时序数据接入方式
    + [支持标准SQL写入时序表](/data-write/data-writing-method/SQL-supported-for-importing-data-to-table.md)
    + [通过驱动写入时序数据](/data-write/data-writing-method/driver-supported-for-importing-data-to-table.md)
    + [从Kafka消费数据](/data-write/data-writing-method/Kafka-supported-for-importing-data-to-table.md)
  + 时序数据写入模型
    + [单值模型和多值模型](/data-write/data-writing-model/single-field-and-multiple-field-model.md)
    + [乱序写入](/data-write/data-writing-model/data-imported-out-of-order.md)
    + [无时间值写入](/data-write/data-writing-model/data-imported-without-the-time-field.md)
  + [时序数据写入性能指标](/data-write/data-write-performance-indicator.md)
+ [时序数据压缩](/data-compression.md)
+ [持续聚合](/continuous-aggregation.md)
+ [时序数据过期删除](/expired-deletion.md)
+ [数据查询能力](/query-data.md)
+ [集群管理](/cluster-management.md)
+ [备份恢复](/backup-restore.md)
+ [安全性](/security.md)
+ SQL语法参考
  + [ALTER TIMESERIES TABLE](/uqbar-sql-syntax/ALTER-TIMESERIES-TABLE.md)
  + [COMMENT](/uqbar-sql-syntax/COMMENT.md)
  + [CONNECT BY](/uqbar-sql-syntax/CONNECT-BY.md)
  + [COPY](/uqbar-sql-syntax/COPY.md)
  + [CREATE MATERIALIZED VIEW](/uqbar-sql-syntax/CREATE-MATERIALIZED-VIEW.md)
  + [CREATE PROCEDURE](/uqbar-sql-syntax/CREATE-PROCEDURE.md)
  + [CREATE RULE](/uqbar-sql-syntax/CREATE-RULE.md)
  + [CREATE SEQUENCE](/uqbar-sql-syntax/CREATE-SEQUENCE.md)
  + [CREATE SYNONYM](/uqbar-sql-syntax/CREATE-SYNONYM.md)
  + [CREATE TABLE AS](/uqbar-sql-syntax/CREATE-TABLE-AS.md)
  + [CREATE TIMESERIES TABLE](/uqbar-sql-syntax/CREATE-TIMESERIES-TABLE.md)
  + [CREATE VIEW](/uqbar-sql-syntax/CREATE-VIEW.md)
  + [CURSOR](/uqbar-sql-syntax/CURSOR.md)
  + [DROP TIMESERIES TABLE](/uqbar-sql-syntax/DROP-TIMESERIES-TABLE.md)
  + [EXPLAIN](/uqbar-sql-syntax/EXPLAIN.md)
  + [GRANT](/uqbar-sql-syntax/GRANT.md)
  + [INSERT](/uqbar-sql-syntax/INSERT.md)
  + [LOCK](/uqbar-sql-syntax/LOCK.md)
  + [SELECT](/uqbar-sql-syntax/SELECT.md)
  + [SELECT-INTO](/uqbar-sql-syntax/SELECT-INTO.md)
  + [VACUUM](/uqbar-sql-syntax/VACUUM.md)
+ [术语](/glossary.md)