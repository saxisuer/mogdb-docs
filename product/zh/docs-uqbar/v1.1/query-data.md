---
title: 时序数据查询能力
summary: 时序数据查询能力
author: Guo Huan
date: 2022-11-24
---

# 时序数据查询能力

时序数据的价值往往是通过对时序数据进行各种聚合分析来体现，对外呈现出某种变化趋势或分布特征。因此时序数据库需要支持丰富的多维聚合能力，以满足时序场景的分析需求。

## 聚合类算子

常用算子支持计划如下：

| 聚合函数            | 用途                   | 是否支持 | 备注                                                         |
| ------------------- | ---------------------- | -------- | ------------------------------------------------------------ |
| COUNT               | 统计数量               | 是       | 继承MogDB                                                    |
| DISTINCT            | 去重                   | 是       | 继承MogDB                                                    |
| INTEGRAL            | 求积分                 | 否       | 暂不支持                                                     |
| AVG                 | 平均值                 | 是       | 继承MogDB使用avg函数替代                                     |
| MEDIAN              | 求中位数               | 是       | 继承MogDB                                                    |
| MODE() WITHIN GROUP | 求最大频次数           | 是       | MogDB存在`mode() WITHIN GROUP (ORDER BY i)`语法              |
| SPREAD              | 求最大值与最小值的差值 | 否       | 可以使用`max(i)-min(i)`                                      |
| STDDEV              | 求标准差               | 是       | 继承MogDB                                                    |
| SUM                 | 求和                   | 是       | 继承MogDB                                                    |
| TIME_BUCKET         | 时间对齐               | 新增支持 | 时间桶算子，支持将时序数据按照时间桶粒度聚合。               |
| TIME_BUCKET_GAPFILL | 带有填充的时间对齐     | 新增支持 | 在time_bucket算子基础上，支持自动填充缺失的值。              |
| SUM...OVER...       | 累积和                 | 是       | MogDB存在`sum(i) over (order by s,i)`语法                    |
| AVG...OVER          | 移动平均               | 是       | MogDB存在`SELECT i,avg(i) OVER ( ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING) FROM t1;`语法 |

### time_bucket()

time_bucket时间范围对齐函数，用于将一个时间按照bucket_width对齐到一个时间点，方便后续对时间聚合。

#### 语法

```sql
time_bucket(bucket_width, time,[offset | origin])
```

#### 参数说明

| 参数名       | 类型      | 属性 | 释义                                                         |
| ------------ | --------- | ---- | ------------------------------------------------------------ |
| bucket_width | TEXT      | 必选 | 对齐的时间间隔。只能使用'正整数+时间标识'的方式，时间标识包括interval类型支持的所有单位，包括'microsecond,millisecond,second,minute,hour,day,week,month,year,decade,century,millennium' 及这些单位的复数形式，如years、months、days等和简写形式，如y、m、d等。 |
| time         | TIMESTAMP | 必选 | 需要对齐的时间。                                             |
| offset       | INTERVAL  | 可选 | 使用这个值调整对齐窗口位置，如果是正值，那么时间窗口整体向将来移动，如果是负值那么窗口位置向过去移动。相当于将默认的origin移动offset的位置。 |
| origin       | TIMESTAMP | 可选 | 对齐的base，默认从’2001-1-1 00:00:00’对齐；此参数与offset互斥。 |

#### 约束

使用time_bucket时，由于offset与origin是互斥的参数，所以time_bucket的第三个参数可能是offset (interval 类型)，也可能是origin (timestamp 类型)。为了方便区分是offset还是origin使用时，如果使用offset或者origin需要显式指定第三个参数的类型，如：

```sql
select time_bucket('5minute',time,'2001-1-1 00:00:00'::timestamp) as bucket ,count(temperature) from weather group by bucket order by bucket asc;
```

或者

```sql
select time_bucket('1minute',time,'1minute'::interval) as bucket ,count(temperature) from weather group by bucket order by bucket asc;
```

#### 示例

```sql
MogDB=#  select * from t1 order by time;
          time          | id | value 
------------------------+----+-------
 2022-06-02 00:01:00+08 |  1 |     9
 2022-06-02 00:04:00+08 |  1 |    11
 2022-06-02 00:06:00+08 |  1 |    10
 2022-06-02 00:07:00+08 |  1 |    11
 2022-06-02 00:15:00+08 |  1 |    12
(5 rows)

MogDB=# select time_bucket('2 minute',time) as bucket, sum(value)  from t1 group by bucket order by bucket; 
          bucket         | sum 
------------------------------------+-----
 2022-06-02 00:00:00+08   |   9
 2022-06-02 00:04:00+08   |  11
 2022-06-02 00:06:00+08   |  21
 2022-06-02 00:14:00+08   |  12
(4 rows)
```

### time_bucket_gapfill()

在time_bucket的基础上增加了填充缺失值的功能。time_bucket默认只对时间列进行填充，不对聚合结果进行填充。time_bucket_gapfill的作用是按照一定的规则，填充聚合结果的值。

假设有6条时序数据，如下所示：

| time | 00:00:00 | 00:01:00 | 00:15:00 | 00:36:00 | 00:45:00 | 00:46:00 |
| ---- | -------- | -------- | -------- | -------- | -------- | -------- |
| temp | 30       | 31       | 32       | 33       | 34       | 35       |

现在计算每 10 分钟temp的平均值，计算结果如下：

| time | 00:00:00 | 00:10:00 | 00:20:00 | 00:30:00 | 00:40:00 |
| ---- | -------- | -------- | -------- | -------- | -------- |
| avg  | 30.5     | 32       |          | 33       | 34.5     |

由于[00:20:00, 00:29:00 ] 这个时间区间内没有采集值上报，因此无法计算这个时间区间内的平均值。time_bucket_gapfill的作用是按照一定规则填充没有值的时间区间，使得这些区间内有聚合的结果值。例如，使用上一个时间区间内聚合结果值作为本区间的填充值，则聚合后的结果如下：

| time | 00:00:00 | 00:10:00 | 00:20:00 | 00:30:00 | 00:40:00 |
| ---- | -------- | -------- | -------- | -------- | -------- |
| avg  | 30.5     | 32       | 32       | 33       | 34.5     |

#### 语法

```sql
time_bucket_gapfill(bucket_width, time, start , finish)
```

#### 参数说明

| 参数名       | 类型      | 属性 | 释义                                                         |
| ------------ | --------- | ---- | ------------------------------------------------------------ |
| bucket_width | TEXT      | 必选 | 对齐的时间间隔。只能使用'正整数+时间标识'的方式，时间标识包括interval类型支持的所有单位，包括'microsecond,millisecond,second,minute,hour,day,week,month,year,decade,century,millennium' 及这些单位的复数形式，如years、months、days等和简写形式，如y、m、d等。 |
| time         | TIMESTAMP | 必选 | 需要对齐的时间。                                             |
| start        | TIMESTAMP | 可选 | 需要gapfill的开始的时间（例如结果集中没有某个时间，但是需要查询结果中有这个时间）。如果不指定此参数需要在where条件中指定。 |
| finish       | TIMESTAMP | 可选 | 需要gapfill的结束的时间（例如结果集中没有某个时间，但是需要查询结果中有这个时间）。如果不指定此参数需要在where条件中指定。 |

#### 约束

- 只有在targelist中存在聚合函数时，才会有缺失填充的效果。

- start <= finish

#### 示例

```sql
MogDB=# select time_bucket_gapfill('2 minute',time) as bucket, sum(value)  from t1 where time < '2022-06-02 00:10:00' and time > '2022-06-02 00:00:00' group by bucket order by bucket; 
         bucket         | sum 
------------------------+-----
 2022-06-02 00:00:00+08 |   9
 2022-06-02 00:02:00+08 |    
 2022-06-02 00:04:00+08 |  11
 2022-06-02 00:06:00+08 |  21
 2022-06-02 00:08:00+08 |    
(5 rows)
```

### locf()

与time_bucket_gapfill一起使用，缺失填充时聚合值最后遇到的一个值。

#### 语法

```sql
locf(value[, prev, treat_null_as_missing])
```

#### 参数说明

| 参数名                | 类型       | 属性 | 释义                                                         |
| --------------------- | ---------- | ---- | ------------------------------------------------------------ |
| value                 | ANY        | 必选 | 需要填充的值表达式。                                         |
| prev                  | EXPRESSION | 可选 | 如果没有找到前一个值，则使用此表达式寻值。（当前版本不支持此参数，效果为不对value插值） |
| treat_null_as_missing | BOOLEAN    | 可选 | 不向后携带空值。（当前版本不支持此参数）                     |

#### 示例

```sql
MogDB=#  select time_bucket_gapfill('2 minute',time) as bucket, sum(value), locf(sum(value))  from t1 where time < '2022-06-02 00:10:00' and time > '2022-06-02 00:00:00' group by bucket order by bucket; 
           bucket         | sum | locf 
-------------------------------------+-----+------
 2022-06-02 00:00:00+08    |   9 |    9
 2022-06-02 00:02:00+08    |     |    9
 2022-06-02 00:04:00+08    |  11 |   11
 2022-06-02 00:06:00+08    |  21 |   21
 2022-06-02 00:08:00+08    |     |   21
(5 rows)
```

### interpolate()

用于对缺失数据进行现行线性插值，即为取其上一行和下一行的平均值，如果上一行或下一行不存在，则值取为空。

#### 语法

```sql
interpolate(value[, prev[, next]])
```

#### 参数说明

| 参数名 | 类型       | 属性 | 释义                                                         |
| ------ | ---------- | ---- | ------------------------------------------------------------ |
| value  | ANY        | 必选 | 需要填充的值表达式。                                         |
| prev   | EXPRESSION | 可选 | 如果没有计算插值的前序值，那么使用此表达式寻值代替前序值。（当前版本不支持此参数，效果为不对value插值） |
| next   | EXPRESSION | 可选 | 如果没有计算插值的后序值，那么使用此表达式寻值代替后序值。（当前版本不支持此参数，效果为不对value插值） |

#### 示例

```sql
MogDB=# select time_bucket_gapfill('2 minute',time) as bucket, sum(value), interpolate(sum(value))  from t1 where time < '2022-06-02 00:10:00' and time > '2022-06-02 00:00:00' group by bucket order by bucket; 
         bucket         | sum | interpolate 
-----------------------------------+-----+-------------
 2022-06-02 00:00:00+08  |   9 |           9
 2022-06-02 00:02:00+08  |     |          10
 2022-06-02 00:04:00+08  |  11 |          11
 2022-06-02 00:06:00+08  |  21 |          21
 2022-06-02 00:08:00+08  |     |            
(5 rows)
```

<br/>

## 选择类算子

常用选择算子支持计划如下：

| 函数       | 用途          | 是否支持 | 备注                                                        |
| ---------- | ------------- | -------- | ----------------------------------------------------------- |
| BOTTOM     | 取最小的N个值 | 否       |                                                             |
| FIRST      | 取最早的值    | 新增支持 |                                                             |
| LAST       | 取最晚的值    | 新增支持 |                                                             |
| MAX        | 取最大的值    | 是       | 继承MogDB                                                   |
| MIN        | 取最小的值    | 是       | 继承MogDB                                                   |
| PERCENTILE | 计算百分位值  | 是       | MogDB存在`percentile_cont(val) WITH GROUP (ORDER BY i)`语法 |
| SAMPLE     | 返回N个抽样   | 否       |                                                             |
| TOP        | 取最大的N个值 | 否       |                                                             |

### first() / last()

使用cmp_col列做比较，返回时间最早或最晚的value指定的列的值。时序的first/last算子在timeseries_catalog这个schema下，区别于public中的first/last。

#### 语法

```sql
select city_name,timeseries_catalog.first(temp_c,time) from weather_metrics group by city_name;
select city_name,timeseries_catalog.last(temp_c,time) from weather_metrics group by city_name;
timeseries_catalog.first(value, cmp_col)
timeseries_catalog.last(value, cmp_col)
```

#### 参数说明

| 参数名  | 类型                     | 属性 | 释义   |
| ------- | ------------------------ | ---- | ------ |
| value   | ANY                      | 必选 | 返回值 |
| cmp_col | TIMESTAMP or TIMESTAMPTZ | 必选 | 比较值 |

#### 约束

当表存在多条记录的cmp_col列最小值/最大值相同时，返回这些记录中第一条/最后一条的value。

当cmp_col列为常量时，时序first/last算子的效果等同于public中的first/last算子， 返回所有记录中的第一条/最后一条的value值。

#### 示例

```sql
MogDB=# select city_name,timeseries_catalog.first(temp_c,time) from weather_metrics group by city_name; 
 city_name | first 
---------------+-------
beijing    | 30
shanghai   | 29 
(2 row)
```

<br/>

## 转换类算子

常用转换算子支持计划如下：

| 函数                    | 用途                   | 是否支持 | 备注      |
| ----------------------- | ---------------------- | -------- | --------- |
| ABS                     | 求绝对值               | 是       | 继承MogDB |
| ACOS                    | 三角函数               | 是       | 继承MogDB |
| ASIN                    | 三角函数               | 是       | 继承MogDB |
| ATAN                    | 三角函数               | 是       | 继承MogDB |
| ATAN2                   | 三角函数               | 是       | 继承MogDB |
| CEIL                    | 向上取整               | 是       | 继承MogDB |
| COS                     | 三角函数               | 是       | 继承MogDB |
| DERIVATIVE              | 返回与上一个值的变化率 | 否       | 暂不支持  |
| DIFFERENCE              | 返回与上一个值的差值   | 否       | 暂不支持  |
| ELAPSED                 | 返回与上一数据的时间差 | 否       | 暂不支持  |
| EXP                     | 求指数                 | 是       | 继承MogDB |
| FLOOR                   | 向下取整               | 是       | 继承MogDB |
| HISTOGRAM               | 计算直方图             | 否       |           |
| LN                      | 对数函数               | 是       | 继承MogDB |
| LOG                     | 对数函数               | 是       | 继承MogDB |
| LOG2                    | 对数函数               | 否       | 暂不支持  |
| LOG10                   | 对数函数               | 否       | 暂不支持  |
| NON_NEGATIVE_DERIVATIVE | 非负变化率             | 否       | 暂不支持  |
| NON_NEGATIVE_DIFFERENCE | 非负差值               | 否       | 暂不支持  |
| POW                     | 乘方运算               | 是       | 继承MogDB |
| ROUND                   | 四舍五入运算           | 是       | 继承MogDB |
| SIN                     | 三角函数               | 是       | 继承MogDB |
| SQRT                    | 求平方根               | 是       | 继承MogDB |
| TAN                     | 三角函数               | 是       | 继承MogDB |
| COT                     | 三角函数               | 是       | 继承MogDB |

<br/>

## 差异化SQL能力

支持以下InfluxDB 1.x版本不支持的算子，构建差异化竞争力。这部分能力继承自MogDB，可以支持如 join语法、case ... when表达式等。

- 按聚合结果列排序：继承MogDB
- Join查询：继承MogDB
- CASE...WHEN：继承MogDB
- ...

<br/>

## 高效多维查询

时序数据库需要支持使用任意Tag组合的查询能力，并具有高效的查询效率。Uqbar能够快速根据查询条件中tag的组合定位到关联的TagSet，这部分能力最终体现在查询性能上。

<br/>

## 查询性能指标

使用TSBS时序数据库Benchmark测试工具的DevOps和IoT模型下，平均查询性能领先TimescaleDB至少30%。

TSBS查询模型：[https://github.com/timescale/tsbs](https://github.com/timescale/tsbs)

<br/>

## 支持索引

在时序表上支持索引相关功能，以便用户根据实际需求使用索引提升查询性能。时序表上支持的索引类型包括Btree和Gin，不支持创建部分索引，Btree可以支持唯一索引。时序表上索引默认是LOCAL索引，不支持创建GLOBAL索引。在时序表执行数据删除时，对应分区上的索引也会一起删除。

### 创建索引

语法和分区表创建索引语法基本相同，但不支持创建GLOBAL索引。

```sql
CREATE [ UNIQUE ] INDEX [ [schema_name.]index_name ] ON table_name [ USING method ]
    ( {{ column_name | ( expression ) } [ COLLATE collation ] [ opclass ] [ ASC | DESC ] [ NULLS LAST ] }[, ...] )
    [ LOCAL [ ( { PARTITION index_partition_name | SUBPARTITION index_subpartition_name [ TABLESPACE index_partition_tablespace ] } [, ...] ) ] ]
    [ INCLUDE ( column_name [, ...] )]
    [ WITH ( { storage_parameter = value } [, ...] ) ]
    [ TABLESPACE tablespace_name ];
```

### 删除索引

```sql
DROP INDEX [ CONCURRENTLY ] [ IF EXISTS ] index_name [, ...] [ CASCADE | RESTRICT ];
```

### 修改索引

- 重命名表索引的名称

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name RENAME TO new_name;
  ```

- 修改表索引的存储参数

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name SET ( {storage_parameter = value} [, ... ] );
  ```

- 设置表索引或索引分区不可用

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name [ MODIFY PARTITION index_partition_name ] UNUSABLE;
  ```

- 重置索引的一个或多个索引方法特定的存储参数为缺省值。与SET一样，可能需要使用REINDEX来完全更新索引。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name RESET ( { storage_parameter } [, …] );
  ```

- 重建表或者索引分区上的索引。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name REBUILD [ PARTITION index_partition_name ];
  ```

- 重命名索引分区

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name RENAME PARTITION index_partition_name TO new_index_partition_name;
  ```

- 修改索引分区的所属表空间

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name MOVE PARTITION index_partition_name TABLESPACE new_tablespace;
  ```

### 重建索引

```sql
REINDEX  { INDEX|  [INTERNAL] TABLE} name PARTITION partition_name [ FORCE  ];
```
