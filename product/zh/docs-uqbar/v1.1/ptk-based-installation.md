---
title: PTK方式安装
summary: PTK方式安装
author: Yao Qian
date: 2022-06-02
---

# PTK方式安装

本文介绍如何使用PTK安装 Uqbar 数据库。

PTK (Provisioning Toolkit)是一款针对 Uqbar 数据库开发的软件安装和运维工具，旨在帮助用户更便捷地安装部署 Uqbar 数据库。

可执行如下命令查看PTK支持安装 Uqbar 的操作系统。

```bash
[root@hostname ~]# ptk candidate os
  software |               version                
-----------+--------------------------------------
  OS       | CentOS 7 (x86_64)                    
           | CentOS 8 (arm64)                     
           | CentOS 8 (x86_64)                    
           | EulerOS 2 (arm64)                    
           | EulerOS 2 (x86_64)                   
           | Kylin V10 (arm64)                    
           | Kylin V10 (x86_64)                   
           | NeoKylin V7 (x86_64)                 
           | Oracle Linux 7 (x86_64)              
           | Oracle Linux 8 (x86_64)              
           | Red Hat Enterprise Linux 7 (x86_64)  
           | Red Hat Enterprise Linux 8 (x86_64)  
           | Rocky Linux 7 (x86_64)               
           | Rocky Linux 8 (x86_64)               
           | SLES 12 (arm64)                      
           | UOS 20 (arm64)                       
           | UOS 20 (x86_64)                      
           | Ubuntu 18 (x86_64)                   
           | openEuler 20 (arm64)                 
           | openEuler 20 (x86_64)                
           | openEuler 22 (arm64)                 
           | openEuler 22 (x86_64)  
```

> **注意**：
>
> - PTK工具本身可以在多种操作系统中运行，支持Linux，macOS，Windows，但是由于 Uqbar 目前仅支持在Linux 系统中运行，因此需确保要运行Uqbar 数据库的服务器为 Linux 操作系统。
> - 需要使用PTK v0.5 安装Uqbar，否则安装可能失败。

## 安装准备

有关环境要求和操作系统配置详情，请参见 MogDB 的[环境要求](https://docs.mogdb.io/zh/mogdb/v3.0/environment-requirement)和[操作系统配置](https://docs.mogdb.io/zh/mogdb/v3.0/os-configuration)等相关章节。

## 下载安装 PTK

有关PTK安装，请访问[安装PTK](https://docs.mogdb.io/zh/ptk/v0.5/install)。

## 通过PTK安装Uqbar

### 准备拓扑配置文件

PTK安装需要用户提供配置文件config.yaml，PTK支持单节点安装以及多节点安装。下面以单节点安装和一主一备节点安装为例。

**单节点安装**

```yaml
# config.yaml
global:
    cluster_name: uqbar1
    user: omm
    group: omm
    base_dir: /opt/uqbar
db_servers:
    - host: 127.0.0.1
      db_port: 26000
```

如果全部使用默认值，则PTK 执行以下操作：

* 在本机安装数据库;
* 运行数据库的操作系统用户为omm ，用户组名称也是omm，该用户没有默认密码；
* 数据库安装在 /opt/uqbar目录下，在该目录下会创建4个目录：app, data, log, tool，分别用于存储数据库软件、数据文件、数据库日志和数据库相关工具；
* 数据库监听端口为26000；

**主备节点安装**

```yaml
# config.yaml
global:
    cluster_name: uqbar_cluster1
    user: omm
    group: omm
    base_dir: /opt/uqbar
db_servers:
    - host: 192.168.0.1
      db_port: 26000
      role: primary
      ssh_option:
        port: 22
        user: root
        password: [此处填写SSH登录密码]
    - host: 192.168.0.2
      db_port: 26000
      role: standby
      ssh_option:
        port: 22
        user: root
        password: [此处填写SSH登录密码]
```

### 检查本机系统

```shell
ptk checkos -f config.yaml
```

确保输出的检查结果均为 `OK` 或者 `Warning` ，如果有 `Abnormal` 或 `ExecuteError`出现，需用户根据日志提示先修正系统参数。

### 执行安装

```shell
ptk install -f config.yaml
```

默认会安装当前已经正式发布的 Uqbar 最新版本，版本号可以在 Uqbar 官网下载页面查询。安装过程中会提示用户输入数据库初始用户的密码，请用户自行记录并安全保存。PTK 自动完成所有安装操作后，会启动数据库实例。

PTK 也支持自定义安装包，可以通过手工下载安装包，指定安装。例如执行以下命令，将使用当前目录下的该安装包进行数据库安装。

```shell
ptk install -f config.yaml --pkg ./Uqbar-1.1.0-openEuler-arm64.tar.gz
```

安装成功后，可以通过 `ptk ls` 来查看安装的实例信息。

```bash
[root@hostname]# ptk ls
  cluster_name |     instances      | user |    data_dir     | db_version   
---------------+--------------------+------+-----------------+--------------
  uqbar1       | 172.16.0.127:26000 | omm  | /opt/uqbar/data | Uqbar-1.1.0  
```

### 访问数据库

```shell
su - omm
gsql -d postgres -p 26000
```

## 通过PTK卸载Uqbar

> **注意**：数据库卸载后无法恢复，请谨慎操作。

执行如下命令卸载数据库：

```bash
ptk uninstall (-f CONFIG.YAML|--name CLUSTER_NAME)
```

在卸载前，PTK 会交互式的询问用户，以确认要删除的数据库信息，确认是否要连带删除系统用户，以及确认是否要连带删除数据库数据。请在回答每一个问题时，确认你的回答，避免由于误操作导致不可恢复的数据丢失！

在 PTK 执行数据库卸载操作时，如果用户指定了删除数据目录，PTK 仅会删除数据目录，不会删除数据目录所在的父目录，需用户手动清理父目录。

卸载成功后，将提示如下信息：

```bash
[root@hostname]# ptk uninstall -n uqbar1
=============================
global:
  cluster_name: uqbar1
  user: omm
  group: omm
  app_dir: /opt/uqbar/app
  data_dir: /opt/uqbar/data
  log_dir: /opt/uqbar/log
  tool_dir: /opt/uqbar/tool
  tmp_dir: /opt/uqbar/tmp
db_servers:
- host: 172.16.0.127
  db_port: 26000
  role: primary
  az_name: AZ1
  az_priority: 1

=============================
Do you really want to uninstall this cluster? Please confirm carefully[Y|Yes](default=N) y
Do you want to delete db data '/opt/uqbar/data'?[Y|Yes](default=N) y
Do you want to delete user 'omm'?[Y|Yes](default=N) y
INFO[2022-07-08T10:27:42.820] check db dirs owner                           host=172.16.0.127
INFO[2022-07-08T10:27:42.828] clean crontab                                 host=172.16.0.127
INFO[2022-07-08T10:27:42.894] kill omm's processes                          host=172.16.0.127
INFO[2022-07-08T10:27:42.970] remove files /opt/uqbar/app,/opt/uqbar/tool,/opt/uqbar/cm,/opt/uqbar/tmp,/opt/uqbar/data,/opt/uqbar/log  host=172.16.0.127
INFO[2022-07-08T10:27:43.073] delete os user omm                            host=172.16.0.127
INFO[2022-07-08T10:27:43.213] clearing /etc/cron.allow                      host=172.16.0.127
INFO[2022-07-08T10:27:43.217] clearing /etc/security/limits.conf            host=172.16.0.127
```

> **注意**：使用配置文件来卸载的前提是配置文件还存在。使用集群名称来指定的前提是 `ptk ls` 可以正常查询到该集群。

## 相关页面

有关PTK工具详细使用手册，请访问[关于PTK](https://docs.mogdb.io/zh/ptk/v0.5/overview)。