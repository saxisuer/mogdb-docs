---
title: ALTER TIMESERIES TABLE
summary: ALTER TIMESERIES TABLE
author: zhang cuiping
date: 2022-11-22
---

# ALTER TIMESERIES TABLE

## 功能描述

修改时序表，包括修改表的列名、列默认值、数据保留策略以及所有者，同时可设置时序表所属的表空间。

## 注意事项

- 除了语法中明确支持的可修改项，不支持修改其他选项。
- 同时只能修改一个选项。
- 时序表不支持重命名表名，执行 rename 会提示错误。

## 语法格式

修改时序表。

```
ALTER TIMESERIES TABLE [ IF EXISTS ] tablename_name actions
```

- 其中action包含：

  ```
  | column_clause
  | ALTER POLICY TO policy_name
  | OWNER TO new_owner 
  | SET SCHEMA new_schema
  ```

- 其中column_clause包含：

  ```
  | RENAME [ COLUMN ] column_name TO new_column_name
  | ALTER [ COLUMN ] column_name { SET DEFAULT expression | DROP DEFAULT }
  ```

## 参数说明

- **table_name**

  指定时序表的名称。

- **policy_name**

  指定时序表的策略名。系统为每个数据库提供一个默认策略，用户也可以重新设置某个策略为默认策略，创建时序表时如果未指定具体策略，则使用所属数据库的默认数据保留策略。

- **new_owner**

  指定时序表的所有者。
  
- **new_schema**

  指定时序表所属的模式。

## 示例

```sql
--修改时序表的所有者为user1。
MogDB=#ALTER TIMESERIES TABLE weather owner TO user1;
ALTER TIMESERIES TABLE
```

## 相关链接

[CREATE TIMESERIES TABLE](./CREATE-TIMESERIES-TABLE.md)，[DROP TIMESERIES TABLE](./DROP-TIMESERIES-TABLE.md)