---
title: CREATE VIEW
summary: CREATE VIEW
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE VIEW

## 功能描述

创建一个视图。视图与基本表不同，是一个虚拟的表。数据库中仅存放视图的定义，而不存放视图对应的数据，这些数据仍存放在原来的基本表中。若基本表中的数据发生变化，从视图中查询出的数据也随之改变。从这个意义上讲，视图就像一个窗口，透过它可以看到数据库中用户感兴趣的数据及变化。

## 注意事项

被授予CREATE ANY TABLE权限的用户，可以在public模式和用户模式下创建视图。

## 语法格式

```ebnf+diagram
CreateView ::= CREATE [ OR REPLACE ] [ TEMP | TEMPORARY ] VIEW view_name [ ( column_name [, ...] ) ]
    [ WITH ( {view_option_name [= view_option_value]} [, ... ] ) ]
    AS query;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 创建视图时使用WITH(security_barrier)可以创建一个相对安全的视图，避免攻击者利用低成本函数的RAISE语句打印出隐藏的基表数据。
> - 旧版MogDB在create or replace创建视图的时候，如果存在同名视图，则replace时不可以更改列信息或者删除列，只能追加列。2.1版之后的MogDB支持REPLACE VIEW语法更新或者删除列信息。

## 参数说明

- **OR REPLACE** <a id = 'replace'> </a>

  如果视图已存在，则重新定义。支持减少列、更改列名操作，仅对非物化视图有效。

- **TEMP | TEMPORARY**

  创建临时视图。

- **view_name**

  要创建的视图名称。可以用模式修饰。

  取值范围: 字符串，符合标识符命名规范。

- **column_name**

  可选的名称列表，用作视图的字段名。如果没有给出，字段名取自查询中的字段名。

  取值范围: 字符串，符合标识符命名规范。

- **view_option_name [= view_option_value]**

  该子句为视图指定一个可选的参数。

  目前view_option_name支持的参数仅有security_barrier，当VIEW试图提供行级安全时，应使用该参数。

  取值范围: Boolean类型，TRUE、FALSE

- **query**

  为视图提供行和列的SELECT或VALUES语句。

## 示例

```sql
--创建字段spcname为pg_default组成的视图。
Uqbar=# CREATE VIEW myView AS
    SELECT * FROM pg_tablespace WHERE spcname = 'pg_default';

--查看视图。
Uqbar=# SELECT * FROM myView ;

--删除视图myView。
Uqbar=# DROP VIEW myView;
```

## 相关链接

[ALTER VIEW](ALTER-VIEW.md)，[DROP VIEW](DROP-VIEW.md)
