---
title: DROP TIMESERIES TABLE
summary: DROP TIMESERIES TABLE
author: zhang cuiping
date: 2022-11-22
---

# DROP TIMESERIES TABLE

## 功能描述

用户可删除一张或多张时序表。执行删除命令后，表的数据和元数据也会删除。

## 语法格式

修改时序表。

```
DROP TIMESERIES TABLE [ IF EXISTS ] table_name [, table1_name ...];
```

## 参数说明

- **IF EXISTS**

  如果指定的表不存在，则发出一个notice而不是抛出一个错误。

- **table_name**

  指定要删除的时序表的名称。

## 示例

```sql
--删除时序表weather。
MogDB=#DROP TIMESERIES TABLE weather;
DROP TIMESERIES TABLE
```

## 相关链接

[CREATE TIMESERIES TABLE](./CREATE-TIMESERIES-TABLE.md)，[ALTER TIMESERIES TABLE](./ALTER-TIMESERIES-TABLE.md)