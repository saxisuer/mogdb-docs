---
title: CREATE TIMESERIES TABLE
summary: CREATE TIMESERIES TABLE
author: zhang cuiping
date: 2022-11-22
---

# CREATE TIMESERIES TABLE

## 功能描述

在当前数据库中创建一个新的空白时序表，该时序表由命令执行者所有。

## 注意事项

- 必须要指定 TSTIME 和 TSTAG 列。

- 只能有一列指定为 TSTIME 列，且数据类型只能为时间类型，包括 timestamp [without time zone]、timestamp with time zone。openGauss中的 date 类型，内部实际类型是 timestamp [without time zone]，除了日期外同时也包含时间信息，TSTIME 也可以支持 date 类型。而 “date” 类型只包含日期信息，不包含时间信息，所以不支持。

- 支持数量为 [1, 32] 的列指定为 TSTAG，只允许数据类型为字符串类型 ，包括 “char”、char(n)、character(n)、nchar(n)、bpchar(n)、varchar(n)、character varying(n)、nvarchar(n)、varchar2(n)、nvarchar2(n) 和 text，不支持 clob 和 name。

- 至少有一列不是 TSTIME 和 TSTAG。

- 表名小于等于63个字符。

- 一个时序表最多指定一个数据保留策略。

- TSTIME 不支持用户设置 default，系统默认设置 TSTIME 为当前时间。

- Field 列允许用户设置 NOT NULL。

- Field 列不支持自定义的数据类型，由于列存数据类型的限制，支持的数据类型如下( DATE 和 DATEARRAY 中 DATE 类型指的是 “date” 而不是 date) ：

  BOOL、HLL、BYTEA、CHAR、INT8、INT2、INT4、INT1、NUMERIC、BPCHAR、VARCHAR、NVARCHAR2、SMALLDATETIME、TEXT、OID、FLOAT4、FLOAT8、ABSTIME、RELTIME、TINTERVAL、INET、DATE、TIME、TIMESTAMP、TIMESTAMPTZ、INTERVAL、TIMETZ、MONEY、CIDR、BIT、VARBIT、CLOB、BOOLARRAY、HLL_ARRAY、BYTEARRAY、CHARARRAY、INT8ARRAY、INT2ARRAY、INT4ARRAY、INT1ARRAY、NUMERICARRAY、BPCHARARRAY、VARCHARARRAY、NVARCHAR2ARRAY、SMALLDATETIMEARRAY、TEXTARRAY、FLOAT4ARRAY、FLOAT8ARRAY、ABSTIMEARRAY、RELTIMEARRAY、INTERVALARRAY、INETARRAY、DATEARRAY、TIMEARRAY、TIMESTAMPARRAY、TIMESTAMPTZARRAY、ARRAYINTERVAL、TIMETZARRAY、MONEYARRAY、CIDRARRAY、BITARRAY、VARBITARRAY

## 语法格式

创建时序表。

```
CREATE TIMESERIES TABLE [ IF NOT EXISTS ] table_name
( column_name data_type [ tslabel ] [ DEFAULT default_expr ] [, ...] [ COLLATE collation ] )
[ POLICY policy_name ]
[ TABLESPACE tablespace_name ];
```

## 参数说明

- **table_name**

  指定时序表的名称。

- **tslabel**

  在创建时序数据库时，可以指定时序表的列数据类型，即将 tslabel 指定为TSTIME或TSTAG。TSTIME标识时序表的时间列。TSTAG标识时序表的标签列。

- **default_expr**

  指定某列的默认值。

- **policy_name**

  指定时序表的策略名。系统为每个数据库提供一个默认策略，用户也可以重新设置某个策略为默认策略，创建时序表时如果未指定具体策略，则使用所属数据库的默认数据保留策略。

- **tablespace_name**

  指定时序表所属的表空间名称。

## 示例

```sql
--创建简单的时序表。
MogDB=# CREATE TIMESERIES TABLE weather(time timestamp TSTIME, city text TSTAG, location text TSTAG, temperature float) POLICY default_policy;
CREATE TIMESERIES TABLE
```

## 相关链接

[ALTER TIMESERIES TABLE](./ALTER-TIMESERIES-TABLE.md)，[DROP TIMESERIES TABLE](./DROP-TIMESERIES-TABLE.md)