---
title: 时序表管理
summary: 时序表管理
author: zhang cuiping
date: 2022-12-02
---

# 时序表管理

本章节主要介绍时序表管理相关内容，包括如何创建、修改、删除时序表以及查看时序表视图。

## 创建时序表

表是建立在数据库中的，在不同的数据库中可以存放相同的表。通过时序表可以对时序数据进行管理。创建表前请先创建相应的时序数据库和数据保留策略。

执行如下命令创建时序表。

```
CREATE TIMESERIES TABLE [ IF NOT EXISTS ] table_name
( column_name data_type [ tslabel ] [ DEFAULT default_expr ] [, ...] [ COLLATE collation ] )
[ POLICY policy_name ]
[ TABLESPACE tablespace_name ];
```

当结果显示为如下信息，则表示创建成功。

```
CREATE TIMESERIES TABLE
```

创建成功后，可查看pg_class、pg_partition、timeseries_catalog.tstable 中相关元数据信息。

## 修改时序表

用户可以修改已存在的时序表。可修改时序表的列名、列默认值、数据保留策略以及所有者，同时可设置时序表所属的表空间。

执行如下命令创建时序表。

```
ALTER TIMESERIES TABLE [ IF EXISTS ] tablename_name actions
```

当结果显示为如下信息，则表示修改成功。

```
ALTER TIMESERIES TABLE
```

可查看pg_class中相关元数据信息已修改成功。

## 删除时序表

用户可删除一张或多张时序表。

执行如下命令删除时序表。

```
DROP TIMESERIES TABLE [ IF EXISTS ] table_name [, table1_name ...];
```

当结果显示为如下信息，则表示删除成功。

```
DROP TIMESERIES TABLE
```

可查看 pg_class、pg_partition、timeseries_catalog.tstable中相关元数据信息已删除，磁盘上相应的数据也已删除。

## 查看时序表

用户可以查看一个时序表的ChunkGroup视图或者查看当前数据库下的所有时序表。

### 查看时序表ChunkGroup视图

用户可查看一个时序表的ChunkGroup列表视图。时序表按照时间范围被拆分为多个分片组（ChunkGroup），以分片组为单位进行过期删除，以便于对时序数据进行高效过期删除。

#### 语法格式

```sql
SELECT * FROM timeseries_views.tschunkgroup WHERE tablename = 'weather';
```

#### timeseries_views.tschunkgroup视图属性

| 属性               | 类型     | 描述                                   |
| ------------------ | -------- | -------------------------------------- |
| schemaname         | name     | 时序表的模式名。                       |
| tablename          | name     | 时序表的表名。                         |
| chunkGroupName     | name     | 时序表分片组名。                       |
| chunkGroupDuration | interval | 时序表分片的时间跨度。                 |
| starttime          | text     | 时序表分片组时间范围的左边界(包含)。   |
| endtime            | text     | 时序表分片组时间范围的右边界(不包含)。 |
| compressed         | bool     | 时序表分片组是否被压缩。               |

#### 示例

```sql
--查看时序表weather的ChunkGroup视图。
MogDB=# SELECT ablename, chunkgroupname, duration,starttime,endtime FROM timeseries_views. tschunkgroup WHERE tablename= 'weather';
 tablename |  chunkname |  duration  |       starttime      |       endtime    
------------------+-----------------+----------------+------------------------------+----------------------------
 weather   |   p_1_1    |   7 days    |  2022-6-16 00:00:00  |  2022-6-23 00:00:00
 weather   |   p_1_2    |   7 days    |  2022-6-16 00:00:00  |  2022-6-23 00:00:00
 weather   |   p_2_1    |   7 days    |  2022-6-23 00:00:00  |  2022-6-30 00:00:00
 weather   |   p_2_2    |   7 days    |  2022-6-23 00:00:00  |  2022-6-30 00:00:00

(4 rows)
```

### 查看所有时序表

用户可查看当前数据库下的所有时序表。

#### 语法格式

```sql
SELECT * FROM timeseries_views.tstable;
```

#### timeseries_views.tstable视图属性

| 属性        | 类型   | 描述                   |
| ----------- | ------ | ---------------------- |
| schemaname  | name   | 时序表的模式名。       |
| tablename   | name   | 时序表的表名。         |
| timecolname | name   | 时序表中时间列的列名。 |
| tagcolname  | name[] | 时序表中tag列的列名。  |
| tspolicy    | name   | 时序表数据表的策略名。 |

#### 示例

```sql
--查看schema为timeseries_views下的所有时序表。
MogDB=# SELECT schemaname, tablename, tablecolname, tagcolname FROM timeseries_views.tstable;
 schemaname |  tablename  |  timecolname  |  tagcolname  
------------------+---------------------+----------------------+---------------------
public       |  weather     |      time      |  {city,location} 
(1 rows)
```