---
title: MogDB源库说明
summary: MDB MogDB源库说明
author: tianzijian
date: 2022-06-30
---

# MogDB为源库

## 支持版本

数据库版本: 不低于 2.0
依赖插件: wal2json

## 数据库设置

### 安装插件 wal2json

具体请参考： [wal2json](https://gitee.com/enmotech/wal2json)

### 修改源库 WAL 参数

修改源库参数配置文件 `postgresql.conf`，参数文件位于数据库 data 目录下。

通过 gsql 工具登录 MogDB 数据库，查询数据目录位置：

```shell
gsql -d postgres -U $USER -c 'show data_directory'
```

修改以上命令查询到的数据目录中的 postgresql.conf 文件

```shell
# 注意替换 <data_directory> 为实际查询到的数据目录位置
vi <data_directory>/postgresql.conf
```

在 postgresql.conf 中找到如下参数，并修改为对应的参数值：

1. wal_level=logical          # 使用logical decoding读取预写日志
2. wal_sender_timeout = 0s    # 超时时间自定义

### 修改源库 HBA 配置

在数据库数据目录（<data_directory>）中，找到 `pg_hba.conf` 配置文件，在其中添加如下配置以便允许复制。

注意： mdb_user 为复制使用的用户，实际环境中若用户名有变更，则需要实际实际情况修改。

```shell
# 注意替换 <data_directory> 为实际查询到的数据目录位置
cat >> <data_directory>/pg_hba.conf <"EOF"

# Add for MDB replication
host  replication  mdb_user   0.0.0.0/0  sha256
EOF
```

### 设置用户权限

创建 mdb_user 用户，用于登录和复制源端 MogDB 数据，实际环境中用户名与密码可按实际需求自行修改。

其中：

- login 权限用于登录数据库
- replication 权限用于同步增量数据
- sysadmin 权限用于对象初始化和全量数据同步

```sql
create user mdb_user with login replication sysadmin PASSWORD 'Enmo@123';
```

### 打开表的 REPLICA IDENTITY 模式

针对需要被同步的所有表，都需要打开 REPLICA IDENTITY 模式，具体命令如下：

注意： 以下仅为命令示例，实际上需要对被同步的所有表，都设置 REPLICA IDENTITY。

```sql
ALTER TABLE public.customers REPLICA IDENTITY FULL;
```

### 开启心跳配置

发送心跳消息使连接器能够将最新检索到的LSN发送到数据库slot，从而允许数据库回收不再需要的WAL文件所使用的磁盘空间。

开启心跳需要在[通道高级参数](./channel_desc.md#通道-高级参数)中，设置heartbeatIntervalMs为正整数，并在源节点执行以下命令。

注意： 以下仅为命令示例，表名heartbeat需与高级参数heartbeatTableName参数值保持一至。

```sql
CREATE TABLE heartbeat (ts TIMESTAMP WITH TIME ZONE);
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## 数据类型映射

| 源库      | 目标库  | 源库类型 | 目标库类型     |
|--------|-----------|----------|-----------|
| MogDB|Oracle|timestamptz|TIMESTAMP WITH TIME ZONE|
| MogDB|Oracle|raw|RAW|
| MogDB|Oracle|bit|RAW|
| MogDB|Oracle|varbit|RAW|
| MogDB|Oracle|blob|XML|
| MogDB|Oracle|bytea|BLOB|
| MogDB|Oracle|byteawithoutordercol|BLOB|
| MogDB|Oracle|byteawithoutorderwithequalcol|BLOB|
| MogDB|Oracle|uuid|BLOB|
| MogDB|Oracle|json|JSON|
| MogDB|Oracle|jsonb|JSON|
| MogDB|Oracle|xml|XMLTYPE|
| MogDB|Oracle|float4|FLOAT|
| MogDB|Oracle|float8|DOUBLE|
| MogDB|Oracle|oid|NUMBER|
| MogDB|Oracle|int1|NUMBER|
| MogDB|Oracle|int2|NUMBER|
| MogDB|Oracle|int4|NUMBER|
| MogDB|Oracle|int8|NUMBER|
| MogDB|Oracle|int16|NUMBER|
| MogDB|Oracle|money|NUMBER|
| MogDB|Oracle|numeric|NUMBER|
| MogDB|Oracle|bpchar|CHAR|
| MogDB|Oracle|char|CHAR|
| MogDB|Oracle|name|CHAR|
| MogDB|Oracle|varchar|VARCHAR2|
| MogDB|Oracle|nvarchar2|NVARCHAR2|
| MogDB|Oracle|bool|VARCHAR2|
| MogDB|Oracle|clob|CLOB|
| MogDB|Oracle|text|CLOB|
| MogDB|Oracle|abstime|TIMESTAMP WITH TIME ZONE|
| MogDB|Oracle|date|DATE|
| MogDB|Oracle|interval|INTERVAL|
| MogDB|Oracle|reltime|TEXT|
| MogDB|Oracle|smalldatetime|DATE|
| MogDB|Oracle|time|DATE|
| MogDB|Oracle|timetz|DATE|
| MogDB|Oracle|timestamp|TIMESTAMP WITHOUT TIME ZONE|

> 默认字段类型映射,字段类型映射下个版本支持

## 字符集映射

| 源库      | 目标库  | 源库字符集          | 目标库字符集    |
 |--------|----------------|----------|-----------|
| MogDB|Oracle| BIG5           |ZHT16BIG5|
| MogDB|Oracle| EUC_JP         |JA16EUC|
| MogDB|Oracle| EUC_JIS_2004   |JA16EUCTILDE|
| MogDB|Oracle| EUC_TW         |ZHT32EUC|
| MogDB|Oracle| GB18030        |ZHS32GB18030|
| MogDB|Oracle| GBK            |ZHS16GBK|
| MogDB|Oracle| ISO_8859_5     |CL8ISO8859P5|
| MogDB|Oracle| ISO_8859_6     |AR8ISO8859P6|
| MogDB|Oracle| ISO_8859_7     |EL8ISO8859P7|
| MogDB|Oracle| ISO_8859_8     |IW8ISO8859P8|
| MogDB|Oracle| JOHAB          |KO16KSCCS|
| MogDB|Oracle| KOI8R          |CL8KOI8R|
| MogDB|Oracle| KOI8U          |CL8KOI8U|
| MogDB|Oracle| LATIN1         |WE8ISO8859P1|
| MogDB|Oracle| LATIN2         |EE8ISO8859P2|
| MogDB|Oracle| LATIN3         |SE8ISO8859P3|
| MogDB|Oracle| LATIN4         |NEE8ISO8859P4|
| MogDB|Oracle| LATIN5         |WE8ISO8859P9|
| MogDB|Oracle| LATIN6         |NE8ISO8859P10|
| MogDB|Oracle| LATIN7         |BLT8ISO8859P13|
| MogDB|Oracle| LATIN8         |CEL8ISO8859P14|
| MogDB|Oracle| LATIN9         |WE8ISO8859P15|
| MogDB|Oracle| SJIS           |JA16SJIS|
| MogDB|Oracle| SHIFT_JIS_2004 |JA16SJISTILDE|
| MogDB|Oracle| SQL_ASCII      |US7ASCII|
| MogDB|Oracle| UHC            |KO16MSWIN949|
| MogDB|Oracle| UTF8           |AL32UTF8|
| MogDB|Oracle| WIN866         |RU8PC866|
| MogDB|Oracle| WIN1250        |EE8MSWIN1250|
| MogDB|Oracle| WIN1251        |CL8MSWIN1251|
| MogDB|Oracle| WIN1252        |WE8MSWIN1252|
| MogDB|Oracle| WIN1253        |EL8MSWIN1253|
| MogDB|Oracle| WIN1254        |TR8MSWIN1254|
| MogDB|Oracle| WIN1255        |IW8MSWIN1255|
| MogDB|Oracle| WIN1256        |AR8MSWIN1256|
| MogDB|Oracle| WIN1257        |BLT8MSWIN1257|
| MogDB|Oracle| WIN1258        |VN8MSWIN1258|
