---
title: MySQL源库说明
summary: MDB MySQL源库说明
author: tianzijian
date: 2022-09-30
---

# MySQL为源库

## 支持版本

5.7, 8.0.x (不支持MariaDB)

## 数据库设置

### 开启binlog

```shell
vi /etc/my.cnf

# 在末尾加上
server_id=2
log_bin = mysql-bin
binlog_format = ROW

# 重启mysql服务
systemctl restart mysqld
```

### 检查binlog

```sql
SHOW VARIABLES WHERE VARIABLE_NAME IN ('log_bin','binlog_format');

-- 期望结果:log_bin: ON, binlog_format: ROW 
```

### 设置用户权限

创建 mdb_user 用户，向用户授予所需的权限：

```sql
CREATE USER 'mdb_user'@'localhost' IDENTIFIED BY 'Enmo@123';
GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'mdb_user' IDENTIFIED BY 'Enmo@123';
FLUSH PRIVILEGES;
```

### 开启心跳配置

心跳消息对于监控连接器是否从数据库接收更改事件非常有用。心跳消息可能有助于减少连接器重新启动时需要重新发送的更改事件数。

开启心跳需要在[通道高级参数](./channel_desc.md#通道-高级参数)中，设置heartbeatIntervalMs为正整数，并在源节点执行以下命令。

注意： 以下仅为命令示例，表名heartbeat需与高级参数heartbeatTableName参数值保持一至。

```sql
CREATE TABLE heartbeat (`ts` timestamp ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## 数据类型映射

| 源库      | 目标库  | 源库类型 | 目标库类型     |
|--------|-----------|----------|-----------|
|MySQL|MogDB|tinytext|text|
|MySQL|MogDB|text|text|
|MySQL|MogDB|mediumtext|text|
|MySQL|MogDB|longtext|text|
|MySQL|MogDB|tinyblob|bytea|
|MySQL|MogDB|blob|bytea|
|MySQL|MogDB|mediumblob|bytea|
|MySQL|MogDB|longblob|bytea|
|MySQL|MogDB|json|json|
|MySQL|MogDB|bit|bytea|
|MySQL|MogDB|binary|bytea|
|MySQL|MogDB|char byte|bytea|
|MySQL|MogDB|varbinary|bytea|
|MySQL|MogDB|tinyint|smallint|
|MySQL|MogDB|smallint|smallint|
|MySQL|MogDB|mediumint|integer|
|MySQL|MogDB|integer|integer|
|MySQL|MogDB|int|integer|
|MySQL|MogDB|bigint|bigint|
|MySQL|MogDB|serial|bigserial|
|MySQL|MogDB|float|float|
|MySQL|MogDB|double|double precision|
|MySQL|MogDB|double precision|double precision|
|MySQL|MogDB|real|double precision|
|MySQL|MogDB|decimal|numeric|
|MySQL|MogDB|numeric|numeric|
|MySQL|MogDB|dec|numeric|
|MySQL|MogDB|fixed|numeric|
|MySQL|MogDB|char|character|
|MySQL|MogDB|character|character|
|MySQL|MogDB|nchar|character|
|MySQL|MogDB|national char|character|
|MySQL|MogDB|varchar|character varying|
|MySQL|MogDB|character varying|character varying|
|MySQL|MogDB|national varchar|character varying|
|MySQL|MogDB|nvarchar|character varying|
|MySQL|MogDB|date|date|
|MySQL|MogDB|timestamp|timestamp|
|MySQL|MogDB|year|int|
|MySQL|MogDB|boolean|boolean|
|MySQL|MogDB|bool|boolean|
|MySQL|MogDB|enum|text|
|MySQL|MogDB|time|time|
|MySQL|MogDB|datetime|timestamp|
|MySQL|MogDB|set|text|
|MySQL|MogDB|geometry|text|
|MySQL|MogDB|point|text|
|MySQL|MogDB|linestring|text|
|MySQL|MogDB|polygon|text|
|MySQL|MogDB|multipoint|text|
|MySQL|MogDB|multilinestring|text|
|MySQL|MogDB|multipolygon|text|
|MySQL|MogDB|geometrycollection|text|
|MySQL|MogDB|geomcollection|text|
|MySQL|MogDB|auto_increase|serial|

> 默认字段类型映射,字段类型映射下个版本支持

## 字符集映射

| 源库      | 目标库  | 源库字符集          | 目标库字符集    |
 |--------|----------------|----------|-----------|
| MySQL|MogDB| utf8mb4           |UTF8|
