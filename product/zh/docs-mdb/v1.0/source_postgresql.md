---
title: PostgreSQL源库说明
summary: MDB PostgreSQL源库说明
author: tianzijian
date: 2022-10-24
---

# PostgreSQL为源库

## 支持版本

数据库版本: 9.6, 10, 11, 12, 13, 14
依赖插件: decoderbufs, wal2json

## 数据库设置

### 安装插件

具体请参考： [decoderbufs](https://github.com/debezium/postgres-decoderbufs)，
           [wal2json](https://github.com/eulerto/wal2json)

### 修改源库 WAL 参数

修改源库参数配置文件 `postgresql.conf`，参数文件位于数据库 data 目录下。

通过 psql 工具登录 PostgreSQL 数据库，查询数据目录位置：

```shell
psql -d postgres -U $USER -c 'show data_directory'
```

修改以上命令查询到的数据目录中的 postgresql.conf 文件

```shell
# 注意替换 <data_directory> 为实际查询到的数据目录位置
vi <data_directory>/postgresql.conf
```

在 postgresql.conf 中找到如下参数，并修改为对应的参数值：

1. wal_level=logical          # 使用logical decoding读取预写日志
2. max_replication_slots = 4       # 最大复制槽数 (变更后重启生效)
3. wal_sender_timeout = 0s         # 超时时间自定义, 0不生效
4. max_wal_senders = 8             # 最大walsender进程数 (变更后重启生效)
5. shared_preload_libraries = 'decoderbufs' # 仅decoderbufs插件配置，wal2json插件请忽略

### 修改源库 HBA 配置

在数据库数据目录（<data_directory>）中，找到 `pg_hba.conf` 配置文件，在其中添加如下配置以便允许复制。

注意： mdb_user 为复制使用的用户，实际环境中若用户名有变更，则需要实际实际情况修改。

```shell
# 注意替换 <data_directory> 为实际查询到的数据目录位置
cat >> <data_directory>/pg_hba.conf <"EOF"

# Add for MDB replication
host  replication  mdb_user   0.0.0.0/0  sha256
EOF
```

### 设置用户权限

创建 mdb_user 用户，用于登录和复制源端 PostgreSQL 数据，实际环境中用户名与密码可按实际需求自行修改。

其中：

- login 权限用于登录数据库
- replication 权限用于同步增量数据
- superuser 权限用于对象初始化和全量数据同步

```sql
create user mdb_user with superuser login replication PASSWORD 'Enmo@123';
```

### 打开表的 REPLICA IDENTITY 模式

针对需要被同步的所有表，都需要打开 REPLICA IDENTITY 模式，具体命令如下：

注意： 以下仅为命令示例，实际上需要对被同步的所有表，都设置 REPLICA IDENTITY。

```sql
ALTER TABLE public.customers REPLICA IDENTITY FULL;
```

### 开启心跳配置

发送心跳消息使连接器能够将最新检索到的LSN发送到数据库slot，从而允许数据库回收不再需要的WAL文件所使用的磁盘空间。

开启心跳需要在[通道高级参数](./channel_desc.md#通道-高级参数)中，设置heartbeatIntervalMs为正整数，并在源节点执行以下命令。

注意： 以下仅为命令示例，表名heartbeat需与高级参数heartbeatTableName参数值保持一至。

```sql
CREATE TABLE heartbeat (ts TIMESTAMP WITH TIME ZONE);
INSERT INTO heartbeat (ts) VALUES (NOW());
```

## 数据类型映射

| 源库      | 目标库  | 源库类型 | 目标库类型     |
|--------|-----------|----------|-----------|
|PostgreSQL| MogDB|inet|inet|
|PostgreSQL| MogDB|macaddr|macaddr|
|PostgreSQL| MogDB|tsquery|bytea|
|PostgreSQL| MogDB|tsvector|bytea|
|PostgreSQL| MogDB|aclitem|bytea|
|PostgreSQL| MogDB|cid|bytea|
|PostgreSQL| MogDB|gtsvector|bytea|
|PostgreSQL| MogDB|int2vector|bytea|
|PostgreSQL| MogDB|oidvector|bytea|
|PostgreSQL| MogDB|oidvector_extend|bytea|
|PostgreSQL| MogDB|refcursor|bytea|
|PostgreSQL| MogDB|regclass|bytea|
|PostgreSQL| MogDB|oid|numeric|
|PostgreSQL| MogDB|bit|bytea|
|PostgreSQL| MogDB|varbit|bytea|
|PostgreSQL| MogDB|bytea|bytea|
|PostgreSQL| MogDB|uuid|uuid|
|PostgreSQL| MogDB|json|text|
|PostgreSQL| MogDB|jsonb|jsonb|
|PostgreSQL| MogDB|xml|text|
|PostgreSQL| MogDB|float4|float4|
|PostgreSQL| MogDB|float8|float8|
|PostgreSQL| MogDB|int2|int2|
|PostgreSQL| MogDB|int4|int4|
|PostgreSQL| MogDB|int8|int8|
|PostgreSQL| MogDB|money|numeric|
|PostgreSQL| MogDB|numeric|numeric|
|PostgreSQL| MogDB|bpchar|bpchar|
|PostgreSQL| MogDB|char|char|
|PostgreSQL| MogDB|name|name|
|PostgreSQL| MogDB|varchar|varchar|
|PostgreSQL| MogDB|bool|bytea|
|PostgreSQL| MogDB|text|text|
|PostgreSQL| MogDB|date|date|
|PostgreSQL| MogDB|interval|interval|
|PostgreSQL| MogDB|time|time|
|PostgreSQL| MogDB|timetz|timetz|
|PostgreSQL| MogDB|timestamp|timestamp|
|PostgreSQL| MogDB|timestamptz|timestamptz|
|PostgreSQL| MogDB|point|text|
|PostgreSQL| MogDB|lseg|bytea|
|PostgreSQL| MogDB|box|bytea|
|PostgreSQL| MogDB|path|bytea|
|PostgreSQL| MogDB|polygon|bytea|
|PostgreSQL| MogDB|circle|bytea|
|PostgreSQL| MogDB|cidr|cidr|
|PostgreSQL| MogDB|regconfig|bytea|
|PostgreSQL| MogDB|regdictionary|bytea|
|PostgreSQL| MogDB|regoper|bytea|
|PostgreSQL| MogDB|regoperator|bytea|
|PostgreSQL| MogDB|regproc|bytea|
|PostgreSQL| MogDB|regprocedure|bytea|
|PostgreSQL| MogDB|regtype|bytea|
|PostgreSQL| MogDB|tid|bytea|
|PostgreSQL| MogDB|txid_snapshot|bytea|
|PostgreSQL| MogDB|xid|bytea|
|PostgreSQL| MogDB|smallserial|serial|
|PostgreSQL| MogDB|int4range|int4range|
|PostgreSQL| MogDB|int8range|int8range|
|PostgreSQL| MogDB|numrange|numrange|
|PostgreSQL| MogDB|tsrange|tsrange|
|PostgreSQL| MogDB|daterange|daterange|
|PostgreSQL| MogDB|ltree|text|
|PostgreSQL| MogDB|citext|text|
|PostgreSQL| MogDB|macaddr8|text|
|PostgreSQL| MogDB|geometry|text|
|PostgreSQL| MogDB|geography|text|
|PostgreSQL| MogDB|hstore|text|
|PostgreSQL| MogDB|enum|text|

> 默认字段类型映射,字段类型映射下个版本支持

## 字符集映射

| 源库      | 目标库  | 源库字符集          | 目标库字符集    |
 |--------|----------------|----------|-----------|
|PostgreSQL|MogDB|BIG5|BIG5|
|PostgreSQL|MogDB|EUC_CN|EUC_CN|
|PostgreSQL|MogDB|EUC_JP|EUC_JP|
|PostgreSQL|MogDB|EUC_JIS_2004|EUC_JIS_2004|
|PostgreSQL|MogDB|EUC_KR|EUC_KR|
|PostgreSQL|MogDB|EUC_TW|EUC_TW|
|PostgreSQL|MogDB|GB18030|GB18030|
|PostgreSQL|MogDB|GBK|GBK|
|PostgreSQL|MogDB|Windows936|Windows936|
|PostgreSQL|MogDB|ISO_8859_5|ISO_8859_5|
|PostgreSQL|MogDB|ISO_8859_6|ISO_8859_6|
|PostgreSQL|MogDB|ISO_8859_7|ISO_8859_7|
|PostgreSQL|MogDB|ISO_8859_8|ISO_8859_8|
|PostgreSQL|MogDB|JOHAB|JOHAB|
|PostgreSQL|MogDB|KOI8R|KOI8R|
|PostgreSQL|MogDB|KOI8U|KOI8U|
|PostgreSQL|MogDB|LATIN1|LATIN1|
|PostgreSQL|MogDB|LATIN2|LATIN2|
|PostgreSQL|MogDB|LATIN3|LATIN3|
|PostgreSQL|MogDB|LATIN4|LATIN4|
|PostgreSQL|MogDB|LATIN5|LATIN5|
|PostgreSQL|MogDB|LATIN6|LATIN6|
|PostgreSQL|MogDB|LATIN7|LATIN7|
|PostgreSQL|MogDB|LATIN8|LATIN8|
|PostgreSQL|MogDB|LATIN9|LATIN9|
|PostgreSQL|MogDB|LATIN10|LATIN10|
|PostgreSQL|MogDB|MULE_INTERNAL|MULE_INTERNAL|
|PostgreSQL|MogDB|SJIS|SJIS|
|PostgreSQL|MogDB|SHIFT_JIS_2004|SHIFT_JIS_2004|
|PostgreSQL|MogDB|SQL_ASCII|SQL_ASCII|
|PostgreSQL|MogDB|UHC|UHC|
|PostgreSQL|MogDB|UTF8|UTF8|
|PostgreSQL|MogDB|WIN866|WIN866|
|PostgreSQL|MogDB|WIN874|WIN874|
|PostgreSQL|MogDB|WIN1250|WIN1250|
|PostgreSQL|MogDB|WIN1251|WIN1251|
|PostgreSQL|MogDB|WIN1252|WIN1252|
|PostgreSQL|MogDB|WIN1253|WIN1253|
|PostgreSQL|MogDB|WIN1254|WIN1254|
|PostgreSQL|MogDB|WIN1255|WIN1255|
|PostgreSQL|MogDB|WIN1256|WIN1256|
|PostgreSQL|MogDB|WIN1257|WIN1257|
|PostgreSQL|MogDB|WIN1258|WIN1258|
