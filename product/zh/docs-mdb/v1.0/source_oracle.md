---
title: Oracle源库说明
summary: MDB Oracle源库说明
author: tianzijian
date: 2022-06-30
---

# Oracle为源库

## 支持版本

11g, 12c, 19c

## 数据库设置

### 创建 MDB 用户

#### 11g

使用管理员用户（或者具有对应权限的用户）登录 Oracle 源库，创建 MDB 软件的登录用户：

登录管理员用户：

```shell
sqlplus / as sysdba
```

创建用户：

```sql
-- 可选步骤： 创建 MDB 用户表空间（表空间名称和路径需依据实际情况修改）
CREATE TABLESPACE mdb_tbs DATAFILE '/u01/app/oracle/oradata/ORCL/mdb_tbs.dbf' SIZE 1024M AUTOEXTEND OFF;

-- 必要步骤： 创建 MDB 用户（用户名和密码可按实际情况修改）
CREATE USER mdb_user IDENTIFIED BY "Enmo@123" DEFAULT TABLESPACE mdb_tbs QUOTA UNLIMITED ON mdb_tbs;
```

用户授权：

```sql
GRANT CREATE SESSION TO mdb_user;
GRANT FLASHBACK ANY TABLE TO mdb_user;
GRANT SELECT ANY TABLE TO mdb_user;
GRANT SELECT_CATALOG_ROLE TO mdb_user;
GRANT EXECUTE_CATALOG_ROLE TO mdb_user;
GRANT SELECT ANY TRANSACTION TO mdb_user;

GRANT CREATE TABLE TO mdb_user;
GRANT LOCK ANY TABLE TO mdb_user;
GRANT CREATE SEQUENCE TO mdb_user;

GRANT EXECUTE ON DBMS_LOGMNR TO mdb_user;
GRANT EXECUTE ON DBMS_LOGMNR_D TO mdb_user;

GRANT SELECT ON V_$DATABASE to mdb_user;
GRANT SELECT ON V_$LOG TO mdb_user;
GRANT SELECT ON V_$LOG_HISTORY TO mdb_user;
GRANT SELECT ON V_$LOGMNR_LOGS TO mdb_user;
GRANT SELECT ON V_$LOGMNR_CONTENTS TO mdb_user;
GRANT SELECT ON V_$LOGMNR_PARAMETERS TO mdb_user;
GRANT SELECT ON V_$LOGFILE TO mdb_user;
GRANT SELECT ON V_$ARCHIVED_LOG TO mdb_user;
GRANT SELECT ON V_$ARCHIVE_DEST_STATUS TO mdb_user;
GRANT SELECT ON V_$TRANSACTION TO mdb_user;
```

#### 12c 19c

使用管理员用户（或者具有对应权限的用户）登录 Oracle 源库，创建 MDB 软件的登录用户：

登录管理员用户：

```shell
sqlplus / as sysdba
```

创建用户：

```sql
-- 可选步骤： 创建 MDB 用户表空间（表空间名称和路径需依据实际情况修改）
-- 默认连接CBD容器
CREATE TABLESPACE mdb_tbs DATAFILE '/opt/oracle/oradata/ORCLCDB/mdb_tbs.dbf' SIZE 1024M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;

-- 切换连接PDB容器 pdb_name:ORCLPDB1
alter session set container=ORCLPDB1; 
CREATE TABLESPACE mdb_tbs DATAFILE '/opt/oracle/oradata/ORCLCDB/ORCLPDB1/mdb_tbs.dbf' SIZE 1024M REUSE AUTOEXTEND ON MAXSIZE UNLIMITED;

-- 必要步骤： 创建 MDB 用户（用户名和密码可按实际情况修改）
conn / as sysdba
CREATE USER c##mdbuser IDENTIFIED BY "Enmo@123" DEFAULT TABLESPACE mdb_tbs QUOTA UNLIMITED ON mdb_tbs CONTAINER=ALL;
alter user c##mdbuser set container_data=all container=current;
```

用户授权：

```sql
GRANT CREATE SESSION TO c##mdbuser CONTAINER=ALL;
GRANT SET CONTAINER TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$DATABASE to c##mdbuser CONTAINER=ALL;
GRANT FLASHBACK ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT_CATALOG_ROLE TO c##mdbuser CONTAINER=ALL;
GRANT EXECUTE_CATALOG_ROLE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ANY TRANSACTION TO c##mdbuser CONTAINER=ALL;
GRANT LOGMINING TO c##mdbuser CONTAINER=ALL;

GRANT CREATE TABLE TO c##mdbuser CONTAINER=ALL;
GRANT LOCK ANY TABLE TO c##mdbuser CONTAINER=ALL;
GRANT CREATE SEQUENCE TO c##mdbuser CONTAINER=ALL;

GRANT EXECUTE ON DBMS_LOGMNR TO c##mdbuser CONTAINER=ALL;
GRANT EXECUTE ON DBMS_LOGMNR_D TO c##mdbuser CONTAINER=ALL;

GRANT SELECT ON V_$LOG TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOG_HISTORY TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_LOGS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_CONTENTS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGMNR_PARAMETERS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$LOGFILE TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$ARCHIVED_LOG TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$ARCHIVE_DEST_STATUS TO c##mdbuser CONTAINER=ALL;
GRANT SELECT ON V_$TRANSACTION TO c##mdbuser CONTAINER=ALL;
```

### 归档设置

#### 检查归档路径

使用 sysdba 连接到数据库中，运行以下命令，检查数据库归档模式：

```sql
COL DEST_NAME FOR A33
COL DESTINATION FOR A55
COL ERROR FOR A55
SET LINES 188 PAGES 111
SELECT DEST_NAME, DESTINATION, ERROR
  FROM V$ARCHIVE_DEST
 WHERE STATUS = 'VALID';
```

归档模式下的输出示例如下（有归档路径，DESTINATION 可能不同，且 ERROR 字段无报错）：

```text
DEST_NAME           DESTINATION                ERROR
------------------- -------------------------- ------------------
LOG_ARCHIVE_DEST_1  USE_DB_RECOVERY_FILE_DEST
```

#### 设置归档

若检查到源库端并未设置归档模式（归档路径为空），则需要手动启用源库归档模式。

在 sqlplus 客户端工具中，以管理员模式登陆数据库：

```shell
sqlplus / as sysdba
```

设置归档路径（二者取其一）：

```sql
-- 使用 FRA (自动管理)
alter system set db_recovery_file_dest_size=100G;
alter system set db_recovery_file_dest='/opt/oracle/oradata/recovery_area' scope=spfile;

-- 使用手动指定的归档路径
alter system set LOG_ARCHIVE_DEST_1='LOCATION=/u01/app/oracle/archivelog' scope=spfile;
alter system set LOG_ARCHIVE_DEST_STATE_1='enable' scope=spfile;
```

启用归档模式（需重启源端 Oracle 数据库）：

```sql
-- 重启数据库到 mount 模式
shutdown immediate
startup mount

-- 启用归档模式
alter database archivelog;

-- 打开数据库
alter database open;

-- 检查归档模式
archive log list

-- 示例输出如下：
-- Database log mode              Archive Mode  -- 归档模式已启用
-- Automatic archival             Enabled
-- Archive destination            USE_DB_RECOVERY_FILE_DEST
-- Oldest online log sequence     49609
-- Next log sequence to archive   49613
-- Current log sequence           49613
```

### 启用补充日志

需在数据库级别启用最小补充日志记录：

```sql
ALTER DATABASE ADD SUPPLEMENTAL LOG DATA;
```

针对需要同步的表，需要启用更全面的补充日志：

```sql
ALTER TABLE inventory.customers ADD SUPPLEMENTAL LOG DATA (ALL) COLUMNS;
```

### CDB和PDB

12c以及12c以上的版本, 如果开启CDB, 添加节点会提示输入PDB名(注意: 需要开启全部PDB, 才能保证数据同步启动成功!)。

### 开启心跳配置

发送心跳消息使连接器能够将最新检索到的SCN发送到offset，避免offset过时，保持offset同步。

开启心跳需要在[通道高级参数](./channel_desc.md#通道-高级参数)中，设置heartbeatIntervalMs为正整数。

## 数据类型映射

| 源库      | 目标库  | 源库类型 | 目标库类型     |
 |--------|-----------|----------|-----------|
| Oracle | MogDB| NCLOB| text                     |
| Oracle | MogDB| BLOB| bytea                    |
| Oracle | MogDB| FLOAT| real                     |
| Oracle | MogDB| NUMBER| numeric                  |
| Oracle | MogDB| CLOB| text                     |
| Oracle | MogDB| RAW| bytea                    |
| Oracle | MogDB| CHAR| character                |
| Oracle | MogDB| JSON| json                     |
| Oracle | MogDB| VARCHAR2| character varying        |
| Oracle | MogDB| NCHAR| character                |
| Oracle | MogDB| INTERVAL DAY TO SECOND| interval day to second   |
| Oracle | MogDB| NVARCHAR2| character varying        |
| Oracle | MogDB| DATE| timestamp                |
| Oracle | MogDB| TIMESTAMP WITH LOCAL TIME ZONE| timestamp with time zone |
| Oracle | MogDB| XMLTYPE| xml                      |
| Oracle | MogDB| LONG RAW| bytea                    |
| Oracle | MogDB| TIMESTAMP WITH TIME ZONE| timestamp with time zone |
| Oracle | MogDB| INTERVAL YEAR TO MONTH| interval year to month   |
| Oracle | MogDB| BINARY_FLOAT| real                     |
| Oracle | MogDB| TIMESTAMP| timestamp                |
| Oracle | MogDB| BFILE| bytea                    |
| Oracle | MogDB| BINARY_DOUBLE| double precision         |
| Oracle | MogDB| LONG| text                     |

> 默认字段类型映射,字段类型映射下个版本支持

## 字符集映射

| 源库      | 目标库  | 源库字符集          | 目标库字符集    |
 |--------|----------------|----------|-----------|
|Oracle| MogDB| ZHT16BIG5      |BIG5|
|Oracle| MogDB| JA16EUC        |EUC_JP|
|Oracle| MogDB| JA16EUCTILDE   |EUC_JIS_2004|
|Oracle| MogDB| ZHT32EUC       |EUC_TW|
|Oracle| MogDB| ZHS32GB18030   |GB18030|
|Oracle| MogDB| ZHS16GBK       |GBK|
|Oracle| MogDB| CL8ISO8859P5   |ISO_8859_5|
|Oracle| MogDB| AR8ISO8859P6   |ISO_8859_6|
|Oracle| MogDB| EL8ISO8859P7   |ISO_8859_7|
|Oracle| MogDB| IW8ISO8859P8   |ISO_8859_8|
|Oracle| MogDB| KO16KSCCS      |JOHAB|
|Oracle| MogDB| CL8KOI8R       |KOI8R|
|Oracle| MogDB| CL8KOI8U       |KOI8U|
|Oracle| MogDB| WE8ISO8859P1   |LATIN1|
|Oracle| MogDB| EE8ISO8859P2   |LATIN2|
|Oracle| MogDB| SE8ISO8859P3   |LATIN3|
|Oracle| MogDB| NEE8ISO8859P4  |LATIN4|
|Oracle| MogDB| WE8ISO8859P9   |LATIN5|
|Oracle| MogDB| NE8ISO8859P10  |LATIN6|
|Oracle| MogDB| BLT8ISO8859P13 |LATIN7|
|Oracle| MogDB| CEL8ISO8859P14 |LATIN8|
|Oracle| MogDB| WE8ISO8859P15  |LATIN9|
|Oracle| MogDB| JA16SJIS       |SJIS|
|Oracle| MogDB| JA16SJISTILDE  |SHIFT_JIS_2004|
|Oracle| MogDB| US7ASCII       |SQL_ASCII|
|Oracle| MogDB| KO16MSWIN949   |UHC|
|Oracle| MogDB| AL32UTF8       |UTF8|
|Oracle| MogDB| RU8PC866       |WIN866|
|Oracle| MogDB| EE8MSWIN1250   |WIN1250|
|Oracle| MogDB| CL8MSWIN1251   |WIN1251|
|Oracle| MogDB| WE8MSWIN1252   |WIN1252|
|Oracle| MogDB| EL8MSWIN1253   |WIN1253|
|Oracle| MogDB| TR8MSWIN1254   |WIN1254|
|Oracle| MogDB| IW8MSWIN1255   |WIN1255|
|Oracle| MogDB| AR8MSWIN1256   |WIN1256|
|Oracle| MogDB| BLT8MSWIN1257  |WIN1257|
|Oracle| MogDB| VN8MSWIN1258   |WIN1258|
