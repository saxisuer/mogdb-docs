---
title: 安装说明
summary: MDB 安装说明
author: tianzijian
date: 2022-06-30
---

# MDB 安装

## 安装需求

MDB 部署的系统需求如下：

- 服务器： Linux x86_64
- 资料库： MogDB
- 磁盘空间：500G

## 支持版本

| 软件     | 版本                                 |
|:-------|:-----------------------------------|
| Java   | 11+                                |
| MogDB  | Database: 2.0+  Plug-ins: wal2json |
| Oracle | 11g, 12c, 19c            |
| MySQL  | 5.7, 8.0.x (不支持MariaDB)   |
|PostgreSQL|Database: 9.6, 10, 11, 12, 13, 14 Plug-ins: decoderbufs, wal2json|

## 安装步骤

### 下载软件

下载最新版 MDB 安装包. 安装包请联系相关销售获取.

### 上传软件并解压

上传 MDB 安装包到服务器，并解压：

```shell
tar -zxvf mdb_v1.0.0.RELEASE_linux_x86_64.tar.gz
```

### 准备资料库

MDB 当前资料库必须使用 MogDB(openGauss) 数据库，版本不低于 2.0。

MogDB 安装请参考： [MogDB单实例安装](https://docs.mogdb.io/zh/mogdb/v2.1/installation-on-a-single-node)

MogDB 软件安装完成后，在 MogDB 数据库服务器中，使用 gsql 客户端工具以本地超级管理员用户登录数据库

```shell
gsql -r
```

在 MogDB 中预先创建 MDB 资料库与资料库用户：

```sql
-- 创建资料库用户，密码请依据实际环境需求修改
create user mdb_repo with password 'MDB@password' login;

-- 创建资料库，资料库 owner 必须设置为 mdb_repo
create database mdb_repo_db with owner mdb_repo;
```

### 检查 Java 版本

MDB 依赖 Java 11，需要确保当前安装服务器中，Java 版本不低于 11：

```shell
java -version
```

若 Java 版本过低，则需要从 Oracle 官网下载 Java 11 对应的 JDK/JRE 软件包，下载链接如下：

<https://www.oracle.com/java/technologies/downloads/#java11>

下载 jdk-11.0.15.1_linux-x64_bin.tar.gz 软件包，并上传到服务器中解压使用：

```shell
tar -zxvf jdk-11.0.15.1_linux-x64_bin.tar.gz

# 获取当前路径, 当前路径: /usr/local
pwd 

# 配置环境变量
vi ~/.bash_profile

# 替换JAVA_HOME路径后, 在文件末尾输入以下内容:
export JAVA_HOME=/usr/local/jdk-11.0.15
export CLASSPATH=$:CLASSPATH:$JAVA_HOME/lib/
export PATH=$PATH:$JAVA_HOME/bin

# 保存退出, 执行以下命令更新配置文件
source ~/.bash_profile
```

再次确认 Java 版本，确保已使用 Java 11:

```shell
java -version

# 以下为示例输出：
# --------------
# java version "11.0.15" 2022-04-19 LTS
# Java(TM) SE Runtime Environment 18.9 (build 11.0.15+8-LTS-149)
# Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.15+8-LTS-149, mixed mode)
```

### 检查 Web 端口占用

MDB Server 需要一个 Web 监听端口（默认 55437），在安装的时候，需要确保该端口未被占用。

```shell
netstat -anp | grep LISTEN | grep 55437    # 确保没有处于 LISTEN 状态的进程
```

### 检查存在残留文件

zookeeper 和 kafka 安装，需要确保/tmp/zookeeper 和 /tmp/kafka-logs目录不存在。如果确定是残留文件，请执行以下命令。

```shell
rm -rf /tmp/zookeeper
rm -rf /tmp/kafka-logs
```

### 调整安装配置

切换到已解压的 MDB 安装包目录，检查安装包中文件的完整性：

```shell
cd mdb_v1.0.0.RELEASE_linux_x86_64

ls -ltr
```

安装包文件说明如下：

| 文件名 | 文件说明 |
|-|-|
| kafka.zip | Kafaka 组件安装包，用于同步时中间数据队列 |
| mdb | MDB 命令执行文件 |
| mdb-data-worker.zip | 数据采集投递任务相关的软件包 |
| mdb-server.zip | MDB 主软件包 |
| config.json | 安装配置文件 |

修改配置文件： `config.json`

```json
{
 "mdbServerPort": 55437,
 "dbHost": "127.0.0.1",
 "dbPort": 26000,
 "dbUser": "mdb_repo",
 "dbPassword": "MDB@password",
 "dbName": "mdb_repo_db"
 }
```

配置项说明如下：

| 配置项 | 默认值 | 配置说明 |
|-|-|-|
| mdbServerPort | 55437 | MDB 服务端口 <br /> 用于 Web 页面访问，需要确保从 MDB 服务器到客户端 Web 之间的网络互通 |
| dbHost | 127.0.0.1 | 资料库监听的 IP 地址 |
| dbPort | 26000 | 资料库监听的服务端口 |
| dbUser | mdb_repo | MDB 资料库用户 |
| dbPassword | MDB@password | MDB 资料库密码 |
| dbName | mdb_repo_db | MDB 资料库数据库名 |

### 安装 MDB

检查并确保已切换到 MDB 安装包目录：

```shell
pwd         # 确保已经位于 mdb_v1.0.0.RELEASE_linux_x86_64 目录中
```

执行安装命令：

```shell
./mdb install
```

正常安装结果如下：

```text
   2022-06-23 14:08:24  ======BEGIN TO INSTALL MDB======
   2022-06-23 14:08:24    1.CHECK MDB FILE
   2022-06-23 14:08:24    2.CHECK MDB ENVIRONMENT
   2022-06-23 14:08:25     JAVA_VERSION: 11.0
   2022-06-23 14:08:25    3.UNZIP KAFKA
   2022-06-23 14:08:26    4.INSTALL MDB SERVER
   2022-06-23 14:08:26    5.INSTALL MDB DATA WORKER
   2022-06-23 14:08:26

   ======END INSTALL MDB RESULT======
   +------------+-----------+-------------+---------------+
   | File_Check | Env_Check | MDB_Install | KAFKA_INSTALL |
   +------------+-----------+-------------+---------------+
   |  Success   |  Success  |   Success   |    Success    |
   +------------+-----------+-------------+---------------+

```

### 申请 LICENSE

执行申请license命令：

```shell
 ./mdb generate-licence test@enmotech.com      #test@enmotech.com 是接收license文件的邮箱，需要上传license文件到服务器
```

正常执行结果如下：

```text
   2022-07-07 14:06:22  邮件成功发送到test@enmotech.com,请查看邮箱.
```

### 应用 LICENSE

执行应用license命令：

```shell
 ./mdb apply-license /u01/mdb/license.json      #/u01/mdb/license.json是license文件在服务器上的路径
```

正常执行结果如下：

```text
   2022-07-04 11:26:01  apply license success
```

### 启动 MDB

执行启动命令：

```shell
 ./mdb start
```

正常启动的执行结果如下：

```text
   2022-06-23 11:33:10  start zookeeper success
   2022-06-23 11:33:21  start kafka success
   2022-06-23 11:33:32  start connector success
   2022-06-23 11:33:42  start mdb-server success
   2022-06-23 11:33:42  MDB启动成功,各程序占用端口如下
   +-------+-----------+-----------+------------+
   | KAFKA | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +-------+-----------+-----------+------------+
   |  9092 |    2181   |    8083   |    55437    |
   +-------+-----------+-----------+------------+
   2022-06-23 11:33:42  MDB各程序服务状态
   +--------+-----------+-----------+------------+
   | KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +--------+-----------+-----------+------------+
   | online |   online  |   online  |   online   |
   +--------+-----------+-----------+------------+
```

启动子服务命令：

```shell
 ./mdb start --help #  --server_name TEXT  可选服务名: zookeeper、kafka、connector、mdb_server
 ./mdb start --server_name zookeeper 
 ./mdb start --server_name kafka 
 ./mdb start --server_name connector 
 ./mdb start --server_name mdb_server 
```

### 查看MDB服务状态

执行查询状态命令：

```shell
 ./mdb status
```

正常执行结果如下：

```text
   2022-06-23 14:06:39  MDB各程序服务状态
   +---------+-----------+-----------+------------+
   |  KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +---------+-----------+-----------+------------+
   | offline |  offline  |  offline  |  offline   |
   +---------+-----------+-----------+------------+
```

### 停用MDB

执行停止命令：

```shell
 ./mdb stop
```

正常执行结果如下：

```text
   2022-06-23 14:02:52  stop connector success
   2022-06-23 14:03:02  stop kafka success
   2022-06-23 14:03:12  stop zookeeper success
   2022-06-23 14:03:22  stop mdb-server success
   2022-06-23 14:03:22  stop mdb-data-worker success
   2022-06-23 14:03:22  MDB各程序服务状态
   +---------+-----------+-----------+------------+
   |  KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +---------+-----------+-----------+------------+
   | offline |  offline  |  offline  |  offline   |
   +---------+-----------+-----------+------------+
```

停止子服务命令：

```shell
 ./mdb stop --help #  --server_name TEXT  可选服务名: zookeeper、kafka、connector、mdb_server
 ./mdb stop --server_name zookeeper 
 ./mdb stop --server_name kafka 
 ./mdb stop --server_name connector 
 ./mdb stop --server_name mdb_server 
```

### 应用config

config.json配置发生变化, 执行该命令更新各个服务的配置文件。执行前需保证服务offline, 请先执行stop命令。

执行应用config命令：

```shell
 ./mdb apply-config  
```

正常执行结果如下：

```text
   2022-08-08 14:54:34  apply config success!
```

### 日志采集

在当前目录生成dump.zip文件。

执行日志采集命令：

```shell
 ./mdb dump
```

正常执行结果如下：

```text
   2022-09-23 10:33:46  dump log success
```

### 登录 MDB

MDB 启动成功后，可在客户端 Web 浏览器中（推荐使用 Chrome 浏览器），检查页面是否正常，检查是否可正常登录：

<http://localhost:55437/mdb/index.html>

- 初始用户名: test@enmotech.com
- 初始密码: Enmo@123

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_18.png?x-oss-process=image/resize,w_1000)
