---
title: 升级说明
summary: MDB 升级说明
author: tianzijian
date: 2022-08-31
---

# MDB 升级

## 升级步骤

### 下载软件

下载最新版 MDB 安装包. 安装包请联系相关销售获取.

### 上传软件并解压

上传 MDB 安装包到服务器，并解压：

```shell
tar -zxvf mdb_v1.0.1.RELEASE_linux_x86_64.tar.gz
```

### 升级前准备

示例: 旧版本MDB安装目录: /u01/mdb_v1.0.0.RELEASE_linux_x86_64

切换到旧版本 MDB 安装目录，停止MDB服务:

```shell
cd /u01/mdb_v1.0.0.RELEASE_linux_x86_64

./mdb stop
```

切换到已解压的 MDB 安装包目录，复制旧版本配置文件：

```shell
cd mdb_v1.0.1.RELEASE_linux_x86_64

# 复制config.json、license.json 到mdb_v1.0.1.RELEASE_linux_x86_64目录中
cp -f /u01/mdb_v1.0.0.RELEASE_linux_x86_64/config.json ./  
cp -f /u01/mdb_v1.0.0.RELEASE_linux_x86_64/license.json ./
```

### 升级 MDB

检查并确保已切换到 MDB 安装包目录：

```shell
pwd         # 确保已经位于 mdb_v1.0.1.RELEASE_linux_x86_64 目录中
```

执行升级命令：

```shell
./mdb upgrade
```

正常执行结果如下：

```text
   2022-08-30 14:36:26  ======BEGIN TO UPGRADE MDB======
   2022-08-30 14:36:26    1.CHECK MDB FILE
   2022-08-30 14:36:26    2.CHECK MDB ENVIRONMENT
   2022-08-30 14:36:26     JAVA_VERSION: 11.0
   2022-08-30 14:36:26     EXIST RESIDUAL DIRS ! PATH: /tmp/kafka-logs ,/tmp/zookeeper
   2022-08-30 14:36:26    3.UNZIP KAFKA
   2022-08-30 14:36:27    4.INSTALL MDB SERVER
   2022-08-30 14:36:28    5.INSTALL MDB DATA WORKER
   2022-08-30 14:36:28  

   ======END UPGRADE MDB RESULT======
   +------------+-----------+-------------+---------------+
   | File_Check | Env_Check | MDB_Install | KAFKA_INSTALL |
   +------------+-----------+-------------+---------------+
   |  Success   |  Success  |   Success   |    Success    |
   +------------+-----------+-------------+---------------+

```

### 启动 MDB

执行启动命令：

```shell
 ./mdb start
```

正常启动的执行结果如下：

```text
   2022-06-23 11:33:10  start zookeeper success
   2022-06-23 11:33:21  start kafka success
   2022-06-23 11:33:32  start connector success
   2022-06-23 11:33:42  start mdb-server success
   2022-06-23 11:33:42  MDB启动成功,各程序占用端口如下
   +-------+-----------+-----------+------------+
   | KAFKA | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +-------+-----------+-----------+------------+
   |  9092 |    2181   |    8083   |    55437    |
   +-------+-----------+-----------+------------+
   2022-06-23 11:33:42  MDB各程序服务状态
   +--------+-----------+-----------+------------+
   | KAFKA  | ZOOKEEPER | CONNECTOR | MDB_SERVER |
   +--------+-----------+-----------+------------+
   | online |   online  |   online  |   online   |
   +--------+-----------+-----------+------------+
```

### 登录 MDB

MDB 启动成功后，可在客户端 Web 浏览器中（推荐使用 Chrome 浏览器），检查页面是否正常，检查是否可使用原用户正常登录：

<http://localhost:55437/mdb/index.html>

登录成功后，在通道列表中重新启动已暂停的服务。
