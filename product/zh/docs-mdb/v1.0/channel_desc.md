---
title: 通道功能描述
summary: MDB 通道功能描述
author: tianzijian
date: 2022-06-30
---

# MDB 通道功能

在 MDB 系统中，通道是一个逻辑单元，通过一系列操作连接源节点和目标节点。

数据复制、调度、错误处理、性能调整和其他操作都发生在通道中。

MDB 将通道内的数据从源节点同步到目标节点。

目前一个通道中仅支持一个源节点和一个目标节点，即实现一对一的数据同步复制。

## 通道列表

​通道列表展示，支持通道名，节点名糊查询

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_5.png?x-oss-process=image/resize,w_1000)

## 新增通道

新增数据同步通道，选择数据从Oracle同步到MogDB。新增通道不能选择两个相同类型的节点

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_4.png?x-oss-process=image/resize,w_1000)

## 删除通道

通道状态不在执行中，删除按钮高亮，可以直接删除。 如果状态处于执行中，不允许删除，需要停止通道中执行的任务，再进行删除操作。

## 通道暂停

通道状态仅为数据同步中可以暂停，仅为数据同步暂停时可以启动。

## 通道详情

展示通道名，状态，同步对象数，同步表数，节点名称。

## 通道-配置

支持修改通道名称。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_6.png?x-oss-process=image/resize,w_1000)

## 通道-对象

### 对象选择

​对象选择，从源节点选择需要同步到目标节点的对象，如果配置映射规则，会经过规则转换。目标对象名大小写会默认转为目标节点对应的规范。
​
![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_7.png?x-oss-process=image/resize,w_1000)

### 对象删除

通道未执行数据同步，可以直接删除已添加的同步对象。如果通道数据已同步，需要停止数据同步，删除后，重新执行对象同步，数据同步。

### 查看字段映射

展示源端和目标端字段映射信息，仅对象类型为TABLE，可以查看。

### 字段映射配置

可以修改目标库schema、表名、字段名。保存后需要重新执行预检查，对象同步，数据同步。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_8.png?x-oss-process=image/resize,w_1000)

### 映射规则

### Schema映射配置

- 可配置多条映射规则，映射规则则会按表格中列出的顺序依次执行,
- 保存后立即生效, 需要重新执行对象同步、数据同步

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_9.png?x-oss-process=image/resize,w_1000)

### 对象映射配置

- 字符替换： 从源库对象名中搜索包含指定字符串的对象，将指定的字符串替换为目标库中的替换字符串。
- 正则替换： 将正则规则应用到所有源库对象名上，如果匹配上源库正则表达式，则替换为目标库中的替换字符串。
- 可配置多条对象映射规则，映射规则则会按表格中列出的顺序依次执行。
- 保存后立即生效, 需要重新执行对象同步、数据同步

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_10.png?x-oss-process=image/resize,w_1000)

## 通道-高级参数

 在MDB中，高级参数控制着通道同步过程中的逻辑，例如skipSnapShot 是否跳过快照，只同步增量数据，overrideMode 是否覆盖目标节点已存在的对象结构等。

| 参数名称             | 默认  | 描述                                   |
|:-----------------|:---:|:-------------------------------------|
| compatibleModel  | ON | 迁移兼容模式： ON-兼容，OFF-不兼容                |
| overrideMode     | NONE | 覆盖模式：NONE_FORCE-不覆盖，NONE-默认，FORCE-覆盖 |
| tableSpaceSwitch | OFF | 是否忽略表空间：  ON-不忽略，OFF-忽略              |
| skipSnapShot     | ON | 是否跳过全量数据：ON-是，OFF-否                  |
| heartbeatIntervalMs     | 0 | 心跳间隔时间(s)：0-不开启  正整数-开启              |
| heartbeatTableName     | heartbeat | 心跳表：在源节点创建心跳表的表名                     |
| skipMigrationObject     | OFF | 是否跳过对象同步                             |
| postgresqlPluginName     | decoderbufs | PostgreSQL插件:decoderbufs, wal2json   |

​![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_11.png?x-oss-process=image/resize,w_1000)

## 通道-同步日志

数据同步操作后，数据捕获和目标节点写入的过程日志。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_19.png?x-oss-process=image/resize,w_1000)

## 通道-操作

### 近期任务列表

通道中，展示同步进度，各阶段执行时间，结果。

### 同步任务

- 执行一键同步 或 依次执行预检查、对象同步、数据同步

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_30.png?x-oss-process=image/resize,w_1000)

- 同步暂停: 暂停数据同步，停止数据抽取和投递服务
- 同步启动: 恢复数据同步，恢复数据抽取和投递服务

### 同步任务详情

- 查看数据同步结果
  
  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_29.png?x-oss-process=image/resize,w_1000)

### 任务失败查看

- 查看预检查异常

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_31.png?x-oss-process=image/resize,w_1000)

- 查看同步对象异常

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_32.png?x-oss-process=image/resize,w_1000)

  目标端SQL 支持编辑、格式化、复制，编辑保存后重新执行，执行成功后异常移除列表。

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_20.png?x-oss-process=image/resize,w_1000)

### 刷新

任务执行后刷新，展示最新通道状态，任务状态。
