---
title: 发布记录
summary: MDB 发布记录
author: tianzijian
date: 2022-06-30
---

# 发布记录

## v1.1.1(2022.12.06)

### Bug Fixes

- 修复了MYSQL5.7环境下查询索引错误的问题
- 修复了MOGDB到MYSQL环境检查错误的问题

## v1.1.0(2022.11.30)

### Features

- 支持从SQL Server到MogDB的对象同步(schema,table)
- 支持从SQL Server到MogDB的对象同步进行自定义名称映射
- 支持从SQL Server到MogDB的全量数据同步以及增量数据同步
- 支持从SQL Server到MogDB的数据同步暂停和继续
- 支持从MogDB到Oracle的全量数据同步以及增量数据同步
- 支持从MogDB到Oracle的增量同步暂停和继续
- 支持从MogDB到MySQL的全量数据同步以及增量数据同步
- 支持从MogDB到MySQL的增量同步暂停和继续
- 支持从MogDB到PostgreSQL的全量数据同步以及增量数据同步
- 支持从MogDB到PostgreSQL的增量同步暂停和继续
- 支持从MogDB到SQL Server的全量数据同步以及增量数据同步
- 支持从MogDB到SQL Server的增量同步暂停和继续
- 节点密码加密
- 对象选择优化
- 新增手动添加对象、数据同步复位功能
- 命令行工具新增临时文件配置

### Bug Fixes

- 修复了时间相关的数据投递异常问题
- 修复了对象选择导致浏览器未响应的问题
- 修复了投递SQL语句组装错误的问题
- 修复了在PG/MOGDB为源的情况下误删除用户已有SLOT的问题
- 修复了投递失败后的数据保存丢失字段的问题
- 修复了ORACLE ROWID类型迁移到MOGDB 后列长度错误的问题
- 修复了对象列名映射名称错误的问题
- 修复了数据同步详情排序错误的问题

## v1.0.5(2022.11.03)

### Bug Fixes

- 修复datetime数据类型同步错误问题

## v1.0.4(2022.10.30)

### Features

- 支持从PostgreSQL到MogDB的对象同步(schema,table)
- 支持从PostgreSQL到MogDB的对象同步进行自定义名称映射
- 支持从PostgreSQL到MogDB的全量数据同步以及增量数据同步
- 支持从PostgreSQL到MogDB的数据同步暂停和继续
- 通道数据同步和增量追踪任务合，通道状态调整
- 新增数据抽取和数据投递服务状态，数据同步异常状态
- 支持异常状态数据同步恢复
- 优化命令行工具检查子服务存活状态的方式
  
### Bug Fixes

- 修复不同schema下同名表对象同步失败场景
- 修复connector连接不存在导致通道暂停失败场景
  
## v1.0.3 (2022.9.23)

### Features

- 支持Oracle字符集: ZHS16GBK
- 新增日志采集命令, 子服务启动停止命令

### Bug Fixes

- 修复schema名或数据库名中存在‘#’，导致数据同步失败场景
- 修复开启心跳，对象选择未过滤心跳表
  
## v1.0.2(2022.9.15)

### Features

- 支持从MySQL到MogDB的对象同步(schema,table)
- 支持从MySQL到MogDB的对象同步进行自定义名称映射
- 支持从MySQL到MogDB的全量数据同步以及增量数据同步
- 支持从MySQL到MogDB的增量同步暂停和继续
- 支持Oracle节点CDB-PDB模式
- 新增通道同步任务详情
- 变更登录URL

## v1.0.1 (2022.8.31)

### Features

- 新增[升级说明](./upgrade.md)

### Bug Fixes

- 解决快照数据同步完成,数据同步任务状态还在"执行中"问题

## v1.0.0 (2022.6.30)

### Features

- 支持从Oracle到MogDB的对象同步(schema,table)
- 支持从Oracle到MogDB的对象同步进行自定义名称映射
- 支持从Oracle到MogDB的全量数据同步以及增量数据同步
- 支持从Oracle到MogDB的增量同步暂停和继续
- 支持通过从浏览器访问mdb进行图形化操作
