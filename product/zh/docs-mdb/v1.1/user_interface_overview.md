---
title: 用户界面概述
summary: MDB 用户界面概述
author: tianzijian
date: 2022-06-30
---

# 用户界面概述

本页介绍MDB web用户界面，帮助您开始使用MDB系统。

## 登录

启动MDB web UI时，用户需要登录，暂未开放用户注册入口，请使用初始用户名，密码登录。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_18.png?x-oss-process=image/resize,w_1000)

## 初始密码重置

第一次登录需要强制重置密码。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_22.png?x-oss-process=image/resize,w_1000)

## 开始页面

登录后展示的是MDB web UI，侧边栏展示节点和通道，第一步从节点开始，新增节点。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_17.png?x-oss-process=image/resize,w_1000)

## 个人中心

用户个人中心展示用户账号、昵称、手机号、密码，支持密码修改。

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_24.png?x-oss-process=image/resize,w_1000)
