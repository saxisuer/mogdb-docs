---
title: 目标库说明
summary: MDB 目标库说明
author: tianzijian
date: 2022-06-30
---

# 目标库说明

## 用户权限

以下命令中，假设 mdb_user 为 MDB 在目标端的登录用户，若实际环境中用户名称与此不同，则需要按照实际情况下修改。

### Oracle

Oracle 作为目标库，直接授予 dba 角色：

```sql
grant dba to mdb_user;
```

### MySQL

MySQL 作为目标库，直接授予所有权限：

```sql
GRANT ALL PRIVILEGES ON *.* TO "mdb_user"@"%" IDENTIFIED BY "Enmo@123" WITH GRANT OPTION; 
```

### PostgreSQL

PostgreSQL 作为目标库，直接授予 superuser 权限：

```sql
alter role mdb_user with superuser;
```

### SQL Server

SQL Server 作为目标库，直接授予 dba 权限：

```sql
exec sp_addrolemember 'db_owner','mdb_user';
```

### MogDB

MogDB 作为目标库，直接给与 sysadmin 权限：

```sql
alter user mdb_user with sysadmin;
```

创建MogDB数据库需指定兼容的数据库的类型，默认兼容A

```sql
create database db1 DBCOMPATIBILITY='A';
```

| 源库      | 目标库  | 兼容值 |
 |--------|-----|----------|
| Oracle|MogDB| A   |
| MySQL|MogDB| B   |
| PostgreSQL|MogDB| PG  |
