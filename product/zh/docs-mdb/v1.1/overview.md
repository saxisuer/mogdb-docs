---
title: MDB 介绍
summary: MDB 介绍
author: tianzijian
date: 2022-06-30
---

# MDB 介绍

MDB 全称 MogDB Data Bridge，是一款异构数据库迁移同步工具。

用于 MogDB/openGauss 以及同类基于 openGauss 的数据库与其他异构数据库（Oracle, DB2, MySQL, PostgreSQL等）之间的数据迁移和同步。

## 架构

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_21.png?x-oss-process=image/resize,h_800)

## 支持数据库类型

| 源数据库                         | 目标数据库      | 是否支持| 预计支持版本 |
|:-----------------------------|:-----------|:----|:-------|
| [Oracle](./source_oracle.md) | [MogDB](./target_db.md#mogdb)| 支持  ||
| [MogDB](./source_mogdb.md)  | [Oracle](./target_db.md#oracle) | 支持(仅支持数据同步)  |   |
| [MySQL](./source_mysql.md)   | [MogDB](./target_db.md#mogdb) | 支持 ||
| [MogDB](./source_mogdb.md)   | [MySQL](./target_db.md#mysql) | 支持(仅支持数据同步) | |
| [PostgreSQL](./source_postgresql.md)   | [MogDB](./target_db.md#mogdb) | 支持 ||
| [MogDB](./source_mogdb.md)   | [PostgreSQL](./target_db.md#postgresql) | 支持(仅支持数据同步) |   |
| [SQL Server](./source_sqlserver.md)   | [MogDB](./target_db.md#mogdb) | 支持 ||
| [MogDB](./source_mogdb.md)   | [SQL Server](./target_db.md#sqlserver) | 支持(仅支持数据同步) |   |

## 支持数据库对象

| 对象名称       | 是否支持 | 说明  | 预计支持版本 |
|---------------|--------|-----|--------|
| Schema        | 支持   |     ||
| Table         | 支持   |     ||
| Constraint   | 支持   | 不支持外键 ||
| Index         | 支持   |     ||
| TableData   | 支持   |表数据     ||
| User   | 未支持   |    | v2.2   |
| Sequence   | 未支持   |    | v2.2   |
| Synonyms   | 未支持   |    | v2.2   |

## 不支持数据库对象

| 对象名称        | 是否支持 | 说明  |
|---------------|--------|-----|
| Trigger        |  不支持  |     |
| Procedure |  不支持  |     |
| Function   | 不支持   |  |
| Package        |  不支持  |     |
| View        |  不支持  |     |
