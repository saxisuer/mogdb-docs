---
title: 节点功能描述
summary: MDB 节点功能描述
author: tianzijian
date: 2022-06-30
---

# MDB 节点功能

​在 MDB 系统中，节点是复制过程中的端点，对应一个数据库系统。

节点可以是本地（即与 MDB 系统位于同一台机器上）或远程（位于远程机器上）。

节点可以作为源库使用，即从节点中进行数据抓取，也可以作为目标库使用，即将数据同步到节点数据库中。

## 节点列表

支持节点名、用户名、IP、服务名模糊查询, 当前仅支持MogDB、Oracle、MySQL

![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_3.png?x-oss-process=image/resize,w_1000)

## 新增节点

- 新增Oracle节点

    Oracle节点可以选择连接类型，ServiceName 或 SID，点击测试连接，测试成功后点击确定，如果通道使用Oracle节点作为源节点，请先参考源库说明菜单中的Oracle说明，数据库配置和用户权限说明。如果作为目标节点使用，建议参考目标库说明

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_1.png?x-oss-process=image/resize,w_1000)

- 新增MogDB节点

    MogDB节点添加完成后，点击测试连接，测试成功后点击确定，如果通道使用MogDB节点作为源节点，请先参考源库说明菜单中的MogDB说明，数据库配置和用户权限说明。如果作为目标节点使用，建议参考目标库说明

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_2.png?x-oss-process=image/resize,w_1000)

- 新增MySQL节点

    MySQL节点添加完成后，点击测试连接，测试成功后点击确定，如果通道使用MySQL节点作为源节点，请先参考源库说明菜单中的MySQL说明，数据库配置和用户权限说明。如果作为目标节点使用，建议参考目标库说明

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_25.png?x-oss-process=image/resize,w_1000)

- 新增PostgreSQL节点

    PostgreSQL节点添加完成后，点击测试连接，测试成功后点击确定，如果通道使用PostgreSQL节点作为源节点，请先参考源库说明菜单中的PostgreSQL说明，数据库配置和用户权限说明。如果作为目标节点使用，建议参考目标库说明

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_28.png?x-oss-process=image/resize,w_1000)

- 新增SQL Server节点

    SQL Server节点添加完成后，点击测试连接，测试成功后点击确定，如果通道使用SQL Server节点作为源节点，请先参考源库说明菜单中的SQL Server说明，数据库配置和用户权限说明。如果作为目标节点使用，建议参考目标库说明

    ![img](https://cdn-mogdb.enmotech.com/docs-media/mdb/v1.0/img_33.png?x-oss-process=image/resize,w_1000)

## 更新节点

如果节点未被通道使用，更新后，连接测试通过保存，如果节点已被通道使用，需要暂停数据同步，节点更新后，通道内需要重新执行相关任务。

## 连接测试/测试链接

节点连通性测试。连接失败会返回连接异常信息。

## 删除节点

节点未被通道使用，可以直接删除。如果节点已被通道使用，需要先删除通道，再删除节点。
