---
title: SQL Server源库说明
summary: MDB SQL Server源库说明
author: tianzijian
date: 2022-11-23
---

# SQL Server为源库

## 支持版本

2016(SP1)及以后版本

## 数据库设置

### 用户权限

开启CDC所需权限：

1、用户是SQL Server的sysadmin固定服务器角色的成员。</br>
2、用户是数据库的db_owner。

### 开启数据库CDC

1、在SQL Server Management Studio的“视图”菜单中，单击“模板资源管理器”。</br>
2、在模板浏览器中，展开SQL Server模板。</br>
3、展开Change Data Capture>Configuration，然后单击Enable Database for CDC。</br>
4、在模板中，用要为CDC启用的数据库名称替换USE语句中的数据库名称。</br>
5、运行存储过程sys.sp_cdc_enable_db启用数据库CDC。

示例：为数据库MyDB启用CDC

```sql
    USE MyDB -- MyDB: 数据库名
    GO
    EXEC sys.sp_cdc_enable_db
    GO
```

### 开启同步表CDC

1、在SQL Server Management Studio的“视图”菜单中，单击“模板资源管理器”。</br>
2、在模板浏览器中，展开SQL Server模板。</br>
3、展开Change Data Capture>Configuration，然后单击启Enable Table Specifying Filegroup Option。</br>
4、在模板中，用要同步的表的名称替换USE语句中的表名。</br>
5、运行存储过程sys.sp_cdc_enable_table。

示例：为同步表MyTable启用CDC

```sql
    USE MyDB -- MyDB: 已开启CDC数据库名
    GO

    EXEC sys.sp_cdc_enable_table
    @source_schema = N'mySchema', -- mySchema: 同步表所属schema名
    @source_name   = N'myTable',  -- myTable: 同步表名
    @role_name     = NULL,  
    @supports_net_changes = 0
    GO
```

### 同步表结构变更

如果已开启CDC的同步表，表结构发生变化，需要重新执行新存储过程。

示例：mySchema.myTable 表结构发生变化

```sql
ALTER TABLE myTable ADD phone_number VARCHAR(32);
```

1、通过运行sys.sp_cdc_enable_table存储过程创建新的捕获实例。

```sql
    EXEC sys.sp_cdc_enable_table 
    @source_schema = 'mySchema', -- schema
    @source_name = 'myTable',  -- table
    @role_name = NULL, 
    @supports_net_changes = 0, 
    @capture_instance = 'mySchema_myTable_v2'; -- 新实例名与旧实例名不能重名，默认实例名:mySchema_myTable
```

2、 通过运行sys.sp_cdc_disable_table存储过程删除旧的捕获实例。

```sql
    EXEC sys.sp_cdc_disable_table 
    @source_schema = 'mySchema', -- schema
    @source_name = 'myTable',  -- table
    @capture_instance = 'mySchema_myTable'; -- 默认实例名:mySchema_myTable
    GO
```

## 数据类型映射

| 源库      | 目标库  | 源库类型 | 目标库类型     |
|--------|-----------|----------|-----------|
|SQL Server|MogDB|bigint|bigint|
|SQL Server|MogDB|numeric|numeric|
|SQL Server|MogDB|bit|bytea|
|SQL Server|MogDB|smallint|smallint|
|SQL Server|MogDB|decimal|numeric|
|SQL Server|MogDB|smallmoney|numeric|
|SQL Server|MogDB|tinyint|tinyint|
|SQL Server|MogDB|money|numeric|
|SQL Server|MogDB|float|float|
|SQL Server|MogDB|real|real|
|SQL Server|MogDB|date|date|
|SQL Server|MogDB|datetime|timestamp|
|SQL Server|MogDB|datetime2|timestamp|
|SQL Server|MogDB|datetimeoffset|timestamptz|
|SQL Server|MogDB|smalldatetime|smalldatetime|
|SQL Server|MogDB|time|time|
|SQL Server|MogDB|char|character|
|SQL Server|MogDB|varchar|character varying|
|SQL Server|MogDB|text|text|
|SQL Server|MogDB|nchar|char|
|SQL Server|MogDB|nvarchar|character varying|
|SQL Server|MogDB|ntext|text|
|SQL Server|MogDB|binary|bytea|
|SQL Server|MogDB|varbinary|bytea|
|SQL Server|MogDB|image|bytea|
|SQL Server|MogDB|xml|text|
|SQL Server|MogDB|geography|bytea|
|SQL Server|MogDB|geometry|bytea|
|SQL Server|MogDB|rowversion|bytea|
|SQL Server|MogDB|hierarchyid|bytea|
|SQL Server|MogDB|uniqueidentifier|text|
|SQL Server|MogDB|sql_variant|text|
|SQL Server|MogDB|sysname|character varying|
|SQL Server|MogDB|int|integer|

> 默认字段类型映射,字段类型映射下个版本支持

## 字符集映射

| 源库      | 目标库  | 源库字符集          | 目标库字符集    |
 |--------|----------------|----------|-----------|
| SQL Server|MogDB|            |UTF8|
