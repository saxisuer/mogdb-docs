---
title: 启停MogDB
summary: 启停MogDB
author: Guo Huan
date: 2021-06-24
---

# 启停MogDB

## 启动MogDB

1. 以操作系统用户omm登录数据库主节点。

2. 使用以下命令启动MogDB。

   ```bash
   gs_om -t start
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  双机启动必须以双机模式启动，若中间过程以单机模式启动，则必须修复才能恢复双机关系，用gs_ctl build进行修复，gs_ctl的使用方法参见“参考指南 > 工具参考 > 系统内部使用的工具 > [gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md)”章节。

## 停止MogDB

1. 以操作系统用户omm登录数据库主节点。

2. 使用以下命令停止MogDB。

   ```bash
   gs_om -t stop
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   > 启停节点及AZ的操作请参见“参考指南 > 工具参考 > 服务端工具 > [gs_om](../../reference-guide/tool-reference/server-tools/8-gs_om.md)”章节。

## 示例

启动MogDB：

```bash
gs_om -t start
Starting cluster.
=========================================
=========================================
Successfully started.
```

停止MogDB：

```bash
gs_om -t stop
Stopping cluster.
=========================================
Successfully stopped cluster.
=========================================
End stop cluster.
```

## 错误排查

如果启动MogDB或者停止MogDB服务失败，请根据日志文件中的日志信息排查错误，参见[日志参考](11-log-reference.md)。

如果是超时导致启动失败，可以执行如下命令，设置启动超时时间，默认超时时间为300s。

```bash
gs_om -t start --time-out=300
```
