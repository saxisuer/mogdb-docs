---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 检查操作系统参数

## 检查办法

通过MogDB提供的gs_checkos工具可以完成操作系统状态检查。

**前提条件**

- 当前的硬件和网络环境正常。
- 各主机间root互信状态正常。
- 只能使用root用户执行gs_checkos命令。

**操作步骤**

1. 以root用户身份登录任意一台服务器。

2. 执行如下命令对MogDB节点服务器的OS参数进行检查。

    ```
    gs_checkos -i A
    ```

    检查节点服务器的OS参数的目的是保证MogDB正常通过预安装，并且在安装成功后可以安全高效的运行。详细的检查项目请参见《参考指南》中“工具参考 > 服务端工具 > [gs_checkos](../../reference-guide/tool-reference/server-tools/2-gs_checkos.md)”章节。

**示例**

执行gs_checkos前需要先使用gs_preinstall工具执行前置脚本，准备环境。以参数“A”为例。

```
gs_checkos -i A
Checking items:
    A1. [ OS version status ]                                   : Normal
    A2. [ Kernel version status ]                               : Normal
    A3. [ Unicode status ]                                      : Normal
    A4. [ Time zone status ]                                    : Normal
    A5. [ Swap memory status ]                                  : Normal
    A6. [ System control parameters status ]                    : Normal
    A7. [ File system configuration status ]                    : Normal
    A8. [ Disk configuration status ]                           : Normal
    A9. [ Pre-read block size status ]                          : Normal
    A10.[ IO scheduler status ]                                 : Normal
    A11.[ Network card configuration status ]                   : Normal
    A12.[ Time consistency status ]                             : Warning
    A13.[ Firewall service status ]                             : Normal
    A14.[ THP service status ]                                  : Normal
Total numbers:14. Abnormal numbers:0. Warning number:1.
```

以参数“B”为例。

```
gs_checkos -i B
Setting items:
    B1. [ Set system control parameters ]                       : Normal
    B2. [ Set file system configuration value ]                 : Normal
    B3. [ Set pre-read block size value ]                       : Normal
    B4. [ Set IO scheduler value ]                              : Normal
    B5. [ Set network card configuration value ]                : Normal
    B6. [ Set THP service ]                                     : Normal
    B7. [ Set RemoveIPC value ]                                 : Normal
    B8. [ Set Session Process ]                                 : Normal
Total numbers:6. Abnormal numbers:0. Warning number:0.
```

<br/>

## 异常处理

使用gs_checkos检查或设置状态为Abnormal，可以使用如下命令查看详细的错误信息。

```
gs_checkos -i A --detail
```

其中，Abnormal为必须处理项，影响MogDB安装。Warning可以不处理，不会影响MogDB安装。

- 如果操作系统版本（A1）检查项检查结果为Abnormal，需要将不属于混编范围的操作系统版本替换为混编范围内的操作系统版本。

- 如果内核版本（A2）检查项检查结果为Warning，则表示MogDB内平台的内核版本不一致。

- 如果Unicode状态（A3）检查项检查结果为Abnormal，需要将各主机的字符集设置为相同的字符集，可以在/etc/profile文件中添加“export LANG=XXX”（XXX为Unicode编码）。

    ```
    vim /etc/profile
    ```

- 如果时区状态（A4）检查项检查结果为Abnormal，需要将各主机的时区设置为相同时区，可以将/usr/share/zoneinfo/目录下的时区文件拷贝为/etc/localtime文件。

    ```
    cp /usr/share/zoneinfo/$地区/$时区 /etc/localtime
    ```

- 如果交换内存状态（A5）检查项检查结果为Abnormal，可能是因为swap空间大于mem空间，可减小Swap解决或者增大Mem空间解决。

- 如果系统控制参数（A6）检查项检查结果为Abnormal，可以使用以下两种方法进行设置。

  - 可以使用如下命令进行设置。

    ```
    gs_checkos -i B1
    ```

  - 根据错误提示信息，在/etc/sysctl.conf文件中进行设置。然后执行sysctl -p使其生效。

    ```
    vim /etc/sysctl.conf
    ```

- 如果文件系统配置状态（A7）检查项检查结果为Abnormal，可以使用如下命令进行设置。

    ```
    gs_checkos -i B2
    ```

- 如果磁盘配置状态（A8）检查项检查结果为Abnormal，需修改磁盘挂载格式为: "rw,noatime,inode64,allocsize=16m"。

    使用linux的man mount命令挂载XFS选项:

    ```
    rw,noatime,inode64,allocsize=16m
    ```

    也可以在/etc/fstab文件中设定XFS选项。如下示例:

    ```
    /dev/data /data xfs rw,noatime,inode64,allocsize=16m 0 0
    ```

- 如果预读块大小（A9）检查项检查结果为Abnormal，可以使用如下命令进行设置。

    ```
    gs_checkos -i B3
    ```

- 如果IO调度状态（A10）检查项检查结果为Abnormal，可以使用如下命令进行设置。

    ```
    gs_checkos -i B4
    ```

- 如果网卡配置状态（A11）检查项检查结果为Warning，可以使用如下命令进行设置。

    ```
    gs_checkos -i B5
    ```

- 如果时间一致性（A12）检查项检查结果为Abnormal，需检查是否安装ntp服务，以及ntp服务是否启动；并与ntp时钟源同步。

- 如果防火墙状态（A13）检查项检查结果为Abnormal，需关闭防火墙服务。使用如下命令进行设置。

  - SuSE:

    ```
    SuSEfirewall2 stop
    ```

  - RedHat7:

    ```
    systemctl disable firewalld
    ```

  - RedHat6:

    ```
    service iptables stop
    ```

- 如果THP服务（A14）检查项检查结果为Abnormal，可以使用如下命令进行设置。

    ```
    gs_checkos -i B6
    ```
