---
title: 升级指南
summary: 升级指南
author: Zhang Cuiping
date: 2021-09-27
---

# 升级指南

## 概述

本文档详细的描述了版本升级、回滚流程、以及具体的操作指导，同时提供了常见的问题解答及故障处理方法。

## 读者对象

本文档主要适用于升级的操作人员。操作人员必须具备以下经验和技能:

- 熟悉当前网络的组网和相关网元的版本信息。
- 有该设备维护经验，熟悉设备的操作维护方式。

## 升级方案

本节为指导用户选择升级方式。

用户根据MogDB提供的新特性和数据库现状，确定是否对现有系统进行升级。

当前支持的升级模式为就地升级和灰度升级。升级方式的策略又分为大版本升级和小版本升级。

用户挑选升级方式后，系统会自动判断并选择合适的升级策略。

* 就地升级: 升级期间需停止业务进行，一次性升级所有节点。

* 灰度升级: 灰度升级支持全业务操作，也是一次性升级所有节点（目前仅支持从1.1.0版本到2.0及以上版本进行灰度升级）。

## 升级前的版本要求（升级路径）

MogDB升级版本要求如[表1](#biaoyi)所示。

**表 1**  升级前的版本要求（升级路径）<a id="biaoyi"> </a>

| 版本                      | 升级说明                            |
| ------------------------- | ----------------------------------- |
| MogDB 1.1.0版本之后的版本 | 可以升级到MogDB 1.1.0之后的任意版本 |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 升级前版本，可以通过执行如下工具查看。
>
> ```bash
> gsql -V | --version
> ```

## 升级影响和升级约束

升级过程需要注意以下事项。

- 升级操作不能和扩容、缩容同时执行。
- 不支持虚拟IP。
- 升级过程中，不允许对wal_level，max_connections，max_prepared_transactions，max_locks_per_transaction这四个GUC参数的值进行修改。如果修改，会导致回滚后实例启动异常。
- 建议在数据库系统空闲情况下进行升级，尽量避开业务繁忙的时间段（可按照经验判断，如节假日等）。
- 升级前尽可能保证数据库正常。可以通过gs_om -t status查询，查询结果的cluster_state为Normal代表数据库正常。
- 升级前保证数据库互信正常，可以在任意节点上，通过ssh hostname命令，连接另外一个节点进行验证。如果各机器间互连不用输入密码，说明互信正常（通常数据库状态正常时，互信一般都是正常的）。
- 升级前后，数据库的部署方式（配置文件）不能发生变化。升级前会对部署方式进行校验，如果改变，会报错。
- 升级前要保证操作系统处于健康状态，通过gs_checkos工具可以完成操作系统状态检查。
- 就地升级需要停止业务，灰度升级支持全业务操作。
- 数据库运行正常且主DN的数据完全同步到备DN。
- 升级过程中不允许打开kerberos开关。
- 请不要修改安装包中解压出来的version.cfg文件。
- 如果升级过程中出现异常导致升级失败，需用户手动回滚，并且必须回滚成功后才能进行下一次升级。
- 如果升级回滚成功后，再次升级成功，未提交阶段设置的GUC参数将失效。
- 执行升级的过程中请不要手动设置GUC参数。
- 灰度升级中，升级的时候都会产生不超过10s的业务中断
- 升级过程中，必须保持内核版本与om版本一致才可执行om操作。这里的一致是指，内核代码和om代码都来自同一个软件包。如果执行了升级包的前置脚本却没有升级，或者升级回滚后没有执行基线包的前置脚本，就会造成内核代码和om代码的不一致。
- 升级过程中如果系统表新增了字段，升级后通过**\d**命令将查看不到这些新增的字段。此时通过**select**命令可以查到这些新增的字段。
- 升级需要guc参数enable_stream_replication=on，该参数为off时不允许升级。
- 灰度升级中，业务并发要小于200并发读加200并发写的情况。
- 若在MogDB 2.0之前的版本中使用了MOT表，则不支持升级到MogDB 2.0版本。

## 升级流程

本章介绍升级到该版本的主要升级过程。

**图 1**  升级流程图

![12](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/upgrade-guide-1.png)

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明:** 本文档中描述的时间仅供参考，实际操作时间以现场情况为准。

**表 2**  升级流程执行效率估计

<table> <tr> <td>  步骤</td> <td>  建议起始时间</td> <td> 耗时（天/小时/分钟） </td> <td> 业务中断时长 </td> <td>  备注 </td> </tr> <tr> <td>升级前准备与检查  </td> <td>  升级操作前一天</td> <td>  约2～3小时。 </td> <td> 对业务无影响。 </td> <td> 升级前检查和备份数据、校验软件包等操作。 </td> </tr> <tr> <td> 升级操作 </td> <td> 业务空闲期 </td> <td>  耗时主要集中在数据库的启动和停止以及每个database的系统表修改处。升级操作耗时一般不会超过30分钟。</td> <td>与操作时长一致，一般不会超过30分钟。 </td> <td>  依据指导书开始升级。</td> </tr> <tr> <td> 升级验证 </td> <td> 业务空闲期</td> <td> 约30分钟。  </td> <td> 与操作时长一致，约30分钟。 </td> <td> - </td> </tr> <tr> <td> 提交升级 </td> <td> 业务空闲期 </td> <td>  提交升级耗时一般不超过10分钟。</td> <td> 与操作时长一致，一般不超过10分钟。 </td> <td> - </td> </tr> <tr> <td>升级版本回滚  </td> <td> 业务空闲期 </td> <td> 版本回滚耗时一般不会超过30分钟。 </td> <td> 与操作时长一致，一般不会超过30分钟。 </td> <td> - </td> </tr> </table>

## 升级前准备与检查

### 升级前准备与检查清单

**表 3**  升级前准备清单

<table> <tr> <td>  序号</td> <td>  升级准备项目项目</td> <td> 准备内容 </td> <td> 建议起始时间 </td> <td>   耗时（天/小时/分钟） </td> </tr> <tr> <td>1  </td> <td>   收集节点信息</td> <td>  收集到数据库涉及节点的名称、IP地址，root、omm用户密码等环境信息。 </td> <td>升级前一天 </td> <td> 1小时 </td> </tr> <tr> <td> 2 </td> <td>  设置root用户远程登录 </td> <td>  设置配置文件，允许root用户远程登录。</td> <td>升级前一天 </td> <td>  2小时</td> </tr> <tr> <td>3 </td> <td> 备份数据</td> <td> 参考《管理指南》中的“备份与恢复”章节进行。  </td> <td> 升级前一天 </td> <td>备份数据量和方案不同，耗时也不同 </td> </tr> <tr> <td>4 </td> <td> 获取并校验升级包 </td> <td>  获取升级软件包，进行完整性校验。</td> <td>   升级前一天 </td> <td>0.5小时 </td> </tr> <tr> <td>5 </td> <td> 健康检查 </td> <td> 使用gs_checkos工具完成操作系统状态检查。 </td> <td> 升级前一天 </td> <td>  0.5小时 </td> </tr> <tr> <td>6 </td> <td>  检查数据库节点磁盘使用率</td> <td>  使用df命令查看磁盘使用率。</td> <td>   升级前一天 </td> <td> 0.5小时</td> </tr> <tr> <td>7 </td> <td> 检查数据库状态 </td> <td> 使用gs_om工具完成数据库状态检查。 </td> <td> 升级前一天 </td> <td> 0.5小时 </td> </tr> </table>

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明:** “耗时”依不同环境（包括现场数据量、服务器性能等原因)会存在一定差异。

### 收集节点信息

联系数据库系统管理员，获取数据库涉及节点的节点名称、节点IP地址。节点的root、omm用户密码等环境信息。

**表 4**  节点信息<a id="biaosi"> </a>

<table>
<tr>
<td>  序号</td>
<td>  节点名称</td>
<td> 节点IP </td>
<td>
root用户密码</td>
<td>  omm用户密码</td>
<td> 备注 </td>
</tr>
<tr>
<td>1  </td>
<td>
-</td>
<td>
- </td>
<td>  -</td>
<td>  -</td>
<td> - </td>
</tr>
</table>

### 备份数据

升级一旦失败，有可能会影响到业务的正常开展。提前备份数据，就可以在风险发生后，尽快的恢复业务。

请参考《管理指南》中的“备份与恢复”章节，完成数据的备份。

### 获取升级包

访问[此页面](https://www.mogdb.io/downloads/mogdb)获取想要升级的升级包。

### 健康检查

通过gs_checkos工具可以完成操作系统状态检查。

**前提条件**

- 当前的硬件和网络环境正常。
- 各主机间root互信状态正常。
- 只能使用root用户执行gs_checkos命令。

**操作步骤**

1. 以root用户身份登录服务器。

2. 执行如下命令对服务器的OS参数进行检查。

    ```
    # gs_checkos -i A
    ```

    检查服务器的OS参数的目的是为了保证数据库正常通过预安装，并且在安装成功后可以安全高效的运行。

#### 检查数据库节点磁盘使用率

建议数据库节点磁盘使用率低于80%时再执行升级操作。

#### 检查数据库状态

本节介绍数据库状态查询的具体操作。

**验证步骤**

1. 以数据库用户（如omm）登录节点，source环境变量。

    ```
    # su - omm
    $ source /home/omm/.bashrc
    ```

2. 执行如下命令查看数据库状态。

    ```bash
    gs_om -t status
    ```

3. 保证数据库状态正常。

## 升级操作

介绍就地升级和灰度升级的详细操作。

**操作步骤**

1. 以root身份登录节点。

2. 创建新包目录。

    ```
    # mkdir -p /opt/software/mogdb_upgrade
    ```

3. 将需要更新的新包上传至目录"/opt/software/mogdb_upgrade"并解压。

4. 进入安装包解压出的script目录下:

    ```
    # cd /opt/software/mogdb_upgrade/script
    ```

5. 在就地升级或灰度升级前执行前置脚本gs_preinstall。<a id="qianzhijiaoben"> </a>

    ```
    # ./gs_preinstall -U omm -G dbgrp  -X /opt/software/mogdb/clusterconfig.xml
    ```

6. 切换至omm用户。

    ```
    # su - omm
    ```

7. 数据库状态正常时，使用如下命令进行就地升级或者灰度升级。

    示例一: 使用gs_upgradectl脚本执行就地升级。

    ```bash
    gs_upgradectl -t auto-upgrade -X /opt/software/mogdb/clusterconfig.xml
    ```

    示例二: 使用gs_upgradectl脚本执行灰度升级。

    ```bash
    gs_upgradectl -t auto-upgrade -X /opt/software/mogdb/clusterconfig.xml --grey
    ```

## 升级验证

本章介绍升级完成后的验证操作。给出验证的用例和详细操作步骤。

### 验证项目的检查表

**表 5**  验证项目的检查表

<table> <tr> <td>  序号</td> <td>     验证项目</td> <td>    检查标准</td> <td>     检查结果</td> </tr> <tr> <td>1  </td> <td>     版本查询</td> <td>   查询升级后版本是否正确 </td> <td>  -</td> </tr> <tr> <td>2 </td> <td>     健康检查</td> <td>    使用gs_checkos工具完成操作系统状态检查。</td> <td>  -</td> </tr> <tr> <td>3  </td> <td>     数据库状态</td> <td>    使用gs_om工具完成数据库状态检查。</td> <td>  -</td> </tr> </table>

### 升级版本查询

本节介绍版本查询的具体操作。

**验证步骤**

1. 以数据库用户（如omm）登录节点，source环境变量。

    ```
    # su - omm
    $ source /home/omm/.bashrc
    ```

2. 执行如下命令查看所有节点的版本信息。

    ```bash
    gs_ssh -c "gsql -V"
    ```

### 检查升级数据库状态

本节介绍数据库状态查询的具体操作。

**验证步骤**

1. 以数据库用户（如omm）登录节点。

    ```
    # su - omm
    ```

2. 执行如下命令查看数据库状态。

    ```bash
    gs_om -t status
    ```

    查询结果的cluster_state为Normal代表数据库正常。

## 提交升级

升级完成后，如果验证也没问题。接下来就可以提交升级。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：一旦提交操作完成，则不能再执行回滚操作。

**操作步骤**

1. 以数据库用户（如omm）登录节点。

    ```
    # su - omm
    ```

2. 执行如下命令完成升级提交。

    ```bash
    gs_upgradectl -t commit-upgrade  -X /opt/software/mogdb/clusterconfig.xml
    ```

3. 重置控制文件格式以兼容2.1.0版本中新增的ustore存储引擎（仅限于2.0.1升级到2.1）。

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif)**警告**:
   >
   > - 此操作不可回退，执行后无法再降级回2.0.1版本。
   > - 执行此操作前，建议参考[逻辑备份恢复](../administrator-guide/br/1-3-br.md)做一次数据全备份。

   ```
   pg_resetxlog -f $PGDATA
   ```

   回显为：

   ```
   Transaction log reset
   ```

## 升级版本回滚

本章介绍版本回滚方法。

**操作步骤**

1. 以数据库用户（如omm）登录节点。

    ```
    # su - omm
    ```

2. 执行如下命令完成版本回滚（回滚内核代码）。回滚完成，如果需要保持内核和om代码的版本一致，可以执行一下旧包的前置命令（参见[执行前置脚本gs_preinstall](#qianzhijiaoben))。

    ```bash
    gs_upgradectl -t auto-rollback  -X /opt/software/mogdb/clusterconfig.xml
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 如果数据库异常，需要强制回滚，可以使用如下命令。
    >
    > ```bash
    >    gs_upgradectl -t auto-rollback -X /opt/software/mogdb/clusterconfig.xml   --force
    > ```

3. 查看回滚之后的版本号。

    ```bash
    gs_om -V | --version
    ```

    如果升级失败，请按照如下方式进行处理:

    a. 排查是否有环境问题。

    如磁盘满、网络故障等，或者升级包、升级版本号是否正确。排除问题后，可以尝试重入升级。

    b. 如果没有发现环境问题，或者重入升级失败，需要收集相关日志，找技术支持工程师定位。

    收集日志命令:

    ```bash
    gs_collector -begin-time='20200724 00:00'  -end-time='20200725 00:00'
    ```

    如果条件允许，建议保留环境。
