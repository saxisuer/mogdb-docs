---
title: 无权限角色导出数据
summary: 无权限角色导出数据
author: Guo Huan
date: 2021-03-04
---

# 无权限角色导出数据

gs_dump和gs_dumpall通过-U指定执行导出的用户帐户。如果当前使用的帐户不具备导出所要求的权限时，会无法导出数据。此时，需要将有权限的用户赋权给无权限用户，然后可在导出命令中设置-role参数来指定具备权限的角色。在执行命令后，gs_dump和gs_dumpall会使用-role参数指定的角色，完成导出动作。

<br/>

## 操作步骤

1. 以操作系统用户omm登录数据库主节点。

2. 使用gs_dump导出human_resource数据库数据。

   用户jack不具备导出数据库human_resource的权限，而角色role1具备该权限，要实现导出数据库human_resource，需要将role1赋权给jack，然后可以在导出命令中设置-role角色为role1，使用role1的权限，完成导出目的。导出文件格式为tar归档格式。

    ```bash
   gs_dump -U jack -f /home/omm/backup/MPPDB_backup.tar -p 8000 human_resource --role role1 --rolepassword  abc@1234 -F t
    Password:
    ```

    **表 1** 常用参数说明

   | 参数   | 参数说明        | 举例      |
   | :------- | :----------------------------------------------------------------- | :----------------------|
   | -U            | 连接数据库的用户名。                                                                                                                                                                     | -U jack                              |
   | -W            | 指定用户连接的密码。<br/>- 如果主机的认证策略是trust，则不会对数据库管理员进行密码验证，即无需输入-W选项。<br/>- 如果没有-W选项，并且不是数据库管理员，会提示用户输入密码。              | -W Bigdata@123                       |
   | -f            | 将导出文件发送至指定目录文件夹。如果这里省略，则使用标准输出。                                                                                                                           | -f /home/omm/backup/MPPDB_backup.tar |
   | -p            | 指定服务器所监听的TCP端口或本地Unix域套接字后缀，以确保连接。                                                                                                                            | -p 8000                              |
   | dbname        | 需要导出的数据库名称                                                                                                                                                                     | human_resource                       |
   | -role         | 指定导出使用的角色名。选择该选项，会使导出工具连接数据库后，发起一个SET ROLE角色名命令。当所授权用户（由-U指定）没有导出工具要求的权限时，该选项会起到作用，即切换到具备相应权限的角色。 | -r role1                             |
   | -rolepassword | 指定具体角色用户的角色密码。                                                                                                                                                             | -rolepassword abc@1234               |
   | -F            | 选择导出文件格式。-F参数值如下: <br/>- p: 纯文本格式<br/>- c: 自定义归档<br/>- d: 目录归档格式<br/>- t: tar归档格式                                                                      | -F t                                 |

    其他参数说明请参见《参考指南》中“工具参考 > 服务端工具 > [gs_dump](../../../reference-guide/tool-reference/server-tools/5-gs_dump.md)”章节或“[gs_dumpall](../../../reference-guide/tool-reference/server-tools/6-gs_dumpall.md)”章节。

<br/>

## 示例

示例一: 执行gs_dump导出数据，用户jack不具备导出数据库human_resource的权限，而角色role1具备该权限，要实现导出数据库human_resource，可以在导出命令中设置-role角色为role1，使用role1的权限，完成导出目的。导出文件格式为tar归档格式。

```bash
$ human_resource=# CREATE USER jack IDENTIFIED BY "1234@abc";
CREATE ROLE
human_resource=# GRANT role1 TO jack;
GRANT ROLE

$ gs_dump -U jack -f /home/omm/backup/MPPDB_backup11.tar -p 8000 human_resource --role role1 --rolepassword abc@1234 -F t
Password:
gs_dump[port='8000'][human_resource][2017-07-21 16:21:10]: dump database human_resource successfully
gs_dump[port='8000'][human_resource][2017-07-21 16:21:10]: total time: 4239  ms
```

示例二: 执行gs_dump导出数据，用户jack不具备导出模式public的权限，而角色role1具备该权限，要实现导出模式public，可以在导出命令中设置-role角色为role1，使用role1的权限，完成导出目的。导出文件格式为tar归档格式。

```bash
$ human_resource=# CREATE USER jack IDENTIFIED BY "1234@abc";
CREATE ROLE
human_resource=# GRANT role1 TO jack;
GRANT ROLE

$ gs_dump -U jack -f /home/omm/backup/MPPDB_backup12.tar -p 8000 human_resource -n public --role role1 --rolepassword abc@1234 -F t
Password:
gs_dump[port='8000'][human_resource][2017-07-21 16:21:10]: dump database human_resource successfully
gs_dump[port='8000'][human_resource][2017-07-21 16:21:10]: total time: 3278  ms
```

示例三: 执行gs_dumpall导出数据，用户jack不具备导出所有数据库的权限，而角色role1（管理员）具备该权限，要实现导出所有数据库，可以在导出命令中设置-role角色为role1，使用role1的权限，完成导出目的。导出文件格式为文本归档格式。

```bash
$ human_resource=# CREATE USER jack IDENTIFIED BY "1234@abc";
CREATE ROLE
human_resource=# GRANT role1 TO jack;
GRANT ROLE

$ gs_dumpall -U jack -f /home/omm/backup/MPPDB_backup.sql -p 8000 --role role1 --rolepassword abc@1234
Password:
gs_dumpall[port='8000'][human_resource][2018-11-14 17:26:18]: dumpall operation successful
gs_dumpall[port='8000'][human_resource][2018-11-14 17:26:18]: total time: 6437  ms
```
