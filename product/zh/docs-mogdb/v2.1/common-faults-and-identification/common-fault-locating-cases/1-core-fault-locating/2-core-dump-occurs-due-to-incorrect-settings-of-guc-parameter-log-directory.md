---
title: GUC参数log_directory设置不正确引起的core问题
summary: GUC参数log_directory设置不正确引起的core问题
author: Guo Huan
date: 2021-05-24
---

# GUC参数log_directory设置不正确引起的core问题

## 问题现象

数据库进程拉起后出现coredump，日志无内容。

## 原因分析

GUC参数log_directory设置的路径不可读取或无访问权限，数据库在启动过程中进行校验失败，通过panic日志退出程序。

## 处理办法

GUC参数log_directory设置为合法路径，具体请参考[log_directory](../../../reference-guide/guc-parameters/10-error-reporting-and-logging/1-logging-destination.md#log_directory)。
