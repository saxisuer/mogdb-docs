---
title: 使用示例
summary: 使用示例
author: Guo Huan
date: 2021-05-19
---

# 使用示例

为了方便用户理解部署过程，假设当前数据库节点信息如下：

```
IP: 10.90.110.130
PORT: 8000
type: single
```

detector服务器信息：

```
IP: 10.90.110.131
listen_host = 0.0.0.0
listen_port = 8080
```

部署的启动流程如下：

## 配置文件修改

首先需要更改配置文件 a-detection.conf，主要涉及其中的两个session：

```
[database]
storage_duration = 12H  # 数据存储时间长度，默认12小时
database_dir = ./data  # 数据存储目录

[security]
tls = False
ca = ./certificate/ca/ca.crt
server_cert = ./certificate/server/server.crt
server_key = ./certificate/server/server.key
agent_cert = ./certificate/agent/agent.crt
agent_key = ./certificate/agent/agent.key

[server]
host = 10.90.110.131
listen_host = 0.0.0.0
listen_port = 8080
white_host =  10.90.110.130
white_port = 8000
[agent]
source_timer_interval = 10S
sink_timer_interval = 10S
channel_capacity = 1000
db_host =  10.90.110.130
db_port = 8080
db_type = single

[forecast]
forecast_alg = auto_arima

[log]
log_dir = ./log
```

## 服务启动与停止

启动本地agent服务：

```
python main.py start --role agent
```

停止本地agent服务：

```
python main.py stop --role agent
```

启动本地collector服务：

```
python main.py start --role collector
```

停止本地collector服务：

```
python main.py stop --role collector
```

启动本地monitor服务：

```
python main.py start --role monitor
```

停止本地monitor服务：

```
python main.py stop --role monitor
```
