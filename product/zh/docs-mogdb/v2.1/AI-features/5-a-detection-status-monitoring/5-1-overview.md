---
title: 概述
summary: 概述
author: Guo Huan
date: 2021-05-19
---

# 概述

Anomaly_detection是MogDB集成的AI工具。可以用于数据库指标采集、预测指标趋势变化、慢SQL根因分析以及异常监控与诊断，是DBMind套间中的一个组件。

Anomaly_detection支持采集的信息分为三块，分别是os_exporter、database_exporter和WDR。

- os_exporter主要包括IO_Read、IO_Write、IO_Wait、CPU_Usage、Memory_Usage、数据库数据目录磁盘空间占用Disk_space；
- database_exporter主要包括QPS、部分关键GUC参数（work_mem、shared_buffers、max_connections）、数据库临时文件、外部进程情况、外部连接数；
- WDR包括慢SQL文本、SQL开始执行时间、SQL结束执行时间相关信息。

在异常监控方面，anomaly_detection可以同时对IO_Read、IO_Write、IO_Wait、CPU_Usage、Memory_Usage和Disk_Space多个指标的未来变化趋势进行预测，当发现某个指标在未来某段时间或者某个时刻会超出人工设置的阈值，该工具会通过日志进行报警。

在慢SQL根因分析方面，工具会定期从WDR报告中拉取慢SQL信息，并对慢SQL的根因进行诊断，最后将诊断结果存放到日志文件中，同时该工具还支持用户交互式慢SQL诊断，即对用户输入的慢SQL进行根因分析，并将结果反馈给用户。

anomaly_detection由agent和detector两大模块组成。

agent和MogDB数据库环境部署在同一个服务器上，该模块主要有两个作用。一个是定时采集数据库指标数据，并将采集到的数据存放到缓冲队列中；另一个作用是将缓冲队列中数据定时发送到detector的collector子模块中。

detector模块由collector模块和monitor模块组成。

- collector模块和agent模块通过http或https进行通信，接受agent模块push的数据并存储到本地。
- monitor模块基于本地数据对指标的未来变化趋势进行预测和异常报警，另外结合系统和WDR报告等各种相关联信息，分析慢SQL的根因。
