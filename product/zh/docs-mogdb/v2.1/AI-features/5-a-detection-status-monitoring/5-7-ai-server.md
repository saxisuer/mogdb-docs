---
title: AI_SERVER
summary: AI_SERVER
author: Guo Huan
date: 2021-10-21
---

# AI_SERVER

ai_server为anomaly_detection特性的分离特性，在原anomaly_detection数据采集功能的基础上增加了采集类型、采集项、数据存储模式，仅用于数据采集，后续将整合到anomaly_detection中，该特性主要包含server组件和agent组件，agent须部署到数据库节点，用于数据采集，server部署在独立节点进行数据收集存储。

数据存储方式包括：sqlite、mongodb、influxdb。

采集项如表1：

**表 1** 采集项说明

<table>
    <tr>
        <th>采集类型</th>
        <th>采集项</th>
        <th>描述</th>
    </tr>
    <tr>
        <td rowspan=5>database</td>
        <td>work_mem</td>
        <td>数据库内存相关GUC参数，对涉及到排序任务的sql，检测分配的空间是否足够。</td>
    </tr>
    <tr>
        <td>shared_buffers</td>
        <td>数据库内存相关GUC参数，不合适的shared_buffer会导致数据库性能变差。</td>
    </tr>
    <tr>
        <td>max_connections</td>
        <td>数据库最大连接数。</td>
    </tr>
    <tr>
        <td>current connections</td>
        <td>数据库当前连接数。</td>
    </tr>
    <tr>
        <td>qps</td>
        <td>数据库性能指标。</td>
    </tr>
    <tr>
        <td rowspan=6>os</td>
        <td>cpu usage</td>
        <td>cpu使用率。</td>
    </tr>
    <tr>
        <td>memory usage</td>
        <td>内存使用率。</td>
    </tr>
    <tr>
        <td>io wait</td>
        <td>系统因为io导致的进程wait。</td>
    </tr>
    <tr>
        <td>io write</td>
        <td>数据磁盘写吞吐量。</td>
    </tr>
    <tr>
        <td>io read</td>
        <td>数据磁盘读吞吐量。</td>
    </tr>
    <tr>
        <td>disk used</td>
        <td>磁盘已使用的大小。</td>
    </tr>
</table>

部署方式请参见[AI_MANAGER](5-8-ai-manager.md)章节。
