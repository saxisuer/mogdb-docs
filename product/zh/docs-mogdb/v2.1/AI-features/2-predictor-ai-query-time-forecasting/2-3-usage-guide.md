---
title: 使用指导
summary: 使用指导
author: Guo Huan
date: 2021-05-19
---

# 使用指导

<br/>

## 数据收集

1. 打开数据收集。

   a. 设置ActiveSQL operator信息相关参数:

      ```bash
      enable_resource_track=on
      resource_track_level=operator
      enable_resource_record=on
      resource_track_cost=10（默认值为100000）
      ```

      > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
      >
      > - resource_track_cost需设置为小于需要收集的查询总代价，满足条件的信息才能被收集。
      > - Cgroups功能正常加载。

   b. 信息收集：

      执行业务查询语句。

      查看实时收集数据：

      ```sql
      select * from gs_wlm_plan_operator_history;
      ```

      预期：满足resource_track_duration和resource_track_cost的作业被全量收集。

2. 关闭数据收集。

   a. 设置ActiveSQL operator信息相关参数：

      ```bash
      enable_resource_track=off 或
      resource_track_level=none 或
      resource_track_level=query
      ```

   b. 执行业务查询语句。

      等待三分钟之后查看当前节点上的数据：

      ```sql
      select * from gs_wlm_plan_operator_info;
      ```

      预期：所查表和视图无新增数据。

3. 数据持久化保存。

   a. 设置ActiveSQL operator信息相关参数：

      ```bash
      enable_resource_track=on
      resource_track_level=operator
      enable_resource_record=on
      resource_track_duration=0（默认值为60s）
      resource_track_cost=10（默认值为100000）
      ```

      > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
      >
      > - resource_track_cost需设置为小于需要收集的查询总代价，满足条件的信息才能被收集。
      > - Cgroups功能正常加载。

   b. 执行业务查询语句。

      等待三分钟之后查看当前节点上的数据：

      ```sql
      select * from gs_wlm_plan_operator_info;
      ```

      预期：满足resource_track_duration和resource_track_cost的作业被全量收集。

<br/>

## 模型管理（系统管理员用户）

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  模型管理操作需要在数据库正常的状态下进行。

1. 新增模型：

   INSERT INTO gs_opt_model values('……')；

   示例：

   ```sql
   INSERT INTO gs_opt_model values('rlstm', 'model_name', 'datname', '127.0.0.1', 5000, 2000, 1, -1, 64, 512, 0 , false, false, '{S, T}', '{0,0}', '{0,0}', 'Text');
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - 具体模型参数设置请参考[GS_OPT_MODEL](../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)。
   > - 目前“template_name”列只支持“rlstm”。
   > - “datname”列请和用于模型使用和训练的数据库保持一致，否则无法使用。
   > - “model_name”一列需要满足unique约束。
   > - 其他参数设置见[最佳实践](2-4-best-practices.md)。

2. 修改模型参数：

   ```sql
   UPDATE gs_opt_model SET <attribute> = <value> WHERE model_name = <target_model_name>;
   ```

3. 删除模型：

   ```sql
   DELETE FROM gs_opt_model WHERE model_name = <target_model_name>;
   ```

4. 查询现有模型及其状态：

   ```sql
   SELECT * FROM gs_opt_model;
   ```

<br/>

## 模型训练（系统管理员用户）

1. 配置/添加模型训练参数：参考模型管理（系统管理员用户）进行模型添加、模型参数修改，来指定训练参数。

   例：

   模型添加：

   ```sql
   INSERT INTO gs_opt_model values('rlstm', 'default', 'mogdb', '127.0.0.1', 5000, 2000, 1, -1, 64, 512, 0 , false, false, '{S, T}', '{0,0}', '{0,0}', 'Text');
   ```

   训练参数更新：

   ```sql
   UPDATE gs_opt_model SET <attribute> = <value> WHERE model_name = <target_model_name>;
   ```

2. 前提条件为数据库状态正常且历史数据正常收集：

   删除原有encoding数据：

   ```sql
   DELETE FROM gs_wlm_plan_encoding_table;
   ```

   进行数据编码，需要指定数据库名：

   ```sql
   SELECT gather_encoding_info('mogdb');
   ```

   开始训练：

   ```sql
   SELECT model_train_opt('rlstm', 'default');
   ```

3. 查看模型训练状态：

   ```sql
   SELECT * FROM track_model_train_opt('rlstm', 'default');
   ```

   返回Tensorboard所用URL：

   ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/usage-guide-1.png)

   打开URL查看模型训练状态：

   ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/usage-guide-2.png)

<br/>

## 模型预测

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 模型预测功能需在数据库状态正常、指定模型已被训练且收敛的条件下进行。
> - 目前，模型训练参数的标签设置中需要包含“S”标签，explain中才可显示“p-time”预测值。 例：INSERT INTO gs_opt_model values('rlstm', 'default', 'mogdb', '127.0.0.1', 5000, 1000, 1, -1, 50, 500, 0 , false, false, '{**S**, T}', '{0,0}', '{0,0}', 'Text');

调用explain接口：

```sql
explain (analyze on, predictor <model_name>)
SELECT ...
```

预期结果：

```bash
例：Row Adapter  (cost=110481.35..110481.35 rows=100 p-time=99..182 width=100) (actual time=375.158..375.160 rows=2 loops=1)
其中，“p-time”列为标签预测值。
```

<br/>

## 其他功能

1. 检查AiEngine是否可连接:

   ```sql
   mogdb=# select check_engine_status('aiEngine-ip-address',running-port);
   ```

2. 查看模型对应日志在AiEngine侧的保存路径：

   ```sql
   mogdb=# select track_model_train_opt('template_name', 'model_name');
   ```
