---
title: 最佳实践
summary: 最佳实践
author: Guo Huan
date: 2021-05-19
---

# 最佳实践

相关参数解释参考表[GS_OPT_MODEL](../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)。

**表 1**

| 模型参数      | 参数建议                                                     |
| :------------ | :----------------------------------------------------------- |
| template_name | ‘rlstm’                                                      |
| model_name    | 自定义，如‘open_ai’，需满足unique约束。                      |
| datname       | 所服务database名称，如‘mogdb’。                              |
| ip            | aiEngine-ip地址，如‘127.0.0.1’。                             |
| port          | aiEngine监听端口，如‘5000’。                                 |
| max_epoch     | 迭代次数，推荐较大数值，保证收敛效果，如‘2000’。             |
| learning_rate | (0, 1]浮点数，推荐较大的学习率，助于加快收敛速度。           |
| dim_red       | 特征值降维系数：<br/>‘-1’：不采用PCA降维，全量特征；<br/>‘（0，1] ’区间浮点数：越小，训练维度越小，收敛速度越快，但影响训练准确率。 |
| hidden_units  | 特征值维度较高时，建议适度增大此参数，提高模型复杂度，如 ‘64，128……’ |
| batch_size    | 根据编码数据量，较大数据量推荐适度增大此参数，加快模型收敛，如‘256，512……’ |
| 其他参数      | 参考表[GS_OPT_MODEL](../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)                           |

推荐参数配置：

```sql
INSERT INTO gs_opt_model values('rlstm', 'open_ai', 'mogdb', '127.0.0.1', 5000, 2000，1, -1, 64, 512, 0 , false, false, '{S, T}', '{0,0}', '{0,0}', 'Text');
```
