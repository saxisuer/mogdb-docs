---
title: 命令参考
summary: 命令参考
author: Guo Huan
date: 2021-05-19
---

# 命令参考

**表 1** 命令行参数

| 参数                   | 参数说明                                                     | 取值范围               |
| :--------------------- | :----------------------------------------------------------- | :--------------------- |
| mode                   | 指定调优程序运行的模式                                       | train，tune，recommend |
| -tuner-config-file, -x | X-Tuner的核心参数配置文件路径，默认路径为安装目录下的xtuner.conf | \-                     |
| -db-config-file, -f    | 调优程序的用于登录到数据库宿主机上的连接信息配置文件路径，若通过该文件配置数据库连接信息，则下述数据库连接信息可省略 | \-                     |
| -db-name               | 指定需要调优的数据库名                                       | \-                     |
| -db-user               | 指定以何用户身份登陆到调优的数据库上                         | \-                     |
| -port                  | 数据库的侦听端口                                             | \-                     |
| -host                  | 数据库实例的宿主机IP                                         | \-                     |
| -host-user             | 指定以何用户身份登陆到数据库实例的宿主机上，要求改用户名的环境变量中可以找到gsql、gs_ctl等数据库运维工具。 | \-                     |
| -host-ssh-port         | 数据库实例所在宿主机的SSH端口号，可选，默认为22              | \-                     |
| -help, -h              | 返回帮助信息                                                 | \-                     |
| -version, -v           | 返回当前工具版本号                                           | \-                     |

**表 2** 配置文件中的参数详解

| 参数名                | 参数说明                                                     | 取值范围                      |
| :-------------------- | :----------------- | :------------------- |
| logfile               | 生成的日志存放路径                                           | \-                            |
| output_tuning_result  | 可选，调优结果的保存路径                                     | \-                            |
| verbose               | 是否打印详情                                                 | on, off                       |
| recorder_file         | 调优中间信息的记录日志存放路径                               | \-                            |
| tune_strategy         | 调优模式下采取哪种策略                                       | rl, gop, auto                 |
| drop_cache            | 是否在每一个迭代轮次中进行drop cache，drop cache可以使benchmark跑分结果更加稳定。若启动该参数，则需要将登录的系统用户加入到 /etc/sudoers 列表中，同时为其增加 **NOPASSWD** 权限（由于该权限可能过高，建议临时启用该权限，调优结束后关闭）。 | on, off                       |
| used_mem_penalty_term | 数据库使用总内存的惩罚系数，用于防止通过无限量占用内存而换取的性能表现。该数值越大，惩罚力度越大。 | 建议0 ~ 1                     |
| rl_algorithm          | 选择何种RL算法                                               | ddpg                          |
| rl_model_path         | RL模型保存或读取路径，包括保存目录名与文件名前缀。在train 模式下该路径用于保存模型，在tune模式下则用于读取模型文件 | \-                            |
| rl_steps              | 深度强化学习算法迭代的步数                                   | \-                            |
| max_episode_steps     | 每个回合的最大迭代步数                                       | \-                            |
| test_episode          | 使用RL算法进行调优模式的回合数                               | \-                            |
| gop_algorithm         | 采取何种全局搜索算法                                         | bayes, pso, auto              |
| max_iterations        | 全局搜索算法的最大迭代轮次（并非确定数值，可能会根据实际情况多跑若干轮） | \-                            |
| particle_nums         | PSO算法下的粒子数量                                          | \-                            |
| benchmark_script      | 使用何种benchmark驱动脚本，该选项指定加载benchmark路径下同名文件，默认支持TPC-C、TPC-H等典型benchmark | tpcc, tpch, tpcds, sysbench … |
| benchmark_path        | benchmark 脚本的存储路径，若没有配置该选项，则使用benchmark驱动脚本中的配置 | \-                            |
| benchmark_cmd         | 启动benchmark 脚本的命令，若没有配置该选项，则使用benchmark驱动脚本中的配置 | \-                            |
| benchmark_period      | 仅对 period benchmark有效，表明整个benchmark的测试周期是多少，单位是秒 | -                             |
| scenario              | 用户指定的当前workload所属的类型                             | tp, ap, htap                  |
| tuning_list           | 准备调优的参数列表文件，可参考 share/knobs.json.template 文件 | \-                            |
