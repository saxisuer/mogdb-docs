---
title: 数据库对象函数
summary: 数据库对象函数
author: Zhang Cuiping
date: 2021-04-20
---

# 数据库对象函数

## 数据库对象尺寸函数

数据库对象尺寸函数计算数据库对象使用的实际磁盘空间。

- pg_column_size(any)

  描述: 存储一个指定的数值需要的字节数（可能压缩过）。

  返回值类型: int

  备注: pg_column_size显示用于存储某个独立数据值的空间。

  ```sql
  mogdb=# SELECT pg_column_size(1);
   pg_column_size
  ----------------
                4
  (1 row)
  ```

- pg_database_size(oid)

  描述: 指定OID代表的数据库使用的磁盘空间。

  返回值类型: bigint

- pg_database_size(name)

  描述: 指定名称的数据库使用的磁盘空间。

  返回值类型: bigint

  备注: pg_database_size接受一个数据库的OID或者名称，然后返回该对象使用的全部磁盘空间。

  示例:

  ```sql
  mogdb=# SELECT pg_database_size('mogdb');
   pg_database_size
  ------------------
           51590112
  (1 row)
  ```

- pg_relation_size(oid)

  描述: 指定OID代表的表或者索引所使用的磁盘空间。

  返回值类型: bigint

- get_db_source_datasize()

  描述: 估算当前数据库非压缩态的数据总容量。

  返回值类型: bigint

  备注: （1）调用该函数前需要做analyze；（2）通过估算列存的压缩率计算非压缩态的数据总容量。

  示例:

  ```sql
  mogdb=# analyze;
  ANALYZE
  mogdb=# select get_db_source_datasize();
   get_db_source_datasize
  ------------------------
              35384925667
  (1 row)
  ```

- pg_relation_size(text)

  描述: 指定名称的表或者索引使用的磁盘空间。表名称可以用模式名修饰。

  返回值类型: bigint

- pg_relation_size(relation regclass, fork text)

  描述: 指定表或索引的指定分叉树（'main'，'fsm'或'vm'）使用的磁盘空间。

  返回值类型: bigint

- pg_relation_size(relation regclass)

  描述: pg_relation_size(…, 'main')的简写。

  返回值类型: bigint

  备注: pg_relation_size接受一个表、索引、压缩表的OID或者名称，然后返回它们的字节大小。

- pg_partition_size(oid,oid)

  描述: 指定OID代表的分区使用的磁盘空间。其中，第一个oid为表的OID，第二个oid为分区的OID。

  返回值类型: bigint

- pg_partition_size(text, text)

  描述: 指定名称的分区使用的磁盘空间。其中，第一个text为表名，第二个text为分区名。

  返回值类型: bigint

- pg_partition_indexes_size(oid,oid)

  描述: 指定OID代表的分区的索引使用的磁盘空间。其中，第一个oid为表的OID，第二个oid为分区的OID。

  返回值类型: bigint

- pg_partition_indexes_size(text,text)

  描述: 指定名称的分区的索引使用的磁盘空间。其中，第一个text为表名，第二个text为分区名。

  返回值类型: bigint

- pg_indexes_size(regclass)

  描述: 附加到指定表的索引使用的总磁盘空间。

  返回值类型: bigint

- pg_size_pretty(bigint)

  描述: 将以64位整数表示的字节值转换为具有单位的易读格式。

  返回值类型: text

- pg_size_pretty(numeric)

  描述: 将以数值表示的字节值转换为具有单位的易读格式。

  返回值类型: text

  备注: pg_size_pretty用于把其他函数的结果格式化成一种易读的格式，可以根据情况使用KB 、MB 、GB 、TB。

- pg_table_size(regclass)

  描述: 指定的表使用的磁盘空间，不计索引（但是包含TOAST，自由空间映射和可见性映射）。

  返回值类型: bigint

- pg_tablespace_size(oid)

  描述: 指定OID代表的表空间使用的磁盘空间。

  返回值类型: bigint

- pg_tablespace_size(name)

  描述: 指定名称的表空间使用的磁盘空间。

  返回值类型: bigint

  备注:

  pg_tablespace_size接受一个数据库的OID或者名称，然后返回该对象使用的全部磁盘空间。

- pg_total_relation_size(oid)

  描述: 指定OID代表的表使用的磁盘空间，包括索引和压缩数据。

  返回值类型: bigint

- pg_total_relation_size(regclass)

  描述: 指定的表使用的总磁盘空间，包括所有的索引和TOAST数据。

  返回值类型: bigint

- pg_total_relation_size(text)

  描述: 指定名称的表所使用的全部磁盘空间，包括索引和压缩数据。表名称可以用模式名修饰。

  返回值类型: bigint

  备注: pg_total_relation_size接受一个表或者一个压缩表的OID或者名称，然后返回以字节计的数据和所有相关的索引和压缩表的尺寸。

- datalength(any)

  描述: 计算一个指定的数据需要的字节数（不考虑数据的管理空间和数据压缩，数据类型转换等情况）。

  返回值类型: int

  备注: datalength用于计算某个独立数据值的空间。

  示例:

  ```sql
  mogdb=# SELECT datalength(1);
  datalength
  ------------
  4
  (1 row)
  ```

  目前支持的数据类型及计算方式见下表：

  <table>
      <tr>
          <th colspan=3>数据类型</th>
          <th>存储空间</th>
      </tr>
      <tr>
          <td rowspan=18>数值类型</td>
          <td rowspan=5>整数类型</td>
          <td>TINYINT</td>
          <td>1</td>
      </tr>
      <tr>
          <td>SMALLINT</td>
          <td>2</td>
      </tr>
      <tr>
          <td>INTEGER</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BINARY_INTEGER</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BIGINT</td>
          <td>8</td>
      </tr>
      <tr>
          <td rowspan=3>任意精度型</td>
          <td>DECIMAL</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td>NUMERIC</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td>NUMBER</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td rowspan=3>序列整型</td>
          <td>SMALLSERIAL</td>
          <td>2</td>
      </tr>
      <tr>
          <td>SERIAL</td>
          <td>4</td>
      </tr>
      <tr>
          <td>BIGSERIAL</td>
          <td>8</td>
      </tr>
      <tr>
          <td rowspan=7>浮点类型</td>
          <td>FLOAT4</td>
          <td>4</td>
      </tr>
      <tr>
          <td>DOUBLE PRECISION</td>
          <td>8</td>
      </tr>
      <tr>
          <td>FLOAT8</td>
          <td>8</td>
      </tr>
      <tr>
          <td>BINARY_DOUBLE</td>
          <td>8</td>
      </tr>
      <tr>
          <td>FLOAT[(p)]</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td>DEC[(p[,s])]</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td>INTEGER[(p[,s])]</td>
          <td>每4位十进制数占两个字节，小数点前后数字分别计算</td>
      </tr>
      <tr>
          <td>布尔类型</td>
          <td>布尔类型</td>
          <td>BOOLEAN</td>
          <td>1</td>
      </tr>
      <tr>
          <td rowspan=11>字符类型</td>
          <td rowspan=11>字符类型</td>
          <td>CHAR</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHARACTER(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>NCHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>VARCHAR(n)</td>
          <td>n</td>
      </tr>
      <tr>
          <td>CHARACTER</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td>VARYING(n)</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td>VARCHAR2(n)</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td>NVARCHAR2(n)</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td>TEXT</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td>CLOB</td>
          <td>字符实际字节数</td>
      </tr>
      <tr>
          <td rowspan=11>时间类型</td>
          <td rowspan=11>时间类型</td>
          <td>DATE</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIME</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIMEZ</td>
          <td>12</td>
      </tr>
      <tr>
          <td>TIMESTAMP</td>
          <td>8</td>
      </tr>
      <tr>
          <td>TIMESTAMPZ</td>
          <td>8</td>
      </tr>
      <tr>
          <td>SMALLDATETIME</td>
          <td>8</td>
      </tr>
      <tr>
          <td>INTERVAL DAY TO SECOND</td>
          <td>16</td>
      </tr>
      <tr>
          <td>INTERVAL</td>
          <td>16</td>
      </tr>
      <tr>
          <td>RELTIME</td>
          <td>4</td>
      </tr>
      <tr>
          <td>ABSTIME</td>
          <td>4</td>
      </tr>
      <tr>
          <td>TINTERVAL</td>
          <td>12</td>
      </tr>
  </table>

## 数据库对象位置函数

- pg_relation_filenode(relation regclass)

  描述: 指定关系的文件节点数。

  返回值类型: oid

  备注: pg_relation_filenode接受一个表、索引、序列或压缩表的OID或者名称，并且返回当前分配给它的“filenode”数。文件节点是关系使用的文件名称的基本组件。对大多数表来说，结果和pg_class.relfilenode相同，但对确定的系统目录来说， relfilenode为0而且这个函数必须用来获取正确的值。如果传递一个没有存储的关系，比如一个视图，那么这个函数返回NULL。

- pg_relation_filepath(relation regclass)

  描述: 指定关系的文件路径名。

  返回值类型: text

  备注: pg_relation_filepath类似于pg_relation_filenode，但是它返回关系的整个文件路径名（相对于MogDB的数据目录PGDATA）。

- pg_filenode_relation(tablespace oid, filenode oid)

    描述：获取对应的tablespace和relfilenode所对应的表名。

    返回类型：regclass

- pg_partition_filenode(partition_oid)

    描述：获取到指定分区表的oid锁对应的filenode。

    返回类型：oid

- pg_partition_filepath(partition_oid)

    描述：指定分区的文件路径名。

    返回值类型：text

## 回收站对象函数

- gs_is_recycle_object(classid, objid, objname)

    描述：判断是否为回收站对象。

    返回值类型：bool
