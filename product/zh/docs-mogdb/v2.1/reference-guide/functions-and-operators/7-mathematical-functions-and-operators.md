---
title: 数字操作函数和操作符
summary: 数字操作函数和操作符
author: Zhang Cuiping
date: 2021-04-20
---

# 数字操作函数和操作符

## 数字操作符

- \+

  描述：加

  示例：

  ```sql
  mogdb=# SELECT 2+3 AS RESULT;
   result
  --------
        5
  (1 row)
  ```

- \-

  描述：减

  示例：

  ```sql
  mogdb=# SELECT 2-3 AS RESULT;
   result
  --------
       -1
  (1 row)
  ```

- \*

  描述：乘

  示例：

  ```sql
  mogdb=# SELECT 2*3 AS RESULT;
   result
  --------
        6
  (1 row)
  ```

- /

  描述：除（除法操作符不会取整）

  示例：

  ```sql
  mogdb=# SELECT 4/2 AS RESULT;
   result
  --------
        2
  (1 row)
  ```

  ```sql
  mogdb=# SELECT 4/3 AS RESULT;
        result
  ------------------
   1.33333333333333
  (1 row)
  ```

- +/-

  描述：正/负

  示例：

  ```sql
  mogdb=# SELECT -2 AS RESULT;
   result
  --------
       -2
  (1 row)
  ```

- %

  描述：模（求余）

  示例：

  ```sql
  mogdb=# SELECT 5%4 AS RESULT;
   result
  --------
        1
  (1 row)
  ```

- @

  描述：绝对值

  示例：

  ```sql
  mogdb=# SELECT @ -5.0 AS RESULT;
   result
  --------
      5.0
  (1 row)
  ```

- \^

  描述：幂（指数运算）

  示例：

  ```sql
  mogdb=# SELECT 2.0^3.0 AS RESULT;
         result
  --------------------
   8.0000000000000000
  (1 row)
  ```

- |/

  描述：平方根

  示例：

  ```sql
  mogdb=# SELECT |/ 25.0 AS RESULT;
   result
  --------
        5
  (1 row)
  ```

- ||/

  描述：立方根

  示例：

  ```sql
  mogdb=# SELECT ||/ 27.0 AS RESULT;
   result
  --------
        3
  (1 row)
  ```

- !

  描述：阶乘

  示例：

  ```sql
  mogdb=# SELECT 5! AS RESULT;
   result
  --------
      120
  (1 row)
  ```

- !!

  描述：阶乘（前缀操作符）

  示例：

  ```sql
  mogdb=# SELECT !!5 AS RESULT;
   result
  --------
      120
  (1 row)
  ```

- &

  描述：二进制AND

  示例：

  ```sql
  mogdb=# SELECT 91&15  AS RESULT;
   result
  --------
       11
  (1 row)
  ```

- |

  描述：二进制OR

  示例：

  ```sql
  mogdb=# SELECT 32|3  AS RESULT;
   result
  --------
       35
  (1 row)
  ```

- \#

  描述：二进制XOR

  示例：

  ```sql
  mogdb=# SELECT 17#5  AS RESULT;
   result
  --------
       20
  (1 row)
  ```

- ~

  描述：二进制NOT

  示例：

  ```sql
  mogdb=# SELECT ~1 AS RESULT;
   result
  --------
       -2
  (1 row)
  ```

- &lt;&lt;

  描述：二进制左移

  示例：

  ```sql
  mogdb=# SELECT 1<<4 AS RESULT;
   result
  --------
       16
  (1 row)
  ```

- &gt;&gt;

  描述：二进制右移

  示例：

  ```sql
  mogdb=# SELECT 8>>2 AS RESULT;
   result
  --------
        2
  (1 row)
  ```

## 数字操作函数

- abs(x)

  描述：绝对值。

  返回值类型：和输入相同。

  示例：

  ```sql
  mogdb=# SELECT abs(-17.4);
   abs
  ------
   17.4
  (1 row)
  ```

- acos(x)

  描述：反余弦。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT acos(-1);
         acos
  ------------------
   3.14159265358979
  (1 row)
  ```

- asin(x)

  描述：反正弦。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT asin(0.5);
         asin
  ------------------
   .523598775598299
  (1 row)
  ```

- atan(x)

  描述：反正切。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT atan(1);
         atan
  ------------------
   .785398163397448
  (1 row)
  ```

- atan2(y, x)

  描述：y/x的反正切。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT atan2(2, 1);
        atan2
  ------------------
   1.10714871779409
  (1 row)
  ```

- bitand(integer, integer)

  描述：计算两个数字与运算(&)的结果。

  返回值类型：bigint类型数字。

  示例：

  ```sql
  mogdb=# SELECT bitand(127, 63);
   bitand
  --------
       63
  (1 row)
  ```

- cbrt(dp)

  描述：立方根。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT cbrt(27.0);
   cbrt
  ------
      3
  (1 row)
  ```

- ceil(x)

  描述：不小于参数的最小的整数。

  返回值类型：整数。

  示例：

  ```sql
  mogdb=# SELECT ceil(-42.8);
   ceil
  ------
    -42
  (1 row)
  ```

- ceiling(dp or numeric)

  描述：不小于参数的最小整数（ceil的别名）。

  返回值类型：dp or numeric，不考虑隐式类型转换的情况下与输入相同。

  示例：

  ```sql
  mogdb=# SELECT ceiling(-95.3);
   ceiling
  ---------
       -95
  (1 row)
  ```

- cos(x)

  描述：余弦。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT cos(-3.1415927);
          cos
  -------------------
   -.999999999999999
  (1 row)
  ```

- cot(x)

  描述：余切。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT cot(1);
         cot
  ------------------
   .642092615934331
  (1 row)
  ```

- degrees(dp)

  描述：把弧度转为角度。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT degrees(0.5);
       degrees
  ------------------
   28.6478897565412
  (1 row)
  ```

- div(y numeric, x numeric)

  描述：y除以x的商的整数部分。

  返回值类型：numeric

  示例：

  ```sql
  mogdb=# SELECT div(9,4);
   div
  -----
     2
  (1 row)
  ```

- exp(x)

  描述：自然指数。

  返回值类型：dp or numeric，不考虑隐式类型转换的情况下与输入相同。

  示例：

  ```sql
  mogdb=# SELECT exp(1.0);
          exp
  --------------------
   2.7182818284590452
  (1 row)
  ```

- floor(x)

  描述：不大于参数的最大整数。

  返回值类型：与输入相同。

  示例：

  ```sql
  mogdb=# SELECT floor(-42.8);
   floor
  -------
     -43
  (1 row)
  ```

- int1(in)

  描述：将传入的text参数转换为int1类型值并返回。

  返回值类型：int1

  示例：

  ```sql
  mogdb=# select int1('123');
   int1
  ------
   123
  (1 row)
  mogdb=# select int1('a');
   int1
  ------
     0
  (1 row)
  ```

- int2(in)

  描述：将传入参数转换为int2类型值并返回。

  支持的入参类型包括float4、float8、numeric、text。

  返回值类型：int2

  示例：

  ```sql
  mogdb=# select int2('1234');
   int2
  ------
   1234
  (1 row)
  mogdb=# select int2(25.3);
   int2
  ------
     25
  (1 row)
  ```

- int4(in)

  描述：将传入参数转换为int4类型值并返回。

  支持的入参类型包括bit、boolean、char、duoble precision、numeric、real、smallint、text。

  返回值类型：int4

  示例：

  ```sql
  mogdb=# select int4('789');
   int4
  ------
   789
  (1 row)
  mogdb=# select int4(99.9);
   int4
  ------
     99
  (1 row)
  ```

- radians(dp)

  描述：把角度转为弧度。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT radians(45.0);
       radians
  ------------------
   .785398163397448
  (1 row)
  ```

- random()

  描述：0.0到1.0之间的随机数。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT random();
        random
  ------------------
   .824823560658842
  (1 row)
  ```

- multiply(x double precision or text, y double precision or text)

  描述：x和y的乘积。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT multiply(9.0, '3.0');
       multiply
  -------------------
                 27
  (1 row)
  mogdb=# SELECT multiply('9.0', 3.0);
       multiply
  -------------------
                 27
  (1 row)
  ```

- ln(x)

  描述：自然对数。

  返回值类型：dp or numeric，不考虑隐式类型转换的情况下与输入相同。

  示例：

  ```sql
  mogdb=# SELECT ln(2.0);
          ln
  -------------------
   .6931471805599453
  (1 row)
  ```

- log(x)

  描述：以10为底的对数。

  返回值类型：与输入相同。

  示例：

  ```sql
  mogdb=# SELECT log(100.0);
          log
  --------------------
   2.0000000000000000
  (1 row)
  ```

- log(b numeric, x numeric)

  描述：以b为底的对数。

  返回值类型：numeric

  示例：

  ```sql
  mogdb=# SELECT log(2.0, 64.0);
          log
  --------------------
   6.0000000000000000
  (1 row)
  ```

- mod(x,y)

  描述：x/y的余数（模）。如果x是0，则返回0。

  返回值类型：与参数类型相同。

  示例：

  ```sql
  mogdb=# SELECT mod(9,4);
   mod
  -----
     1
  (1 row)
  ```

  ```sql
  mogdb=# SELECT mod(9,0);
   mod
  -----
     9
  (1 row)
  ```

- pi()

  描述："π"常量。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT pi();
          pi
  ------------------
   3.14159265358979
  (1 row)
  ```

- power(a double precision, b double precision)

  描述：a的b次幂。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT power(9.0, 3.0);
          power
  ----------------------
   729.0000000000000000
  (1 row)
  ```

- round(x)

  描述：离输入参数最近的整数。

  返回值类型：与输入相同。

  示例：

  ```sql
  mogdb=# SELECT round(42.4);
   round
  -------
      42
  (1 row)

  mogdb=# SELECT round(42.6);
   round
  -------
      43
  (1 row)
  ```

- round(v numeric, s int)

  描述：保留小数点后s位，s后一位进行四舍五入。

  返回值类型：numeric

  示例：

  ```sql
  mogdb=# SELECT round(42.4382, 2);
   round
  -------
   42.44
  (1 row)
  ```

- setseed(dp)

  描述：为随后的random()调用设置种子(-1.0到1.0之间，包含)。

  返回值类型：void

  示例：

  ```sql
  mogdb=# SELECT setseed(0.54823);
   setseed
  ---------

  (1 row)
  ```

- sign(x)

  描述：输出此参数的符号。

  返回值类型：-1表示负数，0表示0，1表示正数。

  示例：

  ```sql
  mogdb=# SELECT sign(-8.4);
   sign
  ------
     -1
  (1 row)
  ```

- sin(x)

  描述：正弦。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT sin(1.57079);
         sin
  ------------------
   .999999999979986
  (1 row)
  ```

- sqrt(x)

  描述：平方根。

  返回值类型：dp or numeric，不考虑隐式类型转换的情况下与输入相同。

  示例：

  ```sql
  mogdb=# SELECT sqrt(2.0);
         sqrt
  -------------------
   1.414213562373095
  (1 row)
  ```

- tan(x)

  描述：正切。

  返回值类型：double precision

  示例：

  ```sql
  mogdb=# SELECT tan(20);
         tan
  ------------------
   2.23716094422474
  (1 row)
  ```

- trunc(x)

  描述：截断（取整数部分）。

  返回值类型：与输入相同。

  示例：

  ```sql
  mogdb=# SELECT trunc(42.8);
   trunc
  -------
      42
  (1 row)
  ```

- trunc(v numeric, s int)

  描述：截断为s位小数。

  返回值类型：numeric

  示例：

  ```sql
  mogdb=# SELECT trunc(42.4382, 2);
   trunc
  -------
   42.43
  (1 row)
  ```

- smgrne(a smgr, b smgr)

  描述：比较两个smgr类型整数是否不相等。

  返回值类型：bool

- smgreq(a smgr, b smgr)

  描述：比较两个smgr类型整数是否相等。

  返回值类型：bool

- int1abs

    描述：返回uint8类型数据的绝对值。

    参数：tinyint

    返回值类型：tinyint

- int1and

    描述：返回两个uint8类型数据按位与的结果。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1cmp

    描述：返回两个uint8类型数据比较的结果，若第一个参数大，则返回1；若第二个参数大，则返回-1；若相等，则返回0。

    参数：tinyint, tinyint

    返回值类型：integer

- int1div

    描述：返回两个uint8类型数据相除的结果，结果为float8类型。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1eq

    描述：比较两个uint8类型数据是否相等。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1ge

    描述：判断两个uint8类型数据是否第一个参数大于等于第二个参数。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1gt

    描述：无符号1字节整数做大于运算。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1larger

    描述：无符号1字节整数求最大值。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1le

    描述：无符号1字节整数做小于等于运算。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1lt

    描述：无符号1字节整数做小于运算。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1smaller

    描述：无符号1字节整数求最小算。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1inc

    描述：无符号1字节整数加一。

    参数：tinyint

    返回值类型：tinyint

- int1mi

    描述：无符号一字节整数做差运算。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1mod

    描述：无符号一字节整数做取余运算。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1mul

    描述：无符号一字节整数做乘法运算。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1ne

    描述：无符号一字节整数不等于运算。

    参数：tinyint, tinyint

    返回值类型：boolean

- int1pl

    描述：无符号一字节整数加法。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1um

    描述：无符号一字节数去相反数并返回有符号二字节整数。

    参数：tinyint

    返回值类型：smallint

- int1xor

    描述：无符号一字节整数异或操作。

    参数：tinyint, tinyint

    返回值类型：tinyint

- cash_div_int1

    描述：对money类型进行除法运算。

    参数：money, tinyint

    返回值类型：money

- cash_mul_int1

    描述：对money类型进行乘法运算。

    参数：money, tinyint

    返回值类型：money

- int1not

    描述：无符号一字节整数二进制位翻转。

    参数：tinyint

    返回值类型：tinyint

- int1or

    描述：无符号一字节整数或运算。

    参数：tinyint, tinyint

    返回值类型：tinyint

- int1shl

    描述：无符号一字节整数左移指定位数。

    参数：tinyint, integer

    返回值类型：tinyint

- width_bucket(op numeric, b1 numeric, b2 numeric, count int)

  描述：返回一个桶，这个桶是在一个有count个桶，上界为b1下界为b2的等深柱图中operand将被赋予的那个桶。

  返回值类型：int

  示例：

  ```sql
  mogdb=# SELECT width_bucket(5.35, 0.024, 10.06, 5);
   width_bucket
  --------------
              3
  (1 row)
  ```

- width_bucket(op dp, b1 dp, b2 dp, count int)

  描述：返回一个桶，这个桶是在一个有count个桶，上界为b1下界为b2的等深柱图中operand将被赋予的那个桶。

  返回值类型：int

  示例：

  ```sql
  mogdb=# SELECT width_bucket(5.35, 0.024, 10.06, 5);
   width_bucket
  --------------
              3
  (1 row)
  ```
