---
title: 负载管理
summary: 负载管理
author: Zhang Cuiping
date: 2021-04-20
---

# 负载管理

未对数据库资源做控制时，容易出现并发任务抢占资源导致操作系统过载甚至最终崩溃。操作系统过载时，其响应用户任务的速度会变慢甚至无响应；操作系统崩溃时，整个系统将无法对用户提供任何服务。MogDB的负载管理功能能够基于可用资源的多少均衡数据库的负载，以避免数据库系统过载。

## use_workload_manager

**参数说明**: 是否开启资源管理功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开资源管理。

- off表示关闭资源管理。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 当使用表[GUC参数设置方式](30-appendix.md)中的方式二来修改参数值时，新参数值只能对更改操作执行后启动的线程生效。此外，对于后台线程以及线程复用执行的新作业，该参数值的改动不会生效。如果希望这类线程即时识别参数变化，可以使用kill session或重启节点的方式来实现。
  > - use_workload_manager参数由off变为on状态后，不会统计off时的存储资源。如果需要统计off时用户使用的存储资源，请在数据库中执行以下命令：
  >
  > ```sql
  > select gs_wlm_readjust_user_space(0);
  > ```

**默认值**: off

## cgroup_name

**参数说明**: 设置当前使用的Cgroups的名称或者调整当前group下排队的优先级。

即如果先设置cgroup_name，再设置session_respool，那么session_respool关联的控制组起作用，如果再切换cgroup_name，那么新切换的cgroup_name起作用。

切换cgroup_name的过程中如果指定到Workload控制组级别，数据库不对级别进行验证。级别的范围只要在1-10范围内都可以。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中方式三的方法进行设置。

建议尽量不要混合使用cgroup_name和session_respool。

**取值范围**: 字符串

**默认值**: InvalidGroup

## cpu_collect_timer

**参数说明**: 设置语句执行时在数据库节点上收集CPU时间的周期。

数据库管理员需根据系统资源（如CPU资源、IO资源和内存资源）情况，调整此数值大小，使得系统支持较合适的收集周期，太小会影响执行效率，太大会影响异常处理的精确度。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~INT_MAX， 单位为秒。

**默认值**: 30

## memory_tracking_mode

**参数说明**: 设置记录内存信息的模式。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**:

- none，不启动内存统计功能。
- peak：表示统计query级内存peak值，此数值计入数据库日志，也可以由explain analyze输出。
- normal，仅做内存实时统计，不生成文件。
- executor，生成统计文件，包含执行层使用过的所有已分配内存的上下文信息。
- fullexec，生成文件包含执行层申请过的所有内存上下文信息。

**默认值**: none

## memory_detail_tracking

**参数说明**: 设置需要的线程内分配内存上下文的顺序号以及当前线程所在query的plannodeid。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符型

**默认值**: 空

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> 该参数不允许用户进行设置，建议保持默认值。

## enable_resource_track

**参数说明**: 是否开启资源实时监控功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开资源监控。
- off表示关闭资源监控。

**默认值**: on

## enable_resource_record

**参数说明**: 是否开启资源监控记录归档功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启资源监控记录归档功能。
- off表示关闭资源监控记录归档功能。

**默认值**: off

## enable_logical_io_statistics

**参数说明**: 设置是否开启资源监控逻辑IO统计功能。开启时，对于PG_TOTAL_USER_RESOURCE_INFO视图中的read_kbytes、write_kbytes、read_counts、write_counts、read_speed和write_speed字段，会统计对应用户的逻辑读写字节数、次数以及速率。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启资源监控逻辑IO统计功能。
- off表示关闭资源监控逻辑IO统计功能。

**默认值**: on

## enable_user_metric_persistent

**参数说明**: 设置是否开启用户历史资源监控转存功能。开启时，对于PG_TOTAL_USER_RESOURCE_INFO视图中数据，会定期采样保存到GS_WLM_USER_RESOURCE_HISTORY系统表中。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

● on表示开启用户历史资源监控转存功能。

● off表示关闭用户历史资源监控转存功能。

**默认值**:  on

## user_metric_retention_time

**参数说明**: 设置用户历史资源监控数据的保存天数。该参数仅在 enable_user_metric_persistent为on时有效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中的方法一和方法二进行设置。

**取值范围**: 整型，0～730，单位为天。

● 值等于0时，用户历史资源监控数据将永久保存。

● 值大于0时，用户历史资源监控数据将保存对应天数。

**默认值**:  7

## enable_instance_metric_persistent

**参数说明**: 设置是否开启实例资源监控转存功能。开启时，对实例的监控数据会保存到GS_WLM_INSTANCE_HISTORY系统表中。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启实例资源监控转存功能。
- off表示关闭实例资源监控转存功能。

**默认值**: on

## instance_metric_retention_time

**参数说明**: 设置实例历史资源监控数据的保存天数。该参数仅在enable_instance_metric_persistent为on时有效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中的方法一和方法二进行设置。

**取值范围**: 整型，0～3650，单位为天。

- 值等于0时，实例历史资源监控数据将永久保存。
- 值大于0时，实例历史资源监控数据将保存对应设置天数。

**默认值**: 7

## resource_track_level

**参数说明**: 设置当前会话的资源监控的等级。该参数只有当参数enable_resource_track为on时才有效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举型

- none，不开启资源监控功能。
- query，开启query级别资源监控功能。
- operator，开启query级别和算子级别资源监控功能。

**默认值**: query

## resource_track_cost

**参数说明**: 设置对当前会话的语句进行资源监控的最小执行代价。该参数只有当参数enable_resource_track为on时才有效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，-1～INT_MAX

- 值为-1时，不进行资源监控。
- 值大于或等于0时，值大于或等于0且小于等于9时，对执行代价大于等于10的语句进行资源监控。
- 值大于或等于10时，对执行代价超过该参数值的语句进行资源监控。

**默认值**: 100000

## resource_track_duration

**参数说明**: 设置资源监控实时视图中记录的语句执行结束后进行历史信息转存的最小执行时间。当执行完成的作业，其执行时间不小于此参数值时，作业信息会从实时视图（以statistics为后缀的视图）转存到相应的历史视图（以history为后缀的视图）中。该参数只有当[enable_resource_track](#enable_resource_track)为on时才有效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX，单位为秒。

- 值为0时，资源监控实时视图中记录的所有语句都进行历史信息归档。
- 值大于0时，资源监控实时视图中记录的语句的执行时间超过这个值就会进行历史信息归档。

**默认值**: 1min

## disable_memory_protect

**参数说明**: 禁止内存保护功能。当系统内存不足时如果需要查询系统视图，可以先将此参数置为on，禁止内存保护功能，保证视图可以正常查询。该参数只适用于在系统内存不足时进行系统诊断和调试，正常运行时请保持该参数配置为off。

该参数属于USERSET类型参数，且只对当前会话有效。请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示禁止内存保护功能。
- off表示启动内存保护功能。

**默认值**: off

## query_band

**参数说明**: 用于标示当前会话的作业类型，由用户自定义。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符型

**默认值**: 空

## memory_fault_percent

**参数说明**: 内存故障测试时内存申请失败的比例，仅用在DEBUG版本。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～2147483647

**默认值**: 0

## enable_bbox_dump

**参数说明**:  是否开启黑匣子功能，在系统不配置core机制的时候仍可产生core文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开黑匣子功能。
- off表示关闭黑匣子功能。

**默认值**: off

## bbox_dump_count

**参数说明**: 在[bbox_dump_path](#bbox_dump_path)定义的路径下，允许存储的MogDB所产生core文件最大数。超过此数量，旧的core文件会被删除。此参数只有当[enable_bbox_dump](#enable_bbox_dump)为on时才生效。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～20

**默认值**: 8

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 在并发产生core文件时，core文件的产生个数可能大于bbox_dump_count。

## bbox_dump_path

**参数说明**: 黑匣子core文件的生成路径。此参数只有当[enable_bbox_dump](#enable_bbox_dump)为on时才生效。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符型

**默认值**: 空。默认生成黑匣子core文件的路径为读取/proc/sys/mogdb/core_pattern下的路径，如果这个路径不是一个目录，或者用户对此目录没有写权限，黑匣子core文件将生成在数据库的data目录下。或者以安装时指定的目录为准。

## enable_ffic_log

**参数说明**:  是否开启FFIC（First Failure Info Capture）功能。

该参数属于POSTMASTER类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开FFIC功能。
- off表示关闭FFIC功能。

**默认值**: on

## io_limits

**参数说明**:  每秒触发IO的上限。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应类型的设置的方法进行设置。

**取值范围**: 整型，0～1073741823

**默认值**: 0

## io_priority

**参数说明**:  IO利用率高达50%时，重消耗IO作业进行IO资源管控时关联的优先级等级。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应类型的设置的方法进行设置。

**取值范围**: 枚举型

- None：表示不受控。
- Low：表示限制iops为该作业原始触发数值的10%。
- Medium：表示限制iops为该作业原始触发数值的20%。
- High：表示限制iops为该作业原始触发数值的50%。

**默认值**: None

## io_control_unit

**参数说明**:  行存场景下，io管控时用来对io次数进行计数的单位。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应类型的设置方法进行设置。

记多少次io触发为一计数单位，通过此计数单位所记录的次数进行io管控。

**取值范围**: 整型，1000~1000000

**默认值**: 6000

## session_respool

**参数说明**:  当前的session关联的resource pool。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应类型的设置方法进行设置。

即如果先设置cgroup_name，再设置session_respool，那么session_respool关联的控制组起作用，如果再切换cgroup_name，那么新切换的cgroup_name起作用。

切换cgroup_name的过程中如果指定到Workload控制组级别，数据库不对级别进行验证。级别的范围只要在1-10范围内都可以。

建议尽量不要混合使用cgroup_name和session_respool。

**取值范围**: string类型，通过create resource pool所设置的资源池。

**默认值**: invalid_pool

## session_statistics_memory

**参数说明**:  设置实时查询视图的内存大小。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，5 * 1024 ～ max_process_memory的50%，单位KB。

**默认值**: 5MB

## topsql_retention_time

**参数说明**:  设置历史TopSQL中gs_wlm_operator_info表中数据的保存时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

取值范围:  整型，0~730，单位为天。

- 值为0时，表示数据永久保存。
- 值大于0时，表示数据能够保存的对应天数。

默认值： 0

## session_history_memory

**参数说明**:  设置历史查询视图的内存大小。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10 * 1024～max_process_memory的50%，单位KB。

**默认值**: 10MB

## transaction_pending_time

**参数说明**:  事务块语句和存储过程语句排队的最大时间。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，-1～INT_MAX/2，单位为秒。

- 值为-1或0： 事务块语句和存储过程语句无超时判断，排队至资源满足可执行条件。
- 值大于0： 事务块语句和存储过程语句排队超过所设数值的时间后，无视当前资源情况强制执行。

**默认值**: 0
