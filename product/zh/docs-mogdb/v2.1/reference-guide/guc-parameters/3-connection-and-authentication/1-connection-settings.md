---
title: 连接设置
summary: 连接设置
author: Zhang Cuiping
date: 2021-04-20
---

# 连接设置

介绍设置客户端和服务器连接方式相关的参数。

## listen_addresses

**参数说明**: 声明服务器侦听客户端的TCP/IP地址。

该参数指定MogDB服务器使用哪些IP地址进行侦听，如IPV4或IPV6（若支持）。服务器主机上可能存在多个网卡，每个网卡可以绑定多个IP地址，该参数就是控制MogDB到底绑定在哪个或者哪几个IP地址上。而客户端则可以通过该参数中指定的IP地址来连接MogDB或者给MogDB发送请求。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**:

- 主机名或IP地址，多个值之间用英文逗号分隔。
- 星号`*`或`0.0.0.0`表示侦听所有IP地址。配置侦听所有IP地址存在安全风险，不推荐用户使用。必须与有效地址结合使用（比如本地IP等），否则，可能造成Build失败的问题。同时，主备环境下配置为`*`或`0.0.0.0`时，主节点数据库路径下postgresql.conf文件中的localport端口号不能为数据库dataPortBase+1，否则会导致数据库无法启动。
- 置空则服务器不会侦听任何IP地址，这种情况下，只有Unix域套接字可以用于连接数据库。

**默认值**: 数据库实例安装好后，根据XML配置文件中不同实例的IP地址配置不同默认值。DN的默认参数值为：listen_addresses = 'x.x.x.x'。

## local_bind_address

**参数说明**: 声明当前节点连接MogDB其他节点绑定的本地IP地址。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**默认值**: 数据库实例安装好后，根据XML配置文件中不同实例的IP地址配置不同默认值。DN的默认参数值为：local_bind_address = 'x.x.x.x'。。

## port

**参数说明**: MogDB服务侦听的TCP端口号。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 该参数由安装时的配置文件指定，请勿轻易修改，否则修改后会影响数据库正常通信。
>

**取值范围**: 整型，1～65535

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 设置端口号时，请设置一个未被占用的端口号。设置多个实例的端口号，不可冲突。
> - 1~1023为操作系统保留端口号，请不要使用。
> - 通过配置文件安装数据库实例时，配置文件中的端口号需要注意通信矩阵预留端口。如：DN还需保留dataPortBase+1作为内部工具使用端口，保留dataPortBase+6作为流引擎消息队列通信端口等。故数据库实例安装阶段，port最大值为：DN可设置65529，同时需要保证端口号不冲突。

**默认值**: 5432（实际值由安装时的配置文件指定）

## max_connections

**参数说明**: 允许和数据库连接的最大并发连接数。此参数会影响MogDB的并发能力。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型。最小值为10（要大于max_wal_senders），理论最大值为262143，实际最大值为动态值，计算公式为“262143 - job_queue_processes - autovacuum_max_workers - AUXILIARY_BACKENDS - AV_LAUNCHER_PROCS - max_inner_tool_connections”。job_queue_processes、autovacuum_max_workers和max_inner_tool_connections的值取决于对应GUC参数的设置。AUXILIARY_BACKENDS为预留辅助线程数，固定为20，AV_LAUNCHER_PROCS为预留autovacuum的lancher线程数，固定为2。

**默认值**: 200

**设置建议**:

数据库主节点中此参数建议保持默认值。

当修改此默认值时，请同时修改undo_zone_count取值，具体设置原则请参见[undo章节](../../../reference-guide/guc-parameters/35-undo.md)。

**配置不当时影响**:

- 若配置max_connections过大，超过计算公式所描述的最大动态值，会出现节点拉起失败问题，报错提示“invalid value for parameter “max_connections””。
- 若未按照对外出口规格配置仅调大max_connections参数值，未同比例调整内存参数。业务压力大时，容易出现内存不足，报错提示“memory is temporarily unavailable”。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 对于管理员用户的连接数限制会略超过max_connections设置，目的是为了让管理员在链接被普通用户占满后仍可以连接上数据库，再超过一定范围（sysadmin_reserved_connections参数）后才会报错。即管理员用户的最大连接数等于max_connections + sysadmin_reserved_connections。
> - 对于普通用户来说，由于内部作业也会使用一些链接，因此会略小于max_connections，具体值取决于内部链接个数。

## max_inner_tool_connections

**参数说明**: 允许和数据库连接的工具的最大并发连接数。此参数会影响MogDB的工具连接并发能力。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为1，最大值为MIN(262143, max_connections)，max_connections的计算方法见上文。

**默认值**: 数据库节点为50。如果该默认值超过内核支持的最大值（在执行gs_initdb的时候判断），系统会提示错误。

**设置建议**:

数据库主节点中此参数建议保持默认值。

增大此参数可能导致MogDB要求更多的SystemV共享内存或者信号量，可能超过操作系统缺省配置的最大值。这种情况下，请酌情对数值加以调整。

## sysadmin_reserved_connections

**参数说明**: 为管理员用户预留的最少连接数, 不建议设置过大。该参数和max_connections参数配合使用，管理员用户的最大连接数等于max_connections+sysadmin_reserved_connections。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为MIN(262143, max_connections)，max_connections的计算方法见上文。

**默认值**: 3

## unix_socket_directory

**参数说明**: 设置MogDB服务器侦听客户端连接的Unix域套接字目录。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

该参数的长度限制于操作系统的长度，超过该限制将会导致Unix-domain socket path "xxx" is too long的问题。

**取值范围**: 字符串

**默认值**: 空字符串（实际值由安装时配置文件指定）

## unix_socket_group

**参数说明**: 设置Unix域套接字的所属组（套接字的所属用户总是启动服务器的用户）。可以与选项[unix_socket_permissions](#unix_socket_permissions)一起用于对套接字进行访问控制。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，其中空字符串表示当前用户的缺省组。

**默认值**: 空字符串

## unix_socket_permissions

**参数说明**: 设置Unix域套接字的访问权限。

Unix域套接字使用普通的Unix文件系统权限集。这个参数的值应该是数值的格式（chmod和umask命令可接受的格式）。如果使用自定义的八进制格式，数字必须以0开头。

建议设置为0770（只有当前连接数据库的用户和同组的人可以访问）或者0700（只有当前连接数据库的用户自己可以访问，同组或者其他人都没有权限）。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0000-0777

**默认值**: 0777

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 在Linux中，文档具有十个属性，其中第一个属性为文档类型，后面九个为权限属性，分别为Owner，Group及Others这三个组别的read、write、execute属性。 文档的权限属性分别简写为r，w，x，这九个属性三个为一组，也可以使用数字来表示文档的权限，对照表如下：
>
> r： 4
>
> w： 2
>
> x： 1
>
> -： 0
>
> 同一组（owner/group/others）的三个属性是累加的。
>
> 例如，-rwxrwx-表示这个文档的权限为：
>
> owner = rwx = 4+2+1 = 7
>
> group = rwx = 4+2+1 = 7
>
> others = - = 0+0+0 = 0
>
> 所以其权限为0770。

## application_name

**参数说明**: 当前连接请求当中，所使用的客户端名称。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。

**默认值**: 空字符串（连接到后端的应用名，以实际安装为准）

## connection_info

**参数说明**: 连接数据库的驱动类型、驱动版本号、当前驱动的部署路径和进程属主用户。

该参数属于USERSET类型参数，属于运维类参数，不建议用户设置。

**取值范围**: 字符串。

**默认值**: 空字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 空字符串，表示当前连接数据库的驱动不支持自动设置connection_info参数或应用程序未设置。
> - 驱动连接数据库的时候自行拼接的connection_info参数格式如下：
>
> ```sql
> {"driver_name"："ODBC","driver_version"： "(MogDB 2.1.1 build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr","driver_path"："/usr/local/lib/psqlodbcw.so","os_user"："omm"}
> ```
>
> 默认显示driver_name和driver_version，driver_path和os_user的显示由用户控制。
