---
title: 通信库参数
summary: 通信库参数
author: Zhang Cuiping
date: 2021-04-20
---

# 通信库参数

本节介绍通信库相关的参数设置及取值范围等内容。

## tcp_keepalives_idle

**参数说明**: 在支持TCP_KEEPIDLE套接字选项的系统上，设置发送活跃信号的间隔秒数。不设置发送保持活跃信号，连接就会处于闲置状态。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果操作系统不支持TCP_KEEPIDLE选项，这个参数的值必须为0。
> - 在通过Unix域套接字进行的连接的操作系统上，这个参数将被忽略。
> - 将该值设置为0时，将使用系统的值。
>
> - 该参数在不同的**会话之间不共享**，也就是说不同的会话连接可能有不同的值。
> - 查看该参数时查出来的是**当前会话连接内的参数值**，而不是**guc副本的值**。

**取值范围**: 0-3600，单位为s。

**默认值**: 0

## tcp_keepalives_interval

**参数说明**: 在支持TCP_KEEPINTVL套接字选项的操作系统上，以秒数声明在重新传输之间等待响应的时间。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0-180，单位为s。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果操作系统不支持TCP_KEEPINTVL选项，这个参数的值必须为0。
> - 在通过Unix域套接字进行的连接的操作系统上，这个参数将被忽略。
> - 将该值设置为0时，将使用系统的值。
> - 该参数在不同的**会话之间不共享**，也就是说不同的会话连接可能有不同的值。
> - 查看该参数时查出来的是**当前会话连接内的参数值**，而不是**guc副本的值**。

## tcp_keepalives_count

**参数说明**: 在支持TCP_KEEPCNT套接字选项的操作系统上，设置MogDB服务端在断开与客户端连接之前可以等待的保持活跃信号个数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果操作系统不支持TCP_KEEPCNT选项，这个参数的值必须为0。
> - 在通过Unix域套接字进行连接的操作系统上，这个参数将被忽略。
> - 将该值设置为0时，将使用系统的值。
> - 该参数在不同的**会话之间不共享**，也就是说不同的会话连接可能有不同的值。
> - 查看该参数时查出来的是**当前会话连接内的参数值**，而不是**guc副本的值**。

**取值范围**: 0-100，其中0表示MogDB未收到客户端反馈的保持活跃信号则立即断开连接。

**默认值**: 0

## comm_proxy_attr

**参数说明**: 通信代理库相关参数配置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 该参数仅支持欧拉2.9系统下的集中式ARM单机。
> - 本功能在线程池开启状态下生效，即enable_thread_pool为on。
> - 配置该参数时需同步配置GUC参数local_bind_address为libos_kni的网卡IP。
> - 参数模板：comm_proxy_attr = '{enable_libnet:true, enable_dfx:false, numa_num:4, numa_bind:[[30,31],[62,63],[94,95],[126,127]]}'
> - 可配置参数说明。
>   - enable_libnet：是否开启用户态协议，取值范围: true、false。
>   - enable_dfx：是否开启通信代理库视图，取值范围: true、false。
>   - numa_num：机器环境中numa的数量，支持2P、4P服务器，取值范围: 4、8。
>   - numa_bind：代理线程绑核参数，每个numa两个CPU绑核，共numa_num组，取值范围: [0，cpu数-1]。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，长度大于0。

**默认值**: 'none'
