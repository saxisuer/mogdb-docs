---
title: 并行导入
summary: 并行导入
author: Zhang Cuiping
date: 2021-04-20
---

# 并行导入

MogDB提供了并行导入功能，以快速、高效地完成大量数据导入。介绍MogDB并行导入的相关参数。

## raise_errors_if_no_files

**参数说明**: 导入时是否区分"导入文件记录数为空"和"导入文件不存在"。raise_errors_if_no_files=TRUE，则"导入文件不存在"的时候，MogDB将抛出"文件不存在的"错误。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示导入时区分"导入文件记录数为空"和"导入文件不存在"。
- off表示导入时不区分"导入文件记录数为空"和"导入文件不存在"。

**默认值**: off

## partition_mem_batch

**参数说明**: 为了优化对列存分区表的批量插入，在批量插入过程中会对数据进行缓存后再批量写盘。通过partition_mem_batch可指定缓存个数。该值设置过大，将消耗较多系统内存资源；设置过小，将降低系统列存分区表批量插入性能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 1~ 65535

**默认值**: 256

## partition_max_cache_size

**参数说明**: 为了优化对列存分区表的批量插入，在批量插入过程中会对数据进行缓存后再批量写盘。通过partition_max_cache_size可指定数据缓存区大小。该值设置过大，将消耗较多系统内存资源；设置过小，将降低列存分区表批量插入性能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**:

列存分区表: 4096~ INT_MAX / 2，最小单位为KB。

**默认值**: 2GB

## enable_delta_store

**参数说明**: 为了增强列存单条数据导入的性能和解决磁盘冗余问题，可通过此参数选择是否开启支持列存delta表功能。该参数开启时，数据导入列存表，会根据表定义时指定的DELTAROW_THRESHOLD决定数据进入delta表存储还是主表CU存储，当数据量小于DELTAROW_THRESHOLD时，数据进入delta表。该参数影响的操作包括insert，copy，vacuum，vacuum full，vacuum deltamerge重分布等所有涉及列存表数据移动的操作。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**:

- on表示开启列存delta表功能。
- off表示不开启列存delta表功能。

**默认值**: off
