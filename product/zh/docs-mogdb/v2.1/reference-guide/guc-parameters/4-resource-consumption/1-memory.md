---
title: 内存
summary: 内存
author: Zhang Cuiping
date: 2021-04-20
---

# 内存

介绍与内存相关的参数设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 这些参数只能在数据库服务重新启动后生效。

## memorypool_enable

**参数说明**: 设置是否允许使用内存池。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许使用内存池。
- off表示不允许使用内存池。

**默认值**: off

## memorypool_size

**参数说明**: 设置内存池大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，128*1024～INT_MAX/2，单位为KB。

**默认值**: 512MB

## enable_memory_limit

**参数说明**: 启用逻辑内存管理模块。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示启用逻辑内存管理模块。
- off表示不启用逻辑内存管理模块。

**默认值**: on

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 若max_process_memory-shared_buffers-cstore_buffers-元数据少于2G，MogDB强制把enable_memory_limit设置为off。其中元数据是MogDB内部使用的内存和部分并发参数，如max_connections、thread_pool_attr、max_prepared_transactions等参数相关。
> - 当该值为off时，不对数据库使用的内存做限制，在大并发或者复杂查询时，使用内存过多，可能导致操作系统OOM问题。

## max_process_memory

**参数说明**: 设置一个数据库节点可用的最大物理内存。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型 , `2*1024*1024` ～ INT_MAX，单位为KB。

**默认值**: 12GB。

**设置建议:**

数据库节点上该数值需要根据系统物理内存及单节点部署主数据库节点个数决定。建议计算公式如下: `(物理内存大小 - vm.min_free_kbytes) * 0.7 / (1 + 主节点个数)`。该系数的目的是尽可能保证系统的可靠性，不会因数据库内存膨胀导致节点OOM。这个公式中提到vm.min_free_kbytes，其含义是预留操作系统内存供内核使用，通常用作操作系统内核中通信收发内存分配，至少为5%内存。即，max_process_memory = `物理内存 * 0.665 / (1 + 主节点个数)`。

## enable_memory_context_control

**参数说明**: 启用检查内存上下文是否超过给定限制的功能。仅适用于DEBUG版本。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示启用最大内存上下文限制检查功能。
- off表示关闭最大内存上下文限制检查功能。

**默认值**: off

## uncontrolled_memory_context

**参数说明**: 启用检查内存上下文是否超过给定限制的功能时，设置不受此功能约束。仅适用于DEBUG版本。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

查询时会在参数值的最前面添加标题含义字符串“MemoryContext white list:”。

**取值范围**: 字符串

**默认值**: 空

## shared_buffers

**参数说明**: 设置MogDB使用的共享内存大小。增加此参数的值会使MogDB比系统默认设置需要更多的System V共享内存。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，16 ~ 1073741823 ，单位为KB。

shared_buffers需要设置为BLCKSZ的整数倍，BLCKSZ目前设置为8KB，即shared_buffers需要设置为8KB整数倍。改变BLCKSZ的值会改变最小值。

**默认值**: 数据库节点为1GB。如果操作系统支持的共享内存小于32MB，则在初始化数据存储区时会自动调整为操作系统支持的最大值。

**设置建议:**

建议设置shared_buffers值为内存的40%以内。行存列存分开对待。行存设大，列存设小。列存: `(单服务器内存/单服务器数据库节点个数) * 0.4 * 0.25`。

如果设置较大的shared_buffers需要同时增加checkpoint_segments的值，因为写入大量新增、修改数据需要消耗更多的时间周期。

## segment_buffers

**参数说明**: 设置MogDB段页式元数据页的内存大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值说明：**整型，16 ~ 1073741823，单位为8KB。

segment_buffers需要设置为BLCKSZ的整数倍，BLCKSZ目前设置为8KB，即segment_buffers需要设置为8KB整数倍。改变BLCKSZ的值会改变最小值。

**默认值**: 8MB

**设置建议**:

segment_buffers 用来缓存段页式段头的内容，属于关键元数据信息，为了提高性能建议常用的表的段头都能缓存在buffer中，不被置换出去。建议按照`表的个数（包括索引和toast表）* 分区数 * 3 + 128` 来设置。乘以3是因为每个表（分区）会有一些额外的元数据段，一般一个表有3个段。最后+128因为段页式表空间管理需要一定数量的buffer。

## bulk_write_ring_size

**参数说明**: 数据并行导入使用的环形缓冲区大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，16384 ~ 2147483647，单位为KB。

**默认值**: 2GB

**设置建议:**建议导入压力大的场景中增加数据库节点中此参数配置。

## standby_shared_buffers_fraction

**参数说明**: 备实例所在服务器使用shared_buffers内存缓冲区大小的比例。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 双精度类型，0.1~1.0

**默认值**: 0.3

## temp_buffers

**参数说明**: 设置每个数据库会话使用的LOCAL临时缓冲区的大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

在每个会话的第一次使用临时表之前可以改变temp_buffers的值，之后的设置将是无效的。

一个会话将按照temp_buffers给出的限制，根据需要分配临时缓冲区。如果在一个并不需要大量临时缓冲区的会话里设置一个大的数值，其开销只是一个缓冲区描述符的大小。当缓冲区被使用，就会额外消耗8192字节。

**取值范围**: 整型，100~1073741823，单位为8KB。

**默认值**: 1MB

## max_prepared_transactions

**参数说明**: 设置可以同时处于"预备"状态的事务的最大数目。增加此参数的值会使MogDB比系统默认设置需要更多的System V共享内存。

当MogDB部署为主备双机时，在备机上此参数的设置必须要高于或等于主机上的，否则无法在备机上进行查询操作。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型。0~262143。

**默认值**: 800

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 为避免在准备步骤失败，此参数的值不能小于max_connections。

## work_mem

**参数说明**:  设置内部排序操作和Hash表在开始写入临时磁盘文件之前使用的内存大小。ORDER BY，DISTINCT和merge joins都要用到排序操作。Hash表在散列连接、散列为基础的聚集、散列为基础的IN子查询处理中都要用到。

对于复杂的查询，可能会同时并发运行好几个排序或者散列操作，每个都可以使用此参数所声明的内存量，不足时会使用临时文件。同样，好几个正在运行的会话可能会同时进行排序操作。因此使用的总内存可能是work_mem的好几倍。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，64~2147483647，单位为KB。

**默认值**: 64MB

**设置建议:**

> 依据查询特点和并发来确定，一旦work_mem限定的物理内存不够，算子运算数据将写入临时表空间，带来5-10倍的性能下降，查询响应时间从秒级下降到分钟级。
>
> - 对于串行无并发的复杂查询场景，平均每个查询有5-10关联操作，建议work_mem=50%内存/10。
> - 对于串行无并发的简单查询场景，平均每个查询有2-5个关联操作，建议work_mem=50%内存/5。
> - 对于并发场景，建议work_mem=串行下的work_mem/物理并发数。

## query_mem

**参数说明**: 设置执行作业所使用的内存。如果设置的query_mem值大于0，在生成执行计划时，优化器会将作业的估算内存调整为该值。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0，或大于32M的整型，默认单位为KB。如果设置值为负数或小于32M，将设置为默认值0，此时优化器不会根据该值调整作业的估算内存。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果设置的query_mem值大于0，在生成执行计划时，优化器会将作业的估算内存调整为该值。
> - 如果设置值为负数或小于32MB，将设置为默认值0，此时优化器不会根据该值调整作业的估算内存。

## query_max_mem

**参数说明**: 设置执行作业所能够使用的最大内存。如果设置的query_max_mem值大于0，当作业执行时所使用内存超过该值时，将报错退出。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0，或大于32M的整型，默认单位为KB。如果设置值为负数或小于32M，将设置为默认值0，此时不会根据该值限制作业的内存使用。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果设置的query_max_mem值大于0，当作业执行时所使用内存超过该值时，将报错退出。
> - 如果设置值为负数或小于32M，将设置为默认值0，此时不会根据该值限制作业的内存使用。

## maintenance_work_mem

**参数说明**: 设置在维护性操作（比如VACUUM、CREATE INDEX、ALTER TABLE ADD FOREIGN KEY等）中可使用的最大的内存。该参数的设置会影响VACUUM、VACUUM FULL、CLUSTER、CREATE INDEX的执行效率。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024~INT_MAX，单位为KB。

**默认值**: 16MB

**设置建议:**

- 建议设置此参数的值大于[work_mem](#work_mem)，可以改进清理和恢复数据库转储的速度。因为在一个数据库会话里，任意时刻只有一个维护性操作可以执行，并且在执行维护性操作时不会有太多的会话。
- 当自动清理线程运行时，autovacuum_max_workers倍数的内存将会被分配，所以此时设置maintenance_work_mem的值应该不小于[work_mem](#work_mem)。
- 如果进行大数据量的cluster等，可以在session中调大该值。

## psort_work_mem

**参数说明**: 设置列存表在进行局部排序中，在开始写入临时磁盘文件之前使用的内存大小。带partial cluster key的表、带索引的表插入、创建表索引、删除表和更新表都会用到。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 同样，多个正在运行的会话可能会同时进行表的局部排序操作。因此，使用的总内存可能是psort_work_mem的几倍。

**取值范围**: 整型64~2147483647，单位为KB。

**默认值**: 512MB

## max_loaded_cudesc

**参数说明**: 设置列存表在做扫描时，每列缓存cudesc信息的个数。增大设置会提高查询性能，但也会增加内存占用，特别是当列存表的列非常多时。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> max_loaded_cudesc设置过高时，有可能引起内存分配不足。

**取值范围**: 100~1073741823。

**默认值**: 1024

## max_stack_depth

**参数说明**: 设置MogDB执行堆栈的最大安全深度。需要这个安全界限是因为在服务器里，并非所有程序都检查了堆栈深度，只是在可能递规的过程，比如表达式计算这样的过程里面才进行检查。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，100~INT_MAX，单位为KB。

**默认值**: 2MB

**设置原则**:

- 数据库需要预留640KB堆栈深度，因此此参数的最佳设置是等于操作系统内核允许的最大值（就是ulimit -s的设置）- 640KB。
- 如果设置此参数的值大于实际的内核限制，则一个正在运行的递归函数可能会导致一个独立的服务器进程崩溃。在MogDB能够检测内核限制的操作系统上（SLES上），将自动限制设置为一个不安全的值。
- 因为并非所有的操作都能够检测，所以建议用户在此设置一个明确的值。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 默认值2MB，这个值相对比较小，不容易导致系统崩溃。

## cstore_buffers

**参数说明**: 设置列存所使用的共享缓冲区的大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，16384～1073741823，单位为KB。

**默认值**: 1GB

**设置建议**:

列存表使用cstore_buffers设置的共享缓冲区，几乎不用shared_buffers。因此在列存表为主的场景中，应减少shared_buffers，增加cstore_buffers。

## bulk_read_ring_size

**参数说明**: 并行导出，使用的环形缓冲区大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，256~2147483647，单位为KB。

**默认值**: 16MB

## enable_early_free

**参数说明**: 控制是否可以实现算子内存的提前释放。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示支持算子内存提前释放。
- off表示不支持算子内存提前释放。

**默认值**: on
