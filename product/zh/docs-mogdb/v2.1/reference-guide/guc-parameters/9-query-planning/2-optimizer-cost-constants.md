---
title: 优化器开销常量
summary: 优化器开销常量
author: Zhang Cuiping
date: 2021-04-20
---

# 优化器开销常量

介绍优化器开销常量。这里描述的开销可以按照任意标准度量。只关心其相对值，因此以相同的系数缩放它们将不会对优化器的选择产生任何影响。缺省时，它们以抓取顺序页的开销为基本单位。也就是说将seq_page_cost设为1.0，同时其他开销参数以它为基准设置。也可以使用其他基准，比如以毫秒计的实际执行时间。

## seq_page_cost

**参数说明**: 设置优化器计算一次顺序磁盘页面抓取的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 1

## random_page_cost

**参数说明**: 设置优化器计算一次非顺序抓取磁盘页面的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 虽然服务器允许将random_page_cost设置的比seq_page_cost小，但是物理上实际不受影响。如果所有数据库都位于随机访问内存中时，两者设置为相等很合理。因为在此种情况下，非顺序抓取页并没有副作用。同样，在缓冲率很高的数据库上，应该相对于CPU参数同时降低这两个值，因为获取内存中的页要比通常情况下开销小很多。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 4

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 对于特别表空间中的表和索引，可以通过设置同名的表空间的参数来覆盖这个值。
> - 相对于seq_page_cost，减少这个值将导致系统更倾向于使用索引扫描，而增加这个值使得索引扫描开销比较高。可以通过同时增加或减少这两个值来调整磁盘I/O相对于CPU的开销。

## cpu_tuple_cost

**参数说明**: 设置优化器计算在一次查询中处理每一行数据的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 0.01

## cpu_index_tuple_cost

**参数说明**: 设置优化器计算在一次索引扫描中处理每条索引的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 0.005

## cpu_operator_cost

**参数说明**: 设置优化器计算一次查询中执行一个操作符或函数的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 0.0025

## effective_cache_size

**参数说明**: 设置优化器在一次单一的查询中可用的磁盘缓冲区的有效大小。

设置这个参数，还要考虑MogDB的共享缓冲区以及内核的磁盘缓冲区。另外，还要考虑预计的在不同表之间的并发查询数目，因为它们将共享可用的空间。

这个参数对MogDB分配的共享内存大小没有影响，它也不会使用内核磁盘缓冲，它只用于估算。数值是用磁盘页来计算的，通常每个页面是8192字节。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～INT_MAX，单位为8KB。

比默认值高的数值可能会导致使用索引扫描，更低的数值可能会导致选择顺序扫描。

**默认值**: 128MB

## allocate_mem_cost

**参数说明**: 设置优化器计算Hash Join创建Hash表开辟内存空间所需的开销，供Hash join估算不准时调优使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～DBL_MAX。

**默认值**: 0
