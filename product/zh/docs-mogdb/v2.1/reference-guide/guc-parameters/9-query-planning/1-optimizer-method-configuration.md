---
title: 优化器方法配置
summary: 优化器方法配置
author: Zhang Cuiping
date: 2021-04-20
---

# 优化器方法配置

这些配置参数提供了影响查询优化器选择查询规划的原始方法。如果优化器为特定的查询选择的缺省规划并不是最优的，可以通过使用这些配置参数强制优化器选择一个不同的规划来临时解决这个问题。更好的方法包括调节优化器开销常量、手动运行ANALYZE、增加配置参数default_statistics_target的值、增加使用ALTER TABLE SET STATISTICS为指定列增加收集的统计信息。

## enable_bitmapscan

**参数说明**: 控制优化器对位图扫描规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## force_bitmapand

**参数说明**: 控制优化器强制使用bitmapand规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## enable_hashagg

**参数说明**: 控制优化器对Hash聚集规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_hashjoin

**参数说明**: 控制优化器对Hash连接规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_indexscan

**参数说明**: 控制优化器对索引扫描规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_indexonlyscan

**参数说明**: 控制优化器对仅索引扫描规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_material

**参数说明**: 控制优化器对实体化的使用。消除整个实体化是不可能的，但是可以关闭这个变量以防止优化器插入实体节点。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_mergejoin

**参数说明**: 控制优化器对融合连接规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## enable_nestloop

**参数说明**: 控制优化器对内表全表扫描嵌套循环连接规划类型的使用。完全消除嵌套循环连接是不可能的，但是关闭这个变量就会让优化器在存在其他方法的时候优先选择其他方法。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## enable_index_nestloop

**参数说明**: 控制优化器对内表参数化索引扫描嵌套循环连接规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_seqscan

**参数说明**: 控制优化器对顺序扫描规划类型的使用。完全消除顺序扫描是不可能的，但是关闭这个变量会让优化器在存在其他方法的时候优先选择其他方法。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_sort

**参数说明**: 控制优化器使用的排序步骤。完全消除明确的排序是不可能的，但是关闭这个变量可以让优化器在存在其他方法的时候优先选择其他方法。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_tidscan

**参数说明**: 控制优化器对TID扫描规划类型的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_kill_query

**参数说明**: CASCADE模式删除用户时，会删除此用户拥有的所有对象。此参数标识是否允许在删除用户的时候，取消锁定此用户所属对象的query。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许取消锁定。
- off表示不允许取消锁定。

**默认值**: off

## enforce_a_behavior

**参数说明**: 控制正则表达式的规则匹配模式。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示正则表达式采用A格式的匹配规则。
- off表示正则表达式采用POSIX格式的匹配规则。

**默认值**: on

## max_recursive_times

**参数说明**: 控制with recursive的最大迭代次数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX。

**默认值**: 200

## enable_vector_engine

**参数说明**: 控制优化器对向量化执行引擎的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## enable_change_hjcost

**参数说明**: 控制优化器在Hash Join代价估算路径选择时，是否使用将内表运行时代价排除在Hash Join节点运行时代价外的估算方式。如果使用，则有利于选择条数少，但运行代价大的表做内表。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## enable_absolute_tablespace

**参数说明**: 控制表空间是否可以使用绝对路径。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示可以使用绝对路径。
- off表示不可以使用绝对路径。

**默认值**: on

## enable_valuepartition_pruning

**参数说明**: 是否对DFS分区表进行静态/动态优化。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示对DFS分区表进行静态/动态优化。
- off表示不对DFS分区表进行静态/动态优化。

**默认值**: on

## qrw_inlist2join_optmode

**参数说明**: 控制是否使用inlist-to-join查询重写。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- disable: 关闭inlist2join查询重写。
- cost_base: 基于代价的inlist2join查询重写。
- rule_base: 基于规则的inlist2join查询重写，即强制使用inlist2join查询重写。
- 任意正整数: inlist2join查询重写阈值，即list内元素个数大于该阈值，进行inlist2join查询重写。

**默认值**: cost_base

## skew_option

**参数说明**: 控制是否使用优化策略。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- off: 关闭策略。
- normal: 采用激进策略。对于不确定是否出现倾斜的场景，认为存在倾斜，并进行相应优化。
- lazy: 采用保守策略。对于不确定是否出现倾斜场景，认为不存在倾斜，不进行优化。

**默认值**: normal

## default_limit_rows

**参数说明**: 设置生成genericplan的缺省limit估算行数。此参数设置为正数时意为直接将设置的值作为估算limit的行数，为负数时代表使用百分比的形式设置默认的估算值，负数转换为默认百分比，即-5代表5%。

**取值范围**: 浮点型，-100~DBL_MAX。

**默认值**: -10

## check_implicit_conversions

**参数说明**: 控制是否对查询中有隐式类型转换的索引列是否会生成候选索引路径进行检查。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示对查询中有隐式类型转换的索引列是否会生成候选索引路径进行检查。
- off表示不进行相关检查。

**默认值**: off
