---
title: 系统性能快照
summary: 系统性能快照
author: Zhang Cuiping
date: 2021-04-20
---

# 系统性能快照

## enable_wdr_snapshot

**参数说明**: 是否开启数据库监控快照功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on: 打开数据库监控快照功能。
- off: 关闭数据库监控快照功能。

**默认值**: off

## wdr_snapshot_retention_days

**参数说明**: 系统中数据库监控快照数据的保留天数，超过设置的值之后，系统每隔wdr_snapshot_interval时间间隔，清理snapshot_id最小的快照数据。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～8。

**默认值**: 8

## wdr_snapshot_query_timeout

**参数说明**: 系统执行数据库监控快照操作时，设置快照操作相关的sql语句的执行超时时间。如果语句超过设置的时间没有执行完并返回结果，则本次快照操作失败。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，100～INT_MAX（秒）。

**默认值**: 100s

## wdr_snapshot_interval

**参数说明**: 后台线程Snapshot自动对数据库监控数据执行快照操作的时间间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10～60（分钟）。

**默认值**: 1h

## asp_flush_mode

**参数说明**: ASP刷新到磁盘上的方式分为写文件和写系统表，当为‘file’时，默认写文件，为‘table’时写系统表，为‘all’时，即写文件也写系统表，仅sysadmin用户可以访问。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，‘table’、‘file’、‘all’。

**默认值**: ‘table’

## asp_flush_rate

**参数说明**: 当内存中样本个数达到asp_sample_num时，会按一定比例把内存中样本刷新到磁盘上，asp_flush_rate为刷新比例。该参数为10时表示按10：1进行刷新。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~10。

**默认值**: 10

## asp_log_filename

**参数说明**: 当ASP写文件时，该参数设置文件名的格式，仅sysadmin用户可以访问。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。

**默认值**: “asp-%Y-%m-%d_%H%M%S.log”

## asp_retention_days

**参数说明**: 当ASP样本写到系统表时，该参数表示保留的最大天数。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~7。

**默认值**: 2

## asp_sample_interval

**参数说明**: 每次采样的间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~10，单位为秒。

**默认值**: 1s

## asp_sample_num

**参数说明**: LOCAL_ACTIVE_SESSION视图最大的样本个数，仅sysadmin用户可以访问。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10000～100000。

**默认值**: 100000

## enable_asp

**参数说明**: 是否开启活跃会话信息active session profile。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on：打开active session profile功能。
- off：关闭active session profile功能。

**默认值**: on
