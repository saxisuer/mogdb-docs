---
title: 全密态数据库支持等值查询
summary: 全密态数据库支持等值查询
author: Zhang Cuiping
date: 2021-04-20
---

# 全密态数据库支持等值查询

## enable_full_encryption

**参数说明**: 全密态数据库开关，控制全密态数据库是否生效。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

enable_full_encryption设置为on后，使用gsql连接数据库时需要输入"-C"参数，或"-enable-client-encryption"，否则连接不成功。

**取值范围**: 布尔型

- on: 全密态数据库开关打开。
- off: 全密态数据库开关关闭。

**默认值**: off
