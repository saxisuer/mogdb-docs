---
title: 备服务器
summary: 备服务器
author: Zhang Cuiping
date: 2021-04-20
---

# 备服务器

## hot_standby

**参数说明**: 设置是否允许备机在恢复过程中连接和查询。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 如果此参数设置为on，wal_level必须设置为hot_standby，否则将导致数据库无法启动。
> - 在双机环境中，因为会对双机其他一些功能产生影响，hot_standby参数不能设置成off。

**取值范围**: 布尔型

- on表示允许备机在恢复过程中连接和查询。
- off表示不允许备机在恢复过程中连接和查询。

**默认值**: on

## max_standby_archive_delay

**参数说明**: 当开启双机热备模式时，如果备机正处理归档WAL日志数据，这时进行查询就会产生冲突，此参数就是设置备机取消查询之前所等待的时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> -1表示允许备机一直等待冲突的查询完成。

**取值范围**: 整型，范围: -1~INT_MAX，单位为毫秒。

**默认值**: 3s（即3000ms）

## max_standby_streaming_delay

**参数说明**: 当开启双机热备模式时，如果备机正通过流复制接收WAL日志数据，这时进行查询就会产生冲突，这个参数就是设置备机取消查询之前所等待的时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> -1表示允许备机一直等待冲突的查询完成。

**取值范围**: 整型（毫秒），范围: -1~INT_MAX。

**默认值**:  3s（即3000ms）

## wal_receiver_status_interval

**参数说明**: 设置WAL日志接收进程的状态通知给主机的最大时间间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，范围: 0 ~ INT_MAX，单位为毫秒。

**默认值**: 5s（即5000ms）

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
> 当该参数设置为0时，表示关闭备机向主机反馈日志接收位置等信息，可能会导致主机事务提交阻塞、switchover操作失败等异常现象。正常业务场景，不建议将该参数设置为0。

## hot_standby_feedback

**参数说明**: 设置是否允许将备机上执行查询的结果反馈给主机，这可以避免查询冲突。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许将备机上执行查询的最小事务号反馈给主机。
- off表示不允许将备机上执行查询的最小事务号反馈给主机。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 当该参数为on时，主机的旧版本数据的清理会受限于备机正在读的事务，即主机只允许清理小于备机反馈回来的事务所作的更改。 所以，若该参数开启时，会影响主机的性能。

## wal_receiver_timeout

**参数说明**: 设置从主机接收数据的最大等待时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ INT_MAX，单位为毫秒。

**默认值**: 6s（即6000ms）

## wal_receiver_connect_timeout

**参数说明**: 设置连接主机的最大等待超时时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ INT_MAX / 1000，单位为秒。

**默认值**: 2s

## wal_receiver_connect_retries

**参数说明**: 设置连接主机的最大尝试次数。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~ INT_MAX。

**默认值**: 1

## wal_receiver_buffer_size

**参数说明**: 备机与从备接收Xlog存放到内存缓冲区的大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，4096~1047552，单位为KB。

**默认值**: 64MB（即65536KB）

## primary_slotname

**参数说明**: 设置备机对应主机的slot name，用于主备校验，与wal日志删除机制。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符型

**默认值**: 空字符串
