---
title: 操作审计
summary: 操作审计
author: Zhang Cuiping
date: 2021-04-20
---

# 操作审计

## audit_system_object

**参数说明**: 该参数决定是否对MogDB数据库对象的CREATE、DROP、ALTER操作进行审计。MogDB数据库对象包括DATABASE、USER、schema、TABLE等。通过修改该配置参数的值，可以只审计需要的数据库对象的操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～524287

- 0代表关闭MogDB数据库对象的CREATE、DROP、ALTER操作审计功能。
- 非0代表只审计MogDB的某类或者某些数据库对象的CREATE、DROP、ALTER操作。

**取值说明**:

该参数的值由20个二进制位的组合求出，这20个二进制位分别代表MogDB的20类数据库对象。如果对应的二进制位取值为0，表示不审计对应的数据库对象的CREATE、DROP、ALTER操作；取值为1，表示审计对应的数据库对象的CREATE、DROP、ALTER操作。这19个二进制位代表的具体审计内容请参见[表1](#audit_system_object)。

**默认值**: 12295

**表 1** audit_system_object取值含义说明<a id="audit_system_object"> </a>

| <span style="white-space:nowrap;">二进制位 </span> | 含义                                                         | 取值说明                                                     |
| -------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 第0位                                              | 是否审计DATABASE对象的CREATE、DROP、ALTER操作。              | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第1位                                              | 是否审计SCHEMA对象的CREATE、DROP、ALTER操作。                | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第2位                                              | 是否审计USER对象的CREATE、DROP、ALTER操作。                  | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第3位                                              | 是否审计TABLE对象的CREATE、DROP、ALTER、TRUNCATE操作。       | - 0表示不审计该对象的CREATE、DROP、ALTER、TRUNCATE操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER、TRUNCATE操作。 |
| 第4位                                              | 是否审计INDEX对象的CREATE、DROP、ALTER操作。                 | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第5位                                              | 是否审计VIEW对象的CREATE、DROP操作。                         | - 0表示不审计该对象的CREATE、DROP操作；<br />- 1表示审计该对象的CREATE、DROP操作。 |
| 第6位                                              | 是否审计TRIGGER对象的CREATE、DROP、ALTER操作。               | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第7位                                              | 是否审计PROCEDURE/FUNCTION对象的CREATE、DROP、ALTER操作。    | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第8位                                              | 是否审计TABLESPACE对象的CREATE、DROP、ALTER操作。            | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第9位                                              | 是否审计RESOURCE POOL对象的CREATE、DROP、ALTER操作。         | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作 |
| 第10位                                             | 是否审计WORKLOAD对象的CREATE、DROP、ALTER操作。              | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作 |
| 第11位                                             | 是否审计SERVER FOR HADOOP对象的CREATE、DROP、ALTER操作。     | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作 |
| 第12位                                             | 是否审计DATA SOURCE对象的CRAETE、DROP、ALTER操作。           | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第13位                                             | 是否审计NODE GROUP对象的CREATE、DROP操作。                   | - 0表示不审计该对象的CREATE、DROP操作；<br />- 1表示审计该对象的CREATE、DROP操作。 |
| 第14位                                             | 是否审计ROW LEVEL SECURITY对象的CREATE、DROP、ALTER操作。    | - 0表示不审计该对象的CREATE、DROP、ALTER操作；<br />- 1表示审计该对象的CREATE、DROP、ALTER操作。 |
| 第15位                                             | 是否审计TYPE对象的CREATE、DROP、ALTER操作。                  | - 0表示不审计TYPE对象的CREATE、DROP、ALTER操作；<br />- 1表示审计TYPE对象的CREATE、DROP、ALTER操作。 |
| 第16位                                             | 是否审计TEXT SEARCH对象（CONFIGURATION和DICTIONARY）的CREATE、DROP、ALTER操作。 | - 0表示不审计TEXT SEARCH对象的CREATE、DROP、ALTER操作；<br />- 1表示审计TEXT SEARCH对象的CREATE、DROP、ALTER操作。 |
| 第17位                                             | 是否审计DIRECTORY对象的CREATE、DROP、ALTER操作。             | - 0表示不审计DIRECTORY对象的CREATE、DROP、ALTER操作；<br />- 1表示审计DIRECTORY对象的CREATE、DROP、ALTER操作。 |
| 第18位                                             | 是否审计SYNONYM对象的CREATE、DROP、ALTER操作。               | - 0表示不审计SYNONYM对象的CREATE、DROP、ALTER操作；<br />- 1表示审计SYNONYM对象的CREATE、DROP、ALTER操作。 |
| 第19位                                             | 是否审计SEQUENCE对象的CREATE、DROP、ALTER操作。              | - 0表示不审计SEQUENCE对象的CREATE、DROP、ALTER操作；<br />- 1表示审计SEQUENCE对象的CREATE、DROP、ALTER操作。 |
| 第20位                                             | 是否审计CMK、CEK对象的CREATE、DROP操作。                     | - 0表示不审计CMK、CEK对象的CREATE、DROP操作；<br />- 1表示审计CMK、CEK对象的CREATE、DROP操作。 |

## audit_dml_state

**参数说明**: 这个参数决定是否对具体表的INSERT、UPDATE、DELETE操作进行审计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭具体表的DML操作（SELECT除外）审计功能。
- 1表示开启具体表的DML操作（SELECT除外）审计功能。

**默认值**: 0

## audit_dml_state_select

**参数说明**: 这个参数决定是否对SELECT操作进行审计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭SELECT操作审计功能。
- 1表示开启SELECT审计操作功能。

**默认值**: 0

## audit_function_exec

参数说明:这个参数决定在执行存储过程、匿名块或自定义函数（不包括系统自带函数）时是否记录审计信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭过程或函数执行的审计功能。
- 1表示开启过程或函数执行的审计功能。

**默认值**: 0

## audit_copy_exec

**参数说明**: 这个参数决定是否对COPY操作进行审计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭COPY审计功能。
- 1表示开启COPY审计功能。

**默认值**: 0

## audit_set_parameter

**参数说明**: 这个参数决定是否对SET操作进行审计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭SET审计功能。
- 1表示开启SET审计功能。

**默认值**: 1

## enableSeparationOfDuty

**参数说明**: 是否开启三权分立选项。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启三权分立。
- off表示不开启三权分立。

**默认值**: off

## enable_nonsysadmin_execute_direct

**参数说明**: 是否允许非系统管理员执行EXECUTE DIRECT ON语句。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许任意用户执行EXECUTE DIRECT ON语句。
- off表示只允许系统管理员执行EXECUTE DIRECT ON语句。

**默认值**: off

## enable_access_server_directory

**参数说明**: 是否开启非初始用户创建、修改和删除DIRECTORY的权限。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启非初始用户创建、修改和删除DIRECTORY的权限。
- off表示不开启非初始用户创建、修改和删除DIRECTORY的权限。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 出于安全考虑，默认情况下，只有初始用户才能够创建、修改和删除DIRECTORY对象。
>
> - 如果开启了enable_access_server_directory，具有SYSADMIN权限的用户和继承了内置角色gs_role_directory_create权限的用户可以创建directory对象；具有SYSADMIN权限的用户、directory对象的属主、被授予了该directory的DROP权限的用户或者继承了内置角色gs_role_directory_drop权限的用户可以删除directory对象；具有SYSADMIN权限的用户和directory对象的属主可以修改directory对象的所有者，且要求该用户是新属主的成员。
