---
title: 用户自定义函数
summary: 用户自定义函数
author: Zhang Cuiping
date: 2021-11-08
---

# 用户自定义函数

## udf_memory_limit

**参数说明**: 控制每个数据库节点执行UDF时可用的最大物理内存量。本参数当前版本不生效，请使用FencedUDFMemoryLimit和UDFWorkerMemHardLimit参数控制fenced udf worker虚存。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，200\*1024～max_process_memory，单位为KB。

**默认值**: 200MB

## FencedUDFMemoryLimit

**参数说明**: 控制每个fenced udf worker进程使用的虚拟内存。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整数，0 \~ 2147483647，单位为KB，设置可带单位（KB，MB，GB）。其中0表示不做内存控制。

**默认值**: 0

## UDFWorkerMemHardLimit

**参数说明**: 控制fencedUDFMemoryLimit的最大值。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整数，0 \~ 2147483647，单位为KB，设置时可带单位（KB，MB，GB）。

**默认值**: 1GB

## pljava_vmoptions

**参数说明**: 用户自定义设置PL/Java函数所使用的JVM虚拟机的启动参数，仅sysadmin用户可以访问。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，支持：

- JDK8 JVM启动参数（可参见JDK[官方](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html)说明）
- JDK8 JVM系统属性参数（以-D开头如-Djava.ext.dirs，可参见JDK[官方](https://docs.oracle.com/javase/tutorial/deployment/doingMoreWithRIA/properties.html)说明）
- 用户自定义参数（以-D开头，如-Duser.defined.option）

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>如果用户在pljava_vmoptions中设置参数不满足上述取值范围，会在使用PL/Java语言函数时报错。

**默认值**: 空
