---
title: 性能统计
summary: 性能统计
author: Zhang Cuiping
date: 2021-04-20
---

# 性能统计

在数据库运行过程中，会涉及到锁的访问、磁盘IO操作、无效消息的处理，这些操作都可能是数据库的性能瓶颈，通过MogDB提供的性能统计方法，可以方便定位性能问题。

**输出性能统计日志**

**参数说明**: 对每条查询，以下4个选项控制在服务器日志里记录相应模块的性能统计数据，具体含义如下:

- log_parser_stats控制在服务器日志里记录解析器的性能统计数据。
- log_planner_stats控制在服务器日志里记录查询优化器的性能统计数据。
- log_executor_stats控制在服务器日志里记录执行器的性能统计数据。
- log_statement_stats控制在服务器日志里记录整个语句的性能统计数据。

这些参数只能辅助管理员进行粗略分析，类似Linux中的操作系统工具getrusage() 。

这些参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - log_statement_stats记录总的语句统计数据，而其他的只记录针对每个模块的统计数据。
> - log_statement_stats不能和其他任何针对每个模块统计的选项一起打开。

**取值范围**: 布尔型

- on表示开启记录性能统计数据的功能。
- off表示关闭记录性能统计数据的功能。

**默认值: off**
