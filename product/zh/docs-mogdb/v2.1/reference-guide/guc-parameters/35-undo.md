---
title: Undo
summary: Undo
author: Zhang Cuiping
date: 2021-11-08
---

# Undo

## undo_space_limit_size

**参数说明**: 用于控制undo强制回收阈值，达到阈值的80%启动强制回收。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，800M~16TB

**默认值**: 32GB

## undo_limit_size_per_transaction

**参数说明**: 用于控制单事务undo分配空间阈值，达到阈值时事务报错回滚。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，2M~16TB

**默认值**: 32GB

## undo_zone_count

**参数说明**: 用于控制在内存中可分配的undo zone数量，达到阈值或本次设置的数值小于上次时报错，重新按规则设置合理取值后可恢复正常。当参数设置为0时，禁用undo，禁止创建ustore表。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~1,048,576

取值约束：

1. 本次设置数值不得小于上次设置的数值，设置错误时会导致数据库无法正常恢复，重新合理设置后重启数据库可恢复正常。
2. 当前undo_zone_count取值需要根据max_connections来调节设置，修改max_connections取值后，需对应修改undo_zone_count，取值规则为max_connections取值的4倍。

**默认值**: 0
