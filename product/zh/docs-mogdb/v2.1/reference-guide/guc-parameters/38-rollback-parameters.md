---
title: 回滚相关参数
summary: 回滚相关参数
author: Zhang Cuiping
date: 2021-11-08
---

# 回滚相关参数

## max_undo_workers

**参数说明**: 异步回滚调用的undoworker线程数量，参数重启生效。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~100

**默认值**: 5
