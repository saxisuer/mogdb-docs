---
title: 关键字
summary: 关键字
author: Zhang Cuiping
date: 2021-05-17
---

# 关键字

SQL里有保留字和非保留字之分。根据标准，保留字决不能用做其他标识符。非保留字只是在特定的环境里有特殊的含义，而在其他环境里是可以用做标识符的。

标识符的命名需要遵守如下规范：

- 标识符需要为字母、下划线、数字（0-9）或美元符号（$）。

- 标识符必须以字母（a-z）或下划线（_）开头。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 此命名规范为建议项，非强制项。
  > - 特殊情况下可以使用双引号规避特殊字符报错。

**表 1** SQL关键字

| 关键字                     | MogDB          |
| :------------------------- | :------------- |
| ALL                        | reserved       |
| ANALYSE                    | reserved       |
| ANALYZE                    | reserved       |
| AND                        | reserved       |
| ANY                        | reserved       |
| ARRAY                      | reserved       |
| AS                         | reserved       |
| ASC                        | reserved       |
| ASYMMETRIC                 | reserved       |
| AUTHID                     | reserved       |
| AUTHORIZATION              | type-func-name |
| BETWEEN                    | col-name       |
| BIGINT                     | col-name       |
| BINARY                     | type-func-name |
| BINARY_DOUBLE              | col-name       |
| BINARY_FLOAT               | col-name       |
| BINARY_INTEGER             | col-name       |
| BIT                        | col-name       |
| BOOLEAN                    | col-name       |
| BOTH                       | reserved       |
| BUCKETCNT                  | col-name       |
| BUCKETS                    | reserved       |
| BYTEAWITHOUTORDER          | col-name       |
| BYTEAWITHOUTORDERWITHEQUAL | col-name       |
| CASE                       | reserved       |
| CAST                       | reserved       |
| CHAR                       | col-name       |
| CHARACTER                  | col-name       |
| CHECK                      | reserved       |
| COALESCE                   | col-name       |
| COLLATE                    | reserved       |
| COLLATION                  | type-func-name |
| COLUMN                     | reserved       |
| COMPACT                    | type-func-name |
| CONCURRENTLY               | type-func-name |
| CONSTRAINT                 | reserved       |
| CREATE                     | reserved       |
| CROSS                      | type-func-name |
| CSN                        | type-func-name |
| CURRENT_CATALOG            | reserved       |
| CURRENT_DATE               | reserved       |
| CURRENT_ROLE               | reserved       |
| CURRENT_SCHEMA             | type-func-name |
| CURRENT_TIME               | reserved       |
| CURRENT_TIMESTAMP          | reserved       |
| CURRENT_USER               | reserved       |
| DATE                       | col-name       |
| DEC                        | col-name       |
| DECIMAL                    | col-name       |
| DECODE                     | col-name       |
| DEFAULT                    | reserved       |
| DEFERRABLE                 | reserved       |
| DELTAMERGE                 | type-func-name |
| DESC                       | reserved       |
| DISTINCT                   | reserved       |
| DO                         | reserved       |
| ELSE                       | reserved       |
| END                        | reserved       |
| EXCEPT                     | reserved       |
| EXCLUDED                   | reserved       |
| EXISTS                     | col-name       |
| EXTRACT                    | col-name       |
| FALSE                      | reserved       |
| FENCED                     | reserved       |
| FETCH                      | reserved       |
| FLOAT                      | col-name       |
| FOR                        | reserved       |
| FOREIGN                    | reserved       |
| FREEZE                     | type-func-name |
| FROM                       | reserved       |
| FULL                       | type-func-name |
| GRANT                      | reserved       |
| GREATEST                   | col-name       |
| GROUP                      | reserved       |
| GROUPING                   | col-name       |
| GROUPPARENT                | reserved       |
| HAVING                     | reserved       |
| HDFSDIRECTORY              | type-func-name |
| ILIKE                      | type-func-name |
| IN                         | reserved       |
| INITIALLY                  | reserved       |
| INNER                      | type-func-name |
| INOUT                      | col-name       |
| INT                        | col-name       |
| INTEGER                    | col-name       |
| INTERSECT                  | reserved       |
| INTERVAL                   | col-name       |
| INTO                       | reserved       |
| IS                         | reserved       |
| JOIN                       | type-func-name |
| LEADING                    | reserved       |
| LEAST                      | col-name       |
| LEFT                       | type-func-name |
| LESS                       | reserved       |
| LIKE                       | type-func-name |
| LIMIT                      | reserved       |
| LOCALTIME                  | reserved       |
| LOCALTIMESTAMP             | reserved       |
| MAXVALUE                   | reserved       |
| MINUS                      | reserved       |
| MODIFY                     | reserved       |
| NATIONAL                   | col-name       |
| NATURAL                    | type-func-name |
| NCHAR                      | col-name       |
| NLSSORT                    | reserved       |
| NONE                       | col-name       |
| NOT                        | reserved       |
| NOTNULL                    | type-func-name |
| NULL                       | reserved       |
| NULLIF                     | col-name       |
| NUMBER                     | col-name       |
| NUMERIC                    | col-name       |
| NVARCHAR2                  | col-name       |
| NVL                        | col-name       |
| OFFSET                     | reserved       |
| ON                         | reserved       |
| ONLY                       | reserved       |
| OR                         | reserved       |
| ORDER                      | reserved       |
| OUT                        | col-name       |
| OUTER                      | type-func-name |
| OVERLAPS                   | type-func-name |
| OVERLAY                    | col-name       |
| PERFORMANCE                | reserved       |
| PLACING                    | reserved       |
| POSITION                   | col-name       |
| PRECISION                  | col-name       |
| PRIMARY                    | reserved       |
| PROCEDURE                  | reserved       |
| REAL                       | col-name       |
| RECYCLEBIN                 | type-func-name |
| REFERENCES                 | reserved       |
| REJECT                     | reserved       |
| RETURNING                  | reserved       |
| RIGHT                      | type-func-name |
| ROW                        | col-name       |
| ROWNUM                     | reserved       |
| SELECT                     | reserved       |
| SESSION_USER               | reserved       |
| SETOF                      | col-name       |
| SIMILAR                    | type-func-name |
| SMALLDATETIME              | col-name       |
| SMALLINT                   | col-name       |
| SOME                       | reserved       |
| SUBSTRING                  | col-name       |
| SYMMETRIC                  | reserved       |
| SYSDATE                    | reserved       |
| SYSTIMESTAMP               | reserved       |
| TABLE                      | reserved       |
| TABLESAMPLE                | type-func-name |
| THEN                       | reserved       |
| TIME                       | col-name       |
| TIMECAPSULE                | type-func-name |
| TIMESTAMP                  | col-name       |
| TIMESTAMPDIFF              | col-name       |
| TINYINT                    | col-name       |
| TO                         | reserved       |
| TRAILING                   | reserved       |
| TREAT                      | col-name       |
| TRIM                       | col-name       |
| TRUE                       | reserved       |
| UNION                      | reserved       |
| UNIQUE                     | reserved       |
| USER                       | reserved       |
| USING                      | reserved       |
| VALUES                     | col-name       |
| VARCHAR                    | col-name       |
| VARCHAR2                   | col-name       |
| VARIADIC                   | reserved       |
| VERBOSE                    | type-func-name |
| VERIFY                     | reserved       |
| WHEN                       | reserved       |
| WHERE                      | reserved       |
| WINDOW                     | reserved       |
| WITH                       | reserved       |
| XMLATTRIBUTES              | col-name       |
| XMLCONCAT                  | col-name       |
| XMLELEMENT                 | col-name       |
| XMLEXISTS                  | col-name       |
| XMLFOREST                  | col-name       |
| XMLPARSE                   | col-name       |
| XMLPI                      | col-name       |
| XMLROOT                    | col-name       |
| XMLSERIALIZE               | col-name       |
