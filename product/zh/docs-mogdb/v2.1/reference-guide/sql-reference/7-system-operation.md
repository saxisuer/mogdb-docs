---
title: 系统操作
summary: 系统操作
author: Zhang Cuiping
date: 2021-05-17
---

# 系统操作

MogDB通过SQL语句执行不同的系统操作，比如：设置变量，显示执行计划和垃圾收集等操作。

## 设置变量

设置会话或事务中需要使用的各种参数，请参考[SET](../../reference-guide/sql-syntax/SET.md)。

## 显示执行计划

显示MogDB为SQL语句规划的执行计划，请参考[EXPLAIN](../../reference-guide/sql-syntax/EXPLAIN.md)。

## 事务日志检查点

预写式日志（WAL）缺省时在事务日志中每隔一段时间放置一个检查点。CHECKPOINT强制立即进行检查，而不是等到下一次调度时的检查点。请参考[CHECKPOINT](../../reference-guide/sql-syntax/CHECKPOINT.md)。

## 垃圾收集

进行垃圾收集以及可选择的对数据库进行分析。请参考[VACUUM](../../reference-guide/sql-syntax/VACUUM.md)。

## 收集统计信息

收集与数据库中表内容相关的统计信息。请参考[ANALYZE | ANALYSE](../../reference-guide/sql-syntax/ANALYZE-ANALYSE.md)。

## 设置当前事务的约束检查模式

设置当前事务里的约束检查的特性。请参考[SET CONSTRAINTS](../../reference-guide/sql-syntax/SET-CONSTRAINTS.md)。

## 关闭当前数据库节点

关闭当前数据库节点。请参考[SHUTDOWN](../../reference-guide/sql-syntax/SHUTDOWN.md)。
