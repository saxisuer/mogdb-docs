---
title: 扩展语法
summary: 扩展语法
author: Zhang Cuiping
date: 2021-05-18
---

# 扩展语法

MogDB提供的扩展语法如下。

**表 1** 扩展SQL语法

<table>
    <tr>
        <th>分类</th>
        <th>语法关键字</th>
        <th>描述</th>
    </tr>
    <tr>
        <td rowspan=2>创建表CREATE TABLE</td>
        <td>INHERITS ( parent_table [, … ] )</td>
        <td>支持继承表。</td>
    </tr>
    <tr>
        <td>column_constraint：<br/>REFERENCES reftable<br/>[ ( refcolumn ) ] [ MATCH<br/>
FULL | MATCH PARTIAL |<br/>MATCH SIMPLE ][ ON<br/>DELETE action ] [ ON<br/>UPDATE action ]</td>
        <td>支持用REFERENCES reftable[ ( refcolumn ) ] [ MATCH FULL |MATCH PARTIAL | MATCH SIMPLE ] [ ON DELETE action ] [ ON UPDATE action ] 为表创建外键约束。</td>
    </tr>
    <tr>
        <td rowspan=2>加载模块</td>
        <td>CREATE EXTENSION</td>
        <td>把一个新的模块（例如DBLINK）加载进当前数据库中。</td>
    </tr>
    <tr>
        <td>DROP EXTENSION</td>
        <td>删除已加载的模块。</td>
    </tr>
    <tr>
        <td rowspan=3>聚集函数</td>
        <td>CREATE AGGREGATE</td>
        <td>定义一个新的聚集函数。</td>
    </tr>
    <tr>
        <td>ALTER AGGREGATE</td>
        <td>修改一个聚集函数的定义。</td>
    </tr>
    <tr>
        <td>DROP AGGREGATE</td>
        <td>删除一个现存的聚集函数。</td>
    </tr>
</table>
