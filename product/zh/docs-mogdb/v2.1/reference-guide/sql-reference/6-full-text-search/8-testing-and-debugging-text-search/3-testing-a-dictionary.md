---
title: 词典测试
summary: 词典测试
author: Zhang Cuiping
date: 2021-05-17
---

# 词典测试

函数ts_lexize用于进行词典测试。

ts_lexize(dict regdictionary, token text) returns text[]如果输入的token可以被词典识别，那么ts_lexize返回词素的数组；如果token可以被词典识别到它是一个停用词，则返回空数组；如果是一个不可识别的词则返回NULL。

比如：

```sql
mogdb=# SELECT ts_lexize('english_stem', 'stars');
 ts_lexize
-----------
 {star}

mogdb=# SELECT ts_lexize('english_stem', 'a');
 ts_lexize
-----------
 {}
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
> ts_lexize函数支持单一token，不支持文本。
