---
title: 二进制类型
summary: 二进制类型
author: Guo Huan
date: 2021-04-06
---

# 二进制类型

MogDB支持的二进制类型请参见[表1](#biao1)。

**表 1** 二进制类型 <a id="biao1"> </a>

| 名称                           | 描述            | 存储空间                             |
| :-------------------------------- | :------------------ | :----------------------- |
| BLOB                           | 二进制大对象<br/>说明：<br />列存不支持BLOB类型     | 最大为1GB-8203字节（即1073733621字节）。                                |
| RAW                            | 变长的十六进制类型<br/>说明：<br />列存不支持RAW类型 | 4字节加上实际的十六进制字符串。最大为1GB-8203字节（即1073733621字节）。 |
| BYTEA                          | 变长的二进制字符串                                  | 4字节加上实际的二进制字符串。最大为1GB-8203字节（即1073733621字节）。   |
| BYTEAWITHOUTORDERWITHEQUALCOL  | 变长的二进制字符串（密态特性新增的类型，如果加密列的加密类型指定为确定性加密，则该列的实际类型为BYTEAWITHOUTORDERWITHEQUALCO），元命令打印加密表将显示原始数据类型。 | 4字节加上实际的二进制字符串。最大为1GB减去53字节（即1073741771字节）。  |
| BYTEAWITHOUTORDERCOL           | 变长的二进制字符串（密态特性新增的类型，如果加密列的加密类型指定为随机加密，则该列的实际类型为BYTEAWITHOUTORDERCOL），元命令打印加密表将显示原始数据类型。 | 4字节加上实际的二进制字符串。最大为1GB减去53字节（即1073741771字节）。  |
| _BYTEAWITHOUTORDERWITHEQUALCOL | 变长的二进制字符串，密态特性新增的类型              | 4字节加上实际的二进制字符串。最大为1GB减去53字节（即1073741771字节）。  |
| _BYTEAWITHOUTORDERCOL          | 变长的二进制字符串，密态特性新增的类型              | 4字节加上实际的二进制字符串。最大为1GB减去53字节（即1073741771字节）。  |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 除了每列的大小限制以外，每个元组的总大小也不可超过1GB-8203字节（即1073733621字节）。
> - 不支持直接使用BYTEAWITHOUTORDERWITHEQUALCOL和BYTEAWITHOUTORDERCOL，_BYTEAWITHOUTORDERWITHEQUALCOL，_BYTEAWITHOUTORDERCOL类型创建表。

示例:

```sql
--创建表。
mogdb=# CREATE TABLE blob_type_t1
(
    BT_COL1 INTEGER,
    BT_COL2 BLOB,
    BT_COL3 RAW,
    BT_COL4 BYTEA
) ;

--插入数据。
mogdb=# INSERT INTO blob_type_t1 VALUES(10,empty_blob(),
HEXTORAW('DEADBEEF'),E'\\xDEADBEEF');

--查询表中的数据。
mogdb=# SELECT * FROM blob_type_t1;
 bt_col1 | bt_col2 | bt_col3  |  bt_col4
---------+---------+----------+------------
      10 |         | DEADBEEF | \xdeadbeef
(1 row)

--删除表。
mogdb=# DROP TABLE blob_type_t1;
```
