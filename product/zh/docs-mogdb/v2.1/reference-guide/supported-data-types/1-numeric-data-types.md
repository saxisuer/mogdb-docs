---
title: 数值类型
summary: 数值类型
author: Guo Huan
date: 2021-04-06
---

# 数值类型

表1列出了所有的可用类型。数字操作符和相关的内置函数请参见[数字操作函数和操作符](../../reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md)。

**表 1** 整数类型

| 名称           | 描述                       | 存储空间 | 范围            |
| :------------------- | :------------------------- | :------- | :------------------------------------------------------ |
| TINYINT        | 微整数，别名为INT1。       | 1字节    | 0 ~ 255         |
| SMALLINT       | 小范围整数，别名为INT2。   | 2字节    | -32,768 ~ +32,767           |
| INTEGER        | 常用的整数，别名为INT4。   | 4字节    | -2,147,483,648 ~ +2,147,483,647                         |
| BINARY_INTEGER | 常用的整数INTEGER的别名。  | 4字节    | -2,147,483,648 ~ +2,147,483,647                         |
| BIGINT         | 大范围的整数，别名为INT8。 | 8字节    | -9,223,372,036,854,775,808 ~ +9,223,372,036,854,775,807 |
| int16 | 十六字节的大范围整数，目前不支持用户用于建表等使用。 | 16字节 | -170,141,183,460,469,231,731,687,303,715,884,105,728 ~ +170,141,183,460,469,231,731,687,303,715,884,105,727 |

示例:

```sql
--创建具有TINYINT类型数据的表。
mogdb=# CREATE TABLE int_type_t1
           (
            IT_COL1 TINYINT
           );

--向创建的表中插入数据。
mogdb=# INSERT INTO int_type_t1 VALUES(10);

--查看数据。
mogdb=# SELECT * FROM int_type_t1;
 it_col1
---------
 10
(1 row)

--删除表。
mogdb=# DROP TABLE int_type_t1;
```

```sql
--创建具有TINYINT,INTEGER,BIGINT类型数据的表。
mogdb=# CREATE TABLE int_type_t2
(
    a TINYINT,
    b TINYINT,
    c INTEGER,
    d BIGINT
);

--插入数据。
mogdb=# INSERT INTO int_type_t2 VALUES(100, 10, 1000, 10000);

--查看数据。
mogdb=# SELECT * FROM int_type_t2;
  a  | b  |  c   |   d
-----+----+------+-------
 100 | 10 | 1000 | 10000
(1 row)

--删除表。
mogdb=# DROP TABLE int_type_t2;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - TINYINT、SMALLINT、INTEGER、BIGINT和INT16类型存储各种范围的数字，也就是整数。试图存储超出范围以外的数值将会导致错误。
> - 常用的类型是INTEGER，因为它提供了在范围、存储空间、性能之间的最佳平衡。一般只有取值范围确定不超过SMALLINT的情况下，才会使用SMALLINT类型。而只有在INTEGER的范围不够的时候才使用BIGINT，因为前者相对快得多。

**表 2** 任意精度型

| 名称               | 描述                  | 存储空间              | 范围  |
| :------------------------------------- | :-------------------------| :------------------------|--------------------------|
| NUMERIC[(p[,s])],<br/>DECIMAL[(p[,s])] | 精度p取值范围为[1,1000]，标度s取值范围为[0,p]。<br/>说明：<br />p为总位数，s为小数位数。 | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 | 未指定精度的情况下，小数点前最大131,072位，小数点后最大16,383位。 |
| NUMBER[(p[,s])]                        | NUMERIC类型的别名。                       | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 | 未指定精度的情况下，小数点前最大131,072位，小数点后最大16,383位。 |

示例:

```sql
--创建表。
mogdb=# CREATE TABLE decimal_type_t1
(
    DT_COL1 DECIMAL(10,4)
);

--插入数据。
mogdb=# INSERT INTO decimal_type_t1 VALUES(123456.122331);

--查询表中的数据。
mogdb=# SELECT * FROM decimal_type_t1;
   dt_col1
-------------
 123456.1223
(1 row)

--删除表。
mogdb=# DROP TABLE decimal_type_t1;
```

```sql
--创建表。
mogdb=# CREATE TABLE numeric_type_t1
(
    NT_COL1 NUMERIC(10,4)
);

--插入数据。
mogdb=# INSERT INTO numeric_type_t1 VALUES(123456.12354);

--查询表中的数据。
mogdb=# SELECT * FROM numeric_type_t1;
   nt_col1
-------------
 123456.1235
(1 row)

--删除表。
mogdb=# DROP TABLE numeric_type_t1;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 与整数类型相比，任意精度类型需要更大的存储空间，其存储效率、运算效率以及压缩比效果都要差一些。在进行数值类型定义时，优先选择整数类型。当且仅当数值超出整数可表示最大范围时，再选用任意精度类型。
> - 使用Numeric/Decimal进行列定义时，建议指定该列的精度p以及标度s。

**表 3** 序列整型

| 名称        | 描述  | 存储空间 | 范围                      |
| :------- | :----- | :----- | :--------------------------- |
| SMALLSERIAL | 二字节序列整型。   | 2字节    | -32,768 ~ +32,767                                            |
| SERIAL      | 四字节序列整型。   | 4字节    | -2,147,483,648 ~ +2,147,483,647                              |
| BIGSERIAL   | 八字节序列整型。   | 8字节    | -9,223,372,036,854,775,808 ~ +9,223,372,036,854,775,807      |
| LARGESERIAL | 十六字节序列整形。 | 16字节   | -170,141,183,460,469,231,731,687,303,715,884,105,728 ~ +170,141,183,460,469,231,731,687,303,715,884,105,727 |

示例:

```sql
--创建表。
mogdb=# CREATE TABLE smallserial_type_tab(a SMALLSERIAL);

--插入数据。
mogdb=# INSERT INTO smallserial_type_tab VALUES(default);

--再次插入数据。
mogdb=# INSERT INTO smallserial_type_tab VALUES(default);

--查看数据。
mogdb=# SELECT * FROM smallserial_type_tab;
 a
---
 1
 2
(2 rows)

--创建表。
mogdb=# CREATE TABLE serial_type_tab(b SERIAL);

--插入数据。
mogdb=# INSERT INTO serial_type_tab VALUES(default);

--再次插入数据。
mogdb=# INSERT INTO serial_type_tab VALUES(default);

--查看数据。
mogdb=# SELECT * FROM serial_type_tab;
 b
---
 1
 2
(2 rows)

--创建表。
mogdb=# CREATE TABLE bigserial_type_tab(c BIGSERIAL);

--插入数据。
mogdb=# INSERT INTO bigserial_type_tab VALUES(default);

--插入数据。
mogdb=# INSERT INTO bigserial_type_tab VALUES(default);

--查看数据。
mogdb=# SELECT * FROM bigserial_type_tab;
 c
---
 1
 2
(2 rows)

--删除表。
mogdb=# DROP TABLE smallserial_type_tab;

mogdb=# DROP TABLE serial_type_tab;

mogdb=# DROP TABLE bigserial_type_tab;
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> SMALLSERIAL，SERIAL和BIGSERIAL类型不是真正的类型，只是为在表中设置唯一标识做的概念上的便利。因此，创建一个整数字段，并且把它的缺省数值安排为从一个序列发生器读取。应用了一个NOT NULL约束以确保NULL不会被插入。在大多数情况下用户可能还希望附加一个UNIQUE或PRIMARY KEY约束避免意外地插入重复的数值，但这个不是自动的。最后，将序列发生器从属于那个字段，这样当该字段或表被删除的时候也一并删除它。目前只支持在创建表时候指定SERIAL列，不可以在已有的表中，增加SERIAL列。另外临时表也不支持创建SERIAL列。因为SERIAL不是真正的类型，也不可以将表中存在的列类型转化为SERIAL。

**表 4** 浮点类型

| 名称                    | 描述                 | 存储空间                  | 范围                         |
| :---------------------- | :-----------------| :----------------------- | :---------------------- |
| REAL,FLOAT4             | 单精度浮点数，不精准。                                       | 4字节                                                        | -3.402E+38~3.402E+38，6位十进制数字精度。                    |
| DOUBLE PRECISION,FLOAT8 | 双精度浮点数，不精准。                                       | 8字节                                                        | -1.79E+308~1.79E+308，15位十进制数字精度。                   |
| FLOAT[(p)]              | 浮点数，不精准。精度p取值范围为[1,53]。<br />说明：<br />p为精度，表示总位数。 | 4字节或8字节                                                 | 根据精度p不同选择REAL或DOUBLE PRECISION作为内部表示。如不指定精度，内部用DOUBLE PRECISION表示。 |
| BINARY_DOUBLE           | 是DOUBLE PRECISION的别名。                                   | 8字节                                                        | -1.79E+308~1.79E+308，15位十进制数字精度。                   |
| DEC[(p[,s])]            | 精度p取值范围为[1,1000]，标度s取值范围为[0,p]。<br />说明：<br />p为总位数，s为小数位位数。 | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 | 未指定精度的情况下，小数点前最大131,072位，小数点后最大16,383位。 |
| INTEGER[(p[,s])]        | 精度p取值范围为[1,1000]，标度s取值范围为[0,p]。              | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 | -                                                            |

示例:

```sql
--创建表。
mogdb=# CREATE TABLE float_type_t2
(
    FT_COL1 INTEGER,
    FT_COL2 FLOAT4,
    FT_COL3 FLOAT8,
    FT_COL4 FLOAT(3),
    FT_COL5 BINARY_DOUBLE,
    FT_COL6 DECIMAL(10,4),
    FT_COL7 INTEGER(6,3)
);

--插入数据。
mogdb=# INSERT INTO float_type_t2 VALUES(10,10.365456,123456.1234,10.3214, 321.321, 123.123654, 123.123654);

--查看数据。
mogdb=# SELECT * FROM float_type_t2 ;
 ft_col1 | ft_col2 |   ft_col3   | ft_col4 | ft_col5 | ft_col6  | ft_col7
---------+---------+-------------+---------+---------+----------+---------
      10 | 10.3655 | 123456.1234 | 10.3214 | 321.321 | 123.1237 | 123.124
(1 row)

--删除表。
mogdb=# DROP TABLE float_type_t2;
```
