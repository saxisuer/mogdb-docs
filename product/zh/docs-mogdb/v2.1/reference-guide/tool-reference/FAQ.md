---
title: FAQ
summary: FAQ
author: Zhang Cuiping
date: 2021-11-17
---

# FAQ

## 执行命令报错“Failed to obtain the GPHOME”

### 问题现象

如果在root用户下执行工具命令时，出现如下信息。

```bash
Failed to obtain the GPHOME.
```

### 原因分析

可能是环境变量中GPHOME变量没有配置正确。需要检查环境变量GPHOME是否包含MogDBXML中gaussdbToolPath路径。

### 操作步骤

使用如下命令检查$GPHOME路径。

```bash
echo $GPHOME
```

如果不是安装时默认的路径，请在配置文件中修改GPHOME的路径。

```bash
vim /etc/profile
```

## gs_ctl 重建备实例过程被中断导致秘钥文件不完整恢复方法

### 问题现象

用户在重建备实例的过程中被中断，再次重建备实例失败，出现以下报错信息。

```bash
CRC checksum does not match value stored in file, maybe the cipher file is corrupt
non obs cipher file or random parameter file is invalid.
read cipher file or random parameter file failed.
2020-06-18 20:58:12.080 5eeb64e3.1 [unknown] 140697304617088 [unknown] 0 dn_6001_6002 F0000 0 [BACKEND] FATAL:  could not load server certificate file "server.crt": no start line
[2020-06-18 20:58:12.086][24066][dn_6001_6002][gs_ctl]:  waitpid 24446 failed, exitstatus is 256, ret is 2
```

### 原因分析

重建中断时证书文件不完整，再次重建会由于证书文件不完整而失败。

### 操作步骤

1. 在数据目录下查看证书文件大小。

   ```bash
   ll
   查看秘钥文件大小
   -rw------- 1 omm omm       0 Jun 18 20:58 server.crt
   -rw------- 1 omm omm       0 Jun 18 20:58 server.key
   -rw------- 1 omm omm       0 Jun 18 20:58 server.key.cipher
   -rw------- 1 omm omm       0 Jun 18 20:58 server.key.rand
   ```

2. 若证书文件大小为0，删除证书文件。

   ```bash
   rm -rf server.crt server.key server.key.cipher server.key.rand
   ```

3. 重建备实例。

   ```bash
   gs_ctl build -D data_dir
   ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  若备机数据库已停止，需要重新生成证书文件或者复制证书文件（$GAUSSHOME/share下的证书文件）到数据目录，启动备机并重建备实例。生成证书文件的相关操作请参见《开发者指南》。

## 使用gs_om -t status --all查询集群状态，长时间没有响应

### 问题现象

使用gs_om -t status --all命令后长时间无响应。

### 原因分析

可能是gaussdb进程hang住，查询动作会调用gsql或者gs_ctl工具查询数据库状态，进程hang住后不会给响应，直到超时后退出。

### 操作步骤

1. 查看gsql能否访问数据库，出现下面提示说明gaussdb进程hang住，数据库异常。

   ```bash
   gsql -d postgres -p 29776
   gsql: wait (null):29776 timeout expired, errno: Success
   ```

2. 查看postgresql-*.log是否有错误提示，根据提示解决问题。

   ```bash
   cd $GAUSSLOG/pg_log/dn_6001;grep "ERROR\|FATAL" postgresql-*.log
   ```

3. 数据库已经hang住，gs_om命令不起作用，可以直接到每个节点上查找进程pid后kill。

   ```bash
   ps -ef|grep $GAUSSHOME/bin/gaussdb|grep -v grep
   kill -9 $pid
   ```

4. 所有节点进程kill完毕后在某一节点执行启动命令。测试环境下可以直接重启数据库，生产商用环境请联系技术支持工程师。

   ```bash
   gs_om -t start
   ```

## gs_sshexkey 相同用户不同密码报错

### 问题现象

openEuler环境下，gs_sshexkey支持相同用户不同密码做互信，但是输入了正确的密码还是报鉴权失败。

### 原因分析

打开系统日志/var/log/secure,查看到有**pam_faillock(sshd:auth): Consecutive login failures for user**日志，说明当前用户密码试错次数太多被暂时锁定。

### 操作步骤

在/etc/pam.d目录下修改相关配置文件system-auth、password-auth、password-auth-crond，将文件中deny=3的地方适当放大，待互信全部建立完成后再恢复。
