---
title: gs_ssh
summary: gs_ssh
author: Zhang Cuiping
date: 2021-06-07
---

# gs_ssh

## 背景信息

MogDB提供了gs_ssh工具帮助用户在MogDB各节点上执行相同的命令。

## 注意事项

- gs_ssh只可以执行当前数据库用户有权限执行的命令。
- gs_ssh所执行的命令不会对当前执行的会话产生影响，比如类似cd或source的命令，只会在执行的进程环境中产生影响，而不会影响到当前执行的会话环境。

## 前提条件

- 各个主机间互信正常。
- MogDB已经正确安装部署。
- 调用命令可用which查询到且在当前用户下有执行权限。
- 需以操作系统用户omm执行gs_ssh命令。

## 语法

- 同步执行命令

  ```bash
  gs_ssh -c cmd
  ```

- 显示帮助信息

  ```bash
  gs_ssh -? | --help
  ```

- 显示版本号信息

  ```bash
  gs_ssh -V | --version
  ```

## 参数说明

- -c

  指定需要在MogDB各主机上执行的linux shell命令名。

- -?, -help

  显示帮助信息。

- -V, -version

  显示版本号信息。

## 示例

在MogDB各主机上执行相同命令。以执行”hostname”命令为例。

```bash
gs_ssh -c "hostname"
Successfully execute command on all nodes.
Output:
[SUCCESS] plat1:
plat1
[SUCCESS] plat2:
plat2
[SUCCESS] plat3:
plat3
[SUCCESS] plat4:
plat4
```
