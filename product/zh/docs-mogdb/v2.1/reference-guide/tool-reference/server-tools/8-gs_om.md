---
title: gs_om
summary: gs_om
author: Zhang Cuiping
date: 2021-06-07
---

# gs_om

## 背景信息

MogDB提供了gs_om工具帮助对MogDB进行维护，包括启动MogDB、停止MogDB、查询MogDB状态、查询静态配置、生成静态配置文件、查询MogDB状态详细信息、生成动态配置文件、SSL证书替换、显示帮助信息和显示版本号信息等功能。

## 前提条件

需以操作系统用户omm执行gs_om命令。

## 语法

- 启动MogDB

  ```bash
  gs_om -t start [-h HOSTNAME] [-D dataDir] [--time-out=SECS] [--security-mode=MODE] [-l LOGFILE]
  ```

- 停止MogDB

  ```bash
  gs_om -t stop [-h HOSTNAME] [-D dataDir]  [--time-out=SECS] [-m MODE] [-l LOGFILE]
  ```

- 重启MogDB

  ```bash
  gs_om -t restart [-h HOSTNAME] [-D dataDir] [--time-out=SECS] [--security-mode=MODE] [-l LOGFILE] [-m MODE]
  ```

- 查询MogDB状态

  ```bash
  gs_om -t status [-h HOSTNAME] [-o OUTPUT] [--detail] [--all] [-l LOGFILE]
  ```

- 生成静态配置文件

  ```bash
  gs_om -t generateconf -X XMLFILE [--distribute] [-l LOGFILE]
  ```

- 生成动态配置文件，备机failover或switchover成主机后，需要执行此操作

  ```bash
  gs_om -t refreshconf
  ```

- 查看静态配置

  ```bash
  gs_om -t view [-o OUTPUT]
  ```

- 查询MogDB状态详细信息

  ```bash
  gs_om -t query [-o OUTPUT]
  ```

- SSL证书替换

  ```bash
  gs_om -t cert --cert-file=CERTFILE [-l LOGFILE]
  gs_om -t cert --rollback
  ```

- 开启、关闭数据库内kerberos认证

  ```bash
  gs_om -t kerberos -m [install|uninstall] -U USER [-l LOGFILE] [--krb-client|--krb-server]
  ```

- 显示帮助信息

  ```bash
  gs_om -? | --help
  ```

- 显示版本号信息

  ```bash
  gs_om -V | --version
  ```

## 参数说明

gs_om参数可以分为如下几类：

- 通用参数：

  - -t

    gs_om命令的类型。

    取值范围: start、stop、status、generateconf、cert、view、query、refreshconf、kerberos。

  - -l

    指定日志文件及存放路径。

    默认值：$GAUSSLOG/om/gs_om-YYYY-MM-DD_hhmmss.log（virtualip的默认值：/tmp/gs_virtualip/gs_om-YYYY-MM-DD_hhmmss.log）

  - -?, -help

    显示帮助信息。

  - -V, -version

    显示版本号信息。

- 启动MogDB参数：

  - -h

    指定需要启动的服务器名称。一次只能启动一个服务器。

    取值范围: 服务器名称。

    不指定服务器名称时，表示启动MogDB。

  - -D

    指定 dn路径

    取值范围: dn路径

    不指定dn路径，表示使用静态文件中的dn路径

  - -time-out=SECS

    指定超时时间，如果超时，om脚本自动退出。单位：s。

    取值范围: 正整数，建议值300。

    默认值：300

  - -security-mode

    指定是否以安全模式启动数据库。

    取值范围:

    - on以安全模式启动。
    - off不以安全模式启动，默认不开启安全模式。

- 停止MogDB参数：

  - -h

    指定需要停止实例所在的服务器名称。一次只能停止一个服务器。

    取值范围: 实例所在的服务器名称。

    不指定服务器名称时，表示停止MogDB。

  - -m, -mode=MODE

    停止模式设置。

    取值范围: 支持两种停止模式。

    - fast方式：保证有主备关系的实例数据是一致的。
    - immediate方式：不保证有主备关系的实例数据是一致的。

    默认值：fast方式。

  - -time-out=SECS

    指定超时时间，如果超时，om脚本自动退出。单位：s。

    取值范围: 正整数，建议值300。

    默认值：300

- 查询状态参数：

    MogDB状态显示结果的参数说明请参见[表1](#zhuangtaishuom)。

  - -h

    指定需要待查询的服务器名称。

    取值范围: 服务器名称。

    不指定服务器时，表示查询MogDB。

  - -az

    指定需要查询的AZ名称，比如-az=AZ1。

    取值范围: AZ名称，不指定AZ名称时，表示查询所有AZ。

  - -o

    输出到指定的output文件中。

    如果不指定，直接显示在屏幕上。

  - -detail

    显示详细信息。如果不指定，只提示该服务器是否正常。

    ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  本参数通过在每个数据库节点执行gs_ctl query命令进行查询并汇总结果，来获取MogDB的详细信息。

  - -all

    显示MogDB所有节点信息。

    ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  本参数通过在每个数据库节点执行gsql查询系统表并汇总结果，来获取MogDB所有节点的信息。为保证显示信息的正确性，在使用本参数前请确认gsql可以查询。

- 生成配置文件参数：

  - -X

    选择MogDB配置文件路径。

    取值范围: clusterconfig.xml的路径。

  - -distribute

    将静态配置文件发布到MogDB实例的安装目录。

  - 开启、关闭数据库内kerberos认证：

  - -U

    指定数据库部署用户。

    取值范围: 数据库部署用户

  - -m

    指定所要进行的操作。

    取值范围:

    install：开启数据库内kerberos认证。

    uninstall：关闭数据库内kerberos认证。

  - -krb-server安装kerberos服务端认证。

  - -krb-client安装kerberos客户端认证。

    注：需要先安装-krb-server，卸载时同步卸载不需要该参数

- SSL证书替换：

  - -cert-file

    指定本地SSL证书zip压缩包所在路径。

  - -rollback

    证书回退到上一次成功备份的状态。

  - -L

    制定该参数时，仅对当前节点进行。

**表 1** 状态说明 <a id="zhuangtaishuom"> </a>

| 字段           | 字段含义                                                     | 字段值                                                       |
| :------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| cluster_state  | MogDB状态。显示MogDB是否运行正常。                           | - Normal：表示MogDB可用，且数据有冗余备份。所有进程都在运行，主备关系正常。<br />- Unavailable：表示MogDB不可用。<br />- Degraded：表示MogDB可用，但数据没有冗余备份。 |
| redistributing | 数据重分布状态                                               | - Yes：表示MogDB处于数据重分布状态。<br />- No：表示MogDB未处于数据重分步状态。 |
| balanced       | 平衡状态。显示是否有MogDB实例发生过主备切换而导致主机负载不均衡。 | - Yes：表示MogDB处于负载均衡状态。<br />- No：表示MogDB未处于负载均衡状态。 |
| node           | 主机名称                                                     | 表示该实例所在的主机名称。多AZ时会显示AZ编号。               |
| node_ip        | 主机IP                                                       | 表示该实例所在的主机IP。                                     |
| instance       | 实例ID                                                       | 表示该实例的ID。                                             |
| state          | 实例状态                                                     | - P: 节点的初始角色是Primary，数据库安装后就不再变动，从系统静态文件读取。<br />- S: 节点的初始角色是Standby，数据库安装后就不再变动，从系统静态文件读取。<br />- C: 节点的初始角色是Cascade Standby，数据库安装后就不再变动，从系统静态文件读取。<br />- Primary：表示实例为主实例。<br />- Standby：表示实例为备实例。<br />- Cascade Standby：表示实例为级联备实例。<br />- Secondary：表示实例为从备实例。<br />- Pending：表示该实例在仲裁阶段。<br />- Unknown：表示实例状态未知。<br />- Down：表示实例处于宕机状态。 |

**表 2** 特性ID说明

| 特性名称                   | 特性ID | MogDB产品         |
| :------------------------- | :----- | :---------------- |
| 多值列                     | 0      | 基础版            |
| JSON                       | 1      | 不支持License控制 |
| XML                        | 2      | 不支持            |
| 数据存储格式ORC            | 3      | 基础版            |
| 单机支持一主多备，备机可读 | 5      | 策略不放开        |
| 多维碰撞分析GPU加速        | 7      | 高级特性          |
| 全文索引                   | 8      | 基础版            |
| EXTENSION_CONNECTOR        | 9      | 基础版            |
| EXPRESS_CLUSTER            | 12     | 高级版            |
| 跨DC协同                   | 13     | 高级版            |
| 图                         | 14     | 不支持            |
| 时序                       | 15     | 不支持            |
| PostGis对接                | 16     | 基础版            |
| MogDB内高可用-一主多备     | 17     | 策略不放开        |
| 行级权限控制               | 18     | 高级版            |
| 透明加密                   | 19     | 高级版            |
| 私有表                     | 20     | 高级版            |

## 示例

- 启动MogDB。

  ```bash
  gs_om -t start
  Starting cluster.
  ======================================================================
  .
  ======================================================================

  Successfully started .
  ```

- 停止MogDB。

  ```bash
  gs_om -t stop
  Stopping cluster.
  =========================================
  Successfully stopped cluster.
  =========================================
  End stop cluster.
  ```

- 查看MogDB详细状态信息，含实例状态信息。

  ```bash
  gs_om -t status --detail

  [   Cluster State   ]

  cluster_state   : Normal
  redistributing  : No
  current_az      : AZ_ALL

  [  Datanode State   ]

      node             node_ip         port      instance                                   state
  -----------------------------------------------------------------------------------------------------
  1  pekpopgsci00235  10.244.62.204    5432      6001 /opt/gaussdb/cluster/data/dn1   P Primary Normal
  2  pekpopgsci00238  10.244.61.81     5432      6002 /opt/gaussdb/cluster/data/dn1   S Standby Normal
  ```

- 在MogDB上执行如下命令，生成配置文件。

  ```bash
  gs_om -t generateconf -X  /opt/software/mogdb/clusterconfig.xml  --distribute
  Generating static configuration files for all nodes.
  Creating temp directory to store static configuration files.
  Successfully created the temp directory.
  Generating static configuration files.
  Successfully generated static configuration files.
  Static configuration files for all nodes are saved in /opt/mogdb/Bigdata/gaussdb/wisequery/script/static_config_files.
  Distributing static configuration files to all nodes.
  Successfully distributed static configuration files.
  ```

  然后打开生成的配置文件目录，会看到新生成的3个文件。

  ```bash
  cd /opt/mogdb/Bigdata/gaussdb/wisequery/script/static_config_files
  ll
  total 456
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat1
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat2
  -rwxr-xr-x 1 omm dbgrp 155648 2016-07-13 15:51 cluster_static_config_plat3
  ```

- SSL证书回退。

  ```bash
  gs_om -t cert --rollback
  [plat1] SSL cert files rollback successfully.
  [plat2] SSL cert files rollback successfully.
  [plat3] SSL cert files rollback successfully.
  ```

- 新License注册。

  ```bash
  gs_om -t license -m register -f MTgsMTkK
  Preparing for the program initialization.
  Lock the OPS operation of OM components.
  Check and make sure the consistency of the license file.
  Backup the license file on all of the cluster hosts.
  Encrypt the product feature information and generate the license file.
  Send message to the 数据库节点 instances to reload the license file.
  Remove the backup license file on all of the cluster hosts.
  License register successfully.
  ```

- 新License反注册。

  ```bash
  gs_om -t license -m unregister -f MTgsMTkK
  Preparing for the program initialization.
  Lock the OPS operation of OM components.
  Check and make sure the consistency of the license file.
  Backup the license file on all of the cluster hosts.
  Encrypt the product feature information and generate the license file.
  Send message to the 数据库节点 instances to reload the license file.
  Remove the backup license file on all of the cluster hosts.
  License unregister successfully.
  ```
