---
title: gs_install_plugin
summary: gs_install_plugin
author: Guo Huan
date: 2021-11-29
---

# gs_install_plugin

## 背景信息

MogDB提供了 gs_install_plugin 工具用于安装插件，目前支持安装的插件包括 pg_repack、pg_trgm、dblink、wal2json、orafce、pg_bulkload、pg_prewarm。本工具适用于一主多备部署的 MogDB。

&nbsp;

## 前提条件

已完成 MogDB 一主多备部署。

> **发布补丁说明**:
>
> 2.1 版本中本工具存在已知的 bug, 会导致因识别不到插件包而安装失败的问题。
> 请您在使用本工具前，先按照如下操作来进行补丁修复，然后再进行后续插件的安装（适用于 2.1.x）：
>
> - 可访问外网时：
>
> 1. 切换到安装数据库的用户下（例如: omm）: `su - omm`
> 2. 执行在线修复： `cd $GPHOME && curl https://cdn-mogdb.enmotech.com/mogdb-media/2.1.1/om_plugin_install_v2.1.1.patch | patch -p1`
>
> - 无法访问外网时：
>
> 1. 请先在有网络的服务器下载补丁文件 [om_plugin_install_v2.1.1.patch](https://cdn-mogdb.enmotech.com/mogdb-media/2.1.1/om_plugin_install_v2.1.1.patch)，然后将补丁文件拷贝到需要修复的服务器上；
> 2. 切换到安装数据库的用户下（例如: omm）： `su - omm`
> 3. 执行 `cd $GPHOME && patch -p1 < [此处替换Patch文件路径]`

&nbsp;

## 语法

```
gs_install_plugin [-h] [-X XML] [--all] [--dblink] [--pg_trgm] [--wal2json] [--pg_repack] [--orafce] [--pg_bulkload] [--pg_prewarm] [--plugins PLUGINS [PLUGINS ...]]
```

&nbsp;

## 参数说明

- -h

  显示帮助信息

- -X

  必填参数，后跟 clusterconfig.xml 配置文件的路径

- --all

  安装全部插件

- --plugins

  可安装多个插件，如：`--plugins dblink pg_repack wal2json`

- --dblink

  只安装 dblink

- --pg_trgm

  只安装 pg_trgm

- --wal2json

  只安装 wal2json

- --pg_repack

  只安装 pg_repack

- --orafce

  只安装 orafce

- --pg_bulkload

  只安装 pg_bulkload

&nbsp;

## 安装示例

1. 进入数据库安装目录下的 script 文件夹：

   ```bash
   cd /opt/software/mogdb/script/
   ```

2. 创建 static 文件夹：

    ```bash
    mkdir -p static
    ```

    > 注意：文件夹名称请勿更改。

3. 访问[MogDB官网下载页面](https://www.mogdb.io/downloads/mogdb)，根据您的操作系统及 CPU 架构下载对应版本的插件，并将插件放入 static 文件夹下。

4. 权限设置

    ```
    chmod -R 755 /opt/software/mogdb/script
    chown -R omm:dbgrp /opt/software/mogdb/script
    ```

5. 配置 clusterconfig.xml 配置文件，这里以一主一备配置文件作为示例，`backIp1s` 为节点ip，`dataNode1` 为数据目录，该程序默认获取到的数据目录为第一个节点的数据目录位置。

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <ROOT>
        <!-- MogDB整体信息 -->
        <CLUSTER>
            <PARAM name="clusterName" value="dbCluster" />
            <PARAM name="nodeNames" value="node1,node2" />
            <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12"/>
            <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
            <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
            <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
            <PARAM name="corePath" value="/opt/mogdb/corefile"/>
            <PARAM name="clusterType" value="single-inst"/>
        </CLUSTER>
        <!-- 每台服务器上的节点部署信息 -->
        <DEVICELIST>
            <!-- node1上的节点部署信息 -->
            <DEVICE sn="1000001">
                <PARAM name="name" value="node1"/>
                <PARAM name="azName" value="AZ1"/>
                <PARAM name="azPriority" value="1"/>
                <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
                <PARAM name="backIp1" value="192.168.0.11"/>
                <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1"/>
            </DEVICE>

            <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
            <DEVICE sn="1000002">
                <PARAM name="name" value="node2"/>
                <PARAM name="azName" value="AZ1"/>
                <PARAM name="azPriority" value="1"/>
                <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
                <PARAM name="backIp1" value="192.168.0.12"/>
                <PARAM name="sshIp1" value="192.168.0.12"/>
    </DEVICE>
        </DEVICELIST>
    </ROOT>
    ```

6. 切换到 omm 用户

    ```
    su - omm
    ```

7. 安装全部插件

    ```bash
    /opt/software/mogdb/script/gs_install_plugin -X /opt/software/mogdb/clusterconfig.xml --all
    # 安装成功时回显如下：
    SUCCESS: dblink.
    SUCCESS: pg_trgm.
    SUCCESS: pg_repack.
    SUCCESS: wal2json.
    SUCCESS: orafce.
    SUCCESS: pg_bulkload.
    SUCCESS: pg_prewarm.
    ```

## 相关页面

[orafce](../../../reference-guide/oracle-plugins/orafce-user-guide.md)、[dblink](../../../reference-guide/oracle-plugins/dblink-user-guide.md)、[pg_repack](../../../reference-guide/oracle-plugins/pg_repack-user-guide.md)、[pg_trgm](../../../reference-guide/oracle-plugins/pg_trgm-user-guide.md)、[wal2json](../../../reference-guide/oracle-plugins/wal2json-user-guide.md)、[pg_bulkload](../../../reference-guide/oracle-plugins/pg_bulkload-user-guide.md)、[pg_prewarm](../../../reference-guide/oracle-plugins/pg_prewarm-user-guide.md)。
