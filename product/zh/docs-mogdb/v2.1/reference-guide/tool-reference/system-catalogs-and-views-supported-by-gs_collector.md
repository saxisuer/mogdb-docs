---
title: gs_collector工具支持收集的系统表和视图列表
summary: gs_collector工具支持收集的系统表和视图列表
author: Zhang Cuiping
date: 2021-11-17
---

# gs_collector工具支持收集的系统表和视图列表

- OS
  - gs_os_run_info
  - pg_os_threads
- Session/Thread
  - gs_session_stat
  - gs_session_time
  - gs_session_cpu_statistics
  - gs_session_memory_statistics
  - gs_session_memory_context
  - gs_session_memory
  - gs_session_memory_detail
  - pg_stat_activity
  - gs_thread_memory_context
- WLM
  - gs_cluster_resource_info
  - pg_total_user_resource_info
  - pg_total_user_resource_info_oid
  - gs_wlm_rebuild_user_resource_pool
  - gs_wlm_resource_pool
  - gs_wlm_workload_records
  - pg_wlm_statistics
  - pg_app_workloadgroup_mapping
- Query-AP Operator
  - gs_wlm_operator_info
  - gs_wlm_operator_history
  - gs_wlm_operator_statistics
- Query-AP
  - gs_wlm_session_info_all
  - gs_wlm_session_history
  - gs_wlm_session_info
  - gs_wlm_user_session_info
  - gs_wlm_session_statistics
  - pg_session_iostat
  - pg_session_wlmstat
- Cache I/O
  - pg_statio_all_indexes
  - pg_statio_all_sequences
  - pg_statio_all_tables
  - pg_statio_sys_indexes
  - pg_statio_sys_sequences
  - pg_statio_sys_tables
  - pg_statio_user_indexes
  - pg_statio_user_sequences
  - pg_statio_user_tables
  - gs_stat_db_cu
  - gs_stat_session_cu
- Memory
  - pg_total_memory_detail
  - pg_shared_memory_detail
  - gs_total_memory_detail
- File
  - gs_file_stat
  - gs_redo_stat
  - pg_stat_bad_block
- Object
  - pg_stat_user_tables
  - pg_stat_user_indexes
  - pg_stat_sys_tables
  - pg_stat_sys_indexes
  - pg_stat_all_tables
  - pg_stat_all_indexes
  - pg_stat_database
  - pg_stat_database_conflicts
  - pg_stat_xact_all_tables
  - pg_stat_xact_sys_tables
  - pg_stat_xact_user_tables
  - pg_stat_xact_user_functions
- Lock
  - pg_locks
- Utility
  - pg_stat_replication
  - pg_replication_slots
  - pg_stat_bgwriter
- Transaction
  - pg_running_xacts
  - pg_prepared_xacts
- Waits
  - pg_thread_wait_status

<!--
**表 1** gs_collector工具支持收集的系统表和视图列表

<table>
 <tr>
  <td>Class</td>
  <td>View Name</td>
 </tr>
 <tr>
  <td rowspan="2">OS</td>
  <td>gs_os_run_info</td>
 </tr>
 <tr>
  <td>pg_os_threads</td>
 </tr>
 <tr>
  <td rowspan="9">
   Session/Thread
  </td>
  <td>
   gs_session_stat
  </td>
 </tr>
 <tr>
  <td>
   gs_session_time
  </td>
 </tr>
 <tr>
  <td>
   gs_session_cpu_statistics
  </td>
 </tr>
 <tr>
  <td>
   gs_session_memory_statistics
  </td>
 </tr>
 <tr>
  <td>
   gs_session_memory_context
  </td>
 </tr>
 <tr>
  <td>
   gs_session_memory
  </td>
 </tr>
 <tr>
  <td>
   gs_session_memory_detail
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_activity
  </td>
 </tr>
 <tr>
  <td>
   gs_thread_memory_context
  </td>
 </tr>
 <tr>
  <td rowspan="8">
   WLM
  </td>
  <td>
   gs_cluster_resource_info
  </td>
 </tr>
 <tr>
  <td>
   pg_total_user_resource_info
  </td>
 </tr>
 <tr>
  <td>
   pg_total_user_resource_info_oid
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_rebuild_user_resource_pool
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_resource_pool
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_workload_records
  </td>
 </tr>
 <tr>
  <td>
   pg_wlm_statistics
  </td>
 </tr>
 <tr>
  <td>
   pg_app_workloadgroup_mapping
  </td>
 </tr>
 <tr>
  <td rowspan="3">
   Query-AP Operator
  </td>
  <td>
   gs_wlm_operator_info
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_operator_history
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_operator_statistics
  </td>
 </tr>
 <tr>
  <td rowspan="7">
   Query-AP
  </td>
  <td>
   gs_wlm_session_info_all
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_session_history
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_session_info
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_user_session_info
  </td>
 </tr>
 <tr>
  <td>
   gs_wlm_session_statistics
  </td>
 </tr>
 <tr>
  <td>
   pg_session_iostat
  </td>
 </tr>
 <tr>
  <td>
   pg_session_wlmstat
  </td>
 </tr>
 <tr>
  <td rowspan="11">
   Cache I/O
  </td>
  <td>
   pg_statio_all_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_all_sequences
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_all_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_sys_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_sys_sequences
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_sys_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_user_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_user_sequences
  </td>
 </tr>
 <tr>
  <td>
   pg_statio_user_tables
  </td>
 </tr>
 <tr>
  <td>
   gs_stat_db_cu
  </td>
 </tr>
 <tr>
  <td>
   gs_stat_session_cu
  </td>
 </tr>
 <tr>
  <td rowspan="3">
   Memory
  </td>
  <td>
   pg_total_memory_detail
  </td>
 </tr>
 <tr>
  <td>
   pg_shared_memory_detail
  </td>
 </tr>
 <tr>
  <td>
   gs_total_memory_detail
  </td>
 </tr>
 <tr>
  <td rowspan="3">
   File
  </td>
  <td>
   gs_file_stat
  </td>
 </tr>
 <tr>
  <td>
   gs_redo_stat
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_bad_block
  </td>
 </tr>
 <tr>
  <td rowspan="12">
   Object
  </td>
  <td>
   pg_stat_user_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_user_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_sys_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_sys_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_all_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_all_indexes
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_database
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_database_conflicts
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_xact_all_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_xact_sys_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_xact_user_tables
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_xact_user_functions
  </td>
 </tr>
 <tr>
  <td>
   Lock
  </td>
  <td>
   pg_locks
  </td>
 </tr>
 <tr>
  <td rowspan="3">
   Utility
  </td>
  <td>
   pg_stat_replication
  </td>
 </tr>
 <tr>
  <td>
   pg_replication_slots
  </td>
 </tr>
 <tr>
  <td>
   pg_stat_bgwriter
  </td>
 </tr>
 <tr>
  <td rowspan="2">
   Transaction
  </td>
  <td>
   pg_running_xacts
  </td>
 </tr>
 <tr>
  <td>
   pg_prepared_xacts
  </td>
 </tr>
 <tr>
  <td>
   Waits
  </td>
  <td>
   pg_thread_wait_status
  </td>
 </tr>
</table> -->
