---
title: pg_repack 安装流程
summary: pg_repack 安装流程
author: Guo Huan
date: 2021-11-29
---

# pg_repack

## pg_repack简介

插件pg_repack用于在线清理表空间，有效解决因对全表大量更新等操作引起的表膨胀问题。pg_repack无需获取排它锁，相比CLUSTER或VACUUM FULL更加轻量化。

<br/>

## pg_repack安装

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

<br/>

## pg_repack使用

1. 连接数据库并且创建用户。

   ```bash
   gsql postgres -r -p 5001
   ```

   ```sql
   create user test password 'Test123456' login sysadmin;
   ```

2. 在sql端执行：

   ```sql
   create extension pg_repack;
   ```

3. 在数据库中创建pg_repack表，必须包含主键。

   ```sql
   create table repack_test(id int primary key, name name);
   ```

4. 检查pg_repack是否创建成功。

   ```sql
   \dx
   ```

5. 在repack_test表中插入2000000条数据。

   ```sql
   insert into repack_test select generate_series(1,2000000),'a';
   ```

6. 查询表repack_test的大小。

   ```sql
   select pg_size_pretty(pg_relation_size('repack_test'));
   ```

7. 删除部分数据，然后查询表大小。

   ```sql
   Delete from repack_test where id>1000000;
   ```

   删除1000000条数据成功之后，可以看表大小并没有改变。这时需要使用pg_repack工具。

8. 使用pg_repack工具。

   ```bash
   pg_repack -d postgres -t test.repack_test -h 127.0.0.1 -U test -p 5001
   ```

   参数含义：

   - -d dbname（数据库名）
   - -t test.repack_test -h hostip（服务端IP地址）
   - -U     user（用户名）
   - -p port（端口号）
   - -e （输出repack过程中执行的所有SQL）
   - -E DEBUG（设置，输出信息的日志级别为DEBUG）

   sql查看repack_test表大小，表空间释放成功。

   ```sql
   select pg_size_pretty(pg_relation_size('repack_test'));
   ```
