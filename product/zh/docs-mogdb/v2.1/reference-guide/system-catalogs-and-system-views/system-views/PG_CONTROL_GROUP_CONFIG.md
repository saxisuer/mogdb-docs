---
title: PG_CONTROL_GROUP_CONFIG
summary: PG_CONTROL_GROUP_CONFIG
author: Guo Huan
date: 2021-06-07
---

# PG_CONTROL_GROUP_CONFIG

PG_CONTROL_GROUP_CONFIG视图存储系统的控制组配置信息。查询该视图需要sysadmin权限。

**表 1** PG_CONTROL_GROUP_CONFIG字段

| 名称                    | 类型 | 描述               |
| :---------------------- | :--- | :----------------- |
| pg_control_group_config | text | 控制组的配置信息。 |
