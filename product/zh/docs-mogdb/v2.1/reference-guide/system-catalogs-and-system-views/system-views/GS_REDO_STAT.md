---
title: GS_REDO_STAT
summary: GS_REDO_STAT
author: Guo Huan
date: 2021-04-19
---

# GS_REDO_STAT

GS_REDO_STAT视图用于统计回话线程日志回放情况。

**表 1** GS_REDO_STAT字段

| 名称      | 类型   | 描述                                     |
| :-------- | :----- | :--------------------------------------- |
| phywrts   | bigint | 日志回放过程中写数据的次数。             |
| phyblkwrt | bigint | 日志回放过程中写数据的块数。             |
| writetim  | bigint | 日志回放过程中写数据所耗的总时间。       |
| avgiotim  | bigint | 日志回放过程中写一次数据的平均消耗时间。 |
| lstiotim  | bigint | 日志回放过程中最后一次写数据消耗的时间。 |
| miniotim  | bigint | 日志回放过程中单次写数据消耗的最短时间。 |
| maxiowtm  | bigint | 日志回放过程中单次写数据消耗的最长时间。 |
