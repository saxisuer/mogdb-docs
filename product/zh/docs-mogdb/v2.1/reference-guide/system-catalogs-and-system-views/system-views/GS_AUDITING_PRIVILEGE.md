---
title: GS_AUDITING_PRIVILEGE
summary: GS_AUDITING_PRIVILEGE
author: Guo Huan
date: 2021-06-07
---

# GS_AUDITING_PRIVILEGE

GS_AUDITING_PRIVILEGE视图显示对数据库DDL相关操作的所有审计信息。需要有系统管理员或安全策略管理员权限才可以访问此视图。

| 名称        | 类型    | 描述                                                        |
| :---------- | :------ | :---------------------------------------------------------- |
| polname     | name    | 策略名称，需要唯一，不可重复。                              |
| pol_type    | text    | 审计策略类型，值为‘privilege’，表示审计DDL 操作。           |
| polenabled  | boolean | 用来表示策略启动开关。                                      |
| access_type | name    | DDL数据库操作相关类型。例如CREATE、ALTER、DROP等。          |
| label_name  | name    | 资源标签名称。对应系统表gs_auditing_policy中的polname字段。 |
| priv_object | text    | 带有数据库对象的全称域名。                                  |
| filter_name | text    | 过滤条件的逻辑字符串。                                      |
