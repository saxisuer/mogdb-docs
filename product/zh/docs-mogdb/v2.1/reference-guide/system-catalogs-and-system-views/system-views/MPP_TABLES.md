---
title: MPP_TABLES
summary: MPP_TABLES
author: Guo Huan
date: 2021-04-19
---

# MPP_TABLES

MPP_TABLES视图显示信息如下。

**表 1** MPP_TABLES字段

| 名称       | 类型             | 描述                  |
| :--------- | :--------------- | :-------------------- |
| schemaname | name             | 包含表的模式名。      |
| tablename  | name             | 表名。                |
| tableowner | name             | 表的所有者。          |
| tablespace | name             | 表所在的表空间。      |
| pgroup     | name             | 节点群的名称。        |
| nodeoids   | oidvector_extend | 表分布的节点OID列表。 |
