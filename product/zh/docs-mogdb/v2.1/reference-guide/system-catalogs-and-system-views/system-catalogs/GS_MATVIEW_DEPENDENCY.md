---
title: GS_MATVIEW_DEPENDENCY
summary: GS_MATVIEW_DEPENDENCY
author: Guo Huan
date: 2021-06-07
---

# GS_MATVIEW_DEPENDENCY

GS_MATVIEW_DEPENDENCY系统表提供了关于数据库中每一个物化视图的基表和mlog表的关联信息。

**表 1** GS_MATVIEW_DEPENDENCY字段

| 名称      | 类型 | 描述                                                         |
| :-------- | :--- | :----------------------------------------------------------- |
| oid       | oid  | 行标识符（隐藏属性，必须明确选择）。                         |
| matviewid | oid  | 物化视图的oid。                                              |
| relid     | oid  | 物化视图基表的oid。                                          |
| mlogid    | oid  | 物化视图mlog表的oid，mlog表为物化视图日志表，与基表一一对应。 |
| mxmin     | int4 | 保留字段。                                                   |
