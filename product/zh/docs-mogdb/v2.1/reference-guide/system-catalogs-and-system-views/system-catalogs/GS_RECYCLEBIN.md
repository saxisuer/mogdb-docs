---
title: GS_RECYCLEBIN
summary: GS_RECYCLEBIN
author: Zhang Cuiping
date: 2021-10-11
---

# GS_RECYCLEBIN

gs_recyclebin描述了回收站对象的详细信息。

**表 1** gs_recyclebin字段

| 名称           | 类型        | 描述                                                         |
| :------------- | :---------- | :----------------------------------------------------------- |
| oid            | oid         | 系统列。                                                     |
| rcybaseid      | oid         | 基表对象id，引用gs_recyclebin.oid。                          |
| rcydbid        | oid         | 当前对象所属数据库oid。                                      |
| rcyrelid       | oid         | 当前对象oid。                                                |
| rcyname        | name        | 回收站对象名称，格式`BIN$unique_id$oid$0`，其中unique_id为最多16字符唯一标识，oid为对象标识符。 |
| rcyoriginname  | name        | 原始对象名称。                                               |
| rcyoperation   | “char”      | 操作类型。<br />- d表示drop<br />- t表示truncate             |
| rcytype        | int         | 对象类型。<br />- 0表示table。<br />- 1表示index。<br />- 2表示toast table。<br />- 3表示toast index。<br />- 4表示sequence，指serial、bigserial类型自动关联的序列对象。 |
| rcyrecyclecsn  | bigint      | 对象drop、truncate时csn。                                    |
| rcyrecycletime | timestamptz | 对象drop、truncate时间。                                     |
| rcycreatecsn   | bigint      | 对象创建时csn。                                              |
| rcychangecsn   | bigint      | 对象定义改变的csn。                                          |
| rcynamespace   | oid         | 包含这个关系的名字空间的OID。                                |
| rcyowner       | oid         | 关系所有者。                                                 |
| rcytablespace  | oid         | 这个关系存储所在的表空间。如果为0，则意味着使用该数据库的缺省表空间。如果关系在磁盘上没有文件，则这个字段没有什么意义。 |
| rcyrelfilenode | oid         | 回收站对象在磁盘上的文件的名称，如果没有则为0，用于TRUNCATE对象恢复时纹理文件还原。 |
| rcycanrestore  | bool        | 是否可以被单独闪回。                                         |
| rcycanpurge    | bool        | 是否可以被单独purge。                                        |
| rcyfrozenxid   | xid32       | 该表中所有在这个之前的事务ID已经被一个固定的（“frozen”）事务ID替换。 |
| rcyfrozenxid64 | xid         | 该表中所有在这个之前的事务ID已经被一个固定的（“frozen”）事务ID替换。 |
