---
title: PG_OBJECT
summary: PG_OBJECT
author: Guo Huan
date: 2021-04-19
---

# PG_OBJECT

PG_OBJECT系统表存储限定类型对象（普通表，索引，序列，视图，存储过程和函数）的创建用户、创建时间和最后修改时间。

**表 1** PG_OBJECT字段

| 名称        | 类型                     | 描述                                                         |
| :---------- | :----------------------- | :----------------------------------------------------------- |
| object_oid  | oid                      | 对象标识符                                                   |
| object_type | "char"                   | 对象类型: <br/>- r 表示普通表<br/>- i 表示索引<br/>- s 表示序列<br/>- v 表示视图<br/>- p 表示存储过程和函数 |
| creator     | oid                      | 创建用户的标识符                                             |
| ctime       | timestamp with time zone | 对象的创建时间                                               |
| mtime       | timestamp with time zone | 对象的最后修改时间，修改行为包括ALTER操作和GRANT、REVOKE操作 |
| createcsn   | int8                     | 对象创建时的CSN。                                            |
| changecsn   | int8                     | 对表或索引执行DDL操作时的CSN。                               |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 无法记录初始化数据库（initdb）过程中所创建或修改的对象，即PG_OBJECT无法查询到该对象记录。
> - 对于升级前创建的对象，再次修改时会记录其修改时间（mtime），对表或索引执行DDL操作时会记录其所属事务的事务提交序列号（changecsn）。由于无法得知该对象创建时间，因此ctime和createcsn为空。
> - ctime和mtime所记录的时间为用户当次操作所属事务的起始时间。
> - 由扩容引起的对象修改时间也会被记录。
> - createcsn和changecsn记录的是用户当次操作所属事务的事务提交序列号。
