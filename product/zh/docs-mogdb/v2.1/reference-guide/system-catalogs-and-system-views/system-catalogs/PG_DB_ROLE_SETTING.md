---
title: PG_DB_ROLE_SETTING
summary: PG_DB_ROLE_SETTING
author: Guo Huan
date: 2021-04-19
---

# PG_DB_ROLE_SETTING

PG_DB_ROLE_SETTING系统表存储数据库运行时每个角色与数据绑定的配置项的默认值 。

**表 1** PG_DB_ROLE_SETTING字段

| 名称        | 类型   | 描述                                            |
| :---------- | :----- | :---------------------------------------------- |
| setdatabase | oid    | 配置项所对应的数据库，如果未指定数据库，则为0。 |
| setrole     | oid    | 配置项所对应的角色，如果未指定角色，则为0。     |
| setconfig   | text[] | 运行时配置项的默认值。                          |
