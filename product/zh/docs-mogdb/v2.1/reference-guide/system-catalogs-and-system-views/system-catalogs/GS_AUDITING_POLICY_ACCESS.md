---
title: GS_AUDITING_POLICY_ACCESS
summary: GS_AUDITING_POLICY_ACCESS
author: Guo Huan
date: 2021-06-07
---

# GS_AUDITING_POLICY_ACCESS

GS_AUDITING_POLICY_ACCESS系统表记录与DML数据库相关操作的统一审计信息。需要有系统管理员或安全策略管理员权限才可以访问此系统表。

**表 1** GS_AUDITING_POLICY_ACCESS字段

| 名称       | 类型                        | 描述                                                        |
| :--------- | :-------------------------- | :---------------------------------------------------------- |
| oid        | oid                         | 行标识符（隐藏属性，必须明确选择）。                        |
| accesstype | name                        | DML数据库操作相关类型。例如SELECT、INSERT、DELETE等。       |
| labelname  | name                        | 资源标签名称。对应系统表gs_auditing_policy中的polname字段。 |
| policyoid  | oid                         | 对应审计策略系统表GS_AUDITING_POLICY中的oid。               |
| modifydate | timestamp without time zone | 创建或修改的最新时间戳。                                    |
