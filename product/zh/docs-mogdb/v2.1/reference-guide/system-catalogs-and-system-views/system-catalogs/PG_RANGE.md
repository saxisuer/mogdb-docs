---
-title: PG_RANGE
summary: PG_RANGE
author: Guo Huan
date: 2021-04-19
---

# PG_RANGE

PG_RANGE系统表存储关于范围类型的信息。除了PG_TYPE里类型的记录。

**表 1** PG_RANGE字段

| 名称         | 类型    | 引用             | 描述                                                         |
| :----------- | :------ | :--------------- | :----------------------------------------------------------- |
| rngtypid     | oid     | PG_TYPE.oid      | 范围类型的OID。                                              |
| rngsubtype   | oid     | PG_TYPE.oid      | 这个范围类型的元素类型（子类型）的OID。                      |
| rngcollation | oid     | PG_COLLATION.oid | 用于范围比较的排序规则的OID，如果没有则为零。                |
| rngsubopc    | oid     | PG_OPCLASS.oid   | 用于范围比较的子类型的操作符类的OID。                        |
| rngcanonical | regproc | PG_PROC.proname  | 转换范围类型为规范格式的函数名，如果没有则为0。              |
| rngsubdiff   | regproc | PG_PROC.proname  | 返回两个double precision元素值的不同的函数名，如果没有则为0。 |

rngsubopc（如果元素类型是可排序的，则加上rngcollation）决定用于范围类型的排序顺序。当元素类型是离散的时使用rngcanonical。
