---
title: PG_SHDESCRIPTION
summary: PG_SHDESCRIPTION
author: Guo Huan
date: 2021-04-19
---

# PG_SHDESCRIPTION

PG_SHDESCRIPTION系统表为共享数据库对象存储可选的注释。可以使用COMMENT命令操作注释的内容，使用psql的\\d命令查看注释内容。

PG_DESCRIPTION提供了类似的功能，它记录了单个数据库中对象的注释。

不同于大多数系统表，PG_SHDESCRIPTION是在MogDB里面所有的数据库之间共享的: 每个MogDB只有一个PG_SHDESCRIPTION，而不是每个数据库一个。

**表 1** PG_SHDESCRIPTION字段

| 名称        | 类型 | 引用         | 描述                           |
| :---------- | :--- | :----------- | :----------------------------- |
| objoid      | oid  | 任意OID属性  | 这条描述所描述的对象的OID。    |
| classoid    | oid  | PG_CLASS.oid | 这个对象出现的系统表的OID。    |
| description | text | -            | 作为对该对象的描述的任意文本。 |
