---
title: GS_ENCRYPTED_COLUMNS
summary: GS_ENCRYPTED_COLUMNS
author: Guo Huan
date: 2021-04-19
---

# GS_ENCRYPTED_COLUMNS

GS_ENCRYPTED_COLUMNS系统表记录密态等值特性中表的加密列相关信息，每条记录对应一条加密列信息。

**表 1** GS_ENCRYPTED_COLUMNS字段

| 名称                   | 类型      | 描述                                                         |
| :--------------------- | :-------- | :----------------------------------------------------------- |
| oid                    | oid       | 行标识符（隐含字段）。                                       |
| rel_id                 | oid       | 表的OID。                                                    |
| column_name            | name      | 加密列的名称。                                               |
| column_key_id          | oid       | 外键，列加密密钥的OID。                                      |
| encryption_type        | int1      | 加密类型。取值为2（DETERMINISTIC）或者1（RANDOMIZED）。      |
| data_type_original_oid | oid       | 加密列的原始数据类型id，参考系统表[PG_TYPE](PG_TYPE.md)中的oid。 |
| data_type_original_mod | int4      | 加密列的原始数据类型修饰符，参考系统表[PG_ATTRIBUTE](PG_ATTRIBUTE.md)中的atttypmod。其值对那些不需要的类型data_type_original_mod通常为-1。 |
| create_date            | timestamp | 创建加密列的时间。                                           |
