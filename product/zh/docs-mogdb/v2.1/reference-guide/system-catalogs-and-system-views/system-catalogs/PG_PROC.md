---
title: PG_PROC
summary: PG_PROC
author: Guo Huan
date: 2021-04-19
---

# PG_PROC

PG_PROC系统表存储函数或过程的信息。

**表 1** PG_PROC字段

| 名称                | 类型              | 描述                                                         |
| :------------------ | :---------------- | :----------------------------------------------------------- |
| oid                 | oid               | 行标识符（隐藏属性，必须明确选择）。                         |
| proname             | name              | 函数名称。                                                   |
| pronamespace        | oid               | 包含该函数名称空间的OID。                                    |
| proowner            | oid               | 函数的所有者。                                               |
| prolang             | oid               | 这个函数的实现语言或调用接口。                               |
| procost             | real              | 估算的执行成本。                                             |
| prorows             | real              | 估算的影响行的数目。                                         |
| provariadic         | oid               | 参数元素的数据类型。                                         |
| protransform        | regproc           | 此函数的简化调用方式。                                       |
| proisagg            | Boolean           | 函数是聚集函数。<br />- t（true）：表示是。<br />- f（false）：表示不是。 |
| proiswindow         | Boolean           | 函数是窗口函数。<br />- t（true）：表示是。<br />- f（false）：表示不是。 |
| prosecdef           | Boolean           | 函数是一个安全定义器（也就是一个“setuid”函数）。<br />- t（true）：表示是。<br />- f（false）：表示不是。 |
| proleakproof        | Boolean           | 函数没副作用。如果函数没有对参数进行防泄露处理，则会抛出错误。<br />- t（true）：表示没副作用。<br />- f（false）：表示有副作用。 |
| proisstrict         | Boolean           | 如果任何调用参数是空，则函数返回空。这时函数实际上连调用都不调用。不是“strict”的函数必须准备处理空输入。 |
| proretset           | Boolean           | 函数返回一个集合（也就是说，指定数据类型的多个数值）。       |
| provolatile         | “char”            | 告诉该函数的结果是否只依赖于它的输入参数，或者还会被外界因素影响。<br />- i：“不可变的”（immutable）函数，这样的函数对于相同的输入总是产生相同的结果。<br />- s：稳定的”（stable）函数它是s，（对于固定输入）其结果在一次扫描里不变。<br />- v：“易变”（volatile）函数它是v，其结果可能在任何时候变化v也用于那些有副作用的函数，因此调用它们无法得到优化。 |
| pronargs            | smallint          | 参数数目。                                                   |
| pronargdefaults     | smallint          | 有默认值的参数数目。                                         |
| prorettype          | oid               | 返回值的数据类型。                                           |
| proargtypes         | oidvector         | 一个存放函数参数的数据类型的数组。数组里只包括输入参数（包括INOUT参数）此代表该函数的调用签名（接口）。 |
| proallargtypes      | oid[]             | 一个包含函数参数的数据类型的数组。数组里包括所有参数的类型（包括OUT和INOUT参数），如果所有参数都是IN参数，则这个字段就会是空。请注意数组下标是以1为起点的，而因为历史原因，proargtypes的下标起点为0。 |
| proargmodes         | “char”[]          | 一个保存函数参数模式的数组，编码如下：<br />- i表示IN参数。<br />- o表示OUT参数。<br />- b表示INOUT参数。<br />- v表示VARIADIC参数。<br />如果所有参数都是IN参数，则这个字段为空。请注意，下标对应的是proallargtypes的位置，而不是proargtypes。 |
| proargnames         | text[]            | 一个保存函数参数的名称的数组。没有名称的参数在数组里设置为空字符串。如果没有一个参数有名称，这个字段将是空。请注意，此数组的下标对应proallargtypes而不是proargtypes。 |
| proargdefaults      | pg_node_tree      | 默认值的表达式树。是PRONARGDEFAULTS元素的列表。              |
| prosrc              | text              | 描述函数或存储过程的定义。例如，对于解释型语言来说就是函数的源程序，或者一个链接符号，一个文件名，或者函数和存储过程创建时指定的其他任何函数体内容，具体取决于语言/调用习惯的实现。 |
| probin              | text              | 关于如何调用该函数的附加信息。同样，其含义也是和语言相关的。 |
| proconfig           | text[]            | 函数针对运行时配置变量的本地设置。                           |
| proacl              | aclitem[]         | 访问权限。具体请参见GRANT和REVOKE。                          |
| prodefaultargpos    | int2vector        | 函数具有默认值的入参的位置。                                 |
| fencedmode          | Boolean           | 函数的执行模式，表示函数是在fence还是not fence模式下执行。如果是fence执行模式，函数的执行会在重新fork的进程中执行。<br />用户创建的C函数，fencedmode字段默认值均为true，即fence模式；系统内建函数，fencedmode字段均为false，即not fence模式。 |
| proshippable        | Boolean           | 表示该函数是否可以下推到数据库节点上执行，默认值是false。<br />- 对于IMMUTABLE类型的函数，函数始终可以下推到数据库节点上执行。<br />- 对于STABLE/VOLATILE类型的函数，仅当函数的属性是SHIPPABLE的时候，函数可以下推到数据库节点执行。 |
| propackage          | Boolean           | 表示该函数是否支持重载，默认值是false。<br />- t（true）：表示支持。<br />- f（false）：表示不支持。 |
| prokind             | “char”            | 表示该对象为函数还是存储过程：<br />- 值为'f'表示该对象为函数。<br />- 值为'p'表示该对象为存储过程。 |
| proargsrc           | text              | 描述兼容oracle语法定义的函数或存储过程的参数输入字符串，包括参数注释。默认值为NULL。 |
| proisprivate        | Boolean           | 描述函数是否是PACKAGE内的私有函数，默认为false。             |
| propackageid        | oid               | 函数所属的package oid，如果不在package内，则为0。            |
| proargtypesext      | oidvector_extend  | 当函数参数较多时，用来存放函数参数的数据类型的数组。数组里只包括输入参数（包括INOUT参数）此代表该函数的调用签名（接口）。 |
| prodefaultargposext | int2vector_extend | 当函数参数较多时，函数具有默认值的入参的位置。               |
