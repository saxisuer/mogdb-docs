---
title: PG_OPERATOR
summary: PG_OPERATOR
author: Guo Huan
date: 2021-04-19
---

# PG_OPERATOR

PG_OPERATOR系统表存储有关操作符的信息。

**表 1** PG_OPERATOR字段

| 名称         | 类型    | 引用             | 描述                          |
| :----------- | :------ | :--------------- | :---------------------------- |
| oid          | oid     | -                | 行标识符（隐藏属性，必须明确选择）。                         |
| oprname      | name    | -                | 操作符的名称。                                               |
| oprnamespace | oid     | PG_NAMESPACE.oid | 包含此操作符的名称空间的OID。                                |
| oprowner     | oid     | PG_AUTHID.oid    | 操作符所有者。                                               |
| oprkind      | "char"  | -                | - b=infix =中缀("两边")<br/>- l=前缀("左边")<br/>- r=后缀("右边") |
| oprcanmerge  | Boolean | -                | 这个操作符是否支持合并连接。<br />- t（true）：表示支持合并连接。<br />- f（false）：表示不支持合并连接。 |
| oprcanhash   | Boolean | -                | 这个操作符是否支持Hash连接。<br />- t（true）：表示支持Hash连接。<br />- f（false）：表示不支持Hash连接。 |
| oprleft      | oid     | PG_TYPE.oid      | 左操作数的类型。                                             |
| oprright     | oid     | PG_TYPE.oid      | 右操作数的类型。                                             |
| oprresult    | oid     | PG_TYPE.oid      | 结果类型。                                                   |
| oprcom       | oid     | PG_OPERATOR.oid  | 此操作符的交换符，如果存在的话。                             |
| oprnegate    | oid     | PG_OPERATOR.oid  | 此操作符的反转器，如果存在的话。                             |
| oprcode      | regproc | PG_PROC.proname  | 实现这个操作符的函数。                                       |
| oprrest      | regproc | PG_PROC.proname  | 此操作符的约束选择性计算函数。                               |
| oprjoin      | regproc | PG_PROC.proname  | 此操作符的连接选择性计算函数。                               |
