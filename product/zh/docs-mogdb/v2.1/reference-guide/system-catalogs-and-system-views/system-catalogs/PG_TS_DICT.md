---
title: PG_TS_DICT
summary: PG_TS_DICT
author: Guo Huan
date: 2021-04-19
---

# PG_TS_DICT

PG_TS_DICT系统表包含定义文本搜索字典的记录。字典取决于文本搜索模板，该模板声明所有需要的实现函数；字典本身提供模板支持的用户可设置的参数的值。

这种分工允许字典通过非权限用户创建。参数由文本字符串dictinitoption指定，参数的格式和意义取决于模板。

**表 1** PG_TS_DICT字段

| 名称           | 类型 | 引用               | 描述                                 |
| :------------- | :--- | :----------------- | :----------------------------------- |
| oid            | oid  | -                  | 行标识符（隐藏属性，必须明确选择）。 |
| dictname       | name | -                  | 文本搜索字典名。                     |
| dictnamespace  | oid  | PG_NAMESPACE.oid   | 包含这个字典的名称空间的OID。        |
| dictowner      | oid  | PG_AUTHID.oid      | 字典的所有者。                       |
| dicttemplate   | oid  | PG_TS_TEMPLATE.oid | 这个字典的文本搜索模板的OID。        |
| dictinitoption | text | -                  | 该模板的初始化选项字符串。           |
