---
title: STATEMENT_HISTORY
summary: STATEMENT_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_HISTORY

获得当前节点的执行语句的信息。查询系统表必须具有sysadmin权限。只可在系统库中查询到结果，用户库中无法查询。

对于此系统表查询有如下约束：

- 必须在postgres库内查询，其它库中不存数据。
- 此系统表受track_stmt_stat_level控制，默认为”OFF,L0”，第一部分控制Full SQL，第二部分控制Slow SQL，具体字段记录级别见下表。
- 对于Slow SQL，当track_stmt_stat_level的值为非OFF时，且SQL执行时间超过log_min_duration_statement，会记录为慢SQL。

**表 1** STATEMENT_HISTORY字段

| 名称                 | 类型                     | 描述                                                         | 记录级别 |
| :------------------- | :----------------------- | :----------------------------------------------------------- | :------- |
| db_name              | name                     | 数据库名称。                                                 | L0       |
| schema_name          | name                     | schema名称。                                                 | L0       |
| origin_node          | integer                  | 节点名称。                                                   | L0       |
| user_name            | name                     | 用户名。                                                     | L0       |
| application_name     | text                     | 用户发起的请求的应用程序名称。                               | L0       |
| client_addr          | text                     | 用户发起的请求的客户端地址。                                 | L0       |
| client_port          | integer                  | 用户发起的请求的客户端端口。                                 | L0       |
| unique_query_id      | bigint                   | 归一化SQL ID。                                               | L0       |
| debug_query_id       | bigint                   | 唯一SQL ID。                                                 | L0       |
| query                | text                     | 归一化SQL。                                                  | L0       |
| start_time           | timestamp with time zone | 语句启动的时间。                                             | L0       |
| finish_time          | timestamp with time zone | 语句结束的时间。                                             | L0       |
| slow_sql_threshold   | bigint                   | 语句执行时慢SQL的标准。                                      | L0       |
| transaction_id       | bigint                   | 事务ID。                                                     | L0       |
| thread_id            | bigint                   | 执行线程ID。                                                 | L0       |
| session_id           | bigint                   | 用户session id。                                             | L0       |
| n_soft_parse         | bigint                   | 软解析次数，n_soft_parse + n_hard_parse可能大于n_calls，因为子查询未计入n_calls。 | L0       |
| n_hard_parse         | bigint                   | 硬解析次数，n_soft_parse + n_hard_parse可能大于n_calls，因为子查询未计入n_calls。 | L0       |
| query_plan           | text                     | 语句执行计划。                                               | L1       |
| n_returned_rows      | bigint                   | SELECT返回的结果集行数。                                     | L0       |
| n_tuples_fetched     | bigint                   | 随机扫描行。                                                 | L0       |
| n_tuples_returned    | bigint                   | 顺序扫描行。                                                 | L0       |
| n_tuples_inserted    | bigint                   | 插入行。                                                     | L0       |
| n_tuples_updated     | bigint                   | 更新行。                                                     | L0       |
| n_tuples_deleted     | bigint                   | 删除行。                                                     | L0       |
| n_blocks_fetched     | bigint                   | buffer的块访问次数。                                         | L0       |
| n_blocks_hit         | bigint                   | buffer的块命中次数。                                         | L0       |
| db_time              | bigint                   | 有效的DB时间花费，多线程将累加（单位：微秒）。               | L0       |
| cpu_time             | bigint                   | CPU时间（单位：微秒）。                                      | L0       |
| execution_time       | bigint                   | 执行器内执行时间（单位：微秒）。                             | L0       |
| parse_time           | bigint                   | SQL解析时间（单位：微秒）。                                  | L0       |
| plan_time            | bigint                   | SQL生成计划时间（单位：微秒）。                              | L0       |
| rewrite_time         | bigint                   | SQL重写时间（单位：微秒）。                                  | L0       |
| pl_execution_time    | bigint                   | plpgsql上的执行时间（单位：微秒）。                          | L0       |
| pl_compilation_time  | bigint                   | plpgsql上的编译时间（单位：微秒）。                          | L0       |
| data_io_time         | bigint                   | IO上的时间花费（单位：微秒）。                               | L0       |
| net_send_info        | text                     | 通过物理连接发送消息的网络状态，包含时间（微秒）、调用次数、吞吐量（字节）。通过该字段可以分析SQL在分布式系统下的网络开销，单机模式下不支持该字段。例如：{“time”:xxx, “n_calls”:xxx, “size”:xxx}。 | L0       |
| net_recv_info        | text                     | 通过物理连接接收消息的网络状态，包含时间（微秒）、调用次数、吞吐量（字节）。通过该字段可以分析SQL在分布式系统下的网络开销，单机模式下不支持该字段。例如：{“time”:xxx, “n_calls”:xxx, “size”:xxx}。 | L0       |
| net_stream_send_info | text                     | 通过逻辑连接发送消息的网络状态，包含时间（微秒）、调用次数、吞吐量（字节）。通过该字段可以分析SQL在分布式系统下的网络开销，单机模式下不支持该字段。例如：{“time”:xxx, “n_calls”:xxx, “size”:xxx}。 | L0       |
| net_stream_recv_info | text                     | 通过逻辑连接接收消息的网络状态，包含时间（微秒）、调用次数、吞吐量（字节）。通过该字段可以分析SQL在分布式系统下的网络开销，单机模式下不支持该字段。例如：{“time”:xxx, “n_calls”:xxx, “size”:xxx}。 | L0       |
| lock_count           | bigint                   | 加锁次数。                                                   | L0       |
| lock_time            | bigint                   | 加锁耗时。                                                   | L1       |
| lock_wait_count      | bigint                   | 加锁等待次数。                                               | L0       |
| lock_wait_time       | bigint                   | 加锁等待耗时。                                               | L1       |
| lock_max_count       | bigint                   | 最大持锁数量。                                               | L0       |
| lwlock_count         | bigint                   | 轻量级加锁次数（预留）。                                     | L0       |
| lwlock_wait_count    | bigint                   | 轻量级等锁次数。                                             | L0       |
| lwlock_time          | bigint                   | 轻量级加锁时间（预留）。                                     | L1       |
| lwlock_wait_time     | bigint                   | 轻量级等锁时间。                                             | L1       |
| details              | bytea                    | 语句锁事件的列表，该列表按时间书序记录事件，记录的数量受参数track_stmt_details_size的影响。<br />事件包括：<br />- 加锁开始<br />- 加锁结束<br />- 等锁开始<br />- 等锁结束<br />- 放锁开始<br />- 放锁结束<br />- 轻量级等锁开始<br />- 轻量级等锁结束 | L2       |
| is_slow_sql          | boolean                  | 该SQL是否为slow SQL。                                        | L0       |
