---
title: PG_COLLATION
summary: PG_COLLATION
author: Guo Huan
date: 2021-04-19
---

# PG_COLLATION

PG_COLLATION系统表描述可用的排序规则，本质上从一个SQL名称映射到操作系统本地类别。

**表 1** PG_COLLATION字段

| 名称          | 类型    | 引用             | 描述                        |
| :------------ | :------ | :--------------- | :-------------------------- |
| oid           | oid     | -                | 行标识符（隐藏属性，必须明确选择）。                         |
| collname      | name    | -                | 排序规则名（每个名称空间和编码唯一）。                       |
| collnamespace | oid     | PG_NAMESPACE.oid | 包含这个排序规则的名称空间的OID。                            |
| collowner     | oid     | PG_AUTHID.oid    | 排序规则的所有者。                                           |
| collencoding  | integer | -                | 排序规则可用的编码，兼容PostgreSQL所有的字符编码类型，如果适用于任意编码为-1。 |
| collcollate   | name    | -                | 这个排序规则对象的LC_COLLATE。                               |
| collctype     | name    | -                | 这个排序规则对象的LC_CTYPE。                                 |
