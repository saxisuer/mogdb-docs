---
title: PG_TS_TEMPLATE
summary: PG_TS_TEMPLATE
author: Guo Huan
date: 2021-04-19
---

# PG_TS_TEMPLATE

PG_TS_TEMPLATE系统表包含定义文本搜索模板的记录。模板是文本搜索字典的类的实现框架。因为模板必须通过C语言级别的函数实现，索引新模板的创建必须由数据库系统管理员创建。

**表 1** PG_TS_TEMPLATE字段

| 名称          | 类型    | 引用             | 描述                                 |
| :------------ | :------ | :--------------- | :----------------------------------- |
| oid           | oid     | -                | 行标识符（隐藏属性；必须明确选择）。 |
| tmplname      | name    | -                | 文本搜索模板名。                     |
| tmplnamespace | oid     | PG_NAMESPACE.oid | 包含这个模板的名称空间的OID。        |
| tmplinit      | regproc | PG_PROC.proname  | 模板的初始化函数名。                 |
| tmpllexize    | regproc | PG_PROC.proname  | 模板的lexize函数名。                 |
