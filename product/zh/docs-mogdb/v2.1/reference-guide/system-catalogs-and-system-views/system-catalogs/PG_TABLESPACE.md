---
title: PG_TABLESPACE
summary: PG_TABLESPACE
author: Guo Huan
date: 2021-04-19
---

# PG_TABLESPACE

PG_TABLESPACE系统表存储表空间信息。

**表 1** PG_TABLESPACE字段

| 名称       | 类型      | 描述                                                         |
| :--------- | :-------- | :----------------------------------------------------------- |
| oid        | oid       | 行标识符（隐藏属性，必须明确选择）。                         |
| spcname    | name      | 表空间名称。                                                 |
| spcowner   | oid       | 表空间的所有者，通常是创建它的人。                           |
| spcacl     | aclitem[] | 访问权限。具体请参见[GRANT](../../../reference-guide/sql-syntax/GRANT.md)和[REVOKE](../../../reference-guide/sql-syntax/REVOKE.md)。 |
| spcoptions | text[]    | 表空间的选项。                                               |
| spcmaxsize | text      | 可使用的最大磁盘空间大小，单位Byte。                         |
| relative   | boolean   | 标识表空间指定的存储路径是否为相对路径。                     |
