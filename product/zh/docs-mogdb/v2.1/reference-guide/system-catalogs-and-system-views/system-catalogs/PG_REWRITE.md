---
title: PG_REWRITE
summary: PG_REWRITE
author: Guo Huan
date: 2021-04-19
---

# PG_REWRITE

PG_REWRITE系统表存储为表和视图定义的重写规则。

**表 1** PG_REWRITE字段

| 名称       | 类型         | 描述                                                         |
| :--------- | :----------- | :----------------------------------------------------------- |
| oid        | oid          | 行标识符（隐藏属性，必须明确选择）。                         |
| rulename   | name         | 规则名称。                                                   |
| ev_class   | oid          | 使用这条规则的表名称。                                       |
| ev_attr    | smallint     | 这条规则适用的字段（目前总是为零，表示整个表）。             |
| ev_type    | "char"       | 规则适用的事件类型: <br/>- 1 = SELECT<br/>- 2 = UPDATE<br/>- 3 = INSERT<br/>- 4 = DELETE |
| ev_enabled | "char"       | 用于控制复制的触发。<br/>- O ="origin"和"local"模式时触发。<br/>- D =禁用触发。<br/>- R ="replica"时触发。<br/>- A ＝任何模式是都会触发。 |
| is_instead | Boolean      | 如果该规则是INSTEAD规则，则为真。                            |
| ev_qual    | pg_node_tree | 规则的资格条件的表达式树（以nodeToString() 形式存在）。      |
| ev_action  | pg_node_tree | 规则动作的查询树（以nodeToString() 形式存在）。              |
