---
title: DBE_PLDEBUGGER Schema概述
summary: DBE_PLDEBUGGER Schema概述
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER Schema概述

DBE_PLDEBUGGER下系统函数用于单机下调试存储过程，目前支持的接口及其描述如下所示。仅管理员有权限执行这些调试接口。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 当在函数体中创建用户时，调用attach、next、continue、 info_code、step、info_breakpoint、backtrace、 finish中会返回密码的明文。因此不建议用户在函数体中创建用户。

对应权限角色为gs_role_pldebugger，可以由管理员用户通过如下命令将debugger权限赋权给该用户。

```sql
GRANT gs_role_pldebugger to user;
```

需要有两个客户端连接数据库，一个客户端负责执行调试接口作为debug端，另一个客户端执行调试函数，控制server端存储过程执行。示例如下。

- 准备调试

  通过PG_PROC，查找到待调试存储过程的oid，并执行DBE_PLDEBUGGER.turn_on(oid)。本客户端就会作为server端使用。

  ```sql
  mogdb=# CREATE OR REPLACE PROCEDURE test_debug ( IN  x INT)
  AS
  BEGIN
        INSERT INTO t1 (a) VALUES (x);
  END;
  /
  CREATE PROCEDURE
  mogdb=# SELECT OID FROM PG_PROC WHERE PRONAME='test_debug';
    oid
  -------
   16389
  (1 row)
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.turn_on(16389);
   nodename | port
  ----------+------
   datanode |    0
  (1 row)
  ```

- 开始调试

  server端执行存储过程，会在存储过程内第一条SQL语句前hang住，等待debug端发送的调试消息。仅支持直接执行存储过程的调试，不支持通过trigger调用执行的存储过程调试。

  ```sql
  mogdb=# call test_debug(1);
  ```

  再起一个客户端，作为debug端，通过turn_on返回的数据，调用DBE_PLDEBUGGER.attach关联到该存储过程上进行调试。

  ```sql
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.attach('datanode',0);
   funcoid |  funcname  | lineno |              query
  ---------+------------+--------+----------------------------------
     16389 | test_debug |      3 |   INSERT INTO t1 (a) VALUES (x);
  (1 row)
  ```

  在执行attach的客户端调试，执行下一条statement。

  ```sql
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.next();
   funcoid |  funcname  | lineno |        query
  ---------+------------+--------+----------------------
     16389 | test_debug |      0 | [EXECUTION FINISHED]
  (1 row)
  ```

  在执行attach的客户端调试，将当前所有变量输出。

  ```sql
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.info_locals();
   varname | vartype | value | package_name
  ---------+---------+-------+--------------
   $1      | int4    | 1     |
  (1 row)
  ```

  直接执行完成当前正在调试的存储过程。

  ```sql
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.continue();
   funcoid |  funcname  | lineno |        query
  ---------+------------+--------+----------------------
     16389 | test_debug |      0 | [EXECUTION FINISHED]
  (1 row)
  ```

  直接退出当前正在调试的存储过程，不执行尚未执行的语句。

  ```sql
  mogdb=# SELECT * FROM DBE_PLDEBUGGER.abort();
   abort
  -------
   t
  (1 row)
  ```

  存储过程执行结束后，调试会自动退出，再进行调试需要重新attach关联。如果server端不需要继续调试，可执行turn_off关闭，或退出session。具体调试接口请见下面列表。

  **表 1** DBE_PLDEBUGGER

  | 接口名称                                                     | 描述                                                         |
  | :----------------------------------------------------------- | :----------------------------------------------------------- |
  | [DBE_PLDEBUGGER.turn_on](DBE_PLDEBUGGER.turn_on.md)          | server端调用，标记存储过程可以调试，调用后执行该存储过程时会hang住等待调试信息。 |
  | [DBE_PLDEBUGGER.turn_off](DBE_PLDEBUGGER.turn_off.md)        | server端调用，标记存储过程关闭调试。                         |
  | [DBE_PLDEBUGGER.local_debug_server_info](DBE_PLDEBUGGER.local_debug_server_info.md) | server端调用，打印本session内所有已turn_on的存储过程。       |
  | [DBE_PLDEBUGGER.attach](DBE_PLDEBUGGER.attach.md)            | debug端调用，关联到正在调试存储过程。                        |
  | [DBE_PLDEBUGGER.info_locals](DBE_PLDEBUGGER.info_locals.md)  | debug端调用，打印正在调试的存储过程中的变量当前值。          |
  | [DBE_PLDEBUGGER.next](DBE_PLDEBUGGER.next.md)                | debug端调用，单步执行。                                      |
  | [DBE_PLDEBUGGER.continue](DBE_PLDEBUGGER.continue.md)        | debug端调用，继续执行，直到断点或存储过程结束。              |
  | [DBE_PLDEBUGGER.abort](DBE_PLDEBUGGER.abort.md)              | debug端调用，停止调试，server端报错长跳转。                  |
  | [DBE_PLDEBUGGER.print_var](DBE_PLDEBUGGER.print_var.md)      | debug端调用，打印正在调试的存储过程中指定的变量当前值。      |
  | [DBE_PLDEBUGGER.info_code](DBE_PLDEBUGGER.info_code.md)      | debug和server端都可以调用，打印指定存储过程的源语句和各行对应的行号。 |
  | [DBE_PLDEBUGGER.step](DBE_PLDEBUGGER.step.md)                | debug端调用，单步进入执行。                                  |
  | [DBE_PLDEBUGGER.add_breakpoint](DBE_PLDEBUGGER.add_breakpoint.md) | debug端调用，新增断点。                                      |
  | [DBE_PLDEBUGGER.delete_breakpoint](DBE_PLDEBUGGER.delete_breakpoint.md) | debug端调用，删除断点。                                      |
  | [DBE_PLDEBUGGER.info_breakpoints](DBE_PLDEBUGGER.info_breakpoints.md) | debug端调用，查看当前的所有断点。                            |
  | [DBE_PLDEBUGGER.backtrace](DBE_PLDEBUGGER.backtrace.md)      | debug端调用，查看当前的调用栈。                              |
  | [DBE_PLDEBUGGER.enable_breakpoint](DBE_PLDEBUGGER.enable_breakpoint.md) | debug端调用，激活被禁用的断点。                              |
  | [DBE_PLDEBUGGER.disable_breakpoint](DBE_PLDEBUGGER.disable_breakpoint.md) | debug端调用，禁用已激活的断点。                              |
  | [DBE_PLDEBUGGER.finish](DBE_PLDEBUGGER.finish.md)            | debug端调用，继续调试，直到断点或返回上一层调用栈。          |
  | [DBE_PLDEBUGGER.set_var](DBE_PLDEBUGGER.set_var.md)          | debug端调用，为变量进行赋值操作。                            |
