---
title: DBE_PLDEBUGGER.turn_off
summary: DBE_PLDEBUGGER.turn_off
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.turn_off

用于去掉turn_on添加的调试标记，返回值表示成功或失败。可通过DBE_PLDEBUGGER.local_debug_server_info查找已经turn_on的存储过程oid。

函数原型为：

```sql
DBE_PLDEBUGGER.turn_off(Oid)
RETURN boolean;
```

**表 1** turn_off 入参和返回值列表

| 名称     | 类型        | 描述               |
| :------- | :---------- | :----------------- |
| func_oid | IN oid      | 函数oid。          |
| turn_off | OUT boolean | turn off是否成功。 |
