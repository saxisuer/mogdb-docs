---
title: DB4AI.MANAGE_SNAPSHOT_INTERNAL
summary: DB4AI.MANAGE_SNAPSHOT_INTERNAL
author: Guo Huan
date: 2021-11-15
---

# DB4AI.MANAGE_SNAPSHOT_INTERNAL

MANAGE_SNAPSHOT_INTERNAL是DB4AI.PUBLISH_SNAPSHOT和DB4AI.ARCHIVE_SNAPSHOT函数的内置执行函数。函数存在信息校验，无法直接调用。

**表 1** DB4AI.MANAGE_SNAPSHOT_INTERNAL入参和返回值列表

| 参数     | 类型                    | 描述               |
| :------- | :---------------------- | :----------------- |
| i_schema | IN NAME                 | 快照存储的模式名字 |
| i_name   | IN NAME                 | 快照名称           |
| publish  | IN BOOLEN               | 是否是发布状态     |
| res      | OUT db4ai.snapshot_name | 结果               |
