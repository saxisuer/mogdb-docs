---
title: WLM_USER_RESOURCE_CONFIG
summary: WLM_USER_RESOURCE_CONFIG
author: Guo Huan
date: 2021-04-19
---

# WLM_USER_RESOURCE_CONFIG

WLM_USER_RESOURCE_CONFIG视图显示用户的资源配置信息。

**表 1** WLM_USER_RESOURCE_CONFIG字段

| 名称       | 类型    | 描述             |
| :--------- | :------ | :--------------- |
| userid     | oid     | 用户oid。        |
| username   | name    | 用户名称。       |
| sysadmin   | boolean | 是否是sysadmin。 |
| rpoid      | oid     | 资源池的oid。    |
| respool    | name    | 资源池的名称。   |
| parentid   | oid     | 父用户的oid。    |
| totalspace | bigint  | 占用总空间大小。 |
| spacelimit | bigint  | 空间大上限。     |
| childcount | integer | 子用户数量。     |
| childlist  | text    | 子用户的列表。   |
