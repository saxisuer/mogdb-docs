---
title: SUMMARY_WORKLOAD_SQL_COUNT
summary: SUMMARY_WORKLOAD_SQL_COUNT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_WORKLOAD_SQL_COUNT

显示MogDB内各数据库主节点的workload上的SQL数量分布。

**表 1** SUMMARY_WORKLOAD_SQL_COUNT字段

| **名称**     | **类型** | **描述**         |
| :----------- | :------- | :--------------- |
| node_name    | name     | 数据库进程名称。 |
| workload     | name     | 负载名称。       |
| select_count | bigint   | select数量。     |
| update_count | bigint   | update数量。     |
| insert_count | bigint   | insert数量。     |
| delete_count | bigint   | delete数量。     |
| ddl_count    | bigint   | ddl数量。        |
| dml_count    | bigint   | dml数量。        |
| dcl_count    | bigint   | dcl数量。        |
