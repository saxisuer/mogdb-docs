---
title: GLOBAL_STATIO_USER_TABLES
summary: GLOBAL_STATIO_USER_TABLES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_USER_TABLES

GLOBAL_STATIO_USER_TABLES视图显示各节点的命名空间中所有用户关系表的IO状态信息。

**表 1** GLOBAL_STATIO_USER_TABLES字段

| **名称**        | **类型** | **描述**                                      |
| :-------------- | :------- | :-------------------------------------------- |
| node_name       | name     | 节点名称。                                    |
| relid           | oid      | 表OID。                                       |
| schemaname      | name     | 该表模式名。                                  |
| relname         | name     | 表名。                                        |
| heap_blks_read  | bigint   | 从该表中读取的磁盘块数。                      |
| heap_blks_hit   | bigint   | 此表缓存命中数。                              |
| idx_blks_read   | bigint   | 从表中所有索引读取的磁盘块数。                |
| idx_blks_hit    | bigint   | 表中所有索引命中缓存数。                      |
| toast_blks_read | bigint   | 此表的TOAST表读取的磁盘块数（如果存在）。     |
| toast_blks_hit  | bigint   | 此表的TOAST表命中缓冲区数（如果存在）。       |
| tidx_blks_read  | bigint   | 此表的TOAST表索引读取的磁盘块数（如果存在）。 |
| tidx_blks_hit   | bigint   | 此表的TOAST表索引命中缓冲区数（如果存在）。   |
