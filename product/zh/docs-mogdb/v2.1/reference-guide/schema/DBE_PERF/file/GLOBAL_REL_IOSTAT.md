---
title: GLOBAL_REL_IOSTAT
summary: GLOBAL_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REL_IOSTAT

获取所有节点上的数据文件IO统计信息。

**表 1** GLOBAL_REL_IOSTAT字段

| **名称**  | **类型** | **描述**             |
| :-------- | :------- | :------------------- |
| node_name | name     | 数据库进程名称。     |
| phyrds    | bigint   | 读物理文件的数目。   |
| phywrts   | bigint   | 写物理文件的数目。   |
| phyblkrd  | bigint   | 读物理文件块的数目。 |
| phyblkwrt | bigint   | 写物理文件块的数目。 |
