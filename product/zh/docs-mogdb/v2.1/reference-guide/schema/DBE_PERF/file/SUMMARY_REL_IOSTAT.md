---
title: SUMMARY_REL_IOSTAT
summary: SUMMARY_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_REL_IOSTAT

获取所有节点上的数据文件IO统计信息。

**表 1** SUMMARY_REL_IOSTAT字段

| **名称**  | **类型** | **描述**               |
| :-------- | :------- | :--------------------- |
| phyrds    | numeric  | 读物理文件的数目。     |
| phywrts   | numeric  | 写物理文件的数目。     |
| phyblkrd  | numeric  | 读物理文件的块的数目。 |
| phyblkwrt | numeric  | 写物理文件的块的数目。 |
