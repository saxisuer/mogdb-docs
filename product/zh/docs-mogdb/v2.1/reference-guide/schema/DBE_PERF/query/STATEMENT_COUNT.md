---
title: STATEMENT_COUNT
summary: STATEMENT_COUNT
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COUNT

显示数据库当前节点当前时刻执行的五类语句（SELECT、INSERT、UPDATE、DELETE、MERGE INTO）和（DDL、DML、DCL）统计信息。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 普通用户查询STATEMENT_COUNT视图仅能看到该用户当前节点的统计信息；管理员权限用户查询STATEMENT_COUNT视图则能看到所有用户当前节点的统计信息。当MogDB或该节点重启时，计数将清零，并重新开始计数。计数以节点收到的查询数为准，MogDB内部进行的查询。例如，数据库主节点收到一条查询，若下发多条查询数据库节点，那将在数据库节点上进行相应次数的计数。

**表 1** STATEMENT_COUNT字段

| **名称**            | **类型** | **描述**                             |
| :------------------ | :------- | :----------------------------------- |
| node_name           | text     | 数据库进程名称。                     |
| user_name           | text     | 用户名。                             |
| select_count        | bigint   | select语句统计结果。                 |
| update_count        | bigint   | update语句统计结果。                 |
| insert_count        | bigint   | insert语句统计结果。                 |
| delete_count        | bigint   | delete语句统计结果。                 |
| mergeinto_count     | bigint   | merge into语句统计结果。             |
| ddl_count           | bigint   | DDL语句的数量。                      |
| dml_count           | bigint   | DML语句的数量。                      |
| dcl_count           | bigint   | DCL语句的数量。                      |
| total_select_elapse | bigint   | 总select的时间花费（单位: 微秒）。   |
| avg_select_elapse   | bigint   | 平均select的时间花费（单位: 微秒）。 |
| max_select_elapse   | bigint   | 最大select的时间花费（单位: 微秒）。 |
| min_select_elapse   | bigint   | 最小select的时间花费（单位: 微秒）。 |
| total_update_elapse | bigint   | 总update的时间花费（单位: 微秒）。   |
| avg_update_elapse   | bigint   | 平均update的时间花费(单位: 微秒)。   |
| max_update_elapse   | bigint   | 最大update的时间花费（单位: 微秒）。 |
| min_update_elapse   | bigint   | 最小update的时间花费（单位: 微秒）。 |
| total_insert_elapse | bigint   | 总insert的时间花费（单位: 微秒）。   |
| avg_insert_elapse   | bigint   | 平均insert的时间花费（单位: 微秒）。 |
| max_insert_elapse   | bigint   | 最大insert的时间花费（单位: 微秒）。 |
| min_insert_elapse   | bigint   | 最小insert的时间花费（单位: 微秒）。 |
| total_delete_elapse | bigint   | 总delete的时间花费（单位: 微秒）。   |
| avg_delete_elapse   | bigint   | 平均delete的时间花费（单位: 微秒）。 |
| max_delete_elapse   | bigint   | 最大delete的时间花费（单位: 微秒）。 |
| min_delete_elapse   | bigint   | 最小delete的时间花费（单位: 微秒）。 |
