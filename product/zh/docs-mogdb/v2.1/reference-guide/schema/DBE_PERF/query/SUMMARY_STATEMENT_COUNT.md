---
title: SUMMARY_STATEMENT_COUNT
summary: SUMMARY_STATEMENT_COUNT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATEMENT_COUNT

显示数据库汇聚各节点（数据库节点）当前时刻执行的五类语句（SELECT、INSERT、UPDATE、DELETE、MERGE INTO）和（DDL、DML、DCL）统计信息。

**表 1** SUMMARY_STATEMENT_COUNT字段

| **名称**            | **类型** | **描述**                             |
| :------------------ | :------- | :----------------------------------- |
| user_name           | text     | 用户名。                             |
| select_count        | numeric  | select语句统计结果。                 |
| update_count        | numeric  | update语句统计结果。                 |
| insert_count        | numeric  | insert语句统计结果。                 |
| delete_count        | numeric  | delete语句统计结果。                 |
| mergeinto_count     | numeric  | merge into语句统计结果。             |
| ddl_count           | numeric  | DDL语句的数量。                      |
| dml_count           | numeric  | DML语句的数量。                      |
| dcl_count           | numeric  | DCL语句的数量。                      |
| total_select_elapse | numeric  | 总select的时间花费（单位: 微秒）。   |
| avg_select_elapse   | bigint   | 平均select的时间花费（单位: 微秒）。 |
| max_select_elapse   | bigint   | 最大select的时间花费（单位: 微秒）。 |
| min_select_elapse   | bigint   | 最小select的时间花费（单位: 微秒）。 |
| total_update_elapse | numeric  | 总update的时间花费（单位: 微秒）。   |
| avg_update_elapse   | bigint   | 平均update的时间花费（单位: 微秒）。 |
| max_update_elapse   | bigint   | 最大update的时间花费（单位: 微秒）。 |
| min_update_elapse   | bigint   | 最小update的时间花费（单位: 微秒）。 |
| total_insert_elapse | numeric  | 总insert的时间花费（单位: 微秒）。   |
| avg_insert_elapse   | bigint   | 平均insert的时间花费（单位: 微秒）。 |
| max_insert_elapse   | bigint   | 最大insert的时间花费（单位: 微秒）。 |
| min_insert_elapse   | bigint   | 最小insert的时间花费（单位: 微秒）。 |
| total_delete_elapse | numeric  | 总delete的时间花费（单位: 微秒）。   |
| avg_delete_elapse   | bigint   | 平均delete的时间花费（单位: 微秒）。 |
| max_delete_elapse   | bigint   | 最大delete的时间花费（单位: 微秒）。 |
| min_delete_elapse   | bigint   | 最小delete的时间花费（单位: 微秒）。 |
