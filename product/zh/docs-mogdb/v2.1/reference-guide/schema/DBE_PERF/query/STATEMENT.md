---
title: STATEMENT
summary: STATEMENT
author: Guo Huan
date: 2021-04-19
---

# STATEMENT

获得当前节点的执行语句（归一化SQL）的信息。查询视图必须具有sysadmin权限或者monitor admin权限。数据库主节点上可以看到此数据库主节点接收到的归一化的SQL的全量统计信息（包含数据库节点）；数据库节点上仅可看到归一化的SQL的此节点执行的统计信息。

**表 1** STATEMENT字段

| **名称**            | **类型**                 | **描述**                                                     |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| node_name           | name                     | 数据库进程名称。                                             |
| node_id             | integer                  | 节点的ID。                                                   |
| user_name           | name                     | 用户名称。                                                   |
| user_id             | oid                      | 用户OID。                                                    |
| unique_sql_id       | bigint                   | 归一化的SQL ID。                                             |
| query               | text                     | 归一化的SQL。                                                |
| n_calls             | bigint                   | 调用次数。                                                   |
| min_elapse_time     | bigint                   | SQL在内核内的最小运行时间（单位: 微秒）。                    |
| max_elapse_time     | bigint                   | SQL在内核内的最大运行时间（单位: 微秒）。                    |
| total_elapse_time   | bigint                   | SQL在内核内的总运行时间（单位: 微秒）。                      |
| n_returned_rows     | bigint                   | SELECT返回的结果集行数。                                     |
| n_tuples_fetched    | bigint                   | 随机扫描行。                                                 |
| n_tuples_returned   | bigint                   | 顺序扫描行。                                                 |
| n_tuples_inserted   | bigint                   | 插入行。                                                     |
| n_tuples_updated    | bigint                   | 更新行。                                                     |
| n_tuples_deleted    | bigint                   | 删除行。                                                     |
| n_blocks_fetched    | bigint                   | buffer的块访问次数。                                         |
| n_blocks_hit        | bigint                   | buffer的块命中次数。                                         |
| n_soft_parse        | bigint                   | 软解析次数，n_soft_parse + n_hard_parse可能大于n_calls，因为子查询未计入n_calls。 |
| n_hard_parse        | bigint                   | 硬解析次数，n_soft_parse + n_hard_parse可能大于n_calls，因为子查询未计入n_calls。 |
| db_time             | bigint                   | 有效的DB时间花费，多线程将累加（单位: 微秒）。               |
| cpu_time            | bigint                   | CPU时间（单位: 微秒）。                                      |
| execution_time      | bigint                   | 执行器内执行时间（单位: 微秒）。                             |
| parse_time          | bigint                   | SQL解析时间（单位: 微秒）。                                  |
| plan_time           | bigint                   | SQL生成计划时间（单位: 微秒）。                              |
| rewrite_time        | bigint                   | SQL重写时间（单位: 微秒）。                                  |
| pl_execution_time   | bigint                   | plpgsql上的执行时间（单位: 微秒）。                          |
| pl_compilation_time | bigint                   | plpgsql上的编译时间（单位: 微秒）。                          |
| net_send_time       | bigint                   | 网络上的时间花费（单位: 微秒）。                             |
| data_io_time        | bigint                   | IO上的时间花费（单位: 微秒）。                               |
| sort_count          | bigint                   | 排序执行的次数。                                             |
| sort_time           | bigint                   | 排序执行的时间（单位: 微秒）。                               |
| sort_mem_used       | bigint                   | 排序过程中使用的work memory大小（单位: KB）。                |
| sort_spill_count    | bigint                   | 排序过程中，若发生落盘，写文件的次数。                       |
| sort_spill_size     | bigint                   | 排序过程中，若发生落盘，使用的文件大小（单位: KB）。         |
| hash_count          | bigint                   | hash执行的次数。                                             |
| hash_time           | bigint                   | hash执行的时间（单位: 微秒）。                               |
| hash_mem_used       | bigint                   | hash过程中使用的work memory大小（单位: KB）。                |
| hash_spill_count    | bigint                   | hash过程中，若发生落盘，写文件的次数。                       |
| hash_spill_size     | bigint                   | hash过程中，若发生落盘，使用的文件大小（单位: KB）。         |
| last_updated        | timestamp with time zone | 最后一次更新该语句的时间。                                   |
