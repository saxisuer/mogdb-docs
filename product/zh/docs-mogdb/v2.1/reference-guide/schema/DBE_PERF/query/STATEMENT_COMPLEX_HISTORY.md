---
title: STATEMENT_COMPLEX_HISTORY
summary: STATEMENT_COMPLEX_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COMPLEX_HISTORY

STATEMENT_COMPLEX_HISTORY视图显示在数据库主节点上执行作业结束后的负载管理记录。此视图的数据直接从系统表GS_WLM_SESSION_QUERY_INFO_ALL获取。具体的字段请参考[附录-表5](../../../../reference-guide/schema/DBE_PERF/appendices/table-5.md)。
