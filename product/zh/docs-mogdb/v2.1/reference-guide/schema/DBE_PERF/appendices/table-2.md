---
title: 表 2
summary: 表 2
author: Guo Huan
date: 2021-04-19
---

**表 2** 轻量级锁等待事件列表<a id="表2"> </a>

| wait_event类型                           | 类型描述                                                     |
| :--------------------------------------- | :----------------------------------------------------------- |
| ShmemIndexLock                           | 用于保护共享内存中的主索引哈希表。                           |
| OidGenLock                               | 用于避免不同线程产生相同的OID。                              |
| XidGenLock                               | 用于避免两个事务获得相同的xid。                              |
| ProcArrayLock                            | 用于避免并发访问或修改ProcArray共享数组。                    |
| SInvalReadLock                           | 用于避免与清理失效消息并发执行。                             |
| SInvalWriteLock                          | 用于避免与其它写失效消息、清理失效消息并发执行。             |
| WALInsertLock                            | 用于避免与其它WAL插入操作并发执行。                          |
| WALWriteLock                             | 用于避免并发WAL写盘。                                        |
| ControlFileLock                          | 用于避免pg_control文件的读写并发、写写并发。                 |
| CheckpointLock                           | 用于避免多个checkpoint并发执行。                             |
| CLogControlLock                          | 用于避免并发访问或者修改Clog控制数据结构。                   |
| SubtransControlLock                      | 用于避免并发访问或者修改子事务控制数据结构。                 |
| MultiXactGenLock                         | 用于串行分配唯一MultiXact id。                               |
| MultiXactOffsetControlLock               | 用于避免对pg_multixact/offset的写写并发和读写并发。          |
| MultiXactMemberControlLock               | 用于避免对pg_multixact/members的写写并发和读写并发。         |
| RelCacheInitLock                         | 用于失效消息场景对init文件进行操作时加锁。                   |
| CheckpointerCommLock                     | 用于向checkpointer发起文件刷盘请求场景，需要串行的向请求队列插入请求结构。 |
| TwoPhaseStateLock                        | 用于避免并发访问或者修改两阶段信息共享数组。                 |
| TablespaceCreateLock                     | 用于确定tablespace是否已经存在。                             |
| BtreeVacuumLock                          | 用于防止vacuum清理B-tree中还在使用的页面。                   |
| AutovacuumLock                           | 用于串行化访问autovacuum worker数组。                        |
| AutovacuumScheduleLock                   | 用于串行化分配需要vacuum的table。                            |
| AutoanalyzeLock                          | 用于获取和释放允许执行Autoanalyze的任务资源。                |
| SyncScanLock                             | 用于确定heap扫描时某个relfilenode的起始位置。                |
| NodeTableLock                            | 用于保护存放数据库节点信息的共享结构。                       |
| PoolerLock                               | 用于保证两个线程不会同时从连接池里取到相同的连接。           |
| RelationMappingLock                      | 用于等待更新系统表到存储位置之间映射的文件。                 |
| AsyncCtlLock                             | 用于避免并发访问或者修改共享通知状态。                       |
| AsyncQueueLock                           | 用于避免并发访问或者修改共享通知信息队列。                   |
| SerializableXactHashLock                 | 用于避免对于可串行事务共享结构的写写并发和读写并发。         |
| SerializableFinishedListLock             | 用于避免对于已完成可串行事务共享链表的写写并发和读写并发。   |
| SerializablePredicateLockListLock        | 用于保护对于可串行事务持有的锁链表。                         |
| OldSerXidLock                            | 用于保护记录冲突可串行事务的结构。                           |
| FileStatLock                             | 用于保护存储统计文件信息的数据结构。                         |
| SyncRepLock                              | 用于在主备复制时保护xlog同步信息。                           |
| DataSyncRepLock                          | 用于在主备复制时保护数据页同步信息。                         |
| CStoreColspaceCacheLock                  | 用于保护列存表的CU空间分配。                                 |
| CStoreCUCacheSweepLock                   | 用于列存CU Cache循环淘汰。                                   |
| MetaCacheSweepLock                       | 用于元数据循环淘汰。                                         |
| ExtensionConnectorLibLock                | 用于初始化ODBC连接场景，在加载与卸载特定动态库时进行加锁。   |
| SearchServerLibLock                      | 用于GPU加速场景初始化加载特定动态库时，对读文件操作进行加锁。 |
| LsnXlogChkFileLock                       | 用于串行更新特定结构中记录的主备机的xlog flush位置点。       |
| ReplicationSlotAllocationLock            | 用于主备复制时保护主机端的流复制槽的分配。                   |
| ReplicationSlotControlLock               | 用于主备复制时避免并发更新流复制槽状态。                     |
| ResourcePoolHashLock                     | 用于避免并发访问或者修改资源池哈希表。                       |
| WorkloadStatHashLock                     | 用于避免并发访问或者修改包含数据库主节点的SQL请求构成的哈希表。 |
| WorkloadIoStatHashLock                   | 用于避免并发访问或者修改用于统计当前数据库节点的IO信息的哈希表。 |
| WorkloadCGroupHashLock                   | 用于避免并发访问或者修改Cgroup信息构成的哈希表。             |
| OBSGetPathLock                           | 用于避免对obs路径的写写并发和读写并发。                      |
| WorkloadUserInfoLock                     | 用于避免并发访问或修改负载管理的用户信息哈希表。             |
| WorkloadRecordLock                       | 用于避免并发访问或修改在内存自适应管理时对数据库主节点收到请求构成的哈希表。 |
| WorkloadIOUtilLock                       | 用于保护记录iostat，CPU等负载信息的结构。                    |
| WorkloadNodeGroupLock                    | 用于避免并发访问或者修改内存中的nodegroup信息构成的哈希表。  |
| JobShmemLock                             | 用于定时任务功能中保护定时读取的全局变量。                   |
| OBSRuntimeLock                           | 用于获取环境变量，如GASSHOME。                               |
| LLVMDumpIRLock                           | 用于导出动态生成函数所对应的汇编语言。                       |
| LLVMParseIRLock                          | 用于在查询开始处从IR文件中编译并解析已写好的IR函数。         |
| CriticalCacheBuildLock                   | 用于从共享或者本地缓存初始化文件中加载cache的场景。          |
| WaitCountHashLock                        | 用于保护用户语句计数功能场景中的共享结构。                   |
| BufMappingLock                           | 用于保护对共享缓冲映射表的操作。                             |
| LockMgrLock                              | 用于保护常规锁结构信息。                                     |
| PredicateLockMgrLock                     | 用于保护可串行事务锁结构信息。                               |
| OperatorRealTLock                        | 用于避免并发访问或者修改记录算子级实时数据的全局结构。       |
| OperatorHistLock                         | 用于避免并发访问或者修改记录算子级历史数据的全局结构。       |
| SessionRealTLock                         | 用于避免并发访问或者修改记录query级实时数据的全局结构。      |
| SessionHistLock                          | 用于避免并发访问或者修改记录query级历史数据的全局结构。      |
| CacheSlotMappingLock                     | 用于保护CU Cache全局信息。                                   |
| BarrierLock                              | 用于保证当前只有一个线程在创建Barrier。                      |
| dummyServerInfoCacheLock                 | 用于保护缓存加速MogDB连接信息的全局哈希表。                  |
| RPNumberLock                             | 用于加速MogDB的数据库节点对正在执行计划的任务线程的计数。    |
| ClusterRPLock                            | 用于加速MogDB的CCN中维护的MogDB负载数据的并发存取控制。      |
| CBMParseXlogLock                         | Cbm 解析xlog时的保护锁                                       |
| RelfilenodeReuseLock                     | 避免错误地取消已重用的列属性文件的链接。                     |
| RcvWriteLock                             | 防止并发调用WalDataRcvWrite。                                |
| PercentileLock                           | 用于保护全局PercentileBuffer                                 |
| CSNBufMappingLock                        | 保护csn页面                                                  |
| UniqueSQLMappingLock                     | 用于保护uniquesql hash table                                 |
| DelayDDLLock                             | 防止并发ddl。                                                |
| CLOG Ctl                                 | 用于避免并发访问或者修改Clog控制数据结构                     |
| Async Ctl                                | 保护Async buffer                                             |
| MultiXactOffset Ctl                      | 保护MultiXact offet的slru buffer                             |
| MultiXactMember Ctl                      | 保护MultiXact member的slrubuffer                             |
| OldSerXid SLRU Ctl                       | 保护old xids的slru buffer                                    |
| ReplicationSlotLock                      | 用于保护ReplicationSlot                                      |
| PGPROCLock                               | 用于保护pgproc                                               |
| MetaCacheLock                            | 用于保护MetaCache                                            |
| DataCacheLock                            | 用于保护datacache                                            |
| InstrUserLock                            | 用于保护InstrUserHTAB。                                      |
| BadBlockStatHashLock                     | 用于保护global_bad_block_stat hash表。                       |
| BufFreelistLock                          | 用于保证共享缓冲区空闲列表操作的原子性。                     |
| CUSlotListLock                           | 用于控制列存缓冲区槽位的并发操作。                           |
| AddinShmemInitLock                       | 保护共享内存对象的初始化。                                   |
| AlterPortLock                            | 保护协调节点更改注册端口号的操作。                           |
| FdwPartitionCaheLock                     | HDFS分区表缓冲区的管理锁。                                   |
| DfsConnectorCacheLock                    | DFSConnector缓冲区的管理锁。                                 |
| DfsSpaceCacheLock                        | HDFS表空间管理缓冲区的管理锁。                               |
| FullBuildXlogCopyStartPtrLock            | 用于保护全量Build中Xlog拷贝的操作。                          |
| DfsUserLoginLock                         | 用于HDFS用户登录以及认证。                                   |
| LogicalReplicationSlotPersistentDataLock | 用于保护逻辑复制过程中复制槽位的数据。                       |
| WorkloadSessionInfoLock                  | 保护负载管理session info内存hash表访问。                     |
| InstrWorkloadLock                        | 保护负载管理统计信息的内存hash表访问。                       |
| PgfdwLock                                | 用于管理实例向Foreign server建立连接。                       |
| InstanceTimeLock                         | 用于获取实例中会话的时间信息。                               |
| XlogRemoveSegLock                        | 保护Xlog段文件的回收操作。                                   |
| DnUsedSpaceHashLock                      | 用于更新会话对应的空间使用信息。                             |
| CsnMinLock                               | 用于计算CSNmin。                                             |
| GPCCommitLock                            | 用于保护全局Plan Cache hash表的添加操作。                    |
| GPCClearLock                             | 用于保护全局Plan Cache hash表的清除操作。                    |
| GPCTimelineLock                          | 用于保护全局Plan Cache hash表检查Timeline的操作。            |
| TsTagsCacheLock                          | 用于时序tag缓存管理。                                        |
| InstanceRealTLock                        | 用于保护共享实例统计信息hash表的更新操作。                   |
| CLogBufMappingLock                       | 用于提交日志缓存管理。                                       |
| GPCMappingLock                           | 用于全局Plan Cache缓存管理。                                 |
| GPCPrepareMappingLock                    | 用于全局Plan Cache缓存管理。                                 |
| BufferIOLock                             | 保护共享缓冲区页面的IO操作。                                 |
| BufferContentLock                        | 保护共享缓冲区页面内容的读取、修改。                         |
| CSNLOG Ctl                               | 用于CSN日志管理。                                            |
| DoubleWriteLock                          | 用于双写的管理操作。                                         |
| RowPageReplicationLock                   | 用于管理行存储的数据页复制。                                 |
| extension                                | 其他轻量锁。                                                 |
