---
title: 表 3
summary: 表 3
author: Guo Huan
date: 2021-04-19
---

**表 3** IO等待事件列表<a id="表3"> </a>

| wait_event类型             | 类型描述                                                     |
| :------------------------- | :----------------------------------------------------------- |
| BufFileRead                | 从临时文件中读取数据到指定buffer。                           |
| BufFileWrite               | 向临时文件中写入指定buffer中的内容。                         |
| ControlFileRead            | 读取pg_control文件。主要在数据库启动、执行checkpoint和主备校验过程中发生。 |
| ControlFileSync            | 将pg_control文件持久化到磁盘。数据库初始化时发生。           |
| ControlFileSyncUpdate      | 将pg_control文件持久化到磁盘。主要在数据库启动、执行checkpoint和主备校验过程中发生。 |
| ControlFileWrite           | 写入pg_control文件。数据库初始化时发生。                     |
| ControlFileWriteUpdate     | 更新pg_control文件。主要在数据库启动、执行checkpoint和主备校验过程中发生。 |
| CopyFileRead               | copy文件时读取文件内容。                                     |
| CopyFileWrite              | copy文件时写入文件内容。                                     |
| DataFileExtend             | 扩展文件时向文件写入内容。                                   |
| DataFileFlush              | 将表数据文件持久化到磁盘                                     |
| DataFileImmediateSync      | 将表数据文件立即持久化到磁盘。                               |
| DataFilePrefetch           | 异步读取表数据文件。                                         |
| DataFileRead               | 同步读取表数据文件。                                         |
| DataFileSync               | 将表数据文件的修改持久化到磁盘。                             |
| DataFileTruncate           | 表数据文件truncate。                                         |
| DataFileWrite              | 向表数据文件写入内容。                                       |
| LockFileAddToDataDirRead   | 读取"postmaster.pid"文件。                                   |
| LockFileAddToDataDirSync   | 将"postmaster.pid"内容持久化到磁盘。                         |
| LockFileAddToDataDirWrite  | 将pid信息写到"postmaster.pid"文件。                          |
| LockFileCreateRead         | 读取LockFile文件"%s.lock"。                                  |
| LockFileCreateSync         | 将LockFile文件"%s.lock"内容持久化到磁盘。                    |
| LockFileCreateWRITE        | 将pid信息写到LockFile文件"%s.lock"。                         |
| RelationMapRead            | 读取系统表到存储位置之间的映射文件                           |
| RelationMapSync            | 将系统表到存储位置之间的映射文件持久化到磁盘。               |
| RelationMapWrite           | 写入系统表到存储位置之间的映射文件。                         |
| ReplicationSlotRead        | 读取流复制槽文件。重新启动时发生。                           |
| ReplicationSlotRestoreSync | 将流复制槽文件持久化到磁盘。重新启动时发生。                 |
| ReplicationSlotSync        | checkpoint时将流复制槽临时文件持久化到磁盘。                 |
| ReplicationSlotWrite       | checkpoint时写流复制槽临时文件。                             |
| SLRUFlushSync              | 将pg_clog、pg_subtrans和pg_multixact文件持久化到磁盘。主要在执行checkpoint和数据库停机时发生。 |
| SLRURead                   | 读取pg_clog、pg_subtrans和pg_multixact文件。                 |
| SLRUSync                   | 将脏页写入文件pg_clog、pg_subtrans和pg_multixact并持久化到磁盘。主要在执行checkpoint和数据库停机时发生。 |
| SLRUWrite                  | 写入pg_clog、pg_subtrans和pg_multixact文件。                 |
| TimelineHistoryRead        | 读取timeline history文件。在数据库启动时发生。               |
| TimelineHistorySync        | 将timeline history文件持久化到磁盘。在数据库启动时发生。     |
| TimelineHistoryWrite       | 写入timeline history文件。在数据库启动时发生。               |
| TwophaseFileRead           | 读取pg_twophase文件。在两阶段事务提交、两阶段事务恢复时发生。 |
| TwophaseFileSync           | 将pg_twophase文件持久化到磁盘。在两阶段事务提交、两阶段事务恢复时发生。 |
| TwophaseFileWrite          | 写入pg_twophase文件。在两阶段事务提交、两阶段事务恢复时发生。 |
| WALBootstrapSync           | 将初始化的WAL文件持久化到磁盘。在数据库初始化发生。          |
| WALBootstrapWrite          | 写入初始化的WAL文件。在数据库初始化发生。                    |
| WALCopyRead                | 读取已存在的WAL文件并进行复制时产生的读操作。在执行归档恢复完后发生。 |
| WALCopySync                | 将复制的WAL文件持久化到磁盘。在执行归档恢复完后发生。        |
| WALCopyWrite               | 读取已存在WAL文件并进行复制时产生的写操作。在执行归档恢复完后发生。 |
| WALInitSync                | 将新初始化的WAL文件持久化磁盘。在日志回收或写日志时发生。    |
| WALInitWrite               | 将新创建的WAL文件初始化为0。在日志回收或写日志时发生。       |
| WALRead                    | 从xlog日志读取数据。两阶段文件redo相关的操作产生。           |
| WALSyncMethodAssign        | 将当前打开的所有WAL文件持久化到磁盘。                        |
| WALWrite                   | 写入WAL文件。                                                |
| WALBufferAccess            | WAL Buffer访问（出于性能考虑，内核代码里只统计访问次数，未统计其访问耗时）。 |
| WALBufferFull              | WAL Buffer满时，写wal文件相关的处理。                        |
| DoubleWriteFileRead        | 双写 文件读取。                                              |
| DoubleWriteFileSync        | 双写 文件强制刷盘。                                          |
| DoubleWriteFileWrite       | 双写 文件写入。                                              |
| PredoProcessPending        | 并行日志回放中当前记录回放等待其它记录回放完成。             |
| PredoApply                 | 并行日志回放中等待当前工作线程等待其他线程回放至本线程LSN。  |
| DisableConnectFileRead     | HA锁分片逻辑文件读取。                                       |
| DisableConnectFileSync     | HA锁分片逻辑文件强制刷盘。                                   |
| DisableConnectFileWrite    | HA锁分片逻辑文件写入。                                       |
