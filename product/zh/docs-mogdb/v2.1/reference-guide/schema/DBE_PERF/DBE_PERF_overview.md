---
title: 概述
summary: 概述
author: Guo Huan
date: 2021-04-19
---

# 概述

DBE_PERF Schema内视图主要用来诊断性能问题，也是WDR Snapshot的数据来源。数据库安装后，默认只有初始用户具有模式dbe_perf的权限。若是由旧版本升级而来，为保持权限的前向兼容，模式dbe_perf的权限与旧版本保持一致。从OS、Instance、Memory等多个维度划分组织视图，并且符合如下命名规范:

- GLOBAL_开头的视图，代表从数据库节点请求数据，并将数据追加对外返回，不会处理数据。
- SUMMARY_开头的视图，代表是将MogDB内的数据概述，多数情况下是返回数据库节点（有时只有数据库主节点的）的数据，会对数据进行加工和汇聚。
- 非这两者开头的视图，一般代表本地视图，不会向其它数据库节点请求数据。

<br/>

## 为其它用户赋予dbe_perf访问权限

如果查询dbe_perf模式下的视图遇到以下报错，则表示权限不足：

```sql
mogdb=> select * from dbe_perf.statement;
permission denied for schema dbe_perf
LINE 1: select * from dbe_perf.statement
                      ^
mogdb=> select * from dbe_perf.statement;
ERROR:  only system/monitor admin can query unique sql view
```

解决方法如下：

使用omm用户将需要访问dbe_perf模式的用户更改为监控管理员，如：

```sql
mogdb=# ALTER USER mogdb_usr MONADMIN;
ALTER ROLE
```

监控管理员是指具有MONADMIN属性的帐户，具有查看dbe_perf模式下视图和函数的权限，亦可以对dbe_perf模式的对象权限进行授予或收回。有关用户及权限的更多信息请参考[管理用户及权限](../../../security-guide/security/2-managing-users-and-their-permissions.md)。
