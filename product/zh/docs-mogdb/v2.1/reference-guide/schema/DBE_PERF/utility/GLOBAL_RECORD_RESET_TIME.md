---
title: GLOBAL_RECORD_RESET_TIME
summary: GLOBAL_RECORD_RESET_TIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_RECORD_RESET_TIME

GLOBAL_RECORD_RESET_TIME用于重置（重启、主备倒换、数据库删除）汇聚MogDB统计信息时间。

**表 1** GLOBAL_RECORD_RESET_TIME字段

| 名称       | 类型                     | 描述             |
| :--------- | :----------------------- | :--------------- |
| node_name  | text                     | 数据库进程名称。 |
| reset_time | timestamp with time zone | 重置时间点。     |
