---
title: STAT_BAD_BLOCK
summary: STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# STAT_BAD_BLOCK

获得当前节点表、索引等文件的读取失败信息。

**表 1** STAT_BAD_BLOCK字段

| **名称**     | **类型**                 | **描述**               |
| :----------- | :----------------------- | :--------------------- |
| nodename     | text                     | 数据库进程名称。       |
| databaseid   | integer                  | database的oid。        |
| tablespaceid | integer                  | tablespace的oid。      |
| relfilenode  | integer                  | relation的file node。  |
| bucketid     | smallint                 | 一致性hash bucket ID。 |
| forknum      | integer                  | fork编号。             |
| error_count  | integer                  | error的数量。          |
| first_time   | timestamp with time zone | 坏块第一次出现的时间。 |
| last_time    | timestamp with time zone | 坏块最后出现的时间。   |
