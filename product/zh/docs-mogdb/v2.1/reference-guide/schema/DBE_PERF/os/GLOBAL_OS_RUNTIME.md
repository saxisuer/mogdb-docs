---
title: GLOBAL_OS_RUNTIME
summary: GLOBAL_OS_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_OS_RUNTIME

提供MogDB中所有正常节点下的操作系统运行状态信息。

**表 1** GLOBAL_OS_RUNTIME字段

| **名称**   | **类型** | **描述**                           |
| :--------- | :------- | :--------------------------------- |
| node_name  | name     | 数据库进程名称。                   |
| id         | integer  | 编号。                             |
| name       | text     | 操作系统运行状态名称。             |
| value      | numeric  | 操作系统运行状态值。               |
| comments   | text     | 操作系统运行状态注释。             |
| cumulative | boolean  | 操作系统运行状态的值是否为累加值。 |
