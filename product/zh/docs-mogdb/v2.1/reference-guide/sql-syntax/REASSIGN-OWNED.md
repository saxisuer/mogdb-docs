---
title: REASSIGN OWNED
summary: REASSIGN OWNED
author: Zhang Cuiping
date: 2021-05-18
---

# REASSIGN OWNED

## 功能描述

修改数据库对象的属主。

REASSIGN OWNED要求系统将所有old_roles拥有的数据库对象的属主更改为new_role。

## 注意事项

- REASSIGN OWNED常用于在删除角色之前的准备工作。
- 执行REASSIGN OWNED需要有原角色和目标角色上的权限。

## 语法格式

```ebnf+diagram
ReassignOwned ::= REASSIGN OWNED BY old_role [, ...] TO new_role;
```

## 参数说明

- **old_role**

  旧属主的角色名。

- **new_role**

  将要成为这些对象属主的新角色的名称。

## 示例

无。
