---
title: DROP CLIENT MASTER KEY
summary: DROP CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP CLIENT MASTER KEY

## 功能描述

删除一个客户端加密主密钥(cmk)。

## 注意事项

- 只有客户端加密主密钥所有者或者被授予了DROP权限的用户有权限执行命令，系统管理员默认拥有此权限。
- 该命令只能删除数据库中系统表记录的元数据信息，不能真正删除CMK密钥文件，需要通过KeyTool工具才能删除CMK密钥文件。

## 语法格式

```ebnf+diagram
DropClientMasterKey ::= DROP CLIENT MASTER KEY [ IF EXISTS ] client_master_key_name [CASCADE];
```

## 参数说明

- **IF EXISTS**

  如果指定的客户端加密主密钥不存在，则发出一个notice而不是抛出一个错误。

- **client_master_key_name**

  要删除的客户端加密主密钥名称。

  取值范围: 字符串，已存在的客户端加密主密钥名称。

- **CASCADE**

  - **CASCADE**：表示允许级联删除依赖于客户端加密主密钥的对象。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 在执行本语法的生命周期中，同时需要客户端和服务端更改状态，发生异常时可能存在服务端已删除密钥信息，但客户端未删除密钥文件的情况。 此时，客户端并不会在执行下一条语法的生命周期中，检查是否有期望被删除但却因发生异常而未被删除的密钥文件，而是需要用户定期检查密钥文件夹，对未被使用的密钥文件进行确认并处理。

## **示例**

```sql
--删除客户端加密主密钥对象。
mogdb=> DROP CLIENT MASTER KEY ImgCMK CASCADE;
NOTICE:  drop cascades to column setting: imgcek
DROP GLOBAL SETTING
```
