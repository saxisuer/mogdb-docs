---
title: PURGE
summary: PURGE
author: Zhang Cuiping
date: 2021-11-01
---

# PURGE

## 功能描述

使用PURGE语句可以实现如下功能：

- 从回收站中清理表或索引，并释放对象相关的全部空间。
- 清理回收站。
- 清理回收站中指定表空间的对象

## 注意事项

- 清除（PURGE）操作支持：表（PURGE TABLE）、索引（PURGE INDEX）、回收站（PURGE RECYCLEBIN）、表空间（PURGE TABLESPACE）。
- 执行PURGE操作的权限要求如下：
  - PURGE TABLE：用户必须是表的所有者，且用户必须拥有表所在模式的USAGE权限，系统管理员默认拥有此权限。
  - PURGE INDEX：用户必须是索引的所有者，用户必须拥有索引所在模式的USAGE权限，系统管理员默认拥有此权限。
  - PURGE TABLESPACE：用户必须是表空间的所有者，用户必须拥有对象所在模式的USAGE权限，且普通用户只允许删除回收站中指定表空间下所有者为自己的表，系统管理员默认可以删除回收站中指定表空间下所有的对象。
  - PURGE RECYCLEBIN：普通用户只能清理回收站中当前用户拥有的对象，且用户必须拥有对象所在模式的USAGE权限，系统管理员默认可以清理回收站所有对象。

## 语法格式

```ebnf+diagram
Purge ::= PURGE { TABLE [schema_name'.']table_name
        | INDEX index_name
        | RECYCLEBIN
      };
```

## 参数说明

- _[ **schema_name.** ]_

  模式名。

- TABLE *[ schema_name. ] table_name*

  清空回收站中指定的表。

- INDEX *index_name*

  清空回收站中指定的索引。

- RECYCLEBIN

  清空回收站中的对象。

## 示例

```
-- 创建表空间reason_table_space
mogdb=#  CREATE TABLESPACE REASON_TABLE_SPACE1 owner tpcds RELATIVE location 'tablespace/tsp_reason1';
-- 在表空间创建表tpcds.reason_t1
mogdb=#  CREATE TABLE tpcds.reason_t1
 (
  r_reason_sk    integer,
  r_reason_id    character(16),
  r_reason_desc  character(100)
  ) tablespace reason_table_space1;
-- 在表空间创建表tpcds.reason_t2
mogdb=#  CREATE TABLE tpcds.reason_t2
 (
  r_reason_sk    integer,
  r_reason_id    character(16),
  r_reason_desc  character(100)
  ) tablespace reason_table_space1;
-- 在表空间创建表tpcds.reason_t3
mogdb=#  CREATE TABLE tpcds.reason_t3
 (
  r_reason_sk    integer,
  r_reason_id    character(16),
  r_reason_desc  character(100)
  ) tablespace reason_table_space1;
mogdb=#  CREATE TABLE tpcds.reason_t4
 (
  r_reason_sk    integer,
  r_reason_id    character(16),
  r_reason_desc  character(100)
  );
mogdb=#  DROP TABLE tpcds.reason_t1;
mogdb=#  DROP TABLE tpcds.reason_t2;
mogdb=#  DROP TABLE tpcds.reason_t3;
mogdb=#  DROP TABLE tpcds.reason_t4;
--查看回收站
mogdb=#  SELECT rcyname,rcyoriginname,rcytablespace FROM GS_RECYCLEBIN;
        rcyname        | rcyoriginname | rcytablespace
-----------------------+---------------+---------------
 BIN$16409$2CEE988==$0 | reason_t1     |         16408
 BIN$16412$2CF2188==$0 | reason_t2     |         16408
 BIN$16415$2CF2EC8==$0 | reason_t3     |         16408
 BIN$16418$2CF3EC8==$0 | reason_t4     |             0
(4 rows)
--清空表
mogdb=#  PURGE TABLE tpcds.reason_t1;
mogdb=#  SELECT rcyname,rcyoriginname,rcytablespace FROM GS_RECYCLEBIN;
        rcyname        | rcyoriginname | rcytablespace
-----------------------+---------------+---------------
 BIN$16412$2CF2188==$0 | reason_t2     |         16408
 BIN$16415$2CF2EC8==$0 | reason_t3     |         16408
 BIN$16418$2CF3EC8==$0 | reason_t4     |             0
(3 rows)
```
