---
title: DROP TABLE
summary: DROP TABLE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TABLE

## 功能描述

删除指定的表。

## 注意事项

- DROP TABLE会强制删除指定的表，删除表后，依赖该表的索引会被删除，而使用到该表的函数和存储过程将无法执行。删除分区表，会同时删除分区表中的所有分区。
- 只有表的所有者或者被授予了表的DROP权限的用户才能执行DROP TABLE，系统管理员默认拥有该权限。

## 语法格式

```ebnf+diagram
DropTable ::= DROP TABLE [ IF EXISTS ]
    { [schema'.']table_name } [, ...] [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的表不存在，则发出一个notice而不是抛出一个错误。

- **schema**

  模式名称。

- **table_name**

  表名称。

- **CASCADE | RESTRICT**

  - CASCADE：级联删除依赖于表的对象（比如视图）。
  - RESTRICT（缺省项）：如果存在依赖对象，则拒绝删除该表。这个是缺省。

## 示例

请参考CREATE TABLE的示例。

## 相关链接

[ALTER TABLE](ALTER-TABLE.md)，[CREATE TABLE](CREATE-TABLE.md)
