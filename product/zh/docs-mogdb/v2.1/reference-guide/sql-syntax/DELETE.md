---
title: DELETE
summary: DELETE
author: GUO HUAN
date: 2022-04-15
---

# DELETE

## 功能描述

DELETE从指定的表里删除满足WHERE子句的行。如果WHERE子句不存在，将删除表中所有行，结果只保留表结构。

## 注意事项

- 要删除表中的数据，用户必须对它有DELETE权限。同样也必须有USING子句引用的表以及condition上读取的表的SELECT权限。
- 对于列存表，暂时不支持RETURNING子句。

## 语法格式

```ebnf+diagram
Delete ::= [ WITH [ RECURSIVE ] with_query [, ...] ]
DELETE [/*+ plan_hint */] [ FROM ] [ ONLY ] table_name [ * ] [ [ AS ] alias ]
    [ USING using_list ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING { * | { output_expr [ [ AS ] output_name ] } [, ...] } ];
```

## 参数说明

- **WITH [ RECURSIVE ] with_query [, …]**

  用于声明一个或多个可以在主查询中通过名称引用的子查询，相当于临时表。

  如果声明了RECURSIVE，那么允许SELECT子查询通过名称引用它自己。

  其中with_query的详细格式为：

  ```ebnf+diagram
  with_query_name ::= with_query_name [ ( column_name [, ...] ) ] AS
  ( {select | values | insert | update | delete} )
  ```

  - with_query_name指定子查询生成的结果集名称，在查询中可使用该名称访问子查询的结果集。
  - column_name指定子查询结果集中显示的列名。
  - 每个子查询可以是SELECT，VALUES，INSERT，UPDATE或DELETE语句。

- **plan_hint子句**

  以`/*+ */`的形式在DELETE关键字后，用于对DELETE对应的语句块生成的计划进行hint调优，详细用法请参见章节[使用Plan Hint进行调优](../../performance-tuning/2-sql/9-hint-based-tuning.md)。每条语句中只有第一个`/*+ plan_hint */`注释块会作为hint生效，里面可以写多条hint。

- **ONLY**

  如果指定ONLY则只有该表被删除；如果没有声明，则该表和它的所有子表将都被删除。

- **table_name**

  目标表的名称（可以有模式修饰）。

  取值范围: 已存在的表名。

- **alias**

  目标表的别名。

  取值范围: 字符串，符合标识符命名规范。

- **using_list**

  using子句。

- **condition**

  一个返回Boolean值的表达式，用于判断哪些行需要被删除。

- **WHERE CURRENT OF cursor_name**

  当前不支持，仅保留语法接口。

- **output_expr**

  DELETE命令删除行之后计算输出结果的表达式。该表达式可以使用表的任意字段。可以使用*返回被删除行的所有字段。

- **output_name**

  一个字段的输出名称。

  取值范围: 字符串，符合标识符命名规范。

## 示例

```sql
--创建表tpcds.customer_address_bak。
mogdb=# CREATE TABLE tpcds.customer_address_bak AS TABLE tpcds.customer_address;

--删除tpcds.customer_address_bak中ca_address_sk小于14888的职员。
mogdb=# DELETE FROM tpcds.customer_address_bak WHERE ca_address_sk < 14888;

--删除tpcds.customer_address_bak中所有数据。
mogdb=# DELETE FROM tpcds.customer_address_bak;

--删除tpcds.customer_address_bak表。
mogdb=# DROP TABLE tpcds.customer_address_bak;
```

## 优化建议

- delete

  如果要删除表中的所有记录，建议使用truncate语法。
