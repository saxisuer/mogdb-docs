---
title: CALL
summary: CALL
author: Zhang Cuiping
date: 2021-05-10
---

# CALL

## 功能描述

使用CALL命令可以调用已定义的函数和存储过程。

## 注意事项

无。

## 语法格式

```ebnf+diagram
Call ::= CALL [schema'.'|package'.'] {func_name| procedure_name} ( param_expr );
```

## 参数说明

- **schema**

  函数或存储过程所在的模式名称。

- **package**

    函数或存储过程所在的package名称。

- **func_name**

  所调用函数或存储过程的名称。

  取值范围: 已存在的函数名称。

- **param_expr**

  参数列表可以用符号”:=“或者”=&gt;“将参数名和参数值隔开，这种方法的好处是参数可以以任意顺序排列。若参数列表中仅出现参数值，则参数值的排列顺序必须和函数或存储过程定义时的相同。

  取值范围: 已存在的函数参数名称或存储过程参数名称。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  > 参数可以包含入参（参数名和类型之间指定“IN”关键字）和出参（参数名和类型之间指定“OUT”关键字），使用CALL命令调用函数或存储过程时，对于非重载的函数，参数列表必须包含出参，出参可以传入一个变量或者任一常量，详见示例。对于重载的package函数，参数列表里可以忽略出参，忽略出参时可能会导致函数找不到。包含出参时，出参只能是常量。

## 示例

```sql
--创建一个函数func_add_sql，计算两个整数的和，并返回结果。
mogdb=# CREATE FUNCTION func_add_sql(num1 integer, num2 integer) RETURN integer
AS
BEGIN
RETURN num1 + num2;
END;
/

--按参数值传递。
mogdb=# CALL func_add_sql(1, 3);

--使用命名标记法传参。
mogdb=# CALL func_add_sql(num1 => 1,num2 => 3);
mogdb=# CALL func_add_sql(num2 := 2, num1 := 3);

--删除函数。
mogdb=# DROP FUNCTION func_add_sql;

--创建带出参的函数。
mogdb=# CREATE FUNCTION func_increment_sql(num1 IN integer, num2 IN integer, res OUT integer)
RETURN integer
AS
BEGIN
res := num1 + num2;
END;
/

--出参传入常量。
mogdb=# CALL func_increment_sql(1,2,1);

--删除函数。
mogdb=# DROP FUNCTION func_increment_sql;
```
