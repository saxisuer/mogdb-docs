---
title: ALTER AGGREGATE
summary: ALTER AGGREGATE
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER AGGREGATE

## 功能描述

修改一个聚合函数的定义。

## 注意事项

要使用 ALTER AGGREGATE ，你必须是该聚合函数的所有者。 要改变一个聚合函数的模式，你必须在新模式上有 CREATE 权限。 要改变所有者，你必须是新所有角色的一个直接或间接成员，并且该角色必须在聚合函数的模式上有 CREATE 权限。（这些限制强制了修改该所有者不会做任何通过删除和重建聚合函数不能做的事情。不过，超级用户可以用任何方法任意更改聚合函数的所属关系）。

## 语法格式

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) RENAME TO new_name;
```

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) OWNER TO new_owner;
```

```ebnf+diagram
AlterAggregate ::= ALTER AGGREGATE name ( argtype [ , ... ] ) SET SCHEMA new_schema;
```

## 参数说明

- **name**

  现有的聚合函数的名称(可以有模式修饰)。

- **argtype**

  聚合函数操作的输入数据类型。要引用一个零参数聚合函数，可以写入*代替输入数据类型列表。

- **new_name**

  聚合函数的新名字。

- **new_owner**

  聚合函数的新所有者。

- **new_schema**

  聚合函数的新模式。

## 示例

把一个接受integer 类型参数的聚合函数myavg重命名为 my_average ：

```sql
ALTER AGGREGATE myavg(integer) RENAME TO my_average;
```

把一个接受integer 类型参数的聚合函数myavg的所有者改为joe ：

```sql
ALTER AGGREGATE myavg(integer) OWNER TO joe;
```

把一个接受integer 类型参数的聚合函数myavg移动到模式myschema里：

```sql
ALTER AGGREGATE myavg(integer) SET SCHEMA myschema;
```

## 兼容性

SQL标准里没有ALTER AGGREGATE语句。
