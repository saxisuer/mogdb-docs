---
title: CREATE DATA SOURCE
summary: CREATE DATA SOURCE
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE DATA SOURCE

## 功能描述

创建一个新的外部数据源对象，该对象用于定义MogDB要连接的目标库信息。

## 注意事项

- Data Source名称在数据库中需唯一，遵循标识符命名规范，长度限制为63字节，过长则会被截断。
- 只有系统管理员或初始用户才有权限创建Data Source对象。且创建该对象的用户为其默认属主。
- 当在OPTIONS中出现password选项时，需要保证MogDB每个节点的`$GAUSSHOME/bin`目录下存在datasource.key.cipher和datasource.key.rand文件，如果不存在这两个文件，请使用`gs_guc`工具生成并使用`gs_ssh`工具发布到openGauss每个节`$GAUSSHOME/bin`目录下。

## 语法格式

```ebnf+diagram
CreateDataSource ::= CREATE DATA SOURCE src_name
    [TYPE 'type_str']
    [VERSION {'version_str' | NULL}]
    [OPTIONS (optname 'optvalue' )];
```

## 参数说明

- **src_name**

  新建Data Source对象的名称，需在数据库内部唯一。

  取值范围: 字符串，要符标识符的命名规范。

- **TYPE**

  新建Data Source对象的类型，可缺省。

  取值范围: 空串或非空字符串。

- **VERSION**

  新建Data Source对象的版本号，可缺省或NULL值。

  取值范围: 空串或非空字符串或NULL。

- **OPTIONS**

  Data Source对象的选项字段，创建时可省略，如若指定，其关键字如下：

  - optname

    选项名称。

    取值范围: dsn， username， password， encoding。不区分大小写

    - dsn  对应odbc配置文件中的DSN
    - username/password对应连接目标库的用户名和密码。

        MogDB在后台会对用户输入的username/password加密以保证安全性。该加密所需密钥文件需要使用gs_guc工具生成并使用gs_ssh工具发布到MogDB每个节点的$GAUSSHOME/bin目录下。username/password不应当包含'encryptOpt'前缀，否则会被认为是加密后的密文。

    - encoding表示与目标库交互的字符串编码方式（含发送的SQL语句和返回的字符类型数据），此处创建对象时不检查encoding取值的合法性，能否正确编解码取决于用户提供的编码方式是否在数据库本身支持的字符编码范围内。

  - optvalue

    选项值。

    取值范围: 空或者非空字符串。

## 示例

```sql
--创建一个空的Data Source对象，不含任何信息。
mogdb=# CREATE DATA SOURCE ds_test1;

--创建一个Data Source对象，含TYPE信息，VERSION为NULL。
mogdb=# CREATE DATA SOURCE ds_test2 TYPE 'MPPDB' VERSION NULL;

--创建一个Data Source对象，仅含OPTIONS。
mogdb=#  CREATE DATA SOURCE ds_test3 OPTIONS (dsn 'MogDB', encoding 'utf8');

--创建一个Data Source对象，含TYPE, VERSION, OPTIONS。
mogdb=# CREATE DATA SOURCE ds_test4 TYPE 'unknown' VERSION '11.2.3' OPTIONS (dsn 'MogDB', username 'userid', password 'pwd@123456', encoding '');

--删除Data Source对象。
mogdb=# DROP DATA SOURCE ds_test1;
mogdb=# DROP DATA SOURCE ds_test2;
mogdb=# DROP DATA SOURCE ds_test3;
mogdb=# DROP DATA SOURCE ds_test4;
```

## 相关链接

[ALTER DATA SOURCE](ALTER-DATA-SOURCE.md),  [DROP DATA SOURCE](DROP-DATA-SOURCE.md)
