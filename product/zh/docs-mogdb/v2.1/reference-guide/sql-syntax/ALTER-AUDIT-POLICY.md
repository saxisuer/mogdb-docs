---
title: ALTER AUDIT POLICY
summary: ALTER AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER AUDIT POLICY

## 功能描述

修改统一审计策略。

## 注意事项

- 只有poladmin，sysadmin或初始用户用户才能进行此操作。
- 需要打开enable_security_policy，开关统一审计策略才可以生效。

## 语法格式

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ADD | REMOVE } { [ privilege_audit_clause ] [ access_audit_clause ] };
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name MODIFY ( filter_group_clause );
ALTER AUDIT POLICY [ IF EXISTS ] policy_name DROP FILTER;
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name COMMENTS policy_comments;
```

```ebnf+diagram
AlterAuditPolicy ::= ALTER AUDIT POLICY [ IF EXISTS ] policy_name { ENABLE | DISABLE };
```

- privilege_audit_clause：

  ```ebnf+diagram
  privilege_audit_clause ::= PRIVILEGES { DDL | ALL };
  ```

- access_audit_clause：

  ```ebnf+diagram
  access_audit_clause ::= ACCESS { DML | ALL };
  ```

- filter_group_clause：

  ```ebnf+diagram
  filter_group_clause ::= FILTER ON {(FILTER_TYPE (filter_value [, ...])) [, ...]};
  ```

## 参数说明

- **policy_name**

  审计策略名称，需要唯一，不可重复。

  取值范围: 字符串，要符合标识符的命名规范。

- **DDL**

  指的是针对数据库执行如下操作时进行审计，目前支持：CREATE、ALTER、DROP、ANALYZE、COMMENT、GRANT、REVOKE、SET、SHOW、LOGIN_ANY、LOGIN_FAILURE、LOGIN_SUCCESS、LOGOUT。

- **ALL**

  指的是上述DDL支持的所有对数据库的操作。

- **DML**

  指的是针对数据库执行如下操作时进行审计，目前支持：SELECT、COPY、DEALLOCATE、DELETE、EXECUTE、INSERT、PREPARE、REINDEX、TRUNCATE、UPDATE。

- **FILTER_TYPE**

  指定审计策略的过滤信息，过滤类型包括：IP、ROLES、APP。

- **filter_value**

  指具体过滤信息内容。

- **policy_comments**

  用于记录策略相关的描述信息。

- **ENABLE|DISABLE**

  可以打开或关闭统一审计策略。若不指定ENABLE|DISABLE，语句默认为ENABLE。

## 示例

请参考[CREATE AUDIT POLICY](CREATE-AUDIT-POLICY.md)的示例。

## 相关链接

[CREATE AUDIT POLICY](CREATE-AUDIT-POLICY.md)，[DROP AUDIT POLICY](DROP-AUDIT-POLICY.md)。
