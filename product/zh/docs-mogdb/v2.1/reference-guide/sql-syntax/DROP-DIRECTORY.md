---
title: DROP DIRECTORY
summary: DROP DIRECTORY
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DIRECTORY

## 功能描述

删除指定的directory对象。

## 注意事项

默认只有初始化用户可以执行drop操作，当enable_access_server_directory开启时，sysadmin权限的用户也可以执行drop操作。

## 语法格式

```ebnf+diagram
DropDictionary ::= DROP DIRECTORY [ IF EXISTS ] directory_name;
```

## 参数说明

- **directory_name**

  目录名称。

  取值范围: 已经存在的目录名。

## 示例

```sql
--创建目录。
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

--删除目录。
mogdb=# DROP DIRECTORY dir;
```

## 相关链接

[CREATE DIRECTORY](CREATE-DIRECTORY.md)，[ALTER DIRECTORY](ALTER-DIRECTORY.md)
