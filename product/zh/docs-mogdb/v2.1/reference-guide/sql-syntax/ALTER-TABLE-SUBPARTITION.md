---
title: ALTER TABLE SUBPARTITION
summary: ALTER TABLE SUBPARTITION
author: Zhang Cuiping
date: 2021-11-01
---

# ALTER TABLE SUBPARTITION

## 功能描述

修改二级分区表分区，包括清空分区、切割分区等。

## 注意事项

- 目前二级分区表只支持清空分区、切割分区。
- 切割分区只能对二级分区（叶子节点）进行切割，被切割分区只能是Range，List分区策略，List分区策略只能是default分区才能被切割。
- 只有分区表的所有者或者被授予了分区表ALTER权限的用户有权限执行ALTER TABLE PARTITION命令，系统管理员默认拥有此权限。

## 语法格式

- 修改表分区主语法。

  ```ebnf+diagram
  AlterTable ::= ALTER TABLE [ IF EXISTS ] { table_name  [*] | ONLY table_name | ONLY ( table_name  )}
      action [, ... ];
  ```

  其中action统指如下分区维护子语法。

  ```ebnf+diagram
  action::= split_clause  |
     truncate_clause
  ```

- split_clause子语法用于把一个分区切割成多个分区。

  ```ebnf+diagram
  split_clause ::= SPLIT SUBPARTITION { subpartition_name} { split_point_clause  } [ UPDATE GLOBAL INDEX ]
  ```

  指定切割点split_point_clause的语法为：

  ```ebnf+diagram
  split_point_clause ::= AT ( subpartition_value ) INTO ( SUBPARTITION subpartition_name [ TABLESPACE tablespacename ] , SUBPARTITION subpartition_name [ TABLESPACE tablespacename ] )
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
  >
  > - 切割点的大小要位于正在被切割的分区的分区键范围内。
  >
  > - 只能把一个分区切割成两个新分区。

- truncate_clause子语法用于清空分区表中的指定分区。

  ```ebnf+diagram
  truncate_clause ::= TRUNCATE SUBPARTITION  { subpartition_name } [ UPDATE GLOBAL INDEX ]
  ```

## 参数说明

- **table_name**

  分区表名。

  取值范围: 已存在的分区表名。

- **subpartition_name**

  二级分区名。

  取值范围: 已存在的二级分区名。

- **tablespacename**

  指定分区要移动到哪个表空间。

  取值范围: 已存在的表空间名。

## 示例

请参考[CREATE TABLE SUBPARTITION](CREATE-TABLE-SUBPARTITION.md)的示例。
