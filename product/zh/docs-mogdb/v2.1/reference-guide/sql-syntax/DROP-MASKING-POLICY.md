---
title: DROP MASKING POLICY
summary: DROP MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# DROP MASKING POLICY

## 功能描述

删除脱敏策略。

## 注意事项

只有poladmin，sysadmin或初始用户才能执行此操作。

## 语法格式

```ebnf+diagram
DropMaskingPolicy ::= DROP MASKING POLICY [IF EXISTS] policy_name;
```

## 参数说明

**policy_name**

审计策略名称，需要唯一，不可重复。

取值范围: 字符串，要符合标识符的命名规范。

## 示例

```sql
--删除一个脱敏策略。
mogdb=# DROP MASKING POLICY IF EXISTS maskpol1;

--删除一组脱敏策略。
mogdb=# DROP MASKING POLICY IF EXISTS maskpol1, maskpol2, maskpol3;
```

## 相关链接

[ALTER MASKING POLICY](ALTER-MASKING-POLICY.md)，[CREATE MASKING POLICY](CREATE-MASKING-POLICY.md)。
