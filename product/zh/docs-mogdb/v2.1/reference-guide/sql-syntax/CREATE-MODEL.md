---
title: CREATE MODEL
summary: CREATE MODEL
author: Zhang Cuiping
date: 2021-11-01
---

# CREATE MODEL

## 功能描述

训练机器学习模型并保存模型。

## 注意事项

- 模型名称具有唯一性约束，注意命名格式。
- AI训练时长波动较大，在部分情况下训练运行时间较长，设置的GUC参数statement_timeout时长过短会导致训练中断。建议statement_timeout设置为0，不对语句执行时长进行限制。

## 语法格式

```ebnf+diagram
CreateModel ::= CREATE MODEL model_name USING algorithm_name
[FEATURES { {expression [ [ AS ] output_name ]} [, ...] }]
[TARGET { {expression [ [ AS ] output_name ]} [, ...] }]
FROM { table_name | select_query }
WITH hyperparameter_name = { hyperparameter_value | DEFAULT } [, ...]
```

## 参数说明

- **model_name**

  对训练模型进行命名，模型名称具有唯一性约束。

  取值范围: 字符串，需要符合标识符的命名规范。

- **architecture_name**

  训练模型的算法类型。

  取值范围: 字符型，当前支持：logistic_regression、linear_regression、svm_classification、kmeans。

- **attribute_list**

  枚举训练模型的输入列名。

  取值范围: 字符型，需要符合数据属性名的命名规范。

- **attribute_name**

  在监督学习任务重训练模型的目标列名(可进行简单的表达式处理)。

  取值范围: 字符型，需要符合数据属性名的命名规范。

- **subquery**

  数据源。

  取值范围: 字符串，符合数据库SQL语法。

- **hyper_parameter_name**

  机器学习模型的超参名称。

  取值范围: 字符串，针对不同算法范围不同。

- **hp_value**

  超参数值。

  取值范围: 字符串，针对不同算法范围不同。

## 示例

```sql
CREATE MODEL price_model USING logistic_regression
 FEATURES size, lot
 TARGET price
 FROM HOUSES
 (WITH learning_rate=0.88, max_iterations=default);
```

## 相关链接

[DROP MODEL](DROP-MODEL.md)，[PREDICT BY](PREDICT-BY.md)
