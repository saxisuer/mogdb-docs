---
title: CREATE USER MAPPING
summary: CREATE USER MAPPING
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE USER MAPPING

## 功能描述

定义一个用户到一个外部服务器的新映射。

## 注意事项

当在OPTIONS中出现password选项时，需要保证MogDB每个节点的`$GAUSSHOME/bin`目录下存在usermapping.key.cipher和usermapping.key.rand文件，如果不存在这两个文件，请使用`gs_guc`工具生成并使用`gs_ssh`工具发布到MogDB每个节点的`$GAUSSHOME/bin`目录下。

## 语法格式

```ebnf+diagram
CreateUserMapping ::= CREATE USER MAPPING FOR { user_name | USER | CURRENT_USER | PUBLIC }
    SERVER server_name
    [ OPTIONS ( option 'value' [ ,' ... '] ) ];
```

## 参数说明

- **user_name**

  要映射到外部服务器的一个现有用户的名称。 `CURRENT_USER`和`USER`匹配当前用户的名称。 当PUBLIC被指定时，一个公共映射会被创建，当没有特定用户的映射可用时将会使用它。

- **server_name**

  将为其创建用户映射的现有服务器的名称。

- **OPTIONS ( { option_name ' value ' } [, …] )**

  这个子句指定用户映射的选项。这些选项通常定义该映射实际的用户名和口令。选项名必须唯一。允许的选项名和值与该服务器的外部数据包装器有关。

  >**说明**:
  >
  >- 用户的口令会加密后保存到系统表[PG_USER_MAPPING](../../reference-guide/system-catalogs-and-system-views/system-catalogs/PG_USER_MAPPING.md)中，加密时需要使用usermapping.key.cipher和usermapping.key.rand作为加密密码文件和加密因子。首次使用前需要通过如下命令创建这两个文件，并将这两个文件放入各节点目录$GAUSSHOME/bin，且确保具有读权限。gs_ssh工具可以协助您快速将文件放入各节点对应目录下。
  >
  >    ```bash
  >    gs_ssh -c "gs_guc generate -o usermapping -S default -D $GAUSSHOME/bin"
  >    ```
  >
  >- 其中-S参数指定default时会随机生成密码，用户也可为-S参数指定密码，此密码用于保证生成密码文件的安全性和唯一性，用户无需保存或记忆。其他参数详见工具参考中gs_guc工具说明。

  oracle_fdw支持的options包括：

  - **user**

    oracle server的用户名。

  - **password**

    oracle用户对应的密码。

  mysql_fdw支持的options包括：

  - **username**

    MySQL Server/MariaDB的用户名。

  - **password**

    MySQL Server/MariaDB用户对应的密码。

  postgres_fdw支持的options包括：

  - **user**

    远端MogDB的用户名。

  - **password**

    远端MogDB用户对应的密码。

    > **说明**:
    > MogDB在后台会对用户输入的password加密以保证安全性。该加密所需密钥文件需要使用gs_guc工具生成并使用gs_ssh工具发布到MogDB每个节点的`$GAUSSHOME/bin`目录下。password不应当包含'encryptOpt'前缀，否则会被认为是加密后的密文。

## 相关链接

[ALTER USER MAPPING](ALTER-USER-MAPPING.md)，[DROP USER MAPPING](DROP-USER-MAPPING.md)
