---
title: DROP GROUP
summary: DROP GROUP
author: Zhang Cuiping
date: 2021-05-18
---

# DROP GROUP

## 功能描述

删除用户组。

DROP GROUP是DROP ROLE的别名。

## 注意事项

DROP GROUP是MogDB管理工具封装的内部接口，用来实现MogDB管理。该接口不建议用户直接使用，以免对MogDB状态造成影响。

## 语法格式

```ebnf+diagram
DropGroup ::= DROP GROUP [ IF EXISTS ] group_name [, ...];
```

## 参数说明

请参见DROP ROLE的参数说明。

## 相关链接

[CREATE GROUP](CREATE-GROUP.md)，[ALTER GROUP](ALTER-GROUP.md)，[DROP ROLE](DROP-ROLE.md)
