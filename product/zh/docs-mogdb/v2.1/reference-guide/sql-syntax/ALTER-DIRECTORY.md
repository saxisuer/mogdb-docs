---
title: ALTER DIRECTORY
summary: ALTER DIRECTORY
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER DIRECTORY

## 功能描述

对directory属性进行修改。

## 注意事项

- 目前只支持修改directory属主。
- 当enable_access_server_directory=off时，只允许初始用户修改directory属主；当enable_access_server_directory=on时，具有SYSADMIN权限的用户和directory对象的属主可以修改directory，且要求该用户是新属主的成员。

## 语法格式

```ebnf+diagram
AlterDirectory ::= ALTER DIRECTORY directory_name
    OWNER TO new_owner;
```

## 参数描述

**directory_name**

需要修改的目录名称，范围为已经存在的目录名称。

## 示例

```sql
--创建目录。
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

--修改目录的owner。
mogdb=# ALTER DIRECTORY dir OWNER TO system;

--删除目录。
mogdb=# DROP DIRECTORY dir;
```

## 相关链接

[CREATE DIRECTORY](CREATE-DIRECTORY.md)，[DROP DIRECTORY](DROP-DIRECTORY.md)
