---
title: GAUSS-01301 - GAUSS-01400
summary: GAUSS-01301 - GAUSS-01400
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01301 - GAUSS-01400

<br/>

## GAUSS-01301 - GAUSS-01310

<br/>

GAUSS-01301: "access method '%s' does not support NULLS FIRST/LAST options"

SQLSTATE: 0A000

错误原因: 索引处理方法不支持NULLS/FIRST/LAST选项。

解决办法: 重建索引并设置索引列的访问方法（pg_am中amcanorder字段）为true支持索引列排序。

GAUSS-01302: "operator class '%s' does not exist for access method '%s'"

SQLSTATE: 42704

错误原因: 索引处理方法不支持的操作符。

解决办法: 请确定操作符是索引支持的类。

GAUSS-01303: "there are multiple default operator classes for data type %s"

SQLSTATE: 42710

错误原因: 类型操作符的种类过多。

解决办法: pg_opclass系统表中类型操作符定义错误，请联系技术支持工程师提供技术支持。

GAUSS-01304: "'%s' does not mean oid '%u'"

SQLSTATE: XX000

错误原因: 分区表OID获取错误。

解决办法: 通过ALTER INDEX…REBUILD;语法重建索引。

GAUSS-01305: "'%u' is not a child of '%u'"

SQLSTATE: XX000

错误原因: 非分区表的子分区。

解决办法: 通过ALTER INDEX…REBUILD;语法重建索引。

GAUSS-01308: "fail to get index info for index %u"

SQLSTATE: XX000

错误原因: 无法得到分区表索引的索引。

解决办法: 在Cache中查找partition表的索引表（pg_index系统表的索引信息）失败，Cache存在异常，请联系技术支持工程师提供技术支持。

GAUSS-01309: "unable to find attribute %d for relation %u."

SQLSTATE: XX000

错误原因: 无法找到表的属性信息。

解决办法: 在Cache中查找partition索引表的属性列失败，Cache存在异常，请联系技术支持工程师提供技术支持。

GAUSS-01310: "cache %d lookup failed for relation %u"

SQLSTATE: XX000

错误原因: 在pg_class表中查找索引表失败。

解决办法: Cache中pg_class表对应的索引表产生异常，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01311 - GAUSS-01320

<br/>

GAUSS-01311: "collation attribute '%s' not recognized"

SQLSTATE: 42601

错误原因: create collation语法不支持。

解决办法: create collation语法不支持。

GAUSS-01314: "collation '%s' for encoding '%s' already exists in schema '%s'"

SQLSTATE: 42710

错误原因: 某类字符集的排序规则已存在。

解决办法: 对一个字符集不创建重复的排序规则。

GAUSS-01315: "collation '%s' already exists in schema '%s'"

SQLSTATE: 42710

错误原因: 排序规则在模式中已存在。

解决办法: 不在同一模式中创建重复的排序规则。

GAUSS-01316: "unlogged sequences are not supported"

SQLSTATE: 0A000

错误原因: 不支持创建不被记录的序列。

解决办法: 请勿创建unlogged类型序列。

GAUSS-01317: "GTM error, could not create sequence"

SQLSTATE: 08006

错误原因: GTM错误，无法创建序列。

解决办法: 通过命令[gs_om -t status -detail]查看gtm状态，如果gtm状态异常，请参考《故障处理》手册定位解决GTM故障问题后再次进行创建序列操作。

GAUSS-01318: "GTM error, could not alter sequence"

SQLSTATE: 08006

错误原因: GTM错误，无法修改序列。

解决办法: 通过命令[gs_om -t status -detail]查看gtm状态，如果gtm状态异常，请参考《故障处理》手册定位解决GTM故障问题后再次进行创建序列操作。

GAUSS-01319: "permission denied for sequence %s"

SQLSTATE: 42501

错误原因: 没有访问序列的权限。

解决办法: 请检查当前用户是否有操作该序列的权限。

GAUSS-01320: "nextval: reached maximum value of sequence '%s' (%s)"

SQLSTATE: 55000

错误原因: 达到序列最大值的限制。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01321 - GAUSS-01330

<br/>

GAUSS-01321: "nextval: reached minimum value of sequence '%s' (%s)"

SQLSTATE: 55000

错误原因: 达到序列最小值的限制。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-01323: "lastval is not yet defined in this session"

SQLSTATE: 55000

错误原因: 序列的最后变量在当前会话中未定义。

解决办法: 请检查当前会话是否已定义该序列的最后变量。

GAUSS-01324: "setval: value %s is out of bounds for sequence '%s' (%s..%s)"

SQLSTATE: 22003

错误原因: 设置的序列值超出了序列范围。

解决办法: 请检查设置值是否超出序列范围。

GAUSS-01325: "GTM error, could not obtain sequence value"

SQLSTATE: 08006

错误原因: GTM错误，无法获得序列值。

解决办法: 通过命令[gs_om -t status -detail]查看gtm状态，如果gtm状态异常，请参考《故障处理》手册定位解决GTM故障问题后再次进行创建序列操作。

GAUSS-01326: "bad magic number in sequence '%s': %08X"

SQLSTATE: XX000

错误原因: 系统内部错误。打开序列表中的魔法数字与期望的魔法数字不一致。

解决办法: 可能由于序列表存在异常导致，请联系技术支持工程师提供技术支持。

GAUSS-01327: "INCREMENT must not be zero"

SQLSTATE: 22023

错误原因: 序列中增量值不能为0。

解决办法: 序列中增量值不能为0。

GAUSS-01328: "MINVALUE (%s) must be less than MAXVALUE (%s)"

SQLSTATE: 22023

错误原因: 序列最小值必须小于最大值。

解决办法: 序列最小值必须小于最大值。

GAUSS-01329: "START value (%s) cannot be less than MINVALUE (%s)"

SQLSTATE: 22023

错误原因: 序列最小值必须小于最大值。

解决办法: 序列初始值不能设置为低于最小值。

GAUSS-01330: "START value (%s) cannot be greater than MAXVALUE (%s)"

SQLSTATE: 22023

错误原因: 序列初始值不能超过最大值。

解决办法: 序列初始值不能设置为超过最大值。

<br/>

## GAUSS-01331 - GAUSS-01340

<br/>

GAUSS-01331: "RESTART value (%s) cannot be less than MINVALUE (%s)"

SQLSTATE: 22023

错误原因: RESTART子句，RESTART的值小于了MINVALUE。

解决办法: 修改RESTART子句，使得RESTART的值不小于MINVALUE。

GAUSS-01332: "RESTART value (%s) cannot be greater than MAXVALUE (%s)"

SQLSTATE: 22023

错误原因: RESTART子句，RESTART的值大于了MAXVALUE。

解决办法: 修改RESTART子句，使得RESTART的值不大于MAXVALUE。

GAUSS-01334: "invalid OWNED BY option"

SQLSTATE: 42601

错误原因: OWNED BY子句中指定的不是NONE。

解决办法: 修改为OWNED BY NONE。

GAUSS-01335: "sequence must have same owner as table it is linked to"

SQLSTATE: 55000

错误原因: sequence与关联的表不是一个所有者。

解决办法: OWNED BY子句只能指定与sequence相同OWNER的table。

GAUSS-01336: "sequence must be in same schema as table it is linked to"

SQLSTATE: 55000

错误原因: sequence与关联的表不是一个schema。

解决办法: OWNED BY子句只能指定与sequence相同schema的table。

GAUSS-01337: "seq_redo: unknown op code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01338: "seq_redo: failed to add item to page"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01340: "unrecognized attribute for text search parser: %d"

SQLSTATE: XX000

错误原因: 通过语法[CREATE TEXT SEARCH CONFIGURATION PARSER…]创建全文检索解析器时，获取系统表pg_ts_parser中指定列错误。

解决办法: 系统表pg_ts_parser属性信息错误，系统产生异常或由人为修改系统表属性导致，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01341 - GAUSS-01350

<br/>

GAUSS-01341: "function %s should return type %s"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH PARSER，某个函数的返回值类型与期望的不符。

解决办法: CREATE TEXT SEARCH PARSER，按照错误提示修改指定函数，新函数的返回值类型必须符合错误提示中的类型。

GAUSS-01342: "must be system admin to create text search parsers"

SQLSTATE: 42501

错误原因: CREATE TEXT SEARCH PARSER，使用了非系统管理员帐户。

解决办法: CREATE TEXT SEARCH PARSER，须得使用管理员帐户。

GAUSS-01343: "text search parser parameter '%s' not recognized"

SQLSTATE: 42601

错误原因: CREATE TEXT SEARCH PARSER，定义了不识别的函数。

解决办法: CREATE TEXT SEARCH PARSER，不要指定start/gettoken/end/headline/lextypes名称之外的函数。

GAUSS-01345: "text search parser gettoken method is required"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH PARSER，没有定义gettoken方法。

解决办法: CREATE TEXT SEARCH PARSER，需要定义gettoken函数。

GAUSS-01347: "text search parser lextypes method is required"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH PARSER，没有定义lextypes方法。

解决办法: CREATE TEXT SEARCH PARSER，需要定义start函数。

GAUSS-01348: "cache lookup failed for text search parser %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01349: "must be system admin to rename text search parsers"

SQLSTATE: 42501

错误原因: ALTER TEXT SEARCH PARSER name RENAME TO new_name，使用了非系统管理员帐户。

解决办法: ALTER TEXT SEARCH PARSER name RENAME TO new_name，须得是系统管理员。

<br/>

## GAUSS-01351 - GAUSS-01360

<br/>

GAUSS-01351: "cache lookup failed for text search template %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01352: "text search template '%s' does not accept options"

SQLSTATE: 42601

错误原因: CREATE TEXT SEARCH DICTIONARY/ALTER TEXT SEARCH DICTIONARY，没有指定INIT方法，但是指定了options。

解决办法: CREATE TEXT SEARCH DICTIONARY/ALTER TEXT SEARCH DICTIONARY，要么在指定INIT时，才指定options选项，要么不要指定任何options。

GAUSS-01353: "text search template is required"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH DICTIONARY，没有提供template。

解决办法: CREATE TEXT SEARCH DICTIONARY，需要提供template。

GAUSS-01354: "cache lookup failed for text search dictionary %u"

SQLSTATE: XX000

错误原因: 在系统表中未找到对应ID的全文检索词典。

解决办法: 请检查是否有会话并发对该词典执行了drop操作。

GAUSS-01355: "text search dictionary '%s' already exists"

SQLSTATE: 42710

错误原因: ALTER TEXT SEARCH DICTIONARY name RENAME TO new_name，提供的新名称与旧名称冲突。

解决办法: ALTER TEXT SEARCH DICTIONARY name RENAME TO new_name，提供的新名称须不与旧名称冲突。

GAUSS-01356: "unrecognized attribute for text search template: %d"

SQLSTATE: XX000

错误原因: 创建全文检索模板时，获取系统表pg_ts_template中指定列错误。

解决办法: 系统表pg_ts_template属性信息错误，系统产生异常或由人为修改系统表属性导致，请联系技术支持工程师提供技术支持。

GAUSS-01357: "must be system admin to create text search templates"

SQLSTATE: 42501

错误原因: CREATE TEXT SEARCH TEMPLATE时，使用了非管理员帐户。

解决办法: CREATE TEXT SEARCH TEMPLATE，需要使用系统管理员帐户。

GAUSS-01358: "text search template parameter '%s' not recognized"

SQLSTATE: 42601

错误原因: CREATE TEXT SEARCH TEMPLATE，提供了不识别的模板参数。

解决办法: CREATE TEXT SEARCH TEMPLATE，不要提供init/lexize名称之外的模板参数。

GAUSS-01359: "text search template lexize method is required"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH TEMPLATE，没有提供合法的lexize方法。

解决办法: CREATE TEXT SEARCH TEMPLATE，需要提供合法的lexize方法。

GAUSS-01360: "must be system admin to rename text search templates"

SQLSTATE: 42501

错误原因: ALTER TEXT SEARCH TEMPLATE name RENAME TO new_name，使用的帐户不是系统管理员帐户。

解决办法: ALTER TEXT SEARCH TEMPLATE name RENAME TO new_name，须得是管理员。

<br/>

## GAUSS-01361 - GAUSS-01370

<br/>

GAUSS-01362: "cache lookup failed for text search configuration %u"

SQLSTATE: XX000

错误原因: 在系统表中未找到对应ID的全文检索配置。

解决办法: 请检查是否有会话并发对该配置做了drop操作。

GAUSS-01363: "text search configuration parameter '%s' not recognized"

SQLSTATE: 42601

错误原因: CREATE TEXT SEARCH CONFIGURATION，提供了不被识别的配置参数。

解决办法: CREATE TEXT SEARCH CONFIGURATION，不要提供除了parser/copy之外的配置参数。

GAUSS-01364: "cannot specify both PARSER and COPY options"

SQLSTATE: 42601

错误原因: CREATE TEXT SEARCH CONFIGURATION，同时指定了PARSER/COPY选项。

解决办法: CREATE TEXT SEARCH CONFIGURATION，不要同时指定PARSER/COPY选项。

GAUSS-01365: "text search parser is required"

SQLSTATE: 42P17

错误原因: CREATE TEXT SEARCH CONFIGURATION，从parser/copy中计算出的最终的parser非法。

解决办法: CREATE TEXT SEARCH CONFIGURATION，从parser/copy中计算出的最终的parser必须是可用的。

GAUSS-01366: "text search configuration '%s' already exists"

SQLSTATE: 42710

错误原因: ALTER TEXT SEARCH CONFIGURATION RENAME，新名称与旧名称冲突。

解决办法: ALTER TEXT SEARCH CONFIGURATION RENAME，新名称须得不与旧名称冲突。

GAUSS-01367: "text search configuration '%s' does not exist"

SQLSTATE: 42704

错误原因: ALTER TEXT SEARCH CONFIGURATION，指定名称的配置不存在。

解决办法: ALTER TEXT SEARCH CONFIGURATION，须得指定名称存在的配置。

GAUSS-01368: "method lextype isn't defined for text search parser %u"

SQLSTATE: XX000

错误原因: 系统表pg_ts_parser中对应的prslextype定义错误。

解决办法: 系统表pg_ts_parser属性信息错误，请联系技术支持工程师提供技术支持。

GAUSS-01369: "token type '%s' does not exist"

SQLSTATE: 22023

错误原因: ALTER TEXT SEARCH CONFIGURATION(ADD/ALTER/DROP MAPPING)，某个token_type不存在。

解决办法: ALTER TEXT SEARCH CONFIGURATION(ADD/ALTER/DROP MAPPING)，必须指定存在的某个token_type。

GAUSS-01370: "mapping for token type '%s' does not exist"

SQLSTATE: 42704

错误原因: ALTER TEXT SEARCH CONFIGURATION(DROP MAPPING)，某个标识的MAPPING不存在。

解决办法: ALTER TEXT SEARCH CONFIGURATION(DROP MAPPING)，必须指定存在的某个mapping。

<br/>

## GAUSS-01371 - GAUSS-01380

<br/>

GAUSS-01371: "invalid parameter list format: '%s'"

SQLSTATE: 42601

错误原因: 反序列化字典选项时，参数list格式不正确，=后面缺少空格。

解决办法: 参数list格式不正确，=后面紧跟一个空格。

GAUSS-01372: "unrecognized deserialize_deflist state: %d"

SQLSTATE: XX000

错误原因: 无法识别全文检索目录反序列化过程状态机。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-01375: "Bad socket."

SQLSTATE: XX000

错误原因: 系统内部错误。read socket不合法。

解决办法: 请检查gds与数据节点的网络连接、网络环境、节点的系统端口是否正常。

GAUSS-01376: "Unexpected EOF on GDS connection '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。与GDS端交互数据时，发生了错误。

解决办法: 请检查gds与数据节点的网络连接、网络环境、节点的系统端口是否正常。请检查GDS的LOG，查看相关WARNING和ERROR日志，定位相关原因。

GAUSS-01377: "Unexpected connection EOF from '%s':%m"

SQLSTATE: XX000

错误原因: 系统内部错误。与GDS端交互数据时，发生了错误。

解决办法: 请检查gds与数据节点的网络连接、网络环境、节点的系统端口是否正常。请检查GDS的LOG，查看相关WARNING和ERROR日志，定位相关原因。

GAUSS-01378: "invalid URI '%s'"

SQLSTATE: XX000

错误原因: 提供给GDS外表的URI非法。

解决办法: 请检查并纠正GDS外表中location相关设置。

GAUSS-01380: "'%s' is a table"

SQLSTATE: 42809

错误原因: 在表上定义了INSTEAD OF触发器。

解决办法: 不要在表上定义INSTEAD OF trigger，只能定义BEFORE/AFTER。

<br/>

## GAUSS-01381 - GAUSS-01390

<br/>

GAUSS-01381: "'%s' is a view"

SQLSTATE: 42809

错误原因: 在视图上定义了row-level BEFORE or AFTER triggers/TRUNCATE触发器。

解决办法: 在视图上只能定义INSTEAD OF触发器。

GAUSS-01382: "'%s' is not a table or view"

SQLSTATE: 42809

错误原因: 在表上或者视图之外的对象上定义触发器。

解决办法: 只能在表和视图上定义视图。

GAUSS-01383: "TRUNCATE FOR EACH ROW triggers are not supported"

SQLSTATE: 0A000

错误原因: 定义了ROW-level TRUNCATE triggers。

解决办法: 不要定义TRUNCATE FOR EACH ROW。

GAUSS-01385: "INSTEAD OF triggers cannot have WHEN conditions"

SQLSTATE: 0A000

错误原因: INSTEAD OF triggers附加了WHEN子句。

解决办法: INSTEAD OF triggers附加了WHEN子句。

GAUSS-01386: "INSTEAD OF triggers cannot have column lists"

SQLSTATE: 0A000

错误原因: INSTEAD OF triggers定义在了表的列上。

解决办法: INSTEAD OF triggers不能在表的列上定义。

GAUSS-01387: "cannot use subquery in trigger WHEN condition"

SQLSTATE: 0A000

错误原因: trigger WHEN子句中使用了子查询。

解决办法: trigger WHEN子句中不能使用子查询。

GAUSS-01388: "cannot use aggregate function in trigger WHEN condition"

SQLSTATE: 42803

错误原因: trigger WHEN子句中使用了聚集函数。

解决办法: trigger WHEN子句中不能使用聚集函数。

GAUSS-01389: "cannot use window function in trigger WHEN condition"

SQLSTATE: 42P20

错误原因: trigger WHEN子句中使用了window函数。

解决办法: trigger WHEN子句中不能使用window函数。

GAUSS-01390: "statement trigger's WHEN condition cannot reference column values"

SQLSTATE: 42P17

错误原因: statement triggers的WHEN子句引用了列值。

解决办法: statement triggers的WHEN子句不能引用列值。

<br/>

## GAUSS-01391 - GAUSS-01400

<br/>

GAUSS-01391: "INSERT trigger's WHEN condition cannot reference OLD values"

SQLSTATE: 42P17

错误原因: INSERT trigger的WHEN子句引用了OLD值。

解决办法: INSERT trigger的WHEN子句不能引用OLD值。

GAUSS-01392: "DELETE trigger's WHEN condition cannot reference NEW values"

SQLSTATE: 42P17

错误原因: DELETE trigger的WHEN子句引用了NEW值。

解决办法: DELETE trigger的WHEN子句不能引用NEW值。

GAUSS-01393: "BEFORE trigger's WHEN condition cannot reference NEW system columns"

SQLSTATE: 0A000

错误原因: BEFORE trigger的WHEN子句引用了NEW系统列值。

解决办法: BEFORE trigger的WHEN子句不能引用NEW系统列值。

GAUSS-01394: "trigger WHEN condition cannot contain references to other relations"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01395: "function %s must return type 'trigger'"

SQLSTATE: 42P17

错误原因: trigger function的返回值不是trigger类型。

解决办法: trigger function的返回值必须是trigger类型。

GAUSS-01396: "trigger '%s' for relation '%s' already exists"

SQLSTATE: 42710

错误原因: relation上已经定义了同名的trigger。

解决办法: 修改create trigger的名称，不要与已有trigger重名。

GAUSS-01397: "confused about RI update function"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01398: "confused about RI delete function"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01399: "could not find tuple for trigger %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01400: "trigger '%s' for table '%s' does not exist"

SQLSTATE: 42704

错误原因: 按照名称去查找trigger，结果发现找不到。

解决办法: 指定存在的trigger名。
