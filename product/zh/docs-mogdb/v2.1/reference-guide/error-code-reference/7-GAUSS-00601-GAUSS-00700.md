---
title: GAUSS-00601 - GAUSS-00700
summary: GAUSS-00601 - GAUSS-00700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00601 - GAUSS-00700

<br/>

## GAUSS-00601 - GAUSS-00610

<br/>

### GAUSS-00601: "Permission denied."

SQLSTATE: 42501

错误原因: CREATE/ALTER/DROP ROLE没有权限操作失败。

解决办法: 确认当前用户是否有权限。

### GAUSS-00602: "Separation of privileges is used,user can't be altered because of too many privileges."

SQLSTATE: 42501

错误原因: 使用三权分立，为用户指定的权限超出范围。

解决办法: 确认三权分立下为用户指定的权限没有超出范围。

### GAUSS-00603: "The account has been locked."

SQLSTATE: 28000

错误原因: 非法操作，帐号被锁定。

解决办法: 联系系统管理员进行处理。通常系统管理员会确认该用户是否有非法操作。确认无非法操作后才会协助进行帐号解锁。

### GAUSS-00606: "The role's password is null in pg_authid, REPLACE should not be specified."

SQLSTATE: XX000

错误原因: 修改密码失败，用户未指定密码，不能指定REPLACE。

解决办法: 请指定密码。

### GAUSS-00610: "Permission denied to drop role."

SQLSTATE: 42501

错误原因: DROP ROLE失败，没有权限。

解决办法: 确认当前用户是否有权限。

<br/>

## GAUSS-00611 - GAUSS-00620

<br/>

### GAUSS-00611: "fail to drop the current schema"

SQLSTATE: 55006

错误原因: DROP ROLE失败，删除当前schema失败。

解决办法: 不能删除当前用户。

### GAUSS-00612: "current user cannot be dropped"

SQLSTATE: 55006

错误原因: DROP ROLE失败，当前用户不能被删除。

解决办法: 不能删除当前用户。

### GAUSS-00613: "session user cannot be dropped"

SQLSTATE: 55006

错误原因: DROP ROLE失败，连接当前会话的用户不能删除。

解决办法: 不能删除当前会员用户。

### GAUSS-00615: "role '%s' cannot be dropped because some objects depend on it"

SQLSTATE: 2BP01

错误原因: DROP ROLE失败，存在依赖当前用户的对象。

解决办法: 消除依赖后重新尝试。

### GAUSS-00616: "session user cannot be renamed"

SQLSTATE: 0A000

错误原因: 更名失败，当前会话用户不能更名。

解决办法: 请使用其他有权限的用户重试。

### GAUSS-00617: "current user cannot be renamed"

SQLSTATE: 0A000

错误原因: 更名失败，当前用户不能被更名。

解决办法: 当前会话用户不能更名。

### GAUSS-00620: "Permission denied to rename role."

SQLSTATE: 42501

错误原因: 没有权限对用户进行重命名。

解决办法: 请确认当前用户是否有权限。

<br/>

## GAUSS-00621 - GAUSS-00630

<br/>

### GAUSS-00621: "column names cannot be included in GRANT/REVOKE ROLE"

SQLSTATE: 0LP01

错误原因: GRANT/REVOKE ROLE包含列名。

解决办法: GRANT/REVOKE ROLE不能包含列名。

### GAUSS-00622: "Permission denied to drop objects."

SQLSTATE: 42501

错误原因: 删除用户拥有的对象失败。

解决办法: 确认当前用户是否有权限。

### GAUSS-00623: "Permission denied to reassign objects."

SQLSTATE: 42501

错误原因: 重新为用户分配对象失败。

解决办法: 确认当前用户是否有权限。

### GAUSS-00625: "must have admin option on role '%s'"

SQLSTATE: 42501

错误原因: 系统管理员用户却不具有SYSADMIN属性。

解决办法: 创建系统管理员用户时，请指定SYSADMIN属性。

### GAUSS-00626: "must be system admin to set grantor"

SQLSTATE: 42501

错误原因: 设置权限授予者失败。

解决办法: 请使用系统管理员权限设置权限授予者。

### GAUSS-00627: "role '%s' is a member of role '%s'"

SQLSTATE: 0LP01

错误原因: 目标用户已经是某个用户的成员。

解决办法: 检查目标用户是否已经是某个用户的成员。

### GAUSS-00628: "schema '%s' doesnot exist"

SQLSTATE: 3F000

错误原因: schema不存在。

解决办法: 检查schema是否存在。

### GAUSS-00629: "cannot cancel current session's query"

SQLSTATE: 42601

错误原因: cancel当前session的查询失败。

解决办法: 检查是否当前session对用户拥有的对象进行加锁。

### GAUSS-00630: "md5-password encryption failed."

SQLSTATE: XX000

错误原因: MD5加密失败。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00631 - GAUSS-00640

<br/>

### GAUSS-00631: "sha256-password encryption failed."

SQLSTATE: XX000

错误原因: SHA256加密失败。

解决办法: 请联系技术支持工程师提供技术支持。

### GAUSS-00633: "Password must contain at least %d upper characters."

SQLSTATE: 28P01

错误原因: 密码应至少包含由GUC参数password_min_uppercase指定个数的大写字母。

解决办法: 在密码中添加大写字母以满足参数要求。

### GAUSS-00634: "Password must contain at least %d lower characters."

SQLSTATE: 28P01

错误原因: 密码应至少包含由GUC参数password_min_lowercase指定个数的小写字母。

解决办法: 在密码中添加小写字母以满足参数要求。

### GAUSS-00635: "Password must contain at least %d digital characters."

SQLSTATE: 28P01

错误原因: 密码应至少包含由GUC参数password_min_digital指定个数的数字。

解决办法: 在密码中添加数字以满足参数要求。

### GAUSS-00636: "Password must contain at least %d special characters."

SQLSTATE: 28P01

错误原因: 密码应至少包含由GUC参数password_min_special指定个数的特殊字符。

解决办法: 在密码中添加特殊字符以满足参数要求。

### GAUSS-00637: "Password must contain at least three kinds of characters."

SQLSTATE: 28P01

错误原因: 密码包含的字符类型少于3种。

解决办法: "请参照密码规则进行修改: 1. 密码默认不少于8个字符。2. 不能和用户名相同。3. 至少包含大写字母（A-Z），小写字母（a-z），数字（0-9），非字母数字字符（限定为~!@#$%^&*()-_=+\|[{}];:,<.>/?）四类字符中的三类字符。"

### GAUSS-00638: "The parameter roleID of CheckPasswordComplexity is NULL"

SQLSTATE: XX000

错误原因: 密码复杂性检查失败，roleID为空。

解决办法: roleID不能为空。

### GAUSS-00639: "The parameter newPasswd of CheckPasswordComplexity is NULL"

SQLSTATE: XX000

错误原因: 密码复杂性检查失败，新密码为空。

解决办法: 新密码不能为空，请重新输入。

### GAUSS-00640: "Password should not equal to the rolname."

SQLSTATE: XX000

错误原因: 新密码和用户名相同。

解决办法: 不允许密码和用户名相同。请重新选择符合要求的密码。

<br/>

## GAUSS-00641 - GAUSS-00650

<br/>

### GAUSS-00641: "reverse_string failed, possibility out of memory"

SQLSTATE: XX000

错误原因: 密码字符串反转失败。

解决办法: 内部错误，检查是否发生OOM。

### GAUSS-00644: "New password should not equal to the reverse of old ones."

SQLSTATE: XX000

错误原因: 新密码和旧密码的反转相同。

解决办法: 不能和旧密码的反转相同，换一个新的重试。

### GAUSS-00645: "the parameter passwd of AddAuthHistory is null"

SQLSTATE: XX000

错误原因: 添加密码修改记录时密码为空。

解决办法: 添加密码修改记录时密码不能为空。

### GAUSS-00646: "sha256-password encryption failed"

SQLSTATE: XX000

错误原因: SHA256加密失败。

解决办法: 内部错误。

### GAUSS-00647: "The password cannot be reused."

SQLSTATE: XX000

错误原因:  密码不能被重用。

解决办法: 检查新密码与旧密码是否相同，重用条件是否满足。

### GAUSS-00648: "TryLockAccount(): roleid is not valid."

SQLSTATE: XX000

错误原因: roleid无效。

解决办法: 检查roleid是否合法。

### GAUSS-00649: "Permission denied."

SQLSTATE: XX000

错误原因: CREATE/ALTER/DROP ROLE没有权限操作失败。

解决办法: 确认当前用户是否有权限。

### GAUSS-00650: "TryLockAccount(): parameter extrafails is less than zero."

SQLSTATE: XX000

错误原因: 参数extrafails小于0。

解决办法: 保证参数extrafails不小于0。

<br/>

## GAUSS-00651 - GAUSS-00660

<br/>

### GAUSS-00651: "The tuple of pg_user_status not found"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

### GAUSS-00652: "TryUnlockAccount(): roleid is not valid."

SQLSTATE: XX000

错误原因: roleid无效。

解决办法: 检查roleid是否合法。

### GAUSS-00653: "IsAccountLocked(): roleid is not valid."

SQLSTATE: XX000

错误原因: roleid无效。

解决办法: 检查roleid是否合法。

### GAUSS-00654: "getAccountLockedStyle: roleid is not valid."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

### GAUSS-00655: "DropUserStatus(): roleid is not valid."

SQLSTATE: XX000

错误原因: roleid无效。

解决办法: 检查roleid是否合法。

### GAUSS-00656: "Invalid username/password,login denied."

SQLSTATE: 42704

错误原因: 用户名或密码无效，登录失败。

解决办法: 检查登录的用户名和密码是否有效。

### GAUSS-00657: "User's passwordtime in pg_auth_history is 0."

SQLSTATE: 28P01

错误原因: pg_auth_history中对应用户的passwordtime为空。

解决办法: 此列理论上不会为空，若出现问题，请联系技术支持工程师提供技术支持。

### GAUSS-00658: "aggregate stype must be specified"

SQLSTATE: 42P13

错误原因: 聚集函数定义时stype没有指定。

解决办法: 聚集函数定义时必须指定stype。

### GAUSS-00659: "aggregate sfunc must be specified"

SQLSTATE: 42P13

错误原因: 聚集函数定义时sfunc没有指定。

解决办法: 聚集函数定义时必须指定sfunc。

### GAUSS-00660: "aggregate input type must be specified"

SQLSTATE: 42P13

错误原因: 聚集函数定义时input type没有指定。

解决办法: 聚集函数定义时必须指定input type。

<br/>

## GAUSS-00661 - GAUSS-00670

<br/>

### GAUSS-00661: "basetype is redundant with aggregate input type specification"

SQLSTATE: 42P13

错误原因: 聚集函数定义时参数发生冗余。

解决办法: pg8.2之后使用新风格定的聚集函数，不用再指定basetype来表述输入类型。

### GAUSS-00662: "aggregate transition data type cannot be %s"

SQLSTATE: 42P13

错误原因: 聚集函数转换的数据类型有误。

解决办法: 转换类型不能为pseudo-type，对于初始用户转换类型可以为internal。

### GAUSS-00663: "function %s already exists in schema '%s'"

SQLSTATE: 42723

错误原因: 模式下函数已存在。

解决办法: 检查是否存在已定义的同名函数。

### GAUSS-00664: "Unsupport feature"

SQLSTATE: XX000

错误原因: 不支持的特性。

解决办法: 列存表不支持继承和CREATE TABLE OF TYPENAME，检查表定义语句。

### GAUSS-00665: "column store unsupport constraint '%s'"

SQLSTATE: XX000

错误原因: 定义列存表不支持的约束。

解决办法: 列存表仅支持NULL、NOT NULL、DEFAULT、CLUSTER约束。

### GAUSS-00666: "PARTIAL_CLUSTER_ROWS cannot be less than MAX_BATCHROW."

SQLSTATE: 42P16

错误原因: PARTIAL_CLUSTER_ROWS小于MAX_BATCHROW。

解决办法: PARTIAL_CLUSTER_ROWS必须大于或等于MAX_BATCHROW。

### GAUSS-00667: "ON COMMIT can only be used on temporary tables"

SQLSTATE: 42P16

错误原因: ON COMMIT参数用于非临时表。

解决办法: ON COMMIT参数只能用于临时表。

### GAUSS-00668: "constraints on foreign tables are not supported"

SQLSTATE: 42809

错误原因: 为外表定义约束。

解决办法: 外表不支持约束。

### GAUSS-00669: "cannot create temporary table within security-restricted operation"

SQLSTATE: 42501

错误原因:  安全限制的情形下创建临时表。

解决办法: 安全限制的情形下禁止创建临时表。

### GAUSS-00670: "only shared relations can be placed in pg_global tablespace"

SQLSTATE: 22023

错误原因: 用户定义的relation不能置于pg_global表空间下。

解决办法: 只有共享的relation可以置于pg_global表空间下。

<br/>

## GAUSS-00671 - GAUSS-00680

<br/>

### GAUSS-00673: "default values on foreign tables are not supported"

SQLSTATE: 42809

错误原因: 定义外表时列指定默认值。

解决办法: 外表不支持列指定默认值。

### GAUSS-00674: "No Datanode defined in cluster"

SQLSTATE: 42704

错误原因: 数据节点未定义。

解决办法: 为当前集群创建数据节点。

### GAUSS-00675: "DROP INDEX CONCURRENTLY does not support dropping multiple objects"

SQLSTATE: 0A000

错误原因: DROP INDEX CONCURRENTLY语句删除多个对象，因为此语句不支持删除多个对象。

解决办法: 禁止删除多个对象的操作。

### GAUSS-00676: "DROP INDEX CONCURRENTLY does not support CASCADE"

SQLSTATE: 0A000

错误原因: DROP INDEX CONCURRENTLY语句指定CASCADE。

解决办法: DROP INDEX CONCURRENTLY语句不支持指定CASCADE。

### GAUSS-00677: "unrecognized drop object type: %d"

SQLSTATE: XX000

错误原因: 对象drop类型不支持。

解决办法: 检查对象drop类型是否支持。

### GAUSS-00678: "permission denied: '%s' is a system catalog"

SQLSTATE: 42501

错误原因: 该表为系统表，用户没有权限删除系统表。

解决办法: 禁止删除系统表。

### GAUSS-00679: "PGXC does not support RESTART IDENTITY yet"

SQLSTATE: 0A000

错误原因: TRUNCATE语句指定RESTART IDENTITY。

解决办法: 暂时不支持TRUNCATE语句指定RESTART IDENTITY。

<br/>

## GAUSS-00681 - GAUSS-00690

<br/>

### GAUSS-00681: "'%s' is not a table"

SQLSTATE: 42809

错误原因: truncate的对象不是一个表。

解决办法: truncate的对象需要是一个表。

### GAUSS-00682: "cannot truncate temporary tables of other sessions"

SQLSTATE: 0A000

错误原因: truncate其他session的临时表。

解决办法: 请勿truncate其他session的临时表。

### GAUSS-00683: "tables can have at most %d columns"

SQLSTATE: 54011

错误原因: 表超过1600列。

解决办法: 表最多拥有1600列。

### GAUSS-00684: "cannot inherit from temporary relation '%s'"

SQLSTATE: 42809

错误原因: 继承临时表。

解决办法: 临时表仅当前session可见，请勿继承临时表。

### GAUSS-00685: "cannot inherit from temporary relation of another session"

SQLSTATE: 42809

错误原因: 继承临时表。

解决办法: 临时表仅当前session可见，请勿继承临时表。

### GAUSS-00686: "relation '%s' would be inherited from more than once"

SQLSTATE: 42P07

错误原因: 表被继承多次。

解决办法: 检查建表语句，确保继承的表中没有重复表。

### GAUSS-00687: "inherited column '%s' has a type conflict"

SQLSTATE: 42804

错误原因: 继承的列存在类型冲突。

解决办法: 检查类型是否一致。

### GAUSS-00688: "inherited column '%s' has a collation conflict"

SQLSTATE: 42P21

错误原因: 继承的列存在排序规则冲突。

解决办法: 检查排序规则是否一致。

### GAUSS-00689: "inherited column '%s' has a storage parameter conflict"

SQLSTATE: 42804

错误原因: 继承的列存在存储参数冲突。

解决办法: 检查存储参数是否一致。

### GAUSS-00690: "column '%s' has a type conflict"

SQLSTATE: 42804

错误原因: 列存在类型冲突。

解决办法: 检查类型是否一致。

<br/>

## GAUSS-00691 - GAUSS-00700

<br/>

### GAUSS-00691: "column '%s' has a collation conflict"

SQLSTATE: 42P21

错误原因: 列存在排序规则冲突。

解决办法: 检查排序规则是否一致。

### GAUSS-00692: "column '%s' has a storage parameter conflict"

SQLSTATE: 42804

错误原因: 列存在存储参数冲突。

解决办法: 检查存储参数是否一致。

### GAUSS-00693: "column '%s' inherits conflicting default values"

SQLSTATE: 42611

错误原因: 列继承与默认值冲突。

解决办法: 列继承不能和默认值冲突。

### GAUSS-00694: "check constraint name '%s' appears multiple times but with different expressions"

SQLSTATE: 42710

错误原因: 不同表达式的check约束名出现多次。

解决办法: 检查不同表达式的check约束名是否出现多次。

### GAUSS-00695: "cannot rename column of typed table"

SQLSTATE: 42809

错误原因: 对type进行更名操作。

解决办法: 不能修改type的列名。

### GAUSS-00696: "'%s' is not a table, view, composite type, index, or foreign table"

SQLSTATE: 42809

错误原因: 更名的对象不是一个表。

解决办法: 不能修改视图、类型、索引或外表的列名。

### GAUSS-00697: "inherited column '%s' must be renamed in child tables too"

SQLSTATE: 42P16

错误原因: 继承的列在更名时没有在子表中进行修改。

解决办法: 继承的列在更名时必须在子表中进行修改。

### GAUSS-00698: "cannot rename system column '%s'"

SQLSTATE: 0A000

错误原因: 修改系统预留列的列名。

解决办法: 不支持修改系统预留列的列名。

### GAUSS-00699: "cannot rename inherited column '%s'"

SQLSTATE: 42P16

错误原因: 修改继承的列名。

解决办法: 禁止修改继承的列名。

### GAUSS-00700: "inherited constraint '%s' must be renamed in child tables too"

SQLSTATE: 42P16

错误原因: 继承的约束在更名时没有在子表中进行修改。

解决办法: 继承的约束在更名时必须在子表中进行修改。
