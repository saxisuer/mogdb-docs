---
title: PKG_SERVICE接口
summary: PKG_SERVICE接口
author: Guo Huan
date: 2022-03-18
---

# PKG_SERVICE接口

MogDB 2.1版本提供了以下PKG_SERVICE接口来实现定时任务管理。

<br/>

表1 接口描述

| 接口名称               | 描述                                                       |
| ---------------------- | ---------------------------------------------------------- |
| PKG_SERVICE.JOB_CANCEL | 通过任务ID来删除定时任务。                                 |
| PKG_SERVICE.JOB_FINISH | 禁用或者启用定时任务。                                     |
| PKG_SERVICE.JOB_SUBMIT | 提交一个定时任务。作业号由系统自动生成或由用户指定。       |
| PKG_SERVICE.JOB_UPDATE | 修改定时任务的属性，包括任务内容、下次执行时间、执行间隔。 |

<br/>

## 接口定义和使用示例

- PKG_SERVICE.JOB_CANCEL

  存储过程CANCEL删除指定的定时任务。

  PKG_SERVICE.JOB_CANCEL函数原型为：

  ```
  PKG_SERVICE.JOB_CANCEL( job IN INTEGER);
  ```

  表2 PKG_SERVICE.JOB_CANCEL接口参数说明

  | 参数 | 类型    | 入参/出参 | 是否可以为空 | 描述           |
  | ---- | ------- | --------- | ------------ | -------------- |
  | id   | integer | IN        | 否           | 指定的作业号。 |

  示例：

  ```sql
  CALL PKG_SERVICE.JOB_CANCEL(101);
  ```

- PKG_SERVICE.JOB_FINISH

  存储过程FINISH禁用或者启用定时任务。

  PKG_SERVICE.JOB_FINISH函数原型为：

  ```
  PKG_SERVICE.JOB_FINISH(
  id IN BIGINT,
  broken IN BOOLEAN, 
  next_time IN TIMESTAMP DEFAULT sysdate);
  ```

  表3 PKG_SERVICE.JOB_FINISH接口参数说明
  
  | 参数      | 类型      | 入参/出参 | 是否可以为空 | 描述                                                         |
  | --------- | --------- | --------- | ------------ | ------------------------------------------------------------ |
  | id        | bigint   | IN        | 否           | 指定的作业号。                                               |
  | broken    | Boolean   | IN        | 否           | 状态标志位，true代表禁用，false代表启用。根据true或false值更新当前job；如果为空值，则不改变原有job的状态。 |
  | next_time | timestamp | IN        | 是           | 下次运行时间，默认为当前系统时间。如果参数broken状态为true，则更新该参数为'4000-1-1'；如果参数broken状态为false，且如果参数next_time不为空值，则更新指定job的next_time值，如果next_time为空值，则不更新next_time值。该参数可以省略，为默认值。 |

- PKG_SERVICE.JOB_SUBMIT

  存储过程JOB_SUBMIT提交一个系统提供的定时任务。

  PKG_SERVICE.JOB_SUBMIT函数原型为：

  ```
  PKG_SERVICE.JOB_SUBMIT(
  id IN BIGINT DEFAULT, 
  content IN TEXT, 
  next_date IN TIMESTAMP DEFAULT sysdate, 
  interval_time IN TEXT DEFAULT 'null', 
  job OUT INTEGER);
  ```
  
  > 说明：当创建一个定时任务（JOB）时，系统默认将当前数据库和用户名与当前创建的定时任务绑定起来。该接口函数可以通过call或select调用，如果通过select调用，可以不填写出参。如果在存储过程中，则需要通过perform调用该接口函数。如果提交的sql语句任务使用到非public的schema，应该指定表或者函数的schema，或者在sql语句前添加`set current_schema = xxx;`语句。
  
  表4 PKG_SERVICE.JOB_SUBMIT接口参数说明
  
  | 参数          | 类型      | 入参/出参 | 是否可以为空 | 描述                                                         |
  | ------------- | --------- | --------- | ------------ | ------------------------------------------------------------ |
  | id            | bigint    | IN        | 否           | 作业号。如果传入id为NULL，则内部会生成作业ID。               |
  | context       | text      | IN        | 否           | 要执行的SQL语句。支持一个或多个‘DML’，‘匿名块’，‘调用存储过程的语句’或3种混合的场景。 |
  | next_time     | timestamp | IN        | 否           | 下次作业运行时间。默认值为当前系统时间（sysdate）。如果是过去时间，在提交作业时表示立即执行。 |
  | interval_time | text      | IN        | 是           | 用来计算下次作业运行时间的时间表达式，可以是interval表达式，也可以是sysdate加上一个numeric值（例如：sysdate+1.0/24）。如果为空值或字符串"null"表示只执行一次，执行后JOB状态STATUS变成'd' 不再执行。 |
  | job           | integer   | OUT       | 否           | 作业号。范围为1～32767。当使用select调用pkg_service.job_submit时，该参数可以省略。 |
  
  示例：
  
  ```sql
  SELECT PKG_SERVICE.JOB_SUBMIT(NULL, 'call pro_xxx();', to_date('20180101','yyyymmdd'),'sysdate+1');
  
  SELECT PKG_SERVICE.JOB_SUBMIT(NULL, 'call pro_xxx();', to_date('20180101','yyyymmdd'),'sysdate+1.0/24');
  
  CALL PKG_SERVICE.JOB_SUBMIT(NULL, 'INSERT INTO T_JOB  VALUES(1);  call pro_1(); call pro_2();', add_months(to_date('201701','yyyymm'),1), 'date_trunc(''day'',SYSDATE) + 1 +(8*60+30.0)/(24*60)' ,:jobid);
  
  SELECT PKG_SERVICE.JOB_SUBMIT (101, 'insert_msg_statistic1;', sysdate, 'sysdate+3.0/24');
  ```

- PKG_SERVICE.JOB_UPDATE

  存储过程UPDATE修改定时任务的属性，包括任务内容、下次执行时间、执行间隔。

  PKG_SERVICE.JOB_UPDATE函数原型为：

  ```
  PKG_SERVICE.JOB_UPDATE(
  id IN BIGINT, 
  next_time IN TIMESTAMP, 
  interval_time IN TEXT, 
  content IN TEXT);
  ```

  表5 PKG_SERVICE.JOB_UPDATE接口参数说明

  | 参数          | 类型      | 入参/出参 | 是否可以为空 | 描述                                                         |
  | ------------- | --------- | --------- | ------------ | ------------------------------------------------------------ |
  | id            | integer   | IN        | 否           | 指定的作业号。                                               |
  | next_time     | timestamp | IN        | 是           | 下次运行时间。如果该参数为空值，则不更新指定job的next_time值，否则更新指定job的next_time值。 |
  | interval_time | text      | IN        | 是           | 用来计算下次作业运行时间的时间表达式。如果该参数为空值，则不更新指定job的interval_time值；如果该参数不为空值，会校验interval_time是否为有效的时间类型或interval类型，则更新指定job的interval_time值。如果为字符串"null"表示只执行一次，执行后JOB状态STATUS变成'd' 不再执行。 |
  | content       | text      | IN        | 是           | 执行的存储过程名或者sql语句块。如果该参数为空值，则不更新指定job的content值，否则更新指定job的content值。 |

  示例：

  ```sql
  CALL PKG_SERVICE.JOB_UPDATE(101, 'call userproc();', sysdate, 'sysdate + 1.0/1440');
  
  CALL PKG_SERVICE.JOB_UPDATE(101, 'insert into tbl_a values(sysdate);', sysdate, 'sysdate + 1.0/1440');
  ```

<br/>

## 定时任务（job）使用示例

本示例介绍如何通过上述接口实现定时任务（job）管理。

- 创建测试表

  ```
  mogdb=# create table t_job (value TIMESTAMP);
  CREATE TABLE
  
  mogdb=# insert into t_job values(sysdate);
  INSERT 0 1
  
  mogdb=# select * from t_job;
  +---------------------+
  | value               |
  |---------------------|
  | 2021-10-09 04:36:20 |
  +---------------------+
  SELECT 1
  ```

- 创建任务，每一分钟插入一条记录

  ```
  mogdb=# select pkg_service.job_submit(null, 'insert into t_job values(sysdate);',sysdate,'sysdate + 1/1440');
  +--------------+
  | job_submit   |
  |--------------|
  | 15566        |
  +--------------+
  SELECT 1
  ```

- 检查JOB运行结果

  ```
  mogdb=# select * from t_job;
  +---------------------+
  | value               |
  |---------------------|
  | 2021-10-09 04:36:20 |
  | 2021-10-09 04:40:54 |
  | 2021-10-09 04:41:54 |
  | 2021-10-09 04:42:54 |
  +---------------------+
  SELECT 4
  ```

- 从系统视图中检查JOB运行情况

  ```
  mogdb=# select job_id,dbname,start_date,next_run_date,interval,failure_count from pg_job;
  +----------+----------+----------------------------+---------------------+------------------+-----------------+
  | job_id   | dbname   | start_date                 | next_run_date       | interval         | failure_count   |
  |----------+----------+----------------------------+---------------------+------------------+-----------------|
  | 15566    | postgres | 2021-10-09 04:40:54.072363 | 2021-10-09 04:56:54 | sysdate + 1/1440 | 0               |
  +----------+----------+----------------------------+---------------------+------------------+-----------------+
  SELECT 1
  Time: 0.089s
  mogdb=# select * from pg_catalog.pg_job_proc pjp where job_id=15566;
  +----------+------------------------------------+
  | job_id   | what                               |
  |----------+------------------------------------|
  | 15566    | insert into t_job values(sysdate); |
  +----------+------------------------------------+
  SELECT 1
  Time: 0.089s
  ```

- 修改为2分钟执行一次

  ```
  mogdb=# select pkg_service.job_update(15566,null,'sysdate + 2/1440',null);
  +--------------+
  | job_update   |
  |--------------|
  |              |
  +--------------+
  SELECT 1
  ```

- 检查修改情况和运行结果

  ```
  mogdb=# select job_id,interval from pg_job where job_id=15566;
  +----------+------------------+
  | job_id   | interval         |
  |----------+------------------|
  | 15566    | sysdate + 2/1440 |
  +----------+------------------+
  SELECT 1
  mogdb=# select * from t_job;
  +---------------------+
  | value               |
  |---------------------|
  | 2021-10-09 04:36:20 |
  | 2021-10-09 04:40:54 |
  | 2021-10-09 04:41:54 |
  | 2021-10-09 04:42:54 |
  | 2021-10-09 04:43:54 |
  | 2021-10-09 04:44:54 |
  | 2021-10-09 04:45:54 |
  | 2021-10-09 04:46:54 |
  | 2021-10-09 04:47:54 |
  | 2021-10-09 04:48:54 |
  | 2021-10-09 04:49:54 |
  | 2021-10-09 04:50:54 |
  | 2021-10-09 04:51:54 |
  | 2021-10-09 04:52:54 |
  | 2021-10-09 04:53:54 |
  | 2021-10-09 04:54:54 |
  | 2021-10-09 04:55:54 |
  | 2021-10-09 04:56:54 |
  | 2021-10-09 04:57:54 |
  | 2021-10-09 04:58:54 |
  | 2021-10-09 04:59:54 |
  | 2021-10-09 05:00:55 |
  | 2021-10-09 05:01:56 | <---
  | 2021-10-09 05:03:57 | <--- 开始间隔2分钟
  +---------------------+
  SELECT 24
  Time: 0.088s
  mogdb=# select job_id,interval,next_run_date from pg_job where job_id=15566;
  +----------+------------------+---------------------+
  | job_id   | interval         | next_run_date       |
  |----------+------------------+---------------------|
  | 15566    | sysdate + 2/1440 | 2021-10-09 05:05:57 |
  +----------+------------------+---------------------+
  SELECT 1
  Time: 0.078s>
  ```

- 禁用和启用任务

  禁用和启用都是同样的函数pkg_service.job_finish，传入不同的参数表示是禁用还是启用。

  ```
  mogdb=# select pkg_service.job_finish(15566,true,null);
  +--------------+
  | job_finish   |
  |--------------|
  |              |
  +--------------+
  SELECT 1
  Time: 0.089s
  mogdb=# select job_id,next_run_date,job_status from pg_job where job_id=15566;
  +----------+---------------------+--------------+
  | job_id   | next_run_date       | job_status   |
  |----------+---------------------+--------------|
  | 15566    | 4000-01-01 00:00:00 | d            |
  +----------+---------------------+--------------+
  SELECT 1
  Time: 0.075s
  mogdb=# select pkg_service.job_finish(15566,false,null);
  +--------------+
  | job_finish   |
  |--------------|
  |              |
  +--------------+
  SELECT 1
  Time: 0.091s
  mogdb=# select job_id,next_run_date,job_status from pg_job where job_id=15566;
  +----------+---------------------+--------------+
  | job_id   | next_run_date       | job_status   |
  |----------+---------------------+--------------|
  | 15566    | 4000-01-01 00:00:00 | s            |
  +----------+---------------------+--------------+
  SELECT 1
  Time: 0.080s
  ```

  可以看到如果重新启用任务的时候，没有指定下次运行时间，那么下次运行时间会始终保持在4000年，意味着仍然不会启动，所以如果禁用任务之后再重新启动，需要手动显式指定下次运行时间。

  ```
  mogdb=# select pkg_service.job_finish(15566,false,sysdate);
  +--------------+
  | job_finish   |
  |--------------|
  |              |
  +--------------+
  SELECT 1
  Time: 0.088s
  mogdb=# select job_id,next_run_date,job_status from pg_job where job_id=15566;
  +----------+---------------------+--------------+
  | job_id   | next_run_date       | job_status   |
  |----------+---------------------+--------------|
  | 15566    | 2021-10-09 05:16:22 | s            |
  +----------+---------------------+--------------+
  SELECT 1
  Time: 0.086s
  ```

- 删除任务

  ```
  mogdb=# select pkg_service.job_cancel(15566);
  +--------------+
  | job_cancel   |
  |--------------|
  |              |
  +--------------+
  SELECT 1
  Time: 0.082s
  mogdb=# select job_id,next_run_date,job_status from pg_job where job_id=15566;
  +----------+-----------------+--------------+
  | job_id   | next_run_date   | job_status   |
  |----------+-----------------+--------------|
  +----------+-----------------+--------------+
  SELECT 0
  Time: 0.086s
  mogdb=# select * from pg_catalog.pg_job_proc pjp where job_id=15566;
  +----------+--------+
  | job_id   | what   |
  |----------+--------|
  +----------+--------+
  SELECT 0
  Time: 0.087s
  ```

<br/>

## 相关页面

[PG_JOB](../../reference-guide/system-catalogs-and-system-views/system-catalogs/PG_JOB.md)、[定时任务](../../reference-guide/guc-parameters/31-scheduled-task.md)