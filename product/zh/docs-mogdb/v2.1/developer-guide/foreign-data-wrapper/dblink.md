---
title: dblink
summary: dblink
author: Guo Huan
date: 2021-10-19
---

# dblink

dblink是一个可以在一个MogDB数据库会话中连接到其它MogDB数据库的工具，同libpq支持的连接参数一致，可参考[链接字符](../../developer-guide/dev/4-development-based-on-libpq/2-libpq/6-connection-characters.md)。MogDB默认不编译dblink，下面依次介绍如何编译和使用dblink。

## 编译dblink

当前dblink的源码放在[contrib/dblink](https://gitee.com/opengauss/openGauss-server/tree/master/contrib/dblink)目录中。在编译安装完MogDB数据库之后，如果用户需要使用dblink，只需要进入上述目录执行如下即可完成dblink的编译安装。

```bash
make
make install
```

## 常用的dblink函数

- 加载dblink扩展

  ```sql
  CREATE EXTENSION dblink;
  ```

- 打开一个到远程数据库的持久连接

  ```sql
  SELECT dblink_connect(text connstr);
  ```

- 关闭一个到远程数据库的持久连接

  ```sql
  SELECT dblink_disconnect();
  ```

- 在远程数据库执行查询

  ```sql
  SELECT * FROM dblink(text connstr, text sql);
  ```

- 在远程数据库执行命令

  ```sql
  SELECT dblink_exec(text connstr, text sql);
  ```

- 返回所有打开的命名dblink连接的名称

  ```sql
  SELECT dblink_get_connections();
  ```

- 发送一个异步查询到远程数据库

  ```sql
  SELECT dblink_send_query(text connname, text sql);
  ```

- 检查连接是否正在忙于一个异步查询

  ```sql
  SELECT dblink_is_busy(text connname);
  ```

- 删除扩展

  ```sql
  DROP EXTENSION dblink;
  ```

## 注意事项

目前dblink仅支持MogDB数据库访问另一个MogDB数据库，不支持MogDB数据库访问PostgreSQL数据库。
