---
title: postgres_fdw
summary: postgres_fdw
author: Zhang Cuiping
date: 2021-05-17
---

# postgres_fdw

postgres_fdw是一款开源插件，其代码随PostgreSQL源码一同发布。MogDB基于开源的 PostgreSQL 9.4.26 版本中的postgres_fdw源码<https://ftp.postgresql.org/pub/source/v9.4.26/postgresql-9.4.26.tar.gz>进行开发适配。

postgres_fdw插件默认参与编译，使用安装包安装好MogDB后，可直接使用postgres_fdw，无须其他操作。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前postgres_fdw仅支持MogDB连接MogDB，不支持MogDB连接PostgreSQL。

## 使用postgres_fdw

- 加载postgres_fdw扩展：**CREATE EXTENSION postgres_fdw;**
- 创建服务器对象：**CREATE SERVER**
- 创建用户映射：**CREATE USER MAPPING**
- 创建外表：**CREATE FOREIGN TABLE** 外表的表结构需要与远端MogDB侧的表结构保持一致。
- 对外表做正常的操作，如 **INSERT** 、 **UPDATE** 、 **DELETE** 、 **SELECT** 、 **EXPLAIN** 、 **ANALYZE** 、 **COPY** 等。
- 删除外表：**DROP FOREIGN TABLE**
- 删除用户映射：**DROP USER MAPPING**
- 删除服务器对象：**DROP SERVER**
- 删除扩展：**DROP EXTENSION postgres_fdw;**

## 常见问题

- 在MogDB上建立外表时，不会同步在远端的MogDB上建表，需要自己利用客户端连接远端MogDB建表。
- 执行**CREATE USER MAPPING**时使用的MogDB用户需要有远程连接MogDB及对表相关操作的权限。使用外表前，可以在本地机器上，使用gsql的客户端，使用对应的用户名密码确认能否成功连接远端MogDB并进行操作。

## 注意事项

- 两个postgres_fdw外表间的**SELECT JOIN**不支持下推到远端MogDB执行，会被分成两条SQL语句传递到远端MogDB执行，然后在本地汇总处理结果。
- 不支持**IMPORT FOREIGN SCHEMA**语法。
- 不支持对外表进行**CREATE TRIGGER**操作。
