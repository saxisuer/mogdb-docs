---
title: 使用
summary: 使用
author: Liuxu
date: 2021-05-21
---

# 使用

## 语法格式

- 创建增量物化视图

  ```sql
  CREATE INCREMENTAL MATERIALIZED VIEW [ view_name ] AS { query_block }; 
  ```

- 全量刷新物化视图

  ```sql
  REFRESH MATERIALIZED VIEW [ view_name ];
  ```

- 增量刷新物化视图

  ```sql
  REFRESH INCREMENTAL MATERIALIZED VIEW [ view_name ];
  ```

- 删除物化视图

  ```sql
  DROP MATERIALIZED VIEW [ view_name ];
  ```

- 查询物化视图

  ```sql
  SELECT * FROM [ view_name ];
  ```

## 示例

```sql
--准备数据。
CREATE TABLE t1(c1 int, c2 int);
INSERT INTO t1 VALUES(1, 1);
INSERT INTO t1 VALUES(2, 2);

--创建增量物化视图。
mogdb=# CREATE INCREMENTAL MATERIALIZED VIEW mv AS SELECT * FROM t1;
CREATE MATERIALIZED VIEW

--插入数据。
mogdb=# INSERT INTO t1 VALUES(3, 3);
INSERT 0 1

--增量刷新物化视图。
mogdb=# REFRESH INCREMENTAL MATERIALIZED VIEW mv;
REFRESH MATERIALIZED VIEW

--查询物化视图结果。
mogdb=# SELECT * FROM mv;
 c1 | c2 
----+----
  1 |  1
  2 |  2
  3 |  3
(3 rows)

--插入数据。
mogdb=# INSERT INTO t1 VALUES(4, 4);
INSERT 0 1

--全量刷新物化视图。
mogdb=# REFRESH MATERIALIZED VIEW mv;
REFRESH MATERIALIZED VIEW

--查询物化视图结果。
mogdb=# select * from mv;
 c1 | c2 
----+----
  1 |  1
  2 |  2
  3 |  3
  4 |  4
(4 rows)

--删除物化视图。
mogdb=# DROP MATERIALIZED VIEW mv;
DROP MATERIALIZED VIEW
```