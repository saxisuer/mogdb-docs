---
title: 创建资源池
summary: 创建资源池
author: Guo Huan
date: 2021-10-22
---

# 创建资源池

## 背景信息

MogDB支持通过创建资源池对主机资源进行划分。开启资源负载管理之后，仅使用默认资源池并不能满足业务对资源负载管理的诉求，必须根据需要创建新的资源池，对系统资源进行重分配，来满足实际业务对系统资源精细管理的需要。普通资源池的特点见表1。

**表 1** 普通资源池的特点

| 资源池分类             | 特点                                                         |
| :--------------------- | :----------------------------------------------------------- |
| 普通资源池（普通场景） | 关联Workload控制组。 且必须没有组资源池关联该控制组对应的子class。即如果创建业务资源池关联控制组‘class1:wd’，那么必须没有组资源池关联‘class1’。mem_percent默认为0%，没有mem_percent相加小于100%的限制。 |

在开启了资源负载管理功能之后，系统会自动创建default_pool，当一个会话或者用户没有指定关联的资源池时，都会被默认关联到default_pool。default_pool默认绑定DefaultClass:Medium控制组，并且不限制所关联的业务的并发数。default_pool的详细属性如表2所示。

**表 2** default_pool属性

| 属性              | 属性值              | 说明                                                         |
| :---------------- | :------------------ | :----------------------------------------------------------- |
| respool_name      | default_pool        | 资源池名称。                                                 |
| mem_percent       | 100                 | 最大占用内存百分比。                                         |
| cpu_affinity      | -1                  | CPU亲和性，保留参数。                                        |
| control_group     | DefaultClass:Medium | 资源池关联的控制组。                                         |
| active_statements | -1                  | 资源池允许的最大并发数。-1为不限制并发数量。                 |
| max_dop           | 1                   | 开启SMP后，算子执行的并发度，保留参数。                      |
| memory_limit      | 8GB                 | 内存使用上限，保留参数。                                     |
| parentid          | 0                   | 父资源池OID。                                                |
| io_limits         | 0                   | 每秒触发IO的次数上限。行存单位是万次/s，列存是次/s。0表示不控制。 |
| io_priority       | None                | IO利用率高达90%时，重消耗IO作业进行IO资源管控时关联的优先级等级。None表示不控制。 |
| nodegroup         | installation        | 资源池所在的逻辑集群的名称。                                 |
| is_foreign        | f                   | 资源池不用于逻辑集群之外的用户。                             |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: MogDB不允许对default_pool参数进行修改。

## 前提条件

已熟悉[CREATE RESOURCE POOL](../../../reference-guide/sql-syntax/CREATE-RESOURCE-POOL.md)、[ALTER RESOURCE POOL](../../../reference-guide/sql-syntax/ALTER-RESOURCE-POOL.md)和[DROP RESOURCE POOL](../../../reference-guide/sql-syntax/DROP-RESOURCE-POOL.md)语法的使用。

## 操作过程

**创建资源池**

1. [使用gsql连接数据库](../../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md)。

2. 创建组资源池关联到指定的子Class控制组。例如下面：名称为“resource_pool_a”的组资源池关联到了“class_a”控制组。

   ```sql
   mogdb=# CREATE RESOURCE POOL resource_pool_a WITH (control_group='class_a');
   mogdb=# CREATE RESOURCE POOL resource_pool_b WITH (control_group='class_b');
   CREATE RESOURCE POOL
   ```

3. 创建业务资源池关联到指定的Workload控制组。例如下面：名称为“resource_pool_a1”的业务资源池关联到了“workload_a1”控制组。

   ```sql
   mogdb=# CREATE RESOURCE POOL resource_pool_a1 WITH (control_group='class_a:workload_a1');
   mogdb=# CREATE RESOURCE POOL resource_pool_a2 WITH (control_group='class_a:workload_a2');
   mogdb=# CREATE RESOURCE POOL resource_pool_b1 WITH (control_group='class_b:workload_b1');
   mogdb=# CREATE RESOURCE POOL resource_pool_b2 WITH (control_group='class_b:workload_b2');
   CREATE RESOURCE POOL
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **NOTE:**
   >
   > - 如果在创建资源池的时候不指定所关联的控制组，则该资源池会被关联到默认控制组（DefaultClass控制组下的“Medium” Timeshare控制组）。
   > - control_group取值区分大小写，指定时要使用单引号。
   > - 若数据库用户指定Timeshare控制组代表的字符串，即“Rush”、“High”、“Medium”或“Low”其中一种，如control_group的字符串为“High”，代表资源池指定到DefaultClass控制组下的“High” Timeshare控制组。
   > - control_group可以指定用户创建Workload控制组，即'class1:wd'，也可以带有控制组的级别，例如：'class1:wd:2'，这个级别范围一定要在1-10的范围内，但这个级别将不做任何区分作用。在旧版本中，允许创建同名Workload控制组，以级别进行区分。但新版本升级后，不允许创建同名控制组，用户如在旧版本中已创建同名Workload控制组，使用过程中其级别将不进行区分，由此可能造成的控制组不明确使用的问题，需要用户自行把旧的同名控制组删除以明确控制组使用。

**管理资源池**

修改资源池的属性。例如下面：修改资源池“resource_pool_a2”关联的控制组为“class_a:workload_a1”（假设class_a:workload_a1未被其他资源池关联）。

```sql
mogdb=# ALTER RESOURCE POOL resource_pool_a2 WITH (control_group="class_a:workload_a1");
ALTER RESOURCE POOL
```

**删除资源池**

删除资源池。例如下面删除资源池“resource_pool_a2”

```sql
mogdb=# DROP RESOURCE POOL resource_pool_a2;
DROP RESOURCE POOL
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 如果某个角色已关联到该资源池，无法删除。
> - 多租户场景下，如果删除组资源池，其业务资源池都将被删除。只有不关联用户时，资源池才能被删除。

## 查看资源池的信息

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 不允许使用INSERT、UPDATE、DELETE、TRUNCATE操作资源负载管理的系统表pg_resource_pool。
> - 不允许修改资源池的memory_limit和cpu_affinity属性。

- 查看当前集群中所有的资源池信息。

  ```sql
  mogdb=# SELECT * FROM PG_RESOURCE_POOL;
  ```

  ```sql
  respool_name     | mem_percent | cpu_affinity |    control_group    | active_statements | max_dop | memory_limit | parentid | io_limits | io_priority |  nodegroup   | is_foreign  | max_worker
  ------------------+-------------+--------------+---------------------+-------------------+---------+--------------+----------+-----------+--------------+--------------+------------
  default_pool     |         100 |           -1 | DefaultClass:Medium |                -1 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_a  |          20 |           -1 | class_a             |                10 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_b  |          20 |           -1 | class_b             |                10 |       1 | 8GB          |        0 |         0 | None        | installation | f  |
  resource_pool_a1 |          20 |           -1 | class_a:workload_a1 |                10 |       1 | 8GB          |    16970 |         0 | None        | installation | f  |
  resource_pool_a2 |          20 |           -1 | class_a:workload_a2 |                10 |       1 | 8GB          |    16970 |         0 | None        | installation | f  |
  resource_pool_b1 |          20 |           -1 | class_b:workload_b1 |                10 |       1 | 8GB          |    16971 |         0 | None        | installation | f  |
  resource_pool_b2 |          20 |           -1 | class_b:workload_b2 |                10 |       1 | 8GB          |    16971 |         0 | None        | installation | f  |
  (7 rows)
  ```

- 查看某个资源池关联的控制组信息，具体内容可以参考[统计信息函数](../../../reference-guide/functions-and-operators/25-statistics-information-functions.md)章节的gs_control_group_info(pool text)函数。

  如下命令中“resource_pool_a1”为资源池名称。

  ```sql
  mogdb=# SELECT * FROM gs_control_group_info('resource_pool_a1');
  ```

  ```sql
          name         |  class  |  workload   | type  | gid | shares | limits | rate | cpucores
  ---------------------+---------+-------------+-------+-----+--------+--------+------+----------
  class_a:workload_a1 | class_a | workload_a1 | DEFWD |  87 |     30 |      0 |    0 | 0-3
  (1 row)
  ```

  **表 3** gs_control_group_info属性

  | 属性     | 属性值              | 说明                                         |
  | :------- | :------------------ | :------------------------------------------- |
  | name     | class_a:workload_a1 | class和workload名称                          |
  | class    | class_a             | Class控制组名称                              |
  | workload | workload_a1         | Workload控制组名称                           |
  | type     | DEFWD               | 控制组类型（Top、CLASS、BAKWD、DEFWD、TSWD） |
  | gid      | 87                  | 控制组id                                     |
  | shares   | 30                  | 占父节点CPU资源的百分比                      |
  | limits   | 0                   | 占父节点CPU核数的百分比                      |
  | rate     | 0                   | Timeshare中的分配比例                        |
  | cpucores | 0-3                 | CPU核心数                                    |
