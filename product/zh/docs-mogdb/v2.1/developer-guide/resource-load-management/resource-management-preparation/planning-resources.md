---
title: 资源规划
summary: 资源规划
author: Guo Huan
date: 2021-10-22
---

# 资源规划

完成资源负载管理功能配置前，需要先根据业务模型完成租户资源的规划。业务运行一段时间后，可以根据资源的使用情况再进行配置调整。

本章节我们假设某大型企业内的两个部门共用同一套集群，MogDB通过将同一个部门需要使用的系统资源集合划分为系统的一个租户，以此来实现不同部门间的资源隔离，其资源规划如表1所示。

**表 1** 租户资源规划

<table>
    <tr>
        <th>租户名称</th>
        <th>参数名称</th>
        <th>取值样例</th>
    </tr>
    <tr>
        <td rowspan="6">租户A</td>
        <td>子Class控制组</td>
        <td>class_a</td>
    </tr>
    <tr>
        <td>Workload控制组</td>
        <td>- workload_a1<br/>- workload_a2</td>
    </tr>
    <tr>
        <td>组资源池</td>
        <td>resource_pool_a</td>
    </tr>
    <tr>
        <td>业务资源池</td>
        <td>- resource_pool_a1<br/>- resource_pool_a2</td>
    </tr>
    <tr>
        <td>组用户</td>
        <td>tenant_a</td>
    </tr>
    <tr>
        <td>业务用户</td>
        <td>- tenant_a1<br/>- tenant_a2</td>
    </tr>
</table>

<table>
    <tr>
        <th>租户名称</th>
        <th>参数名称</th>
        <th>取值样例</th>
    </tr>
    <tr>
        <td rowspan="6">租户B</td>
        <td>子Class控制组</td>
        <td>class_b</td>
    </tr>
    <tr>
        <td>Workload控制组</td>
        <td>- workload_b1<br/>- workload_b2</td>
    </tr>
    <tr>
        <td>组资源池</td>
        <td>resource_pool_b</td>
    </tr>
    <tr>
        <td>业务资源池</td>
        <td>- resource_pool_b1<br/>- resource_pool_b2</td>
    </tr>
    <tr>
        <td>组用户</td>
        <td>tenant_b</td>
    </tr>
    <tr>
        <td>业务用户</td>
        <td>- tenant_b1<br/>- tenant_b2</td>
    </tr>
</table>
