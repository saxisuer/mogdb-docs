---
title: 附录
summary: 附录
author: Guo Huan
date: 2021-04-27
---

# 附录

## <a id="PG_STAT_ACTIVITY">**表 1**</a> PG_STAT_ACTIVITY字段

| 名称             | 类型                     | 描述 |
| :--------------- | :----------------------- | :---------------------------------------------|
| datid            | oid                      | 用户会话在后台连接到的数据库OID。              |
| datname          | name                     | 用户会话在后台连接到的数据库名称。             |
| pid              | bigint                   | 后台线程ID。                                   |
| sessionid        | bigint                   | 会话ID。                                       |
| usesysid         | oid                      | 登录该后台的用户OID。                          |
| usename          | name                     | 登录该后台的用户名。                           |
| application_name | text                     | 连接到该后台的应用名。                         |
| client_addr      | inet                     | 连接到该后台的客户端的IP地址。 如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum。|
| client_hostname  | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。                   |
| client_port      | integer                  | 客户端用于与后台通讯的TCP端口号，如果使用Unix套接字，则为-1。              |
| backend_start    | timestamp with time zone | 该过程开始的时间，即当客户端连接服务器时。     |
| xact_start       | timestamp with time zone | 启动当前事务的时间，如果没有事务是活跃的，则为null。如果当前查询是首个事务，则这列等同于query_start列。                            |
| query_start      | timestamp with time zone | 开始当前活跃查询的时间， 如果state的值不是active，则这个值是上一个查询的开始时间。                                                 |
| state_change     | timestamp with time zone | 上次状态改变的时间。                           |
| waiting          | Boolean                  | 如果后台当前正等待锁则为true。                 |
| enqueue          | text                     | 该字段不支持。                                 |
| state            | text                     | 该后台当前总体状态。可能值是: <br />- active: 后台正在执行一个查询。<br />- idle: 后台正在等待一个新的客户端命令。<br />- idle in transaction: 后台在事务中，但事务中没有语句在执行。<br />- idle in transaction (aborted): 后台在事务中，但事务中有语句执行失败。<br />- fastpath function call: 后台正在执行一个fast-path函数。<br />- disabled: 如果后台禁用track_activities，则报告这个状态。<br />说明:<br />普通用户只能查看到自己帐户所对应的会话状态。即其他帐户的state信息为空。例如以judy用户连接数据库后，在pg_stat_activity中查看到的普通用户joe及初始用户omm的state信息为空: <br /> *SELECT datname, usename, usesysid, state,pid FROM pg_stat_activity;* <br /> datname | usename | usesysid | state | pid ----+---+----+---+------ postgres | omm | 10 |  | 139968752121616 postgres | omm | 10 |  | 139968903116560 db_tpcc | judy | 16398 | active | 139968391403280 postgres | omm | 10 |  | 139968643069712 postgres | omm | 10 |  | 139968680818448 postgres | joe | 16390 |  | 139968563377936 (6 rows)` |
| resource_pool    | name                     | 用户使用的资源池。                             |
| query_id         | bigint                   | 查询语句的ID。                                 |
| query            | text                     | 该后台的最新查询。如果state状态是active（活跃的），此字段显示当前正在执行的查询。所有其他情况表示上一个查询。                      |
| connection_info  | text                     | json格式字符串，记录当前连接数据库的驱动类型、驱动版本号、当前驱动的部署路径、进程属主用户等信息（参见connection_info）。          |

## <a id="GS_WLM">**表 2** </a>GS_WLM_SESSION_HISTORY的字段

| 名称                | 类型                     | 描述|
| ------------------- | ------------------- | ---------------- |
| datid               | oid                      | 连接后端的数据库OID。 |
| dbname              | text                     | 连接后端的数据库名称。                                |
| schemaname          | text                     | 模式的名称。                                          |
| nodename            | text                     | 语句执行的数据库节点名称。                            |
| username            | text                     | 连接到后端的用户名。                                  |
| application_name    | text                     | 连接到后端的应用名。                                  |
| client_addr         | inet                     | 连接到后端的客户端的IP地址。 如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum。 |
| client_hostname     | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。                                  |
| client_port         | integer                  | 客户端用于与后端通讯的TCP端口号，如果使用Unix套接字，则为-1。          |
| query_band          | text                     | 用于标示作业类型，可通过GUC参数query_band进行设置，默认为空字符串。    |
| block_time          | bigint                   | 语句执行前的阻塞时间，包含语句解析和优化时间，单位ms。|
| start_time          | timestamp with time zone | 语句执行的开始时间。                                  |
| finish_time         | timestamp with time zone | 语句执行的结束时间。                                  |
| duration            | bigint                   | 语句实际执行的时间，单位ms。                          |
| estimate_total_time | bigint                   | 语句预估执行时间，单位ms。                            |
| status              | text                     | 语句执行结束状态: 正常为finished，异常为aborted。     |
| abort_info          | text                     | 语句执行结束状态为aborted时显示异常信息。             |
| resource_pool       | text                     | 用户使用的资源池。                                    |
| control_group       | text                     | 该字段不支持                                          |
| estimate_memory     | integer                  | 语句估算内存大小。                                    |
| min_peak_memory     | integer                  | 语句在数据库节点上的最小内存峰值，单位MB。            |
| max_peak_memory     | integer                  | 语句在数据库节点上的最大内存峰值，单位MB。            |
| average_peak_memory | integer                  | 语句执行过程中的内存使用平均值，单位MB。              |
| memory_skew_percent | integer                  | 语句数据库节点间的内存使用倾斜率。                    |
| spill_info          | text                     | 语句在数据库节点上的下盘信息: <br />- None: 数据库节点均未下盘。<br />- All: 数据库节点均下盘。<br />- [a:b]: 数量为b个数据库节点中有a个数据库节点下盘。|
| min_spill_size      | integer                  | 若发生下盘，数据库节点上下盘的最小数据量，单位MB，默认为0。            |
| max_spill_size      | integer                  | 若发生下盘，数据库节点上下盘的最大数据量，单位MB，默认为0。            |
| average_spill_size  | integer                  | 若发生下盘，数据库节点上下盘的平均数据量，单位MB，默认为0。            |
| spill_skew_percent  | integer                  | 若发生下盘，数据库节点间下盘倾斜率。                  |
| min_dn_time         | bigint                   | 语句在数据库节点上的最小执行时间，单位ms。            |
| max_dn_time         | bigint                   | 语句在数据库节点上的最大执行时间，单位ms。            |
| average_dn_time     | bigint                   | 语句在数据库节点上的平均执行时间，单位ms。            |
| dntime_skew_percent | integer                  | 语句在数据库节点间的执行时间倾斜率。                  |
| min_cpu_time        | bigint                   | 语句在数据库节点上的最小CPU时间，单位ms。             |
| max_cpu_time        | bigint                   | 语句在数据库节点上的最大CPU时间，单位ms。             |
| total_cpu_time      | bigint                   | 语句在数据库节点上的CPU总时间，单位ms。               |
| cpu_skew_percent    | integer                  | 语句在数据库节点间的CPU时间倾斜率。                   |
| min_peak_iops       | integer                  | 语句在数据库节点上的每秒最小IO峰值（列存单位是次/s，行存单位是万次/s）。                  |
| max_peak_iops       | integer                  | 语句在数据库节点上的每秒最大IO峰值（列存单位是次/s，行存单位是万次/s）。                  |
| average_peak_iops   | integer                  | 语句在数据库节点上的每秒平均IO峰值（列存单位是次/s，行存单位是万次/s）。                  |
| iops_skew_percent   | integer                  | 语句在数据库节点间的IO倾斜率。                        |
| warning             | text                     | 主要显示如下几类告警信息: <br />- Spill file size large than 256MB<br />- Broadcast size large than 100MB<br />- Early spill<br />- Spill times is greater than 3<br />- Spill on memory adaptive<br />- Hash table conflict |
| queryid             | bigint                   | 语句执行使用的内部query id。                          |
| query               | text                     | 执行的语句。                                          |
| query_plan          | text                     | 语句的执行计划。                                      |
| node_group          | text                     | 该字段不支持。                                        |
| cpu_top1_node_name  | text                     | 当前数据库节点名称。                                  |
| cpu_top2_node_name  | text                     | 该字段不支持。                                        |
| cpu_top3_node_name  | text                     | 该字段不支持。                                        |
| cpu_top4_node_name  | text                     | 该字段不支持。                                        |
| cpu_top5_node_name  | text                     | 该字段不支持。                                        |
| mem_top1_node_name  | text                     | 当前数据库节点名称。                                  |
| mem_top2_node_name  | text                     | 该字段不支持。                                        |
| mem_top3_node_name  | text                     | 该字段不支持。                                        |
| mem_top4_node_name  | text                     | 该字段不支持。                                        |
| mem_top5_node_name  | text                     | 该字段不支持。                                        |
| cpu_top1_value      | bigint                   | 当前数据库节点cpu使用率。                             |
| cpu_top2_value      | bigint                   | 该字段不支持。                                        |
| cpu_top3_value      | bigint                   | 该字段不支持。                                        |
| cpu_top4_value      | bigint                   | 该字段不支持。                                        |
| cpu_top5_value      | bigint                   | 该字段不支持。                                        |
| mem_top1_value      | bigint                   | 当前数据库节点内存使用量。                            |
| mem_top2_value      | bigint                   | 该字段不支持。                                        |
| mem_top3_value      | bigint                   | 该字段不支持。                                        |
| mem_top4_value      | bigint                   | 该字段不支持。                                        |
| mem_top5_value      | bigint                   | 该字段不支持。                                        |
| top_mem_dn          | text                     | 当前数据库节点内存使用量信息。                        |
| top_cpu_dn          | text                     | 当前数据库节点cpu使用量信息。                         |
