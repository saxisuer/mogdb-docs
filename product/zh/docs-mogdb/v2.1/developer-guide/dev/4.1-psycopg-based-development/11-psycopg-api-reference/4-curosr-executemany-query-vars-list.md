---
title: curosr.executemany(query,vars_list)
summary: curosr.executemany(query,vars_list)
author: Zhang Cuiping
date: 2021-10-11
---

# curosr.executemany(query,vars_list)

## 功能描述

此方法执行SQL命令所有参数序列或序列中的SQL映射。

## 原型

```
curosr.executemany(query,vars_list)
```

## 参数

**表 1** curosr.executemany参数

| **关键字** | **参数说明**                    |
| :--------- | :------------------------------ |
| query      | 待执行的SQL语句。               |
| vars_list  | 变量列表，匹配query中%s占位符。 |

## 返回值

无

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。
