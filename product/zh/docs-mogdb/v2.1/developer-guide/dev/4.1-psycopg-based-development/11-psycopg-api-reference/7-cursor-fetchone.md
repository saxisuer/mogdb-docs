---
title: cursor.fetchone()
summary: cursor.fetchone()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.fetchone()

## 功能描述

此方法提取查询结果集的下一行，并返回一个元组。

## 原型

```
cursor.fetchone()
```

## 参数

无

## 返回值

单个元组，为结果集的第一条结果，当没有更多数据可用时，返回为“None”。

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。