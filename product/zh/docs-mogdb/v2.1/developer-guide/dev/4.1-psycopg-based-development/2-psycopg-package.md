---
title: Psycopg包
summary: Psycopg包
author: Zhang Cuiping
date: 2021-10-11
---

# Psycopg包

从发布包中获取，包名为GaussDB-Kernel-VxxxRxxxCxx-操作系统版本号-64bit-Python.tar.gz。

解压后有两个文件夹：

- psycopg2：psycopg2库文件。
- lib：lib库文件。