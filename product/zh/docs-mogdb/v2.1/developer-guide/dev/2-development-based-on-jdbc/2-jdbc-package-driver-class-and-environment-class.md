---
title: JDBC包、驱动类和环境类
summary: JDBC包、驱动类和环境类
author: Guo Huan
date: 2021-04-26
---

# JDBC包、驱动类和环境类

<br/>

## JDBC包

在linux服务器端源代码目录下执行build.sh，获得驱动jar包postgresql.jar，包位置在源代码目录下。从发布包中获取，包名为[openGauss-x.x.x-JDBC.tar.gz](https://opengauss.org/zh/download/)。

驱动包与PostgreSQL保持兼容，其中类名、类结构与PostgreSQL驱动完全一致，曾经运行于PostgreSQL的应用程序可以直接移植到当前系统使用。

<br/>

## 驱动类

在创建数据库连接之前，需要加载数据库驱动类"org.postgresql.Driver"。

> **说明:**
> 由于MogDB在JDBC的使用上与PG的使用方法保持兼容，所以同时在同一进程内使用两个JDBC驱动的时候，可能会类名冲突。

<br/>

## 环境类

客户端需配置JDK1.8，配置方法如下:

1. DOS窗口输入"java -version"，查看JDK版本，确认为JDK1.8版本。如果未安装JDK，请从官方网站下载安装包并安装。若系统环境JDK版本低于1.8，请参考[WebSphere配置MogDB数据源参考](../../../quick-start/mogdb-access/use-middleware-to-access-mogdb/websphere-configures-mogdb-data-source-reference.md)。

2. 根据如下步骤配置系统环境变量。

    a. 右键单击"我的电脑"，选择"属性"。

    b. 在"系统"页面左侧导航栏单击"高级系统设置"。

    c. 在"系统属性"页面，"高级"页签上单击"环境变量"。

    d. 在"环境变量"页面上，"系统变量"区域单击"新建"或"编辑"配置系统变量。变量说明请参见表1。

**表 1** 变量说明

| 变量名    | 操作        | 变量值  |
| :-------- | :--------------- | :-------------------- |
| JAVA_HOME | - 若存在，则单击"编辑"。<br />- 若不存在，则单击"新建"。 | JAVA的安装目录。<br />例如: C:\\Program Files\\Java\\jdk1.8.0_131                                                                                                                    |
| Path      | 编辑                                                     | - 若配置了JAVA_HOME，则在变量值的最前面加上:  %JAVA_HOME%\\bin;<br />- 若未配置JAVA_HOME，则在变量值的最前面加上 JAVA安装的全路径: <br />C:\\Program Files\\Java\\jdk1.8.0_131\\bin; |
| CLASSPATH | 新建                                                     | ".;%JAVA_HOME%\\lib;%JAVA_HOME%\\lib\\tools.jar;"                                                                                                                                    |
