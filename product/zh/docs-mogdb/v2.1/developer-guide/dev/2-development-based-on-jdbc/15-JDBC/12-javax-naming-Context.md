---
title: javax.naming.Context
summary: javax.naming.Context
author: Guo Huan
date: 2021-05-17
---

# javax.naming.Context

javax.naming.Context是连接配置的上下文接口。

**表 1** 对javax.naming.Context的支持情况

| 方法名                                 | 返回值类型 | 支持JDBC 4 |
| :------------------------------------- | :--------- | :--------- |
| bind(Name name, Object obj)            | void       | Yes        |
| bind(String name, Object obj)          | void       | Yes        |
| lookup(Name name)                      | Object     | Yes        |
| lookup(String name)                    | Object     | Yes        |
| rebind(Name name, Object obj)          | void       | Yes        |
| rebind(String name, Object obj)        | void       | Yes        |
| rename(Name oldName, Name newName)     | void       | Yes        |
| rename(String oldName, String newName) | void       | Yes        |
| unbind(Name name)                      | void       | Yes        |
| unbind(String name)                    | void       | Yes        |
