---
title: 关闭连接
summary: 关闭连接
author: Guo Huan
date: 2021-06-07
---

# 关闭连接

在使用数据库连接完成相应的数据操作后，需要关闭数据库连接。

关闭数据库连接可以直接调用其close方法即可。如：**Connection conn = DriverManager.getConnection(“url”,“user”,“password”) ; conn.close()**；
