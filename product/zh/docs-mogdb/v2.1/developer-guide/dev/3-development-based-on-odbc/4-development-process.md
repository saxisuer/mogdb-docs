---
title: 开发流程
summary: 开发流程
author: Guo Huan
date: 2021-04-26
---

# 开发流程

**图 1** ODBC开发应用程序的流程

![ODBC开发应用程序的流程](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-process-3.png)

<br/>

## 开发流程中涉及的API

**表 1** 相关API说明

| **功能**                      | **API**                                                                                                                                                |
| :---------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------- |
| 申请句柄资源                  | SQLAllocHandle: 申请句柄资源，可替代如下函数: <br />- SQLAllocEnv: 申请环境句柄<br />- SQLAllocConnect: 申请连接句柄<br />- SQLAllocStmt: 申请语句句柄 |
| 设置环境属性                  | SQLSetEnvAttr                                                                                                                                          |
| 设置连接属性                  | SQLSetConnectAttr                                                                                                                                      |
| 设置语句属性                  | SQLSetStmtAttr                                                                                                                                         |
| 连接数据源                    | SQLConnect                                                                                                                                             |
| 绑定缓冲区到结果集的列中      | SQLBindCol                                                                                                                                             |
| 绑定SQL语句的参数标志和缓冲区 | SQLBindParameter                                                                                                                                       |
| 查看最近一次操作错误信息      | SQLGetDiagRec                                                                                                                                          |
| 为执行SQL语句做准备           | SQLPrepare                                                                                                                                             |
| 执行一条准备好的SQL语句       | SQLExecute                                                                                                                                             |
| 直接执行SQL语句               | SQLExecDirect                                                                                                                                          |
| 结果集中取行集                | SQLFetch                                                                                                                                               |
| 返回结果集中某一列的数据      | SQLGetData                                                                                                                                             |
| 获取结果集中列的描述信息      | SQLColAttribute                                                                                                                                        |
| 断开与数据源的连接            | SQLDisconnect                                                                                                                                          |
| 释放句柄资源                  | SQLFreeHandle: 释放句柄资源，可替代如下函数: <br />- SQLFreeEnv: 释放环境句柄<br />- SQLFreeConnect: 释放连接句柄<br />- SQLFreeStmt: 释放语句句柄     |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 数据库中收到的一次执行请求（不在事务块中），如果含有多条语句，将会被打包成一个事务，同时如果其中有一个语句失败，那么整个请求都将会被回滚。
