---
title: SQLAllocConnect
summary: SQLAllocConnect
author: Guo Huan
date: 2021-05-17
---

# SQLAllocConnect

在ODBC 3.x版本中，ODBC 2.x的函数SQLAllocConnect已被SQLAllocHandle代替。有关详细信息请参阅SQLAllocHandle。
