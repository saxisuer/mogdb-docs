---
title: SQLFreeStmt
summary: SQLFreeStmt
author: Guo Huan
date: 2021-05-17
---

# SQLFreeStmt

在ODBC 3.x版本中，ODBC 2.x的函数SQLFreeStmt已被SQLFreeHandle代替。有关详细信息请参阅SQLFreeHandle。
