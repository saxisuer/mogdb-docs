---
title: SQLBindParameter
summary: SQLBindParameter
author: Guo Huan
date: 2021-05-17
---

# SQLBindParameter

## 功能描述

将一条SQL语句中的一个参数标志和一个缓冲区绑定起来。

## 原型

```c
SQLRETURN SQLBindParameter(SQLHSTMT       StatementHandle,
                           SQLUSMALLINT   ParameterNumber,
                           SQLSMALLINT    InputOutputType,
                           SQLSMALLINT    ValuetType,
                           SQLSMALLINT    ParameterType,
                           SQLULEN    ColumnSize,
                           SQLSMALLINT    DecimalDigits,
                           SQLPOINTER     ParameterValuePtr,
                           SQLLEN     BufferLength,
                           SQLLEN     *StrLen_or_IndPtr);
```

## 参数

**表 1**  SQLBindParameter

| **关键词**        | **参数说明**                                                 |
| :---------------- | :----------------------------------------------------------- |
| StatementHandle   | 语句句柄。                                                   |
| ParameterNumber   | 参数序号，起始为1，依次递增。                                |
| InputOutputType   | 输入输出参数类型。                                           |
| ValueType         | 参数的C数据类型。                                            |
| ParameterType     | 参数的SQL数据类型。                                          |
| ColumnSize        | 列的大小或相应参数标记的表达式。                             |
| DecimalDigits     | 列的十进制数字或相应参数标记的表达式。                       |
| ParameterValuePtr | 指向存储参数数据缓冲区的指针。                               |
| BufferLength      | ParameterValuePtr指向缓冲区的长度，以字节为单位。            |
| StrLen_or_IndPtr  | 缓冲区的长度或指示器指针。若为空值，则未使用任何长度或指示器值。 |

## 返回值

- SQL_SUCCESS：表示调用正确。
- SQL_SUCCESS_WITH_INFO：表示会有一些警告信息。
- SQL_ERROR：表示比较严重的错误，如：内存分配失败、建立连接失败等。
- SQL_INVALID_HANDLE：表示调用无效句柄。其他API的返回值同理。

## 注意事项

当SQLBindParameter返回SQL_ERROR或SQL_SUCCESS_WITH_INFO时，通过调用SQLGetDiagRec函数，并将HandleType和Handle参数设置为SQL_HANDLE_STMT和StatementHandle，可得到一个相关的SQLSTATE值，通过SQLSTATE值可以查出调用此函数的具体信息。

## 示例

参见：ODBC - 示例
