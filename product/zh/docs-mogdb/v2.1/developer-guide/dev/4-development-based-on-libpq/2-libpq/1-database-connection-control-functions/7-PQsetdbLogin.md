---
title: PQsetdbLogin
summary: PQsetdbLogin
author: Guo Huan
date: 2021-05-17
---

# PQsetdbLogin

## 功能描述

与数据库服务器建立一个新的链接。

## 原型

```
PGconn *PQsetdbLogin(const char *pghost,
                     const char *pgport,
                     const char *pgoptions,
                     const char *pgtty,
                     const char *dbName,
                     const char *login,
                     const char *pwd);
```

## 参数

**表 1** PQsetdbLogin参数

| **关键字** | **参数说明**                                                 |
| :--------- | :----------------------------------------------------------- |
| pghost     | 要链接的主机名，详见链接字符章节描述的host字段。             |
| pgport     | 主机服务器的端口号，详见链接字符描述的port字段。             |
| pgoptions  | 添加命令行选项以在运行时发送到服务器，详见链接字符描述的options字段。 |
| pgtty      | 忽略（以前，这个选项声明服务器日志的输出方向）               |
| dbName     | 要链接的数据库名，详见链接字符描述的dbname字段。             |
| login      | 要链接的用户名，详见链接字符章节描述的user字段。             |
| pwd        | 如果服务器要求口令认证，所用的口令，详见链接字符描述的password字段。 |

## 返回值

PGconn *：指向包含链接的对象指针，内存在函数内部申请。

## 注意事项

- 该函数为PQconnectdb前身，参数个数固定，未定义参数被调用时使用缺省值，若需要给固定参数设置缺省值，则可赋值NULL或者空字符串。
- 若dbName中包含“=”或链接URL的有效前缀，则该dbName被看做一个conninfo字符串并传递至PQconnectdb中，其余参数与PQconnectdbParams保持一致。

## 示例

请参见示例章节。
