---
title: PQsendQueryPrepared
summary: PQsendQueryPrepared
author: Guo Huan
date: 2021-05-17
---

# PQsendQueryPrepared

## 功能描述

发送一个请求执行带有给出参数的预备语句，不等待结果。

## 原型

```c
int PQsendQueryPrepared(PGconn *conn,
                        const char *stmtName,
                        int nParams,
                        const char * const *paramValues,
                        const int *paramLengths,
                        const int *paramFormats,
                        int resultFormat);
```

## 参数

**表 1** PQsendQueryPrepared参数

| **关键字**   | **参数说明**                 |
| :----------- | :--------------------------- |
| conn         | 指向包含链接信息的对象指针。 |
| stmtName     | 需要执行的stmt名称。         |
| nParams      | 参数个数。                   |
| paramValues  | 参数值。                     |
| paramLengths | 参数长度。                   |
| paramFormats | 参数格式。                   |
| resultFormat | 结果的格式。                 |

## 返回值

int：执行结果为1表示成功，0表示失败，失败原因存到conn-&gt;error_message中。

## 注意事项

该函数类似于PQsendQueryParams，但是要执行的命令是通过命名一个预先准备的语句来指定的，而不是提供一个查询字符串。该函数的参数与PQexecPrepared一样处理。和PQexecPrepared一样，它也不能在2.0协议的连接上工作。

## 示例

请参见示例章节。
