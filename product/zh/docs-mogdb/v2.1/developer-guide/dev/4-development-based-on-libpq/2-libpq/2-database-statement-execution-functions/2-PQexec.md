---
title: PQexec
summary: PQexec
author: Guo Huan
date: 2021-05-17
---

# PQexec

## 功能描述

向服务器提交一条命令并等待结果。

## 原型

```
PGresult *PQexec(PGconn *conn, const char *command);
```

## 参数

**表 1** PQexec参数

| **关键字** | **参数说明**             |
| :--------- | :----------------------- |
| conn       | 指向包含链接的对象指针。 |
| command    | 需要执行的查询字符串。   |

## 返回值

PGresult：包含查询结果的对象指针。

## 注意事项

应该调用PQresultStatus函数来检查任何错误的返回值（包括空指针的值，在这种情况下它将返回PGRES_FATAL_ERROR）。使用PQerrorMessage获取有关错误的更多信息。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
> 命令字符串可以包括多个SQL命令（用分号分隔）。在一个PQexec调用中发送的多个查询是在一个事务里处理的，除非在查询字符串里有明确的BEGIN/COMMIT命令把整个字符串分隔成多个事务。请注意，返回的PGresult结构只描述字符串里执行的最后一条命令的结果，如果有一个命令失败，那么字符串处理的过程就会停止，并且返回的PGresult会描述错误条件。

## 示例

请参见示例章节。
