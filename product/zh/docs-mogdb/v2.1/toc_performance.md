<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## 性能优化指南

+ 系统优化指南
  + [操作系统参数调优](/performance-tuning/1-system/1-optimizing-os-parameters.md)
  + [数据库系统参数调优](/performance-tuning/1-system/2-optimizing-database-parameters.md)
  + [配置SMP](/performance-tuning/1-system/4-configuring-smp.md)
  + [配置LLVM](/performance-tuning/1-system/3-configuring-llvm.md)
+ SQL优化指南
  + [Query执行流程](/performance-tuning/2-sql/1-query-execution-process.md)
  + [SQL执行计划介绍](/performance-tuning/2-sql/2-introduction-to-the-sql-execution-plan.md)
  + [调优流程](/performance-tuning/2-sql/3-tuning-process.md)
  + [更新统计信息](/performance-tuning/2-sql/4-updating-statistics.md)
  + [审视和修改表定义](/performance-tuning/2-sql/5-reviewing-and-modifying-a-table-definition.md)
  + [典型SQL调优点](/performance-tuning/2-sql/6-typical-sql-optimization-methods.md)
  + [SQL语句改写规则](/performance-tuning/2-sql/7-experience-in-rewriting-sql-statements.md)
  + [SQL调优关键参数调整](/performance-tuning/2-sql/8-resetting-key-parameters-during-sql-tuning.md)
  + [使用Plan Hint进行调优](/performance-tuning/2-sql/9-hint-based-tuning.md)
+ [WDR解读指南](/performance-tuning/wdr-snapshot-schema.md)
+ [TPCC性能优化指南](/performance-tuning/TPCC-performance-tuning-guide.md)
