<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## 安装指南

+ [容器化安装](/installation-guide/docker-installation/docker-installation.md)
+ 极简安装
  + [安装准备](/installation-guide/simplified-installation-process/1-installation-preparation.md)
  + [单节点安装](/installation-guide/simplified-installation-process/2-installation-on-a-single-node.md)
  + [一主一备节点安装](/installation-guide/simplified-installation-process/3-installation-on-primary-standby-nodes.md)
+ 标准安装
  + [安装概述](/installation-guide/standard-installation/1-installation-overview.md)
  + 安装准备
    + [环境要求](/installation-guide/standard-installation/2-environment-requirement.md)
    + [修改操作系统配置](/installation-guide/standard-installation/3-modifying-os-configuration.md)
  + [安装MogDB](/installation-guide/standard-installation/4-installing-mogdb.md)
  + [安装验证](/installation-guide/standard-installation/5-verifying-installation.md)
  + [推荐参数设置及新建测试库](/installation-guide/standard-installation/7-recommended-parameter-settings.md)
  + [卸载MogDB](/installation-guide/standard-installation/6-uninstallation.md)
+ [手动安装](/installation-guide/manual-installation.md)
