---
title: WebLogic配置MogDB(PostgreSQL)数据源参考
summary: WebLogic配置MogDB(PostgreSQL)数据源参考
author: Guo Huan
date: 2021-08-11
---

# WebLogic配置MogDB(PostgreSQL)数据源参考

## 概述

WebLogic配置MogDB数据源参考，同样适用WebLogic配置PostgreSQL数据源，详细内容见下文描述。

<br/>

## 配置前提

### WebLogic JDK版本确认

```bash
$ /home/jdk1.7.0_80/bin/java -version
java version "1.7.0_80"
Java(TM) SE Runtime Environment (build 1.7.0_80-b15)
Java HotSpot(TM) 64-Bit Server VM (build 24.80-b11, mixed mode)
```

找到使用的JDK，并检查版本。

JDBC驱动针对不同的JDK大版本有不同的jar包，明确JDK大版本才能保证下载对应的驱动。

当前环境以1.7.0为测试环境。

### 下载对应JDBC驱动包

```
当前版本42.2.23。

这是驱动程序的当前版本。除非您有不寻常的需求（运行旧的应用程序或JVM），否则这就是您应该使用的驱动程序。它支持PostgreSQL 8.2或更高版本，并且需要Java 6或更高版本。它包含对SSL和javax.sql包的支持。

如果您使用的是Java8或更高版本，那么应该使用JDBC4.2版本。
如果您使用的是Java7，那么应该使用JDBC4.1版本。
如果您使用的是Java6，那么应该使用JDBC4.0版本。
```

各JDK对应下载JDBC版本链接如下：

| JDK版本 | JDBC驱动链接地址                                             |
| ------- | ------------------------------------------------------------ |
| 1.6     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre6.jar> |
| 1.7     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre7.jar> |
| 1.8     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jar>  |

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-1.png)

使用 42.2.23 是最佳选择，支持PostgreSQL 8.2或更高版本，而且是当前最稳定版本。

下载地址如下：[https://jdbc.postgresql.org/download/](https://jdbc.postgresql.org/download/)

<br/>

## 配置过程

### 确认db相关信息

| 数据库名 | 用户  | 密码   | 端口  | IP   |
| -------- | ----- | ------ | ----- | ---- |
| postgres | mogdb | 123456 | 25431 |      |

### 确认端口联通

在中间件主机telnet ip port ，确保能正常访问到db端口

### Console配置数据源

#### 添加驱动到CLASSPATH

a. 备份启动文件

```
cd ${DOMAIN_HOME}/bin
cp startWebLogic.sh startWebLogic.sh.bak
```

b. 上传驱动到WebLogic主机

 例如当前JDK版本为1.7，上传到指定路径

/home/postgresql-42.2.23.jre7.jar

c. 修改启动文件添加驱动包

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-2.png)

CLASSPATH="${SAVE_CLASSPATH}"

改为：

CLASSPATH="${SAVE_CLASSPATH}:/home/postgresql-42.2.23.jre7.jar"

d. 重启server生效

#### 配置数据源

a.   新建数据源

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-3.png)

b.  填写JNDI及相关信息

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-4.png)

c.   选择默认驱动

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-5.png)

d.  选默认

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-6.png)

e.   填写db信息

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-7.png)

f.   测试连接

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-8.png)

g.  确保测试成功

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-9.png)

h.  根据需求选择server

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-10.png)

i.   完成配置并激活更改生效

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-11.png)

<br/>

## 使用openGauss驱动连接MogDB

openGauss JDBC驱动只提供JDK 1.8版本，JDK 1.8的应用使用openGauss驱动能够正常连接使用MogDB。

如果是JDK 1.6、JDK 1.7的应用，那么只能使用PostgreSQL官方提供的JDBC驱动连接MogDB。

openGauss驱动下载地址：

[https://opengauss.org/zh/download/](https://opengauss.org/zh/download/)

<br/>

## 常见问题

当驱动版本高于JDK版本时，启动server报错如下

```
<xxx 10, 2021 10:43:53 PM CST> <Error> <Deployer> <BEA-149205> <Failed to initialize the application 'pg JDBC Data Source' due to error java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0.
java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0
        at java.lang.ClassLoader.defineClass1(Native Method)
        at java.lang.ClassLoader.defineClass(ClassLoader.java:800)
        at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
        at java.net.URLClassLoader.defineClass(URLClassLoader.java:449)
        at java.net.URLClassLoader.access$100(URLClassLoader.java:71)
        Truncated. see log file for complete stacktrace
Caused By: java.lang.UnsupportedClassVersionError: org/postgresql/Driver : Unsupported major.minor version 52.0
        at java.lang.ClassLoader.defineClass1(Native Method)
        at java.lang.ClassLoader.defineClass(ClassLoader.java:800)
        at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
        at java.net.URLClassLoader.defineClass(URLClassLoader.java:449)
        at java.net.URLClassLoader.access$100(URLClassLoader.java:71)
        Truncated. see log file for complete stacktrace
>
```
