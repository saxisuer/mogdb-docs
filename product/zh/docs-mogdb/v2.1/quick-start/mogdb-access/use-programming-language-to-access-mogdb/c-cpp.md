---
title: 使用C/C++访问MogDB
summary: 使用C/C++访问MogDB
author: Guo Huan
date: 2022-03-25
---



# 使用C/C++访问MogDB

libpq是PostgreSQL的C应用程序接口，是一套允许客户程序向PostgreSQL服务进程发送查询并且获得查询结果的库函数。libpq同时也是其他几个PostgreSQL应用接口下面的引擎， 包括C++、Perl、Python、Tcl和ECPG。

关于使用C/C++访问MogDB数据库的详细说明请参考《开发者指南》中[基于libpq开发](../../../developer-guide/dev/4-development-based-on-libpq/1-development-based-on-libpq.md)章节的内容。
