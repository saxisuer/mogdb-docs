---
title: 使用Go访问MogDB
summary: 使用Go访问MogDB
author: Guo Huan
date: 2022-03-31
---



# 使用Go访问MogDB

openGauss基于[lib/pq](https://github.com/lib/pq)提供了用于Go数据库/sql包的Go openGauss驱动程序，该驱动程序同样适用于Go语言连接MogDB。

您可以点击以下链接查看此驱动程序的源码及操作指导：

[源码地址](https://gitee.com/opengauss/openGauss-connector-go-pq)、[操作指导](https://gitee.com/opengauss/openGauss-connector-go-pq/blob/master/README.cn.md)
