---
title: 使用Rust访问MogDB
summary: 使用Rust访问MogDB
author: Guo Huan
date: 2022-03-31
---



# 使用Rust访问MogDB

openGauss基于[rust-postgres](https://github.com/sfackler/rust-postgres)提供了用于Rust的驱动程序，该驱动程序同样适用于Rust语言连接MogDB。

您可以点击以下链接查看此驱动程序的源码及操作指导：

[源码地址](https://gitee.com/opengauss/openGauss-connector-rust)、[操作指导](https://gitee.com/opengauss/openGauss-connector-rust/blob/master/README.md)
