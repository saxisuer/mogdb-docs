---
title: 使用DBeaver访问MogDB
summary: 使用DBeaver访问MogDB
author: Guo Huan
date: 2022-03-15
---

# 使用DBeaver访问MogDB

本文档介绍如何通过常用的图形化工具DBeaver访问MogDB数据库。

<br/>

## 工具介绍

Dbeaver是一个跨平台的数据库开发者工具，包括SQL编程、数据库管理和分析。它支持任意适配JDBC驱动的数据库系统。同时该工具也支持一些非JDBC的数据源，如MongoDB、Cassandra、Redis、DynamoDB等。

* 该工具提供了许多强大的特性，诸如元数据编辑器、SQL编辑器、富文本数据编辑器、ERD、数据导入/导出/迁移，SQL执行计划等；
* 该工具基于eclipse平台开发；
* 适配的数据库有MySQL/MariaDB、PostgreSQL、Greenplum、Oracle、DB2 LUW、Exasol、SQL Server、Sybase/SAP ASE、SQLite、Firebird、H2、HSQLDB、Derby、Teradata、Vertica、Netezza、Informix等。

<br/>

## 下载安装

Dbeaver是一款开源软件，下载地址如下：[https://dbeaver.io/download/](https://dbeaver.io/download/)

根据您的操作系统选择相应的安装包，下载完成后直接双击安装即可。

<br/>

## 配置连接串

首次打开软件会弹出“**创建新连接**”窗口，如下图所示。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-8.png)

您也可以点击“**数据库 -> 新建连接**”打开此窗口。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-9.png)

选择“**Popular -> PostgreSQL**”类型，点击“**下一步**”，在弹出的页面中下载PostgreSQL驱动文件。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-10.png)

下载完成后将弹出“**创建新连接**”窗口，在此窗口中填写数据库信息，然后点击“**完成**”即可建立数据库连接。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-11.png)

<br/>

## 界面展示

连接成功后的界面如下所示。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-12.png)

<br/>

## 后续步骤

如需了解更多关于DBeaver的使用说明，请点击“**帮助 -> 帮助内容**”，或点击以下页面在线浏览官方文档：[https://dbeaver.com/docs/wiki/](https://dbeaver.com/docs/wiki/)。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-13.png)
