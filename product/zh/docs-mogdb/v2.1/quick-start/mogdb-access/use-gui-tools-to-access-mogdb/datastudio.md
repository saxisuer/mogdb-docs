---
title: 使用Data Studio访问MogDB
summary: 使用Data Studio访问MogDB
author: Guo Huan
date: 2022-03-15
---

# 使用Data Studio访问MogDB

本文档介绍如何通过图形化工具Data Studio访问MogDB数据库。

<br/>

## 工具介绍

Data Studio是一个集成开发环境（IDE），帮助数据库开发人员便捷地构建应用程序，以图形化界面形式提供数据库关键特性。数据库开发人员仅需掌握少量的编程知识，即可使用该工具进行数据库对象操作。
Data Studio提供丰富多样的特性，例如：

- 创建和管理数据库对象
- 执行SQL语句/脚本
- 编辑和执行PL/SQL语句
- 图形化查看执行计划和开销
- 导出表数据等

创建和管理数据库对象，包括：

- 数据库
- 模式
- 函数
- 过程
- 表
- 序列
- 索引
- 视图
- 表空间
- 同义词

Data Studio还提供SQL助手用于在SQL终端和PL/SQLViewer中执行各种查询/过程/函数。

<br/>

## 系统要求

本节介绍使用Data Studio的最低系统要求。

**硬件要求**

表1 Data Studio硬件要求

| **硬件要求** | **配置**                                                     |
| ------------ | ------------------------------------------------------------ |
| CPU          | x86 64位                                                     |
| 可用内存     | 至少1GB内存                                                  |
| 可用硬盘     | Data Studio安装目录需要至少1GB空间，用户HOME目录需要至少100MB空间 |
| 网络         | 千兆以太网                                                   |

**操作系统要求**

运行在通用x86服务器上的Microsoft Windows系统，版本包括Windows 7 (64 位)、Windows 10 (64 位)、Windows 2012 (64 位)、Windows 2016 (64 位)。

**软件要求**

Java 1.8.0_181或更高版本。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> * 请参阅[https://java.com/en/download/help/path.xml](https://java.com/en/download/help/path.xml)以设置Java Home路径。
> * 保证最佳体验的情况下，推荐的最小屏幕分辨率是1080 x 768。低于此分辨率，界面会异常。

<br/>

## 下载安装

Data Studio的下载地址如下：<https://opengauss.org/zh/download/>

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/datastudio-1.png)

下载完成后解压安装包，双击**Data Studio.exe**即可运行软件。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-2.png)

<br/>

## 配置连接串

首次打开软件会弹出“**新建/选择数据库连接**”窗口，如下图所示。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-3.png)

您也可以点击“**文件 -> 新建连接**”打开此窗口。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-4.png)

填写右侧“**通用**”标签页中的各项信息，点击“**确定**”，即可建立数据库连接。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-5.png)

<br/>

## 界面展示

连接成功后的界面如下所示，右侧有语法和操作列表示例。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-6.png)

<br/>

## 后续步骤

如需了解更多关于Data Studio的使用说明，请点击“**帮助 -> 用户手册**”，或访问以下页面在线浏览官方文档：[Data Studio用户手册](https://opengauss.obs.cn-south-1.myhuaweicloud.com/2.0.0/Data%20Studio%20%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8C.pdf)。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/use-gui-tools-to-access-mogdb-7.png)
