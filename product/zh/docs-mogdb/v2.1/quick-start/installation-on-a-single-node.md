---
title: 单实例安装
summary: 单实例安装
author: Zhang Cuiping
date: 2021-04-2
---

# 单实例安装

本文介绍MogDB极简版安装流程，极简版安装主要针对高校和个人测试环境，相对企业安装流程更简单快捷。该软件包中不含OM工具，采用脚本就可以实现一键式安装部署。

<br/>

## 准备软硬件安装环境

**软件环境：** 访问[MogDB下载页面](https://www.mogdb.io/downloads/mogdb)下载对应平台的安装包。解压标准安装包后得到的`MogDB-x.x.x-openEuler-64bit.tar.bz2`即为极简安装包。

**硬件环境：** 个人开发者最低配置2核4G，推荐配置4核8G。

> 说明：
>
> 极简版为了适应小内存机器，在部署时将部分重要内存参数设置较低，如：“shared_buffers = 32MB”、“cstore_buffers = 512MB”。另外，极简版安装的数据库字符集将原先默认的SQL_ACSII字符集改为en_US.UTF-8，同时初始用户密码不做强制修改，即`modify_initial_password = false`。

<br/>

## 运行环境配置

1. 关闭防火墙，selinux

   ```
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   setenforce=0
   sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
   ```

2. 安装依赖包

   ```
   yum install libaio-devel flex bison ncurses-devel glibc-devel patch redhat-lsb-core readline-devel -y
   ```

3. 设置时区

   ```
   rm -fr /etc/localtime
   ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
   ll /etc/localtime
   ```

4. openEuler操作系统需要关闭RemoveIPC，CentOS操作系统默认为关闭，可以跳过该步骤。

   ```
   # 修改/etc/systemd/logind.conf文件中的“RemoveIPC”值为“no”。
   vim  /etc/systemd/logind.conf
   RemoveIPC=no
   
   # 修改/usr/lib/systemd/system/systemd-logind.service文件中的“RemoveIPC”值为“no”。
   vim /usr/lib/systemd/system/systemd-logind.service
   RemoveIPC=no
   
   # 重新加载配置参数。
   systemctl daemon-reload
   systemctl restart systemd-logind
   
   # 检查修改是否生效。
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

<br/>

## 安装步骤

1. 创建相关目录、用户、组，并授权。

   ```
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```
   
2. 解压极简版安装包到安装目录。

    ```
    su - omm
    cd /opt/software/mogdb/
    tar -jxf MogDB-x.x.x-openEuler-64bit.tar.bz2 -C /opt/software/mogdb/
    ```

3. 进入解压后目录下的simpleInstall。

    ```bash
    cd /opt/software/mogdb/simpleInstall/
    ```

4. 执行install.sh脚本安装MogDB。

    ```bash
    sh install.sh  -w xxxx
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    >
    > - -w: 初始化数据库密码（gs_initdb指定），安全需要必须设置。
    > - -p: 指定的MogDB端口号，如不指定，默认为5432。
    > - -h|-help 打印使用说明。
    > - 安装后，数据库的名称为sgnode。
    > - 安装后，数据库目录安装路径`/opt/software/mogdb/data/single_node`，其中`/opt/software/mogdb`为解压包路径，`data/single_node`为新创建的数据库节点目录。

<br/>

## 检查及使用数据库

```
## 配置PATH
echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
source ~/.bash_profile
-bash: ulimit: open files: cannot modify limit: Operation not permitted
（默认设置fd可用大小超过系统设置，可以忽略该设置）

## 登录数据库
gsql -d postgres -p 5432 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```