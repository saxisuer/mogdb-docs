---
title: PG接口兼容
summary: PG接口兼容
author: Guo Huan
date: 2022-05-07
---

# PG接口兼容

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

兼容PSQL客户端，兼容PostgreSQL标准接口。

## 客户价值

兼容PSQL客户端，兼容PostgreSQL标准接口，能够与PG生态工具无缝对接。

## 特性描述

兼容PSQL客户端，兼容PostgreSQL标准接口。

## 特性增强

无。

## 特性约束

无。

## 依赖关系

无。