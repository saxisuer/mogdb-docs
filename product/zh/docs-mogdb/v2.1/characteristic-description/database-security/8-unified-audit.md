---
title: 统一审计机制
summary: 统一审计机制
author: Guo Huan
date: 2022-05-07
---

# 统一审计机制

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

审计机制是行之有效的安全管理方案，可有效解决攻击者抵赖，审计的范围越大，可监控的行为就越多，而产生的审计日志就越多，影响实际审计效率。统一审计机制是一种通过定制化审计策略实现高效安全审计管理的技术。当管理员定义审计对象和审计行为后，用户执行的任务如果关联到对应的审计策略，则生成对应的审计行为，并记录审计日志。定制化审计策略可涵盖常见的用户管理活动、DDL和DML行为，满足日常审计诉求。

## 客户价值

审计是日常安全管理中必不可少的行为，当使用传统审计机制审计某种行为时，如SELECT，会导致产生大量的审计日志，进而增加整个系统的I/O，影响系统的性能；另一方面，大量的审计日志会影响管理员的审计效率。统一审计机制使得客户可以定制化生成审计日志的策略，如只审计数据库账户A查询某个表的行为。通过定制化审计，可以大大减少生成审计日志的数量，从而在保障审计行为的同时降低对系统性能的影响。而定制化审计策略可以提升管理员的审计效率。

## 特性描述

统一审计机制基于资源标签进行审计行为定制化，且将当前所支持的审计行为划分为access类和privileges类。一个完整的审计策略创建的SQL语法如下所示：

```
CREATE RESOURCE LABEL auditlabel add table(table_for_audit1, table_for_audit2);

CREATE AUDIT POLICY audit_select_policy ACCESS SELECT ON LABEL(auditlabel) FILTER ON ROLES(usera);

CREATE AUDIT POLICY audit_admin_policy PRIVILEGES ALTER, DROP ON LABEL(auditlabel) FILTER ON IP(local);
```

其中，auditlabel为本轮计划审计的资源标签，该资源标签中包含了两个表对象；audit_select_policy定义了用户usera对auditlabel对象的SELECT行为的审计策略，不区分访问源；audit_admin_policy定义了从本地对auditlabel对象进行ALTER和DROP操作行为的审计策略，不区分执行用户；当不指定ACCESS和PRIVILEGES的具体行为时，表示审计针对某一资源标签的所有支持的DDL和DML行为。当不指定具体的审计对象时，表示审计针对所有对象的操作行为。统一审计策略的增删改也会记录在统一审计日志中。

当前，统一审计支持的审计行为包括：

| SQL类型 | 支持操作和对象类型                                           |
| ------- | ------------------------------------------------------------ |
| **DDL** | 操作：ALL ALTER ANALYZE COMMENT CREATE DROP GRANT REVOKE<br/>SET SHOW<br/>对象：DATABASE SCHEMA FUNCTION TRIGGER TABLE SEQUENCE FOREIGN_SERVER FOREIGN_TABLE TABLESPACE ROLE/USER INDEX VIEW DATA_SOURCE |
| **DML** | 操作：ALL COPY DEALLOCATE DELETE EXECUTE REINDEX INSERT<br/>PREPARE SELECT TRUNCATE UPDATE |

## 特性增强

无。

## 特性约束

- 统一审计策略需要由具备POLADMIN或SYSADMIN属性的用户或初始用户创建，普通用户无访问安全策略系统表和系统视图的权限。
- 统一审计策略语法要么针对DDL行为，要么针对DML语法行为，同一个审计策略不可同时包含DDL和DML行为；统一审计策略目前支持最多设置98个。
- 统一审计监控用户通过客户端执行的SQL语句，而不会记录数据库内部SQL语句。
- 同一个审计策略下，相同资源标签可以绑定不同的审计行为，相同行为可以绑定不同的资源标签， 操作”ALL”类型包括DDL或者DML下支持的所有操作。
- 同一个资源标签可以关联不同的统一审计策略，统一审计会按照SQL语句匹配的策略依次打印审计信息。
- 统一审计策略的审计日志单独记录，暂不提供可视化查询接口，整个日志依赖于操作系统自带rsyslog服务，通过配置完成日志归档。
- 在云服务场景下，日志需要存储在OBS服务中；在混合云场景下，可部署Elastic Search进行日志收集和可视化处理。
- FILTER中的APP项建议仅在同一信任域内使用，由于客户端不可避免的可能出现伪造名称的情况，该选项使用时需要与客户端联合形成一套安全机制，减少误用风险。一般情况下不建议使用，使用时需要注意客户端仿冒的风险。
- FILTER中的IP地址以ipv4为例支持如下格式。

| ip地址格式 | 示例                     |
| ---------- | ------------------------ |
| 单ip       | 127.0.0.1                |
| 掩码表示ip | 127.0.0.1\|255.255.255.0 |
| cidr表示ip | 127.0.0.1/24             |
| ip区间     | 127.0.0.1-127.0.0.5      |

## 依赖关系

在公有云服务场景下，依赖OSS服务或OBS服务存储日志。