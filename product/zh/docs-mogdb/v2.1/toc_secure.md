<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## 安全指南

+ [客户端接入认证](/security-guide/security/1-client-access-authentication.md)
+ [管理用户及权限](/security-guide/security/2-managing-users-and-their-permissions.md)
+ [设置数据库审计](/security-guide/security/3-configuring-database-audit.md)
+ [设置密态等值查询](/security-guide/security/4-setting-encrypted-equality-query.md)
+ [设置账本数据库](/security-guide/security/5-setting-a-ledger-database.md)
