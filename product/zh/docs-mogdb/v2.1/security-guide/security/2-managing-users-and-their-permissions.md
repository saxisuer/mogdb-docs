---
title: 管理用户及权限
summary: 管理用户及权限
author: Guo Huan
date: 2021-03-04
---

# 管理用户及权限

<br/>

## 默认权限机制

数据库对象创建后，进行对象创建的用户就是该对象的所有者。MogDB安装后的默认情况下，未开启[三权分立](#三权分立)，数据库系统管理员具有与对象所有者相同的权限。也就是说对象创建后，默认只有对象所有者或者系统管理员可以查询、修改和销毁对象，以及通过GRANT将对象的权限授予其他用户。

为使其他用户能够使用对象，必须向用户或包含该用户的角色授予必要的权限。

MogDB支持以下的权限: SELECT、INSERT、UPDATE、DELETE、TRUNCATE、REFERENCES、CREATE、CONNECT、EXECUTE、ALTER、DROP、COMMENT、INDEX、VACUUM和USAGE。不同的权限与不同的对象类型关联。有关各权限的详细信息，请参见GRANT。

要撤消已经授予的权限，可以使用REVOKE。对象所有者的权限（例如ALTER、 DROP、COMMENT、INDEX、VACUUM、GRANT和REVOKE）是隐式的，无法授予或撤消。即只要拥有对象就可以执行对象所有者的这些隐式权限。对象所有者可以撤消自己的普通权限，例如，使表对自己以及其他人只读，系统管理员用户除外。

系统表和系统视图要么只对系统管理员可见，要么对所有用户可见。标识了需要系统管理员权限的系统表和视图只有系统管理员可以查询。 有关信息，请参考系统表和系统视图。

数据库提供对象隔离的特性，对象隔离特性开启时，用户只能查看有权限访问的对象(表、视图、字段、函数)，系统管理员不受影响。有关信息，请参考ALTER DATABASE。

<br/>

## 管理员

### 初始用户

数据库安装过程中自动生成的帐户称为初始用户。初始用户拥有系统的最高权限，能够执行所有的操作。如果安装时不指定初始用户名称则该帐户与进行数据库安装的操作系统用户同名。如果在安装时不指定初始用户的密码，安装完成后密码为空，在执行其他操作前需要通过gsql客户端修改初始用户的密码。如果初始用户密码为空，则除修改密码外无法执行其他SQL操作以及升级、扩容、节点替换等操作。

初始用户会绕过所有权限检查。建议仅将此初始用户作为DBA管理用途，而非业务应用。

### 系统管理员

系统管理员是指具有SYSADMIN属性的帐户，默认安装情况下具有与对象所有者相同的权限，但不包括dbe_perf模式的对象权限。

要创建新的系统管理员，请以初始用户或者系统管理员用户身份连接数据库，并使用带SYSADMIN选项的CREATE USER语句或ALTER USER语句进行设置。

```sql
mogdb=# CREATE USER sysadmin WITH SYSADMIN password "xxxxxxxxx";
```

或者

```sql
mogdb=# ALTER USER joe SYSADMIN;
```

> ALTER USER时，要求用户已存在。

### 监控管理员

监控管理员是指具有MONADMIN属性的帐户，具有查看dbe_perf模式下视图和函数的权限，亦可以对dbe_perf模式的对象权限进行授予或收回。

要创建新的监控管理员，请以系统管理员身份连接数据库，并使用带MONADMIN选项的CREATE USER语句或ALTER USER语句进行设置。

```sql
mogdb=# CREATE USER monadmin WITH MONADMIN password "xxxxxxxxx";
```

或者

```sql
mogdb=# ALTER USER joe MONADMIN;
```

> ALTER USER时，要求用户已存在。

### 运维管理员

运维管理员是指具有OPRADMIN属性的帐户，具有使用Roach工具执行备份恢复的权限。

要创建新的运维管理员，请以初始用户身份连接数据库，并使用带OPRADMIN选项的CREATE USER语句或ALTER USER语句进行设置。

```sql
mogdb=# CREATE USER opradmin WITH OPRADMIN password "xxxxxxxxx";
```

或者

```sql
mogdb=# ALTER USER joe OPRADMIN;
```

> ALTER USER时，要求用户已存在。

### 安全策略管理员

安全策略管理员是指具有POLADMIN属性的帐户，具有创建资源标签，脱敏策略和统一审计策略的权限。

要创建新的安全策略管理员，请以系统管理员用户身份连接数据库，并使用带POLADMIN选项的CREATE USER语句或ALTER USER语句进行设置。

```sql
mogdb=# CREATE USER poladmin WITH POLADMIN password "xxxxxxxxx";
```

或者

```sql
mogdb=# ALTER USER joe POLADMIN;
```

> ALTER USER时，要求用户已存在。

<br/>

## 三权分立

[默认权限机制](#默认权限机制)和[管理员](#管理员)两节的描述基于的是MogDB创建之初的默认情况。从前面的介绍可以看出，默认情况下拥有SYSADMIN属性的系统管理员，具备系统最高权限。

在实际业务管理中，为了避免系统管理员拥有过度集中的权利带来高风险，可以设置三权分立。将系统管理员的部分权限分立给安全管理员和审计管理员，形成系统管理员、安全管理员和审计管理员三权分立。

三权分立后，系统管理员将不再具有CREATEROLE属性（安全管理员）和AUDITADMIN属性（审计管理员）能力。即不再拥有创建角色和用户的权限，并不再拥有查看和维护数据库审计日志的权限。关于CREATEROLE属性和AUDITADMIN属性的更多信息请参考CREATE ROLE。

三权分立后，系统管理员只会对自己作为所有者的对象有权限。

初始用户的权限不受三权分立设置影响。因此建议仅将此初始用户作为DBA管理用途，而非业务应用。

三权分立的设置办法为: 将参数enableSeparationOfDuty设置为on。

三权分立前的权限详情及三权分立后的权限变化，请分别参见[表1](#表2.3.1)和[表2](#表2.3.2)。

**表 1** 默认的用户权限 <a id="表2.3.1"> </a>

<table>
  <tr>
    <th>对象名称</th>
    <th>初始用户（id为10）</th>
    <th>系统管理员</th>
    <th>监控管理员</th>
    <th>运维管理员</th>
    <th>安全策略管理员</th>
    <th>安全管理员</th>
    <th>审计管理员</th>
    <th>普通用户</th>
  </tr>
  <tr>
    <td>表空间</td>
    <td rowspan="7">具有除私有用户表对象访问权限外，所有的权限。</td>
    <td>对表空间有创建、修改、删除、访问、分配操作的权限。</td>
    <td colspan="6">不具有对表空间进行创建、修改、删除、分配的权限，访问需要被赋权。</td>
  </tr>
  <tr>
    <td>表</td>
    <td>对所有表有所有的权限。</td>
    <td colspan="6">仅对自己的表有所有的权限，对其他用户的表无权限。</td>
  </tr>
  <tr>
    <td>索引</td>
    <td>可以在所有的表上建立索引。</td>
    <td colspan="6">仅可以在自己的表上建立索引。</td>
  </tr>
  <tr>
    <td>模式</td>
    <td>对除dbe_perf以外的所有模式有所有的权限。</td>
    <td>仅对dbe_perf模式和自己的模式有所有的权限，对其他用户的模式无权限。</td>
    <td colspan="5">仅对自己的模式有所有的权限，对其他用户的模式无权限。</td>
  </tr>
  <tr>
    <td>函数</td>
    <td>对除dbe_perf模式下的函数以外的所有的函数有所有的权限。</td>
    <td>仅对dbe_perf模式下的和自己的函数有所有的权限，对其他用户放在public这个公共模式下的函数有调用的权限，对其他用户放在其他模式下的函数无权限。</td>
    <td colspan="5">仅对自己的函数有所有的权限，对其他用户放在public这个公共模式下的函数有调用的权限，对其他用户放在其他模式下的函数无权限。</td>
  </tr>
  <tr>
    <td>自定义视图</td>
    <td>对除dbe_perf模式下的视图以外的所有的视图有所有的权限。</td>
    <td>仅对dbe_perf模式下的和自己的视图有所有的权限，对其他用户的视图无权限。</td>
    <td colspan="5">仅对自己的视图有所有的权限，对其他用户的视图无权限。</td>
  </tr>
  <tr>
    <td>系统表和系统视图</td>
    <td>可以查看所有系统表和视图。</td>
    <td colspan="6">只可以查看部分系统表和视图。详细请参见系统表和系统视图。</td>
  </tr>
</table>

**表 2** 三权分立较非三权分立权限变化说明 <a id="表2.3.2"> </a>

<table>
  <tr>
    <th>对象名称</th>
    <th>初始用户（id为10）</th>
    <th>系统管理员</th>
    <th>监控管理员</th>
    <th>运维管理员</th>
    <th>安全策略管理员</th>
    <th>安全管理员</th>
    <th>审计管理员</th>
    <th>普通用户</th>
  </tr>
  <tr>
    <td>表空间</td>
    <td rowspan="7">无变化。<br/>依然具有除私有用户表对象访问权限外，所有的权限。</td>
    <td>无变化。</td>
    <td colspan="6">无变化。</td>
  </tr>
  <tr>
    <td>表</td>
    <td>权限缩小。<br/>只对自己的表及其他用户放在public模式下的表有所有的权限，对其他用户放在属于各自模式下的表无权限。</td>
    <td colspan="6">无变化。</td>
  </tr>
  <tr>
    <td>索引</td>
    <td>权限缩小。<br/>只可以在自己的表及其他用户放在public模式下的表上建立索引。</td>
    <td colspan="6">无变化。</td>
  </tr>
  <tr>
    <td>模式</td>
    <td>权限缩小。<br/>只对自己的模式有所有的权限，对其他用户的模式无权限。</td>
    <td>无变化。</td>
    <td colspan="5">无变化。</td>
  </tr>
  <tr>
    <td>函数</td>
    <td>权限缩小。<br/>只对自己的函数及其他用户放在public模式下的函数有所有的权限，对其他用户放在属于各自模式下的函数无权限。</td>
    <td>无变化。</td>
    <td colspan="5">无变化。</td>
  </tr>
  <tr>
    <td>自定义视图</td>
    <td>权限缩小。<br/>只对自己的视图及其他用户放在public模式下的视图有所有的权限，对其他用户放在属于各自模式下的视图无权限。</td>
    <td>无变化。</td>
    <td colspan="5">无变化。</td>
  </tr>
  <tr>
    <td>系统表和系统视图</td>
    <td>无变化。</td>
    <td colspan="6">无变化。</td>
  </tr>
</table>
<br/>

## 用户

使用CREATE USER和ALTER USER可以创建和管理数据库用户。MogDB包含一个或多个已命名数据库。用户和角色在整个MogDB范围内是共享的，但是其数据并不共享。即用户可以连接任何数据库，但当连接成功后，任何用户都只能访问连接请求里声明的那个数据库。

非[三权分立](#三权分立)下，MogDB用户帐户只能由系统管理员或拥有CREATEROLE属性的安全管理员创建和删除。三权分立时，用户帐户只能由初始用户和安全管理员创建。

在用户登录MogDB时会对其进行身份验证。用户可以拥有数据库和数据库对象（例如表），并且可以向用户和角色授予对这些对象的权限以控制谁可以访问哪个对象。除系统管理员外，具有CREATEDB属性的用户可以创建数据库并授予对这些数据库的权限。

**创建、修改和删除用户**

- 要创建用户，请使用SQL语句CREATE USER。

  例如: 创建用户joe，并设置用户拥有CREATEDB属性。

  ```sql
  mogdb=# CREATE USER joe WITH CREATEDB PASSWORD "XXXXXXXXX";
  CREATE ROLE
  ```

- 要创建系统管理员，请使用带有SYSADMIN选项的CREATE USER语句。

- 要删除现有用户，请使用DROP USER。

- 要更改用户帐户（例如，重命名用户或更改密码），请使用ALTER USER。

- 要查看用户列表，请查询视图PG_USER:

  ```sql
  mogdb=# SELECT * FROM pg_user;
  ```

- 要查看用户属性，请查询系统表PG_AUTHID:

  ```sql
  mogdb=# SELECT * FROM pg_authid;
  ```

**私有用户**

对于有多个业务部门，各部门间使用不同的数据库用户进行业务操作，同时有一个同级的数据库维护部门使用数据库管理员进行维护操作的场景下，业务部门可能希望在未经授权的情况下，管理员用户只能对各部门的数据进行控制操作（DROP、ALTER、TRUNCATE），但是不能进行访问操作（INSERT、DELETE、UPDATE、SELECT、COPY）。即针对管理员用户，表对象的控制权和访问权要能够分离，提高普通用户数据安全性。

[三权分立](#三权分立)情况下，管理员对其他用户放在属于各自模式下的表无权限。但是，这种无权限包含了无控制权限，因此不能满足上面的诉求。为此，MogDB提供了私有用户方案。即在非三权分立模式下，创建具有INDEPENDENT属性的私有用户。

```sql
mogdb=# CREATE USER user_independent WITH INDEPENDENT IDENTIFIED BY "1234@abc";
```

针对该用户的对象，系统管理员和拥有CREATEROLE属性的安全管理员在未经其授权前，只能进行控制操作（DROP、ALTER、TRUNCATE），无权进行INSERT、DELETE、SELECT、UPDATE、COPY、GRANT、REVOKE、ALTER OWNER操作。

**永久用户**

MogDB提供永久用户方案，即创建具有PERSISTENCE属性的永久用户。

```sql
mogdb=# CREATE USER user_persistence WITH persistence IDENTIFIED BY "1234@abc";
```

只允许初始用户创建、修改和删除具有PERSISTENCE属性的永久用户。

<br/>

## 角色

角色是一组用户的集合。通过GRANT把角色授予用户后，用户即具有了角色的所有权限。推荐使用角色进行高效权限分配。例如，可以为设计、开发和维护人员创建不同的角色，将角色GRANT给用户后，再向每个角色中的用户授予其工作所需数据的差异权限。在角色级别授予或撤消权限时，这些更改将作用到角色下的所有成员。

MogDB提供了一个隐式定义的拥有所有角色的组PUBLIC，所有创建的用户和角色默认拥有PUBLIC所拥有的权限。关于PUBLIC默认拥有的权限请参考GRANT。要撤销或重新授予用户和角色对PUBLIC的权限， 可通过在GRANT和REVOKE指定关键字PUBLIC实现。

要查看所有角色，请查询系统表PG_ROLES:

```sql
SELECT * FROM PG_ROLES;
```

**创建、修改和删除角色**

非[三权分立](#三权分立)时，只有系统管理员和具有CREATEROLE属性的用户才能创建、修改或删除角色。三权分立下，只有初始用户和具有CREATEROLE属性的用户才能创建、修改或删除角色。

- 要创建角色，请使用CREATE ROLE。
- 要在现有角色中添加或删除用户，请使用ALTER ROLE。
- 要删除角色，请使用DROP ROLE。DROP ROLE只会删除角色，并不会删除角色中的成员用户帐户。

**内置角色**

MogDB提供了一组默认角色，以gs_role_开头命名。它们提供对特定的、通常需要高权限的操作的访问，可以将这些角色GRANT给数据库内的其他用户或角色，让这些用户能够使用特定的功能。在授予这些角色时应当非常小心，以确保它们被用在需要的地方。[表3](#表1)描述了内置角色允许的权限范围：

**表 3** 内置角色权限描述<a id="表1"></a>

| 角色                     | 权限描述                                                     |
| :----------------------- | :----------------------------------------------------------- |
| gs_role_copy_files       | 具有执行copy … to/from filename 的权限，但需要先打开GUC参数enable_copy_server_files。 |
| gs_role_signal_backend   | 具有调用函数pg_cancel_backend、pg_terminate_backend和pg_terminate_session来取消或终止其他会话的权限，但不能操作属于初始用户和PERSISTENCE用户的会话。 |
| gs_role_tablespace       | 具有创建表空间（tablespace）的权限。                         |
| gs_role_replication      | 具有调用逻辑复制相关函数的权限，例如kill_snapshot、pg_create_logical_replication_slot、pg_create_physical_replication_slot、pg_drop_replication_slot、pg_replication_slot_advance、pg_create_physical_replication_slot_extern、pg_logical_slot_get_changes、pg_logical_slot_peek_changes、pg_logical_slot_get_binary_changes、pg_logical_slot_peek_binary_changes。 |
| gs_role_account_lock     | 具有加解锁用户的权限，但不能加解锁初始用户和PERSISTENCE用户。 |
| gs_role_pldebugger       | 具有执行dbe_pldebugger下调试函数的权限。                     |
| gs_role_directory_create | 具有执行创建directory对象的权限，但需要先打开GUC参数enable_access_server_directory。 |
| gs_role_directory_drop   | 具有执行删除directory对象的权限，但需要先打开GUC参数enable_access_server_directory。 |

关于内置角色的管理有如下约束：

- 以gs_role_开头的角色名作为数据库的内置角色保留名，禁止新建以gs_role_开头的用户/角色，也禁止将已有的用户/角色重命名为以gs_role_开头；

- 禁止对内置角色的ALTER和DROP操作；

- 内置角色默认没有LOGIN权限，不设预置密码；

- gsql元命令\\du和**\\dg**不显示内置角色的相关信息，但若显示指定了pattern为特定内置角色则会显示。

- 三权分立关闭时，初始用户、具有SYSADMIN权限的用户和具有内置角色ADMIN OPTION权限的用户有权对内置角色执行GRANT/REVOKE管理。三权分立打开时，初始用户和具有内置角色ADMIN OPTION权限的用户有权对内置角色执行GRANT/REVOKE管理。例如：

  ```sql
  GRANT gs_role_signal_backend TO user1;
  REVOKE gs_role_signal_backend FROM user1;
  ```

<br/>

## Schema

Schema又称作模式。通过管理Schema，允许多个用户使用同一数据库而不相互干扰，可以将数据库对象组织成易于管理的逻辑组，同时便于将第三方应用添加到相应的Schema下而不引起冲突。

每个数据库包含一个或多个Schema。数据库中的每个Schema包含表和其他类型的对象。数据库创建初始，默认具有一个名为public的Schema，且所有用户都拥有此Schema的usage权限，只有系统管理员和初始化用户可以在public Schema下创建函数、存储过程和同义词对象，其他用户即使赋予create权限后也不可以创建上述三种对象。可以通过Schema分组数据库对象。Schema类似于操作系统目录，但Schema不能嵌套。

相同的数据库对象名称可以应用在同一数据库的不同Schema中，而没有冲突。例如，a_schema和b_schema都可以包含名为mytable的表。具有所需权限的用户可以访问数据库的多个Schema中的对象。

CREATE USER创建用户的同时，系统会在执行该命令的数据库中，为该用户创建一个同名的SCHEMA。

数据库对象是创建在数据库搜索路径中的第一个Schema内的。有关默认情况下的第一个Schema情况及如何变更Schema顺序等更多信息，请参见[搜索路径](#搜索路径)。

**创建、修改和删除Schema**

- 要创建Schema，请使用CREATE SCHEMA。默认初始用户和系统管理员可以创建Schema，其他用户需要具备数据库的CREATE权限才可以在该数据库中创建Schema，赋权方式请参考GRANT中将数据库的访问权限赋予指定的用户或角色中的语法。

- 要更改Schema名称或者所有者，请使用ALTER SCHEMA。Schema所有者可以更改Schema。

- 要删除Schema及其对象，请使用DROP SCHEMA。Schema所有者可以删除Schema。

- 要在Schema内创建表，请以schema_name.table_name格式创建表。不指定schema_name时，对象默认创建到[搜索路径](#搜索路径)中的第一个Schema内。

- 要查看Schema所有者，请对系统表PG_NAMESPACE和PG_USER执行如下关联查询。语句中的schema_name请替换为实际要查找的Schema名称。

  ```sql
  mogdb=# SELECT s.nspname,u.usename AS nspowner FROM pg_namespace s, pg_user u WHERE nspname='schema_name' AND s.nspowner = u.usesysid;
  ```

- 要查看所有Schema的列表，请查询PG_NAMESPACE系统表。

  ```sql
  mogdb=# SELECT * FROM pg_namespace;
  ```

- 要查看属于某Schema下的表列表，请查询系统视图PG_TABLES。例如，以下查询会返回Schema PG_CATALOG中的表列表。

  ```sql
  mogdb=# SELECT distinct(tablename),schemaname from pg_tables where schemaname = 'pg_catalog';
  ```

**搜索路径** <a id="搜索路径"> </a>

搜索路径定义在search_path参数中，参数取值形式为采用逗号分隔的Schema名称列表。如果创建对象时未指定目标Schema，则将该对象会被添加到搜索路径中列出的第一个Schema中。当不同Schema中存在同名的对象时，查询对象未指定Schema的情况下，将从搜索路径中包含该对象的第一个Schema中返回对象。

- 要查看当前搜索路径，请使用SHOW。

  ```sql
  mogdb=# SHOW SEARCH_PATH;
   search_path
  ----------------
   "$user",public
  (1 row)
  ```

  search_path参数的默认值为: `"$user",public`。`$user`表示与当前会话用户名同名的Schema名，如果这样的模式不存在，`$user`将被忽略。所以默认情况下，用户连接数据库后，如果数据库下存在同名Schema，则对象会添加到同名Schema下，否则对象被添加到Public Schema下。

- 要更改当前会话的默认Schema，请使用SET命令。

  执行如下命令将搜索路径设置为myschema、public，首先搜索myschema。

  ```sql
  mogdb=# SET SEARCH_PATH TO myschema, public;
  SET
  ```

<br/>

## 用户权限设置

- 给用户直接授予某对象的权限，请使用GRANT。

  将Schema中的表或者视图对象授权给其他用户或角色时，需要将表或视图所属Schema的USAGE权限同时授予该用户或角色。否则用户或角色将只能看到这些对象的名称，并不能实际进行对象访问。

  例如，下面示例将Schema tpcds的权限赋给用户joe后，将表tpcds.web_returns的select权限赋给用户joe。

  ```sql
  mogdb=# GRANT USAGE ON SCHEMA tpcds TO joe;
  mogdb=# GRANT SELECT ON TABLE tpcds.web_returns to joe;
  ```

- 给用户指定角色，使用户继承角色所拥有的对象权限。

  1. 创建角色。

     新建一个角色lily，同时给角色指定系统权限CREATEDB:

     ```sql
     CREATE ROLE lily WITH CREATEDB PASSWORD "XXXXXXXXX";
     ```

  2. 给角色赋予对象权限，请使用GRANT。

     例如，将模式tpcds的权限赋给角色lily后，将表tpcds.web_returns的select权限赋给角色lily。

     ```sql
     mogdb=# GRANT USAGE ON SCHEMA tpcds TO lily;
     mogdb=# GRANT SELECT ON TABLE tpcds.web_returns to lily;
     ```

  3. 将角色的权限赋予用户。

     ```sql
     GRANT lily to joe;
     ```

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明:**
     > 当将角色的权限赋予用户时，角色的属性并不会传递到用户。

- 回收用户权限，请使用REVOKE。

<br/>

## 行级访问控制

行级访问控制特性将数据库访问控制精确到数据表行级别，使数据库达到行级访问控制的能力。不同用户执行相同的SQL查询操作，读取到的结果是不同的。

用户可以在数据表创建行访问控制(Row Level Security)策略，该策略是指针对特定数据库用户、特定SQL操作生效的表达式。当数据库用户对数据表访问时，若SQL满足数据表特定的Row Level Security策略，在查询优化阶段将满足条件的表达式，按照属性(PERMISSIVE | RESTRICTIVE)类型，通过AND或OR方式拼接，应用到执行计划上。

行级访问控制的目的是控制表中行级数据可见性，通过在数据表上预定义Filter，在查询优化阶段将满足条件的表达式应用到执行计划上，影响最终的执行结果。当前受影响的SQL语句包括SELECT，UPDATE，DELETE。

示例: 某表中汇总了不同用户的数据，但是不同用户只能查看自身相关的数据信息，不能查看其他用户的数据信息。

```sql
--创建用户alice, bob, peter
mogdb=# CREATE ROLE alice PASSWORD 'Gauss@123';
mogdb=# CREATE ROLE bob PASSWORD 'Gauss@123';
mogdb=# CREATE ROLE peter PASSWORD 'Gauss@123';

--创建表all_data，包含不同用户数据信息
mogdb=# CREATE TABLE all_data(id int, role varchar(100), data varchar(100));

--向数据表插入数据
mogdb=# INSERT INTO all_data VALUES(1, 'alice', 'alice data');
mogdb=# INSERT INTO all_data VALUES(2, 'bob', 'bob data');
mogdb=# INSERT INTO all_data VALUES(3, 'peter', 'peter data');

--将表all_data的读取权限赋予alice，bob和peter用户
mogdb=# GRANT SELECT ON all_data TO alice, bob, peter;

--打开行访问控制策略开关
mogdb=# ALTER TABLE all_data ENABLE ROW LEVEL SECURITY;

--创建行访问控制策略，当前用户只能查看用户自身的数据
mogdb=# CREATE ROW LEVEL SECURITY POLICY all_data_rls ON all_data USING(role = CURRENT_USER);

--查看表详细信息
mogdb=# \d+ all_data
                 Table "public.all_data"
 Column |      Type      | Modifiers | Storage  | Stats target | Description
--------+------------------------+-----------+----------+--------------+-------------
 id   | integer        |       | plain  |        |
 role   | character varying(100) |       | extended |        |
 data   | character varying(100) |       | extended |        |
Row Level Security Policies:
  POLICY "all_data_rls"
    USING (((role)::name = "current_user"()))
Has OIDs: no
Location Nodes: ALL DATANODES
Options: orientation=row, compression=no, enable_rowsecurity=true

--切换至用户alice，执行SQL"SELECT * FROM public.all_data"
mogdb=# SELECT * FROM public.all_data;
 id | role  |  data
----+-------+------------
  1 | alice | alice data
(1 row)

mogdb=# EXPLAIN(COSTS OFF) SELECT * FROM public.all_data;
               QUERY PLAN
----------------------------------------------------------------
 Streaming (type: GATHER)
   Node/s: All datanodes
   ->  Seq Scan on all_data
     Filter: ((role)::name = 'alice'::name)
 Notice: This query is influenced by row level security feature
(5 rows)

--切换至用户peter，执行SQL"SELECT * FROM public.all_data"
mogdb=# SELECT * FROM public.all_data;
 id | role  |  data
----+-------+------------
  3 | peter | peter data
(1 row)

mogdb=#  EXPLAIN(COSTS OFF) SELECT * FROM public.all_data;
               QUERY PLAN
----------------------------------------------------------------
 Streaming (type: GATHER)
   Node/s: All datanodes
   ->  Seq Scan on all_data
     Filter: ((role)::name = 'peter'::name)
 Notice: This query is influenced by row level security feature
(5 rows)
```

<br/>

## 设置安全策略

### 设置帐户安全策略

**背景信息**

MogDB为帐户提供了自动锁定和解锁帐户、手动锁定和解锁异常帐户和删除不再使用的帐户等一系列的安全措施，保证数据安全。

**自动锁定和解锁帐户**

- 为了保证帐户安全，如果用户输入密码次数超过一定次数（failed_login_attempts），系统将自动锁定该帐户，默认值为10。次数设置越小越安全，但是在使用过程中会带来不便。

- 当帐户被锁定时间超过设定值（password_lock_time），则当前帐户自动解锁，默认值为1天。时间设置越长越安全，但是在使用过程中会带来不便。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
  >
  > - 参数password_lock_time的整数部分表示天数，小数部分可以换算成时、分、秒。
  > - 当failed_login_attempts设置为0时，表示不限制密码错误次数。当password_lock_time设置为0时，表示即使超过密码错误次数限制导致帐户锁定，也会在短时间内自动解锁。因此，只有两个配置参数都为正数时，才可以进行常规的密码失败检查、帐户锁定和解锁操作。
  > - 这两个参数的默认值都符合安全标准，用户可以根据需要重新设置参数，提高安全等级。建议用户使用默认值。

配置failed_login_attempts参数。

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

   ```bash
   gsql -d postgres -p 8000
   ```

   postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

3. 查看已配置的参数。

   ```sql
   mogdb=# SHOW failed_login_attempts;
      failed_login_attempts
     -----------------------
      10
     (1 row)
   ```

   如果显示结果不为10，执行\\q命令退出数据库。

4. 执行如下命令设置成默认值10。

   ```bash
   gs_guc reload -D /mogdb/data/dbnode -c "failed_login_attempts=10"
   ```

配置password_lock_time参数。

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

   ```bash
   gsql -d postgres -p 8000
   ```

   postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

3. 查看已配置的参数。

   ```sql
   mogdb=# SHOW password_lock_time;
   password_lock_time
   -----------------------
    1
   (1 row)
   ```

   如果显示结果不为1，执行\\q命令退出数据库。

4. 执行如下命令设置成默认值1。

   ```bash
   gs_guc reload -N all -I all -c "password_lock_time=1"
   ```

**手动锁定和解锁帐户**

若管理员发现某帐户被盗、非法访问等异常情况，可手动锁定该帐户。

当管理员认为帐户恢复正常后，可手动解锁该帐户。

以手动锁定和解锁用户joe为例，用户的创建请参见[用户](#用户)，命令格式如下:

- 手动锁定

  ```sql
  mogdb=# ALTER USER joe ACCOUNT LOCK;
  ALTER ROLE
  ```

- 手动解锁

  ```sql
  mogdb=# ALTER USER joe ACCOUNT UNLOCK;
  ALTER ROLE
  ```

**删除不再使用的帐户**

当确认帐户不再使用，管理员可以删除帐户。该操作不可恢复。

当删除的用户正处于活动状态时，此会话状态不会立马断开，用户在会话状态断开后才会被完全删除。

以删除帐户joe为例，命令格式如下:

```sql
mogdb=# DROP USER joe  CASCADE;
DROP ROLE
```

<br/>

### 设置帐号有效期

<br/>

**注意事项**

创建新用户时，需要限制用户的操作期限（有效开始时间和有效结束时间）。

不在有效操作期内的用户需要重新设定帐号的有效操作期。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

   ```bash
   gsql -d postgres -p 8000
   ```

   postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

3. 创建用户并制定用户的有效开始时间和有效结束时间。

   ```sql
   CREATE USER joe WITH PASSWORD 'XXXXXXXXX' VALID BEGIN '2015-10-10 08:00:00' VALID UNTIL '2016-10-10 08:00:00';
   ```

   显示如下信息表示创建用户成功。

   ```sql
    CREATE ROLE
   ```

4. 用户已不在有效使用期内，需要重新设定帐号的有效期，这包括有效开始时间和有效结束时间。

   ```sql
   ALTER USER joe WITH VALID BEGIN '2016-11-10 08:00:00' VALID UNTIL '2017-11-10 08:00:00';
   ```

   显示如下信息表示重新设定成功。

   ```sql
   ALTER ROLE
   ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 若在"CREATE ROLE"或"ALTER ROLE"语法中不指定"VALID BEGIN"，表示不对用户的开始操作时间做限定；若不指定"VALID UNTIL"，表示不对用户的结束操作时间做限定；若两者均不指定，表示该用户一直有效。

<br/>

### 设置密码安全策略

<br/>

**操作步骤**

用户密码存储在系统表pg_authid中，为防止用户密码泄露，MogDB对用户密码进行加密存储，所采用的加密算法由配置参数password_encryption_type决定。

- 当参数password_encryption_type设置为0时，表示采用md5方式对密码加密。MD5加密算法安全性低，存在安全风险，不建议使用。
- 当参数password_encryption_type设置为1时，表示采用sha256和md5方式对密码加密。MD5加密算法安全性低，存在安全风险，不建议使用。
- 当参数password_encryption_type设置为2时，表示采用sha256方式对密码加密，为默认配置。
- 当参数password_encryption_type设置为3时，表示采用sm3方式对密码加密。

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

   ```bash
   gsql -d postgres -p 8000
   ```

   postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

3. 查看已配置的加密算法。

   ```sql
   mogdb=# SHOW password_encryption_type;
   password_encryption_type
   --------------------------
   2
   (1 row)
   ```

   如果显示结果为0或1，执行\\q命令退出数据库。

4. 执行如下命令将其设置为安全的加密算法。

   ```bash
   gs_guc reload -N all -I all -c "password_encryption_type=2"
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:** 为防止用户密码泄露，在执行CREATE USER/ROLE命令创建数据库用户时，不能指定UNENCRYPTED属性，即新创建的用户的密码只能是加密存储的。

5. 配置密码安全参数。

   - 密码复杂度

     初始化数据库、创建用户、修改用户时需要指定密码。密码必须要符合复杂度（password_policy）的要求，否则会提示用户重新输入密码。

     - 参数password_policy设置为1时表示采用密码复杂度校验，默认值。
     - 参数password_policy设置为0时表示不采用任何密码复杂度校验，设置为0会存在安全风险，不建议设置为0，即使需要设置也要将所有MogDB节点中的password_policy都设置为0才能生效。

     配置password_policy参数。

     a. 使用如下命令连接数据库。

     ```bash
     gsql -d postgres -p 8000
     ```

     postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

     b. 查看已配置的参数。

     ```sql
     mogdb=# SHOW password_policy;
     password_policy
     ---------------------
     1
     (1 row)
     ```

     如果显示结果不为1，执行\q命令退出数据库。

     c. 执行如下命令设置成默认值1。

     ```bash
     gs_guc reload -N all -I all -c "password_policy=1"
     ```

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
     >
     > 帐户密码的复杂度要求如下：
     >
     > - 包含大写字母（A-Z）的最少个数（password_min_uppercase）
     >
     > - 包含小写字母（a-z）的最少个数（password_min_lowercase）
     >
     > - 包含数字（0-9）的最少个数（password_min_digital）
     >
     > - 包含特殊字符的最少个数（password_min_special）（特殊字符的列表请参见[表3](#表2.9.3.1)。
     >
     > - 密码的最小长度（password_min_length）
     >
     > - 密码的最大长度（password_max_length）
     >
     > - 至少包含上述四类字符中的三类。
     >
     > - 不能和用户名、用户名倒写相同，本要求为非大小写敏感。
     >
     > - 不能和当前密码、当前密码的倒写相同。
     >
     > - 不能是弱口令。
     >
     > - 弱口令指的是强度较低，容易被破解的密码，对于不同的用户或群体，弱口令的定义可能会有所区别，用户需自己添加定制化的弱口令。
     >
     > - 弱口令字典中的口令存放在gs_global_config系统表中，当创建用户、修改用户需要设置密码时，系统将会把用户设置口令和弱口令字典中存放的口令进行对比，如果符合，则会提示用户该口令为弱口令，设置密码失败。
     >
     > - 弱口令字典默认为空，用户通过以下语法可以对弱口令字典进行增加和删除，示例
     >
     >   下：
     >
     >   ```sql
     >   mogdb=# CREATE WEAK PASSWORD DICTIONARY WITH VALUES ('password1'), ('password2');
     >   mogdb=# DROP WEAK PASSWORD DICTIONARY;
     >   ```

   - 密码重用

     用户修改密码时，只有超过不可重用天数（password_reuse_time）或不可重用次数（password_reuse_max）的密码才可以使用。参数配置说明如[表5](#表2.9.3.2)所示。

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 不可重用天数默认值为60天，不可重用次数默认值是0。这两个参数值越大越安全，但是在使用过程中会带来不便，其默认值符合安全标准，用户可以根据需要重新设置参数，提高安全等级。

     配置password_reuse_time参数。

     a. 使用如下命令连接数据库。

     ```bash
     gsql -d postgres -p 8000
     ```

     postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

     b. 查看已配置的参数。

     ```sql
     mogdb=# SHOW password_reuse_time;
     password_reuse_time
     ---------------------
     60
     (1 row)
     ```

     如果显示结果不为60，执行\q命令退出数据库。

     c. 执行如下命令设置成默认值60。

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 不建议设置为0，即使需要设置也要将所有MogDB节点中的password_reuse_time都设置为0才能生效。

     ```bash
     gs_guc reload -N all -I all -c "password_reuse_time=60"
     ```

     配置password_reuse_max参数。

     a. 使用如下命令连接数据库。

     ```bash
     gsql -d postgres -p 8000
     ```

     postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

     b. 查看已配置的参数。

     ```sql
     mogdb=# SHOW password_reuse_max;
     password_reuse_max
     --------------------
     0
     (1 row)
     ```

     如果显示结果不为0，执行\q命令退出数据库。

     c. 执行如下命令设置成默认值0。

     ```bash
     gs_guc reload -N all -I all -c "password_reuse_max = 0"
     ```

   - 密码有效期

     数据库用户的密码都有密码有效期（password_effect_time），当达到密码到期提醒天数（password_notify_time）时，系统会在用户登录数据库时提示用户修改密码。

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 考虑到数据库使用特殊性及业务连续性，密码过期后用户还可以登录数据库，但是每次登录都会提示修改密码，直至修改为止。

     配置password_effect_time参数。

     a. 使用如下命令连接数据库。

     ```bash
     gsql -d postgres -p 8000
     ```

     postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

     b. 查看已配置的参数。

     ```sql
     mogdb=# SHOW password_effect_time;
     password_effect_time
     ----------------------
     90
     (1 row)
     ```

     如果显示结果不为90，执行\q命令退出数据库。

     c. 执行如下命令设置成默认值90（不建议设置为0）。

     ```bash
     gs_guc reload -N all -I all -c "password_effect_time = 90"
     ```

     配置password_notify_time参数。

     a. 使用如下命令连接数据库。

     ```bash
     gsql -d postgres -p 8000
     ```

     postgres为需要连接的数据库名称，8000为数据库主节点的端口号。

     b. 查看已配置的参数。

     ```sql
     mogdb=# SHOW password_notify_time;
     password_notify_time
     ----------------------
     7
     (1 row)
     ```

     c. 如果显示结果不为7，执行如下命令设置成默认值7（不建议设置为0）。

     ```bash
     gs_guc reload -N all -I all -c "password_notify_time = 7"
     ```

   - 密码修改

     - 在安装数据库时，会新建一个和初始化用户重名的操作系统用户，为了保证帐户安全，请定期修改操作系统用户的密码。

       以修改用户user1密码为例，命令格式如下:

       ```sql
       passwd user1
       ```

       根据提示信息完成修改密码操作。

     - 建议系统管理员和普通用户都要定期修改自己的帐户密码，避免帐户密码被非法窃取。

       以修改用户user1密码为例，以系统管理员用户连接数据库并执行如下命令：

       ```sql
       mogdb=# ALTER USER user1 IDENTIFIED BY "1234@abc" REPLACE "5678@def";
       ALTER ROLE
       ```

       > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  1234@abc、5678@def分别代表用户user1的新密码和原始密码，这些密码要符合规则，否则会执行失败。

     - 管理员可以修改自己的或者其他帐户的密码。通过修改其他帐户的密码，解决用户密码遗失所造成无法登录的问题。

       以修改用户joe帐户密码为例，命令格式如下：

       ```sql
       mogdb=# ALTER USER joe IDENTIFIED BY "abc@1234";
       ALTER ROLE
       ```

       > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
       >
       > - 系统管理员之间不允许互相修改对方密码。
       >
       > - 系统管理员可以修改普通用户密码且不需要用户原密码。
       > - 系统管理员修改自己密码但需要管理员原密码。

   - 密码验证

     设置当前会话的用户和角色时，需要验证密码。如果输入密码与用户的存储密码不一致，则会报错。

     以设置用户joe为例，命令格式如下：

     ```sql
     mogdb=# SET ROLE joe PASSWORD "abc@1234";
     ERROR:  Invalid username/password,set role denied.
     ```

     **表 4** 特殊字符<a id="表2.9.3.1"> </a>

     | 编号 | 字符 | 编号 | 字符       | 编号 | 字符 | 编号 | 字符 |
     | :--- | :--- | :--- | :--------- | :--- | :--- | :--- | :--- |
     | 1    | ~    | 9    | *          | 17   | \|   | 25   | &lt; |
     | 2    | ！   | 10   | (          | 18   | [    | 26   | .    |
     | 3    | @    | 11   | )          | 19   | {    | 27   | &gt; |
     | 4    | #    | 12   | -          | 20   | }    | 28   | /    |
     | 5    | $    | 13   | _          | 21   | ]    | 29   | ？   |
     | 6    | %    | 14   | =          | 22   | ；   | -    | -    |
     | 7    | ^    | 15   | +          | 23   | :    | -    | -    |
     | 8    | &    | 16   | &lt;/p&gt; | 24   | ，   | -    | -    |

     **表 5** 不可重用天数和不可重用次数参数说明<a id="表2.9.3.2"> </a>

     | 参数                                | 取值范围                                                     | 配置说明                                                     |
     | :---------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
     | 不可重用天数（password_reuse_time） | 正数或0，其中整数部分表示天数，小数部分可以换算成时，分，秒。<br/>默认值为60。 | - 如果参数变小，则后续修改密码按新的参数进行检查。<br/>- 如果参数变大（比如由a变大为b），因为b天之前的历史密码可能已经删除，所以b天之前的密码仍有可能被重用。则后续修改密码按新的参数进行检查。<br/>说明:<br/>时间以绝对时间为准，历史密码记录的都是当时的时间，不识别时间的修改。 |
     | 不可重用次数（password_reuse_max）  | 正整数或0。<br/>默认值为0，表示不检查重用次数。              | - 如果参数变小，则后续修改密码按新的参数进行检查。<br/>- 如果参数变大（比如由a变大为b），因为b次之前的历史密码可能已经删除，所以b次之前的密码仍有可能被重用。则后续修改密码按新的参数进行检查。 |

6. 设置用户密码失效。

   具有CREATEROLE权限的用户在创建用户时可以强制用户密码失效，新用户首次登陆数据库后需要修改密码才允许执行其他查询操作，命令格式如下：

   ```sql
   mogdb=# CREATE USER joe PASSWORD "abc@1234" EXPIRED;
   CREATE ROLE
   ```

   具有CREATEROLE权限的用户可以强制用户密码失效或者强制修改密码且失效，命令格式如下：

   ```sql
   mogdb=# ALTER USER joe PASSWORD EXPIRED;
   ALTER ROLE
   mogdb=# ALTER USER joe PASSWORD "abc@2345" EXPIRED;
   ALTER ROLE
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - 密码失效的用户登录数据库后，当执行简单查询或者扩展查询时，会提示用户修改密码。修改密码后可以正常执行语句。
   > - 只有初始用户、系统管理员（sysadmin）或拥有创建用户（CREATEROLE）权限的用户才可以设置用户密码失效，其中系统管理员也可以设置自己或其他系统管理员密码失效。不允许设置初始用户密码失效。
