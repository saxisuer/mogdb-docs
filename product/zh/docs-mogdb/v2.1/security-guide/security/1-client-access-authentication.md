---
title: 客户端接入认证
summary: 客户端接入认证
author: Guo Huan
date: 2021-03-04
---

# 客户端接入认证

<br/>

## 配置客户端接入认证

**背景信息**

如果主机需要远程连接数据库，必须在数据库系统的配置文件中增加此主机的信息，并且进行客户端接入认证。配置文件（默认名称为pg_hba.conf）存放在数据库的数据目录里。hba（host-based authentication）表示是基于主机的认证。

- 本产品支持如下三种认证方式，这三种方式都需要配置"pg_hba.conf"文件。
  - 基于主机的认证: 服务端根据客户端的IP地址、用户名及要访问的数据库来查看配置文件从而判断用户是否通过认证。
  - 口令认证: 包括远程连接的加密口令认证和本地连接的非加密口令认证。
  - SSL加密: 使用OpenSSL（开源安全通信库）提供服务端和客户端安全连接的环境。
- "pg_hba.conf"文件的格式是一行写一条信息，表示一个认证规则，空白和注释（以#开头）被忽略。
- 每个认证规则是由若干空格和/，空格和制表符分隔的字段组成。如果字段用引号包围，则它可以包含空白。一条记录不能跨行存在。

<br/>

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 配置客户端认证方式，允许客户端以"jack"用户连接到本机，此处远程连接禁止使用"omm"用户（即数据库初始化用户）。

    例如，下面示例中配置允许IP地址为10.10.0.30的客户端访问本机。

    ```bash
    gs_guc set -N all -I all -h "host all jack 10.10.0.30/32 sha256"
    ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用"jack"用户前，需先本地连接数据库，并在数据库中使用如下语句建立"jack"用户:
>
>     ```sql
>     mogdb=# CREATE USER jack PASSWORD 'Test@123';
>     ```
>
> - -N all表示MogDB的所有主机。
> - -I all表示主机的所有实例。
> - -h表示指定需要在"pg_hba.conf"增加的语句。
> - all表示允许客户端连接到任意的数据库。
> - jack表示连接数据库的用户。
> - 10.10.0.30/32表示只允许IP地址为10.10.0.30的主机连接。此处的IP地址不能为MogDB内的IP，在使用过程中，请根据用户的网络进行配置修改。32表示子网掩码为1的位数，即255.255.255.255。
> - sha256表示连接时jack用户的密码使用sha256算法加密。

这条命令在数据库主节点实例对应的"pg_hba.conf"文件中添加了一条规则，用于对连接数据库主节点的客户端进行鉴定。

"pg_hba.conf"文件中的每条记录可以是下面四种格式之一，四种格式的参数说明请参见[配置文件参考](#配置文件参考)。

```text
local     DATABASE USER METHOD [OPTIONS]
host      DATABASE USER ADDRESS METHOD [OPTIONS]
hostssl   DATABASE USER ADDRESS METHOD [OPTIONS]
hostnossl DATABASE USER ADDRESS METHOD [OPTIONS]
```

因为认证时系统是为每个连接请求顺序检查"pg_hba.conf"里的记录的，所以这些记录的顺序是非常关键的。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 在配置"pg_hba.conf"文件时，请依据通讯需求按照格式内容从上至下配置记录，优先级高的需求需要配置在前面。MogDB和扩容配置的IP优先级最高，用户手动配置的IP请放在这二者之后，如果已经进行的客户配置和扩容节点的IP在同一网段，请在扩容前删除，扩容成功后再进行配置。

因此对于认证规则的配置建议如下:

- 靠前的记录有比较严格的连接参数和比较弱的认证方法。
- 靠后的记录有比较宽松的连接参数和比较强的认证方法。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 一个用户要想成功连接到特定的数据库，不仅需要通过pg_hba.conf中的规则检查，还必须要有该数据库上的CONNECT权限。如果希望控制某些用户只能连接到指定数据库，赋予/撤销CONNECT权限通常比在pg_hba.conf中设置规则更为简单。
> - 对应MogDB外部客户端连接，trust为不安全的认证方式，请将认证方式设置为sha256。

<br/>

**异常处理**

用户认证失败有很多原因，通过服务端返回给客户端的提示信息，可以看到用户认证失败的原因。常见的错误提示请参见[表1](#表1.1.1)。

**表 1** 错误提示<a id="表1.1.1"> </a>

| 问题现象             | 解决方法                                |
| :------------------------- | :------------------------------- |
| 用户名或密码错误: <br/>`FATAL: invalid username/password,login denied`                                          | 这条信息说明用户名或者密码错误，请检查输入是否有误。                                                                                                             |
| 连接的数据库不存在: <br/>`FATAL: database "TESTDB" does not exist`                                              | 这条信息说明尝试连接的数据库不存在，请检查连接的数据库名输入是否有误。                                                                                           |
| 未找到客户端匹配记录: <br/>`FATAL: no pg_hba.conf entry for host "10.10.0.60", user "ANDYM", database "TESTDB"` | 这条信息说明已经连接了服务器，但服务器拒绝了连接请求，因为没有在它的pg_hba.conf配置文件里找到匹配的记录。请联系数据库管理员在pg_hba.conf配置文件加入用户的信息。 |

<br/>

**示例**

```
TYPE  DATABASE        USER            ADDRESS                 METHOD

#表示只允许在安装时-U参数指定的用户（默认是omm用户）从服务器本机进行连接。
local   all             omm                                     trust
IPv4 local connections:
#表示允许jack用户从10.10.0.50主机上连接到任意数据库，使用sha256算法对密码进行加密。
host    all           jack             10.10.0.50/32            sha256
#表示允许任何用户从10.10.0.0/24网段的主机上连接到任意数据库，使用sha256算法对密码进行加密，并且经过SSL加密传输。
hostssl    all             all             10.10.0.0/24            sha256
#表示允许任何用户从10.10.0.0/24网段的主机上连接到任意数据库，使用Kerberos认证方式，当前版本暂不支持客户端kerberos认证。
host    all             all             10.10.0.0/24            gss         include_realm=1        krb_realm=HADOOP.COM
```

<br/>

## 配置文件参考

**表 2** 参数说明

| 参数名称    | 描述        | 取值范围  |
| :---------- | :------------------- | :------------------|
| local       | 表示这条记录只接受通过Unix域套接字进行的连接。没有这种类型的记录，就不允许Unix域套接字的连接。<br/>只有在从服务器本机使用gsql连接且在不指定-U参数的情况下，才是通过Unix域套接字连接。 | -   |
| host        | 表示这条记录既接受一个普通的TCP/IP套接字连接，也接受一个经过SSL加密的TCP/IP套接字连接。   | - |
| hostssl     | 表示这条记录只接受一个经过SSL加密的TCP/IP套接字连接。 | 用SSL进行安全的连接，需要配置申请数字证书并配置相关参数，详细信息请参见[用SSL进行安全的TCP/IP连接](#用SSL进行安全的TCP/IP连接)。|
| hostnossl   | 表示这条记录只接受一个普通的TCP/IP套接字连接。    | -     |
| DATABASEGUC | 声明记录所匹配且允许访问的数据库。         | - all: 表示该记录匹配所有数据库。<br/>- sameuser: 表示如果请求访问的数据库和请求的用户同名，则匹配。<br/>- samerole: 表示请求的用户必须是与数据库同名角色中的成员。<br/>- samegroup: 与samerole作用完全一致，表示请求的用户必须是与数据库同名角色中的成员。<br/>- 一个包含数据库名的文件或者文件中的数据库列表: 文件可以通过在文件名前面加前缀@来声明。文件中的数据库列表以逗号或者换行符分隔。<br/>- 特定的数据库名称或者用逗号分隔的数据库列表。<br/>说明:<br/>值replication表示如果请求一个复制链接，则匹配，但复制链接不表示任何特定的数据库。如需使用名为replication的数据库，需在database列使用记录"replication"作为数据库名。 |
| USER        | 声明记录所匹配且允许访问的数据库用户。                                                                                                                                                | - all: 表明该记录匹配所有用户。<br/>- +用户角色: 表示匹配任何直接或者间接属于这个角色的成员。<br/>说明:<br/>+表示前缀符号。<br/>- 一个包含用户名的文件或者文件中的用户列表: 文件可以通过在文件名前面加前缀@来声明。文件中的用户列表以逗号或者换行符分隔。<br/>- 特定的数据库用户名或者用逗号分隔的用户列表。   |
| ADDRESS     | 指定与记录匹配且允许访问的IP地址范围。                                                                                                                                                | 支持IPv4和IPv6，可以使用如下两种形式来表示: <br/>- IP地址/掩码长度。例如，10.10.0.0/24<br/>- IP地址子网掩码。例如，10.10.0.0 255.255.255.0<br/>说明:<br/>以IPv4格式给出的IP地址会匹配那些拥有对应地址的IPv6连接，比如127.0.0.1将匹配IPv6地址 ::ffff:127.0.0.1  |
| METHOD      | 声明连接时使用的认证方法。                                                                                                                                                            | 本产品支持如下几种认证方式，详细解释请参见[表3](#表1.2.2): <br/>- trust<br/>- reject<br/>- md5（不推荐使用，默认不支持，可通过password_encryption_type参数配置）<br/>说明：<br />MD5加密算法安全性低，存在安全风险，建议使用更安全的加密算法。<br />- sha256<br/>- sm3<br />- cert<br/>- gss（仅用于MogDB内部节点间认证）<br />- peer (仅用于local模式)                |

<br/>

**表 3** 认证方式<a id="表1.2.2"> </a>

| 认证方式 | 说明  |
| :------------ | :----------------------------------------------- |
| trust    | 采用这种认证模式时，本产品只完全信任从服务器本机使用gsql且不指定-U参数的连接，此时不需要口令。<br/>trust认证对于单用户工作站的本地连接是非常合适和方便的，通常不适用于多用户环境。如果想使用这种认证方法，可利用文件系统权限限制对服务器的Unix域套接字文件的访问。要使用这种限制有两个方法: <br/>- 设置参数unix_socket_permissions和unix_socket_group。<br/>- 设置参数unix_socket_directory，将Unix域套接字文件放在一个经过恰当限制的目录里。<br/>须知: 设置文件系统权限只能Unix域套接字连接，它不会限制本地TCP/IP连接。为保证本地TCP/IP安全，MogDB不允许远程连接使用trust认证方法。 |
| reject   | 无条件地拒绝连接。常用于过滤某些主机。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| md5      | 要求客户端提供一个md5加密的口令进行认证。<br/>须知: <br/>- MD5加密算法安全性低，存在安全风险，建议使用更安全的加密算法。<br />- MogDB保留md5认证和密码存储，是为了便于第三方工具的使用（比如TPCC评测工具）。                                                                                                                                                                                                                                                                                                                                    |
| sha256   | 要求客户端提供一个sha256算法加密的口令进行认证，该口令在传送过程中结合salt（服务器发送给客户端的随机数）的单向sha256加密，增强了安全性。                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| sm3 | 要求客户端提供一个sm3算法加密口令进行认证，该口令在传送过程中结合salt（服务器发送给客户端的随机数）的单项sm3的加密，增加了安全性。 |
| cert     | 客户端证书认证模式，此模式需进行SSL连接配置且需要客户端提供有效的SSL证书，不需要提供用户密码。<br/>须知: <br/>该认证方式只支持hostssl类型的规则。                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| gss      | 使用基于gssapi的kerberos认证。<br/>须知: <br/>- 该认证方式依赖kerberos server等组件，仅支持MogDB内部通信认证。当前版本暂不支持外部客户端通过kerberos认证连接。<br/>- 开启MogDB内部kerberos认证会使增加内部节点建连时间，即影响首次涉及内部建连的SQL操作性能，内部连接建立好后， 后续操作不受影响。                                                                                                                                                                                                                                                                                   |
| peer | 获取客户端所在操作系统用户名，并检查与数据库初始用户名是否一致。此方式只支持local模式本地连接，并支持通过配置pg_ident.conf建立操作系统用户与数据库用户映射关系。<br />假设操作系统用户名为omm，数据库初始用户为dbAdmin，在pg_hba.conf中配置local模式为peer认证：<br />`local   all    all        peer    map=mymap`<br />其中map=mymap指定使用的用户名映射，并在pg_ident.conf中添加映射名称为mymap的用户名映射如下：<br />`# MAPNAME       SYSTEM-USERNAME         PG-USERNAME mymap                omm                                  dbAdmin`<br />说明：<br />通过gs_guc reload方式修改pg_hba.conf配置可以立即生效无需重启数据库。直接编辑修改pg_ident.conf配置后下次连接时自动生效无需重启数据库。 |

<br/>

## 用SSL进行安全的TCP/IP连接

**背景信息**

MogDB支持SSL标准协议（TLS 1.2），SSL协议是安全性更高的协议标准，它们加入了数字签名和数字证书来实现客户端和服务器的双向身份验证，保证了通信双方更加安全的数据传输。

<br/>

**前提条件**

从CA认证中心申请到正式的服务器、客户端的证书和密钥。（假设服务器的私钥为server.key，证书为server.crt，客户端的私钥为client.key，证书为client.crt，CA根证书名称为cacert.pem。）

<br/>

**注意事项**

- 当用户远程连接到数据库主节点时，需要使用sha256的认证方式。
- 当内部服务器之间连接时，需要使用trust的认证方式，支持IP白名单认证。

<br/>

**操作步骤**

MogDB在MogDB部署完成后，默认已开启SSL认证模式。服务器端证书，私钥以及根证书已经默认配置完成。用户需要配置客户端的相关参数。

配置SSL认证相关的数字证书参数，具体要求请参见[表4](#表1.3.1)。

- 配置客户端参数。

    已从CA认证中心申请到客户端默认证书，私钥，根证书以及私钥密码加密文件。假设证书、私钥和根证书都放在"/home/omm"目录。

    双向认证需配置如下参数:

    ```bash
    export PGSSLCERT="/home/omm/client.crt"
    export PGSSLKEY="/home/omm/client.key"
    export PGSSLMODE="verify-ca"
    export PGSSLROOTCERT="/home/omm/cacert.pem"
    ```

    单向认证需要配置如下参数:

    ```bash
    export PGSSLMODE="verify-ca"
    export PGSSLROOTCERT="/home/omm/cacert.pem"
    ```

- 修改客户端密钥的权限。

    客户端根证书，密钥，证书以及密钥密码加密文件的权限，需保证权限为600。如果权限不满足要求，则客户端无法以SSL连接到MogDB。

    ```bash
    chmod 600 client.key
    chmod 600 client.crt
    chmod 600 client.key.cipher
    chmod 600 client.key.rand
    chmod 600 cacert.pem
    ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 从安全性考虑，建议使用双向认证方式。
> 配置客户端环境变量，必须包含文件的绝对路径。

**表 4** 认证方式<a id="表1.3.1"> </a>

| 认证方式         | 含义         | 配置客户端环境变量            | 维护建议           |
| :---------------------- | :----------------------------| :------------------------------- |:------------------------------- |
| 双向认证（推荐） | 客户端验证服务器证书的有效性，同时服务器端也要验证客户端证书的有效性，只有认证成功，连接才能建立。                                                 | 设置如下环境变量: <br/>- PGSSLCERT<br/>- PGSSLKEY<br/>- PGSSLROOTCERT<br/>- PGSSLMODE | 该方式应用于安全性要求较高的场景。使用此方式时，建议设置客户端的PGSSLMODE变量为verify-ca。确保了网络数据的安全性。       |
| 单向认证         | 客户端只验证服务器证书的有效性，而服务器端不验证客户端证书的有效性。服务器加载证书信息并发送给客户端，客户端使用根证书来验证服务器端证书的有效性。 | 设置如下环境变量: <br/>- PGSSLROOTCERT<br/>- PGSSLMODE                              | 为防止基于TCP链接的欺骗，建议使用SSL证书认证功能。除配置客户端根证书外，建议客户端使用PGSSLMODE变量为verify-ca方式连接。 |

<br/>

**相关参考**

在服务器端的postgresql.conf文件中配置相关参数，详细信息请参见[表5](#表1.3.2)。

**表 5** 服务器参数<a id="表1.3.2"> </a>

| 参数          | 描述            | 取值范围   |
| :------------ | :-------------------- | :-------------------|
| ssl           | 表示是否启动SSL功能。                                                                                              | - on: 开启SSL功能。<br/>- off: 关闭SSL功能。<br/> **默认值** : on                                                                                      |
| require_ssl   | 设置服务器端是否强制要求SSL连接。该参数只有当参数ssl为on时才有效。                                                 | - on: 服务器端强制要求SSL连接。<br/>- off: 服务器端对是否通过SSL连接不作强制要求。<br/> **默认值** :  off                                              |
| ssl_cert_file | 指定服务器证书文件，包含服务器端的公钥。服务器证书用以表明服务器身份的合法性，公钥将发送给对端用来对数据进行加密。 | 请以实际的证书名为准。其相对路径是相对于数据目录的。<br/> **默认值** : server.crt                                                  |
| ssl_key_file  | 指定服务器私钥文件，用以对公钥加密的数据进行解密。                                                                 | 请以实际的服务器私钥名称为准。其相对路径是相对于数据目录的。<br/> **默认值** : server.key                                            |
| ssl_ca_file   | CA服务器的根证书。此参数可选择配置，需要验证客户端证书的合法性时才需要配置。                                       | 请以实际的CA服务器根证书名称为准。<br/> **默认值** : cacert.pem                                                                                       |
| ssl_crl_file  | 证书吊销列表，如果客户端证书在该列表中，则当前客户端证书被视为无效证书。                                           | 请以实际的证书吊销列表名称为准。<br/> **默认值** : 空，表示没有吊销列表。                                                                             |
| ssl_ciphers   | SSL通讯使用的加密算法。                                                                                            | 本产品支持的加密算法的详细信息请参见[表4](#表1.3.4)。<br/> **默认值**: ALL，表示允许对端使用产品支持的所有加密算法，但不包含ADH、LOW、EXP、MD5算法。 |
| ssl_cert_notify_time | SSL服务器证书到期前提醒的天数。 | 请按照需求配置证书过期前提醒天数。<br/> **默认值**: 90 |

在客户端配置SSL认证相关的环境变量，详细信息请参见[表6](#表1.3.3)。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 客户端环境变量的路径以_/home/omm_为例，在实际操作中请使用实际路径进行替换。

**表 6** 客户端参数<a id="表1.3.3"> </a>

| 环境变量      | 描述                         | 取值范围                      |
| :------------ | :--------------------------- | :-------------------------- |
| PGSSLCERT     | 指定客户端证书文件，包含客户端的公钥。客户端证书用以表明客户端身份的合法性，公钥将发送给对端用来对数据进行加密。 | 必须包含文件的绝对路径，如：<br />`export PGSSLCERT='/home/omm/client.crt'`<br />**默认值**: 空 |
| PGSSLKEY      | 指定客户端私钥文件，用以对公钥加密的数据进行解密。           | 必须包含文件的绝对路径，如：<br />`export PGSSLKEY='/home/omm/client.key'`<br />**默认值**: 空 |
| PGSSLMODE     | 设置是否和服务器进行SSL连接协商，以及指定SSL连接的优先级。   | **取值及含义：**<br />- disable：只尝试非SSL连接。<br />- allow：首先尝试非SSL连接，如果连接失败，再尝试SSL连接。<br />- prefer：首先尝试SSL连接，如果连接失败，将尝试非SSL连接。<br />- require：只尝试SSL连接。如果存在CA文件，则按设置成verify-ca的方式验证。<br />- verify-ca：只尝试SSL连接，并且验证服务器是否具有由可信任的证书机构签发的证书。<br />- verify-full：只尝试SSL连接，并且验证服务器是否具有由可信任的证书机构签发的证书，以及验证服务器主机名是否与证书中的一致。<br />**默认值：**prefer |
| PGSSLROOTCERT | 指定为客户端颁发证书的根证书文件，根证书用于验证服务器证书的有效性。 | 必须包含文件的绝对路径，如：<br />`export PGSSLROOTCERT='/home/omm/certca.pem'`<br />**默认值：**空 |
| PGSSLCRL      | 指定证书吊销列表文件，用于验证服务器证书是否在废弃证书列表中，如果在，则服务器证书将会被视为无效证书。 | 必须包含文件的绝对路径，如：<br />`export PGSSLCRL='/home/omm/sslcrl-file.crl'`<br />**默认值：**空 |

服务器端参数ssl、require_ssl与客户端参数sslmode配置组合结果如下:

- 服务器 ssl = on

    | sslmode(客户端) | require_ssl(服务器) | 结果               |
    | :------- | :------- | :------- |
    | disable      | on    | 由于服务器端要求使用SSL,但客户端连接禁用了SSL,无法建立连接。 |
    | allow        | on    | 连接经过加密。                                 |
    | prefer       | on    | 连接经过加密。                                 |
    | require      | on    | 连接经过加密。                                 |
    | verify-ca    | on    | 连接经过加密，且验证了服务器证书。                       |
    | verify-full  | on    | 连接经过加密，且验证了服务器证书和主机名。                   |
    | verify-full  | off   | 连接经过加密，且验证了服务器证书和主机名。                   |

- 服务器 ssl = off

    | sslmode(客户端) | require_ssl(服务器) | 结果               |
    | :------- | :------- | :------- |
    | disable      | on               | 连接未加密。                             |
    | allow        | on               | 连接未加密。                             |
    | prefer       | on               | 连接未加密。                             |
    | require      | on               | 由于客户端要求使用SSL，但服务器端禁用了SSL，因此无法建立连接。 |
    | verify-ca    | on               | 由于客户端要求使用SSL，但服务器端禁用了SSL，因此无法建立连接。 |
    | verify-full  | on               | 由于客户端要求使用SSL，但服务器端禁用了SSL，因此无法建立连接。 |
    | verify-full  | off              | 由于客户端要求使用SSL，但服务器端禁用了SSL，因此无法建立连接。 |

SSL传输支持一系列不同强度的加密和认证算法。用户可以通过修改postgresql.conf中的ssl_ciphers参数指定数据库服务器使用的加密算法。目前本产品SSL支持的加密算法如[表7](#表1.3.4)所示。

**表 7** 加密算法<a id="表1.3.4"> </a>

| 加密强度 | 安全程度 | 加密算法描述              |
| :------- | :------- | :------------------------ |
| stronger | high     | DHE-RSA-AES256-GCM-SHA384 |
| stronger | high     | DHE-RSA-AES128-GCM-SHA256 |
| stronger | high     | DHE-DSS-AES256-GCM-SHA384 |
| stronger | high     | DHE-DSS-AES128-GCM-SHA256 |
| stronger | medium   | DHE-RSA-AES256-SHA256     |
| stronger | medium   | DHE-RSA-AES128-SHA256     |
| stronger | medium   | DHE-DSS-AES256-SHA256     |
| stronger | medium   | DHE-DSS-AES128-SHA256     |
| stronger | high     | DHE-RSA-AES256-CCM        |
| stronger | high     | DHE-RSA-AES128-CCM        |
| stronger | medium   | DHE-RSA-AES256-SHA        |
| stronger | medium   | DHE-RSA-AES128-SHA        |
| stronger | medium   | DHE-DSS-AES256-SHA        |
| stronger | medium   | DHE-DSS-AES128-SHA        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - SSL目前只支持加密强度在strong以上的加密算法。
>
> - 配置参数ssl_ciphers的默认值为ALL，表示支持上表中的所有加密算法。如果对加密算法没有特殊要求，建议用户使用该默认值。
>
> - 如指定以上多种加密，加密算法之间需要使用分号分割。
>
>   如在postgresql.conf设置ssl_ciphers='DHE-RSA-AES256-GCM-SHA384;DHE-RSA-AES256-SHA256;DHE-RSA-AES256-CCM'
>
> - 如果要使用上表中和DSS相关的加密算法（如DHE-DSS-AES256-GCM-SHA384、DHE-DSS-AES256-SHA256、DHE-DSS-AES256-SHA等）必须加载使用DSA算法签名的证书文件。如何使用openssl产生DSA算法签名的证书文件，请参见openssl官方文档。
>
> - SSL连接认证不仅增加了登录（创建SSL环境）及退出过程（清理SSL环境）的时间消耗，同时需要消耗额外的时间用于加解密所需传输的内容，因此对性能有一定影响。特别的，对于频繁的登录登出，短时查询等场景有较大的影响。
>
> - 在证书有效期小于7天的时候，连接登录会在日志中产生告警提醒。

<br/>

## 用SSH隧道进行安全的TCP/IP连接

**背景信息**

为了保证服务器和客户端之间的安全通讯，可以在服务器和客户端之间构建安全的SSH隧道。SSH是目前较可靠，专为远程登录会话和其他网络服务提供安全性的协议。

从SSH客户端来看，SSH提供了两种级别的安全验证:

- 基于口令的安全验证: 使用帐号和口令登录到远程主机。所有传输的数据都会被加密，但是不能保证正在连接的服务器就是需要连接的服务器。可能会有其他服务器冒充真正的服务器，也就是受到"中间人"方式的攻击。
- 基于密钥的安全验证: 用户必须为自己创建一对密钥，并把公用密钥放在需要访问的服务器上。这种级别的认证不仅加密所有传送的数据，而且避免"中间人"攻击方式。但是整个登录的过程可能需要10秒。

<br/>

**前提条件**

SSH服务和数据库运行在同一台服务器上。

<br/>

**操作步骤**

以OpenSSH为例介绍配置SSH隧道，对于如何配置基于密钥的安全验证不作赘述，OpenSSH提供了多种配置适应网络的各种限制，更多详细信息请参考OpenSSH的相关文档。

从本地主机建立到服务器的SSH隧道。

```bash
ssh -L 63333:localhost:8000 username@hostIP
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - -L参数的第一串数字（63333）是通道本端的端口号，可以自由选择。
> - 第二串数字（8000）是通道远端的端口号，也就是服务器使用的端口号。
> - localhost是本机IP地址，username是要连接的服务器上的用户名，hostIP是要连接的主机IP地址。

<br/>

## 查看数据库连接数

**背景信息**

当用户连接数达到上限后，无法建立新的连接。因此，当数据库管理员发现某用户无法连接到数据库时，需要查看是否连接数达到了上限。控制数据库连接的主要以下几种选项。

- 全局的最大连接数: 由运行参数max_connections指定，默认值为5000。
- 某用户的连接数: 在创建用户时由CREATE ROLE命令的CONNECTION LIMIT connlimit子句直接设定，也可以在设定以后用ALTER ROLE的CONNECTION LIMIT connlimit子句修改。
- 某数据库的连接数: 在创建数据库时，由CREATE DATABASE的CONNECTION LIMIT connlimit参数指定。

<br/>

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

    ```bash
    gsql -d mogdb -p 8000
    ```

    mogdb为需要连接的数据库名称，8000为数据库主节点的端口号。

3. 查看全局会话连接数限制。

    ```sql
    mogdb=# SHOW max_connections;
     max_connections
    -----------------
        800
    (1 row)
    ```

    其中800是最大会话连接数。

4. 查看已使用的会话连接数。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
    > 除了创建的时候用双引号引起的数据库和用户名称外，以下命令中用到的数据库名称和用户名称，其中包含的英文字母必须使用小写。

    + 查看指定用户的会话连接数上限

        执行如下命令查看连接到指定用户omm的会话连接数上限。其中-1表示没有对用户omm设置连接数的限制。

        ```sql
        mogdb=# SELECT ROLNAME,ROLCONNLIMIT FROM PG_ROLES WHERE ROLNAME='omm';
         rolname | rolconnlimit
        ---------+--------------
         omm     |           -1
        (1 row)
        ```

    + 查看指定用户已使用的会话连接数

        执行如下命令查看指定用户omm已使用的会话连接数。其中，1表示omm已使用的会话连接数。

        ```sql
        mogdb=# CREATE OR REPLACE VIEW DV_SESSIONS AS
        SELECT
        sa.sessionid AS SID,
        0::integer AS SERIAL#,
        sa.usesysid AS USER#,
        ad.rolname AS USERNAME
        FROM pg_stat_get_activity(NULL) AS sa
        LEFT JOIN pg_authid ad ON(sa.usesysid = ad.oid)
        WHERE sa.application_name <> 'JobScheduler';
        mogdb=# SELECT COUNT() FROM DV_SESSIONS WHERE USERNAME='omm';
        count
        -------
            1
        (1 row)
        ```

    + 查看指定数据库的会话连接数上限

        执行如下命令查看连接到指定数据库mogdb的会话连接数上限。其中-1表示没有对数据库mogdb设置连接数的限制。

        ```sql
        mogdb=# SELECT DATNAME,DATCONNLIMIT FROM PG_DATABASE WHERE DATNAME='mogdb';
         datname | datconnlimit
        ---------+--------------
         mogdb   |           -1
        (1 row)
        ```

    + 查看指定数据库已使用的会话连接数

        执行如下命令查看指定数据库mogdb上已使用的会话连接数。其中，1表示数据库mogdb上已使用的会话连接数。

        ```sql
        mogdb=# SELECT COUNT() FROM PG_STAT_ACTIVITY WHERE DATNAME='mogdb';
         count
        -------
            1
        (1 row)
        ```

    + 查看所有用户已使用会话连接数

        执行如下命令查看所有用户已使用的会话连接数。

        ```sql
        mogdb=# CREATE OR REPLACE VIEW DV_SESSIONS AS
        SELECT
        sa.sessionid AS SID,
        0::integer AS SERIAL#,
        sa.usesysid AS USER#,
        ad.rolname AS USERNAME
        FROM pg_stat_get_activity(NULL) AS sa
        LEFT JOIN pg_authid ad ON(sa.usesysid = ad.oid)
        WHERE sa.application_name <> 'JobScheduler';
        mogdb=# SELECT COUNT(*) FROM DV_SESSIONS;
         count
        -------
            23
        (1 row)
        ```

## SSL证书管理

MogDB默认配置了通过openssl生成的安全证书、私钥。并且提供证书替换的接口，方便用户进行证书的替换。

<br/>

### 证书生成

**操作场景**

在测试环境下，用户可以用通过以下方式进行数字证书测试。在客户的运行环境中，请使用从CA认证中心申请的数字证书。

<br/>

**前提条件**

Linux环境安装了openssl组件。

<br/>

**自认证证书生成过程**

1. 搭建CA环境。

    ```bash
    # 假设用户为omm已存在,搭建CA的路径为test
    # 以root用户身份登录Linux环境,切换到用户omm
    mkdir test
    cd /etc/pki/tls
    # copy 配置文件openssl.cnf到test下
    cp openssl.cnf ~/test
    cd ~/test
    # 到test文件夹下，开始搭建CA环境
    # 创建文件夹demoCA./demoCA/newcerts./demoCA/private
    mkdir ./demoCA ./demoCA/newcerts ./demoCA/private
    chmod 700 ./demoCA/private
    # 创建serial文件,写入01
    echo '01'>./demoCA/serial
    # 创建文件index.txt
    touch ./demoCA/index.txt
    # 修改openssl.cnf配置文件中的参数
    dir  = ./demoCA
    default_md      = sha256
    # 至此CA环境搭建完成
    ```

2. 生成根私钥。

    ```bash
    # 生成CA私钥
    openssl genrsa -aes256 -out demoCA/private/cakey.pem 2048
    Generating RSA private key, 2048 bit long modulus
    .................+++
    ..................+++
    e is 65537 (0x10001)
    # 设置根私钥的保护密码，假设为Test@123
    Enter pass phrase for demoCA/private/cakey.pem:
    # 再次输入私钥密码 Test@123
    Verifying - Enter pass phrase for demoCA/private/cakey.pem:
    ```

3. 生成根证书请求文件。

    ```bash
    # 生成CA根证书申请文件careq.pem
    openssl req -config openssl.cnf -new -key demoCA/private/cakey.pem -out demoCA/careq.pem
    Enter pass phrase for demoCA/private/cakey.pem:
    # 输入根私钥密码 Test@123
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----

    # 以下名称请牢记，生成服务器证书和客户端证书时填写的信息需要与此处的一致
    Country Name (2 letter code) [AU]:CN
    State or Province Name (full name) [Some-State]:shanxi
    Locality Name (eg, city) []:xian
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:Abc
    Organizational Unit Name (eg, section) []:hello
    # Common Name可以随意命名
    Common Name (eg, YOUR name) []:world
    # Email可以选择性填写
    Email Address []:

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:
    An optional company name []:
    ```

4. 生成自签发根证书。

    ```bash
    # 生成根证书时，需要修改openssl.cnf文件，设置basicConstraints=CA:TRUE
    vi openssl.cnf
    # 生成CA自签发根证书
    openssl ca -config openssl.cnf -out demoCA/cacert.pem -keyfile demoCA/private/cakey.pem -selfsign -infiles demoCA/careq.pem
    Using configuration from openssl.cnf
    Enter pass phrase for demoCA/private/cakey.pem:
    # 输入根私钥密码 Test@123
    Check that the request matches the signature
    Signature ok
    Certificate Details:
            Serial Number: 1 (0x1)
            Validity
                Not Before: Feb 28 02:17:11 2017 GMT
                Not After : Feb 28 02:17:11 2018 GMT
            Subject:
                countryName               = CN
                stateOrProvinceName       = shanxi
                organizationName          = Abc
                organizationalUnitName    = hello
                commonName                = world
            X509v3 extensions:
                X509v3 Basic Constraints:
                    CA:FALSE
                Netscape Comment:
                    OpenSSL Generated Certificate
                X509v3 Subject Key Identifier:
                    F9:91:50:B2:42:8C:A8:D3:41:B0:E4:42:CB:C2:BE:8D:B7:8C:17:1F
                X509v3 Authority Key Identifier:
                    keyid:F9:91:50:B2:42:8C:A8:D3:41:B0:E4:42:CB:C2:BE:8D:B7:8C:17:1F

    Certificate is to be certified until Feb 28 02:17:11 2018 GMT (365 days)
    Sign the certificate? [y/n]:y

    1 out of 1 certificate requests certified, commit? [y/n]y
    Write out database with 1 new entries
    Data Base Updated
    # 至此CA根证书自签发完成，根证书demoCA/cacert.pem。
    ```

5. 生成服务端证书私钥。

    ```bash
    # 生成服务器私钥文件server.key
    openssl genrsa -aes256 -out server.key 2048
    Generating a 2048 bit RSA private key
    .......++++++
    ..++++++
    e is 65537 (0x10001)
    Enter pass phrase for server.key:
    # 服务器私钥的保护密码，假设为Test@123
    Verifying - Enter pass phrase for server.key:
    # 再次确认服务器私钥的保护密码，即为Test@123
    ```

6. 生成服务端证书请求文件。

    ```bash
    # 生成服务器证书请求文件server.req
    openssl req -config openssl.cnf -new -key server.key -out server.req
    Enter pass phrase for server.key:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----

    # 以下填写的信息与创建CA时的信息一致
    Country Name (2 letter code) [AU]:CN
    State or Province Name (full name) [Some-State]:shanxi
    Locality Name (eg, city) []:xian
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:Abc
    Organizational Unit Name (eg, section) []:hello
    # Common Name可以随意命名
    Common Name (eg, YOUR name) []:world
    Email Address []:
    # 以下信息可以选择性填写
    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:
    An optional company name []:
    ```

7. 生成服务端证书。

    ```bash
    # 生成服务端/客户端证书时，修改openssl.cnf文件，设置basicConstraints=CA:FALSE
    vi openssl.cnf
    # 修改demoCA/index.txt.attr中属性为no。
    vi demoCA/index.txt.attr

    # 对生成的服务端证书请求文件进行签发，签发后将生成正式的服务端证书server.crt
    openssl ca  -config openssl.cnf -in server.req -out server.crt -days 3650 -md sha256
    Using configuration from /etc/ssl/openssl.cnf
    Enter pass phrase for ./demoCA/private/cakey.pem:
    Check that the request matches the signature
    Signature ok
    Certificate Details:
            Serial Number: 2 (0x2)
            Validity
                Not Before: Feb 27 10:11:12 2017 GMT
                Not After : Feb 25 10:11:12 2027 GMT
            Subject:
                countryName               = CN
                stateOrProvinceName       = shanxi
                organizationName          = Abc
                organizationalUnitName    = hello
                commonName                = world
            X509v3 extensions:
                X509v3 Basic Constraints:
                    CA:FALSE
                Netscape Comment:
                    OpenSSL Generated Certificate
                X509v3 Subject Key Identifier:
                    EB:D9:EE:C0:D2:14:48:AD:EB:BB:AD:B6:29:2C:6C:72:96:5C:38:35
                X509v3 Authority Key Identifier:
                    keyid:84:F6:A1:65:16:1F:28:8A:B7:0D:CB:7E:19:76:2A:8B:F5:2B:5C:6A

    Certificate is to be certified until Feb 25 10:11:12 2027 GMT (3650 days)
    # 选择y对证书进行签发
    Sign the certificate? [y/n]:y

    # 选择y，证书签发结束
    1 out of 1 certificate requests certified, commit? [y/n]y
    Write out database with 1 new entries
    Data Base Updated
    ```

    去掉私钥密码保护，方法如下:

    ```bash
    # 去掉服务端私钥的密码保护
    openssl rsa -in server.key -out server.key
    # 如果不去掉服务端私钥的密码保护需要使用gs_guc工具对存储密码进行加密保护
    gs_guc encrypt -M server -D ./
    # 根据提示输入服务端私钥的密码，加密后会生成server.key.cipher,server.key.rand两个私钥密码保护文件
    ```

8. 客户端证书，私钥的生成。

    生成客户端证书和客户端私钥的方法和要求与服务器相同。

    ```bash
    # 生成客户端私钥
    openssl genrsa -aes256 -out client.key 2048
    # 生成客户端证书请求文件
    openssl req -config openssl.cnf -new -key client.key -out client.req
    # 对生成的客户端证书请求文件进行签发，签发后将生成正式的客户端证书client.crt
    openssl ca -config openssl.cnf -in client.req -out client.crt -days 3650 -md sha256
    ```

    去掉私钥密码保护，方法如下:

    ```bash
    # 去掉客户端私钥的密码保护
    openssl rsa -in client.key -out client.key
    # 如果不去掉客户端私钥的密码保护需要使用gs_guc工具对存储密码进行加密保护
    gs_guc encrypt -M client -D ./
    根据提示输入服务端私钥的密码，加密后会生成client.key.cipher,client.key.rand两个私钥密码保护文件。
    ```

    将客户端密钥转化为DER格式，方法如下:

    ```bash
    openssl pkcs8 -topk8 -outform DER -in client.key -out client.key.pk8 -nocrypt
    ```

9. 吊销证书列表的生成。

    如果需要吊销列表，可按照如下方法生成:

    ```
    # 首先创建crlnumber文件
    echo '00'>./demoCA/crlnumber
    # 吊销服务端证书
    openssl ca -config openssl.cnf -revoke server.crt
    # 生成证书吊销列表sslcrl-file.crl
    openssl ca -config openssl.cnf -gencrl -out sslcrl-file.crl
    ```

<br/>

### 证书替换

**操作场景**

MogDB默认配置了SSL连接所需要的安全的证书、私钥，用户如果需要替换为自己的证书、私钥则可按照此方法进行替换。

<br/>

**前提条件**

用户需要从CA认证中心申请到正式的服务器、客户端的证书和密钥。

<br/>

**注意事项**

MogDB目前只支持X509v3的PEM格式证书。

<br/>

**操作步骤**

1. 准备证书、私钥。

    服务端各个配置文件名称约定:

    - 证书名称约定: server.crt。

    - 私钥名称约定: server.key。
    - 私钥密码加密文件约定: server.key.cipher、server.key.rand。

    客户端各个配置文件名称约定:

    - 证书名称约定: client.crt。
    - 私钥名称约定: client.key。
    - 私钥密码加密文件约定: client.key.cipher、client.key.rand。
    - 根证书名称约定: cacert.pem。
    - 吊销证书列表文件名称约定: sslcrl-file.crl。

2. 制作压缩包。

    压缩包名称约定: db-cert-replacement.zip。

    压缩包格式约定: ZIP。

    压缩包文件列表约定: server.crt、server.key、server.key.cipher、server.key.rand、client.crt、client.key、client.key.cipher、client.key.   rand、cacert.pem。如果需要配置吊销证书列表，则列表中包含sslcrl-file.crl。

3. 调用接口，执行替换。

    a. 将制作好的压缩包db-cert-replacement.zip上传到MogDB用户下的任意路径。

    例如: /home/xxxx/db-cert-replacement.zip。

    b. 调用如下命令进行替换。

    ```bash
    gs_om -t cert --cert-file=/home/xxxx/db-cert-replacement.zip
    ```

4. 重启MogDB。

    ```bash
    gs_om -t stop
    gs_om -t start
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    > 证书具有rollback功能，可以把上一次执行证书替换之前的证书，进行回退。可以使用gs_om -t cert -rollback进行远程调用该接口；使用gs_om -t cert -rollback -L进行本地调用该接口。以上一次成功执行证书替换后，被替换的证书版本为基础进行回退。
