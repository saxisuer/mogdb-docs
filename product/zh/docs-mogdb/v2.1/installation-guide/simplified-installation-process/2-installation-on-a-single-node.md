---
title: 单节点安装
summary: 单节点安装
author: Zhang Cuiping
date: 2021-06-11
---

# 单节点安装

本节以openEuler系统为例描述如何安装单节点。

1. 创建相关目录、用户、组，并授权。

   ```
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. 解压极简版安装包到安装目录。

   ```
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.bz2 -C /opt/software/mogdb/
   ```

3. 进入解压后目录下的simpleInstall。

   ```
   cd /opt/software/mogdb/simpleInstall
   ```

4. 执行install.sh脚本安装MogDB。

   ```
   sh install.sh  -w xxxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - -w：初始化数据库密码（gs_initdb指定）,安全需要必须设置。密码应包括大小写、字符、数字。
   > - -p：指定的MogDB端口号, 如不指定，默认为5432。
   > - -h|-help 打印使用说明。
   > - 安装后，数据库的名称为sgnode。
   > - 安装后，数据库目录安装路径/opt/software/mogdb/data/single_node，其中/opt/software/mogdb为解压包路径，data/single_node为新创建的数据库节点目录。

5. 安装执行完成后，使用ps和gs_ctl查看进程是否正常。

   ```
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/single_node
   ```

   执行ps命令，显示类似如下信息：

   ```
   omm      24209 11.9  1.0 1852000 355816 pts/0  Sl   01:54   0:33 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/single_node
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   执行gs_ctl命令，显示类似如下信息：

   ```
   gs_ctl query ,datadir is /opt/software/mogdb/data/single_node
   HA state:
       local_role                     : Normal
       static_connections             : 0
       db_state                       : Normal
       detail_information             : Normal

   Senders info:
       No information

    Receiver info:
   No information
   ```

6. 使用数据库

   ```
   -- 配置PATH
   echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
   source ~/.bash_profile
   -bash: ulimit: open files: cannot modify limit: Operation not permitted
   （默认设置fd可用大小超过系统设置，可以忽略该设置）

   -- 登录数据库
   gsql -d postgres -p 5432 -r
   gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.

   MogDB=#
   ```
