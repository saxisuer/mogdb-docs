---
title: 卸载MogDB
summary: 卸载MogDB
author: Zhang Cuiping
date: 2021-04-2
---

# 卸载MogDB

卸载MogDB的过程包含卸载MogDB以及清理MogDB服务器环境。

## 执行卸载

MogDB提供了卸载脚本帮助用户完整的卸载MogDB。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用gs_uninstall卸载MogDB。

    ```
    gs_uninstall --delete-data
    ```

    或者在MogDB中每个节点执行本地卸载。

    ```
    gs_uninstall --delete-data -L
    ```

**示例**

使用gs_uninstall脚本进行卸载MogDB。

```bash
gs_uninstall --delete-data
Checking uninstallation.
Successfully checked uninstallation.
Stopping the cluster.
Successfully stopped the cluster.
Successfully deleted instances.
Uninstalling application.
Successfully uninstalled application.
Uninstallation succeeded.
```

单机卸载场景使用gs_uninstall脚本进行卸载。

```bash
gs_uninstall --delete-data
Checking uninstallation.
Successfully checked uninstallation.
Stopping the cluster.
Successfully stopped the cluster.
Successfully deleted instances.
Uninstalling application.
Successfully uninstalled application.
Uninstallation succeeded.
```

**错误排查**

如果卸载失败请根据"$GAUSSLOG/om/gs_uninstall-YYYY-MM-DD_HHMMSS.log"中的日志信息排查错误。

## 一键式环境清理

在MogDB卸载完成后，如果不需要在环境上重新部署MogDB，可以运行脚本gs_postuninstall对MogDB服务器上环境信息做清理。MogDB环境清理是对环境准备脚本gs_preinstall所做设置的清理。

**前提条件**

- MogDB卸载执行成功。
- root用户互信可用。
- 只能使用root用户执行gs_postuninstall命令。

**操作步骤**

1. 以root用户登录MogDB服务器。

2. 查看root用户互信是否建立，如果root用户没有建立互信，需要手工建立root用户互信，操作参考**手工建立互信**。

    查看互信是否建成功，可以互相ssh主机名。输入exit退出。

    ```bash
    plat1:~ # ssh plat2
    Last login: Tue Jan  5 10:28:18 2016 from plat1
    Huawei's internal systems must only be used for conducting Huawei's business or for purposes authorized by Huawei management.Use is subject to audit at any time by Huawei management.
    plat2:~ # exit
    logout
    Connection to plat2 closed.
    plat1:~ #
    ```

3. 进入script路径下。

    ```bash
    cd /opt/software/mogdb/script
    ```

4. 使用gs_postuninstall进行清理。若为环境变量分离的模式安装的数据库需要source环境变量分离文件ENVFILE。

    ```bash
    ./gs_postuninstall -U omm -X /opt/software/mogdb/clusterconfig.xml --delete-user --delete-group
    ```

    或者在MogDB中每个节点执行本地后置清理。

    ```bash
    ./gs_postuninstall -U omm -X /opt/software/mogdb/clusterconfig.xml --delete-user --delete-group -L
    ```

    omm为运行MogDB的操作系统用户名，/opt/software/mogdb/clusterconfig.xml为MogDB配置文件路径。

    若为环境变量分离的模式安装的数据库需删除之前source的环境变量分离的env参数。

    ```bash
    unset MPPDB_ENV_SEPARATE_PATH
    ```

5. 删除MogDB数据库各节点root用户的互信，操作参考**手工建立互信**的删除root用户互信部分内容。

**示例**

清理主机的环境。

```bash
gs_postuninstall -U omm -X /opt/software/mogdb/clusterconfig.xml --delete-user
Parsing the configuration file.
Successfully parsed the configuration file.
Check log file path.
Successfully checked log file path.
Checking unpreinstallation.
Successfully checked unpreinstallation.
Deleting Cgroup.
Successfully deleted Cgroup.
Deleting the instance's directory.
Successfully deleted the instance's directory.
Deleting the installation directory.
Successfully deleted the installation directory.
Deleting the temporary directory.
Successfully deleted the temporary directory.
Deleting remote OS user.
Successfully deleted remote OS user.
Deleting software packages and environmental variables of other nodes.
Successfully deleted software packages and environmental variables of other nodes.
Deleting logs of other nodes.
Successfully deleted logs of other nodes.
Deleting software packages and environmental variables of the local node.
Successfully deleted software packages and environmental variables of the local nodes.
Deleting local OS user.
Successfully deleted local OS user.
Deleting local node's logs.
Successfully deleted local node's logs.
Successfully cleaned environment.
```

 **错误排查**

如果一键式环境清理失败请根据"\$GAUSSLOG/om/gs_postuninstall-YYYY-MM-DD_HHMMSS.log"中的日志信息排查错误。
