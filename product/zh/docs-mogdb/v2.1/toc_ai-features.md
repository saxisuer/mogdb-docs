<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.1

## AI特性指南

+ [概述](/AI-features/1-AI-features-overview.md)
+ Predictor：AI查询时间预测
  + [概述](/AI-features/2-predictor-ai-query-time-forecasting/2-1-ai-query-time-forecasting-overview.md)
  + [环境部署](/AI-features/2-predictor-ai-query-time-forecasting/2-2-environment-deployment.md)
  + [使用指导](/AI-features/2-predictor-ai-query-time-forecasting/2-3-usage-guide.md)
  + [最佳实践](/AI-features/2-predictor-ai-query-time-forecasting/2-4-best-practices.md)
  + [常见问题处理](/AI-features/2-predictor-ai-query-time-forecasting/2-5-faqs.md)
+ X-Tuner：参数调优与诊断
  + [概述](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-1-x-tuner-overview.md)
  + [使用准备](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-2-preparations.md)
  + [使用示例](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-3-examples.md)
  + [获取帮助](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-4-obtaining-help-information.md)
  + [命令参考](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-5-command-reference.md)
  + [常见问题处理](/AI-features/3-x-tuner-parameter-optimization-and-diagnosis/3-6-Troubleshooting.md)
+ SQLdiag：慢SQL发现
  + [概述](/AI-features/4-sqldiag-slow-sql-discovery/4-1-overview.md)
  + [使用指导](/AI-features/4-sqldiag-slow-sql-discovery/4-2-usage-guide.md)
  + [获取帮助](/AI-features/4-sqldiag-slow-sql-discovery/4-3-obtaining-help-information.md)
  + [命令参考](/AI-features/4-sqldiag-slow-sql-discovery/4-4-command-reference.md)
  + [常见问题处理](/AI-features/4-sqldiag-slow-sql-discovery/4-5-troubleshooting.md)
+ Anomaly-detection：数据库指标采集、预测与异常监控
  + [概述](/AI-features/5-a-detection-status-monitoring/5-1-overview.md)
  + [使用准备](/AI-features/5-a-detection-status-monitoring/5-2-preparations.md)
  + [添加监控参数](/AI-features/5-a-detection-status-monitoring/5-3-adding-monitoring-parameters.md)
  + [获取帮助](/AI-features/5-a-detection-status-monitoring/5-4-obtaining-help-information.md)
  + [使用示例](/AI-features/5-a-detection-status-monitoring/5-6-examples.md)
  + [命令参考](/AI-features/5-a-detection-status-monitoring/5-5-command-reference.md)
  + [AI_SERVER](/AI-features/5-a-detection-status-monitoring/5-7-ai-server.md)
  + [AI_MANAGER](/AI-features/5-a-detection-status-monitoring/5-8-ai-manager.md)
+ Index-advisor：索引推荐
  + [单query索引推荐](/AI-features/6-index-advisor-index-recommendation/6-1-single-query-index-recommendation.md)
  + [虚拟索引](/AI-features/6-index-advisor-index-recommendation/6-2-virtual-index.md)
  + [workload级别索引推荐](/AI-features/6-index-advisor-index-recommendation/6-3-workload-level-index-recommendation.md)
+ DeepSQL：库内AI算法
  + [概述](/AI-features/7-deepsql/7-1-overview.md)
  + [环境部署](/AI-features/7-deepsql/7-2-environment-deployment.md)
  + [使用指导](/AI-features/7-deepsql/7-3-usage-guide.md)
  + [最佳实践](/AI-features/7-deepsql/7-4-best-practices.md)
  + [常见问题处理](/AI-features/7-deepsql/7-5-troubleshooting.md)
+ DB4AI：数据库原生AI引擎
  + [概述](/AI-features/8-db4ai/8-1-overview.md)
  + [DB4AI-Snapshots数据版本管理](/AI-features/8-db4ai/8-2-db4ai-snapshots-for-data-version-management.md)
  + [DB4AI-Query：模型训练和推断](/AI-features/8-db4ai/8-3-db4ai-query-for-model-training-and-prediction.md)
  + [plpython-fenced模式](/AI-features/8-db4ai/8-4-pl-python-fenced-mode.md)