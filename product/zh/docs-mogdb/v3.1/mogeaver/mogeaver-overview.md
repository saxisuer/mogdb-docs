---
title: Mogeaver 简介
summary: Mogeaver 简介
author: Bin.Liu
date: 2022-06-15
---

# Mogeaver

## 关于Mogeaver

Mogeaver [moˈgi:və(r) ] 基于流行的开源图形化工具[DBeaver](https://dbeaver.io/)，在严格遵循DBeaver Community Edition 的[ASL](https://dbeaver.io/product/dbeaver_license.txt)开源协议基础上，进行了二次开发和封装，支持对于MogDB数据库的图形化开发及管理，支持通过图形化的方式创建，修改，调试数据库内的存储过程，自定义函数，及包。

## 为什么选择Mogeaver

- 多平台支持

  - 支持macOS Intel版本，支持macOS Apple Silicon版本，

  - 支持Windows，包括但不限于Windows 10， Windows 11

  - 支持Linux，包括但不限于openEuler，Kylin Linux，UOS，Ubuntu，CentOS

- 兼容MogDB的特性

  - 支持创建数据库选择不同的兼容模式（A，B，PG）

  - 支持管理MogDB数据库角色（role）

  - 支持管理Large序列（MogDB 3.0之后版本支持）

  - 支持管理分区表（Partition Table）

  - 支持管理数据库内任务（Job）

  - 支持BLOB和CLOB字段的修改和查看

- 支持图形化调试

  - 支持MogDB服务端的dbe_pldebugger调试功能
  - 支持断点调试和单步调试
  - 支持查看调试中变量值实时变化
  - 支持定位并跳转至准确的代码报错位置

## 使用Mogeaver

Mogeaver的使用方式请参见[使用Mogeaver访问MogDB](../quick-start/mogdb-access/use-gui-tools-to-access-mogdb/mogeaver-usage.md)。
