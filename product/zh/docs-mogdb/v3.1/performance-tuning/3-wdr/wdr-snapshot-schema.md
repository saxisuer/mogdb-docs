---
title: WDR Snapshot Schema
summary: WDR Snapshot Schema
author: Zhang Cuiping
date: 2021-03-11
---

# WDR Snapshot Schema

WDR Snapshot在启动后（打开参数enable_wdr_snapshot），会在用户表空间"pg_default"，数据库"postgres"下新建schema "snapshot"，用于持久化WDR快照数据。默认初始化用户或monadmin用户可以访问Snapshot Schema。

根据参数wdr_snapshot_retention_days来自动管理快照的生命周期。

<br/>

## WDR Snapshot 原信息表

<br/>

### SNAPSHOT.SNAPSHOT <a id='SNAPSHOT'> </a>

SNAPSHOT表记录当前系统中存储的WDR快照数据的索引信息、开始、结束时间。只能在系统库中查询到结果，在用户库中无法查询。

**表 1** SNAPSHOT表属性

| 名称        | 类型      | 描述                | 示例                          |
| :---------- | :-------- | :------------------ | :---------------------------- |
| snapshot_id | bigint    | WDR快照序号。       | 1                             |
| start_ts    | timestamp | WDR快照的开始时间。 | 2019-12-28 17:11:27.423742+08 |
| end_ts      | timestamp | WDR快照的结束时间。 | 2019-12-28 17:11:43.67726+08  |

<br/>

### SNAPSHOT.TABLES_SNAP_TIMESTAMP <a id='tables_snap_timestamp'> </a>

TABLES_SNAP_TIMESTAMP表记录所有存储的WDR snapshot中数据库、表对象、以及数据采集的开始和结束时间。

**表 2** TABLES_SNAP_TIMESTAMP表属性

| 名称        | 类型      | 描述                         | 示例                          |
| :---------- | :-------- | :--------------------------- | :---------------------------- |
| snapshot_id | bigint    | WDR快照序号。                | 1                             |
| db_name     | text      | WDR snapshot对应的database。 | tpcc1000                      |
| tablename   | text      | WDR snapshot对应的table。    | snap_xc_statio_all_indexes    |
| start_ts    | timestamp | WDR快照的开始时间。          | 2019-12-28 17:11:27.425849+08 |
| end_ts      | timestamp | WDR快照的结束时间。          | 2019-12-28 17:11:27.707398+08 |

<br/>

### SNAP_SEQ

snap_seq是一个递增的sequence，其为WDR snapshot提供快照的ID。

<br/>

## WDR Snapshot 数据表

WDR Snapshot数据表命名原则：snap_{源数据表}。

WDR Snapshot数据表来源为"DBE_PERF Schema"下所有的视图。

执行以下命令即可查询所有WDR Snapshot数据表：

```sql
select * from pg_catalog.pg_tables where schemaname='snapshot';
```

下表列出所有WDR Snapshot数据表及相关介绍页面以供查询。

| schemaname | tablename                                                    |
| ---------- | ------------------------------------------------------------ |
| snapshot   | [tables_snap_timestamp](#tables_snap_timestamp)              |
| snapshot   | [snapshot](#SNAPSHOT)                                        |
| snapshot   | [snap_global_os_runtime](../../reference-guide/schema/DBE_PERF/os/GLOBAL_OS_RUNTIME.md) |
| snapshot   | [snap_global_os_threads](../../reference-guide/schema/DBE_PERF/os/GLOBAL_OS_THREADS.md) |
| snapshot   | [snap_global_instance_time](../../reference-guide/schema/DBE_PERF/instance/GLOBAL_INSTANCE_TIME.md) |
| snapshot   | [snap_summary_workload_sql_count](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_SQL_COUNT.md) |
| snapshot   | [snap_summary_workload_sql_elapse_time](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_SQL_ELAPSE_TIME.md) |
| snapshot   | [snap_global_workload_transaction](../../reference-guide/schema/DBE_PERF/workload/GLOBAL_WORKLOAD_TRANSACTION.md) |
| snapshot   | [snap_summary_workload_transaction](../../reference-guide/schema/DBE_PERF/workload/SUMMARY_WORKLOAD_TRANSACTION.md) |
| snapshot   | [snap_global_thread_wait_status](../../reference-guide/schema/DBE_PERF/session-thread/GLOBAL_THREAD_WAIT_STATUS.md) |
| snapshot   | [snap_global_memory_node_detail](../../reference-guide/schema/DBE_PERF/memory/GLOBAL_MEMORY_NODE_DETAIL.md) |
| snapshot   | [snap_global_shared_memory_detail](../../reference-guide/schema/DBE_PERF/memory/GLOBAL_SHARED_MEMORY_DETAIL.md) |
| snapshot   | [snap_global_stat_db_cu](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STAT_DB_CU.md) |
| snapshot   | [snap_global_stat_database](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_DATABASE.md) |
| snapshot   | [snap_summary_stat_database](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_DATABASE.md) |
| snapshot   | [snap_global_stat_database_conflicts](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_DATABASE_CONFLICTS.md) |
| snapshot   | [snap_summary_stat_database_conflicts](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_DATABASE_CONFLICTS.md) |
| snapshot   | [snap_global_stat_bad_block](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_BAD_BLOCK.md) |
| snapshot   | [snap_summary_stat_bad_block](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_BAD_BLOCK.md) |
| snapshot   | [snap_global_file_redo_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_FILE_REDO_IOSTAT.md) |
| snapshot   | [snap_summary_file_redo_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_FILE_REDO_IOSTAT.md) |
| snapshot   | [snap_global_rel_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_REL_IOSTAT.md) |
| snapshot   | [snap_summary_rel_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_REL_IOSTAT.md) |
| snapshot   | [snap_global_file_iostat](../../reference-guide/schema/DBE_PERF/file/GLOBAL_FILE_IOSTAT.md) |
| snapshot   | [snap_summary_file_iostat](../../reference-guide/schema/DBE_PERF/file/SUMMARY_FILE_IOSTAT.md) |
| snapshot   | [snap_global_replication_slots](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REPLICATION_SLOTS.md) |
| snapshot   | [snap_global_bgwriter_stat](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_BGWRITER_STAT.md) |
| snapshot   | [snap_global_replication_stat](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REPLICATION_STAT.md) |
| snapshot   | [snap_global_transactions_running_xacts](../../reference-guide/schema/DBE_PERF/transaction/GLOBAL_TRANSACTIONS_RUNNING_XACTS.md) |
| snapshot   | [snap_summary_transactions_running_xacts](../../reference-guide/schema/DBE_PERF/transaction/SUMMARY_TRANSACTIONS_RUNNING_XACTS.md) |
| snapshot   | [snap_global_transactions_prepared_xacts](../../reference-guide/schema/DBE_PERF/transaction/GLOBAL_TRANSACTIONS_PREPARED_XACTS.md) |
| snapshot   | [snap_summary_transactions_prepared_xacts](../../reference-guide/schema/DBE_PERF/transaction/SUMMARY_TRANSACTIONS_PREPARED_XACTS.md) |
| snapshot   | [snap_summary_statement](../../reference-guide/schema/DBE_PERF/query/SUMMARY_STATEMENT.md) |
| snapshot   | [snap_global_statement_count](../../reference-guide/schema/DBE_PERF/query/GLOBAL_STATEMENT_COUNT.md) |
| snapshot   | [snap_summary_statement_count](../../reference-guide/schema/DBE_PERF/query/SUMMARY_STATEMENT_COUNT.md) |
| snapshot   | [snap_global_config_settings](../../reference-guide/schema/DBE_PERF/configuration/GLOBAL_CONFIG_SETTINGS.md) |
| snapshot   | [snap_global_wait_events](../../reference-guide/schema/DBE_PERF/wait-events/GLOBAL_WAIT_EVENTS.md) |
| snapshot   | [snap_summary_user_login](../../reference-guide/schema/DBE_PERF/utility/SUMMARY_USER_LOGIN.md) |
| snapshot   | [snap_global_ckpt_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_CKPT_STATUS.md) |
| snapshot   | [snap_global_double_write_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_DOUBLE_WRITE_STATUS.md) |
| snapshot   | [snap_global_pagewriter_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_PAGEWRITER_STATUS.md) |
| snapshot   | [snap_global_redo_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_REDO_STATUS.md) |
| snapshot   | [snap_global_rto_status](../../reference-guide/schema/DBE_PERF/rto/global_rto_status.md) |
| snapshot   | [snap_global_recovery_status](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_RECOVERY_STATUS.md) |
| snapshot   | [snap_global_threadpool_status](../../reference-guide/schema/DBE_PERF/session-thread/GLOBAL_THREADPOOL_STATUS.md) |
| snapshot   | [snap_statement_responsetime_percentile](../../reference-guide/schema/DBE_PERF/query/STATEMENT_RESPONSETIME_PERCENTILE.md) |
| snapshot   | [snap_global_statio_all_indexes](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_INDEXES.md) |
| snapshot   | [snap_summary_statio_all_indexes](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_INDEXES.md) |
| snapshot   | [snap_global_statio_all_sequences](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_SEQUENCES.md) |
| snapshot   | [snap_summary_statio_all_sequences](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_SEQUENCES.md) |
| snapshot   | [snap_global_statio_all_tables](../../reference-guide/schema/DBE_PERF/cache-io/GLOBAL_STATIO_ALL_TABLES.md) |
| snapshot   | [snap_summary_statio_all_tables](../../reference-guide/schema/DBE_PERF/cache-io/SUMMARY_STATIO_ALL_TABLES.md) |
| snapshot   | [snap_global_stat_all_indexes](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_ALL_INDEXES.md) |
| snapshot   | [snap_summary_stat_all_indexes](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_ALL_INDEXES.md) |
| snapshot   | [snap_summary_stat_user_functions](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_USER_FUNCTIONS.md) |
| snapshot   | [snap_global_stat_user_functions](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_USER_FUNCTIONS.md) |
| snapshot   | [snap_global_stat_all_tables](../../reference-guide/schema/DBE_PERF/object/GLOBAL_STAT_ALL_TABLES.md) |
| snapshot   | [snap_summary_stat_all_tables](../../reference-guide/schema/DBE_PERF/object/SUMMARY_STAT_ALL_TABLES.md) |
| snapshot   | [snap_class_vital_info](../../reference-guide/schema/DBE_PERF/utility/CLASS_VITAL_INFO.md) |
| snapshot   | [snap_global_record_reset_time](../../reference-guide/schema/DBE_PERF/utility/GLOBAL_RECORD_RESET_TIME.md) |

<br/>

## WDR Snapshot生成性能报告

基于WDR Snapshot数据表汇总、统计，生成性能报告，默认初始化用户或监控管理员用户可以生成报告。

**前提条件**

WDR Snasphot启动（即参数[enable_wdr_snapshot](../../reference-guide/guc-parameters/27-system-performance-snapshot.md#enable_wdr_snapshot)）为on时，且快照数量大于等于2。

**操作步骤**

1. 执行如下命令新建报告文件。

    ```
    touch  /home/om/wdrTestNode.html
    ```

2. 执行以下命令连接postgres数据库。

    ```
    gsql -d postgres -p 端口号 -r
    ```

3. 执行如下命令查询已经生成的快照，以获取快照的snapshot_id。

    ```
    select * from snapshot.snapshot;
    ```

4. （可选）在CCN上执行如下命令手动创建快照。数据库中只有一个快照或者需要查看在当前时间段数据库的监控数据，可以选择手动执行快照操作，该命令需要用户具有sysadmin权限。

    ```
    select create_wdr_snapshot();
    ```

    > **说明**:  执行“cm_ctl query -Cdvi”，回显中“Central Coordinator State”下显示的信息即为CCN信息。

5. 执行如下命令，在本地生成HTML格式的WDR报告。

    a. 执行如下命令，设置报告格式。\a: 不显示表行列符号， \t: 不显示列名 ，\o: 指定输出文件。

    ```
    gsql> \a
    gsql> \t
    gsql> \o /home/om/wdrTestNode.html
    ```

    b. 执行如下命令，生成HTML格式的WDR报告。

    ```
    gsql> select generate_wdr_report(begin_snap_id Oid, end_snap_id Oid, int report_type, int report_scope, int node_name );
    ```

    示例一，生成集群级别的报告：

    ```
    select generate_wdr_report(1, 2, 'all', 'cluster',null);
    ```

    示例二，生成某个节点的报告：

    ```
    select generate_wdr_report(1, 2, 'all', 'node', pgxc_node_str()::cstring);
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前MogDB的节点名固定是“dn_6001_6002_6003”，也可直接代入。

    **表 3** generate_wdr_report函数参数说明

    | 参数          | 说明             | 取值范围   |
    | :------------ | :---------------|-----------|
    | begin_snap_id | 查询时间段开始的snapshot的id（表snapshot.snaoshot中的snapshot_id）。                                                                                                          | -                                                                           |
    | end_snap_id   | 查询时间段结束snapshot的id。默认end_snap_id大于begin_snap_id（表snapshot.snaoshot中的snapshot_id）。                                                                          | -                                                                           |
    | report_type   | 指定生成report的类型。例如，summary/detail/all。                                                                                                                              | summary: 汇总数据。<br />detail: 明细数据。<br />all: 包含summary和detail。 |
    | report_scope  | 指定生成report的范围，可以为cluster或者node。                                                                                                                                 | cluster: 数据库级别的信息。<br />node: 节点级别的信息。                     |
    | node_name     | 在report_scope指定为node时，需要把该参数指定为对应节点的名称。（节点名称可以执行select * from pg_node_env;查询）。在report_scope为cluster时，该值可以指定为省略、空或者为NULL。 | node: MogDB中的节点名称。<br />cluster: 省略、空或者NULL。                  |

    c. 执行如下命令关闭输出选项及格式化输出命令。

    ```
    \o \a \t
    ```

6. 在/home/om/下根据需要[查看WDR报告](./wdr-report.md)。

**示例**

```sql
--创建报告文件
touch  /home/om/wdrTestNode.html

--连接数据库
gsql -d postgres -p 端口号 -r

--查询已经生成的快照。
MogDB=# select * from snapshot.snapshot;
 snapshot_id |           start_ts            |            end_ts
-------------+-------------------------------+-------------------------------
           1 | 2020-09-07 10:20:36.763244+08 | 2020-09-07 10:20:42.166511+08
           2 | 2020-09-07 10:21:13.416352+08 | 2020-09-07 10:21:19.470911+08
(2 rows)


--生成格式化性能报告wdrTestNode.html。
MogDB=# \a \t \o /home/om/wdrTestNode.html
Output format is unaligned.
Showing only tuples.

--向性能报告wdrTestNode.html中写入数据。
MogDB=# select generate_wdr_report(1, 2, 'all', 'node', 'dn_6001_6002_6003');

--关闭性能报告wdrTestNode.html。
MogDB=# \o

--生成格式化性能报告wdrTestCluster.html。
MogDB=# \o /home/om/wdrTestCluster.html

--向格式化性能报告wdrTestCluster.html中写入数据。
MogDB=# select generate_wdr_report(1, 2, 'all', 'cluster');

--关闭性能报告wdrTestCluster.html。
MogDB=# \o \a \t
Output format is aligned.
Tuples only is off.
```
