---
title: 统计信息函数
summary: 统计信息函数
author: Zhang Cuiping
date: 2021-04-20
---

# 统计信息函数

统计信息函数根据访问对象分为两种类型：针对某个数据库进行访问的函数，以数据库中每个表或索引的OID作为参数，标识需要报告的数据库；针对某个服务器进行访问的函数，以一个服务器进程号为参数，其范围从1到当前活跃服务器的数目。

- pg_stat_get_db_conflict_tablespace(oid)

  描述：由于恢复与数据库中删除的表空间发生冲突而取消的查询数。

  返回值类型：bigint

- pg_control_group_config

  描述：在当前节点上打印cgroup配置。

  返回值类型：record

- pg_stat_get_db_stat_reset_time(oid)

  描述：上次重置数据库统计信息的时间。首次连接到每个数据库期间初始化为系统时间。当您在数据库上调用pg_stat_reset以及针对其中的任何表或索引执行pg_stat_reset_single_table_counters时，重置时间都会更新。

  返回值类型：timestamptz

- pg_stat_get_function_total_time(oid)

  描述：该函数花费的总挂钟时间，以微秒为单位。包括花费在此函数调用其它函数上的时间。

  返回值类型：bigint

- pg_stat_get_xact_tuples_returned(oid)

  描述：当前事务中参数为表时通过顺序扫描读取的行数，或参数为索引时返回的索引条目数。

  返回值类型：bigint

- pg_stat_get_xact_numscans(oid)

  描述：当前事务中参数为表时执行的顺序扫描次数，或参数为索引时执行的索引扫描次数。

  返回值类型：bigint

- pg_stat_get_xact_blocks_fetched(oid)

  描述：当前事务中对表或索引的磁盘块获取请求数。

  返回值类型：bigint

- pg_stat_get_xact_blocks_hit(oid)

  描述：当前事务中对缓存中找到的表或索引的磁盘块获取请求数。

  返回值类型：bigint

- pg_stat_get_xact_function_calls(oid)

  描述：在当前事务中调用该函数的次数。

  返回值类型：bigint

- pg_stat_get_xact_function_self_time(oid)

  描述：在当前事务中仅花费在此函数上的时间，不包括花费在此函数内部调用其它函数上的时间。

  返回值类型：bigint

- pg_stat_get_xact_function_total_time(oid)

  描述：当前事务中该函数所花费的总挂钟时间（以微秒为单位），包括花费在此函数内部调用其它函数上的时间。

  返回值类型：bigint

- pg_stat_get_wal_senders()

  描述：在主机端查询walsender信息。

  返回值类型：setofrecord

  返回字段说明如下：

  **表 1** 返回字段说明

  | 字段名称                   | 字段类型                 | 字段说明                           |
  | :------------------------- | :----------------------- | :--------------------------------- |
  | pid                        | bigint                   | walsender的线程号。                |
  | sender_pid                 | integer                  | walsender的pid相对的轻量级线程号。 |
  | local_role                 | text                     | 主节点类型。                       |
  | peer_role                  | text                     | 备节点类型。                       |
  | peer_state                 | text                     | 备节点状态。                       |
  | state                      | text                     | walsender状态。                    |
  | catchup_start              | timestamp with time zone | catchup启动时间。                  |
  | catchup_end                | timestamp with time zone | catchup结束时间。                  |
  | sender_sent_location       | text                     | 主节点发送位置。                   |
  | sender_write_location      | text                     | 主节点落盘位置。                   |
  | sender_flush_location      | text                     | 主节点flush磁盘位置。              |
  | sender_replay_location     | text                     | 主节点redo位置。                   |
  | receiver_received_location | text                     | 备节点接收位置。                   |
  | receiver_write_location    | text                     | 备节点落盘位置。                   |
  | receiver_flush_location    | text                     | 备节点flush磁盘位置。              |
  | receiver_replay_location   | text                     | 备节点redo磁盘位置。               |
  | sync_percent               | text                     | 同步百分比。                       |
  | sync_state                 | text                     | 同步状态。                         |
  | sync_priority              | text                     | 同步复制的优先级。                 |
  | sync_most_available        | text                     | 最大可用模式设置。                 |
  | channel                    | text                     | walsender信道信息。                |

- get_paxos_replication_info()

  描述：查询Paxos模式下主机或备机的复制状态。

  返回值类型：setofrecord

  返回字段说明如下：

  **表 2** 返回字段说明

  | 字段名称              | 字段类型 | 字段说明                        |
  | :-------------------- | :------- | :------------------------------ |
  | paxos_write_location  | text     | 已经写入DCF的XLog位置。         |
  | paxos_commit_location | text     | 已经在DCF中达成一致的XLog位置。 |
  | local_write_location  | text     | 节点的落盘位置。                |
  | local_flush_location  | text     | 节点的flush磁盘位置。           |
  | local_replay_location | text     | 节点的redo磁盘位置。            |
  | dcf_replication_info  | text     | 节点的DCF模块信息。             |

- pg_stat_get_stream_replications()

  描述：查询主备复制状态。

  返回值类型：setofrecord

  返回值说明如下。

  **表 3** 返回值说明

  | 返回参数           | 返回参数类型 | 返回参数说明 |
  | :----------------- | :----------- | :----------- |
  | local_role         | text         | 本地角色。   |
  | static_connections | integer      | 连接统计。   |
  | db_state           | text         | 数据库状态。 |
  | detail_information | text         | 详细信息。   |

- pg_stat_get_db_numbackends(oid)

  描述：处理该数据库活跃的服务器进程数目。

  返回值类型：integer

- pg_stat_get_db_xact_commit(oid)

  描述：数据库中已提交事务的数量。

  返回值类型：bigint

- pg_stat_get_db_xact_rollback(oid)

  描述：数据库中回滚事务的数量。

  返回值类型：bigint

- pg_stat_get_db_blocks_fetched(oid)

  描述：数据库中磁盘块抓取请求的总数。

  返回值类型：bigint

- pg_stat_get_db_blocks_hit(oid)

  描述：数据库在缓冲区中找到的磁盘块抓取请求的总数。

  返回值类型：bigint

- pg_stat_get_db_tuples_returned(oid)

  描述：为数据库返回的Tuple数。

  返回值类型：bigint

- pg_stat_get_db_tuples_fetched(oid)

  描述：为数据库中获取的Tuple数。

  返回值类型：bigint

- pg_stat_get_db_tuples_inserted(oid)

  描述：在数据库中插入Tuple数。

  返回值类型：bigint

- pg_stat_get_db_tuples_updated(oid)

  描述：在数据库中更新的Tuple数。

  返回值类型：bigint

- pg_stat_get_db_tuples_deleted(oid)

  描述：数据库中删除Tuple数。

  返回值类型：bigint

- pg_stat_get_db_conflict_lock(oid)

  描述：数据库中锁冲突的数量。

  返回值类型：bigint

- pg_stat_get_db_deadlocks(oid)

  描述：数据库中死锁的数量。

  返回值类型：bigint

- pg_stat_get_numscans(oid)

  描述：如果参数是一个表，则顺序扫描读取的行数目。如果参数是一个索引，则返回索引行的数目。

  返回值类型：bigint

- pg_stat_get_role_name(oid)

  描述：根据用户oid获取用户名。仅sysadmin和monitor admin用户可以访问。

  返回值类型：text

  示例：

  ```sql
  MogDB=# select pg_stat_get_role_name(10);
   pg_stat_get_role_name
  -----------------------
   aabbcc
  (1 row)
  ```

- pg_stat_get_tuples_returned(oid)

  描述：如果参数是一个表，则顺序扫描读取的行数目。如果参数是一个索引，则返回的索引行的数目。

  返回值类型：bigint

- pg_stat_get_tuples_fetched(oid)

  描述：如果参数是一个表，则位图扫描抓取的行数目。如果参数是一个索引，则用简单索引扫描抓取的行数目。

  返回值类型：bigint

- pg_stat_get_tuples_inserted(oid)

  描述：插入表中行的数量。

  返回值类型：bigint

- pg_stat_get_tuples_updated(oid)

  描述：在表中已更新行的数量。

  返回值类型：bigint

- pg_stat_get_tuples_deleted(oid)

  描述：从表中删除行的数量。

  返回值类型：bigint

- pg_stat_get_tuples_changed(oid)

  描述：该表上一次analyze或autoanalyze之后插入、更新、删除行的总数量。

  返回值类型：bigint

- pg_stat_get_tuples_hot_updated(oid)

  描述：表热更新的行数。

  返回值类型：bigint

- pg_stat_get_live_tuples(oid)

  描述：表活行数。

  返回值类型：bigint

- pg_stat_get_dead_tuples(oid)

  描述：表死行数。

  返回值类型：bigint

- pg_stat_get_blocks_fetched(oid)

  描述：表或者索引的磁盘块抓取请求的数量。

  返回值类型：bigint

- pg_stat_get_blocks_hit(oid)

  描述：在缓冲区中找到的表或者索引的磁盘块请求数目。

  返回值类型：bigint

- pg_stat_get_partition_tuples_inserted(oid)

  描述：插入相应表分区中行的数量。

  返回值类型：bigint

- pg_stat_get_partition_tuples_updated(oid)

  描述：在相应表分区中已更新行的数量。

  返回值类型：bigint

- pg_stat_get_partition_tuples_deleted(oid)

  描述：从相应表分区中删除行的数量。

  返回值类型：bigint

- pg_stat_get_partition_tuples_changed(oid)

  描述：该表分区上一次analyze或autoanalyze之后插入、更新、删除行的总数量。

  返回值类型：bigint

- pg_stat_get_partition_live_tuples(oid)

  描述：分区表活行数。

  返回值类型：bigint

- pg_stat_get_partition_dead_tuples(oid)

  描述：分区表死行数。

  返回值类型：bigint

- pg_stat_get_xact_tuples_fetched(oid)

  描述：事务中扫描的tuple行数。

  返回值类型：bigint

- pg_stat_get_xact_tuples_inserted(oid)

  描述：表相关的活跃子事务中插入的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_tuples_deleted(oid)

  描述：表相关的活跃子事务中删除的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_tuples_hot_updated(oid)

  描述：表相关的活跃子事务中热更新的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_tuples_updated(oid)

  描述：表相关的活跃子事务中更新的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_partition_tuples_inserted(oid)

  描述：表分区相关的活跃子事务中插入的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_partition_tuples_deleted(oid)

  描述：表分区相关的活跃子事务中删除的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_partition_tuples_hot_updated(oid)

  描述：表分区相关的活跃子事务中热更新的tuple数。

  返回值类型：bigint

- pg_stat_get_xact_partition_tuples_updated(oid)

  描述：表分区相关的活跃子事务中更新的tuple数。

  返回值类型：bigint

- pg_stat_get_last_vacuum_time(oid)

  描述：用户在该表上最后一次手动启动清理或者autovacuum线程启动清理的时间。

  返回值类型：timestamptz

- pg_stat_get_last_autovacuum_time(oid)

  描述：autovacuum守护进程在该表上最后一次启动清理的时间。

  返回值类型：timestamptz

- pg_stat_get_vacuum_count(oid)

  描述：用户在该表上启动清理的次数。

  返回值类型：bigint

- pg_stat_get_autovacuum_count(oid)

  描述：autovacuum守护进程在该表上启动清理的次数。

  返回值类型：bigint

- pg_stat_get_last_analyze_time(oid)

  描述：用户在该表上最后一次手动启动分析或者autovacuum线程启动分析的时间。

  返回值类型：timestamptz

- pg_stat_get_last_autoanalyze_time(oid)

  描述：autovacuum守护进程在该表上最后一次启动分析的时间。

  返回值类型：timestamptz

- pg_stat_get_analyze_count(oid)

  描述：用户在该表上启动分析的次数。

  返回值类型：bigint

- pg_stat_get_autoanalyze_count(oid)

  描述：autovacuum守护进程在该表上启动分析的次数。

  返回值类型：bigint

- pg_total_autovac_tuples(bool,bool)

  描述：返回total autovac相关的tuple记录，如nodename、nspname、relname以及各类tuple的IUD信息，入参分别为：是否查询relation信息、是否查询local信息。

  返回值类型：setofrecord

  返回参数说明如下。

  **表 4** 返回参数说明

  | 返回参数              | 返回参数类型 | 返回参数说明               |
  | :-------------------- | :----------- | :------------------------- |
  | nodename              | name         | 节点名称。                 |
  | nspname               | name         | 名称空间名称。             |
  | relname               | name         | 表、索引、视图等对象名称。 |
  | partname              | name         | 分区名称。                 |
  | n_dead_tuples         | bigint       | 表分区内的死行数。         |
  | n_live_tuples         | bigint       | 表分区内的活行数。         |
  | changes_since_analyze | bigint       | analyze产生改变的数量。    |

- pg_autovac_status(oid)

  描述：返回和autovac状态相关的参数信息，如nodename、nspname、relname、analyze、vacuum设置，analyze/vacuum阈值, analyze/vacuum tuple数等。仅sysadmin可以使用该函数。

  返回值类型：setofrecord

  返回值参数说明如下。

  **表 5** 返回值参数说明

  | 返回参数  | 返回参数类型 | 返回参数说明               |
  | :-------- | :----------- | :------------------------- |
  | nspname   | text         | 名称空间名称。             |
  | relname   | text         | 表、索引、视图等对象名称。 |
  | nodename  | text         | 节点名称。                 |
  | doanalyze | Boolean      | 是否执行analyze。          |
  | anltuples | bigint       | analyze tuple数量。        |
  | anlthresh | bigint       | analyze阈值。              |
  | dovacuum  | Boolean      | 是否执行vacuum。           |
  | vactuples | bigint       | vacuum tuple数量。         |
  | vacthresh | bigint       | vacuum阈值。               |

- pg_autovac_timeout(oid)

  描述：返回某个表做autovac连续超时的次数，表信息非法或node信息异常返回NULL。

  返回值类型：bigint

- pg_stat_get_last_data_changed_time(oid)

  描述：insert/update/delete, exchange/truncate/drop partition在该表上最后一次操作的时间，PG_STAT_ALL_TABLES视图last_data_changed列的数据是通过该函数求值，在表数量很大的场景中，通过视图获取表数据最后修改时间的性能较差，建议直接使用该函数获取表数据的最后修改时间。

  返回值类型：timestamptz

- pg_stat_set_last_data_changed_time(oid)

  描述：手动设置该表上最后一次insert/update/delete, exchange/truncate/drop partition操作的时间。

  返回值类型：void

- pg_backend_pid()

  描述：当前会话的服务器线程的线程ID。

  返回值类型：integer

- pg_stat_get_activity(integer)

  描述：返回一个关于带有特殊PID的后台进程的记录信息，当参数为NULL时，则返回每个活动的后台进程的记录。返回结果是PG_STAT_ACTIVITY视图中的一个子集，不包含connection_info列。

  示例：

  ```sql
  MogDB=# select * from pg_stat_get_activity(139754904483584);
   datid |       pid       |    sessionid    | usesysid | application_name | state |                    query                     | waiting | xact_start |          query_start          |         backend_start         |         state_cha
  nge          | client_addr | client_hostname | client_port | enqueue | query_id | srespool | global_sessionid
  -------+-----------------+-----------------+----------+------------------+-------+----------------------------------------------+---------+------------+-------------------------------+-------------------------------+------------------
  -------------+-------------+-----------------+-------------+---------+----------+----------+------------------
   16099 | 139754904483584 | 139754904483584 |       10 | coordinator2     | idle  | SET SESSION AUTHORIZATION DEFAULT;RESET ALL; | f       |            | 2021-06-09 16:40:58.391727+08 | 2021-06-09 16:03:52.040929+08 | 2021-06-09 16:40:
  58.392143+08 | ::1         |                 |       35210 |         |        0 | unknown  | 0#0#0
  (1 row)
  ```

  返回值类型：setofrecord

  返回参数说明如下。

  **表 6** 返回参数说明

  | 返回参数         | 返回参数类型             | 返回参数说明                                                 |
  | :--------------- | :----------------------- | :----------------------------------------------------------- |
  | datid            | oid                      | 用户会话在后台连接到的数据库OID。                            |
  | pid              | bigint                   | 后台线程ID。                                                 |
  | sessionid        | bigint                   | 会话ID。                                                     |
  | usesysid         | oid                      | 登录该后台的用户OID。                                        |
  | application_name | text                     | 连接到该后台的应用名。                                       |
  | state            | text                     | 该后台当前总体状态。                                         |
  | query            | text                     | 该后台的最新查询。如果state状态是active（活跃的），此字段显示当前正在执行的查询。所有其他情况表示上一个查询。 |
  | waiting          | Boolean                  | 如果后台当前正等待锁则为true。                               |
  | xact_start       | timestamp with time zone | 启动当前事务的时间，如果没有事务是活跃的，则为null。如果当前查询是首个事务，则这列等同于query_start列。 |
  | query_start      | timestamp with time zone | 开始当前活跃查询的时间，如果state的值不是active，则这个值是上一个查询的开始时间。 |
  | backend_start    | timestamp with time zone | 该过程开始的时间，即当客户端连接服务器时。                   |
  | state_change     | timestamp with time zone | 上次状态改变的时间。                                         |
  | client_addr      | inet                     | 连接到该后台的客户端的IP地址。如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum。 |
  | client_hostname  | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。 |
  | client_port      | integer                  | 客户端用于与后台通讯的TCP端口号，如果使用Unix套接字，则为-1。 |
  | enqueue          | text                     | 该字段暂不支持。                                             |
  | query_id         | bigint                   | 查询语句的ID。                                               |
  | srespool         | name                     | 资源池名字。                                                 |
  | global_sessionid | text                     | 全局会话id。                                                 |
  | unique_sql_id    | bigint                   | 语句的unique sql id。                                        |
  | trace_id         | text                     | 驱动传入的trace id，与应用的一次请求相关联。                 |

- pg_stat_get_activity_with_conninfo(integer)

  描述：返回一个关于带有特殊PID的后台进程的记录信息，当参数为NULL时，则返回每个活动的后台进程的记录。初始用户、系统管理员和monadmin可以查看所有的数据，普通用户只能查询自己的结果。

  返回值类型：setofrecord

  返回值说明如下。

  **表 7** 返回值说明

  | 返回值           | 返回值类型               | 返回值说明                                                   |
  | :--------------- | :----------------------- | :----------------------------------------------------------- |
  | datid            | oid                      | 用户会话在后台连接到的数据库OID。                            |
  | pid              | bigint                   | 后台线程ID。                                                 |
  | sessionid        | bigint                   | 会话ID。                                                     |
  | usesysid         | oid                      | 登录该后台的用户OID。                                        |
  | application_name | text                     | 连接到该后台的应用名。                                       |
  | state            | text                     | 改后台当前总体状态。                                         |
  | query            | text                     | 该后台的最新查询。如果state状态是active（活跃的），此字段显示当前正在执行的查询。所有其他情况表示上一个查询。 |
  | waiting          | Boolean                  | 如果后台当前正等待锁则为true                                 |
  | xact_start       | timestamp with time zone | 启动当前事务的时间，如果没有事务是活跃的，则为null。如果当前查询是首个事务，则这列等同于query_start列。 |
  | query_start      | timestamp with time zone | 开始当前活跃查询的时间，如果state的值不是active，则这个值是上一个查询的开始时间。 |
  | backend_start    | timestamp with time zone | 该过程开始的时间，即当客户端连接服务器时。                   |
  | state_change     | timestamp with time zone | 上次状态改变的时间。                                         |
  | client_addr      | inet                     | 连接到该后台的客户端的IP地址。如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum |
  | client_hostname  | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。 |
  | client_port      | integer                  | 客户端用于与后台通讯的TCP端口号，如果使用Unix套接字，则为-1。 |
  | enqueue          | text                     | 该字段暂不支持。                                             |
  | query_id         | bigint                   | 查询语句的ID。                                               |
  | connection_info  | text                     | json格式字符串，记录当前连接数据库的驱动类型、驱动版本号、当前驱动的部署路径、进程属主用户等信息。 |
  | srespool         | name                     | 资源池名字。                                                 |
  | global_sessionid | text                     | 全局会话ID。                                                 |
  | unique_sql_id    | bigint                   | 语句的unique sql id。                                        |
  | trace_id         | text                     | 驱动传入的trace id，与应用的一次请求相关联。                 |

- pg_user_iostat(text)

  描述：显示和当前用户执行作业正在运行时的IO负载管理相关信息。

  返回值类型：record

  函数返回字段说明如下：

  | 名称           | 类型 | 描述                                                         |
  | :------------- | :--- | :----------------------------------------------------------- |
  | userid         | oid  | 用户id。                                                     |
  | min_curr_iops  | int4 | 当前该用户io在数据库节点中的最小值。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | max_curr_iops  | int4 | 当前该用户io在数据库节点中的最大值。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | min_peak_iops  | int4 | 该用户io峰值中，数据库节点的最小值。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | max_peak_iops  | int4 | 该用户io峰值中，数据库节点的最大值。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | io_limits      | int4 | 用户指定的资源池所设置的io_limits。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | io_priority    | text | 该用户所设io_priority。对于行存，以万次/s为单位；对于列存，以次/s为单位。 |
  | curr_io_limits | int4 | 使用io_priority管控io时的实时io_limits值。                   |

- pg_stat_get_function_calls(oid)

  描述：函数已被调用次数。

  返回值类型：bigint

- pg_stat_get_function_self_time(oid)

  描述：只有在此函数上所花费的时间。此函数调用其它函数上花费的时间被排除在外。

  返回值类型：bigint

- pg_stat_get_backend_idset()

  描述：设置当前活动的服务器进程数（从1到活动服务器进程的数量）。

  返回值类型：setofinteger

- pg_stat_get_backend_pid(integer)

  描述：给定的服务器线程的线程ID。

  返回值类型：bigint

- pg_stat_get_backend_dbid(integer)

  描述：给定服务器进程的数据库ID。

  返回值类型：oid

- pg_stat_get_backend_userid(integer)

  描述：给定服务器进程的用户ID。

  返回值类型：oid

- pg_stat_get_backend_activity(integer)

  描述：给定服务器进程的当前活动查询，仅在调用者是系统管理员或被查询会话的用户，并且打开track_activities的时候才能获得结果。

  返回值类型：text

- pg_stat_get_backend_waiting(integer)

  描述：如果给定服务器进程在等待某个锁，并且调用者是系统管理员或被查询会话的用户，并且打开track_activities的时候才返回真。

  返回值类型：Boolean

- pg_stat_get_backend_activity_start(integer)

  描述：给定服务器进程当前正在执行的查询的起始时间，仅在调用者是系统管理员或被查询会话的用户，并且打开track_activities的时候才能获得结果。

  返回值类型：timestampwithtimezone

- pg_stat_get_backend_xact_start(integer)

  描述：给定服务器进程当前正在执行的事务的开始时间，但只有当前用户是系统管理员或被查询会话的用户，并且打开track_activities的时候才能获得结果。

  返回值类型：timestampwithtimezone

- pg_stat_get_backend_start(integer)

  描述：给定服务器进程启动的时间，如果当前用户不是系统管理员或被查询的后端的用户，则返回NULL。

  返回值类型：timestampwithtimezone

- pg_stat_get_backend_client_addr(integer)

  描述：连接到给定客户端后端的IP地址。如果是通过Unix域套接字连接的则返回NULL；如果当前用户不是系统管理员或被查询会话的用户，也返回NULL。

  返回值类型：inet

- pg_stat_get_backend_client_port(integer)

  描述：连接到给定客户端后端的TCP端口。如果是通过Unix域套接字连接的则返回-1；如果当前用户不是系统管理员或被查询会话的用户，也返回NULL。

  返回值类型：integer

- pg_stat_get_bgwriter_timed_checkpoints()

  描述：后台写进程开启定时检查点的时间（因为checkpoint_timeout时间已经过期了）。

  返回值类型：bigint

- pg_stat_get_bgwriter_requested_checkpoints()

  描述：后台写进程开启基于后端请求的检查点的时间，因为已经超过了checkpoint_segments或因为已经执行了CHECKPOINT。

  返回值类型：bigint

- pg_stat_get_bgwriter_buf_written_checkpoints()

  描述：在检查点期间后台写进程写入的缓冲区数目。

  返回值类型：bigint

- pg_stat_get_bgwriter_buf_written_clean()

  描述：为日常清理脏块，后台写进程写入的缓冲区数目。

  返回值类型：bigint

- pg_stat_get_bgwriter_maxwritten_clean()

  描述：后台写进程停止清理扫描的时间，因为已经写入了更多的缓冲区（相比bgwriter_lru_maxpages参数声明的缓冲区数）。

  返回值类型：bigint

- pg_stat_get_buf_written_backend()

  描述：后端进程写入的缓冲区数，因为它们需要分配一个新的缓冲区。

  返回值类型：bigint

- pg_stat_get_buf_alloc()

  描述：分配的总缓冲区数。

  返回值类型：bigint

- pg_stat_clear_snapshot()

  描述：清理当前的统计快照。

  返回值类型：void

- pg_stat_reset()

  描述：为当前数据库重置统计计数器为0（需要系统管理员权限）。

  返回值类型：void

- pg_stat_reset_shared(text)

  描述：重置shared cluster每个节点当前数据统计计数器为0（需要系统管理员权限）。

  返回值类型：void

- pg_stat_reset_single_table_counters(oid)

  描述：为当前数据库中的一个表或索引重置统计为0（需要系统管理员权限）。

  返回值类型：void

- pg_stat_reset_single_function_counters(oid)

  描述：为当前数据库中的一个函数重置统计为0（需要系统管理员权限）。

  返回值类型：void

- pg_stat_session_cu(int, int, int)

  描述：获取当前节点所运行session的CU命中统计信息。

  返回值类型：record

- pg_stat_get_cu_mem_hit(oid)

  描述：获取当前节点当前数据库中一个列存表的CU内存命中次数。

  返回值类型：bigint

- pg_stat_get_cu_hdd_sync(oid)

  描述：获取当前节点当前数据库中一个列存表从磁盘同步读取CU次数。

  返回值类型：bigint

- pg_stat_get_cu_hdd_asyn(oid)

  描述：获取当前节点当前数据库中一个列存表从磁盘异步读取CU次数。

  返回值类型：bigint

- pg_stat_get_db_cu_mem_hit(oid)

  描述：获取当前节点一个数据库CU内存命中次数。

  返回值类型：bigint

- pg_stat_get_db_cu_hdd_sync(oid)

  描述：获取当前节点一个数据库从磁盘同步读取CU次数。

  返回值类型：bigint

- fenced_udf_process(integer)

  描述：查看本地UDF Master和Work进程数。入参为1时查看master进程数，入参为2时查看worker进程数，入参为3时杀死所有worker进程。

  返回值类型：text

- total_cpu()

  描述：获取当前节点使用的CPU时间，单位是jiffies。

  返回值类型：bigint

- mot_global_memory_detail()

  描述：检查MOT全局内存的大小，主要包括数据和索引。

  返回值类型：record

- mot_local_memory_detail()

  描述：检查MOT局部内存的大小，主要包括数据和索引。

  返回值类型：record

- mot_session_memory_detail()

  描述：检查所有会话对MOT内存的使用情况。

  返回值类型：record

- total_memory()

  描述：获取当前节点使用的虚拟内存大小，单位KB。

  返回值类型：bigint

- pg_stat_get_db_cu_hdd_asyn(oid)

  描述：获取当前节点一个数据库从磁盘异步读取CU次数。

  返回值类型：bigint

- pg_stat_bad_block(text, int, int, int, int, int, timestamp with time zone, timestamp with time zone)

  描述：获取当前节点自启动后，读取出现Page/CU的损坏信息。

  例: select * from pg_stat_bad_block();

  返回值类型：record

- pg_stat_bad_block_clear()

  描述：清理节点记录的读取出现的Page/CU损坏信息（需要系统管理员权限）。

  返回值类型：void

- gs_respool_exception_info(pool text)

  描述：查看某个资源池关联的查询规则信息。

  返回值类型：record

- gs_control_group_info(pool text)

  描述：查看资源池关联的控制组信息

  返回值类型：record

  返回信息如下：

  | 属性     | 属性值              | 描述                                         |
  | :------- | :------------------ | :------------------------------------------- |
  | name     | class_a:workload_a1 | class和workload名称                          |
  | class    | class_a             | Class控制组名称                              |
  | workload | workload_a1         | Workload控制组名称                           |
  | type     | DEFWD               | 控制组类型（Top、CLASS、BAKWD、DEFWD、TSWD） |
  | gid      | 87                  | 控制组id                                     |
  | shares   | 30                  | 占父节点CPU资源的百分比                      |
  | limits   | 0                   | 占父节点CPU核数的百分比                      |
  | rate     | 0                   | Timeshare中的分配比例                        |
  | cpucores | 0-3                 | CPU核心数                                    |

- gs_all_control_group_info()

  描述：查看数据库内所有的控制组信息。

  返回值类型：record

- gs_get_control_group_info()

  描述：查看所有的控制组信息。

  返回值类型：record

- get_instr_workload_info(integer)

  描述：获取数据库主节点上事务量信息，事务时间信息。

  返回值类型：record

  | 属性                | 属性值       | 描述                               |
  | :------------------ | :----------- | :--------------------------------- |
  | resourcepool_oid    | 10           | 资源池的oid(逻辑同负载等价)        |
  | commit_counter      | 4            | 前端事务commit数量                 |
  | rollback_counter    | 1            | 前端事务rollback数量               |
  | resp_min            | 949          | 前端事务最小响应时间（单位：微秒） |
  | resp_max            | 201891       | 前端事务最大响应时间（单位：微秒） |
  | resp_avg            | 43564        | 前端事务平均响应时间(单位：微秒)   |
  | resp_total          | 217822       | 前端事务总响应时间（单位：微秒）   |
  | bg_commit_counter   | 910          | 后端事务commit数量                 |
  | bg_rollback_counter | 0            | 后端事务rollback数量               |
  | bg_resp_min         | 97           | 后端事务最小响应时间（单位：微秒） |
  | bg_resp_max         | 678080687    | 后端事务最大响应时间（单位：微秒） |
  | bg_resp_avg         | 327847884    | 后端事务平均响应时间（单位：微秒） |
  | bg_resp_total       | 298341575300 | 后端事务总响应时间（单位：微秒）   |

- pv_instance_time()

  描述：获取当前节点上各个关键阶段的时间消耗。

  返回值类型：record

  | Stat_name属性       | 属性值  | 描述                                                       |
  | :------------------ | :------ | :--------------------------------------------------------- |
  | DB_TIME             | 1062385 | 所有线程端到端的墙上时间（WALL TIME）消耗总和(单位： 微秒) |
  | CPU_TIME            | 311777  | 所有线程CPU时间消耗总和(单位： 微秒)                       |
  | EXECUTION_TIME      | 380037  | 消耗在执行器上的时间总和(单位： 微秒)                      |
  | PARSE_TIME          | 6033    | 消耗在SQL解析上的时间总和(单位： 微秒)                     |
  | PLAN_TIME           | 173356  | 消耗在执行计划生成上的时间总和(单位： 微秒)                |
  | REWRITE_TIME        | 2274    | 消耗在查询重写上的时间总和(单位： 微秒)                    |
  | PL_EXECUTION_TIME   | 0       | 消耗在PL/SQL执行上的时间总和(单位： 微秒)                  |
  | PL_COMPILATION_TIME | 557     | 消耗在SQL编译上的时间总和(单位： 微秒)                     |
  | NET_SEND_TIME       | 1673    | 消耗在网络发送上的时间总和(单位： 微秒)                    |
  | DATA_IO_TIME        | 426622  | 消耗在数据读写上的时间总和(单位： 微秒)                    |

- DBE_PERF.get_global_instance_time()

  描述：提供MogDB各个关键阶段的时间消耗，查询该函数必须具有sysadmin权限。

  返回值类型：record

- get_instr_unique_sql()

  描述：获取当前节点的执行语句（归一化SQL）信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- reset_unique_sql(text, text, bigint)

描述：重置系统执行语句（归一化SQL）信息，执行该函数必须具有sysadmin权限。第一个参数取值范围“global/local”，global表示清理所有节点上的信息，local表示只清理当前节点；第二参数取值范围“ALL/BY_USERID/BY_CNID”，ALL表示清理所有信息，BY_USERID表示通过指定USERID清理只属于该用户的sql信息，BY_CNID表示清理系统中涉及到该数据库主节点的sql信息；第三个参数表示具体的CNID和USERID，如果第二个参数为ALL，第三个参数不起作用，可以取任意值。

返回值类型：boolean

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 此函数中所说节点指分布式节点，当前MogDB为集中式数据库，global与local功能一致，取值范围不支持BY_CNID。

- get_instr_wait_event(NULL)

  描述：获取当前节点event等待的统计信息。

  返回值类型：record

- get_instr_user_login()

  描述：获取当前节点的用户登入登出次数信息，查询该函数必须具有sysadmin或者monitor admin权限。

  返回值类型：record

- get_instr_rt_percentile(integer)

  描述：获取数据库SQL响应时间P80、P95分布信息。

  返回值类型：record

- get_node_stat_reset_time()

  描述：获取当前节点的统计信息重置（重启、主备倒换、数据库删除）时间。

  返回值类型：record

- DBE_PERF.get_global_os_runtime()

  描述：显示当前操作系统运行的状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_os_threads()

  描述：提供MogDB中所有正常节点下的线程状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_workload_sql_count()

  描述：提供MogDB中不同负载SELECT，UPDATE，INSERT，DELETE，DDL，DML，DCL计数信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_workload_sql_elapse_time()

  描述：提供MogDB中不同负载SELECT，UPDATE，INSERT，DELETE，响应时间信息（TOTAL,AVG, MIN, MAX），查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_workload_transaction()

  描述：获取MogDB内所有节点上的事务量信息，事务时间信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_session_stat()

描述：获取MogDB节点上的会话状态信息，查询该函数必须具有sysadmin权限。

返回值类型：record

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 状态信息有17项：commit、rollback、sql、table_scan、blocks_fetched、physical_read_operation、 shared_blocks_dirtied、local_blocks_dirtied、shared_blocks_read、local_blocks_read、 blocks_read_time、blocks_write_time、sort_imemory、sort_idisk、cu_mem_hit、 cu_hdd_sync_read、cu_hdd_asyread。

- DBE_PERF.get_global_session_time()

  描述：提供MogDB各节点各个关键阶段的时间消耗，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_session_memory()

  描述：汇聚各节点的Session级别的内存使用情况，包含执行作业在数据节点上Postgres线程和Stream线程分配的所有内存，单位为MB，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_session_memory_detail()

  描述：汇聚各节点的线程的内存使用情况，以MemoryContext节点来统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- create_wlm_session_info(int flag)

  描述：将当前内存中记录的TopSQL查询语句级别相关统计信息清理。该函数只有管理员用户可以执行。

  返回值类型：int

- pg_stat_get_wlm_session_info(int flag)

  描述：获取当前内存中记录的TopSQL查询语句级别相关统计信息，当传入的参数不为0时，会将这部分信息从内存中清理掉。该函数只有system admin和monitor admin用户可以执行。

  返回值类型：record

- gs_paxos_stat_replication()

  描述：在主机端查询备机信息。目前只支持集中式DCF模式。

  返回值类型：setofrecord

  返回字段说明如下：

  | 字段名称                 | 字段类型 | 字段说明                            |
  | ------------------------ | -------- | ----------------------------------- |
  | local_role               | text     | 发送日志节点的角色。                |
  | peer_role                | text     | 接收日志节点的角色。                |
  | local_dcf_role           | text     | 发送日志节点的DCF角色。             |
  | peer_dcf_role            | text     | 接收日志节点的DCF角色。             |
  | peer_state               | text     | 接收日志节点的状态。                |
  | sender_write_location    | text     | 发送日志节点写到xlog buffer的位置。 |
  | sender_commit_location   | text     | 发送日志节点DCF日志达成一致性点。   |
  | sender_flush_location    | text     | 发送日志节点写到xlog disk的位置。   |
  | sender_replay_location   | text     | 发送日志节点replay的位置。          |
  | receiver_write_location  | text     | 接收日志节点写到xlog buffer的位置。 |
  | receiver_commit_location | text     | 接收日志节点DCF日志达成一致性点。   |
  | receiver_flush_location  | text     | 接收日志节点写到xlog disk的位置。   |
  | receiver_replay_location | text     | 接收日志节点重演xlog的位置。        |
  | sync_percent             | text     | 同步百分比。                        |
  | dcf_run_mode             | int4     | DCF同步模式。                       |
  | channel                  | text     | 信道信息。                          |

- gs_wlm_get_resource_pool_info(int)

  描述：获取所有用户的资源使用统计信息，入参为int类型可以为可以为任意int值或NULL。

  返回值类型：record

- gs_wlm_get_all_user_resource_info()

  描述：获取所有用户的资源使用统计信息。

  返回值类型：record

- gs_wlm_get_user_info(int)

  描述：获取所有用户的相关信息，入参为int类型可以为可以为任意int值或NULL。该函数只有sysadmin权限的用户可以执行。

  返回值类型：record

- gs_wlm_get_workload_records()

  描述：获取动态负载管理下的所有作业信息，该函数只在动态负载管理开的情况下有效。

  返回值类型：record

- gs_wlm_readjust_user_space()

  描述：修正所有用户的存储空间使用情况。该函数只有管理员用户可以执行。

  返回值类型：record

- gs_wlm_readjust_user_space_through_username(text name)

  描述：修正指定用户的存储空间使用情况。该函数普通用户只能修正自己的使用情况，只有管理员用户可以修正所有用户的使用情况。当name指定位“0000”，表示需要修正所有用户的使用情况。

  返回值类型：record

- gs_wlm_readjust_user_space_with_reset_flag(text name, boolean isfirst)

  描述：修正指定用户的存储空间使用情况。入参isfirst为true表示从0开始统计，否则从上一次结果继续统计。该函数普通用户只能修正自己的使用情况，只有管理员用户可以修正所有用户的使用情况。当name指定位“0000”，表示需要修正所有用户的使用情况。

  返回值类型：record

- gs_wlm_session_respool(bigint)

  描述：获取当前所有后台线程的session resouce pool相关信息，入参为bigint类型可以为可以为任意bigint值或NULL。

  返回值类型：record

- gs_wlm_get_session_info()

  描述：目前该接口已废弃，暂不可用。

- gs_wlm_get_user_session_info()

  描述：目前该接口已废弃，暂不可用。

- gs_io_wait_status()

  描述：目前该接口不支持单机和集中式，暂不可用。

- global_stat_get_hotkeys_info()

  描述：获取整个数据库实例中热点key的统计情况。目前该接口不支持单机和集中式，暂不可用。

- global_stat_clean_hotkeys()

  描述：清理整个数据库实例中热点key的统计信息。目前该接口不支持单机和集中式，暂不可用。

- DBE_PERF.get_global_session_stat_activity()

  描述：汇聚MogDB内各节点上正在运行的线程相关的信息，查询该函数必须具有monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_thread_wait_status()

  描述：汇聚所有节点上工作线程（backend thread）以及辅助线程（auxiliary thread）的阻塞等待情况，查询该函数必须具有sysadmin和monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_operator_history_table()

  描述：汇聚当前用户数据库主节点上执行作业结束后的算子相关记录（持久化），查询该函数必须具有sysadmin和monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_operator_history()

  描述：汇聚当前用户数据库主节点上执行作业结束后的算子相关记录，查询该函数必须具有sysadmin和monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_operator_runtime()

  描述：汇聚当前用户数据库主节点上执行作业实时的算子相关记录，查询该函数必须具有sysadmin和monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_statement_complex_history()

  描述：汇聚当前用户数据库主节点上复杂查询的历史记录，查询该函数必须具有monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_statement_complex_history_table()

  描述：汇聚当前用户数据库主节点上复杂查询的历史记录（持久化），查询该函数必须具有monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_statement_complex_runtime()

  描述：汇聚当前用户数据库主节点上复杂查询的实时信息，查询该函数必须具有sysadmin和monadmin权限。

  返回值类型：record

- DBE_PERF.get_global_memory_node_detail()

  描述：汇聚所有节点某个数据库节点内存使用情况，查询该函数必须具有monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_shared_memory_detail()

  描述：汇聚所有节点已产生的共享内存上下文的使用信息，查询该函数必须具有monitoradmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_all_indexes()

  描述：汇聚所有节点当前数据库中的每个索引行，显示特定索引的I/O的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_all_tables()

  描述：显示汇聚各节点数据中每个表（包括TOAST表）的一行的统计信息

  返回值类型：record

- DBE_PERF.get_global_stat_all_tables()

  描述：显示各节点数据中每个表（包括TOAST表）的一行的统计信息。

  返回值类型：record

- DBE_PERF.get_local_toastname_and_toastindexname()

  描述：提供本地toast表的name和index和其关联表的对应关系，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_all_indexes()

  描述：统计所有节点当前数据库中的每个索引行，显示特定索引的I/O的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_all_sequences()

  描述：提供命名空间中所有sequences的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_all_tables()

  描述：汇聚各节点的数据库中每个表I/O的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_all_tables()

  描述：统计MogDB内数据库中每个表I/O的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_local_toast_relation()

  描述：提供本地toast表的name和其关联表的对应关系，查询该函数必须具有sysadmin权限

  返回值类型：record

- DBE_PERF.get_global_statio_sys_indexes()

  描述：汇聚各节点的命名空间中所有系统表索引的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_sys_indexes()

  描述：统计各节点的命名空间中所有系统表索引的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_sys_sequences()

  描述：提供命名空间中所有系统表为sequences的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_sys_tables()

  描述：提供各节点的命名空间中所有系统表的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_sys_tables()

  描述：MogDB内汇聚命名空间中所有系统表的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_user_indexes()

  描述：各节点的命名空间中所有用户关系表索引的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_user_indexes()

  描述：MogDB内汇聚命名空间中所有用户关系表索引的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_user_sequences()

  描述：显示各节点的命名空间中所有用户的sequences的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statio_user_tables()

  描述：显示各节点的命名空间中所有用户关系表的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statio_user_tables()

  描述：MogDB内汇聚命名空间中所有用户关系表的IO状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_stat_db_cu()

  描述：视图查询MogDB各个节点，每个数据库的CU命中情况，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_all_indexes()

  描述：汇聚所有节点数据库中每个索引的统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_all_indexes()

  描述：统计所有节点数据库中每个索引的统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_sys_tables()

  描述：汇聚各节点pg_catalog、information_schema模式的所有命名空间中系统表的统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_sys_tables()

  描述：统计各节点pg_catalog、information_schema模式的所有命名空间中系统表的统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_sys_indexes()

  描述：汇聚各节点pg_catalog、information_schema模式中所有系统表的索引状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_sys_indexes()

  描述：统计各节点pg_catalog、information_schema模式中所有系统表的索引状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_user_tables()

  描述：汇聚所有命名空间中用户自定义普通表的状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_user_tables()

  描述：统计所有命名空间中用户自定义普通表的状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_user_indexes()

  描述：汇聚所有数据库中用户自定义普通表的索引状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_user_indexes()

  描述：统计所有数据库中用户自定义普通表的索引状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_database()

  描述：汇聚所有节点数据库统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_database_conflicts()

  描述：统计所有节点数据库统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_xact_all_tables()

  描述：汇聚命名空间中所有普通表和toast表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_xact_all_tables()

  描述：统计命名空间中所有普通表和toast表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_xact_sys_tables()

  描述：汇聚所有节点命名空间中系统表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_xact_sys_tables()

  描述：统计所有节点命名空间中系统表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_xact_user_tables()

  描述：汇聚所有节点命名空间中用户表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_stat_xact_user_tables()

  描述：统计所有节点命名空间中用户表的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_user_functions()

  描述：汇聚所有节点命名空间中用户定义函数的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_xact_user_functions()

  描述：统计所有节点命名空间中用户定义函数的事务状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_stat_bad_block()

  描述：汇聚所有节点表、索引等文件的读取失败信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_file_redo_iostat()

  描述：统计所有节点表、索引等文件的读取失败信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_file_iostat()

  描述：汇聚所有节点数据文件IO的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_locks()

  描述：汇聚所有节点的锁信息，查询该函数必须具有sysadmin和monadmin权限。

  返回值类型：record

- DBE_PERF.get_global_replication_slots()

  描述：汇聚所有节点上逻辑复制信息，查询该函数必须具有sysadmin和monadmin权限。

  返回值类型：record

- DBE_PERF.get_global_bgwriter_stat()

  描述：汇聚所有节点后端写进程活动的统计信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_replication_stat()

  描述：汇聚各节点日志同步状态信息，如发起端发送日志位置、收端接收日志位置等，查询该函数必须具有sysadmin和monadmin权限。

  返回值类型：record

- DBE_PERF.get_global_transactions_running_xacts()

  描述：汇聚各节点运行事务的信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_transactions_running_xacts()

  描述：统计各节点运行事务的信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_transactions_prepared_xacts()

  描述：汇聚各节点当前准备好进行两阶段提交的事务的信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_transactions_prepared_xacts()

  描述：统计各节点当前准备好进行两阶段提交的事务的信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_statement()

  描述：汇聚各节点历史执行语句状态信息，查询该函数必须具有monitor admin和sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_statement_count()

  描述：汇聚各节点SELECT、UPDATE、INSERT、DELETE，响应时间信息（TOTAL、AVG、MIN、 MAX），查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_config_settings()

  描述：汇聚各节点GUC参数配置信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_wait_events()

  描述：汇聚各节点wait events状态信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_statement_responsetime_percentile()

  描述：获取MogDB SQL响应时间P80、P95分布信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_summary_user_login()

  描述：统计MogDB各节点用户登入登出次数信息，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.get_global_record_reset_time()

  描述：汇聚MogDB统计信息重置（重启、主备倒换、数据库删除）时间，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.track_memory_context(context_list text)

  描述：设置需要统计内存申请详细信息的内存上下文。入参为内存上下文的名称，使用“，”分隔，如“ThreadTopMemoryContext, SessionCacheMemoryContext”，注意该内存上下文名称是上下文敏感的。此外，单个内存上下文的长度为63，超过的部分会被截断。而且一次能够统计的内存上下文上限为16个，设置超过16个内存上下文会设置失败。每一次调用该函数都会将上次统计的结果清空，当入参指定为“”时，表示取消该统计功能。只有初始用户（super user）或者具有monadmin权限的用户可以执行该函数。

  返回值类型：boolean

- DBE_PERF.track_memory_context_detail()

  描述：获取DBE_PERF.track_memory_context函数指定的内存上下文的内存申请详细信息。返回值的定义见视图DBE_PERF.track_memory_context_detail。只有初始用户（super user）或者具有monadmin权限的用户可以执行该函数。

  返回值类型：record

- pg_stat_get_mem_mbytes_reserved(tid)

  描述：统计资源管理相关变量值，仅用于定位问题使用。

  参数：线程id。

  返回值类型：text

- gs_wlm_user_resource_info(name text)

  描述：查询具体某个用户的资源限额和资源使用情况。

  返回值类型：record

- pg_stat_get_file_stat()

  描述：通过对数据文件IO的统计，反映数据的IO性能，用以发现IO操作异常等性能问题。

  返回值类型: record

- pg_stat_get_redo_stat()

  描述：用于统计会话线程日志回放情况。

  返回值类型: record

- pg_stat_get_status(int8)

  描述：可以检测当前实例中工作线程（backend thread）以及辅助线程（auxiliary thread）的阻塞等待情况。

  返回值类型：record

- get_local_rel_iostat()

  描述：查询当前节点的数据文件IO状态累计值。

  返回值类型：record

- DBE_PERF.get_global_rel_iostat()

  描述：汇聚所有节点数据文件IO的统计，查询该函数必须具有sysadmin权限。

  返回值类型：record

- DBE_PERF.global_threadpool_status()

  描述：显示在所有节点上的线程池中工作线程及会话的状态信息。函数返回信息具体字段GLOBAL_THREADPOOL_STATUS字段。

  返回值类型：record

- remote_bgwriter_stat()

  描述：显示数据库所有实例的bgwriter线程刷页信息，候选buffer链中页面个数，buffer淘汰信息（本节点除外、DN上不可使用）。

  返回值类型：record

- pv_os_run_info

  描述：显示当前操作系统运行的状态信息，具体字段信息参考GS_OS_RUN_INFO。

  参数：nan

  返回值类型：setof record

- pv_session_stat

  描述：以会话线程或AutoVacuum线程为单位，统计会话状态信息，具体字段信息参考GS_SESSION_STAT。

  参数：nan

  返回值类型：setof record

- pv_session_time

  描述：用于统计会话线程的运行时间信息，及各执行阶段所消耗时间，具体字段信息参考GS_SESSION_TIME。

  参数：nan

  返回值类型：setof record

- pg_stat_get_db_temp_bytes

  描述：用于统计通过数据库查询写入临时文件的数据总量。计算所有临时文件，不论为什么创建临时文件，而且不管log_temp_files设置。

  参数：oid

  返回值类型：bigint

- pg_stat_get_db_temp_files

  描述：通过数据库查询创建的临时文件数量。计算所有临时文件，不论为什么创建临时文件（比如排序或者哈希），而且不管log_temp_files设置。

  参数：oid

  返回值类型：bigint

- remote_candidate_stat()

  描述：用于显示数据库所有实例的检查点信息和各类日志刷页情况（本节点除外），集中式不支持。

  返回值类型：record

- dbe_perf.gs_stat_activity_timeout(int)

  描述：获取当前节点上执行时间超过超时阈值的查询作业信息。需要GUC参数track_activities设置为on才能正确返回结果。超时阈值的取值范围是0~2147483。

  返回值类型：setof record

  | 名称             | 类型        | 描述                       |
  | :--------------- | :---------- | :------------------------- |
  | database         | name        | 用户会话连接的数据库名称。 |
  | pid              | bigint      | 后台线程ID。               |
  | sessionid        | bigint      | 会话ID。                   |
  | usesysid         | oid         | 登录该后台的用户OID。      |
  | application_name | text        | 连接到该后台的应用名。     |
  | query            | text        | 该后台正在执行的查询。     |
  | xact_start       | timestamptz | 启动当前事务的时间。       |
  | query_start      | timestamptz | 开始当前查询的时间。       |
  | query_id         | bigint      | 查询语句ID。               |

- gs_wlm_user_resource_info(name text)

  描述：查询具体某个用户的资源限额和资源使用情况。普通用户只能查询到自己相关的信息，管理员权限的用户可以查看全部用户的信息。

  返回值类型：record

- create_wlm_instance_statistics_info

  描述：将当前实例的历史监控数据进行持久化保存。

  参数：nan

  返回值类型：integer

- gs_session_memory

描述：统计Session级别的内存使用情况，包含执行作业在数据节点上Postgres线程和Stream线程分配的所有内存。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 若GUC参数enable_memory_limit=off，该函数不能使用。

返回值类型：record

**表 8** 返回值说明

| 名称     | 类型    | 描述                                               |
| :------- | :------ | :------------------------------------------------- |
| sessid   | text    | 线程启动时间+线程标识。                            |
| init_mem | integer | 当前正在执行作业进入执行器前已分配的内存，单位MB。 |
| used_mem | integer | 当前正在执行作业已分配的内存，单位MB。             |
| peak_mem | integer | 当前正在执行作业已分配的内存峰值，单位MB。         |

- gs_wlm_persistent_user_resource_info()

  描述：将当前所有的用户资源使用统计信息归档到gs_wlm_user_resource_history系统表中，只有sysadmin有权限查询。

  返回值类型：record

- create_wlm_operator_info(int flag)

  描述：将当前内存中记录的TopSQL算子级别相关统计信息清理，当传入的参数大于0时，会将这部分信息归档到gs_wlm_operator_info和gs_wlm_ec_operator_info中，否则不会归档。该函数只有sysadmin权限的用户可以执行。

  返回值类型：int

- GS_ALL_NODEGROUP_CONTROL_GROUP_INFO(text)

描述：提供了所有逻辑数据库实例的控制组信息。该函数在调用的时候需要指定要查询逻辑数据库实例的名称。例如要查询'installation'逻辑数据库实例的控制组信息：

```sql
SELECT * FROM GS_ALL_NODEGROUP_CONTROL_GROUP_INFO('installation')
```

返回值类型：record

函数返回字段如下：

| 名称     | 类型   | 描述                          |
| :------- | :----- | :---------------------------- |
| name     | text   | 控制组的名称。                |
| type     | text   | 控制组的类型。                |
| gid      | bigint | 控制组ID。                    |
| classgid | bigint | Workload所属Class的控制组ID。 |
| class    | text   | Class控制组。                 |
| workload | text   | Workload控制组。              |
| shares   | bigint | 控制组分配的CPU资源配额。     |
| limits   | bigint | 控制组分配的CPU资源限额。     |
| wdlevel  | bigint | Workload控制组层级。          |
| cpucores | text   | 控制组使用的CPU核的信息。     |

- gs_total_nodegroup_memory_detail

  描述：返回当前数据库逻辑数据库使用内存的信息，单位为MB得到一个逻辑数据库。

  返回值类型：setof record

- local_redo_time_count()

  描述：返回本节点各个回放线程的各个流程的耗时统计（仅在备机上有有效数据）。

  返回值如下：

  local_redo_time_count返回参数说明。

  | 字段名      | 描述                                                         |
  | :---------- | :----------------------------------------------------------- |
  | thread_name | 线程名字                                                     |
  | step1_total | step1的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- batch redo：从队列中获取一条日志<br/>- redo manager：从队列中获取一条日志<br/>- redo worker：从队列中获取一条日志<br/>- trxn manager：从队列中读取一条日志<br/>- trxn worker：从队列中读取一条日志<br/>- read worker：从文件中读取一次xlog page（整体）<br/>- read page worker：从队列中获取一个日志<br/>- startup：从队列中获取一个日志<br/>并行回放：<br/>- page redo：从队列中获取一条日志<br/>- startup：读取一条日志 |
  | step1_count | step1的统计次数                                              |
  | step2_total | step2的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- batch redo：处理日志（整体）<br/>- redo manager：处理日志（整体）<br/>- redo worker：处理日志（整体）<br/>- trxn manager：处理日志（整体）<br/>- trxn worker：处理日志（整体）<br/>- redo worker：读取xlog page耗时<br/>- read page worker：生成和发送lsn forwarder<br/>- startup：check stop(是否回放到指定位置）<br/>并行回放：<br/>- page redo：处理日志（整体）<br/>- startup：check stop(是否回放到指定位置） |
  | step2_count | step2的统计次数                                              |
  | step3_total | step3的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- batch redo：更新standbystate<br/>- redo manager：数据日志处理<br/>- redo worker：回放page也日志（整体）<br/>- trxn manager：更新flush lsn<br/>- trxn worker：回放日志处理<br/>- redo worker：推进xlog segment<br/>- read page worker：获取一个新的item<br/>- startup：redo delay（延迟回放特性等待时间）<br/>并行回放：<br/>- page redo：更新standbystate<br/>- startup：redo delay（延迟回放特性等待时间） |
  | step3_count | step3的统计次数                                              |
  | step4_total | step4的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- batch redo：解析xlog<br/>- redo manager：DDL处理<br/>- redo worker：读取数据page页<br/>- trxn manager：同步等待时间<br/>- trxn worker：更新本线程lsn<br/>- read page worker：将日志放入分发线程<br/>- startup：分发（整体）<br/>并行回放：<br/>- page redo：undo 日志回放<br/>- startup：分发（整体） |
  | step4_count | step4的统计次数                                              |
  | step5_total | step5的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- batch redo：分发给redo manager<br/>- redo manager：分发给redo worker<br/>- redo worker：回放数据page页的日志<br/>- trxn manager：分发给trxn worker<br/>- trxn worker：强同步wait时间<br/>- read page worker：更新本线程lsn<br/>- startup：日志decode<br/>并行回放：<br/>- page redo：sharetrxn 日志回放<br/>- startup：日志回放 |
  | step5_count | step5的统计次数                                              |
  | step6_total | step6的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>- redo worker：回放非数据页page日志<br/>- trxn manager：全局lsn更新<br/>- read page worker：日志crc校验<br/>并行回放：<br/>- page redo：synctrxn 日志回放<br/>- startup：强同步等待 |
  | step6_count | step6的统计次数                                              |
  | step7_total | step7的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>redo worker：fsm更新<br/>并行回放：<br/>page redo：single 日志回放 |
  | step7_count | step7的统计次数                                              |
  | step8_total | step8的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>redo worker：强同步等待<br/>并行回放：<br/>page redo：all workers do 日志回放 |
  | step8_count | step8的统计次数                                              |
  | step9_total | step9的总时间，每个线程对应的流程如下：<br/>极致RTO：<br/>无<br/>并行回放：<br/>page redo：muliti workers do 日志回放 |
  | step9_count | step9的统计次数                                              |

- local_xlog_redo_statics()

  描述：返回本节点已经回放的各个类型类型的日志统计信息（仅在备机上有有效数据）。

  返回值如下：

  **表 9** local_xlog_redo_statics返回参数说明

  | 字段名    | 描述                                                         |
  | :-------- | :----------------------------------------------------------- |
  | xlog_type | 日志类型                                                     |
  | rmid      | resource manager id                                          |
  | info      | xlog operation                                               |
  | num       | 日志个数                                                     |
  | extra     | 针对page回放日志和xact日志有有效值。page页回放日志标识从磁盘读取page的个数。xact日志表示删除文件的个数。 |

- gs_get_shared_memctx_detail(text)

  描述：返回指定内存上下文上的内存申请的详细信息，包含每一处内存申请所在的文件、行号和大小（同一文件同一行大小会做累加）。只支持查询通过pg_shared_memory_detail视图查询出来的内存上下文，入参为内存上下文名称（即pg_shared_memory_detail返回结果的contextname列）。查询该函数必须具有sysadmin权限或者monitor admin权限。

  返回值类型：setof record

  | 名称 | 类型 | 描述                                             |
  | :--- | :--- | :----------------------------------------------- |
  | file | text | 申请内存所在文件的文件名。                       |
  | line | int8 | 申请内存所在文件的代码行号。                     |
  | size | int8 | 申请的内存大小，同一文件同一行多次申请会做累加。 |

  > **说明**：该视图不支持release版本小型化场景。

- gs_get_session_memctx_detail(text)

  描述：返回指定内存上下文上的内存申请的详细信息，包含每一处内存申请所在的文件、行号和大小（同一文件同一行大小会做累加）。仅在线程池模式下生效。只支持查询通过gs_session_memory_context视图查询出来的内存上下文，入参为内存上下文名称（即gs_session_memory_context返回结果的contextname列）。查询该函数必须具有sysadmin权限或者monitor admin权限。

  返回值类型：setof record

  | 名称 | 类型 | 描述                                             |
  | :--- | :--- | :----------------------------------------------- |
  | file | text | 申请内存所在文件的文件名。                       |
  | line | int8 | 申请内存所在文件的代码行号。                     |
  | size | int8 | 申请的内存大小，同一文件同一行多次申请会做累加。 |

  > **说明**：该视图仅在线程池模式下生效，且该视图不支持release版本小型化场景。

- gs_get_thread_memctx_detail(tid,text)

  描述：返回指定内存上下文上的内存申请的详细信息，包含每一处内存申请所在的文件，行号和大小（同一文件同一行大小会做累加）。只支持查询通过gs_thread_memory_context视图查询出来的内存上下文，第一个入参为线程id（即gs_thread_memory_context返回数据的tid列），第二个参数为内存上下文名称（即gs_thread_memory_context返回数据的contextname列）。查询该函数必须具有sysadmin权限或者monitor admin权限。

  返回值类型：setof record

  | 名称 | 类型 | 描述                                                         |
  | :--- | :--- | :----------------------------------------------------------- |
  | file | text | 申请内存所在文件的文件名。                                   |
  | line | int8 | 申请内存所在文件的代码行号。                                 |
  | size | int8 | 申请的内存大小，单位为byte，同一文件同一行多次申请会做累加。 |

  > **说明**：该视图不支持release版本小型化场景。

- DBE_PERF.standby_statement_history(bool \[, time1, time2\])

  参数：

  - bool only_slow：是否仅查询慢SQL，true为是，false或NULL为查询全部SQL。

  - VARIADIC timestamptz finish_time: 可选输入，最多输入两个时间time1, time2，表示查询的SQL的finish_time所属时间段。

  描述：备机中用来查询FULL SQL记录的函数，主机通过表statement_history进行查询，备机通过此函数进行查询。只有初始用户或者具有monadmin权限的用户可以执行该函数。

  返回值类型：record，同表statement_history。

  >**说明**：
  >
  >- 第一个bool参数的说明： true表示仅查询慢sql，相当于 select .. where is_slow_sql = true; false或NULL表示查询全部SQL，相当于不对is_slow_sql进行过滤。
  >
  >- 两个时间参数time1 time2的说明：表示查询的SQL的finish_time所属时间段, 分别表示起始与终止时间, 输入NULL或者不输入表示没有限制, 功能等同与 select .. where finish_time between time1 and time2。
  >
  >- 备机上数据并非存在表里，不存在start_time列的索引，推荐使用参数对finish_time进行条件查找，内部有对这进行一些性能的优化。但注意修改了系统时间可能会导致此功能不准确。
  >
  >- 查询结果基本会是按照finish_time自动进行降序（新到旧）排序的。
  >
  >- 除了FunctionScan算子计算过程本身之外，对于数据的扫描会额外占用16或32M内存来做一个临时buffer。
  >
  >- 此函数的稳定性等级为'v'。
  >
  >- 由于备机Full SQL采用异步下盘方式，所以用户SQL信息存储时刻可能有所滞后，建议客户查询此接口适当扩大时间查询范围。
