---
title: 数据损坏检测修复函数
summary: 数据损坏检测修复函数
author: Guo Huan
date: 2022-05-10
---

# 数据损坏检测修复函数

- gs_verify_data_file(verify_segment bool)

  描述：校验当前实例当前库是否存在文件丢失的情况。校验只包括数据表主文件是否有中间段丢失的情况。默认参数是false，表示不校验段页式表数据文件。参数设置为true时仅校验段页式表文件。默认只有初始化用户、具有sysadmin属性的用户以及在运维模式下具有运维管理员属性的用户可以查看，其余用户需要赋权后才可以使用。

  返回的结果：

  - 非段页式表：rel_oid和rel_name是对应文件的表oid和表名，miss_file_path表示丢失文件的相对路径。
  - 段页式表：因所有表存放在相同文件中，所以rel_oid和rel_name无法显示具体表的信息。对于段页式表，如果第一个文件损坏，不会检查出后面的.1 .2等文件。例如3、3.1、3.2损坏，只能检查出3损坏。当段页式文件不足5个时，使用函数检测时，未生成的文件也会校验出来，例如只有1和2文件，校验段页式时，也会检测出3，4，5文件。以下示例，第一个是校验非段页式表的示例，第二是校验段页式表的示例。

  参数说明：

  - verify_segment

    指定文件校验的范围。false校验非段页式表；true校验段页式表。

    取值范围：true和false，默认是false。

  返回值类型：record

  示例：

  校验非段页式表

  ```
  MogDB=# select * from gs_verify_data_file();
  node_name         | rel_oid |  rel_name    |  miss_file_path
  ------------------+---------+--------------+------------------
  dn_6001_6002_6003 |   16554 |     test     | base/16552/24745
  ```

  校验段页式表

  ```
  MogDB=# select * from gs_verify_data_file(true);
       node_name     | rel_oid | rel_name | miss_file_path
  -------------------+---------+----------+----------------
   dn_6001_6002_6003 |       0 | none     | base/16573/2
  ```

- gs_repair_file(tableoid Oid，path text, timeout int)

  描述：根据传入的参数修复文件，仅支持有正常主备连接的主DN使用。参数依据gs_verify_data_file函数返回的oid和路径填写。段页式表tableoid赋值为0到4,294,967,295的任意值（内部校验根据文件路径判断是否是段页式表文件，段页式表文件则不使用tableoid）。修复成功返回值为true，修复失败会显示具体失败原因。默认只有在主DN节点上，使用初始化用户、具有sysadmin属性的用户以及在运维模式下具有运维管理员属性的用户可以查看，其余用户需要赋权后才可以使用。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **注意：** 1. 当DN实例上存在文件损坏时，进行升主会校验出错，报PANIC退出无法升主，为正常现象。可在其他DN升主后，通过备DN自动修复进行修复。 2. 当文件存在但是大小为0时，此时不会去修复该文件，若想要修复该文件，需要将为0的文件删除后再修复。 3. 删除文件需要等文件fd自动关闭后再修复，人工操作可以执行重启进程、主备切换命令。

参数说明：

- tableoid

  要修复的文件对应的表oid，依据gs_verify_data_file函数返回的列表中rel_oid一列填写。

  取值范围： Oid，0 - 4294967295。注意：输入负值等都会被强制转成非负整数类型。

- path

  需要修复的文件路径，依据gs_verify_data_file函数返回的列表中miss_file_path一列填写。

  取值范围：字符串。

- timeout

  等待备DN回放的时长，修复文件需要等待备DN回放到当前主DN对应的位置，根据备DN回放所需时长设定。

  取值范围：60s - 3600s。

返回值类型：bool

示例：

````
```
MogDB=# select * from gs_repair_file(16554,'base/16552/24745',360);
gs_repair_file
----------------
t
```
````

- local_bad_block_info()

  描述：显示本实例页面损坏的情况。从磁盘读取页面，发现页面CRC校验失败时进行记录。默认只有初始化用户、具有sysadmin属性的用户、具有监控管理员属性的用户以及在运维模式下具有运维管理员属性的用户、以及监控用户可以查看，其余用户需要赋权后才可以使用。

  显示信息：file_path是损坏文件的相对路径，如果是段页式表，则显示的是逻辑信息，不是实际的物理文件信息。block_num是该文件损坏的具体页面号，页面号从0开始。check_time表示发现页面损坏的时间。repair_time表示修复页面的时间。

  返回值类型：record

  示例：

  ```
  MogDB=# select * from local_bad_block_info();
  node_name    | spc_node | db_node | rel_node| bucket_node | fork_num | block_num |    file_path     |  check_time            |   repair_time
  -----------------+-------+--------+--------+--------------+----------+-----------+-----------------+--------------------------+-------------------------------
  dn_6001_6002_6003|  1663 |  16552 |  24745 |        -1    |    0    | 0        | base/16552/24745 | 2022-01-13 20:19:08.385004+08 | 2022-01-13 20:19:08.407314+08
      
  ```

- local_clear_bad_block_info()

  描述：清理local_bad_block_info中已修复页面的数据，也就是repair_time不为空的信息。默认只有初始化用户、具有sysadmin属性的用户以及在运维模式下具有运维管理员属性的用户、以及监控用户可以查看，其余用户需要赋权后才可以使用。

  返回值类型：bool

  示例：

  ```
  MogDB=# select * from local_clear_bad_block_info();
  result
  --------
  t
  ```

- gs_verify_and_tryrepair_page (path text, blocknum oid, verify_mem bool, is_segment bool)

  描述：校验本实例指定页面的情况。默认只有在主DN节点上，使用初始化用户、具有sysadmin属性的用户以及在运维模式下具有运维管理员属性的用户可以查看，其余用户需要赋权后才可以使用。

  返回的结果信息：disk_page_res表示磁盘上页面的校验结果；mem_page_res表示内存中页面的校验结果；is_repair表示在校验的过程中是否触发修复功能，t表示已修复，f表示未修复。

  注意：当DN实例上存在页面损坏时，进行升主会校验出错，报PANIC退出无法升主，为正常现象。可在其他DN升主后，通过备DN自动修复进行修复。

  参数说明：

  - path

    损坏文件的路径。依据local_bad_block_info中file_path一列填写。

    取值范围：字符串。

  - blocknum

    损坏文件的页号。依据local_bad_block_info中block_num一列填写。

    取值范围：Oid，0 - 4294967295。注意：输入负值等都会被强制转成非负整数类型。

  - verify_mem

    指定是否校验内存中的指定页面。设定为false时，只校验磁盘上的页面。设置为true时，校验内存中的页面和磁盘上的页面。如果发现磁盘上页面损坏，会将内存中的页面做一个基本信息校验刷盘，修复磁盘上页面。如果校验内存页面时发现页面不在内存中，会经内存接口读取磁盘上的页面。此过程中如果磁盘页面有问题，则会触发远程读自动修复功能。

    取值范围：bool，true和false。

  - is_segment

    是否是段页式表。根据local_bad_block_info中的bucket_node列值决定。如果bucket_node为-1时，表示不是段页式表，将is_segment设置为false；非-1的情况将is_segment设置为true。

    取值范围：bool，true和false。

  返回值类型：record

  示例：

  ```
  MogDB=# select * from gs_verify_and_tryrepair_page('base/16552/24745',0,false,false);
  node_name         |       path      |  blocknum  |        disk_page_res        | mem_page_res | is_repair
  ------------------+------------------+------------+-----------------------------+---------------+----------
  dn_6001_6002_6003 | base/16552/24745 |     0      | page verification succeeded.|              | f
  ```

- gs_repair_page(path text, blocknum oid, is_segment bool, timeout int)

  描述：修复本实例指定页面，仅支持有正常主备连接的主DN使用。页面修复成功返回true，修复过程中出错会有报错信息提示。默认只有在主DN节点上，使用初始化用户、具有sysadmin属性的用户以及在运维模式下具有运维管理员属性的用户可以查看，其余用户需要赋权后才可以使用。

  注意：当DN实例上存在页面损坏时，进行升主会校验出错，报PANIC退出无法升主，为正常现象。可在其他DN升主后，通过备DN自动修复进行修复。

  参数说明：

  - path

    损坏页面的路径。根据local_bad_block_info中file_path一列设置，或者是gs_verify_and_tryrepair_page函数中path一列设置。

    取值范围：字符串。

  - blocknum

    损坏页面的页面号。根据local_bad_block_info中block_num一列设置，或者是gs_verify_and_tryrepair_page函数中blocknum一列设置。

    取值范围：Oid，0 - 4294967295。注意：输入负值等都会被强制转成非负整数类型。

  - is_segment

    是否是段页式表。根据local_bad_block_info中的bucket_node列值决定，如果bucket_node为-1时，表示不是段页式表，将is_segment设置为false；非-1的情况将is_segment设置为true。

    取值范围：bool，true或者false。

  - timeout

    等待备DN回放的时长。修复页面需要等待备DN回放到当前主DN对应的位置，根据备DN回放所需时长设定。

    取值范围：60s - 3600s。

  返回值类型：bool

  示例：

  ```
  MogDB=# select * from gs_repair_page('base/16552/24745',0,false,60);
  result
  --------
  t
  ```