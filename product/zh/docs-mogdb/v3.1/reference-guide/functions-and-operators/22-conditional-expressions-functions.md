---
title: 条件表达式函数
summary: 条件表达式函数
author: Zhang Cuiping
date: 2021-04-20
---

# 条件表达式函数

- coalesce(expr1, expr2, …, exprn)

  描述：

  返回参数列表中第一个非NULL的参数值。

  COALESCE(expr1, expr2) 等价于CASE WHEN expr1 IS NOT NULL THEN expr1 ELSE expr2 END。

  示例：

  ```sql
  MogDB=# SELECT coalesce(NULL,'hello');
   coalesce
  ----------
   hello
  (1 row)
  ```

  备注：

  - 如果表达式列表中的所有表达式都等于NULL，则本函数返回NULL。
  - 它常用于在显示数据时用缺省值替换NULL。
  - 和CASE表达式一样，COALESCE不会计算不需要用来判断结果的参数；即在第一个非空参数右边的参数不会被计算。

- decode(base_expr, compare1, value1, Compare2,value2, … default)

  描述：把base_expr与后面的每个compare(n) 进行比较，如果匹配返回相应的value(n)。如果没有发生匹配，则返回default。

  示例：

  ```sql
  MogDB=# SELECT decode('A','A',1,'B',2,0);
   case
  ------
   1
  (1 row)
  ```

- nullif(expr1, expr2)

  描述：当且仅当expr1和expr2相等时，NULLIF才返回NULL，否则它返回expr1。

  nullif(expr1, expr2) 逻辑上等价于CASE WHEN expr1 = expr2 THEN NULL ELSE expr1 END。

  示例：

  ```sql
  MogDB=# SELECT nullif('hello','world');
   nullif
  --------
   hello
  (1 row)
  ```

  备注：

  如果两个参数的数据类型不同，则：

  - 若两种数据类型之间存在隐式转换，则以其中优先级较高的数据类型为基准将另一个参数隐式转换成该类型，转换成功则进行计算，转换失败则返回错误。如：

    ```sql
    MogDB=# SELECT nullif('1234'::VARCHAR,123::INT4);
    nullif
    --------
    1234
    (1 row)
    ```

    ```sql
    MogDB=# SELECT nullif('1234'::VARCHAR,'2012-12-24'::DATE);
    ERROR:  invalid input syntax for type timestamp: "1234"
    ```

  - 若两种数据类型之间不存在隐式转换，则返回错误 。如：

    ```sql
    MogDB=# SELECT nullif(TRUE::BOOLEAN,'2012-12-24'::DATE);
    ERROR:  operator does not exist: boolean = timestamp without time zone
    LINE 1: SELECT nullif(TRUE::BOOLEAN,'2012-12-24'::DATE) FROM sys_dummy;
    ^
    HINT:  No operator matches the given name and argument type(s). You might need to add explicit type casts.
    ```

- nvl( expr1 , expr2 )

  描述：

  - 如果expr1为NULL则返回expr2。
  - 如果expr1非NULL，则返回expr1。

  示例：

  ```sql
  MogDB=# SELECT nvl('hello','world');
    nvl
  -------
   hello
  (1 row)
  ```

  备注：参数expr1和expr2可以为任意类型，当NVL的两个参数不属于同类型时，看第二个参数是否可以向第一个参数进行隐式转换。如果可以则返回第一个参数类型，否则返回错误。

- greatest(expr1 [, …])

  描述：获取并返回参数列表中值最大的表达式的值。

  返回值类型：

  示例：

  ```sql
  MogDB=# SELECT greatest(1*2,2-3,4-1);
   greatest
  ----------
          3
  (1 row)
  ```

  ```sql
  MogDB=# SELECT greatest('HARRY', 'HARRIOT', 'HAROLD');
   greatest
  ----------
   HARRY
  (1 row)
  ```

- least(expr1 [, …])

  描述：获取并返回参数列表中值最小的表达式的值。

  示例：

  ```sql
  MogDB=# SELECT least(1*2,2-3,4-1);
   least
  -------
      -1
  (1 row)
  ```

  ```sql
  MogDB=# SELECT least('HARRY','HARRIOT','HAROLD');
   least
  --------
   HAROLD
  (1 row)
  ```

- EMPTY_BLOB()

  描述：使用EMPTY_BLOB在INSERT或UPDATE语句中初始化一个BLOB变量，取值为NULL。

  返回值类型：BLOB

  示例：

  ```sql
  --新建表
  MogDB=# CREATE TABLE blob_tb(b blob,id int);
  --插入数据
  MogDB=# INSERT INTO blob_tb VALUES (empty_blob(),1);
  --删除表
  MogDB=# DROP TABLE blob_tb;
  ```

  备注：使用DBE_LOB.GET_LENGTH求得的长度为0。
