---
title: PG_REPLICATION_SLOTS
summary: PG_REPLICATION_SLOTS
author: Guo Huan
date: 2021-04-19
---

# PG_REPLICATION_SLOTS

PG_REPLICATION_SLOTS视图查看复制节点的信息。

**表 1** PG_REPLICATION_SLOTS字段

| 名称            | 类型    | 描述                                         |
| :-------------- | :------ | :------------------------------------------- |
| slot_name       | text    | 复制节点的名称。                             |
| plugin          | text    | 逻辑复制槽对应的输出插件名。                 |
| slot_type       | text    | 复制节点的类型。                             |
| datoid          | oid     | 复制节点的数据库OID。                        |
| database        | name    | 复制节点的数据库名称。                       |
| active          | Boolean | 复制节点是否为激活状态。                     |
| xmin            | xid     | 复制节点事务标识。                           |
| catalog_xmin    | xid     | 逻辑复制槽对应的最早解码事务标识。           |
| restart_lsn     | text    | 复制节点的Xlog文件信息。                     |
| dummy_standby   | Boolean | 复制节点是否为假备。                         |
| confirmed_flush | text    | 逻辑复制槽专用，客户端确认接收到的日志位置。 |
