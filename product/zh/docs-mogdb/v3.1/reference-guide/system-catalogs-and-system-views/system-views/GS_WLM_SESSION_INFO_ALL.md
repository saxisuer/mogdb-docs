---
title: GS_WLM_SESSION_INFO_ALL
summary: GS_WLM_SESSION_INFO_ALL
author: Guo Huan
date: 2022-10-31
---

# GS_WLM_SESSION_INFO_ALL

GS_WLM_SESSION_INFO_ALL视图显示在数据库实例上执行作业结束后的负载管理记录。查询该视图需要sysadmin或者monitor admin权限。

**表 1** GS_WLM_SESSION_INFO_ALL的字段

| 名称                | 类型                     | 描述                                                         |
| :------------------ | :----------------------- | :----------------------------------------------------------- |
| datid               | oid                      | 连接后端的数据库OID。                                        |
| dbname              | text                     | 连接后端的数据库名称。                                       |
| schemaname          | text                     | 模式的名称。                                                 |
| nodename            | text                     | 语句执行的CN名称。                                           |
| username            | text                     | 连接到后端的用户名。                                         |
| application_name    | text                     | 连接到后端的应用名。                                         |
| client_addr         | inet                     | 连接到后端的客户端的IP地址。如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum。 |
| client_hostname     | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。 |
| client_port         | integer                  | 客户端用于与后端通讯的TCP端口号，如果使用Unix套接字，则为-1。 |
| query_band          | text                     | 用于标示作业类型，可通过GUC参数query_band进行设置，默认为空字符串。 |
| block_time          | bigint                   | 语句执行前的阻塞时间，包含语句解析和优化时间，单位ms。       |
| start_time          | timestamp with time zone | 语句执行的开始时间。                                         |
| finish_time         | timestamp with time zone | 语句执行的结束时间。                                         |
| duration            | bigint                   | 语句实际执行的时间，单位ms。                                 |
| estimate_total_time | bigint                   | 语句预估执行时间，单位ms。                                   |
| status              | text                     | 语句执行结束状态：正常为finished，异常为aborted。            |
| abort_info          | text                     | 语句执行结束状态为aborted时显示异常信息。                    |
| resource_pool       | text                     | 用户使用的资源池。                                           |
| control_group       | text                     | 语句所使用的Cgroup。                                         |
| estimate_memory     | integer                  | 语句估算内存大小。                                           |
| min_peak_memory     | integer                  | 语句在所有DN上的最小内存峰值，单位MB。                       |
| max_peak_memory     | integer                  | 语句在所有DN上的最大内存峰值，单位MB。                       |
| average_peak_memory | integer                  | 语句执行过程中的内存使用平均值，单位MB。                     |
| memory_skew_percent | integer                  | 语句各DN间的内存使用倾斜率。                                 |
| spill_info          | text                     | 语句在所有DN上的下盘信息：<br/>- None：所有DN均未下盘。<br/>- All：所有DN均下盘。<br/>- [a:b]：数量为b个DN中有a个DN下盘。 |
| min_spill_size      | integer                  | 若发生下盘，所有DN上下盘的最小数据量，单位MB，默认为0。      |
| max_spill_size      | integer                  | 若发生下盘，所有DN上下盘的最大数据量，单位MB，默认为0。      |
| average_spill_size  | integer                  | 若发生下盘，所有DN上下盘的平均数据量，单位MB，默认为0。      |
| spill_skew_percent  | integer                  | 若发生下盘，DN间下盘倾斜率。                                 |
| min_dn_time         | bigint                   | 语句在所有DN上的最小执行时间，单位ms。                       |
| max_dn_time         | bigint                   | 语句在所有DN上的最大执行时间，单位ms。                       |
| average_dn_time     | bigint                   | 语句在所有DN上的平均执行时间，单位ms。                       |
| dntime_skew_percent | integer                  | 语句在各DN间的执行时间倾斜率。                               |
| min_cpu_time        | bigint                   | 语句在所有DN上的最小CPU时间，单位ms。                        |
| max_cpu_time        | bigint                   | 语句在所有DN上的最大CPU时间，单位ms。                        |
| total_cpu_time      | bigint                   | 语句在所有DN上的CPU总时间，单位ms。                          |
| cpu_skew_percent    | integer                  | 语句在DN间的CPU时间倾斜率。                                  |
| min_peak_iops       | integer                  | 语句在所有DN上的每秒最小IO峰值（列存单位是次/s，行存单位是万次/s）。 |
| max_peak_iops       | integer                  | 语句在所有DN上的每秒最大IO峰值（列存单位是次/s，行存单位是万次/s）。 |
| average_peak_iops   | integer                  | 语句在所有DN上的每秒平均IO峰值（列存单位是次/s，行存单位是万次/s）。 |
| iops_skew_percent   | integer                  | 语句在DN间的IO倾斜率。                                       |
| warning             | text                     | 主要显示如下几类告警信息以及SQL自诊断调优相关告警：<br/>- Spill file size large than 256MB<br/>- Broadcast size large than 100MB<br/>- Early spill<br/>- Spill times is greater than 3<br/>- Spill on memory adaptive<br/>- Hash table conflict |
| queryid             | bigint                   | 语句执行使用的内部query id。                                 |
| query               | text                     | 执行的语句。                                                 |
| query_plan          | text                     | 语句的执行计划。                                             |
| node_group          | text                     | 语句所属用户对应的逻辑集群。                                 |
| cpu_top1_node_name  | text                     | cpu使用率第1的节点名称。                                     |
| cpu_top2_node_name  | text                     | cpu使用率第2的节点名称。                                     |
| cpu_top3_node_name  | text                     | cpu使用率第3的节点名称。                                     |
| cpu_top4_node_name  | text                     | cpu使用率第4的节点名称。                                     |
| cpu_top5_node_name  | text                     | cpu使用率第5的节点名称。                                     |
| mem_top1_node_name  | text                     | 内存使用量第1的节点名称。                                    |
| mem_top2_node_name  | text                     | 内存使用量第2的节点名称。                                    |
| mem_top3_node_name  | text                     | 内存使用量第3的节点名称。                                    |
| mem_top4_node_name  | text                     | 内存使用量第4的节点名称。                                    |
| mem_top5_node_name  | text                     | 内存使用量第5的节点名称。                                    |
| cpu_top1_value      | bigint                   | cpu使用率。                                                  |
| cpu_top2_value      | bigint                   | cpu使用率。                                                  |
| cpu_top3_value      | bigint                   | cpu使用率。                                                  |
| cpu_top4_value      | bigint                   | cpu使用率。                                                  |
| cpu_top5_value      | bigint                   | cpu使用率。                                                  |
| mem_top1_value      | bigint                   | 内存使用量。                                                 |
| mem_top2_value      | bigint                   | 内存使用量。                                                 |
| mem_top3_value      | bigint                   | 内存使用量。                                                 |
| mem_top4_value      | bigint                   | 内存使用量。                                                 |
| mem_top5_value      | bigint                   | 内存使用量。                                                 |
| top_mem_dn          | text                     | 内存使用量topN信息。                                         |
| top_cpu_dn          | text                     | cpu使用量topN信息。                                          |
| n_returned_rows     | bigint                   | SELECT返回的结果集行数。                                     |
| n_tuples_fetched    | bigint                   | 随机扫描行。                                                 |
| n_tuples_returned   | bigint                   | 顺序扫描行。                                                 |
| n_tuples_inserted   | bigint                   | 插入行。                                                     |
| n_tuples_updated    | bigint                   | 更新行。                                                     |
| n_tuples_deleted    | bigint                   | 删除行。                                                     |
| n_blocks_fetched    | bigint                   | buffer的块访问次数。                                         |
| n_blocks_hit        | bigint                   | buffer的块命中次数。                                         |
| db_time             | bigint                   | 有效的DB时间花费，多线程将累加（单位：微秒）。               |
| cpu_time            | bigint                   | CPU时间（单位：微秒）。                                      |
| execution_time      | bigint                   | 执行器内执行时间（单位：微秒）。                             |
| parse_time          | bigint                   | SQL解析时间（单位：微秒）。                                  |
| plan_time           | bigint                   | SQL生成计划时间（单位：微秒）。                              |
| rewrite_time        | bigint                   | SQL重写时间（单位：微秒）。                                  |
| pl_execution_time   | bigint                   | plpgsql上的执行时间（单位：微秒）。                          |
| pl_compilation_time | bigint                   | plpgsql上的编译时间（单位：微秒）。                          |
| net_send_time       | bigint                   | 网络上的时间花费（单位：微秒）。                             |
| data_io_time        | bigint                   | IO上的时间花费（单位：微秒）。                               |
| is_slow_query       | bigint                   | 是否是慢SQL记录。                                            |