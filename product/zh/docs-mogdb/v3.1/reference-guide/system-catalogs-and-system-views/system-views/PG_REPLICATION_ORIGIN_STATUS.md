---
title: PG_REPLICATION_ORIGIN_STATUS
summary: PG_REPLICATION_ORIGIN_STATUS
author: Guo Huan
date: 2022-05-12
---

# PG_REPLICATION_ORIGIN_STATUS

获取复制源的复制状态。

**表 1** PG_REPLICATION_ORIGIN_STATUS字段

| 名称        | 类型 | 描述              |
| :---------- | :--- | :---------------- |
| local_id    | oid  | 复制源ID。        |
| external_id | text | 复制源名称。      |
| remote_lsn  | LSN  | 复制源的lsn位置。 |
| local_lsn   | LSN  | 本地的lsn位置。   |