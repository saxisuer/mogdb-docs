---
title: PGXC_PREPARED_XACTS
summary: PGXC_PREPARED_XACTS
author: Guo Huan
date: 2022-05-13
---

# PGXC_PREPARED_XACTS

PGXC_PREPARED_XACTS视图显示当前处于prepared阶段的两阶段事务。只有system admin和monitor admin用户有权限查看。

**表 1** PGXC_PREPARED_XACTS字段

| 名称               | 类型 | 描述                                   |
| :----------------- | :--- | :------------------------------------- |
| pgxc_prepared_xact | text | 查看当前处于prepared阶段的两阶段事务。 |