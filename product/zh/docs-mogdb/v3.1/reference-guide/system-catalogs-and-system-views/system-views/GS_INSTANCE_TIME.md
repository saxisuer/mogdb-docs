---
title: GS_INSTANCE_TIME
summary: GS_INSTANCE_TIME
author: Guo Huan
date: 2021-04-19
---

# GS_INSTANCE_TIME

提供当前集节点下的各种时间消耗信息，主要分为以下类型：

- DB_TIME：作业在多核下的有效时间花销。
- CPU_TIME：CPU的时间花销。
- EXECUTION_TIME：执行器内的时间花销。
- PARSE_TIME：SQL解析的时间花销。
- PLAN_TIME：生成Plan的时间花销。
- REWRITE_TIME：SQL重写的时间花销。
- PL_EXECUTION_TIME：plpgsql（存储过程）执行的时间花销。
- PL_COMPILATION_TIME：plpgsql（存储过程）编译的时间花销。
- NET_SEND_TIME：网络上的时间花销。
- DATA_IO_TIME：IO的时间花销。

**表 1** GS_INSTANCE_TIME字段

| **名称**  | **类型** | **描述**               |
| :-------- | :------- | :--------------------- |
| stat_id   | integer  | 统计编号。             |
| stat_name | text     | 类型名称。             |
| value     | bigint   | 时间值（单位: 微秒）。 |
