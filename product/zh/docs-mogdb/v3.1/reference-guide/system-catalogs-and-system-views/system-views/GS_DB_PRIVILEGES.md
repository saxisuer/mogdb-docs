---
title: GS_DB_PRIVILEGES
summary: GS_DB_PRIVILEGES
author: Guo Huan
date: 2022-05-12
---

# GS_DB_PRIVILEGES

GS_DB_PRIVILEGES系统视图记录ANY权限的授予情况，每条记录对应一条授权信息。

**表 1** GS_DB_PRIVILEGES字段

| 名称           | 类型    | 描述                                                         |
| :------------- | :------ | :----------------------------------------------------------- |
| rolename       | name    | 用户名。                                                     |
| privilege_type | text    | 用户拥有的ANY权限，取值参考[GRANT](../../../reference-guide/sql-syntax/GRANT.md)中的表1。       |
| admin_option   | boolean | 是否具有privilege_type列记录的ANY权限的再授权权限。<br/>- yes：表示具有。<br/>- no：表示不具有。 |