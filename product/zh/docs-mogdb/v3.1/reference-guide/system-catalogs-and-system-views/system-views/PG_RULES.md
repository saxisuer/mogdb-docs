---
title: PG_RULES
summary: PG_RULES
author: Guo Huan
date: 2021-04-19
---

# PG_RULES

PG_RULES视图提供对查询重写规则的有用信息访问的接口。

**表 1** PG_RULES字段

| 名称       | 类型 | 描述                                 |
| :--------- | :--- | :----------------------------------- |
| schemaname | name | 包含表的模式的名称。                 |
| tablename  | name | 规则作用的表的名称。                 |
| rulename   | name | 规则的名称。                         |
| definition | text | 规则定义（一个重新构造的创建命令）。 |
