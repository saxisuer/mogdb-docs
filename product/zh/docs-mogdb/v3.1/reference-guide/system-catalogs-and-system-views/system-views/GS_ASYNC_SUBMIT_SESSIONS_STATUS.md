---
title: GS_ASYNC_SUBMIT_SESSIONS_STATUS
summary: GS_ASYNC_SUBMIT_SESSIONS_STATUS
author: Guo Huan
date: 2022-06-13
---

# GS_ASYNC_SUBMIT_SESSIONS_STATUS

GS_ASYNC_SUBMIT_SESSIONS_STATUS视图用于查看使用异步提交的session状态。

**表 1** GS_ASYNC_SUBMIT_SESSIONS_STATUS字段

| 名称                                  | 类型    | 描述                                                         |
| ------------------------------------- | ------- | ------------------------------------------------------------ |
| nodename                              | text    | 节点名                                                       |
| groupid                               | int     | 线程组号                                                     |
| waiting_xlog_flush_session_num        | int     | 等待日志刷盘的session数                                      |
| waiting_sync_rep_receive_session_num | int     | 等待备份同步日志receive的session数                           |
| waiting_sync_rep_write_session_num   | int     | 等待备份同步日志write的session数                             |
| waiting_sync_rep_flush_session_num   | int     | 等待备份同步日志flush的session数                             |
| waiting_sync_rep_apply_session_num   | int     | 等待备份同步日志apply的session数                             |
| waiting_sync_paxos_session_num        | integer | 等待dcf同步日志的session数                                   |
| waiting_commit_session_num            | int     | 已经完成日志刷盘（和备份同步）但还没有完成异步提交的session数 |
| finished_commit_session_num           | int     | 已经完成异步提交的session数                                  |
