---
title: PG_VIEWS
summary: PG_VIEWS
author: Guo Huan
date: 2021-04-19
---

# PG_VIEWS

PG_VIEWS视图提供访问数据库中每个视图的有用信息。

**表 1** PG_VIEWS字段

| 名称       | 类型 | 引用                 | 描述               |
| :--------- | :--- | :------------------- | :----------------- |
| schemaname | name | PG_NAMESPACE.nspname | 包含视图的模式名。 |
| viewname   | name | PG_CLASS.relname     | 视图名。           |
| viewowner  | name | PG_AUTHID.Erolname   | 视图的所有者。     |
| definition | text | -                    | 视图的定义。       |
