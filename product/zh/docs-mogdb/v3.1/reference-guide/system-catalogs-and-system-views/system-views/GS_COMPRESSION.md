---
title: GS_COMPRESSION
summary: GS_COMPRESSION
author: Guo Huan
date: 2022-11-10
---

# GS_COMPRESSION

GS_COMPRESSION视图用于观测系统所有压缩表的压缩情况。

**表 1** GS_COMPRESSION字段

| 名称           |  类型  | 描述                       |
| -------------- | ----   | -------------------------- |
| comrelid       | oid    | 对应表的oid。               |
| comsavespace   | bigint | 压缩后节省的空间（字节）。   |
| comfullnum     | bigint | 全量压缩的page数。          |
| comfullsize    | bigint | 全量压缩节省总空间（字节）。  |
| comincnum      | bigint | 增量压缩的page数。          |
| comincsize     | bigint | 增量压缩节省总空间（字节）。 |
| comuncmprpages | int    | 待压缩的page数。           |
| comcmprpages   | int    | 已压缩的page数。           |
| comnum         | int    | 该表已压缩次数。            |
