---
title: DV_SESSIONS
summary: DV_SESSIONS
author: Guo Huan
date: 2022-05-12
---

# DV_SESSIONS

DV_SESSIONS视图存储当前会话的所有会话信息。默认只有系统管理员权限才可以访问此系统表，普通用户需要授权才可以访问。

**表 1** DV_SESSIONS字段

| 名称     | 类型    | 描述                                                         |
| :------- | :------ | :----------------------------------------------------------- |
| sid      | bigint  | 当前活动的后台线程的OID。                                    |
| serial#  | integer | 当前活动的后台线程的序号，在MogDB中为0。                     |
| user#    | oid     | 登录此后台线程的用户的OID。oid 为0表示此后台线程为全局辅助线程(auxiliary)。 |
| username | name    | 登录此后台线程的用户名。username为空表示此后台线程为全局辅助线程(auxiliary)。<br/>可以通过和pg_stat_get_activity() 关联查询，识别出application_name。<br/>例如：<br/>`select s.*,a.application_name from DV_SESSIONS as s left join pg_stat_get_activity(NULL) as a on s.sid=a.sessionid;` |