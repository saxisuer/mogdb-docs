---
title: PG_STAT_USER_TABLES
summary: PG_STAT_USER_TABLES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_USER_TABLES

PG_STAT_USER_TABLES视图显示所有命名空间中用户自定义普通表和toast表的状态信息。

**表 1** PG_STAT_USER_TABLES字段

| 名称              | 类型                     | 描述                                            |
| :---------------- | :----------------------- | :---------------------------------------------- |
| relid             | oid                      | 表的OID。                                       |
| schemaname        | name                     | 该表的模式名。                                  |
| relname           | name                     | 表名。                                          |
| seq_scan          | bigint                   | 该表发起的顺序扫描数。                          |
| seq_tup_read      | bigint                   | 顺序扫描抓取的活跃行数。                        |
| idx_scan          | bigint                   | 该表发起的索引扫描数。                          |
| idx_tup_fetch     | bigint                   | 索引扫描抓取的活跃行数。                        |
| n_tup_ins         | bigint                   | 插入行数。                                      |
| n_tup_upd         | bigint                   | 更新行数。                                      |
| n_tup_del         | bigint                   | 删除行数。                                      |
| n_tup_hot_upd     | bigint                   | HOT更新行数（即没有更新所需的单独索引）。       |
| n_live_tup        | bigint                   | 估计活跃行数。                                  |
| n_dead_tup        | bigint                   | 估计死行数。                                    |
| last_vacuum       | timestamp with time zone | 最后一次该表是手动清理的（不计算VACUUM FULL）。 |
| last_autovacuum   | timestamp with time zone | 上次被autovacuum守护进程清理的表。              |
| last_analyze      | timestamp with time zone | 上次手动分析这个表。                            |
| last_autoanalyze  | timestamp with time zone | 上次被autovacuum守护进程分析的表。              |
| vacuum_count      | bigint                   | 这个表被手动清理的次数（不计算VACUUM FULL）。   |
| autovacuum_count  | bigint                   | 这个表被autovacuum清理的次数。                  |
| analyze_count     | bigint                   | 这个表被手动分析的次数。                        |
| autoanalyze_count | bigint                   | 这个表被autovacuum守护进程分析的次数。          |
| last_data_changed | timestamp with time zone | 这个表数据最近修改时间。                        |
