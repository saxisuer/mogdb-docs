---
title: PG_SUBSCRIPTION
summary: PG_SUBSCRIPTION
author: Guo Huan
date: 2022-05-12
---

# PG_SUBSCRIPTION

系统表PG_SUBSCRIPTION包含所有现有的逻辑复制订阅。需要有系统管理员权限才可以访问此系统表。

和大部分系统表不同，pg_subscription在数据库实例的所有数据库之间共享，即在每个节点上有只有一份pg_replication_origin，而不是每个数据库一份。

**表 1** PG_SUBSCRIPTION字段

| 名称            | 类型   | 描述                                                         |
| :-------------- | :----- | :----------------------------------------------------------- |
| oid             | oid    | 行标识符（隐含属性，必须明确选择）。                         |
| subdbid         | oid    | 订阅所在的数据库的OID。                                      |
| subname         | text   | 订阅的名称。                                                 |
| subowner        | oid    | 订阅的拥有者。                                               |
| subenabled      | bool   | 如果为真，订阅被启用并且应该被复制。                         |
| subconninfo     | text   | 到发布端数据库的连接信息。                                   |
| subslotname     | text   | 发布端数据库中复制槽的名称。空表示为NONE。                   |
| subsynccommit   | text   | 订阅worker的synchronous_commit设置的值。                     |
| subpublications | text[] | 被订阅的publication名称的数组。这些引用的是发布者服务器上的publication。 |