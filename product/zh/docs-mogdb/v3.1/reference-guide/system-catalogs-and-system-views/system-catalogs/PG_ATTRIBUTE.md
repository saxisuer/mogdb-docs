---
title: PG_ATTRIBUTE
summary: PG_ATTRIBUTE
author: Guo Huan
date: 2021-04-19
---

# PG_ATTRIBUTE

PG_ATTRIBUTE系统表存储关于表字段的信息。

**表 1** PG_ATTRIBUTE字段

| 名称          | 类型      | 描述                                                         |
| :------------ | :-------- | :----------------------------------------------------------- |
| attrelid      | oid       | 此字段所属表。                                               |
| attname       | name      | 字段名。                                                     |
| atttypid      | oid       | 字段类型。                                                   |
| attstattarget | integer   | 控制ANALYZE为这个字段积累的统计细节的级别。<br/>- 零值表示不收集统计信息。<br/>- 负数表示使用系统缺省的统计对象。<br/>- 正数值的确切信息是和数据类型相关的。<br/>对于标量数据类型，ATTSTATTARGET既是要收集的"最常用数值"的目标数目，也是要创建的柱状图的目标数量。 |
| attlen        | smallint  | 是本字段类型的pg_type.typlen的拷贝。                         |
| attnum        | smallint  | 字段编号。                                                   |
| attndims      | integer   | 如果该字段是数组，则是维数，否则是0 。                       |
| attcacheoff   | integer   | 在磁盘上的时候总是-1 ，但是如果加载入内存中的行描述器中，它可能会被更新以缓冲在行中字段的偏移量。 |
| atttypmod     | integer   | 记录创建新表时支持的类型特定的数据（比如一个varchar字段的最大长度）。它传递给类型相关的输入和长度转换函数当做第三个参数。其值对那些不需要ATTTYPMOD的类型通常为-1。 |
| attbyval      | Boolean   | 这个字段类型的pg_type.typbyval的拷贝。                       |
| attstorage    | "char"    | 这个字段类型的pg_type.typstorage的拷贝。                     |
| attalign      | "char"    | 这个字段类型的pg_type.typalign的拷贝。                       |
| attnotnull    | Boolean   | 这代表一个非空约束。可以改变这个字段以打开或者关闭这个约束。 |
| atthasdef     | Boolean   | 这个字段有一个缺省值，此时它对应pg_attrdef表里实际定义此值的记录。 |
| attisdropped  | Boolean   | 这个字段已经被删除了，不再有效。一个已经删除的字段物理上仍然存在表中，但会被分析器忽略，因此不能再通过SQL访问。 |
| attislocal    | Boolean   | 这个字段是局部定义在关系中的。请注意一个字段可以同时是局部定义和继承的。 |
| attcmprmode   | tinyint   | 对某一列指定压缩方式。压缩方式包括: <br/>- ATT_CMPR_NOCOMPRESS<br/>- ATT_CMPR_DELTA<br/>- ATT_CMPR_DICTIONARY<br/>- ATT_CMPR_PREFIX<br/>- ATT_CMPR_NUMSTR |
| attinhcount   | integer   | 这个字段所拥有的直接父表的个数。如果一个字段的父表个数非零，则它就不能被删除或重命名。 |
| attcollation  | oid       | 对此列定义的校对列。                                         |
| attacl        | aclitem[] | 列级访问权限控制。                                           |
| attoptions    | text[]    | 字段属性。目前支持以下两种属性：<br />n_distinct，表示该字段的distinct值数量（不包含字表）<br />n_distinct_inherited，表示该字段的distinct值数量（包含字表） |
| attfdwoptions | text[]    | 外表字段属性。当前支持的dist_fdw、file_fdw、log_fdw未使用外表字段属性。 |
| attinitdefval | bytea     | 存储了此列默认的值表达式。行存表的ADD COLUMN需要使用此字段。 |
| attkvtype     | tinyint   | 对某一列指定key value类型。类型包括: <br/>0. ATT_KV_UNDEFINED : 默认<br/>1. ATT_KV_TAG : 维度<br/>2. ATT_KV_FIELD : 指标<br/>3. ATT_KV_TIMETAG : 时间列 |
