---
title: PGXC_GROUP
summary: PGXC_GROUP
author: Guo Huan
date: 2022-05-13
---

# PGXC_GROUP

PGXC_GROUP系统表存储节点组信息。PGXC_GROUP系统表仅在分布式场景下有具体含义，MogDB只能查询表定义。

**表 1** PGXC_GROUP字段

| 名称              | 类型             | 描述                                                         |
| :---------------- | :--------------- | :----------------------------------------------------------- |
| oid               | oid              | 行标识符（隐含字段，必须明确选择）。                         |
| group_name        | name             | 节点组名称。                                                 |
| in_redistribution | “char”           | 是否需要重分布。取值包括n，y，t。<br/>- n：表示NodeGroup没有再进行重分布。<br/>- y：表示NodeGroup是重分布过程中的源节点组。<br/>- t：表示NodeGroup是重分布过程中的目的节点组。 |
| group_members     | oidvector_extend | 节点组的节点OID列表。                                        |
| group_buckets     | text             | 分布数据桶的集合。                                           |
| is_installation   | Boolean          | 是否安装子集群。<br/>- t（true）：表示安装。<br/>- f（false）：表示不安装。 |
| group_acl         | aclitem[]        | 访问权限。                                                   |
| group_kind        | “char”           | node group类型，取值包括i, n, v, e。<br/>- i：表示installation node group。<br/>- n：表示普通非逻辑集群node group。<br/>- v：表示逻辑集群node group。<br/>- e：表示弹性集群。 |
| group_parent      | oid              | 如果是子node group，该字段表示父node group的OID，如果是父node group，该字段值为空。 |
