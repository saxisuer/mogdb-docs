---
title: PG_PUBLICATION
summary: PG_PUBLICATION
author: Guo Huan
date: 2022-05-12
---

# PG_PUBLICATION

系统表pg_publication包含当前数据库中创建的所有publication。

**表 1** PG_PUBLICATION字段

| 名称         | 类型 | 描述                                                         |
| :----------- | :--- | :----------------------------------------------------------- |
| pubname      | text | publication的名称。                                          |
| pubowner     | oid  | publication的拥有者。                                        |
| puballtables | bool | 如果为真，这个publication自动包括数据库中的所有表，包括未来将会创建的任何表。 |
| pubinsert    | bool | 如果为真，为publication中的表复制INSERT操作。                |
| pubupdate    | bool | 如果为真，为publication中的表复制UPDATE操作。                |
| pubdelete    | bool | 如果为真，为publication中的表复制DELETE操作。                |