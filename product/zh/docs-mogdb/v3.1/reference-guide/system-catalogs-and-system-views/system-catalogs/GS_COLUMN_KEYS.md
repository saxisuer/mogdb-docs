---
title: GS_COLUMN_KEYS
summary: GS_COLUMN_KEYS
author: Guo Huan
date: 2021-04-19
---

# GS_COLUMN_KEYS

GS_COLUMN_KEYS系统表记录密态等值特性中列加密密钥相关信息，每条记录对应一个列加密密钥。

**表 1** GS_COLUMN_KEYS字段

| 名称                      | 类型      | 描述                                        |
| :------------------------ | :-------- | :------------------------------------------ |
| oid                       | oid       | 行标识符（隐含字段）。                      |
| column_key_name           | name      | 列加密密钥（cek）名称。                     |
| column_key_distributed_id | oid       | 根据加密密钥（cek）全称域名hash值得到的id。 |
| global_key_id             | oid       | 外键。客户端加密主密钥（cmk）的OID。        |
| key_namespace             | oid       | 包含此列加密密钥（cek）的命名空间OID。      |
| key_owner                 | oid       | 列加密密钥（cek）的所有者。                 |
| create_date               | timestamp | 创建列加密密钥的时间。                      |
| key_acl                   | aclitem[] | 创建该列加密密钥时所拥有的访问权限。        |
