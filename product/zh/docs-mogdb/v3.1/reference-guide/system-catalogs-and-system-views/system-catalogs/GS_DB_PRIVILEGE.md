---
title: GS_DB_PRIVILEGE
summary: GS_DB_PRIVILEGE
author: Guo Huan
date: 2022-05-12
---

# GS_DB_PRIVILEGE

GS_DB_PRIVILEGE系统表记录ANY权限的授予情况，每条记录对应一条授权信息。

**表 1** GS_DB_PRIVILEGE字段

| 名称           | 类型    | 描述                                                         |
| :------------- | :------ | :----------------------------------------------------------- |
| oid            | oid     | 行标识符（隐含字段，必须明确选择）。                         |
| roleid         | oid     | 用户标识。                                                   |
| privilege_type | text    | 用户拥有的ANY权限，取值参考[GRANT](../../../reference-guide/sql-syntax/GRANT.md)中的表1。       |
| admin_option   | boolean | 是否具有privilege_type列记录的ANY权限的再授权权限。<br/>- t：表示具有。<br/>- f：表示不具有。 |