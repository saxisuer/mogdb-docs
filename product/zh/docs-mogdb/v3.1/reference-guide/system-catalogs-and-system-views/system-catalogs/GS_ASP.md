---
title: GS_ASP
summary: GS_ASP
author: Guo Huan
date: 2022-05-12
---

# GS_ASP

GS_ASP显示被持久化的ACTIVE SESSION PROFILE样本，该表只在系统库下查询，在用户库下查询无数据。

**表 1** GS_ASP字段

| 名称              | 类型                     | 描述                                                         |
| :---------------- | :----------------------- | :----------------------------------------------------------- |
| sampleid          | bigint                   | 采样ID。                                                     |
| sample_time       | timestamp with time zone | 采样的时间。                                                 |
| need_flush_sample | boolean                  | 该样本是否需要刷新到磁盘。<br/>- t（true）：表示需要。<br/>- f（false）：表示不需要。 |
| databaseid        | oid                      | 数据库ID。                                                   |
| thread_id         | bigint                   | 线程的ID。                                                   |
| sessionid         | bigint                   | 会话的ID。                                                   |
| start_time        | timestamp with time zone | 会话的启动时间。                                             |
| event             | text                     | 具体的事件名称。                                             |
| lwtid             | integer                  | 当前线程的轻量级线程号。                                     |
| psessionid        | bigint                   | streaming线程的父线程。                                      |
| tlevel            | integer                  | streaming线程的层级。与执行计划的层级（id）相对应。          |
| smpid             | integer                  | smp执行模式下并行线程的并行编号。                            |
| userid            | oid                      | session用户的id。                                            |
| application_name  | text                     | 应用的名字。                                                 |
| client_addr       | inet                     | client端的地址。                                             |
| client_hostname   | text                     | client端的名字。                                             |
| client_port       | integer                  | 客户端用于与后端通讯的TCP端口号。                            |
| query_id          | bigint                   | debug query id。                                             |
| unique_query_id   | bigint                   | unique query id。                                            |
| user_id           | oid                      | unique query的key中的user_id。                               |
| cn_id             | integer                  | 表示下发该unique sql的节点id。unique query的key中的cn_id。   |
| unique_query      | text                     | 规范化后的Unique SQL文本串。                                 |
| locktag           | text                     | 会话等待锁信息，可通过locktag_decode解析。                   |
| lockmode          | text                     | 会话等待锁模式：<br/>- LW_EXCLUSIVE：排他锁<br/>- LW_SHARED：共享锁<br/>- LW_WAIT_UNTIL_FREE：等待LW_EXCLUSIVE可用 |
| block_sessionid   | bigint                   | 如果会话正在等待锁，阻塞该会话获取锁的会话标识。             |
| wait_status       | text                     | 描述event列的更多详细信息。                                  |
| global_sessionid  | text                     | 全局会话ID。                                                 |
| plan_node_id      | int                      | 执行计划树的算子id                                           |