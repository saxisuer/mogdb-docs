---
title: PG_LARGEOBJECT_METADATA
summary: PG_LARGEOBJECT_METADATA
author: Guo Huan
date: 2021-04-19
---

# PG_LARGEOBJECT_METADATA

PG_LARGEOBJECT_METADATA系统表存储与大数据相关的元数据。实际的大对象数据存储在PG_LARGEOBJECT里。

**表 1** PG_LARGEOBJECT_METADATA字段

| 名称     | 类型      | 引用          | 描述                                 |
| :------- | :-------- | :------------ | :----------------------------------- |
| oid      | oid       | -             | 行标识符（隐含属性，必须明确选择）。 |
| lomowner | oid       | PG_AUTHID.oid | 大对象的所有者。                     |
| lomacl   | aclitem[] | -             | 访问权限。                           |
