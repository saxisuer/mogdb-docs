---
title: cm_agent参数
summary: cm_agent参数
author: Guo Huan
date: 2022-05-23
---

# cm_server参数

## log_dir

**参数说明**: log_dir决定存放cm_server日志文件的目录。它可以是绝对路径，或者是相对路径（相对于$GAUSSLOG的路径）。

**取值范围**: 字符串。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: “log”，表示在$GAUSSLOG下对应的cm目录下生成cm_server日志。

## log_file_size

**参数说明**: 控制日志文件的大小。当日志文件达到指定大小时，则重新创建一个日志文件记录日志信息。

**取值范围**: 整型，取值范围0~2047，单位为MB。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 16MB

## log_min_messages

**参数说明**: 控制写到cm_server日志文件中的消息级别。每个级别都包含排在它后面的所有级别中的信息。级别越低，服务器运行日志中记录的消息就越少。

**取值范围**: 枚举类型，有效值有debug5、debug1、log、warning、error、fatal。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: warning

## thread_count

**参数说明**: cm_server线程池的线程数。

**取值范围**: 整型，2~1000。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1000

## alarm_component

**参数说明**: 设置用于处理告警内容的告警组件的位置。

**取值范围**: 字符串。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 若前置脚本gs_preinstall中的–alarm-type参数设置为5时，表示未对接第三方组件，告警写入system_alarm日志，此时GUC参数alarm_component的取值为：/opt/snas/bin/snas_cm_cmd。
- 若前置脚本gs_preinstall中的–alarm-type参数设置为1时，表示对接第三方组件，此时GUC参数alarm_component的值为第三方组件的可执行程序的绝对路径。

**默认值**: /opt/snas/bin/snas_cm_cmd

## instance_failover_delay_timeout

**参数说明**: cm_server检测到主机宕机，failover备机的延迟时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 0

## instance_heartbeat_timeout

**参数说明**: 实例心跳超时时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 6

## cmserver_ha_connect_timeout

**参数说明**: cm_server主备连接超时时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 2

## cmserver_ha_heartbeat_timeout

**参数说明**: cm_server主备心跳超时时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 6

## phony_dead_effective_time

**参数说明**: 用于DN进程的僵死检测，当检测到的僵死次数大于该参数值，认为进程僵死，将进程重启。

**取值范围**: 整型，单位为次数。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 5

## enable_transaction_read_only

**参数说明**: 控制数据库是否为只读模式开关。

**取值范围**: 布尔型，有效值有on，off，true，false，yes，no，1，0。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: on

## datastorage_threshold_check_interval

**参数说明**: 检测磁盘占用的时间间隔。间隔用户指定时间，检测一次磁盘占用。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10

## datastorage_threshold_value_check

**参数说明**: 设置数据库只读模式的磁盘占用阈值，当数据目录所在磁盘占用超过这个阈值，自动将数据库设置为只读模式。

**取值范围**: 整型，1 ~ 99，表示百分比。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 85

## max_datastorage_threshold_check

**参数说明**: 设置磁盘使用率的最大检测间隔时间。当用户手动修改只读模式参数后，会自动在指定间隔时间后开启磁盘满检测操作。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 43200

## cmserver_ha_status_interval

**参数说明**: cm_server主备同步状态信息间隔时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## cmserver_self_vote_timeout

**参数说明**: cm_server自仲裁超时时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)行设置。

**默认值**: 6

## alarm_report_interval

**参数说明**: 指定告警上报的时间间隔。

**取值范围**: 非负整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 3

## alarm_report_max_count

**参数说明**: 指定告警上报的最大次数。

**取值范围**: 非负整型。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## enable_az_auto_switchover

**参数说明**: AZ自动切换开关，若打开，则表示允许cm_server自动切换AZ。否则当发生dn故障等情况时，即使当前AZ已经不再可用，也不会自动切换到其它AZ上，除非手动执行切换命令。

**取值范围**: 非负整型，0或1，0表示开关关闭，1表示开关打开。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## instance_keep_heartbeat_timeout

**参数说明**: cm_agent会定期检测实例状态并上报给cm_server，若实例状态长时间无法成功检测，累积次数超出该数值，则cm_server将下发命令给agent重启该实例。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 40

## az_switchover_threshold

**参数说明**: 若一个AZ内DN分片的故障率（故障的dn分片数 / 总dn分片数 * 100%）超过该数值，则会触发AZ自动切换。

**取值范围**: 整型，0~100。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 100

## az_check_and_arbitrate_interval

**参数说明**: 当某个AZ状态不正常时，会触发AZ自动切换，该参数是检测AZ状态的时间间隔。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 2

## az_connect_check_interval

**参数说明**: 定时检测AZ间的网络连接，该参数表示连续两次检测之间的间隔时间。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 60

## az_connect_check_delay_time

**参数说明**: 每次检测AZ间的网络连接时有多次重试，该参数表示两次重试之间的延迟时间**。**

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 150

## cmserver_demote_delay_on_etcd_fault

**参数说明**: 因为etcd不健康而导致cm_server从主降为备的时间间隔**。**

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 8

## instance_phony_dead_restart_interval

**参数说明**: 当dn实例僵死时，会被cm_agent重启，相同的实例连续因僵死被杀时，其间隔时间不能小于该参数数值，否则cm_agent不会下发命令**。**

**取值范围**: 整型，单位为秒。最小生效值为1800，如果设置小于此值实际生效值为1800。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 21600

## cm_auth_method

**参数说明**: CM模块端口认证方式，trust表示未配置端口认证，gss表示采用kerberos端口认证。必须注意的是：只有当kerberos服务端和客户端成功安装后才能修改为gss，否则CM模块无法正常通信，将影响数据库状态。

**取值范围**: 枚举类型，有效值有trust, gss。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: trust

## cm_krb_server_keyfile

**参数说明**: kerberos服务端key文件所在位置，需要配置为绝对路径。该文件通常为${GAUSSHOME}/kerberos路径下，以keytab格式结尾，文件名与数据库运行所在用户名相同。与上述cm_auth_method参数是配对的，当cm_auth_method参数修改为gss时，该参数也必须配置为正确路径，否则将影响数据库状态

**取值范围**: 字符串类型，修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: ${GAUSSHOME}/kerberos/{UserName}.keytab，默认值无法生效，仅作为提示

## cm_server_arbitrate_delay_base_time_out

**参数说明**: cm_server仲裁延迟基础时长。cm_server主断连后，仲裁启动计时开始，经过仲裁延迟时长后，将选出新的cm_server主。其中仲裁延迟时长由仲裁延迟基础时长、节点index（server ID序号）和增量时长共同决定。公式为：仲裁延迟时长=仲裁延迟基础时长+节点index*仲裁延迟增量时长参数

**取值范围**: 整型，index>0，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10

## cm_server_arbitrate_delay_incrememtal_time_out

**参数说明**: cm_server仲裁延迟增量时长。cm_server主断连后，仲裁启动计时开始，经过仲裁延迟时长后，将选出新的cm_server主。其中仲裁延迟时长由仲裁延迟基础时长、节点index（server ID序号）和增量时长共同决定。公式为：仲裁延迟时长=仲裁延迟基础时长+节点index*仲裁延迟增量时长参数

**取值范围**: 整型，index>0，单位为秒。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 3

## force_promote

**参数说明**: cm_server是否打开强起逻辑（指数据库状态为Unknown的时候以丢失部分数据为代价保证数据库基本功能可用）的开关。0代表功能关闭，1代表功能开启。该参数同时适用于dn。

**取值范围**: 整型，0~1。在cm_server上修改后需要重启cm_server才能生效。

**默认值**: 0

## switch_rto

**参数说明**: cm_server强起逻辑等待时延。在force_promote被置为1时，当数据库的某一分片处于无主状态开始计时，等待该延迟时间后开始执行强起逻辑。

**取值范围**: 整型，60~2147483647，单位为秒。修改后需要重启cm_server才能生效。

**默认值**: 0

## backup_open

**参数说明**: 灾备数据库实例设置，开启后CM按照灾备数据库实例模式运行

**取值范围**: 整型，0~1。修改后需要重启cm_server才能生效。非灾备数据库实例不能开启该参数。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示关闭。
- 1表示开启。

**默认值**: 0

## enable_dcf

**参数说明**: DCF模式开关。

**取值范围**: 布尔型。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示关闭。
- 1表示开启。

**默认值**: off

## install_type

**参数说明**: 容灾数据库实例相关的设置，用来区别是否是基于dorado的数据库实例。

**取值范围**: 整型，0~2。修改后需要重启cm_server才能生效。非灾备数据库实例不能开启该参数。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示未搭建容灾关系的数据库实例。
- 1表示基于dorado的数据库实例。
- 2表示基于流式的数据库实例。

**默认值**: 0

## enable_ssl

**参数说明**: ssl证书开关。

**取值范围**: 布尔型。打开后使用ssl证书加密通信。修改后需要重启才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- on表示启用ssl。

- off表示不启用ssl。

- **默认值**: off

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 出于安全性考虑，建议不要关闭该配置。关闭后cm将**不使用**加密通信，所有信息明文传播，可能带来窃听、篡改、冒充等安全风险。

## ssl_cert_expire_alert_threshold

**参数说明**: ssl证书过期告警时间。

**取值范围**: 整型，单位为天。证书过期时间少于该时间时，上报证书即将过期告警。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 90

## ssl_cert_expire_check_interval

**参数说明**: ssl证书过期检测周期。

**取值范围**: 整型，单位为秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 86400

## delay_arbitrate_timeout

**参数说明**: 设置等待跟主DN同AZ节点redo回放，优先选择同AZ升主的时间。

**取值范围**: 整型，[0, 21474836]，单位：秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 0

## ddb_type

**参数说明**: etcd，dcc模式切换开关。

**取值范围**: 整型。0：etcd；1：dcc。修改后需要重启cm_server才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 0

## ddb_log_level

**参数说明**: 设置ddb日志级别。

关闭日志：“NONE”，NONE表示关闭日志打印，不能与以下日志级别混合使用。

开启日志：“RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER”日志级别可以从上述字符串中选取字符串并使用竖线组合使用，不能配置空串。

**取值范围**: 字符串，RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: RUN_ERR|RUN_WAR|DEBUG_ERR|OPER|RUN_INF|PROFILE

## ddb_log_backup_file_count

**参数说明**: 最大保存日志文件个数。

**取值范围**: 整型，[1, 100]。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10

## ddb_max_log_file_size

**参数说明**: 单条日志最大字节数。

**取值范围**: 字符串，[1M, 1000M]。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10M

## ddb_log_suppress_enable

**参数说明**: 是否开启日志抑制功能。

**取值范围**: 整型，0：关闭；1：开启。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## ddb_election_timeout

**参数说明**: dcc选举超时时间。

**取值范围**: 整型，[1, 600],单位：秒。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 3
