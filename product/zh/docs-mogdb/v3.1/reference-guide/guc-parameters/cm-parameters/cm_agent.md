---
title: cm_agent参数
summary: cm_agent参数
author: Guo Huan
date: 2022-05-23
---

# cm_agent参数

## log_dir

**参数说明**: log_dir决定存放cm_agent日志文件的目录。可以是绝对路径，或者是相对路径（相对于$GAUSSLOG的路径）。

**取值范围**: 字符串。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: “log”，表示在$GAUSSLOG下对应的cm目录下生成cm_agent日志。

## log_file_size

**参数说明**: 控制日志文件的大小。当日志文件达到指定大小时，则重新创建一个日志文件记录日志信息。

**取值范围**: 整型，取值范围0~2047，单位为MB。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 16MB

## log_min_messages

**参数说明**: 控制写到cm_agent日志文件中的消息级别。每个级别都包含排在它后面的所有级别中的信息。级别越低，服务器运行日志中记录的消息就越少。

**取值范围**: 枚举类型，有效值有debug5、debug1、warning、error、log、fatal。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: warning

## incremental_build

**参数说明**: 控制重建备DN模式是否为增量。打开这个开关，则增量重建备DN；否则，全量重建备DN。

**取值范围**: 布尔型，有效值有on、off。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: on

## alarm_component

**参数说明**: 设置用于处理告警内容的告警组件的位置。

**取值范围**: 字符串。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 若前置脚本gs_preinstall中的–alarm-type参数设置为5时，表示未对接第三方组件，告警写入system_alarm日志，此时GUC参数alarm_component的取值为：/opt/snas/bin/snas_cm_cmd。
- 若前置脚本gs_preinstall中的–alarm-type参数设置为1时，表示对接第三方组件，此时GUC参数alarm_component的值为第三方组件的可执行程序的绝对路径。

**默认值**: /opt/snas/bin/snas_cm_cmd

## alarm_report_interval

**参数说明**: 指定告警上报的时间间隔。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**取值范围**: 非负整型，单位为秒。

**默认值**: 1

## alarm_report_max_count

**参数说明**: 指定告警上报的最大次数。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**取值范围**: 非负整型。

**默认值**: 1

## agent_report_interval

**参数说明**: cm_agent上报实例状态的时间间隔。

**取值范围**: 整型，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## agent_phony_dead_check_interval

**参数说明**: cm_agent检测DN进程是否僵死的时间间隔。

**取值范围**: 整型，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10

## agent_check_interval

**参数说明**: cm_agent查询DN等实例状态的时间间隔。

**取值范围**: 整型，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 2

## agent_heartbeat_timeout

**参数说明**: cm_server心跳超时时间。

**取值范围**: 整型，2~231 - 1，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 8

## agent_connect_timeout

**参数说明**: cm_agent连接cm_server超时时间。

**取值范围**: 整型，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## agent_connect_retries

**参数说明**: cm_agent连接cm_server尝试次数。

**取值范围**: 整型。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 15

## agent_kill_instance_timeout

**参数说明**: 当cm_agent在无法连接cm_server主节点后，发起一次杀死本节点上所有实例的操作之前，所需等待的时间间隔。

**取值范围**: 整型。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 0，不发起杀死本节点上所有实例的操作。

## security_mode

**参数说明**: 控制是否以安全模式启动DN。打开这个开关，则以安全模式启动DN；否则，以非安全模式启动DN。

**取值范围**: 布尔型，有效值有on、off。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: off

## upgrade_from

**参数说明**: 就地升级过程中使用，用于标示升级前数据库的内部版本号，此参数禁止手动修改。

**取值范围**: 非负整型。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 0

## process_cpu_affinity

**参数说明**: 控制是否以绑核优化模式启动主DN进程。配置该参数为0，则不进行绑核优化；否则，进行绑核优化，且物理CPU片数为2n个。数据库、cm_agent重启生效。仅支持ARM。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**取值范围**: 整型，0~2。

**默认值**: 0

## log_threshold_check_interval

**参数说明**: 日志压缩和清除的时间间隔，每1800秒压缩和清理一次。

**取值范围**: 整型，0~2147483647，单位为秒。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1800

## dilatation_shard_count_for_disk_capacity_alarm

**参数说明**: 扩容场景下，设置新增的扩容分片数，用于上报磁盘容量告警时的阈值计算。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该分片数请与实际扩容分片数设置为一致。

**取值范围**: 整型，0~232 - 1，单位为个。该参数设置为0，表示关闭磁盘扩容告警上报；该参数设置为大于0，表示开启磁盘扩容告警上报，且告警上报的阈值根据此参数设置的分片数量进行计算。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 1

## log_max_size

**参数说明**: 控制日志最大存储值。

**取值范围**: 整型，0~2147483647，单位为MB。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10240

## log_max_count

**参数说明**: 硬盘上可存储的最多日志数量。

**取值范围**: 整型，0~10000，单位为个。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 10000

## log_saved_days

**参数说明**: 日志保存的天数。

**取值范围**: 整型，0~1000，单位为天。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 90

## enable_log_compress

**参数说明**: 控制压缩日志功能。

**取值范围**: 布尔型。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- on表示允许压缩日志。
- off表示不允许压缩日志。

**默认值**: on

## log_pattern_%s

**参数说明**: enable_log_compress=on时使用，定义压缩日志的格式，压缩含有关键字%s的所有日志，%s为服务端工具名称。

**取值范围**: 表1中所有参数对应的值。参数修改请参考[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值**: 表1 中各参数对应的默认值。不支持用户修改默认值。

**表 1**

| 参数（log_pattern_%s）           | 默认值                |
| :------------------------------- | :-------------------- |
| log_pattern_cm_ctl               | cm_ctl-               |
| log_pattern_gs_clean             | gs_clean-             |
| log_pattern_gs_ctl               | gs_ctl-               |
| log_pattern_gs_guc               | gs_guc-               |
| log_pattern_gs_dump              | gs_dump-              |
| log_pattern_gs_dumpall           | gs_dumpall-           |
| log_pattern_gs_restore           | gs_restore-           |
| log_pattern_gs_initcm            | gs_initcm-            |
| log_pattern_gs_initdb            | gs_initdb-            |
| log_pattern_gs_initgtm           | gs_initgtm-           |
| log_pattern_gtm_ctl              | gtm_ctl-              |
| log_pattern_cm_agent             | cm_agent-             |
| log_pattern_system_call          | system_call-          |
| log_pattern_cm_server            | cm_server-            |
| log_pattern_om_monitor           | om_monitor-           |
| log_pattern_gs_local             | gs_local-             |
| log_pattern_gs_preinstall        | gs_preinstall-        |
| log_pattern_gs_install           | gs_install-           |
| log_pattern_gs_replace           | gs_replace-           |
| log_pattern_gs_uninstall         | gs_uninstall-         |
| log_pattern_gs_om                | gs_om-                |
| log_pattern_gs_upgradectl        | gs_upgradectl-        |
| log_pattern_gs_expand            | gs_expand-            |
| log_pattern_gs_shrink            | gs_shrink-            |
| log_pattern_gs_postuninstall     | gs_postuninstall-     |
| log_pattern_gs_backup            | gs_backup-            |
| log_pattern_gs_checkos           | gs_checkos-           |
| log_pattern_gs_collector         | gs_collector-         |
| log_pattern_GaussReplace         | GaussReplace-         |
| log_pattern_GaussOM              | GaussOM-              |
| log_pattern_gs_checkperf         | gs_checkperf-         |
| log_pattern_gs_check             | gs_check-             |
| log_pattern_roach-agent          | roach-agent-          |
| log_pattern_roach-controller     | roach-controller-     |
| log_pattern_postgresql           | postgresql-           |
| log_pattern_gtm                  | gtm-                  |
| log_pattern_sessionstat          | sessionstat-          |
| log_pattern_sync                 | sync-                 |
| log_pattern_system_alarm         | system_alarm-         |
| log_pattern_pg_perf              | pg_perf-              |
| log_pattern_slow_query_log       | slow_query_log-       |
| log_pattern_asp                  | asp-                  |
| log_pattern_etcd                 | etcd-                 |
| log_pattern_gs_cgroup            | gs_cgroup-            |
| log_pattern_pscp                 | pscp-                 |
| log_pattern_postgresql-query-log | postgresql-query-log- |
| log_pattern_gs_hotpatch          | gs_hotpatch-          |
| log_pattern_pssh                 | pssh-                 |

## agent_backup_open

**参数说明**: 灾备数据库实例设置，开启后CM按照灾备数据库实例模式运行。

**取值范围**: 整型，0~1。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示关闭。
- 1表示开启。

**默认值**: 0

## enable_xc_maintenance_mode

**参数说明**: 在数据库实例为只读模式下，控制是否可以修改pgxc_node系统表。

**取值范围**: 布尔型。参数修改请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- on表示开启可以修改pgxc_node系统表功能。
- off表示关闭可以修改pgxc_node系统表功能。

**默认值**: on

## unix_socket_directory

**参数说明**: unix套接字的目录位置。

**取值范围**: 字符串。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

**默认值：”**

## enable_dcf

**参数说明**: DCF模式开关。

**取值范围**: 布尔型。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示关闭。
- 1表示开启。

**默认值**: 0

## disaster_recovery_type

**参数说明**: 主备数据库实例灾备关系的类型。

**取值范围**: 整型，0~2。修改后需要重启cm_agent才能生效。参数修改请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)进行设置。

- 0表示未搭建灾备关系。
- 1表示搭建了obs灾备关系。
- 2表示搭建了流式灾备关系

**默认值**: 0
