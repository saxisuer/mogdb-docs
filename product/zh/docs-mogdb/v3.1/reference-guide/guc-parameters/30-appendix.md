---
title: 附录
summary: 附录
author: Zhang Cuiping
date: 2021-04-20
---

# 附录

## **表 1** GUC参数分类

| 参数类型   | 说明                                                         | 设置方式                                                     |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| INTERNAL   | 固定参数，在创建数据库的时候确定，用户无法修改，只能通过show语法或者pg_settings视图进行查看。 | 无                                                           |
| POSTMASTER | 数据库服务端参数，在数据库启动时确定，可以通过配置文件指定。 | 支持表2中的方式一、方式四。                                  |
| SIGHUP     | 数据库全局参数，可在数据库启动时设置或者在数据库启动后，发送指令重新加载。 | 支持表2中的方式一、方式二、方式四。                          |
| BACKEND    | 会话连接参数。在创建会话连接时指定，连接建立后无法修改。连接断掉后参数失效。内部使用参数，不推荐用户设置。 | 支持表2中的方式一、方式二、方式四。<br/>说明：设置该参数后，下一次建立会话连接时生效。 |
| SUSET      | 数据库管理员参数。可在数据库启动时、数据库启动后或者数据库管理员通过SQL进行设置。 | 支持表2中的方式一、方式二或由数据库管理员通过方式三设置。    |
| USERSET    | 普通用户参数。可被任何用户在任何时刻设置。                   | 支持表2中的方式一、方式二或方式三设置。                      |

## **表 2** GUC参数设置方式

| 序号   | 设置方法                                                     |
| ------ | ------------------------------------------------------------ |
| 方式一 | 1. 使用如下命令修改参数。<br />`gs_guc set -D datadir -c "paraname=value"`<br />说明：如果参数是一个字符串变量，则使用-c parameter="'value'"或者使用-c "parameter = 'value'"。<br />使用以下命令在数据库节点上同时设置某个参数。<br />`gs_guc set -N all -I all -c "paraname=value"`<br />使用以下命令在数据库节点上设置cm_agent某个参数。<br />`gs_guc set -Z cmagent -c "paraname=value"`<br />`gs_guc set -Z cmagent -N all -I all -c "paraname=value"`<br />使用以下命令在数据库节点上设置cm_server某个参数。<br />`gs_guc set -Z cmserver -c "paraname=value"`<br />`gs_guc set -Z cmserver -N all -I all -c "paraname=value"`<br />2. 重启数据库使参数生效。<br />说明：<br />重启MogDB操作会导致用户执行操作中断，请在操作之前规划好合适的执行窗口。<br />`gs_om -t stop && gs_om -t start` |
| 方式二 | `gs_guc reload -D datadir -c "paraname=value"`<br />说明：<br />使用以下命令在数据库节点上同时设置某个参数。<br />`gs_guc reload -N all -I all -c "paraname=value"` |
| 方式三 | 修改指定数据库、用户、会话级别的参数。<br />- 设置数据库级别的参数<br />`MogDB=# ALTER DATABASE dbname SET paraname TO value;`<br />在下次会话中生效。<br />- 设置用户级别的参数<br />`MogDB=# ALTER USER username SET paraname TO value;`<br />在下次会话中生效。<br />- 设置会话级别的参数<br />`MogDB=# SET paraname TO value;`<br />修改本次会话中的取值。退出会话后，设置将失效。<br />说明：<br />SET设置的会话级参数优先级最高，其次是ALTER设置的，其中ALTER DATABASE设置的参数值优先级高于ALTER USER设置，这三种设置方式设置的优先级都高于gs_guc设置方式。 |
| 方式四 | 使用ALTER SYSTEM SET修改数据库参数。<br />- 设置POSTMASERT级别的参数<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />重启后生效。<br />- 设置SIGHUP级别的参数<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />立刻生效（实际等待线程重新加载参数略有延迟）。<br />- 设置BACKEND级别的参数<br />`MogDB=# ALTER SYSTEM SET paraname TO value;`<br />在下次会话中生效。 |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **注意：** 使用方式一和方式二设置参数时，若所设参数不属于当前环境，数据库会提示参数不在支持范围内的相关信息。
