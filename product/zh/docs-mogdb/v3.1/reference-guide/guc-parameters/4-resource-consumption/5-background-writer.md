---
title: 后端写进程
summary: 后端写进程
author: Zhang Cuiping
date: 2021-04-20
---

# 后端写进程

介绍后端写（background writer）进程的参数配置。后端写进程的功能就是把共享缓冲区中的脏数据（指共享缓冲区中新增或者修改的内容）写入到磁盘。目的是让数据库进程在进行用户查询时可以很少或者几乎不等待写动作的发生（写动作由后端写进程完成）。

此机制同样也减少了检查点造成的性能下降。后端写进程将持续的把脏页面刷新到磁盘上，所以在检查点到来的时候，只有几个页面需要刷新到磁盘上。但是这样还是增加了I/O的总净负荷，因为以前的检查点间隔里，一个重复弄脏的页面可能只会冲刷一次，而同一个间隔里，后端写进程可能会写好几次。在大多数情况下，连续的低负荷要比周期性的尖峰负荷好，但是在本节讨论的参数可以用于按实际需要调节其行为。

## bgwriter_thread_num

**参数说明**: 设置用于增量检查点打开后后台刷页的线程数，将可以淘汰的脏页刷盘，不脏的页面放入到候选buffer链，设置此选项有助于加快buffer淘汰速度，提升性能。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～8

- 开发期间为测试关闭该特性的效果，可以设置为0，但是当前如果设置为0，会在代码中修正为1，不再支持关闭该特性。
- 当参数设置为1~8时，表示会启动相对应数量的background线程，用于维护候选buffer链，将满足条件的脏页刷盘，不脏的页放入到候选list中。

**默认值**: 2

## bgwriter_delay

**参数说明**: 设置后端写进程写“脏”共享缓冲区之间的时间间隔。每一次，后端写进程都会为一些脏的缓冲区发出写操作（用bgwriter_lru_maxpages参数控制每次写的量），然后休眠bgwriter_delay毫秒后才再次启动。

在许多系统上，休眠延时的有效分辨率是10毫秒。因此，设置一个不是10的倍数的数值与把它设置为下一个10的倍数是一样的效果。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10~10000，单位为毫秒。

**默认值**: 2s

**设置建议**：在数据写压力比较大的场景中可以尝试减小该值以降低checkpoint的压力。

## candidate_buf_percent_target

**参数说明**: 设置用于增量检查点打开时，候选buffer链中可用buffer数目占据shared_buffer内存缓冲区百分比的期望值，当前候选链中的数目少于目标值时，bgwriter线程会启动将满足条件的脏页刷盘。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 双精度浮点类型，0.1～0.85

**默认值**: 0.3

## bgwriter_lru_maxpages

**参数说明**: 设置后端写进程每次可写入磁盘的"脏"缓存区的个数。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～1000

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> 此参数设置为0表示禁用后端写功能，禁用后端写功能不会对checkpoints产生影响。

**默认值**: 100

## bgwriter_lru_multiplier

**参数说明**: 通过与已使用缓存区数目的乘积评估下次服务器需要的缓存区数目。

写“脏”缓存区到磁盘的数目取决于服务器最近几次使用的缓存区数目。最近的buffers数目的平均值乘以bgwriter_lru_multiplier是为了评估下次服务器进程需要的buffers数目。在有足够多的干净的、可用的缓存区之前，后端写进程会一直写“脏”缓存区的（每次写的缓存区数目不会超过bgwriter_lru_maxpages的值）。

设置bgwriter_lru_multiplier的值为1.0表示一种“实时”策略，其作用是精准预测下次写“脏”缓冲区的数目。设置为较大的值可以应对突然的需求高峰，而较小的值则可以让服务器进程执行更多的写操作。

设置较小的bgwriter_lru_maxpages和bgwriter_lru_multiplier会减小后端写进程导致的额外I/O开销，但是服务器进程必须自己发出写操作，增加了对查询的响应时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0~10。

**默认值**: 2

## pagewriter_thread_num

**参数说明**: 设置用于增量检查点打开后后台刷页的线程数，主要是按照脏页置脏的顺序刷盘，用于推进recovery点。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～16

**默认值**: 4

## dirty_page_percent_max

**参数说明**: 设置用于增量检查点打开后脏页数量占shared_buffers的百分比。达到这个设定值时，后台刷页线程将以设置的max_io_capacity计算出的最大值刷脏页。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.1～1

**默认值**: 0.9

## pagewriter_sleep

**参数说明**: 设置用于增量检查点打开后，pagewrite线程每隔pagewriter_sleep的时间刷一批脏页下盘。当脏页占据shared_buffers的比例达到dirty_page_percent_max时，每批页面数量以设定的max_io_capacity计算出的值刷页，其余情况每批页面数量按比例相对减少。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～3600000（毫秒）

**默认值**: 2000ms

## max_io_capacity

**参数说明**: 设置后端写进程批量刷页每秒的IO上限，需要根据具体业务场景和机器磁盘IO能力进行设置。要求RTO很短时间或者数据量比共享内存大多倍的情况，业务访问数据量又是随机访问时，该值不宜过小。该参数设置较小会减小后端写进程刷页个数，如果业务触发页面淘汰多时，该值设置小会影响业务。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，30720~10485760。单位是KB。

**默认值**: 512000KB（500MB）

## enable_consider_usecount

**参数说明**: 设置backend线程在页面置换时是否考虑页面热度，建议大容量场景下开启此参数。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示考虑页面热度。
- off/false表示不考虑页面热度。

**默认值**: off

## dw_file_num

**参数说明**: 设置批量双写文件的数量，该值与pagewriter_thread_num有关，不会大于pagwriter_thread_num，如果设置过大，内部会纠正为pagewriter_thread_num。

该参数属于POSTMASTER类型参数，请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～16

**默认值**: 1

## dw_file_size

**参数说明**: 设置每个批量双写文件的大小。

该参数属于POSTMASTER类型参数，请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，32～256

**默认值**: 256
