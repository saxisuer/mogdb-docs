---
title: 异步IO
summary: 异步IO
author: Zhang Cuiping
date: 2021-04-20
---

# 异步IO

## enable_adio_debug

**参数说明**: 允许维护人员输出一些与ADIO相关的日志，便于定位ADIO相关问题。开发人员专用，不建议普通用户使用。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启此日志开关。
- off/false表示关闭此日志开关。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前版本暂不支持打开该开关，即使用户手动设置为打开，系统内部也会自动设置为关闭状态。

## enable_adio_function

**参数说明**: 是否开启ADIO功能。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前版本暂不支持开启异步IO功能，默认该功能关闭。

**取值范围**: 布尔型

- on/true表示开启此功能。
- off/false表示关闭此功能。

**默认值**: off

## enable_fast_allocate

**参数说明**: 磁盘空间快速分配开关。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。只有在XFS文件系统上才能开启该开关。

**取值范围**: 布尔型

- on/true表示开启此功能。
- off/false表示关闭此功能。

**默认值**: off

## prefetch_quantity

**参数说明**: 描述行存储使用ADIO预读取IO量的大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，128～131072，单位为8KB。

**默认值**: 32MB (4096 * 8KB)

## backwrite_quantity

**参数说明**: 描述行存储使用ADIO写入IO量的大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，128～131072，单位为8KB。

**默认值**: 8MB (1024 * 8KB)

## cstore_prefetch_quantity

**参数说明**: 描述列存储使用ADIO预取IO量的大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024～1048576，单位为KB。

**默认值**: 32MB

## cstore_backwrite_quantity

**参数说明**: 描述列存储使用ADIO写入IO量的大小。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024～1048576，单位为KB。

**默认值**: 8MB

## cstore_backwrite_max_threshold

**参数说明**: 描述列存储使用ADIO写入数据库可缓存最大的IO量。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，4096～INT_MAX/2，单位为KB。

**默认值**: 2GB

## fast_extend_file_size

**参数说明**: 描述列存储使用ADIO预扩展磁盘的大小。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024～1048576，单位为KB。

**默认值**: 8MB

## effective_io_concurrency

**参数说明**: 磁盘子系统可以同时有效处理的请求数。对于RAID阵列，此参数应该是阵列中驱动器主轴的数量。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型 ，0~1000

**默认值**: 1

## checkpoint_flush_after

**参数说明**: 设置checkpointer线程刷页个数超过设定的阈值时，告知操作系统开始将操作系统缓存中的页面异步刷盘。MogDB中，磁盘页大小为8KB。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256（0表示关闭异步刷盘功能）。例如，取值32，表示checkpointer线程连续写32个磁盘页，即32*8=256KB磁盘空间后会进行异步刷盘。

**默认值**: 32

## bgwriter_flush_after

**参数说明**: 设置background writer线程刷页个数超过设定的阈值时，告知操作系统开始将操作系统缓存中的页面异步刷盘。MogDB中，磁盘页大小为8KB。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256（0表示关闭异步刷盘功能），单位页面（8KB）。例如，取值64，表示background writer线程连续写64个磁盘页，即64*8=512KB磁盘空间后会进行异步刷盘。

**默认值**: 512KB（即64个页面）

## backend_flush_after

**参数说明**: 设置backend线程刷页个数超过设定的阈值时，告知操作系统开始将操作系统缓存中的页面异步刷盘。MogDB中，磁盘页大小为8KB。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256（0表示关闭异步刷盘功能），单位页面（8KB）。例如，取值64，表示backend线程连续写64个磁盘页，即64*8=512KB磁盘空间后会进行异步刷盘。

**默认值**: 0
