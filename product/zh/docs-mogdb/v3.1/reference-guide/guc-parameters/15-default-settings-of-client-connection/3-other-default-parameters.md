---
title: 其他缺省
summary: 其他缺省
author: Zhang Cuiping
date: 2021-04-20
---

# 其他缺省

主要介绍数据库系统默认的库加载参数。

## dynamic_library_path

**参数说明**: 设置数据查找动态加载的共享库文件的路径。当需要打开一个可以动态装载的模块并且在CREATE FUNCTION或LOAD命令里面声明的名称没有目录部分时，系统将搜索这个目录以查找声明的文件。

用于dynamic_library_path的数值必须是一个冒号分隔的绝对路径列表。当一个路径名称以特殊变量\$libdir为开头时，会替换为MogDB发布提供的模块安装路径。例如：

```bash
dynamic_library_path = '/usr/local/lib/mogdb:/opt/testgs/lib:$libdir'
```

该参数属于SUSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
> 设置为空字符串，表示关闭自动路径搜索。

**默认值**:  `$libdir`

## gin_fuzzy_search_limit

**参数说明**: 设置GIN索引返回的集合大小的上限。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~2147483647

**默认值**: 0

## local_preload_libraries

**参数说明**: 指定一个或多个共享库，它们在开始连接前预先加载。多个加载库之间用逗号分隔，除了双引号，所有的库名都转换为小写。

- 并非只有系统管理员才能更改此选项，因此只能加载安装的标准库目录下plugins子目录中的库文件，数据库管理员有责任确保该目录中的库都是安全的。local_preload_libraries中指定的项可以明确含有该目录，例如`$libdir/plugins/mylib`; 也可以仅指定库的名称，例如mylib（等价于`$libdir/plugins/mylib`）。
- 与shared_preload_libraries不同，在会话开始之前加载模块与在会话中使用到该模块的时候临时加载相比并不具有性能优势。相反，这个特性的目的是为了调试或者测量在特定会话中不明确使用LOAD加载的库。例如针对某个用户将该参数设为ALTER USER SET来进行调试。
- 当指定的库未找到时，连接会失败。
- 每一个支持MogDB的库都有一个"magic block"用于确保兼容性，因此不支持MogDB的库不能通过这个方法加载。

该参数属于BACKEND类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空
