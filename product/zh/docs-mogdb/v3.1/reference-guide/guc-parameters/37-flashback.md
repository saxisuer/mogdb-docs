---
title: 闪回相关参数
summary: 闪回相关参数
author: Zhang Cuiping
date: 2021-11-08
---

# 闪回相关参数

本章节介绍闪回功能相关参数。

## enable_recyclebin

**参数说明**: 用来控制回收站的实时打开和关闭。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**：off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **注意：** recyclebin不支持Astore，只支持Ustore。

## recyclebin_retention_time

参数说明：设置回收站对象保留时间，超过该时间的回收站对象将被自动清理。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

取值范围: 整型，单位为s，最小值为1，最大值为2147483647。

默认值：15min（即900s）

## version_retention_age

**参数说明**: 设置旧版本保留的事务数，超过该事务数的旧版本将被回收清理。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～576460752303423487，值为0表示不延迟。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 该参数已弃用。

## vacuum_defer_cleanup_age

**参数说明**: 指定VACUUM使用的事务数，VACUUM会延迟清除无效的行存表记录，延迟的事务个数通过vacuum_defer_cleanup_age进行设置。即VACUUM和VACUUM FULL操作不会立即清理刚刚被删除元组。也可以通过设置该参数，配置闪回功能旧版本保留期限。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～1000000，值为0表示不延迟。取值范围需要扩展到1亿。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 在进行Ustore闪回时，无需关注该参数。其服务于之前版本的astore闪回功能，同时具有其他用途。本版本闪回功能已不使用。

## undo_retention_time

**参数说明**: 设置undo旧版本保留时间。

该参数属于SIGHUP类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位为s，最小值为0，最大值为2147483647。

**默认值**: 0
