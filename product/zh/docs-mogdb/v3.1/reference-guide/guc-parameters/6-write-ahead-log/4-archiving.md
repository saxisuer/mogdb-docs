---
title: 归档
summary: 归档
author: Zhang Cuiping
date: 2021-04-20
---

# 归档

## archive_mode

**参数说明**: 表示是否进行归档操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> 当wal_level设置成minimal时，archive_mode参数无法使用。

**取值范围**: 布尔型

- on表示进行归档。
- off表示不进行归档。

**默认值**: off

## archive_command

**参数说明**: 由管理员设置的用于归档WAL日志的命令，建议归档路径为绝对路径。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> - 当archive_dest和archive_command同时配置时，WAL日志优先保存到archive_dest所设置的目录中，archive_command配置的命令不生效。
>
> - 字符串中任何%p都被要归档的文件的绝对路径代替，而任何%f都只被该文件名代替（相对路径都相对于数据目录的）。如果需要在命令里嵌入%字符就必须双写%。
>
> - 这个命令当且仅当成功的时候才返回零。示例如下:
>
>   ```
>   archive_command = 'cp --remove-destination %p /mnt/server/archivedir/%f'
>   ```
>
> - --remove-destination选项作用为：拷贝前如果目标文件已存在，会先删除已存在的目标文件，然后执行拷贝操作。
>
> - 如果归档命令有多条，则需将其写入SHELL脚本文件中，然后将archive_command配置为执行该脚本的命令。示例如下：
>
>   ```
>   --假设多条命令如下。
>   test ! -f dir/%f && cp %p dir/%f
>   --则test.sh脚本内容如下。
>   test ! -f dir/$2 && cp $1 dir/$2
>   --归档命令如下。
>   archive_command='sh dir/test.sh %p %f'
>   ```

**取值范围**: 字符串

**默认值**: (disabled)

## archive_dest

**参数说明**: 由管理员设置的用于归档WAL日志的目录，建议归档路径为绝对路径。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> - 当archive_dest和archive_command同时配置时，WAL日志优先保存到archive_dest所设置的目录中，archive_command配置的命令不生效。
>
> - 字符串中如果是相对路径为相对于数据目录的。示例如下。
>
>   ```
>   archive_dest = '/mnt/server/archivedir/'
>   ```

**取值范围**: 字符串

**默认值**: 空字符串

## archive_timeout

**参数说明**: 表示归档周期。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> - 超过该参数设定的时间时强制切换WAL段。
> - 由于强制切换而提早关闭的归档文件仍然与完整的归档文件长度相同。因此，将archive_timeout设为很小的值将导致占用巨大的归档存储空间，建议将archive_timeout设置为60秒。

**取值范围**: 整型，0 ~ INT_MAX，单位为秒。其中0表示禁用该功能。

**默认值**: 0
