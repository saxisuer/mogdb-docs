---
title: 高效数据压缩算法相关参数
summary: 高效数据压缩算法相关参数
author: zhang cuiping
date: 2022-11-01
---

# 高效数据压缩算法相关参数

## pca_shared_buffer

**参数说明：** 类似于shared_buffers，用于设置页面压缩块地址映射管理buffer的大小。

该参数属于POSTMASTER类型参数，请参考[GUC参数分类](./30-appendix.md)中对应设置方法进行设置。

**取值范围：** 最小值64K，最大值16G。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：** 
>
> - 如果设置值小于64K，设置报错。 
> - 如果设置值大于16G，参数可以设置成功，但实际运行时候，自动内存运行设置为16G。 
> - 如果设置参数不带单位，默认是8K(一个页面的大小是8K)乘以设置的参数大小。

**默认值：** 64K