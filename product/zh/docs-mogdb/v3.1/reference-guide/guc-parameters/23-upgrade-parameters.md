---
title: 升级参数
summary: 升级参数
author: Zhang Cuiping
date: 2021-04-20
---

# 升级参数

## IsInplaceUpgrade

**参数说明**: 标示是否在升级的过程中。该参数用户无法修改。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示在升级过程中。
- off表示不在升级过程中。

**默认值**: off

## inplace_upgrade_next_system_object_oids

**参数说明**: 标示就地升级过程中，新增系统对象的OID。该参数用户无法修改。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空

## upgrade_mode

**参数说明**: 标示升级模式。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 整数，0~INT_MAX

- 0表示不在升级过程中。
- 1表示在就地升级过程中。
- 2表示在灰度升级过程中。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  特殊情况：在使用灰度升级的情况下，若选择策略为大版本升级，即需要执行升级脚本和替换二进制包，会将upgrade_mode设置为2，选择策略为小版本升级，只替换二进制包，则不会设置upgrade_mode设置为2。
