---
title: 开发人员选项
summary: 开发人员选项
author: Zhang Cuiping
date: 2021-04-20
---

# 开发人员选项

## allow_system_table_mods

**参数说明**: 设置是否允许修改系统表的结构。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许修改系统表的结构。
- off表示不允许修改系统表的结构。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif)**注意**：不建议修改该参数默认值，如果设置为on，可能导致系统表损坏，甚至数据库无法启动。

## debug_assertions

**参数说明**: 控制打开各种断言检查。能够协助调试，当遇到奇怪的问题或者崩溃，请把此参数打开，因为它能暴露编程的错误。要使用这个参数，必须在编译MogDB的时候定义宏USE_ASSERT_CHECKING（通过configure选项--enable-cassert完成）。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开断言检查。
- off表示不打开断言检查。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> 当启用断言选项编译MogDB时，debug_assertions缺省值为on 。

**默认值**: off

## ignore_checksum_failure

**参数说明**: 设置读取数据时是否忽略校验信息检查失败（但仍然会告警），继续执行可能导致崩溃，传播或隐藏损坏数据，无法从远程节点恢复数据及其他严重问题。不建议用户修改设置。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示忽略数据校验错误。
- off表示数据校验错误正常报错。

**默认值**: off

## ignore_system_indexes

**参数说明**: 读取系统表时忽略系统索引（但是修改系统表时依然同时修改索引）。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：这个参数在从系统索引被破坏的表中恢复数据的时候非常有用。
>

**取值范围**: 布尔型

- on表示忽略系统索引。
- off表示不忽略系统索引。

**默认值**: off

## post_auth_delay

**参数说明**: 在认证成功后，延迟指定时间，启动服务器连接。允许调试器附加到启动进程上。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为2147，单位为秒。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  此参数只用于调试和问题定位，为避免影响正常业务运行，生产环境下请确保参数值为默认值0。参数设置为非0时可能会因认证延迟时间过长导致数据库实例状态异常。

## pre_auth_delay

**参数说明**: 启动服务器连接后，延迟指定时间，进行认证。允许调试器附加到认证过程上。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0～60，单位为秒。

**默认值**: 0

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  此参数只用于调试和问题定位，为避免影响正常业务运行，生产环境下请确保参数值为默认值0。参数设置为非0时可能会因认证延迟时间过长导致数据库实例状态异常。

## trace_notify

**参数说明**: 为LISTEN和NOTIFY命令生成大量调试输出。client_min_messages或log_min_messages级别必须是DEBUG1或者更低时，才能把这些输出分别发送到客户端或者服务器日志。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开输出功能。
- off表示关闭输出功能。

**默认值**: off

## trace_recovery_messages

**参数说明**: 启用恢复相关调试输出的日志录，否则将不会被记录。该参数允许覆盖正常设置的log_min_messages，但是仅限于特定的消息，这是为了在调试备机中使用。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型，有效值有debug5、debug4、debug3、debug2、debug1、log，取值的详细信息请参见[log_min_messages](../../reference-guide/guc-parameters/10-error-reporting-and-logging/2-logging-time.md#log_min_messages)。

**默认值**: log

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> - 默认值log表示不影响记录决策。
> - 除默认值外，其他值会导致优先级更高的恢复相关调试信息被记录，因为它们有log优先权。对于常见的log_min_messages设置，这会导致无条件地将它们记录到服务器日志上。

## trace_sort

**参数说明**: 控制是否在日志中打印排序操作中的资源使用相关信息。这个选项只有在编译MogDB的时候定义了TRACE_SORT宏的时候才可用，不过目前TRACE_SORT是由缺省定义的。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开控制功能。
- off表示关闭控制功能。

**默认值**: off

## zero_damaged_pages

**参数说明**: 控制检测导致MogDB报告错误的损坏的页头，终止当前事务。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

设置为on时，会导致系统报告一个警告，把损坏的页面填充为零然后继续处理。这种行为会破坏数据，也就是所有在已经损坏页面上的行记录。但是它允许绕开坏页面然后从表中尚存的未损坏页面上继续检索数据行。因此它在因为硬件或者软件错误导致的崩溃中进行恢复是很有用的。通常不应该把它设置为on，除非不需要从崩溃的页面中恢复数据。

**默认值**: off

## remotetype

**参数说明**: 设置远程连接类型。

该参数不支持修改。

**取值范围**: 枚举类型，有效值有application、datanode、internaltool。

**默认值**: application

## max_user_defined_exception

**参数说明**: 异常最大个数，默认值不可更改。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，当前只能取固定值1000

**默认值**: 1000

## enable_fast_numeric

**参数说明**: 标识是否开启Numeric类型数据运算优化。Numeric数据运算是较为耗时的操作之一，通过将Numeric转化为int64/int128类型，提高Numeric运算的性能。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启Numeric优化。
- off/false表示关闭Numeric优化。

**默认值**: on

## enable_compress_spill

**参数说明**: 标识是否开启下盘压缩功能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启下盘优化。
- off/false表示关闭下盘优化。

**默认值**: on

## resource_track_log

**参数说明**: 控制自诊断的日志级别。目前仅对多列统计信息进行控制。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- summary：显示简略的诊断信息。
- detail：显示详细的诊断信息。

目前这两个参数值只在显示多列统计信息未收集的告警的情况下有差别，summary不显示未收集多列统计信息的告警，detail会显示这类告警。

**默认值**: summary

## show_acce_estimate_detail

**参数说明**: 评估信息一般用于运维人员在维护工作中使用，因此该参数默认关闭，此外为了避免这些信息干扰正常的explain信息显示，只有在explain命令的verbose选项打开的情况下才显示评估信息

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示可以在explain命令的输出中显示评估信息。
- off表示不在explain命令的输出中显示评估信息。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前版本不支持加速数据库实例，因此该参数设置后不生效。

## support_batch_bind

**参数说明**: 控制是否允许通过JDBC、ODBC、Libpq等接口批量绑定和执行PBE形式的语句。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用批量绑定和执行。
- off表示不使用批量绑定和执行。

**默认值**: on

## numa_distribute_mode

**参数说明**: 用于控制部分共享数据和线程在NUMA节点间分布的属性。用于大型多NUMA节点的ARM服务器性能调优，一般不用设置。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，当前有效取值为'none', 'all'。

- none：表示不启用本特性。
- all：表示将部分共享数据和线程分布到不同的NUMA节点下，减少远端访存次数，提高性能。目前仅适用于拥有多个NUMA节点的ARM服务器，并且要求全部NUMA节点都可用于数据库进程，不支持仅选择一部分NUMA节点。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  当前版本x86平台下不支持numa_distribute_mode设置为all。

**默认值**: 'none'

## log_pagewriter

**参数说明**: 设置用于增量检查点打开后，显示线程的刷页信息以及增量检查点的详细信息，信息比较多，不建议设置为true。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**: on

## advance_xlog_file_num

**参数说明**: 用于控制在后台周期性地提前初始化xlog文件的数目。该参数是为了避免事务提交时执行xlog文件初始化影响性能，但仅在超重负载时才可能出现，因此一般不用配置。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~1000000（0表示不提前初始化）。例如，取值10，表示后台线程会周期性地根据当前xlog写入位置提前初始化10个xlog文件。

**默认值**: 0

## enable_beta_opfusion

**参数说明**: 在enable_opfusion参数打开的状态下，如果开启该参数，可以支持TPCC中出现的聚集函数，排序两类SQL语句的加速执行，提升SQL执行性能。

该参数属于USERSET类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启。
- off表示不开启。

**默认值**: off

## string_hash_compatible

**参数说明**: 该参数用来说明char类型和varchar/text类型的hash值计算方式是否相同，以此来判断进行分布列从char类型到相同值的varchar/text类型转换，数据分布变化时，是否需要进行重分布。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示计算方式相同，不需要进行重分布。
- off表示计算方式不同，需要进行重分布。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  计算方式的不同主要体现在字符串计算hash值时传入的字节长度上。（如果为char，则会忽略字符串后面空格的长度，如果为text或varchar，则会保留字符串后面空格的长度。）hash值的计算会影响到查询的计算结果，因此此参数一旦设置后，在整个数据库使用过程中不能再对其进行修改，以避免查询错误。

**默认值**: off

## pldebugger_timeout

**参数说明**: 该参数用来控制pldebugger server端等待debug端响应的超时时间。

该参数属于USERSET类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1 ~ 86400，单位为秒。

**默认值**: 15min

## plsql_show_all_error

**参数说明**: 该参数用来控制编译PLPGSQL对象时是否支持跳过报错继续编译，具体影响请参考[DBE_PLDEVELOPER](../../reference-guide/schema/DBE_PLDEVELOPER/overview-of-DBE_PLDEVELOPER.md)内的说明。

该参数属于USERSET类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**: off
