---
title: 告警检测
summary: 告警检测
author: Zhang Cuiping
date: 2021-04-20
---

# 告警检测

在MogDB运行的过程中，会对数据库中的错误场景进行检测，便于用户及早感知到MogDB的错误。

## enable_alarm

**参数说明**: 允许打开告警检测线程，检测数据库中可能的错误场景。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许打开告警检测线程。
- off表示不允许打开告警检测线程。

**默认值**: on

## connection_alarm_rate

**参数说明**: 允许和数据库连接的最大并发连接数的比率限制。数据库连接的最大并发连接数为max_connections * connection_alarm_rate。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.0~1.0

**默认值**: 0.9

## alarm_report_interval

**参数说明**: 指定告警上报的时间间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位为秒。

**默认值**: 10

## alarm_component

**参数说明**: 在对告警做上报时，会进行告警抑制，即同一个实例的同一个告警项在alarm_report_interval（默认值为10s）内不做重复上报。在这种情况下设置用于处理告警内容的告警组件的位置。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: /opt/snas/bin/snas_cm_cmd

## table_skewness_warning_threshold

**参数说明**: 设置用于表倾斜告警的阈值。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0～1

**默认值**: 1

## table_skewness_warning_rows

**参数说明**: 设置用于表倾斜告警的行数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX

**默认值**: 100000
