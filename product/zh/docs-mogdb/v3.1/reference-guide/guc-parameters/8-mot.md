---
title: 内存表
summary: 内存表
author: Zhang Cuiping
date: 2021-04-20
---

# 内存表

介绍内存表的配置参数。

## enable_codegen_mot

**参数说明**: 设置是否启用原生LLVM Lite执行简单查询。如果当前平台上不支持原生LLVM，那么将使用伪LLVM。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔值

**默认值**: true

## force_pseudo_codegen_mot

**参数说明**: 设置是否强制伪LLVM Lite执行简单查询，即使当前平台上支持原生LLVM。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔值

**默认值**: true

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：即使将force_pseudo_codegen_mot设置为true，而当前平台不支持原生LLVM，那么仍然会使用伪LLVM。

## enable_codegen_mot_print

**参数说明**: 设置是否打印生成函数的IR字节码（如果使用伪 LLVM，则打印伪IR字节码）。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔值

**默认值**: true

## codegen_mot_limit

**参数说明**: 设置全局缓存计划源的数量限制以及每个会话的克隆计划。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: uint32

**默认值**: 100

## mot_allow_index_on_nullable_column

**参数说明**: 设置是否允许在内存表nullable列上创建索引。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔值

**默认值**: true

## mot_config_file

**参数说明**: 指定MOT的主配置文件。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: NULL
