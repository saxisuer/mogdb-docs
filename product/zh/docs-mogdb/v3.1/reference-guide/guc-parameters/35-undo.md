---
title: Undo
summary: Undo
author: Zhang Cuiping
date: 2021-11-08
---

# Undo

## undo_space_limit_size

**参数说明**: 用于控制undo强制回收阈值，达到阈值的80%启动强制回收，用户需要根据自己的业务情况，设置该值，可以通过先设置一个较大值，然后观察实际业务运行占用undo空间，再将该值调整为合理值。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，800M~16TB

**默认值**: 256GB

## undo_limit_size_per_transaction

**参数说明**: 用于控制单事务undo分配空间阈值，达到阈值时事务报错回滚。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，2M~16TB

**默认值**: 32GB
