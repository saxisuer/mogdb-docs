---
title: DROP SYNONYM
summary: DROP SYNONYM
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SYNONYM

## 功能描述

删除指定的SYNONYM对象。

## 注意事项

SYNONYM的所有者或者被授予了DROP ANY SEQUENCE权限的用户有权限执行DROP SYNONYM命令，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
DropSynonym ::= DROP SYNONYM [ IF EXISTS ] synonym_name [ CASCADE | RESTRICT ];
```

## 参数描述

- **IF EXISTS**

  如果指定的同义词不存在，则发出一个notice而不是抛出一个错误。

- **synonym_name**

  同义词名字，可以带模式名。

- **CASCADE | RESTRICT**

  - CASCADE：级联删除依赖同义词的对象（比如视图）。
  - RESTRICT：如果有依赖对象存在，则拒绝删除同义词。此选项为缺省值。

## 示例

请参考CREATE SYNONYM的示例。

## 相关链接

[ALTER SYNONYM](ALTER-SYNONYM.md)，[CREATE SYNONYM](CREATE-SYNONYM.md)
