---
title: CHECKPOINT
summary: CHECKPOINT
author: Zhang Cuiping
date: 2021-05-10
---

# CHECKPOINT

## 功能描述

检查点（CHECKPOINT）是一个事务日志中的点，所有数据文件都在该点被更新以反映日志中的信息，所有数据文件都将被刷新到磁盘。

设置事务日志检查点。预写式日志（WAL）缺省时在事务日志中每隔一段时间放置一个检查点。可以使用gs_guc命令设置相关运行时参数（checkpoint_segments和checkpoint_timeout）来调整这个原子化检查点的间隔。

## 注意事项

- 只有系统管理员可以调用CHECKPOINT。
- CHECKPOINT强制立即进行检查，而不是等到下一次调度时的检查点。

## 语法格式

```ebnf+diagram
Checkpoint ::= CHECKPOINT;
```

## 参数说明

无。

## 示例

```sql
--设置检查点。
MogDB=# CHECKPOINT;
```
