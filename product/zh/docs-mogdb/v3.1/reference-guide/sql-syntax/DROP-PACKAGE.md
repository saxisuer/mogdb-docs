---
title: DROP PACKAGE
summary: DROP PACKAGE
author: Zhang Cuiping
date: 2021-11-08
---

# DROP PACKAGE

## 功能描述

删除已存在的PACKAGE或者PACKAGE BODY。

## 注意事项

删除PACKAGE BODY后，PACKAGE内的存储过程及函数会同时失效。

## 语法格式

```ebnf+diagram
DropPackage ::= DROP PACKAGE [ IF EXISTS ] package_name;
```

```ebnf+diagram
DropPackage ::= DROP PACKAGE BODY [ IF EXISTS ] package_name;
```