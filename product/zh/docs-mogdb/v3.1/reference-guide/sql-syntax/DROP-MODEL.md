---
title: DROP MODEL
summary: DROP MODEL
author: Zhang Cuiping
date: 2021-11-01
---

# DROP MODEL

## 功能描述

删除一个已训练完成保存的模型对象。

## 注意事项

所删除模型可在系统表gs_model_warehouse中查看到。

## 语法格式

```ebnf+diagram
DropModel ::= DROP MODEL model_name;
```

## 参数说明

model_name

模型名称

取值范围: 字符串，需要符合标识符的命名规范。

## 相关链接

[CREATE MODEL](CREATE-MODEL.md)，[PREDICT BY](PREDICT-BY.md)
