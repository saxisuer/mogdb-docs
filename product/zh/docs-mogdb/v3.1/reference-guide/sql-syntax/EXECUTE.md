---
title: EXECUTE
summary: EXECUTE
author: Zhang Cuiping
date: 2021-05-18
---

# EXECUTE

## 功能描述

执行一个前面准备好的预备语句。因为一个预备语句只在会话的生命期里存在，那么预备语句必须是在当前会话的前些时候用PREPARE语句创建的。

## 注意事项

如果创建预备语句的PREPARE语句声明了一些参数，那么传递给EXECUTE语句的必须是一个兼容的参数集，否则就会生成一个错误。

## 语法格式

```ebnf+diagram
Execute ::= EXECUTE name [ ( parameter [, ...] ) ];
```

## 参数说明

- **name**

  要执行的预备语句的名称。

- **parameter**

  给预备语句的一个参数的具体数值。它必须是一个和生成与创建这个预备语句时指定参数的数据类型相兼容的值的表达式。

## 示例

```sql
--创建表reason。
MogDB=# CREATE TABLE reason (
    CD_DEMO_SK          INTEGER          NOT NULL,
    CD_GENDER           character(16)            ,
    CD_MARITAL_STATUS   character(100)
)
;

--插入数据。
MogDB=# INSERT INTO reason VALUES(51, 'AAAAAAAADDAAAAAA', 'reason 51');

--创建表reason_t1。
MogDB=# CREATE TABLE reason_t1 AS TABLE reason;

--为一个INSERT语句创建一个预备语句然后执行它。
MogDB=# PREPARE insert_reason(integer,character(16),character(100)) AS INSERT INTO reason_t1 VALUES($1,$2,$3);

MogDB=# EXECUTE insert_reason(52, 'AAAAAAAADDAAAAAA', 'reason 52');

--删除表reason和reason_t1。
MogDB=# DROP TABLE reason;
MogDB=# DROP TABLE reason_t1;
```
