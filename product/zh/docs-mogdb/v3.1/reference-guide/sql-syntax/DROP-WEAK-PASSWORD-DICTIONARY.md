---
title: DROP WEAK PASSWORD DICTIONARY
summary: DROP WEAK PASSWORD DICTIONARY
author: Zhang Cuiping
date: 2021-11-01
---

# DROP WEAK PASSWORD DICTIONARY

## 功能描述

清空gs_global_config中的所有弱口令。

## 注意事项

只有初始用户、系统管理员和安全管理员拥有权限执行本语法。

## 语法格式

```ebnf+diagram
DropWeakPasswordDictionary ::= DROP WEAK PASSWORD DICTIONARY;
```

## 参数说明

无。

## 示例

参见[CREATE WEAK PASSWORD DICTIONARY](CREATE-WEAK-PASSWORD-DICTIONARY.md)的示例。

## 相关链接

[CREATE WEAK PASSWORD DICTIONARY](CREATE-WEAK-PASSWORD-DICTIONARY.md)