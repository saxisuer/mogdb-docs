---
title: CREATE MASKING POLICY
summary: CREATE MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE MASKING POLICY

## 功能描述

创建脱敏策略。

## 注意事项

只有poladmin、sysadmin或初始用户能执行此操作。

需要开启安全策略开关，即设置GUC参数enable_security_policy=on，脱敏策略才可以生效。具体设置请参考《安全加固指南》中“数据库配置&gt;数据库安全管理策略&gt;数据动态脱敏”章节。

## 语法格式

```ebnf+diagram
CreateMaskingPolicy ::= CREATE MASKING POLICY policy_name masking_clause[, ...]* policy_filter [ENABLE | DISABLE];
```

- masking_clause：

  ```ebnf+diagram
  masking_clause ::= masking_function ON LABEL(label_name[, ...]*)
  ```

- masking_function：

  maskall不是预置函数，硬编码在代码中，不支持\\df展示。

  预置时脱敏方式如下：

  ```ebnf+diagram
  masking_function ::= maskall | randommasking | creditcardmasking | basicemailmasking | fullemailmasking | shufflemasking | alldigitsmasking | regexpmasking
  ```

- policy_filter:

  ```ebnf+diagram
  policy_filter ::= FILTER ON FILTER_TYPE(filter_value [,...]*)[,...]*
  ```

- FILTER_TYPE:

  ```ebnf+diagram
  filter_type ::= IP | APP | ROLES
  ```

## 参数说明

- **policy_name**

  审计策略名称，需要唯一，不可重复。

  取值范围: 字符串，要符合标识符的命名规范。

- **label_name**

  资源标签名称。

- **masking_clause**

  指出使用何种脱敏函数对被label_name标签标记的数据库资源进行脱敏，支持用schema.function的方式指定脱敏函数。

- **policy_filter**

  指出该脱敏策略对何种身份的用户生效，若为空表示对所用用户生效。

- **FILTER_TYPE**

  描述策略过滤的条件类型，包括IP | APP | ROLES。

- **filter_value**

  指具体过滤信息内容，例如具体的IP，具体的APP名称，具体的用户名。

- **ENABLE|DISABLE**

  可以打开或关闭脱敏策略。若不指定ENABLE|DISABLE，语句默认为ENABLE。

## 示例

```sql
--创建dev_mask和bob_mask用户。
MogDB=# CREATE USER dev_mask PASSWORD 'dev@1234';
MogDB=# CREATE USER bob_mask PASSWORD 'bob@1234';

--创建一个表tb_for_masking
MogDB=# CREATE TABLE tb_for_masking(col1 text, col2 text, col3 text);

--创建资源标签标记敏感列col1
MogDB=# CREATE RESOURCE LABEL mask_lb1 ADD COLUMN(tb_for_masking.col1);

--创建资源标签标记敏感列col2
MogDB=# CREATE RESOURCE LABEL mask_lb2 ADD COLUMN(tb_for_masking.col2);

--对访问敏感列col1的操作创建脱敏策略
MogDB=# CREATE MASKING POLICY maskpol1 maskall ON LABEL(mask_lb1);

--创建仅对用户dev_mask和bob_mask,客户端工具为psql和gsql，IP地址为'10.20.30.40', '127.0.0.0/24'场景下生效的脱敏策略。
MogDB=# CREATE MASKING POLICY maskpol2 randommasking ON LABEL(mask_lb2) FILTER ON ROLES(dev_mask, bob_mask), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24');
```

## 相关链接

[ALTER MASKING POLICY](ALTER-MASKING-POLICY.md)，[DROP MASKING POLICY](DROP-MASKING-POLICY.md)
