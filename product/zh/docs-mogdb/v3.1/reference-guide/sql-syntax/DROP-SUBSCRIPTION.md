---
title: DROP SUBSCRIPTION
summary: DROP SUBSCRIPTION
author: Guo Huan
date: 2022-05-16
---

# DROP SUBSCRIPTION

## **功能描述**

删除数据库实例中的一个订阅。

## **注意事项**

- 只有系统管理员才可以删除订阅。
- 如果该待删除订阅与复制槽相关联，就不能在事务块内部执行DROP SUBSCRIPTION。

## **语法格式**

```ebnf+diagram
DropSubscription ::= DROP SUBSCRIPTION [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## 参数说明

- **name**

  要删除的订阅的名称。

- **CASCADE|RESTRICT**

  当前这些关键词没有任何作用，因为订阅没有依赖关系。

## 示例

请参见[CREATE SUBSCRIPTION](CREATE-SUBSCRIPTION.md)中的示例。

## 相关链接

[ALTER SUBSCRIPTION](ALTER-SUBSCRIPTION.md)、[CREATE SUBSCRIPTION](CREATE-SUBSCRIPTION.md)
