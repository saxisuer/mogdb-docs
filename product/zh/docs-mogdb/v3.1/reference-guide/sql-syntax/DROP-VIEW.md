---
title: DROP VIEW
summary: DROP VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# DROP VIEW

## 功能描述

数据库中强制删除已有的视图。

## 注意事项

视图的所有者或者被授予了视图DROP权限的用户或拥有DROP ANY TABLE权限的用户，有权限执行DROP VIEW的命令，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
DropView ::= DROP VIEW [ IF EXISTS ] view_name [, ...] [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的视图不存在，则发出一个notice而不是抛出一个错误。

- **view_name**

  要删除的视图名称。

  取值范围: 已存在的视图。

- **CASCADE | RESTRICT**

  - CASCADE：级联删除依赖此视图的对象（比如其他视图）。
  - RESTRICT：如果有依赖对象存在，则拒绝删除此视图。此选项为缺省值。

## 示例

请参见[CREATE VIEW](CREATE-VIEW.md)的示例。

## 相关链接

[ALTER VIEW](ALTER-VIEW.md)，[CREATE VIEW](CREATE-VIEW.md)
