---
title: ALTER MASKING POLICY
summary: ALTER MASKING POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER MASKING POLICY

## 功能描述

修改脱敏策略。

## 注意事项

- 只有poladmin，sysadmin或初始用户才能执行此操作。
- 需要打开enable_security_policy开关，脱敏策略才可以生效。

## 语法格式

- 修改策略描述：

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name COMMENTS policy_comments;
  ```

- 修改脱敏方式：

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name [ADD | REMOVE | MODIFY] masking_actions[, ...]*;
  ```

  其中masking_action:

  ```ebnf+diagram
  masking_action ::= masking_function ON LABEL(label_name[, ...]*)
  ```

- 修改脱敏策略生效场景：

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name MODIFY(FILTER ON FILTER_TYPE(filter_value[, ...]*)[, ...]*);
  ```

- 移除脱敏策略生效场景，使策略对所用场景生效：

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name DROP FILTER;
  ```

- 修改脱敏策略开启/关闭：

  ```ebnf+diagram
  AlterMaskingPolicy ::= ALTER MASKING POLICY policy_name [ENABLE | DISABLE];
  ```

## 参数说明

- **policy_name**

  脱敏策略名称，需要唯一，不可重复。

  取值范围: 字符串，要符合标识符的命名规范。

- **policy_comments**

  需要为脱敏策略添加或修改的描述信息。

- **masking_function**

  指的是预置的八种脱敏方式或者用户自定义的函数，支持模式。

  maskall不是预置函数，硬编码在代码中，不支持\\df展示。

  预置时脱敏方式如下：

  ```sql
  maskall | randommasking | creditcardmasking | basicemailmasking | fullemailmasking | shufflemasking | alldigitsmasking | regexpmasking
  ```

- **label_name**

  资源标签名称。

- **FILTER_TYPE**

  指定脱敏策略的过滤信息，过滤类型包括：IP、ROLES、APP。

- **filter_value**

  指具体过滤信息内容，例如具体的IP，具体的APP名称，具体的用户名。

- **ENABLE|DISABLE**

  可以打开或关闭脱敏策略。若不指定ENABLE|DISABLE，语句默认为ENABLE。

## 示例

```sql
--创建dev_mask和bob_mask用户。
MogDB=# CREATE USER dev_mask PASSWORD 'dev@1234';
MogDB=# CREATE USER bob_mask PASSWORD 'bob@1234';

--创建一个表tb_for_masking
MogDB=# CREATE TABLE tb_for_masking(col1 text, col2 text, col3 text);

--创建资源标签标记敏感列col1
MogDB=# CREATE RESOURCE LABEL mask_lb1 ADD COLUMN(tb_for_masking.col1);

--创建资源标签标记敏感列col2
MogDB=# CREATE RESOURCE LABEL mask_lb2 ADD COLUMN(tb_for_masking.col2);

--对访问敏感列col1的操作创建脱敏策略
MogDB=# CREATE MASKING POLICY maskpol1 maskall ON LABEL(mask_lb1);

--为脱敏策略maskpol1添加描述
MogDB=# ALTER MASKING POLICY maskpol1 COMMENTS 'masking policy for tb_for_masking.col1';

--修改脱敏策略maskpol1，新增一项脱敏方式
MogDB=# ALTER MASKING POLICY maskpol1 ADD randommasking ON LABEL(mask_lb2);

--修改脱敏策略maskpol1，移除一项脱敏方式
MogDB=# ALTER MASKING POLICY maskpol1 REMOVE randommasking ON LABEL(mask_lb2);

--修改脱敏策略maskpol1，修改一项脱敏方式
MogDB=# ALTER MASKING POLICY maskpol1 MODIFY randommasking ON LABEL(mask_lb1);

--修改脱敏策略maskpol1使之仅对用户dev_mask和bob_mask,客户端工具为psql和gsql，IP地址为'10.20.30.40', '127.0.0.0/24'场景生效。
MogDB=# ALTER MASKING POLICY maskpol1 MODIFY (FILTER ON ROLES(dev_mask, bob_mask), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24'));

--修改脱敏策略maskpol1，使之对所有用户场景生效
MogDB=# ALTER MASKING POLICY maskpol1 DROP FILTER;

--禁用脱敏策略maskpol1
MogDB=# ALTER MASKING POLICY maskpol1 DISABLE;
```

## 相关链接

[CREATE MASKING POLICY](CREATE-MASKING-POLICY.md)，[DROP MASKING POLICY](DROP-MASKING-POLICY.md)。
