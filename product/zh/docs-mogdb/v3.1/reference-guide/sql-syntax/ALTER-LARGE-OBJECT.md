---
title: ALTER LARGE OBJECT
summary: ALTER LARGE OBJECT
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER LARGE OBJECT

## 功能描述

ALTER LARGE OBJECT用于更改一个large object的定义。它的唯一的功能是分配一个新的所有者。

## 注意事项

使用ALTER LARGE OBJECT必须是系统管理员或者是其所有者。

## 语法格式

```ebnf+diagram
AlterLargeObject ::= ALTER LARGE OBJECT large_object_oid
    OWNER TO new_owner;
```

## 参数说明

- **large_object_oid**

  要被变large object的OID 。

  取值范围: 已存在的大对象名。

- **OWNER TO new_owner**

  large object新的所有者。

  取值范围: 已存在的用户名/角色名。

## 示例

无。
