---
title: ALTER PUBLICATION
summary: ALTER PUBLICATION
author: Guo Huan
date: 2022-05-16
---

# ALTER PUBLICATION

## 功能描述

更改发布PUBLICATION的属性。

## 注意事项

发布的属主和系统管理员才能执行ALTER PUBLICATION。新所有者角色的直接或间接成员才可以改变所有者。新的所有者必须在当前数据库上拥有CREATE权限。此外，FOR ALL TABLES发布的新所有者必须是系统管理员。但是，系统管理员可以在避开这些限制的情况下更改发布的所有权。

## 语法格式

- 用指定的表替换当前发布的表。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name SET TABLE table_name [, ...]
  ```

- 从发布中添加一个或多个表。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name ADD TABLE table_name [, ...]
  ```

- 从发布中删除一个或多个表。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name DROP TABLE table_name [, ...]
  ```

- 改变在CREATE PUBLICATION中指定的所有发布属性，未提及的属性保留其之前的设置。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name SET ( publication_parameter [= value] [, ... ] )
  ```

- 更改发布的所有者。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name OWNER TO { new_owner | CURRENT_USER | SESSION_USER }
  ```

- 更改发布的名称。

  ```ebnf+diagram
  AlterPublication ::= ALTER PUBLICATION name RENAME TO new_name
  ```

## 参数说明

- **name**

待修改的发布的名称。

- **table_name**

现有表的名称。

- **SET ( publication_parameter [= value] [, ...] )。**

该子句修改最初由CREATE PUBLICATION设置的发布参数。

- **new_owner**

发布的新所有者的用户名。

- **new_name**

发布的新名称。

## 示例

详情请参见[CREATE PUBLICATION](CREATE-PUBLICATION.md)中的示例。

## 相关链接

[CREATE PUBLICATION](CREATE-PUBLICATION.md)、[DROP PUBLICATION](DROP-PUBLICATION.md)
