---
title: DROP GLOBAL CONFIGURATION
summary: DROP GLOBAL CONFIGURATION
author: Guo Huan
date: 2022-05-16
---

# DROP GLOBAL CONFIGURATION

## 功能描述

删除系统表gs_global_config中的参数值。

## 注意事项

- 仅支持数据库初始用户运行此命令。
- 不支持删除关键字为weak_password。

## 语法格式

```ebnf+diagram
DropGlobalConfiguration ::= DROP GLOBAL CONFIGURATION argname [, ...];
```

## 参数说明

参数名称是gs_global_config中已经存在的参数，删除不存在的参数将报错。
