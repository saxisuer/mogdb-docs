---
title: SET
summary: SET
author: Zhang Cuiping
date: 2021-05-18
---

# SET

## 功能描述

用于修改运行时配置参数。

## 注意事项

大多数运行时参数都可以用SET在运行时设置，但有些则在服务运行过程中或会话开始之后不能修改。

## 语法格式

- 设置所处的时区。

  ```ebnf+diagram
  Set ::= SET [ SESSION | LOCAL ] TIME ZONE { timezone | LOCAL | DEFAULT };
  ```

- 设置所属的模式。

  ```ebnf+diagram
  Set ::= SET [ SESSION | LOCAL ]
      {CURRENT_SCHEMA { TO | = } { schema | DEFAULT }
      | SCHEMA 'schema'};
  ```

- 设置客户端编码集。

  ```ebnf+diagram
  Set ::= SET [ SESSION | LOCAL ] NAMES encoding_name;
  ```

- 设置XML的解析方式。

  ```ebnf+diagram
  Set ::= SET [ SESSION | LOCAL ] XML OPTION { DOCUMENT | CONTENT };
  ```

- 设置其他运行时参数。

  ```ebnf+diagram
  Set ::= SET [ LOCAL | SESSION ]
      { {config_parameter { { TO | = } { value | DEFAULT }
                          | FROM CURRENT }}};
  ```

- 设置运行时的参数。

  ```ebnf+diagram
  Set ::= SET { GLOBAL | '@@GLOBAL.'}
      { {config_parameter = { expr | DEFAULT }}};
  ```

  ```ebnf+diagram
  Set ::= SET [ SESSION | '@@SESSION.' | '@@']
    { {config_parameter = { expr | DEFAULT }}};
  ```

- 设置自定义变量

  ```
  Set ::= SET @var_name := expr [, @var_name := expr] ...
  ```
  
  ```
  Set ::= SET @var_name = expr [, @var_name = expr] ...
  ```
  
## 参数说明

- **SESSION**

  声明的参数只对当前会话起作用。如果SESSION和LOCAL都没出现，则SESSION为缺省值。

  如果在事务中执行了此命令，命令的产生影响将在事务回滚之后消失。如果该事务已提交，影响将持续到会话的结束，除非被另外一个SET命令重置参数。

- **LOCAL**

  声明的参数只在当前事务中有效。在COMMIT或ROLLBACK之后，会话级别的设置将再次生效。

  不论事务是否提交，此命令的影响只持续到当前事务结束。一个特例是：在一个事务里面，即有SET命令，又有SET LOCAL命令，且SET LOCAL在SET后面，则在事务结束之前，SET LOCAL命令会起作用，但事务提交之后，则是SET命令会生效。

- **TIME ZONE timezone**

  用于指定当前会话的本地时区。

  取值范围: 有效的本地时区。该选项对应的运行时参数名称为TimeZone，DEFAULT缺省值为PRC。

- **CURRENT_SCHEMA**

  **schema**

  CURRENT_SCHEMA用于指定当前的模式。

  取值范围: 已存在模式名称。如果模式名不存在，会导致CURRENT_SCHEMA值为空。

- **SCHEMA schema**

  同CURRENT_SCHEMA。此处的schema是个字符串。

  例如：set schema 'public';

- **NAMES encoding_name**

  用于设置客户端的字符编码。等价于set client_encoding to encoding_name。

  取值范围: 有效的字符编码。该选项对应的运行时参数名称为client_encoding，默认编码为UTF8。

- **XML OPTION option**

  用于设置XML的解析方式。

  取值范围: CONTENT（缺省）、DOCUMENT

- **config_parameter**

  可设置的运行时参数的名称。可用的运行时参数可以使用SHOW ALL命令查看。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：部分通过SHOW ALL查看的参数不能通过SET设置。如max_datanodes。

- **value**

  config_parameter的新值。可以声明为字符串常量、标识符、数字，或者逗号分隔的列表。DEFAULT用于把这些参数设置为它们的缺省值。

- **GLOBAL | @@GLOBAL.**
  
  声明的参数生效范围为postmaster、sighup、backend，可通过pg_settings系统视图的context字段确定。设置参数范围和生效方式与ALTER SYSTEM SET 语法相同。支持config\_parameter赋值为表达式。
  
- **SESSION | @@SESSION. | @@**

  声明的参数生效方式为superuser、user，可通过pg_settings系统视图的context字段确定，如果没有出现GLOBAL /SESSION，则SESSION为缺省值。支持config\_parameter赋值为表达式。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**：
  >
  > 1. SET SESSION/GLOBAL 语法只有在B模式下（sql_compatibility = B）支持，并且GUC参数enable_set_variable_b_format打开的场景下才支持（enable_set_variable_b_format = on)。
  > 2. 使用@@config\_parameter进行操作符运算时，尽量使用空格隔开。比如set @config\_parameter1=@config\_parameter1 \* 2; 命令中，会将=@当做操作符，可将其修改为set @config\_parameter1= @config\_parameter1 \* 2。

- **var_name**

  自定义变量名。变量名只能由数字、字母、下划线（_），点（.）、$组成，如果使用单引号、双引号等引用是，则可以使用其他字符，如'var_name'，"var_name"，\`var_name\`。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**： 
  >
  > 1. SET自定义用户变量的只有在B模式下（sql_compatibility = B）支持，并且GUC参数enable_set_variable_b_format打开的场景下才支持（enable_set_variable_b_format = on）。
  > 2. 自定义变量只会存储整型，浮点型，字符串，位串和NULL。对于BOOLEAN，INT1，INT2，INT4，INT8类型会转为INT8类型；FLOAT4，FLOAT8，NUMBERIC会转化为FLOAT8进行存储（需要注意浮点型可能会有精度丢失）；BIT类型以BIT存储，VARBIT类型以VARBIT存储；NULL值以NULL存储；其他类型若可转化为字符串，则转为TEXT存储。
  > 3. 使用@var_name进行操作符运算时，尽量使用空格隔开。比如set @v1=@v2+1;命令中，会将=@当做操作符，可将其修改为set @v1= @v2+1。
  > 4. 当sql_compatibility = B && enable_set_variable_b_format = on时，对于MogDB原始的@ expr，请参考[数字操作符](../functions-and-operators/7-mathematical-functions-and-operators.md)，@需要与expr有空格，否则会将其解析成用户变量。
  > 5. 未初始化的变量值未NULL。
  > 6. Prepare语句中用户自定义变量存储的字符串只支持select/insert/update/delete/merge语法。
  > 7. 对于连续赋值的场景，只支持@var_name1 := @var_name2 := ... := expr和@var_name1 = @var_name2 := ... := expr，等号（=）只有放在首位才表示赋值，其他位置表示比较操作符。

- expr

  表达式，支持可直接或间接转为整型，浮点型，字符串，位串和NULL的表达式。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**注意**：
  >
  > 字符串表达式中避免包含口令等敏感信息的函数，如加解密类函数gs_encrypt，gs_decrypt等，防止敏感信息泄露。

## 示例

```sql
-- 设置模式搜索路径。
MogDB=# SET search_path TO tpcds, public;

-- 把日期时间风格设置为传统的 POSTGRES 风格(日在月前)。
MogDB=# SET datestyle TO postgres，dym;

-- set自定义用户变量的功能
b=# show sql_compatibility;
 sql_compatibility
-------------------
 B
(1 row)

b=# show enable_set_variable_b_format;
 enable_set_variable_b_format
------------------------------
 on
(1 row)

b=# set @v1 := 1, @v2 := 1.1, @v3 := true, @v4 := 'dasda', @v5 := x'41';
SET
b=# select @v1, @v2, @v3, @v4, @v5, @v6, @v7;
 @v1 | @v2 | @v3 |  @v4  |   @v5    | @v6 | @v7
-----+-----+-----+-------+----------+-----+-----
   1 | 1.1 |   1 | dasda | 01000001 |     |
(1 row)

-- prepare语法
b=# set @sql = 'select 1';
SET
b=# prepare stmt as @sql;
PREPARE
b=# execute stmt;
 ?column?
----------
        1
(1 row)

-- Global变量设置
set global most_available_sync = t;
set @@global.most_available_sync = t;

-- Session变量设置
MogDB=# set @@codegen_cost_threshold = 10000;
MogDB=# set @@session.codegen_cost_threshold = @@codegen_cost_threshold * 2;
```

## 相关链接

[RESET](RESET.md)，[SHOW](SHOW.md)
