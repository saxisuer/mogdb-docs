---
title: CREATE RESOURCE LABEL
summary: CREATE RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE RESOURCE LABEL

## 功能描述

创建资源标签。

## 注意事项

只有poladmin、sysadmin或初始用户能正常执行此操作。

## 语法格式

```ebnf+diagram
CreateResourceLabel ::= CREATE RESOURCE LABEL [IF NOT EXISTS] label_name ADD label_item_list[, ...]*;
```

- label_item_list：

  ```ebnf+diagram
  label_item_list ::= resource_type(resource_path[, ...]*)
  ```

- resource_type：

  ```ebnf+diagram
  resource_type ::= TABLE | COLUMN | SCHEMA | VIEW | FUNCTION
  ```

## 参数说明

- **label_name**

  资源标签名称，创建时要求不能与已有标签重名。

  取值范围: 字符串，要符合标识符的命名规范。

- **resource_type**

  指的是要标记的数据库资源类型。

- **resource_path**

  指的是描述具体的数据库资源的路径。

## 示例

```sql
--创建一个表tb_for_label
MogDB=# CREATE TABLE tb_for_label(col1 text, col2 text, col3 text);

--创建一个模式schema_for_label
MogDB=# CREATE SCHEMA schema_for_label;

--创建一个视图view_for_label
MogDB=# CREATE VIEW view_for_label AS SELECT 1;

--创建一个函数func_for_label
MogDB=# CREATE FUNCTION func_for_label RETURNS TEXT AS $$ SELECT col1 FROM tb_for_label; $$ LANGUAGE SQL;

--基于表创建资源标签
MogDB=# CREATE RESOURCE LABEL IF NOT EXISTS table_label add TABLE(public.tb_for_label);

--基于列创建资源标签
MogDB=# CREATE RESOURCE LABEL IF NOT EXISTS column_label add COLUMN(public.tb_for_label.col1);

--基于模式创建资源标签
MogDB=# CREATE RESOURCE LABEL IF NOT EXISTS schema_label add SCHEMA(schema_for_label);

--基于视图创建资源标签
MogDB=# CREATE RESOURCE LABEL IF NOT EXISTS view_label add VIEW(view_for_label);

--基于函数创建资源标签
MogDB=# CREATE RESOURCE LABEL IF NOT EXISTS func_label add FUNCTION(func_for_label);
```

## 相关链接

[ALTER RESOURCE LABEL](ALTER-RESOURCE-LABEL.md)，[DROP RESOURCE LABEL](DROP-RESOURCE-LABEL.md)
