---
title: ALTER RESOURCE LABEL
summary: ALTER RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER RESOURCE LABEL

## 功能描述

修改资源标签。

## 注意事项

只有poladmin, sysadmin或初始用户才能执行此操作。

## 语法格式

```ebnf+diagram
AlterResourceLabel ::= ALTER RESOURCE LABEL label_name (ADD|REMOVE)
  label_item_list[, ...]*;
```

- label_item_list：

  ```ebnf+diagram
  label_item_list ::= resource_type(resource_path[, ...]*)
  ```

- resource_type：

  ```ebnf+diagram
  label_item_list ::= TABLE | COLUMN | SCHEMA | VIEW | FUNCTION
  ```

## 参数说明

- **label_name**

  资源标签名称。

  取值范围: 字符串，要符合标识符的命名规范。

- **resource_type**

  指的是要标记的数据库资源类型。

- **resource_path**

  指的是描述具体的数据库资源的路径。

## 示例

```sql
--创建基本表table_for_label。
MogDB=# CREATE TABLE table_for_label(col1 int, col2 text);

--创建资源标签table_label。
MogDB=# CREATE RESOURCE LABEL table_label ADD COLUMN(table_for_label.col1);

--将col2添加至资源标签table_label中
MogDB=# ALTER RESOURCE LABEL table_label ADD COLUMN(table_for_label.col2)

--将资源标签table_label中的一项移除
MogDB=# ALTER RESOURCE LABEL table_label REMOVE COLUMN(table_for_label.col1);
```

## 相关链接

[CREATE RESOURCE LABEL](CREATE-RESOURCE-LABEL.md)，[DROP RESOURCE LABEL](DROP-RESOURCE-LABEL.md)。
