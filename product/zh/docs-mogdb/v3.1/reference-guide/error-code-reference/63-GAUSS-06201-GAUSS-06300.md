---
title: GAUSS-06201 - GAUSS-06300
summary: GAUSS-06201 - GAUSS-06300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-06201 - GAUSS-06300

<br/>

## GAUSS-06201 - GAUSS-06210

<br/>

GAUSS-06201: "failed on assertion in %s line %d : %s"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06202: "unexpected rtekind when set relation path list: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06203: "All orientations are not covered."

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06204: "bad levelsup for CTE '%s' when set cte pathlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06205: "could not find CTE '%s' when set cte pathlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06206: "could not find plan for CTE '%s' when set cte pathlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06207: "bad levelsup for CTE '%s' when set worktable pathlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06208: "could not find plan for CTE '%s' when set worktable pathlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06209: "unrecognized joinlist node type when build access paths by joinlist: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06210: "unrecognized node type when pushdown recurse through setOperations tree: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06211 - GAUSS-06220

<br/>

GAUSS-06211: "wrong number of tlist entries when compare a subquery targetlist datatypes"

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06212: "unrecognized node type when recurse push qual through setOperations tree: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06213: "unrecognized node type when create partiterator path: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06214: "Unsupported Using Index FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06215: "unrecognized node type of a bitmap index path when get pages: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06216: "unrecognized node type when extract cost and selectivity from a bitmap tree node: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06217: "left and right pathkeys do not match in mergejoin when initlize cost"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06218: "cannot handle unplanned sub-select when costing quals"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06219: "unrecognized join type when calculate joinrel size estimate: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06220: "unrecognized join type when match unsorted outer: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06221 - GAUSS-06230

<br/>

GAUSS-06221: "Null value error for building partitionwise join"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06222: "could not find opfamilies for equality operator %u when make pathkey from sortinfo"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06223: "operator %u is not a valid ordering operator when make pathkey from sortinfo"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06224: "volatile EquivalenceClass has no sortref when convert subquery pathkeys"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06225: "too few pathkeys for mergeclauses when make inner pathkeys for merge"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06226: "outer pathkeys do not match mergeclause when make inner pathkeys for merge"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06227: "unrecognized join type when make a join rel: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06228: "Unsupported node type %s to check need stream setup for recursive union"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06229: "encounters invalid varno"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06230: "invalid column number %d for table "

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06231 - GAUSS-06240

<br/>

GAUSS-06231: "There is no exist vararrno with 0"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06232: "tle can not be found from targetlist"

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06233: "No data node information for table: %s"

SQLSTATE: XX006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06234: "No data node found for u_sess->pgxc_cxt.gc_fdw_current_idx: %d, u_sess->pgxc_cxt.gc_fdw_max_idx: %d"

SQLSTATE: XX006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06235: "Unsupported Table Sample FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06236: "Unsupported Index Scan FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06237: "Unsupported Bitmap Heap Scan FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06238: "Unsupported Bitmap And FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06239: "Unsupported Bitmap OR FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06240: "Unsupported Bitmap Index Scan FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06134: "'%s' is not a valid EOL string, EOL string must not be empty"

SQLSTATE: 无

错误原因: 自定义换行符不能为空。

解决办法: 请检查指定的自定义换行符，确保自定义换行符不为空。

GAUSS-06135: "'%s' is not a valid EOL string, EOL string must not exceed the maximum length (10 bytes)"

SQLSTATE: 无

错误原因: 自定义换行符超出最大长度（10字节）。

解决办法: 请检查指定的自定义换行符的长度，确保自定义换行符的长度不超出最大长度。

GAUSS-06136: "COPY delimiter cannot contain user-define EOL string"

SQLSTATE: 无

错误原因: 分隔符不能包含自定义换行符。

解决办法: 请检查分隔符和换行符，确保分隔符不包含自定义换行符。

GAUSS-06137: "COPY null representation cannot contain user-define EOL string"

SQLSTATE: 无

错误原因: NULL值不能包含自定义换行符。

解决办法: 请检查NULL值，确保NULL值不包含自定义换行符。

GAUSS-06138: "EOL string '%s' cannot contain any characters in'%s'"

SQLSTATE: 无

错误原因: 自定义换行符不能包含任何字符。

解决办法: 请检查自定义换行符，确保不包含任何字符。

GAUSS-06139: "EOL specification can not be used with non-text format using COPY FROM or READ ONLY foreign table"

SQLSTATE: 无

错误原因: 自定义换行符不能用于非TEXT格式的copy导入或只读外表。

解决办法: 请检查copy导入或只读外表的设置的文件格式，确保为TEXT格式。

GAUSS-06140: "EOL specification can not be used with non-text format using COPY TO or WRITE ONLY foreign table except 0x0D0A and 0x0A"

SQLSTATE: 无

错误原因: 自定义换行符不能用于非TEXT格式的copy导出或只写外表，但是允许自定义换行符0x0D0A和0x0A。

解决办法: 请检查copy导出或只写外表的设置的文件格式，确保为TEXT格式。

<br/>

## GAUSS-06241 - GAUSS-06250

<br/>

GAUSS-06241: "unrecognized node type: %d when modify worktable wtParam."

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06242: "recursive_union_plan can not be NULL"

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06243: "non-LATERAL parameter required by subquery"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06244: "unexpected type of subquery parameter"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06245: "Unsupported Direct Scan FOR TIMESERIES."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06246: "fail to find TargetEntry referenced by SortGroupClause"

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06247: "Unsupported FOR UPDATE/SHARE system table."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06248: "Unsupported FOR UPDATE/SHARE in non shippable plan."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06249: "Only Scan operator have BucketInfo attribute"

SQLSTATE: XX006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06250: "Unsupported FOR UPDATE/SHARE at non-top-level query in stream plan."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06251 - GAUSS-06260

<br/>

GAUSS-06251: "Unsupported FOR UPDATE/SHARE with limit in stream plan."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06252: "Unsupported FOR UPDATE/SHARE multiple table in stream plan."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06253: "can not find var with varno = %u and varattno = %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06254: "qry_tle should not be null"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06255: "Partition column can't be updated in current version"

SQLSTATE: 42P10

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06256: "Build subPlan failed.. "

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06257: "convert to SubPlan failed. "

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06258: "Fail to process sublinks mutator."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06259: "Fail to get sort group clause."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06260: "failed to find relation %d in joinlist when remove relation from joinlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06261 - GAUSS-06270

<br/>

GAUSS-06261: "unrecognized joinlist node type when remove relation from joinlist: %d"

SQLSTATE: XX006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06262: "cheapest_total_path should not exist para_info"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06263: "unrecognized node type when add base_rels to query: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06264: "unrecognized node type when add vars to targetlist: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06265: "expected Var or PlaceHolderVar, others unsupported. "

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06266: "unrecognized join type in one level processing of deconstruct jointree: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06267: "unrecognized node type in one level of deconstruct jointree: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06268: "cannot cope with variable-free clause when distribute restrictinfo to rels"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06269: "relid must not be less than zero."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06270: "too many range table entries when set plan reference."

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06271 - GAUSS-06280

<br/>

GAUSS-06271: "targetlist of stream node with plan_node_id %d should be equal to its child's targetlist"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06272: "unrecognized node type in set plan refs: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06273: "Could not find the Aggref node when setting agg plan refernece."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06274: "Could not find the Aggref node when setting agg plan reference."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06275: "mark_stream_unsupport."

SQLSTATE: 0A100

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06276: "Fail to generate subquery plan."

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06277: "unrecognized node type when process qual condition: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06278: "unrecognized node type when get base relation indexes: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06279: "OBS and HDFS foreign table can NOT be in the same plan."

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06280: "pl_size should not be zero"

SQLSTATE: 22012

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06281 - GAUSS-06290

<br/>

GAUSS-06281: "Failed to get the runtime info from the compute pool."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06282: "version is not compatible between local cluster and the compute pool"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06283: "There is no optional index path for index column: %s.Please check for potential performance problem."

SQLSTATE: 01000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06284: "MERGE INTO on replicated table does not yet support using distributed tables."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06285: "unrecognized bool test type: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06286: "RangeTblRef not found."

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06287: "Valid rel not found. "

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06288: "No Such Relation"

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06289: "subquery's setOperations tree should not be NULL in pull_up_simple_union_all"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06290: "jointree in subquery could not be NULL"

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06291 - GAUSS-06300

<br/>

GAUSS-06291: "Join range table do not have system column."

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06292: "Invalid join alias var"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06293: "Invalid agg param which used in a join clause"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06294: "Column should NOT be in JOIN clause."

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06295: "unsupported command type: %d."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06296: "infinite recursion detected, please check the row level security policies for relation '%s'"

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06297: "could not convert table '%s' to a view because it has row level security enabled"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06298: "could not convert table '%s' to a view because it has row level security policies"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06299: "Fail to find base rel."

SQLSTATE: 22004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06300: "predcate_classify returned a bogus value"

SQLSTATE: XX008

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
