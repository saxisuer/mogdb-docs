---
title: GAUSS-01801 - GAUSS-01900
summary: GAUSS-01801 - GAUSS-01900
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01801 - GAUSS-01900

<br/>

## GAUSS-01801 - GAUSS-01810

<br/>

GAUSS-01801: "cannot drop objects owned by %s because they are required by the database system"

SQLSTATE: 2BP01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01802: "unexpected dependency type"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01803: "cannot reassign ownership of objects owned by %s because they are required by the database system"

SQLSTATE: 2BP01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01804: "unexpected shared pin"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01805: "unexpected classid %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01806: "smgr_redo: unknown op code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01811 - GAUSS-01820

<br/>

GAUSS-01811: "could not access file '%s': %m"

SQLSTATE: XX000

错误原因: 需要将该动态库拷贝到其他物理节点，但是查询不到该文件，报错。

解决办法: 检查该动态文件是否真实存在并核对其地址和相关链接信息。

GAUSS-01815: "The name of dynamic library is too long"

SQLSTATE: 42622

错误原因: 使用的动态库名称过长。

解决办法: 当前支持的最大长度是1000，请确保不要超过该长度。

GAUSS-01816: "allParameterTypes is not a 1-D Oid array"

SQLSTATE: XX000

错误原因: 创建procedure时，出现异常: 传入参数有误。

解决办法: 请检查传入参数allParameterTypes的有效性。

GAUSS-01817: "parameterModes is not a 1-D char array"

SQLSTATE: XX000

错误原因: 创建procedure时，出现异常: 传入参数有误。

解决办法: 请检查传入参数parameterModes的有效性。

GAUSS-01818: "cannot determine result data type"

SQLSTATE: 42P13

错误原因: 由于没有输入项导致无法确认返回的数据类型。

解决办法: 请确认输入的参数类型是正确的，这样才可确定返回结果类型。

GAUSS-01819: "unsafe use of pseudo-type 'internal'"

SQLSTATE: 42P13

错误原因: 由于没有相关的internal输入项，使得internal的返回类型存在风险。

解决办法: 需要确认存在相关的internal输入项。

GAUSS-01820: "'%s' is already an attribute of type %s"

SQLSTATE: 42701

错误原因: 在创建函数时的复合类型与已有的属性类型相重名，因此报错。

解决办法: 在创建函数时使用的任何名称都应与已有的属性名称不相同。

<br/>

## GAUSS-01821 - GAUSS-01830

<br/>

GAUSS-01821: "variadic parameter must be last"

SQLSTATE: XX000

错误原因: 创建procedure时，variadic参数出现异常。

解决办法: 仅最后一个参数可以是variadic。

GAUSS-01822: "variadic parameter is not an array"

SQLSTATE: XX000

错误原因: 创建procedure时，variadic参数出现异常（不是数组）。

解决办法: 请检查variadic参数。

GAUSS-01823: "invalid parameter mode '%c'"

SQLSTATE: XX000

错误原因: 创建procedure时，出现无效的PROARGMODE参数。

解决办法: 请检查PROARGMODE参数。

GAUSS-01824: "more than one function '%s' already exist, please drop function first"

SQLSTATE: 42723

错误原因: 在创建函数时发现已有一个名称相同的函数存在。

解决办法: 在创建函数前，请确认当前模式中没有同名函数。

GAUSS-01825: "function '%s' already exists with same argument types"

SQLSTATE: 42723

错误原因: 同名函数已经存在，且要创建的函数和原函数参数相同，因此不能重载。

解决办法: 检查现在创建的函数是否为重新创建。

GAUSS-01826: "cannot change return type of existing function"

SQLSTATE: 42P13

错误原因: 无法改变已经创建的函数的返回类型。

解决办法: 检查新建函数的返回类型或者drop之前创建的函数。

GAUSS-01827: "cannot change name of input parameter '%s'"

SQLSTATE: 42P13

错误原因: 创建函数时不可改变输入参数的类型。

解决办法: 检查函数内部是否有改变函数参数的操作，并将当前的函数丢弃。

GAUSS-01828: "function '%s' is an aggregate function"

SQLSTATE: 42809

错误原因: 内部错误，创建的函数为一个聚集函数，不可以改变其状态。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01829: "function '%s' is not an aggregate function"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01830: "function '%s' is a window function"

SQLSTATE: 42809

错误原因: 创建的函数为一个窗口函数，不可以改变其状态。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01831 - GAUSS-01840

<br/>

GAUSS-01831: "function '%s' is not a window function"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01832: "null prosrc"

SQLSTATE: XX005

错误原因: 获取自定义函数时出错。

解决办法: 请检查函数名是否正确。

GAUSS-01833: "there is no built-in function named '%s'"

SQLSTATE: 42883

错误原因: 根据给定的函数名称找不到对应的build-in function。

解决办法: 检查pg_proc表中是否存在对应的函数名称。

GAUSS-01834: "null prosrc for C function %u"

SQLSTATE: XX005

错误原因: 获取自定义C函数时出错。

解决办法: 请检查函数名是否正确。

GAUSS-01835: "null probin for C function %u"

SQLSTATE: XX005

错误原因: 获取内置C函数时出错。

解决办法: 请检查函数名是否正确。

GAUSS-01836: "SQL functions cannot return type %s"

SQLSTATE: 42P13

错误原因: SQL函数当前不支持返回format_type_be(proc->prorettype)型的数据类型。

解决办法: 检查sql语句的返回并进行确认。

GAUSS-01837: "SQL functions cannot have arguments of type %s"

SQLSTATE: 42P13

错误原因: 函数当中不允许使用format_type_be(proc->proargtypes.values[i])类型的参数。

解决办法: 检查sql函数中参数的类型。

GAUSS-01838: "In XC, SQL functions cannot contain utility statements"

SQLSTATE: 42601

错误原因: sql函数中不可包含utility statements。

解决办法: 检查sql函数中的内容。

GAUSS-01840: "no conversion name supplied"

SQLSTATE: XX000

错误原因: 在创建conversion使，没有提供conversion name。

解决办法: 请检查conversion name是否为空。

<br/>

## GAUSS-01841 - GAUSS-01850

<br/>

GAUSS-01841: "conversion '%s' already exists"

SQLSTATE: 42710

错误原因: 当前创建的名为conname的conversion已经创建，无法创建同名的conversion。

解决办法: 检查需要创建的conversion是否存在与之相同名称的conversion。

GAUSS-01842: "default conversion for %s to %s already exists"

SQLSTATE: 42710

错误原因: 当前创建的由conforencoding到contoencoding得conversion已经存在，无法创建具有相同功能的conversion。

解决办法: 检查所需创建的conversion功能是否已经存在，无需创建具有相同功能的conversion。

GAUSS-01843: "could not find tuple for conversion %u"

SQLSTATE: XX000

错误原因: 从pg_conversion中删除tuple时出错: 未找到该tuple。

解决办法: 请检查tuple oid是否正确。

GAUSS-01844: "pgxc class relid invalid."

SQLSTATE: XX000

错误原因: 在创建pgxc_class时，出现无效的relid。

解决办法: 请检查传入参数relid的合法性。

GAUSS-01845: "pgxc_group should have at least one default node group"

SQLSTATE: XX000

错误原因: pgxc_group不应该是NULL。

解决办法: 请检查创建pgxc_class时，pgxc_group状态。

GAUSS-01847: "could not obtain lock on partition '%s'"

SQLSTATE: 55P03

错误原因: 无法获取锁用于当前的分区。

解决办法: 检查资源利用以及当前的分区状态。

GAUSS-01848: "partition '%s' does not exist"

SQLSTATE: 42P01

错误原因: 名为partitionName的分区并不存在。

解决办法: 检查对应于表格的分区名称，并进行核对。

GAUSS-01849: "the object with oid %u is not a partitioned object"

SQLSTATE: 42P17

错误原因: 当前的对象并非具有分区特性。

解决办法: 确认对应对象的属性，或检查是否按需求为其创建分区。

GAUSS-01850: "object with oid %u is not a partition object"

SQLSTATE: 42704

错误原因: 当前的对象并非含分区对象。

解决办法: 确认对应对象的属性，或检查是否按需求为其创建分区。

<br/>

## GAUSS-01851 - GAUSS-01860

<br/>

GAUSS-01851: "invalid input parameters when searching for local index under some index"

SQLSTATE: 42704

错误原因: 在检查索引OID有效性时，发现输入的参数是无效的。

解决办法: 检查输入参数的有效性。

GAUSS-01852: "%u is not a partitioned index"

SQLSTATE: 42704

错误原因: 参数OID对应的对象并非是一个分区索引。

解决办法: 检查在执行流程中传入的参数的实际属性，可通过系统表来检查。

GAUSS-01853: "%u not found in pg_class"

SQLSTATE: 42704

错误原因: 无法在pg_class系统表中找到参数OID对应的对象。

解决办法: 确认想要查找的对象的OID并确认是否已经完成analyze操作并正确导入系统表。

GAUSS-01854: "not found local index %u in pg_partition"

SQLSTATE: 42704

错误原因: 在pg_partition系统表中无法查找到对应的local index。

解决办法: 检查local index参数的有效性并与实际创建时所创建的属性进行核对。

GAUSS-01855: "none is the son local index of index %u"

SQLSTATE: 42704

错误原因: 在partitionedIndexid对应的索引中没有找到局部索引。

解决办法: 检查所需查找的局部索引的正确性。

GAUSS-01859: "cache lookup failed for index partition %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01860: "more than one item satisfied parttype is %c, parentOid is %u"

SQLSTATE: 0A000

错误原因: 在一个表中找到了两个同样类型的分区。

解决办法: 在创建分区时应保障不存在相同的分区。

<br/>

## GAUSS-01861 - GAUSS-01870

<br/>

GAUSS-01861: "'%s' does not require a toast table"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01862: "shared tables cannot be toasted after gs_initdb"

SQLSTATE: 55000

错误原因: 当初始化数据库完成后，不允许对共享内存表做toast操作。

解决办法: 请检查当前状态，若已完成初始化，则不允许toast共享内存表。

GAUSS-01863: "cache lookup failed for relation or partition %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01864: "grant options can only be granted to roles"

SQLSTATE: 0LP01

错误原因: 只能对私有用户进行授权，无法对公有用户进行授权。

解决办法: 检查用户的权限，并提交管理员受理。

GAUSS-01865: "unrecognized object kind: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01866: "unrecognized GrantStmt.targtype: %d"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01867: "unrecognized GrantStmt.objtype: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01868: "column privileges are only valid for relations"

SQLSTATE: 0LP01

错误原因: 只有表对象才可拥有列存特性。

解决办法: 检查当前对象的特性，或确认是否是对表对象赋予列存特性。

GAUSS-01869: "AccessPriv node must specify privilege or columns"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01870: "default privileges cannot be set for columns"

SQLSTATE: 0LP01

错误原因: 属性列无默认访问权限。

解决办法: 检查对象本身。

<br/>

## GAUSS-01871 - GAUSS-01880

<br/>

GAUSS-01871: "AccessPriv node must specify privilege"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01872: "could not find tuple for default ACL %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01873: "unexpected default ACL type: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01875: "column number out of range"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01876: "'%s' is an index"

SQLSTATE: 42809

错误原因: 无法对一个索引对象进行赋权限。

解决办法: 检查需要赋予权限的list中的对象情况，确保对象需全为表对象。

GAUSS-01877: "invalid privilege type USAGE for table"

SQLSTATE: 0LP01

错误原因: USAGE这种权限类型只支持sequences，不支持non-sequences场景。

解决办法: 检查表的类型。

GAUSS-01878: "invalid privilege type %s for column"

SQLSTATE: 0LP01

错误原因: 该权限类型不适用于列属性。

解决办法: 检查上下文中关于权限类型的说明以及对象类型的说明。

GAUSS-01879: "language '%s' is not trusted"

SQLSTATE: 42809

错误原因: 系统不信任的语言格式类型。

解决办法: 确认当前系统信任的语言格式。

<br/>

## GAUSS-01881 - GAUSS-01890

<br/>

GAUSS-01881: "cache lookup failed for tablespace %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01882: "cannot set privileges of array types"

SQLSTATE: 0LP01

错误原因: 不能为集合类型设定权限。

解决办法: 在设定权限前确认对象的类型。

GAUSS-01883: "unrecognized privilege type '%s'"

SQLSTATE: 42601

错误原因: 不认识的权限类型。

解决办法: 检查上下文看类型定义是否正确或被修改。

GAUSS-01886: "permission denied for column '%s' of relation '%s'"

SQLSTATE: 42501

错误原因: 无法对表的属性列进行相关权限操作。

解决办法: 检查权限操作定义本身。

GAUSS-01887: "role with OID %u does not exist"

SQLSTATE: 42704

错误原因: 对应用户不存在。

解决办法: 检查用户列表，确认用户是否存在。

GAUSS-01888: "unrecognized objkind: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01889: "attribute %d of relation with OID %u does not exist"

SQLSTATE: 42703

错误原因: 表中不存在该属性的访问权限列表。

解决办法: 检查表定义时候的权限列表情况。

GAUSS-01890: "relation with OID %u does not exist"

SQLSTATE: 42P01

错误原因: 在出口程序中检查用户的访问权限时，发现对应表格不存在。

解决办法: 检查pg_class系统表中OID所对应的表格是否存在。

<br/>

## GAUSS-01891 - GAUSS-01900

<br/>

GAUSS-01891: "database with OID %u does not exist"

SQLSTATE: 3D000

错误原因: 在检查用户的访问权限时，发现不存在对应的数据库。

解决办法: 检查pg_database系统表中关于数据库的状态是否正常。

GAUSS-01892: "function with OID %u does not exist"

SQLSTATE: 42883

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的函数。

解决办法: 检查pg_proc系统表中是否存在该函数定义。

GAUSS-01893: "language with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的语言格式。

解决办法: 检查pg_language系统表中是否存在该language定义。

GAUSS-01894: "schema with OID %u does not exist"

SQLSTATE: 3F000

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的schema空间。

解决办法: 检查pg_namespace系统表中是否存在该命名空间定义。

GAUSS-01895: "tablespace with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的表空间。

解决办法: 检查pg_tablespace系统表中是否存在该表空间定义。

GAUSS-01896: "foreign-data wrapper with OID %u does not exist"

SQLSTATE: XX000

错误原因: foreign-data wrapper不存在。

解决办法: 检查是否已经创建了对应的foreign-data wrapper。

GAUSS-01897: "foreign server with OID %u does not exist"

SQLSTATE: XX000

错误原因: foreign server不存在。

解决办法: 检查是否已经创建了对应的foreign server。

GAUSS-01898: "type with OID %u does not exist"

SQLSTATE: XX000

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的数据类型。

解决办法: 检查pg_type系统表中所有的type的类型定义。

GAUSS-01899: "type with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的数据类型。

解决办法: 检查pg_type系统表中所有的type的类型定义。

GAUSS-01900: "operator with OID %u does not exist"

SQLSTATE: 42883

错误原因: 在检查用户权限时，发现不存在对应的操作类型。

解决办法: 检查pg_operator系统表中operator的定义。
