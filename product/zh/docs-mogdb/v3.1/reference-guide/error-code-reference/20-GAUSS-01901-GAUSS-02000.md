---
title: GAUSS-01901 - GAUSS-02000
summary: GAUSS-01901 - GAUSS-02000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01901 - GAUSS-02000

<br/>

## GAUSS-01901 - GAUSS-01910

<br/>

GAUSS-01901: "language with OID %u does not exist"

SQLSTATE: 42883

错误原因: 在出口程序中检查用户的访问权限时，发现不存在对应的语言格式。

解决办法: 检查pg_language系统表中是否存在该language定义。

GAUSS-01904: "text search dictionary with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在检查用户权限时，发现不存在对应的文件搜索路径。

解决办法: 检查pg_ts_dict系统表中所有的文件搜索路径。

GAUSS-01905: "text search configuration with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在检查用户权限时，发现不存在对应的文件搜索构图。

解决办法: 检查pg_ts_config系统表中所有的文件搜索构图。

GAUSS-01906: "collation with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在检查用户权限时，发现不存在对应的字符集。

解决办法: 检查pg_collation系统表中所有的字符集。

GAUSS-01907: "conversion with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在检查用户权限时，发现不存在对应的conversion。

解决办法: 检查pg_collation系统表中所有的conversion。

GAUSS-01908: "extension with OID %u does not exist"

SQLSTATE: 42704

错误原因: 在检查用户权限时，发现不存在对应的extension。

解决办法: 检查pg_extension系统表中所有的extension。

GAUSS-01909: "relation '%s' has relchecks = 0"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01911 - GAUSS-01920

<br/>

GAUSS-01911: "constraint '%s' for domain %s already exists"

SQLSTATE: 42710

错误原因: 在当前表对象中，重命名时想要使用的constraint名称已经存在。

解决办法: 请确认当前域中的已经存在的constraint名称。

GAUSS-01912: "table '%s' has multiple constraints named '%s'"

SQLSTATE: 42710

错误原因: 当前表中至少有两个相同名称的constraint。

解决办法: 请确保对于同一张表，其constraint名称不相重合。

GAUSS-01913: "constraint '%s' for table '%s' does not exist"

SQLSTATE: 42704

错误原因: 当前表中不存在该constraint。

解决办法: 请确认当前表中存在该constraint，或核查该表上的constraint。

GAUSS-01914: "domain '%s' has multiple constraints named '%s'"

SQLSTATE: 42710

错误原因: 当前域中至少有两个相同名称的constraint。

解决办法: 请确保在同一域中，其constraint名称不相重合。

GAUSS-01915: "constraint '%s' for domain '%s' does not exist"

SQLSTATE: 42704

错误原因: 当前域中不存在该constraint。

解决办法: 请确认该域所持有的constraint。

GAUSS-01916: "null conkey for constraint %u"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01917: "conkey is not a 1-D smallint array"

SQLSTATE: 42804

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01918: "no aggregate name supplied"

SQLSTATE: XX000

错误原因: Aggregate未提供name。

解决办法: 请提供Aggregate name。

GAUSS-01919: "aggregate must have a transition function"

SQLSTATE: XX000

错误原因: Aggregate必须有一个transition function。

解决办法: 请提供transition function。

GAUSS-01920: "cannot determine transition data type"

SQLSTATE: 42P13

错误原因: 在创建聚集函数时对于复合类型无法确定其变换类型。

解决办法: 对于复合类型应制定其转换类型。

<br/>

## GAUSS-01921 - GAUSS-01930

<br/>

GAUSS-01921: "return type of transition function %s is not %s"

SQLSTATE: 42804

错误原因: 依据transfn确定的返回类型与声明的返回类型不相匹配。

解决办法: 对于复合类型必须确保其返回类型与声明的一致性。

GAUSS-01922: "must not omit initial value when transition function is strict and transition type is not compatible with input type"

SQLSTATE: 42P13

错误原因: 当转换函数是严格的，且转换类型与输入类型不一致时，不可忽略初始值的类型。

解决办法: 此时确保第一个值的类型必须与转换类型一致，或至少是二进制兼容的。

GAUSS-01923: "return type of collection function %s is not %s"

SQLSTATE: 42804

错误原因: collection函数的返回类型与预期的aggTreanType不同。

解决办法: 检查是否调用了正确的collection函数。

GAUSS-01924: "cannot determine result data type"

SQLSTATE: 42804

错误原因: 由于没有输入项导致无法确认返回的数据类型。

解决办法: 请确认输入的参数类型是正确的，这样才可确定返回结果类型。

GAUSS-01925: "sort operator can only be specified for single-argument aggregates"

SQLSTATE: 42P13

错误原因: 排序算子只能出现在单参数的聚集函数中。

解决办法: 请确认该聚集函数的参数个数，并确认是否需要调用排序算子。

GAUSS-01926: "function %s returns a set"

SQLSTATE: 42804

错误原因:  聚集函数只能返回单个值不可返回一个集合。

解决办法: 请确认聚集函数的返回情况。

GAUSS-01927: "function %s requires run-time type coercion"

SQLSTATE: 42804

错误原因: 函数要求在执行过程中进行类型强转，但是在nodeAgg.c中并没有处理该场景。

解决办法: 确保在执行过程中不存在类型转换。

GAUSS-01928: "cannot drop %s because %s requires it"

SQLSTATE: 2BP01

错误原因: 由于其他对象依赖他，无法删除这个对象。

解决办法: 解除这种依赖或者删除依赖对象。

GAUSS-01929: "incorrect use of PIN dependency with %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01930: "unrecognized dependency type '%c' for %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01931 - GAUSS-01940

<br/>

GAUSS-01931: "cannot drop %s because other objects depend on it"

SQLSTATE: 2BP01

错误原因: 由于其他对象依赖他，无法删除这个对象。

解决办法: 解除这种依赖或者删除依赖对象。

GAUSS-01932: "cannot drop desired object(s) because other objects depend on them"

SQLSTATE: 2BP01

错误原因: 由于其他对象依赖这些对象，无法删除这些对象。

解决办法: 使用drop cascade来级联删除依赖对象。

GAUSS-01933: "invalid varlevelsup %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01934: "invalid varno %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01935: "already-planned subqueries not supported"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01936: "invalid resultRelation %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01937: "invalid objectSubId 0 for object class %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01938: "could not find tuple for rule %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01939: "no namespace name supplied"

SQLSTATE: XX000

错误原因: 创建namespace时，未提供namespace的名称。

解决办法: 请提供namespace的名称。

GAUSS-01940: "primary keys cannot be expressions"

SQLSTATE: 0A000

错误原因: 主键不允许是表达式。

解决办法: 请重新选择主键。

<br/>

## GAUSS-01941 - GAUSS-01950

<br/>

GAUSS-01942: "too few entries in colnames list"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01943: "must index at least one column"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01944: "user-defined indexes on system catalog tables are not supported"

SQLSTATE: 0A000

错误原因: 无法在系统表上创建用户定义索引。

解决办法: 不要在系统表上创建索引。

GAUSS-01945: "concurrent index creation on system catalog tables is not supported"

SQLSTATE: 0A000

错误原因: 并发在系统表上创建索引不支持。

解决办法: 不要并发在系统表上创建索引。

GAUSS-01946: "shared indexes cannot be created after gs_initdb"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01947: "constraint must be PRIMARY, UNIQUE or EXCLUDE"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01948: "constraints cannot have index expressions"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01949: "DROP INDEX CONCURRENTLY must be first action in transaction"

SQLSTATE: 0A000

错误原因: 并发删除索引必须是事务块的第一条语句。

解决办法: 调整事务块语句顺序，放置并发删除索引语句为第一句。

GAUSS-01950: "invalid indnatts %d for index %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01951 - GAUSS-01960

<br/>

GAUSS-01951: "could not find tuple for partition %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01952: "cannot reindex temporary tables of other sessions"

SQLSTATE: 0A000

错误原因: 不能重组其他session临时表上索引。

解决办法: 不要重组其他session上的临时表上索引。

GAUSS-01953: "cannot reindex while reindexing"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01954: "cache lookup failed for partitioned index %u"

SQLSTATE: XX000

错误原因: 系统表缓存查找分区索引信息失败。

解决办法: 检查分区索引是否存在。

GAUSS-01955: "Memory alloc failed for indexInfo"

SQLSTATE: XX000

错误原因: 生成IndexInfo结构失败。

解决办法: 检查有足够的内存。

GAUSS-01956: "cache lookup failed for index %u"

SQLSTATE: XX000

错误原因: 系统表缓存查找索引信息失败。

解决办法: 检查索引名称是否正确。

GAUSS-01957: "mergingBtreeIndexes, zero or less than 2 or greater than 4 source index relations"

SQLSTATE: XX000

错误原因:  实现索引合并时，输入了一个索引或者超过了最大值(300)个索引作为源索引。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01958: "mergingBtreeIndexes, only btree indexes can be merged"

SQLSTATE: XX000

错误原因: 只能合并B树索引。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01959: "target merging index '%s' already contains data"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01961 - GAUSS-01970

<br/>

GAUSS-01961: "invalid enum label '%s'"

SQLSTATE: 42602

错误原因: 无效的枚举类型。

解决办法: 请检查SQL语句中是否包含所提示的无效枚举类型。

GAUSS-01962: "'%s' is not an existing enum label"

SQLSTATE: 22023

错误原因: 无效的枚举类型。

解决办法: 请检查SQL语句中是否包含所提示的无效枚举类型。

GAUSS-01963: "ALTER TYPE ADD BEFORE/AFTER is incompatible with binary upgrade"

SQLSTATE: 22023

错误原因: 修改枚举类型与二进制升级不兼容。

解决办法: 请检查SQL语句中是否包含对于枚举类型的修改。

GAUSS-01965: "alignment '%c' is invalid for passed-by-value type of size %d"

SQLSTATE: 42P17

错误原因: 对于PASS-BY-VALUE类型无效的长度。

解决办法: 检查创建类型的定义。

GAUSS-01966: "internal size %d is invalid for passed-by-value type"

SQLSTATE: 42P17

错误原因: 对于PASS-BY-VALUE类型无效的长度。

解决办法: 检查创建类型的定义。

GAUSS-01967: "alignment '%c' is invalid for variable-length type"

SQLSTATE: 42P17

错误原因: 变长类型不合法的alignment。

解决办法: 检查创建类型的定义。

GAUSS-01969: "cannot assign new OID to existing shell type"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01970: "could not form array type name for type '%s'"

SQLSTATE: 42710

错误原因: 数组类型名称超长。

解决办法: 请检查当前创建类型的定义语法中填写的数组类型长度是否超过最大值63。

<br/>

## GAUSS-01971 - GAUSS-01980

<br/>

GAUSS-01973: "could not stat file or directory '%s': %m"

SQLSTATE: XX000

错误原因: 程序运行过程中需要访问的文件或目录不存在。

解决办法: 手动创建该文件目录，或重新初始化数据目录。

GAUSS-01977: "could not open file '%s': %m"

SQLSTATE: XX000

错误原因: 打开某个文件失败，可能原因是文件不存在，权限不对，或文件损坏。

解决办法: 可根据错误信息分析具体原因，如果文件不存在，需要创建对应文件；权限不对则修改权限；文件损坏则修复文件系统或磁盘。

<br/>

## GAUSS-01981 - GAUSS-01990

<br/>

GAUSS-01983: "cannot continue WAL streaming, recovery has already ended"

SQLSTATE: XX000

错误原因: 主机上不用启动walreceiver，如果启动了报错退出。

解决办法: 正常报错，不用处理。

GAUSS-01984: "invalid standby handshake message type %d"

SQLSTATE: 08P01

错误原因: 主机收到备机无效的握手消息类型。

解决办法: 检查主备节点网络环境，排查是否存在丢包现象。如果没有，请联系技术支持工程师提供技术支持。

GAUSS-01986: "invalid standby message type '%c'"

SQLSTATE: 08P01

错误原因: 主机收到备机无效的消息类型。

解决办法: 检查主备节点网络环境，排查是否存在丢包现象。如果没有，请联系技术支持工程师提供技术支持。

GAUSS-01987: "number of requested standby connections exceeds max_wal_senders (currently %d)"

SQLSTATE: 53300

错误原因: 主机上备机连接数达到上限，可能原因是备机尝试连接主机了很多次，每次连接都失败了，也可能原因是主机上max_wal_senders设置过小，特别是在备机执行build命令的情况下，因为此时主机要启动两个发送线程来完成数据和日志的发送。

解决办法: 查看之前的连接是否已关闭，如果没有则人工关掉。查看max_wal_senders设置是否偏小，如果偏小则手工调大。
