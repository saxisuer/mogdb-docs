---
title: GAUSS-03501 - GAUSS-03600
summary: GAUSS-03501 - GAUSS-03600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03501 - GAUSS-03600

<br/>

## GAUSS-03501 - GAUSS-03510

<br/>

GAUSS-03501: "value too long for restore point (maximum %d characters)"

SQLSTATE: 22023

错误原因: 恢复点的名称长度超过了63个字节。

解决办法: 请缩短恢复点的名称，重新创建。

GAUSS-03502: "could not parse transaction log location '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03504: "recovery is not in progress"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03505: "invalid input syntax for transaction log location: '%s'"

SQLSTATE: 22P02

错误原因: 输入参数错误。

解决办法: 请检查函数参数，重新执行。

GAUSS-03507: "could not access status of transaction %lu"

SQLSTATE: XX000

错误原因: 获取文件状态失败。

解决办法: 请检查文件路径和权限是否正确。

GAUSS-03508: "unrecognized SimpleLru error 错误原因:  %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03509: "cannot make new WAL entries during recovery"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03511 - GAUSS-03520

<br/>

GAUSS-03513: "xlog write request %X/%X is past end of log %X/%X"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03514: "xlog flush request %X/%X is not satisfied - flushed only to %X/%X"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03515: "not enough data in file '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03516: "InstallXLogFileSegment should not have failed"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03517: "invalid XLogFileRead source %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03518: "could not open transaction log directory '%s': %m"

SQLSTATE: XX000

错误原因: 打开文件夹pg_xlog失败。

解决办法: 请检查文件夹路径和权限是否正确。

GAUSS-03519: "required WAL directory '%s' does not exist"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03520: "could not create missing directory '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03521 - GAUSS-03530

<br/>

GAUSS-03523: "syntax error in history file: %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03524: "invalid data in history file: %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03525: "invalid data in history file '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03527: "sizeof(ControlFileData) is larger than PG_CONTROL_SIZE; fix either one"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03529: "could not write to control file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03530: "could not fsync control file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03531 - GAUSS-03540

<br/>

GAUSS-03531: "could not close control file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03532: "could not open control file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03533: "could not read from control file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03534: "database files are incompatible with server"

SQLSTATE: XX000

错误原因: 数据库文件和软件版本不匹配。

解决办法: 请使用正确的软件版本重建数据库。

GAUSS-03535: "incorrect checksum in control file"

SQLSTATE: XX000

错误原因: pg_control文件内容异常。

解决办法: 请使用备份数据恢复或者重建数据库。

GAUSS-03536: "could not write bootstrap transaction log file: %m"

SQLSTATE: XX000

错误原因: 磁盘空间不足。

解决办法: 请检查磁盘空间是否充足，重启数据库。

GAUSS-03537: "could not fsync bootstrap transaction log file: %m"

SQLSTATE: XX000

错误原因: 文件权限不正确。

解决办法: 请检查文件权限是否正确，重启数据库。

GAUSS-03538: "could not close bootstrap transaction log file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03539: "could not open recovery command file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03540: "recovery_target_timeline is not a valid number: '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03541 - GAUSS-03550

<br/>

GAUSS-03541: "recovery_target_xid is not a valid number: '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03542: "recovery_target_name is too long (maximum %d characters)"

SQLSTATE: 22023

错误原因: 配置恢复目标名称太长。

解决办法: 请缩短配置恢复目标名称后，重新恢复数据库。

GAUSS-03543: "unrecognized recovery parameter '%s'"

SQLSTATE: XX000

错误原因: 配置恢复参数不支持。

解决办法: 请正确配置恢复参数后，重新恢复数据库。

GAUSS-03544: "recovery command file '%s' must specify restore_command when standby mode is not enabled"

SQLSTATE: XX000

错误原因: 配置恢复命令为空。

解决办法: 请正确配置恢复命令后，重新恢复数据库。

GAUSS-03545: "recovery target timeline %u does not exist"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03546: "hot standby is not possible because %s = %d is a lower setting than on the master server (its value was %d)"

SQLSTATE: 22023

错误原因: 备机的参数配置比主机小。

解决办法: 请修改配置，重启数据库。

GAUSS-03548: "control file contains invalid data"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03549: "requested timeline %u is not a child of database system timeline %u"

SQLSTATE: XX000

错误原因: 备份的WAL日志文件与数据库不匹配。

解决办法: 请确保WAL日志文件与数据库一致后，重启恢复数据库。

GAUSS-03550: "could not find redo location referenced by checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03551 - GAUSS-03560

<br/>

GAUSS-03551: "could not locate required checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03552: "could not locate a valid checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03553: "invalid next transaction ID"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03554: "invalid redo in checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03555: "invalid redo record in shutdown checkpoint"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03556: "backup_label contains data inconsistent with control file"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03558: "requested recovery stop point is before consistent recovery point"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03559: "WAL ends before end of online backup"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03560: "WAL ends before consistent recovery point"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03561 - GAUSS-03570

<br/>

GAUSS-03562: "concurrent transaction log activity while database system is shutting down"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03563: "online backup was canceled, recovery cannot continue"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03564: "unexpected timeline ID %u (after %u) in checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03565: "unexpected timeline ID %u (should be %u) in checkpoint record"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03566: "unrecognized wal_sync_method: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03571 - GAUSS-03580

<br/>

GAUSS-03571: "WAL level not sufficient for making an online backup"

SQLSTATE: 55000

错误原因: 配置WAL级别不足。

解决办法: 配置WAL级别为archive或hot_standby，重启数据库后执行备份操作。

GAUSS-03572: "backup label too long (max %d bytes)"

SQLSTATE: 22023

错误原因: 备份路径名称超过限制。

解决办法: 修改配置路径名称，重新执行备份操作。

GAUSS-03573: "a backup is already in progress"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03574: "WAL generated with full_page_writes=off was replayed since last restartpoint"

SQLSTATE: 55000

错误原因: 配置项full_page_writes为off。

解决办法: 配置full_page_writes为on，在主机做CHECKPOINT后，再次执行备份。

GAUSS-03575: "could not write file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03576: "a backup is not in progress"

SQLSTATE: 55000

错误原因: 没有任何正在执行的备份供停止。

解决办法: SELECT pg_start_backup和SELECT pg_stop_backup是成对出现的。必须先执行了开始备份，才能执行关闭备份。请确认相关备份是否已开启。

GAUSS-03577: "invalid data in file '%s'"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03578: "the standby was promoted during online backup"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03579: "WAL generated with full_page_writes=off was replayed during online backup"

SQLSTATE: 55000

错误原因: 配置项full_page_writes为off。

解决办法: 请配置full_page_writes为on，在主机做CHECKPOINT后，再次执行备份。

GAUSS-03580: "invalid record offset at %X/%X."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03581 - GAUSS-03590

<br/>

GAUSS-03581: "not enough shared memory for pg_lsnxlogflushchk share memory"

SQLSTATE: 53200

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03582: "transaction identifier '%s' is too long"

SQLSTATE: 22023

错误原因: 事务标识参数太长。

解决办法: 请检查输入参数后，重新执行PREPARE TRANSACTION操作。

GAUSS-03583: "prepared transactions are disabled"

SQLSTATE: 55000

错误原因: 配置项max_prepared_transactions为0。

解决办法: 请配置max_prepared_transactions为非0值。

GAUSS-03584: "transaction identifier '%s' is already in use"

SQLSTATE: 42710

错误原因: 事务标识参数正在使用。

解决办法: 请检查输入参数，重新执行PREPARE TRANSACTION操作。

GAUSS-03585: "maximum number of prepared transactions reached"

SQLSTATE: 53200

错误原因: 配置项max_prepared_transactions值较小。

解决办法: 请配置max_prepared_transactions为合理值或提交回滚一些事务。

GAUSS-03586: "prepared transaction with identifier '%s' is busy"

SQLSTATE: 55000

错误原因: 事务标识参数正在使用。

解决办法: 请重新执行操作。

GAUSS-03587: "permission denied to finish prepared transaction"

SQLSTATE: 42501

错误原因: 权限不正确。

解决办法: 请由事务发起者或管理员结束已准备好的事务。

GAUSS-03588: "prepared transaction belongs to another database"

SQLSTATE: 0A000

错误原因: 连接的数据库不正确。

解决办法: 请检查和确认数据库的正确性后，重新连接。

GAUSS-03589: "prepared transaction with identifier '%s' does not exist"

SQLSTATE: 42704

错误原因: 事务标识参数不存在。

解决办法: 请检查输入参数，重新执行操作。

GAUSS-03590: "failed to find %p in GlobalTransaction array"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03591 - GAUSS-03600

<br/>

GAUSS-03591: "failed to find GlobalTransaction for xid %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03594: "could not write two-phase state file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03596: "could not close two-phase state file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03598: "could not recreate two-phase state file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03599: "could not fsync two-phase state file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
