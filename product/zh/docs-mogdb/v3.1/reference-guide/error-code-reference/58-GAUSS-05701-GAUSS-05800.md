---
title: GAUSS-05701 - GAUSS-05800
summary: GAUSS-05701 - GAUSS-05800
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05701 - GAUSS-05800

<br/>

## GAUSS-05701 - GAUSS-05710

<br/>

GAUSS-05701: "Failed to get decode DEK for transparent encryption. Failure content is [%s]."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05702: "Decode transparent_encrypted_string failed, please check it."

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05703: "Decrypt transparent_encrypted_string failed, please check it!"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05704: "Out of memory while getting transparent encryption iv."

SQLSTATE: 53200

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05705: "TDE is not supported."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05706: "The data type %u is not supported for bloom filter curently."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05707: "failed to init sigpool mutex: %m."

SQLSTATE: 55P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05708: "failed to lock sigpool mutex: %m."

SQLSTATE: 55P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05709: "failed to unlock sigpool mutex: %m."

SQLSTATE: 55P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05710: "log2m = %d is out of range, it should be in range %d to %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05711 - GAUSS-05720

<br/>

GAUSS-05711: "regwidth = %d is out of range, it should be in range %d to %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05712: "expthresh = %ld is out of range, it should be in range %d to %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05713: "sparseon should be %d or %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05714: "no such parameters of input"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05715: "hll_add_trans_normal outside transition context"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05716: "hll_union_collect_compressed outside transition context"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05717: "hll_union_trans_normal outside transition context"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05718: "hll_union_trans_compressed context"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05719: "Fail to build partitionmap for realtion'%s'."

SQLSTATE: 42P23

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05720: "Can not run transaction to remote nodes during recovery."

SQLSTATE: 25009

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05721 - GAUSS-05730

<br/>

GAUSS-05721: "could not find tuple with partition OID %u."

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05722: "Fail to build partitionmap for partitioned table '%s'."

SQLSTATE: 42P23

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05723: "Fail to build partitionmap for partitioned table '%u'."

SQLSTATE: 42P23

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05724: "Failed to set the default password for private key File: '%s'"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05725: "too many writing times(>%d):%d, write %d, errno %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05726: "%d: write %d, errno %d, detail:%s"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05727: "unrecognized error:%d, write %d, errno %d"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05728: "failed to initialize SSL library, detail:%s"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05729: "failed to create the SSL context, detail:%s"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05730: "invlid cert file directory"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05731 - GAUSS-05740

<br/>

GAUSS-05731: "failed to load the root CA Certificate %s(%s)"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05732: "failed to certificate the %s(%s) file in SSL context"

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05733: "stat cipher file '%s' failed, detail: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05734: "stat rand file '%s' failed, detail: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05735: "cipher file '%s' has group or world access;permissions should be u=rw (0600) or less with Error: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05736: "rand file '%s' has group or world access;permissions should be u=rw (0600) or less with Error: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05737: "Failed to create the SSL."

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05738: "Failed to set the socket(%d) for SSL"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05739: "Failed to certificate file '%s' in SSL object"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05740: "Failed to check the private key File."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05741 - GAUSS-05750

<br/>

GAUSS-05741: "SSL connect failed, code %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05742: "gz_open failed: errno %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05743: "gz_read failed: offset %ld, errno %d, returned size %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05744: "vfd_file_open failed: vfd(-1), errno %d, file '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05745: "vfd_file_open2 failed: vfd(-1), errno %d, file '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05746: "vfd_file_read2 failed: errno %d, returned size %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05747: "vfd_file_write2 failed: errno %d, returned size %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05748: "unzOpen2_64 failed: file '%s'"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05749: "unzGetGlobalInfo64 failed: file '%s', err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05750: "unzOpenCurrentFile failed: file '%s', err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05751 - GAUSS-05760

<br/>

GAUSS-05751: "unzCloseCurrentFile failed: err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05752: "unzReadCurrentFile failed: err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05753: "unzGoToNextFile failed: err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05754: "unzOpenCurrentFile failed: err %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05755: "invalid compute pool message subtype %d"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05756: "only coordinator could receive compute pool message type 'I'"

SQLSTATE: 42601

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05757: "only datanode could receive compute pool message type 'R'"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05758: "only coordinator could receive compute pool message type 'C'"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05759: "only coordinator could receive compute pool message type 'D'"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05760: "len is invalid[%d]"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05761 - GAUSS-05770

<br/>

GAUSS-05761: "Failed to get runtime info from the compute pool."

SQLSTATE: 08006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05762: "Failed to send request to ccn: %s for cp runtime info! 错误原因:  %s"

SQLSTATE: 57P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05763: "No CCN in cluster!"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05764: "Failed to send request to ccn: %s for dn list! dnnum: %d, 错误原因:  %s"

SQLSTATE: 57P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05765: "Failed to get dn list from CCN: %s, 错误原因:  %s"

SQLSTATE: 57P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05766: "Failed to send request to CCN."

SQLSTATE: 57P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05767: "invalid ccn index %d"

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05768: "invalid size [%lu]"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05769: "something wrong in CPmonitor thread, so reboot CPmonitor thread."

SQLSTATE: 57P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05770: "Failed to get the values of $GAUSSHOME"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05771 - GAUSS-05780

<br/>

GAUSS-05771: "Failed to open config file to connect compute pool. file path: %s"

SQLSTATE: 58P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05772: "Failed to get the size of the config file to connect compute pool. file path: %s"

SQLSTATE: 58P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05773: "Failed to get the data of the config file to connect compute pool. file path: %s"

SQLSTATE: 58P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05774: "'cpip', 'cpport', 'username', 'password', 'version', 'dnnum', 'pl' are needed to connect to the compute pool."

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05775: "pl should be greater than 0, pl is %d current."

SQLSTATE: XX001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05776: "no valid config info in cp_client.conf."

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05777: "get reload signal in %s"

SQLSTATE: D0011

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05778: "the config of node is changed."

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05779: "init transaction error, data nodes or coordinators num init failed"

SQLSTATE: D0014

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05780: "no the version of the compute pool is provided."

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05781 - GAUSS-05790

<br/>

GAUSS-05781: "The key string of 'R' request could not be NULL"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05782: "estimate_memory will out of MAX_INT, orign[%d], addMemory[%d]"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05783: "%s space is out of %s's %s space limit"

SQLSTATE: 57014

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05784: "the space used on DN has exceeded the sql use space limit (%d kB)."

SQLSTATE: 57014

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05785: "Remote Sender: Failed to send command to datanode"

SQLSTATE: 22003

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05786: "Normal user could not readjust other user space"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05787: "cannot open database"

SQLSTATE: 58000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05788: "user space is out of space limit"

SQLSTATE: 57014

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05789: "The memory usage of %s is out of control. it will exit!"

SQLSTATE: 0B000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05790: "role %u does not exist"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05791 - GAUSS-05800

<br/>

GAUSS-05791: "resource pool %u does not exist in htab."

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05792: "resource pool with control_group %s has been existed in the two-layer resource pool list "

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05793: "cannot create resource pool with class '%s', it has been existed in the normal resource pool list"

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05794: "mem_percent of two-layer resource pools cannot be 0"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05795: "memory percent value is beyond the available range."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05796: "resource pool information of '%s' is missing."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05797: "cannot alter control_group between different groups or alter to a different layer. "

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05798: "resource pool with control group '%s' already exists"

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05799: "cannot alter normal resource pool to the two-layer resource pool list."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05800: "mem_percent of two-layer resource pool cannot be altered to 0."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
