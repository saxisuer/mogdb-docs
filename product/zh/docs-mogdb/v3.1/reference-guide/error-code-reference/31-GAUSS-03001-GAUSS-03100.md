---
title: GAUSS-03001 - GAUSS-03100
summary: GAUSS-03001 - GAUSS-03100
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03001 - GAUSS-03100

<br/>

## GAUSS-03001 - GAUSS-03010

<br/>

GAUSS-03001: "smalldatetime out of range"

SQLSTATE: 22008

错误原因: 数值越界。

解决办法: 检查输入数据。

GAUSS-03002: "smalldatetime cannot be NaN"

SQLSTATE: 22008

错误原因: smalldatetime类型数据不能为无限值。

解决办法: 检查输入数据。

GAUSS-03003: "timestamp(%d) precision must be between %d and %d"

SQLSTATE: 22023

错误原因: timestamp的精度只能界于0到6之间。

解决办法: 检查输入的精度是否越界。

GAUSS-03004: "unexpected dtype %d while parsing timestamptz '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03005: "interval out of range"

SQLSTATE: 22008

错误原因: interval类型数据越界。

解决办法: 检查输入数据。

GAUSS-03006: "unexpected dtype %d while parsing interval '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03007: "could not convert interval to tm"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03008: "invalid INTERVAL type modifier"

SQLSTATE: 22023

错误原因: 非法的interval类型精度值。

解决办法: 检查类型精度值。

GAUSS-03009: "INTERVAL(%d) precision must not be negative"

SQLSTATE: 22023

错误原因: interval类型的精度值不能为负。

解决办法: 检查类型精度值。

GAUSS-03010: "invalid INTERVAL typmod: 0x%x"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03011 - GAUSS-03020

<br/>

GAUSS-03013: "invalid argument for EncodeSpecialTimestamp"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03014: "cannot subtract infinite timestamps"

SQLSTATE: 22008

错误原因: 不能减去无限值的timestamp值。

解决办法: 检查输入数据。

GAUSS-03015: "expected 2-element interval array"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03016: "timestamp units '%s' not supported"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03017: "timestamp units '%s' not recognized"

SQLSTATE: 22023

错误原因: timestamp没有时间单位。

解决办法: 检查输入参数。

GAUSS-03018: "timestamp with time zone units '%s' not supported"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03019: "timestamp with time zone units '%s' not recognized"

SQLSTATE: 22023

错误原因: 带timezone的timestamp没有时间单位。

解决办法: 检查输入参数。

GAUSS-03020: "interval units '%s' not supported"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03021 - GAUSS-03030

<br/>

GAUSS-03021: "interval units '%s' not recognized"

SQLSTATE: 22023

错误原因: interval类型没有时间单位。

解决办法: 检查输入参数。

GAUSS-03022: "could not convert to time zone '%s'"

SQLSTATE: 22023

错误原因: 无法转换到指定的time zone。

解决办法: 检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03023: "time zone '%s' not recognized"

SQLSTATE: 22023

错误原因: 需要转换的timezone不能识别。

解决办法: 检查输入参数。

GAUSS-03024: "interval time zone '%s' must not specify month"

SQLSTATE: 22023

错误原因: interval time zone类型不能指定月份。

解决办法: 检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03027: "the format is not correct!"

SQLSTATE: 0A000

错误原因: 日期的格式不正确。

解决办法: 请检查输入数据的格式是否正确。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03028: "the year is invalid"

SQLSTATE: 0A000

错误原因: 年份值非法。

解决办法: 请检查年份的位数是否为4。

GAUSS-03029: "the month is invalid!"

SQLSTATE: 0A000

错误原因: 月份值非法。

解决办法: 请检查月份的位数是否为2。

GAUSS-03030: "the format is not correct"

SQLSTATE: 0A000

错误原因: 时间字符串中有多于两个的分隔符，不合法。

解决办法: 请检查输入数据的合法性。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

<br/>

## GAUSS-03031 - GAUSS-03040

<br/>

GAUSS-03031: "the time is not correct!"

SQLSTATE: 0A000

错误原因: 时间字符串长度越界。

解决办法: 请检查输入数据的范围是否满足要求。

GAUSS-03032: "the hour is invalid!"

SQLSTATE: 0A000

错误原因: 小时值非法。

解决办法: 请检查小时的位数是否为2。

GAUSS-03033: "the minute is invalid!"

SQLSTATE: 0A000

错误原因: 分钟值非法。

解决办法: 请检查分钟的位数是否为2。

GAUSS-03034: "the year = %d is illegal"

SQLSTATE: 0A000

错误原因: 年份值非法。

解决办法: 请检查年份值是否在0-9999之间。

GAUSS-03035: "the day = %d for february in leap year is illegal"

SQLSTATE: 0A000

错误原因: 闰年的二月份日期非法。

解决办法: 检查日期值。

GAUSS-03036: "the day = %d for big month is illegal"

SQLSTATE: 0A000

错误原因: 大月份的日期非法。

解决办法: 检查日期值。

GAUSS-03037: "the day = %d for small month is illegal"

SQLSTATE: 0A000

错误原因: 小月份的日期非法。

解决办法: 检查日期值。

GAUSS-03038: "the day = %d for February in commen year is illegal"

SQLSTATE: 0A000

错误原因: 平年的二月份日期非法。

解决办法: 检查日期值。

GAUSS-03039: "the month = %d is illegal"

SQLSTATE: 0A000

错误原因: 月份值非法。

解决办法: 请检查月份值是否在1-12之间。

GAUSS-03040: "the hour = %d is illegal"

SQLSTATE: 0A000

错误原因: 小时值非法。

解决办法: 请检查小时值是否在0-24之间。

<br/>

## GAUSS-03041 - GAUSS-03050

<br/>

GAUSS-03041: "the minute = %d is illegal"

SQLSTATE: 0A000

错误原因: 分钟值非法。

解决办法: 请检查分钟值是否在0-59之间。

GAUSS-03042: "the second = %d is illegal"

SQLSTATE: 0A000

错误原因: 秒值非法。

解决办法: 请检查秒值是否在0-59之间。

GAUSS-03043: "encoding conversion from %s to ASCII not supported"

SQLSTATE: 0A000

错误原因: 不支持从当前编码格式转换为ascii。

解决办法: 请检查输入字符的编码格式。

GAUSS-03044: "too many points requested"

SQLSTATE: 54000

错误原因: 需要获取的点太多。

解决办法: 检查输入参数。

GAUSS-03045: "could not format 'path' value"

SQLSTATE: 22023

错误原因: 无法获取path信息。

解决办法: 检查输入参数。

GAUSS-03046: "invalid input syntax for type box: '%s'"

SQLSTATE: 22P02

错误原因: box类型数值非法。

解决办法: 检查输入参数。

GAUSS-03047: "invalid input syntax for type line: '%s'"

SQLSTATE: 22P02

错误原因: line类型数据非法。

解决办法: 检查输入参数。

GAUSS-03048: "type 'line' not yet implemented"

SQLSTATE: 0A000

错误原因: line类型不支持。

解决办法: 检查此版本是否支持line类型。

GAUSS-03049: "invalid input syntax for type path: '%s'"

SQLSTATE: 22P02

错误原因: path类型数据非法。

解决办法: 检查输入参数。

GAUSS-03050: "invalid number of points in external 'path' value"

SQLSTATE: 22P03

错误原因: path中的点数非法。

解决办法: 检查输入参数。

<br/>

## GAUSS-03051 - GAUSS-03060

<br/>

GAUSS-03051: "invalid input syntax for type point: '%s'"

SQLSTATE: 22P02

错误原因: point类型的数据非法。

解决办法: 检查输入参数。

GAUSS-03052: "invalid input syntax for type lseg: '%s'"

SQLSTATE: 22P02

错误原因: lseg类型的数据非法。

解决办法: 检查输入参数。

GAUSS-03053: "function 'dist_lb' not implemented"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03054: "function 'close_lb' not implemented"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03056: "invalid input syntax for type polygon: '%s'"

SQLSTATE: 22P02

错误原因: 输入的polygon类型值无法解析。

解决办法: 检查输入参数，应该以点对(a,b)格式使用","间隔的字符串输入。

GAUSS-03057: "invalid number of points in external 'polygon' value"

SQLSTATE: 22P03

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03058: "function 'poly_distance' not implemented"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03059: "function 'path_center' not implemented"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03060: "open path cannot be converted to polygon"

SQLSTATE: 22023

错误原因: 没有回路的path不能转换成多角形。

解决办法: 检查输入路径是否有回路。

<br/>

## GAUSS-03061 - GAUSS-03070

<br/>

GAUSS-03061: "invalid input syntax for type circle: '%s'"

SQLSTATE: 22P02

错误原因: circle类型值非法。

解决办法: 检查输入参数。

GAUSS-03062: "could not format 'circle' value"

SQLSTATE: 22023

错误原因: 使用输入的circle类型值无法构造circle类型数据。

解决办法: 无（不可能出现）。

GAUSS-03063: "invalid radius in external 'circle' value"

SQLSTATE: 22P03

错误原因: 输入的circle的半径小于0。

解决办法: 检查输入的circle值。

GAUSS-03064: "cannot convert circle with radius zero to polygon"

SQLSTATE: 0A000

错误原因: 不能将半径为0的圆转换为多角形。

解决办法: 检查输入的circle值。

GAUSS-03065: "must request at least 2 points"

SQLSTATE: 22023

错误原因: 输出至少包含两个点。

解决办法: 检查输入的需要点数。

GAUSS-03066: "cannot convert empty polygon to circle"

SQLSTATE: 22023

错误原因: 不能将没有点集的多角形转换为circle型值。

解决办法: 检查输入的多角形值。

GAUSS-03067: "NULL pointer"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03068: "value '%s' is out of range for type integer"

SQLSTATE: 22003

错误原因: 参数值超过了integer类型的取值范围。

解决办法: 请检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03069: "value '%s' is out of range for type smallint"

SQLSTATE: 22003

错误原因: 参数值超过了smallint类型的取值范围。

解决办法: 请检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03070: "value '%s' is out of range for 8-bit integer"

SQLSTATE: 22003

错误原因: 参数值超过了8位int类型的取值范围。

解决办法: 请检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

<br/>

## GAUSS-03071 - GAUSS-03080

<br/>

GAUSS-03071: "unsupported result size: %d"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03072: "invalid regular expression: %s"

SQLSTATE: 2201B

错误原因: 输入的表达式非法。

解决办法: 检查数据库encoding。

GAUSS-03073: "invalid regexp option: '%c'"

SQLSTATE: 22023

错误原因: 非法的regexp转换类型。

解决办法: 检查输入参数。

GAUSS-03074: "invalid escape string"

SQLSTATE: 22025

错误原因: 非法的转义字符。

解决办法: 检查输入参数。

GAUSS-03075: "regexp_split does not support the global option"

SQLSTATE: 22023

错误原因: regexp_split目前不支持global选项。

解决办法: 修改分隔选项。

GAUSS-03076: "invalid match ending position"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03077: "invalid match starting position"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03078: "unexpected dtype %d while parsing abstime '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03079: "cannot convert abstime 'invalid' to timestamp"

SQLSTATE: 0A000

错误原因: 绝对时间非法，无法转换为timestamp类型。

解决办法: 请检查输入参数。关于数据的取值范围请参考zh-cn_topic_0237121926.html。

GAUSS-03080: "unexpected dtype %d while parsing reltime '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03081 - GAUSS-03090

<br/>

GAUSS-03081: "invalid status in external 'tinterval' value"

SQLSTATE: 22P03

错误原因: 输入的字符串中获得的interval错误状态与数据对应的状态不一致。

解决办法: 检测输入字符串。

GAUSS-03082: "cannot convert reltime 'invalid' to interval"

SQLSTATE: 0A000

错误原因: reltime类型值非法，不能转换为interval类型。

解决办法: 检查输入参数。

GAUSS-03083: "invalid input syntax for type tinterval: '%s'"

SQLSTATE: 22007

错误原因: 输入字符串转换为tinterval类型时语法错误。

解决办法: 检查输入字符串。

GAUSS-03084: "invalid input syntax for type boolean: '%s'"

SQLSTATE: 22P02

错误原因: 输入字符串转换为Boolean类型时语法错误。

解决办法: 检查输入字符串。

GAUSS-03085: "missing dimension value"

SQLSTATE: 22P02

错误原因: 没有指定数组的维度。

解决办法: 检查输入字符串。

GAUSS-03086: "missing ']' in array dimensions"

SQLSTATE: 22P02

错误原因: 指定数组的维度时缺少"]"。

解决办法: 检查输入字符串。

GAUSS-03087: "upper bound cannot be less than lower bound"

SQLSTATE: 2202E

错误原因: 数组上界不能小于下界。

解决办法: 检查输入字符串。

GAUSS-03088: "array value must start with '{' or dimension information"

SQLSTATE: 22P02

错误原因: 数组字符串必须以"{"开始或者维度信息。

解决办法: 检查输入字符串。

GAUSS-03089: "missing assignment operator"

SQLSTATE: 22P02

错误原因: 如果指定数组维度信息，必须给出"="操作符。

解决办法: 检查输入字符串。

GAUSS-03090: "array dimensions incompatible with array literal"

SQLSTATE: 22P02

错误原因: 数组的维度与数组的数据不相符。

解决办法: 检查输入字符串。

<br/>

## GAUSS-03091 - GAUSS-03100

<br/>

GAUSS-03091: "malformed array literal: '%s'"

SQLSTATE: 22P02

错误原因: 数组输入数据格式错误。

解决办法: 请确认输入字符串，是否可以转换成数组常量。

GAUSS-03092: "multidimensional arrays must have array expressions with matching dimensions"

SQLSTATE: 22P02

错误原因: 多维的数组必须具有与维度相匹配的数组表达式。

解决办法: 检查输入字符串。

GAUSS-03093: "array size exceeds the maximum allowed (%d)"

SQLSTATE: 54000

错误原因: 数组长度超出了最大值。

解决办法: 请检查输入字符串中包含的数组元素是否超过了错误信息中指定的数值。

GAUSS-03095: "invalid number of dimensions: %d"

SQLSTATE: 22P03

错误原因: 数组的维度小于0。

解决办法: 检查输入参数。

GAUSS-03096: "invalid array flags"

SQLSTATE: 22P03

错误原因: 数组标志非法，只能为0或1。

解决办法: 检查输入参数。

GAUSS-03097: "wrong element type"

SQLSTATE: 42804

错误原因: 数组元素的类型不一致。

解决办法: 检查输入参数。

GAUSS-03098: "insufficient data left in message"

SQLSTATE: 22P03

错误原因: message中剩余的长度小于message首获取的长度值。

解决办法: 检查当前字符串。

GAUSS-03099: "improper binary format in array element %d"

SQLSTATE: 22P03

错误原因: 读完数组时，缓冲区还有数据。

解决办法: 输入数据有误，检查之。
