---
title: GAUSS-02501 - GAUSS-02600
summary: GAUSS-02501 - GAUSS-02600
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-02501 - GAUSS-02600

<br/>

## GAUSS-02501 - GAUSS-02510

<br/>

GAUSS-02501: "could not create unique index '%s'"

SQLSTATE: 23505

错误原因: 系统内部错误: 无法创建唯一索引。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02502: "reversedirection_index_hash is not implemented"

SQLSTATE: 0A000

错误原因: 系统内部错误: 当前函数未实现。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02503: "copytup_datum() should not be called"

SQLSTATE: 0A000

错误原因: 系统内部错误: 未实现函数，无法调用。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02504: "Failed to fetch from data node cursor"

SQLSTATE: 22P08

错误原因: 系统内部错误: 无法从DN游标中获取数据。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02505: "Node id %d is incorrect"

SQLSTATE: XX006

错误原因: 系统内部错误: 获取到的节点ID号错误。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02506: "%s"

SQLSTATE: XX000

错误原因: 语法错误。

解决办法: 检查sql语句的语法是否正确。

GAUSS-02507: "Unexpected response from the data nodes"

SQLSTATE: XX006

错误原因: 系统内部错误: 非期望的来自于DN的响应信息。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02508: "invalid BatchSort state"

SQLSTATE: XX006

错误原因: 系统内部错误: 无效的列存排序状态。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02509: "invalid batchsort state"

SQLSTATE: XX006

错误原因: 系统内部错误: 无效的列存排序状态。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02510: "batchsort_restorepos failed"

SQLSTATE: D0011

错误原因: 系统内部错误: 列存排序过程中存取位置信息出错。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02511 - GAUSS-02520

<br/>

GAUSS-02511: "failed to initialize hash table '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02512: "cannot insert into frozen hashtable '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02513: "out of shared memory"

SQLSTATE: 53200

错误原因: 共享内存不足。

解决办法: 1.可能需要将max_locks_per_transaction参数调大；2.可能当前节点上内存资源不足，需要通过释放相应的内存来解决；

GAUSS-02514: "unrecognized hash action code: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02515: "cannot freeze shared hashtable '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02516: "cannot freeze hashtable '%s' because it has active scans"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02517: "hash table '%s' corrupted"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02518: "too many active hash_seq_search scans, cannot start one on '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02519: "no hash_seq_search scan for hash table '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02520: "could not change directory to '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02521 - GAUSS-02530

<br/>

GAUSS-02521: "could not get current working directory: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02522: "cannot set parameter '%s' within security-restricted operation"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02524: "role '%s' is not permitted to login"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02525: "too many connections for role '%s'"

SQLSTATE: 53300

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02526: "permission denied to set session authorization"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02527: "invalid role OID: %u"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02528: "could not create lock file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02529: "could not open lock file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02530: "could not read lock file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02531 - GAUSS-02540

<br/>

GAUSS-02532: "lock file '%s' already exists"

SQLSTATE: F0001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02533: "pre-existing shared memory block (key %lu, ID %lu) is still in use"

SQLSTATE: F0001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02534: "could not remove old lock file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02535: "could not write lock file '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02536: "'%s' is not a valid data directory"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02537: "database files are incompatible with server"

SQLSTATE: 22023

错误原因: 数据库文件和软件版本不匹配。

解决办法: 请使用正确的软件版本重建数据库。

GAUSS-02538: "could not set timer for authorization timeout"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02539: "could not disable timer for authorization timeout"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02540: "database '%s' has disappeared from pg_database"

SQLSTATE: 3D000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02541 - GAUSS-02550

<br/>

GAUSS-02541: "database '%s' is not currently accepting connections"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02542: "permission denied for database '%s'"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02543: "too many connections for database '%s'"

SQLSTATE: 53300

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02544: "database locale is incompatible with operating system"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02545: "bad backend ID: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02546: "new replication connections are not allowed during database shutdown"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02547: "must be system admin to connect during database shutdown"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02548: "must be system admin to connect in binary upgrade mode"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02551 - GAUSS-02560

<br/>

GAUSS-02551: "database %u does not exist"

SQLSTATE: 3D000

错误原因: 数据库不存在。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02552: "Forbid remote connection with trust method!"

SQLSTATE: 42601

错误原因: 在可信方法下禁止远程连接。

解决办法: 请检查连接的安全设置。

GAUSS-02554: "not able to set up signal action handler"

SQLSTATE: 53000

错误原因: 无法为线程创建信号处理函数。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02555: "failed to create timer for thread"

SQLSTATE: XX000

错误原因:  无法为线程创建定时器。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02556: "could not find function '%s' in file '%s'"

SQLSTATE: 42883

错误原因: 无法找到函数定义。

解决办法: 请检查语句中的函数定义是否合法。

GAUSS-02557: "could not load library '%s': %s"

SQLSTATE: XX000

错误原因: 无法载入库。

解决办法: 请根据错误原因提示检查库。

GAUSS-02558: "incompatible library '%s': missing magic block"

SQLSTATE: XX000

错误原因: 不兼容的库，缺失魔数。

解决办法: 请检查该库是否损坏。

GAUSS-02559: "incompatible library '%s': version mismatch"

SQLSTATE: XX000

错误原因: 不兼容版本的库。

解决办法: 请检查是否兼容该版本的库。

GAUSS-02560: "incompatible library '%s': magic block mismatch"

SQLSTATE: XX000

错误原因: 不兼容的库，魔数不匹配。

解决办法: 请检查是否兼容此库。

<br/>

## GAUSS-02561 - GAUSS-02570

<br/>

GAUSS-02563: "invalid macro name in dynamic library path: %s"

SQLSTATE: 42602

错误原因: 动态库地址中包含不合法的路径宏。

解决办法: 请检查动态库地址中的路径宏是否合法。

GAUSS-02564: "zero-length component in parameter 'dynamic_library_path'"

SQLSTATE: 42602

错误原因: 动态库地址长度为0，不合法。

解决办法: 请检查动态库的地址。

GAUSS-02565: "component in parameter 'dynamic_library_path' is not an absolute path"

SQLSTATE: 42602

错误原因: 动态库地址不是绝对路径。

解决办法: 请检查动态库的地址。

GAUSS-02566: "init_MultiFuncCall cannot be called more than once"

SQLSTATE: 2F000

错误原因: 函数init_MultiFuncCall不能被调用多次。

解决办法: 请检查调用逻辑。

GAUSS-02567: "could not determine actual result type for function '%s' declared to return type %s"

SQLSTATE: 42804

错误原因: 无法确定函数的返回类型。

解决办法: 请检查函数的返回类型。

GAUSS-02568: "proallargtypes is not a 1-D Oid array"

SQLSTATE: XX000

错误原因: 函数参数不合法。

解决办法: 请检查传入参数。

GAUSS-02569: "proargnames must have the same number of elements as the function has arguments"

SQLSTATE: 22023

错误原因: 系统内部错误: 函数参数数目与proarnames不匹配。

解决办法: 请检查输入参数。

<br/>

## GAUSS-02571 - GAUSS-02580

<br/>

GAUSS-02572: "number of aliases does not match number of columns"

SQLSTATE: 42804

错误原因: 别名的数量和列的数量无法匹配。

解决办法: 通过\\d+ tablename确认列的数目后，请检查当前查询语句的别名的数量和列的数量是否匹配。

GAUSS-02573: "no column alias was provided"

SQLSTATE: 42804

错误原因: 列的别名没有提供。

解决办法: 请提供列的别名。

GAUSS-02574: "could not determine row description for function returning record"

SQLSTATE: 42804

错误原因: 无法确定RECORD返回类型函数的RECORD类型定义。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02575: "internal function '%s' is not in internal lookup table"

SQLSTATE: 42883

错误原因: 无法在函数查找表内找到函数定义。

解决办法: 检查SQL语句中的函数是否有效。

GAUSS-02576: "unrecognized function API version: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02577: "language %u has old-style handler"

SQLSTATE: 42P13

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02578: "null result from info function '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02579: "unrecognized API version %d reported by info function '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02580: "fmgr_oldstyle received NULL pointer"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02581 - GAUSS-02590

<br/>

GAUSS-02581: "function %u has too many arguments (%d, maximum is %d)"

SQLSTATE: 54023

错误原因: 函数输入参数过多。

解决办法: 通过\\df检查函数定义的参数数目和当前查询语句输入的参数数目是否一致。函数最大参数数目为16。

GAUSS-02583: "input function %u returned non-NULL"

SQLSTATE: XX005

错误原因: 列数据进行类型处理后返回了空字符串，与not null要求冲突。

解决办法: 修正列数据不为空，满足not null约束。

GAUSS-02584: "input function %u returned NULL"

SQLSTATE: XX005

错误原因: 列数据进行类型处理后返回了非空字符串，与null要求冲突。

解决办法: 修正列数据为空，满足null约束。

GAUSS-02585: "receive function %u returned non-NULL"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02586: "receive function %u returned NULL"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02587: "could not reopen file '%s' as stderr: %m"

SQLSTATE: XX000

错误原因: 无法作为STDERR重定向文件打开。

解决办法: 检查文件是否存在。

GAUSS-02588: "could not reopen file '%s' as stdout: %m"

SQLSTATE: XX000

错误原因: 无法作为STDOUT重定向文件打开。

解决办法: 检查文件是否存在。

GAUSS-02590: "buffer %d is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02591 - GAUSS-02600

<br/>

GAUSS-02591: "catcache reference %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02592: "catcache list reference %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02594: "partcache reference %s is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02595: "fakerel reference %s is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02596: "plancache reference %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02597: "tupdesc reference %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02598: "snapshot reference %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02599: "temporery file %d is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02600: "pthread mutex %p is not owned by resource owner %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
