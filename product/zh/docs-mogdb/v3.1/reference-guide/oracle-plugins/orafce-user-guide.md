---
title: orafce 使用手册
summary: orafce 使用手册
author: Guo Huan
date: 2021-11-29
---

# orafce

## orafce简介

orafce是MogDB针对Oracle的兼容包，可以支持一些Oracle的表、函数和数据类型。orafce提供兼容Oracle RDBMS的函数和操作符。

本插件包含一些实用功能，能够帮助将Oracle应用程序迁移到MogDB/PostgreSQL。内置Oracle日期函数已针对Oracle 10进行了一致性测试，支持从1960到2070的日期范围。由于Oracle内含的bug，目前无法验证1100-03-01之前的日期。

所有函数与Oracle完全兼容，并支持所有已知格式的字符串。详细介绍参见[GitHub orafce页面](https://github.com/orafce/orafce)。

> **注意**：orafce插件和whale插件不能同时使用，后续补丁版本将解决该问题。

 <br/>

## orafce安装

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

 <br/>

## orafce创建及使用

```sql
create extension orafce;
```

检查所有模式：

```sql
\dn
```

 <br/>

### dbms_output 使用方法

检查函数，其中函数分为有参数、无参数，参数类型为boolean。

```sql
\df dbms_output.*
```

无参：

```sql
select dbms_output.disable();
```

有参：

```sql
select dbms_output.put_line('sss');
```

boolean：（这里boolean值为0、1）

```sql
select dbms_output.serveroutput(0);
```

<br/>

### dbms_random使用方法

检查函数，函数分为有参、无参

```sql
\df
```

有参：

```sql
select dbms_random.initialize(1);
select dbms_random.value(256.1243434351237831823812312,12333.3111);
```

value可以不传参数。

无参：

```sql
select dbms_random.value();
```

<br/>

### dbms_utility使用

检查函数

```sql
\df
```

```sql
select dbms_utility.format_call_stack();
select dbms_utility.format_call_stack('o');
```

有参数只允许使用[ops]

```sql
select dbms_utility.get_time();
```

<br/>

### oracle函数使用

检查函数:

```sql
\df
```

```sql
select oracle.btrim('enmo');
select oracle.get_full_version_num();
select oracle.get_major_version();
select oracle.get_major_version_num();
select oracle.get_platform();
select oracle.get_status();
select oracle.length(1);
select oracle.lpad('enmo', 1);
select oracle.ltrim('enmo', 'enmo');
select oracle.numtodsinterval(12.22,'1232');
select oracle.nvl(1,2);
select oracle.regexp_count('enmo', 'tech');
select oracle.regexp_instr('enmo', 'tech');
select oracle.regexp_replace('enmo', 'tech', 'sss', 1);
select oracle.regexp_substr('enmo', 'tech', 1);
```

replace_empty_strings:

```sql
CREATE TABLE trg_test(a varchar, b int, c varchar, d date, e int);
CREATE TRIGGER trg_test_xx BEFORE INSERT OR UPDATE ON trg_test FOR EACH ROW EXECUTE PROCEDURE oracle.replace_empty_strings(true);
\pset null ***
INSERT INTO trg_test VALUES('',10, 'AHOJ', NULL, NULL);
INSERT INTO trg_test VALUES('AHOJ', NULL, '', '2020-01-01', 100);
SELECT * FROM trg_test;
```

replace_null_strings:

```sql
CREATE TABLE trg_test(a varchar, b int, c varchar, d date, e int);
CREATE TRIGGER trg_test_xx BEFORE INSERT OR UPDATE ON trg_test FOR EACH ROW EXECUTE PROCEDURE oracle.replace_null_strings();
\pset null ***
INSERT INTO trg_test VALUES(NULL, 10, 'AHOJ', NULL, NULL);
INSERT INTO trg_test VALUES('AHOJ', NULL, NULL, '2020-01-01', 100);
SELECT * FROM trg_test;
```

```sql
SELECT * FROM trg_test;
select oracle.round(1.212, 1);
select oracle.rpad('enmo', 1);
select oracle.rtrim('yunhe', 'enmo');
select oracle.sessiontimezone();
select oracle.substr(111.122,1);
select oracle.to_char('14-Jan08 11:44:49+05:30');
select oracle.translate_oracle_modifiers('icnsmx',true);
```

（参数必须为'icnsmx'一个或多个组合，会把m转换为n，n转换为s，boolean为true，字符串后边会添加一个g，false不添加）

```sql
select oracle.trunc(122.31, 1);
select oracle.unistr('yunhe-enmo');
```

<br/>

### utl_file使用

检查函数

```sql
\df
```

<br/>

### 文件检查操作

```sql
CREATE OR REPLACE FUNCTION checkFlushFile(dir text) RETURNS void AS $$
DECLARE
 f utl_file.file_type;
 f1 utl_file.file_type;
 ret_val text;
 i integer;
BEGIN
 f := utl_file.fopen(dir, 'regressflush_orafce.txt', 'a');
 PERFORM utl_file.put_line(f, 'ABC');
 PERFORM utl_file.new_line(f);
 PERFORM utl_file.put_line(f, '123'::numeric);
 PERFORM utl_file.new_line(f);
 PERFORM utl_file.putf(f, '[1=%s, 2=%s, 3=%s, 4=%s, 5=%s]', '1', '2', '3', '4', '5');
 PERFORM utl_file.fflush(f);
 f1 := utl_file.fopen(dir, 'regressflush_orafce.txt', 'r');
 ret_val=utl_file.get_nextline(f1);
 i:=1;
 WHILE ret_val IS NOT NULL LOOP
  RAISE NOTICE '[%] >>%<<', i,ret_val;
  ret_val := utl_file.get_nextline(f1);
  i:=i+1;
 END LOOP;
 RAISE NOTICE '>>%<<', ret_val;
 f1 := utl_file.fclose(f1);
 f := utl_file.fclose(f);
END;
$$ LANGUAGE plpgsql
```

<br/>

### 文件读取操作

```sql
CREATE OR REPLACE FUNCTION read_file(dir text) RETURNS void AS $$
DECLARE
 f utl_file.file_type;
BEGIN
 f := utl_file.fopen(dir, 'regress_orafce.txt', 'r');
 FOR i IN 1..11 LOOP
  RAISE NOTICE '[%] >>%<<', i, utl_file.get_line(f);
 END LOOP;
 RAISE NOTICE '>>%<<', utl_file.get_line(f, 4);
 RAISE NOTICE '>>%<<', utl_file.get_line(f, 4);
 RAISE NOTICE '>>%<<', utl_file.get_line(f);
 RAISE NOTICE '>>%<<', utl_file.get_line(f);
 EXCEPTION
  -- WHEN no_data_found THEN,  8.1 plpgsql doesn't know no_data_found
  WHEN others THEN
   RAISE NOTICE 'finish % ', sqlerrm;
   RAISE NOTICE 'is_open = %', utl_file.is_open(f);
   PERFORM utl_file.fclose_all();
   RAISE NOTICE 'is_open = %', utl_file.is_open(f);
 END;
$$ LANGUAGE plpgsql;
```
