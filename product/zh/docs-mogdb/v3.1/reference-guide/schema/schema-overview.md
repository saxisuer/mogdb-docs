---
title: Schema
summary: Schema
author: zhang cuiping
date: 2022-10-24
---

# Schema

MogDB的Schema如下表所示。

**表 1** MogDB支持的Schema

| Schema名称     | 描述                                                         |
| :------------- | :----------------------------------------------------------- |
| blockchain     | 用于存储账本数据库特性中创建防篡改表时自动创建的用户历史表。 |
| cstore         | 该模式用于储存列存表相关的辅助表如cudesc或者delta表。        |
| db4ai          | 用于管理AI训练中不同版本的数据信息。                         |
| dbe_perf       | DBE_PERF Schema内视图主要用来诊断性能问题，也是WDR Snapshot的数据来源。数据库安装后，默认只有初始用户和监控管理员具有模式dbe_perf的权限，有权查看该模式下的视图和函数。 |
| dbe_pldebugger | 用于调试plpgsql函数及存储过程。                              |
| snapshot       | 用于管理WDR snapshot的相关的数据信息，默认初始化用户或监控管理员用户可以访问。 |
| sqladvisor     | 用于分布列推荐，MogDB不可用。                                |
| sys            | 用于提供系统信息视图接口。                                   |
| pg_catalog     | 用于维护系统的catalog信息，包含系统表和所有内置数据类型、函数、操作符。 |
| pg_toast       | 用于存储大对象（系统内部使用）。                             |
| public         | 公共模式，缺省时，创建的表（以及其它对象）自动放入该模式。   |
| pkg_service    | 用于管理package服务相关信息。                                |