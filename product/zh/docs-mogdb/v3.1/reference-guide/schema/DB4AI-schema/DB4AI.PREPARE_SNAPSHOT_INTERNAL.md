---
title: DB4AI.PREPARE_SNAPSHOT_INTERNAL
summary: DB4AI.PREPARE_SNAPSHOT_INTERNAL
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PREPARE_SNAPSHOT_INTERNAL

PREPARE_SNAPSHOT_INTERNAL是db4ai.prepare_snapshot函数的内置执行函数。函数存在信息校验，无法直接调用。

**表 1** DB4AI.PREPARE_SNAPSHOT_INTERNAL入参和返回值列表

| 参数        | 类型         | 描述                    |
| :---------- | :----------- | :-----------------------|
| s_id        | IN BIGINT    | 快照ID。                                         |
| p_id        | IN BIGINT    | 父快照ID。                                       |
| m_id        | IN BIGINT    | 矩阵id。                                         |
| r_id        | IN BIGINT    | 根快照ID。                                       |
| i_schema    | IN NAME      | 快照模式。                                       |
| i_name      | IN NAME      | 快照名称。                                       |
| i_commands  | IN TEXT[]    | 定义快照修改的DDL和DML命令。                     |
| i_comment   | IN TEXT      | 快照描述。                                       |
| i_owner     | IN NAME      | 快照所有者。                                     |
| i_idx       | INOUT INT    | exec_cmds的索引。                                |
| i_exec_cmds | INOUT TEXT[] | 用于执行的DDL和DML。                             |
| i_mapping   | IN NAME[]    | 将用户列映射到备份列；如果不为NULL，则生成规则。 |
