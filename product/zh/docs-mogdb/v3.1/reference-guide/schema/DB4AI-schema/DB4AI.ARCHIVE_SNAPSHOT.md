---
title: DB4AI.ARCHIVE_SNAPSHOT
summary: DB4AI.ARCHIVE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.ARCHIVE_SNAPSHOT

ARCHIVE_SNAPSHOT是DB4AI特性用于存档快照的接口函数。通过语法ARCHIVE SNAPSHOT调用。生效后的快照无法参数训练等任务。

**表 1** DB4AI.ARCHIVE_SNAPSHOT入参和返回值列表

| 参数     | 类型                    | 描述                    |
| :------- | :---------------------- | :---------------------- |
| i_schema | IN NAME                 | 快照存储的模式名字，默认值是当前用户 |
| i_name   | IN NAME                 | 快照名称                             |
| res      | OUT db4ai.snapshot_name | 结果                                 |
