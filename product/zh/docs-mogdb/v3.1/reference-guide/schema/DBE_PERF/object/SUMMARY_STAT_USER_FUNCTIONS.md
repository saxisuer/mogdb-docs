---
title: SUMMARY_STAT_USER_FUNCTIONS
summary: SUMMARY_STAT_USER_FUNCTIONS
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STAT_USER_FUNCTIONS

SUMMARY_STAT_USER_FUNCTIONS用来统计所数据库节点用户自定义视图的相关统计信息。

**表 1** SUMMARY_STAT_USER_FUNCTIONS字段

| **名称**   | **类型**         | **描述**                        |
| :--------- | :--------------- | :------------------------------ |
| schemaname | name             | schema的名称。                                               |
| funcname   | name             | 用户function的名称。                                         |
| calls      | numeric          | 总调用次数。                                                 |
| total_time | double precision | 调用此function的总时间花费，包含调用其它function的时间（单位: 毫秒）。 |
| self_time  | double precision | 调用此function自己时间的花费，不包含调用其它function的时间（单位: 毫秒）。 |
