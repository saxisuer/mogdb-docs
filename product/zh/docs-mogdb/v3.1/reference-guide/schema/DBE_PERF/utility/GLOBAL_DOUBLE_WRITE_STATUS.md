---
title: GLOBAL_DOUBLE_WRITE_STATUS
summary: GLOBAL_DOUBLE_WRITE_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_DOUBLE_WRITE_STATUS

GLOBAL_DOUBLE_WRITE_STATUS视图显示MogDB所有实例的双写文件的情况。它是由每个节点的local_double_write_stat视图组成，属性完全一致。

**表 1** GLOBAL_DOUBLE_WRITE_STATUS字段

| 名称                  | 类型   | 描述                                                         |
| :-------------------- | :----- | :----------------------------------------------------------- |
| node_name             | text   | 节点名称。                                                   |
| curr_dwn              | bigint | 当前双写文件的序列号。                                       |
| curr_start_page       | bigint | 当前双写文件恢复起始页面。                                   |
| file_trunc_num        | bigint | 当前双写文件复用的次数。                                     |
| file_reset_num        | bigint | 当前双写文件写满后发生重置的次数。                           |
| total_writes          | bigint | 当前双写文件总的I/O次数。                                    |
| low_threshold_writes  | bigint | 低效率写双写文件的I/O次数（一次I/O刷页数量少于16页面）。     |
| high_threshold_writes | bigint | 高效率写双写文件的I/O次数（一次I/O刷页数量多于一批，421个页面）。 |
| total_pages           | bigint | 当前刷页到双写文件区的总的页面个数。                         |
| low_threshold_pages   | bigint | 低效率刷页的页面个数。                                       |
| high_threshold_pages  | bigint | 高效率刷页的页面个数。                                       |
| file_id               | bigint | 当前双写文件的id号。                                         |
