---
title: GLOBAL_CKPT_STATUS
summary: GLOBAL_CKPT_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_CKPT_STATUS

GLOBAL_CKPT_STATUS视图用于显示MogDB所有实例的检查点信息和各类日志刷页情况。

**表 1** GLOBAL_CKPT_STATUS字段

| 名称                     | 类型   | 描述                                  |
| :----------------------- | :----- | :------------------------------------ |
| node_name                | text   | 数据库进程名称。                      |
| ckpt_redo_point          | test   | 当前实例的检查点。                    |
| ckpt_clog_flush_num      | bigint | 从启动到当前时间clog刷盘页面数。      |
| ckpt_csnlog_flush_num    | bigint | 从启动到当前时间csnlog刷盘页面数。    |
| ckpt_multixact_flush_num | bigint | 从启动到当前时间multixact刷盘页面数。 |
| ckpt_predicate_flush_num | bigint | 从启动到当前时间predicate刷盘页面数。 |
| ckpt_twophase_flush_num  | bigint | 从启动到当前时间twophase刷盘页面数。  |
