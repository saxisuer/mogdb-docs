---
title: BGWRITER_STAT
summary: BGWRITER_STAT
author: Guo Huan
date: 2021-04-19
---

# BGWRITER_STAT

BGWRITER_STAT视图显示关于后端写进程活动的统计信息。

**表 1** BGWRITER_STAT字段

| **名称**              | **类型**                 | **描述**             |
| :-------------------- | :----------------------- | :----------------------------- |
| checkpoints_timed     | bigint                   | 执行的定期检查点数。                                         |
| checkpoints_req       | bigint                   | 执行的需求检查点数。                                         |
| checkpoint_write_time | double precision         | 花费在检查点处理部分的时间总量，其中文件被写入到磁盘，以毫秒为单位。 |
| checkpoint_sync_time  | double precision         | 花费在检查点处理部分的时间总量，其中文件被同步到磁盘，以毫秒为单位。 |
| buffers_checkpoint    | bigint                   | 检查点写缓冲区数量。                                         |
| buffers_clean         | bigint                   | 后端写进程写缓冲区数量。                                     |
| maxwritten_clean      | bigint                   | 后端写进程停止清理扫描时间数，因为它写了太多缓冲区。         |
| buffers_backend       | bigint                   | 通过后端直接写缓冲区数。                                     |
| buffers_backend_fsync | bigint                   | 后端不得不执行自己的fsync调用的时间数（通常后端写进程处理这些即使后端确实自己写）。 |
| buffers_alloc         | bigint                   | 分配的缓冲区数量。                                           |
| stats_reset           | timestamp with time zone | 这些统计被重置的时间。                                       |
