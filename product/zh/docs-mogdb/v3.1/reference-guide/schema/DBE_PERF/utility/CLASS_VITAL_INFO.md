---
title: CLASS_VITAL_INFO
summary: CLASS_VITAL_INFO
author: Guo Huan
date: 2021-04-19
---

# CLASS_VITAL_INFO

CLASS_VITAL_INFO视图用于做WDR时校验相同的表或者索引的oid是否一致。

**表 1** CLASS_VITAL_INFO字段

| 名称       | 类型   | 描述                                                         |
| :--------- | :----- | :----------------------------------------------------------- |
| relid      | oid    | 表的oid。                                                    |
| schemaname | name   | schema名称。                                                 |
| relname    | name   | 表名。                                                       |
| relkind    | "char" | 表示对象类型，取值范围如下: <br/>- r: 表示普通表。<br/>- t: 表示toast表。<br/>- i: 表示索引。 |
