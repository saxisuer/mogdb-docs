---
title: USER_TRANSACTION
summary: USER_TRANSACTION
author: Guo Huan
date: 2021-11-15
---

# USER_TRANSACTION

USER_TRANSACTION用来统计用户执行的事务信息。monadmin用户能看到所有用户执行事务的信息，普通用户只能查询到自己执行的事务信息。

**表 1** USER_TRANSACTION字段

| **名称**            | **类型** | **描述**                             |
| :------------------ | :------- | :----------------------------------- |
| username            | name     | 用户的名称。                         |
| commit_counter      | bigint   | 用户事务commit数量。                 |
| rollback_counter    | bigint   | 用户事务rollback数量。               |
| resp_min            | bigint   | 用户事务最小响应时间（单位：微秒）。 |
| resp_max            | bigint   | 用户事务最大响应时间（单位：微秒）。 |
| resp_avg            | bigint   | 用户事务平均响应时间（单位：微秒）。 |
| resp_total          | bigint   | 用户事务总响应时间（单位：微秒）。   |
| bg_commit_counter   | bigint   | 后台事务commit数量。                 |
| bg_rollback_counter | bigint   | 后台事务rollback数量。               |
| bg_resp_min         | bigint   | 后台事务最小响应时间（单位：微秒）。 |
| bg_resp_max         | bigint   | 后台事务最大响应时间（单位：微秒）。 |
| bg_resp_avg         | bigint   | 后台事务平均响应时间（单位：微秒）。 |
| bg_resp_total       | bigint   | 后台事务总响应时间（单位：微秒）。   |
