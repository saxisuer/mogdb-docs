---
title: OS_RUNTIME
summary: OS_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# OS_RUNTIME

显示当前操作系统运行的状态信息。

**表 1** OS_RUNTIME字段

| **名称**   | **类型** | **描述**                           |
| :--------- | :------- | :--------------------------------- |
| id         | integer  | 编号。                             |
| name       | text     | 操作系统运行状态名称。             |
| value      | numeric  | 操作系统运行状态值。               |
| comments   | text     | 操作系统运行状态注释。             |
| cumulative | boolean  | 操作系统运行状态的值是否为累加值。 |
