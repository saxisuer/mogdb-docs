---
title: LOCAL_REL_IOSTAT
summary: LOCAL_REL_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# LOCAL_REL_IOSTAT

获取当前节点中数据文件IO状态的累计值，显示为所有数据文件IO状态的总和。

**表 1** LOCAL_REL_IOSTAT字段

| **名称**  | **类型** | **描述**               |
| :-------- | :------- | :--------------------- |
| phyrds    | bigint   | 读物理文件的数目。     |
| phywrts   | bigint   | 写物理文件的数目。     |
| phyblkrd  | bigint   | 读物理文件的块的数目。 |
| phyblkwrt | bigint   | 写物理文件的块的数目。 |
