---
title: SUMMARY_STATIO_ALL_INDEXES
summary: SUMMARY_STATIO_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_INDEXES

SUMMARY_STATIO_ALL_INDEXES视图包含MogDB内汇聚的数据库中的每个索引行， 显示特定索引的I/O的统计。

| **名称**      | **类型** | **描述**                 |
| :------------ | :------- | :----------------------- |
| schemaname    | name     | 该索引的模式名。         |
| relname       | name     | 该索引的表名。           |
| indexrelname  | name     | 索引名称。               |
| idx_blks_read | numeric  | 从索引中读取的磁盘块数。 |
| idx_blks_hit  | numeric  | 索引命中缓存数。         |
