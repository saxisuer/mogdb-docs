---
title: STATEMENT_COMPLEX_HISTORY
summary: STATEMENT_COMPLEX_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COMPLEX_HISTORY

STATEMENT_COMPLEX_HISTORY视图显示在数据库主节点上执行作业结束后的负载管理记录。具体的字段请参考[GS_SESSION_MEMORY_DETAIL](../../../../reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_DETAIL.md)表1。
