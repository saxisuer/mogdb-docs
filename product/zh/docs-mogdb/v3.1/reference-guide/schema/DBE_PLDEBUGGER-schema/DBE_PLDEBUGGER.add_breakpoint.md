---
title: DBE_PLDEBUGGER.add_breakpoint
summary: DBE_PLDEBUGGER.add_breakpoint
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.add_breakpoint

debug端调试过程中，调用add_breakpoint增加新的断点。如果返回-1则说明指定的断点不合法，请参考[DBE_PLDEBUGGER.info_code](DBE_PLDEBUGGER.info_code.md)的canbreak字段确定断点合适的位置。

**表 1** add_breakpoint入参和返回值列表

| 名称         | 类型        | 描述       |
| :----------- | :---------- | :--------- |
| funcoid      | IN text     | 函数ID。   |
| lineno       | IN integer  | 行号。     |
| breakpointno | OUT integer | 断点编号。 |
