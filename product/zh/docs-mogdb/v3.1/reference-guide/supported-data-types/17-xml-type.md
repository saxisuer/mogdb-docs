---
title: XML类型
summary: XML类型
author: Guo Huan
date: 2021-04-06
---

# XML类型

MogDB支持XML类型，使用示例如下。

```sql
MogDB=# CREATE TABLE xmltest ( id int, data xml );
MogDB=# INSERT INTO xmltest VALUES (1, 'one');
MogDB=# INSERT INTO xmltest VALUES (2, 'two');
MogDB=# SELECT * FROM xmltest ORDER BY 1;
 id | data
----+--------------------
1 | one
2 | two
(2 rows)
MogDB=# SELECT xmlconcat('', NULL, '');
xmlconcat
(1 row)
MogDB=# SELECT xmlconcat('', NULL, '');
xmlconcat
(1 row)
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 该功能默认未开启，如需使用，需要重新使用build.sh脚本编译数据库，修改./configure配置参数，在其中加入--with-libxml参数。
> - 在执行编译之前，需要先执行`yum install -y libxml2-devel`，否则会有”configure: error: library 'xml2' (version &gt;= 2.6.23) is required for XML support”的报错。
> - 在执行编译之前，需要三方库二进制文件中dependeny操作系统环境/libobs/comm/lib加入到系统环境变量LD_LIBRARY_PATH中，否则会报错”libiconv.so不存在”。
