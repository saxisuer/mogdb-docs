---
title: 统一数据库管理工具
summary: 统一数据库管理工具
author: Guo Huan
date: 2022-06-08
---

# 统一数据库管理工具

CM（Cluster Manager）是一款集群资源管理软件，提供数据库主备状态监控、网络通信故障监控、文件系统故障监控、故障自动主备切换等能力，支持集群、节点、实例级的启停，集群状态查询、主备切换、日志管理等功能。提供了通过REST接口远程查询和接收集群状态的能力。

需要注意的是：一主一备模式下，CM只支持基本的安装，启停，检测能力，其他功能不支持。

## 特性介绍

### cm_agent

cm_agent是部署在数据库每个主机上，用来启停和监控各个数据库实例进程的数据库管理组件。

主要功能有：

- 数据库实例启动和停止时负责拉起和停止本主机上部署的实例进程。
- 监控本主机上运行的实例状态并将状态上报发送给CM Server。
- 执行CM Server仲裁下发的命令。

**命令说明**：

- 公共选项：

  - -V, --version

    打印cm_agent版本信息，然后退出。

  - -?, -h,--help

    显示关于cm_agent命令行参数的帮助信息，然后退出。

- 日志信息记录的位置选项：

  - 0

    记录在设定的日志文件中。

  - 1

    记录在syslog文件中。

  - 2

    记录在设定的日志文件中。

  - 3

    空文件，即不记录日志信息。

- 启动模式选项：

  - normal

    正常模式启动。

  - abnormal

    非正常模式启动。
  
### cm_server

cm_server是用来进行数据库实例管理和实例仲裁的组件。主要功能有：

- 接收各个节点上cm_agent发送的数据库各实例状态。
- 提供数据库实例整体状态的查询功能。
- 监控实例的状态变化并进行仲裁命令的下发。

**命令说明**：

- 公共选项：

  - -V, --version

    打印cm_server版本信息，然后退出。

  - -?, -h,--help

    显示关于cm_server命令行参数的帮助信息，然后退出。

- 日志信息记录的位置选项：

  - 0

    记录在设定的日志文件中。

  - 1

    记录在syslog文件中。

  - 2

    记录在设定的日志文件中。

  - 3

    空文件，即不记录日志信息。
  
### 支持自定义资源监控

当前CM支持对无状态资源（即各资源实例角色平等，不区分主备，或资源自身就能够自行进行主备仲裁）进行监控，主要功能包括： - 资源启动、停止 - 资源进程监控、异常拉起 - 查询资源进程状态 - 进程僵死检测和恢复（需结合资源脚本）

配置方法： 安装好自定义资源后，若要使用自定义资源监控功能，需要配置两个文件：

1. 资源脚本
   主要用于指定资源的启停、状态检查等指令，一个样例如下：

   ```
   #!/bin/bash
   #set -ex   #取消该行注释可帮助调试脚本
   #资源名称
   resName=sharding
   #资源binpath
   shardingPath=/home/test/home/apache-shardingsphere-5.1.1-shardingsphere-proxy-bin/bin
   #用于过滤资源实例的命令关键词
   cmdKey=org.apache.shardingsphere.proxy.Bootstrap
   #用于保存首次检测到资源僵死时间的文件
   phony_dead_time_file=.sharding_phony_dead_time
   #最长僵死时间，单位为s
   PHONY_MAX_TIME=20
   
   function exec_start
   {
   #资源启动命令
   sh ${shardingPath}/start.sh; exit $?
   }
   
   function exec_stop
   {
   #资源停止命令
   sh ${shardingPath}/stop.sh; exit $?
   }
   
   function exec_check
   {
   #查询资源实例pid
   pid=`ps x | grep "$cmdKey" | grep -v grep | awk '{print $1}'`
   if [ "${pid}" == "" ]; then
   echo "$resName is not running."
   exit 1
   fi
   #查询资源实例进程状态
   state=`cat /proc/$pid/status | grep "State" | awk '{print $2}'`
   if [ "$state" == "T" ]; then
   #僵死检查和处理
   if [ ! -f $phony_dead_time_file ]; then
     touch ./${phony_dead_time_file}
     echo "export firstphonytime=''" > ./${phony_dead_time_file}
   fi
   source ./$phony_dead_time_file;
   curtime=$(date +%s);
   if [ "$firstphonytime" == "" ]; then
     #首次检测到资源僵死，将首次检测到僵死的时间写入僵死时间存储文件
     #firstphonytime为用于保存当前资源实例僵死时间的变量名称，
     #若当前节点存在多个自定义资源实例，该名称需要指定为不同的名称
     echo "export firstphonytime=$curtime" > ./$phony_dead_time_file;
     exit 0;
   fi
   dead_time=$(( $curtime - $firstphonytime ));
   #若僵死时间大于等于用户设定的最大僵死时间，则立即杀死资源实例，否则不做处理正常退出
   if [ $dead_time -ge $PHONY_MAX_TIME ]; then
     echo "$resName is detected in a state of phony dead(T) and will be forcibly killed!"
     kill -9 $pid
     rm ./${phony_dead_time_file} -f
     sh ${shardingPath}/start.sh; exit $?
   else
     exit 0
   fi
   elif [ "$state" == "S" ]; then
   #未处于僵死状态清理环境后正常退出
   rm ./${phony_dead_time_file} -f
   exit 0
   fi
   }
   
   #以下为固定接口无需更改，对应以上三个函数，且必须实现
   if [ $1 == '-start' ]; then
   exec_start $2
   elif [ $1 == '-stop' ]; then
   exec_stop $2
   elif [ $1 == '-check' ]; then
   exec_check $2
   fi
   ```

   以上样例可以作为模板使用，用户主要需要修改的地方包括： 资源名称、资源binPath、用于过滤资源实例的命令关键词、用于保存首次检测到资源僵死时间的文件（可选）、最长僵死时间、记录首次僵死时间的变量名（如果同一节点存在多个不同的自定义资源实例）

2. 自定义资源配置文件cm_resource.json 该文件位置为cmdir/cm_agent/cm_resource.json，配置该文件后需要重启集群

   ```
   {
   "resources": [
   {
     "name": "sharding",
     "instances": [
       {
         "node_id": 1,
         "res_instance_id": 1
       },
       {
         "node_id": 2,
         "res_instance_id": 2
       }
     ],
     "script": "/usr2/omm/install/cm/cm_agent/sharding.sh",
     "check_interval": 1,
     "time_out": 5,
     "restart_delay":3,
     "restart_period":5,
     "restart_times":10
   },
   {
     "name": "test",
     "instances": [
       {
         "node_id": 1,
         "res_instance_id": 1
       },
       {
         "node_id": 2,
         "res_instance_id": 2
       }
     ],
     "script": "/usr2/omm/install/cm/cm_agent/test.sh",
     "check_interval": 1,
     "time_out": 5,
     "restart_delay":0,
     "restart_period":0,
     "restart_times":1000
   }
   ]
   }
   ```

配置说明：

1. resources: 自定义资源对象列表，名称固定不能更改
2. name: 自定义资源对象名称，字符串类型，最大长度为32（包含末尾'\0'）
3. instances: 自定义资源所在节点列表
4. node_id: 资源实例所在节点的node_id
5. res_instance_id: 资源实例id，大于等于0，同一种资源的不同实例id不同
6. script: 资源脚本位置
7. check_interval: 上报资源状态时间间隔，大于等于0，单位s
8. time_out: 脚本执行的超时时间，大于等于0，单位s
9. restart_delay: 故障之后重启延迟时间，单位为s，取值范围[0,1800]
10. restart_period: 当前时间-最近重启时间若大于restart_period，则再次重启资源重启次数加1
11. restart_times: 周期内最多重启次数，超过则不再重启，并将资源标记为不可用，取值范围[0,9999]，0表示无限重启

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **注意：**
> 资源配置文件需要在所有节点上都有，且保持一致。用户需要保证资源脚本能够正确运行。

### 支持集群信息查询和推送

**功能介绍：**
通过运行组件CMRestAPI，CM能够支持：

1. 通过http/https服务远程查询到集群的状态，便于管理人员、运维平台等监控集群状态
2. 在数据库集群发生切主事件时，通过http/https服务及时地将集群最新的主备信息推送到应用端注册的接收地址，便于应用端及时的感知到集群的主备变化，从而能够快速的连接到新的主机和备机。

**参数说明：**
-e 数据库环境变量文件，必须指定
-w 访问来源ip白名单，如果不需要设置白名单则可以不用指定
启动命令：
java -jar cmrestapi-xxx.jar -e envFile [-w appWhiteList]

**接口说明：**

1. 集群或节点状态查询
   该接口使用GET方法。
   其中：
   ip为运行CMRestAPI的节点ip；
   port为CMRestAPI服务的监听端口；
   keyword为待查询的信息关键词，当前支持查询的信息即关键词包括：
   集群状态**ClusterStatus**

   节点状态**NodeStatus**，指定nodeId等于n，则可以查询节点n的状态，若不指定nodeId，则默认返回提供服务的节点即链接中ip所指定节点的状态。

2. 注册和更新主备机信息接收地址
   如果应用端想要接收到CMRestAPI推送的集群当前最新的主备信息，需要向CMRestAPI注册一个信息接收地址并且需要在该地址上进行监听。接收到该请求后CMRestAPI会通过dcc将注册的接收地址保存到集群所在环境，dcc存储数据的形式为key-value形式，使用的key为/CMRestAPI/RecvAddrList/**ip**/**app**，其中ip为应用端所在机器的ip地址，app为用户自定义的应用名称，主要用于区分同一环境上的多个应用注册的接收地址。如果key已存在，即来源ip和应用名称均相同，则会更新key对应的主备信息接收地址。
   该接口需使用PUT方法，需要提供两个参数：
   url——待注册的接收地址；
   app——应用名称，若不提供该参数则以**前缀+应用端ip**为key。

3. 删除主备机信息接收地址
   该接口使用DELETE方法，需要提供一个参数：
   app——应用名称。若不提供该参数则以**前缀+应用端ip**为key。

4. 信息接收地址说明
   信息接收地址示例：CMRestAPI使用PUT方法，推送主机信息context为MasterInfo，发送对象类型为String，主机信息格式为ip:port，推送备机context为StanbyInfo，发送对象类型为String，备机信息格式为ip1:port1,ip2:port2,...,ipn:portn，一个应用端的demo参见CMRestAPI仓库中的applicationdemo。

**其他使用说明：**

1. 安全相关
   (1) CMRestAPI默认使用http服务，支持配置访问白名单，可通过启动参数-w配置访问来源ip的白名单文件，白名单文件配置格式为每行一个ip地址；
   (2) 若要使用https服务，则可以在启动时jar包时指定系统参数server.ssl相关参数来是CMRestAPI启动https服务，或将相关参数写入application.properties文件然后在启动命令中指定配置文件，或配置源码resource目录下的application.properties文件然后自行编译，自定义配置参数示例：

   ```
   -Dserver.port=服务监听端口 -Dserver.ssl.key-store=秘钥文件路径 -Dserver.ssl.key-store-password=秘钥文件密码 -Dserver.ssl.key-store-type=秘钥类型  
   如：  
   指定参数方式系统参数方式
   java -jar -Dserver.port=8443 -Dserver.ssl.key-store=/home/omm/keystore.p12 -Dserver.ssl.key-store-password=Abcdef@123 -Dserver.ssl.key-store-type=PKCS12 cmrestapi-xxx.jar -e envFile  
   指定配置文件方式  
   java -jar -Dspring.config.location=/configpath/application.properties cmrestapi-xxx.jar -e envFile
   ```

更多相关配置参数可自行搜索配置

1. 内存相关
   由于本程序使用了springboot框架，默认启动会占用较大内存（约1G左右），若并发量不大不希望该程序占用较大内存，则可以在启动时指定一些系统参数减小内存占用，启动参数示例：

   ```
   -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=56m -Xms128m -Xmx128m -Xmn32m -Xss328k -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC  
   如：java -jar -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=56m -Xms128m -Xmx128m -Xmn32m -Xss328k -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC cmrestapi-xxx.jar -e envFile
   ```

   更多相关配置参数可自行搜索配置

2. 自定义资源配置文件
   本程序需要依赖cm相关进程和指令，所以必须与cm同时运行，需配置自定义资源配置文件，配置方法详见自定义资源监控特性相关内容。

**操作步骤说明：**

安装带cm的数据库集群，配置资源脚本和自定义资源文件，资源脚本示例如下：
cmrestapi.sh

```
#!/bin/bash
#set -ex
#资源名称
resName=CM-RestAPI
#资源binpath
cmrestapiPath=/home/cmrestapi/cmrestapi-3.1.0-RELEASE.jar
#资源启动命令关键词
cmdKey=cmrestapi-3.1.0-RELEASE.jar
#用于保存首次检测到资源假死时间的文件
phony_dead_time_file=.cmrestapi_phony_dead_time
#最长假死时间，单位为s
PHONY_MAX_TIME=20
envFile=/home/cmrestapi/envfile
#appWhiteListFile=/home/cmrestapi/appWhiteListFile
source $envFile

function exec_start
{
    nohup java -jar -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=56m -Xms128m -Xmx128m -Xmn32m -Xss328k -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC -Dserver.port=8080 $cmrestapiPath -e $envFile >> $GAUSSLOG/cm/cmrestapi/cmrestapi.log 2>&1 &
    exit $?
}

function exec_stop
{
    ps x | grep "$cmdKey" | grep -v grep | awk '{print $1}' | xargs kill -9; exit $?
}

function exec_check
{
    pid=`ps x | grep "$cmdKey" | grep -v grep | awk '{print $1}'`
    if [ "${pid}" == "" ]; then
        echo "$resName is not running."
        exit 1
    fi
    state=`cat /proc/$pid/status | grep "State" | awk '{print $2}'`
    if [ "$state" == "T" ]; then
        if [ ! -f $phony_dead_time_file ]; then
            touch ./${phony_dead_time_file}
            echo "export firstphonytime=''" > ./${phony_dead_time_file}
        fi
        source ./$phony_dead_time_file;
        curtime=$(date +%s);
        if [ "$firstphonytime" == "" ]; then
            echo "export firstphonytime=$curtime" > ./$phony_dead_time_file;
            exit 0;
        fi
        dead_time=$(( $curtime - $firstphonytime ));
        if [ $dead_time -ge $PHONY_MAX_TIME ]; then
            echo "$resName is detected in a state of phony dead(T) and will be forcibly killed!"
            kill -9 $pid
            rm ./${phony_dead_time_file} -f
            exec_start
        else
            exit 0
        fi
    elif [ "$state" == "S" ]; then
        rm ./${phony_dead_time_file} -f
        echo "$resName is running normally."
        exit 0
    fi
}

if [ $1 == '-start' ]; then
    exec_start $2
elif [ $1 == '-stop' ]; then
    exec_stop $2
elif [ $1 == '-check' ]; then
    exec_check $2
fi
```

自定义资源文件cm_resource.json示例如下：

```
{
  "resources": [
    {
      "name": "CM-RestAPI",
      "instances": [
        {
          "node_id": 1,
          "res_instance_id": 1
        },
        {
          "node_id": 2,
          "res_instance_id": 2
        },
        {
          "node_id": 3,
          "res_instance_id": 3
        }
      ],
      "script": "/home/cmrestapi/install/cm/cm_agent/cmrestapi.sh",
      "check_interval": 1,
      "time_out": 10,
      "restart_delay":0,
      "restart_period":0,
      "restart_times":1000
    }
  ]
}
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **注意：**
> 使用cm的自定义资源管理功能需将进程放到后台执行，所以需要将日志输出重定向至日志文件或配置日志输出相关选项，并且使用nohup和&将本程序放置到后台运行 本程序需要运行在有数据库的节点；如果在集群发生切换时需要使用主备信息主动推送功能，则需要将该程序运行在集群中所有数据库节点 2. 启动集群，即可通过浏览器等访问上述集群或节点信息查询接口查询对应信息 3. 应用端开发（可参考源码仓库的demo），启动应用端 4. 注册信息接收地址

## cm_ctl工具介绍

cm_ctl是MogDB提供的用来控制数据库实例服务的工具。该工具主要供OM调用，及数据库实例服务自恢复时使用。cm_ctl的主要功能有：

- 启动数据库实例服务、AZ的所有实例、单个主机上的所有实例或单独启动某个实例进程。
- 停止数据库实例服务、AZ的所有实例、单个主机上的所有实例或单独停止某个节点实例进程。
- 重启逻辑数据库实例服务。
- 查询数据库实例状态或者单个主机的状态。
- 切换主备实例或重置实例状态。
- 重建备机。
- 查看数据库实例配置文件。
- 设置日志级别，一主多备数据库实例部署下cm_server的仲裁模式、AZ之间的切换模式。
- 获取日志级别，一主多备数据库实例部署下cm_server的仲裁模式、AZ之间的切换模式。
- 检测实例进程状态。

与cm_ctl工具相关的文件：

- cluster_manual_start

  该文件是数据库实例启停标志文件。文件位于_$GAUSSHOME/bin_下。其中，GAUSSHOME为环境变量。启动数据库实例时，cm_ctl会删除该文件；停止数据库实例时，cm_ctl会生成该文件，并向文件写入停止模式。

- instance_manual_start_X（X是实例编号）

  该文件是单个实例启停标志文件。文件位于_$GAUSSHOME/bin_下。其中，GAUSSHOME为环境变量。启动实例时，cm_ctl会删除该文件；停止实例时，cm_ctl会生成该文件，并向文件写入停止模式。

cm_ctl的相关约束：

- 在集群模式下，使用cm_ctl集群工具来切换数据库角色，而不是gs_ctl数据库工具。

### 命令说明

cm_ctl参数可分为如下几类：

- option参数，详细请参见 [表 option参数](#table1)。
- 公共参数，详细请参见 [表 公共参数](#table2)。
- start模式的参数，详细参见 [表 start参数](#table15)。
- switchover模式的参数，详细请参见 [表 switchover参数](#table3)。
- build模式的参数，详细请参见 [表 build参数](#table4)。
- check模式的参数，详细请参见 [表 check参数](#table5)。
- stop模式的参数，详细请参见 [表 stop参数](#table6)。
- query模式的参数，详细请参见 [表 query参数](#table7)。
- view模式的参数，详细请参见 [表 view参数](#table11)。
- set模式的参数，详细请参见 [表 set参数](#table8) [表 set cm参数](#table9)。
- get模式的参数，详情请参见 [表 get参数](#table10)。
- setrunmode模式的参数，详细请参见 [表 setrunmode参数](#table12)。
- changerole模式的参数，详细请参见 [表 changerole参数](#table13)。
- changemember功能的参数，详细请参见 [表 changemember参数](#table14)。
- reload模式的参数，详细请参见 [表 reload 参数](#table16)。
- list模式的参数，详细请参见 [表 list参数](#table17)。
- encrypt模式的参数，详细请参见 [表 encrypt参数](#table18)。
- ddb模式的参数，详细请参见 [表 ddb参数](#table20)。
- switch模式的参数，详细请参见 [表 switch参数](#table19)。

使用方法:

```
cm_ctl start [-z AVAILABILITY_ZONE [--cm_arbitration_mode=ARBITRATION_MODE]] | [-n NODEID [-D DATADIR]] [-t SECS]
cm_ctl switchover [-z AVAILABILITY_ZONE] | [-n NODEID -D DATADIR [-f]] | [-a] | [-A] [-t SECS]
cm_ctl finishredo
cm_ctl build [-c] [-n NODEID] [-D DATADIR [-t SECS] [-f] [-b full] [-j NUM]]
cm_ctl check -B BINNAME -T DATAPATH
cm_ctl stop [[-z AVAILABILITY_ZONE] | [-n NODEID [-D DATADIR]]] [-t SECS] [-m SHUTDOWN-MODE]
cm_ctl query [-z ALL] [-l FILENAME] [-v [-C [-s] [-S] [-d] [-i] [-F] [-x] [-p]] | [-r]] [-t SECS] [--minorityAz=AZ_NAME]
cm_ctl view [-v | -N | -n NODEID] [-l FILENAME]
cm_ctl set [--log_level=LOG_LEVEL] [--cm_arbitration_mode=ARBITRATION_MODE] [--cm_switchover_az_mode=SWITCHOVER_AZ_MODE] [--cmsPromoteMode=CMS_PROMOTE_MODE -I INSTANCEID]
cm_ctl set --param --agent | --server [-n [NODEID]] -k [PARAMETER]="[value]"
cm_ctl get [--log_level] [--cm_arbitration_mode] [--cm_switchover_az_mode]
cm_ctl setrunmode -n NODEID -D DATADIR  [[--xmode=normal] | [--xmode=minority --votenum=NUM]]
cm_ctl changerole [--role=PASSIVE | --role=FOLLOWER] -n NODEID -D DATADIR [-t SECS]
cm_ctl changemember [--role=PASSIVE | --role=FOLLOWER] [--group=xx] [--priority=xx] -n NODEID -D DATADIR [-t SECS]
cm_ctl reload --param [--agent | --server]
cm_ctl list --param --agent | --server
cm_ctl encrypt [-M MODE] -D DATADIR
cm_ctl ddb DCC_CMD
cm_ctl switch [--ddb_type=[DDB]] [--commit] [--rollback]
```

**表 1** option参数 <a id="table1"> </a>

| 参数           | 参数说明                                                     |
| :------------- | :----------------------------------------------------------- |
| start          | 一主多备数据库部署模式下启动数据库实例服务、单个主机上的所有实例或单独启动某个节点实例进程，或者直接启动整个AZ。 |
| switchover     | 一主多备数据库部署模式下切换数据库主备实例，<font color="red">dcf模式下只支持-n NODEID -D DATADIR。</font> |
| finishredo     | 所有备机停止回放，每个分片中选择一个强制升主。<br/>注意：该参数属于高风险操作，请谨慎执行。 |
| build          | 重建备实例。                                                 |
| check          | 检测实例进程运行状态，用户无需关注，不建议使用。             |
| stop           | 一主多备数据库部署模式下停止数据库实例服务、单个主机上的所有实例或单独停止某个节点实例进程。 |
| query          | 一主多备数据库部署模式下查询数据库实例状态或者单个主机的状态。 |
| view           | 查看数据库实例配置文件。                                     |
| set            | 设置日志级别，一主多备数据库部署模式下cm_server的仲裁模式、AZ之间的切换模式、cm_server升主模式。 |
| set --param    | 设置cm参数，默认set所有节点上的参数，也可以通过-n参数指定set某个节点，具体参数可以参考CM配置参数介绍 |
| get            | 获取日志级别，一主多备数据库部署模式下cm_server的仲裁模式、AZ之间的切换模式。 |
| setrunmode     | DCF部署方式下，设置DCF投票数，主要用于DCF强启。              |
| changerole     | DCF模式下，将角色为primary的修改为passive或者follower。      |
| changemember   | DCF模式下，改变指定DCF节点属性，包括节点角色、节点所在的逻辑组、节点的选举优先级等。 |
| reload         | 在线加载数据库实例静态配置文件，用户无需关注，不建议使用。   |
| reload --param | 加载可以动态生效的cm参数，部分参数不支持reload，只能重启cm才能生效。 |
| list           | 列出cm_agent或cm_server所有的参数。                          |
| encrypt        | 对输入的密码进行加密操作，密码支持8~15位，且必须包含三种字符（数字，字母，符号）。 |
| ddb            | DCC模式下，执行dcc命令行。                                   |
| switch         | 执行ddb模式的切换。<br/>说明：MogDB当前只支持切换到DCC模式。 |

**表 2** 公共参数<a id="table2"> </a>

| 参数                 | 参数说明                                                     |
| :------------------- | :----------------------------------------------------------- |
| -D DATADIR           | 指定实例数据目录。仅用于对数据库节点进行操作的命令，如start、stop、switchover、build、setrunmode、changerole、changemember、encrypt。 |
| -l FILENAME          | 查询结果输出到指定文件。仅用于查询类的命令，如query、view。  |
| -n NODEID            | 指定节点。                                                   |
| -z AVAILABILITY_ZONE | 指定AZ名称。                                                 |
| -t SECS              | 指定超时时间。                                               |
| -V, --version        | 打印cm_ctl版本信息，然后退出。                               |
| -?, -h,--help        | 显示关于cm_ctl命令行参数的帮助信息，然后退出。               |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：**
>
> 此处列出的公共参数并不一定适用于所有命令，而是多个命令支持，为避免冗余信息，所以统一在此说明，详细的使用方法见以上使用方法，也可以使用cm_ctl –help进行查询。

**表 3** switchover参数 <a id="table3"> </a>

| 参数 | 参数说明                                                     |
| :--- | :----------------------------------------------------------- |
| -a   | 重置节点状态为初始配置状态。<br/>说明：switchover为维护操作：确保数据库实例状态正常，所有业务结束，并使用pgxc_get_senders_catchup_time()视图查询无主备追赶后，再进行switchover操作。 |
| -A   | 将所有节点实例统一从主切换到备。                             |
| -f   | 指定进行-f类型switchover。<br/>说明：<br/>- switchover为维护操作：确保数据库实例状态正常，所有业务结束，并使用pgxc_get_senders_catchup_time()视图查询无主备追赶后，再进行switchover操作。<br/>- 使用方式：cm_ctl switchover -n NODEID -D DATADIR -f。 |

**表 4** build参数<a id="table4"> </a>

| 参数    | 参数说明                                                     |
| :------ | :----------------------------------------------------------- |
| -f      | 强制重建备机。                                               |
| -b full | 指定进行全量build。不指定情况下，对于一主多备数据库实例部署模式进行auto build。auto build指：先调用增量build，失败之后调用全量build。 |
| -c      | 重建 cm_server（将主节点的DCC数据目录拷贝到指定节点，只适用于一主一备模式）。 |

**表 5** check参数<a id="table5"> </a>

| 参数        | 参数说明                                                  |
| :---------- | :-------------------------------------------------------- |
| -B BINNAME  | 指定进程名，其进程名包括“cm_agent”,“mogdb”和“cm_server”。 |
| -T DATAPATH | 指定实例数据目录。                                        |

**表 6** stop参数<a id="table6"> </a>

| 参数             | 参数说明                                                     |
| :--------------- | :----------------------------------------------------------- |
| -m SHUTDOWN-MODE | 指定停止模式，停止模式有以下几种：<br/>- smart（s）：等待用户业务结束后，停止所有数据库实例。<br/>- fast（f）：不等待用户业务结束，指定数据库实例退出。<br/>- immediate（i）：不等待用户业务结束，指定数据库实例强制退出。 |

**表 7** query参数<a id="table7"> </a>

| 参数         | 参数说明                                                     |
| :----------- | :----------------------------------------------------------- |
| -s           | 显示导致各个节点主备实例数量不均衡的实例。<br/>说明：-s参数需要和-v以及-C参数一起使用才能按主备关系成对显示导致各个节点主备实例数量不均衡的实例，使用-s参数时，必须指定-C、-v参数。 |
| -C           | 按主备关系成对显示数据库实例状态。<br/>说明：-C参数需要和-v参数一起使用才能按主备关系成对显示数据库实例详细状态信息，使用-C时，必须指定-v参数。 |
| -v           | 显示详细数据库实例状态。<br/>说明：数据库实例有如下几种状态：<br/>- Normal：表示数据库实例可用，且数据有冗余备份。所有进程都在运行，主备关系正常。<br/>- Degraded：表示数据库实例可用，但数据没有冗余备份。<br/>- Unavailable：表示数据库实例不可用。 |
| -d           | 显示实例数据目录。<br/>说明：-d参数需要和-v以及-C参数一起使用。 |
| -i           | 显示物理节点ip。<br/>说明：-i参数需要和-v以及-C参数一起使用。 |
| -F           | 显示各个节点Fenced UDF状态。<br/>说明：-F参数需要和-v以及-C参数一起使用才能显示各个节点Fenced UDF状态，使用-F参数时，必须指定-C、-v参数。 |
| -z ALL       | 显示数据库实例所在AZ名称。<br/>说明：-z参数需要和-v以及-C参数一起使用，并且-z后面需要跟参数ALL。 |
| -r           | 显示备机redo状态。<br/>说明：使用-r时，必须指定-v参数。      |
| -g           | 显示备份和恢复群集信息。                                     |
| -x           | 显示所有异常的数据库实例。说明：-x参数需要和-v以及-C参数一起使用。 |
| -S           | 显示数据库实例启动时的状态检查结果。<br/>说明：-S参数需要和-v以及-C参数一起使用才能按显示数据库实例的状态检查结果。有以下三种状态：<br/>- Normal：表示数据库实例可用，且数据有冗余备份。所有进程都在运行，主备关系正常。<br/>- Degraded：表示数据库实例可用，但数据没有冗余备份。<br/>- Unavailable：表示数据库实例不可用。 |
| --minorityAz | 只查询指定AZ的cms。<br/>说明：此参数会忽略非指定AZ的cms节点，可以在少数派场景提高查询速度。 |
| -p           | 显示数据库实例所有节点端口。<br/>说明：-p参数需要和-v以及-C参数一起使用。 |

**表 8** set参数<a id="table8"> </a>

| 参数                                            | 参数说明                                                     |
| :---------------------------------------------- | :----------------------------------------------------------- |
| --log_level=LOG_LEVEL                           | 设置主cm_server日志级别。共分为DEBUG5、DEBUG1、WARNING、LOG、ERROR和FATAL六个级别，日志打印信息级别越来越高。设置日志级别越高，输出日志信息就越少。 |
| --cm_arbitration_mode=ARBITRATION_MODE          | 一主多备功能，设置cm_server的仲裁模式。共有MAJORITY、MINORITY两种模式，MAJORITY为多数派模式，MINORITY为少数派模式。MogDB不支持少数派，此参数可以设置成MINORITY，但不会生效。 |
| --cm_switchover_az_mode=SWITCHOVER_AZ_MODE      | 一主多备功能，设置AZ之间的自动切换开关。共有NON_AUTO、AUTO两种模式，NON_AUTO为非自动切换模式，AUTO为自动切换模式。AUTO模式由主cm_server自动控制AZ1和AZ2之间的节点实例切换。 |
| --cmsPromoteMode=CMS_PROMOTE_MODE -I INSTANCEID | 设置cm_server的升主方式，共有AUTO、PRIMARY_F两种模式，AUTO为默认自选主方式，<font color="red">PRIMARY_F会强制使-I指定的节点升主，无论当前是否有主，因此，有可能会导致cms存在多主情况。</font> |

**表 9** set cm参数 <a id="table9"> </a><a id="table9"> </a>

| 参数                 | 参数说明                                                     |
| :------------------- | :----------------------------------------------------------- |
| --param              | 表明需要设置cm参数，不带此参数则不能执行设置cm参数。         |
| --agent \| --server  | 此参数为必带参数，表明当前需要设置cm_server还是cm_agent的参数。 |
| -k parameter="value" | 指定设置的参数和参数的值，只能设置已经存在的参数，不支持增加或删减参数。 |

**表 10** get参数<a id="table10"> </a>

| 参数                                       | 参数说明                                                     |
| :----------------------------------------- | :----------------------------------------------------------- |
| --log_level=LOG_LEVEL                      | 获取主cm_server日志级别。共分为DEBUG5、DEBUG1、WARNING、LOG、ERROR和FATAL六个级别，日志打印信息级别越来越高。设置日志级别越高，输出日志信息就越少。 |
| --cm_arbitration_mode=ARBITRATION_MODE     | 一主多备功能，获取cm_server的仲裁模式。共有MAJORITY、MINORITY两种模式，MAJORITY为多数派模式，MINORITY为少数派模式。少数派模式适用于一主多备数据库部署并且只有AZ3存活时，此时cm_server可以进行正常的仲裁业务，非此模式下将仲裁模式设置成少数派成功后，cm会自动将仲裁模式改为多数派，以保证集群正常运转；多数派模式适用于一主多备数据库部署并且各个组件（cm_server，节点）存活数量大于一半的场景。数据库实例正常情况下默认为多数派模式。<br/>注意：MogDB不支持少数派 |
| --cm_switchover_az_mode=SWITCHOVER_AZ_MODE | 一主多备功能，获取AZ之间的自动切换开关。共有NON_AUTO、AUTO两种模式，NON_AUTO为非自动切换模式，AUTO为自动切换模式。AUTO模式由主cm_server自动控制AZ1和AZ2之间的节点实例切换。 |

**表 11** view参数<a id="table11"> </a>

| 参数 | 参数说明                                                     |
| :--- | :----------------------------------------------------------- |
| -v   | 显示数据库实例所有节点的静态配置详细信息。<br/>说明：相对于cm_ctl view命令，-v 显示内容增加了cmserver、节点组件的编号显示，如下：cmseverInstanceID，datanodeInstanceID。 |
| -N   | 只显示本节点的静态配置信息，即执行cm_ctl view命令的节点的信息，N表示Native。 |

**表 12** setrunmode参数<a id="table12"> </a>

| 参数      | 参数说明                        | 取值范围                                                     |
| :-------- | :------------------------------ | :----------------------------------------------------------- |
| --xmode   | 指定DCF的运行模式。             | - normal：正常模式。<br/>- minority：少数派模式，需要--votenum指定投票数。 |
| --votenum | 指定DCF少数派运行方式的投票数。 | 正整数，不高于DCF总副本数。                                  |

**表 13** changerole参数<a id="table13"> </a>

| 参数   | 参数说明                                                | 取值范围                                              |
| :----- | :------------------------------------------------------ | :---------------------------------------------------- |
| --role | DCF模式下，将角色为primary的修改为passive或者follower。 | - passive：passive角色。<br/>- follower：follow角色。 |

**表 14** changemember参数<a id="table14"> </a>

| 参数       | 参数说明                                                | 取值范围                                              |
| :--------- | :------------------------------------------------------ | :---------------------------------------------------- |
| --role     | DCF模式下，将角色为primary的修改为passive或者follower。 | - passive：passive角色。<br/>- follower：follow角色。 |
| --group    | DCF模式下，修改group的值。                              | 0~2147483647                                          |
| --priority | DCF模式下，修改priority的值。                           | 0~2147483647                                          |

**表 15** start参数<a id="table15"> </a>

| 参数                                   | 参数说明                                                     |
| :------------------------------------- | :----------------------------------------------------------- |
| --cm_arbitration_mode=ARBITRATION_MODE | 一主多备功能，获取cm_server的仲裁模式。共有MAJORITY、MINORITY两种模式，MAJORITY为多数派模式，MINORITY为少数派模式。少数派模式适用于一主多备数据库部署并且只有AZ3存活时，此时cm_server可以进行正常的仲裁业务，非此模式下将仲裁模式设置成少数派成功后，cm会自动将仲裁模式改为多数派，以保证集群正常运转；多数派模式适用于一主多备数据库部署并且各个组件（cm_server，节点）存活数量大于一半的场景。数据库实例正常情况下默认为多数派模式。<br/>注意：MogDB不支持少数派 |

**表 16** reload 参数<a id="table16"> </a>

| 参数                | 参数说明                                             |
| :------------------ | :--------------------------------------------------- |
| --param             | 表明需要加载cm参数，不带此参数则不能执行加载cm参数。 |
| --agent \| --server | 表明当前需要动态加载cm_server还是cm_agent的参数。    |

**表 17** list参数<a id="table17"> </a>

| 参数                | 参数说明                                                     |
| :------------------ | :----------------------------------------------------------- |
| --param             | 此参数为必带参数，表明需要列出cm参数信息。                   |
| --agent \| --server | 此参数为必带参数，表明当前需要查看cm_server还是cm_agent的参数。 |

**表 18** encrypt参数<a id="table18"> </a>

| 参数 | 参数说明                                             |
| :--- | :--------------------------------------------------- |
| -M   | 指定加密类型，支持server、client。默认类型为server。 |
| -D   | 指定生成的加密密码文件路径。                         |

**表 19** switch参数<a id="table19"> </a>

| 参数             | 参数说明                                                     |
| :--------------- | :----------------------------------------------------------- |
| --ddb_type=[DDB] | 选择需要切换到哪个DDB模式（MogDB只支持DCC模式）。            |
| --commit         | 执行切换会使数据库实例无法选主，需要执行commit恢复数据库实例。 |
| --rollback       | 回滚操作，执行切换失败需要执行回滚操作。                     |

**表 20** ddb参数<a id="table20"> </a>

| 参数                | 参数说明                                                     |
| :------------------ | :----------------------------------------------------------- |
| --put [key] [value] | 往DCC中插入键值对，如果键值对已存在则会修改键key所对应的值value。 |
| --get [key]         | 查询DCC中key对应的value。                                    |
| --delete [key]      | 删除DCC中指定的键值对。                                      |
| --prefix            | get或者delete后添加prefix参数，可以实现模糊匹配查询和删除。  |
| --cluster_info      | 获取数据库实例信息。                                         |
| --leader_info       | 获取主节点信息。                                             |
| --help，-h          | 显示DCC命令帮助信息。                                        |
| --version，-v       | 显示DCC版本信息。                                            |

### 命令参考

- 启动实例：

  ```shell
  cm_ctl start [-z AVAILABILITY_ZONE [--cm_arbitration_mode=ARBITRATION_MODE]] | [-n NODEID [-D DATADIR]] [-t SECS]
  ```

- 数据库主备倒换：

  ```shell
  cm_ctl switchover [-z AVAILABILITY_ZONE] | [-n NODEID -D DATADIR [-f]] | [-a] | [-A] [-t SECS]
  ```

- 所有备机停止回放，每个分片中选择一个强制升主：

  ```shell
  cm_ctl finishredo
  ```

- 重建备节点：

  ```shell
  cm_ctl build -n NODEID -D DATADIR [-t SECS] [-f] [-b full]
  ```

- 检测实例进程运行状态：

  ```shell
  cm_ctl check -B BINNAME -T DATAPATH
  ```

- 停止实例：

  ```shell
  cm_ctl stop [[-z AVAILABILITY_ZONE] | [-n NODEID [-D DATADIR [-R]]]] [-t SECS] [-m SHUTDOWN-MODE]
  ```

- 查询集群状态：

  ```shell
  cm_ctl query [-z ALL] [-l FILENAME] [-v [-C [-s] [-S] [-d] [-i] [-F] [-x] [-p]] | [-r]] [-t SECS] [--minorityAz=AZ_NAME]
  ```

- 查看集群配置文件：

  ```shell
  cm_ctl view [-v | -N | -n NODEID] [-l FILENAME]
  ```

- 设置参数：

  ```shell
  cm_ctl set [--log_level=LOG_LEVEL] [--cm_arbitration_mode=ARBITRATION_MODE] [--cm_switchover_az_mode=SWITCHOVER_AZ_MODE]
  ```

- 设置CM参数：

  ```shell
  cm_ctl set --param --agent | --server [-n NODEID] -k "PARAMETER='value'"
  ```

- 获取参数：

  ```shell
  cm_ctl get [--log_level] [--cm_arbitration_mode] [--cm_switchover_az_mode]
  ```

- 设置DCF投票数：

  ```shell
  cm_ctl setrunmode -n NODEID -D DATADIR  [[--xmode=normal] | [--xmode=minority --votenum=NUM]]
  ```

- 改变dcf角色信息：

  ```shell
  cm_ctl changerole [--role=PASSIVE | --role=FOLLOWER] -n NODEID -D DATADIR [-t SECS]
  ```

- 改变dcf节点属性：

  ```shell
  cm_ctl changemember [--role=PASSIVE | --role=FOLLOWER] [--group=xx] [--priority=xx] -n NODEID -D DATADIR [-t SECS]
  ```

- 动态加载CM参数：

  ```shell
  cm_ctl reload --param [--agent | --server]
  ```

- 列出所有CM参数：

  ```shell
  cm_ctl list --param [--agent | --server]
  ```

- 加密：

  ```shell
  cm_ctl encrypt [-M MODE] -D DATADIR
  ```

- 执行DCC命令行：

  ```shell
  cm_ctl ddb DCC_CMD
  设置：cm_ctl ddb --put [key] [value]
  删除：cm_ctl ddb --delete [key]
  查看DCC命令帮助信息：cm_ctl ddb --help
  ```

- 执行switch ddb命令：

  ```shell
  cm_ctl switch [--ddb_type=[DDB]] [--commit] [--rollback]
  ```

## 安全设计

### 手动替换证书步骤

- 创建自认证证书

自认证证书生成请参考[证书生成](../../security-guide/security/1-client-access-authentication.md#证书生成)。

对私钥密码的保护，使用cm encrypt工具，请参考[cm_ctl工具介绍](#cm_ctl工具介绍)。

```shell
cm_ctl encrypt [-M MODE] -D DATADIR
```

1. 生成服务器端密钥因子server.key.cipher、server.key.rand。

   ```shell
   cm_ctl encrypt -M server -D DATADIR
   please enter the password:（使用密码需要与服务器私钥的保护密码相同）
   ```

2. 生成客户端密钥因子client.key.cipher、client.key.rand。

   ```shell
   cm_ctl encrypt -M client -D DATADIR
   please enter the password:（使用密码需要与客户端私钥的保护密码相同）
   ```

### 证书使用指南

1. 使用证书需配置cm_server参数为on (默认关闭)，

   ```shell
   cm_ctl set --param --server -k enable_ssl="on"
   ```

2. 证书文件需要存在于所有节点的$GAUSSHOME/share/sslcert/cm中，证书替换后重启集群生效。

所需证书文件：server.crt、server.key、client.crt、client.key、cacert.pem、server.key.cipher、server.key.rand、client.key.cipher、client.key.rand

根证书、密钥、证书以及密钥密码加密文件的权限，需保证权限为400。如果权限不满足要求，则无法使用ssl。

- chmod 400 cacert.pem
- chmod 400 server.crt
- chmod 400 server.key
- chmod 400 server.key.cipher
- chmod 400 server.key.rand
- chmod 400 client.crt
- chmod 400 client.key
- chmod 400 client.key.cipher
- chmod 400 client.key.rand

证书有效期的检测周期为1天，可通过ssl_cert_expire_check_interval设置。证书有效期剩余90天时会开始产生告警，可通过[ssl_cert_expire_alert_threshold](../../reference-guide/guc-parameters/cm-parameters/cm_server.md#ssl_cert_expire_alert_threshold)设置。

## CM配置参数介绍

cm_agent相关参数可通过cm_agent数据目录下的cm_agent.conf文件查看，cm_server相关参数可通过cm_server数据目录下的cm_server.conf文件查看。

### cm_agent参数

#### log_dir

**参数说明**: log_dir决定存放cm_agent日志文件的目录。 可以是绝对路径，或者是相对路径（相对于$GAUSSLOG的路径）。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --agent -k log_dir=“'/log/dir'“。

**取值范围**: 字符串，最大长度为1024。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: “log”，表示在$GAUSSLOG下对应的cm目录下生成cm_agent日志。

#### log_file_size

**参数说明**: 控制日志文件的大小。当'cm_agent-xx-current.log'日志文件达到指定大小时，则重新创建一个日志文件记录日志信息。

**取值范围**: 整型，[0, 2047]，实际生效范围[1, 2047]，单位：MB。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 16MB。

#### log_min_messages

**参数说明**: 控制写到cm_agent日志文件中的消息级别。每个级别都包含排在它后面的所有级别中的信息。级别越低，服务器运行日志中记录的消息就越少。

**取值范围**: 枚举类型，有效值有debug5、debug1、warning、error、log、fatal（不区分大小写）。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: warning

#### incremental_build

**参数说明**: 控制重建备节点模式是否为增量。打开这个开关，则增量重建备节点；否则，全量重建备节点。

**取值范围**: 布尔型。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示增量重建备节点。
- off、no、false、0：表示全量重建备节点。

**默认值**: on

#### security_mode

**参数说明**: 控制是否以安全模式启动节点。打开这个开关，则以安全模式启动节点；否则，以非安全模式启动节点。

**取值范围**: 布尔型。修改后可以动态生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示以安全模式启动节点。
- off、no、false、0：表示以非安全模式启动节点。

**默认值**: off

#### upgrade_from

**参数说明**: 就地升级过程中使用，用于标示升级前数据库的内部版本号，此参数禁止手动修改。

**取值范围**: 非负整型，[0, 4294967295]。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 0

#### alarm_component

**参数说明**: 在使用第一种告警方式时，设置用于处理告警内容的告警组件的位置。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --agent -k alarm_component=“'/alarm/dir'“。

**取值范围**: 字符串，最大长度为1024。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: /opt/snas/bin/snas_cm_cmd

#### alarm_report_interval

**参数说明**: 指定告警上报的时间间隔。

**取值范围**: 非负整型，[0, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### agent_heartbeat_timeout

**参数说明**: cm_server心跳超时时间。

**取值范围**: 整型，[2, 2147483647]，单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 5

#### agent_connect_timeout

**参数说明**: cm_agent连接cm_server超时时间。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### agent_connect_retries

**参数说明**: cm_agent连接cm_server尝试次数。

**取值范围**: 整型，[0, 2147483647]。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 15

#### agent_kill_instance_timeout

**参数说明**: 当cm_agent在无法连接cm_server主节点后，发起一次杀死本节点上所有实例的操作之前，所需等待的时间间隔。

**取值范围**: 整型，[0, 2147483647]。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 0，不发起杀死本节点上所有实例的操作。

#### agent_report_interval

**参数说明**: cm_agent上报实例状态的时间间隔。

**取值范围**: 整型，[0, 2147483647]。单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### alarm_report_max_count

**参数说明**: 指定告警上报的最大次数。

**取值范围**: 非负整型，[1, 2592000]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### agent_check_interval

**参数说明**: cm_agent查询实例状态的时间间隔。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 2

#### enable_log_compress

**参数说明**: 控制压缩日志功能。

**取值范围**: 布尔型。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示允许压缩日志。
- off、no、false、0：表示不允许压缩日志。

**默认值**: on

#### process_cpu_affinity

**参数说明**: 控制是否以绑核优化模式启动主节点进程。配置该参数为0，则不进行绑核优化；否则，进行绑核优化，且物理CPU片数为2n个。数据库、cm_agent重启生效。仅支持ARM。参数修改请参考[表 set cm参数](#table9)进行设置。

**取值范围**: 整型，[0, 2]。

**默认值**: 0

#### enable_xc_maintenance_mode

**参数说明**: 在数据库为只读模式下，控制是否可以修改pgxc_node系统表。

**取值范围**: 布尔型。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示开启可以修改pgxc_node系统表功能。
- off、no、false、0：表示关闭可以修改pgxc_node系统表功能。

**默认值**: on

#### log_threshold_check_interval

**参数说明**: cm日志压缩和清除的时间间隔，每1800秒压缩和清理一次。

**生效范围**：\$GAUSSLOG/cm/cm_ctl；\$GAUSSLOG/cm/cm_server；\$GAUSSLOG/cm/om_monitor目录下的文件以及$GAUSSLOG/cm/cm_agent目录下以cm_agent-、system_call-、system_alarm-为前缀的日志文件。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1800

#### log_max_size

**参数说明**: 控制cm日志最大存储值，当CM日志总大小超过（log_max_size*95/100）MB时，根据压缩日志生成时间，依次删除历史压缩日志，直到日志总大小小于（log_max_size*95/100）MB。

**生效范围**：\$GAUSSLOG/cm/cm_ctl；\$GAUSSLOG/cm/cm_server；\$GAUSSLOG/cm/om_monitor目录下的文件以及$GAUSSLOG/cm/cm_agent目录下以cm_agent-、system_call-、system_alarm-为前缀的日志文件。

**取值范围**: 整型，[0, 2147483647]，单位：MB。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10240

#### log_max_count

**参数说明**: cm可存储的最多日志数量，当cm日志总个数超过该值，根据压缩日志文件名时间，删除超过保留天数log_saved_days的压缩日志。

**生效范围**：\$GAUSSLOG/cm/cm_ctl；\$GAUSSLOG/cm/cm_server；\$GAUSSLOG/cm/om_monitor目录下的文件以及$GAUSSLOG/cm/cm_agent目录下以cm_agent-、system_call-、system_alarm-为前缀的日志文件。

**取值范围**: 整型，[0, 10000]，单位：个。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10000

#### log_saved_days

**参数说明**: cm压缩日志保存的天数，cm压缩日志超过该值并且cm日志总个数超过log_max_count，删除压缩日志。

**生效范围**：\$GAUSSLOG/cm/cm_ctl；\$GAUSSLOG/cm/cm_server；\$GAUSSLOG/cm/om_monitor目录下的文件以及$GAUSSLOG/cm/cm_agent目录下以cm_agent-、system_call-、system_alarm-为前缀的日志文件。

**取值范围**: 整型，[0, 1000]，单位天。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 90

#### agent_phony_dead_check_interval

**参数说明**: cm_agent检测进程是否僵死的时间间隔。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10

#### unix_socket_directory

**参数说明**: unix套接字的目录位置。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --agent -k unix_socket_directory=“'/unix/dir'“。

**取值范围**: 字符串，最大长度为1024。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值：”**

#### dilatation_shard_count_for_disk_capacity_alarm

**参数说明**: 扩容场景下，设置新增的扩容分片数，用于上报磁盘容量告警时的阈值计算。

**取值范围**: 整型，[0, 2147483647]，单位：个。该参数设置为0，表示关闭磁盘扩容告警上报；该参数设置为大于0，表示开启磁盘扩容告警上报，且告警上报的阈值根据此参数设置的分片数量进行计算。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### enable_dcf

**参数说明**: DCF模式开关。

**取值范围**: 布尔型。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示启用dcf。
- off、no、false、0：表示不启用dcf。

**默认值**: off

#### disaster_recovery_type

**参数说明**: 主备数据库灾备关系的类型。

**取值范围**: 整型，[0, 2]。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- 0表示未搭建灾备关系。
- 1表示搭建了obs灾备关系。
- 2表示搭建了流式灾备关系

**默认值**: 0

#### agent_backup_open

**参数说明**: 灾备模式设置，开启后CM按照灾备模式运行。

**取值范围**: 整型，[0, 2]。修改后需要重启cm_agent才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- 0表示未搭建灾备关系。
- 1表示搭建了obs灾备关系（之后不再支持）。
- 2表示搭建了流式灾备关系。

**默认值**: 0

### cm_server参数

#### log_dir

**参数说明**: log_dir决定存放cm_server日志文件的目录。 它可以是绝对路径，或者是相对路径（相对于$GAUSSLOG的路径）。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --server -k log_dir=“'/log/dir'“。

**取值范围**: 字符串，最大长度为1024。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: “log”，表示在$GAUSSLOG下对应的cm目录下生成cm_server日志。

#### log_file_size

**参数说明**: 控制日志文件的大小。当'cm_server-xx-current.log'日志文件达到指定大小时，则重新创建一个日志文件记录日志信息。

**取值范围**: 整型，[0, 2047]，实际生效范围[1, 2047]，单位：MB。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 16MB

#### log_min_messages

**参数说明**: 控制写到cm_server日志文件中的消息级别。每个级别都包含排在它后面的所有级别中的信息。级别越低，服务器运行日志中记录的消息就越少。

**取值范围**: 枚举类型，有效值有debug5、debug1、log、warning、error、fatal（不区分大小写）。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: warning

#### thread_count

**参数说明**: cm_server线程池的线程数。

**取值范围**: 整型，[2, 1000]。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1000

#### instance_heartbeat_timeout

**参数说明**: 实例心跳超时时间。

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 6

#### instance_failover_delay_timeout

**参数说明**: cm_server检测到主机宕机，failover备机的延迟时间。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 0

#### cmserver_ha_connect_timeout

**参数说明**: cm_server主备连接超时时间。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 2

#### cmserver_ha_heartbeat_timeout

**参数说明**: cm_server主备心跳超时时间。

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 6

#### cmserver_ha_status_interval

**参数说明**: cm_server主备同步状态信息间隔时间。

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### cmserver_self_vote_timeout

**参数说明**: cm_server之间相互投票的超时时间。旧版本遗留参数，实际不生效。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值：6**

#### phony_dead_effective_time

**参数说明**: 用于数据库节点僵死检测，当检测到的僵死次数大于该参数值，认为进程僵死，将进程重启。

**取值范围**: 整型，[1, 2147483647]，单位：次数。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 5

#### cm_server_arbitrate_delay_base_time_out

**参数说明**: cm_server仲裁延迟基础时长。cm_server主断连后，仲裁启动计时开始，经过仲裁延迟时长后，将选出新的cm_server主。其中仲裁延迟时长由仲裁延迟基础时长、节点index（server ID序号）和增量时长共同决定。公式为：仲裁延迟时长=仲裁延迟基础时长+节点index*仲裁延迟增量时长参数。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10

#### cm_server_arbitrate_delay_incrememtal_time_out

**参数说明**: cm_server仲裁延迟增量时长。cm_server主断连后，仲裁启动计时开始，经过仲裁延迟时长后，将选出新的cm_server主。其中仲裁延迟时长由仲裁延迟基础时长、节点index（server ID序号）和增量时长共同决定。公式为：仲裁延迟时长=仲裁延迟基础时长+节点index*仲裁延迟增量时长参数。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 3

#### alarm_component

**参数说明**: 在使用第一种告警方式时，设置用于处理告警内容的告警组件的位置。参数修改请参考[表 set cm参数](#table9)进行设置。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --server -k alarm_component=“'/alarm/dir'“。

**取值范围**: 字符串，最大长度为1024。修改后需要重启cm_server才能生效。

**默认值**: /opt/snas/bin/snas_cm_cmd

#### alarm_report_interval

**参数说明**: 指定告警上报的时间间隔。

**取值范围**: 非负整型，[0, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 3

#### alarm_report_max_count

**参数说明**: 指定告警上报的最大次数。

**取值范围**: 非负整型，[1, 2592000]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### instance_keep_heartbeat_timeout

**参数说明**: cm_agent会定期检测实例状态并上报给cm_server，若实例状态长时间无法成功检测，累积次数超出该数值，则cm_server将下发命令给agent重启该实例。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 40

#### az_switchover_threshold

**参数说明**: 若一个AZ内节点分片的故障率（故障的节点分片数 / 总节点分片数 * 100%）超过该数值，则会触发AZ自动切换。

**取值范围**: 整型，[1, 100]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 100

#### az_check_and_arbitrate_interval

**参数说明**: 当某个AZ状态不正常时，会触发AZ自动切换，该参数是检测AZ状态的时间间隔。

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 2

#### az_connect_check_interval

**参数说明**: 定时检测AZ间的网络连接，该参数表示连续两次检测之间的间隔时间。

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 60

#### az_connect_check_delay_time

**参数说明**: 每次检测AZ间的网络连接时有多次重试，该参数表示两次重试之间的延迟时间**。**

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 150

#### cmserver_demote_delay_on_etcd_fault

**参数说明**: 因为etcd不健康而导致cm_server从主降为备的时间间隔**。**

**取值范围**: 整型，[1, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 8

#### instance_phony_dead_restart_interval

**参数说明**: 当数据库实例僵死时，会被cm_agent重启，相同的实例连续因僵死被杀时，其间隔时间不能小于该参数数值，否则cm_agent不会下发命令**。**

**取值范围**: 整型，[1800, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 21600

#### enable_transaction_read_only

**参数说明**: 控制数据库是否为只读模式开关。

**取值范围**: 布尔型，有效值有on，off，true，false，yes，no，1，0。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: on

#### datastorage_threshold_check_interval

**参数说明**: 检测磁盘占用的时间间隔。间隔时间由用户指定，表示检测一次磁盘的间隔时间。

**取值范围**: 整型，[1, 2592000]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10

#### datastorage_threshold_value_check

**参数说明**: 设置数据库只读模式的磁盘占用阈值，当数据目录所在磁盘占用超过这个阈值，自动将数据库设置为只读模式。

**取值范围**: 整型，[1, 99]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 85

#### max_datastorage_threshold_check

**参数说明**: 设置磁盘使用率的最大检测间隔时间。当用户手动修改只读模式参数后，会自动在指定间隔时间后开启磁盘检测操作。

**取值范围**: 整型，[1, 2592000]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 43200

#### enable_az_auto_switchover

**参数说明**: AZ自动切换开关，若打开，则表示允许cm_server自动切换AZ。否则当发生节点故障等情况时，即使当前AZ已经不再可用，也不会自动切换到其他AZ上，除非手动执行切换命令。

**取值范围**: 非负整型，0或1，0：开关关闭，1：开关打开。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### cm_krb_server_keyfile

**参数说明**: kerberos服务端key文件所在位置，需要配置为绝对路径。该文件通常为${GAUSSHOME}/kerberos路径下，以keytab格式结尾，文件名与集群运行所在用户名相同。与上述cm_auth_method参数是配对的，当cm_auth_method参数修改为gss时，该参数也必须配置为正确路径，否则将影响集群状态。通过cm_ctl设置绝对路径时需要将路径用”把路径包含起来，例如：cm_ctl set --param --server -k cm_krb_server_keyfile=“'/krb/dir'“。

**取值范围**: 字符串类型，修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: ${GAUSSHOME}/kerberos/{UserName}.keytab，默认值无法生效，仅作为提示。

#### switch_rto

**参数说明**: cm_server强起逻辑等待时延。在force_promote被置为1时，当集群的某一分片处于无主状态开始计时，等待该延迟时间后开始执行强起逻辑。

**取值范围**: 整型，[60, 2147483647]，单位：秒。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 600

#### force_promote

**参数说明**: cm_server是否打开强起逻辑（指集群状态为Unknown的时候以丢失部分数据为代价保证集群基本功能可用）的开关。0代表功能关闭，1代表功能开启。

**取值范围**: 整型，[0, 1]。在cm_server上修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 0

#### backup_open

**参数说明**: 灾备集群设置，开启后CM按照灾备集群模式运行。

**取值范围**: 整型，[0, 1]。修改后需要重启cm_server才能生效。非灾备集群不能开启该参数。参数修改请参考[表 set cm参数](#table9)进行设置。

- 0表示关闭。
- 1表示开启

**默认值**: 0

#### enable_dcf

**参数说明**: DCF模式开关。

**取值范围**: 布尔型。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

- on、yes、true、1：表示启用dcf。
- off、no、false、0：表示不启用dcf。

**默认值**: off

#### ddb_type

**参数说明**: ETCD，DCC模式切换开关。

**取值范围**: 整型。0：ETCD；1：DCC。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> MogDB仅支持DCC模式。

#### enable_ssl

**参数说明**: ssl证书开关。

**取值范围**: 布尔型。打开后使用ssl证书加密通信。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**:

- on、yes、true、1：表示启用ssl。
- off、no、false、0：表示不启用ssl。
- **默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 出于安全性考虑，建议不要关闭该配置。关闭后cm将**不使用**加密通信，所有信息明文传播，可能带来窃听、篡改、冒充等安全风险。

#### ssl_cert_expire_alert_threshold

**参数说明**: ssl证书过期告警时间。

**取值范围**: 整型，[7, 180]，单位：天。证书过期时间少于该时间时，上报证书即将过期告警。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 90

#### ssl_cert_expire_check_interval

**参数说明**: ssl证书过期检测周期。

**取值范围**: 整型，[0, 2147483647]，单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 86400

#### ddb_log_level

**参数说明**: 设置DDB日志级别。

关闭日志：“NONE”，NONE表示关闭日志打印，不能与以下日志级别混合使用。

开启日志：“RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER”日志级别可以从上述字符串中选取字符串并使用竖线组合使用，不能配置空串。

**取值范围**: 字符串，RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: RUN_ERR|RUN_WAR|DEBUG_ERR|OPER|RUN_INF|PROFILE

#### ddb_log_backup_file_count

**参数说明**: 最大保存日志文件个数。

**取值范围**: 整型，[1, 100]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10

#### ddb_max_log_file_size

**参数说明**: 单条日志最大字节数。

**取值范围**: 字符串，长度最大为1024，[1M, 1000M]。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 10M

#### ddb_log_suppress_enable

**参数说明**: 是否开启日志抑制功能。

**取值范围**: 整型，0：关闭； 1：开启。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 1

#### ddb_election_timeout

**参数说明**: DCC 选举超时时间。

**取值范围**: 整型，[1, 600], 单位：秒。修改后可以reload生效，参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**: 3

#### delay_arbitrate_timeout

**参数说明**：设置等待跟主DN同AZ节点redo回放，优先选择同AZ升主的时间。

**取值范围**：整型，[0, 2147483647]，单位：秒。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值**：0

#### cm_auth_method

**参数说明：**CM模块端口认证方式，trust表示未配置端口认证，gss表示采用kerberos端口认证。必须注意的是：只有当kerberos服务端和客户端成功安装后才能修改为gss，否则CM模块无法正常通信，将影响数据库状态。

**取值范围：**枚举类型，有效值有trust, gss。修改后需要重启cm_server才能生效。参数修改请参考[表 set cm参数](#table9)进行设置。

**默认值：**trust

#### dn_arbitrate_mode

**参数说明**：DN仲裁模式。

**取值范围**：字符串。修改后可以reload生效。参数修改请参考[表 set cm参数](#table9)进行设置，share_disk模式下，不建议用户修改仲裁模式。

- quorum
- paxos
- share_disk

**默认值**：quorum