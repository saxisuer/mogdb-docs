---
title: kdestroy
summary: kdestroy
author: Zhang Cuiping
date: 2021-06-07
---

# kdestroy

## 功能介绍

为MogDB认证服务Kerberos提供销毁用户的授权票据操作。

## 参数说明

Kerberos工具为开源第三方提供，具体参数说明请参考Kerberos官方文档：<https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>
