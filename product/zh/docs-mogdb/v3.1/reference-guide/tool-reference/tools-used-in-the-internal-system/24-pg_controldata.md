---
title: pg_controldata
summary: pg_controldata
author: Zhang Cuiping
date: 2021-06-07
---

# pg_controldata

## 功能介绍

pg_controldata工具用来显示数据库目录下control文件的信息。

## 前提条件

确保数据目录正确，以及相应数据目录下的pg_control文件存在。

## 语法

- 查看control文件信息

  ```bash
  pg_controldata DATADIR
  ```

- 显示版本号信息

  ```bash
  pg_controldata -V | --version
  ```

- 显示帮助信息

  ```bash
  pg_controldata -? | --help
  ```

## 参数说明

- DATADIR

  查看control文件信息时所用的参数，DATADIR为实例对应的数据库目录。

- -V, --version

  显示版本信息。

- -?,--help

  打印帮助信息。
