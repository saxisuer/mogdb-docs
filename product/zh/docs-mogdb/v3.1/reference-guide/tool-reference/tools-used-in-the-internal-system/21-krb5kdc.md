---
title: krb5kdc
summary: krb5kdc
author: Zhang Cuiping
date: 2021-06-07
---

# krb5kdc

## 功能介绍

为MogDB认证服务Kerberos提供列认证和秘钥管理服务。

## 参数说明

Kerberos工具为开源第三方提供，具体参数说明请参考Kerberos官方文档：<https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>
