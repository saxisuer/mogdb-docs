---
title: 事务控制
summary: 事务控制
author: Zhang Cuiping
date: 2021-05-17
---

# 事务控制

事务是用户定义的一个数据库操作序列，这些操作要么全做要么全不做，是一个不可分割的工作单位。

## 启动事务

MogDB通过START TRANSACTION和BEGIN语法启动事务，请参考[START TRANSACTION](../../reference-guide/sql-syntax/START-TRANSACTION.md)和[BEGIN](../../reference-guide/sql-syntax/BEGIN.md)。

## 设置事务

MogDB通过SET TRANSACTION或者SET LOCAL TRANSACTION语法设置事务，请参考[SET TRANSACTION](../../reference-guide/sql-syntax/SET-TRANSACTION.md)。

## 提交事务

MogDB通过COMMIT或者END可完成提交事务的功能，即提交事务的所有操作，请参考[COMMIT | END](../../reference-guide/sql-syntax/COMMIT-END.md)。

## 回滚事务

回滚是在事务运行的过程中发生了某种故障，事务不能继续执行，系统将事务中对数据库的所有已完成的操作全部撤销。请参考[ROLLBACK](../../reference-guide/sql-syntax/ROLLBACK.md)。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
> 数据库中收到的一次执行请求（不在事务块中），如果含有多条语句，将会被打包成一个事务，如果其中有一个语句失败，那么整个请求都将会被回滚。
