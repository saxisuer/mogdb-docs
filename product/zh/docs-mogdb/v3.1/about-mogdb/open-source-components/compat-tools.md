---
title: compat-tools
summary: compat-tools
author: Zhang Cuiping
date: 2021-07-14
---

# compat-tools

本项目是一个兼容工具集合，旨在为从其他异构数据库，迁移到 MogDB 之后的系统，创建必要的函数，以及系统视图的兼容。为后续的系统运维与应用改造提供便利。

脚本中带有版本控制，直接运行脚本时，会根据如下三种情况进行处理：

1. 若待创建对象在目标数据库中不存在，则直接进行创建
2. 若待创建对象版本高于目标数据库中的对象版本，则进行升级重建
3. 若待创建对象版本不高于目标数据库中的对象版本，则跳过创建

组件获取及使用方法详见[compat-tools仓库页面](https://gitee.com/enmotech/compat-tools)。
