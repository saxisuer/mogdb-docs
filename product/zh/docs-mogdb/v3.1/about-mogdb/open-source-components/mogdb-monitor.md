---
title: mogdb-monitor
summary: mogdb-monitor
author: Guo Huan
date: 2022-04-14
---

# mogdb-monitor

mogdb-monitor是MogDB数据库集群监控部署工具，借助当前非常流行的开源监控体系prometheus框架，结合由恩墨数据库团队开发的opengauss_exporter，可以实现对MogDB数据库进行全方位的检测。

核心监控组件opengauss_exporter具有以下特点：

- 支持所有版本MogDB/openGauss数据库
- 支持监控数据库集群
- 支持集群内主备角色判断
- 支持自动发现数据库
- 支持自定义查询query
- 支持在线加载配置文件
- 支持配置线程并发数
- 支持数据采集信息缓存

在grafana展示方面，云和恩墨也提供了一套完整的dashboard，既有展示每个实例详细信息的实例级dashboard，也有展示所有实例汇总信息的展示大屏，结合alertmanager组件，可以第一时间将符合规则的报警触发到相关人员。

<br/>

组件获取及使用方法详见[mogdb-monitor仓库页面](https://gitee.com/enmotech/mogdb-monitor)。