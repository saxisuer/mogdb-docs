---
title: mog_filedump使用说明
summary: mog_filedump使用说明
author: Guo Huan
date: 2021-11-15
---



# mog_filedump使用说明

## 简介

mog_filedump是根据pg_filedump工具改进兼容性，移植到MogDB上的解析数据文件工具，用于将MogDB的heap/index/control文件转换成用户可读的格式内容。本工具能够根据需要，解析数据列中一部分字段，也能直接以二进制格式转储数据内容。该工具可以通过文件中块的数据，自动确定文件的类型。要格式化pg_control文件必须使用-c选项。

<br/>

## 原理说明

实现步骤主要分三步：

1. 读取数据文件中的数据块。

2. 以对应数据类型的回调函数，解析对应类型的数据。

3. 调用对应的数据类型函数输出，打印数据内容。

<br/>

## 恩墨的改进

1. 对MogDB做了兼容性移植。

2. 修复官方bug：数据类型char的解析bug。

3. 修复官方bug：在多字段场景下，解析数据文件，数据类型name会产生数据长度不对齐bug。

<br/>

## 安装方法

访问[MogDB官网下载页面](https://www.mogdb.io/downloads/mogdb)下载对应版本的工具包，将工具放在MogDB安装路径的bin目录下即可。如下图所示，toolkits-xxxxxx.tar.gz即为包含mog_filedump的工具包。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/open-source-components-3.png)

<br/>

## 使用方法

```
mog_filedump [-abcdfhikxy] [-R startblock [endblock]] [-D attrlist] [-S blocksize] [-s segsize] [-n segnumber] file
```

对heap和index文件有效的选项如下：

| 选项  | 功能                                                         |
| ---- | ------------------------------------------------------------ |
| -a   | 显示绝对路径                                                 |
| -b   | 输出一个范围的二进制block images                             |
| -d   | 输出文件块内容                                               |
| -D   | 表的数据类型。<br/>目前支持的数据类型有：bigint、bigserial、bool、charN、date、float、float4、float8、int、json、macaddr、name、oid、real、serial、smallint、smallserial、text、time、timestamp、timetz、uuid、varchar、varcharN、xid、xml、~。 <br/>'~'表示忽略后面所有的数据类型，比如元组有10列，`-D 前三列数据类型, ~`表示只解析该表元组前三列数据。 |
| -f   | 输出并解析数据块的内容                                       |
| -h   | 显示使用说明和帮助信息                                       |
| -i   | 输出并解析item的详细信息（其中包含XMIN、XMAX、Block Id、linp Index、Attributes、Size、infomask） |
| -k   | 核实数据块的校验和                                           |
| -R   | 解析并输出指定LSN范围的数据文件内容，例如 -R startblock [endblock]。如果只有startblock没有endblock，则只输出单个数据块内容 |
| -s   | 设置segment大小                                             |
| -n   | 设置segment数量                                             |
| -S   | 设置数据块大小                                               |
| -x   | 把块items当作索引item格式解析并输出（默认自带）              |
| -y   | 把块items当作堆item格式解析并输出（默认自带）                |

对控制文件有效的选项如下：

| 选项 | 功能                     |
| ---- | ------------------------ |
| -c   | 解析控制文件的列表目录   |
| -f   | 输出并解析数据块的内容   |
| -S   | 设置控制文件解析的块大小 |

可以结合-i和-f参数，得到更有效的数据以帮助运维人员分析与参考。

<br/>

## 示例

test表基本上覆盖了mog_filedump所包含的数据类型。

此处是展示数据解析功能的用例，其它参数请根据实际具体需要添加使用。

```sql
-- 创建表test:
create table test(serial serial, smallserial smallserial, bigserial bigserial, bigint bigint, bool bool, char char(3), date date, float float, float4 float4, float8 float8, int int, json json, macaddr macaddr, name name, oid oid, real real, smallint smallint, text text, time time, timestamp timestamp, timetz timetz, uuid uuid, varchar varchar(20), xid xid, xml xml);

-- 插入数据：
insert into test(bigint, bool, char, date, float, float4, float8, int, json, macaddr, name, oid, real, smallint, text, time, timestamp, timetz, uuid, varchar, xid, xml) values(123456789, true, 'abc', '2021-4-02 16:45:00', 3.1415926, 3.1415926, 3.14159269828412, 123456789, '{"a":1, "b":2, "c":3}'::json, '04-6C-59-99-AF-07', 'lvhui', 828243, 3.1415926, 12345, 'text', '2021-04-02 16:48:23', '2021-04-02 16:48:23', '2021-04-02 16:48:23', 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 'adsfghkjlzc', '9973::xid', '<title>Book0001</title>');

-- 查询表test的数据文件所在目录。该处gs_initdb指定的数据目录为db_p。所以表test数据文件在db_p/base/15098/32904
postgres=# select pg_relation_filepath('test');
base/15098/32904 (1 row)

-- 用mog_filedump工具解析数据文件内容：
./mog_filedump -D serial,smallserial,bigserial,bigint,bool,charN,date,float,float4,float8,int,json,macaddr,name,oid,real,smallint,text,time,timestamp,timetz,uuid,varchar,xid,xml db_p/base/15098/32904
```

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/mog_filedump.png)
