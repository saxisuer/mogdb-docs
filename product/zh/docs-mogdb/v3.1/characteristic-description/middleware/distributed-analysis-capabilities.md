---
title: 分布式分析能力
summary: 分布式分析能力
author: zhang cuiping
date: 2022-10-13
---

# 分布式分析能力

## 可获得性

本特性自MogDB 3.1.0版本开始引入。

## 特性简介

基于openLookeng实现分布式分析能力，与shardingsphere配合MogDB组成HTAP数据库。

## 客户价值

通过openLookeng快速实现海量数据分析。

## 特性描述

openLookeng复用shardingsphere中间件的分库分表能力，使openLookeng可以获取海量数据进行分析运算。

## 特性增强

无。

## 特性约束

无。

## 依赖关系

openLookeng中间件、shardingsphere中间件。