---
title: 数据加密存储
summary: 数据加密存储
author: Guo Huan
date: 2022-05-07
---

# 数据加密存储

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

提供对导入数据的加密存储。

## 客户价值

为客户提供加密导入接口，对客户认为是敏感信息的数据进行加密后存储在表内。

## 特性描述

MogDB提供加密函数gs_encrypt_aes128()、gs_encrypt()和解密函数gs_decrypt_aes128()、gs_decrypt()接口。通过加密函数，可以对需要输入到表内的某列数据进行加密后再存储到表格内。调用格式为:

**gs_encrypt_aes128(column, key),gs_encrypt(decryptstr,keystr,decrypttype)**

其中key为用户指定的初始口令，用于派生加密密钥。当客户需要对整张表进行加密处理时，则需要为每一列单独书写加密函数。

当具有对应权限的用户需要查看具体的数据时，可通过解密函数接口对相应的属性列进行解密处理，调用格式为：

**gs_decrypt_aes128(column, key),gs_decrypt(decryptstr,keystr,decrypttype)**

## 特性增强

无。

## 特性约束

无。

## 依赖关系

无。