---
title: 插件拆分
summary: 插件拆分
author: Guo Huan
date: 2022-11-21
---

# 插件拆分

## 可获得性

本特性自MogDB 3.1.0版本开始引入。

## 特性简介

MogDB 3.1版本中的插件将独立发布，每个插件均可以从官网下载到server与插件对应的单独版本，便于用户安装最新的插件包，且各个插件可以单独升级。

此外，Dolphin MySQL兼容插件集成使用，和MogDB server一起发布不做单独打包，无需任何安装命令，创建B兼容性库，即可默认实现MySQL兼容的所有功能。

## 客户价值

| 用户角色 | 功能实现                                                     |
| -------- | ------------------------------------------------------------ |
| 交付     | 根据实际需求，通过安装脚本将各个插件安装到对应目录。每个插件均可以从官网下载到server与插件对应的单独版本。 |
| 运维     | 插件均有对应md5校验一致性，确保安装的插件版本和二进制与操作系统对应。 |
| 运维     | 插件可以单独升级，完成bug fix或者功能增加。                  |
| 运维     | Dolphin插件集成使用，创建B兼容性库，即可体验到完整的MySQL兼容性。 |

## 特性描述

**插件列表**：

1. Orafce：oracle兼容插件

2. Pg_bulkload：数据批量导入插件

3. Pg_prewarm：数据预热插件

4. Pg_repack：无锁vacuum插件

5. Postgis：空间数据插件

6. Wal2json：逻辑复制插件

7. Db_link：同构数据库连接插件

8. Pg_trgm：全文搜索插件

9. Whale：Oracle兼容插件（MogDB版，非openGauss版）

**fdw类**：

1. Postgresql_fdw ：支持PostgreSQL/openGauss的fdw插件，采用openGauss自身libpq打包。

2. Oracle_fdw：支持Oracle的fdw插件，采用Oracle 19c打包，提供arm/x86的兼容包。

3. MySQL_fdw：支持MySQL的fdw插件，采用MySQL 8.0客户端包。

**和server一起发布不做单独打包**：

1. Dolphin MySQL兼容插件

## 特性约束

1. 对于需要server内置的插件（3.1开始，MySQL（dolphin）与Oracle兼容插件（whale）），以及server自带的插件（db_link、pg_trgm）会与server一起打包发布，初次部署不受限制。

2. 暂不考虑热升级插件。

3. 暂不考虑卸载插件。

4. 插件命名规则：{插件名称}-{插件社区版本号}-{server大版本号}-{支持的操作系统}-{CPU架构}.tar.gz

## 相关页面

[插件获取](https://www.mogdb.io/downloads/mogdb/)