---
title: 系统KPI辅助诊断
summary: 系统KPI辅助诊断
author: Guo Huan
date: 2022-05-07
---

# 系统KPI辅助诊断

## 可获得性

本特性自MogDB 1.1.0 版本开始引入。

## 特性简介

KPI是内核组件或者整体性能关键指标的视图呈现，基于这些指标，用户可以了解到系统运行的实时或者历史状态。

## 客户价值

- 系统负载概要诊断

  系统负载异常（过载、失速、业务SLA）精准告警，系统负载精准画像。

- 系统时间模型概要诊断

  Instance和Query级别时间模型细分，诊断Instance和Query性能问题根因。

- Query性能诊断

  数据库级Query概要信息，TopSQL，SQL CPU，IO消耗，执行计划，硬解析过多。

- 磁盘IO、索引、buffer性能问题

- 连接池，线程池异常

- Checkpoint，Redo（RTO）性能问题

- 系统I/O、LWLock、Waits性能问题诊断

  诊断60+模块，240+关键操作性能问题。

- 函数级性能看护诊断（GSTRACE），功能诊断

  50+存储和执行层函数trace。

## 特性描述

MogDB提供涵盖11大类，26个子类的KPI，包括：Instance、File、Object、Workload、Communication、Session、Thread、Cache IO、Lock、Wait Event、Cluster。

KPI指标内核的分布如图1所示。

**图 1** KPI指标内核分布

 ![KPI指标内核分布](https://cdn-mogdb.enmotech.com/docs-media/mogdb/characteristic-description/system-kpi-aided-diagnosis.png)

## 特性增强

无。

## 特性约束

无。

## 依赖关系

无。