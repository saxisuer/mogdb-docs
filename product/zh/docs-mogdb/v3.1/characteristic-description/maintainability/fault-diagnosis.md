---
title: 故障诊断
summary: 故障诊断
author: Zhang Cuiping
date: 2022-06-17
---

# 故障诊断

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

为了快速定位故障、收集系统故障信息、导出故障数据，进而修复故障，MogDB 3.0增强了OM功能以及gstrace诊断能力。

## 客户价值

故障诊断能力的增强，可以方便研发人员及时修复故障，保证系统的正常运行。

## 特性描述

gs_check工具可以对场景检查结果进行差异比较，并输出差异分析报告，帮助用户快速定位问题。

gs_watch工具可监测MogDB进程，当发现进程崩溃时，自动触发调用gs_collector对系统状态进行收集，以便后期分析。

gs_gucquery工具可自动收集整理并导出GUC值，比较不同时刻GUC值的变化。

gstrace诊断能力增强。支持按模块名和方法名打开一个或多个component（模块）和function（方法）的trace项；提升代码中gstrace点数量，增强gstrace输出信息表达能力；支持新的对关键数据结构PGPROC和user session数据的导出；实现故障注入，包括系统调用报错模拟和变量内容攒写。

## 相关页面

[gs_check](../../reference-guide/tool-reference/server-tools/1-gs_check.md)，[gs_gucquery](../../reference-guide/tool-reference/server-tools/gs_gucquery.md)，[gstrace](../../reference-guide/tool-reference/tools-used-in-the-internal-system/16-gstrace.md)，[gs_watch](../../reference-guide/tool-reference/server-tools/gs_watch.md)
