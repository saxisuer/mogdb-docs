---
title: 支持WDR诊断报告
summary: 支持WDR诊断报告
author: Guo Huan
date: 2022-05-07
---

# 支持WDR诊断报告

## 可获得性

本特性自MogDB 1.1.0 版本开始引入。

## 特性简介

WDR报告提供数据库性能诊断报告，该报告基于基线性能数据和增量数据两个版本，从性能变化得到性能报告。

## 客户价值

- WDR报表是长期性能问题最主要的诊断手段。基于SNAPSHOT的性能基线，从多维度做性能分析，能帮助DBA掌握系统负载繁忙程度、各个组件的性能表现及性能瓶颈。
- SNAPSHOT也是后续性能问题自诊断和自优化建议的重要数据来源。

## 特性描述

WDR（Workload Diagnosis Report）基于两次不同时间点系统的性能快照数据，生成这两个时间点之间的性能表现报表，用于诊断数据库内核的性能故障。

使用generate_wdr_report(…) 可以生成基于两个性能快照的性能报告。

WDR主要依赖两个组件：

- SNAPSHOT性能快照：性能快照可以配置成按一定时间间隔从内核采集一定量的性能数据，持久化在用户表空间。任何一个SNAPSHOT可以作为一个性能基线，其他SNAPSHOT与之比较的结果，可以分析出与基线的性能表现。
- WDR Reporter：报表生成工具基于两个SNAPSHOT，分析系统总体性能表现，并能计算出更多项具体的性能指标在这两个时间段之间的变化量，生成SUMMARY 和DETAIL两个不同级别的性能数据。如表1、表2所示。

**表 1** SUMMARY级别诊断报告

| 诊断类别                        | 描述                                                         |
| :------------------------------ | :----------------------------------------------------------- |
| Database Stat                   | 主要用于评估当前数据库上的负载和IO状况，负载和IO状况是衡量TP系统最重要的特性。包含当前连接到该数据库的session，提交、回滚的事务数，读取的磁盘块的数量，高速缓存中已经发现的磁盘块的次数，通过数据库查询返回、抓取、插入、更新、删除的行数，冲突、死锁发生的次数，临时文件的使用量，IO读写时间等。 |
| Load Profile                    | 从时间，IO，事务，SQL几个维度评估当前系统负载的表现。包含作业运行elapse time、CPU time，事务日质量，逻辑和物理读的量，读写IO次数、大小，登入登出次数，SQL、事务执行量，SQL P80、P95响应时间等。 |
| Instance Efficiency Percentages | 用于评估当前系统的缓存的效率。主要包含数据库缓存命中率。     |
| Events                          | 用于评估当前系统内核关键资源，关键事件的性能。主要包含数据库内核关键事件的发生次数，事件的等待时间。 |
| Wait Classes                    | 用于评估当前系统关键事件类型的性能。主要包含数据内核在主要的等待事件的种类上的发布：STATUS、LWLOCK_EVENT、LOCK_EVENT、IO_EVENT。 |
| CPU                             | 主要包含CPU在用户态、内核态、Wait IO、空闲状态下的时间发布。 |
| IO Profile                      | 主要包含数据库Database IO次数、Database IO数据量、Redo IO次数、Redo IO量。 |
| Memory Statistics               | 包含最大进程内存、进程已经使用内存、最大共享内存、已经使用共享内存大小等。 |

**表 2** DETAIL级别诊断报告

| 诊断类别               | 描述                                                         |
| :--------------------- | :----------------------------------------------------------- |
| Time Model             | 主要用于评估当前系统在时间维度的性能表现。包含系统在各个阶段上消耗的时间：内核时间、CPU时间、执行时间、解析时间、编译时间、查询重写时间、计划生成时间、网络时间、IO时间。 |
| SQL Statistics         | 主要用于SQL语句性能问题的诊断。包含归一化的SQL的性能指标在多个维度上的排序：Elapsed Time、CPU Time、Rows Returned、Tuples Reads、Executions、Physical Reads、Logical Reads。这些指标的种类包括：执行时间，执行次数、行活动、Cache IO等。 |
| Wait Events            | 主要用于系统关键资源，关键时间的详细性能诊断。包含所有关键事件在一段时间内的表现，主要是事件发生的次数，消耗的时间。 |
| Cache IO Stats         | 用于诊断用户表和索引的性能。包含所有用户表、索引上的文件读写，缓存命中。 |
| Utility status         | 用于诊断后端作业性能的诊断。包含页面操作，复制等后端操作的性能。 |
| Object stats           | 用于诊断数据库对象的性能。包含用户表、索引上的表、索引扫描活动，insert、update、delete活动，有效行数量，表维护操作的状态等。 |
| Configuration settings | 用于判断配置是否有变更。包含当前所有配置参数的快照。         |
| SQL detail             | 显示unique query text信息。                                  |

## 特性增强

无。

## 特性约束

- WDR snapshot性能快照会采集不同database的性能数据，如果数据库实例中有大量的database或者大量表，做一次WDR snapshot会花费很长时间。
- 如果在大量DDL期间做WDR snapshot可能造成WDR snapshot失败。
- 在drop database时，做WDR snapshot可能造成WDR snapshot失败。

## 依赖关系

无。