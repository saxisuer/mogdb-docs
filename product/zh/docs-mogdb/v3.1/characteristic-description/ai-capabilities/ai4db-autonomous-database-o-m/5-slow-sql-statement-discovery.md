---
title: 慢SQL发现
summary: 慢SQL发现
author: Guo Huan
date: 2022-05-10
---

# 慢SQL发现

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

本功能是一个SQL语句执行时间预测工具，通过模板化方法，实现在不获取SQL语句执行计划的前提下，依据语句逻辑相似度与历史执行记录，预测SQL语句的执行时间。

## 客户价值

- 工具不需要用户提供SQL执行计划，对数据库性能不会有任何影响。
- 不同于业内其他算法只局限于OLAP或者OLTP，本工具场景更加广泛。

## 特性描述

SQLdiag着眼于数据库的历史SQL语句，通过对历史SQL语句的执行表现进行总结归纳，将之再用于推断新的未知业务上。由于短时间内数据库SQL语句执行时长不会有太大的差距，SQLdiag可以从历史数据中检测出与已执行SQL语句相似的语句结果集，并基于SQL向量化技术和模板化方法预测SQL语句执行时长。

## 特性增强

无

## 特性约束

- 需要保证用户提供的历史日志及待预测负载的格式符合要求，可以使用数据库GUC参数开启收集，也可以通过监控工具采集。
- 为保证预测准确率，用户提供的历史语句日志应尽可能全面并具有代表性。
- 按照要求配置python环境。

## 依赖关系

无