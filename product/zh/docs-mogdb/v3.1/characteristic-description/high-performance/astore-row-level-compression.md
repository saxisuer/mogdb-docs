---
title: Astore行级压缩性能增强
summary: Astore行级压缩性能增强
author: Guo Huan
date: 2022-11-10
---

# Astore行级压缩性能增强

## 可获得性

本特性自MogDB 3.1.0版本开始引入。

## 特性简介

MogDB 3.1完全继承了MogDB 3.0相关压缩SQL语法，包括create table、alter table、vacuum、 copy、主备复制等，在压缩特性的内部实现上做了较大的提升和优化，相对于MogDB 3.0，在性能相当的前提下，实现更高的压缩率。

## 客户价值

| 目的                                                  | 结果                                           |
| ----------------------------------------------------- | ---------------------------------------------- |
| 创建压缩表                                            | 节约存储成本，提升性能                         |
| 修改表压缩属性                                        | 灵活配置表压缩开关，适配各个场景用户           |
| 对压缩表进行读写操作，读写过程中系统自动完成压缩/解压 | 读写表过程中的压缩/解压动作透明                |
| 压缩表主备同步                                        | 压缩表在主备节点上表现相同                     |
| 压缩表能正常进行vacuum full操作                       | Vacuum full之后压缩表能够保持正常的压缩状态    |
| Copy到压缩表，自动完成压缩                            | Copy操作在前台完成压缩，导入数据随即完成压缩   |
| 展示系统压缩状态                                      | 支持通过GS_COMPRESSION视图查看每个表的压缩情况 |

## 特性描述

### 1. 创建压缩表

通过SQL语句[CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md)新增行级压缩相关参数，来新建压缩表，包括普通表、分区表（一级和二级分区表）、临时表、unlogged表、物化视图），压缩表创建成功后，对该表进行写操作，数据库系统会自动进行压缩操作，压缩效果对用户透明；用户可以通过获取表相关属性，来判断一个表是否是压缩表。

**特性约束**

- 该特性仅对Astore行存表生效，不能用于Ustore行存表、列存表和MOT；
- 压缩表创建完成后，对该表的其他操作，如DDL、DML等操作对用户透明；
- 默认创建非压缩表；
- 不能为系统表指定压缩属性；
- 不能为外表指定压缩属性；
- 不支持tablespace压缩属性。

### 2. 修改压缩表

通过SQL语句[ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md)将非压缩表修改为压缩表，或者将压缩表修改为非压缩表。

**特性约束**

- 通过ALTER TABLE新增压缩属性后，新增数据会进行压缩，但该表的存量数据不会立即压缩，而是由后台任务异步完成压缩；

- 通过ALTER TABLE取消压缩属性后，新增数据不再压缩，存量数据保持压缩状态。

- 表和索引的压缩属性完全独立，各自通过相关语法进行设置；

### 3. 读/写压缩表

压缩表在读写流程中，系统自动完成压缩和解压缩，过程对用户透明。

### 4. 压缩表同步

支持压缩表的主备间同步，主备数据一致。

### 5. 压缩表的vacuum/vacuum full操作

支持压缩表过期版本回收，过期数据能够被正确清理，剩余数据正确。

### 6. 数据导入

通过copy from命令（包括并行导入）将数据导入到压缩表中，数据同步完成压缩，典型场景压缩率达2~4倍，copy性能损失低于20%。

### 7. 压缩表视图

新增[GS_COMPRESSION](../../reference-guide/system-catalogs-and-system-views/system-views/GS_COMPRESSION.md)视图，通过该视图能够观测到系统所有压缩表的压缩情况。

## 相关页面

[CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md)、[ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md)、[VACUUM](../../reference-guide/sql-syntax/VACUUM.md)、[COPY](../../reference-guide/sql-syntax/COPY.md)、[GS_COMPRESSION](../../reference-guide/system-catalogs-and-system-views/system-views/GS_COMPRESSION.md)