---
title: SQL trace观测功能
summary: SQL trace观测功能
author: Guo Huan
date: 2022-11-15
---

# SQL trace观测功能

## 可获得性

本特性自MogDB 3.1.0版本开始引入。

## 特性简介

MogDB SQL trace，是可观测性增强的高级功能，可以在MogDB数据库运行的情况下提供对SQL运行的所有过程进行动态导出和观测的能力。在可观测性的范畴内，提升在关键业务系统不停机的状态下进行观测和故障诊断的能力。

## 客户价值

gstrace SQL trace能让技术支持人员在数据库生产不停库的情况下，导出实际运行状况下的所有SQL执行细节，并用于之后的分析及诊断，进而提高观测性能力和故障诊断效率。

## 特性描述

SQL trace在已有的gstrace模块上增加一项功能，可以在用户指定的一段时间内追踪SQL运行过程和内容。 用户可以开启、导出、关闭gstrace的SQL trace功能。SQL trace导出的内容包括机器和数据库相关的基本信息、PARSE（解析）、EXEC（执行）语句的总执行时间，以及主要算子SCAN、SORT AGGREGATE的执行时间、执行条数等。

## 相关页面

[gstrace](../../reference-guide/tool-reference/tools-used-in-the-internal-system/16-gstrace.md)