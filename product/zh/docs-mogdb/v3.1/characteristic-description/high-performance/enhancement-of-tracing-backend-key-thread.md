---
title: 后台关键线程trace观测增强
summary: 后台关键线程trace观测增强
author: Guo Huan
date: 2022-11-14
---

# 后台关键线程trace观测增强

## 可获得性

本特性自MogDB 3.1.0版本开始引入。

## 特性简介

MogDB 3.1版本针对walwriter、pagewriter以及checkpointer 3个模块（每个模块代表了一个关键线程）打开trace功能。当调试人员认为后台某个线程运行不正常时，可以指定gstrace追踪该模块以及模块中的某些函数。

## 客户价值

在生产库运行的情况下，用户可以指定gstrace以模块的形式追踪3个新增后台关键线程。以此得到更有针对性的追踪结果，快速定位问题，提高诊断效率。

## 特性描述

在已有的gstrace针对模块打开trace的功能下，增加三个trace模块（walwriter模块、 pagewriter模块和checkpointer模块），允许用户使用`gstrace start`追踪这三个模块，并指定追踪模块提前定义好的与后台线程相关的一个、多个或所有函数。需要注意的是某些函数会插入了若干个data probe点位来帮助后期的观测与诊断。

通过gstrace的开启、导出、关闭，用户可以指定从开启gstrace到执行导出这个时间段内，追踪指定的模块。

当调试人员认为后台某个线程运行不正常时，可以指定gstrace只追踪该模块以及模块中提前定义好的某个或多个函数。通过`gstrace dump`得到用户指定时间段内，模块中已追踪函数的执行路径（调用栈方式显示），以及追踪函数中关键数据或数据结构的具体信息。通过此方法可以得到更有针对性的追踪结果，快速定位问题，提高诊断效率。

## 相关页面

[gstrace](../../reference-guide/tool-reference/tools-used-in-the-internal-system/16-gstrace.md)