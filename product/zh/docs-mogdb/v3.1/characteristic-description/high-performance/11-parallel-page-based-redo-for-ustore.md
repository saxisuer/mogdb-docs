---
title: Parallel Page-based Redo For Ustore
summary: Parallel Page-based Redo For Ustore
author: Guo Huan
date: 2022-05-07
---

# Parallel Page-based Redo For Ustore

## 可获得性

本特性自MogDB 1.1.0 版本开始引入。

## 特性简介

优化Ustore Inplace Update WAL log写入，Ustore DML Operation回放提高并行度。

## 客户价值

对于Update的WAL使用空间减少，Ustore DML Operation回放提高并行度。

## 特性描述

通过利用Prefix和suffix来减少update WAL log的写入，通过把回放线程分多个类型来解决Ustore DML WAL大多都是多页面回放问题；同时把Ustore的数据页面回放按照blkno去分发。

## 特性增强

无。

## 特性约束

无。

## 依赖关系

依赖于Ustore引擎。