---
title: BTree 索引压缩
summary: BTree 索引压缩
author: zhang cuiping
date: 2022-11-10
---

# BTree 索引压缩

## 可获得性

本特性自MogDB 3.1.0 版本开始引入。

## 特性简介

BTree 索引压缩特性通过对索引数据进行去重压缩，压缩后的索引数据加载到内存，减少了内存占用空间，同时一次可加载更多的索引数据。

![image](https://cdn-mogdb.enmotech.com/docs-media/mogdb/characteristic-description/btree-index-compression-1.png)

## 客户价值

BTree 索引压缩特性提供一种有效索引存储机制。可以在系统内存配比不高的场景下，随着系统业务负载不断增加，用户业务的数据查询效率不会受到大幅度影响。

## 特性描述

为了加速数据查询效率，一般会为数据表列创建索引。本特性采用BTree索引存储结构，将索引数据进行压缩存储于内存中，保证系统业务数据的查询效率。特别适用于用户数据表一个列或多个列存在大量重复数据的索引创建和索引数据去重压缩。

本特性支持非唯一索引数据的去重压缩处理。兼容唯一索引，只是没有压缩效果。

## 特性增强

无。

## 特性约束

- 不支持include index类型索引
- 不支持系统表索引
- 只支持BTree索引类型

## 依赖关系

无。