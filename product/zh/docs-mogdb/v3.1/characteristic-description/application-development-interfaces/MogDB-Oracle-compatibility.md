---
title: 兼容Oracle
summary: 兼容Oracle
author: Zhang Cuiping
date: 2022-06-17
---

# 兼容Oracle

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

MogDB使用whale插件兼容Oracle的相关函数以及package功能。

函数部分，主要新增instrb，nls_charset_id，nls_charset_name，nls_lower，nls_upper， ora_hash， remainder，replace，show，show_parameter，to_timestamp， to_yminterval，tz_offset，nullif，ratio_to_report等。

package一般只用于存储过程中，根据ORACLE数据的package规则，新package放到对应的schema下。支持的Oracle管理包有dbms_random，dbms_output，dbms_lock，dbms_application_info，dbms_metadata，dbms_job，dbms_utility。

有关函数以及package详情，请参见[whale插件](../../reference-guide/oracle-plugins/whale.md)。

## 客户价值

通过使用whale插件，增强MogDB与Oracle的兼容性，增强MogDB的功能。

## 相关页面

[whale插件](../../reference-guide/oracle-plugins/whale.md)，[字符处理函数和操作符](../../reference-guide/functions-and-operators/3-character-processing-functions-and-operators.md)，[数字操作函数和操作符](../../reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md)，[时间和日期处理函数和操作符](../../reference-guide/functions-and-operators/8-date-and-time-processing-functions-and-operators.md)，[HLL函数和操作符](../../reference-guide/functions-and-operators/13.1-hll-functions-and-operators.md)，[窗口函数](../../reference-guide/functions-and-operators/18-window-functions.md)，[系统信息函数](../../reference-guide/functions-and-operators/23-system-information-functions.md)