---
title: SQL运行状态观测
summary: SQL运行状态观测
author: Guo Huan
date: 2022-06-17
---

# SQL运行状态观测

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

采样线程默认1s采样一次，可通过guc参数asp_sample_interval控制，内存中最多采样100000行数据，通过asp_sample_num控制，到达上限就会刷新到磁盘上。

## 客户价值

当发现某条SQL语句存在性能瓶颈，且无法在采样视图中查询到该SQL每个算子的执行情况时，可以通过plan_node_id定位性能问题。

算子：SQL语句执行过程中各个步骤的具体动作（例如：SELECT、SUM、WHERE、Group By、Having、Order By、Limit等）

## 特性描述

在[dbe_perf.local_active_session](../../reference-guide/schema/DBE_PERF/session-thread/LOCAL_ACTIVE_SESSION.md)和[GS_ASP](../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_ASP.md)中新增一列plan_node_id来记录SQL语句每个算子操作的执行情况。

现有的监控级别由guc参数[resource_track_level](../../reference-guide/guc-parameters/13-load-management.md#resource_track_level)控制，该参数按照级别存在三个值，分别是：

- none：不开启资源记录功能；

- query：开启query级别资源记录功能；

- operator：开启query级别和算子级别资源记录功能，

所以只有当将resource_track_level设成operator的时候才会对SQL语句每个算子操作进行采样。

MogDB启动后会启动一个后台worker采样线程，为避免浪费资源，该采样线程不会时刻采样，而是每隔一个采样周期对MogDB进行采样，收集MogDB当时的运行快照保存到内存中，查询视图dbe_perf.local_active_session可以查询到实时的采样信息，该采样周期由guc参数[asp_sample_interval](../../reference-guide/guc-parameters/27-system-performance-snapshot.md#asp_sample_interval)控制，默认采样周期为1s，MogDB每在内存中采样100000行（由guc参数[asp_sample_num](../../reference-guide/guc-parameters/27-system-performance-snapshot.md#asp_sample_num)控制）会将内存中的采样数据刷新到GS_ASP表中以供历史查询，只有语句执行时间大于采样时间，才会被采样线程收集到运行信息。

## 使用场景

1. 首先在session1中创建表test，并执行插入操作：

   ```sql
   MogDB=# create table test(c1 int);
   CREATE TABLE
   MogDB=# insert into test select generate_series(1, 1000000000);
   ```

2. 在session2中，从活跃会话视图中查询出该SQL的query_id

   ```sql
   MogDB=# select query,query_id from pg_stat_activity where query like 'insert into test select%';
                       query                                  |    query_id
   -----------------------------------------------------------+-----------------
    insert into test select generate_series(1, 100000000000); | 562949953421368
   (1 row)
   ```

3. 在session2中，根据该query_id从活跃作业管理视图中查询出该语句的带plan_node_id的执行计划（该语句执行cost需要大于guc值**resource_track_cost**才会被记录到该视图中，该guc参数默认值为100000，session级别可更新，所以为了方便测试，可在测试中将该值改成10）

   Set resource_track_cost=10;

   ```sql
   MogDB=# select query_plan from dbe_perf.statement_complex_runtime where queryid = 562949953421368;
                                    query_plan
   ----------------------------------------------------------------------------
    Coordinator Name: datanode1                                               +
    1 | Insert on test  (cost=0.00..17.51 rows=1000 width=8)                         +
    2 |  ->  Subquery Scan on "*SELECT*"  (cost=0.00..17.51 rows=1000 width=8)    +
    3 |   ->  Result  (cost=0.00..5.01 rows=1000 width=0)                         +
                                                                           +
   (1 row)
   ```

4. 在session2中，根据query_id从采样视图dbe_perf.local_active_session中查询出该语句的采样情况，结合上面查询的执行计划做性能分析。

   ```sql
   MogDB=# select plan_node_id, count(plan_node_id) from dbe_perf.local_active_session where query_id = 562949953421368 group by plan_node_id;
    plan_node_id | count
   --------------+-------
           3     |   12
           1     |   366
           2     |   2
   (3 rows)
   ```

5. 在session2中执行，当内存数据到达上限值（由guc参数**asp_sample_num**控制）的时候，则会将现有内存的采样数据刷新到gs_asp表中，刷盘后查询gs_asp表也会查到该语句的算子采样的数据。

   ```sql
   MogDB=# select plan_node_id, count(plan_node_id) from gs_asp where query_id = 562949953421368 group by plan_node_id;
    plan_node_id | count
   --------------+-------
     3           |    19
     1           |   582
     2           |     3
   
   (3 rows)
   ```

## 结论

当发现`insert into test select generate_series(1, 1000000000)`存在性能瓶颈，通过以上的步骤定位发现，insert操作在整个SQL语句执行过程中被采样的数值最高（ plan_node_id =1 ，count=366），可以对其进行优化。

## 相关页面

[GS_ASP](../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_ASP.md)、[LOCAL_ACTIVE_SESSION](../../reference-guide/schema/DBE_PERF/session-thread/LOCAL_ACTIVE_SESSION.md)