---
title: 伪列ROWNUM
summary: 伪列ROWNUM
author: Guo Huan
date: 2022-05-07
---

# 伪列ROWNUM

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

ROWNUM为查询出来的每一行记录生成一个序号，从1开始依次递增且不会重复。

## 客户价值

- 兼容Oracle特性，方便数据库迁移。
- 与LIMIT特性类似，能够筛选出结果集的前n条记录。

## 特性描述

ROWNUM（伪列），用于对SQL查询中符合条件的记录进行顺序标注。查询结果第一行ROWNUM为1，第二行为2，依次类推，第n行为n。通常用来筛选出查询结果集中的前n行数据，与MogDB中LIMIT功能类似。

## 特性增强

在内部执行时，优化器会将ROWNUM重写成LIMIT去执行，加快执行速率。

## 特性约束

- ROWNUM是伪列，不可作为别名，以免SQL语句出现歧义；
- 创建索引时不可使用ROWNUM。例如：create index index_name on table(rownum);
- 创建表时default值不可为ROWNUM。例如：create table table_name(id int default rownum);
- Where子句中不可使用rownum的别名。例如：select rownum rn from table where rn < 5;
- 在插入数据时不可使用ROWNUM。例如：insert into table values(rownum,’blue’);
- 不可在无表查询中使用ROWNUM。例如：select * from (values(rownum,1)), x(a,b);
- 若 having 子句中含有ROWNUM（且不在聚合函数中）时，group by子句中必须含有ROWNUM（且不在聚合函数中）。

## 依赖关系

无。