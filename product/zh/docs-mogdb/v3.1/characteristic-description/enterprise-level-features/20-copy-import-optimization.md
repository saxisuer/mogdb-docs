---
title: COPY导入优化
summary: COPY导入优化
author: Guo Huan
date: 2022-06-16
---

# COPY导入优化

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

COPY是使用较多的导入用户表数据的方式，本特性通过利用现代CPU的SIMD特性来提升COPY在解析阶段的性能，从而提升COPY的性能，提升导入的速度。

COPY从文件中导入数据的时候在解析阶段查找分隔符以及在判断CSV/TEXT解析出的数据是否合法的时候，理论上都是字符串比较操作，可以利用SIMD特性将现有一次一个字符比较转换成一次比较多个字符，从而减少分支判断次数提升性能。

## 客户价值

利用SIMD指令针对COPY解析过程中的行列分隔符查找做优化。本特性的最终用户为普通客户群体，如数据库DBA，软件开发人员等。COPY性能提升为10%-30%。

| 数据总条数       | **100000000**（1亿） |
| ---------------- | -------------------- |
| **数据总大小**   | **24GB**             |
| **平均性能提升** | **12.29%**           |

测试结果如下：

| 测试次序   | 未使用SIMD特性耗时（秒） | 使用SIMD特性耗时（秒） |
| ---------- | ------------------------ | ---------------------- |
| 第一次测试 | 761.01                   | 671.05                 |
| 第二次测试 | 747.06                   | 662.60                 |
| 第三次测试 | 770.22                   | 663.03                 |
| 第四次测试 | 747.940                  | 674.03                 |
| 第五次测试 | 787.22                   | 674.13                 |
| 平均耗时   | 762.69                   | 668.97                 |

## 特性约束

- 只支持X86架构的机器，只支持text和csv文件，且如下情况不支持：转义字符、escape以及quote，null值替换以及自定义列分隔符；

- 因为该字符串比较指令值是SSE4.2才开始支持，所以只有支持SSE4.2的X86才可以使用该优化。

可以使用如下命令来判断机器是否支持SSE4.2指令集(以root或者omm用户登录都可以)：

```shell
--显示为SSE 4.2 supported表示支持
[omm3@hostname ~]$ grep -q sse4_2 /proc/cpuinfo && echo "SSE 4.2 supported" || echo "SSE 4.2 not supported"
SSE 4.2 supported

--显示为SSE 4.2 supported表示不支持
[xxxx@hostname ~]$ grep -q sse4_2 /proc/cpuinfo && echo "SSE 4.2 supported" || echo "SSE 4.2 not supported"
SSE 4.2 not supported
```

可以使用以下命令开启或关闭enable_sse42特性

登录数据库

```shell
[omm3@hostname ~]$ gsql -d postgres -p18000
gsql ((MogDB 3.0.0 build 945141ad) compiled at 2022-05-28 16:14:02 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.
```

开启enable_sse42特性

```sql
MogDB=# set enable_sse42 to on;
SET
MogDB=# show enable_sse42;
enable_sse42 
--------------
 on
(1 row)
```

关闭enable_sse42特性

```sql
MogDB=# set enable_sse42 to off;
SET
MogDB=# show enable_sse42;
enable_sse42 
--------------
off
(1 row)
```

## 相关页面

[COPY](../../reference-guide/sql-syntax/COPY.md)