---
title: 支持SQL hint
summary: 支持SQL hint
author: Guo Huan
date: 2022-05-07
---

# 支持SQL hint

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

支持SQL hint影响执行计划生成。

## 客户价值

提升SQL查询性能。

## 特性描述

Plan Hint为用户提供了直接影响执行计划生成的手段，用户可以通过指定join顺序，join、stream、scan方法，指定结果行数，指定重分布过程中的倾斜信息等多个手段来进行执行计划的调优，以提升查询的性能。

## 特性增强

支持planhint设置session级优化器参数。

支持指定子查询不展开。

支持单query禁用gpc。

## 特性约束

无。

## 依赖关系

无。