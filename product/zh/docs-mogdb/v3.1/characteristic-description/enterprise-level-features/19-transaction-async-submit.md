---
title: 事务异步提交
summary: 事务异步提交
author: Guo Huan
date: 2022-06-13
---

# 事务异步提交

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

在MogDB的线程池模式下为每个线程组增加了committer线程，当事务提交过程中等待日志刷盘的时候将session挂起，交由committer线程在日志刷盘完成之后继续提交事务，而原worker线程在该session挂起之后可以去处理其他待处理的session。

## 客户价值

在TP场景中，对高并发下事务处理的性能有较高的需求。然而MogDB中事务提交的过程中需要同步等待日志落盘，这一期间的worker线程处于空闲状态，并不能被利用去处理其他的事务。虽然在MogDB中有“synchronous_commit=off”的实现，但该实现并不能保证数据库中途崩溃之后数据的完整性。

本特性能够实现真正意义上的事务异步提交，在保证数据库可靠性的前提下更加充分利用CPU，提升高并发场景下事务处理处理能力，尤其在小查询的增删改操作上会有明显的体现。

## 特性约束

- 该功能仅在线程池模式下打开有效，非线程池模式下不支持事务异步提交。即设置“enable_thread_pool = on”和“synchronous_commit”不为“off”。

## 相关页面

[GS_ASYNC_SUBMIT_SESSIONS_STATUS](../../reference-guide/system-catalogs-and-system-views/system-views/GS_ASYNC_SUBMIT_SESSIONS_STATUS.md)、[async_submit](../../reference-guide/guc-parameters/20-MogDB-transaction.md#async_submit)、[LOCAL_THREADPOOL_STATUS](../../reference-guide/schema/DBE_PERF/session-thread/LOCAL_THREADPOOL_STATUS.md)