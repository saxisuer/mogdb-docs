---
title: BLOOM索引
summary: BLOOM索引
author: Guo Huan
date: 2022-06-21
---

# BLOOM索引

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

Bloom提供了一种基于布鲁姆过滤器的索引访问方法。布鲁姆过滤器是一种空间高效的数据结构，它被用来测试一个元素是否为一个集合的成员。在索引访问方法的情况下，它可以通过尺寸在索引创建时决定的签名来快速地排除不匹配的元祖。

## 客户价值

Bloom索引适用于表中有很多列，并且查询可以使用任意列组合的场景。对于传统的索引，如Btree在这种场景下可能需要建很多个索引来覆盖可能的查询条件，可能导致索引占用空间很大，且影响插入和更新的性能，而Bloom索引只需要建一个就可以应对。

## 特性描述

用户在Bloom索引的适用场景下可以通过Bloom索引进行查询，同时相对于Btree索引还可以减少索引占用空间大小。

## 使用场景

当表具有很多属性并且查询可能会测试其中任意组合时，Blooms类型的索引最有用，传统的btree索引会比Bloom索引更快，但需要很多btree索引来支持所有可能的查询，而对于Bloom只需要一个即可。

### 创建Bloom索引

#### 约束

- Bloom 索引只支持在行存表上创建

- 创建索引的列类型只能是4字节长度的int类型或变长字符串类型，在MogDB中4字节长度的int类型可以是int、int4和integer，变长字符串类型可以是varchar、text和clob。

- Bloom不支持创建唯一索引

- 不支持索引null值，null值跳过
- 只支持等值查询
- 不支持分区表Global索引

#### 示例

```sql
CREATE INDEX ON <table_name> USING bloom(col1,col2...) with (length=80,col1=2,col2=4);
```

#### 参数说明

**length**

指定一条索引中生成的签名使用多少bit表示。默认值是80，内部会将用户指定的值会向上取整为16的整数倍（对用户透明），最小值是1，最大值是4096。如果设置值不在限定范围内，命令执行报错，并提示数值的正确范围。

**col1-col32**

指定每一个索引列使用多少bit表示。默认值是2，最小值是1，最大值是4095。如果设置值不在限定范围内，命令执行报错，并提示数值的正确范围。

如果对非行存表创建Bloom索引，报错，提示不能对非行存表创建。

如果创建Bloom的唯一索引，报错，提示不能创建唯一索引。

如果创建分区表的Global索引，且索引类型是Bloom，报错，提示分区表Global索引只能是Btree类型。

### 删除Bloom索引

示例：

```sql
MogDB=# DROP INDEX bloom_idx;
DROP INDEX
```

### 重建Bloom索引

本特性支持重建一个已存在的Bloom索引。用户通过客户端或者数据库驱动下发重建Bloom索引的命令，索引最终重建成功，索引元数据正确，索引可以正常使用（如，原来执行计划走 Bloom 索引的，在索引重建后，执行计划仍能使用重建后的 Bloom 索引）。

示例：

```sql
MogDB=# REINDEX INDEX bloom_idx;
REINDEX
```

### 修改Bloom索引

本特性支持修改一个已存在的Bloom索引的属性。索引属性相关约束与创建索引时的约束保持一致，如Bloom索引的length修改后需要在[1,4096]的范围内。

关注点：

- 修改的索引属性需要符合索引规则和约束，如果不符合需要报错，返回对应的提示信息；
- 重命名索引，在pg_class中校验索引元数据是否正确；
- 修改Bloom索引的length或col属性，如果要验证属性是否生效可以简单通过pg_class中relpages字段的值的变化判断，或者通过SELECT pg_size_pretty(pg_relation_size(‘blidx’))等命令查看索引大小的变化；
- 修改tablespace，可以通过在磁盘上查看数据文件的方式，确认是否生效。

示例：

```sql
MogDB=# ALTER INDEX IF EXISTS bloom_idx RENAME TO newbloom_idx;
ALTER INDEX
MogDB=# SELECT oid,relname,relfilenode,relpages FROM pg_class WHERE relname = 'newbloom_idx';
  oid  |   relname    | relfilenode | relpages 
-------+--------------+-------------+----------
 41159 | newbloom_idx |       41160 |       30
(1 row)
MogDB=# ALTER INDEX IF EXISTS newbloom_idx SET (length=160);
ALTER INDEX
MogDB=# REINDEX INDEX newbloom_idx;
REINDEX
MogDB=# SELECT oid,relname,relfilenode,relpages FROM pg_class WHERE relname = 'newbloom_idx';
  oid  |   relname    | relfilenode | relpages 
-------+--------------+-------------+----------
 41159 | newbloom_idx |       41162 |       49
(1 row)
```

### 使用Bloom索引查询

当表上存在Bloom索引，且查询条件符合Bloom索引的适用条件时，执行计划会显示已使用Bloom索引。

示例：

```sql
CREATE TABLE tbloom AS
   SELECT
     (random() * 1000000)::int as i1,
     (random() * 1000000)::int as i2,
     (random() * 1000000)::int as i3,
     (random() * 1000000)::int as i4,
     (random() * 1000000)::int as i5,
     (random() * 1000000)::int as i6
   FROM
  generate_series(1,10000000);

CREATE INDEX btreeidx ON tbloom (i1, i2, i3, i4, i5, i6);
CREATE INDEX bloomidx ON tbloom USING bloom (i1, i2, i3, i4, i5, i6);

MogDB=# EXPLAIN ANALYZE SELECT * FROM tbloom where i3 = 100 AND i5 = 1000;
                                                          QUERY PLAN                                                          
------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on tbloom  (cost=178436.31..179393.23 rows=250 width=24) (actual time=138.209..138.209 rows=0 loops=1)
   Recheck Cond: ((i3 = 100) AND (i5 = 1000))
   Rows Removed by Index Recheck: 21936
   Heap Blocks: exact=18673
   ->  Bitmap Index Scan on bloomidx  (cost=0.00..178436.25 rows=250 width=0) (actual time=85.681..85.681 rows=21936 loops=1)
         Index Cond: ((i3 = 100) AND (i5 = 1000))
 Total runtime: 138.412 ms
(7 rows)

MogDB=# EXPLAIN ANALYZE SELECT * FROM tbloom where i1 = 100 AND i2 = 1000;
                                                      QUERY PLAN                                                       
-----------------------------------------------------------------------------------------------------------------------
 [Bypass]
 Index Only Scan using btreeidx on tbloom  (cost=0.00..8.27 rows=1 width=24) (actual time=0.084..0.084 rows=0 loops=1)
   Index Cond: ((i1 = 100) AND (i2 = 1000))
   Heap Fetches: 0
 Total runtime: 0.134 ms
(5 rows)
```

### 更新Bloom索引

示例：

```sql
MogDB=# select i2,i3,i4 from tbloom where rownum <=5;
   i2   |   i3   |   i4   
--------+--------+--------
 778090 | 624067 | 948170
 927435 | 800792 | 904419
 325217 | 726778 | 834407
 925272 | 221411 | 826500
  93906 | 992575 | 997677
  
UPDATE tbloom SET i1 = 10 
            WHERE i2 = 325217 AND 
                  i3 = 726778 AND 
                  i4 = 834407; 
                  
MogDB=# select * from tbloom where i2 = 325217 and i3 = 726778;
 i1 |   i2   |   i3   |   i4   |   i5   |   i6   
----+--------+--------+--------+--------+--------
 10 | 325217 | 726778 | 834407 | 702579 | 525581
(1 row)

MogDB=# explain select * from tbloom where i2 = 325217 and i3 = 726778;
                                 QUERY PLAN                                 
----------------------------------------------------------------------------
 Bitmap Heap Scan on tbloom  (cost=178439.48..178443.50 rows=1 width=24)
   Recheck Cond: ((i2 = 325217) AND (i3 = 726778))
   ->  Bitmap Index Scan on bloomidx  (cost=0.00..178439.48 rows=1 width=0)
         Index Cond: ((i2 = 325217) AND (i3 = 726778))
(4 rows)

DELETE FROM tbloom WHERE i2 = 1000 AND i3 = 789678 AND i4 = 311551; 
select * from tbloom where i2 = 1000 and i3 = 789678;
 i1 | i2 | i3 | i4 | i5 | i6 
----+----+----+----+----+----
(0 rows)
explain select * from tbloom where i2 = 1000 and i3 = 789678;
 Bitmap Heap Scan on tbloom  (cost=178440.26..178444.28 rows=1 width=24)
   Recheck Cond: ((i2 = 1000) AND (i3 = 789678))
   ->  Bitmap Index Scan on tbloomidx  (cost=0.00..178440.26 rows=1 width=0)
         Index Cond: ((i2 = 1000) AND (i3 = 789678))
(4 rows)
```

## 相关页面

[CREATE INDEX](../../reference-guide/sql-syntax/CREATE-INDEX.md)、[DROP INDEX](../../reference-guide/sql-syntax/DROP-INDEX.md)、[ALTER INDEX](../../reference-guide/sql-syntax/ALTER-INDEX.md)