---
title: x86下安装完成后发生coredump
summary: x86下安装完成后发生coredump
author: Guo Huan
date: 2021-12-09
---

# x86下安装完成后发生coredump

## 问题现象

x86架构下安装MogDB完成后发生coredump，出现如下报错

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-after-installation-on-x86-1.png)

## 原因分析

x86架构不包括rdtscp指令，常见于在本地windows系统上，虚拟化安装Linux服务器，但虚拟化版本太低，部署MogDB启动失败。

## 处理办法

使用`lscpu | grep rdtscp`查看是否支持rdtscp指令集

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-after-installation-on-x86-2.png)

可以通过主机的管理侧设置开启支持该指令集。云主机CPU模式，设置为host-passthrough，重新reboot。
