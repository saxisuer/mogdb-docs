---
title: 用户自定义函数
summary: 用户自定义函数
author: Guo Huan
date: 2021-03-04
---

# 用户自定义函数

在MogDB数据库中，为了实现特定的功能，用户可以自定义函数，就像其它大多数语言一样，除了系统的库函数，程序员还会编写很多自定义的函数。

MogDB用户自定义函数是存储在数据库服务器上并可以使用SQL界面调用的一组SQL和过程语句（声明、分配、循环、控制流程等）的组合，在表现形式上，用户自定义函数和存储过程是非常相似的，只是存储过程无返回值，而函数有返回值。

用户自定义函数的创建及调用办法请参考[CREATE FUNCTION](../reference-guide/sql-syntax/CREATE-FUNCTION.md)。

[存储过程](1-1-stored-procedure.md)一节所提到的存储过程编写方法与本章节用户自定义函数的编写方法相通。具体介绍请参看[PL/pgSQL-SQL过程语言](../developer-guide/plpgsql/1-1-plpgsql-overview.md)章节，除非特别声明，否则其中内容通用于存储过程和用户自定义函数。
