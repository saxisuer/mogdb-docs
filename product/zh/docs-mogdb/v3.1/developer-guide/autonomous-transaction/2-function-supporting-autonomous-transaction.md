---
title: 用户自定义函数支持自治事务
summary: 用户自定义函数支持自治事务
author: Zhang Cuiping
date: 2021-05-10
---

# 用户自定义函数支持自治事务

自治事务可以在函数中定义，标识符为PRAGMA AUTONOMOUS_TRANSACTION，执行的函数块中使用包含start transaction和commit/rollback的sql，其余语法与[CREATE FUNCTION](../../reference-guide/sql-syntax/CREATE-FUNCTION.md)创建函数语法类似，一个简单的用例如下：

```sql
--建表
MogDB=# create table t2(a int, b int);
MogDB=# insert into t2 values(1,2);
MogDB=# select * from t2;
a | b
---+---
1 | 2
(1 row)

--创建包含自治事务的存储过程
CREATE OR REPLACE PROCEDURE autonomous_4(a int, b int)  AS 
DECLARE 
    num3 int := a;
    num4 int := b;
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    insert into t2 values(num3, num4); 
    
END;
/
--创建调用自治事务存储过程的普通存储过程
CREATE OR REPLACE PROCEDURE autonomous_5(a int, b int)  AS 
DECLARE 
BEGIN
    
    insert into t2 values(666, 666);
    autonomous_4(a,b);
    rollback;
END;
/
--调用普通存储过程
MogDB=# select autonomous_5(11,22);
autonomous_5
(1 row)
--查看表结果
MogDB=# select * from t2 order by a;
a | b
----+----
1 | 2
11 | 22
(2 rows)
```

上述例子，最后在回滚的事务块中执行包含自治事务的函数，直接说明了自治事务的特性，即主事务的回滚，不会影响自治事务已经提交的内容。
