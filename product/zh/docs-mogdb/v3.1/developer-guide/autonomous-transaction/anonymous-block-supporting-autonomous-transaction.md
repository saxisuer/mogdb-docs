---
title: 匿名块支持自治事务
summary: 匿名块支持自治事务
author: Guo Huan
date: 2021-10-15
---

# 匿名块支持自治事务

自治事务可以在匿名块中定义，标识符为PRAGMA AUTONOMOUS_TRANSACTION，其余语法与创建匿名块语法相同，示例如下。

```sql
MogDB=# create table t1(a int ,b text);
CREATE TABLE

START TRANSACTION;
DECLARE 
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    
    insert into t1 values(1,'you are so cute,will commit!');
END;
/
MogDB=# insert into t1 values(1,'you will rollback!');
INSERT 0 1
MogDB=# rollback;
ROLLBACK

MogDB=# select * from t1;
a | b
---+------------------------------
1 | you are so cute,will commit!
(1 row)
```

上述例子，最后在回滚的事务块前执行包含自治事务的匿名块，也能直接说明了自治事务的特性，即主事务的回滚，不会影响自治事务已经提交的内容。
