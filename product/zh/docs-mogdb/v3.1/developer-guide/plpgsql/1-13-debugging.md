---
title: 调试
summary: 调试
author: Guo Huan
date: 2021-03-04
---

# 调试

## 语法

### RAISE语法

有以下五种语法格式：

**图 1** raise_format::=

![raise_format](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-1.png)

**图 2** raise_condition::=

![raise_condition](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-2.png)

**图 3** raise_sqlstate::=

![raise_sqlstate](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-3.png)

**图 4** raise_option::=

![raise_option](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-4.png)

**图 5** raise::=<a id="raise::="> </a>

![raise](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-5.png)

**参数说明：**

- level选项用于指定错误级别，有DEBUG，LOG，INFO，NOTICE，WARNING以及EXCEPTION（默认值）。EXCEPTION抛出一个正常终止当前事务的异常，其他的仅产生不同异常级别的信息。特殊级别的错误信息是否报告到客户端、写到服务器日志由[log_min_messages](../../reference-guide/guc-parameters/10-error-reporting-and-logging/2-logging-time.md#log_min_messages)和[client_min_messages](../../reference-guide/guc-parameters/10-error-reporting-and-logging/2-logging-time.md#client_min_messages)这两个配置参数控制。

- format：格式字符串，指定要报告的错误消息文本。格式字符串后可跟表达式，用于向消息文本中插入。在格式字符串中，%由format后面跟着的参数的值替换，%%用于打印出%。例如：

    ```
    --v_job_id 将替换字符串中的 %:
    RAISE NOTICE 'Calling cs_create_job(%)',v_job_id;
    ```

- option = expression：向错误报告中添加另外的信息。关键字option可以是MESSAGE、DETAIL、HINT以及ERRCODE，并且每一个expression可以是任意的字符串。

  - MESSAGE，指定错误消息文本，这个选项不能用于在USING前包含一个格式字符串的RAISE语句中。
  - DETAIL，说明错误的详细信息。
  - HINT，用于打印出提示信息。
  - ERRCODE，向报告中指定错误码（SQLSTATE）。可以使用条件名称或者直接用五位字符的SQLSTATE错误码。

- condition_name：错误码对应的条件名。

- sqlstate：错误码。

如果在RAISE EXCEPTION命令中既没有指定条件名也没有指定SQLSTATE，默认用RAISE EXCEPTION (P0001)。如果没有指定消息文本，默认用条件名或者SQLSTATE作为消息文本。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif)**须知**:
>
> - 当由SQLSTATE指定了错误码，则不局限于已定义的错误码，可以选择任意包含五个数字或者大写的ASCII字母的错误码，而不是00000。建议避免使用以三个0结尾的错误码，因为这种错误码是类别码，会被整个种类捕获。
> - 兼容O模式下，SQLCODE等于SQLSTATE。
>
> ![https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**:
>
> - [图5](#raise::=)所示的语法不接任何参数。这种形式仅用于一个BEGIN块中的EXCEPTION语句，它使得错误重新被处理。
>
> - 用于ERRCODE指定的条件名称和condition_name可见[SQL标准错误码说明](../../reference-guide/error-code-reference/description-of-sql-error-codes.md)，仅支持其中的ERROR类条件名。

### EXCEPTION_INIT语法

兼容O模式下，支持使用EXCEPTION_INIT语法自定义错误码SQLCODE。语法格式如下：

**图 6** exception_init::=

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/debugging-6.png)

**参数说明：**

- exception_name为用户申明的异常名，EXCEPTION_INIT语法必须出现在与申明异常相同部分，位于申明异常之后。
- sqlcode为自定义的SQLCODE，必须为负整数，取值范围-2147483647~-1。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 使用EXCEPTION_INIT语法自定义错误码SQLCODE时，SQLSTATE与SQLCODE相同，SQLERRM格式为” xxx: non-MogDB Exception”。比如自定义SQLCODE=-1，则SQLSTATE=“-1”，SQLERRM=” 1: non-MogDB Exception”。

## **示例**

终止事务时，给出错误和提示信息：

```sql
CREATE OR REPLACE PROCEDURE proc_raise1(user_id in integer)
AS
BEGIN
RAISE EXCEPTION 'Noexistence ID --> %',user_id USING HINT = 'Please check your user ID';
END;
/

call proc_raise1(300011);

--执行结果
ERROR:  Noexistence ID --> 300011
HINT:  Please check your user ID
```

两种设置SQLSTATE的方式：

```sql
CREATE OR REPLACE PROCEDURE proc_raise2(user_id in integer)
AS
BEGIN
RAISE 'Duplicate user ID: %',user_id USING ERRCODE = 'unique_violation';
END;
/

\set VERBOSITY verbose
call proc_raise2(300011);

--执行结果
ERROR:  Duplicate user ID: 300011
SQLSTATE: 23505
```

如果主要的参数是条件名或者是SQLSTATE，可以使用：

RAISE division_by_zero;

RAISE SQLSTATE '22012';

例如：

```sql
CREATE OR REPLACE PROCEDURE division(div in integer, dividend in integer)
AS
DECLARE
res int;
    BEGIN
    IF dividend=0 THEN
        RAISE division_by_zero;
        RETURN;
    ELSE
        res := div/dividend;
        RAISE INFO 'division result: %', res;
        RETURN;
    END IF;
    END;
/
call division(3,0);

--执行结果
ERROR:  division_by_zero
```

或者另一种方式：

```sql
RAISE unique_violation USING MESSAGE = 'Duplicate user ID: ' || user_id;
```

兼容O模式下，支持使用语法EXCEPTION_INIT自定义错误码SQLCODE：

```sql
declare
    deadlock_detected exception;
    pragma exception_init(deadlock_detected, -1);
begin
    if 1 > 0 then
        raise deadlock_detected;
    end if;
exception
    when deadlock_detected then
        raise notice 'sqlcode:%,sqlstate:%,sqlerrm:%',sqlcode,sqlstate,sqlerrm;
end;
/
--执行结果
NOTICE:  sqlcode:-1,sqlstate:-1,sqlerrm: 1: non-GaussDB Exception
```
