---
title: 数据类型转换
summary: 数据类型转换
author: Guo Huan
date: 2021-03-04
---

# 数据类型转换

数据库中允许有些数据类型进行隐式类型转换（赋值、函数调用的参数等），有些数据类型间不允许进行隐式数据类型转换，可尝试使用MogDB提供的类型转换函数，例如CAST进行数据类型强转。

MogDB数据库常见的隐式类型转换，请参见[表1](#隐式类型转换表)。

> **须知:**
> MogDB支持的DATE的效限范围是: 公元前4713年到公元294276年。

**表 1** 隐式类型转换表<a id="隐式类型转换表"> </a>

| 原始数据类型 | 目标数据类型 | 备注                         |
| :----------- | :----------- | :--------------------------- |
| CHAR         | VARCHAR2     | -                            |
| CHAR         | NUMBER       | 原数据必须由数字组成。       |
| CHAR         | DATE         | 原数据不能超出合法日期范围。 |
| CHAR         | RAW          | -                            |
| CHAR         | CLOB         | -                            |
| VARCHAR2     | CHAR         | -                            |
| VARCHAR2     | NUMBER       | 原数据必须由数字组成。       |
| VARCHAR2     | DATE         | 原数据不能超出合法日期范围。 |
| VARCHAR2     | CLOB         | -                            |
| NUMBER       | CHAR         | -                            |
| NUMBER       | VARCHAR2     | -                            |
| DATE         | CHAR         | -                            |
| DATE         | VARCHAR2     | -                            |
| RAW          | CHAR         | -                            |
| RAW          | VARCHAR2     | -                            |
| CLOB         | CHAR         | -                            |
| CLOB         | VARCHAR2     | -                            |
| CLOB         | NUMBER       | 原数据必须由数字组成。       |
| INT4         | CHAR         | -                            |
| INT4         | BOOLEAN      | -                            |
| INT4         | CHAR         | -                            |
| BOOLEAN      | INT4         | -                            |
