---
title: 快速设置
summary: 快速设置
author: Guo Huan
date: 2022-04-29
---

# 快速设置

首先在postgresql.conf中设置配置选项：

```bash
wal_level = logical
```

对于一个基础设置来说，其他所需的设置使用默认值就足够了。

需要调整pg_hba.conf以允许复制（这里的值取决于实际的网络配置以及用于连接的用户）：

```bash
host     all     repuser     0.0.0.0/0     sha256
```

然后在发布者数据库上：

```sql
CREATE PUBLICATION mypub FOR TABLE users, departments;
```

并且在订阅者数据库上：

```sql
CREATE SUBSCRIPTION mysub CONNECTION 'dbname=foo host=bar user=repuser' PUBLICATION mypub;
```

上面的语句将开始复制过程，复制对那些表的增量更改。
