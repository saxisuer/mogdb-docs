---
title: 如何确定是否使用了分区裁剪
summary: 如何确定是否使用了分区裁剪
author: Guo Huan
date: 2022-06-22
---

### 如何确定是否使用了分区裁剪

MogDB是否使用分区裁剪会反映在语句的执行计划中，可以通过EXPLAIN VERBOSE或EXPLAIN ANALYZE语句查看。

分区裁剪信息反映在执行计划列Iterations和Selected Partitions中。
