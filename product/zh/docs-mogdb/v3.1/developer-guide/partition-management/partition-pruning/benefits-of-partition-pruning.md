---
title: 分区裁剪的好处
summary: 分区裁剪的好处
author: Guo Huan
date: 2022-06-14
---

# 分区裁剪的好处

分区裁剪是分区表中常用的性能优化手段。在扫描分区表前，如果能通过检查分区约束条件与每个分区的定义，提前排除不需要扫描的分区，可以极大地提升扫描性能。在查询规划阶段，如果分区约束为确定的表达式，在查询规划阶段就可以根据分区约束表达式裁掉不需要扫描的分区，这种分区裁剪方式一般称为**静态分区裁剪**。静态裁剪从EXPLAIN VERBOSE输出就可以看出裁剪结果，如下所示。

explain verbose select * from prune_tt01 where a<8 ;

```sql
MogDB=# drop table if exists prune_tt01;
DROP TABLE
MogDB=# CREATE TABLE prune_tt01(a int, b int)
PARTITION BY RANGE(a)
(
        PARTITION prune_tt01_p1 VALUES LESS THAN(5),
        PARTITION prune_tt01_p2 VALUES LESS THAN(10),
        PARTITION prune_tt01_p3 VALUES LESS THAN(15),
        PARTITION prune_tt01_p4 VALUES LESS THAN(MAXVALUE)
);
CREATE TABLE
MogDB=# INSERT INTO prune_tt01 VALUES (generate_series(1, 20), generate_series(1,20));
INSERT 0 20
MogDB=# CREATE INDEX index_prune_tt01 ON prune_tt01 USING btree(a) LOCAL;
CREATE INDEX
MogDB=# explain verbose select * from prune_tt01 where a<8 ;
                                             QUERY PLAN                                             
----------------------------------------------------------------------------------------------------
 Partition Iterator  (cost=13.80..27.75 rows=716 width=8)
   Output: a, b
   Iterations: 2
   Selected Partitions:  1..2
   ->  Partitioned Bitmap Heap Scan on public.prune_tt01  (cost=13.80..27.75 rows=716 width=8)
         Output: a, b
         Recheck Cond: (prune_tt01.a < 8)
         ->  Partitioned Bitmap Index Scan on index_prune_tt01  (cost=0.00..13.62 rows=716 width=0)
               Index Cond: (prune_tt01.a < 8)
(9 rows)
```

但是在很多场景下，例如PREPARE-EXECUTE执行方式和分区约束表达式中包含子查询的场景在查询规划阶段，分区约束表达式是不确定的或包含未知的参数，查询规划阶段是不能裁剪的，只能在执行阶段，通过外部参数和子查询的结果确定分区表达式后进行裁剪，通常将在执行阶段做的裁剪称为**动态分区裁剪**。动态裁剪可以通过explain verbose从执行看到裁剪信息（Selected Partitions:  PART）如下所示。

explain verbose select * from prune_tt01 where a < (select 8);

```sql
MogDB=# explain verbose select * from prune_tt01 where a < (select 8);
                                      QUERY PLAN                                      
--------------------------------------------------------------------------------------
 Partition Iterator  (cost=0.01..36.87 rows=716 width=8)
   Output: prune_tt01.a, prune_tt01.b
   Iterations: PART
   Selected Partitions:  PART
   InitPlan 1 (returns $0)
     ->  Result  (cost=0.00..0.01 rows=1 width=0)
           Output: 8
   ->  Partitioned Seq Scan on public.prune_tt01  (cost=0.00..36.86 rows=716 width=8)
         Output: prune_tt01.a, prune_tt01.b
         Filter: (prune_tt01.a < $0)
(10 rows)
```

MogDB自3.0版本后引入了分区表的动态裁剪，大大减少了从磁盘检索的数据量并缩短了处理时间，从而提高查询性能并优化资源利用率。
