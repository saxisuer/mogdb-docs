---
title: 内核MySQL兼容性说明
summary: 内核MySQL兼容性说明
author: zhang cuiping
date: 2022-10-24
---

# 内核MySQL兼容性说明

**表 1** 在MogDB内核中实现的MySQL兼容性特性列表

| 分类         | 概述                                           | 详细说明链接                                                 |
| :----------- | :--------------------------------------------- | :----------------------------------------------------------- |
| DDL          | CREATE DATABASE支持IF NOT EXISTS选项           | [CREATE DATABASE](../../reference-guide/sql-syntax/CREATE-DATABASE.md) |
| DDL          | CREATE SCHEMA支持IF NOT EXISTS选项             | [CREATE SCHEMA](../../reference-guide/sql-syntax/CREATE-SCHEMA.md) |
| DDL          | ALTER USER支持IF EXISTS选项                    | [ALTER USER](../../reference-guide/sql-syntax/ALTER-USER.md) |
| DDL          | ALTER VIEW支持DEFINER选项                      | [ALTER VIEW](../../reference-guide/sql-syntax/ALTER-VIEW.md) |
| DDL          | CREATE VIEW支持DEFINER选项                     | [CREATE VIEW](../../reference-guide/sql-syntax/CREATE-VIEW.md) |
| DDL          | 支持在创建、修改对象等语句中指定COMMENT选项    | [ALTER FUNCTION](../../reference-guide/sql-syntax/ALTER-FUNCTION.md)、 [ALTER PROCEDURE](../../reference-guide/sql-syntax/ALTER-PROCEDURE.md)、 [ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md)、 [CREATE FUNCTION](../../reference-guide/sql-syntax/CREATE-FUNCTION.md)、 [CREATE INDEX](../../reference-guide/sql-syntax/CREATE-INDEX.md)、 [CREATE PROCEDURE](../../reference-guide/sql-syntax/CREATE-PROCEDURE.md)、 [CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md)、 [CREATE TABLE PARTITION](../../reference-guide/sql-syntax/CREATE-TABLE-PARTITION.md)、 [CREATE TABLE SUBPARTITION](../../reference-guide/sql-syntax/CREATE-TABLE-SUBPARTITION.md) |
| DDL          | CREATE TABLE支持创建主键、UNIQUE索引、外键约束 | [CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md) |
| DDL          | ALTER TABLE支持创建主键、UNIQUE索引、外键约束  | [ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md) |
| DDL          | CREATE TABLE支持创建自增列                     | [CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md) |
| DML          | DELETE支持从多个表中删除数据                   | [DELETE](../../reference-guide/sql-syntax/DELETE.md)         |
| DML          | DELETE支持ORDER BY                             | [DELETE](../../reference-guide/sql-syntax/DELETE.md)         |
| DML          | DELETE支持从指定分区（或子分区）删除数据       | [DELETE](../../reference-guide/sql-syntax/DELETE.md)         |
| DML          | UPDATE支持从多个表中更新数据                   | [UPDATE](../../reference-guide/sql-syntax/UPDATE.md)         |
| DML          | UPDATE支持ORDER BY和LIMIT                      | [UPDATE](../../reference-guide/sql-syntax/UPDATE.md)         |
| DCL          | 支持在会话中set用户自定义变量                  | [SET](../../reference-guide/sql-syntax/SET.md)               |
| DCL          | 支持set全局变量增强                            | [SET](../../reference-guide/sql-syntax/SET.md)               |
| 数据类型     | 支持NVARCHAR类型                               | [字符类型](../../reference-guide/supported-data-types/4-character-data-types.md) |
| 数据类型     | 支持SET数据类型                                | [SET类型](../../reference-guide/supported-data-types/set-type.md) |
| 函数和操作符 | 支持安全等于操作符<=>                          | [简单表达式](../../reference-guide/sql-reference/4-expressions/1-simple-expressions.md) |
| 函数和操作符 | 支持group_concat函数                           | [聚集函数](../../reference-guide/functions-and-operators/17-aggregate-functions.md) |
| 函数和操作符 | 支持安全函数aes_decrypt/aes_encrypt            | [安全函数](../../reference-guide/functions-and-operators/19-security-functions.md) |
| 函数和操作符 | 支持字符处理函数sha/sha1/sha2                  | [字符处理函数和操作符](../../reference-guide/functions-and-operators/3-character-processing-functions-and-operators.md) |