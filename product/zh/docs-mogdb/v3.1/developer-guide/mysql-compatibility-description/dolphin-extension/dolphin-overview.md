---
title: dolphin概述
summary: dolphin概述
author: zhang cuiping
date: 2022-10-24
---

MogDB提供dolphin Extension（版本为dolphin-1.0.0）。dolphin Extension是MogDB的MySQL兼容性数据库（dbcompatibility='B'）扩展，从关键字、数据类型、常量与宏、函数和操作符、表达式、类型转换、DDL/DML/DCL语法、存储过程/自定义函数、系统视图等方面兼容MySQL数据库。