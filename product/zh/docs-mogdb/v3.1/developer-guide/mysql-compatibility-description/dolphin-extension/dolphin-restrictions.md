---
title: dolphin限制
summary: dolphin限制
author: zhang cuiping
date: 2022-10-24
---

# dolphin限制

- 不支持小型化版本。
- 不支持删除dolphin插件。
- dolphin插件只能在B兼容性数据库下创建。
- dolphin插件需要在pg_catalog等schema下创建数据类型、函数等，所以加载dolphin插件需要初始用户权限。MogDB将在第一次通过初始用户或拥有初始用户权限的用户连接B数据库时自动加载dolphin插件。如果一个B兼容性数据库从来没有被初始用户或拥有初始用户权限的用户连接过，那么它也不会加载dolphin插件。
- dolphin中所有新增/修改的语法不支持在gsql客户端通过`\h`查看帮助说明，不支持在gsql客户端自动补齐。
- dolphin插件的创建会删除数据库存在的插件所需的同名函数和类型以及之前存在的与之依赖的对象。