---
title: 咨询锁函数
summary: 咨询锁函数
author: zhang cuiping
date: 2022-10-24
---

# 咨询锁函数

咨询锁函数用于管理咨询锁（Advisory Lock）。

- pg_advisory_lock(key bigint)

  描述：获取会话级别的排它咨询锁。

  返回值类型：void

  备注：pg_advisory_lock锁定应用程序定义的资源，该资源可以用一个64位或两个不重叠的32位键值标识。如果已经有另外的会话锁定了该资源，则该函数将阻塞到该资源可用为止。这个锁是排它的。多个锁定请求将会被压入栈中，因此，如果同一个资源被锁定了三次，它必须被解锁三次以将资源释放给其他会话使用。

- pg_advisory_lock(key1 int, key2 int)

  描述：获取会话级别的排它咨询锁。

  返回值类型：void

  备注：只允许sysadmin对键值对(65535, 65535)加会话级别的排它咨询锁，普通用户无权限。

- pg_advisory_lock(int4, int4, Name)

  描述：获取指定数据库的排它咨询锁。

  返回值类型：void

- pg_advisory_lock_shared(key bigint)

  描述：获取会话级别的共享咨询锁。

  返回值类型：void

- pg_advisory_lock_shared(key1 int, key2 int)

  描述：获取会话级别的共享咨询锁。

  返回值类型：void

  备注：pg_advisory_lock_shared类似于pg_advisory_lock，不同之处仅在于共享锁会话可以和其他请求共享锁的会话共享资源，但排它锁除外。

- pg_advisory_unlock(key bigint)

  描述：释放会话级别的排它咨询锁。

  返回值类型：Boolean

- pg_advisory_unlock(key1 int, key2 int)

  描述：释放会话级别的排它咨询锁。

  返回值类型：Boolean

  备注：pg_advisory_unlock释放先前取得的排它咨询锁。如果释放成功则返回true。如果实际上并未持有指定的锁，将返回false并在服务器中产生一条SQL警告信息。

- pg_advisory_unlock(int4, int4, Name)

  描述：释放指定数据库上的排它咨询锁。

  返回值类型：Boolean

  备注：如果释放成功则返回true；如果未持有锁，则返回false。

- pg_advisory_unlock_shared(key bigint)

  描述：释放会话级别的共享咨询锁。

  返回值类型：Boolean

- pg_advisory_unlock_shared(key1 int, key2 int)

  描述：释放会话级别的共享咨询锁。

  返回值类型：Boolean

  备注：pg_advisory_unlock_shared类似于pg_advisory_unlock，不同之处在于该函数释放的是共享咨询锁。

- pg_advisory_unlock_all()

  描述：释放当前会话持有的所有咨询锁。

  返回值类型：void

  备注：pg_advisory_unlock_all将会释放当前会话持有的所有咨询锁，该函数在会话结束的时候被隐含调用，即使客户端异常地断开连接也是一样。

- pg_advisory_xact_lock(key bigint)

  描述：获取事务级别的排它咨询锁。

  返回值类型：void

- pg_advisory_xact_lock(key1 int, key2 int)

  描述：获取事务级别的排它咨询锁。

  返回值类型：void

  备注：pg_advisory_xact_lock类似于pg_advisory_lock，不同之处在于锁是自动在当前事务结束时释放，而且不能被显式的释放。只允许sysadmin对键值对(65535, 65535)加事务级别的排它咨询锁，普通用户无权限。

- pg_advisory_xact_lock_shared(key bigint)

  描述：获取事务级别的共享咨询锁。

  返回值类型：void

- pg_advisory_xact_lock_shared(key1 int, key2 int)

  描述：获取事务级别的共享咨询锁。

  返回值类型：void

  备注：pg_advisory_xact_lock_shared类似于pg_advisory_lock_shared，不同之处在于锁是在当前事务结束时自动释放，而且不能被显式的释放。

- pg_try_advisory_lock(key bigint)

  描述：尝试获取会话级排它咨询锁。

  返回值类型：Boolean

  备注：pg_try_advisory_lock类似于pg_advisory_lock，不同之处在于该函数不会阻塞以等待资源的释放。它要么立即获得锁并返回true，要么返回false表示目前不能锁定。

- pg_try_advisory_lock(key1 int, key2 int)

  描述：尝试获取会话级排它咨询锁。

  返回值类型：Boolean

  备注：只允许sysadmin对键值对(65535, 65535)加会话级别的排它咨询锁，普通用户无权限。

- pg_try_advisory_lock_shared(key bigint)

  描述：尝试获取会话级共享咨询锁。

  返回值类型：Boolean

- pg_try_advisory_lock_shared(key1 int, key2 int)

  描述：尝试获取会话级共享咨询锁。

  返回值类型：Boolean

  备注：pg_try_advisory_lock_shared类似于pg_try_advisory_lock，不同之处在于该函数尝试获得共享锁而不是排它锁。

- pg_try_advisory_xact_lock(key bigint)

  描述：尝试获取事务级别的排它咨询锁。

  返回值类型：Boolean

- pg_try_advisory_xact_lock(key1 int, key2 int)

  描述：尝试获取事务级别的排它咨询锁。

  返回值类型：Boolean

  备注：pg_try_advisory_xact_lock类似于pg_try_advisory_lock，不同之处在于如果得到锁，在当前事务的结束时自动释放，而且不能被显式的释放。只允许sysadmin对键值对(65535, 65535)加事务级别的排它咨询锁，普通用户无权限。

- pg_try_advisory_xact_lock_shared(key bigint)

  描述：尝试获取事务级别的共享咨询锁。

  返回值类型：Boolean

- pg_try_advisory_xact_lock_shared(key1 int, key2 int)

  描述：尝试获取事务级别的共享咨询锁。

  返回值类型：Boolean

  备注：pg_try_advisory_xact_lock_shared类似于pg_try_advisory_lock_shared，不同之处在于如果得到锁，在当前事务结束时自动释放，而且不能被显式的释放。

- lock_cluster_ddl()

  描述：尝试对MogDB内所有存活的数据库主节点获取会话级别的排他咨询锁。

  返回值类型：Boolean

  备注：只允许sysadmin调用，普通用户无权限。

- unlock_cluster_ddl()

  描述：尝试对数据库主节点会话级别的排他咨询锁。

  返回值类型：Boolean

- get_lock(text,text)

  描述：用指定的字符串在数据库加用户锁，第二个参数是加锁等待时间。

  返回值类型：Int

- get_lock(text,double)

  描述：用指定的字符串在数据库加用户锁，第二个参数是加锁等待时间。

  返回值类型：Int

- get_lock(text)

  描述：用指定的字符串在数据库加用户锁。

  返回值类型：Int

- release_lock(text)

  描述：释放指定的锁，如果释放成功，返回1，如果当前会话并未持有指定锁，返回0，如果当前锁并不存在（锁必须有人持有才会出现），返回NULL。

  返回值类型：Int

- is_free_lock(text)

  描述：检查字符串是否空闲，如果没有被加锁返回1，否则返回0，如果检查期间出现其他错误，返回NULL。

  返回值类型：Int

- is_used_lock(text)

  描述：检查字符串的锁被谁持有，返回对应的用户的会话id，如果指定锁无人持有，返回NULL。

  返回值类型：Bigint

- clear_all_invalid_locks()

  描述：清除lockname HASH表中无效锁的信息，返回清除的锁的数量。

  返回值类型：Bigint

- release_all_locks()

  描述：释放当前会话持有的所有的锁，返回释放的次数（对于单个字符串持有多个的情况，按对应数字计算而非只计算一次）。

  返回值类型：Bigint

- get_all_locks()

  描述：查询当前库中的所有用户锁，以记录的形式返回所有用户锁的名字和持有人信息。

  返回值类型：Record