---
title: SHOW PLUGINS
summary: SHOW PLUGINS
author: zhang cuiping
date: 2022-10-24
---

# SHOW PLUGINS

## 功能描述

查看当前数据库中插件清单。

## 注意事项

N/A

## 语法格式

```
SHOW PLUGINS
```

## 参数说明

N/A

## 示例

```sql
--查看插件清单
MogDB=# SHOW PLUGINS;
      Name       |  Status  | Type | Library | License |                      Comment                       
-----------------+----------+------+---------+---------+----------------------------------------------------
 roach_api_stub  | DISABLED |      | NULL    |         | roach api stub
 file_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 security_plugin | ACTIVE   |      | NULL    |         | provides security functionality
 hdfs_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 plpgsql         | ACTIVE   |      | NULL    |         | PL/pgSQL procedural language
 dolphin         | ACTIVE   |      | NULL    |         | sql engine
 dist_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for distfs access
 postgres_fdw    | DISABLED |      | NULL    |         | foreign-data wrapper for remote PostgreSQL servers
 hstore          | ACTIVE   |      | NULL    |         | data type for storing sets of (key, value) pairs
 log_fdw         | ACTIVE   |      | NULL    |         | Foreign Data Wrapper for accessing logging data

--插件状态的更新显示
MogDB=# drop extension hstore;
MogDB=# SHOW PLUGINS;
      Name       |  Status  | Type | Library | License |                      Comment                       
-----------------+----------+------+---------+---------+----------------------------------------------------
 roach_api_stub  | DISABLED |      | NULL    |         | roach api stub
 file_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 security_plugin | ACTIVE   |      | NULL    |         | provides security functionality
 hdfs_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 plpgsql         | ACTIVE   |      | NULL    |         | PL/pgSQL procedural language
 dolphin         | ACTIVE   |      | NULL    |         | sql engine
 dist_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for distfs access
 postgres_fdw    | DISABLED |      | NULL    |         | foreign-data wrapper for remote PostgreSQL servers
 hstore          | DISABLED |      | NULL    |         | data type for storing sets of (key, value) pairs
 log_fdw         | ACTIVE   |      | NULL    |         | Foreign Data Wrapper for accessing logging data

MogDB=# CREATE extension hstore;
MogDB=# show plugins;
      Name       |  Status  | Type | Library | License |                      Comment                       
-----------------+----------+------+---------+---------+----------------------------------------------------
 roach_api_stub  | DISABLED |      | NULL    |         | roach api stub
 file_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 security_plugin | ACTIVE   |      | NULL    |         | provides security functionality
 hdfs_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for flat file access
 plpgsql         | ACTIVE   |      | NULL    |         | PL/pgSQL procedural language
 dolphin         | ACTIVE   |      | NULL    |         | sql engine
 dist_fdw        | ACTIVE   |      | NULL    |         | foreign-data wrapper for distfs access
 postgres_fdw    | DISABLED |      | NULL    |         | foreign-data wrapper for remote PostgreSQL servers
 hstore          | ACTIVE   |      | NULL    |         | data type for storing sets of (key, value) pairs
 log_fdw         | ACTIVE   |      | NULL    |         | Foreign Data Wrapper for accessing logging data
```