---
title: PREPARE
summary: PREPARE
author: zhang cuiping
date: 2022-10-24
---

# PREPARE

## 功能描述

创建一个预备语句。

预备语句是服务端的对象，可以用于优化性能。在执行PREPARE语句的时候，指定的查询被解析、分析、重写。当随后发出EXECUTE语句的时候，预备语句被规划和执行。这种设计避免了重复解析、分析工作。PREPARE语句创建后在整个数据库会话期间一直存在，一旦创建成功，即便是在事务块中创建，事务回滚，PREPARE也不会删除。只能通过显式调用DEALLOCATE进行删除，会话结束时，PREPARE也会自动删除。

## 注意事项

N/A

## 语法格式

```
PREPARE name [ ( data_type [, ...] ) ] AS statement;
PREPARE name FROM statement;
```

## 参数说明

- **name**

  指定预备语句的名称。它必须在该会话中是唯一的。

- **data_type**

  参数的数据类型。

- **statement**

  是SELECT INSERT、UPDATE、DELETE、MERGE INTO或VALUES语句之一。

## 示例

```sql
MogDB=# CREATE TABLE test(name text, age int);
CREATE TABLE
MogDB=# INSERT INTO test values('a',18);
INSERT 0 1
MogDB=# PREPARE stmt FROM SELECT * FROM test;
PREPARE
MogDB=# EXECUTE stmt;
 name | age 
------+-----
 a    |  18
(1 row)
```