---
title: 内核MySQL兼容性说明
summary: 内核MySQL兼容性说明
author: zhang cuiping
date: 2022-10-24
---

# DROP TABLESPACE

## 功能描述

删除一个表空间。

## 注意事项

相比于原始的MogDB，dolphin对于`DROP TABLESPACE`语法的修改主要为：

新增`ENGINE [=] engine_name`可选项，无实际意义，仅作语法兼容。

## 语法格式

```
DROP TABLESPACE [ IF EXISTS ] tablespace_name [ENGINE [=] engine_name];
```

## 参数说明

- **IF EXISTS**

  如果指定的表空间不存在，则发出一个notice而不是抛出一个错误。

- **tablespace_name**

  表空间的名称。

  取值范围：已存在的表空间的名称。

- **engine_name**

  无实际意义。

  取值范围：任意字符串。

## 示例

请参见CREATE TABLESPACE的[示例](./dolphin-create-tablespace.md)。

## 相关链接

[ALTER TABLESPACE](./dolphin-alter-tablespace.md)，[CREATE TABLESPACE](./dolphin-create-tablespace.md)