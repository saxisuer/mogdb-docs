---
title: AST
summary: AST
author: zhang cuiping
date: 2022-10-24
---

# AST

## 功能描述

MogDB语法树校验。

对AST语法后的语句是否支持生成MogDB语法树作判断。

## 注意事项

校验不通过时，会抛出语法解析相应错误。校验通过时不作任何回显操作。

## 语法格式

```
AST [ STMT ] ;
```

## 参数说明

- **STMT**

支持任意类型SQL语句、存储过程语句等。

## 示例

```
-- 建表语句校验
MogDB=# AST CREATE TABLE TEST(ID INT6);

-- 不支持语句校验
MogDB=# AST CREATE TABLE TEST;
ERRPR: syntax error at or near ";"
LINE 1:AST CREATE TABLE TEST;
                            ^ 
```