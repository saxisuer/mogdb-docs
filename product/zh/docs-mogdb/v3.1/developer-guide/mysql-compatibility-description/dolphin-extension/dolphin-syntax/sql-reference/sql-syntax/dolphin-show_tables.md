---
title: SHOW TABLES
summary: SHOW TABLES
author: zhang cuiping
date: 2022-10-24
---

# SHOW TABLES

## 功能描述

查看当前库（或schema)的表清单。

## 注意事项

- 若不指定db_name，查询的是当前库(或schema)下的表清单。

## 语法格式

```
SHOW [FULL] TABLES
   [{FROM | IN} db_name]
   [LIKE 'pattern' | WHERE expr]
```

## 参数说明

- **db_name**

  库名（或schema）,可选项，若不指定，则查询的是当前库或schema。

- **LIKE 'pattern'**

  patternp匹配显示结果第一列(列名为'Tables_in_dbname [`pattern`]'）。

## 示例

```sql
--创建简单表
MogDB=# CREATE SCHEMA tst_schema;
MogDB=# SET SEARCH_PATH TO tst_schema;

MogDB=# CREATE TABLE tst_t1
MogDB-# (
MogDB(# id int primary key,
MogDB(# name varchar(20) NOT NULL,
MogDB(# addr text COLLATE "de_DE",
MogDB(# phone text COLLATE "es_ES",
MogDB(# addr_code text
MogDB(# );
MogDB=# CREATE VIEW tst_v1 AS SELECT * FROM tst_t1;
MogDB=# CREATE TABLE t_t2(id int);

--查看库（或SCHEMA）下表清单信息
MogDB=# show tables;
 Tables_in_tst_schema 
----------------------
 tst_t1
 tst_v1
 t_t2

MogDB=# show full tables;
 Tables_in_tst_schema | Table_type 
----------------------+------------
 tst_t1               | BASE TABLE
 tst_v1               | VIEW
 t_t2                 | BASE TABLE

MogDB=# show full tables in tst_schema;
 Tables_in_tst_schema | Table_type 
----------------------+------------
 tst_t1               | BASE TABLE
 tst_v1               | VIEW
 t_t2                 | BASE TABLE
 
--模糊匹配、过滤
MogDB=# show full tables like '%tst%';
 Tables_in_tst_schema (%tst%) | Table_type 
------------------------------+------------
 tst_t1                       | BASE TABLE
 tst_v1                       | VIEW

MogDB=# show full tables where Table_type='VIEW';
 Tables_in_tst_schema | Table_type 
----------------------+------------
 tst_v1               | VIEW
 
```