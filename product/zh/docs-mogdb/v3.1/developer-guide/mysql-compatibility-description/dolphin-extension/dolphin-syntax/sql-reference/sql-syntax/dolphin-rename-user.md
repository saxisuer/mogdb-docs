---
title: RENAME USER
summary: RENAME USER
author: zhang cuiping
date: 2022-10-24
---

# RENAME USER

## 功能描述

修改数据库中的用户名。

## 注意事项

- RENAME USER会修改用户名，并且只能修改当前表的user的名称。
- 如果修改多个用户，并且其中一个用户名不存在或其他原因导致执行失败，整条语句都会失败，所有用户名都会保持不变。
- 与`ALTER USER ... RENAME TO ...`等价。

## 语法格式

```
RENAME USER 
    old_user1 TO new_user1,
    old_user2 TO new_user2,
    ...
```

## 参数说明

- **old_user** 旧的用户名，必须已存在
- **new_user** 新的用户名

## 示例

```
rename user 
    user1 to user4, 
    user2 to user5,
    user3 to user6;
```

## 相关链接

[ALTER USER](../../../../../../reference-guide/sql-syntax/ALTER-USER.md)