---
title: 字符类型
summary: 字符类型
author: zhang cuiping
date: 2022-10-24
---

# 字符类型

相比于原始的MogDB，dolphin对于字符类型的修改主要为：

1. 修改`CHARACTER/NCHAR`类型n的含义，n是指字符长度而不是字节长度。
2. 所有的字符数据类型在对比时，均忽略尾部空格，如where条件过滤场景、join场景等。例如 'a'::text = 'a '::text 为真。需要特别注意的是，对于VARCHAR、VARCHAR2、NVARCHAR2、NVARCHAR、TEXT、CLOB类型，只有GUC参数 string_hash_compatible 为on的情况下，hash join以及hash agg才会忽略尾部空格。
3. 新增`TEXT`支持可选的修饰符(n)，即支持`TEXT(n)`的用法，n无实际意义，不影响任何表现。
4. 新增`TINYTEXT(n)/MEDIUMTEXT(n)/LONGTEXT(n)`数据类型，是`TEXT`的别名，n无实际作用，不影响任何表现。

**表 1** 字符类型

| 名称                                          | 描述                                                         | 存储空间                                                     |
| :-------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| CHAR(n)CHARACTER(n)NCHAR(n)                   | 定长字符串，不足补空格。n是指字符长度，如不带精度n，默认精度为1。 | 最大为10MB。                                                 |
| TEXT(n),TINYTEXT(n),MEDIUMTEXT(n),LONGTEXT(n) | 变长字符串。n无实际含义，不影响任何表现                      | 最大为1GB-1，但还需要考虑到列描述头信息的大小， 以及列所在元组的大小限制（也小于1GB-1），因此TEXT类型最大大小可能小于1GB-1。 |

示例：

```sql
--创建表。
MogDB=# CREATE TABLE char_type_t1 
(
    CT_COL1 CHARACTER(4),
    CT_COL2 TEXT(10),
    CT_COL3 TINYTEXT(11),
    CT_COL4 MEDIUMTEXT(12),
    CT_COL5 LONGTEXT(13)
);

--查看表结构
MogDB=# \d char_type_t1 
    Table "public.char_type_t1"
 Column  |     Type     | Modifiers
---------+--------------+-----------
 ct_col1 | character(4) |
 ct_col2 | text         |
 ct_col3 | text         |
 ct_col4 | text         |
 ct_col5 | text         |

--插入数据
MogDB=# INSERT INTO char_type_t1 VALUES('四个字符');
MogDB=# INSERT INTO char_type_t1 VALUES('e   ');

--查看数据
MogDB=# SELECT CT_COL1,length(CT_COL1) FROM char_type_t1;
 ct_col1  | length
----------+--------
 四个字符 |      4
 e        |      1
(2 rows)

--过滤数据
MogDB=# SELECT CT_COL1 FROM char_type_t1 WHERE CT_COL1 = 'e';
 ct_col1
---------
 e
(1 row)

MogDB=# SELECT CT_COL1 FROM char_type_t1 WHERE CT_COL1 = 'e ';
 ct_col1
---------
 e
(1 row)

--删除表
MogDB=# DROP TABLE char_type_t1;
```