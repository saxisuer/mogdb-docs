---
title: USE db_name
summary: USE db_name
author: zhang cuiping
date: 2022-10-24
---

# USE db_name

## 功能描述

USE db_name语句将db_name数据库作为默认（当前）数据库使用，用于后续语句。该数据库保持为默认数据库，直到语段的结尾，或者直到发布一个不同的USE语句。

## 注意事项

N/A

## 语法格式

```
USE db_name
```

## 参数说明

- **db_name**

   数据库名。

## 示例

```sql
--切换到db1库
MogDB=# USE db1;
SET
MogDB=# CREATE TABLE test(a text);
CREATE TABLE
MogDB=# INSERT INTO test VALUES('db1');
INSERT 0 1

--切换到db2库
MogDB=# USE db2;
SET
MogDB=# CREATE TABLE test(a text);
CREATE TABLE
MogDB=# INSERT INTO test VALUES('db2');
INSERT 0 1
MogDB=# select a from db1.test;
  a  
-----
 db1
(1 row)

MogDB=# select a from db2.test;
  a  
-----
 db2
(1 row)

MogDB=# select a from test;
  a  
-----
 db2
(1 row)

--切换到db1库
MogDB=# USE db1;
SET
MogDB=# select a from test;
  a  
-----
 db1
(1 row)
```

## 相关链接

N/A