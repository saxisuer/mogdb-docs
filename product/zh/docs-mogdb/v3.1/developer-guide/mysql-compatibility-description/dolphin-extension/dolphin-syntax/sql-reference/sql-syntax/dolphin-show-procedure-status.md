---
title: SHOW PROCEDURE STATUS
summary: SHOW PROCEDURE STATUS
author: zhang cuiping
date: 2022-10-24
---

# SHOW PROCEDURE STATUS

## 注意事项

N/A

## 功能描述

显示有关存储过程的信息。

## 语法格式

```
SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE expr]
```

## 参数说明

参考[SHOW FUNCTION STATUS](./dolphin-show-function-status.md).

## 展示内容

参考[SHOW FUNCTION STATUS](./dolphin-show-function-status.md).

## 示例

```sql
MogDB=# -- 创建存储过程
MogDB=# create or replace procedure proc1() as declare genre_rec record; --声明记录类型
MogDB$# begin
MogDB$# for genre_rec in (select e1.ename from public.emp1 e1 join public.emp1mot e1m on e1.mgr = e1m.mgr)
MogDB$# loop
MogDB$# raise notice '%', genre_rec."ename"; --打印
MogDB$# end loop;
MogDB$# end;
MogDB$# /
CREATE PROCEDURE
    MogDB=# -- 查看信息
MogDB=# show procedure status like 'proc%';
   Db   | Name  |   Type    | Definer |           Modified            |            Created            | Security_type | Comment | character_set_client | collation_connection |Database Collation
--------+-------+-----------+---------+-------------------------------+-------------------------------+---------------+---------+----------------------+----------------------+--------------------
 public | proc1 | PROCEDURE | wyc     | 2022-09-24 14:46:40.868293+08 | 2022-09-24 14:46:40.868293+08 | INVOKER       |         |                      |                      |en_US.UTF-8
(1 row)
```