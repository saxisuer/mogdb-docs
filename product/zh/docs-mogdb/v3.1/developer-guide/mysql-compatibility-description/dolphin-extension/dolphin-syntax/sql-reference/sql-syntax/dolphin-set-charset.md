---
title: SET CHARSET
summary: SET CHARSET
author: zhang cuiping
date: 2022-10-24
---

# SET CHARSET

## 功能描述

设置客户端的字符编码类型。

## 注意事项

- MogDB中该语句等价于set client_encoding。
- 请根据前端业务的情况确定，客户端编码和服务器端编码尽量保持一致，提高效率。
- 兼容PostgreSQL所有的字符编码类型。

## 语法格式

```
SET {CHARACTER SET | CHARSET} {'charset_name' | DEFAULT}
```

## 参数说明

- **{CHARACTER SET | CHARSET}**

  两者是等价的。

- **{'charset_name' | DEFAULT}**

  charset_name支持MogDB可设置的字符编码类型，如utf8、gbk等；指定DEFAULT时会将字符集重置为默认的字符集。 charset_name支持以下形式：

  1. utf8
  2. 'utf8'
  3. “utf8”

## 示例

```sql
MogDB=# show client_encoding;
-[ RECORD 1 ]---+----
client_encoding | GBK

MogDB=# set charset gbk;
SET
db_show=# show client_encoding;
-[ RECORD 1 ]---+----
client_encoding | GBK

MogDB=# set charset default;
SET
MogDB=# show client_encoding;
-[ RECORD 1 ]---+-----
client_encoding | UTF8

MogDB=# set character set 'gbk';
SET
MogDB=# show client_encoding;
-[ RECORD 1 ]---+----
client_encoding | GBK

MogDB=# set character set default;
SET
MogDB=# show client_encoding;
-[ RECORD 1 ]---+-----
client_encoding | UTF8
```