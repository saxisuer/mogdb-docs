---
title: SHOW SLAVE HOSTS
summary: SHOW SLAVE HOSTS
author: zhang cuiping
date: 2022-10-24
---

# SHOW SLAVE HOSTS

## 功能描述

查看wal（xlog）日志同步状态信息，例如发起端发送日志位置，接收端接收日志位置等。

## 注意事项

- 在主库执行有效。
- 展示结果和select * from pg_stat_replication一致。

## 语法格式

```
SHOW SLAVE HOSTS
```

## 参数说明

- **pid**

  线程的PID。

- **usesysid**

  用户系统ID。

- **usename**

  用户名。

- **application_name**

  程序名称。

- **client_addr**

  客户端地址。

- **client_port**

  客户端端口。

- **backend_start**

  程序启动时间。

- **state**

  日志复制的状态： 追赶状态、 一致的流状态。

- **sender_sent_location**

  发送端发送日志位置。

- **receiver_write_location**

  接收端write日志位置。

- **receiver_flush_location**

  接收端flush日志位置。

- **receiver_replay_location**

  接收端replay日志位置。

- **sync_priority**

  同步复制的优先级（0表示异步）。

- **sync_priority**

  同步状态： 异步复制、 同步复制、 潜在同步者。

## 示例

```sql
MogDB=# show slave hosts;
-[ RECORD 1 ]------------+----------------------------------
pid                      | 140395615176448
usesysid                 | 10
usename                  | mogdb
application_name         | WalSender to Standby[walreceiver]
client_addr              | 127.0.0.1
client_hostname          |
client_port              | 43174
backend_start            | 2022-08-23 18:41:12.398717+08
state                    | Streaming
sender_sent_location     | 0/1098BB08
receiver_write_location  | 0/1098BB08
receiver_flush_location  | 0/1098BB08
receiver_replay_location | 0/1098BB08
sync_priority            | 1
sync_state               | Sync
```