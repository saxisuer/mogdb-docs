---
title: DCL语法一览表
summary: DCL语法一览表
author: zhang cuiping
date: 2022-10-24
---

# DCL语法一览表

DCL（Data Control Language数据控制语言），是用来创建用户角色、设置或更改数据库用户或角色权限的语句。

## SHOW

MogDB支持使用SHOW命令展示各种对象信息。所涉及的SQL语句，请参考下表。

**表 1** SHOW语句相关SQL

| 功能                                 | 相关SQL                                                      |
| :----------------------------------- | :----------------------------------------------------------- |
| 展示索引信息                         | [SHOW INDEX](sql-syntax/dolphin-show-index.md)               |
| 查当前外部连接（或内部线程）相关信息 | [SHOW PROCESSLIST](sql-syntax/dolphin-show-processlist.md)   |
| 展示创建表语句                       | [SHOW-CREATE-TABLE](sql-syntax/dolphin-create-table.md)      |
| 展示创建函数语句                     | SHOW CREATE FUNCTION                                         |
| 展示创建存储过程语句                 | SHOW CREATE PROCEDURE                                        |
| 展示创建数据库语句                   | SHOW CREATE DATABASE                                         |
| 展示创建触发器语句                   | SHOW CREATE TRIGGER                                          |
| 展示创建视图语句                     | SHOW CREATE VIEW                                             |
| 查询guc参数                          | SHOW-VARIABLES                                               |
| 显示MogDB中对用户的权限信息          | [SHOW GRANTS](sql-syntax/dolphin-show-grants.md)             |
| 显示有关存储函数的信息               | [SHOW FUNCTION STATUS](sql-syntax/dolphin-show-function-status.md) |
| 显示有关存储过程的信息               | [SHOW PROCEDURE STATUS](sql-syntax/dolphin-show-procedure-status.md) |
| 显示有关触发器的信息                 | [SHOW TIRRGER](sql-syntax/dolphin-show-triggers.md)          |

## GRANT

MogDB支持使用GRANT命令授予各种权限。所涉及的SQL语句，请参考下表。

**表 2** GRANT语句相关SQL

| 功能                                    | 相关SQL                                                      |
| :-------------------------------------- | :----------------------------------------------------------- |
| 允许新建索引的权限                      | [GRANT INDEX](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 允许对function和procedure进行修改的权限 | [GRANT ALTER ROUTINE](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 允许新建function和procedure的权限       | [GRANT CREATE ROUTINE](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 允许创建临时表的权限                    | [GRANT CREATE TEMPORARY TABLES](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 允许当前用户新建用户的权限              | [GRANT CREATE USER](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 允许创建新的表空间的权限                | [GRANT CREATE TABLESPACE](../../../../../reference-guide/sql-syntax/GRANT.md) |
| 授予代理者权限                          | [GRANT PROXY](../../../../../reference-guide/sql-syntax/GRANT.md) |

## REVOKE

MogDB支持使用REVOKE命令撤销各种权限。所涉及的SQL语句，请参考下表。

**表 3** REVOKE语句相关SQL

| 功能                                    | 相关SQL                                                      |
| :-------------------------------------- | :----------------------------------------------------------- |
| 允许新建索引的权限                      | [REVOKE INDEX](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 允许对function和procedure进行修改的权限 | [REVOKE ALTER ROUTINE](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 允许新建function和procedure的权限       | [REVOKE CREATE ROUTINE](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 允许创建临时表的权限                    | [REVOKE CREATE TEMPORARY TABLES](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 允许当前用户新建用户的权限              | [REVOKE CREATE USER](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 允许创建新的表空间的权限                | [REVOKE CREATE TABLESPACE](../../../../../reference-guide/sql-syntax/REVOKE.md) |
| 召回代理者权限                          | [REVOKE PROXY](../../../../../reference-guide/sql-syntax/REVOKE.md) |

## KILL

MogDB支持使用KILL命令终止指定连接或该连接下执行的SQL语句。所涉及的SQL语句，请参考下表。

**表 4** KILL语句相关SQL

| 功能                                  | 相关SQL                            |
| :------------------------------------ | :--------------------------------- |
| 终止指定连接或该连接下执行的SQL语句。 | [KILL](sql-syntax/dolphin-kill.md) |

## SET PASSWORD

MogDB支持使用SET PASSWORD命令修改用户密码。所涉及的SQL语句，请参考下表。

**表 5** SET PASSWORD语句相关SQL

| 功能           | 相关SQL                                            |
| :------------- | :------------------------------------------------- |
| 修改用户密码。 | [SET PASSWORD](sql-syntax/dolphin-set-password.md) |