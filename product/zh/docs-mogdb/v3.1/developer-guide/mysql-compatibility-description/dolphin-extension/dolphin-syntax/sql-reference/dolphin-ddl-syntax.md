---
title: DDL语法一览表
summary: DDL语法一览表
author: zhang cuiping
date: 2022-10-24
---

# DDL语法一览表

DDL（Data Definition Language数据定义语言），用于定义或修改数据库中的对象。如：表、索引、视图等。

## 定义表空间

表空间用于管理数据对象，与磁盘上的一个目录对应。所涉及的SQL语句，请参考下表。

**表 1** 表空间定义相关SQL

| 功能           | 相关SQL                                                      |
| :------------- | :----------------------------------------------------------- |
| 创建表空间     | [CREATE TABLESPACE](../../../../../reference-guide/sql-syntax/CREATE-TABLESPACE.md) |
| 修改表空间属性 | [ALTER TABLESPACE](../../../../../reference-guide/sql-syntax/ALTER-TABLESPACE.md) |
| 删除表空间     | [DROP TABLESPACE](../../../../../reference-guide/sql-syntax/DROP-TABLESPACE.md) |

## 定义表

表是数据库中的一种特殊数据结构，用于存储数据对象以及对象之间的关系。所涉及的SQL语句，请参考下表。

**表 2** 表定义相关SQL

| 功能       | 相关SQL                                                      |
| :--------- | :----------------------------------------------------------- |
| 创建表     | [CREATE TABLE](../../../../../reference-guide/sql-syntax/CREATE-TABLE.md) |
| 修改表属性 | [ALTER TABLE](../../../../../reference-guide/sql-syntax/ALTER-TABLE.md) |

## 定义索引

索引是对数据库表中一列或多列的值进行排序的一种结构，使用索引可快速访问数据库表中的特定信息。所涉及的SQL语句，请参考下表。

**表 3** 索引定义相关SQL

| 功能     | 相关SQL                                                      |
| :------- | :----------------------------------------------------------- |
| 定义索引 | [CREATE INDEX](../../../../../reference-guide/sql-syntax/CREATE-INDEX.md) |

## 定义分区表

分区表是一种逻辑表，数据是由普通表存储的，主要用于提升查询性能。所涉及的SQL语句，请参考下表。

**表 4** 分区表定义相关SQL

| 功能           | 相关SQL                                                      |
| :------------- | :----------------------------------------------------------- |
| 创建分区表     | [CREATE TABLE PARTITION](../../../../../reference-guide/sql-syntax/CREATE-TABLE-PARTITION.md) |
| 创建分区       | [ALTER TABLE PARTITION](../../../../../reference-guide/sql-syntax/ALTER-TABLE-PARTITION.md) |
| 修改分区表属性 | [ALTER TABLE PARTITION](../../../../../reference-guide/sql-syntax/ALTER-TABLE-PARTITION.md) |
| 删除分区       | [DROP TABLE](../../../../../reference-guide/sql-syntax/DROP-TABLE.md) |

## 定义存储过程

存储过程是一组为了完成特定功能的SQL语句集，经编译后存储在数据库中，用户通过指定存储过程的名称并给出参数（如果该存储过程带有参数）来执行它。所涉及的SQL语句，请参考下表。

**表 5** 存储过程定义相关SQL

| 功能         | 相关SQL                                                      |
| :----------- | :----------------------------------------------------------- |
| 创建存储过程 | [CREATE PROCEDURE](../../../../../reference-guide/sql-syntax/CREATE-PROCEDURE.md) |
| 修改存储过程 | [ALTER PROCEDURE](../../../../../reference-guide/sql-syntax/ALTER-PROCEDURE.md) |

## 定义函数

在MogDB中，它和存储过程类似，也是一组SQL语句集，使用上没有差别。所涉及的SQL语句，请参考下表。

**表 6** 函数定义相关SQL

| 功能         | 相关SQL                                                      |
| :----------- | :----------------------------------------------------------- |
| 创建函数     | [CREATE FUNCTION](../../../../../reference-guide/sql-syntax/CREATE-FUNCTION.md) |
| 修改函数属性 | [ALTER FUNCTION](../../../../../reference-guide/sql-syntax/ALTER-FUNCTION.md) |

## 计算表数据校验和

针对查询时刻对所查询表的可见的数据的校验和计算，该校验和与数据的先后顺序、存入位置、表名等无关，仅仅针对实际数据。所涉及的SQL语句，请参考下表。

**表 7** 函数定义相关SQL

| 功能             | 相关SQL                                                |
| :--------------- | :----------------------------------------------------- |
| 计算表数据校验和 | [CHECKSUM TABLE](sql-syntax/dolphin-checksum-table.md) |