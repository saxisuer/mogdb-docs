---
title: SET PASSWORD
summary: SET PASSWORD
author: zhang cuiping
date: 2022-10-24
---

# SET PASSWORD

## 功能描述

修改用户密码。

## 注意事项

- 不指定用户则修改当前连接用户密码。
- 初始用户可以修改任何用户的密码（包括自身密码），不需要指定REPLACE校验当前密码。
- 非初始用户不能修改初始用户的密码。
- sysadmin和拥有createrole权限的用户可以修改其他（非初始化、非sysadmin，非createrole权限）用户密码，不需要指定REPLACE校验当前密码。
- sysadmin和拥有createrole权限的用户修改自身密码时，需要指定REPLACE校验当前密码。

## 语法格式

```
SET PASSWORD [FOR user] = password_option [REPLACE 'current_auth_string']

password_option: {
    'auth_string'
  | PASSWORD('auth_string')
}
```

## 参数说明

- **[FOR user]**

  user支持以下形式：

  1. user（不区分大小写）。
  2. 'user'（区分大小写）。
  3. “user”（区分大小写）。
  4. 'user'@'host'（区分大小写）。
  5. current_user()/current_user。

- **auth_string**

  需要设置的密码。

- **current_auth_string**

  当前密码。

## 示例

```sql
--修改指定用户密码
MogDB=# create user user1 with password 'abcd@123';
CREATE ROLE
MogDB=# set password for user1 = 'abcd@124';
ALTER ROLE

--修改当前用户密码
MogDB=# set password = 'abcd@123';
ALTER ROLE
MogDB=# set password for current_user = 'abcd@123';
ALTER ROLE
MogDB=# set password for current_user() = 'abcd@123';
ALTER ROLE
```