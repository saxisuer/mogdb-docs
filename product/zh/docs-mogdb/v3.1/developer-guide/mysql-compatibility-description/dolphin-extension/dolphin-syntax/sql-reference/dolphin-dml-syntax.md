---
title: DML语法一览表
summary: DML语法一览表
author: zhang cuiping
date: 2022-10-24
---

# DML语法一览表

DML（Data Manipulation Language数据操作语言），用于对数据库表中的数据进行操作。如：插入、更新、查询、删除。

## 插入数据

插入数据是往数据库表中添加一条或多条记录，请参考[INSERT](../../../../../reference-guide/sql-syntax/INSERT.md)。

## 修改数据

修改数据是修改数据库表中的一条或多条记录，请参考[UPDATE](../../../../../reference-guide/sql-syntax/UPDATE.md)。

## 查询数据

数据库查询语句SELECT是用于在数据库中检索适合条件的信息，请参考[SELECT](../../../../../reference-guide/sql-syntax/SELECT.md)。