---
title: 赋值操作符
summary: 赋值操作符
author: zhang cuiping
date: 2022-10-24
---

# 赋值操作符

相比于原始的MogDB，dolphin对于赋值操作符的修改主要为: 

1. 新增支持通过`:=`的方式赋值。如 `UPDATE table_name SET col_name := new_val;`。