---
title: 数值类型
summary: 数值类型
author: zhang cuiping
date: 2022-10-24
---

# 数值类型

数字操作符和相关的内置函数请参见[数字操作函数和操作符](../../../../../../reference-guide/functions-and-operators/7-mathematical-functions-and-operators.md)。

相比于原始的MogDB，dolphin对于数值类型的修改主要为:

1. 新增`INT/TINYINT/SMALLINT/BIGINT`支持可选的修饰符(n)，即支持`TINYINT(n)/SMALLINT(n)/BIGINT(n)`的用法，n无实际意义，不影响任何表现。
2. 新增`MEDIUMINT(n)`数据类型，是INT4的别名，n无实际作用，不影响任何表现。存储空间为4字节，数据范围为`-2,147,483,648 ~ +2,147,483,647`。
3. 新增`FIXED[(p[,s])]`数据类型，是NUMERIC类型的别名。用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。未指定精度的情况下，小数点前最大131,072位，小数点后最大16,383位。
4. 新增`float4(p[,s])`的方式，等价于`dec(p[,s])`。
5. 新增`double`数据类型，是float8的别名。
6. 新增`float4/float`支持可选的修饰符(n)，即支持`float4(n)/float(n)`的用法，当n在 [1,24]之间时，`float4(n)/float(n)`代表单精度浮点数；当n在 [25,53]之间时，`float4(n)/float(n)`代表双精度浮点数。
7. 对于`decimal`数据类型，在未指定精度的情况下，默认精度为`(10,0)`，即总位数为10，小数位数为0。
8. 新增`UNSIGNED INT/TINYINT/SMALLINT/BIGINT`类型，与普通整型相比，其最高位是数字位而非符号位。
9. 新增zerofill属性修饰，只是语法上的支持，实际并没有填充零的效果。与UNSIGNED的作用等价。
10. 新增cast函数类型转换参数signed/unsigned，其中cast as unsigned将类型转换为uint8，cast as signed将类型转换为int8.

**表 1** 整数类型

| 名称                  | 描述                                                         | 存储空间 | 范围                                                    |
| :-------------------- | :----------------------------------------------------------- | :------- | :------------------------------------------------------ |
| TINYINT(n)            | 微整数，别名为INT1。n无实际作用，不影响任何表现。            | 1字节    | 0 ~ 255                                                 |
| SMALLINT(n)           | 小范围整数，别名为INT2。n无实际作用，不影响任何表现。        | 2字节    | -32,768 ~ +32,767                                       |
| INTEGER(n)            | 常用的整数，别名为INT4。n无实际作用，不影响任何表现。        | 4字节    | -2,147,483,648 ~ +2,147,483,647                         |
| MEDIUMINT(n)          | INT4的别名，n无实际作用，不影响任何表现。                    | 4字节    | -2,147,483,648 ~ +2,147,483,647                         |
| BIGINT(n)             | 大范围的整数，别名为INT8。n无实际作用，不影响任何表现。      | 8字节    | -9,223,372,036,854,775,808 ~ +9,223,372,036,854,775,807 |
| TINYINT(n) UNSIGNED   | 无符号微整数，别名为UINT1。n无实际作用，不影响任何表现。     | 1字节    | 0 ~ 255                                                 |
| SMALLINT(n) UNSIGNED  | 无符号小范围整数，别名为UINT2。n无实际作用，不影响任何表现。 | 2字节    | 0 ~ +65,535                                             |
| INTEGER(n) UNSIGNED   | 无符号整数，别名为UINT4。n无实际作用，不影响任何表现。       | 4字节    | 0 ~ +4,294,967,295                                      |
| MEDIUMINT(n) UNSIGNED | UINT4的别名，n无实际作用，不影响任何表现。                   | 4字节    | 0 ~ +4,294,967,295                                      |
| BIGINT(n) UNSIGNED    | 大范围的无符号整数，别名为UINT8。n无实际作用，不影响任何表现。 | 8字节    | 0 ~ +18,446,744,073,709,551,615                         |

示例：

```sql
--创建具有TINYINT(n), SMALLINT(n), MEDIUMINT(n), BIGINT(n)类型数据的表。
MogDB=# CREATE TABLE int_type_t1
           (
            IT_COL1 TINYINT(10),
            IT_COL2 SMALLINT(20),
            IT_COL3 MEDIUMINT(30),
            IT_COL4 BIGINT(40),
            IT_COL5 INTEGER(50)
           );

--查看表结构。
MogDB=# \d int_type_t1
   Table "public.int_type_t1"
 Column  |   Type   | Modifiers
---------+----------+-----------
 it_col1 | tinyint  |
 it_col2 | smallint |
 it_col3 | integer  |
 it_col4 | bigint   |
 it_col5 | integer  |

--创建带zerofill属性字段的表。
MogDB=# CREATE TABLE int_type_t2
           (
            IT_COL1 TINYINT(10) zerofill,
            IT_COL2 SMALLINT(20) unsigned zerofill,
            IT_COL3 MEDIUMINT(30) unsigned,
            IT_COL4 BIGINT(40) zerofill,
            IT_COL5 INTEGER(50) zerofill
           );

--查看表结构。
MogDB=# \d int_type_t2
   Table "public.int_type_t2"
 Column  | Type  | Modifiers
---------+-------+-----------
 it_col1 | uint1 |
 it_col2 | uint2 |
 it_col3 | uint4 |
 it_col4 | uint8 |
 it_col5 | uint4 |

--删除表。
MogDB=# DROP TABLE int_type_t1, int_type_t2;

--利用cast unsigned将表达式转换为uint8类型
MogDB=# select cast(1 - 2 as unsigned);
        uint8
----------------------
 18446744073709551615
(1 row)

--利用cast signed将表达式转换为int8类型
MogDB=# select cast(1 - 2 as signed);
 int8
------
   -1
(1 row)
```

**表 2** 任意精度型

| 名称                                            | 描述                                                         | 存储空间                                                     | 范围                                                         |
| :---------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| NUMERIC[(p[,s])],DECIMAL[(p[,s])]FIXED[(p[,s])] | 精度p取值范围为[1,1000]，标度s取值范围为[0,p]。说明：p为总位数，s为小数位数。 | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 | 未指定精度的情况下，等价于(10,0)，即小数点前最大10位，小数点后0位。 |

示例：

```sql
--创建具有FIXED(p,s), FIXED, decimal类型数据的表。
MogDB=# CREATE TABLE dec_type_t1
           (
            DEC_COL1 FIXED,
            DEC_COL2 FIXED(20,5),
            DEC_COL3 DECIMAL
           );

--查看表结构。
MogDB=# \d dec_type_t1
      Table "public.dec_type_t1"
  Column  |     Type      | Modifiers
----------+---------------+-----------
 dec_col1 | numeric(10,0) |
 dec_col2 | numeric(20,5) |
 dec_col3 | numeric(10,0) |

--删除表。
MogDB=# DROP TABLE dec_type_t1;
```

**表 3** 浮点类型

| 名称                           | 描述                                                         | 存储空间                                                     | 范围                                                         |
| :----------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| FLOAT[(p)],FLOAT4[(p)]         | 浮点数，不精准。精度p取值范围为[1,53]。                      | 4字节或8字节                                                 | 当精度p在 [1,24]之间时，选项REAL作为内部表示，当精度p在 [25,53]之间时，选项DOUBLE PRECISION作为内部表示。如不指定精度，内部用REAL表示。 |
| DOUBLE PRECISION,FLOAT8,DOUBLE | 双精度浮点数，不精准。                                       | 8字节                                                        | -1.79E+308~1.79E+308，15位十进制数字精度。                   |
| FLOAT4(p,s)                    | 精度p取值范围为[1,1000]，标度s取值范围为[0,p]。说明：p为总位数，s为小数位位数。等价于dec(p,s) | 用户声明精度。每四位（十进制位）占用两个字节，然后在整个数据上加上八个字节的额外开销。 |                                                              |

示例：

```sql
--创建具有float4(p,s), double, float4(n), float(n)类型数据的表。
MogDB=# CREATE TABLE float_type_t1
           (
            F_COL1 FLOAT4(10,4),
            F_COL2 DOUBLE,
            F_COL3 float4(10),
            F_COL4 float4(30),
            F_COL5 float(10),
            F_COL6 float(30)
           );

--查看表结构。
MogDB=# \d float_type_t1
     Table "public.float_type_t1"
 Column |       Type       | Modifiers
--------+------------------+-----------
 f_col1 | numeric(10,4)    |
 f_col2 | double precision |
 f_col3 | real             |
 f_col4 | double precision |
 f_col5 | real             |
 f_col6 | double precision |

--删除表。
MogDB=# DROP TABLE float_type_t1;
```