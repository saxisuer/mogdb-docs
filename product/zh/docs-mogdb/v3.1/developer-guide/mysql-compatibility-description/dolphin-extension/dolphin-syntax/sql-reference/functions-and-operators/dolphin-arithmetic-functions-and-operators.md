---
title: 数字操作函数和操作符
summary: 数字操作函数和操作符
author: zhang cuiping
date: 2022-10-24
---

# 数字操作函数和操作符

相比于原始的MogDB，dolphin对于时间/日期函数的修改主要为:

1. 新增`DIV/MOD/XOR/^`操作符。
2. 新增`truncate/rand/crc32/conv/float8_bool`函数。

- DIV

  描述：除（取整）

  示例：

  ```sql
  MogDB=# SELECT 8 DIV 3 AS RESULT;
   result 
  --------
        2
  (1 row)
  ```

- MOD

  描述：模（求余）

  示例：

  ```sql
  MogDB=# SELECT 4 MOD 3 AS RESULT;
   result 
  --------
        1
  (1 row)
  ```

- XOR

  描述：二进制XOR

  示例：

  ```sql
  MogDB=# SELECT 4 XOR 3 AS RESULT;
   result 
  --------
        0
  (1 row)
  ```

- truncate(v numeric, s int)

  描述：截断为s位小数。等价于trunc

  返回值类型：numeric

  示例：

  ```sql
  MogDB=# SELECT truncate(42.4382, 2);
   truncate
  ----------
      42.43
  (1 row)
  ```

- rand()

  描述：0.0到1.0之间的随机数。等价于random

  返回值类型：double precision

  示例：

  ```sql
  MogDB=# SELECT rand();
         rand
  -------------------
   0.254671605769545
  (1 row)
  ```

- crc32(string)

  描述：计算string的crc32数值

  返回值类型：int

  示例：

  ```sql
  MogDB=# SELECT crc32('abc');
     crc32
  -----------
   891568578
  (1 row)
  ```

- conv(input in, current_base int, new_base int)

  描述：将数字或字符串从一个数字基本系统转换为另一个数字基本系统。in支持数字和字符串两种类型

  返回值类型：text

  示例：

  ```sql
  MogDB=# SELECT conv(20, 10, 2);
   conv
  -------
   10100
  (1 row)
      
  MogDB=# SELECT conv('8D', 16, 10);
   conv
  ------
   141
  (1 row)
  ```

- ^

  描述：实现两个整数之间的按位异或。

  返回值类型：INT

  示例：

  ```sql
   MogDB=# SELECT 1^1;
    ?column?
    ----------
           0
    （1 row）
  ```

- float8_bool(float)

  描述：根据浮点数的取值返回布尔型（为零时返回false，否则返回true）。

  返回值类型：boolean

  示例：

  ```sql
    MogDB=# select float8_bool(0.1);
     float8_bool 
    -------------
     t
    (1 row)
    MogDB=# select float8_bool(0.0);
     float8_bool 
    -------------
     f
    (1 row)
  ```