---
title: CHECKSUM TABLE
summary: CHECKSUM TABLE
author: zhang cuiping
date: 2022-10-24
---

# CHECKSUM TABLE

## 功能描述

计算表数据校验和。

## 注意事项

- 不支持QUICK模式（返回NULL）。
- 对于非普通表（例如视图）、不存在的表均返回NULL。
- 不支持与异构数据库的表校验和的可比性。 （例如对于相同数目，在MogDB和mysql中查询结果无法对比）。

## 语法格式

```
CHECKSUM TABLE tbl_name [, tbl_name] ... [QUICK | EXTENDED]
```

## 参数说明

- **tbl_name**

  表名，可指定表名。也可以指定schema_name.table_name。

- **[QUICK | EXTENDED]** 校验模式，只支持EXTENDED（也即默认值）。

## 示例

```sql
--创建简单表
MogDB=# CREATE SCHEMA tst_schema1;
MogDB=# SET SEARCH_PATH TO tst_schema1;
MogDB=# CREATE TABLE tst_t1
(
id int,
name VARCHAR(20),
addr text,
phone text,
addr_code text
);
MogDB=# CREATE TABLE tst_t2 AS SELECT * FROM tst_t1;
INSERT 0 0

--不同插入顺序校验
MogDB=# INSERT INTO tst_t1 values(2022001, 'tst_name1', 'tst_addr1', '15600000001', '000001');
INSERT INTO tst_t1 values(2022002, 'tst_name2', 'tst_addr2', '15600000002', '000002');
INSERT INTO tst_t1 values(2022003, 'tst_name3', 'tst_addr3', '15600000003', '000003');
INSERT INTO tst_t1 values(2022004, 'tst_name4', 'tst_addr4', '15600000004', '000004');

INSERT INTO tst_t2 (SELECT * FROM tst_t1 ORDER BY id DESC);

MogDB=# checksum table tst_t1,tst_t2,xxx;
       Table        |  Checksum  
--------------------+------------
 tst_schema1.tst_t1 | 1579899754
 tst_schema1.tst_t2 | 1579899754
 tst_schema1.xxx    | NULL
 
 --含大段字段的表测试
 MogDB=# CREATE TABLE blog
(
id int,
title text,
content text
);
MogDB=# CREATE TABLE blog_v2 AS SELECT * FROM blog;
MogDB=# INSERT INTO blog values(1, 'title1', '01234567890'), (2, 'title2', '0987654321');
MogDB=# CREATE OR REPLACE FUNCTION loop_insert_result_toast(n integer)
RETURNS integer AS $$
DECLARE
    count integer := 0;
BEGIN
    LOOP
        EXIT WHEN count = n;
        UPDATE blog SET content=content||content where id = 2;
        count := count + 1;
    END LOOP;
    RETURN count;
END; $$
LANGUAGE PLPGSQL;
MogDB=# select loop_insert_result_toast(16);
 loop_insert_result_toast 
--------------------------
                       16
MogDB=# INSERT INTO blog_v2 (SELECT * FROM blog);
MogDB=# checksum table blog,blog_v2;
        Table        |  Checksum  
---------------------+------------
 tst_schema1.blog    | 6249493220
 tst_schema1.blog_v2 | 6249493220

--段页式表测试
MogDB=# CREATE TABLE tst_seg_t1(id int, name VARCHAR(20)) WITH (segment=on);
MogDB=# CREATE TABLE tst_seg_t2(id int, name VARCHAR(20)) WITH (segment=on);
MogDB=# INSERT INTO tst_seg_t1 values(2022001, 'name_example_1');
INSERT INTO tst_seg_t1 values(2022002, 'name_example_2');
INSERT INTO tst_seg_t1 values(2022003, 'name_example_3');
MogDB=# INSERT INTO tst_seg_t2 (SELECT * FROM tst_seg_t1);
MogDB=# checksum table tst_seg_t1,tst_seg_t2;
         Table          |  Checksum  
------------------------+------------
 tst_schema1.tst_seg_t1 | 5620410817
 tst_schema1.tst_seg_t2 | 5620410817
```