---
title: DESCRIBE
summary: DESCRIBE
author: zhang cuiping
date: 2022-10-24
---

# DESCRIBE

## 功能描述

查看指定表结构。

## 注意事项

- 临时表需要指定临时表对应的schema查询
- 复合主键索引所有参与列都会在Key字段中显示为PRI
- 复合唯一索引所有参与列都会在Key字段中显示为UNI
- 如果一个列参与了多个索引的创建，以该列第一个创建的索引为准显示Key字段
- 生成列会在Default中显示生成式
- 不支持表同义词

## 语法格式

```
{DESCRIBE | DESC} tbl_name
```

## 参数说明

- **{DESCRIBE | DESC}**

  使用DESCRIBE和DESC效果是等价的。

- **tbl_name**

  表名，可指定表名。也可以指定schema_name.table_name。

## 示例

```sql
--创建test表
MogDB=# CREATE TABLE test2
MogDB-# (
MogDB(# id int PRIMARY KEY
MogDB(# );
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test2_pkey" for table "test2"
CREATE TABLE
MogDB=# create table test
MogDB-# (
MogDB(# a SERIAL,
MogDB(# b varchar(10),
MogDB(# c varchar(10),
MogDB(# d varchar(10),
MogDB(# e varchar(10),
MogDB(# f varchar(10),
MogDB(# g varchar(10) DEFAULT 'g',
MogDB(# h varchar(10) NOT NULL,
MogDB(# i int DEFAULT 0,
MogDB(# j int DEFAULT 0,
MogDB(# k int GENERATED ALWAYS AS (i + j) STORED,
MogDB(# l int DEFAULT 0,
MogDB(# m int CHECK (m < 50),
MogDB(# PRIMARY KEY (a, b),
MogDB(# FOREIGN KEY(l) REFERENCES test2(id)
MogDB(# );
NOTICE:  CREATE TABLE will create implicit sequence "test_a_seq" for serial column "test.a"
NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "test_pkey" for table "test"
CREATE TABLE
MogDB=# CREATE UNIQUE INDEX idx_c on test (c);
CREATE INDEX
MogDB=# CREATE UNIQUE INDEX idx_d_e on test (d, e);
CREATE INDEX
MogDB=# CREATE INDEX idx_f on test (f);
CREATE INDEX

--查看test表结构
MogDB=# desc test;
 Field |         Type          | Null | Key |             Default             | Extra
-------+-----------------------+------+-----+---------------------------------+-------
 a     | integer               | NO   | PRI | nextval('test_a_seq'::regclass) |
 b     | character varying(10) | NO   | PRI | NULL                            |
 c     | character varying(10) | YES  | UNI | NULL                            |
 d     | character varying(10) | YES  | UNI | NULL                            |
 e     | character varying(10) | YES  | UNI | NULL                            |
 f     | character varying(10) | YES  | MUL | NULL                            |
 g     | character varying(10) | YES  |     | 'g'::character varying          |
 h     | character varying(10) | NO   |     | NULL                            |
 i     | integer               | YES  |     | 0                               |
 j     | integer               | YES  |     | 0                               |
 k     | integer               | YES  |     | (i + j)                         |
 l     | integer               | YES  | MUL | 0                               |
 m     | integer               | YES  |     | NULL                            |
(13 rows)

MogDB=# desc public.test;
 Field |         Type          | Null | Key |             Default             | Extra
-------+-----------------------+------+-----+---------------------------------+-------
 a     | integer               | NO   | PRI | nextval('test_a_seq'::regclass) |
 b     | character varying(10) | NO   | PRI | NULL                            |
 c     | character varying(10) | YES  | UNI | NULL                            |
 d     | character varying(10) | YES  | UNI | NULL                            |
 e     | character varying(10) | YES  | UNI | NULL                            |
 f     | character varying(10) | YES  | MUL | NULL                            |
 g     | character varying(10) | YES  |     | 'g'::character varying          |
 h     | character varying(10) | NO   |     | NULL                            |
 i     | integer               | YES  |     | 0                               |
 j     | integer               | YES  |     | 0                               |
 k     | integer               | YES  |     | (i + j)                         |
 l     | integer               | YES  | MUL | 0                               |
 m     | integer               | YES  |     | NULL                            |
(13 rows)

MogDB=# describe public.test;
 Field |         Type          | Null | Key |             Default             | Extra
-------+-----------------------+------+-----+---------------------------------+-------
 a     | integer               | NO   | PRI | nextval('test_a_seq'::regclass) |
 b     | character varying(10) | NO   | PRI | NULL                            |
 c     | character varying(10) | YES  | UNI | NULL                            |
 d     | character varying(10) | YES  | UNI | NULL                            |
 e     | character varying(10) | YES  | UNI | NULL                            |
 f     | character varying(10) | YES  | MUL | NULL                            |
 g     | character varying(10) | YES  |     | 'g'::character varying          |
 h     | character varying(10) | NO   |     | NULL                            |
 i     | integer               | YES  |     | 0                               |
 j     | integer               | YES  |     | 0                               |
 k     | integer               | YES  |     | (i + j)                         |
 l     | integer               | YES  | MUL | 0                               |
 m     | integer               | YES  |     | NULL                            |
(13 rows)
```

## 相关链接

N/A