---
title: SHOW DATABASES
summary: SHOW DATABASES
author: zhang cuiping
date: 2022-10-24
---

# SHOW DATABASES

## 功能描述

列出所有或按条件查询相关schema。

## 注意事项

- b数据库的show databases是查查询据库操作，MogDB的show databases是查询schema操作。
- schema会按名称顺序展示。

## 语法格式

```
SHOW {DATABASES | SCHEMAS} [LIKE 'pattern' | WHERE expr]
```

## 参数说明

- **{DATABASES | SCHEMAS}**

  两者是等价的。

- **[LIKE 'pattern' | WHERE expr]**

  pattern支持like语法，可以是schema_name的全称或者一部分，用于模糊查询；expr支持任意表达式，通常的用法是：show database where database = 'name'

## 示例

```sql
--查看当前数据库下所有schema
MogDB=# create schema a1;
CREATE SCHEMA

MogDB=# show databases;
      Database
--------------------
 a1
 blockchain
 cstore
 db4ai
 dbe_perf
 dbe_pldebugger
 dbe_pldeveloper
 information_schema
 pg_catalog
 pg_toast
 pkg_service
 public
 snapshot
 sqladvisor
(14 rows)

--按条件查询schema
MogDB=# create schema abb1;
CREATE SCHEMA
MogDB=# create schema abb2;
CREATE SCHEMA
MogDB=# create schema abb3;
CREATE SCHEMA
MogDB=# show databases like '%bb%';
 Database
----------
 abb1
 abb2
 abb3
(3 rows)

MogDB=# show databases like 'a%';
 Database
----------
 a1
 abb1
 abb2
 abb3
(4 rows)

MogDB=# show schemas where database = 'a1';
 Database
----------
 a1
(1 row)
```