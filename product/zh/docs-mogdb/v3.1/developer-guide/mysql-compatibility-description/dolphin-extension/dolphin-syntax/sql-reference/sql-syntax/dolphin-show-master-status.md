---
title: SHOW MASTER STATUS
summary: SHOW MASTER STATUS
author: zhang cuiping
date: 2022-10-24
---

# SHOW MASTER STATUS

## 功能描述

查看当前wal（xlog）日志的相关进度。

## 注意事项

- 该语句在非主库也可以执行。
- 在主库执行时，Xlog_Lsn和pg_current_xlog_location的结果一致；在非主库执行时，Xlog_Lsn和pg_last_xlog_replay_location的结果一致。
- 主库用该语句查询当前xlog写入的实时进度。
- 备库用该语句查询当前xlog回放的实时进度。

## 语法格式

```
SHOW MASTER STATUS
```

## 参数说明

- **Xlog_File_Name**

  当前处理的xlog文件名。

- **Xlog_File_Offset**

  当前处理的xlog的文件偏移位置。

- **Xlog_Lsn**

  当前xlog的LSN。

## 示例

```sql
MogDB=# show master status;
      Xlog_File_Name      | Xlog_File_Offset |  Xlog_Lsn
--------------------------+------------------+------------
 000000010000000000000010 |          7142672 | 0/106CFD10
(1 row)
```