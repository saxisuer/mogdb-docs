---
title: ALTER TABLE
summary: ALTER TABLE
author: zhang cuiping
date: 2022-10-24
---

# ALTER TABLE

## 功能描述

修改表，包括修改表的定义、重命名表、重命名表中指定的列、重命名表的约束、设置表的所属模式、添加/更新多个列、打开/关闭行访问控制开关。

## 注意事项

- 本章节只包含dolphin新增的语法，原MogDB的语法未做删除和修改。
- 当一条语句下有多条子命令时，drop index和rename index会优先其他子命令执行，这两种命令的优先级一致。

## 语法格式

- 修改表的定义。

  ```
  ALTER TABLE [ IF EXISTS ] { table_name [*] | ONLY table_name | ONLY ( table_name ) }
      action [, ... ];
  ```

  其中具体表操作action可以是以下子句之一：

  ```
  column_clause
      | {DISABLE | ENABLE} KEYS
      | DROP INDEX index_name [ RESTRICT | CASCADE ]
      | DROP PRIMARY KEY [ RESTRICT | CASCADE ]
      | DROP FOREIGN KEY foreign_key_name [ RESTRICT | CASCADE ]
      | RENAME INDEX index_name to new_index_name
      | ADD table_indexclause
      | MODIFY column_name column_type ON UPDATE CURRENT_TIMESTAMP
  ```

- 对一个表进行重建。

  ```
  ALTER TABLE table_name FORCE;
  ```

- 重命名表。对名称的修改不会影响所存储的数据。

  ```
  ALTER TABLE [ IF EXISTS ] table_name 
      RENAME { TO | AS } new_table_name;
  ```

- 对表timestamp列添加ON UPDATE属性。

  ```sql
  ALTER TABLE table_name
      MODIFY column_name column_type ON UPDATE CURRENT_TIMESTAMP;
  ```

- 对表timestamp列删除ON UPDATE属性。

  ```sql
  ALTER TABLE table_name
      MODIFY column_name column_type;
  ```

- **ADD table_indexclause**

  在表上新增一个索引

  ```
  {INDEX | KEY} [index_name] [index_type] (key_part,...)[index_option]...
  ```

  其中参数index_type为：

  ```
  USING {BTREE | HASH | GIN | GIST | PSORT | UBTREE}
  ```

  其中参数key_part为：

  ```
  {col_name[(length)] | (expr)} [ASC | DESC]
  ```

  其中参数index_option为：

  ```
  index_option:{
        COMMENT 'string'
      | index_type
  }
  ```

  COMMENT、index_type 的顺序和数量任意，但相同字段仅最后一个值生效。

## 参数说明

- **{DISABLE | ENABLE} KEYS**

  禁用和启用一个表的所有非唯一索引。

- **DROP INDEX index_name [ RESTRICT | CASCADE ]**

  删除一个表的索引。

- **DROP PRIMARY KEY [ RESTRICT | CASCADE ]**

  删除一个表的外键。

- **DROP FOREIGN KEY foreign_key_name [ RESTRICT | CASCADE ]**

  删除一个表的外键。

- **RENAME INDEX index_name to new_index_name**

  重命名一个表的索引。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：**
>
> 涉及的参数说明可见[ALTER TABLE](../../../../../../reference-guide/sql-syntax/ALTER-TABLE.md)。

## 示例

— 创建表、外键和非唯一索引。

```sql
MogDB=# CREATE TABLE alter_table_tbl1 (a INT PRIMARY KEY, b INT);
MogDB=# CREATE TABLE alter_table_tbl2 (c INT PRIMARY KEY, d INT);
MogDB=# ALTER TABLE alter_table_tbl2 ADD CONSTRAINT alter_table_tbl_fk FOREIGN KEY (d) REFERENCES alter_table_tbl1 (a);
MogDB=# CREATE INDEX alter_table_tbl_b_ind ON alter_table_tbl1(b);
```

— 禁用和启用非唯一索引。

```sql
MogDB=# ALTER TABLE alter_table_tbl1 DISABLE KEYS;
MogDB=# ALTER TABLE alter_table_tbl1 ENABLE KEYS;
```

— 删除索引。

```sql
MogDB=# ALTER TABLE alter_table_tbl1 DROP KEY alter_table_tbl_b_ind;
```

— 删除主键。

```sql
MogDB=# ALTER TABLE alter_table_tbl2 DROP PRIMARY KEY;
```

— 删除外键。

```sql
MogDB=# ALTER TABLE alter_table_tbl2 DROP FOREIGN KEY alter_table_tbl_fk;
```

— 重建表。

```sql
MogDB=# ALTER TABLE alter_table_tbl1 FORCE;
```

— 重命名索引。

```sql
MogDB=# CREATE INDEX alter_table_tbl_b_ind ON alter_table_tbl1(b);
MogDB=# ALTER TABLE alter_table_tbl1 RENAME INDEX alter_table_tbl_b_ind TO new_alter_table_tbl_b_ind;
```

— 删除表。

```sql
MogDB=# DROP TABLE alter_table_tbl1, alter_table_tbl2;
```

## 相关链接

[ALTER TABLE](../../../../../../reference-guide/sql-syntax/ALTER-TABLE.md)