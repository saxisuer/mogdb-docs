---
title: CREATE FUNCTION
summary: CREATE FUNCTION
author: zhang cuiping
date: 2022-10-24
---

# CREATE FUNCTION

## 功能描述

创建一个函数。

## 注意事项

相比于原始的MogDB，dolphin对于CREATE FUNCTION语法的修改为：

1. 增加 LANGUAGE 默认值 plpgsql。
2. 增加语法兼容项 [NOT] DETERMINISTIC。
3. 增加语法兼容项 { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA } 。
4. 增加语法兼容项 SQL SECURITY { DEFINER | INVOKER }。

## 语法格式

dolphin加载后，CREATE FUNCTION 语法的格式为

- 兼容PostgreSQL风格的创建自定义函数语法。

  ```
  CREATE [ OR REPLACE  ] FUNCTION function_name
    ( [  { argname [ argmode  ] argtype [  { DEFAULT  | :=  | =  } expression  ]
  }  [, ...]  ] )
    [ RETURNS rettype
        | RETURNS TABLE (  { column_name column_type  }  [, ...] )]
    [
        {IMMUTABLE  | STABLE  | VOLATILE}
        | {SHIPPABLE | NOT SHIPPABLE}
        | [ NOT  ] LEAKPROOF
        | WINDOW
        | {CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT}
        | {[ EXTERNAL| SQL  ] SECURITY INVOKER  | [ EXTERNAL| SQL  ] SECURITY DEFINER | AU
  THID DEFINER  | AUTHID CURRENT_USER}
        | {FENCED | NOT FENCED}
        | {PACKAGE}
        | COST execution_cost
        | ROWS result_rows
        | SET configuration_parameter { {TO | =} value | FROM CURRENT }
        | COMMENT 'text'
        | {DETERMINISTIC | NOT DETERMINISTIC}
        | LANGUAGE lang_name
        | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
    ] [...]
    {
        AS 'definition'
        | AS 'obj_file', 'link_symbol'
    }
  
  ```

- O风格的创建自定义函数的语法。

  ```
  CREATE [ OR REPLACE ] FUNCTION function_name ( [ { argname [ argmode ] argtype [ { DEFAULT | := | = } expression ] } [, …] ] ) RETURN rettype [ {IMMUTABLE | STABLE | VOLATILE } | {SHIPPABLE | NOT SHIPPABLE} | {PACKAGE} | [ NOT ] LEAKPROOF | {CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT } | {[ EXTERNAL| SQL ] SECURITY INVOKER | [ EXTERNAL| SQL ] SECURITY DEFINER | | AUTHID DEFINER | AUTHID CURRENT_USER} | COST execution_cost | ROWS result_rows | SET configuration_parameter { {TO | =} value | FROM CURRENT } | COMMENT 'text' | {DETERMINISTIC | NOT DETERMINISTIC} | LANGUAGE lang_name | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA } ][…] { IS | AS } plsql_body /
  ```

## 参数说明

- **LANGUAGE lang_name**

  用以实现函数的语言的名称。PostgreSQL风格函数默认值 sql O风格默认值 plpgsql。

- **SQL SECURITY INVOKER**

  表明该函数将带着调用它的用户的权限执行。该参数可以省略。

  SQL SECURITY INVOKER和SECURITY INVOKER和AUTHID CURRENT_USER的功能相同。

- **SQL SECURITY DEFINER**

  声明该函数将以创建它的用户的权限执行。

  SQL SECURITY DEFINER和AUTHID DEFINER和SECURITY DEFINER的功能相同。

- **CONTAINS SQL** | **NO SQL** | **READS SQL DATA** | **MODIFIES SQL DATA**

  语法兼容项。

## 示例

```sql
--指定 CONTAINS SQL
MogDB=# CREATE FUNCTION func_test (s CHAR(20)) RETURNS int  
CONTAINS SQL AS $$ select 1 $$ ;

--指定 DETERMINISTIC
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int 
CONTAINS SQL DETERMINISTIC  AS $$ select s; $$ ;

--指定 LANGUAGE SQL
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int  
CONTAINS SQL LANGUAGE SQL AS $$ select s; $$ ;

--指定 NO SQL
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int  
NO SQL AS $$ select s; $$ ;

--指定  READS SQL DATA
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int 
CONTAINS SQL  READS SQL DATA  AS $$ select s; $$ ;

--指定 MODIFIES SQL DATA
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int  
CONTAINS SQL LANGUAGE SQL NO SQL  MODIFIES SQL DATA AS $$ select s; $$ ;

--指定 SECURITY DEFINER
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int 
NO SQL SQL SECURITY DEFINER AS $$ select s; $$ ;

--指定 SECURITY INVOKER
MogDB=# CREATE FUNCTION func_test (s int) RETURNS int  
SQL SECURITY INVOKER  READS SQL DATA LANGUAGE SQL AS $$ select s; $$ ;
```

## 相关链接

[CREATE FUNCTION](../../../../../../reference-guide/sql-syntax/CREATE-FUNCTION.md)