---
title: CREATE PROCEDURE
summary: CREATE PROCEDURE
author: zhang cuiping
date: 2022-10-24
---

# CREATE PROCEDURE

## 功能描述

创建一个新的存储过程。

## 注意事项

相比于原始的MogDB，dolphin对于CREATE PROCEDURE语法的修改为：

1. 增加 LANGUAGE 选项。
2. 增加语法兼容项 [NOT] DETERMINISTIC。
3. 增加语法兼容项 { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA } 。
4. 增加语法兼容项 SQL SECURITY { DEFINER | INVOKER }。

## 语法格式

```
CREATE [ OR REPLACE ] PROCEDURE procedure_name
    [ ( {[ argname ] [ argmode ] argtype [ { DEFAULT | := | = } expression ]}[,...]) ]
    [
       { IMMUTABLE | STABLE | VOLATILE }
       | { SHIPPABLE | NOT SHIPPABLE }
       | {PACKAGE}
       | [ NOT ] LEAKPROOF
       | { CALLED ON NULL INPUT | RETURNS NULL ON NULL INPUT | STRICT }
       | {[ EXTERNAL |SQL ] SECURITY INVOKER | [ EXTERNAL|SQL ] SECURITY DEFINER | AUTHID DEFINER | AUTHID CURRENT_USER}
       | COST execution_cost
       | SET configuration_parameter { TO value | = value | FROM CURRENT }
       | COMMENT text
       | {DETERMINISTIC | NOT DETERMINISTIC}
       | LANGUAGE lang_name
       | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
    ][ ... ]
 { IS | AS }
plsql_body
/
```

## 参数说明

- **LANGUAGE lang_name**

  用以实现存储过程的语言的名称。默认值 plpgsql。

- **SQL SECURITY INVOKER**

  表明该存储过程将带着调用它的用户的权限执行。该参数可以省略。

  SQL SECURITY INVOKER和SECURITY INVOKER和AUTHID CURRENT_USER的功能相同。

- **SQL SECURITY DEFINER**

  声明该存储过程将以创建它的用户的权限执行。

  SQL SECURITY DEFINER和AUTHID DEFINER和SECURITY DEFINER的功能相同。

- **CONTAINS SQL** | **NO SQL** | **READS SQL DATA** | **MODIFIES SQL DATA**

  语法兼容项。

## 相关链接

[CREATE PROCEDURE](../../../../../../reference-guide/sql-syntax/CREATE-PROCEDURE.md)