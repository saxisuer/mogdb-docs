---
title: PG_TYPE_NONSTRICT_BASIC_VALUE
summary: PG_TYPE_NONSTRICT_BASIC_VALUE
author: zhang cuiping
date: 2022-10-24
---

# PG_TYPE_NONSTRICT_BASIC_VALUE

PG_TYPE_NONSTRICT_BASIC_VALUE视图存储类型的基础值，用于insert values()的插入。默认只有系统管理员权限才可以访问此系统表，普通用户需要授权才可以访问。

**表 1** PG_TYPE_NONSTRICT_BASIC_VALUE字段

| 名称        | 类型 | 描述       |
| :---------- | :--- | :--------- |
| typename    | text | 类型名称   |
| basic_value | text | 类型基础值 |