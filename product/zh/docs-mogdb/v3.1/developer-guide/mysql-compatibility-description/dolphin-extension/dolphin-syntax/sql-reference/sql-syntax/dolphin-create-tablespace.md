---
title: CREATE TABLESPACE
summary: CREATE TABLESPACE
author: zhang cuiping
date: 2022-10-24
---

# CREATE TABLESPACE

## 功能描述

在数据库中创建一个新的表空间。

## 注意事项

- 本章节只包含dolphin新增的语法，原MogDB的语法未做删除和修改。
- 由于路径的特殊字符校验，在使用ADD DATAFILE创建表空间时，若输入路径以.ibd结尾，dolphin插件会将路径名称更改为以_ibd结尾。

## 语法格式

```
CREATE TABLESPACE tablespace_name
    tablespace_details;
```

其中创建表空间的详细信息tablespace_details为：

```
[ OWNER user_name ] [RELATIVE] LOCATION 'directory' [ MAXSIZE 'space_size' ] [with_option_clause] [ ENGINE [=] engine_name ]
| ADD DATAFILE 'directory' [ ENGINE [=] engine_name ]
```

## 参数说明

- **ENGINE [=] engine_name**

  指定存储引擎；该特性目前只有语法没有功能。

## 示例

```sql
--使用ADD DATAFILE语法创建表空间。
MogDB=# CREATE TABLESPACE t_tbspace ADD DATAFILE 'my_tablespace' ENGINE = test_engine;
CREATE TABLESPACE

--使用ADD DATAFILE语法创建表空间，输入路径以.ibd结尾
MogDB=# CREATE TABLESPACE test_tbspace_ibd ADD DATAFILE 'test_tbspace1.ibd';
WARNING:  Suffix ".ibd" of datafile path detected. The actual path will be renamed as "test_tbspace1_ibd"
CREATE TABLESPACE
MogDB=# CREATE TABLE t_tbspace(num int) TABLESPACE test_tbspace_ibd;
CREATE TABLE
MogDB=# \d t_tbspace
   Table "public.t_tbspace"
 Column |  Type   | Modifiers
--------+---------+-----------
 num    | integer |
Tablespace: "test_tbspace_ibd"
```