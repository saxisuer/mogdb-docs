---
title: REVOKE
summary: REVOKE
author: zhang cuiping
date: 2022-10-24
---

# REVOKE

## 功能描述

REVOKE用于撤销一个或多个角色的权限。

## 注意事项

本章节只包含dolphin新增的语法，原MogDB的语法未做删除和修改。 增加ALTER ROUTINE、CRAETE ROUTINE、CREATE TEMPORARY TABLES、CREATE USER、CREATE TABLESPACE、INDEX权限

## 语法格式

- 新增`ALTER ROUTINE`权限

  与function和procedure的alter权限基本一致

  修改后的语法说明为：

  ```
  复制代码REVOKE { { EXECUTE | ALTER ROUTINE | ALTER | DROP | COMMENT } [, ...] | ALL [ PRIVILEGES ] }
  ON {FUNCTION {function_name ( [ {[ argmode ] [ arg_name ] arg_type} [, ...] ] )} | PROCEDURE {proc_name ( [ {[ argmode ] [ arg_name ] arg_type} [, ...] ] )} [, ...] | ALL FUNCTIONS IN SCHEMA schema_name [, ...] | ALL PROCEDURE IN SCHEMA schema_name [, ...] | schema_name.*}
  FROM { [ GROUP ] role_name | PUBLIC } [, ...]
  [ WITH GRANT OPTION ];
  ```

- 新增`CREATE ROUTINE`权限

  与CREATE ANY FUNCTION权限基本一致

  修改后的语法说明为：

  ```
  REVOKE { CREATE ANY TABLE | ALTER ANY TABLE | DROP ANY TABLE | SELECT ANY TABLE | INSERT ANY TABLE | UPDATE ANY TABLE |
    DELETE ANY TABLE | CREATE ANY SEQUENCE | CREATE ANY INDEX | CREATE ANY FUNCTION | CREATE ROUTINE | EXECUTE ANY FUNCTION |
    CREATE ANY PACKAGE | EXECUTE ANY PACKAGE | CREATE ANY TYPE } [, ...]
    [ON *.*]
    FROM [ GROUP ] role_name [, ...]
    [ WITH ADMIN OPTION ];
  ```

- 新增`CREATE TEMPORARY TABLES`权限

  与TEMPORARY权限基本一致

  修改后的语法说明为：

  ```
  REVOKE { { CREATE | CONNECT | CREATE TEMPORARY TABLES | TEMPORARY | TEMP | ALTER | DROP | COMMENT } [, ...]
      | ALL [ PRIVILEGES ] }
      ON { DATABASE database_name [, ...] | database_name.* }
      FROM { [ GROUP ] role_name | PUBLIC } [, ...]
      [ WITH GRANT OPTION ];
  ```

- 新增`CREATE USER`权限

  控制用户创建新用户的权限，与用户的CREATEROLE和 NOCREATEROLE权限基本一致

  新增的语法说明为：

  ```
  REVOKE CREATE USER ON *.* FROM ROLE_NAME;
  ```

- 新增`CREATE TABLESPACE`权限

  控制用户创建新表空间的权限

  新增的语法说明为：

  ```
  REVOKE CREATE TABLESPACE ON *.* FROM ROLE_NAME;
  ```

- 新增`INDEX`权限

  与CREATE ANY INDEX权限基本一致

  修改后的语法说明为：

  ```
  REVOKE { CREATE ANY TABLE | ALTER ANY TABLE | DROP ANY TABLE | SELECT ANY TABLE | INSERT ANY TABLE | UPDATE ANY TABLE |
    DELETE ANY TABLE | CREATE ANY SEQUENCE | CREATE ANY INDEX | INDEX | CREATE ANY FUNCTION | EXECUTE ANY FUNCTION |
    CREATE ANY PACKAGE | EXECUTE ANY PACKAGE | CREATE ANY TYPE } [, ...]
    { ON *.* }
    FROM [ GROUP ] role_name [, ...]
    [ WITH ADMIN OPTION ];
  ```

## 参数说明

N/A

## 示例

```sql
REVOKE ALTER ROUTINE ON FUNCTION TEST FROM USER_TESTER;
REVOKE CREATE ANY FUNCTION FROM USER_TESTER;
REVOKE CREATE TEMPORARY TABLES ON DATABASE DATABASE_TEST FROM USER_TESTER; 
REVOKE CREATE USER ON *.* FROM USER_TESTER;
REVOKE CREATE TABLESPACE ON *.* FROM USER_TESTER;
REVOKE INDEX FROM TEST_USER;
```

## 相关链接

[REVOKE](../../../../../../reference-guide/sql-syntax/REVOKE.md)