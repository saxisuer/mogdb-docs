---
title: PostGIS概述
summary: PostGIS概述
author: Guo Huan
date: 2022-04-29
---

# PostGIS概述

MogDB提供PostGIS Extension（版本为PostGIS-2.4.2）。PostGIS Extension是PostgreSQL的空间数据库扩展，提供空间对象、空间索引、空间操作函数和空间操作符等空间信息服务功能，可用于：

- 大气科学、海洋科学、地质学、环境科学、交通管理等，处理和分析复杂的空间数据，并进行制图；
- 移动通信、移动定位、移动互联网；
- 城市管理、灾害响应、资源开发等方面；

PostGIS Extension完全遵循OpenGIS规范。

PostGIS Extension依赖第三方开源软件如下：

- Geos 3.6.2
- Proj 4.9.2
- Json 0.12.1
- Libxml2 2.7.1
- Gdal 1.11.0