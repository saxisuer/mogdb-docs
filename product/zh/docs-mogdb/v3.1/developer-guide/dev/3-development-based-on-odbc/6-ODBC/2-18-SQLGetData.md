---
title: SQLGetData
summary: SQLGetData
author: Guo Huan
date: 2021-05-17
---

# SQLGetData

## 功能描述

SQLGetData返回结果集中某一列的数据。可以多次调用它来部分地检索不定长度的数据。

## 原型

```
SQLRETURN SQLGetData(SQLHSTMT        StatementHandle,
                     SQLUSMALLINT    Col_or_Param_Num,
                     SQLSMALLINT     TargetType,
                     SQLPOINTER      TargetValuePtr,
                     SQLLEN          BufferLength,
                     SQLLEN          *StrLen_or_IndPtr);
```

## 参数

**表 1** SQLGetData参数

| **关键字**       | **参数说明**                                                 |
| :--------------- | :----------------------------------------------------------- |
| StatementHandle  | 语句句柄，通过SQLAllocHandle获得。                           |
| Col_or_Param_Num | 要返回数据的列号。结果集的列按增序从1开始编号。书签列的列号为0。 |
| TargetType       | TargetValuePtr缓冲中的C数据类型的类型标识符。若TargetType为SQL_ARD_TYPE，驱动使用ARD中SQL_DESC_CONCISE_TYPE字段的类型标识符。若为SQL_C_DEFAULT，驱动根据源的SQL数据类型选择缺省的数据类型。 |
| TargetValuePtr   | **输出参数**：指向返回数据所在缓冲区的指针。                 |
| BufferLength     | TargetValuePtr所指向缓冲区的长度。                           |
| StrLen_or_IndPtr | **输出参数**：指向缓冲区的指针，在此缓冲区中返回长度或标识符的值。 |

## 返回值

- SQL_SUCCESS：表示调用正确。
- SQL_SUCCESS_WITH_INFO：表示会有一些警告信息。
- SQL_ERROR：表示比较严重的错误，如：内存分配失败、建立连接失败等。
- SQL_NO_DATA：表示SQL语句不返回结果集。
- SQL_INVALID_HANDLE：表示调用无效句柄。其他API的返回值同理。
- SQL_STILL_EXECUTING：表示语句正在执行。

## 注意事项

当调用SQLGetData函数返回SQL_ERROR或SQL_SUCCESS_WITH_INFO时，通过调用SQLGetDiagRec函数，并将HandleType和Handle参数分别设置为SQL_HANDLE_STMT和StatementHandle，可得到一个相关的SQLSTATE值，通过SQLSTATE值可以查出调用此函数的具体信息。

## 示例

参见：[示例](2-23-Examples.md)
