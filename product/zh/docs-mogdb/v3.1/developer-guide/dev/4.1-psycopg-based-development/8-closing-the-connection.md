---
title: 关闭连接
summary: 关闭连接
author: Zhang Cuiping
date: 2021-10-11
---

# 关闭连接

在使用数据库连接完成相应的数据操作后，需要关闭数据库连接。关闭数据库连接可以直接调用其close方法，如connection.close()。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 此方法关闭数据库连接，并不自动调用commit()。如果只是关闭数据库连接而不调用commit()方法，那么所有更改将会丢失。