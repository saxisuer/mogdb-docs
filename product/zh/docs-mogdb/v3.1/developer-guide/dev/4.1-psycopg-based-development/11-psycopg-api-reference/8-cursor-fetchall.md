---
title: cursor.fetchall()
summary: cursor.fetchall()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.fetchall()

## 功能描述

此方法获取查询结果的所有（剩余）行，并将它们作为元组列表返回。

## 原型

```
cursor.fetchall()
```

## 参数

无

## 返回值

元组列表，为结果集的所有结果。空行时则返回空列表。

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。