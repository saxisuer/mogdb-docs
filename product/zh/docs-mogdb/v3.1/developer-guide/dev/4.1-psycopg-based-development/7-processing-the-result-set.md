---
title: 处理结果集
summary: 处理结果集
author: Zhang Cuiping
date: 2021-10-11
---

# 处理结果集

1. cursor.fetchone()：这种方法提取查询结果集的下一行，返回一个序列，没有数据可用时则返回空。
2. cursor.fetchall()：这个例程获取所有查询结果（剩余）行，返回一个列表。空行时则返回空列表。