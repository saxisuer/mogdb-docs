---
title: Psycopg包
summary: Psycopg包
author: Zhang Cuiping
date: 2021-10-11
---

# Psycopg包

访问[openGauss下载页面](https://opengauss.org/zh/download/)获取。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/psycopg-package.png)

解压后有两个文件夹：

- psycopg2：psycopg2库文件。
- lib：lib库文件。