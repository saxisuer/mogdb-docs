---
title: java.sql.PreparedStatement
summary: java.sql.PreparedStatement
author: Guo Huan
date: 2021-05-17
---

# java.sql.PreparedStatement

java.sql.PreparedStatement是预处理语句接口。

**表 1** 对java.sql.PreparedStatement的支持情况

| 方法名                                                       | 返回值类型        | 支持JDBC 4 |
| :----------------------------------------------------------- | :---------------- | :--------- |
| clearParameters()                                            | void              | Yes        |
| execute()                                                    | Boolean           | Yes        |
| executeQuery()                                               | ResultSet         | Yes        |
| excuteUpdate()                                               | int               | Yes        |
| executeLargeUpdate()                                         | long              | No         |
| getMetaData()                                                | ResultSetMetaData | Yes        |
| getParameterMetaData()                                       | ParameterMetaData | Yes        |
| setArray(int parameterIndex, Array x)                        | void              | Yes        |
| setAsciiStream(int parameterIndex, InputStream x, int length) | void              | Yes        |
| setBinaryStream(int parameterIndex, InputStream x)           | void              | Yes        |
| setBinaryStream(int parameterIndex, InputStream x, int length) | void              | Yes        |
| setBinaryStream(int parameterIndex, InputStream x, long length) | void              | Yes        |
| setBlob(int parameterIndex, InputStream inputStream)         | void              | Yes        |
| setBlob(int parameterIndex, InputStream inputStream, long length) | void              | Yes        |
| setBlob(int parameterIndex, Blob x)                          | void              | Yes        |
| setCharacterStream(int parameterIndex, Reader reader)        | void              | Yes        |
| setCharacterStream(int parameterIndex, Reader reader, int length) | void              | Yes        |
| setClob(int parameterIndex, Reader reader)                   | void              | Yes        |
| setClob(int parameterIndex, Reader reader, long length)      | void              | Yes        |
| setClob(int parameterIndex, Clob x)                          | void              | Yes        |
| setDate(int parameterIndex, Date x, Calendar cal)            | void              | Yes        |
| setNull(int parameterIndex, int sqlType)                     | void              | Yes        |
| setNull(int parameterIndex, int sqlType, String typeName)    | void              | Yes        |
| setObject(int parameterIndex, Object x)                      | void              | Yes        |
| setObject(int parameterIndex, Object x, int targetSqlType)   | void              | Yes        |
| setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) | void              | Yes        |
| setSQLXML(int parameterIndex, SQLXML xmlObject)              | void              | Yes        |
| setTime(int parameterIndex, Time x)                          | void              | Yes        |
| setTime(int parameterIndex, Time x, Calendar cal)            | void              | Yes        |
| setTimestamp(int parameterIndex, Timestamp x)                | void              | Yes        |
| setTimestamp(int parameterIndex, Timestamp x, Calendar cal)  | void              | Yes        |
| setUnicodeStream(int parameterIndex, InputStream x, int length) | void              | Yes        |
| setURL(int parameterIndex, URL x)                            | void              | Yes        |
| setBoolean(int parameterIndex, boolean x)                    | void              | Yes        |
| setBigDecimal(int parameterIndex, BigDecimal x)              | void              | Yes        |
| setByte(int parameterIndex, byte x)                          | void              | Yes        |
| setBytes(int parameterIndex, byte[] x)                       | void              | Yes        |
| setDate(int parameterIndex, Date x)                          | void              | Yes        |
| setDouble(int parameterIndex, double x)                      | void              | Yes        |
| setFloat(int parameterIndex, float x)                        | void              | Yes        |
| setInt(int parameterIndex, int x)                            | void              | Yes        |
| setLong(int parameterIndex, long x)                          | void              | Yes        |
| setShort(int parameterIndex, short x)                        | void              | Yes        |
| setString(int parameterIndex, String x)                      | void              | Yes        |
| setNString(int parameterIndex, String x)                     | void              | Yes        |
| addBatch()                                                   | void              | Yes        |
| executeBatch()                                               | int[]             | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - addBatch()、execute()必须在clearBatch()之后才能执行。
> - 调用executeBatch()方法并不会清除batch。用户必须显式使用clearBatch()清除 。
> - 在添加了一个batch的绑定变量后，用户若想重用这些值(再次添加一个batch)，无需再次使用set*()方法 。
> - 以下方法是从java.sql.Statement继承而来：close，execute，executeQuery，executeUpdate，getConnection，getResultSet，getUpdateCount，isClosed，setMaxRows, setFetchSize。
> - executeLargeUpdate()方法必须在JDBC4.2及以上使用。
