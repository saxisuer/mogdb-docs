---
title: javax.naming.spi.InitialContextFactory
summary: javax.naming.spi.InitialContextFactory
author: Guo Huan
date: 2021-05-17
---

# javax.naming.spi.InitialContextFactory

javax.naming.spi.InitialContextFactory是初始连接上下文工厂接口。

**表 1** 对javax.naming.spi.InitialContextFactory的支持情况

| 方法名                                              | 返回值类型 | 支持JDBC 4 |
| :-------------------------------------------------- | :--------- | :--------- |
| getInitialContext(Hashtable&lt;?,?&gt; environment) | Context    | Yes        |
