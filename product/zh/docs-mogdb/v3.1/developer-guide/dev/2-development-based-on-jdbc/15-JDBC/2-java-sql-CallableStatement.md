---
title: java.sql.CallableStatement
summary: java.sql.CallableStatement
author: Guo Huan
date: 2021-05-17
---

# java.sql.CallableStatement

java.sql.CallableStatement是存储过程执行接口。

**表 1** 对java.sql.CallableStatement的支持情况

| 方法名                                             | 返回值类型 | 支持JDBC 4 |
| :------------------------------------------------- | :--------- | :--------- |
| getArray(int parameterIndex)                       | Array      | Yes        |
| getBigDecimal(int parameterIndex)                  | BigDecimal | Yes        |
| getBlob(int parameterIndex)                        | Blob       | Yes        |
| getBoolean(int parameterIndex)                     | boolean    | Yes        |
| getByte(int parameterIndex)                        | byte       | Yes        |
| getBytes(int parameterIndex)                       | byte[]     | Yes        |
| getClob(int parameterIndex)                        | Clob       | Yes        |
| getDate(int parameterIndex)                        | Date       | Yes        |
| getDate(int parameterIndex, Calendar cal)          | Date       | Yes        |
| getDouble(int parameterIndex)                      | double     | Yes        |
| getFloat(int parameterIndex)                       | float      | Yes        |
| getInt(int parameterIndex)                         | int        | Yes        |
| getLong(int parameterIndex)                        | long       | Yes        |
| getObject(int parameterIndex)                      | Object     | Yes        |
| getObject(int parameterIndex, Class&lt;T&gt; type) | Object     | Yes        |
| getShort(int parameterIndex)                       | short      | Yes        |
| getSQLXML(int parameterIndex)                      | SQLXML     | Yes        |
| getString(int parameterIndex)                      | String     | Yes        |
| getNString(int parameterIndex)                     | String     | Yes        |
| getTime(int parameterIndex)                        | Time       | Yes        |
| getTime(int parameterIndex, Calendar cal)          | Time       | Yes        |
| getTimestamp(int parameterIndex)                   | Timestamp  | Yes        |
| getTimestamp(int parameterIndex, Calendar cal)     | Timestamp  | Yes        |
| registerOutParameter(int parameterIndex, int type) | void       | Yes        |
| wasNull()                                          | Boolean    | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 不允许含有OUT参数的语句执行批量操作。
> - 以下方法是从java.sql.Statement继承而来：close，execute，executeQuery，executeUpdate，getConnection，getResultSet，getUpdateCount，isClosed，setMaxRows , setFetchSize。
> - 以下方法是从java.sql.PreparedStatement继承而来：addBatch，clearParameters，execute，executeQuery，executeUpdate，getMetaData，setBigDecimal，setBoolean，setByte，setBytes，setDate，setDouble，setFloat，setInt，setLong，setNull，setObject，setString，setTime，setTimestamp 。
