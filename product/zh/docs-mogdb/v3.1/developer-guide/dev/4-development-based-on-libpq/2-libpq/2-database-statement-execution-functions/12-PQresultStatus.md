---
title: PQresultStatus
summary: PQresultStatus
author: Guo Huan
date: 2021-05-17
---

# PQresultStatus

## 功能描述

返回命令的结果状态。

## 原型

```
ExecStatusType PQresultStatus(const PGresult *res);
```

## 参数

**表 1** PQresultStatus参数

| **关键字** | **参数说明**             |
| :--------- | :----------------------- |
| res        | 包含查询结果的对象指针。 |

## 返回值

PQresultStatus：命令执行结果的枚举，包括：

```
PQresultStatus可以返回下面数值之一：
PGRES_EMPTY_QUERY
发送给服务器的字串是空的。

PGRES_COMMAND_OK
成功完成一个不返回数据的命令。

PGRES_TUPLES_OK
成功执行一个返回数据的查询（比如SELECT或者SHOW）。

PGRES_COPY_OUT
（从服务器）Copy Out （拷贝出）数据传输开始。

PGRES_COPY_IN
Copy In（拷贝入）（到服务器）数据传输开始。

PGRES_BAD_RESPONSE
服务器的响应无法理解。

PGRES_NONFATAL_ERROR
发生了一个非致命错误（通知或者警告）。

PGRES_FATAL_ERROR
发生了一个致命错误。

PGRES_COPY_BOTH
拷贝入/出（到和从服务器）数据传输开始。这个特性当前只用于流复制， 所以这个状态不会在普通应用中发生。

PGRES_SINGLE_TUPLE
PGresult包含一个来自当前命令的结果元组。 这个状态只在查询选择了单行模式时发生
```

## 注意事项

- 请注意，恰好检索到零行的SELECT命令仍然显示PGRES_TUPLES_OK。PGRES_COMMAND_OK用于永远不能返回行的命令（插入或更新，不带返回子句等）。PGRES_EMPTY_QUERY响应可能表明客户端软件存在bug。
- 状态为PGRES_NONFATAL_ERROR的结果永远不会由PQexec或其他查询执行函数直接返回，此类结果将传递给通知处理程序。

## 示例

请参见示例章节。
