---
title: PQsendQuery
summary: PQsendQuery
author: Guo Huan
date: 2021-05-17
---

# PQsendQuery

## 功能描述

向服务器提交一个命令而不等待结果。如果查询成功发送则返回1，否则返回0。

## 原型

```c
int PQsendQuery(PGconn *conn, const char *command);
```

## 参数

**表 1** PQsendQuery参数

| **关键字** | **参数说明**             |
| :--------- | :----------------------- |
| conn       | 指向包含链接的对象指针。 |
| command    | 需要执行的查询字符串.    |

## 返回值

int：执行结果为1表示成功，0表示失败，失败原因存到conn-&gt;errorMessage中。

## 注意事项

在成功调用PQsendQuery后，调用PQgetResult一次或者多次获取结果。PQgetResult返回空指针表示命令已执行完成，否则不能再次调用PQsendQuery（在同一连接上）。

## 示例

请参见示例章节。
