---
title: PQconnectdbParams
summary: PQconnectdbParams
author: Guo Huan
date: 2021-05-17
---

# PQconnectdbParams

## 功能描述

与数据库服务器建立一个新的连接。

## 原型

```
PGconn *PQconnectdbParams(const char * const *keywords,
                          const char * const *values,
                          int expand_dbname);
```

## 参数

**表 1** PQconnectdbParams参数

| **关键字**    | **参数说明**                                                 |
| :------------ | :----------------------------------------------------------- |
| keywords      | 定义为一个字符串的数组，每个都成为一个关键字。               |
| values        | 给每个关键字一个值。                                         |
| expand_dbname | 当expand_dbname是非零的时，允许将dbname的关键字值看做一个连接字符串。只有第一个出现的dbname是这样展开的，任何随后的dbname值作为纯数据库名处理。 |

## 返回值

PGconn *：指向包含链接的对象指针，内存在函数内部申请。

## 注意事项

这个函数用从两个NULL结束的数组中来的参数打开一个新的数据库连接。与PQsetdbLogin不同的是，可以不必更换函数签名（名字）就可以扩展参数集，所以建议应用程序中使用这个函数（或者它的类似的非阻塞变种PQconnectStartParams和PQconnectPoll）。

## 示例

请参见[示例](../../libpq-example.md)章节。
