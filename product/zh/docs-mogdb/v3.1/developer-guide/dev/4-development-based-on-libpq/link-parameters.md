---
title: 链接参数
summary: 链接参数
author: Guo Huan
date: 2022-04-26
---

# 链接参数

**表 1** 链接参数

| 参数                      | 描述                                                         |
| :------------------------ | :----------------------------------------------------------- |
| host                      | 要链接的主机名。如果主机名以斜杠开头，则它声明使用Unix域套接字通讯而不是TCP/IP通讯；该值就是套接字文件所存储的目录。如果没有声明host，那么默认是与位于/tmp目录（或者安装数据库的时候声明的套接字目录）里面的Unix-域套接字链接。在没有Unix域套接字的机器上，默认与localhost链接。<br/>接受以‘,’分割的字符串来指定多个主机名，支持指定多个主机名。 |
| hostaddr                  | 与之链接的主机的IP地址，是标准的IPv4地址格式，比如，172.28.40.9。如果机器支持IPv6，那么也可以使用IPv6的地址。如果声明了一个非空的字符串，那么使用TCP/IP通讯机制。<br/>接受以‘,’分割的字符串来指定多个IP地址，支持指定多个IP地址。<br/>使用hostaddr取代host可以让应用避免一次主机名查找，这一点对于那些有时间约束的应用来说可能是非常重要的。不过，GSSAPI或SSPI认证方法要求主机名（host）。因此，应用下面的规则：<br/>1. 如果声明了不带hostaddr的host那么就强制进行主机名查找。<br/>2. 如果声明中没有host，hostaddr的值给出服务器网络地址；如果认证方法要求主机名，那么链接尝试将失败。<br/>3. 如果同时声明了host和hostaddr，那么hostaddr的值作为服务器网络地址。host的值将被忽略，除非认证方法需要它，在这种情况下它将被用作主机名。<br/>须知：<br/>- 要注意如果host不是网络地址hostaddr处的服务器名，那么认证很有可能失败。<br/>- 如果主机名（host）和主机地址都没有，那么libpq将使用一个本地的Unix域套接字进行链接；或者是在没有Unix域套接字的机器上，它将尝试与localhost链接。 |
| port                      | 主机服务器的端口号，或者在Unix域套接字链接时的套接字扩展文件名。<br/>接受以‘,’分割的字符串来指定多个端口号，支持指定多个端口号。 |
| user                      | 要链接的用户名，缺省是与运行该应用的用户操作系统名同名的用户。 |
| dbname                    | 数据库名，缺省和用户名相同。                                 |
| password                  | 如果服务器要求口令认证，所用的口令。                         |
| connect_timeout           | 链接的最大等待时间，以秒计（用十进制整数字符串书写），0或者不声明表示无穷。不建议把链接超时的值设置得小于2秒。 |
| client_encoding           | 为这个链接设置client_encoding配置参数。除了对应的服务器选项接受的值，你可以使用auto从客户端中的当前环境中确定正确的编码（Unix系统上是LC_CTYPE环境变量）。 |
| tty                       | 忽略（以前，该参数指定了发送服务器调试输出的位置）。         |
| options                   | 添加命令行选项以在运行时发送到服务器。                       |
| application_name          | 为application_name配置参数指定一个值，表明当前用户身份。     |
| fallback_application_name | 为[application_name](http://www.postgres.cn/docs/12/runtime-config-logging.html#GUC-APPLICATION-NAME)配置参数指定一个后补值。如果通过一个连接参数或PGAPPNAME环境变量没有为application_name给定一个值，将使用这个值。在希望设置一个默认应用名但不希望它被用户覆盖的一般工具程序中指定一个后补值很有用。 |
| keepalives                | 控制客户端侧的TCP保持激活是否使用。缺省值是1，意思为打开，但是如果不想要保持激活，你可以更改为0，意思为关闭。通过Unix域套接字做的链接忽略这个参数。 |
| keepalives_idle           | 在TCP应该发送一个保持激活的信息给服务器之后，控制不活动的秒数。0值表示使用系统缺省。通过Unix域套接字做的链接或者如果禁用了保持激活则忽略这个参数。 |
| keepalives_interval       | 在TCP保持激活信息没有被应该传播的服务器承认之后，控制秒数。0值表示使用系统缺省。通过Unix域套接字做的链接或者如果禁用了保持激活则忽略这个参数。 |
| keepalives_count          | 控制TCP发送保持激活信息的次数。0值表示使用系统缺省。通过Unix域套接字做的链接或者如果禁用了保持激活则忽略这个参数。 |
| tcp_user_timeout          | 在支持TCP_USER_TIMEOUT套接字选项的操作系统上，指定传输的数据在TCP连接被强制关闭之前可以保持未确认状态的最大时长。0值表示使用系统缺省。通过Unix域套接字做的链接忽略这个参数。 |
| rw_timeout                | 设置客户端连接读写超时时间。                                 |
| sslmode                   | 启用SSL加密的方式：<br/> - disable：不使用SSL安全连接。<br/>- allow：如果数据库服务器要求使用，则可以使用SSL安全加密连接，但不验证数据库服务器的真实性。<br/>- prefer：如果数据库支持，那么首选使用SSL安全加密连接，但不验证数据库服务器的真实性。<br/>- require：必须使用SSL安全连接，但是只做了数据加密，而并不验证数据库服务器的真实性。<br/>- verify-ca：必须使用SSL安全连接，当前windows odbc不支持cert方式认证。<br/>- verify-full：必须使用SSL安全连接，当前windows odbc不支持cert方式认证。 |
| sslcompression            | 如果设置为1（默认），SSL连接之上传送的数据将被压缩（这要求OpenSSL版本为0.9.8或更高）。如果设置为0，压缩将被禁用（这要求OpenSSL版本为1.0.0或更高）。如果建立的是一个没有SSL的连接，这个参数会被忽略。如果使用的OpenSSL版本不支持该参数，它也会被忽略。压缩会占用CUP时间，但是当瓶颈为网络时可以提高吞吐量。如果CPU性能是限制因素，禁用压缩能够改进响应时间和吞吐量。 |
| sslcert                   | 这个参数指定客户端SSL证书的文件名，它替换默认的~/.postgresql/postgresql.crt。如果没有建立SSL连接，这个参数会被忽略。 |
| sslkey                    | 这个参数指定用于客户端证书的密钥位置。它能指定一个会被用来替代默认的~/.postgresql/postgresql.key的文件名，或者它能够指定一个从外部“引擎”（引擎是OpenSSL的可载入模块）得到的密钥。一个外部引擎说明应该由一个冒号分隔的引擎名称以及一个引擎相关的关键标识符组成。如果没有建立SSL连接，这个参数会被忽略。 |
| sslrootcert               | 这个参数指定一个包含SSL证书机构（CA）证书的文件名称。如果该文件存在，服务器的证书将被验证是由这些机构之一签发。默认值是~/.postgresql/root.crt。 |
| sslcrl                    | 这个参数指定SSL证书撤销列表（CRL）的文件名。列在这个文件中的证书如果存在，在尝试认证该服务器证书时会被拒绝。默认值是~/.postgresql/root.crl。 |
| requirepeer               | 这个参数指定服务器的操作系统用户，例如requirepeer=postgres。当建立一个Unix域套接字连接时，如果设置了这个参数，客户端在连接开始时检查服务器进程是否运行在指定的用户名之下。如果发现不是，该连接会被一个错误中断。这个参数能被用来提供与TCP/IP连接上SSL证书相似的服务器认证（注意，如果Unix域套接字在/tmp或另一个公共可写的位置，任何用户能启动一个在那里侦听的服务器。使用这个参数来保证你连接的是一个由可信用户运行的服务器）。这个选项只在实现了peer认证方法的平台上受支持。 |
| krbsrvname                | 当用GSSAPI认证时，要使用的Kerberos服务名。为了让Kerberos认证成功，这必须匹配在服务器配置中指定的服务名。 |
| gsslib                    | 用于GSSAPI认证的GSS库。只用在Windows上。设置为gssapi可强制libpq用GSSAPI库来代替默认的SSPI进行认证。 |
| service                   | 用于附加参数的服务名。它指定保持附加连接参数的pg_service.conf中的一个服务名。这允许应用只指定一个服务名，这样连接参数能被集中维护。 |
| authtype                  | 不再使用“authtype”，因此将其标记为“不显示”。我们将其保留在数组中，以免拒绝旧应用程序中的conninfo字符串，这些应用程序可能仍在尝试设置它。 |
| remote_nodename           | 指定连接本地节点的远端节点名称。                             |
| localhost                 | 指定在一个连接通道中的本地地址。                             |
| localport                 | 指定在一个连接通道中的本地端口。                             |
| fencedUdfRPCMode          | 控制fenced UDF RPC协议是使用unix域套接字或特殊套接字文件名。缺省值是0，意思为关闭，使用unix domain socket模式，文件类型为“.s.PGSQL.%d”，但是要使用fenced udf ，文件类型为.s.fencedMaster_unixdomain，可以更改为1，意思为开启。 |
| replication               | 这个选项决定是否该连接应该使用复制协议而不是普通协议。这是PostgreSQL的复制连接以及pg_basebackup之类的工具在内部使用的协议，但也可以被第三方应用使用。支持下列值，大小写无关：<br/>- true、on、yes、1：连接进入到物理复制模式。<br/>- database：连接进入到逻辑复制模式，连接到dbname参数中指定的数据库。<br/>- false、off、no、0：该连接是一个常规连接，这是默认行为。在物理或者逻辑复制模式中，仅能使用简单查询协议。 |
| backend_version           | 传递到远端的后端版本号。                                     |
| prototype                 | 设置当前协议级别，默认：PROTO_TCP。                          |
| enable_ce                 | 控制是否允许客户端连接全密态数据库。默认0，如果需要开启，则修改为1。 |
| connection_info           | Connection_info是一个包含driver_name、driver_version、driver_path和os_user的json字符串。<br/>如果不为NULL，使用connection_info忽略connectionExtraInf<br/>如果为NULL，生成与libpq相关的连接信息字符串，当connectionExtraInf为false时connection_info只有driver_name和driver_version。 |
| connectionExtraInf        | 设置connection_info是否存在扩展信息，默认值为0，如果包含其他信息，则需要设置为1。 |
| target_session_attrs      | 设定连接的主机的类型。主机的类型和设定的值一致时才能连接成功。target_session_attrs的设置规则如下：<br/>- any(默认值)：可以对所有类型的主机进行连接。<br/>- read-write：当连接的主机允许可读可写时，才进行连接。<br/>- read-only：仅对可读的主机进行连接。<br/>- primary：仅对主备系统中的主机能进行连接。<br/>- standby: 仅对主备系统中的备机进行连接。<br/>- prefer-standby：首先尝试找到一个备机进行连接。如果对hosts列表的所有机器都连接失败，那么尝试“any”模式进行连接。 |
