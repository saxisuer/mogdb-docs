<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# MogDB Documentation 3.1

## AI特性指南

+ [AI特性概述](/AI-features/1-AI-features-overview.md)
+ [AI4DB：数据库自治运维](/AI-features/ai4db/ai4db-autonomous-database-o&m.md)
  + [DBMind模式说明](/AI-features/ai4db/dbmind-mode/dbmind-mode.md)
    + [service子命令](/AI-features/ai4db/dbmind-mode/1-service.md)
    + [component子命令](/AI-features/ai4db/dbmind-mode/2-component.md)
    + [set子命令](/AI-features/ai4db/dbmind-mode/3-set.md)
  + [DBMind的支持组件](/AI-features/ai4db/components-that-support-dbmind/components-that-support-dbmind.md)
    + Prometheus Exporter组件
      + [概述](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-overview.md)
      + [环境部署](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-environment-deployment.md)
      + [使用指导](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/components-that-support-dbmind/prometheus-exporter/prometheus-exporter-troubleshooting.md)
  + DBMind的AI子功能
    + X-Tuner：参数调优与诊断
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-1-x-tuner-overview.md)
      + [使用准备](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-2-preparations.md)
      + [使用示例](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-3-examples.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-4-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-5-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/1-x-tuner-parameter-optimization-and-diagnosis/1-6-Troubleshooting.md)
    + Index-advisor：索引推荐
      + [单query索引推荐](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-1-single-query-index-recommendation.md)
      + [虚拟索引](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-2-virtual-index.md)
      + [workload级别索引推荐](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/2-index-advisor-index-recommendation/2-3-workload-level-index-recommendation.md)
    + Slow Query Diagnosis：慢SQL根因分析
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-1-overview.md)
      + [环境部署](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-2-environment-deployment.md)
      + [使用指导](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-3-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-4-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-5-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/3-slow-query-diagnosis-root-cause-analysis-for-slow-sql-statements/3-6-troubleshooting.md)
    + Forecast：趋势预测
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-1-overview.md)
      + [环境部署](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-2-environment-deployment.md)
      + [使用指导](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-3-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-4-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-5-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/4-forcast-trend-prediction/4-6-troubleshooting.md)
    + SQLdiag：慢SQL发现
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-1-overview.md)
      + [使用指导](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-2-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-3-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-4-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/5-sqldiag-slow-sql-discovery/5-5-troubleshooting.md)
    + SQL Rewriter: SQL语句改写
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-1-overview.md)
      + [使用指导](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-2-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-3-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-4-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/6-sql-rewriter-sql-statement-rewriting/6-5-troubleshooting.md)
    + Anomaly detection: 异常检测
      + [概述](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-1-overview.md)
      + [使用指导](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-2-usage-guide.md)
      + [获取帮助](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-3-obtaining-help-information.md)
      + [命令参考](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-4-command-reference.md)
      + [常见问题处理](/AI-features/ai4db/ai-sub-functions-of-the-dbmind/7-anomaly-detection/7-5-troubleshooting.md)
+ [DB4AI：数据库驱动AI](/AI-features/db4ai/db4ai.md)
  + [原生DB4AI引擎](/AI-features/db4ai/native-db4ai-engine.md)
  + [全流程AI](/AI-features/db4ai/full-process-ai/full-process-ai.md)
    + [PLPython Fenced模式](/AI-features/db4ai/full-process-ai/plpython-fenced-mode.md)
    + [DB4AI-Snapshots数据版本管理](/AI-features/db4ai/full-process-ai/db4ai-snapshots-for-data-version-management.md)
+ AI in DB：数据库内AI功能
  + 智能Explain：SQL语句查询时间预测
    + [概述](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-overview.md)
    + [环境部署](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-environment-deployment.md)
    + [使用指导](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-user-guide.md)
    + [最佳实践](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-best-practices.md)
    + [常见问题处理](/AI-features/ai-in-db/intelligence-explain/intelligence-explain-faqs.md)
+ ABO 优化器
  + 智能基数估计
    + [概述](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-overview.md)
    + [前置条件](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-prerequisites.md)
    + [使用指导](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-usage-guide.md)
    + [最佳实践](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-best-practices.md)
    + [常见问题处理](/AI-features/abo-optimizer/intelligent-cardinality-estimation/intelligent-cardinality-estimation-troubleshooting.md)
  + 自适应计划选择
    + [概述](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-overview.md)
    + [前置条件](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-prerequisites.md)
    + [使用指导](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-usage-guide.md)
    + [最佳实践](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-best-practices.md)
    + [常见问题处理](/AI-features/abo-optimizer/adaptive-plan-selection/adaptive-plan-selection-troubleshooting.md)