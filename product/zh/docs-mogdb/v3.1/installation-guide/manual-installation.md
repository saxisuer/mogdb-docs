---
title: MogDB/openGauss 手动部署(非OM工具)单机，主备，主备级联架构
summary: MogDB/openGauss 手动部署(非OM工具)单机，主备，主备级联架构
author: Zhang Cuiping
date: 2021-10-14
---

# MogDB/openGauss 手动部署(非OM工具)单机，主备，主备级联架构

## 使用场景

在某些安装程序尚未支持的操作系统上，安装程序（比如OM）会报操作系统不支持的错误，此时可以通过手动方式安装MogDB。

## 获取安装包

访问[MogDB下载页面](https://www.mogdb.io/downloads/mogdb)下载对应平台的安装包。解压标准安装包后得到的`MogDB-x.x.x-openEuler-64bit.tar.gz`即为手动安装所需的内核包。

## 前期准备

1. 关闭防火墙，selinux

   ```bash
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   setenforce=0
   sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
   ```

2. 安装依赖包

   ```bash
   yum install libaio-devel -y
   ```

3. 创建相关目录，用户，组

   ```bash
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   echo "Enmo@123" | passwd --stdin omm
   mkdir -p /opt/mogdb/software
   mkdir -p /opt/mogdb/data
   chown -R omm:dbgrp /opt/mogdb
   ```

4. 上传并解压二进制文件

   ```bash
   [root@mogdb-kernel-0001 software]# pwd
   /opt/mogdb/software
   [root@mogdb-kernel-0001 software]# ls -lrt
   total 90236
   -r-------- 1 root root 92401412 Jun 13 06:14 MogDB-3.1.0-openEuler-64bit.tar.gz
   chown omm:dbgrp MogDB-3.1.0-openEuler-64bit.tar.gz
   su - omm
   cd /opt/mogdb/software/
   tar -xf MogDB-3.1.0-openEuler-64bit.tar.gz
   ```

## 初始化数据库（单机）

1. 配置环境变量

   ```bash
   echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
   echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
   echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
   source /home/omm/.bashrc
   ```

2. init数据库

   ```bash
   bin/gs_initdb --pgdata=/opt/mogdb/data --nodename=primary --pwpasswd=Enmo@123 --encoding=UTF-8 --locale=en_US.UTF-8
   ```

3. 修改初始化参数

   ```bash
   echo "port=26000" >> /opt/mogdb/data/postgresql.conf
   echo "listen_addresses = '0.0.0.0'" >> /opt/mogdb/data/postgresql.conf
   echo "password_encryption_type = 0" >> /opt/mogdb/data/postgresql.conf
   echo "log_directory = 'pg_log'" >> /opt/mogdb/data/postgresql.conf
   echo "remote_read_mode=non_authentication" >> /opt/mogdb/data/postgresql.conf
   echo "host all all 0.0.0.0/0 md5" >> /opt/mogdb/data/pg_hba.conf
   ```

4. 启动数据库

   ```bash
   gs_ctl start -D /opt/mogdb/data
   ```

**至此单机安装完成**

## 主备安装

1. 主库操作

    - 配置连接通道

    ```bash
    echo "replconninfo1='localhost=172.16.0.106 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.245 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'
    " >> /opt/mogdb/data/postgresql.conf
    ```

    **localhost为主库IP,remotehost为备库IP**

    - 将主库以primary方式启动

    ```bash
    gs_ctl restart -D /opt/mogdb/data/ -M primary
    ```

2. 备库操作

- 前期准备工作如上（一）

- 配置环境变量

  ```bash
  echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
  echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
  echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
  source /home/omm/.bashrc
  - 将主库的配置文件传到备库
  scp /opt/mogdb/data/pg_hba.conf /opt/mogdb/data/postgresql.conf 172.16.0.245:/opt/mogdb/data/
  ```

- 配置连接通道,将localhost和remotehost对调.

  ```bash
  sed -i “/^replconninfo1/creplconninfo1=‘localhost=172.16.0.245 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.106 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004’” /opt/mogdb/data/postgresql.conf
  ```

  **localhost为备库IP,remotehost为主库IP**

- 构建主备关系

  ```bash
  gs_ctl build -D /opt/mogdb/data/ -b full -M standby
  ```

- 查询主备状态

  - 主库

    ```bash
    gs_ctl query -D /opt/mogdb/data/
    [2021-06-13 07:51:41.119][159054][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
    HA state:
            local_role                     : Primary
            static_connections             : 1
            db_state                       : Normal
            detail_information             : Normal

    Senders info:
            sender_pid                     : 159041
            local_role                     : Primary
            peer_role                      : Standby
            peer_state                     : Normal
            state                          : Streaming
            sender_sent_location           : 0/14000258
            sender_write_location          : 0/14000258
            sender_flush_location          : 0/14000258
            sender_replay_location         : 0/14000258
            receiver_received_location     : 0/14000258
            receiver_write_location        : 0/14000258
            receiver_flush_location        : 0/14000258
            receiver_replay_location       : 0/14000258
            sync_percent                   : 100%
            sync_state                     : Sync
            sync_priority                  : 1
            sync_most_available            : Off
            channel                        : 172.16.0.106:26001-->172.16.0.245:60856

    Receiver info:
    No information
    ```

  - 备库

    ```bash
    gs_ctl query -D /opt/mogdb/data/
    [2021-06-13 07:51:32.743][123204][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
    HA state:
            local_role                     : Standby
            static_connections             : 1
            db_state                       : Normal
            detail_information             : Normal
    
    Senders info:
    No information
    Receiver info:
            receiver_pid                   : 123194
            local_role                     : Standby
            peer_role                      : Primary
            peer_state                     : Normal
            state                          : Normal
            sender_sent_location           : 0/14000140
            sender_write_location          : 0/14000140
            sender_flush_location          : 0/14000140
            sender_replay_location         : 0/14000140
            receiver_received_location     : 0/14000140
            receiver_write_location        : 0/14000140
            receiver_flush_location        : 0/14000140
            receiver_replay_location       : 0/14000140
            sync_percent                   : 100%
            channel                        : 172.16.0.245:60856<--172.16.0.106:26001
    ```

**至此主备已安装完成**

## 主备级联安装

1. 主备安装如上(一，二，三)

2. 添加复制通道

    - 主库操作

    ```bash
    gsql -d postgres -p26000 -c “alter system set replconninfo2 to ‘localhost=172.16.0.106 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.127 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004 iscascade=true’;”
    ```

    - 备库操作

    ```bash
    gsql -d postgres -p26000 -c “alter system set replconninfo2 to ‘localhost=172.16.0.245 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.127 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004 iscascade=true’;”
    ```

3. 级联库操作

- 前期准备工作如上（一）

- 配置环境变量

  ```bash
  echo "export GAUSSHOME=/opt/mogdb/software"  >> /home/omm/.bashrc && \
  echo "export PATH=\$GAUSSHOME/bin:\$PATH " >> /home/omm/.bashrc && \
  echo "export LD_LIBRARY_PATH=\$GAUSSHOME/lib:\$LD_LIBRARY_PATH" >> /home/omm/.bashrc
  source /home/omm/.bashrc
  ```

- 将备库的配置文件传到级联库

  ```bash
  scp /opt/mogdb/data/pg_hba.conf /opt/mogdb/data/postgresql.conf 172.16.0.127:/opt/mogdb/data/
  ```

- 配置连接通道

  ```bash
  sed -i "/^replconninfo1/creplconninfo1='localhost=172.16.0.127 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.106 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'" /opt/mogdb/data/postgresql.conf
  sed -i "/replconninfo2/creplconninfo2='localhost=172.16.0.127 localport=26001 localheartbeatport=26005 localservice=26004 remotehost=172.16.0.245 remoteport=26001 remoteheartbeatport=26005 remoteservice=26004'" /opt/mogdb/data/postgresql.conf
  ```

  **localhost为级联IP，remotehost为主库IP和备库IP。**

- 构建主备关系

  ```bash
  gs_ctl build -D /opt/mogdb/data/ -b full -M cascade_standby
  ```

4 查看主备级联状态

- 主库

  ```bash
  gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:37:03.281][207069][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Primary
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal

   Senders info:
          sender_pid                     : 206143
          local_role                     : Primary
          peer_role                      : Standby
          peer_state                     : Normal
          state                          : Streaming
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          sync_state                     : Sync
          sync_priority                  : 1
          sync_most_available            : Off
          channel                        : 172.16.0.106:26001-->172.16.0.245:34586

   Receiver info:
  No information
  ```

- 备库

  ```bash
  gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:37:09.128][147065][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Standby
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal

   Senders info:
          sender_pid                     : 147043
          local_role                     : Standby
          peer_role                      : Cascade Standby
          peer_state                     : Normal
          state                          : Streaming
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          sync_state                     : Async
          sync_priority                  : 0
          sync_most_available            : Off
          channel                        : 172.16.0.245:26001-->172.16.0.127:49110

   Receiver info:
          receiver_pid                   : 146771
          local_role                     : Standby
          peer_role                      : Primary
          peer_state                     : Normal
          state                          : Normal
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          channel                        : 172.16.0.245:34586<--172.16.0.106:26001
  ```

- 级联库

  ```bash
  gs_ctl query -D /opt/mogdb/data
  [2021-06-13 08:36:56.223][273241][][gs_ctl]: gs_ctl query ,datadir is /opt/mogdb/data
   HA state:
          local_role                     : Cascade Standby
          static_connections             : 2
          db_state                       : Normal
          detail_information             : Normal
  
   Senders info:
  No information
   Receiver info:
          receiver_pid                   : 273237
          local_role                     : Cascade Standby
          peer_role                      : Standby
          peer_state                     : Normal
          state                          : Normal
          sender_sent_location           : 0/1A000140
          sender_write_location          : 0/1A000140
          sender_flush_location          : 0/1A000140
          sender_replay_location         : 0/1A000140
          receiver_received_location     : 0/1A000140
          receiver_write_location        : 0/1A000140
          receiver_flush_location        : 0/1A000140
          receiver_replay_location       : 0/1A000140
          sync_percent                   : 100%
          channel                        : 172.16.0.127:49110<--172.16.0.245:26001
  ```

**至此主备级联安装完成**
