---
title: 标准安装
summary: 标准安装
author: Zhang Cuiping
date: 2021-04-02
---

# 标准安装

## 创建XML配置文件

安装MogDB前需要创建cluster_config.xml文件。cluster_config.xml文件包含部署MogDB的服务器信息、安装路径、IP地址以及端口号等。用于告知MogDB如何部署。用户需根据不同场配置对应的XML文件。

### 配置数据库名称及各项目录

在script/gspylib/etc/conf/cluster_config_template.xml获取XML文件模板。以下配置内容为示例，可自行替换。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
<CLUSTER>
<PARAM name="clusterName" value="Cluster_template"/>
<PARAM name="nodeNames" value="node1_hostname,node2_hostname"/>
<PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app"/>
<PARAM name="gaussdbLogPath" value="/var/log/omm"/>
<PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
<PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om"/>
<PARAM name="corePath" value="/opt/mogdb/corefile"/>
<PARAM name="backIp1s" value="192.168.0.1,192.168.0.2"/>
</CLUSTER>
<DEVICELIST>
<DEVICE sn="node1_hostname">
<PARAM name="name" value="node1_hostname"/>
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<PARAM name="backIp1" value="192.168.0.1"/>
<PARAM name="sshIp1" value="192.168.0.1"/>
<!--  dn  -->
<PARAM name="dataNum" value="1"/>
<PARAM name="dataPortBase" value="15400"/>
<PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
<PARAM name="dataNode1_syncNum" value="0"/>
</DEVICE>
<DEVICE sn="node2_hostname">
<PARAM name="name" value="node2_hostname"/>
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<PARAM name="backIp1" value="192.168.0.2"/>
<PARAM name="sshIp1" value="192.168.0.2"/>
</DEVICE>
</DEVICELIST>
</ROOT>
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - “/opt/mogdb/install/om”存放互信等工具，为了避免权限问题，不要把实例数据目录放在此目录下。
> - 安装目录和数据目录需为空或者不存在，否则可能导致安装失败。
> - 在对数据库节点的实例进行具体配置时，需确保配置的目录之间不相互耦合。即各个配置目录不关联，删除其中任意一个目录，不会级联删除其它目录。如gaussdbAppPath为“/opt/mogdb/install/app”，gaussdbLogPath为“/opt/mogdb/install/app/omm”。当gaussdbAppPath目录被删除时，会级联删除gaussdbLogPath目录，从而引起其它问题。
> - 若需要安装脚本自动创建安装用户时，各配置的目录需保证不与系统创建的默认用户目录耦合关联。
> - 配置MogDB路径和实例路径时，路径中不能包含“|”、“;”、“&”、“\$”、“<”、“>”、“\`”、“\\”、“'”、“\”、“,”、“{”，“}”、“(”，“)”、“[”，“]”、“~”、“*”、“?”特殊字符。
> - 配置数据库节点名称时，请通过hostname命令获取数据库节点的主机名称，替换示例中的**node1_hostname**，**node2_hostname**。
> - 配置dcf_config时，角色的配置有leader、follower、passive和logger，其中可投票的角色有leader、follower和logger。配置角色组网时，可投票的角色不能少于3个，因此dcf模式下至少需要三个节点。

**表 1** 参数说明

<table>
    <tr>
      <td><b>参数</b></td>
      <td><b>说明</b></td>
    </tr>
    <tr>
      <td>clusterName</td>
      <td>MogDB名称。</td>
    </tr>
    <tr>
      <td>nodeNames</td>
      <td>MogDB中主机名称。</td>
    </tr>
    <tr>
      <td>backIp1s</td>
      <td>
        主机在后端存储网络中的IP地址（内网IP）。所有MogDB主机使用后端存储网络通讯。
      </td>
    </tr>
    <tr>
      <td>gaussdbAppPath</td>
      <td>
        MogDB程序安装目录。此目录应满足如下要求：<br />- 磁盘空间&gt;1GB<br />-
        与数据库所需其它路径相互独立，没有包含关系。
      </td>
    </tr>
    <tr>
      <td>gaussdbLogPath</td>
      <td>
        MogDB运行日志和操作日志存储目录。此目录应满足如下要求：<br />-
        磁盘空间建议根据主机上的数据库节点数规划。数据库节点预留1GB空间的基础上，再适当预留冗余空间。<br />-
        与MogDB所需其它路径相互独立，没有包含关系。<br />
        此路径可选。不指定的情况下，MogDB安装时会默认指定“$GAUSSLOG/安装用户名”作为日志目录。
      </td>
    </tr>
    <tr>
      <td>tmpdbPath</td>
      <td>
        数据库临时文件存放目录。<br />
        若不配置tmpdbPath，默认存放在/opt/mogdb/tools/perfadm_db目录下。
      </td>
    </tr>
    <tr>
      <td>gaussdbToolPath</td>
      <td>
        MogDB系统工具目录，主要用于存放互信工具等。此目录应满足如下要求：<br />-
        磁盘空间&gt;100MB <br />-
        固定目录，与数据库所需其它目录相互独立，没有包含关系。<br />
        此目录可选。不指定的情况下，MogDB安装时会默认指定“/opt/mogdb/tools”作为数据库系统工具目录。
      </td>
    </tr>
    <tr>
      <td>corePath</td>
      <td>MogDB core文件的指定目录。</td>
    </tr>
    <tr>
      <td>enable_dcf</td>
      <td>是否开启DCF模式。</td>
    </tr>
    <tr>
      <td>dcf_config</td>
      <td>开启DCF模式时配置，DCF启动节点信息。</td>
    </tr>
</table>

### 配置Host基本信息

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 每台Host服务器都必须有如下信息，如下示例以node1为例。
> - 加粗字体内容为示例，可自行替换。每行信息均有注释进行说明。

```xml
<!-- 每台服务器上的节点部署信息 -->
<DEVICELIST>
<!-- 节点1上的部署信息 -->
<DEVICE sn="node1_hostname">
<!-- 节点1的主机名称 -->
<PARAM name="name" value="node1_hostname"/>
<!-- 节点1所在的AZ及AZ优先级 -->
<PARAM name="azName" value="AZ1"/>
<PARAM name="azPriority" value="1"/>
<!-- 节点1的IP，如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
<PARAM name="backIp1" value="192.168.0.1"/>
<PARAM name="sshIp1" value="192.168.0.1"/>
<!-- node1是否为级联备, on表示该实例为级联备，另外级联备机在相同的AZ里需要配有备机 -->
<PARAM name="cascadeRole" value="on"/>
```

**表 2** 参数说明

| 参数       | 说明                                                         |
| :--------- | :----------------------------------------------------------- |
| name       | 主机名称。                                                   |
| azName     | 指定azName（Available Zone Name），字符串（不能含有特殊字符），例如AZ1、AZ2、AZ3。 |
| azPriority | 指定azPriority的优先级。                                     |
| backIp1    | 主机在后端存储网络中的IP地址（内网IP）。所有MogDB主机使用后端存储网络通讯。 |
| sshIp1     | 设置SSH可信通道IP地址（外网IP）。若无外网，则可以不设置该选项或者同backIp1设置相同IP。 |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 配置文件中所有IP参数（包含backIp、sshIp、listenIp等）均只支持配置一个IP。如果配置第二个IP参数，则不会读取第二个参数的配置值。 示例：xml配置文件中同时配置backIp1和backIp2参数： 在解析配置文件时仅读取backIp1参数的配置值，不会读取backIp2参数的配置值。
>
> ```xml
> <PARAM name="backIp1" value="192.168.0.1"/>
> <PARAM name="backIp2" value="192.168.0.2"/>
> ```

### 配置数据库主节点信息

内容为示例，可自行替换。每行信息均有注释进行说明。

**数据库主节点配置以下信息。**

```xml
<!--DBnode-->
<PARAM name="dataNum" value="1"/>
<!--数据库端口号-->
<PARAM name="dataPortBase" value=""/>
<!--DBnode侦听IP-->
<PARAM name="dataListenIp1" value="172.31.13.124,172.31.6.198"/>
<!--数据库主节点上的数据目录，及备机数据目录-->
<PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
<!--数据库主节点上的xlog目录，及备机xlog目录-->
<PARAM name="dataNodeXlogPath1" value="/home/omm/gauss_xlog,/home/omm/gauss_xlog "/>
<!--数据库节点上设定同步模式的节点数-->
<PARAM name="dataNode1_syncNum" value="0"/>
```

**表 3** DBnode参数说明

| 参数              | 说明                                                         |
| :---------------- | :----------------------------------------------------------- |
| dataNum           | 当前主机上需要部署的数据库节点个数。                         |
| dataPortBase      | 数据库节点的基础端口号，默认值40000。                        |
| dataListenIp1     | 侦听的IP地址。未设置时，使用对应主机上的backIp1生成。第一个IP是主节点所在主机IP，第二个IP是备节点所在主机IP。 |
| dataNode1         | 用于指定当前主机上的数据库节点的数据存储目录。此目录为数据库的数据存储目录。应规划到数据盘上。 |
| dataNodeXlogPath1 | 可选参数，用于指定当前数据库中xlog存储路径。此目录为数据库xlog日志存储目录，只支持绝对路径。如不指定，则默认存放在数据目录的pg_xlog目录下。 |
| dataNode1_syncNum | 可选参数，用于指定当前数据库中同步模式的节点数目。取值范围为0~数据库备机节点数。 |

### 配置CM_SERVER（主、非主）信息

**非CMS主节点配置以下信息。**

```xml
<!-- cm -->
<PARAM name="cmServerPortStandby" value="25500"/>
<PARAM name="cmDir" value="/opt/mogdb/data/cm"/>
```

**表 4** CM参数说明

| 参数                | 说明                                                         |
| :------------------ | :----------------------------------------------------------- |
| cmServerPortBase    | 主CM Server端口号，默认值5000。                              |
| cmServerPortStandby | 备CM Server端口号，默认值5500。                              |
| cmServerListenIp1   | CM Server用于侦听CM Agent连接请求或DBA管理请求的IP地址。     |
| cmServerHaIp1       | 主、备CM Server间通信的IP地址。Value中左边为主CM Server的主机IP地址，右边为备CM Server的主机IP地址。未设置时，默认根据主、备CM Server所在主机的backIp1生成。 |
| cmDir               | CM数据文件路径。保存CM Server和CM Agent用到的数据文件，参数文件等。各集群主机上均需配置该参数。 |

**表 5** 目录说明

| 目录            | 说明                                                         |
| :-------------- | :----------------------------------------------------------- |
| /opt/mogdb/data | 集群实例的数据存储总目录。此目录为数据库的数据存储目录。应规划到数据盘上。此目录应满足如下要求：磁盘空间请根据实际业务数据量大小规划。各实例间的数据路径相互独立，彼此间没有包含关系。例如本指南中各实例的数据目录规划如表6所示。 |

**表 6** 数据库实例数据目录

| 实例名称 | 实例数据目录                                                 |
| :------- | :----------------------------------------------------------- |
| CM       | /opt/mogdb/data/cm                                           |
| 主DN     | /opt/mogdb/data/masterX其中，X为从1开始的连续正整数，用于标识当前主机上的第X个DN。 |
| 备DN     | /opt/mogdb/data/slaveX其中，X为从1开始的连续正整数，用X标识当前主机上的第X个备DN。 |

### 示例

#### 单节点配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <!-- 数据库名称 -->
        <PARAM name="clusterName" value="dbCluster" />
        <!-- 数据库节点名称(hostname) -->
        <PARAM name="nodeNames" value="node1_hostname" />
        <!-- 数据库安装目录-->
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <!-- 日志目录-->
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <!-- 临时文件目录-->
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp" />
        <!-- 数据库工具目录-->
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <!-- 数据库core文件目录-->
        <PARAM name="corePath" value="/opt/mogdb/corefile" />
        <!-- 节点IP，与数据库节点名称列表一一对应 -->
        <PARAM name="backIp1s" value="192.168.0.1"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- 节点1上的部署信息 -->
        <DEVICE sn="node1_hostname">
            <!-- 节点1的主机名称 -->
            <PARAM name="name" value="node1_hostname"/>
            <!-- 节点1所在的AZ及AZ优先级 -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 节点1的IP，如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>

      <!--dbnode-->
      <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。

#### 一主一备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <!-- 数据库名称 -->
        <PARAM name="clusterName" value="Cluster_template" />
        <!-- 数据库节点名称(hostname) -->
        <PARAM name="nodeNames" value="node1_hostname,node2_hostname" />
        <!-- 数据库安装目录-->
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <!-- 日志目录-->
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <!-- 临时文件目录-->
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
        <!-- 数据库工具目录-->
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <!-- 数据库core文件目录-->
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <!-- 节点IP，与数据库节点名称列表一一对应 -->
        <PARAM name="backIp1s" value="192.168.0.1,192.168.0.2"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- 节点1上的部署信息 -->
        <DEVICE sn="node1_hostname">
            <!-- 节点1的主机名称 -->
            <PARAM name="name" value="node1_hostname"/>
            <!-- 节点1所在的AZ及AZ优先级 -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 节点1的IP，如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>

      <!--dn-->
            <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>

        <!-- 节点2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="node2_hostname">
            <!-- 节点2的主机名称 -->
            <PARAM name="name" value="node2_hostname"/>
            <!-- 节点2所在的AZ及AZ优先级 -->
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 节点2的IP，如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.2"/>
            <PARAM name="sshIp1" value="192.168.0.2"/>
  </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。

#### 一主二备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="Cluster_template" />
        <PARAM name="nodeNames" value="node1_hostname,node2_hostname,node3_hostname" />

        <PARAM name="gaussdbAppPath" value="/opt/mogdb/install/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/omm" />
        <PARAM name="tmpMppdbPath" value="/opt/mogdb/tmp"/>
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/install/om" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="backIp1s" value="192.168.0.1,192.168.0.2,192.168.0.3"/>

    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="node1_hostname">
            <PARAM name="name" value="node1_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.1"/>
            <PARAM name="sshIp1" value="192.168.0.1"/>
            <!--CM节点部署信息-->
            <PARAM name="cmsNum" value="1"/>
            <PARAM name="cmServerPortBase" value="15000"/>
            <PARAM name="cmServerListenIp1" value="192.168.0.1,192.168.0.2,192.168.0.3"/>
            <PARAM name="cmServerHaIp1" value="192.168.0.1,192.168.0.2,192.168.0.3"/>
            <PARAM name="cmServerlevel" value="1"/>
            <PARAM name="cmServerRelation" value="node1_hostname,node2_hostname,node3_hostname"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
      <!--dn-->
            <PARAM name="dataNum" value="1"/>
      <PARAM name="dataPortBase" value="15400"/>
      <PARAM name="dataNode1" value="/opt/mogdb/install/data/dn,node2_hostname,/opt/mogdb/install/data/dn,node3_hostname,/opt/mogdb/install/data/dn"/>
            <PARAM name="dataNode1_syncNum" value="0"/>
        </DEVICE>

        <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="node2_hostname">
            <PARAM name="name" value="node2_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.2"/>
            <PARAM name="sshIp1" value="192.168.0.2"/>
            <!-- cm -->
            <PARAM name="cmServerPortStandby" value="15000"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
  </DEVICE>

        <!-- node3上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="node3_hostname">
            <PARAM name="name" value="node3_hostname"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.3"/>
            <PARAM name="sshIp1" value="192.168.0.3"/>
            <!-- cm -->
            <PARAM name="cmServerPortStandby" value="15000"/>
            <PARAM name="cmDir" value="/opt/mogdb/data/cmserver"/>
  </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。

<br/>

## 下载安装包

安装前可访问[MogDB下载页面](https://www.mogdb.io/downloads/mogdb)下载适用于您当前操作系统和CPU的安装包。以CentOS系统为例，各安装包及功能介绍见下表。

| **安装包**                            | **功能**                                                     |
| ------------------------------------- | ------------------------------------------------------------ |
| MogDB-x.x.x-CentOS-x86_64.tar     | 包含所有安装包所有文件                                       |
| MogDB-x.x.x-CentOS-64bit.tar.gz      | （主备）MogDB主备（Datanode）。负责存储业务数据、执行数据查询任务以及向客户端返回执行结果。MogDB实例包含主、备两种类型，支持一主多备。建议将主、备MogDB实例分散部署在不同的物理节点中。 |
| MogDB-x.x.x-CentOS-64bit.sha256       | MogDB-x.x.x-CentOS-64bit.tar.gz的sha256的hash值             |
| MogDB-x.x.x-CentOS-64bit-Libpq.tar.gz | C程序访问MogDB数据库的驱动                                     |
| MogDB-x.x.x-CentOS-64bit-om.tar.gz    | 运维管理模块（Operation  Manager）。提供数据库日常运维、配置管理的管理接口、工具。 |
| MogDB-x.x.x-CentOS-64bit-om.sha256    | MogDB-x.x.x-CentOS-64bit-om.tar.gz的sha256的hash值           |
| MogDB-x.x.x-CentOS-64bit-tools.tar.gz | 客户端驱动（Client  Driver）。负责接收来自应用的访问请求，并向应用返回执行结果。客户端驱动负责与MogDB实例通信，发送应用的SQL命令，接收MogDB实例的执行结果 |
| upgrade_sql.tar.gz                    | 升级所需要的SQL                                              |
| upgrade_sql.sha256                    | upgrade_sql.tar.gz的sha256的hash值                           |

<br/>

## 环境初始化

下文以CentOS 7.6系统为例介绍安装过程。

**操作步骤**

1. 以root用户执行以下命令安装所需的基础环境包，以避免后续安装过程中因系统采用最小化安装或者安装包不全导致的报错。

   ```shell
   yum install -y libaio-devel gcc gcc-c++ zlib-devel expect
   ```

2. 以root用户登录待安装MogDB的任意主机，并按规划创建存放安装包的目录，下文以`/opt/software/mogdb`为例。

   ```shell
   mkdir -p /opt/software/mogdb
   chmod 755 -R /opt/software
   ```

3. 进入`/opt/software/mogdb`，将安装包及配置文件`clusterconfig.xml`上传至此目录。若未创建配置文件，可通过`vi clusterconfig.xml`新建并编辑配置文件。

   ```shell
   cd /opt/software/mogdb
   ```

   > 说明：
   > 在允许访问外网的环境中，可通过`wget`命令直接下载适用于当前操作系统和CPU的安装包：
   >
   > ```shell
   > wget -c https://cdn-mogdb.enmotech.com/mogdb-media/3.1.0/MogDB-3.1.0-CentOS-x86_64.tar
   > ```

4. 在安装包所在的目录下，依次解压安装包。

   ```shell
   tar -xvf  MogDB-x.x.x-CentOS-x86_64.tar
   tar -xvf  MogDB-x.x.x-CentOS-64bit-om.tar.gz
   ```

5. 进入到工具脚本存放目录下。

   ```shell
   cd /opt/software/mogdb/script
   ```

6. 为确保openssl版本正确，执行预安装前请加载安装包中lib库。执行命令如下，其中`{packagePath}`为用户安装包放置的路径，本示例中为`/opt/software/mogdb`。

   ```shell
   export LD_LIBRARY_PATH={packagePath}/script/gspylib/clib:$LD_LIBRARY_PATH
   ```

7. 为确保成功安装，请执行如下命令检查主机名称是否一致。预安装过程中，会对hostname进行检查。

   ```shell
   hostname
   vi /etc/hostname
   ```

8. 若您使用的为openEuler系统，需打开`performance.sh`文件注释`#sysctl -w vm.min_free_kbytes=112640 &> /dev/null`

   ```shell
   vi /etc/profile.d/performance.sh
   ```

   完成后跳过第9步即可。

9. CentOS系统需要安装python3，请依次执行以下命令。

   注意：CentOS系统中进行标准安装，请务必安装Python 3.6.x版本，目前不支持Python更高版本。

   ```shell
   wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
   mkdir -p /usr/local/python3
   tar -zxvf Python-3.6.5.tgz
   cd Python-3.6.5
   ./configure --prefix=/usr/local/python3 --enable-shared CFLAGS=-fPIC && make && make install
   ln -s /usr/local/python3/bin/python3 /usr/bin/python3
   ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
   ```

   > 说明：
   >
   > - 如遇`zipimport.ZipImportError: can't decompress data; zlib not available`报错，请执行`yum -y install zlib*`安装解压缩类库，之后重新安装。
   >
   > - 如遇`find:'/run/user/1000/gvfs':权限不够`的报错，可忽略。
   >

<br/>

## 初始化脚本

执行以下命令以初始化脚本：

```shell
/opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml
```

回显如下：

```
Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Distributing package.
Begin to distribute package to tool path.
Successfully distribute package to tool path.
Begin to distribute package to package path.
Successfully distribute package to package path.
Successfully distributed package.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Setting SCTP service.
Successfully set SCTP service.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting Cgroup.
Successfully set Cgroup.
Set ARM Optimization.
Successfully set ARM Optimization.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```

> 说明：
>
> - 运行过程中会创建omm用户，输入yes，然后输入相关密码，需保证复杂度要求，密码应包括大小写、字符、数字，例如Enmo@123。
>
> - 如遇以下错误，则表示Python版本过高。
>
>   ```shell
>   [root@localhost mogdb]# /opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /mogdb/etc/mogdb_clusterconfig.xml
>   Failed to execute cmd: rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib /psutil/_psutil_posix.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' . Error:
>   cp: cannot stat '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9': No such file or directory
>   ```

<br/>

## 执行安装

修改安装目录下lib及script文件夹的用户以及用户组：

```shell
chown -R omm:dbgrp /opt/software/mogdb/lib
chown -R omm:dbgrp /opt/software/mogdb/script
```

依次执行以下命令安装MogDB：

```shell
su - omm
gs_install -X /opt/software/mogdb/clusterconfig.xml --gsinit-parameter="--locale=en_US.UTF-8" --gsinit-parameter="--encoding=UTF-8"
```

回显如下：

```
Parsing the configuration file.
Check preinstall on every node.
Successfully checked preinstall on every node.
Creating the backup directory.
Successfully created the backup directory.
begin deploy..
Installing the cluster.
begin prepare Install Cluster..
Checking the installation environment on all nodes.
begin install Cluster..
Installing applications on all nodes.
Successfully installed APP.
begin init Instance..
encrypt ciper and rand files for database.
Please enter password for database:
Please repeat for database:
begin to create CA cert files
The sslcert will be generated in /opt/mogdb/cluster/app/share/sslcert/om
Cluster installation is completed.
Configuring.
Deleting instances from all nodes.
Successfully deleted instances from all nodes.
Checking node configuration on all nodes.
Initializing instances on all nodes.
Updating instance configuration on all nodes.
Check consistence of memCheck and coresCheck on DN nodes.
Successful check consistence of memCheck and coresCheck on all nodes.
Configuring pg_hba on all nodes.
Configuration is completed.
Successfully started cluster.
Successfully installed application.
```

> 说明：在执行过程中，用户需根据提示输入数据库的密码，密码具有一定的复杂度，为保证用户正常使用该数据库，请记住输入的数据库密码。

<br/>

## 连接数据库

安装完成后，可使用omm用户通过“gsql -d postgres -p 15400 -r”命令连接MogDB数据库，其中“-p 15400”为数据库端口号，请根据实际情况替换。键入“\copyright”可查看版权信息。

```sql
[omm@hostname ~]$ gsql -d postgres -p 15400 -r
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# \copyright
MogDB Kernel Database Management System
Copyright (c) Yunhe Enmo (Beijing) Information Technology Co., Ltd. Copyright © 2009-2022 , All rights reserved.
```

<br/>

## 更改同步节点

默认备库为异步，如果想配置同步需在主库做如下配置。

```shell
[omm@hostname ~]$ sed -i "/synchronous_standby_names/synchronous_standby_names = 'dn_6002'" /mogdb/data/db1/postgresql.conf
[omm@hostname ~]$ gs_om -t restart
```

dn_6002值比较特殊，其值为固定值，主库为dn_6001，备库1为dn_6002，备库2为dn_6003，依次顺延。

'dn_6002,dn_6003'表示备库1，2为同步节点。

请替换相应的数据存储路径。
