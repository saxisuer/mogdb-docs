---
title: 获取安装包
summary: 获取安装包
author: Zhang Cuiping
date: 2021-06-11
---

## 获取安装包

云和恩墨MogDB网站下载页面提供了安装包的获取方式。

安装包下载链接：<https://www.mogdb.io/downloads/mogdb>

**操作步骤**

本节以openEuler系统为例描述如何获取安装包。

1. 上传下载好的标准安装包，并解压以获得安装包。

   ```bash
   tar -xvf MogDB-x.x.x-openEuler-arm64.tar.gz
   ```

   > **说明**：网站上提供标准安装包MogDB-x.x.x-openEuler-arm64.tar.gz，解压标准安装包后得到的MogDB-x.x.x-openEuler-64bit.tar.gz即为极简安装包。

2. 检查安装包。

   解压安装包，检查安装目录及文件是否齐全。在安装包所在目录执行以下命令：

   ```bash
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.gz
   ls -1b
   ```

   执行ls命令，显示类似如下信息：

   ```bash
   total 90296
   drwx------ 3 root root     4096 Mar 31 21:18 bin
   drwx------ 3 root root     4096 Mar 31 21:18 etc
   drwx------ 3 root root     4096 Mar 31 21:18 include
   drwx------ 4 root root     4096 Mar 31 21:18 jre
   drwx------ 5 root root     4096 Mar 31 21:18 lib
   -rw------- 1 root root 92427499 Apr  1 09:43 MogDB-x.x.x-openEuler-64bit.tar.gz
   drwx------ 5 root root     4096 Mar 31 21:18 share
   drwx------ 2 root root     4096 Mar 31 21:18 simpleInstall
   -rw------- 1 root root       32 Mar 31 21:18 version.cfg
   ```