---
title: 容器化安装
summary: 容器化安装
author: Guo Huan
date: 2021-06-01
---

# 容器化安装

本文档主要介绍MogDB容器版安装过程。MogDB容器版本不支持 MogHA工具、OM工具，目前最多支持一主八备。MogDB 3.1容器版支持最新版的compat-tools和插件功能。

由于考虑尽可能缩减容器镜像的大小，以方便快速下载和部署，从2.0版本开始，容器化MogDB在容器里运行的操作系统在x86和ARM架构下分别运行在Ubuntu和Debian之上。

x86-64架构的MogDB容器运行在Ubuntu 18.04操作系统中。

ARM64架构的MogDB容器运行在Debian 10 操作系统中。

<br/>

## Docker部署

### 单实例安装

#### 准备工作

容器化MogDB的安装与宿主机的操作系统无关，能够运行容器引擎的所有操作系统均可支持，比如Linux，Windows，macOS。
在安装容器版MogDB之前，您需要先安装容器运行时环境，比如Docker。

Docker是一个开源的应用容器引擎，基于Go语言并遵从Apache2.0协议开源。Docker可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的Linux机器上，也可以实现虚拟化。

Docker Desktop下载地址：<https://www.docker.com/products/docker-desktop>

<br/>

#### 安装步骤

1. 启动Docker服务。

2. 输入以下命令获取最新版MogDB镜像文件：

   ```bash
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
   ```
   
   > 说明：MogDB镜像支持运行在x86和ARM架构中，会根据发起命令的服务器架构自动下载相应的镜像，无需手工指定。

3. 输入以下命令为 MogDB 创建运行目录，下文以“mogdb”为例：

   ```bash
   mkdir /mogdb
   ```

4. 继续输入以下命令创建一个新的容器，将容器命名为“mogdb”，以启动 MogDB 实例：

   ```bash
   docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123  -v /mogdb:/var/lib/mogdb  -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
   ```

   对于Windows系统而言：

   - 如果Docker使用Hyper-V做引擎，请依次执行以下命令，在Hyper-V中创建卷的逻辑对象mogdata，然后创建容器：

     ```bash
     docker volume create mogdata

     docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v mogdata:/var/lib/mogdb -p 15453:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
     ```

   - 如果Docker启用WSL 2做引擎，请执行以下命令创建容器：

     ```bash
     docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v C:\mogdb:/var/lib/mogdb -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0
     ```

   > 注意：
   >
   > - MogDB的默认监听启动在容器内的5432端口上，如果想要从容器外部访问数据库，则需要在`docker run`的时候指定`-p`参数。比如以上命令将允许使用15432端口访问容器数据库。
   > - 容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在`docker run`的时候指定`-v`参数来实现。比如以上命令将会指定将MogDB的所有数据文件存储在宿主机的`/mogdb`下。如果使用podman，会有目标路径检查，需要预先创建宿主机目标路径（步骤4）。

5. 进入容器终端：

   ```bash
   docker exec -it mogdb bash
   ```

   至此，MogDB 容器版单实例安装完成。

<br/>

#### 使用MogDB

安装完成并进入容器后，通过`su - omm`切换为omm用户，即可通过gsql进行数据库访问以正常体验MogDB各项功能：

```
root@384ac97543bd:/# su - omm
omm@384ac97543bd:~$ gsql -d postgres
gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=#
```

<br/>

#### 环境变量

为了更灵活地使用MogDB镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

**GS_PASSWORD**

使用MogDB镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了MogDB数据库的超级用户omm以及测试用户mogdb的密码。MogDB安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户mogdb是在entrypoint.sh中自定义创建的用户。

MogDB镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

MogDB的密码有复杂度要求，密码长度8个字符以上，必须同时包含大写字母、小写字母、数字、以及特殊符号（特殊符号仅包含( \# ? ! @ $ % ^ & * \- )，并且 ! $ & 需要用转义符"\\"进行转义。

**GS_NODENAME**

指定数据库节点名称，默认为mogdb。

**GS_USERNAME**

指定数据库连接用户名，默认为mogdb。

**GS_PORT**

指定数据库端口，默认为5432。

<br/>

### 主备安装

#### 准备工作

如需进行MogDB容器版主备搭建，请先完成上文“单实例安装”中的步骤。

#### 操作步骤

1. 切换到root用户：

   ```bash
   su - root
   ```

2. 创建自定义网络，创建容器便于使用的固定IP：

   ```bash
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. 创建主库容器：

   ```bash
   docker run --name mogdb_master \
   --network myNetwork --ip 172.18.0.10 --privileged=true \
   --hostname mogdb_master --detach \
   --env GS_PORT=6432 \
   --env OG_SUBNET=172.18.0.0/16 \
   --env GS_PASSWORD=Enmo@1234 \
   --env NODE_NAME=mogdb_master \
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" \
   swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:3.1.0 -M primary
   ```

4. 创建备库容器：

   ```bash
    docker run --name mogdb_slave_one \
    --network myNetwork --ip 172.18.0.11 --privileged=true \
    --hostname mogdb_slave_one --detach \
    --env GS_PORT=6432 \
    --env OG_SUBNET=172.18.0.0/16 \
    --env GS_PASSWORD=Enmotech@1234 \
    --env NODE_NAME=mogdb_slave_one \
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" \
    enmotech/mogdb:3.1.0 -M standby
   ```

5. 查询主库角色状态：

   ```
   [root@localhost ~]# docker exec -it mogdb_master bash 
   [root@mogdb_master /] # su - omm
   [omm@mogdb_master ~] $ gs_ctl query -D /var/lib/mogdb/data/
   [2021-04-07 13:28:15.492][323][][gs_ctl] : gs_ctl query ,datadir is /var/lib/mogdb/data
    HA state:
         local_role                   : Primary
         static_connections           : 1
         db_state                     : Normal
         detail_information           : Normal
         
    Senders info:
         sender_pid                   : 286
         local_role                   : Primary
         peer_role                    : Standby
         peer_state                   : Normal
         state                        : Streaming
         sender_sent_location         : 0/40007D0
         sender_write_location        : 0/40007D0
         sender_flush_location        : 0/40007D0
         sender_replay_location       : 0/40007D0
         receiver_received_location   : 0/40007D0
         receiver_write_location      : 0/40007D0
         receiver_flush_location      : 0/40007D0
         receiver_replay_location     : 0/40007D0
         sync_percent                 : 100%
         sync_state                   : Sync
         sync_priority                : 1
         sync_most_available          : On
         channel                      : 172.18.0.10:6439-->172.18.0.11: 45640
   
    Receiver info: 
   No information
   ```

6. 查询备库角色状态：

   ```
   [root@localhost ~]# docker exec -it mogdb_slave_one bash
   [root@mogdb_slave_one /] # su - omm
   Last login: Wed Apr 7 13:32:23 UTC 2021 on pts/0
   [omm@mogdb_slave_one ~] $ gs_ctl query -D /var/lib/mogdb/data/
   [2021-04-07 14:46:15.492][393][][gs_ctl] : gs_ctl query ,datadir is /var/lib/mogdb/data
    HA state:
         local_role                   : Standby
         static_connections           : 1
         db_state                     : Normal
         detail_information           : Normal
         
    Senders info:
   No information
    Receiver info: 
         receiver_pid                 : 312
         local_role                   : Standby
         peer_role                    : Primary
         peer_state                   : Normal
         state                        : Normal
         sender_sent_location         : 0/4005D50
         sender_write_location        : 0/4005D50
         sender_flush_location        : 0/4005D50
         sender_replay_location       : 0/4005D50
         receiver_received_location   : 0/4005D50
         receiver_write_location      : 0/4005D50
         receiver_flush_location      : 0/4005D50
         receiver_replay_location     : 0/4005D50
         sync_percent                 : 100%
         channel                      : 172.18.0.11:45640<--172.18.0.10:6439
   ```
   
   > 说明：从上面主库Senders信息和备库Receiver可以看到主备状态正常。

#### 读写测试

主库写测试：

```sql
[omm@mogdb_master ~] $ gsql -p6432
gsql ((MogDB 3.1.0 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# create table test(ID int);
CREATE TABLE
MogDB=# insert into test values(1);
INSERT 0 1
```

备库读测试：

```sql
[omm@mogdb_slave_one ~] $ gsql -p6432
gsql ((MogDB 3.1.0 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# select * from test;
 id 
----
  1
(1 row)

MogDB=# delete from test;
ERROR: cannot execute DELETE in a read-only transaction
```

> 说明：命令结果显示主库可写，备库可读但不可写。

#### 切换测试

将mogdb_slave_one切换为主库，mogdb_master切换为备库。

mogdb_slave_one执行switchover：

```
[omm@mogdb_slave_one ~] $ gs_ctl switchover -D /var/lib/mogdb/data/
[2021-04-07 15:31:45.246][428][][gs_ctl]: gs_ctl switchover ,datadir is /var/lib/mogdb/data
[2021-04-07 15:31:45.247][428][][gs_ctl]: switchover term(1)
[2021-04-07 15:31:45.251][428][][gs_ctl]: waiting for server to switchover......
[2021-04-07 15:31:45.325][428][][gs_ctl]: done
[2021-04-07 15:31:45.325][428][][gs_ctl]: switchover completed (/var/lib/mogdb/data)
```

mogdb_slave_one查询状态：

```
[omm@mogdb_slave_one ~] $ gs_ctl query -D /var/lib/mogdb/data/
[2021-04-07 15:42:45.246][459][][gs_ctl]: gs_ctl query ,datadir is /var/lib/mogdb/data
 HA state:
      local_role                   : Primary
      static_connections           : 1
      db_state                     : Normal
      detail_information           : Normal
      
 Senders info:
      sender_pid                   : 442
      local_role                   : Primary
      peer_role                    : Standby
      peer_state                   : Normal
      state                        : Streaming
      sender_sent_location         : 0/40102B8
      sender_write_location        : 0/40102B8
      sender_flush_location        : 0/40102B8
      sender_replay_location       : 0/40102B8
      receiver_received_location   : 0/40102B8
      receiver_write_location      : 0/40102B8
      receiver_flush_location      : 0/40102B8
      receiver_replay_location     : 0/40102B8
      sync_percent                 : 100%
      sync_state                   : Sync
      sync_priority                : 1
      sync_most_available          : On
      channel                      : 172.18.0.11:6439-->172.18.0.10: 49696

 Receiver info: 
No information
```

mogdb_master查询状态：

```
[omm@mogdb_master ~] $ gs_ctl query -D /var/lib/mogdb/data/
[2021-04-07 15:46:35.246][619][][gs_ctl]: gs_ctl query ,datadir is /var/lib/mogdb/data
 HA state:
      local_role                   : Standby
      static_connections           : 1
      db_state                     : Normal
      detail_information           : Normal
      
 Senders info:
No information
 Receiver info: 
      receiver_pid                 : 590
      local_role                   : Standby
      peer_role                    : Primary
      peer_state                   : Normal
      state                        : Normal
      sender_sent_location         : 0/4010718
      sender_write_location        : 0/4010718
      sender_flush_location        : 0/4010718
      sender_replay_location       : 0/4010718
      receiver_received_location   : 0/4010718
      receiver_write_location      : 0/4010718
      receiver_flush_location      : 0/4010718
      receiver_replay_location     : 0/4010718
      sync_percent                 : 100%
      channel                      : 172.18.0.10:49696<--172.18.0.11:6439
```

可以看到mogdb_master变为备库，mogdb_slave_one变为主库，切换成功。

#### 数据读写验证

切换后的主库mogdb_slave_one做写入验证：

```sql
[omm@mogdb_slave_one ~] $ gsql -p6432
gsql ((MogDB 3.1.0 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# select * from test;
 id 
----
  1
(1 row)

MogDB=# insert into test values(2);
INSERT 0 1
```

切换后的备库mogdb_master做读取验证：

```sql
[omm@mogdb_master ~] $ gsql -p6432
gsql ((MogDB 3.1.0 build 56189e20) compiled at 2022-03-21 18:47:53 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

MogDB=# select * from test;
 id 
----
  1
  2
(2 row)

MogDB=# delete from test;
ERROR: cannot execute DELETE in a read-only transaction
```

可以看到，原备库切换为主库后可写，原主库切换为备库后可读但不可写。

<br/>

## Kubernetes部署

MogDB 2.1版本以后支持Kubernetes部署。

- x86
  
  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.1.0/k8s_amd.yaml
  pod/mogdb created
  ```

- arm

  ```
  $ kubectl apply -f https://gitee.com/enmotech/enmotech-docker-mogdb/raw/master/3.1.0/k8s_arm.yaml
  pod/mogdb created
  ```

- k8s连接pod

  进入容器内通过gsql命令登录MogDB。

  ```
  $ kubectl exec -it mogdb -- bash
  root@mogdb:/# su - omm
  omm@mogdb:~$ gsql -d postgres
  gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
  Non-SSL connection (SSL connection is recommended when requiring high-security)
  Type "help" for help.
  
  MogDB=#
  ```

<br/>

## 后续步骤

MogDB容器版不支持MogHA工具、OM工具，仅用于测试使用，目前最多支持一主八备。MogDB企业版包含MogHA组件。容器版与企业版基础功能完全一致，在生产环境中建议使用MogDB企业版。
