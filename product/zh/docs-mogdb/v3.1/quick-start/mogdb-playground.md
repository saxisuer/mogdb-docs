---
title: MogDB实训环境
summary: MogDB实训环境
author: Guo Huan
date: 2022-03-18
---

# MogDB实训环境

MogDB实训环境是由[墨天轮](https://www.modb.pro/)提供的MogDB数据库线上学习与实训平台，同时提供Oracle/MySQL兼容包，以及Mogila样本数据集。您可以通过浏览器的命令行终端一键连接数据库，方便快速地体验MogDB的各项功能，无需自备环境进行安装。

<br/>

## 进入实训环境

访问[MogDB在线实训环境](https://www.modb.pro/terminal)，进入后，左侧会显示相关操作文档，右侧会显示操作平台，此时还未连接上，需要点击【点击进入实训环境】，随后在弹出窗口中注册或登录墨天轮账号即可免费使用。

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-1.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-6.png)

## 开始学习

连接成功后，开始正式运行，这时候就可以执行相关操作了。

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-2.png)

## 参考文档

操作时，可以根据左侧的操作文档进行操作，同时左侧的操作文档可以进行折叠。

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-3.png)

## 关闭环境

想要关闭数据库可以点击右下角的关闭按钮进行关闭，不过关闭后之前创建的数据库数据则被清空，再次连接则是一个新的环境，请谨慎操作。

![image.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-4.png)

## 常见问题

**如果中途退出页面怎么办？**

用户如果直接关闭当前页面，事实上数据库资源还在运行中，只要在30分钟内再进入，之前创建的数据不会被删除（刷新当前页面也是一样的），并且中途退出当前页面，会有相关提示确定是否离开。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/playground-5.png)