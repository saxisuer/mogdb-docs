---
title: 使用NodeJS访问MogDB
summary: 使用NodeJS访问MogDB
author: Guo Huan
date: 2022-03-31
---

# 使用NodeJS访问MogDB

openGauss基于[node-postgres](https://github.com/brianc/node-postgres)提供了用于NodeJS的驱动程序，该驱动程序同样适用于NodeJS连接MogDB。

您可以点击以下链接查看此驱动程序的源码及操作指导：

[源码地址](https://gitee.com/opengauss/openGauss-connector-nodejs)、[操作指导](https://gitee.com/opengauss/openGauss-connector-nodejs/blob/master/README.md)
