---
title: 使用.Net访问MogDB
summary: 使用.Net访问MogDB
author: Guo Huan
date: 2022-07-28
---

# 使用.Net访问MogDB

Npgsql是用于PostgreSQL的开源.NET数据提供程序。它允许您使用.NET连接PostgreSQL服务器并与之交互。openGauss基于[Npgsql](https://www.npgsql.org/)提供了用于.NET的驱动程序，该驱动程序同样适用于.NET语言连接MogDB。

您可以点击以下链接查看此驱动程序的源码及操作指导：

[源码地址](https://gitee.com/opengauss/openGauss-connector-adonet)、[操作指导](https://gitee.com/opengauss/openGauss-connector-adonet/blob/master/README.md)
