---
title: 使用Python访问MogDB
summary: 使用Python访问MogDB
author: Guo Huan
date: 2022-03-25
---

# 使用Python访问MogDB

Psycopg是一种用于执行SQL语句的PythonAPI，可以为PostgreSQL、GaussDB数据库提供统一访问接口，应用程序可基于它进行数据操作。Psycopg2是对libpq的封装，主要使用C语言实现，既高效又安全。它具有客户端游标和服务器端游标、异步通信和通知、支持“COPY TO/COPY FROM”功能。支持多种类型Python开箱即用，适配PostgreSQL数据类型；通过灵活的对象适配系统，可以扩展和定制适配。Psycopg2兼容Unicode和Python 3。

MogDB数据库提供了对Psycopg2特性的支持，并且支持psycopg2通过SSL模式链接。关于使用Python访问MogDB数据库的详细说明请参考《开发者指南》中[基于Psycopg开发](../../../developer-guide/dev/4.1-psycopg-based-development/1-psycopg-based-development.md)章节的内容。

驱动下载：

- [psycopg2](https://opengauss.org/zh/download/)
- [sqlalchemy-mogdb](https://pypi.org/project/sqlalchemy-mogdb/)
- [py-opengauss](https://gitee.com/opengauss/openGauss-connector-python-pyog)
