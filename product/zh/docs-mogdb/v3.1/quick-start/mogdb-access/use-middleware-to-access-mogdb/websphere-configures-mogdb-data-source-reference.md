---
title: WebSphere配置MogDB(PostgreSQL)数据源参考
summary: WebSphere配置MogDB(PostgreSQL)数据源参考
author: Guo Huan
date: 2021-08-09
---

# WebSphere配置MogDB(PostgreSQL)数据源参考

WebSphere配置MogDB数据源参考，同样适用WebSphere配置PostgreSQL数据源，详细内容见下文描述。

<br/>

## 配置前提

### WebSphere JDK版本确认

```bash
C:\Windows\system32>D:\IBM1\WebSphere\AppServer\java\bin\java -version
java version "1.6.0"
Java(TM) SE Runtime Environment (build pwa6460_26sr8fp26ifix-20160708_01(SR8 FP26+IX90174))
IBM J9 VM (build 2.6, JRE 1.6.0 Windows 8 amd64-64 Compressed References 20160406_298378 (JIT enabled, AOT enabled)
J9VM - R26_Java626_SR8_20160406_0830_B298378
JIT  - tr.r11_20160328_114192
GC   - R26_Java626_SR8_20160406_0830_B298378_CMPRSS
J9CL - 20160406_298378)
JCL  - 20160507_01

C:\Windows\system32>
```

找到使用的JDK，并检查版本。

JDBC驱动针对不同的JDK大版本有不同的jar包，明确JDK大版本才能保证下载对应的驱动。

### 下载对应JDBC驱动包

```
当前版本42.2.23。

这是驱动程序的当前版本。除非您有不寻常的需求（运行旧的应用程序或JVM），否则这就是您应该使用的驱动程序。它支持PostgreSQL 8.2或更高版本，并且需要Java 6或更高版本。它包含对SSL和javax.sql包的支持。

如果您使用的是Java8或更高版本，那么应该使用JDBC4.2版本。
如果您使用的是Java7，那么应该使用JDBC4.1版本。
如果您使用的是Java6，那么应该使用JDBC4.0版本。
```

各JDK对应下载JDBC版本链接如下：

| JDK版本 | JDBC驱动链接地址                                             |
| ------- | ------------------------------------------------------------ |
| 1.6     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre6.jar> |
| 1.7     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jre7.jar> |
| 1.8     | <https://jdbc.postgresql.org/download/postgresql-42.2.23.jar>  |

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/weblogic-configures-mogdb-data-source-reference-1.png)

使用 42.2.23 是最佳选择，支持PostgreSQL 8.2或更高版本，而且是当前最稳定版本。

下载地址如下：[https://jdbc.postgresql.org/download/](https://jdbc.postgresql.org/download/)

<br/>

## 配置过程

### 确认db相关信息

| 数据库名 | 用户     | 密码   | 端口 | IP   |
| -------- | -------- | ------ | ---- | ---- |
| postgres | postgres | 123456 | 5432 |      |

### 确认端口联通

telnet ip port 能正常访问到端口

### Console配置

#### JDBC提供程序

a.   新建JDBC提供程序

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-1.png)

b.  创建新的JDBC提供程序

org.postgresql.ds.PGConnectionPoolDataSource

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-2.png)

c.   指定驱动的路径

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-3.png)

d.  下一步完成保存

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-4.png)

#### 配置数据源

a.   新建

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-5.png)

b.  填写JNDI及相关信息

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-6.png)

c.   选择现有的JDBC提供程序

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-7.png)

d.  选默认

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-8.png)

e.   下一步

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-9.png)

f.   完成保存

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-10.png)

#### 配置db认证

a.   点击刚配置的pgtest数据源

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-11.png)

b.  JAAS － J2C 认证数据

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-12.png)

c.   新建

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-13.png)

d.  指定用户密码，别名随意起名

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-14.png)

e.   确定保存

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-15.png)

f.   重新点击刚新建的数据源pgtest，配置“安全性设置”

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-16.png)

g.  确定保存。

#### 定制属性

a.   点击刚配置的pgtest数据源

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-17.png)

 b.  定制属性

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-18.png)

c.   过滤

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-19.png)

d.  过滤serverName

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-20.png)

e.   点击serverName

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-21.png)

f.   填写对应的db的IP地址

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-22.png)

g.  确定保存

h.  同理过滤输入portNumber/ databaseName

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-23.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-24.png)

#### 重启server测试连接

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-25.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-26.png)

<br/>

## 使用openGauss驱动连接MogDB

openGauss JDBC驱动只提供JDK 1.8版本，JDK 1.8的应用使用openGauss驱动能够正常连接使用MogDB。

如果是JDK 1.6、JDK 1.7的应用，那么只能使用PostgreSQL官方提供的JDBC驱动连接MogDB。

openGauss驱动下载地址：

[https://opengauss.org/zh/download/](https://opengauss.org/zh/download/)

<br/>

## 常见问题

驱动版本与JDK版本不匹配

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/websphere-configures-mogdb-data-source-reference-27.png)
