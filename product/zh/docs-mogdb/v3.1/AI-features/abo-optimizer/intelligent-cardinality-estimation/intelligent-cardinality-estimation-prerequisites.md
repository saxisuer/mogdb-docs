---
title: 前置条件
summary: 前置条件
author: Guo Huan
date: 2022-10-24
---

# 前置条件

数据库运行正常，GUC参数enable_ai_stats设置为on，multi_stats_type设置为'BAYESNET'或者'ALL'。
