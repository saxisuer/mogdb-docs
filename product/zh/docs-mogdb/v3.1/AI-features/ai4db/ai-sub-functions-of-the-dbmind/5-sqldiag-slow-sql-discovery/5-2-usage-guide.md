---
title: 使用指导
summary: 使用指导
author: Guo Huan
date: 2021-10-21
---

# 使用指导

## 前提条件

- 需要保证用户提供训练数据。
- 如果用户通过提供的工具收集训练数据，则需要启用WDR功能，涉及到的参数为track_stmt_stat_level和log_min_duration_statement，具体情况见下面小节。
- 为保证预测准确率，用户提供的历史语句日志应尽可能全面并具有代表性。

## SQL流水采集方法

本工具需要用户提前准备数据，训练数据格式如下，每个样本通过换行符分隔：

```
SQL,EXECUTION_TIME
```

预测数据格式如下：

```
SQL
```

其中SQL表示**SQL语句的文本**，EXECUTION_TIME表示**SQL语句的执行时间**，样例数据见sample_data中的train.csv和predict.csv。

用户可以按照要求格式自己收集训练数据，工具也提供了脚本自动采集（load_sql_from_rd），该脚本基于WDR报告获取SQL信息，涉及到的参数有log_min_duration_statement和track_stmt_stat_level：

- 其中log_min_duration_statement表示慢SQL阈值，如果为0则全量收集，时间单位为毫秒；
- track_stmt_stat_level表示信息捕获的级别，建议设置为track_stmt_stat_level='L0,L0'

参数开启后，可能占用一定的系统资源，但一般不大。持续的高并发场景可能产生5%以内的损耗，数据库并发较低的场景，性能损耗可忽略。下述脚本存在于sqldiag根目录（$**GAUSSHOME**/bin/components/sqldiag）中。

```bash
使用脚本获取训练集方式：
load_sql_from_wdr.py [-h] --port PORT --start_time START_TIME
                            --finish_time FINISH_TIME [--save_path SAVE_PATH]
例如：
    python load_sql_from_wdr.py --start_time "2021-04-25 00:00:00" --finish_time "2021-04-26 14:00:00" --port 5432  --save_path ./data.csv
```

## 操作步骤

1. 提供历史日志以供模型训练

2. 进行训练与预测操作：

   ```bash
   基于模板法的训练与预测：
      gs_dbmind component sqldiag [train, predict] -f FILE --model template --model-path template_model_path 
   基于DNN的训练与预测：
      gs_dbmind component sqldiag [train, predict] -f FILE --model dnn --model-path dnn_model_path
   ```

## 使用方法示例

使用提供的测试数据进行模板化训练：

```bash
gs_dbmind component sqldiag train -f ./sample_data/train.csv --model template --model-path ./template 
```

使用提供的测试数据进行模板化预测：

```bash
gs_dbmind component sqldiag predict -f ./sample_data/predict.csv --model template --model-path ./template --predicted-file ./result/t_result
```

使用提供的测试数据进行模板化模型更新：

```bash
gs_dbmind component sqldiag finetune -f ./sample_data/train.csv --model template --model-path ./template 
```

使用提供的测试数据进行DNN训练：

```bash
gs_dbmind component sqldiag train -f ./sample_data/train.csv --model dnn --model-path ./dnn_model 
```

使用提供的测试数据进行DNN预测：

```bash
gs_dbmind component sqldiag predict -f ./sample_data/predict.csv --model dnn --model-path ./dnn_model --predicted-file 
```

使用提供的测试数据进行DNN模型更新：

```bash
gs_dbmind component sqldiag finetune -f ./sample_data/train.csv --model dnn --model-path ./dnn_model
```
