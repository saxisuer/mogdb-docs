---
title: 命令参考
summary: 命令参考
author: Guo Huan
date: 2022-05-06
---

# 命令参考

reprocessing-exporter的使用帮助详情：

```
gs_dbmind component reprocessing_exporter --help
usage:  [-h] [--disable-https] [--ssl-keyfile SSL_KEYFILE] [--ssl-certfile SSL_CERTFILE] [--ssl-ca-file SSL_CA_FILE] [--web.listen-address WEB.LISTEN_ADDRESS] [--web.listen-port WEB.LISTEN_PORT]
        [--collector.config COLLECTOR.CONFIG] [--log.filepath LOG.FILEPATH] [--log.level {debug,info,warn,error,fatal}] [-v]
        prometheus_host prometheus_port

Reprocessing Exporter: A re-processing module for metrics stored in the Prometheus server.

positional arguments:
  prometheus_host       from which host to pull data
  prometheus_port       the port to connect to the Prometheus host

optional arguments:
  -h, --help            show this help message and exit
  --disable-https       disable Https scheme
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --ssl-ca-file SSL_CA_FILE
                        set the path of ssl ca file
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --collector.config COLLECTOR.CONFIG
                        according to the content of the yaml file for metric collection
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above. Valid levels: [debug, info, warn, error, fatal]
  -v, --version         show program's version number and exit
```

**表 1** reprocessing-exporter的命令行参数详情表

| 参数                 | 参数说明                               | 取值范围                        |
| :------------------- | :------------------------------------- | :------------------------------ |
| prometheus_host      | Prometheus-server的IP地址              | -                               |
| prometheus_port      | Prometheus-server的服务侦听端口号      | 1024-65535                      |
| -h, --help           | 帮助选项                               | -                               |
| --disable-https      | 禁用Https协议                          | -                               |
| --ssl-keyfile        | Https协议使用的证书私钥文件路径        | -                               |
| --ssl-certfile       | Https协议使用的证书文件路径            | -                               |
| --ssl-ca-file        | Https协议使用的CA证书文件路径          | -                               |
| --web.listen-address | 该exporter服务的绑定IP                 | -                               |
| --web.listen-port    | 该exporter服务的侦听端口               | 1024-65535                      |
| --collector.config   | 显性指定的待采集指标配置文件路径       | -                               |
| --log.filepath       | 日志文件保存路径，默认保存在当前目录下 | -                               |
| --log.level          | 日志文件的打印级别，默认为INFO级别     | debug, info, warn, error, fatal |
| --version            | 显示版本信息                           | -                               |

openGauss-exporter的使用帮助详情：

```
gs_dbmind component opengauss_exporter --help
usage:  [-h] --url URL [--config-file CONFIG_FILE] [--include-databases INCLUDE_DATABASES] [--exclude-databases EXCLUDE_DATABASES] [--constant-labels CONSTANT_LABELS]
        [--web.listen-address WEB.LISTEN_ADDRESS] [--web.listen-port WEB.LISTEN_PORT] [--disable-cache] [--disable-settings-metrics]
        [--disable-statement-history-metrics] [--disable-https] [--disable-agent] [--ssl-keyfile SSL_KEYFILE] [--ssl-certfile SSL_CERTFILE] [--ssl-ca-file SSL_CA_FILE] [--parallel PARALLEL]
        [--log.filepath LOG.FILEPATH] [--log.level {debug,info,warn,error,fatal}] [-v]

openGauss Exporter (DBMind): Monitoring or controlling for openGauss.

optional arguments:
  -h, --help            show this help message and exit
  --url URL             openGauss database target url. It is recommended to connect to the postgres database through this URL, so that the exporter can actively discover and monitor other databases.
  --config-file CONFIG_FILE, --config CONFIG_FILE
                        path to config file.
  --include-databases INCLUDE_DATABASES
                        only scrape metrics from the given database list. a list of label=value separated by comma(,).
  --exclude-databases EXCLUDE_DATABASES
                        scrape metrics from the all auto-discovered databases excluding the list of database. a list of label=value separated by comma(,).
  --constant-labels CONSTANT_LABELS
                        a list of label=value separated by comma(,).
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --disable-cache       force not using cache.
  --disable-settings-metrics
                        not collect pg_settings.yml metrics.
  --disable-statement-history-metrics
                        not collect statement-history metrics (including slow queries).
  --disable-https       disable Https scheme
  --disable-agent       by default, this exporter also assumes the role of DBMind-Agent, that is, executing database operation and maintenance actions issued by the DBMind service. With this argument,
                        users can disable the agent functionality, thereby prohibiting the DBMind service from making changes to the database.
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --ssl-ca-file SSL_CA_FILE
                        set the path of ssl ca file
  --parallel PARALLEL   not collect pg_settings.yml metrics.
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above. Valid levels: [debug, info, warn, error, fatal]
  -v, --version         show program's version number and exit
```

**表 2** openGauss-exporter的命令行参数详情表

| 参数                                | 参数说明                                                     | 取值范围                                                     |
| :---------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| --url                               | 数据库server的连接地址，例如 postgres://user:pwd@host:port/dbname | 如果该url涉及到的各字段URL包含特殊字符（如@, /等），则需要通过URL编码进行转义，例如密码中的“@“应转义为%40, “/“应转义为%2F. 否则各字段的含义会被错误识别和切分，具体转义规则可以参考URL编码的转义规则, 该URL地址规则遵循 [RFC-1738](https://www.ietf.org/rfc/rfc1738.txt) 标准。 |
| --constant-labels                   | 常量列表，k=v格式，用逗号隔开，表明该exporter自带的常量标签  | 格式如cluster_name=demo,cluster_id=1                         |
| -h, --help                          | 帮助选项                                                     | -                                                            |
| --disable-https                     | 禁用Https协议                                                | -                                                            |
| --ssl-keyfile                       | Https协议使用的证书私钥文件路径                              | -                                                            |
| --ssl-certfile                      | Https协议使用的证书文件路径                                  | -                                                            |
| --ssl-ca-file                       | Https协议使用的CA证书文件路径                                | -                                                            |
| --web.listen-address                | 该exporter服务的绑定IP                                       | -                                                            |
| --web.listen-port                   | 该exporter服务的侦听端口                                     | 1024-65535                                                   |
| --config, --config-file             | 显性指定的待采集指标配置文件路径                             | -                                                            |
| --log.filepath                      | 日志文件保存路径，默认保存在当前目录下                       | -                                                            |
| --log.level                         | 日志文件的打印级别，默认为INFO级别                           | debug, info, warn, error, fatal                              |
| --version                           | 显示版本信息                                                 | -                                                            |
| --disable-cache                     | 禁止使用缓存                                                 | -                                                            |
| --disable-settings-metrics          | 禁止采集pg_settings表的值                                    | -                                                            |
| --disable-statement-history-metrics | 禁止收集statement_history表中的慢SQL信息                     | -                                                            |
| --disable-agent                     | 禁止agent行为                                                | -                                                            |
| --include-databases                 | 显性表明仅采集的数据库名，指定多个数据库时用逗号（,）隔开    | -                                                            |
| --exclude-databases                 | 显性表明不监控的数据库名，指定多个数据库时用逗号（,）隔开    | -                                                            |
| --parallel                          | 连接到MogDB的数据库连接池的大小                              | 正整数                                                       |

cmd-exporter的使用帮助详情：

```
usage:  [-h] [--constant-labels CONSTANT_LABELS]
                   [--web.listen-address WEB.LISTEN_ADDRESS]
                   [--web.listen-port WEB.LISTEN_PORT]
                   [--disable-https]
                   [--config CONFIG] [--ssl-keyfile SSL_KEYFILE]
                   [--ssl-certfile SSL_CERTFILE] [--ssl-ca-file SSL_CA_FILE]
                   [--parallel PARALLEL] [--log.filepath LOG.FILEPATH]
                   [--log.level {debug,info,warn,error,fatal}] [-v]

Command Exporter (DBMind): scrape metrics by performing shell commands.

optional arguments:
  -h, --help            show this help message and exit
  --constant-labels CONSTANT_LABELS
                        a list of label=value separated by comma(,).
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --disable-https       disable Https scheme
  --config CONFIG       path to config dir or file.
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --ssl-ca-file SSL_CA_FILE
                        set the path of ssl ca file
  --parallel PARALLEL   performing shell command in parallel.
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above.
                        Valid levels: [debug, info, warn, error, fatal]
  -v, --version         show program's version number and exit
```

**表 3** cmd-exporter的命令行参数详情表

| 参数                 | 参数说明                                                    | 取值范围                                                     |
| :------------------- | :---------------------------------------------------------- | :----------------------------------------------------------- |
| -h, --help           | 帮助选项                                                    | -                                                            |
| --disable-https      | 禁用Https协议                                               | -                                                            |
| --ssl-keyfile        | Https协议使用的证书私钥文件路径                             | -                                                            |
| --ssl-certfile       | Https协议使用的证书文件路径                                 | -                                                            |
| --ssl-ca-file        | Https协议使用的CA证书文件路径                               |                                                              |
| --web.listen-address | 该exporter服务的绑定IP                                      | -                                                            |
| --web.listen-port    | 该exporter服务的侦听端口                                    | 1024-65535                                                   |
| --config             | 显性指定的待采集指标配置文件路径                            | 默认是该功能yamls目录下的default.yml文件，可以参考该配置文件格式，错误配置会报错 |
| --log.filepath       | 日志文件保存路径，默认保存在当前目录下                      | -                                                            |
| --log.level          | 日志文件的打印级别，默认为INFO级别                          | debug, info, warn, error, fatal                              |
| --parallel           | 并行执行shell命令的并发度                                   | 正整数                                                       |
| --constant-labels    | 常量列表，k=v格式，用逗号隔开，表明该exporter自带的常量标签 | 格式如cluster_name=demo,cluster_id=1                         |
| --version            | 显示版本信息                                                | -                                                            |