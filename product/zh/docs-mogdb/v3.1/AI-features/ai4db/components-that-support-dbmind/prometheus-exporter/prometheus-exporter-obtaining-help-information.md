---
title: 获取帮助
summary: 获取帮助
author: Guo Huan
date: 2022-05-06
---

# 获取帮助

用户可以通过--help命令获取帮助信息，例如：

```
gs_dbmind component opengauss_exporter --help
gs_dbmind component reprocessing_exporter --help
gs_dbmind component cmd_exporter --help
```
