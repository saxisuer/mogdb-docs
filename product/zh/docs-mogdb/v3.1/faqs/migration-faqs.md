---
title: 迁移FAQs
summary: 迁移FAQs
author: Guo Huan
date: 2021-09-26
---

# 迁移FAQs

**Q1：目前支持哪些数据库迁移到MogDB？**

通过云和恩墨自研的MTK异构数据库迁移工具，您可以将以下数据库的数据结构、全量数据高速导入到MogDB。

* Oracle
* MySQL
* DB2 
* openGauss
* PostgreSQL（存储过程不支持）
* SQL Server（分区表不支持）

更多信息请访问[MTK官方文档](http://docs.mogdb.io:8000/zh/mtk/v2.0/overview)。