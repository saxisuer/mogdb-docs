---
title: 导出并查看wdr诊断报告
summary: 导出并查看wdr诊断报告
author: GUO HUAN
date: 2022-10-13
---

# 导出并查看WDR诊断报告

访问WDR快照数据需要sysadmin或monadmin权限，因此需要使用root账号或其他拥有权限的账号来生成WDR诊断报告。

1. 执行如下命令新建报告文件。

   ```
   touch  /home/om/wdrTestNode.html
   ```

2. 连接系统库postgres。

   ```
   gsql -d postgres -p 端口号 -r
   ```

3. 选择snapshot.snapshot表中两个不同的snapshot，当这两个snapshot之间未发生服务重启，便可以使用这两个snapshot生成报告。

   ```
   gsql> select * from snapshot.snapshot order by start_ts desc limit 10;
   ```

4. 执行如下命令，在本地生成HTML格式的WDR报告。

   1. 执行如下命令，设置报告格式。\a: 不显示表行列符号， \t: 不显示列名 ，\o: 指定输出文件。

      ```
      gsql> \a      
      gsql> \t 
      gsql> \o {报告路径}
      ```

   2. 执行如下命令，生成HTML格式的WDR报告。

      ```
      gsql> select generate_wdr_report(begin_snap_id Oid, end_snap_id Oid, int report_type, int report_scope, int node_name );
      ```

   示例一，生成集群级别的报告：

   ```
     select generate_wdr_report(1, 2, 'all', 'cluster',null);
   ```

   示例二，生成某个节点的报告：

   ```
     select generate_wdr_report(1, 2, 'all', 'node', pgxc_node_str()::cstring);
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：** 当前MogDB的节点名固定是“dn_6001_6002_6003”，也可直接代入。

   **表 1** 参数说明

   | 参数          | 说明                                                         | 取值范围                                                     |
   | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | begin_snap_id | 要查看的某段时间性能的开始的snapshot的id（表snapshot.snaoshot中的snapshot_id） | -                                                            |
   | end_snap_id   | 结束snapshot的id，默认end_snap_id大于begin_snap_id（表snapshot.snaoshot中的snapshot_id） | -                                                            |
   | report_type   | 指定生成report的类型。                                       | - summary<br/>- detail<br/>- all，即同时包含summary和detail。 |
   | report_scope  | 指定生成report的范围。                                       | - cluster：集群<br/>- node：集群中某个节点。                 |
   | node_name     | - 在report_scope指定为single node时，需要把该参数指定为对应节点的名称。<br/>- 在report_scope为cluster时，该值可以指定为省略或者为NULL。 | -                                                            |

5. 执行如下命令关闭输出选项及格式化输出命令。

   ```
   \o \a \t 
   ```

6. 在/home/om/下根据需要查看WDR报告内容。

**表 2** WDR报表主要内容

| 项目                                             | 描述                                                         |
| :----------------------------------------------- | :----------------------------------------------------------- |
| Database Stat（集群范围）                        | 数据库维度性能统计信息：事务，读写，行活动，写冲突，死锁等。 |
| Load Profile（集群范围）                         | 集群维度的性能统计信息：CPU时间，DB时间，逻辑读/物理读，IO性能，登入登出，负载强度，负载性能表现等。 |
| Instance Efficiency Percentages（集群/节点范围） | 集群级或者节点缓冲命中率。                                   |
| IO Profile（集群/节点范围）                      | 集群或者节点维度的IO的使用情况。                             |
| Top 10 Events by Total Wait Time（节点范围）     | 最消耗时间的事件。                                           |
| Wait Classes by Total Wait Time（节点范围）      | 最消耗时间的等待时间分类。                                   |
| Host CPU（节点范围）                             | 主机CPU消耗。                                                |
| Memory Statistics（节点范围）                    | 内核内存使用分布。                                           |
| Time Model（节点范围）                           | 节点范围的语句的时间分布信息。                               |
| Wait Events（节点范围）                          | 节点级别的等待事件的统计信息。                               |
| Cache IO Stats (集群/节点范围)                   | 用户的表、索引的IO的统计信息。                               |
| Utility status （节点范围）                      | 复制槽和后台checkpoint的状态信息。                           |
| Object stats（集群/节点范围）                    | 表、索引维度的性能统计信息。                                 |
| Configuration settings（节点范围）               | 节点配置。                                                   |
| SQL Statistics（集群/节点范围）                  | SQL语句各个维度性能统计：端到端时间，行活动，缓存命中，CPU消耗，时间消耗细分。 |
| SQL Detail（集群/节点范围）                      | SQL语句文本详情。                                            |
