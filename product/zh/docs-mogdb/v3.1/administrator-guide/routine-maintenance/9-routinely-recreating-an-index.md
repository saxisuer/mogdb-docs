---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 例行重建索引

## 背景信息

数据库经过多次删除操作后，索引页面上的索引键将被删除，造成索引膨胀。例行重建索引，可有效的提高查询效率。

数据库支持的索引类型为B-tree索引，例行重建索引可有效的提高查询效率。

- 如果数据发生大量删除后，索引页面上的索引键将被删除，导致索引页面数量的减少，造成索引膨胀。重建索引可回收浪费的空间。
- 新建的索引中逻辑结构相邻的页面，通常在物理结构中也是相邻的，所以一个新建的索引比更新了多次的索引访问速度要快。

<br/>

## 重建索引

重建索引有以下两种方式:

- 先运行DROP INDEX语句删除索引，再运行CREATE INDEX语句创建索引。

  在删除索引过程中，会在父表上增加一个短暂的排他锁，阻止相关读写操作。在创建索引过程中，会锁住写操作但是不会锁住读操作，此时读操作只能使用顺序扫描。

- 使用REINDEX语句重建索引。

  - 使用REINDEX TABLE语句重建索引，会在重建过程中增加排他锁，阻止相关读写操作。
  - 使用REINDEX INTERNAL TABLE语句重建desc表（包括列存表的cudesc表）的索引，会在重建过程中增加排他锁，阻止相关读写操作。

<br/>

## 操作步骤

假定在导入表"areaS"上的"area_id"字段上存在普通索引"areaS_idx"。重建索引有以下两种方式:

- 先删除索引（DROP INDEX），再创建索引（CREATE INDEX）

  1. 删除索引。

     ```sql
     MogDB=# DROP INDEX areaS_idx;
     ```
     
     当结果显示如下信息，则表示删除成功。
     
     ```
     DROP INDEX
     ```

  2. 创建索引。

     ```sql
     MogDB=# CREATE INDEX areaS_idx ON areaS (area_id);
     ```
     
     当结果显示如下信息，则表示创建成功。
     
     ```
     CREATE INDEX
     ```

- 使用REINDEX重建索引。

  - 使用REINDEX TABLE语句重建索引。

      ```sql
      MogDB=# REINDEX TABLE areaS;
      ```
      
      当结果显示如下信息，则表示重建成功。
      
      ```
      REINDEX
      ```

  - 使用REINDEX INTERNAL TABLE重建desc表（包括列存表的cudesc表）的索引。

      ```sql
      MogDB=# REINDEX INTERNAL TABLE areaS;
      ```
      
      当结果显示如下信息，则表示重建成功。
      
      ```
      REINDEX
      ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 在重建索引前，用户可以通过临时增大maintenance_work_mem和psort_work_mem的取值来加快索引的重建。
