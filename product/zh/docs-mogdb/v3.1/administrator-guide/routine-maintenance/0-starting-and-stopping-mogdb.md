---
title: 启停MogDB
summary: 启停MogDB
author: Guo Huan
date: 2021-06-24
---

# 启停MogDB

## OM启停

### 启动MogDB

1. 以操作系统用户omm登录数据库主节点。

2. 使用以下命令启动MogDB。

   ```bash
   gs_om -t start
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  双机启动必须以双机模式启动，若中间过程以单机模式启动，则必须修复才能恢复双机关系，用gs_ctl build进行修复，gs_ctl的使用方法参见“参考指南 > 工具参考 > 系统内部使用的工具 > [gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md)”章节。

### 停止MogDB

1. 以操作系统用户omm登录数据库主节点。

2. 使用以下命令停止MogDB。

   ```bash
   gs_om -t stop
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   > 启停节点及AZ的操作请参见“参考指南 > 工具参考 > 服务端工具 > [gs_om](../../reference-guide/tool-reference/server-tools/8-gs_om.md)”章节。

### 示例

启动MogDB：

```bash
gs_om -t start
Starting cluster.
=========================================
=========================================
Successfully started.
```

停止MogDB：

```bash
gs_om -t stop
Stopping cluster.
=========================================
Successfully stopped cluster.
=========================================
End stop cluster.
```

### 错误排查

如果启动MogDB或者停止MogDB服务失败，请根据日志文件中的日志信息排查错误，参见[日志参考](11-log-reference.md)。

如果是超时导致启动失败，可以执行如下命令，设置启动超时时间，默认超时时间为300s。

```bash
gs_om -t start --time-out=300
```

<br/>

## PTK启停

### 启动集群

> 下面的集群操作以集群 `cluster_slirist` 为例

在安装完数据库集群后，PTK 默认会将数据库集群启动。

如果安装时指定了 `--skip-launch-db` 的话，数据库会处于停止状态。

此时可通过 `cluster start` 指令启动集群，需指定集群的集群名称。

示例：

```shell
# ptk cluster -n cluster_slirist start
INFO[2022-08-02T11:40:48.728] Operating: Starting.
INFO[2022-08-02T11:40:48.728] =========================================
INFO[2022-08-02T11:40:48.784] starting host 192.168.122.101
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.102
INFO[2022-08-02T11:40:56.329] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:40:56.613] waiting for check cluster state...
INFO[2022-08-02T11:41:01.861] =========================================
INFO[2022-08-02T11:41:01.861] Successfully started.
INFO[2022-08-02T11:41:01.861] Operation succeeded: Start.
```

同时，PTK 默认会启动集群内所有实例，PTK 也支持指定单个实例启动：

```shell
# ptk cluster -n cluster_slirist start -H 192.168.122.101
INFO[2022-08-02T11:50:04.442] Operating: Starting.
INFO[2022-08-02T11:50:04.442] =========================================
INFO[2022-08-02T11:50:06.692] starting host 192.168.122.101 successfully
```

更多启动参数请查看帮助文档：

```shell
# ptk cluster start -h
启动数据库实例或集群

Usage:
  ptk cluster start [flags]

Flags:
  -h, --help                   help for start
  -H, --host string            操作的实例IP
  -n, --name string            集群名称
      --security-mode string   是否使用安全模式启动数据库
                               可选项: on/off
      --time-out duration      启动超时时间 (default 10m0s)
```

### 停止集群

> 下面的集群操作以集群 `cluster_slirist` 为例

如果想要停止数据库集群，可以通过 `cluster stop` 指令，默认会停止集群内所有实例：

```shell
# ptk cluster -n cluster_slirist stop
INFO[2022-08-02T11:49:40.685] Operating: Stopping.
INFO[2022-08-02T11:49:40.685] =========================================
INFO[2022-08-02T11:49:40.891] stopping host 192.168.122.102
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.101
INFO[2022-08-02T11:49:43.004] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:49:43.004] =========================================
INFO[2022-08-02T11:49:43.004] Successfully stoped.
INFO[2022-08-02T11:49:43.004] Operation succeeded: Stop.
```

如果想要停止集群内某个实例，可通过 `-H` 指定实例的IP

```shell
# ptk cluster -n cluster_slirist stop -H 192.168.122.101
INFO[2022-08-02T11:56:32.880] Operating: Stopping.
INFO[2022-08-02T11:56:32.881] =========================================
INFO[2022-08-02T11:56:34.154] stopping host 192.168.122.101 successfully
```

停止集群的更多参数，请查看帮助文档：

```shell
# ptk cluster stop -h
停止数据库实例或集群

Usage:
  ptk cluster stop [flags]

Flags:
  -h, --help                help for stop
  -H, --host string         操作的实例IP
  -n, --name string         集群名称
      --time-out duration   停止超时时间 (default 10m0s)
```

### 重启集群

> 下面的集群操作以集群 `cluster_slirist` 为例

重启集群的操作，本质上是先停止数据库，再启动数据库的组合操作。

可通过 `cluster restart` 指令来实现：

```shell
# ptk cluster -n cluster_slirist restart
INFO[2022-08-02T11:59:31.037] Operating: Stopping.
INFO[2022-08-02T11:59:31.037] =========================================
INFO[2022-08-02T11:59:31.217] stopping host 192.168.122.102
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.102 successfully
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.101
INFO[2022-08-02T11:59:33.309] stopping host 192.168.122.101 successfully
INFO[2022-08-02T11:59:33.309] =========================================
INFO[2022-08-02T11:59:33.309] Successfully stoped.
INFO[2022-08-02T11:59:33.309] Operation succeeded: Stop.

INFO[2022-08-02T11:59:33.310] Operating: Starting.
INFO[2022-08-02T11:59:33.310] =========================================
INFO[2022-08-02T11:59:33.376] starting host 192.168.122.101
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.101 successfully
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.102
INFO[2022-08-02T11:59:36.787] starting host 192.168.122.102 successfully
INFO[2022-08-02T11:59:36.995] waiting for check cluster state...
INFO[2022-08-02T11:59:42.247] =========================================
INFO[2022-08-02T11:59:42.247] Successfully started.
INFO[2022-08-02T11:59:42.247] Operation succeeded: Start.
```

重启集群的更多参数，请查看帮助文档：

```shell
# ptk cluster restart -h
重启数据库实例或集群

Usage:
  ptk cluster restart [flags]

Flags:
  -h, --help                   help for restart
  -H, --host string            操作的实例IP
  -n, --name string            集群名称
      --security-mode string   是否使用安全模式启动数据库
                               可选项: on/off
      --time-out duration      启动超时时间 (default 10m0s)
```
