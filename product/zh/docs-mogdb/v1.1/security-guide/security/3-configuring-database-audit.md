---
title: 设置数据库审计
summary: 设置数据库审计
author: Guo Huan
date: 2021-03-04
---

# 设置数据库审计

<br/>

## 审计概述

**背景信息**

数据库安全对数据库系统来说至关重要。MogDB将用户对数据库的所有操作写入审计日志。数据库安全管理员可以利用这些日志信息，重现导致数据库现状的一系列事件，找出非法操作的用户、时间和内容等。

关于审计功能，用户需要了解以下几点内容:

- 审计总开关audit_enabled支持动态加载。在数据库运行期间修改该配置项的值会立即生效，无需重启数据库。默认值为on，表示开启审计功能。
- 除了审计总开关，各个审计项也有对应的开关。只有开关开启，对应的审计功能才能生效。
- 各审计项的开关支持动态加载。在数据库运行期间修改审计开关的值，不需要重启数据库便可生效。

目前，MogDB支持以下审计项如[表1](#表3.1.1)所示。

**表 1** 配置审计项<a id="表3.1.1"> </a>

| 配置项                                  | 描述                                                                                                                                          |
| :-------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------- |
| 用户登录、注销审计                      | 参数: audit_login_logout<br/>默认值为7，表示开启用户登录、退出的审计功能。设置为0表示关闭用户登录、退出的审计功能。不推荐设置除0和7之外的值。 |
| 数据库启动、停止、恢复和切换审计        | 参数: audit_database_process<br/>默认值为1，表示开启数据库启动、停止、恢复和切换的审计功能。                                                  |
| 用户锁定和解锁审计                      | 参数: audit_user_locked<br/>默认值为1，表示开启审计用户锁定和解锁功能。                                                                       |
| 用户访问越权审计                        | 参数: audit_user_violation<br/>默认值为0，表示关闭用户越权操作审计功能。                                                                      |
| 授权和回收权限审计                      | 参数: audit_grant_revoke<br/>默认值为1，表示开启审计用户权限授予和回收功能。                                                                  |
| 数据库对象的CREATE，ALTER，DROP操作审计 | 参数: audit_system_object<br/>默认值为12295，表示只对DATABASE、SCHEMA、USER、DATA SOURCE这四类数据库对象的CREATE、ALTER、DROP操作进行审计。   |
| 具体表的INSERT、UPDATE和DELETE操作审计  | 参数: audit_dml_state<br/>默认值为0，表示关闭具体表的DML操作（SELECT除外）审计功能。                                                          |
| SELECT操作审计                          | 参数: audit_dml_state_select<br/>默认值为0，表示关闭SELECT操作审计功能。                                                                      |
| COPY审计                                | 参数: audit_copy_exec<br/>默认值为0，表示关闭copy操作审计功能。                                                                               |
| 存储过程和自定义函数的执行审计          | 参数: audit_function_exec<br/>默认值为0，表示不记录存储过程和自定义函数的执行审计日志。                                                       |
| SET审计                                 | 参数: audit_set_parameter<br/>默认值为1，表示记录set操作审计日志                                                                              |

安全相关参数及默认值请参见[表2](#表3.1.2)。

**表 2** 安全相关参数及默认值<a id="表3.1.2"> </a>

| 参数名                      | 默认值     | 说明                                                              |
| :-------------------------- | :--------- | :--------------------------------------------------------------- |
| ssl                         | on         | 指定是否启用SSL连接。                                                                                                                      |
| require_ssl                 | off        | 指定服务器端是否强制要求SSL连接。                                                                                                          |
| ssl_ciphers                 | ALL        | 指定SSL支持的加密算法列表。                                                                                                                |
| ssl_cert_file               | server.crt | 指定包含SSL服务器证书的文件的名称。                                                                                                        |
| ssl_key_file                | server.key | 指定包含SSL私钥的文件名称。                                                                                                                |
| ssl_ca_file                 | cacert.pem | 指定包含CA信息的文件的名称。                                                                                                               |
| ssl_crl_file                | NULL       | 指定包含CRL信息的文件的名称。                                                                                                              |
| password_policy             | 1          | 指定是否进行密码复杂度检查。                                                                                                               |
| password_reuse_time         | 60         | 指定是否对新密码进行可重用天数检查。                                                                                                       |
| password_reuse_max          | 0          | 指定是否对新密码进行可重用次数检查。                                                                                                       |
| password_lock_time          | 1          | 指定帐户被锁定后自动解锁的时间。                                                                                                           |
| failed_login_attempts       | 10         | 如果输入密码错误的次数达到此参数值时，当前帐户被锁定。                                                                                     |
| password_encryption_type    | 2          | 指定采用何种加密方式对用户密码进行加密存储。                                                                                               |
| password_min_uppercase      | 0          | 密码中至少需要包含大写字母的个数。                                                                                                         |
| password_min_lowercase      | 0          | 密码中至少需要包含小写字母的个数。                                                                                                         |
| password_min_digital        | 0          | 密码中至少需要包含数字的个数。                                                                                                             |
| password_min_special        | 0          | 密码中至少需要包含特殊字符的个数。                                                                                                         |
| password_min_length         | 8          | 密码的最小长度。<br/>说明:<br/>在设置此参数时，请将其设置成不大于password_max_length，否则进行涉及密码的操作会一直出现密码长度错误的提示   |
| password_max_length         | 32         | 密码的最大长度。<br/>说明:<br/>在设置此参数时，请将其设置成不小于password_min_length，否则进行涉及密码的操作会一直出现密码长度错误的提示。 |
| password_effect_time        | 90         | 密码的有效期限。                                                                                                                           |
| password_notify_time        | 7          | 密码到期提醒的天数。                                                                                                                       |
| audit_enabled               | on         | 控制审计进程的开启和关闭。                                                                                                                 |
| audit_directory             | pg_audit   | 审计文件的存储目录。                                                                                                                       |
| audit_data_format           | binary     | 审计日志文件的格式，当前仅支持二进制格式（binary）。                                                                                       |
| audit_rotation_interval     | 1d         | 指定创建一个新审计日志文件的时间间隔。当现在的时间减去上次创建一个审计日志的时间超过了此参数值时，服务器将生成一个新的审计日志文件。       |
| audit_rotation_size         | 10MB       | 指定审计日志文件的最大容量。当审计日志消息的总量超过此参数值时，服务器将生成一个新的审计日志文件。                                         |
| audit_resource_policy       | on         | 控制审计日志的保存策略，以空间还是时间限制为优先策略，on表示以空间为优先策略。                                                             |
| audit_file_remain_time      | 90         | 表示需记录审计日志的最短时间要求，该参数在audit_resource_policy为off时生效。                                                               |
| audit_space_limit           | 1GB        | 审计文件占用磁盘空间的最大值。                                                                                                             |
| audit_file_remain_threshold | 1048576    | 审计目录下审计文件的最大数量。                                                                                                             |
| audit_login_logout          | 7          | 指定是否审计数据库用户的登录（包括登录成功和登录失败）、注销。                                                                             |
| audit_database_process      | 1          | 指定是否审计数据库启动、停止、切换和恢复的操作。                                                                                           |
| audit_user_locked           | 1          | 指定是否审计数据库用户的锁定和解锁。                                                                                                       |
| audit_user_violation        | 0          | 指定是否审计数据库用户的越权访问操作。                                                                                                     |
| audit_grant_revoke          | 1          | 指定是否审计数据库用户权限授予和回收的操作。                                                                                               |
| audit_system_object         | 12295      | 指定是否审计数据库对象的CREATE、DROP、ALTER操作。                                                                                          |
| audit_dml_state             | 0          | 指定是否审计具体表的INSERT、UPDATE、DELETE操作。                                                                                           |
| audit_dml_state_select      | 0          | 指定是否审计SELECT操作。                                                                                                                   |
| audit_copy_exec             | 0          | 指定是否审计COPY操作。                                                                                                                     |
| audit_function_exec         | 0          | 指定在执行存储过程、匿名块或自定义函数（不包括系统自带函数）时是否记录审计信息。                                                           |
| audit_set_parameter         | 1          | 指定是否审计SET操作。                                                                                                                      |
| enableSeparationOfDuty      | off        | 指定是否开启三权分立。                                                                                                                     |
| session_timeout             | 10min      | 建立连接会话后，如果超过此参数的设置时间，则会自动断开连接。                                                                               |
| auth_iteration_count        | 10000      | 认证加密信息生成过程中使用的迭代次数。                                                                                                     |

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

    ```bash
    gsql -d mogdb -p 8000
    ```

    mogdb为需要连接的数据库名称，8000为数据库主节点的端口号。

    连接成功后，系统显示类似如下信息:

    ```bash
    gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-05-08 02:59:43 commit 2143 last mr 131
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.

    mogdb=#
    ```

3. 检查审计总开关状态。

    a. 用show命令显示审计总开关audit_enabled的值。

    ```sql
    mogdb=# SHOW audit_enabled;
    ```

    如果显示为off，执行\\q命令退出数据库。

    b. 执行如下命令开启审计功能，参数设置立即生效。

    ```bash
    gs_guc set -N all -I all -c "audit_enabled=on"
    ```

4. 配置具体的审计项。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    >
    > - 只有开启审计功能，用户的操作才会被记录到审计文件中。
    > - 各审计项的默认参数都符合安全标准，用户可以根据需要开启其他审计功能，但会对性能有一定影响。

    以开启对数据库所有对象的增删改操作的审计开关为例，其他配置项的修改方法与此相同，修改配置项的方法如下所示:

    ```bash
    gs_guc reload -N all -I all -c "audit_system_object=12295"
    ```

    其中audit_system_object代表审计项开关，12295为该审计开关的值。

<br/>

## 查看审计结果

**前提条件**

- 审计功能总开关已开启。
- 需要审计的审计项开关已开启。
- 数据库正常运行，并且对数据库执行了一系列增、删、改、查操作，保证在查询时段内有审计结果产生。
- 数据库各个节点审计日志单独记录。

**背景信息**

- 只有拥有AUDITADMIN属性的用户才可以查看审计记录。有关数据库用户及创建用户的办法请参见[用户](#用户)。

- 审计查询命令是数据库提供的sql函数pg_query_audit，其原型为:

    ```bash
    pg_query_audit(timestamptz startime,timestamptz endtime,audit_log)
    ```

    参数startime和endtime分别表示审计记录的开始时间和结束时间，audit_log表示所查看的审计日志信息所在的物理文件路径，当不指定audit_log时，默认查看连接当前实例的审计日志信息。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
  > startime和endtime的差值代表要查询的时间段，其有效值为从startime日期中的00:00:00开始到endtime日期中的23:59:59之间的任何值。请正确指定这两个参数，否则将查不到需要的审计信息。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

    ```bash
    gsql -d mogdb -p 8000
    ```

    mogdb为需要连接的数据库名称，8000为数据库主节点的端口号。

    连接成功后，系统显示类似如下信息:

    ```bash
    gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-05-08 02:59:43 commit 2143 last mr 131
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.

    mogdb=#
    ```

3. 查询审计记录。

    ```sql
    mogdb=# SELECT * FROM pg_query_audit('2015-07-15 08:00:00','2015-07-15 09:47:33');
    ```

    查询结果如下:

    ```sql
    time          |     type      | result | username |    database    | client_conninfo |  object_name   |                          detail_info                | node_name |            thread_id            | local_port | remote_port
    ------------------------+---------------+--------+----------+----------------+-----------------+----------------+---------------------------------------------------------------+-----------+---------------------------------+------------+-------------
    2015-07-15 08:03:55+08 | login_success | ok     | omm | mogdb       | gsql@::1    | mogdb       | login db(mogdb) success,the current user is:omm       | dn_5003   | 139808902997776@490233835920483 | 9000       | 55805
    ```

    该条记录表明，用户omm在2015-07-15 08:03:55+08登录数据库mogdb。其中client_conninfo字段在log_hostname启动且IP连接时，字符@后显示反向DNS查找得到的主机名。

<br/>

## 维护审计日志

**前提条件**

用户必须拥有审计权限。

**背景信息**

- 与审计日志相关的配置参数及其含义请参见[表1](#表3.3.1)。

    **表 1** 审计日志相关配置参数<a id="表3.3.1"> </a>

    | 配置项                      | 含义                           | 默认值                         |
    | :-------------------------- | :----------------------------- | :----------------------------- |
    | audit_directory             | 审计文件的存储目录。           | /var/log/mogdb/用户名/pg_audit |
    | audit_resource_policy       | 审计日志的保存策略。           | on（表示使用空间配置策略）     |
    | audit_space_limit           | 审计文件占用的磁盘空间总量。   | 1GB                            |
    | audit_file_remain_time      | 审计日志文件的最小保存时间。   | 90                             |
    | audit_file_remain_threshold | 审计目录下审计文件的最大数量。 | 1048576                        |

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    >
    > 如果使用gs_om工具部署MogDB，则审计日志路径为 "/var/log/mogdb/用户名/pg_audit"。

- 审计日志删除命令为数据库提供的sql函数pg_delete_audit，其原型为:

    ```
    pg_delete_audit(timestamp startime,timestamp endtime)
    ```

    其中参数startime和endtime分别表示审计记录的开始时间和结束时间。

- 目前常用的记录审计内容的方式有两种: 记录到数据库的表中、记录到OS文件中。这两种方式的优缺点比较如[表2](#表3.3.2)所示。

    **表 2** 审计日志保存方式比较<a id="表3.3.2"> </a>

    | 方式           | 优点                                                                   | 缺点                                                                                                                                 |
    | :------------- | :--------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------- |
    | 记录到表中     | 不需要用户维护审计日志。                                               | 由于表是数据库的对象，如果一个数据库用户具有一定的权限，就能够访问到审计表。如果该用户非法操作审计表，审计记录的准确性难以得到保证。 |
    | 记录到OS文件中 | 比较安全，即使一个帐户可以访问数据库，但不一定有访问OS这个文件的权限。 | 需要用户维护审计日志。                                                                                                               |

    从数据库安全角度出发，MogDB采用记录到OS文件的方式来保存审计结果，保证了审计结果的可靠性。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

    ```bash
    gsql -d mogdb -p 8000
    ```

    mogdb为需要连接的数据库名称，8000为数据库主节点的端口号。

    连接成功后，系统显示类似如下信息:

    ```bash
    gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-05-08 02:59:43 commit 2143 last mr 131
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.

    mogdb=#
    ```

3. 选择日志维护方式进行维护。

    - 设置自动删除审计日志

        审计文件占用的磁盘空间或者审计文件的个数超过指定的最大值时，系统将删除最早的审计文件，并记录审计文件删除信息到审计日志中。

        > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
        > 审计文件占用的磁盘空间大小默认值为1024MB，用户可以根据磁盘空间大小重新设置参数。

        配置审计文件占用磁盘空间的大小（audit_space_limit）。

        a. 查看已配置的参数。

        ```sql
        mogdb=# SHOW audit_space_limit;
         audit_space_limit
        -------------------
         1GB
        (1 row)
        ```

        如果显示结果不为1GB（1024MB），执行\\q命令退出数据库。

        b. 建议执行如下命令设置成默认值1024MB。

        ```bash
        gs_guc reload -N all -I all -c "audit_space_limit=1024MB"
        ```

        配置审计文件个数的最大值（audit_file_remain_threshold）。
        a. 查看已配置的参数。

        ```sql
        mogdb=# SHOW audit_file_remain_threshold;
         audit_file_remain_threshold
        -----------------------------
         1048576
        (1 row)
        ```

        如果显示结果不为1048576，执行\\q命令退出数据库。

        b. 建议执行如下命令设置成默认值1048576。

        ```bash
        gs_guc reload -N all -I all -c "audit_file_remain_threshold=1048576"
        ```

    - 手动备份审计文件

        当审计文件占用的磁盘空间或者审计文件的个数超过配置文件指定的值时，系统将会自动删除较早的审计文件，因此建议用户周期性地对比较重要的审计日志进行保存。

        a. 使用show命令获得审计文件所在目录（audit_directory）。

        ```sql
        mogdb=# SHOW audit_directory;
        ```

        b. 将审计目录整个拷贝出来进行保存。

    - 手动删除审计日志

        当不再需要某时段的审计记录时，可以使用审计接口命令pg_delete_audit进行手动删除。

        以删除2012/9/20到2012/9/21之间的审计记录为例:

        ```sql
        mogdb=# SELECT pg_delete_audit('2012-09-20 ','2012-09-21');
        ```

<br/>

## 设置文件权限安全策略

**背景信息**

数据库在安装过程中，会自动对其文件权限（包括运行过程中生成的文件，如日志文件等）进行设置。其权限规则如下:

- 数据库程序目录的权限为0750。

- 数据库数据文件目录的权限为0700。

    MogDB部署时通过创建xml配置文件中的tmpMppdbPath参数指定目录（若未指定，则默认创建/tmp/$USER_mppdb目录）来存放".s.PGSQL.*"文件，该目录和文件权限设置为0700。

- 数据库的数据文件、审计日志和其他数据库程序生成的数据文件的权限为0600，运行日志的权限默认不高于0640。

- 普通操作系统用户不允许修改和删除数据库文件和日志文件。

**数据库程序目录及文件权限**

数据库安装后，部分程序目录及文件权限如[表1](#表3.4.1)所示。

**表 1** 文件及目录权限<a id="表3.4.1"> </a>

| 文件/目录                       | 父目录                 | 权限 |
| :------------------------------ | :--------------------- | :--- |
| bin                             | -                      | 0700 |
| lib                             | -                      | 0700 |
| share                           | -                      | 0700 |
| data（数据库节点/数据库主节点） | -                      | 0700 |
| base                            | 实例数据目录           | 0700 |
| global                          | 实例数据目录           | 0700 |
| pg_audit                        | 实例数据目录（可配置） | 0700 |
| pg_log                          | 实例数据目录（可配置） | 0700 |
| pg_xlog                         | 实例数据目录           | 0700 |
| postgresql.conf                 | 实例数据目录           | 0600 |
| pg_hba.conf                     | 实例数据目录           | 0600 |
| postmaster.opts                 | 实例数据目录           | 0600 |
| pg_ident.conf                   | 实例数据目录           | 0600 |
| gs_initdb                       | bin                    | 0700 |
| gs_dump                         | bin                    | 0700 |
| gs_ctl                          | bin                    | 0700 |
| gs_guc                          | bin                    | 0700 |
| gsql                            | bin                    | 0700 |
| archive_status                  | pg_xlog                | 0700 |
| libpq.so.5.5                    | lib                    | 0600 |

**建议**

数据库在安装过程中，会自动对其文件权限（包括运行过程中生成的文件，如日志文件等）进行设置，适合大多数情况下的权限要求。如果用户产品对相关权限有特殊要求，建议用户安装后定期检查相关权限设置，确保完全符合产品要求。
