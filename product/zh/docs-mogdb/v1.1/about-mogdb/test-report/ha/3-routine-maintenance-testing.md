---
title: MogDB日常维护测试
summary: MogDB日常维护测试
author: Liu Xu
date: 2021-03-04
---

# MogDB日常维护测试

## 测试范围

1. HA工具切换switchover

2. gs_ctl切换 switchover

3. HA工具切换failover

4. gs_ctl切换 failover

5. gs_ctl启动与HA配合相悖测试

6. 双主脑裂测试

<br/>

## 测试环境

| 类别     |      服务端配置       |  客户端配置   | 数量  |
| -------- | :-------------------: | :-----------: | :---: |
| CPU      |      Kunpeng 920      |  Kunpeng 920  |  128  |
| 内存     |     DDR4,2933MT/s     | DDR4,2933MT/s | 2048G |
| 硬盘     |       Nvme 3.5T       |    Nvme 3T    |  4个  |
| 文件系统 |          Xfs          |      Xfs      |   4   |
| OS       | openEuler 20.03 (LTS) |   Kylin V10   |       |
| 数据库   |   MogDB1.1.0安装包    |               |       |
| 测试工具 |        pgbench        |               |       |

<br/>

## HA工具切换switchover

测试步骤:

1. 查询数据库状态。

2. 在备库上执行切换命令，之后查询数据库状态。

    ```
    /home/omm/venv/bin/python3 /home/omm/openGaussHA_standlone/switch.py -config /home/omm/openGaussHA_standlone/node.conf --switchover
    ```

3. 在新主库上查询vip。

测试结果:

主备角色切换正常。

<br/>

## gs_ctl切换 switchover

测试步骤:

1. 查询数据库状态。
2. 使用命令进行切换，完成后查询状态。
3. 查询vip。

测试结果:

主备角色切换正常。

<br/>

## HA工具切换failover

测试步骤:

1. 查看数据库当前状态。

2. 执行ha的failover命令。

3. 观察集群状态。

4. 发现原备库成为新主库。

测试结果:

1. 原先的备库成为新主库并有了vip。

2. 原先的主库被杀掉。

3. TPS在failover之后约10秒从6000升到13000。

<br/>

## gs_ctl切换 failover

测试步骤:

1. 查看数据库当前状态。

2. 执行gs_ctl的failover命令。

3. 观察新主库状态。

4. 观察原先的主库。

5. 观察脚本变化。

测试结果:

1. 新主库等待10s被自动关闭,原先的主库不受影响，vip依然在原先的主库上。

2. 在备库failover后约5、6秒，TPS从9000升到13000。除波动外无其他变化。

<br/>

## gs_ctl启动与HA配合相悖测试

测试步骤:

1. 记录集群状态。

2. 同时stop主库和备库。

3. gs_ctl 启动主库和备库，指定原来的备库为主库，主库为备库。

4. 观察脚本及集群状态。

测试结果:

1. HA配置跟随实际情况调整。

2. TPS在关闭主库和备库库后从9000降至0，HA在检测到集群配置改变并在10s内修复挂起了新主库的vip，之后后约5、6s，TPS从0升到13000。

<br/>

## 双主脑裂测试

测试步骤:

1. 查看当前集群状态。

2. 用gs_ctl 命令重启备库为主库。

测试结果:

1. 超时10s后，非正常操作重新start的主库被监控脚本kill掉。

2. 修复备库并加入集群。

    ```
    gs_ctl build -b full -D /gaussdata/openGauss/db1
    ```

3. 重启备库为主库后TPS从6000升到13000。

4. 备库重新加入集群后TPS从13000降到9000的一主一备水平。
