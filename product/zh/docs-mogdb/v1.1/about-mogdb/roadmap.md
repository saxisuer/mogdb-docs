---
title: 功能迭代计划
summary: 功能迭代计划
author: Guo Huan
date: 2021-04-14
---

# 功能迭代计划

本文档主要介绍MogDB 2.0.0的功能迭代计划。2.0.0与之前版本特性功能保持兼容，新增功能如下:

## MogDB Server

- 支持主库设置为全局只读，可用于网络隔离情况下的防脑裂处理
- 支持pg trgm插件（全文索引插件）
- 支持自定义操作符，自定义操作符类
- 降低数据库指标采集的资源消耗
- 分区创建语法优化
- 复制增加函数，在receiver线程未启动时候，可以查看xlog相关信息
- 优化对CJK（东亚字符集）的排序支持，针对大数据量排序有更好的表现
- 支持基于xid的闪回查询
- 支持show语法模糊查询会话设置

## MogDB Client

- go客户端: 新增支持go的sha256加密算法支持
- python客户端: 新增支持sha256加密算法，支持主库自动识别
- c客户端: 支持主库自动识别
- odbc客户端: 支持主库自动识别

## MogHA

- 新增支持主库实例自动重启
- 新增支持数据库cpu使用限额
- 新增支持HA切换自定义操作
- 新增配套WEB界面，api等，方便小规模部署使用（EA阶段）

## MogDB plugin

- pg_repack 插件兼容支持，用于在线的数据文件空间回收
- wal2json 插件兼容支持，用于现在的逻辑日志导出以及异构复制
- pg_dirtyread 插件支持，用于特殊情况下的数据恢复和回查
- walminer 插件支持，用于在线的wal日志解析（不依赖逻辑日志）
- db_link 插件支持，用于从MogDB连接PostgreSQL数据库

## MogDB toolkit

- gs_xlogdump 用于离线导出wal日志内容
- gs_filedump 用于离线导出数据文件内容
