---
title: wal2json extention for MogDB&openGauss
summary: wal2json extention for MogDB&openGauss
author: Guo Huan
date: 2021-06-02
---

# wal2json extention for MogDB&openGauss

## 逻辑解码（wal2json）

基于MogDB和openGauss的wal2json插件，可以将逻辑日志文件输出为JSON格式供您查看。

## 前提条件

已设置实例参数wal_level = logical。

## 背景信息

wal2json是逻辑解码插件，使用该插件可以访问由INSERT和UPDATE生成的元组，解析WAL中的内容。

wal2json插件会在每个事务中生成一个JSON对象。JSON对象中提供了所有新/旧元组，额外选项还可以包括事务时间戳、限定架构、数据类型、事务ID等属性。

## 通过SQL获取JSON对象

1. 登录MogDB数据库

2. 执行如下命令建表及初始化插件。

   ```
   --开另一个session执行以下
   pg_recvlogical -d mogdb --slot test_slot --create-slot -P wal2json
   pg_recvlogical -d mogdb --slot test_slot --start -o pretty-print=1 -f -
   --进行一些基本的DML操作
   CREATE TABLE test_table (
       id char(10) NOT NULL,
       code        char(10),
       PRIMARY KEY (id)
   );
   mogdb=# INSERT INTO test_table (id, code) VALUES('id1', 'code1');
   INSERT 0 1
   mogdb=# update test_table set code='code2' where id='id1';
   UPDATE 1
   mogdb=# delete from test_table where id='id1';
   DELETE 1
   ```

   对应DML的输出：

   **INSERT**

   ```json
   {
     "change": [
       {
         "kind": "insert",
         "schema": "mdmv2",
         "table": "test_table",
         "columnnames": ["id", "code"],
         "columntypes": ["character(10)", "character(10)"],
         "columnvalues": ["id1       ", "code1     "]
       }
     ]
   }
   ```

   **UPDATE**

   ```json
   {
     "change": [
       {
         "kind": "update",
         "schema": "mdmv2",
         "table": "test_table",
         "columnnames": ["id", "code"],
         "columntypes": ["character(10)", "character(10)"],
         "columnvalues": ["id1       ", "code2     "],
         "oldkeys": {
           "keynames": ["id"],
           "keytypes": ["character(10)"],
           "keyvalues": ["id1       "]
         }
       }
     ]
   }
   ```

   **DELETE**

   ```json
   {
     "change": [
       {
         "kind": "delete",
         "schema": "mdmv2",
         "table": "test_table",
         "oldkeys": {
           "keynames": ["id"],
           "keytypes": ["character(10)"],
           "keyvalues": ["id1       "]
         }
       }
     ]
   }
   ```

   也可以用REPLICA IDENTITY来决定对表UPDATE和DELETE操作时，logical日志输出信息的详细程度。

   - DEFAULT：逻辑日志中包含表中主键列被UPDATE或DELETE的前值的信息。
   - NOTHING：逻辑日志中不包含表中任何UPDATE或DELETE变更的信息。
   - FULL：逻辑日志中包含表中被UPDATE或DELETE列的前值的整行信息。
   - USING INDEX ：仅包含指定索引中所有列的前值信息。
