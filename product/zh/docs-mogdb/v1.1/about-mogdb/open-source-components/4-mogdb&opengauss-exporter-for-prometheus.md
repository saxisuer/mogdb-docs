---
title: MogDB & openGauss Server Exporter
summary: MogDB & openGauss Server Exporter
author: Guo Huan
date: 2021-06-02
---

# MogDB & openGauss Server Exporter

本文介绍适用于MogDB和openGauss服务器指标的Prometheus exporter工具。

## 限制

- openGauss密码认证方式必须为md5

## 快速开始

本软件包适用于Docker：

```
# 启动一个示例数据库
docker run --net=host -it --rm -e GS_PASSWORD=mtkOP@128 enmotech/opengauss
# 连接该数据库
docker run --net=host -e DATA_SOURCE_NAME="postgresql://postgres:password@localhost:5432/postgres?sslmode=disable" mogdb/opengauss_exporter
```

## 构建及运行

### github

make命令默认构建二进制文件：

```
$ go get github.com/enmotech/opengauss_exporter
$ cd ${GOPATH-$HOME/go}/src/github.com/enmotech/opengauss_exporter
$ make build
$ export DATA_SOURCE_NAME="postgresql://login:password@hostname:port/dbname"
$ ./bin/opengauss_exporter <flags>
```

### gitee

make命令默认构建二进制文件：

```
$ go get gitee.com/enmotech/opengauss_exporter
$ cd ${GOPATH-$HOME/go}/src/gitee.com/enmotech/opengauss_exporter
$ make build
$ export DATA_SOURCE_NAME="postgresql://login:password@hostname:port/dbname"
$ ./bin/opengauss_exporter <flags>
```

要构建docker，请运行`make docker`。

### 标记

- `help` 显示上下文相关的帮助说明（也可以使用 --help-long 和 --help-man）。
- `web.listen-address` web界面和遥测监听端口。默认为`:9187`。
- `web.telemetry-path` 暴露指标的路径。默认为`/metrics`。
- `disable-settings-metrics` 如果不想采集`pg_settings`，请使用此标记。
- `auto-discover-databases` 是否动态发现服务器上的数据库。
- `config` 包含要运行的查询的YAML文件的路径。访问[`og_exporter.yaml`](https://github.com/enmotech/opengauss_exporter/blob/master/og_exporter_default.yaml)可查看格式示例。
- `--dry-run` 不运行-打印指标映射的内部表示。在调试自定义查询文件时很有用。
- `constantLabels` 要在所有指标中设置的标签。以英文逗号分隔的`label=value`对的列表。
- `version` 显示应用版本。
- `exclude-databases` 启用autoDiscoverDatabases时要删除的数据库列表。
- `log.level` 设置日志级别：`debug`、`info`、`warn`、`error`、`fatal`
- `log.format` 设置日志输出目标和格式。如`logger:syslog?appname=bob&local=7`或`logger:stdout?json=true`，默认为`logger:stderr`。

### 环境变量

以下环境变量用于配置exporter：

- `DATA_SOURCE_NAME` `PG_EXPORTER_URL` 默认的遗留格式。接受URI形式和key=value格式的参数。URI可以包含要连接的用户名和密码。
- `OG_EXPORTER_WEB_LISTEN_ADDRESS` web界面和遥测监听端口。默认为`:9187`。
- `OG_EXPORTER_WEB_TELEMETRY_PATH` 暴露指标的路径。默认为`/metrics`。
- `OG_EXPORTER_DISABLE_SETTINGS_METRICS` 如果不想采集`pg_settings`，请使用此标记。值可以为`true`或`false`。默认为`false`。
- `OG_EXPORTER_AUTO_DISCOVER_DATABASES` 是否动态发现服务器上的数据库。值可以为`true`或`false`。默认为`false`。
- `OG_EXPORTER_CONSTANT_LABELS` 要在所有指标中设置的标签。以英文逗号分隔的`label=value`对的列表。
- `OG_EXPORTER_EXCLUDE_DATABASES` 启用autoDiscoverDatabases时要删除的数据库列表（以英文逗号分隔）。默认为空字符串。

如果指定CLI标记，则以`OG_`开头的环境变量设置项将被相应的CLI标记覆盖。

### 设置openGauss服务器的数据源名称

openGauss服务器的[数据源名称](http://en.wikipedia.org/wiki/Data_source_name)必须通过`DATA_SOURCE_NAME`环境变量来设置。

此设置便于在默认的Debian/Ubuntu操作系统中以本地方式运行exporter工具（需要时转换为初始化脚本）：

```
DATA_SOURCE_NAME="user=postgres host=/var/run/postgresql/ sslmode=disable" opengauss_exporter
```

您也可以设置一个数据源列表，以便在一个exporter程序中采集不同的实例，只需定义一个以英文逗号分隔的字符串即可。

```
DATA_SOURCE_NAME="port=5432,port=6432" opengauss_exporter
```

有关格式化连接字符串的其他方法，请参阅[github.com/lib/pq](http://github.com/lib/pq)模块。

### 通过配置文件添加新的指标

--config命令行参数指定包含要运行的其他查询的YAML文件。[og_exporter.yaml](https://github.com/enmotech/opengauss_exporter/blob/master/og_exporter_default.yaml)中提供了一些示例。

### 自动发现数据库

为了采集数据库服务器上所有数据库的指标，可以通过`--auto-discover-databases`标记动态发现数据库DSN。当此标记设置为true时，工具将为所有已配置的DSN运行`SELECT datname FROM pg_database WHERE datallowconn = true AND datistemplate = false and datname != current_database()`。运行结果将创建一组新的DSN，以便采集指标。

此外，选项`--exclude-databases`可对自动发现的结果进行过滤，以丢弃不需要的数据库。

### 运行测试

```
make build
cd test;sh test.sh ../bin/opengauss_exporter <config_file>
```

### openGauss

### 监控用户

```
CREATE USER dbuser_monitor with login monadmin PASSWORD 'Mon@1234';
grant usage on schema dbe_perf to dbuser_monitor;
grant select on pg_stat_replication to dbuser_monitor;
```

### 主备节点

```
docker network create opengauss_network --subnet=172.11.0.0/24
docker run --network opengauss_network --ip 172.11.0.101 \
  --privileged=true --name opengauss_primary  -h opengauss_primary  -p 1111:5432 -d \
  -e GS_PORT=5432 -e OG_SUBNET=172.11.0.0/24 -e GS_PASSWORD=Gauss@123 -e NODE_NAME=opengauss_primary \
  -e 'REPL_CONN_INFO=replconninfo1 = '\''localhost=172.11.0.101 localport=5434 localservice=5432 remotehost=172.11.0.102 remoteport=5434 remoteservice=5432'\''\n' enmotech/opengauss:1.1.0 -M primary
docker run --network opengauss_network --ip 172.11.0.102 \
  --privileged=true --name opengauss_standby1 -h opengauss_standby1 -p 1112:5432 -d \
  -e GS_PORT=5432 -e OG_SUBNET=172.11.0.0/24 -e GS_PASSWORD=Gauss@123 -e NODE_NAME=opengauss_standby1 \
  -e 'REPL_CONN_INFO=replconninfo1 = '\''localhost=172.11.0.102 localport=5434 localservice=5432 remotehost=172.11.0.101 remoteport=5434 remoteservice=5432'\''\n' enmotech/opengauss:1.1.0 -M standby
```
