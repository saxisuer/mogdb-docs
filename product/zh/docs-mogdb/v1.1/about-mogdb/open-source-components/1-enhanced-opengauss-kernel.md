---
title: 增强的openGauss内核
summary: 增强的openGauss内核
author: Liuxu
date: 2021-06-09
---

# 增强的openGauss内核

<br/>

MogDB是在openGauss开源内核的基础上封装和改善的对于企业应用更加友好的企业级数据库。在openGauss内核的基础上，MogDB增加了MogHA、时序（应对IoT场景）、分布式（实现每个数据节点为一个openGauss内核的分布式环境）、Oracle视图兼容（DBA_和V$视图）等多种插件，其中MogHA插件用于进行主备架构下高可用的自动化管理，对于企业级应用来说是至关重要的。同时也同步研发了MogDB Manager管理软件，其中包括备份恢复、性能监控，健康检查工具包、自动化部署、数据库迁移、数据同步、兼容性分析、性能压测和极限数据恢复多种针对企业级易用性需求的组件。
