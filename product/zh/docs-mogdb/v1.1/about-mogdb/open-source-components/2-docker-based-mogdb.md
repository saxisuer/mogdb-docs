---
title: 容器版本的MogDB
summary: 容器版本的MogDB
author: Liuxu
date: 2021-06-09
---

# 容器版本的MogDB

<br/>

## 特点

- 随着每一个MogDB的版本变化，第一时间发布镜像的新版本。
- 容器版本数据库镜像内置最佳实践的初始化参数配置。
- 容器版本数据库同时支持x86平台和ARM平台。

**目前已经支持x86-64和ARM64两种架构，请根据您宿主机的机器架构获取对应的容器镜像。**

- x86-64架构的MogDB运行在[CentOS7.6操作系统](https://www.centos.org/)中
- ARM64架构的MogDB运行在[openEuler 20.03 LTS操作系统](https://openeuler.org/zh/)中

<br/>

## 如何使用本镜像

您可以从下方获取容器版本的详细内容：

[安装指南-容器化安装](../../installation-guide/docker-installation/docker-installation.md)
