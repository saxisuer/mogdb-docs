---
title: ALTER INDEX
summary: ALTER INDEX
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER INDEX

## 功能描述

ALTER INDEX用于修改现有索引的定义。

它有几种子形式：

- IF EXISTS

  如果指定的索引不存在，则发出一个notice而不是error。

- RENAME TO

  只改变索引的名称。对存储的数据没有影响。

- SET TABLESPACE

  这个选项会改变索引的表空间为指定表空间，并且把索引相关的数据文件移动到新的表空间里。

- SET ( { STORAGE_PARAMETER = value } [, …] )

  改变索引的一个或多个索引方法特定的存储参数。 需要注意的是索引内容不会被这个命令立即修改，根据参数的不同，可能需要使用REINDEX重建索引来获得期望的效果。

- RESET ( { storage_parameter } [, …] )

  重置索引的一个或多个索引方法特定的存储参数为缺省值。与SET一样，可能需要使用REINDEX来完全更新索引。

- [ MODIFY PARTITION index_partition_name ] UNUSABLE

  用于设置表或者索引分区上的索引不可用。

- REBUILD [ PARTITION index_partition_name ]

  用于重建表或者索引分区上的索引。

- RENAME PARTITION

  用于重命名索引分区。

- MOVE PARTITION

  用于修改索引分区的所属表空间。

## 注意事项

只有索引的所有者或者拥有索引所在表的INDEX权限的用户有权限执行此命令，系统管理员默认拥有此权限。

## 语法格式

- 重命名表索引的名称。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      RENAME TO new_name;
  ```

- 修改表索引的所属空间。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      SET TABLESPACE tablespace_name;
  ```

- 修改表索引的存储参数。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      SET ( {storage_parameter = value} [, ... ] );
  ```

- 重置表索引的存储参数。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      RESET ( storage_parameter [, ... ] ) ;
  ```

- 设置表索引或索引分区不可用。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      [ MODIFY PARTITION index_partition_name ] UNUSABLE;
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  列存表不支持该语法。

- 重建表索引或索引分区。

  ```sql
  ALTER INDEX index_name
      REBUILD [ PARTITION index_partition_name ];
  ```

- 重命名索引分区。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      RENAME PARTITION index_partition_name TO new_index_partition_name;
  ```

- 修改索引分区的所属表空间。

  ```sql
  ALTER INDEX [ IF EXISTS ] index_name
      MOVE PARTITION index_partition_name TABLESPACE new_tablespace;
  ```

## 参数说明

- **index_name**

  要修改的索引名。

- **new_name**

  新的索引名。

  取值范围: 字符串，且符合标识符命名规范。

- **tablespace_name**

  表空间的名称。

  取值范围: 已存在的表空间。

- **storage_parameter**

  索引方法特定的参数名。

- **value**

  索引方法特定的存储参数的新值。根据参数的不同，这可能是一个数字或单词。

- **new_index_partition_name**

  新索引分区名。

- **index_partition_name**

  索引分区名。

- **new_tablespace**

  新表空间。

## 示例

请参见CREATE INDEX的示例。
