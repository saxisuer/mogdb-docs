---
title: EXPLAIN PLAN
summary: EXPLAIN PLAN
author: Zhang Cuiping
date: 2021-05-18
---

# EXPLAIN PLAN

## 功能描述

通过EXPLAIN PLAN命令可以将查询执行的计划信息存储于PLAN_TABLE表中。与EXPLAIN命令不同的是，EXPLAIN PLAN仅将计划信息进行存储，而不会打印到屏幕。

## 语法格式

```sql
EXPLAIN PLAN
[ SET STATEMENT_ID = string ]
FOR statement ;
```

## 参数说明

- EXPLAIN中的PLAN选项表示需要将计划信息存储于PLAN_TABLE中，存储成功将返回“EXPLAIN SUCCESS”。

- STATEMENT_ID用户可以对查询设置标签，输入的标签信息也将存储于PLAN_TABLE中。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  > 用户在执行EXPLAIN PLAN时，如果没有进行SET STATEMENT_ID，则默认为空值。同时，用户可输入的STATEMENT_ID最大长度为30个字节，超过长度将会产生报错。

## 注意事项

- EXPLAIN PLAN不支持在数据库节点上执行。
- 对于执行错误的SQL无法进行计划信息的收集。
- PLAN_TABLE中的数据是session级生命周期并且session隔离和用户隔离，用户只能看到当前session、当前用户的数据。

## 示例

使用EXPLAIN PLAN收集SQL语句的执行计划，通常包括以下步骤：

1. 执行EXPLAN PLAN。

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   > 执行EXPLAIN PLAN 后会将计划信息自动存储于PLAN_TABLE中，不支持对PLAN_TABLE进行INSERT、UPDATE、ANALYZE等操作。
   > PLAN_TABLE详细介绍见**PLAN_TABLE**。

   ```sql
   explain plan set statement_id='TPCH-Q4' for
   select
   o_orderpriority,
   count(*) as order_count
   from
   orders
   where
   o_orderdate >= '1993-07-01'::date
   and o_orderdate < '1993-07-01'::date + interval '3 month'
   and exists (
   select
   *
   from
   lineitem
   where
   l_orderkey = o_orderkey
   and l_commitdate < l_receiptdate
   )
   group by
   o_orderpriority
   order by
   o_orderpriority;
   ```

2. 查询PLAN_TABLE。

   ```sql
   SELECT * FROM PLAN_TABLE;
   ```

   ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/EXPLAIN-PLAN.png)

3. 清理PLAN_TABLE表中的数据。

   ```sql
   DELETE FROM PLAN_TABLE WHERE xxx;
   ```
