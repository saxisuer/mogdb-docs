---
title: DROP INDEX
summary: DROP INDEX
author: Zhang Cuiping
date: 2021-05-18
---

# DROP INDEX

## 功能描述

删除索引。

## 注意事项

只有索引的所有者或者拥有索引所在表的INDEX权限的用户有权限执行DROP INDEX命令，系统管理员默认拥有此权限。

## 语法格式

```sql
DROP INDEX [ CONCURRENTLY ] [ IF EXISTS ]
    index_name [, ...] [ CASCADE | RESTRICT ];
```

## 参数说明

- **CONCURRENTLY**

  以不加锁的方式删除索引。删除索引时，一般会阻塞其他语句对该索引所依赖表的访问。加此关键字，可实现删除过程中不做阻塞。

  此选项只能指定一个索引的名称， 并且CASCADE选项不支持。

  普通DROP INDEX命令可以在事务内执行，但是DROP INDEX CONCURRENTLY不可以在事务内执行。

- **IF EXISTS**

  如果指定的索引不存在，则发出一个notice而不是抛出一个错误。

- **index_name**

  要删除的索引名。

  取值范围: 已存在的索引。

- **CASCADE | RESTRICT**

  - CASCADE：表示允许级联删除依赖于该索引的对象。
  - RESTRICT（缺省值）：表示有依赖与此索引的对象存在，则该索引无法被删除。

## 示例

请参见CREATE INDEX的示例。
