---
title: ALTER RULE
summary: ALTER RULE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER RULE

## 功能描述

修改一个规则的定义。

## 注意事项

- 要使用ALTER RULE，用户必须是应用了指定规则的表或视图的所有者。
- 目前仅可以修改规则的名称。

## 语法格式

```sql
ALTER RULE name ON table_name RENAME TO new_name
```

## 参数说明

- **name**

  要修改规则的名称。

- **table_name**

  需要创建规则的表名称。

  取值范围: 数据库中已经存在的表名称。

- **new_name**

  规则的新名称。

## 示例

```sql
ALTER RULE notify_all ON emp RENAME TO notify_me;
```
