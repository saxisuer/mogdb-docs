---
title: DROP SEQUENCE
summary: DROP SEQUENCE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP SEQUENCE

## 功能描述

从当前数据库里删除序列。

## 注意事项

只有序列的所有者或者被授予了序列DROP权限的用户才能删除，系统管理员默认拥有该权限。

## 语法格式

```sql
DROP SEQUENCE [ IF EXISTS ] {[schema.]sequence_name} [ , ... ] [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的序列不存在，则发出一个notice而不是抛出一个错误。

- **name**

  序列名称。

- **CASCADE**

  级联删除依赖序列的对象。

- **RESTRICT**

  如果存在任何依赖的对象，则拒绝删除序列。此项是缺省值。

## 示例

```sql
--创建一个名为serial的递增序列，从101开始。
mogdb=# CREATE SEQUENCE serial START 101;

--删除序列。
mogdb=# DROP SEQUENCE serial;
```
