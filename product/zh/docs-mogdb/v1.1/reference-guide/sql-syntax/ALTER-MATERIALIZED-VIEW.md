---
title: ALTER MATERIALIZED VIEW
summary: ALTER MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER MATERIALIZED VIEW

## 功能描述

更改一个现有物化视图的多个辅助属性。

可用于ALTER MATERIALIZED VIEW的语句形式和动作是ALTER TABLE的一个子集，并且在用于物化视图时具有相同的含义。

## 注意事项

- 只有物化视图的所有者有权限执行ALTER TMATERIALIZED VIEW命令，系统管理员默认拥有此权限。
- 不支持更改物化视图结构。

## 语法格式

- 修改物化视图的定义。

  ```sql
  ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      action [, ... ];
  ```

- 修改物化视图的列。

  ```sql
  ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME [ COLUMN ] column_name TO new_column_name;
  ```

- 重命名物化视图。

  ```sql
  ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME TO new_name;
  ```

- 设置物化视图的所属模式。

  ```sql
  ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      SET SCHEMA new_schema;
  ```

- 设置物化视图的表空间。

  ```sql
  ALTER MATERIALIZED VIEW mv_name [ IF EXISTS ]
      SET TABLESPACE new_tablespace;
  ```

- 其中具体表操作action可以是以下子句之一：

  ```sql
  ALTER [ COLUMN ] column_name SET STATISTICS integer
  ALTER [ COLUMN ] column_name RESET ( attribute_option [, ... ] )
  ALTER [ COLUMN ] column_name SET ( attribute_option = value [, ... ] )
  ALTER [ COLUMN ] column_name SET STORAGE { PLAIN | EXTERNAL | EXTENDED | MAIN }
  CLUSTER ON index_name
  SET WITHOUT CLUSTER
  SET ( storage_parameter = value [, ... ] )
  OWNER TO { new_owner }
  RESET ( storage_parameter [, ... ] )
  ```

## 参数说明

- **mv_name**

  一个现有物化视图的名称，可以用模式修饰。

- **column_name**

  一个新的或者现有的。

- **new_column_name**

  一个现有列的新名称。

- **new_owner**

该物化视图的新拥有者的用户名。

- **new_name**

  该物化视图的新名称。

- **new_schema**

  该物化视图的新模式。

## 示例

```sql
--把物化视图foo重命名为bar。
mogdb=# ALTER MATERIALIZED VIEW foo RENAME TO bar;
```
