---
title: ALTER SYSTEM SET
summary: ALTER SYSTEM SET
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SYSTEM SET

## 功能描述

ALTER SYSTEM SET命令用于设置POSTMASTER、SIGHUP、BACKEND级别的GUC参数。此命令会将参数写入配置文件，不同级别生效方式有所不同。

## 注意事项

- 此命令仅拥有管理员权限的用户才可使用。
- 不同级别GUC参数生效时间如下：
  - POSTMASTER级别的GUC参数需要重启后才生效。
  - BACKEND级别的GUC参数需要会话重新连接后才生效。
  - SIGHUP级别的GUC参数立即生效（需要等待线程重新加载参数，实际略微有延迟）。
- 通过配置 audit_set_parameter参数，可以配置此操作是否被审计。
- 操作可被备机同步。
- 同gs_guc一致，并不关注数据库是主或备节点、是否只读。
- 不可在事务中执行，因为此操作无法被回滚。

## 语法格式

```sql
ALTER SYSTEM SET parameter TO value;
```

## 参数说明

- **parameter**

  GUC参数名。

- **value**

  GUC参数值。

## 示例

```sql
--设置SIGHUP级别参数audit_enabled。
mogdb=#  alter system set audit_enabled to off;
ALTER SYSTEM SET
mogdb=#  show audit_enabled;
 audit_enabled
---------------
 off
(1 row)

--设置POSTMASTER级别参数 enable_thread_pool，将在重启之后生效。
mogdb=# alter system set enable_thread_pool to on;
NOTICE:  please restart the database for the POSTMASTER level parameter to take effect.
ALTER SYSTEM SET
```
