---
title: CREATE SYNONYM
summary: CREATE SYNONYM
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SYNONYM

## 功能描述

创建一个同义词对象。同义词是数据库对象的别名，用于记录与其他数据库对象名间的映射关系，用户可以使用同义词访问关联的数据库对象。

## 注意事项

- 定义同义词的用户成为其所有者。
- 若指定模式名称，则同义词在指定模式中创建。否则，在当前模式创建。
- 支持通过同义词访问的数据库对象包括：表、视图、函数和存储过程。
- 使用同义词时，用户需要具有对关联对象的相应权限。
- 支持使用同义词的DML语句包括：SELECT、INSERT、UPDATE、DELETE、EXPLAIN、CALL。

## 语法格式

```sql
CREATE [ OR REPLACE ] SYNONYM synonym_name
    FOR object_name;
```

## 参数说明

- **synonym**

  创建的同义词名字，可以带模式名。

  取值范围: 字符串，要符合标识符的命名规范。

- **object_name**

  关联的对象名字，可以带模式名。

  取值范围: 字符串，要符合标识符的命名规范。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  > object_name可以是不存在的对象名称。

## 示例

```sql
--创建模式ot。
mogdb=# CREATE SCHEMA ot;

--创建表ot.t1及其同义词t1。
mogdb=# CREATE TABLE ot.t1(id int, name varchar2(10));
mogdb=# CREATE OR REPLACE SYNONYM t1 FOR ot.t1;

--使用同义词t1。
mogdb=# SELECT * FROM t1;
mogdb=# INSERT INTO t1 VALUES (1, 'ada'), (2, 'bob');
mogdb=# UPDATE t1 SET t1.name = 'cici' WHERE t1.id = 2;

--创建同义词v1及其关联视图ot.v_t1。
mogdb=# CREATE SYNONYM v1 FOR ot.v_t1;
mogdb=# CREATE VIEW ot.v_t1 AS SELECT * FROM ot.t1;

--使用同义词v1。
mogdb=# SELECT * FROM v1;

--创建重载函数ot.add及其同义词add。
mogdb=# CREATE OR REPLACE FUNCTION ot.add(a integer, b integer) RETURNS integer AS
$$
SELECT $1 + $2
$$
LANGUAGE sql;

mogdb=# CREATE OR REPLACE FUNCTION ot.add(a decimal(5,2), b decimal(5,2)) RETURNS decimal(5,2) AS
$$
SELECT $1 + $2
$$
LANGUAGE sql;

mogdb=# CREATE OR REPLACE SYNONYM add FOR ot.add;

--使用同义词add。
mogdb=# SELECT add(1,2);
mogdb=# SELECT add(1.2,2.3);

--创建存储过程ot.register及其同义词register。
mogdb=# CREATE PROCEDURE ot.register(n_id integer, n_name varchar2(10))
SECURITY INVOKER
AS
BEGIN
    INSERT INTO ot.t1 VALUES(n_id, n_name);
END;
/

mogdb=# CREATE OR REPLACE SYNONYM register FOR ot.register;

--使用同义词register，调用存储过程。
mogdb=# CALL register(3,'mia');

--删除同义词。
mogdb=# DROP SYNONYM t1;
mogdb=# DROP SYNONYM IF EXISTS v1;
mogdb=# DROP SYNONYM IF EXISTS add;
mogdb=# DROP SYNONYM register;
mogdb=# DROP SCHEMA ot CASCADE;
```
