---
title: CREATE DIRECTORY
summary: CREATE DIRECTORY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE DIRECTORY

## 功能描述

使用CREATE DIRECTORY语句创建一个目录对象，该目录对象定义了服务器文件系统上目录的别名，用于存放用户使用的数据文件。

## 注意事项

- 默认只允许初始化用户创建，如果开启enable_access_server_directory，sysadmin权限的用户也可以创建目录。
- 创建用户默认拥有此路径的READ和WRITE操作权限。
- 目录的默认owner为创建directory的用户。
- 以下路径禁止创建：
  - 路径含特殊字符。
  - 路径是相对路径。
  - 路径是符号连接。
- 创建目录时会进行以下合法性校验：
  - 创建时会检查添加路径是否为操作系统实际存在路径，如不存在会提示用户使用风险。
  - 创建时会校验数据库初始化（omm）用户对于添加路径的权限(即操作系统目录权限，读/写/执行 - R/W/X)，如果权限不全，会提示用户使用风险。
- 在openGauss环境下用户指定的路径需要用户保证各节点上路径的一致性，否则在不同节点上执行会产生找不到路径的问题。

## 语法格式

```sql
CREATE [OR REPLACE] DIRECTORY directory_name
AS 'path_name';
```

## 参数说明

- **directory_name**

  目录名称。

  取值范围: 字符串，要符合标识符的命名规范。

- **path_name**

  操作系统的路径。

  取值范围:  有效的操作系统路径。

## 示例

```sql
--创建目录。
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';
```
