---
title: CREATE CLIENT MASTER KEY
summary: CREATE CLIENT MASTER KEY
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE CLIENT MASTER KEY

## 功能描述

创建一个客户端加密主密钥(cmk)。

## 注意事项

需要在使用gsql连接数据库的时候添加-C参数，开启密态数据库开关，才可以生效。

在创建客户端加密主密钥(cmk)之前，需要使用KeyTool工具生成密钥。

## 语法格式

```sql
CREATE CLIENT MASTER KEY client_master_key_name WITH '(' master_key_params ')';
```

master_key_params：

```sql
KEY_STORE '=' key_store_value ',' KEY_PATH '=' key_path_value ',' ALGORITHM '=' algorithm_value
```

## 参数说明

- **client_master_key_name**

  同一命名空间下，客户端加密主密钥(cmk)名称，需要唯一，不可重复。

  取值范围: 字符串，要符合标识符的命名规范。

- **mater_key_params**

    指的是创建客户端加密主密钥时所涉及的参数信息，具体包括：

  - KEY_STORE，目前取值为gs_ktool。
  - KEY_PATH值为KeyTool工具生成密钥的ID，类似”gs_ktool/1”这种写法。
  - ALGORITHM为加密列加密密钥使用的算法，目前只支持AES_256_CBC。

## 示例

```sql
--创建dev_ce用户。
mogdb=# CREATE USER dev_ce PASSWORD 'dev@1234';
--连接密态数据库
gsql -p 57101 mogdb -U dev_ce -r -C
gsql ((MogDB Kernel 1.1.0 build 5be05d82) compiled at 2020-11-24 20:03:57 commit 1093 last mr 1793 debug)
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

mogdb=>
--使用Key_Tool工具创建一个密钥
mogdb=> \! gs_ktool -g
--创建客户端加密主密钥(CMK)
mogdb=> CREATE CLIENT MASTER KEY ImgCMK WITH ( KEY_STORE = gs_ktool , KEY_PATH = "gs_ktool/1" , ALGORITHM = AES_256_CBC);
```
