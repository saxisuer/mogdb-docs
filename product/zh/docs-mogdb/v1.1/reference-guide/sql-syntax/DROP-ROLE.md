---
title: DROP ROLE
summary: DROP ROLE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP ROLE

## 功能描述

删除指定的角色。

## 注意事项

无。

## 语法格式

```sql
DROP ROLE [ IF EXISTS ] role_name [, ...];
```

## 参数说明

- **IF EXISTS**

  如果指定的角色不存在，则发出一个notice而不是抛出一个错误。

- **role_name**

  要删除的角色名称。

  取值范围: 已存在的角色。

## 示例

请参见CREATE ROLE的示例。
