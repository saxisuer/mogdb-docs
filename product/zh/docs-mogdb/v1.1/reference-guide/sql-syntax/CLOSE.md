---
title: CLOSE
summary: CLOSE
author: Zhang Cuiping
date: 2021-05-10
---

# CLOSE

## 功能描述

CLOSE释放和一个游标关联的所有资源。

## 注意事项

- 不允许对一个已关闭的游标再做任何操作。
- 一个不再使用的游标应该尽早关闭。
- 当创建游标的事务用COMMIT或ROLLBACK终止之后，每个不可保持的已打开游标都隐含关闭。
- 当创建游标的事务通过ROLLBACK退出之后，每个可以保持的游标都将隐含关闭。
- 当创建游标的事务成功提交，可保持的游标将保持打开，直到执行一个明确的CLOSE或者客户端断开。
- MogDB没有明确打开游标的OPEN语句，因为一个游标在使用CURSOR命令定义的时候就打开了。可以通过查询系统视图pg_cursors看到所有可用的游标。

## 语法格式

```sql
CLOSE { cursor_name | ALL } ;
```

## 参数说明

- **cursor_name**

  一个待关闭的游标名称。

- **ALL**

  关闭所有已打开的游标。

## 示例

请参考FETCH的示例。
