---
title: 发送端服务器
summary: 发送端服务器
author: Zhang Cuiping
date: 2021-04-20
---

# 发送端服务器

## max_wal_senders

**参数说明**: 指定事务日志发送进程的并发连接最大数量。不可大于等于max_connections。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:** wal_level必须设置为archive或者hot_standby以允许备机的连接。

**取值范围**: 整型，0～262143

**默认值**: 16

## wal_keep_segments

**参数说明**: Xlog日志文件段数量。设置"pg_xlog"目录下保留事务日志文件的最小数目，备机通过获取主机的日志进行流复制。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，2 ~ INT_MAX

**默认值**: 16

**设置建议:**

- 当服务器开启日志归档或者从检查点恢复时，保留的日志文件数量可能大于wal_keep_segments设定的值。
- 如果此参数设置过小，则在备机请求事务日志时，此事务日志可能已经被产生的新事务日志覆盖，导致请求失败，主备关系断开。
- 当双机为异步传输时，以COPY方式连续导入4G以上数据需要增大wal_keep_segments配置。以T6000单板为例，如果导入数据量为50G，建议调整参数为1000。您可以在导入完成并且日志同步正常后，动态恢复此参数设置。

## wal_sender_timeout

**参数说明**: 设置本端等待事务日志接收端接收日志的最大等待时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 如果主机数据较大，重建备机数据库时需要增大此参数的值，主机数据在 500G时，此参数的参考值为 600s。
> - 此值不能大于wal_receiver_timeout或数据库重建时的超时参数。

**取值范围**: 整型， 0 ~ INT_MAX，单位为毫秒（ms）。

**默认值**: 6s

## max_replication_slots

**参数说明**: 设置主机端的日志复制slot个数。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型， 0~262143。

**默认值**: 8

**设置建议**:

使用逻辑解码时，该参数值建议设为: 当前物理流复制槽数+所需的逻辑复制槽数。

- 物理流复制槽提供了一种自动化的方法来确保主节点在所有备节点或从备节点收到xlog之前，xlog不会被移除。也就是说物理流复制槽用于支撑主备HA。数据库所需要的物理流复制槽数为备节点加从备的和与主节点之间的比例。例如，假设数据库高可用方案为1主、1备、1从备，则所需物理流复制槽数为2。假设数据库的高可用方案为1主3备，则所需物理流复制槽数为3。
- 关于逻辑复制槽数，请按如下规则考虑:
  - 一个逻辑复制槽只能解码一个数据库的修改，如果需要解码多个数据库，则需要创建多个逻辑复制槽。
  - 如果需要多路逻辑复制同步给多个目标数据库，在源端数据库需要创建多个逻辑复制槽，每个逻辑复制槽对应一条逻辑复制链路。

## enable_slot_log

**参数说明**: 是否开启逻辑复制槽主备同步特性。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启逻辑复制槽主备同步特性。
- off表示不开启逻辑复制槽主备同步特性。

**默认值**: off

## max_changes_in_memory

**参数说明**: 逻辑解码时单条事务在内存中缓存的大小上限，单位字节。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~2147483647

**默认值**: 4096

## max_cached_tuplebufs

**参数说明**: 逻辑解码时总元组信息在内存中缓存的大小上限，单位字节。建议设置为max_changes_in_memory的两倍以上。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~2147483647

**默认值**: 8192

## replconninfo1

**参数说明**: 设置本端侦听和鉴权的第一个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第一个节点信息。

**默认值**: 空字符串

## replconninfo2

**参数说明**: 设置本端侦听和鉴权的第二个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第二个节点信息。

**默认值**: 空字符串

## replconninfo3

**参数说明**: 设置本端侦听和鉴权的第三个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第三个节点信息。

**默认值**: 空字符串

## replconninfo4

**参数说明**: 设置本端侦听和鉴权的第四个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第四个节点信息。

**默认值**: 空字符串

## replconninfo5

**参数说明**: 设置本端侦听和鉴权的第五个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第五个节点信息。

**默认值**: 空字符串

## replconninfo6

**参数说明**: 设置本端侦听和鉴权的第六个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第六个节点信息。

**默认值**: 空字符串

## replconninfo7

**参数说明**: 设置本端侦听和鉴权的第七个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第七个节点信息。

**默认值**: 空字符串

## replconninfo8

**参数说明**: 设置本端侦听和鉴权的第八个节点信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置第八个节点信息。

**默认值**: 空字符串

## available_zone

**参数说明**: 设置本端节点所在区域信息。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。其中空字符串表示没有配置节点信息。

**默认值**: 空字符串
