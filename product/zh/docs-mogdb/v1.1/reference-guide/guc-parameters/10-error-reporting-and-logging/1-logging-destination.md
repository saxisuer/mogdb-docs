---
title: 记录日志的位置
summary: 记录日志的位置
author: Zhang Cuiping
date: 2021-04-20
---

# 记录日志的位置

## log_destination

**参数说明**: MogDB支持多种方法记录服务器日志，log_destination的取值为一个逗号分隔开的列表（如log_destination="stderr,csvlog"）。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

有效值为stderr、csvlog、syslog。

- 取值为stderr，表示日志打印到屏幕。

- 取值为csvlog，表示日志的输出格式为"逗号分隔值"即CSV （Comma Separated Value）格式。使用csvlog记录日志的前提是将[logging_collector](#logging_collector)设置为on，请参见**使用CSV格式写日志**。

- 取值为syslog，表示通过操作系统的syslog记录日志。MogDB使用syslog的LOCAL0 ～ LOCAL7记录日志，请参见[syslog_facility](#syslog_facility)。使用syslog记录日志需在操作系统后台服务配置文件中添加代码:

  ```
  local0.*  /var/log/postgresql
  ```

**默认值**: stderr

## logging_collector

**参数说明**: 控制开启后端日志收集进程logger进行日志收集。该进程捕获发送到stderr或csvlog的日志消息并写入日志文件。

这种记录日志的方法比将日志记录到syslog更加有效，因为某些类型的消息在syslog的输出中无法显示。例如动态链接库加载失败消息和脚本（例如archive_command）产生的错误消息。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 将服务器日志发送到stderr时可以不使用logging_collector参数，此时日志消息会被发送到服务器的stderr指向的空间。这种方法的缺点是日志回滚困难，只适用于较小的日志容量。

**取值范围**: 布尔型

- on表示开启日志收集功能。
- off表示关闭日志收集功能。

**默认值**: off

## log_directory

**参数说明**: logging_collector设置为on时，log_directory决定存放服务器日志文件的目录。 它可以是绝对路径，或者是相对路径（相对于数据目录的路径）。log_directory支持动态修改，可以通过gs_guc reload实现。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 当配置文件中log_directory的值为非法路径时，会导致MogDB无法重新启动。
> - 通过gs_guc reload动态修改log_directory时，当指定路径为合法路径时，日志输出到新的路径下。当指定路径为非法路径时，日志输出到上一次合法的日志输出路径下而不影响数据库正常运行。此时即使指定的log_directory的值非法，也会写入到配置文件中。
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 合法路径: 用户对此路径有读写权限
> 非法路径: 用户对此路径无读写权限

**取值范围**: 字符串

**默认值**: 安装时指定。

## log_filename

**参数说明**: logging_collector设置为on时，log_filename决定服务器运行日志文件的名称。通常日志文件名是按照strftime模式生成，因此可以用系统时间定义日志文件名，用%转义字符实现。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 建议使用%转义字符定义日志文件名称，否则难以对日志文件进行有效的管理。
> - 当log_destination设为csvlog时，系统会生成附加了时间戳的日志文件名，文件格式为csv格式，例如"server_log.1093827753.csv"。

**取值范围**: 字符串

**默认值**: postgresql-%Y-%m-%d_%H%M%S.log

## log_file_mode

**参数说明:** [logging_collector](#logging_collector)设置为on时，log_file_mode设置服务器日志文件的权限。通常log_file_mode的取值是能够被chmod和umask系统调用接受的数字。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 使用此选项前请设置log_directory，将日志存储到数据目录之外的地方。
> - 因日志文件可能含有敏感数据，故不能将其设为对外可读。

**取值范围**: 整型，0000～0777 （8进制计数，转化为十进制 0 ~ 511）。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 0600表示只允许服务器管理员读写日志文件。
> - 0640表示允许管理员所在用户组成员只能读日志文件。

**默认值**: 0600

## log_truncate_on_rotation

**参数说明**: logging_collector设置为on时，log_truncate_on_rotation设置日志消息的写入方式。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

示例如下:

假设日志需要保留7天，每天生成一个日志文件，日志文件名设置为server_log.Mon、server_log.Tue等。第二周的周二生成的日志消息会覆盖写入到server_log.Tue。设置方法: 将log_filename设置为server_log.%a ，log_truncate_on_rotation设置为on，log_rotation_age设置为1440，即日志有效时间为1天。

**取值范围**: 布尔型

- on表示MogDB以覆盖写入的方式写服务器日志消息。
- off表示MogDB将日志消息附加到同名的现有日志文件上。

**默认值**: off

## log_rotation_age

**参数说明**: logging_collector设置为on时，log_rotation_age决定创建一个新日志文件的时间间隔。当现在的时间减去上次创建一个服务器日志的时间超过了log_rotation_age的值时，将生成一个新的日志文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ 35791394，单位为min。其中0表示关闭基于时间的新日志文件的创建。

**默认值**: 1440(min)

## log_rotation_size

**参数说明**: logging_collector设置为on时，log_rotation_size决定服务器日志文件的最大容量。当日志消息的总量超过日志文件容量时，服务器将生成一个新的日志文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ INT_MAX / 1024，单位为kB。

0表示关闭基于容量的新日志文件的创建。

建议该值大小设置级别至少为MB级,利于日志文件的及时划分。

**默认值**: 20MB

## syslog_facility

**参数说明**: log_destination设置为syslog时，syslog_facility配置使用syslog记录日志的"设备"。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型，有效值有local0、local1、local2、local3、local4、local5、local6、local7。

**默认值**: local0

## syslog_ident

**参数说明:** [log_destination](#log_destination)设置为syslog时，syslog_ident设置在syslog日志中MogDB日志消息的标识。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: postgres

## event_source

**参数说明**: 该参数仅在windows环境下生效, MogDB暂不支持。log_destination设置为eventlog时，event_source设置在日志中MogDB日志消息的标识。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: PostgreSQL
