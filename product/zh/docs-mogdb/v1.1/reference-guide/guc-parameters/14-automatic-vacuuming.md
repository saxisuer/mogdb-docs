---
title: 自动清理
summary: 自动清理
author: Zhang Cuiping
date: 2021-04-20
---

# 自动清理

系统自动清理进程（autovacuum）自动执行VACUUM和ANALYZE命令，回收被标识为删除状态的记录空间，并更新表的统计数据。

## autovacuum

**参数说明**: 控制数据库自动清理进程（autovacuum）的启动。自动清理进程运行的前提是将track_counts设置为on。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 如果希望系统在故障恢复后，具备自动清理两阶段事务的功能，请将autovacuum设置为on;
> - 当设置autovacuum为on，[autovacuum_max_workers](#autovacuum_max_workers)为0时，表示系统不会自动进行autovacuum，只会在故障恢复后，自动清理两阶段事务；
> - 当设置autovacuum为on，[autovacuum_max_workers](#autovacuum_max_workers)大于0时，表示系统不仅在故障恢复后，自动清理两阶段事务，并且还可以自动清理进程。
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 即使此参数设置为off，当事务ID回绕即将发生时，数据库也会自动启动自动清理进程。对于create/drop database发生异常时，可能有的节点提交或回滚，有的节点未提交（prepared状态），此时系统不能自动修复，需要手动修复。

**取值范围**: 布尔型

- on表示开启数据库自动清理进程。
- off表示关闭数据库自动清理进程。

**默认值**: on

## autovacuum_mode

**参数说明**: 该参数仅在autovacuum设置为on的场景下生效，它控制autoanalyze或autovacuum的打开情况。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- analyze表示只做autoanalyze。
- vacuum表示只做autovacuum。
- mix表示autoanalyze和autovacuum都做。
- none表示二者都不做。

**默认值**: mix

## autoanalyze_timeout

**参数说明**: 设置autoanalyze的超时时间。在对某张表做autoanalyze时，如果该表的analyze时长超过了autoanalyze_timeout，则自动取消该表此次analyze。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: int类型，单位是s，0~2147483。

**默认值**: 5min（即300s）

## autovacuum_io_limits

**参数说明**: 控制autovacuum进程每秒触发IO的上限。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～1073741823和-1。其中-1表示不控制，而是使用系统默认控制组。

**默认值**: -1

## log_autovacuum_min_duration

**参数说明**: 当自动清理的执行时间大于或者等于某个特定的值时，向服务器日志中记录自动清理执行的每一步操作。设置此选项有助于追踪自动清理的行为。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

举例如下:

将log_autovacuum_min_duration设置为250ms，记录所有运行大于或者等于250ms的自动清理命令的相关信息。

**取值范围**: 整型，最小值为-1，最大值为2147483647，单位为毫秒。

- 当参数设置为0时，表示所有的自动清理操作都记录到日志中。
- 当参数设置为-1时，表示所有的自动清理操作都不记录到日志中。
- 当参数设置为非-1时，当由于锁冲突的存在导致一个自动清理操作被跳过，记录一条消息。

**默认值**: -1

## autovacuum_max_workers

**参数说明**: 设置能同时运行的自动清理线程的最大数量，该参数的取值上限与GUC参数max_connections和job_queue_processes大小有关。

该参数属于POSTMASTER类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0（表示不会自动进行autovacuum），理论最大值为262143，实际最大值为动态值，计算公式为"262143 - max_connections - job_queue_processes - 辅助线程数 - autovacuum的lancher线程数 - 1"，其中辅助线程数和autovacuum的lancher线程数由两个宏来指定，当前版本的默认值分别为20和2。

**默认值**: 3

## autovacuum_naptime

**参数说明**: 设置两次自动清理操作的时间间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位为s，最小值为1，最大值为2147483。

**默认值**: 10min（即600s）

## autovacuum_vacuum_threshold

**参数说明**: 设置触发VACUUM的阈值。当表上被删除或更新的记录数超过设定的阈值时才会对这个表执行VACUUM操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为2147483647。

**默认值**: 50

## autovacuum_analyze_threshold

**参数说明**: 设置触发ANALYZE操作的阈值。当表上被删除、插入或更新的记录数超过设定的阈值时才会对这个表执行ANALYZE操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为2147483647。

**默认值**: 50

## autovacuum_vacuum_scale_factor

**参数说明**: 设置触发一个VACUUM时增加到autovacuum_vacuum_threshold的表大小的缩放系数。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.0～100.0

**默认值**: 0.2

## autovacuum_analyze_scale_factor

**参数说明**: 设置触发一个ANALYZE时增加到autovacuum_analyze_threshold的表大小的缩放系数。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.0～100.0

**默认值**: 0.1

## autovacuum_freeze_max_age

**参数说明**: 设置事务内的最大时间，使得表的pg_class.relfrozenxid字段在VACUUM操作执行之前被写入。

- VACUUM也可以删除pg_clog/子目录中的旧文件。
- 即使自动清理进程被禁止，系统也会调用自动清理进程来防止循环重复。

该参数属于POSTMASTER类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围: 长**整型，100 000～576 460 752 303 423 487

**默认值**: 20000000000

## autovacuum_vacuum_cost_delay

**参数说明**: 设置在自动VACUUM操作里使用的开销延迟数值。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，-1～100，单位为毫秒（ms）。其中-1表示使用常规的vacuum_cost_delay。

**默认值**: 20ms

## autovacuum_vacuum_cost_limit

**参数说明**: 设置在自动VACUUM操作里使用的开销限制数值。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，-1～10000。其中-1表示使用常规的vacuum_cost_limit。

**默认值**: -1

## defer_csn_cleanup_time

**参数说明**: 用来指定本地回收时间间隔，单位为毫秒（ms）。

**取值范围**: 整型，0~INT_MAX。

**默认值**: 5s（即5000ms）
