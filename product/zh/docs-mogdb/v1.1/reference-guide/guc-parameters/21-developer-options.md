---
title: 开发人员选项
summary: 开发人员选项
author: Zhang Cuiping
date: 2021-04-20
---

# 开发人员选项

## allow_system_table_mods

**参数说明**: 设置是否允许修改系统表的结构。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

不建议修改该参数默认值，如果设置为on，可能导致系统表损坏，甚至数据库无法启动。

**取值范围**: 布尔型

- on表示允许修改系统表的结构。
- off表示不允许修改系统表的结构。

**默认值**: off

## debug_assertions

**参数说明**: 控制打开各种断言检查。能够协助调试，当遇到奇怪的问题或者崩溃，请把此参数打开，因为它能暴露编程的错误。要使用这个参数，必须在编译MogDB的时候定义宏USE_ASSERT_CHECKING（通过configure选项 -enable-cassert完成）。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开断言检查。
- off表示不打开断言检查。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> 当启用断言选项编译MogDB时，debug_assertions缺省值为on 。

**默认值**: off

## ignore_checksum_failure

**参数说明**: 设置读取数据时是否忽略校验信息检查失败（但仍然会告警），继续执行可能导致崩溃，传播或隐藏损坏数据，无法从远程节点恢复数据及其他严重问题。不建议用户修改设置。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示忽略数据校验错误。
- off表示数据校验错误正常报错。

**默认值**: off

## enable_force_vector_engine

**参数说明**: 对于支持向量化的执行器算子，如果其子节点是非向量化的算子，通过设置此参数为on，强制生成向量化的执行计划。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示可以向量化的算子强制生成向量化。
- off表示由向量算子优化器决定是否向量化。

**默认值**: off

## explain_dna_file

**参数说明**: 指定[explain_perf_mode](#explain_perf_mode)为run，导出的csv信息的目标文件。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 这个参数的取值必须是绝对路径加上.csv格式的文件名。

**取值范围**: 字符串

**默认值**: 空

## explain_perf_mode

**参数说明**: 此参数用来指定explain的显示格式。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: normal、pretty、summary、run

- normal: 代表使用默认的打印格式。
- pretty: 代表使用MogDB改进后的新显示格式。新的格式层次清晰，计划包含了plan node id，性能分析简单直接。
- summary: 是在pretty的基础上增加了对打印信息的分析。
- run: 在summary的基础上，将统计的信息输出到csv格式的文件中，以便于进一步分析。

**默认值**: normal

## ignore_system_indexes

**参数说明**: 读取系统表时忽略系统索引（但是修改系统表时依然同时修改索引）。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 这个参数在从系统索引被破坏的表中恢复数据的时候非常有用。

**取值范围**: 布尔型

- on表示忽略系统索引。
- off表示不忽略系统索引。

**默认值**: off

## post_auth_delay

**参数说明**: 在认证成功后，延迟指定时间，启动服务器连接。允许调试器附加到启动进程上。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为2147，单位为秒。

**默认值**: 0

## pre_auth_delay

**参数说明**: 启动服务器连接后，延迟指定时间，进行认证。允许调试器附加到认证过程上。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0～60，单位为秒。

**默认值**: 0

## trace_notify

**参数说明**: 为LISTEN和NOTIFY命令生成大量调试输出。client_min_messages或log_min_messages级别必须是DEBUG1或者更低时，才能把这些输出分别发送到客户端或者服务器日志。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开输出功能。
- off表示关闭输出功能。

**默认值**: off

## trace_recovery_messages

**参数说明**: 启用恢复相关调试输出的日志录，否则将不会被记录。该参数允许覆盖正常设置的log_min_messages，但是仅限于特定的消息，这是为了在调试备机中使用。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型，有效值有debug5、debug4、debug3、debug2、debug1、log，取值的详细信息请参见log_min_messages。

**默认值**: log

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 默认值log表示不影响记录决策。
> - 除默认值外，其他值会导致优先级更高的恢复相关调试信息被记录，因为它们有log优先权。对于常见的log_min_messages设置，这会导致无条件地将它们记录到服务器日志上。

## trace_sort

**参数说明**: 控制是否在日志中打印排序操作中的资源使用相关信息。这个选项只有在编译MogDB的时候定义了TRACE_SORT宏的时候才可用，不过目前TRACE_SORT是由缺省定义的。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开控制功能。
- off表示关闭控制功能。

**默认值**: off

## zero_damaged_pages

**参数说明**: 控制检测导致MogDB报告错误的损坏的页头，终止当前事务。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

设置为on时，会导致系统报告一个警告，把损坏的页面填充为零然后继续处理。这种行为会破坏数据，也就是所有在已经损坏页面上的行记录。但是它允许绕开坏页面然后从表中尚存的未损坏页面上继续检索数据行。因此它在因为硬件或者软件错误导致的崩溃中进行恢复是很有用的。通常不应该把它设置为on，除非不需要从崩溃的页面中恢复数据。

**默认值**: off

## string_hash_compatible

**参数说明**: 是否使用相同的方法计算char类型哈希值和varchar或text类型哈希值。根据此参数的设置，可以确定在将分布列从char类型的数据分布转换为varchar或text类型的数据分布时，是否需要重新分布。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on 表示使用相同的计算方法且不需要重新分布。
- off 表示使用不同的计算方法且需要重新分布。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> 计算方法的不同之处在于用于计算哈希值的输入字符串的长度。（对于char类型的哈希值，字符串后面的空格不作为长度。对于text或varchar类型的哈希值，将计算空格。）哈希值会影响查询的计算结果。设置完毕后，运行数据库时请不要修改该参数，以免查询错误。

**默认值**:**off**

## cost_param

**参数说明**: 该参数用于控制在特定的客户场景中，使用不同的估算方法使得估算值与真实值更接近。此参数可以同时控制多种方法，与某一方法对应的位做与操作，不为0表示该方法被选择。

当cost_param & 1 不为0，表示对于求不等值连接选择率时选择一种改良机制，此方法在自连接（两个相同的表之间连接）的估算中更加准确。目前，已弃用cost_param & 1 不为0时的路径，默认选择更优的估算公式；

当cost_param & 2 不为0，表示求多个过滤条件（Filter）的选择率时，选择最小的作为总的选择率，而非两者乘积，此方法在过滤条件的列之间关联性较强时估算更加准确；

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～INT_MAX

**默认值**: 0

## convert_string_to_digit

**参数说明**: 设置隐式转换优先级，是否优先将字符串转为数字。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示优先将字符串转为数字。
- off表示不优先将字符串转为数字。

**默认值**: on

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 请谨慎调整该参数，调整该参数会修改内部数据类型转换规则并可能导致不可预期的行为。

## nls_timestamp_format

**参数说明**: 设置时间戳默认格式。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: DD-Mon-YYYY HH:MI:SS.FF AM

## remotetype

**参数说明**: 设置远程连接类型。

该参数不支持修改。

**取值范围**: 枚举类型，有效值有application，datanode，internaltool。

**默认值**: application

## enable_partitionwise

**参数说明**: 分区表连接操作是否选择智能算法。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示选择智能算法。
- off表示不选择智能算法。

**默认值**: off

## max_function_args

**参数说明**: 函数参数最大个数。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 整型

**默认值**: 666

## max_user_defined_exception

**参数说明**: 异常最大个数，默认值不可更改。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，当前只能取固定值1000

**默认值**: 1000

## enable_debug_vacuum

**参数说明**: 允许输出一些与VACUUM相关的日志，便于定位VACUUM相关问题。开发人员专用，不建议普通用户使用。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启此日志开关。
- off/false表示关闭此日志开关。

**默认值**: off

## enable_global_stats

**参数说明**: 标识当前统计信息模式，区别采用全局统计信息收集模式还是单节点统计信息收集模式，默认创建为采用全局统计信息模式。当关闭该参数时，则默认收集MogDB第一个节点的统计信息，此时可能会影响生成查询计划的质量，但信息收集性能较优，建议客户谨慎考虑。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示全局统计信息。
- off/false表示数据库节点统计信息。

**默认值**: on

## enable_fast_numeric

**参数说明**: 标识是否开启Numeric类型数据运算优化。Numeric数据运算是较为耗时的操作之一，通过将Numeric转化为int64/int128类型，提高Numeric运算的性能。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启Numeric优化。
- off/false表示关闭Numeric优化。

**默认值**: on

## rewrite_rule

**参数说明**: 标识开启的可选查询重写规则。有部分查询重写规则是可选的，开启它们并不能总是对查询效率有提升效果。在特定的客户场景中，通过此GUC参数对查询重写规则进行设置，使得查询效率最优。

此参数可以控制查询重写规则的组合，比如有多个重写规则: rule1、rule2、rule3、rule4。可以设置:

```sql
set rewrite_rule=rule1;          --启用查询重写规则rule1
set rewrite_rule=rule2,rule3;    --启用查询重写规则rule2和rule3
set rewrite_rule=none;           --关闭所有可选查询重写规则
```

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- none: 不使用任何可选查询重写规则
- lazyagg: 使用Lazy Agg查询重写规则（消除子查询中的聚集运算）

**默认值**: magicset

## enable_compress_spill

**参数说明**: 标识是否开启下盘压缩功能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启下盘优化。
- off/false表示关闭下盘优化。

**默认值**: on

## analysis_options

**参数说明**: 通过开启对应选项中所对应的功能选项使用相应的定位功能，包括数据校验，性能统计等，参见取值范围中的选项说明。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- LLVM_COMPILE表示在explain performance显示界面中显示每个线程的codegen编译时间。
- HASH_CONFLICT表示在数据库节点进程的pg_log目录中的log日志中显示hash表的统计信息，包括hash表大小，hash链长，hash冲突情况。
- STREAM_DATA_CHECK表示对网络传输前后的数据进行CRC校验。

**默认值**: ALL,on(),off(LLVM_COMPILE,HASH_CONFLICT,STREAM_DATA_CHECK)，不开启任何定位功能。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:** 设置时，选择开启或者关闭的选项请使用'on()'或'off()'包括，未被显示指定的功能选项会维持原来的值。参考格式:  'on(option1, option2, …)' 'off(ALL)'

## resource_track_log

**参数说明**: 控制自诊断的日志级别。目前仅对多列统计信息进行控制。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- summary: 显示简略的诊断信息。
- detail: 显示详细的诊断信息。

目前这两个参数值只在显示多列统计信息未收集的告警的情况下有差别，summary不显示未收集多列统计信息的告警，detail会显示这类告警。

**默认值**: summary

## udf_memory_limit

**参数说明**: 控制每个数据库节点执行UDF时可用的最大物理内存量。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，200*1024～max_process_memory，单位为KB。

**默认值**: 200MB

## FencedUDFMemoryLimit

**参数说明**: 控制每个fenced udf worker进程使用的虚拟内存。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**设置建议:**不建议设置此参数，可用[udf_memory_limit](#udf_memory_limit)代替。

**取值范围**: 整数，0 ~ 2147483647，单位为KB，设置可带单位（KB，MB，GB）。其中0表示不做内存控制。

**默认值**: 0

## UDFWorkerMemHardLimit

**参数说明**: 控制fencedUDFMemoryLimit的最大值。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**设置建议:**不建议设置此参数，可用[udf_memory_limit](#udf_memory_limit)代替。

**取值范围**: 整数，0 ~ 2147483647，单位为KB，设置时可带单位（KB，MB，GB）。

**默认值**: 1GB

## pljava_vmoptions

**参数说明**: 用户自定义设置PL/Java函数所使用的JVM虚拟机的启动参数。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串， 支持:

- JDK8 JVM启动参数（可参见JDK官方说明）
- JDK8 JVM系统属性参数（以-D开头如-Djava.ext.dirs，可参见JDK官方说明）
- 用户自定义参数（以-D开头，如-Duser.defined.option）

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:** 如果用户在pljava_vmoptions中设置参数不满足上述取值范围，会在使用PL/Java语言函数时报错。此参数的详细说明参见**PL/pgSQL语言函数**。

**默认值**: 空

## enable_pbe_optimization

**参数说明**: 设置优化器是否对以PBE（Parse Bind Execute）形式执行的语句进行查询计划的优化。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型。

- on表示优化器将优化PBE语句的查询计划。
- off表示不使用优化。

**默认值**: on

## enable_light_proxy

**参数说明**: 设置优化器是否对简单查询在数据库主节点上优化执行，应用端和内核端字符集不匹配时，该参数不生效，建议建库时将字符集设为UTF8。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型。

- on表示优化器将优化数据库主节点上简单查询的执行。
- off表示不使用优化。

**默认值**: on

## enable_global_plancache

**参数说明**: 设置是否对PBE查询和存储过程的执行计划进行缓存共享，开启该功能可以节省高并发下数据库节点的内存使用。

在打开enable_global_plancache的情况下，为保证GPC生效，默认local_syscache_threshold不小于16MB。即如当前local_syscache_threshold小于16MB，则设置为16MB，如大于16MB，则不改变。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型。

- on表示对PBE查询和存储过程的执行计划进行缓存共享。
- off表示不共享。

**默认值**: off

## checkpoint_flush_after

**参数说明**: 设置checkpointer线程在连续写多少个磁盘页后会进行异步刷盘操作。MogDB中，磁盘页大小为8KB。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256 （0表示关闭异步刷盘功能）。例如，取值32，表示checkpointer线程连续写32个磁盘页，即32*8=256KB磁盘空间后会进行异步刷盘。

**默认值**: 32

## bgwriter_flush_after

**参数说明**: 设置background writer线程在连续写多少个磁盘页后会进行异步刷盘操作。MogDB中，磁盘页大小为8KB。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256 （0表示关闭异步刷盘功能），单位页面（8K）。例如，取值64，表示background writer线程连续写64个磁盘页，即64*8=512KB磁盘空间后会进行异步刷盘。

**默认值**: 512KB（即64个页面）

## backend_flush_after

**参数说明**: 设置backend线程在连续写多少个磁盘页后会产生异步刷盘操作。MogDB中，磁盘页大小为8KB。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~256 （0表示关闭异步刷盘功能），单位页面（8K）。例如，取值64，表示backend线程连续写64个磁盘页，即64*8=512KB磁盘空间后会进行异步刷盘。

**默认值**: 0

## enable_parallel_ddl

**参数说明**: 是否多个数据库节点可以同步对同一个数据库对象执行DDL操作。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on 表示DDL操作可以同步执行，且不会出现分布式死锁。
- off 表示同步执行DDL操作时可能会出现分布式死锁。

**默认值**: on

## show_acce_estimate_detail

**参数说明**: 评估信息一般用于运维人员在维护工作中使用，因此该参数默认关闭，此外为了避免这些信息干扰正常的explain信息显示，只有在explain命令的verbose选项打开的情况下才显示评估信息

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示可以在explain命令的输出中显示评估信息。
- off表示不在explain命令的输出中显示评估信息。

**默认值**: off

## enable_prevent_job_task_startup

**参数说明**: 控制是否启动job线程。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示不能启动job线程。
- off表示可以启动job线程。

**默认值**: off

## enable_early_free

**参数说明**: 控制是否可以实现算子内存的提前释放。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示支持算子内存提前释放。
- off表示不支持算子内存提前释放。

**默认值**: on

## support_batch_bind

**参数说明**: 控制是否允许通过JDBC、ODBC、Libpq等接口批量绑定和执行PBE形式的语句。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用批量绑定和执行。
- off表示不使用批量绑定和执行。

**默认值**: on

## check_implicit_conversions

**参数说明**: 控制是否对查询中有隐式类型转换的索引列是否会生成候选索引路径进行检查。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示对查询中有隐式类型转换的索引列是否会生成候选索引路径进行检查。
- off表示不进行相关检查。

**默认值**: off

## enable_thread_pool

**参数说明**: 控制是否使用线程池功能。该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启线程池功能。
- off表示不开启线程池功能。

**默认值**: off

## thread_pool_attr

**参数说明**: 用于控制线程池功能的详细属性，该参数仅在enable_thread_pool打开后生效。该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，长度大于0

该参数分为3个部分，'thread_num, group_num, cpubind_info'，这3个部分的具体含义如下:

- thread_num: 线程池中的线程总数，取值范围是0~4096。其中0的含义是数据库根据系统CPU core的数量来自动配置线程池的线程数，如果参数值大于0，线程池中的线程数等于thread_num。
- group_num: 线程池中的线程分组个数，取值范围是0~64。其中0的含义是数据库根据系统NUMA组的个数来自动配置线程池的线程分组个数，如果参数值大于0，线程池中的线程组个数等于group_num。
- cpubind_info: 线程池是否绑核的配置参数。可选择的配置方式有集中: 1. '(nobind)' ，线程不做绑核；2. '(allbind)'，利用当前系统所有能查询到的CPU core做线程绑核；3. '(nodebind: 1, 2)'，利用NUMA组1,2中的CPU core进行绑核；4. '(cpubind: 0-30)'，利用0-30号CPU core进行绑核。该参数不区分大小写。

**默认值**: '16, 2, (nobind)'

## numa_distribute_mode

**参数说明**: 用于控制部分共享数据和线程在NUMA节点间分布的属性。用于大型多NUMA节点的ARM服务器性能调优，一般不用设置。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，当前有效取值为'none', 'all'。

- none:  表示不启用本特性。
- all: 表示将部分共享数据和线程分布到不同的NUMA节点下，减少远端访存次数，提高性能。目前仅适用于拥有多个NUMA节点的ARM服务器，并且要求全部NUMA节点都可用于数据库进程，不支持仅选择一部分NUMA节点。

**默认值**: 'none'

## log_pagewriter

**参数说明**: 设置用于增量检查点打开后，显示线程的刷页信息以及增量检查点的详细信息，信息比较多，不建议设置为true。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**: off

## enable_opfusion

**参数说明**: 控制是否对简单增删改查进行优化。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

简单查询限制如下:

- 只支持indexscan和indexonlyscan，且全部WHERE语句的过滤条件都在索引上。

- 只支持单表增删改查，不支持join、using。

- 只支持行存表，不支持分区表，表不支持有触发器。

- 不支持active sql、QPS等信息统计特性。

- 不支持正在扩容和缩容的表。

- 不支持查询或者修改系统列。

- 只支持简单SELECT语句，例如:

  ```sql
  SELECT c3 FROM t1 WHERE c1 = ? and c2 =10;
  ```

  仅可以查询目标表的列，c1和c2列为索引列，后边可以是常量或者参数，可以使用 for update。

- 只支持简单INSERT语句，例如:

  ```sql
  INSERT INTO t1 VALUES (?,10,?);
  ```

  仅支持一个VALUES，VALUES里面的类型可以是常量和参数，不支持returning。

- 只支持简单DELETE语句，例如:

  ```sql
  DELETE FROM t1 WHERE c1 = ? and c2 = 10;
  ```

  c1和c2列为索引列，后边可以是常量或者参数。

- 只支持简单UPDATE语句，例如:

  ```sql
  UPDATE t1 SET c3 = c3+? WHERE c1 = ? and c2 = 10;
  ```

  c3列修改的值可以是常量和参数，也可以是一个简单的表达式，c1和c2列为索引列，后边可以是常量或者参数。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## advance_xlog_file_num

**参数说明**: 用于控制在后台周期性地提前初始化xlog文件的数目。该参数是为了避免事务提交时执行xlog文件初始化影响性能，但仅在超重负载时才可能出现，因此一般不用配置。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~100（0表示不提前初始化）。例如，取值10，表示后台线程会周期性地根据当前xlog写入位置提前初始化10个xlog文件。

**默认值**: 0

## enable_partition_opfusion

**参数说明**: 在enable_opfusion参数打开的状态下，如果开启该参数，可以对分区表的简单查询进行查询优化，提升SQL执行性能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: off

## enable_beta_nestloop_fusion

**参数说明**: 在enable_opfusion和enable_beta_opfusion两个参数都打开的状态下，如果开启该参数，可以支持TPCC中出现的nestloop join类SQL语句的加速执行，提升SQL执行性能。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启。
- off表示不开启。

**默认值**: off

## enable_beta_opfusion

**参数说明**: 当参数enable_opfusion设置为on时，是否加速TPC-C中聚合函数、排序、nestloop join等SQL语句的执行。对于nestloop join SQL语句，enable_beta_nestloop_fusion必须设置为on。

**取值范围**:  布尔型

**默认值**: off

## sql_beta_feature

**参数说明**: 标识开启的可选SQL引擎Beta特性，其中包括对行数估算、查询等价估算等优化。

开启它们可以对特定的场景进行优化，但也可能会导致部分没有被测试覆盖的场景发生性能劣化。在特定的客户场景中，通过此GUC参数对查询重写规则进行设置，使得查询效率最优。

此参数可以控制SQL引擎Beta特性的组合，比如有多个Beta特性: feature1、feature2、feature3、feature4。可以设置:

```sql
--启用SQL引擎Beta特性feature1。
set sql_beta_feature=feature1;
--启用SQL引擎Beta特性feature2和feature3。
set sql_beta_feature=feature2,feature3;
--关闭所有可选SQL引擎Beta特性。
set sql_beta_feature=none;
```

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- none: 不使用任何Beta优化器特性。
- sel_semi_poisson: 使用泊松分布对等值的半连接和反连接选择率进行校准。
- sel_expr_instr: 使用字符串匹配的行数估算方法对instr(col, 'const') &gt; 0, = 0, = 1进行更准确的估算。
- param_path_gen: 生成更多可能的参数化路径。
- rand_cost_opt: 对小数据量表的随机读取代价进行优化。
- param_path_opt: 利用表的膨胀系数优化索引analyze信息。
- page_est_opt: 优化对非列存表索引analyze信息的relpages估算。

**默认值**: none
