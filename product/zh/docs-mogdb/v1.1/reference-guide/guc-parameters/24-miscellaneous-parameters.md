---
title: 其它选项
summary: 其它选项
author: Zhang Cuiping
date: 2021-04-20
---

# 其它选项

## server_version

**参数说明**: 报告服务器版本号(字符串形式)。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 字符串

**默认值**: 9.2.4

## server_version_num

**参数说明**: 报告服务器版本号(整数形式)。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 整数

**默认值**: 90204

## block_size

**参数说明**: 报告当前数据库所使用的块大小。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 8192

**默认值**: 8192

## segment_size

**参数说明**: 报告当前数据库所使用的段文件大小。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**单位**: 8KB

**默认值**: 131072, 即1GB

## max_index_keys

**参数说明**: 报告当前数据库能够支持的索引键值的最大数目。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**默认值**: 32

## integer_datetimes

**参数说明**: 报告是否支持64位整数形式的日期和时间格式。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 布尔型

- on表示支持。
- off表示不支持。

**默认值**: on

## lc_collate

**参数说明**: 报告当前数据库的字符串排序区域设置。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**默认值**: 依赖于MogDB安装部署时的配置

## lc_ctype

**参数说明**: 报告当前数据库的字母类别区域设置。如: 哪些字符属于字母，它对应的大写形式是什么。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**默认值**: 依赖于MogDB安装部署时的配置

## max_identifier_length

**参数说明**: 报告当前系统允许的标识符最大长度。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 整型

**默认值**: 63

## server_encoding

**参数说明**: 报告当前数据库的服务端编码字符集。

默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**默认值**: 在创建数据库的时候由当前系统环境决定的。

## enable_upgrade_merge_lock_mode

**参数说明**: 当该参数设置为on时，通过提升deltamerge内部实现的锁级别，避免和update/delete并发操作时的报错。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on，提升deltamerge内部实现的锁级别，并发执行deltamerge和update/delete操作时，一个操作先执行，另一个操作被阻塞，在前一个操作完成后，后一个操作再执行。
- off，在对表的delta table的同一行并发执行deltamerge和update/delete操作时，后一个对同一行数据更新的操作会报错退出。

**默认值**: off

## job_queue_processes

**参数说明**: 表示系统可以并发执行的job数目。该参数为postmaster级别，通过gs_guc设置，需要重启MogDB才能生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0～1000

功能:

- 当job_queue_processes设置为0值，表示不启用定时任务功能，任何job都不会被执行（因为开启定时任务的功能会对系统的性能有影响，有些局点可能不需要定时任务的功能，可以通过设置为0不启用定时任务功能）。
- 当job_queue_processes为大于0时，表示启用定时任务功能且系统能够并发处理的最大任务数。

启用定时任务功能后，job_scheduler线程会在定时时间间隔轮询pg_job系统表，系统设置定时任务检查周期默认为1s。

由于并行运行的任务数太多会消耗更多的系统资源，因此需要设置系统并发处理的任务数，当前并发的任务数达到job_queue_processes时，且此时又有任务到期，那么这些任务本次得不到执行而延期到下一轮询周期。因此，建议用户需要根据每个任务的执行时长合理的设置任务的时间间隔（即submit接口中的interval参数），来避免由于任务执行时间太长而导致下个轮询周期无法正常执行。

注: 如果同一时间内并行的job数很多，过小的参数值会导致job等待。而过大的参数值则消耗更多的系统资源，建议设置此参数为100，用户可以根据系统资源情况合理调整。

**默认值**: 10

## ngram_gram_size

**参数说明**: ngram解析器分词的长度。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型 ，1～4

**默认值**: 2

## ngram_grapsymbol_ignore

**参数说明**: ngram解析器是否忽略图形化字符。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示忽略图形化字符。
- off表示不忽略图形化字符。

**默认值**: off

## ngram_punctuation_ignore

**参数说明**: ngram解析器是否忽略标点符号。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示忽略标点符号。
- off表示不忽略标点符号。

**默认值**: on

## transparent_encrypted_string

**参数说明**: 它存储的是透明加密的一个样本串，使用数据库加密密钥加密固定串"TRANS_ENCRYPT_SAMPLE_STRING"后的密文，用来校验二次启动时获取的DEK是否正确。如果校验失败，那么数据库节点将拒绝启动。该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，设置为空表示MogDB非加密。

**默认值**: 空

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 请勿手动设置该参数，设置不当将导致MogDB不可用。

## transparent_encrypt_kms_url

**参数说明**: 它存储的是透明加密的数据库密钥获取地址，内容要求不可出现RFC3986标准外的字符，最大长度2047字节。格式为`kms://协议@KMS主机名1;KMS主机名2:KMS端口号/kms`，例如 kms://https@linux175:29800/。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空

## enable_auto_explain

**参数说明**: 控制是否开启自动打印执行计划。该参数是用来定位慢存储过程或慢查询，只对当前连接的数据库主节点有效。

**取值范围**: 布尔型，true表示开启，false表示关闭。

**默认值**: false。

## auto_explain_level

**参数说明**: 控制自动打印执行计划的日志等级。

**取值范围**: 枚举型，LOG或NOTICE，LOG表示在日志中打印执行计划，NOTICE表示以提示知的形式打印出计划。

**默认值**: LOG。

## transparent_encrypt_kms_region

**参数说明**: 它存储的是MogDB的部署区域，内容要求不可出现RFC3986标准外的字符，最大长度2047字节。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空

## basebackup_timeout

**参数说明**: 备份传输完成后连接无读写的超时时间。 通过gs_basebackup工具作传输时，如果指定较高压缩率时，可能在传输表空间完成后超时（客户端需要压缩传输数据）。

**取值范围**: 整型，0 ~ INT_MAX，单位为秒。其中0表示禁用该功能。

**默认值**:  600s

## behavior_compat_options

**参数说明**: 数据库兼容性行为配置项，该参数的值由若干个配置项用逗号隔开构成。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 当前只支持[表1](#兼容性配置项)。
> - 配置多个兼容性配置项时，相邻配置项用逗号隔开，例如: set behavior_compat_options='end_month_calculate,display_leading_zero';

**表 1** 兼容性配置项<a id="兼容性配置项"> </a>

| **兼容性配置项**          | **兼容性行为控制**                                           |
| ------------------------- | ------------------------------------------------------------ |
| display_leading_zero      | 浮点数显示配置项。<br />- 不设置此配置项时，对于-1~0和0~1之间的小数，不显示小数点前的0。比如，0.25显示为.25。<br />- 设置此配置项时，对于-1~0和0~1之间的小数，显示小数点前的0。比如，0.25显示为0.25。 |
| end_month_calculate       | add_months函数计算逻辑配置项。<br />假定函数add_months的两个参数分别为param1和param2，param1的月份和param2的和为月份result。<br />- 不设置此配置项时，如果param1的日期（Day字段）为月末，并且param1的日期（Day字段）比result月份的月末日期小，计算结果中的日期字段（Day字段）和param1的日期字段保持一致。比如，<br />`postgres=# select add_months('2018-02-28',3) from dual;`<br />`add_months`<br />`2018-05-28 00:00:00 (1 row)`<br />- 设置此配置项时，如果param1的日期（Day字段）为月末，并且param1的日期（Day字段）比result月份的月末日期比小，计算结果中的日期字段（Day字段）和result的月末日期保持一致。比如，<br />postgres=# select add_months('2018-02-28',3) from dual; add_months 2018-05-31 00:00:00 (1 row)`|
| compat_analyze_sample     | analyze采样行为配置项。<br />设置此配置项时，会优化analyze的采样行为，主要体现在analyze时全局采样会更精确的控制在3万条左右，更好的控制analyze时DBnode端的内存消耗，保证analyze性能的稳定性。 |
| bind_schema_tablespace    | 绑定模式与同名表空间配置项。<br />如果存在与模式名sche_name相同的表空间名，那么如果设置search_path为sche_name， default_tablespace也会同步切换到sche_name。 |
| bind_procedure_searchpath | 未指定模式名的数据库对象的搜索路径配置项。<br />在存储过程中如果不显示指定模式名，会优先在存储过程所属的模式下搜索。<br />如果找不到，则有两种情况: <br />- 若不设置此参数，报错退出。<br />- 若设置此参数，按照search_path中指定的顺序继续搜索。如果还是找不到，报错退出。 |
| correct_to_number         | 控制to_number()结果兼容性的配置项。<br />若设置此配置项，则to_number()函数结果与pg11保持一致，否则默认与O db保持一致。 |
| unbind_dive_bound         | 控制对整数除法的结果进行范围校验。<br />若设置此配置项，则不需要对除法结果做范围校验，例如，INT_MIN/(-1)可以得到输出结果为INT_MAX+1，反之，则会因为超过结果大于INT_MAX而报越界错误。 |
| merge_update_multi        | 控制merge into匹配多行时是否进行update操作。<br />若设置此配置项，匹配多行时update不报错，否则默认与a db保持一致，报错。 |
| return_null_string        | 控制函数lpad()和rpad()结果为空字符串"的显示配置项。<br />- 不设置此配置项时，空字符串显示为NULL。<br />`postgres=# select length(lpad('123',0,'*')) from dual; length (1 row)`<br />- 设置此配置项时，空字符串显示为"。<br />`postgres=# select length(lpad('123',0,'*')) from dual; length 0 (1 row)` |
| compat_concat_variadic    | 控制函数concat()和concat_ws()对variadic类型结果兼容性的配置项。<br />若设置此配置项，当concat函数参数为variadic类型时，保留a db和Teradata兼容模式下不同的结果形式；否则默认a db和Teradata兼容模式下结果相同，且与a db保持一致。由于MY无variadic类型，所以该选项对MY无影响。 |
| merge_update_multi        | 控制在使用MERGE INTO … WHEN MATCHED THEN UPDATE（参考MERGE INTO） 和INSERT … ON DUPLICATE KEY UPDATE（参考INSERT）时，当目标表中一条目标数据与多条源数据冲突时UPDATE行为。<br />若设置此配置项，当存在上述场景时，该冲突行将会多次执行UPDATE；否则（默认）报错，即MERGE或INSERT操作失败。 |

## table_skewness_warning_threshold

**参数说明**: 设置用于表倾斜告警的阈值。

该参数属于USERSET类型参数，请参考**表GUC参数分类**中对应设置方法进行设置。

**取值范围**: 浮点型，0～1

**默认值**: 1

## table_skewness_warning_rows

**参数说明**: 设置用于表倾斜告警的行数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX

**默认值**: 100000

## datanode_heartbeat_interval

**参数说明**: 设置心跳线程间心跳消息发送时间间隔，建议值不超过wal_receiver_timeout / 2。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1000～60000（毫秒）

**默认值**: 1s

## bgwriter_thread_num

**参数说明**: 设置用于增量检查点打开后后台刷页的线程数，将可以淘汰的脏页刷盘，不脏的页面放入到候选buffer链，设置此选项有助于加快buffer淘汰速度，提升性能。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～8

- 开发期间为测试关闭该特性的效果，可以设置为0，但是当前如果设置为0，会在代码中修正为1，不再支持关闭该特性。
- 当参数设置为1~8时，表示会启动相对应数量的background线程，用于维护候选buffer链，将满足条件的脏页刷盘，不脏的页放入到候选list中。

**默认值**: 2

## candidate_buf_percent_target

**参数说明**: 设置用于增量检查点打开，并且bgwriter_thread_num数目不是0时，候选buffer链中可用buffer数目占据shared_buffer内存缓冲区百分比的期望值，当前候选链中的数目少于目标值时，bgwriter线程会启动将满足条件的脏页刷盘。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 双精度类型，0.1～0.85

**默认值**: 0.3

## pagewriter_thread_num

**参数说明**: 设置用于增量检查点打开后后台刷页的线程数，主要是按照脏页置脏的顺序刷盘，用于推进recovery点。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～8

**默认值**: 2

## pagewriter_threshold

**参数说明**: 启用增量检查点时，线程必须达到的脏页数量，以便在不休眠的情况下继续刷新脏页。该参数将不再生效。

使用dirty_page_percent_max参数设置该值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围** : 整型， 1~ INT_MAX

**默认值**: 818

## dirty_page_percent_max

**参数说明**: 设置用于增量检查点打开后脏页数量占shared_buffers的百分比达到这个设定值时，后台刷页线程将一直刷脏页，不sleep。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.1～1

**默认值**: 0.9

## pagewriter_sleep

**参数说明**: 设置用于增量检查点打开后，脏页数量不足pagewriter_threshold时，后台刷页线程将sleep设置的时间继续刷页。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～3600000（毫秒）

**默认值**: 2000ms

## remote_read_mode

参数说明:远程读功能开关。读取主机上的页面失败时可以从备机上读取对应的页面。

取值范围: 枚举类型

- off表示关闭远程读功能
- non_authentication表示开启远程读功能，但不进行证书认证
- authentication表示开启远程读功能，但要进行证书认证

默认值: authentication
