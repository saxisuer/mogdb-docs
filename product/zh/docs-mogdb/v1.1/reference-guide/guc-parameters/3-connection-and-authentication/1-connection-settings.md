---
title: 连接设置
summary: 连接设置
author: Zhang Cuiping
date: 2021-04-20
---

# 连接设置

介绍设置客户端和服务器连接方式相关的参数。

## listen_addresses

**参数说明**: 声明服务器侦听客户端的TCP/IP地址。

该参数指定MogDB服务器使用哪些IP地址进行侦听，如IPV4或IPV6（若支持）。服务器主机上可能存在多个网卡，每个网卡可以绑定多个IP地址，该参数就是控制MogDB到底绑定在哪个或者哪几个IP地址上。而客户端则可以通过该参数中指定的IP地址来连接MogDB或者给MogDB发送请求。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**:

- 主机名或IP地址，多个值之间用英文逗号分隔。
- 星号"*"或"0.0.0.0"表示侦听所有IP地址。配置侦听所有IP地址存在安全风险，不推荐用户使用。必须与有效地址结合使用（比如本地IP等），否则，可能造成Build失败的问题。
- 置空则服务器不会侦听任何IP地址，这种情况下，只有Unix域套接字可以用于连接数据库。

**默认值**: localhost

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> localhost表示只允许进行本地"回环"连接。

## local_bind_address

**参数说明**: 声明当前节点连接MogDB其他节点绑定的本地IP地址。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**默认值**: 0.0.0.0（实际值由安装时的配置文件指定）

## port

**参数说明**: MogDB服务侦听的TCP端口号。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> port参数修改后，需要执行下面命令刷新静态配置文件。
>
> ```
> gs_om -t generateconf -X XMLFILE [--distribute] [-l LOGFILE]
> ```

**取值范围**: 整型，1～65535

**默认值**: 5432（实际值由安装时的配置文件指定）

## max_connections

**参数说明**: 允许和数据库连接的最大并发连接数。此参数会影响MogDB的并发能力。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型。最小值为10，最大值为262143。

**默认值**: 数据库节点: 5000。如果该默认值超过内核支持的最大值（在执行gs_initdb的时候判断），系统会提示错误。

**设置建议:**

数据库主节点中此参数建议保持默认值。

增大这个参数可能导致MogDB要求更多的SystemV共享内存或者信号量，可能超过操作系统缺省配置的最大值。这种情况下，请酌情对数值加以调整。

## max_inner_tool_connections

**参数说明**: 允许和数据库连接的工具的最大并发连接数。此参数会影响MogDB的工具连接并发能力。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为1，最大值为MIN(262143, max_connections)，max_connections的计算方法见上文。

**默认值**: 数据库节点为50。如果该默认值超过内核支持的最大值（在执行gs_initdb的时候判断），系统会提示错误。

**设置建议:**

数据库主节点中此参数建议保持默认值。

增大此参数可能导致MogDB要求更多的SystemV共享内存或者信号量，可能超过操作系统缺省配置的最大值。这种情况下，请酌情对数值加以调整。

## sysadmin_reserved_connections

**参数说明**: 为管理员用户预留的最少连接数, 不建议设置过大。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为MIN(262143, max_connections)，max_connections的计算方法见上文。

**默认值**: 3

## unix_socket_directory

**参数说明**: 设置MogDB服务器侦听客户端连接的Unix域套接字目录。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

该参数的长度限制于操作系统的长度，超过该限制将会导致Unix-domain socket path "xxx" is too long的问题。

**取值范围**: 字符串

**默认值**: 空字符串（实际值由安装时配置文件指定）

## unix_socket_group

**参数说明**: 设置Unix域套接字的所属组（套接字的所属用户总是启动服务器的用户）。可以与选项[unix_socket_permissions](#unix_socket_permissions)一起用于对套接字进行访问控制。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，其中空字符串表示当前用户的缺省组。

**默认值**: 空字符串

## unix_socket_permissions

**参数说明**: 设置Unix域套接字的访问权限。

Unix域套接字使用普通的Unix文件系统权限集。这个参数的值应该是数值的格式（chmod和umask命令可接受的格式）。如果使用自定义的八进制格式，数字必须以0开头。

建议设置为0770（只有当前连接数据库的用户和同组的人可以访问）或者0700（只有当前连接数据库的用户自己可以访问，同组或者其他人都没有权限）。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0000-0777

**默认值**: 0700

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> 在Linux中，文档具有十个属性，其中第一个属性为文档类型，后面九个为权限属性，分别为Owner，Group及Others这三个组别的read、write、execute属性。 文档的权限属性分别简写为r，w，x，这九个属性三个为一组，也可以使用数字来表示文档的权限，对照表如下:
>
> r: 4
>
> w: 2
>
> x: 1
>
> -: 0
>
> 同一组（owner/group/others）的三个属性是累加的。
>
> 例如，-rwxrwx-表示这个文档的权限为:
>
> owner = rwx = 4+2+1 = 7
>
> group = rwx = 4+2+1 = 7
>
> others = - = 0+0+0 = 0
>
> 所以其权限为0770。

## application_name

**参数说明**: 当前连接请求当中，所使用的客户端名称。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。

**默认值**: 空字符串(连接到后端的应用名，以实际安装为准)

## connection_info

**参数说明**: 连接数据库的驱动类型、驱动版本号、当前驱动的部署路径和进程属主用户。

该参数属于USERSET类型参数，属于运维类参数，不建议用户设置。

**取值范围**: 字符串。

**默认值**: 空字符串**。**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 空字符串，表示当前连接数据库的驱动不支持自动设置connection_info参数或应用程序未设置。
> - 驱动连接数据库的时候自行拼接的connection_info参数格式如下:
>
> ```sql
> {"driver_name":"ODBC","driver_version": "(MogDB 1.1.0 build 5be05d82) compiled at 2020-05-08 02:59:43 commit 2143 last mr 131 debug","driver_path":"/usr/local/lib/psqlodbcw.so","os_user":"omm"}
> ```
>
> 默认显示driver_name和driver_version，driver_path和os_user的显示由用户控制。
