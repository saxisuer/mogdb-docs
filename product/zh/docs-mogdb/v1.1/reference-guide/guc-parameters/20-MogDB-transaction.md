---
title: MogDB事务
summary: MogDB事务
author: Zhang Cuiping
date: 2021-04-20
---

# MogDB事务

介绍MogDB事务隔离、事务只读、最大prepared事务数、维护模式目的参数设置及取值范围等内容。

## transaction_isolation

**参数说明**: 设置当前事务的隔离级别。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，只识别以下字符串，大小写空格敏感:

- serializable: MogDB中等价于REPEATABLE READ。
- read committed: 只能读取已提交的事务的数据（缺省），不能读取到未提交的数据。
- repeatable read: 仅能读取事务开始之前提交的数据，不能读取未提交的数据以及在事务执行期间由其它并发事务提交的修改。
- default: 设置为default_transaction_isolation所设隔离级别。

**默认值**: read committed

## transaction_read_only

**参数说明**: 设置当前事务是只读事务。

该参数在数据库恢复过程中或者在备机里，固定为on；否则，固定为default_transaction_read_only的值。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示设置当前事务为只读事务。
- off表示该事务可以是非只读事务。

**默认值**: off

## xc_maintenance_mode

**参数说明**: 设置系统进入维护模式。

该参数属于SUSET类型参数，仅支持表[GUC参数分类](30-appendix.md)中的方式三进行设置。

**取值范围**: 布尔型

- on表示该功能启用。
- off表示该功能被禁用。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 谨慎打开这个开关，避免引起MogDB数据不一致。

**默认值**: off

## allow_concurrent_tuple_update

**参数说明**: 设置是否允许并发更新。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示该功能启用。
- off表示该功能被禁用。

**默认值**: on

## transaction_deferrable

**参数说明**: 指定是否允许一个只读串行事务延迟执行，使其不会执行失败。该参数设置为on时，当一个只读事务发现读取的元组正在被其他事务修改，则延迟该只读事务直到其他事务修改完成。目前，MogDB暂时未用到这个参数。与该参数类似的还有一个default_transaction_deferrable，设置它来指定一个事务是否允许延迟。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许执行。
- off表示不允许执行。

**默认值**: off

## enforce_two_phase_commit

**参数说明**: 强制使用两阶段提交，为了兼容历史版本功能保留该参数，当前版本设置无效。

该参数属于SUSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示强制使用两阶段提交。
- off表示不强制使用两阶段提交。

**默认值**: on

## enable_show_any_tuples

**参数说明**: 该参数只有在只读事务中可用，用于分析。当这个参数被置为on/true时，表中元组的所有版本都会可见。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示表中元组的所有版本都会可见。
- off/false表示表中元组的所有版本都不可见。

**默认值**: off

## replication_type

**参数说明**: 标记当前HA模式是单主机模式、主备从模式还是一主多备模式。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

该参数用户不能自己去设置参数值。

**取值范围**: 0~2

- 2 表示单主机模式，此模式无法扩展备机。
- 1 表示使用一主多备模式，全场景覆盖，推荐使用。
- 0 表示主备从模式，目前此模式暂不支持。

**默认值**: 1
