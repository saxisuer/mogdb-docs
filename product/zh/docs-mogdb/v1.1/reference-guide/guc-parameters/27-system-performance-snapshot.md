---
title: 系统性能快照
summary: 系统性能快照
author: Zhang Cuiping
date: 2021-04-20
---

# 系统性能快照

## enable_wdr_snapshot

**参数说明**: 是否开启数据库监控快照功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on: 打开数据库监控快照功能。
- off: 关闭数据库监控快照功能。

**默认值**: off

## wdr_snapshot_retention_days

**参数说明**: 系统中数据库监控快照数据的保留天数，超过设置的值之后，系统每隔wdr_snapshot_interval时间间隔，清理snapshot_id最小的快照数据。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1～8。

**默认值**: 8

## wdr_snapshot_query_timeout

**参数说明**: 系统执行数据库监控快照操作时，设置快照操作相关的sql语句的执行超时时间。如果语句超过设置的时间没有执行完并返回结果，则本次快照操作失败。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，100～INT_MAX（秒）。

**默认值**: 100s

## wdr_snapshot_interval

**参数说明**: 后台线程Snapshot自动对数据库监控数据执行快照操作的时间间隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10～60（分钟）。

**默认值**: 1h
