---
title: 日志回放
summary: 日志回放
author: Zhang Cuiping
date: 2021-04-20
---

# 日志回放

## recovery_time_target

**参数说明**: 设置recovery_time_target秒能够让备机完成日志写入和回放。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~3600 （秒）

0是指不开启日志流控，1~3600是指备机能够在recovery_time_target时间内完成日志的写入和回放，可以保证主机与备机切换时能够在recovery_time_target秒完成日志写入和回放，保证备机能够快速升主机。recovery_time_target设置时间过小会影响主机的性能，设置过大会失去流控效果。

**默认值**: 0

## recovery_max_workers

**参数说明**: 设置最大并行回放线程个数。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~20

**默认值**: 1 （安装工具默认将设置为4以获得更好的性能）

## recovery_parse_workers

**参数说明**: 是极致RTO特性中ParseRedoRecord线程的数量。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~16

仅在开启极致RTO情况下可以设置recovery_parse_workers为&gt;1。需要配合recovery_redo_workers使用。若同时开启recovery_parse_workers和recovery_max_workers，以开启极致RTO的recovery_parse_workers为准，并行回放特性失效。因极致RTO不支持hot standby模式和主备从模式, 仅在参数hot_standby设置成off，replication_type设置成1时可以设置recovery_parse_workers为&gt;1。另外，极致RTO也不支持列存，在已经使用列存表或者即将使用列存表的系统中，请关闭极致RTO.

**默认值**: 1

## recovery_redo_workers

**参数说明**: 是极致RTO特性中每个ParseRedoRecord线程对应的PageRedoWorker数量。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~8

需要配合recovery_parse_workers使用。在配合recovery_parse_workers使用时，只有recovery_parse_workers大于0，recovery_redo_workers参数才生效。

**默认值**: 1

## recovery_parallelism

**参数说明**: 查询实际回放线程个数，该参数为只读参数。

**取值范围**: 整型，1~2147483647

该参数受参数: recovery_max_workers及参数: recovery_parse_workers影响，上述任意一参数值大于0时，该参数值将重新计算。

**默认值**: 1

## enable_page_lsn_check

**参数说明**: 数据页lsn检查开关。回放时，检查数据页当前的lsn是否是期望的lsn。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**: on
