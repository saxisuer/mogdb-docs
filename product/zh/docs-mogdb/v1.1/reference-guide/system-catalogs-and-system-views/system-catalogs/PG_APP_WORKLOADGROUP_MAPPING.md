---
title: PG_APP_WORKLOADGROUP_MAPPING
summary: PG_APP_WORKLOADGROUP_MAPPING
author: Guo Huan
date: 2021-04-19
---

# PG_APP_WORKLOADGROUP_MAPPING

PG_APP_WORKLOADGROUP_MAPPING系统表提供了数据库负载映射组的信息。

**表 1** PG_APP_WORKLOADGROUP_MAPPING字段

| 名称            | 类型 | 描述                                 |
| :-------------- | :--- | :----------------------------------- |
| oid             | oid  | 行标识符（隐藏属性，必须明确选择）。 |
| appname         | name | 应用名称。                           |
| workload_gpname | name | 映射到的负载组名称。                 |
