---
title: PG_DEPEND
summary: PG_DEPEND
author: Guo Huan
date: 2021-04-19
---

# PG_DEPEND

PG_DEPEND系统表记录数据库对象之间的依赖关系。这个信息允许DROP命令找出哪些其它对象必须由DROP CASCADE删除，或者是在DROP RESTRICT的情况下避免删除。

这个表的功能类似PG_SHDEPEND，用于记录那些在MogDB之间共享的对象之间的依赖性关系。

**表 1** PG_DEPEND字段

| 名称        | 类型    | 引用         | 描述                                                         |
| :---------- | :------ | :----------- | :----------------------------------------------------------- |
| classid     | oid     | PG_CLASS.oid | 有依赖对象所在系统表的OID。                                  |
| objid       | oid     | 任意OID属性  | 指定的依赖对象的OID。                                        |
| objsubid    | integer | -            | 对于表字段，这个是该属性的字段数（objid和classid引用表本身）。对于所有其它对象类型，目前这个字段是零。 |
| refclassid  | oid     | PG_CLASS.oid | 被引用对象所在的系统表的OID。                                |
| refobjid    | oid     | 任意OID属性  | 指定的被引用对象的OID。                                      |
| refobjsubid | integer | -            | 对于表字段，这个是该字段的字段号（refobjid和refclassid引用表本身）。对于所有其它对象类型，目前这个字段是零。 |
| deptype     | "char"  | -            | 一个定义这个依赖关系特定语义的代码。                         |

在所有情况下，一个PG_DEPEND记录表示被引用的对象不能在有依赖的对象被删除前删除。不过，这里还有几种由deptype定义的情况:

- DEPENDENCY_NORMAL (n): 独立创建的对象之间的一般关系。有依赖的对象可以在不影响被引用对象的情况下删除。被引用对象只有在声明了CASCADE的情况下删除，这时有依赖的对象也被删除。例子: 一个表字段对其数据类型有一般依赖关系。
- DEPENDENCY_AUTO (a): 有依赖对象可以和被引用对象分别删除，并且如果删除了被引用对象则应该被自动删除（不管是RESTRICT或CASCADE模式）。例子: 一个表上面的命名约束是在该表上的自动依赖关系，因此如果删除了表，它也会被删除。
- DEPENDENCY_INTERNAL (i): 有依赖的对象是作为被引用对象的一部分创建的，实际上只是它的内部实现的一部分。 DROP有依赖对象是不能直接允许的（将告诉用户发出一条删除被引用对象的DROP）。一个对被引用对象的DROP将传播到有依赖对象，不管是否声明了CASCADE。
- DEPENDENCY_EXTENSION (e): 依赖对象是被依赖对象extension的一个成员（请参见PG_EXTENSION）。依赖对象只可以通过在被依赖对象上DROP EXTENSION删除。函数上这个依赖类型和内部依赖一样动作，但是它为了清晰和简化gs_dump保持分开。
- DEPENDENCY_PIN (p): 没有依赖对象；这种类型的记录标志着系统本身依赖于被引用对象，因此这个对象决不能被删除。这种类型的记录只有在initdb的时候创建。有依赖对象的字段里是零。
