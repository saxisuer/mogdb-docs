---
title: PG_JOB
summary: PG_JOB
author: Guo Huan
date: 2021-04-19
---

# PG_JOB

PG_JOB系统表存储用户创建的定时任务的任务详细信息，定时任务线程定时轮询pg_job系统表中的时间，当任务到期会触发任务的执行，并更新pg_job表中的任务状态。该系统表属于Shared Relation，所有创建的job记录对所有数据库可见。

**表 1** PG_JOB字段

| 名称                 | 类型                        | 描述                                                         |
| :------------------- | :-------------------------- | :----------------------------------------------------------- |
| oid                  | oid                         | 行标识符（隐藏属性，必须明确选择）。                         |
| job_id               | bigint                      | 作业ID，主键，是唯一的（有唯一索引）                         |
| current_postgres_pid | bigint                      | 如果当前任务已被执行，那么此处记录运行此任务的postgres线程ID。默认为 -1，表示此任务未被执行过。 |
| log_user             | name                        | 创建者的UserName                                             |
| priv_user            | name                        | 作业执行者的UserName                                         |
| dbname               | name                        | 标识作业要在哪个数据库执行的数据库名称                       |
| node_name            | name                        | 标识当前作业是在哪个数据库主节点上创建和执行                 |
| job_status           | "char"                      | 当前任务的执行状态，取值范围: ('r', 's', 'f', 'd')，默认为's'，取值含义: <br/>Status of job step: r=running, s=successfully finished, f=job failed, d=disable<br/>当job连续执行失败16次，会将job_status自动设置为失效状态'd'，后续不再执行该job。<br/>注: 当用户将定时任务关闭（即: guc参数job_queue_processes为0时），由于监控job执行的线程不会启动，所以该状态不会根据job的实时状态进行设置，用户不需要关注此状态。只有当开启定时任务功能（即: guc参数job_queue_processes为非0时），系统才会根据当前job的实时状态刷新该字段值。 |
| start_date           | timestamp without time zone | 作业第一次开始运行时间，时间精确到毫秒。                     |
| next_run_date        | timestamp without time zone | 下次定时执行任务的时间，时间精确到毫秒。                     |
| failure_count        | smallint                    | 失败计数，作业连续执行失败16次，不再继续执行。               |
| interval             | text                        | 作业执行的重复时间间隔。                                     |
| last_start_date      | timestamp without time zone | 上次运行开始时间，时间精确到毫秒。                           |
| last_end_date        | timestamp without time zone | 上次运行的结束时间，时间精确到毫秒。                         |
| last_suc_date        | timestamp without time zone | 上次成功运行的开始时间，时间精确到毫秒。                     |
| this_run_date        | timestamp without time zone | 正在运行任务的开始时间，时间精确到毫秒。                     |
| nspname              | name                        | 标识作业执行时的schema的名称。                               |
