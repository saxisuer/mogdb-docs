---
title: GS_WLM_USER_RESOURCE_HISTORY
summary: GS_WLM_USER_RESOURCE_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_USER_RESOURCE_HISTORY

GS_WLM_USER_RESOURCE_HISTORY系统表存储与用户使用资源相关的信息，仅在数据库主节点上有效。该系统表的每条记录都是对应时间点某用户的资源使用情况，包括: 内存、CPU核数、存储空间、临时空间、算子落盘空间、逻辑IO流量、逻辑IO次数和逻辑IO速率信息。其中，内存、CPU、IO相关监控项仅记录用户复杂作业的资源使用情况。对于IO相关监控项，当参数enable_logical_io_statistics为on时才有效；当参数enable_user_metric_persistent为on时，才会开启用户监控数据保存功能。GS_WLM_USER_RESOURCE_HISTORY系统表的数据来源于PG_TOTAL_USER_RESOURCE_INFO视图。

**表 1** GS_WLM_USER_RESOURCE_HISTORY

| 名称              | 类型                     | 描述 |
| :---------------- | :----------------------- | :--------------------------|
| username          | text                     | 用户名       |
| timestamp         | timestamp with time zone | 时间戳                           |
| used_memory       | integer                  | 正在使用的内存大小，单位MB。     |
| total_memory      | integer                  | 可以使用的内存大小，单位为MB。值为0表示未限制最大可用内存，其限制取决于数据库最大可用内存。 |
| used_cpu          | real                     | 正在使用的CPU核数。              |
| total_cpu         | integer                  | 该机器节点上，用户关联控制组的CPU核数总和。                  |
| used_space        | bigint                   | 已使用的存储空间大小，单位KB。   |
| total_space       | bigint                   | 可使用的存储空间大小，单位KB，值为-1表示未限制最大存储空间。 |
| used_temp_space   | bigint                   | 已使用的临时存储空间大小，单位KB。                           |
| total_temp_space  | bigint                   | 可使用的临存储空间大小，单位KB，值为-1表示未限制最大临时存储空间。 |
| used_spill_space  | bigint                   | 已使用的算子落盘存储空间大小，单位KB。                       |
| total_spill_space | bigint                   | 可使用的算子落盘存储空间大小，单位KB，值为-1表示未限制最大算子落盘存储空间。 |
| read_kbytes       | bigint                   | 监控周期内，读操作的字节流量，单位KB。                       |
| write_kbytes      | bigint                   | 监控周期内，写操作的字节流量，单位KB。                       |
| read_counts       | bigint                   | 监控周期内，读操作的次数，单位次。                           |
| write_counts      | bigint                   | 监控周期内，写操作的次数，单位次。                           |
| read_speed        | real                     | 监控周期内，读操作的字节速率，单位KB/s。                     |
| write_speed       | real                     | 监控周期内，写操作的字节速率，单位KB/s。                     |
