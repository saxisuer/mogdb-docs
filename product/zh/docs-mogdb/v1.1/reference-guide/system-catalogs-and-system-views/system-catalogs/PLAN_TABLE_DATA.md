---
title: PLAN_TABLE_DATA
summary: PLAN_TABLE_DATA
author: Guo Huan
date: 2021-04-19
---

# PLAN_TABLE_DATA

PLAN_TABLE_DATA存储了用户通过执行EXPLAIN PLAN收集到的计划信息。与PLAN_TABLE视图不同的是PLAN_TABLE_DATA表存储了所有session和user执行EXPLAIN PLAN收集的计划信息。

**表 1** PLAN_TABLE_DATA字段

| 名称         | 类型           | 描述                                                         |
| :----------- | :------------- | :----------------------------------------------------------- |
| session_id   | text           | 表示插入该条数据的会话，由服务线程启动时间戳和服务线程ID组成。受非空约束限制。 |
| user_id      | oid            | 用户ID，用于标识触发插入该条数据的用户。受非空约束限制。     |
| statement_id | varchar2(30)   | 用户输入的查询标签。                                         |
| plan_id      | bigint         | 查询标识。                                                   |
| id           | int            | 计划中的节点编号。                                           |
| operation    | varchar2(30)   | 操作描述。                                                   |
| options      | varchar2(255)  | 操作选项。                                                   |
| object_name  | name           | 操作对应的对象名，来自于用户定义。                           |
| object_type  | varchar2(30)   | 对象类型。                                                   |
| object_owner | name           | 对象所属schema，来自于用户定义。                             |
| projection   | varchar2(4000) | 操作输出的列信息。                                           |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - PLAN_TABLE_DATA中包含了当前节点所有用户、所有会话的数据，仅管理员有访问权限。普通用户可以通过PLAN_TABLE视图查看属于自己的数据。
> - PLAN_TABLE_DATA中的数据是用户通过执行EXPLAIN PLAN命令后由系统自动插入表中，因此禁止用户手动对数据进行插入或更新，否则会引起表中的数据混乱。需要对表中数据删除时，建议通过PLAN_TABLE视图。
> - statement_id、object_name、object_owner和projection字段内容遵循用户定义的大小写存储，其它字段内容采用大写存储。
