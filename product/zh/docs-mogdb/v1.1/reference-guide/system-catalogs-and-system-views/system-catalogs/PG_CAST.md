---
title: PG_CAST
summary: PG_CAST
author: Guo Huan
date: 2021-04-19
---

# PG_CAST

PG_CAST系统表存储数据类型之间的转化关系。

**表 1** PG_CAST字段

| 名称        | 类型   | 描述                                                         |
| :---------- | :----- | :----------------------------------------------------------- |
| oid         | oid    | 行标识符（隐藏属性，必须明确选择）。                         |
| castsource  | oid    | 源数据类型的OID。                                            |
| casttarget  | oid    | 目标数据类型的OID。                                          |
| castfunc    | oid    | 转化函数的OID。如果为零表明不需要转化函数。                  |
| castcontext | "char" | 源数据类型和目标数据类型间的转化方式: <br/>- 'e': 表示只能进行显式转化（使用CAST或::语法）。<br/>- 'i': 表示只能进行隐式转化。<br/>- 'a': 表示类型间同时支持隐式和显式转化。 |
| castmethod  | "char" | 转化方法: <br/>- 'f': 使用castfunc字段中指定的函数进行转化。<br/>- 'b': 类型间是二进制强制转化，不使用castfunc。 |
