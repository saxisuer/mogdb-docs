---
title: PG_INHERITS
summary: PG_INHERITS
author: Guo Huan
date: 2021-04-19
---

# PG_INHERITS

PG_INHERITS系统表记录关于表继承层次的信息。数据库里每个直接的子系表都有一条记录。间接的继承可以通过追溯记录链来判断。

**表 1** PG_INHERITS字段

| 名称      | 类型    | 引用         | 描述                                                         |
| :-------- | :------ | :----------- | :------------ |
| inhrelid  | oid     | PG_CLASS.oid | 子表的OID。                                                  |
| inhparent | oid     | PG_CLASS.oid | 父表的OID。                                                  |
| inhseqno  | integer | -            | 如果一个子表存在多个直系父表（多重继承），这个数字表明此继承字段的排列顺序。计数从1开始。 |
