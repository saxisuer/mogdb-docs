---
title: PG_DEFAULT_ACL
summary: PG_DEFAULT_ACL
author: Guo Huan
date: 2021-04-19
---

# PG_DEFAULT_ACL

PG_DEFAULT_ACL系统表存储为新建对象设置的初始权限。

**表 1** PG_DEFAULT_ACL字段

| 名称            | 类型      | 描述                                      |
| :-------------- | :-------- | :---------------------------------------- |
| oid             | oid       | 行标识符（隐藏属性，必须明确选择）。      |
| defaclrole      | oid       | 与此权限相关的角色ID。                    |
| defaclnamespace | oid       | 与此权限相关的名称空间，如果没有，则为0。 |
| defaclobjtype   | "char"    | 此权限的对象类型。                        |
| defaclacl       | aclitem[] | 创建该类型时所拥有的访问权限。            |
