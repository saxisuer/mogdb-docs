---
title: PG_TS_CONFIG_MAP
summary: PG_TS_CONFIG_MAP
author: Guo Huan
date: 2021-04-19
---

# PG_TS_CONFIG_MAP

PG_TS_CONFIG_MAP系统表包含为每个文本搜索配置的解析器的每个输出符号类型，显示哪个文本搜索字典应该被咨询、以什么顺序搜索的记录。

**表 1** PG_TS_CONFIG_MAP字段

| 名称         | 类型    | 引用             | 描述                                      |
| :----------- | :------ | :--------------- | :---------------------------------------- |
| mapcfg       | oid     | PG_TS_CONFIG.oid | 拥有这个映射记录的PG_TS_CONFIG记录的OID。 |
| maptokentype | integer | -                | 由配置的解析器发出的一个符号类型。        |
| mapseqno     | integer | -                | 以什么顺序咨询这个记录。                  |
| mapdict      | oid     | PG_TS_DICT.oid   | 要咨询的文本搜索字典的OID。               |
