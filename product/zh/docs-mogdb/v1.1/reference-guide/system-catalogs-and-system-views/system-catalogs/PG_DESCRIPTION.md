---
title: PG_DESCRIPTION
summary: PG_DESCRIPTION
author: Guo Huan
date: 2021-04-19
---

# PG_DESCRIPTION

PG_DESCRIPTION系统表可以给每个数据库对象存储一个可选的描述（注释）。许多内置的系统对象的描述提供了PG_DESCRIPTION的初始内容。

这个表的功能类似PG_SHDESCRIPTION，用于记录MogDB范围内共享对象的注释。

**表 1** PG_DESCRIPTION字段

| 名称        | 类型    | 引用         | 描述                                                         |
| :---------- | :------ | :----------- | :-----------------|
| objoid      | oid     | 任意OID属性  | 这条描述所描述的对象的OID。                                  |
| classoid    | oid     | PG_CLASS.oid | 这个对象出现的系统表的OID。                                  |
| objsubid    | integer | -            | 对于一个表字段的注释，它是字段号（objoid和classoid指向表自身）。对于其它对象类型，它是零。 |
| description | text    | -            | 对该对象描述的任意文本。                                     |
