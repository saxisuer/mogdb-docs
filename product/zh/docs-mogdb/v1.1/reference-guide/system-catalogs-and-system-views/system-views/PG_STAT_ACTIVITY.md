---
title: PG_STAT_ACTIVITY
summary: PG_STAT_ACTIVITY
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_ACTIVITY

PG_STAT_ACTIVITY视图显示和当前用户查询相关的信息。

**表 1** PG_STAT_ACTIVITY字段

| 名称             | 类型                     | 描述                                                         |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| datid            | oid                      | 用户会话在后台连接到的数据库OID。                            |
| datname          | name                     | 用户会话在后台连接到的数据库名称。                           |
| pid              | bigint                   | 后台线程ID。                                                 |
| sessionid        | bigint                   | 会话ID。                                                     |
| usesysid         | oid                      | 登录该后台的用户OID。                                        |
| usename          | name                     | 登录该后台的用户名。                                         |
| application_name | text                     | 连接到该后台的应用名。                                       |
| client_addr      | inet                     | 连接到该后台的客户端的IP地址。 如果此字段是null，它表明通过服务器机器上UNIX套接字连接客户端或者这是内部进程，如autovacuum。 |
| client_hostname  | text                     | 客户端的主机名，这个字段是通过client_addr的反向DNS查找得到。这个字段只有在启动log_hostname且使用IP连接时才非空。 |
| client_port      | integer                  | 客户端用于与后台通讯的TCP端口号，如果使用Unix套接字，则为-1。 |
| backend_start    | timestamp with time zone | 该过程开始的时间，即当客户端连接服务器时。                   |
| xact_start       | timestamp with time zone | 启动当前事务的时间，如果没有事务是活跃的，则为null。如果当前查询是首个事务，则这列等同于query_start列。 |
| query_start      | timestamp with time zone | 开始当前活跃查询的时间， 如果state的值不是active，则这个值是上一个查询的开始时间。 |
| state_change     | timestamp with time zone | 上次状态改变的时间。                                         |
| waiting          | Boolean                  | 如果后台当前正等待锁则为true。                               |
| enqueue          | text                     | 该字段不支持。                                               |
| state            | text                     | 该后台当前总体状态。可能值是: <br/>- active: 后台正在执行一个查询。<br/>- idle: 后台正在等待一个新的客户端命令。<br/>- idle in transaction: 后台在事务中，但事务中没有语句在执行。<br/>- idle in transaction (aborted): 后台在事务中，但事务中有语句执行失败。<br/>- fastpath function call: 后台正在执行一个fast-path函数。<br/>- disabled: 如果后台禁用track_activities，则报告这个状态。<br/>说明:<br/>普通用户只能查看到自己帐户所对应的会话状态。即其他帐户的state信息为空。例如以judy用户连接数据库后，在pg_stat_activity中查看到的普通用户joe及初始用户omm的state信息为空: <br/>`SELECT datname, usename, usesysid, state,pid FROM pg_stat_activity;`<br/><br/>datname  \| usename \| usesysid \| state  \|       pid<br/>----+---+----+---+------<br/>postgres \| omm     \|       10 \|        \| 139968752121616<br/>postgres \| omm     \|       10 \|        \| 139968903116560<br/>db_tpcc \| judy    \|    16398 \| active \| 139968391403280<br/>postgres \| omm     \|       10 \|        \| 139968643069712<br/>postgres \| omm     \|       10 \|        \| 139968680818448<br/>postgres \| joe     \|    16390 \|        \| 139968563377936<br/>(6 rows) |
| resource_pool    | name                     | 用户使用的资源池。                                           |
| query_id         | bigint                   | 查询语句的ID。                                               |
| query            | text                     | 该后台的最新查询。如果state状态是active（活跃的），此字段显示当前正在执行的查询。所有其他情况表示上一个查询。 |
| connection_info  | text                     | json格式字符串，记录当前连接数据库的驱动类型、驱动版本号、当前驱动的部署路径、进程属主用户等信息（参见connection_info）。 |
