---
title: GS_OS_RUN_INFO
summary: GS_OS_RUN_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_OS_RUN_INFO

GS_OS_RUN_INFO视图显示当前操作系统运行的状态信息。

**表 1** GS_OS_RUN_INFO字段

| 名称       | 类型    | 描述                               |
| :--------- | :------ | :--------------------------------- |
| id         | integer | 编号。                             |
| name       | text    | 操作系统运行状态名称。             |
| value      | numeric | 操作系统运行状态值。               |
| comments   | text    | 操作系统运行状态注释。             |
| cumulative | Boolean | 操作系统运行状态的值是否为累加值。 |
