---
title: PG_GTT_ATTACHED_PIDS
summary: PG_GTT_ATTACHED_PIDS
author: Guo Huan
date: 2021-04-19
---

# PG_GTT_ATTACHED_PIDS

PG_GTT_ATTACHED_PIDS视图查看哪些会话正在使用全局临时表，调用pg_get_attached_pid函数。

**表 1** PG_GTT_ATTACHED_PIDS字段

| 名称       | 类型     | 描述              |
| :--------- | :------- | :---------------- |
| schemaname | name     | schema名称。      |
| tablename  | name     | 全局临时表名称。  |
| relid      | oid      | 全局临时表的oid。 |
| pids       | bigint[] | 线程pid列表。     |
