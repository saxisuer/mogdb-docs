---
title: PG_STAT_BAD_BLOCK
summary: PG_STAT_BAD_BLOCK
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_BAD_BLOCK

PG_STAT_BAD_BLOCK视图显示自节点启动后，读取数据时出现Page/CU校验失败的统计信息。

**表 1** PG_STAT_BAD_BLOCK字段

| 名称         | 类型                     | 描述                   |
| :----------- | :----------------------- | :--------------------- |
| nodename     | text                     | 节点名。               |
| databaseid   | integer                  | 数据库OID。            |
| tablespaceid | integer                  | 表空间OID。            |
| relfilenode  | integer                  | 文件对象ID。           |
| bucketid     | smallint                 | 一致性hash bucket ID。 |
| forknum      | integer                  | 文件类型。             |
| error_count  | integer                  | 出现校验失败的次数。   |
| first_time   | timestamp with time zone | 第一次出现时间。       |
| last_time    | timestamp with time zone | 最近一次出现时间。     |
