---
title: PG_TDE_INFO
summary: PG_TDE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_TDE_INFO

PG_TDE_INFO视图提供了MogDB加密信息。

**表 1** PG_TDE_INFO字段

| 名称       | 类型    | 描述                                                        |
| :--------- | :------ | :---------------------------------------------------------- |
| is_encrypt | Boolean | 是否加密MogDB。<br/>- f: 非加密MogDB。<br/>- t: 加密MogDB。 |
| g_tde_algo | text    | 加密算法。<br/>- SM4-CTR-128<br/>- AES-CTR-128              |
| remain     | text    | 保留字段。                                                  |
