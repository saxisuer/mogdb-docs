---
title: GS_WLM_OPERATOR_HISTORY
summary: GS_WLM_OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_OPERATOR_HISTORY

GS_WLM_OPERATOR_HISTORY视图显示的是当前用户当前数据库主节点上执行作业结束后的算子的相关记录。

记录的数据同GS_WLM_OPERATOR_INFO -&gt; 表1。
