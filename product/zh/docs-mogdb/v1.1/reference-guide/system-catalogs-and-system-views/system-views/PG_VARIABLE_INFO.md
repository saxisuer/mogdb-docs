---
title: PG_VARIABLE_INFO
summary: PG_VARIABLE_INFO
author: Guo Huan
date: 2021-04-19
---

# PG_VARIABLE_INFO

PG_VARIABLE_INFO视图用于查询MogDB中当前节点的xid、oid的状态。

**表 1** PG_VARIABLE_INFO字段

| 名称                  | 类型 | 描述                                     |
| :-------------------- | :--- | :--------------------------------------- |
| node_name             | text | 节点名称。                               |
| nextOid               | oid  | 该节点下一次生成的oid。                  |
| nextXid               | xid  | 该节点下一次生成的事务号。               |
| oldestXid             | xid  | 该节点最老的事务号。                     |
| xidVacLimit           | xid  | 强制autovacuum的临界点。                 |
| oldestXidDB           | oid  | 该节点datafrozenxid最小的数据库oid。     |
| lastExtendCSNLogpage  | xid  | 最后一次扩展csnlog的页面号。             |
| startExtendCSNLogpage | xid  | csnlog扩展的起始页面号。                 |
| nextCommitSeqNo       | xid  | 该节点下次生成的csn号。                  |
| latestCompletedXid    | xid  | 该节点提交或者回滚后节点上的最新事务号。 |
| startupMaxXid         | xid  | 该节点关机前的最后一个事务号。           |
