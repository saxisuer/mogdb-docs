---
title: GS_WLM_SESSION_INFO_ALL
summary: GS_WLM_SESSION_INFO_ALL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_SESSION_INFO_ALL

GS_WLM_SESSION_INFO_ALL视图显示在数据库主节点上执行作业结束后的负载管理记录。

**表 1** GS_WLM_SESSION_INFO_ALL字段

| 名称                | 类型     | 描述                                           |
| :------------------ | :------- | :--------------------------------------------- |
| userid              | oid      | 用户OID。                                      |
| username            | name     | 用户名。                                       |
| sysadmin            | boolean  | 是否是管理员用户。                             |
| rpoid               | oid      | 关联的资源池的OID。                            |
| respool             | name     | 关联的资源池的名称。                           |
| parentid            | oid      | 用户组的OID。                                  |
| totalspace          | bigint   | 用户的可用空间上限。                           |
| spacelimit          | bigint   | 用户表空间限制。                               |
| childcount          | interger | 子用户的个数。                                 |
| childlist           | text     | 子用户列表。                                   |
| n_returned_rows     | bigint   | SELECT返回的结果集行数。                       |
| n_tuples_fetched    | bigint   | 随机扫描行。                                   |
| n_tuples_returned   | bigint   | 顺序扫描行。                                   |
| n_tuples_inserted   | bigint   | 插入行。                                       |
| n_tuples_updated    | bigint   | 更新行。                                       |
| n_tuples_deleted    | bigint   | 删除行。                                       |
| n_blocks_fetched    | bigint   | buffer的块访问次数。                           |
| n_blocks_hit        | bigint   | buffer的块命中次数。                           |
| db_time             | bigint   | 有效的DB时间花费，多线程将累加（单位: 微秒）。 |
| cpu_time            | bigint   | CPU时间（单位: 微秒）。                        |
| execution_time      | bigint   | 执行器内执行时间（单位: 微秒）。               |
| parse_time          | bigint   | SQL解析时间（单位: 微秒）。                    |
| plan_time           | bigint   | SQL生成计划时间（单位: 微秒）。                |
| rewrite_time        | bigint   | SQL重写时间（单位: 微秒）。                    |
| pl_execution_time   | bigint   | plpgsql上的执行时间（单位: 微秒）。            |
| pl_compilation_time | bigint   | plpgsql上的编译时间（单位: 微秒）。            |
| net_send_time       | bigint   | 网络上的时间花费（单位: 微秒）。               |
| data_io_time        | bigint   | IO上的时间花费（单位: 微秒）。                 |
| is_slow_query       | bigint   | 是否是慢SQL记录。                              |
