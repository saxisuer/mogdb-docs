---
title: 安全函数
summary: 安全函数
author: Zhang Cuiping
date: 2021-04-20
---

# 安全函数

- gs_encrypt_aes128(encryptstr,keystr)

  描述: 以keystr为密钥对encryptstr字符串进行加密，返回加密后的字符串。keystr的长度范围为8~16字节，至少包含3种字符（大写字母、小写字母、数字、特殊字符）。

  返回值类型: text

  返回值长度: 至少为92字节，不超过4*[(Len+68)/3]字节，其中Len为加密前数据长度（单位为字节）。

  示例:

  ```sql
  mogdb=# SELECT gs_encrypt_aes128('MPPDB','Asdf1234');

                                 gs_encrypt_aes128
  -------------------------------------------------------------------------------------
  gwditQLQG8NhFw4OuoKhhQJoXojhFlYkjeG0aYdSCtLCnIUgkNwvYI04KbuhmcGZp8jWizBdR1vU9CspjuzI0lbz12A=
  (1 row)
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 由于该函数的执行过程需要传入解密口令，为了安全起见，gsql工具不会将该函数记录入执行历史。即无法在gsql里通过上下翻页功能找到该函数的执行历史。

- gs_decrypt_aes128(decryptstr,keystr)

  描述: 以keystr为密钥对decrypt字符串进行解密，返回解密后的字符串。解密使用的keystr必须保证与加密时使用的keystr一致才能正常解密。keystr不得为空。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 此参数需要结合gs_encrypt_aes128加密函数共同使用。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT gs_decrypt_aes128('gwditQLQG8NhFw4OuoKhhQJoXojhFlYkjeG0aYdSCtLCnIUgkNwvYI04KbuhmcGZp8jWizBdR1vU9CspjuzI0lbz12A=','1234');
   gs_decrypt_aes128
  -------------------
   MPPDB
  (1 row)
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 由于该函数的执行过程需要传入解密口令，为了安全起见，gsql工具不会将该函数记录入执行历史；即无法在gsql里通过上下翻页功能找到该函数的执行历史。

- gs_password_deadline

  描述: 显示当前帐户密码离过期还距离多少天。

  返回值类型: interval

  示例:

  ```sql
  mogdb=# SELECT gs_password_deadline();
    gs_password_deadline
  -------------------------
   83 days 17:44:32.196094
  (1 row)
  ```

- gs_password_notifytime

  描述: 显示帐户密码到期前提醒的天数。

  返回值类型: int32

- login_audit_messages

  描述: 查看登录用户的登录信息。

  返回值类型: 元组

  示例:

  - 查看上一次登录认证通过的日期、时间和IP等信息。

    ```sql
    mogdb=> select * from login_audit_messages(true);
     username | database |       logintime        |    mytype     | result | client_conninfo
    ----------+----------+------------------------+---------------+--------+-----------------
     omm      | mogdb | 2020-06-29 21:56:40+08 | login_success | ok     | gsql@[local]
    (1 row)
    ```

  - 查看上一次登录认证失败的日期、时间和IP等信息。

    ```sql
    mogdb=>  select * from login_audit_messages(false) ORDER BY logintime desc limit 1;
     username | database |       logintime        |    mytype    | result |  client_conninfo
    ----------+----------+------------------------+--------------+--------+-------------------
     omm      | mogdb | 2020-06-29 21:57:55+08 | login_failed | failed | [unknown]@[local]
    (1 row)
    ```

  - 查看自从最后一次认证通过以来失败的尝试次数、日期和时间。

    ```sql
    mogdb=>  select * from login_audit_messages(false);
     username | database |       logintime        |    mytype    | result |  client_conninfo
    ----------+----------+------------------------+--------------+--------+-------------------
     omm      | mogdb | 2020-06-29 21:57:55+08 | login_failed | failed | [unknown]@[local]
     omm      | mogdb | 2020-06-29 21:57:53+08 | login_failed | failed | [unknown]@[local]
    (2 rows)
    ```

- login_audit_messages_pid

  描述: 查看登录用户的登录信息。与login_audit_messages的区别在于结果基于当前backendid向前查找。所以不会因为同一用户的后续登录，而影响本次登录的查询结果。也就是查询不到该用户后续登录的信息。

  返回值类型: 元组

  示例:

  - 查看上一次登录认证通过的日期、时间和IP等信息。

    ```sql
    mogdb=> SELECT * FROM login_audit_messages_pid(true);
     username | database |       logintime        |    mytype     | result | client_conninfo |    backendid
    ----------+----------+------------------------+---------------+--------+-----------------+-----------------
     omm      | mogdb | 2020-06-29 21:56:40+08 | login_success | ok     | gsql@[local]    | 139823109633792
    (1 row)
    ```

  - 查看上一次登录认证失败的日期、时间和IP等信息。

    ```sql
    mogdb=> SELECT * FROM login_audit_messages_pid(false) ORDER BY logintime desc limit 1;
     username | database |       logintime        |    mytype    | result |  client_conninfo  |    backendid
    ----------+----------+------------------------+--------------+--------+-------------------+-----------------
     omm      | mogdb | 2020-06-29 21:57:55+08 | login_failed | failed | [unknown]@[local] | 139823109633792
    (1 row)
    ```

  - 查看自从最后一次认证通过以来失败的尝试次数、日期和时间。

    ```sql
    mogdb=> SELECT * FROM login_audit_messages_pid(false);
     username | database |       logintime        |    mytype    | result |  client_conninfo  |    backendid
    ----------+----------+------------------------+--------------+--------+-------------------+-----------------
     omm      | mogdb | 2020-06-29 21:57:55+08 | login_failed | failed | [unknown]@[local] | 139823109633792
     omm      | mogdb | 2020-06-29 21:57:53+08 | login_failed | failed | [unknown]@[local] | 139823109633792
    (2 rows)
    ```

- inet_server_addr

  描述: 显示服务器IP信息。

  返回值类型: inet

  示例:

  ```sql
  mogdb=# SELECT inet_server_addr();
   inet_server_addr
  ------------------
   10.10.0.13
  (1 row)
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
  >
  > - 上面是以客户端在10.10.0.50上，服务器端在10.10.0.13上为例。
  > - 如果是通过本地连接，使用此接口显示为空。

- inet_client_addr

  描述: 显示客户端IP信息。

  返回值类型: inet

  示例:

  ```sql
  mogdb=# SELECT inet_client_addr();
   inet_client_addr
  ------------------
   10.10.0.50
  (1 row)
  ```

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
  >
  > - 上面是以客户端在10.10.0.50上，服务器端在10.10.0.13上为例。
  > - 如果是通过本地连接，使用此接口显示为空。

- pg_query_audit

  描述: 查看数据库主节点审计日志。

  返回值类型: record

  函数返回字段如下:

  | 名称            | 类型                     | 描述             |
  | :-------------- | :----------------------- | :--------------- |
  | time            | timestamp with time zone | 操作时间         |
  | type            | text                     | 操作类型         |
  | result          | text                     | 操作结果         |
  | username        | text                     | 执行操作的用户名 |
  | database        | text                     | 数据库名称       |
  | client_conninfo | text                     | 客户端连接信息   |
  | object_name     | text                     | 操作对象名称     |
  | detail_info     | text                     | 执行操作详细信息 |
  | node_name       | text                     | 节点名称         |
  | thread_id       | text                     | 线程id           |
  | local_port      | text                     | 本地端口         |
  | remote_port     | text                     | 远端端口         |

  函数使用方法及示例请参考查看审计结果。

- pg_delete_audit

  描述: 删除指定时间段的审计日志。

  返回值类型: void

  函数使用方法及示例请参考维护审计日志。
