---
title: 其它函数
summary: 其它函数
author: Zhang Cuiping
date: 2021-04-20
---

# 其它函数

- plan_seed()

  描述: 获取前一次查询语句的seed值（内部使用）。

  返回值类型: int

- pg_stat_get_env()

  描述: 获取当前节点的环境变量信息，仅sysadmin和monitor admin可以访问。

  返回值类型: record

  示例:

  ```sql
  mogdb=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/MogDB_Kernel_TRUNK/install,/data1/MogDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  描述: 清理节点上无人使用的全局计划缓存。

  返回值类型: bool

- pg_catalog.plancache_status()

  描述: 显示节点上全局计划缓存的信息，函数返回信息和GLOBAL_PLANCACHE_STATUS一致。

  返回值类型: record

- textlen()

  描述: 提供查询text的逻辑长度的方法。

  返回值类型: int

- threadpool_status()

  描述: 显示线程池中工作线程及会话的状态信息。

  返回值类型: record

- get_local_active_session()

  描述: 提供当前节点保存在内存中的历史活跃session状态的采样记录。

  返回值类型: record

- get_global_active_session()

  描述: 提供所有节点保存在内存中的历史活跃session状态的采样记录。

  返回值类型: record

- get_global_gs_asp()

  描述: 提供所有节点保存在系统表gs_asp中的历史活跃session状态的采样记录。

  返回值类型: record

- get_datanode_active_session()

  描述: 提供从主节点查询数据库其它节点上保存在内存中的历史活跃session状态的采样记录。

  返回值类型: record

- get_datanode_active_session_hist()

  描述: 提供从主节点查询数据库其它节点上保存在系统表gs_asp中的历史活跃session状态的采样记录。

  返回值类型: record

- pg_stat_get_thread()

  描述: 提供当前节点下所有线程的状态信息。

  返回值类型: record

- pg_stat_get_sql_count()

  描述: 提供当前节点中所有用户执行的SELECT/UPDATE/INSERT/DELETE/MERGE INTO语句的计数结果。

  返回值类型: record

- pg_stat_get_data_senders()

  描述: 提供当前活跃的数据复制发送线程的详细信息。

  返回值类型: record

- generate_wdr_report(begin_snap_id Oid, end_snap_id Oid, int report_type, int report_scope, int node_name)

  描述: 基于两个snapshot生成系统诊断报告。

  返回值类型: record

  **表 1** generate_wdr_report 参数说明

  | 参数          | 说明                                                         | 取值范围                                        |
  | ------------- | ------------------------------------------------------------ | ----------------------------------------------- |
  | begin_snap_id | 生成某段时间内性能诊断报告的开始snapshotid。                 | -                                               |
  | end_snap_id   | 结束snapshot的id，默认end_snap_id大于begin_snap_id。         | -                                               |
  | report_type   | 指定生成report的类型。                                       | summarydetailall，即同时包含summary 和 detail。 |
  | report_scope  | 指定生成report的范围。                                       | node: 数据库MogDB中的节点。                     |
  | node_name     | 在"report_scope"指定为"node"时，需要把该参数指定为对应节点的名称。 | -                                               |

- create_wdr_snapshot(stat text, dbid Oid)

  描述: 手工生成系统诊断快照。

  返回值类型: record

  **表 2** create_wdr_snapshot参数说明

  | 参数 | 描述                                                         |
  | ---- | ------------------------------------------------------------ |
  | stat | 表示每次snapshot时节点的名称，默认为收集所有节点的状态信息。 |
  | dbid | 表示数据库的ID，收集指定数据库的状态信息。                   |

- reset_unique_sql

  描述: 用来清理数据库节点内存中的Unique SQL（需要sysadmin权限）。

  返回值类型: bool

  **表 3** reset_unique_sql参数说明

  | 参数        | 类型 | 描述                                                         |
  | ----------- | ---- | ------------------------------------------------------------ |
  | scope       | text | 清理范围类型: 'GLOBAL': 清理所有的节点，如果是'GLOBAL'，则只可以为主节点执行此函数。'LOCAL': 清理本节点。 |
  | clean_type  | text | 'BY_USERID': 按用户ID来进行清理Unique SQL。'BY_CNID': 按主节点的ID来进行清理Unique SQL。'ALL': 全部清理。 |
  | clean_value | int8 | 具体清理type对应的清理值。                                   |

- wdr_xdb_query(db_name_str text, query text)

  描述: 提供本地跨数据库执行query的能力。例如: 在连接到mogdb库时, 访问test库下的表。

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  返回值类型: record

- pg_wlm_jump_queue(pid int)

  描述: 调整任务到数据库主节点队列的最前端。

  返回值类型: boolean

  - true: 成功。
  - false: 失败。

- gs_wlm_switch_cgroup(pid int, cgroup text)

  描述: 调整作业的优先级到新控制组。

  返回值类型: boolean

  - true: 成功。
  - false: 失败。

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

  描述: 将线程tid的MemoryContextName内存上下文信息记录到"$GAUSSLOG/pg_log/${node_name}/dumpmem"目录下的"threadid_timestamp.log"文件中。其中threadid可通过表PV_SESSION_MEMORY_DETAIL中的sessid后获得。在正式发布的版本中仅接受MemoryContextName为空串（两个单引号表示输入为空串，即"）的输入，此时会记录所有的内存上下文信息，否则不会有任何操作。对供内部开发人员和测试人员调试用的DEBUG版本，可以指定需要统计的MemoryContextName，此时会将该Context所有的内存使用情况记录到指定文件。该函数需要管理员权限的用户才能执行。

  返回值类型: boolean

  - true: 成功。
  - false: 失败。

- pg_shared_memctx_detail(MemoryContextName text)

  描述: 将MemoryContextName内存上下文信息记录到"$GAUSSLOG/pg_log/${node_name}/dumpmem"目录下的"threadid_timestamp.log"文件中。该函数功能仅在DEBUG版本中供内部开发人员和测试人员调试使用，在正式发布版本中调用该函数不会有任何操作。该函数需要管理员权限的用户才能执行。

  返回值类型: boolean

  - true: 成功。
  - false: 失败。

- local_bgwriter_stat()

  描述: 显示本实例的bgwriter线程刷页信息，候选buffer链中页面个数，buffer淘汰信息。

  返回值类型: record

- local_ckpt_stat()

  描述: 显示本实例的检查点信息和各类日志刷页情况。

  返回值类型: record

- local_double_write_stat()

  描述: 显示本实例的双写文件的情况。

  返回值类型: record

- local_single_flush_dw_stat()

  描述: 显示本实例的单页面淘汰双写文件的情况。

  返回值类型: record

- local_pagewriter_stat()

  描述: 显示本实例的刷页信息和检查点信息。

  返回值类型: record

- local_redo_stat()

  描述: 显示本实例的备机的当前回放状态。

  返回值类型: record

  备注: 返回的回放状态主要包括当前回放位置，回放最小恢复点位置等信息。

- local_recovery_status()

  描述: 显示本实例的主机和备机的日志流控信息。

  返回值类型: record

- local_rto_status()

  描述: 显示本实例的主机和备机的日志流控信息。

  返回值类型: record

- gs_wlm_node_recover(boolean isForce)

  描述: 获取当前内存中记录的TopSQL查询语句级别相关统计信息，当传入的参数不为0时，会将这部分信息从内存中清理掉。

  返回值类型: record

- gs_cgroup_map_ng_conf(group name)

  描述: 读取指定逻辑数据库的cgroup配置文件。

  返回值类型: record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  描述: 切换指定会话的控制组。

  返回值类型: record

- hdfs_fdw_handler()

  描述: 用于外表重写功能，定义外表时需要定义的函数。

  返回值类型: record

- hdfs_fdw_validator(text[], oid)

  描述: 用于外表重写功能，定义外表时需要定义的函数。

  返回值类型: record

- comm_client_info()

  描述: 用于查询单个节点活跃的客户端连接信息。

  返回值类型: setof record

- pg_sync_cstore_delta(text)

  描述: 同步指定列存表的delta表表结构，使其与列存表主表一致。

  返回值类型: bigint

- pg_sync_cstore_delta()

  描述: 同步所有列存表的delta表表结构，使其与列存表主表一致。

  返回值类型: bigint

- gs_create_log_tables()

  描述: 用于创建运行日志和性能日志的外表和视图。

  返回值类型: void

  示例:

  ```sql
  mogdb=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述: 获取数据库级的全量SQL(Full SQL)信息。

  返回值类型: record

  **表 4** dbe_perf.get_global_full_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | --------------- | ------------------------ | ----------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述: 获取数据库级的慢SQL(Slow SQL)信息。

  返回值类型: record

  **表 5** dbe_perf.get_global_slow_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | --------------- | ------------------------ | ----------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- statement_detail_decode(detail text, format text, pretty boolean)

  描述: 解析全量/慢SQL语句中的details字段的信息。

  返回值类型: text

  **表 6** statement_detail_decode参数说明

  | 参数   | 类型    | 描述                                                         |
  | ------ | ------- | ------------------------------------------------------------ |
  | detail | text    | SQL语句产生的事件的集合(不可读)。                            |
  | format | text    | 解析输出格式，取值为 plaintext。                             |
  | pretty | boolean | 当format为plaintext时，是否以优雅的格式展示: true表示通过"\\n"分隔事。false表示通过","分隔事件。 |

- pg_control_system()

  描述: 返回系统控制文件状态。

  返回类型: SETOF record

- pg_control_checkpoint()

  描述: 返回系统检查点状态。

  返回类型: SETOF record

- get_delta_info

  描述: 用于获取某个列存表的delta表中数据存储情况。

  参数: rel text

  返回值类型: part_name text, total_live_tuple bigint, total_data_size bigint, max_blocknum bigint

- get_prepared_pending_xid

  描述: 当恢复完成时，返回nextxid。

  参数: nan

  返回值类型: text

- pg_clean_region_info

  描述: 清理regionmap。

  参数: nan

  返回值类型: character varying

- pg_get_delta_info

  描述: 从单个dn获取delta info。

  参数: rel text, schema_name text

  返回值类型: part_name text, live_tuple bigint, data_size bigint, blocknum bigint

- pg_get_replication_slot_name

  描述: 获取slot name。

  参数: nan

  返回值类型: text

- pg_get_running_xacts

  描述: 获取运行中的xact。

  参数: nan

  返回值类型: handle integer, gxid xid, state tinyint, node text, xmin xid, vacuum boolean, timeline bigint, prepare_xid xid, pid bigint, next_xid xid

- pg_get_variable_info

  描述: 获取共享内存变量cache。

  参数: nan

  返回值类型: node_name text, nextOid oid, nextXid xid, oldestXid xid, xidVacLimit xid, oldestXidDB oid, lastExtendCSNLogpage xid, startExtendCSNLogpage xid, nextCommitSeqNo xid, latestCompletedXid xid, startupMaxXid xid

- pg_get_xidlimit

  描述: 从共享内存获取事物id信息。

  参数: nan

  返回值类型: nextXid xid, oldestXid xid, xidVacLimit xid, xidWarnLimit xid, xidStopLimit xid, xidWrapLimit xid, oldestXidDB oid

- pg_relation_compression_ratio

  描述: 查询表压缩率，默认返回1.0。

  参数: text

  返回值类型: real

- pg_relation_with_compression

  描述: 查询表是否压缩。

  参数: text

  返回值类型: boolean

- pg_stat_file_recursive

  描述: 列出路径下所有文件。

  参数: location text

  返回值类型: path text, filename text, size bigint, isdir boolean

- pg_stat_get_activity_for_temptable

  描述: 返回临时表相关的后台进程的记录。

  参数: nan

  返回值类型: datid oid, timelineid integer, tempid integer, sessionid bigint

- pg_stat_get_activity_ng

  描述: 返回nodegroup相关的后台进程的记录。

  参数: pid bigint

  返回值类型: datid oid, pid bigint, sessionid bigint, node_group text

- pg_stat_get_cgroup_info

  描述: 返回cgroup信息。

  参数: nan

  返回值类型: cgroup_name text, percent integer, usage_percent integer, shares bigint, usage bigint, cpuset text, relpath text, valid text, node_group text

- pg_stat_get_realtime_info_internal

  描述: 返回实时信息，当前该接口已不可用，返回FailedToGetSessionInfo。

  参数: oid, oid, bigint, cstring, oid

  返回值类型: text

- pg_stat_get_session_wlmstat

  描述: 返回当前会话负载信息。

  参数: pid integer

  返回值类型: datid oid, threadid bigint, sessionid bigint, threadpid integer, usesysid oid, appname text, query text, priority bigint, block_time bigint, elapsed_time bigint, total_cpu_time bigint, skew_percent integer, statement_mem integer, active_points integer, dop_value integer, current_cgroup text, current_status text, enqueue_state text, attribute text, is_plana boolean, node_group text, srespool name

- pg_stat_get_wlm_ec_operator_info

  描述: 从内部哈希表中获取EC执行计划算子信息。

  参数: nan

  返回值类型: queryid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, ec_operator integer, ec_status text, ec_execute_datanode text, ec_dsn text, ec_username text, ec_query text, ec_libodbc_type text, ec_fetch_count bigint

- pg_stat_get_wlm_instance_info

  描述: 返回当前实例负载信息。

  参数: nan

  返回值类型: instancename text, timestamp timestamp with time zone, used_cpu integer, free_memory integer, used_memory integer, io_await double precision, io_util double precision, disk_read double precision, disk_write double precision, process_read bigint, process_write bigint, logical_read bigint, logical_write bigint, read_counts bigint, write_counts bigint

- pg_stat_get_wlm_instance_info_with_cleanup

  描述: 返回当前实例负载信息，并且保存到系统表中。

  参数: nan

  返回值类型: instancename text, timestamp timestamp with time zone, used_cpu integer, free_memory integer, used_memory integer, io_await double precision, io_util double precision, disk_read double precision, disk_write double precision, process_read bigint, process_write bigint, logical_read bigint, logical_write bigint, read_counts bigint, write_counts bigint

- pg_stat_get_wlm_node_resource_info

  描述: 获取当前节点资源信息。

  参数: nan

  返回值类型: min_mem_util integer, max_mem_util integer, min_cpu_util integer, max_cpu_util integer, min_io_util integer, max_io_util integer, used_mem_rate integer

- pg_stat_get_wlm_operator_info

  描述: 从内部哈希表中获取执行计划算子信息。

  参数: nan

  返回值类型: queryid bigint, pid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, query_dop integer, estimated_rows bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, warning text

- pg_stat_get_wlm_realtime_ec_operator_info

  描述: 从内部哈希表中获取EC执行计划算子信息。

  参数: nan

  返回值类型: queryid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, ec_operator integer, ec_status text, ec_execute_datanode text, ec_dsn text, ec_username text, ec_query text, ec_libodbc_type text, ec_fetch_count bigint

- pg_stat_get_wlm_realtime_operator_info

  描述: 从内部哈希表中获取实时执行计划算子信息。

  参数: nan

  返回值类型: queryid bigint, pid bigint, plan_node_id integer, plan_node_name text, start_time timestamp with time zone, duration bigint, status text, query_dop integer, estimated_rows bigint, tuple_processed bigint, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, warning text

- pg_stat_get_wlm_realtime_session_info

  描述: 返回实时会话负载信息。

  参数: nan

  返回值类型: nodename text, threadid bigint, block_time bigint, duration bigint, estimate_total_time bigint, estimate_left_time bigint, schemaname text, query_band text, spill_info text, control_group text, estimate_memory integer, min_peak_memory integer, max_peak_memory integer, average_peak_memory integer, memory_skew_percent integer, min_spill_size integer, max_spill_size integer, average_spill_size integer, spill_skew_percent integer, min_dn_time bigint, max_dn_time bigint, average_dn_time bigint, dntime_skew_percent integer, min_cpu_time bigint, max_cpu_time bigint, total_cpu_time bigint, cpu_skew_percent integer, min_peak_iops integer, max_peak_iops integer, average_peak_iops integer, iops_skew_percent integer, warning text, query text, query_plan text, cpu_top1_node_name text, cpu_top2_node_name text, cpu_top3_node_name text, cpu_top4_node_name text, cpu_top5_node_name text, mem_top1_node_name text, mem_top2_node_name text, mem_top3_node_name text, mem_top4_node_name text, mem_top5_node_name text, cpu_top1_value bigint, cpu_top2_value bigint, cpu_top3_value bigint, cpu_top4_value bigint, cpu_top5_value bigint, mem_top1_value bigint, mem_top2_value bigint, mem_top3_value bigint, mem_top4_value bigint, mem_top5_value bigint, top_mem_dn text, top_cpu_dn text

- pg_stat_get_wlm_session_info_internal

  描述: 返回会话负载信息。

  参数: oid, bigint, bigint, oid

  返回值类型: SETOF text

- pg_stat_get_wlm_session_iostat_info

  描述: 返回会话负载IO信息。

  参数: nan

  返回值类型: threadid bigint, maxcurr_iops integer, mincurr_iops integer, maxpeak_iops integer, minpeak_iops integer, iops_limits integer, io_priority integer

- pg_stat_get_wlm_statistics

  描述: 返回会话负载统计数据。

  参数: nan

  返回值类型: statement text, block_time bigint, elapsed_time bigint, total_cpu_time bigint, qualification_time bigint, skew_percent integer, control_group text, status text, action text

- pg_stat_get_workload_struct_info

  描述: 返回负载管理数据结构。

  参数: nan

  返回值类型: text

- pg_test_err_contain_err

  描述: 测试错误类型和返回信息。

  参数: integer

  返回值类型: void

- pv_session_memory_detail_tp

  描述: 返回会话的内存使用情况，参考pv_session_memory_detail。

  参数: nan

  返回值类型: sessid text, sesstype text, contextname text, level smallint, parent text, totalsize bigint, freesize bigint, usedsize bigint
