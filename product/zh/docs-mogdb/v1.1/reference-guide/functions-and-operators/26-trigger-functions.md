---
title: 触发器函数
summary: 触发器函数
author: Zhang Cuiping
date: 2021-04-20
---

# 触发器函数

- pg_get_triggerdef(oid)

  描述: 获取触发器的定义信息。

  参数: 待查触发器的OID。

  返回值类型: text

  示例:

  ```sql
  mogdb=# select pg_get_triggerdef(oid) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN ((new.a IS NOT NULL)) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)
  ```

- pg_get_triggerdef(oid, boolean)

  描述: 获取触发器的定义信息。

  参数: 待查触发器的OID及是否以pretty方式展示。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** 仅在创建trigger时指定WHEN条件的情况下，布尔类型参数才生效。

  返回值类型: text

  示例:

  ```sql
  mogdb=# select pg_get_triggerdef(oid,true) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN (new.a IS NOT NULL) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)

  mogdb=# select pg_get_triggerdef(oid,false) from pg_trigger;
                                                                                       pg_get_triggerdef
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   CREATE TRIGGER tg1 BEFORE INSERT ON gtest26 FOR EACH STATEMENT EXECUTE PROCEDURE gtest_trigger_func()
   CREATE TRIGGER tg03 AFTER INSERT ON gtest26 FOR EACH ROW WHEN ((new.a IS NOT NULL)) EXECUTE PROCEDURE gtest_trigger_func()
  (2 rows)
  ```
