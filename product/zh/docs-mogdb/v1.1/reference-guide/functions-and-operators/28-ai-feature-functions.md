---
title: AI特性函数
summary: AI特性函数
author: Zhang Cuiping
date: 2021-04-20
---

# AI特性函数

- gs_index_advise(text)

  描述: 针对单条查询语句推荐索引。

  参数: SQL语句字符串

  返回值类型: record

  示例请参见单query索引推荐。

- hypopg_create_index(text)

  描述: 创建虚拟索引。

  参数: 创建索引语句的字符串

  返回值类型: record

  示例请参见虚拟索引。

- hypopg_display_index()

  描述: 显示所有创建的虚拟索引信息。

  参数: 无

  返回值类型: record

  示例请参见虚拟索引。

- hypopg_drop_index(oid)

  描述: 删除指定的虚拟索引。

  参数: 索引的oid

  返回值类型: bool

  示例请参见虚拟索引。

- hypopg_reset_index()

  描述: 清除所有虚拟索引。

  参数: 无

  返回值类型: 无

  示例请参见虚拟索引。

- hypopg_estimate_size(oid)

  描述: 估计指定索引创建所需的空间大小。

  参数: 索引的oid

  返回值类型: int8

  示例请参见虚拟索引。
