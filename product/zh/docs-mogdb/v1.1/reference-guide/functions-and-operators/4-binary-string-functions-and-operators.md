---
title: 二进制字符串函数和操作符
summary: 二进制字符串函数和操作符
author: Zhang Cuiping
date: 2021-04-20
---

# 二进制字符串函数和操作符

<span style="font:14px">**字符串操作符**</span>

SQL定义了一些字符串函数，在这些函数里使用关键字而不是逗号来分隔参数。

- octet_length(string)

  描述: 二进制字符串中的字节数。

  返回值类型: int

  示例:

  ```sql
  mogdb=# SELECT octet_length(E'jo\\000se'::bytea) AS RESULT;
   result
  --------
        5
  (1 row)
  ```

- overlay(string placing string from int [for int])

  描述: 替换子串。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT overlay(E'Th\\000omas'::bytea placing E'\\002\\003'::bytea from 2 for 3) AS RESULT;
       result
  ----------------
   \x5402036d6173
  (1 row)
  ```

- position(substring in string)

  描述: 特定子字符串的位置。

  返回值类型: int

  示例:

  ```sql
  mogdb=# SELECT position(E'\\000om'::bytea in E'Th\\000omas'::bytea) AS RESULT;
   result
  --------
        3
  (1 row)
  ```

- substring(string [from int] [for int])

  描述: 截取子串。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT substring(E'Th\\000omas'::bytea from 2 for 3) AS RESULT;
    result
  ----------
   \x68006f
  (1 row)
  ```

- substr(string, from int [, for int])

  描述: 截取子串。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT substr(E'Th\\000omas'::bytea from 2 for 3) AS RESULT;
    result
  ----------
   \x68006f
  (1 row)
  ```

- trim([both] bytes from string)

  描述: 从string的开头和结尾删除只包含bytes中字节的最长字符串。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT trim(E'\\000'::bytea from E'\\000Tom\\000'::bytea) AS RESULT;
    result
  ----------
   \x546f6d
  (1 row)
  ```

<span style="font:14px">**二进制字符串函数**</span>

MogDB也提供了函数调用所使用的常用语法。

- btrim(string bytea,bytes bytea)

  描述: 从string的开头和结尾删除只包含bytes中字节的最长的字符串。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT btrim(E'\\000trim\\000'::bytea, E'\\000'::bytea) AS RESULT;
     result
  ------------
   \x7472696d
  (1 row)
  ```

- get_bit(string, offset)

  描述: 从字符串中抽取位。

  返回值类型: int

  示例:

  ```sql
  mogdb=# SELECT get_bit(E'Th\\000omas'::bytea, 45) AS RESULT;
   result
  --------
        1
  (1 row)
  ```

- get_byte(string, offset)

  描述: 从字符串中抽取字节。

  返回值类型: int

  示例:

  ```sql
  mogdb=# SELECT get_byte(E'Th\\000omas'::bytea, 4) AS RESULT;
   result
  --------
      109
  (1 row)
  ```

- set_bit(string,offset, newvalue)

  描述: 设置字符串中的位。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT set_bit(E'Th\\000omas'::bytea, 45, 0) AS RESULT;
        result
  ------------------
   \x5468006f6d4173
  (1 row)
  ```

- set_byte(string,offset, newvalue)

  描述: 设置字符串中的字节。

  返回值类型: bytea

  示例:

  ```sql
  mogdb=# SELECT set_byte(E'Th\\000omas'::bytea, 4, 64) AS RESULT;
        result
  ------------------
   \x5468006f406173
  (1 row)
  ```
