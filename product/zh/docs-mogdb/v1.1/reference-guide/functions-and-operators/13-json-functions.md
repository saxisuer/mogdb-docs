---
title: JSON函数
summary: JSON函数
author: Zhang Cuiping
date: 2021-04-20
---

# JSON函数

JSON函数表示可以用于JSON类型（请参考"JSON类型"）数据的函数。

- array_to_json(anyarray [, pretty_bool])

  描述: 返回JSON类型的数组。一个多维数组成为一个JSON数组的数组。如果pretty_bool为true，将在一维元素之间添加换行符。

  返回类型: json

  示例:

  ```sql
  mogdb=# SELECT array_to_json('{{1,5},{99,100}}'::int[]);
  array_to_json
  ------------------
  [[1,5],[99,100]]
  (1 row)
  ```

- row_to_json(record [, pretty_bool])

  描述: 返回JSON类型的行。如果pretty_bool为true，将在第一级元素之间添加换行符。

  返回类型: json

  示例:

  ```sql
  mogdb=# SELECT row_to_json(row(1,'foo'));
       row_to_json
  ---------------------
   {"f1":1,"f2":"foo"}
  (1 row)
  ```
