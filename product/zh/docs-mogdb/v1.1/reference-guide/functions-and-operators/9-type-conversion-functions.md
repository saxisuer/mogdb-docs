---
title: 类型转换函数
summary: 类型转换函数
author: Zhang Cuiping
date: 2021-04-20
---

# 类型转换函数

- cast(x as y)

  描述: 类型转换函数，将x转换成y指定的类型。

  示例:

  ```sql
  mogdb=# SELECT cast('22-oct-1997' as timestamp);
        timestamp
  ---------------------
   1997-10-22 00:00:00
  (1 row)
  ```

- hextoraw(string)

  描述: 将一个十六进制构成的字符串转换为二进制。

  返回值类型: raw

  示例:

  ```sql
  mogdb=# SELECT hextoraw('7D');
   hextoraw
  ----------
   7D
  (1 row)
  ```

- numtoday(numeric)

  描述: 将数字类型的值转换为指定格式的时间戳。

  返回值类型: timestamp

  示例:

  ```sql
  mogdb=# SELECT numtoday(2);
   numtoday
  ----------
   2 days
  (1 row)
  ```

- pg_systimestamp()

  描述: 获取系统时间戳。

  返回值类型: timestamp with time zone

  示例:

  ```sql
  mogdb=# SELECT pg_systimestamp();
          pg_systimestamp
  -------------------------------
   2015-10-14 11:21:28.317367+08
  (1 row)
  ```

- rawtohex(string)

  描述: 将一个二进制构成的字符串转换为十六进制的字符串。

  结果为输入字符的ACSII码，以十六进制表示。

  返回值类型: varchar

  示例:

  ```sql
  mogdb=# SELECT rawtohex('1234567');
      rawtohex
  ----------------
   31323334353637
  (1 row)
  ```

- to_char (datetime/interval [, fmt])

  描述: 将一个DATE、TIMESTAMP、TIMESTAMP WITH TIME ZONE或者TIMESTAMP WITH LOCAL TIME ZONE类型的DATETIME或者INTERVAL值按照fmt指定的格式转换为VARCHAR类型。

  - 可选参数fmt可以为以下几类: 日期、时间、星期、季度和世纪。每类都可以有不同的模板，模板之间可以合理组合，常见的模板有: HH、MM、SS、YYYY、MM、DD。
  - 模板可以有修饰词，常用的修饰词是FM，可以用来抑制前导的零或尾随的空白。

  返回值类型: varchar

  示例:

  ```sql
  mogdb=# SELECT to_char(current_timestamp,'HH12:MI:SS');
   to_char
  ----------
   10:19:26
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char(current_timestamp,'FMHH12:FMMI:FMSS');
   to_char
  ----------
   10:19:46
  (1 row)
  ```

- to_char(double precision, text)

  描述: 将双精度类型的值转换为指定格式的字符串。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT to_char(125.8::real, '999D99');
   to_char
  ---------
    125.80
  (1 row)
  ```

- to_char (integer/number[, fmt])

  描述: 将一个整型或者浮点类型的值转换为指定格式的字符串。

  - 可选参数fmt可以为以下几类: 十进制字符、"分组"符、正负号和货币符号，每类都可以有不同的模板，模板之间可以合理组合，常见的模板有: 9、0、,（千分隔符）、.（小数点）。
  - 模板可以有类似FM的修饰词，但FM不抑制由模板0指定而输出的0。
  - 要将整型类型的值转换成对应16进制值的字符串，使用模板X或x。

  返回值类型: varchar

  示例:

  ```sql
  mogdb=# SELECT to_char(1485,'9,999');
   to_char
  ---------
    1,485
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char( 1148.5,'9,999.999');
    to_char
  ------------
    1,148.500
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char(148.5,'990999.909');
     to_char
  -------------
      0148.500
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_char(123,'XXX');
   to_char
  ---------
     7B
  (1 row)
  ```

- to_char(interval, text)

  描述: 将时间间隔类型的值转换为指定格式的字符串。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT to_char(interval '15h 2m 12s', 'HH24:MI:SS');
   to_char
  ----------
   15:02:12
  (1 row)
  ```

- to_char(int, text)

  描述: 将整数类型的值转换为指定格式的字符串。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT to_char(125, '999');
   to_char
  ---------
    125
  (1 row)
  ```

- to_char(numeric, text)

  描述: 将数字类型的值转换为指定格式的字符串。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT to_char(-125.8, '999D99S');
   to_char
  ---------
   125.80-
  (1 row)
  ```

- to_char (string)

  描述: 将CHAR、VARCHAR、VARCHAR2、CLOB类型转换为VARCHAR类型。

  如使用该函数对CLOB类型进行转换，且待转换CLOB类型的值超出目标类型的范围，则返回错误。

  返回值类型: varchar

  示例:

  ```sql
  mogdb=# SELECT to_char('01110');
   to_char
  ---------
   01110
  (1 row)
  ```

- to_char(timestamp, text)

  描述: 将时间戳类型的值转换为指定格式的字符串。

  返回值类型: text

  示例:

  ```sql
  mogdb=# SELECT to_char(current_timestamp, 'HH12:MI:SS');
   to_char
  ----------
   10:55:59
  (1 row)
  ```

- to_clob(char/nchar/varchar/varchar2/nvarchar2/text/raw)

  描述: 将RAW类型或者文本字符集类型CHAR、NCHAR、VARCHAR、VARCHAR2、NVARCHAR2、TEXT转成CLOB类型。

  返回值类型: clob

  示例:

  ```sql
  mogdb=# SELECT to_clob('ABCDEF'::RAW(10));
   to_clob
  ---------
   ABCDEF
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('hello111'::CHAR(15));
   to_clob
  ----------
   hello111
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss123'::NCHAR(10));
   to_clob
  ----------
   gauss123
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss234'::VARCHAR(10));
   to_clob
  ----------
   gauss234
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss345'::VARCHAR2(10));
   to_clob
  ----------
   gauss345
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('gauss456'::NVARCHAR2(10));
   to_clob
  ----------
   gauss456
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_clob('World222!'::TEXT);
    to_clob
  -----------
   World222!
  (1 row)
  ```

- to_date(text)

  描述: 将文本类型的值转换为指定格式的时间戳。目前只支持两类格式:

  - 格式一: 无分隔符日期，如20150814，需要包括完整的年月日。

  - 格式二: 带分隔符日期，如2014-08-14，分隔符可以是单个任意非数字字符。

  返回值类型: timestamp

  示例:

  ```sql
    mogdb=# SELECT to_date('2015-08-14');
           to_date
    ---------------------
     2015-08-14 00:00:00
    (1 row)
  ```

- to_date(text, text)

  描述: 将字符串类型的值转换为指定格式的日期。

  返回值类型: timestamp

  示例:

  ```sql
  mogdb=# SELECT to_date('05 Dec 2000', 'DD Mon YYYY');
         to_date
  ---------------------
   2000-12-05 00:00:00
  (1 row)
  ```

- to_date(string, fmt)

  描述:

  将字符串string按fmt指定格式转化为DATE类型的值。

  该函数不能直接支持CLOB类型，但是CLOB类型的参数能够通过隐式转换实现。

  返回值类型: date

  示例:

  ```sql
  mogdb=# SELECT TO_DATE('05 Dec 2010','DD Mon YYYY');
         to_date
  ---------------------
   2010-12-05 00:00:00
  (1 row)
  ```

- to_number ( expr [, fmt])

  描述: 将expr按指定格式转换为一个NUMBER类型的值。

  类型转换格式请参考[表1](#数值格式化)。

  转换十六进制字符串为十进制数字时，最多支持16个字节的十六进制字符串转换为无符号数。

  转换十六进制字符串为十进制数字时，格式字符串中不允许出现除'x'或'X'以外的其他字符，否则报错。

  返回值类型: number

  示例:

  ```sql
  mogdb=# SELECT to_number('12,454.8-', '99G999D9S');
   to_number
  -----------
    -12454.8
  (1 row)
  ```

- to_number(text, text)

  描述: 将字符串类型的值转换为指定格式的数字。

  返回值类型: numeric

  示例:

  ```sql
  mogdb=# SELECT to_number('12,454.8-', '99G999D9S');
   to_number
  -----------
    -12454.8
  (1 row)
  ```

- to_timestamp(double precision)

  描述: 把UNIX纪元转换成时间戳。

  返回值类型: timestamp with time zone

  示例:

  ```sql
  mogdb=# SELECT to_timestamp(1284352323);
        to_timestamp
  ------------------------
   2010-09-13 12:32:03+08
  (1 row)
  ```

- to_timestamp(string [,fmt])

  描述: 将字符串string按fmt指定的格式转换成时间戳类型的值。不指定fmt时，按参数nls_timestamp_format所指定的格式转换。

  MogDB的to_timestamp中，

  - 如果输入的年份YYYY=0，系统报错。
  - 如果输入的年份YYYY&lt;0，在fmt中指定SYYYY，则正确输出公元前绝对值n的年份。

  fmt中出现的字符必须与日期/时间格式化的模式相匹配，否则报错。

  返回值类型: timestamp without time zone

  示例:

  ```sql
  mogdb=# SHOW nls_timestamp_format;
      nls_timestamp_format
  ----------------------------
   DD-Mon-YYYY HH:MI:SS.FF AM
  (1 row)

  mogdb=# SELECT to_timestamp('12-sep-2014');
      to_timestamp
  ---------------------
   2014-09-12 00:00:00
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('12-Sep-10 14:10:10.123000','DD-Mon-YY HH24:MI:SS.FF');
        to_timestamp
  -------------------------
   2010-09-12 14:10:10.123
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('-1','SYYYY');
        to_timestamp
  ------------------------
   0001-01-01 00:00:00 BC
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('98','RR');
      to_timestamp
  ---------------------
   1998-01-01 00:00:00
  (1 row)
  ```

  ```sql
  mogdb=# SELECT to_timestamp('01','RR');
      to_timestamp
  ---------------------
   2001-01-01 00:00:00
  (1 row)
  ```

- to_timestamp(text, text)

  描述: 将字符串类型的值转换为指定格式的时间戳。

  返回值类型: timestamp

  示例:

  ```sql
  mogdb=# SELECT to_timestamp('05 Dec 2000', 'DD Mon YYYY');
      to_timestamp
  ---------------------
   2000-12-05 00:00:00
  (1 row)
  ```

**表 1** 数值格式化的模版模式<a id="数值格式化"> </a>

| 模式      | 描述                                  |
| --------- | ------------------------------------- |
| 9         | 带有指定数值位数的值                  |
| 0         | 带前导零的值                          |
| .（句点） | 小数点                                |
| ,（逗号） | 分组（千）分隔符                      |
| PR        | 尖括号内负值                          |
| S         | 带符号的数值（使用区域设置）          |
| L         | 货币符号（使用区域设置）              |
| D         | 小数点（使用区域设置）                |
| G         | 分组分隔符（使用区域设置）            |
| MI        | 在指明的位置的负号（如果数字 &lt; 0） |
| PL        | 在指明的位置的正号（如果数字 &gt; 0） |
| SG        | 在指明的位置的正/负号                 |
| RN        | 罗马数字（输入在 1 和 3999 之间）     |
| TH或th    | 序数后缀                              |
| V         | 移动指定位（小数）                    |
