---
title: 对象标识符类型
summary: 对象标识符类型
author: Guo Huan
date: 2021-04-06
---

# 对象标识符类型

MogDB在内部使用对象标识符（OID）作为各种系统表的主键。系统不会给用户创建的表增加一个OID系统字段，OID类型代表一个对象标识符。

目前OID类型用一个四字节的无符号整数实现。因此不建议在创建的表中使用OID字段做主键。

**表 1** 对象标识符类型

| 名称          | 引用         | 描述         | 示例            |
| :------------ | :----------- | :--------------- | :------------------|
| OID           | -            | 数字化的对象标识符。             | 564182          |
| CID           | -            | 命令标识符。它是系统字段cmin和cmax的数据类型。命令标识符是32位的量。     | -               |
| XID           | -            | 事务标识符。它是系统字段xmin和xmax的数据类型。事务标识符也是32位的量。   | -               |
| TID           | -            | 行标识符。它是系统表字段ctid的数据类型。行ID是一对数值（块号，块内的行索引），它标识该行在其所在表内的物理位置。 | -               |
| REGCONFIG     | pg_ts_config | 文本搜索配置。     | english         |
| REGDICTIONARY | pg_ts_dict   | 文本搜索字典。               | simple          |
| REGOPER       | pg_operator  | 操作符名。                   | -               |
| REGOPERATOR   | pg_operator  | 带参数类型的操作符。         | *(integer,integer)或-(NONE,integer) |
| REGPROC       | pg_proc      | 函数名称。                   | sum             |
| REGPROCEDURE  | pg_proc      | 带参数类型的函数。           | sum(int4)       |
| REGCLASS      | pg_class     | 关系名。 | pg_type         |
| REGTYPE       | pg_type      | 数据类型名。                 | integer         |

OID类型: 主要作为数据库系统表中字段使用。

示例:

```sql
mogdb=# SELECT oid FROM pg_class WHERE relname = 'pg_type';
 oid
------
 1247
(1 row)
```

OID别名类型REGCLASS: 主要用于对象OID值的简化查找。

示例:

```sql
mogdb=# SELECT attrelid,attname,atttypid,attstattarget FROM pg_attribute WHERE attrelid = 'pg_type'::REGCLASS;
 attrelid |  attname   | atttypid | attstattarget
----------+------------+----------+---------------
     1247 | xc_node_id     |       23 |             0
     1247 | tableoid       |       26 |             0
     1247 | cmax           |       29 |             0
     1247 | xmax           |       28 |             0
     1247 | cmin           |       29 |             0
     1247 | xmin           |       28 |             0
     1247 | oid            |       26 |             0
     1247 | ctid           |       27 |             0
     1247 | typname        |       19 |            -1
     1247 | typnamespace   |       26 |            -1
     1247 | typowner       |       26 |            -1
     1247 | typlen         |       21 |            -1
     1247 | typbyval       |       16 |            -1
     1247 | typtype        |       18 |            -1
     1247 | typcategory    |       18 |            -1
     1247 | typispreferred |       16 |            -1
     1247 | typisdefined   |       16 |            -1
     1247 | typdelim       |       18 |            -1
     1247 | typrelid       |       26 |            -1
     1247 | typelem        |       26 |            -1
     1247 | typarray       |       26 |            -1
     1247 | typinput       |       24 |            -1
     1247 | typoutput      |       24 |            -1
     1247 | typreceive     |       24 |            -1
     1247 | typsend        |       24 |            -1
     1247 | typmodin       |       24 |            -1
     1247 | typmodout      |       24 |            -1
     1247 | typanalyze     |       24 |            -1
     1247 | typalign       |       18 |            -1
     1247 | typstorage     |       18 |            -1
     1247 | typnotnull     |       16 |            -1
     1247 | typbasetype    |       26 |            -1
     1247 | typtypmod      |       23 |            -1
     1247 | typndims       |       23 |            -1
     1247 | typcollation   |       26 |            -1
     1247 | typdefaultbin  |      194 |            -1
     1247 | typdefault     |       25 |            -1
     1247 | typacl         |     1034 |            -1
(38 rows)
```
