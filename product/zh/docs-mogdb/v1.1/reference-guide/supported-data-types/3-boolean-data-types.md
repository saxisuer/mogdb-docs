---
title: 布尔类型
summary: 布尔类型
author: Guo Huan
date: 2021-04-06
---

# 布尔类型

**表 1** 布尔类型

| 名称    | 描述     | 存储空间 | 取值                                                   |
| :------ | :------- | :------- | :----------------------------------------------------- |
| BOOLEAN | 布尔类型 | 1字节。  | - true: 真<br/>- false: 假<br/>- null: 未知（unknown） |

"真"值的有效文本值是:

TRUE、't'、'true'、'y'、'yes'、'1'、'TRUE'、true、整数范围内1-2^63-1^、整数范围内-1-2^63^。

"假"值的有效文本值是:

FALSE、'f'、'false'、'n'、'no'、'0'、'FALSE'、false、0。

使用TRUE和FALSE是比较规范的用法（也是SQL兼容的用法）。

## 示例

显示用字母t和f输出Boolean值。

```sql
--创建表。
mogdb=# CREATE TABLE bool_type_t1
(
    BT_COL1 BOOLEAN,
    BT_COL2 TEXT
);

--插入数据。
mogdb=# INSERT INTO bool_type_t1 VALUES (TRUE, 'sic est');

mogdb=# INSERT INTO bool_type_t1 VALUES (FALSE, 'non est');

--查看数据。
mogdb=# SELECT * FROM bool_type_t1;
 bt_col1 | bt_col2
---------+---------
 t       | sic est
 f       | non est
(2 rows)

mogdb=# SELECT * FROM bool_type_t1 WHERE bt_col1 = 't';
 bt_col1 | bt_col2
---------+---------
 t       | sic est
(1 row)

--删除表。
mogdb=# DROP TABLE bool_type_t1;
```
