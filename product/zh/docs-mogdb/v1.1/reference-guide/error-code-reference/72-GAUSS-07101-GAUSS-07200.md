---
title: GAUSS-07101 - GAUSS-07200
summary: GAUSS-07101 - GAUSS-07200
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-07101 - GAUSS-07200

<br/>

## GAUSS-07101 - GAUSS-07110

<br/>

GAUSS-07101: "memory is temporarily unavailable while allocate CBM block array"

SQLSTATE: 53000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07102: "could not force tracking cbm because cbm tracking function is not enabled!"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07103: "the current valid prepared xid list or the next valid prepared list should not be NULL"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07104: "Invalid prepared list lenth: %d"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07105: "could not read two-phase state from xlog at %X/%X, errormsg: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07106: "expected two-phase state data is not present in xlog at %X/%X"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07107: "invalid twophase resource manager lock id"

SQLSTATE: 29000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07108: "cannot commit prepared transaction %lu, it was already aborted"

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07109: "cannot abort prepared transaction %lu, it was already committed"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07110: "Can not connect to gtm when getting gxid, there is a connection error."

SQLSTATE: 08000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07111 - GAUSS-07120

<br/>

GAUSS-07111: "GTM Mode: remote node sub xact can not get gxid directly from gtm"

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07112: "GTM Mode: remote node sub xact xid should be larger than parent xid."

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07113: "GTM-FREE-MODE: latestCompletedXid %lu larger than next alloc xid %lu."

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07114: "cannot have more than 2^32-2 commands in a transaction"

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07115: "Node %s: prepare gid is %s, and top xid is %lu, different transaction!"

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07116: "Failed to receive GTM commit transaction response after CN local PREPARE TRANSACTION '%s'."

SQLSTATE: 08000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07117: "AbortCurrentTransaction: unexpected state %s"

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07118: "non-execute cn or dn: there is already a transaction in progress"

SQLSTATE: 25001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07119: "AbortOutOfAnyTransaction reserving top xact abort: unexpected state %s"

SQLSTATE: 25000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07120: "SUBXACT_TEST %s: Commit subtransaction %s before notice GTM failed."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07121 - GAUSS-07130

<br/>

GAUSS-07121: "xactWillRemoveRelFiles: unknown op code %u"

SQLSTATE: DB001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07122: "the proc group is corrupted, the head is %u, the wakeidx is %u, localProc %p, proc %p, WALInsertLocks %p."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07123: "space reserved for WAL record does not match what was written"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07124: "could not find WAL buffer for %X/%X"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07125: "could not seek in log file %s to offset %u: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07126: "could not write to log file %s at offset %u, length %lu: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07127: "could not open file '%s' (log segment %s): %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07128: "lseek2:could not seek in log file %s to offset %u: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07129: "could not open xlog file '%s' (log segment %s): %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07130: "could not close log file %s: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07131 - GAUSS-07140

<br/>

GAUSS-07131: "could not parse xlog segment file '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07132: "recover failed could not open control file '%s': %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07133: "recover failed could not write to control file: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07134: "recover failed could not fsync control file: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07135: "recover failed could not close control file: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07136: "nNumaNodes is zero."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07137: "XLOGShmemInit num_xloginsert_locks should be multiple of NUMA node number in the system."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07138: "XLOGShmemInit could not alloc memory on node %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07139: "cann't read a random number from file '/dev/urandom'."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07140: "could not open file '%s'"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07141 - GAUSS-07150

<br/>

GAUSS-07141: "lseek file error '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07142: "could not write file '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07143: "could not fsync file '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07144: "could not close file '%s' "

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07145: "hot standby is not possible because wal_level was not set to 'hot_standby' or higher on the master server"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07146: "old version XLog must be shutdown checkpoint or online checkpoint."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07147: "redo can not support old version!!!!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07148: "there are some received xlog have not been redo the tail of last redo lsn:%X/%X, received lsn:%X/%X"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07149: "curMinRecLSN little prev checkpoint lsn is %08X/%08X,now lsn is %08X/%08X"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07150: "current dirty page list head recLSN %08X/%08X smaller than redo lsn %08X/%08X"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07151 - GAUSS-07160

<br/>

GAUSS-07151: "unexpected XLogReadBufferForRedo result when restoring backup block"

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07152: "could not fsync log segment %s: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07153: "could not fsync log file %s: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07154: "could not fsync write-through log file %s: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07155: "could not fdatasync log file %s: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07156: "failed to stat %s file:%m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07157: "must be system admin or replication role or operator admin in operation mode to run a backup"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07158: "file size is wrong, '%s': %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07159: "the file is not exist '%s': %m"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07160: "failed to touch %s file during enable xlog delay: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07161 - GAUSS-07170

<br/>

GAUSS-07161: "could not remove %s file: %m. This will lead to residual of stale xlog segments"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07162: "could not open rewind file: %s"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07163: "could not enable delay ddl when enable_cbm_tracking is off!"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07164: "could not remove %s file: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07165: "failed to write %s file during enable delay ddl recycle"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07166: " could not disable delay ddl when enable_cbm_tracking is off!"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07167: "valid input lsn is not supported while doing forceful disable delay ddl"

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07168: "failed to write %s file during disable delay ddl recycle"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07169: "could not remove %s file after write failure: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07170: "Failed to get cbm information during execute delayed DDL: %s"

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07171 - GAUSS-07180

<br/>

GAUSS-07171: "could not remove %s file before unlink col relation files: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07172: "mpfl_pwrite_file failed, MPFL_FILE_SIZE %d, offset %ld, write_size %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07173: "mpfl_pwrite_file: Write file error"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07174: "mpfl_pwrite_file: Write file size mismatch: expected %d, written %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07175: "Could not remove max_flush_lsn file"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07176: "Could not create file '%s'"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07177: "Could not open file '%s'"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07178: "maximum number of WAL record block references exceeded"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07179: "too many registered buffers"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07180: "too much WAL data"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07181 - GAUSS-07190

<br/>

GAUSS-07181: "no block with id %d registered with WAL insertion"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07182: "invalid xlog info mask %hhx"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07183: "invalid page header in block %u, spc oid %u db oid %u relfilenode %u"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07184: "xlog record length is %u, more than XLogRecordMaxSize %u"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07185: "Must be system admin or operator admin in operation mode to control xlog delay function."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07186: "Must be system admin or operator admin in operation mode to switch transaction log files."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07187: "Must be system admin or operator admin in operation mode to create a restore point."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07188: "Must be system admin or operator admin in operation mode to control recovery."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07189: "Must be system admin or operator admin in operation mode to control ddl delay function."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07190: "could not parse xlog location '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-07191 - GAUSS-07200

<br/>

GAUSS-07191: "Must be system admin or operator admin in operation mode to control resume_bkp_flag function."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07192: "could not close log file %u, segment %lu: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07193: "[REDO_LOG_TRACE]WAL contains references to invalid pages, count:%u"

SQLSTATE: DB001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07194: "can not get xlog lsn from record page block %hhu lsn %lu"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07195: "lsn check error, lsn in record (%lu) ,lsn in current page %lu, page info:%u/%u/%u block %hhu forknum %d lsn %lu blknum:%u"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07196: "failed to locate backup block with ID %d"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07197: "block with WILL_INIT flag in WAL record must be zeroed by redo routine"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07198: "block to be initialized in redo routine must be marked with WILL_INIT flag in the WAL record"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07199: "failed to restore block image"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-07200: "could not seek in log segment %s to offset %u: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
