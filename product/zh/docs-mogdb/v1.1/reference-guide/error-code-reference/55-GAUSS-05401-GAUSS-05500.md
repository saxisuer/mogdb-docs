---
title: GAUSS-05401 - GAUSS-05500
summary: GAUSS-05401 - GAUSS-05500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05401 - GAUSS-05500

<br/>

## GAUSS-05401 - GAUSS-05410

<br/>

GAUSS-05401: "must be system admin to call gs_get_max_dbsize_name."

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05402: "Please check if another schema is in redistribution in the same database."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05403: "system schema '%s' does not support transfer"

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05404: "Only system admin can lock the cluster."

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05405: "temp table is not supported in online expansion"

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05406: "Only system admin can use the function on coordinator"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05407: "Can not run the function during initdb or upgrade"

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05408: "Can not get temporary tables defination."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05409: "cache lookup failed for table %d."

SQLSTATE: 29P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05410: "Not a ordinary table or foreign table."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05411 - GAUSS-05420

<br/>

GAUSS-05411: "Invalid attribute relation option."

SQLSTATE: 02002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05412: "recover failed could not write to relation mapping file '%s': %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05413: "recover failed could not fsync relation mapping file '%s': %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05414: "recover failed could not close relation mapping file '%s': %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05415: "relmap_redo: wrong size %d in relmap update record"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05416: "Invalid tablespace relation option."

SQLSTATE: 02002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05417: "unexpected duplicate for tablespace %u, relfilenode %u"

SQLSTATE: LL002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05418: "corrupted hashtable"

SQLSTATE: LL002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05419: "namespace %s was invalid after retry"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05420: "relation %s was invalid after retry"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05421 - GAUSS-05430

<br/>

GAUSS-05421: "Table object with oid %u does not exists (has been dropped)"

SQLSTATE: 42P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05422: "cache lookup failed for cfg %u"

SQLSTATE: 29P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05423: "cache lookup failed for enumlabelid %u"

SQLSTATE: 29P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05424: "null group_members for tuple %u"

SQLSTATE: XX005

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05425: "unrecognized distribution key found in source like table"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05426: "Can't get nodeoid for relation %s"

SQLSTATE: XX005

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05427: "There is no installation group for system catalogs!"

SQLSTATE: 55000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05428: "can not open pg_resource_pool"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05429: "Catalog attribute %d for relation '%s' has been updated concurrently"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05430: "Catalog is missing %d attribute(s) for relid %u"

SQLSTATE: 02000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05431 - GAUSS-05440

<br/>

GAUSS-05431: "bucket key column's number is not a 1-D smallint array"

SQLSTATE: 2202E

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05432: "could not find pg_class entry for %u"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05433: "Invalid Oid while setting new relfilenode for tag table."

SQLSTATE: 42602

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05434: "pg_class entry for relid %u vanished during RelationGetIndexList"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05435: "could not open parent relation with OID %u"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05436: "unknown attrKind %u"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05437: "global plan can't be rebuild in share status stmt name %s :global session id :%lu session_id:%lu"

SQLSTATE: 26000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05438: "could not open index with OID %u"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05439: "SearchCatCacheCheck:current transaction is aborted, commands ignored until end of transaction block"

SQLSTATE: 25P02

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05440: "pg_attribute does not have syscache with id %d"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05441 - GAUSS-05450

<br/>

GAUSS-05441: "pg_localtime must not be null!"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05442: "parse error on statement %s."

SQLSTATE: 42601

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05443: "parse error on query %s."

SQLSTATE: 42601

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05444: "could not load library '%s', get error report failed"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05445: "function returned NULL"

SQLSTATE: XX005

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05446: "datum is NULL"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05447: "Division by zero when calculate element numbers!"

SQLSTATE: 22012

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05448: "invalid path:%s"

SQLSTATE: 58P03

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05449: "Current user id is invalid. Please try later."

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05450: "Abnormal process. UserOid has been reseted. Current userOid[%u], reset username is %s."

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05451 - GAUSS-05460

<br/>

GAUSS-05451: "bogus lock file '%s',could not unlink it : %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05452: "bogus data in lock file '%s': '%s', please kill the instance process, than remove the damaged lock file"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05453: "Unable to get architecture to check ARM LSE!"

SQLSTATE: 58P03

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05454: "Unable to open /proc/cpuinfo to check ARM LSE!"

SQLSTATE: 58P03

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05455: "Normal user is not allowed to use HA channel!"

SQLSTATE: 28000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05456: "Only applications can connect remotely."

SQLSTATE: 28000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05457: "Forbid remote connection via internal maintenance tools."

SQLSTATE: 28000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05458: "Inner maintenance tools only for the initial user."

SQLSTATE: D0011

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05459: "Only allow initial user or operator admin to use operation tool gs_roach."

SQLSTATE: D0011

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05460: "Already too many clients, active/non-active/reserved: %d/%d/%d."

SQLSTATE: 53300

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05461 - GAUSS-05470

<br/>

GAUSS-05461: "pg_server_to_any returns null."

SQLSTATE: 58000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05462: "Row Level Security is not supported."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05463: "'%s' is not a normal table"

SQLSTATE: 42809

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05464: "do not support row level security policy on temp table '%s'"

SQLSTATE: 42809

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05465: "do not support row level security policy on dfs table '%s'"

SQLSTATE: 42809

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05466: "The text entered contains illegal characters!"

SQLSTATE: 22P05

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05467: "failed to initialize %s to " INT64_FORMAT

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05468: "Incorrect backend environment variable $PGDATA"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05469: "must be initial account to examine '%s'"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05470: "Current degree of parallelism can only be set within [-64,64]"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05471 - GAUSS-05480

<br/>

GAUSS-05471: "Failed to get logic cluster information by user(oid %d)."

SQLSTATE: 42704

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05472: "permission denied to set role '%s'"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05473: "first stage encryption password failed"

SQLSTATE: 28P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05474: "second stage encryption password failed"

SQLSTATE: 28P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05475: "Invalid password stored"

SQLSTATE: 28P01

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05476: "replication_type is not allowed set 1 in Current Version. Set to default (0)."

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05477: "Length of file or line is too long."

SQLSTATE: F0000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05478: "Failed to initilize the memory(%uM) of search server, maybe it exceed the half of maxChunksPerProcess(%dM)."

SQLSTATE: 53200

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05479: "invalid operation on memory context"

SQLSTATE: D0011

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05480: "We can not set memory context parent with different session number"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05481 - GAUSS-05490

<br/>

GAUSS-05481: "%s Memory Context could not find block containing block"

SQLSTATE: OP002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05482: "%s Memory Context could not find block"

SQLSTATE: OP002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05483: "portal is NULL"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05484: "Accessing null portal entry found in portal hash table."

SQLSTATE: XX005

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05485: "The block was freed before this time."

SQLSTATE: OP002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05486: "The memory use was overflow."

SQLSTATE: OP002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05487: "detected write past chunk end in %s"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05488: "Error on %s Memory Context happened when executing memcpy_s:%d"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05489: "failed to initialize rwlock in AllocSetContextCreate."

SQLSTATE: D0014

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05490: "could not find block"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05491 - GAUSS-05500

<br/>

GAUSS-05491: "data cache block %d is not owned by resource owner %s"

SQLSTATE: 01007

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05492: "meta cache block %d is not owned by resource owner %s"

SQLSTATE: 01007

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05493: "fakepart reference %u is not owned by resource owner %s"

SQLSTATE: 01007

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05494: "partition map reference is not owned by resource owner %s"

SQLSTATE: 01007

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05495: "Failed to obtain address of the block!"

SQLSTATE: 29P02

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05496: "could not write to row store temp file: %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05497: "could not read from row store temp file: %m"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05498: "row store : dn node id(%d) exceeds max bank num(%d)"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05499: "row store : cn node id(%d) exceeds max bank num(%d)"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05500: "failed to get cn/dn node id for OID %u"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
