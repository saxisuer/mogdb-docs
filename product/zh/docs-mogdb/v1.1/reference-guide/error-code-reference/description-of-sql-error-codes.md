---
title: SQL标准错误码说明
summary: SQL标准错误码说明
author: Zhang Cuiping
date: 2021-03-11
---

# SQL标准错误码说明

根据X/Open和SQL Access Group SQL CAE规范（1992）所进行的定义，SQLERROR返回SQLSTATE值。SQLSTATE值是包含五个字符的字符串，由2个字符的SQL错误类和3个字符的子类构成。五个字符包含数值或者大写字母， 代表各种错误或者警告条件的代码。成功的状态是由00000标识的。SQLSTATE代码大多数情况下都是定义在SQL标准里的。

MogDB也遵循SQL标准返回错误码的SQLSTATE值，详细说明请参见[表1](#errorcode)。

**表 1** MogDB错误码SQLSTATE值 <a id="errorcode"></a>

| 错误码SQLSTATE值                                    | 错误码含义                                                   |
| --------------------------------------------------- | ------------------------------------------------------------ |
| 类 00 - 成功完成                                    |                                                              |
| 00000                                               | 成功完成（SUCCESSFUL_COMPLETION）                            |
| 类 01 - 警告                                        |                                                              |
| 01000                                               | 警告（WARNING）                                              |
| 0100C                                               | 返回了动态结果集（DYNAMIC_RESULT_SETS_RETURNED）             |
| 01008                                               | 警告，隐含补齐了零比特位（IMPLICIT_ZERO_BIT_PADDING）        |
| 01003                                               | 在集合函数里消除了NULL（NULL_VALUE_ELIMINATED_IN_SET_FUNCTION） |
| 01007                                               | 没有赋予权限（PRIVILEGE_NOT_GRANTED）                        |
| 01006                                               | 没有撤销权限（PRIVILEGE_NOT_REVOKED）                        |
| 01004                                               | 字符串数据在右端截断（STRING_DATA_RIGHT_TRUNCATION）         |
| 01P01                                               | 废弃的特性（DEPRECATED_FEATURE）                             |
| 类 02 - 没有数据（按照SQL标准的要求，这也是警告类） |                                                              |
| 02000                                               | 没有数据（NO_DATA）                                          |
| 02001                                               | 返回了无附加动态结果集（NO_ADDITIONAL_DYNAMIC_RESULT_SETS_RETURNED） |
| 类 03 - SQL语句尚未结束                             |                                                              |
| 03000                                               | SQL语句尚未结束（SQL_STATEMENT_NOT_YET_COMPLETE）            |
| 类 08 - 连接异常                                    |                                                              |
| 08000                                               | 连接异常（CONNECTION_EXCEPTION）                             |
| 08003                                               | 连接不存在（CONNECTION_DOES_NOT_EXIST）                      |
| 08006                                               | 连接失败（CONNECTION_FAILURE）                               |
| 08001                                               | SQL客户端不能建立SQL连接（SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION） |
| 08004                                               | SQL服务器拒绝建立SQL连接（SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION） |
| 08007                                               | 未知的事务解析（TRANSACTION_RESOLUTION_UNKNOWN）             |
| 08P01                                               | 违反协议（PROTOCOL_VIOLATION）                               |
| 类09 - 触发动作异常                                 |                                                              |
| 09000                                               | 触发动作异常（TRIGGERED_ACTION_EXCEPTION）                   |
| 类 0A - 不支持特性                                  |                                                              |
| 0A000                                               | 不支持此特性（FEATURE_NOT_SUPPORTED）                        |
| 0A100                                               | 不支持流（STREAM_NOT_SUPPORTED）                             |
| 类 0B - 非法事务初始化                              |                                                              |
| 0B000                                               | 非法事务初始化（INVALID_TRANSACTION_INITIATION）             |
| 类 0F - 定位器异常                                  |                                                              |
| 0F000                                               | 定位器异常（LOCATOR_EXCEPTION）                              |
| 0F001                                               | 非法的定位器声明（INVALID_LOCATOR_SPECIFICATION）            |
| 类 0L - 非法赋权者                                  |                                                              |
| 0L000                                               | 非法赋权者（INVALID_GRANTOR）                                |
| 0LP01                                               | 非法赋权操作（INVALID_GRANT_OPERATION）                      |
| 类 0P - 非法角色声明                                |                                                              |
| 0P000                                               | 非法角色声明（INVALID_ROLE_SPECIFICATION）                   |
| 类 0Z - 诊断异常                                    |                                                              |
| 0Z000                                               | 诊断异常（DIAGNOSTICS_EXCEPTION）                            |
| 0Z002                                               | 没有在活跃的处理程序下访问堆栈诊断信息（STACKED_DIAGNOSTICS_ACCESSED_WITHOUT_ACTIVE_HANDLER） |
| 类 20 - 未找到CASE                                  |                                                              |
| 20000                                               | 未找到CASE（CASE_NOT_FOUND）                                 |
| 类 21 - 势违例                                      |                                                              |
| 21000                                               | 势违例（CARDINALITY_VIOLATION）                              |
| 类 22 - 数据异常                                    |                                                              |
| 22000                                               | 数据异常（DATA_EXCEPTION）                                   |
| 2202E                                               | 数组下标错误（ARRAY_SUBSCRIPT_ERROR）                        |
| 22021                                               | 字符不被计算机命令系统识别（CHARACTER_NOT_IN_REPERTOIRE）    |
| 22008                                               | 日期时间字段溢出（DATETIME_FIELD_OVERFLOW）                  |
| 22012                                               | 被零除（DIVISION_BY_ZERO）                                   |
| 22005                                               | 赋值中出错（ERROR_IN_ASSIGNMENT）                            |
| 2200B                                               | 转义字符冲突（ESCAPE_CHARACTER_CONFLICT）                    |
| 22022                                               | 指示器溢出（INDICATOR_OVERFLOW）                             |
| 22015                                               | 内部字段溢出（INTERVAL_FIELD_OVERFLOW）                      |
| 2201E                                               | 对数运算的非法参数（INVALID_ARGUMENT_FOR_LOGARITHM）         |
| 22014                                               | NTILE函数的非法参数（INVALID_ARGUMENT_FOR_NTILE_FUNCTION）   |
| 22016                                               | N值函数的非法参数（INVALID_ARGUMENT_FOR_NTH_VALUE_FUNCTION） |
| 2201F                                               | 幂函数的非法参数（INVALID_ARGUMENT_FOR_POWER_FUNCTION）      |
| 2201G                                               | 宽桶函数的非法参数（INVALID_ARGUMENT_FOR_WIDTH_BUCKET_FUNCTION） |
| 22018                                               | 类型转换时非法的字符值（INVALID_CHARACTER_VALUE_FOR_CAST）   |
| 22007                                               | 非法日期时间格式（INVALID_DATETIME_FORMAT）                  |
| 22019                                               | 非法的转义字符（INVALID_ESCAPE_CHARACTER）                   |
| 2200D                                               | 非法的转义字节（INVALID_ESCAPE_OCTET）                       |
| 22025                                               | 非法转义序列（INVALID_ESCAPE_SEQUENCE）                      |
| 22P06                                               | 非标准使用转义字符（NONSTANDARD_USE_OF_ESCAPE_CHARACTER）    |
| 22010                                               | 非法指示器参数值（INVALID_INDICATOR_PARAMETER_VALUE）        |
| 22023                                               | 非法参数值（INVALID_PARAMETER_VALUE）                        |
| 2201B                                               | 非法正则表达式（INVALID_REGULAR_EXPRESSION）                 |
| 2201W                                               | LIMIT子句中行号非法（INVALID_ROW_COUNT_IN_LIMIT_CLAUSE）     |
| 2201X                                               | 结果集中行号非法（INVALID_ROW_COUNT_IN_RESULT_OFFSET_CLAUSE） |
| 2202H                                               | 非法抽样参数（ERRCODE_INVALID_TABLESAMPLE_ARGUMENT）         |
| 2202G                                               | 非法重复抽样（ERRCODE_INVALID_TABLESAMPLE_REPEAT）           |
| 22009                                               | 非法时区显示值（INVALID_TIME_ZONE_DISPLACEMENT_VALUE）       |
| 2200C                                               | 非法使用转义字符（INVALID_USE_OF_ESCAPE_CHARACTER）          |
| 2200G                                               | 最相关类型不匹配（MOST_SPECIFIC_TYPE_MISMATCH）              |
| 22004                                               | 不允许NULL值（NULL_VALUE_NOT_ALLOWED）                       |
| 22002                                               | NULL值不能做指示器参数（NULL_VALUE_NO_INDICATOR_PARAMETER）  |
| 22003                                               | 数值越界（NUMERIC_VALUE_OUT_OF_RANGE）                       |
| 22017                                               | 并发数超限（ERRCODE_DOP_VALUE_OUT_OF_RANGE）                 |
| 22026                                               | 字符串数据长度不匹配（STRING_DATA_LENGTH_MISMATCH）          |
| 22028                                               | 正则表达式不匹配（ERRCODE_REGEXP_MISMATCH）                  |
| 22001                                               | 字符串数据右截断（STRING_DATA_RIGHT_TRUNCATION）             |
| 22011                                               | 抽取子字符串错误（SUBSTRING_ERROR）                          |
| 22027                                               | 截断错误（TRIM_ERROR）                                       |
| 22024                                               | 未结束的C字符串（UNTERMINATED_C_STRING）                     |
| 2200F                                               | 零长度的字符串（ZERO_LENGTH_CHARACTER_STRING）               |
| 22P01                                               | 浮点异常（FLOATING_POINT_EXCEPTION）                         |
| 22P02                                               | 非法文本表现形式（INVALID_TEXT_REPRESENTATION）              |
| 22P03                                               | 非法二进制表现形式（INVALID_BINARY_REPRESENTATION）          |
| 22P04                                               | 错误的COPY格式（BAD_COPY_FILE_FORMAT）                       |
| 22P05                                               | 不可翻译字符（UNTRANSLATABLE_CHARACTER）                     |
| 22P06                                               | 内存查找失败（ERRCODE_CACHE_LOOKUP_FAILED）                  |
| 22P07                                               | 文件读取失败（ERRCODE_FILE_READ_FAILED）                     |
| 22P08                                               | 获取数据失败（ERRCODE_FETCH_DATA_FAILED）                    |
| 2200L                                               | 非XML文件（NOT_AN_XML_DOCUMENT）                             |
| 2200M                                               | 非法的XML文件（INVALID_XML_DOCUMENT）                        |
| 2200N                                               | 非法的XML内容（INVALID_XML_CONTENT）                         |
| 2200O                                               | 非法的XML错误信息（ERRCODE_INVALID_XML_ERROR_CONTEXT）       |
| 2200S                                               | 非法的XML注释（INVALID_XML_COMMENT）                         |
| 2200T                                               | 非法的XML处理命令（INVALID_XML_PROCESSING_INSTRUCTION）      |
| 2200X                                               | 关闭RELATION错误（RELATION_CLOSE_ERROR）                     |
| 类 23 - 违反完整性约束                              |                                                              |
| 23000                                               | 违反完整性约束（INTEGRITY_CONSTRAINT_VIOLATION）             |
| 23001                                               | 违反限制（RESTRICT_VIOLATION）                               |
| 23502                                               | 违反非空约束（NOT_NULL_VIOLATION）                           |
| 23503                                               | 违反外键约束（FOREIGN_KEY_VIOLATION）                        |
| 23505                                               | 违反唯一约束（UNIQUE_VIOLATION）                             |
| 23514                                               | 违反CHECK约束（CHECK_VIOLATION）                             |
| 23P01                                               | 违反排他约束（EXCLUSION_VIOLATION）                          |
| 类 24 - 非法游标状态                                |                                                              |
| 24000                                               | 非法游标状态（INVALID_CURSOR_STATE）                         |
| 类 25 - 非法事务状态                                |                                                              |
| 25000                                               | 非法事务状态（INVALID_TRANSACTION_STATE）                    |
| 25001                                               | 活跃的SQL状态（ACTIVE_SQL_TRANSACTION）                      |
| 25002                                               | 分支事务已激活（BRANCH_TRANSACTION_ALREADY_ACTIVE）          |
| 25008                                               | 持有的游标要求同样的隔离级别（HELD_CURSOR_REQUIRES_SAME_ISOLATION_LEVEL） |
| 25003                                               | 分支事务访问方式不当（INAPPROPRIATE_ACCESS_MODE_FOR_BRANCH_TRANSACTION） |
| 25004                                               | 分支事务隔离级别不当（INAPPROPRIATE_ISOLATION_LEVEL_FOR_BRANCH_TRANSACTION） |
| 25005                                               | 分支事务没有活跃的SQL事务（NO_ACTIVE_SQL_TRANSACTION_FOR_BRANCH_TRANSACTION） |
| 25006                                               | 只读的SQL事务（READ_ONLY_SQL_TRANSACTION）                   |
| 25007                                               | 不支持模式和数据语句混合使用（SCHEMA_AND_DATA_STATEMENT_MIXING_NOT_SUPPORTED） |
| 25009                                               | 恢复期间无法启用事务（ERRCODE_RUN_TRANSACTION_DURING_RECOVERY） |
| 25010                                               | 事务ID不存在（ERRCODE_GXID_DOES_NOT_EXIST）                  |
| 25P01                                               | 没有活跃的SQL事务（NO_ACTIVE_SQL_TRANSACTION）               |
| 25P02                                               | 在失败的SQL事务中（IN_FAILED_SQL_TRANSACTION）               |
| 类 26 - 非法SQL语句名                               |                                                              |
| 26000                                               | 非法SQL语句名（INVALID_SQL_STATEMENT_NAME）                  |
| 类 27 - 触发的数据改变违规                          |                                                              |
| 27000                                               | 触发的数据改变违规（TRIGGERED_DATA_CHANGE_VIOLATION）        |
| 27001                                               | 触发的元素非法（ERRCODE_TRIGGERED_INVALID_TUPLE）            |
| 类 28 - 非法授权声明                                |                                                              |
| 28000                                               | 非法授权声明（INVALID_AUTHORIZATION_SPECIFICATION）          |
| 28P01                                               | 非法密码（INVALID_PASSWORD）                                 |
| 28P02                                               | 未修改初始密码（INITIAL_PASSWORD_NOT_MODIFIED）              |
| 类29 - 无效或意外的状态                             |                                                              |
| 29P06                                               | 缓存在CACHE中的PLAN无效（INVALID_CACHE_PLAN）                |
| 类 2B - 依然存在依赖的优先级描述符                  |                                                              |
| 2B000                                               | 依然存在依赖的优先级描述符（DEPENDENT_PRIVILEGE_DESCRIPTORS_STILL_EXIST） |
| 2BP01                                               | 依赖性对象仍然存在（DEPENDENT_OBJECTS_STILL_EXIST）          |
| 类 2D - 非法的事务终止                              |                                                              |
| 2D000                                               | 非法的事务终止（INVALID_TRANSACTION_TERMINATION）            |
| 类 2F - SQL过程异常                                 |                                                              |
| 2F000                                               | SQL过程异常（SQL_ROUTINE_EXCEPTION）                         |
| 2F005                                               | 执行的函数没有返回语句（FUNCTION_EXECUTED_NO_RETURN_STATEMENT） |
| 2F002                                               | 不允许修改SQL数据（MODIFYING_SQL_DATA_NOT_PERMITTED）        |
| 2F003                                               | 企图使用禁止的SQL语句（PROHIBITED_SQL_STATEMENT_ATTEMPTED）  |
| 2F004                                               | 不允许读取SQL数据（READING_SQL_DATA_NOT_PERMITTED）          |
| 类 34 - 非法游标名                                  |                                                              |
| 34000                                               | 非法游标名（INVALID_CURSOR_NAME）                            |
| 类 38 - 外部过程异常                                |                                                              |
| 38000                                               | 外部过程异常（EXTERNAL_ROUTINE_EXCEPTION）                   |
| 38001                                               | 不允许包含SQL（CONTAINING_SQL_NOT_PERMITTED）                |
| 38002                                               | 不允许修改SQL数据（MODIFYING_SQL_DATA_NOT_PERMITTED）        |
| 38003                                               | 企图使用禁止的SQL语句（PROHIBITED_SQL_STATEMENT_ATTEMPTED）  |
| 38004                                               | 不允许读取SQL数据（READING_SQL_DATA_NOT_PERMITTED）          |
| 类 39 - 外部过程调用异常                            |                                                              |
| 39000                                               | 外部过程调用异常（EXTERNAL_ROUTINE_INVOCATION_EXCEPTION）    |
| 39001                                               | 返回了非法的SQLSTATE（INVALID_SQLSTATE_RETURNED）            |
| 39004                                               | 不允许空值（NULL_VALUE_NOT_ALLOWED）                         |
| 39P01                                               | 违反触发协议（TRIGGER_PROTOCOL_VIOLATED）                    |
| 39P02                                               | 违反SRF协议（SRF_PROTOCOL_VIOLATED）                         |
| 类 3B - 保存点异常                                  |                                                              |
| 3B000                                               | 保存点异常（SAVEPOINT_EXCEPTION）                            |
| 3B001                                               | 无效的保存点声明（INVALID_SAVEPOINT_SPECIFICATION）          |
| 类 3D - 非法数据库名                                |                                                              |
| 3D000                                               | 非法数据库名（INVALID_CATALOG_NAME）                         |
| 类 3F - 非法模式名                                  |                                                              |
| 3F000                                               | 非法模式名（INVALID_SCHEMA_NAME）                            |
| 类 40 - 事务回滚                                    |                                                              |
| 40000                                               | 事务回滚（TRANSACTION_ROLLBACK）                             |
| 40002                                               | 违反事务完整性约束（TRANSACTION_INTEGRITY_CONSTRAINT_VIOLATION） |
| 40001                                               | 串行化失败（SERIALIZATION_FAILURE）                          |
| 40003                                               | 未知语句是否结束（STATEMENT_COMPLETION_UNKNOWN）             |
| 40P01                                               | 侦测到死锁（DEADLOCK_DETECTED）                              |
| 类 42 - 语法错误或者违反访问规则                    |                                                              |
| 42000                                               | 语法错误或者违反访问规则（SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION） |
| 42601                                               | 语法错误（SYNTAX_ERROR）                                     |
| 42501                                               | 权限不够（INSUFFICIENT_PRIVILEGE）                           |
| 42846                                               | 无法进行类型转换（CANNOT_COERCE）                            |
| 42803                                               | 分组错误（GROUPING_ERROR）                                   |
| 42P20                                               | 窗口错误（WINDOWING_ERROR）                                  |
| 42P19                                               | 无效递归（INVALID_RECURSION）                                |
| 42830                                               | 非法的外键（INVALID_FOREIGN_KEY）                            |
| 42602                                               | 非法名称（INVALID_NAME）                                     |
| 42622                                               | 名称太长（NAME_TOO_LONG）                                    |
| 42939                                               | 保留名称（RESERVED_NAME）                                    |
| 42804                                               | 数据类型不匹配（DATATYPE_MISMATCH）                          |
| 42P18                                               | 不确定的数据类型（INDETERMINATE_DATATYPE）                   |
| 42P21                                               | 排序规则不匹配（COLLATION_MISMATCH）                         |
| 42P22                                               | 不确定的排序规则（INDETERMINATE_COLLATION）                  |
| 42P23                                               | 分区错误（ERRCODE_PARTITION_ERROR）                          |
| 42P24                                               | 非法属性值（ERRCODE_INVALID_ATTRIBUTE）                      |
| 42P25                                               | 非法聚集函数（ERRCODE_INVALID_AGG）                          |
| 42P26                                               | 资源池错误（ERRCODE_RESOURCE_POOL_ERROR）                    |
| 42P27                                               | 未找到父级计划（ERRCODE_PLAN_PARENT_NOT_FOUND）              |
| 42P28                                               | 更新冲突（ERRCODE_MODIFY_CONFLICTS）                         |
| 42P29                                               | 分布错误（ERRCODE_DISTRIBUTION_ERROR）                       |
| 42809                                               | 错误的对象类型（WRONG_OBJECT_TYPE）                          |
| 42703                                               | 未定义的字段（UNDEFINED_COLUMN）                             |
| 42883                                               | 未定义的函数（UNDEFINED_FUNCTION）                           |
| 42P01                                               | 未定义的表（UNDEFINED_TABLE）                                |
| 42P02                                               | 未定义的参数（UNDEFINED_PARAMETER）                          |
| 42704                                               | 未定义对象（UNDEFINED_OBJECT）                               |
| 42701                                               | 重复的字段（DUPLICATE_COLUMN）                               |
| 42P03                                               | 重复的游标（DUPLICATE_CURSOR）                               |
| 42P04                                               | 重复的数据库（DUPLICATE_DATABASE）                           |
| 42723                                               | 重复的函数（DUPLICATE_FUNCTION）                             |
| 42P05                                               | 重复的预编译语句（DUPLICATE_PREPARED_STATEMENT）             |
| 42P06                                               | 重复的模式（DUPLICATE_SCHEMA）                               |
| 42P07                                               | 重复的表（DUPLICATE_TABLE）                                  |
| 42712                                               | 重复的别名（DUPLICATE_ALIAS）                                |
| 42710                                               | 重复的对象（DUPLICATE_OBJECT）                               |
| 42702                                               | 模糊的字段（AMBIGUOUS_COLUMN）                               |
| 42725                                               | 模糊的函数（AMBIGUOUS_FUNCTION）                             |
| 42P08                                               | 模糊的参数（AMBIGUOUS_PARAMETER）                            |
| 42P09                                               | 模糊的别名（AMBIGUOUS_ALIAS）                                |
| 42P10                                               | 非法字段引用（INVALID_COLUMN_REFERENCE）                     |
| 42611                                               | 非法字段定义（INVALID_COLUMN_DEFINITION）                    |
| 42P11                                               | 非法游标定义（INVALID_CURSOR_DEFINITION）                    |
| 42P12                                               | 非法数据库定义（INVALID_DATABASE_DEFINITION）                |
| 42P13                                               | 非法函数定义（INVALID_FUNCTION_DEFINITION）                  |
| 42P14                                               | 非法预编译语句定义（INVALID_PREPARED_STATEMENT_DEFINITION）  |
| 42P15                                               | 非法模式定义（INVALID_SCHEMA_DEFINITION）                    |
| 42P16                                               | 非法表定义（INVALID_TABLE_DEFINITION）                       |
| 42P17                                               | 非法对象定义（INVALID_OBJECT_DEFINITION）                    |
| 类 44 - 违反WITH CHECK选项                          |                                                              |
| 44000                                               | 违反WITH CHECK选项（WITH_CHECK_OPTION_VIOLATION）            |
| 类 53 - 资源不足                                    |                                                              |
| 53000                                               | 资源不足（INSUFFICIENT_RESOURCES）                           |
| 53100                                               | 磁盘满（DISK_FULL）                                          |
| 53200                                               | 内存耗尽（OUT_OF_MEMORY）                                    |
| 53300                                               | 太多连接（TOO_MANY_CONNECTIONS）                             |
| 53400                                               | 超过配置限制（CONFIGURATION_LIMIT_EXCEEDED）                 |
| 类 54 - 超过程序限制                                |                                                              |
| 54000                                               | 超过程序限制（PROGRAM_LIMIT_EXCEEDED）                       |
| 54001                                               | 语句太复杂（STATEMENT_TOO_COMPLEX）                          |
| 54011                                               | 字段太多（TOO_MANY_COLUMNS）                                 |
| 54023                                               | 参数太多（TOO_MANY_ARGUMENTS）                               |
| 类 55 - 对象不在预先要求的状态                      |                                                              |
| 55000                                               | 对象不在预先要求的状态（OBJECT_NOT_IN_PREREQUISITE_STATE）   |
| 55006                                               | 对象在使用中(OBJECT_IN_USE)                                  |
| 55P02                                               | 无法修改运行时参数（CANT_CHANGE_RUNTIME_PARAM）              |
| 55P03                                               | 锁不可获得（LOCK_NOT_AVAILABLE）                             |
| 类 57 - 操作者干涉                                  |                                                              |
| 57000                                               | 操作者干涉（OPERATOR_INTERVENTION）                          |
| 57014                                               | 查询被取消（QUERY_CANCELED）                                 |
| 57015                                               | 内部查询取消（QUERY_INTERNAL_CANCEL）                        |
| 57P01                                               | 管理员关闭系统（ADMIN_SHUTDOWN）                             |
| 57P02                                               | 崩溃宕机（CRASH_SHUTDOWN）                                   |
| 57P03                                               | 现在无法连接（CANNOT_CONNECT_NOW）                           |
| 57P04                                               | 丢弃数据库（DATABASE_DROPPED）                               |
| 类 58 - 系统错误（MogDB自己内部的错误）             |                                                              |
| 58000                                               | 系统错误（SYSTEM_ERROR）                                     |
| 58030                                               | IO错误（IO_ERROR）                                           |
| 58P01                                               | 未定义的文件（UNDEFINED_FILE）                               |
| 58P02                                               | 重复的文件（DUPLICATE_FILE）                                 |
| 类 F0 - 配置文件错误                                |                                                              |
| F0000                                               | 配置文件错误（CONFIG_FILE_ERROR）                            |
| F0001                                               | 锁文件存在（LOCK_FILE_EXISTS）                               |
| 类 HV - 外部数据错误（SQL/MED）                     |                                                              |
| HV000                                               | 外部数据错误（FDW_ERROR）                                    |
| HV005                                               | 未找到列名（FDW_COLUMN_NAME_NOT_FOUND）                      |
| HV002                                               | 需要动态参数值（FDW_DYNAMIC_PARAMETER_VALUE_NEEDED）         |
| HV010                                               | 函数序列错误（FDW_FUNCTION_SEQUENCE_ERROR）                  |
| HV021                                               | 描述信息不一致（FDW_INCONSISTENT_DESCRIPTOR_INFORMATION）    |
| HV024                                               | 非法属性值（FDW_INVALID_ATTRIBUTE_VALUE）                    |
| HV007                                               | 非法列名称（FDW_INVALID_COLUMN_NAME）                        |
| HV008                                               | 非法列数（FDW_INVALID_COLUMN_NUMBER）                        |
| HV004                                               | 非法数据类型（FDW_INVALID_DATA_TYPE）                        |
| HV006                                               | 非法数据类型描述符（FDW_INVALID_DATA_TYPE_DESCRIPTORS）      |
| HV091                                               | 非法字段标识符（FDW_INVALID_DESCRIPTOR_FIELD_IDENTIFIER）    |
| HV00B                                               | 非法处理（FDW_INVALID_HANDLE）                               |
| HV00C                                               | 非法索引选项（FDW_INVALID_OPTION_INDEX）                     |
| HV00D                                               | 非法选项名称（FDW_INVALID_OPTION_NAME）                      |
| HV090                                               | 非法字符串长度或缓冲区长度（FDW_INVALID_STRING_LENGTH_OR_BUFFER_LENGTH） |
| HV00A                                               | 非法字符长格式（FDW_INVALID_STRING_FORMAT）                  |
| HV009                                               | 非法使用NULL指针（FDW_INVALID_USE_OF_NULL_POINTER）          |
| HV014                                               | 太多句柄（FDW_TOO_MANY_HANDLES）                             |
| HV001                                               | 内存耗尽（FDW_OUT_OF_MEMORY）                                |
| HV00P                                               | 无模式（FDW_NO_SCHEMAS）                                     |
| HV00J                                               | 未找到选项名称（FDW_OPTION_NAME_NOT_FOUND）                  |
| HV00K                                               | 回复句柄（FDW_REPLY_HANDLE）                                 |
| HV00Q                                               | 未找到模式（FDW_SCHEMA_NOT_FOUND）                           |
| HV00R                                               | 未找到表（FDW_TABLE_NOT_FOUND）                              |
| HV00S                                               | 不正确的服务器类型(FDW_INVALID_SERVER_TYPE)                  |
| HV00L                                               | 无法建立执行（FDW_UNABLE_TO_CREATE_EXECUTION）               |
| HV00M                                               | 不能创建回复（FDW_UNABLE_TO_CREATE_REPLY）                   |
| HV00N                                               | 无法建立连接（FDW_UNABLE_TO_ESTABLISH_CONNECTION）           |
| 类 P0 - PL/pgSQL错误                                |                                                              |
| P0000                                               | PLPGSQL错误（PLPGSQL_ERROR）                                 |
| P0001                                               | 抛出异常（RAISE_EXCEPTION）                                  |
| P0002                                               | 未找到数据（NO_DATA_FOUND）                                  |
| P0003                                               | 行太多（TOO_MANY_ROWS）                                      |
| P0004                                               | FORALL需要DML操作（FORALL_NEED_DML）                         |
| 类 XX - 内部错误                                    |                                                              |
| XX000                                               | 内部错误（INTERNAL_ERROR）                                   |
| XX001                                               | 数据损坏（DATA_CORRUPTED）                                   |
| XX002                                               | 索引损坏（INDEX_CORRUPTED）                                  |
| XX003                                               | 关闭远程流接口（STREAM_REMOTE_CLOSE_SOCKET）                 |
| XX004                                               | 未知节点类型（ERRCODE_UNRECOGNIZED_NODE_TYPE）               |
| XX005                                               | 异常空值（ERRCODE_UNEXPECTED_NULL_VALUE）                    |
| XX006                                               | 异常节点状态（ERRCODE_UNEXPECTED_NODE_STATE）                |
| XX007                                               | JUNK列为空（ERRCODE_NULL_JUNK_ATTRIBUTE）                    |
| XX008                                               | 优化器状态不一致（ERRCODE_OPTIMIZER_INCONSISTENT_STATE）     |
| XX009                                               | 重复查询编号（ERRCODE_STREAM_DUPLICATE_QUERY_ID）            |
| XX010                                               | 无效缓冲区（INVALID_BUFFER）                                 |
| XX011                                               | 无效缓冲区引用（INVALID_BUFFER_REFERENCE）                   |
| XX012                                               | 节点编号不匹配（ERRCODE_NODE_ID_MISSMATCH）                  |
| XX013                                               | 不能修改xid base（CANNOT_MODIFY_XIDBASE）                    |
| XX014                                               | TOAST表数据损坏（UNEXPECTED_CHUNK_VALUE）                    |
| 类 YY - SQL重试错误                                 |                                                              |
| YY001                                               | 对端连接重置（CONNECTION_RESET_BY_PEER）                     |
| YY002                                               | 对端流重置（STREAM_CONNECTION_RESET_BY_PEER）                |
| YY003                                               | 锁等待超时（LOCK_WAIT_TIMEOUT）                              |
| YY004                                               | 连接超时（CONNECTION_TIMED_OUT）                             |
| YY005                                               | 查询设置错误（SET_QUERY_ERROR）                              |
| YY006                                               | 超出逻辑内存（OUT_OF_LOGICAL_MEMORY）                        |
| YY007                                               | 通信库内存分配（SCTP_MEMORY_ALLOC）                          |
| YY008                                               | 无通信库缓存数据（SCTP_NO_DATA_IN_BUFFER）                   |
| YY009                                               | 通信库释放内存关闭（SCTP_RELEASE_MEMORY_CLOSE）              |
| YY010                                               | SCTP、TCP断开（SCTP_TCP_DISCONNECT）                         |
| YY011                                               | 通信库断开（SCTP_DISCONNECT）                                |
| YY012                                               | 通信库远程关闭（SCTP_REMOTE_CLOSE）                          |
| YY013                                               | 等待未知通信库通信（SCTP_WAIT_POLL_UNKNOW）                  |
| YY014                                               | 无效快照（SNAPSHOT_INVALID）                                 |
| YY015                                               | 通讯接收信息错误（ERRCODE_CONNECTION_RECEIVE_WRONG）         |
| 类SI - SPI接口错误                                  |                                                              |
| SP000                                               | SPI接口错误（ERRCODE_SPI_ERROR）                             |
| SP001                                               | SPI通讯失败（ERRCODE_SPI_CONNECTION_FAILURE）                |
| SP002                                               | SPI完成失败（ERRCODE_SPI_FINISH_FAILURE）                    |
| SP003                                               | SPI准备失败（ERRCODE_SPI_PREPARE_FAILURE）                   |
| SP004                                               | SPI游标开启失败（ERRCODE_SPI_CURSOR_OPEN_FAILURE）           |
| SP005                                               | SPI执行失败（ERRCODE_SPI_EXECUTE_FAILURE）                   |
| SP006                                               | SPI不当调用（ERRORCODE_SPI_IMPROPER_CALL）                   |
| 类RB - RBTree错误                                   |                                                              |
| RB001                                               | RBTree无效节点状态（RBTREE_INVALID_NODE_STATE）              |
| RB002                                               | RBTree无效迭代顺序（RBTREE_INVALID_ITERATOR_ORDER）          |
| 类 PD - PL调试器错误                                |                                                              |
| D0000                                               | PL调试器内部错误（PLDEBUGGER_INTERNAL_ERROR）                |
| D0001                                               | 重复断点（DUPLICATE_BREAKPOINT）                             |
| D0002                                               | 哈希函数没有被初始化（FUNCTION_HASH_IS_NOT_INITIALIZED）     |
| D0003                                               | 断点不存在（BREAKPOINT_IS_NOT_PRESENT）                      |
| D0004                                               | 已附加调试服务器（DEBUG_SERVER_ALREADY_IS_ATTACHED）         |
| D0005                                               | 未附加调试服务器（DEBUG_SERVER_NOT_ATTACHED）                |
| D0006                                               | 已同步调试服务器（DEBUG_SERVER_ALREADY_IN_SYNC）             |
| D0007                                               | 未同步调试服务器（DEBUG_TARGET_SERVERS_NOT_IN_SYNC）         |
| D0008                                               | 已同步目标服务（TARGET_SERVER_ALREADY_IN_SYNC）              |
| D0009                                               | 变量不存在（NON_EXISTANT_VARIABLE）                          |
| D0010                                               | 非法目标会话ID（INVALID_TARGET_SESSION_ID）                  |
| D0011                                               | 非法操作（INVALID_OPERATION）                                |
| D0012                                               | 达到最大调试会话数（MAXIMUM_NUMBER_OF_DEBUG_SESSIONS_REACHED） |
| D0013                                               | 达到最大断点数（MAXIMUM_NUMBER_OF_BREAKPOINTS_REACHED）      |
| 类LL - 逻辑解码错误码                               |                                                              |
| LL001                                               | 逻辑解码错误（LOGICAL_DECODE_ERROR）                         |
| LL002                                               | 哈希表搜索错误（RELFILENODEMAP）                             |
