---
title: GAUSS-01201 - GAUSS-01300
summary: GAUSS-01201 - GAUSS-01300
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-01201 - GAUSS-01300

<br/>

## GAUSS-01201 - GAUSS-01210

<br/>

GAUSS-01201: "can not specify EOL in BINARY mode"

SQLSTATE: 0A000

错误原因: 不能在BINARY模式下指定终止符。

解决办法: 请勿在BINARY模式下使用终止符。

GAUSS-01202: "FIX specification only available using COPY FROM or READ ONLY foreign table"

SQLSTATE: 42000

错误原因: 指定FIX参数只能在copy from和只读外表的场景中使用。

解决办法: 请在copy from或外表场景使用指定FIX参数的操作。

GAUSS-01203: "FILEHEADER specification only available using HEAD"

SQLSTATE: 42601

错误原因: 用户在导出的时候指定从哪个文件来读取HEAD信息，但是并没有指定是否真正需要HEAD信息。

解决办法: 在导出语句中明确指定HEADER项，或者去掉读取的文件名信息。

GAUSS-01204: "table '%s' does not have OIDs"

SQLSTATE: 42703

错误原因: 导入数据时指定了文本中存在OID项，但是用户表并不存在OID列。

解决办法: 检查要导入的数据是否与用户表的定义在OID上是否一致，要么删除导入的OID项，要么对用户表增加OID列。

GAUSS-01206: "unexpected rewrite result"

SQLSTATE: XX000

错误原因:  系统内部错误。导入SQL语句的重写结果非预期。

解决办法: 运行时错误，请联系技术支持工程师提供技术支持。

GAUSS-01207: "COPY (SELECT INTO) is not supported"

SQLSTATE: 0A000

错误原因: 不支持COPY中SELECT INTO语法。

解决办法: COPY中不建议使用SELECT INTO语法。

GAUSS-01208: "FORCE QUOTE column '%s' not referenced by COPY"

SQLSTATE: 42P10

错误原因: 在CSV COPY TO模式下，在每个声明的字段周围对所有非NULL值没有使用引号包围。

解决办法: 在CSV COPY TO模式下，强制在每个声明的字段周围对所有非NULL值都使用引号包围。NULL输出不会被引号包围。

GAUSS-01209: "FORCE NOT NULL column '%s' not referenced by COPY"

SQLSTATE: 42P10

错误原因: 在CSV COPY FROM模式下，指定的字段输入为空。

解决办法: 在CSV COPY FROM模式下，指定的字段输入不能为空。

GAUSS-01210: "could not close file '%s': %m"

SQLSTATE: XX000

错误原因: 无法关闭指定文件。

解决办法: 检查指定文件是否被破坏或权限问题。

<br/>

## GAUSS-01211 - GAUSS-01220

<br/>

GAUSS-01211: "cannot copy from view '%s'"

SQLSTATE: 42809

错误原因: 无法从视图中COPY数据。

解决办法: 请勿从视图中COPY数据。

GAUSS-01212: "cannot copy from foreign table '%s'"

SQLSTATE: 42809

错误原因: 无法从外表中COPY数据。

解决办法: 请勿从外表中COPY数据。

GAUSS-01213: "cannot copy from sequence '%s'"

SQLSTATE: 42809

错误原因: 无法从序列中COPY数据。

解决办法: 请勿从序列中COPY数据。

GAUSS-01214: "cannot copy from non-table relation '%s'"

SQLSTATE: 42809

错误原因: 无法从非表关系中COPY数据。

解决办法: 请勿从非表关系中COPY数据。

GAUSS-01215: "relative path not allowed for COPY to file"

SQLSTATE: 42602

错误原因: COPY目的文件不能用相对路径格式。

解决办法: COPY目的文件使用绝对路径格式。

GAUSS-01216: "could not open file '%s' for writing: %m"

SQLSTATE: XX000

错误原因: 无法打开指定文件。

解决办法: 请检查指定文件是否被破坏或权限问题。

GAUSS-01217: "'%s' is a directory"

SQLSTATE: 42809

错误原因: 期望为文件的参数是目录。

解决办法: 请指定COPY的文件名而非目录名。

GAUSS-01218: "could not chmod file '%s' : %m"

SQLSTATE: XX000

错误原因: 无法给文件变更权限。

解决办法: 请检查文件目前的权限及所属用户是否正常。

GAUSS-01219: "Invalid file format"

SQLSTATE: XX000

错误原因: 不支持的文件格式。

解决办法: 请使用COPY支持的文本、二进制、CSV格式。

GAUSS-01220: "null value in column '%s' violates not-null constraint"

SQLSTATE: 23502

错误原因: 向有非空约束的列插入空值。

解决办法: 修改与非空约束冲突的空值为有效值。

<br/>

## GAUSS-01221 - GAUSS-01230

<br/>

GAUSS-01221: "Column Store unsupport CHECK constraint"

SQLSTATE: XX000

错误原因: 当前列存不支持CHECK约束。

解决办法: 删除CHECK约束。

GAUSS-01222: "cannot copy to view '%s'"

SQLSTATE: 42809

错误原因: 无法COPY数据到视图。

解决办法: 请勿COPY数据到视图中。

GAUSS-01223: "cannot copy to foreign table '%s'"

SQLSTATE: 42809

错误原因: 无法COPY数据到外表。

解决办法: 请勿COPY数据到外表中。

GAUSS-01224: "cannot copy to sequence '%s'"

SQLSTATE: 42809

错误原因: 无法COPY数据到序列。

解决办法: 请勿COPY数据到序列中。

GAUSS-01225: "cannot copy to non-table relation '%s'"

SQLSTATE: 42809

错误原因: 无法COPY数据非正规表。

解决办法: 请勿COPY数据非正规表中。

GAUSS-01226: "invalid COPY file header (COPY SEND)"

SQLSTATE: 22P04

错误原因: 无效COPY操作文件头。

解决办法: COPY数据文件被破坏，请检查文件。

GAUSS-01227: "Copy failed on a Datanode"

SQLSTATE: 08000

错误原因: 导入数据失败。

解决办法: 需要查看数据节点上的日志来找出发生的错误类型，视具体问题采用不同的措施。

GAUSS-01228: "Missing data when batch insert compressed data !"

SQLSTATE: XX000

错误原因: 向行存表导入压缩数据的过程中，出现了数据丢失的现象。

解决办法: 使用alter table关闭行存表的压缩特性，重新导入数据。

GAUSS-01229: "Non-shippable ROW triggers not supported with COPY"

SQLSTATE: 0A000

错误原因: COPY操作不支持行触发器。

解决办法: 请勿在COPY中使用行触发器。

GAUSS-01230: "could not open file '%s' for reading: %m"

SQLSTATE: XX000

错误原因: 打开文件失败。

解决办法: 通过错误码，检查文件打开失败的原因并进行修正（不存在，无权限，磁盘空间满等）。

<br/>

## GAUSS-01231 - GAUSS-01240

<br/>

GAUSS-01231: "COPY file signature not recognized"

SQLSTATE: 22P04

错误原因: COPY数据文件签名无法识别。

解决办法: COPY数据文件被破坏，请检查文件。

GAUSS-01232: "invalid COPY file header (missing flags)"

SQLSTATE: 22P04

错误原因: COPY数据丢失文件头。

解决办法: COPY数据文件被破坏，请检查文件。

GAUSS-01233: "unrecognized critical flags in COPY file header"

SQLSTATE: 22P04

错误原因: COPY数据文件头无法识别。

解决办法: COPY数据文件被破坏，请检查文件。

GAUSS-01236: "missing data for OID column"

SQLSTATE: 22P04

错误原因: copy时OID为空值。

解决办法: 检查数据文件首列是否有空值。

GAUSS-01237: "null OID in COPY data"

SQLSTATE: 22P04

错误原因: copy操作时对应的OID为空。

解决办法: 检查copy语句，确保需要导入数据的表的OID不为空。

GAUSS-01238: "invalid OID in COPY data"

SQLSTATE: 22P04

错误原因: 数据所对应的OID是一个无效的OID。

解决办法: 检查copy语句中属性列与实际数据是否相对应。

GAUSS-01239: "missing data for column '%s'"

SQLSTATE: 22P04

错误原因: copy语句中希望导入的列数大于实际的数据列数，使得其中的某列没有数据。

解决办法: 检查copy语句中的属性列与导入的数据量列数是否一致。

GAUSS-01240: "received copy data after EOF marker"

SQLSTATE: 22P04

错误原因: 在EOF标记后仍然读取了数据，这是因为在旧协议中没有定义EOF标记。

解决办法: 确保按照当前协议书写的数据不会超过预期想要的。

<br/>

## GAUSS-01241 - GAUSS-01250

<br/>

GAUSS-01241: "row field count is %d, expected %d"

SQLSTATE: 22P04

错误原因: 属性个数期望的与实际不一致。

解决办法: "1、从DN端返回给CN端的属性个数出错，属于内部错误，请联系技术支持工程师提供技术支持。 2、目标表属性个数与要拷贝的源文件属性个数不一致。需要检查待copy文件的列数和表中指定的列数是否一致。"

GAUSS-01242: "literal carriage return found in data"

SQLSTATE: 22P04

错误原因: copy的数据中包含回车符。

解决办法: 确保数据中不包含回车符。

GAUSS-01243: "literal newline found in data"

SQLSTATE: 22P04

错误原因: 在CSV文件中发现不可识别的数据格式。

解决办法: 请检查文件格式及文件数据内容规范性。

GAUSS-01244: "end-of-copy marker does not match previous newline style"

SQLSTATE: 22P04

错误原因: 文件中出现异常字符。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01245: "end-of-copy marker corrupt"

SQLSTATE: 22P04

错误原因: 文件中出现异常字符。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01246: "unterminated CSV quoted field"

SQLSTATE: 22P04

错误原因: 文件中出现异常字符。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01247: "unexpected EOF in COPY data"

SQLSTATE: 22P04

错误原因: 读取COPY文件出现失败。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01248: "invalid field size"

SQLSTATE: 22P04

错误原因: 读取COPY文件中数据大小错误。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01249: "incorrect binary data format"

SQLSTATE: 22P03

错误原因: 错误的二进制文件格式。

解决办法: 请检查文件数据格式合法性，详细信息请参见zh-cn_topic_0237122096.html。

GAUSS-01250: "Failed to initialize Datanodes for COPY"

SQLSTATE: 08000

错误原因: 无法为COPY操作初始化数据节点之间的连接。

解决办法: 需要依次检查: 1. 集群状态是否正常；2. 节点之间的连接是否正常；3. 当前节点之间的网络连接是否正常；4. 所有节点上的内存资源是充足的，未发生OOM问题；

<br/>

## GAUSS-01251 - GAUSS-01260

<br/>

GAUSS-01251: "could not read symbolic link '%s': %m"

SQLSTATE: XX000

错误原因: 读取符号链接文件失败。

解决办法: 请检查符号链接文件正确性。

GAUSS-01252: "symbolic link '%s' target is too long"

SQLSTATE: XX000

错误原因: 符号链接文件过长。

解决办法: 请检查符号链接文件正确性。

GAUSS-01253: "Invalid URL '%s' in LOCATION"

SQLSTATE: XX000

错误原因: GDS服务中URL参数错误。

解决办法: 请检查URL参数值及格式正确性。

GAUSS-01254: "unrecognized URL '%s'"

SQLSTATE: XX000

错误原因: GDS服务中URL参数错误。

解决办法: 请检查URL参数值及格式正确性。

GAUSS-01255: "wrong URL format '%s'"

SQLSTATE: XX000

错误原因: GDS服务中URL参数错误。

解决办法: 请检查URL参数值及格式正确性。

GAUSS-01256: "unable to open file '%s'"

SQLSTATE: XX000

错误原因: 无法打开操作文件。

解决办法: 请检查文件格式及权限正确性。

GAUSS-01257: "unable to fseek file '%s'"

SQLSTATE: XX000

错误原因: 无法设置文件指针位置。

解决办法: 请检查文件格式及权限正确性。

GAUSS-01258: "no files found to import"

SQLSTATE: XX000

错误原因: 检测到导入的文件数目为0，在raise_errors_if_no_files开启的时候，进行报错处理。

解决办法: 关闭raise_errors_if_no_files参数。

GAUSS-01259: "%s"

SQLSTATE: XX000

错误原因: "this is a sql syntax errorsql语法错误"。

解决办法: "check your sql syntax.检查sql语句的语法是否正确"。

GAUSS-01260: "Incomplete Message from GDS ."

SQLSTATE: XX000

错误原因: 从GDS中收到了非预期的消息数据。

解决办法: 首先需要检查当前网络运行状况（丢包率是否较高）；若网络运行良好，则属于内部错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-01261 - GAUSS-01270

<br/>

GAUSS-01261: "unimplemented bulkload mode"

SQLSTATE: XX000

错误原因: 不支持的导入模式。

解决办法: 在normal/shared/private中选择一个有效的导入模式。

GAUSS-01262: "relative path not allowed for writable foreign table file"

SQLSTATE: 42602

错误原因: 在可写外表中使用了相对路径。

解决办法: 请在可写外表中使用绝对路径。

GAUSS-01264: "Found invalid error recored"

SQLSTATE: XX000

错误原因: 元组中数据记录错误。

解决办法: 请结合报错Context，尝试定位文本中导致此报错的数据行，检查是否存在非法字符。若否，请联系技术支持工程师提供技术支持。

GAUSS-01265: "could not cache error info:%m"

SQLSTATE: XX000

错误原因: 错误信息无法缓存。

解决办法: 请结合报错Context，尝试定位文本中导致此报错的数据行，检查是否存在非法字符。若否，请联系技术支持工程师提供技术支持。

GAUSS-01266: "could not fetch error record:%m"

SQLSTATE: XX000

错误原因: 错误信息获取失败。

解决办法: 请结合报错Context，尝试定位文本中导致此报错的数据行，检查是否存在非法字符。若否，请联系技术支持工程师提供技术支持。

GAUSS-01267: "incomplete error record"

SQLSTATE: XX000

错误原因: 不完整的错误信息记录。

解决办法: 请结合报错Context，尝试定位文本中导致此报错的数据行，检查是否存在非法字符。若否，请联系技术支持工程师提供技术支持。

GAUSS-01268: "access method '%s' does not exist"

SQLSTATE: 42704

错误原因: 系统缓存中获取元组失败。

解决办法: 系统表信息错误，请联系技术支持工程师提供技术支持。

GAUSS-01269: "must specify at least one column"

SQLSTATE: 42P17

错误原因: 创建索引过程中没有指定列。

解决办法: 创建索引必须至少指定一列。

GAUSS-01270: "cannot use more than %d columns in an index"

SQLSTATE: 54011

错误原因: 一个索引中不能同时指定超过32列。

解决办法: 请勿在一个索引中同时指定超过32列。

<br/>

## GAUSS-01271 - GAUSS-01280

<br/>

GAUSS-01271: "non-partitioned table does not support local partitioned indexes "

SQLSTATE: 0A000

错误原因: 非分区表不支持创建local模式下的分区索引。

解决办法: 1、如果需要创建local模式下的分区索引，需要重新创建基表为分区表。

2、如果不需要创建local模式下的分区索引，需要删除Create unique index…local;语法最后的local参数。

GAUSS-01274: "cannot create index on foreign table '%s'"

SQLSTATE: 42809

错误原因: 不能在外表上创建索引。

解决办法: 请勿在外表上创建索引。

GAUSS-01275: "cannot create indexes on temporary tables of other sessions"

SQLSTATE: 0A000

错误原因: 不能在其他会话的临时表上创建索引。

解决办法: 请勿在其他会话的临时表上创建索引。

GAUSS-01276: "when creating partitioned index, get table partitions failed"

SQLSTATE: XX000

错误原因: 创建分区索引时，获取分区表失败。

解决办法: 请检查分区表的分区定义是否正确，如果不正确，请重建分区表。

GAUSS-01278: "number of partitions of LOCAL index must equal that of the underlying table"

SQLSTATE: 42P17

错误原因: 分区表中分区数目小于分区索引的数目。

解决办法: 请重建分区表中分区数，必须等于分区索引的数目。

GAUSS-01279: "unique index columns must contain the partition key"

SQLSTATE: 42P17

错误原因: 唯一索引列必须包含分区键。

解决办法: 唯一索引列必须包含分区键。

GAUSS-01280: "unique index columns must contain the partition key and collation must be default collation"

SQLSTATE: 42P17

错误原因: 唯一索引列必须包含分区键。

解决办法: 唯一索引列必须包含分区键，排序方式必须使用默认方式。

<br/>

## GAUSS-01281 - GAUSS-01290

<br/>

GAUSS-01281: "access method '%s' does not support unique indexes"

SQLSTATE: 0A000

错误原因: 该索引方式不支持唯一索引。

解决办法: 请勿使用报错提示的索引类建立唯一索引。

GAUSS-01282: "access method '%s' does not support multicolumn indexes"

SQLSTATE: 0A000

错误原因: 该索引方式不支持组合索引。

解决办法: 请勿使用报错提示的索引类建立唯一索引。

GAUSS-01283: "access method '%s' does not support exclusion constraints"

SQLSTATE: 0A000

错误原因: 该索引方式不支持排他约束。

解决办法: 请勿使用报错提示的索引类附加排他约束。

GAUSS-01284: "Cannot create index whose evaluation cannot be enforced to remote nodes"

SQLSTATE: 0A000

错误原因: MogDB主键约束通过unique btree索引实现。主键约束没有包含分布列时，无法成功创建索引。

解决办法: 请检查主键约束中是否包含分布列。

GAUSS-01285: "unknown constraint type"

SQLSTATE: XX000

错误原因: 创建索引时定义的约束类型错误。

解决办法: 创建索引的约束类型关键字只能为PRIMARY KEY、UNIQUE、EXCLUDE，请使用有效的约束关键字重新创建索引。

GAUSS-01286: "fail to get index info when create index partition"

SQLSTATE: 42P17

错误原因: 创建分区索引时，获取索引信息失败。

解决办法: 请重新创建分区索引，如仍然提示失败，请联系技术支持工程师提供技术支持。

GAUSS-01287: "index partition with name '%s' already exists"

SQLSTATE: 42704

错误原因: 分区索引名已存在。

解决办法: 修改分区索引名请勿重复。

GAUSS-01288: "unsupport partitioned strategy"

SQLSTATE: 42P17

错误原因: 不支持的分区策略。

解决办法: 目前仅支持对RANGE或INTERNAL分区表创建分区索引时，请重新修改分区表为RANGE或INTERNAL类型分区表，再创建分区索引。

GAUSS-01289: "collation '%s' does not exist"

SQLSTATE: 42P17

错误原因: 索引的排序模式不存在。

解决办法: 检查索引的排序模式是否正确。

GAUSS-01290: "cannot use subquery in index predicate"

SQLSTATE: 0A000

错误原因: 表达式索引中不能使用子查询。

解决办法: 请勿在表达式索引中使用子查询。

<br/>

## GAUSS-01291 - GAUSS-01300

<br/>

GAUSS-01291: "cannot use aggregate in index predicate"

SQLSTATE: 42803

错误原因: 表达式索引中不能使用聚集操作。

解决办法: 请勿在表达式索引中使用聚集操作。

GAUSS-01292: "functions in index predicate must be marked IMMUTABLE"

SQLSTATE: 42P17

错误原因: 表达式索引中不能使用易变函数。

解决办法: 保证表达式索引中的函数为不变函数。

GAUSS-01293: "cannot use subquery in index expression"

SQLSTATE: 0A000

错误原因: 表达式索引中不能使用子查询。

解决办法: 请勿在表达式索引中使用子查询。

GAUSS-01294: "cannot use aggregate function in index expression"

SQLSTATE: 42803

错误原因: 表达式索引中不能使用聚集操作。

解决办法: 请勿在表达式索引中使用聚集操作。

GAUSS-01295: "functions in index expression must be marked IMMUTABLE"

SQLSTATE: 42P17

错误原因: 表达式索引中的函数必须不能变化。

解决办法: 保证表达式索引中的函数为不变函数。

GAUSS-01296: "could not determine which collation to use for index expression"

SQLSTATE: 42P22

错误原因: 无法决定在表达式索引中使用哪种排序方式。

解决办法: 需要在创建表达式索引时指定排序方式。

GAUSS-01297: "operator %s is not commutative"

SQLSTATE: 42809

错误原因: 操作符不可交换。

解决办法: 排他约束中必须使用可交换操作符。

GAUSS-01298: "cache lookup failed for opfamily %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-01299: "operator %s is not a member of operator family '%s'"

SQLSTATE: 42809

错误原因: 操作符不在期望的操作符家族之列。

解决办法: 排除操作符必须和索引约束有关。

GAUSS-01300: "access method '%s' does not support ASC/DESC options"

SQLSTATE: 0A000

错误原因: 索引处理方法不支持ASC/DESC选项。

解决办法: 重建索引并设置索引列的访问方法（pg_am中amcanorder字段）为true支持索引列排序。
