---
title: GAUSS-04001 - GAUSS-04100
summary: GAUSS-04001 - GAUSS-04100
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04001 - GAUSS-04100

<br/>

## GAUSS-04001 - GAUSS-04010

<br/>

GAUSS-04001: "invalid option '%s'"

SQLSTATE: HV00D

错误原因: 参数选项不合法。

解决办法: 请检查输入参数，再次执行操作。

GAUSS-04002: "SHARED mode can only be used with TEXT format"

SQLSTATE: XX000

错误原因: 用户尝试在使用shared模式的GDS外表时使用TEXT以外的格式。

解决办法: 报错对应的GDS外表仅能够读取TEXT格式文件流，请在导入规格中做对应修改。

GAUSS-04003: "can't find error record table '%s'"

SQLSTATE: XX000

错误原因: 系统内存不足，导致错误表加载失败。

解决办法: 待操作系统回收部分内存后，再次执行操作。

GAUSS-04005: "could not read from file: %m"

SQLSTATE: XX000

错误原因: 文件不存在或权限不正确。

解决办法: 请检查文件是否存在或权限是否正确，再次执行操作。

GAUSS-04006: "unable to open URL '%s'"

SQLSTATE: XX000

错误原因: 文件不存在或权限不正确。

解决办法: 请检查文件是否存在或权限是否正确，再次执行操作。

GAUSS-04007: "fill_missing_fields can't be set while '%s' is NOT NULL"

SQLSTATE: 42601

错误原因: 非空列不能设置fill_missing_fields。

解决办法: 请检查输入参数后，再次执行操作。

GAUSS-04008: "unsupport BINARY format"

SQLSTATE: XX000

错误原因: 不支持二进制格式数据。

解决办法: 请检查输入数据格式后，再次执行操作。

GAUSS-04009: "only both text && csv formats are supported for foreign table"

SQLSTATE: 0A000

错误原因: 外表仅支持text和csv格式数据。

解决办法: 检查外表数据格式，再次执行操作。

GAUSS-04010: "%s doesn't exist, please create it first"

SQLSTATE: XX000

错误原因: 文件夹不存在或权限不正确。

解决办法: 请检查文件夹是否存在或权限是否正确，再次执行操作。

<br/>

## GAUSS-04011 - GAUSS-04020

<br/>

GAUSS-04011: "%s exists and is a file, please remove it first and create directory"

SQLSTATE: XX000

错误原因: 文件夹不存在或权限不正确。

解决办法: 请删除并创建文件夹或使用其他的路径，再次执行操作。

GAUSS-04012: "location filepath is too long when importing data to foreign table"

SQLSTATE: 22023

错误原因: 外表的文件夹路径太长。

解决办法: 请使用合适的路径，再次执行操作。

GAUSS-04014: "no Snowball stemmer available for language '%s' and encoding '%s'"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04015: "multiple Language parameters"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04016: "unrecognized Snowball parameter: '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04017: "missing Language parameter"

SQLSTATE: 22023

错误原因: Language参数缺失。

解决办法: 请增加语言参数后，重新执行操作。

GAUSS-04018: "could not translate host name '%s' to address: %s"

SQLSTATE: XX000

错误原因: 不能将主机名转换为IP地址。

解决办法: 请检查hba.conf中配置的主机名称，IP地址是否正确。

GAUSS-04019: "unsupported integer size %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04020: "no data left in message"

SQLSTATE: 08P01

错误原因: 消息中已经没有数据。

解决办法: 数据库内部通信异常，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04021 - GAUSS-04030

<br/>

GAUSS-04021: "insufficient data left in message"

SQLSTATE: 08P01

错误原因: message中剩余的长度小于message首获取的长度值。

解决办法: 检查当前字符串。

GAUSS-04022: "invalid string in message"

SQLSTATE: 08P01

错误原因: 消息无效。

解决办法: 数据库内部通信异常，请联系技术支持工程师提供技术支持。

GAUSS-04023: "invalid message format"

SQLSTATE: 08P01

错误原因: 消息格式无效。

解决办法: 数据库内部通信异常，请联系技术支持工程师提供技术支持。

GAUSS-04024: "Postgres-XC does not support large object yet"

SQLSTATE: 0A000

错误原因: 目前还不支持large object。

解决办法: 不建议使用此特性。

GAUSS-04025: "invalid large-object descriptor: %d"

SQLSTATE: 42704

错误原因: 无效的大对象描述符。

解决办法: 不支持的特性。

GAUSS-04026: "permission denied for large object %u"

SQLSTATE: 42501

错误原因: 目前还不支持超大对象。

解决办法: 不支持的特性。

GAUSS-04027: "large object descriptor %d was not opened for writing"

SQLSTATE: 55000

错误原因: 目前还不支持超大对象。

解决办法: 不支持的特性。

GAUSS-04028: "must be system admin to use server-side lo_import()"

SQLSTATE: 42501

错误原因: 必须为系统管理员权限才能使用服务器端的lo_import函数。

解决办法: 不支持的特性。

GAUSS-04029: "could not open server file '%s': %m"

SQLSTATE: XX000

错误原因: 打开服务器文件失败。

解决办法: 不支持的特性。

GAUSS-04030: "could not read server file '%s': %m"

SQLSTATE: XX000

错误原因: 读取服务器文件失败。

解决办法: 不支持的特性。

<br/>

## GAUSS-04031 - GAUSS-04040

<br/>

GAUSS-04031: "must be system admin to use server-side lo_export()"

SQLSTATE: 42501

错误原因: 必须为系统管理员权限才能使用服务器端的lo_export函数。

解决办法: 禁止使用不支持的函数。

GAUSS-04032: "could not create server file '%s': %m"

SQLSTATE: XX000

错误原因: 创建服务器文件失败。

解决办法: 禁止使用不支持的函数。

GAUSS-04033: "could not chmod server file '%s': %m"

SQLSTATE: XX000

错误原因: 修改服务器文件权限失败。

解决办法: 禁止使用不支持的函数。

GAUSS-04034: "could not write server file '%s': %m"

SQLSTATE: XX000

错误原因: 写入服务器文件失败。

解决办法: 禁止使用不支持的函数。

GAUSS-04035: "connection requires a valid client certificate"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04038: "pg_hba.conf rejects connection for host '%s', user '%s', database '%s', %s"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04039: "pg_hba.conf rejects connection for host '%s', user '%s', database '%s'"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04040: "no pg_hba.conf entry for host '%s', user '%s', database '%s', %s"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04041 - GAUSS-04050

<br/>

GAUSS-04041: "no pg_hba.conf entry for host '%s', user '%s', database '%s'"

SQLSTATE: 28000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04045: "Invalid username/password,login denied."

SQLSTATE: 28000

错误原因: 用户名或密码无效，登录失败。

解决办法: 检查登录的用户名和密码是否有效。

GAUSS-04047: "SSPI is not supported in protocol version 2"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04048: "out of memory"

SQLSTATE: XX000

错误原因: 无法申请内存。

解决办法: 请检查系统看是否有足够的内存。

GAUSS-04050: "malloc failed"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04051 - GAUSS-04060

<br/>

GAUSS-04051: "could not set the cipher list (no valid ciphers available)"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04052: "sslciphers can not be null"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04053: "unrecognized ssl ciphers name: '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04055: "could not load server certificate file '%s': %s"

SQLSTATE: F0000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04057: "private key file '%s' has group or world access"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04058: "could not load private key file '%s': %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04059: "check of private key '%s'failed: %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04061 - GAUSS-04070

<br/>

GAUSS-04061: "could not load the ca certificate file"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04062: "could not load root certificate file '%s': %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04063: "could not load SSL certificate revocation list file '%s': %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04068: "cipher file '%s' has group or world access"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04069: "rand file '%s' has group or world access"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04070: "spin.c does not support S_LOCK_FREE()"

SQLSTATE: XX000

错误原因: 不支持S_LOCK_FREE()。

解决办法: 检查操作系统是否在支持的范围内。

<br/>

## GAUSS-04071 - GAUSS-04080

<br/>

GAUSS-04071: "not enough elements in RWConflictPool to record a read/write conflict"

SQLSTATE: 53200

错误原因: RWConflictPool 内存不足，不能记录更多的读写冲突。

解决办法: 增加max_connections设置，或在新事务中重新执行。

GAUSS-04072: "not enough elements in RWConflictPool to record a potential read/write conflict"

SQLSTATE: 53200

错误原因: RWConflictPool 内存不足，不能记录更多的读写冲突。

解决办法: 增加max_connections设置，或在新事务中重新执行。

GAUSS-04073: "not enough shared memory for elements of data structure '%s' (%lu bytes requested)"

SQLSTATE: 53200

错误原因: 没有足够共享内存初始化PredXactList。

解决办法: 检查系统剩余内存是否足够。

GAUSS-04074: "cannot use serializable mode in a hot standby"

SQLSTATE: 0A000

错误原因: hot standby不能设置为可串行化模式。

解决办法: 检查default_transaction_isolation设置，设置为repeatable read。

GAUSS-04075: "a snapshot-importing transaction must not be READ ONLY DEFERRABLE"

SQLSTATE: 0A000

错误原因: 不支持READ ONLY DEFERRABLE事务导入快照。

解决办法: 请调整事务级别。

GAUSS-04076: "could not serialize access due to read/write dependencies among transactions"

SQLSTATE: 40001

错误原因: 可串行化下读写冲突。

解决办法: 尝试再次执行。

GAUSS-04077: "unrecognized return value from HeapTupleSatisfiesVacuum: %u"

SQLSTATE: XX000

错误原因: HeapTupleSatisfiesVacuum返回值不识别。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04078: "deadlock seems to have disappeared"

SQLSTATE: XX000

错误原因: 死锁非正常消失。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04079: "inconsistent results during deadlock check"

SQLSTATE: XX000

错误原因: 死锁检测出现不一致结果。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04080: "deadlock detected"

SQLSTATE: 40P01

错误原因: 检测到死锁。

解决办法: 请使用SELECT pg_cancel_backend结束掉死锁线程，再次执行操作。

<br/>

## GAUSS-04081 - GAUSS-04090

<br/>

GAUSS-04081: "proc header uninitialized"

SQLSTATE: XX000

错误原因: ProcGlobal 未初始化。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04082: "you already exist"

SQLSTATE: XX000

错误原因: 进程已退出。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04084: "could not set timer for process wakeup"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04085: "Lock wait timeout: thread %lu on node %s waiting for %s on %s after %ld.%03d ms"

SQLSTATE: YY002

错误原因: 分布式锁等待超时。

解决办法: 检查并发执行情况。

GAUSS-04086: "could not disable timer for process wakeup"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04087: "stuck spinlock (%p) detected at %s:%d"

SQLSTATE: XX000

错误原因: spinlock卡住时间过长。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04089: "unrecognized lock mode: %d"

SQLSTATE: XX000

错误原因: lock mode不识别。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04090: "cannot acquire lock mode %s on database objects while recovery is in progress"

SQLSTATE: 55000

错误原因: 恢复过程中申请的锁级别不正确。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04091 - GAUSS-04100

<br/>

GAUSS-04091: "proclock table corrupted"

SQLSTATE: XX000

错误原因: proclock 哈希表损坏。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04092: "LockAcquire failed"

SQLSTATE: XX000

错误原因: 常规锁申请失败。

解决办法: 检查并发执行情况。

GAUSS-04093: "lock table corrupted"

SQLSTATE: XX000

错误原因: lock 哈希表损坏。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04094: "lock %s on object %u/%u/%u is already held"

SQLSTATE: XX000

错误原因: 加锁的对象已经持有了这个锁。

解决办法: 检查对象加锁情况。

GAUSS-04095: "locallock table corrupted"

SQLSTATE: XX000

错误原因: locallock 哈希表损坏。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04096: "failed to re-find shared lock object"

SQLSTATE: XX000

错误原因: 重新查找shared lock失败。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04097: "failed to re-find shared proclock object"

SQLSTATE: XX000

错误原因: 重新查找shared proclock失败。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04098: "too many conflicting locks found"

SQLSTATE: XX000

错误原因: 常规锁冲突数>MaxBackends。

解决办法: 降低业务并发。

GAUSS-04099: "cannot PREPARE while holding both session-level and transaction-level locks on the same object"

SQLSTATE: 0A000

错误原因: 不能对同一对象同时持有session级和事务级锁。

解决办法: 检查对象加锁情况。

GAUSS-04100: "we seem to have dropped a bit somewhere"

SQLSTATE: XX000

错误原因: 持有锁和放锁mask不匹配。

解决办法: 请联系技术支持工程师提供技术支持。
