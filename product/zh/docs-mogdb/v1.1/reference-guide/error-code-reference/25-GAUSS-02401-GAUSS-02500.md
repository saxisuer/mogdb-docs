---
title: GAUSS-02401 - GAUSS-02500
summary: GAUSS-02401 - GAUSS-02500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-02401 - GAUSS-02500

<br/>

## GAUSS-02401 - GAUSS-02410

<br/>

GAUSS-02401: "cannot lock rows in view '%s'"

SQLSTATE: 42809

错误原因: 错误的对象类型，不能为视图加锁。

解决办法: 修改SQL语句。

GAUSS-02402: "cannot lock rows in foreign table '%s'"

SQLSTATE: 42809

错误原因: 错误的对象类型，不能为外表加锁。

解决办法: 修改SQL语句。

GAUSS-02403: "cannot lock rows in relation '%s'"

SQLSTATE: 42809

错误原因: 错误的对象类型，存在不能加锁的对象。

解决办法: 修改SQL语句。

GAUSS-02404: "new row for relation '%s' violates check constraint '%s'"

SQLSTATE: 23514

错误原因: 新的数据和check约束冲突。

解决办法: 检查数据是否和check约束冲突。

GAUSS-02405: "failed to find ExecRowMark for rangetable index %u"

SQLSTATE: XX000

错误原因: 通过index在Rangetable中未找到ExecRowMark。

解决办法: 检查SQL语句和执行计划。

GAUSS-02407: "t_xmin is uncommitted in tuple to be updated"

SQLSTATE: XX000

错误原因: 欲更新的元组xmin未提交。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02408: "abort transaction due to concurrent update"

SQLSTATE: 40001

错误原因: 事务不允许对同一个元组进行并发更新，所以事务回滚。

解决办法: 重新执行失败回滚的更新事务。

GAUSS-02409: "EvalPlanQual doesn't support locking rowmarks"

SQLSTATE: XX000

错误原因: 读已提交模式下，要检查更新的元组能不能被操作，对应的表不能加锁。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02410: "failed to fetch tuple for EvalPlanQual recheck"

SQLSTATE: 22P08

错误原因: recheck被更新的元组时，获取原则失败。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02411 - GAUSS-02420

<br/>

GAUSS-02411: "failed to fetch tuple for EvalPlanQual recheck"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02412: "found self tuple multiple times in index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02416: "unrecognized hashjoin state: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02417: "could not rewind hash-join temporary file: %m"

SQLSTATE: XX000

错误原因: 下盘临时文件操作失败。

解决办法: 根据提示的错误信息进行分析。

GAUSS-02419: "could not read from hash-join temporary file: %m"

SQLSTATE: XX000

错误原因: 下盘临时文件操作失败。

解决办法: 根据提示的错误信息进行分析。

GAUSS-02420: "Result nodes do not support mark/restore"

SQLSTATE: XX000

错误原因: result算子不支持mark/restore操作。

解决办法: 计划问题，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02421 - GAUSS-02430

<br/>

GAUSS-02421: "Hash node does not support ExecProcNode call convention"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02422: "BitmapAnd doesn't support zero inputs"

SQLSTATE: XX000

错误原因: 系统内部错误。BitmapAnd算子的输入为空。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02424: "ExecLockRows:target relation cannot be NULL"

SQLSTATE: 40001

错误原因: BitmapAnd算子的输入为空。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02428: "aggregate function %u called as normal function"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02431 - GAUSS-02440

<br/>

GAUSS-02434: "latch already owned"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02435: "cannot wait on a latch owned by another process"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02436: "failed to create event for socket: error code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02437: "failed to set up event for socket: error code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02439: "WaitForMultipleObjects() failed: error code %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02440: "failed to enumerate network events: error code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02441 - GAUSS-02450

<br/>

GAUSS-02441: "unexpected return code from WaitForMultipleObjects(): %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02442: "pipe() failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02443: "fcntl() failed on read-end of self-pipe: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02444: "fcntl() failed on write-end of self-pipe: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02445: "poll() failed: %m"

SQLSTATE: XX000

错误原因: 系统调用poll失败。

解决办法: 数据库内部错误，请根据系统错误提示信息，请联系技术支持工程师提供技术支持。

GAUSS-02446: "read() on self-pipe failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02447: "unexpected EOF on self-pipe"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02448: "sem_open('%s') failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02449: "sem_init failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02450: "too many semaphores created"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02451 - GAUSS-02460

<br/>

GAUSS-02451: "sem_trywait failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02452: "sem_wait failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02453: "sem_post failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02454: "could not get size for full pathname of datadir %s: error code %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02455: "could not allocate memory for shared memory name"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02456: "could not generate full pathname for datadir %s: error code %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02457: "could not create shared memory segment: error code %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02458: "pre-existing shared memory block is still in use"

SQLSTATE: XX000

错误原因:  WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02461 - GAUSS-02470

<br/>

GAUSS-02461: "reattaching to shared memory returned unexpected address (got %p, expected %p)"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02462: "reattaching to shared memory returned non-PostgreSQL memory"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02463: "could not create signal listener pipe for PID %d: error code %lu"

SQLSTATE: XX000

错误原因:  WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02464: "could not create semaphores: %m"

SQLSTATE: XX000

错误原因: 创建的信号量超过系统限制。

解决办法: 调整系统内核参数（/proc/sys/kernel/sem），增加允许创建的信号量，或调小数据库max_connections 。

GAUSS-02465: "semop(id=%d) failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02466: "could not create shared memory segment: %m"

SQLSTATE: XX000

错误原因: 共享内存大小超过了系统限制。

解决办法: 调整系统共享内存内核参数（SHMMAX，SHMMIN），或调小数据库shared_buffers。

GAUSS-02467: "shmat(id=%d) failed: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02468: "could not stat data directory '%s': %m"

SQLSTATE: XX000

错误原因: data目录不存在或权限错误。

解决办法: 检查data数据目录是否存在或权限是否正确。

GAUSS-02470: "could not create semaphore: error code %d"

SQLSTATE: XX000

错误原因: WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02471 - GAUSS-02480

<br/>

GAUSS-02471: "could not lock semaphore: error code %d"

SQLSTATE: XX000

错误原因: WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02472: "could not unlock semaphore: error code %d"

SQLSTATE: XX000

错误原因: WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02473: "could not try-lock semaphore: error code %d"

SQLSTATE: XX000

错误原因: WIN32平台不涉及。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02474: "dynamic load not supported"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02475: "out of on_shmem_exit slots"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02476: "too late to call tuplestore_set_eflags"

SQLSTATE: 23514

错误原因: 系统内部错误: 调用tuplestore_set_eflags过晚。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02477: "too late to require new tuplestore eflags"

SQLSTATE: 23514

错误原因: 系统内部错误: 设置新的tuplestore 标记过晚。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02478: "tuplestore seek failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 寻找不到对应位置的tuple数据信息。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02479: "invalid tuplestore state"

SQLSTATE: XX006

错误原因: 系统内部错误: 当前tuplestor状态错误。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02481 - GAUSS-02490

<br/>

GAUSS-02481: "tuplestore seek to EOF failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 从文件中读取数据时未搜到结束符。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02482: "bogus tuple length in backward scan"

SQLSTATE: 22P07

错误原因: NA

解决办法: NA

GAUSS-02483: "tuplestore seek to start failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 读取数据时未搜到开始标记。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02484: "unexpected end of tape"

SQLSTATE: 22000

错误原因: 系统内部错误: 从逻辑Tape上读取数据过程中遇到非预期的结束。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02485: "unexpected end of data"

SQLSTATE: 22P07

错误原因: 系统内部错误: 从逻辑Tape上读取数据过程中遇到非预期的数据终止。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02487: "too late to call batchstore_set_eflags"

SQLSTATE: XX006

错误原因: 系统内部错误: 调用batchstore_set_eflags过晚。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02488: "too late to require new batchstore eflags"

SQLSTATE: XX006

错误原因: 系统内部错误: 设置新的batchstore 标记过晚。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02489: "batchstore seek failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 寻找不到对应位置的数据信息。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02490: "invalid batchstore state"

SQLSTATE: XX006

错误原因: 系统内部错误: 无效的batchstore状态。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-02491 - GAUSS-02500

<br/>

GAUSS-02491: "batchstore seek to EOF failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 从文件中读取数据时未读到结束符

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02492: "batchstore seek to start failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 读取数据时未搜到开始标记。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02493: "could not write block %ld of temporary file: %m"

SQLSTATE: XX000

错误原因: 系统内部写临时文件出错。

解决办法: 请检查磁盘是否有剩余空间，请联系技术支持工程师提供技术支持。

GAUSS-02494: "could not read block %ld of temporary file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误:无法从临时文件中读取block数据块。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02495: "invalid logtape state: should be dirty"

SQLSTATE: 22000

错误原因: 系统内部错误: 无效的logtape状态。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02496: "function %u returned NULL"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-02497: "insufficient memory allowed for sort"

SQLSTATE: 53000

错误原因: 执行sort过程中内存不足。

解决办法: work_mem过小，请调大work_mem参数取值。

GAUSS-02498: "invalid tuplesort state"

SQLSTATE: XX006

错误原因: 系统内部错误: 无效的行存排序状态。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02499: "retrieved too many tuples in a bounded sort"

SQLSTATE: 23001

错误原因: 系统内部错误: 在排序过程中，遇到了非预期的结束符，导致在bounded排序时，所需的数据不足。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-02500: "tuplesort_restorepos failed"

SQLSTATE: 22P07

错误原因: 系统内部错误: 在做mergejoin过程中无法重新计量当前位置。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。
