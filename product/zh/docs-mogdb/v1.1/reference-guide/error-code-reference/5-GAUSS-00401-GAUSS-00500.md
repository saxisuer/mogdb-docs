---
title: GAUSS-00401 - GAUSS-00500
summary: GAUSS-00401 - GAUSS-00500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00401 - GAUSS-00500

<br/>

## GAUSS-00401 - GAUSS-00410

<br/>

GAUSS-00401: "access method 'psort' does not support index expressions"

SQLSTATE: 0A000

错误原因: 在CREATE INDEX或ALTER TABLE语法中INDEX子句中指定的访问方式如果为psort时，INDEX子句中不支持表达式。

解决办法: 在CREATE INDEX或ALTER TABLE语法中INDEX子句中指定的访问方式如果为psort时，请检查INDEX子句中是否存在表达式，并修改语法。

GAUSS-00402: "Partition key value can not be null"

SQLSTATE: 42601

错误原因: 创建范围分区表时，通过PARTATION BY RANGE （partition_key）(range_partition_definition_list)指定的分区键值不能为空。

解决办法: 请检查创建范围分区表时，通过PARTATION BY RANGE （partition_key）指定的分区键是否为空，并增加有效的分区键值。

GAUSS-00403: "partition key value must be const or const-evaluable expression"

SQLSTATE: 42601

错误原因: 创建范围分区表时，通过PARTATION BY RANGE （partition_key）(range_partition_definition_list)指定的分区键值不可度量。

解决办法: 创建范围分区表时，通过PARTATION BY RANGE （partition_key）(range_partition_definition_list)指定的分区键值时，需要保证分区键值必须为可度量的常量或常量表达式。

GAUSS-00404: "argument name '%s' used more than once"

SQLSTATE: 42601

错误原因: 语法错误，参数名重名。

解决办法: 修改重名的参数名。

GAUSS-00405: "positional argument cannot follow named argument"

SQLSTATE: 42601

错误原因: 语法错误，位置参数不能再命名参数之后。

解决办法: 修改参数位置。

GAUSS-00406: "%s(*) specified, but %s is not an aggregate function"

SQLSTATE: 42809

错误原因: 指定了%s(*)，但是函数却不是聚集函数。

解决办法: 选择一个有效的聚集函数。

GAUSS-00407: "DISTINCT specified, but %s is not an aggregate function"

SQLSTATE: 42809

错误原因: 指定了DISTINCT，但是函数却不是聚集函数。

解决办法: 选择一个有效的聚集函数。

GAUSS-00408: "ORDER BY specified, but %s is not an aggregate function"

SQLSTATE: 42809

错误原因: 指定了ORDER BY，但是函数却不是聚集函数。

解决办法: 选择一个有效的聚集函数。

GAUSS-00409: "OVER specified, but %s is not a window function nor an aggregate function"

SQLSTATE: 42809

错误原因: 指定了OVER，但是函数却不是窗口函数或者聚集函数。

解决办法: 选择一个有效的窗口函数或聚集函数。

GAUSS-00410: "function %s is not unique"

SQLSTATE: 42725

错误原因: 函数不唯一。

解决办法: 增加精确的类型转换来选择一个最佳的函数。

<br/>

## GAUSS-00411 - GAUSS-00420

<br/>

GAUSS-00411: "function %s does not exist"

SQLSTATE: 42883

错误原因: 函数不存在。

解决办法: 检查是否已定义需要用到的函数。

GAUSS-00412: "%s(*) must be used to call a parameterless aggregate function"

SQLSTATE: 42809

错误原因: 在创建函数或表达式为聚合函数时聚合函数的参数为空但不为"*"。

解决办法: 在创建函数或表达式为聚合函数，需要确认如果聚合函数的参数为空，那么参数必须为星号"*"。

GAUSS-00413: "aggregates cannot return sets"

SQLSTATE: 42P13

错误原因: 创建函数或表达式为聚合函数时聚合函数不能返回了集合类型。

解决办法: 创建函数或表达式为聚合函数时，聚合函数不能返回集合类型，请确认聚合函数的返回类型是否正确，并修改为非集合类型。

GAUSS-00414: "aggregates cannot use named arguments"

SQLSTATE: 0A000

错误原因: 调用函数或表达式为聚合函数时聚合函数参数错误使用了参数名称。

解决办法: 调用函数或表达式为聚合函数时聚合函数参数不能指定为参数名称，必须要使用有效值。

GAUSS-00415: "window function call requires an OVER clause"

SQLSTATE: 42809

错误原因: 调用函数或表达式为窗口函数时，OVER子句未定义。

解决办法: 调用函数或表达式为窗口函数时，该窗口函数必须使用OVER子句定义，请检查并修改语法。

GAUSS-00416: "DISTINCT is not implemented for window functions"

SQLSTATE: 0A000

错误原因: 调用函数或表达式为窗口函数时，该窗口函数存在DISTINCT子句。

解决办法: 调用函数或表达式为窗口函数时，窗口函数中不能包含DISTINCT子句，请检查并修改语法。

GAUSS-00417: "aggregate ORDER BY is not implemented for window functions"

SQLSTATE: 0A000

错误原因: 调用函数或表达式为窗口函数时，该窗口函数存在ORDER BY子句。

解决办法: 调用函数或表达式为窗口函数时，窗口函数中不能包含ORDER BY子句，请检查并修改语法。

GAUSS-00418: "window functions cannot return sets"

SQLSTATE: 42P13

错误原因: 创建函数或表达式为窗口函数时，该窗口函数不能返回集合类型。

解决办法: 创建函数或表达式为窗口函数时，窗口函数不能返回集合类型，请确认窗口函数的返回类型是否正确，并修改为非集合类型。

GAUSS-00419: "window functions cannot use named arguments"

SQLSTATE: 0A000

错误原因: 窗口函数为函数或表达式时，参数不能使用参数名称，必须使用参数的值。

解决办法: 调用函数或表达式为窗口函数时，窗口函数参数不能指定为参数名称，必须要使用有效值。

GAUSS-00420: "not enough default arguments"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00421 - GAUSS-00430

<br/>

GAUSS-00421: "function %s asks parameters"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00422: "aggregate %s(*) does not exist"

SQLSTATE: 42883

错误原因: 聚合函数的参数无效。

解决办法: 请检查确认聚合函数的参数数目是否为0。

GAUSS-00423: "aggregate %s does not exist"

SQLSTATE: 42883

错误原因: 无效的聚合函数。

解决办法: 请检查确认聚合函数是否在pg_proc表中存在。

GAUSS-00424: "function %s is not an aggregate"

SQLSTATE: 42809

错误原因: 创建的函数不是有效的聚合函数。

解决办法: 请确认函数是否为聚合函数，使用"（*）"作为参数的必须为聚合函数。

GAUSS-00426: "inconsistent types deduced for parameter $%d"

SQLSTATE: 42P08

错误原因: 不支持的用法。

解决办法: 修改SQL Statement。

GAUSS-00427: "could not determine data type of parameter $%d"

SQLSTATE: 42P08

错误原因: 参数类型无法确定。

解决办法: 修改参数值为系统可识别的类型，系统可识别类型见zh-cn_topic_0237121926.html。

GAUSS-00428: "could not create syslogger data transfer thread: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00429: "could not create pipe for syslog: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00430: "could not redirect stdout: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00431 - GAUSS-00440

<br/>

GAUSS-00431: "could not redirect stderr: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00432: "checkpoint request failed"

SQLSTATE: XX000

错误原因: checkpoint内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00433: "ForwardFsyncRequest must not be called in checkpointer"

SQLSTATE: XX000

错误原因: checkpoint内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00434: "could not open archive status directory '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00435: "no free worker found"

SQLSTATE: XX000

错误原因: 目前没有可用的清理线程两阶段提交事务。

解决办法: 将GUC参数autovacuum_max_workers调大。

GAUSS-00436: "cache lookup failed for database %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00437: "the options of -M is not recognized"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00438: "-%s requires a value"

SQLSTATE: 42601

错误原因: 使用命令时指定了某个参数，但没有指定该参数对应的值。

解决办法: 使用命令时如果指定了某个需要有值的参数，则需要指定该参数对应的值。

GAUSS-00439: "-c %s requires a value"

SQLSTATE: 42601

错误原因: 使用命令时指定了某个参数，但没有指定该参数对应的值。

解决办法: 使用命令时如果指定了某个需要有值的参数，则需要指定该参数对应的值。

GAUSS-00440: "invalid list syntax for 'listen_addresses'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00441 - GAUSS-00450

<br/>

GAUSS-00442: "could not create any TCP/IP sockets"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00444: "no socket created for listening"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00445: "could not create I/O completion port for child queue"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00446: "could not load pg_hba.conf"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00447: "%s: could not locate my own executable path"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00448: "%s: could not locate matching postgres executable"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00449: "could not open directory '%s': %m"

SQLSTATE: XX000

错误原因: 打开某个目录失败，可能原因是目录不存在，权限不对，或目录损坏。

解决办法: 可根据错误信息分析具体原因，如果目录不存在，需要创建对应目录；权限不对则修改权限；目录损坏则修复文件系统或磁盘。

GAUSS-00450: "data directory '%s' does not exist"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00451 - GAUSS-00460

<br/>

GAUSS-00451: "could not read permissions of directory '%s': %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00452: "specified data directory '%s' is not a directory"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00453: "data directory '%s' has wrong ownership"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00454: "data directory '%s' has group or world access"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00457: "hot standby is not possible because wal_level was not set to 'hot_standby'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00458: "when starting as dual mode, we must ensure wal_level was not 'minimal' and max_wal_senders was set at least 1"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00461 - GAUSS-00470

<br/>

GAUSS-00466: "sorry, too many clients already"

SQLSTATE: 53300

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00471 - GAUSS-00480

<br/>

GAUSS-00474: "fcntl F_GETFD failed!"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00475: "fcntl F_SETFD failed!"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00476: "could not set timer for startup packet timeout"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00477: "could not disable timer for startup packet timeout"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00478: "out of memory"

SQLSTATE: 53200

错误原因: 无法申请内存。

解决办法: 请检查系统看是否有足够的内存。

<br/>

## GAUSS-00481 - GAUSS-00490

<br/>

GAUSS-00481: "parameter error in ParseHaListenAddr()"

SQLSTATE: XX000

错误原因: ParseHaListenAddr函数参数错误。

解决办法: 检查传入参数为何为空指针。

GAUSS-00482: "must be system admin to reset statistics counters"

SQLSTATE: 42501

错误原因: 系统管理员才能重置统计计数。

解决办法: 切换数据库用户为系统管理员。

GAUSS-00483: "unrecognized reset target: '%s'"

SQLSTATE: 22023

错误原因: 重置目标参数错误。

解决办法: 参数必须为bgwriter。

GAUSS-00484: "could not read statistics message: %m"

SQLSTATE: XX000

错误原因: pgStat读入统计消息出错。

解决办法: 数据库内部线程通信出错，请联系技术支持工程师提供技术支持。

GAUSS-00485: "database hash table corrupted during cleanup - abort"

SQLSTATE: XX000

错误原因: 数据库在清理过程中哈希表损坏。

解决办法: 数据库内部错误，请联系技术支持工程师提供技术支持。

GAUSS-00486: "could not create sysauditor data transfer thread: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00487: "could not create pipe for sysaudit: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00488: "could not write to audit file: %m"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00489: "set-valued function called in context that cannot accept a set"

SQLSTATE: 0A000

错误原因: 返回结果集的函数不能返回结果集。

解决办法: 检查函数定义是否支持返回结果集。

GAUSS-00490: "materialize mode required, but it is not allowed in this context"

SQLSTATE: 0A000

错误原因: 返回tuplestore在当前上下文不支持。

解决办法: 修改返回值类型。

<br/>

## GAUSS-00491 - GAUSS-00500

<br/>

GAUSS-00491: "return type must be a row type"

SQLSTATE: XX000

错误原因: 构建的元组描述符不是一个行结果信息。

解决办法: 重新执行查询，如果仍然是报错，请联系技术支持工程师提供技术支持。

GAUSS-00492: "attribute count of the return row type not matched"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00493: "permission denied to query audit"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00494: "permission denied to delete audit"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00495: "conflicting or redundant options"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00496: "option '%s' not recognized"

SQLSTATE: XX000

错误原因: 不识别copy中某个选项的值。

解决办法: 检查copy中该不识别的参数是否正确。

GAUSS-00497: "%d is not a valid encoding code"

SQLSTATE: 42704

错误原因: 将字符串转换为ASCII编码格式时，未使用指定的编码标识符。

解决办法: 将字符串转换为ASCII编码格式时，请确认使用指定的编码标识符编号必须小于42。

GAUSS-00498: "%s is not a valid encoding name"

SQLSTATE: 42704

错误原因: 将字符串转换为ASCII编码格式时，使用的字符编码名无效。

解决办法: "将字符串转换为ASCII编码格式时，请确认使用指定的编码标识符名必须为以下列表中的有效名称。

GAUSS-00500: "permission denied to create database"

SQLSTATE: 42501

错误原因: 当前用户角色没有权限创建数据库。

解决办法: 只有拥有CREATEDB权限的用户或系统管理员才可以创建新数据库，请确认用户权限是否正确。
