---
title: SHARED_MEMORY_DETAIL
summary: SHARED_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# SHARED_MEMORY_DETAIL

查询当前节点所有已产生的共享内存上下文的使用信息。

**表 1** SHARED_MEMORY_DETAIL字段

| **名称**    | **类型** | **描述**                       |
| :---------- | :------- | :----------------------------- |
| contextname | text     | 内存上下文的名称。             |
| level       | smallint | 内存上下文的级别。             |
| parent      | text     | 上级内存上下文。               |
| totalsize   | bigint   | 共享内存总大小(单位: 字节)。   |
| freesize    | bigint   | 共享内存剩余大小(单位: 字节)。 |
| usedsize    | bigint   | 共享内存使用大小(单位: 字节)。 |
