---
title: STATEMENT_RESPONSETIME_PERCENTILE
summary: STATEMENT_RESPONSETIME_PERCENTILE
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_RESPONSETIME_PERCENTILE

获取MogDB SQL响应时间P80，P95分布信息。

**表 1** STATEMENT_RESPONSETIME_PERCENTILE的字段

| **名称** | **类型** | **描述**                                 |
| :------- | :------- | :--------------------------------------- |
| p80      | bigint   | MogDB 80%的SQL的响应时间（单位: 微秒）。 |
| p95      | bigint   | MogDB 95%的SQL的响应时间（单位: 微秒）。 |
