---
title: STATEMENT_USER_COMPLEX_HISTORY
summary: STATEMENT_USER_COMPLEX_HISTORY
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_USER_COMPLEX_HISTORY

STATEMENT_USER_COMPLEX_HISTORY系统表显示数据库主节点执行作业结束后的负载管理记录。此数据是从内核中转储到系统表中的数据。具体的字段请参考附录-表5。
