---
title: STAT_DATABASE
summary: STAT_DATABASE
author: Guo Huan
date: 2021-04-19
---

# STAT_DATABASE

视图将包含本节点中每个数据库的统计信息。

**表 1** STAT_DATABASE字段

| **名称**       | **类型**                 | **描述**                                                                                                                     |
| :------------- | :----------------------- | :--------------------------------------------------------------------------------------------------------------------------- |
| datid          | oid                      | 数据库的OID。                                                                                                                |
| datname        | name                     | 此数据库的名称。                                                                                                             |
| numbackends    | integer                  | 当前连接到该数据库的后端数。这是在返回一个反映目前状态值的视图中唯一的列；自上次重置所有其他列返回累积值。                   |
| xact_commit    | bigint                   | 此数据库中已经提交的事务数。                                                                                                 |
| xact_rollback  | bigint                   | 此数据库中已经回滚的事务数。                                                                                                 |
| blks_read      | bigint                   | 在这个数据库中读取的磁盘块的数量。                                                                                           |
| blks_hit       | bigint                   | 高速缓存中已经发现的磁盘块的次数，这样读取是不必要的（这只包括PostgreSQL缓冲区高速缓存，没有操作系统的文件系统缓存）。       |
| tup_returned   | bigint                   | 通过数据库查询返回的行数。                                                                                                   |
| tup_fetched    | bigint                   | 通过数据库查询抓取的行数。                                                                                                   |
| tup_inserted   | bigint                   | 通过数据库查询插入的行数。                                                                                                   |
| tup_updated    | bigint                   | 通过数据库查询更新的行数。                                                                                                   |
| tup_deleted    | bigint                   | 通过数据库查询删除的行数。                                                                                                   |
| conflicts      | bigint                   | 由于数据库恢复冲突取消的查询数量（只在备用服务器发生的冲突）。请参见STAT_DATABASE_CONFLICTS获取更多信息。                    |
| temp_files     | bigint                   | 通过数据库查询创建的临时文件数量。计算所有临时文件，不论为什么创建临时文件（比如排序或者哈希），而且不管log_temp_files设置。 |
| temp_bytes     | bigint                   | 通过数据库查询写入临时文件的数据总量。计算所有临时文件，不论为什么创建临时文件，而且不管log_temp_files设置。                 |
| deadlocks      | bigint                   | 在该数据库中检索的死锁数。                                                                                                   |
| blk_read_time  | double precision         | 通过数据库后端读取数据文件块花费的时间，以毫秒计算。                                                                         |
| blk_write_time | double precision         | 通过数据库后端写入数据文件块花费的时间，以毫秒计算。                                                                         |
| stats_reset    | timestamp with time zone | 重置当前状态统计的时间。                                                                                                     |
