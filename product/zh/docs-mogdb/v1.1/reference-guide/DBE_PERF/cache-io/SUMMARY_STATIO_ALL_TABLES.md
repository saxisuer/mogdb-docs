---
title: SUMMARY_STATIO_ALL_TABLES
summary: SUMMARY_STATIO_ALL_TABLES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_TABLES

SUMMARY_STATIO_ALL_TABLES视图将包含MogDB内汇聚的数据库中每个表(包括TOAST表）的I/O的统计。

**表 1** SUMMARY_STATIO_ALL_TABLES字段

| **名称**        | **类型** | **描述**                                      |
| :-------------- | :------- | :-------------------------------------------- |
| schemaname      | name     | 该表模式名。                                  |
| relname         | name     | 表名。                                        |
| heap_blks_read  | numeric  | 从该表中读取的磁盘块数。                      |
| heap_blks_hit   | numeric  | 此表缓存命中数。                              |
| idx_blks_read   | numeric  | 从表中所有索引读取的磁盘块数。                |
| idx_blks_hit    | numeric  | 表中所有索引命中缓存数。                      |
| toast_blks_read | numeric  | 此表的TOAST表读取的磁盘块数（如果存在）。     |
| toast_blks_hit  | numeric  | 此表的TOAST表命中缓冲区数（如果存在）。       |
| tidx_blks_read  | numeric  | 此表的TOAST表索引读取的磁盘块数（如果存在）。 |
| tidx_blks_hit   | numeric  | 此表的TOAST表索引命中缓冲区数（如果存在）。   |
