---
title: SUMMARY_FILE_REDO_IOSTAT
summary: SUMMARY_FILE_REDO_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_FILE_REDO_IOSTAT

MogDB内汇聚所有的Redo(WAL)相关的统计信息。

**表 1** SUMMARY_FILE_REDO_IOSTAT字段

| **名称**  | **类型** | **描述**                                           |
| :-------- | :------- | :------------------------------------------------- |
| phywrts   | numeric  | 向wal buffer中写的次数。                           |
| phyblkwrt | numeric  | 向wal buffer中写的block的块数。                    |
| writetim  | numeric  | 向xlog文件中写操作的时间（单位: 微秒）。           |
| avgiotim  | bigint   | 平均写xlog的时间(writetim/phywrts)（单位: 微秒）。 |
| lstiotim  | bigint   | 最后一次写xlog的时间（单位: 微秒）。               |
| miniotim  | bigint   | 最小的写xlog时间（单位: 微秒）。                   |
| maxiowtm  | bigint   | 最大的写xlog时间（单位: 微秒）。                   |
