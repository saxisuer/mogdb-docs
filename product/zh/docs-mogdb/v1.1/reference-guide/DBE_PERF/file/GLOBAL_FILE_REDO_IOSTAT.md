---
title: GLOBAL_FILE_REDO_IOSTAT
summary: GLOBAL_FILE_REDO_IOSTAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_FILE_REDO_IOSTAT

得到MogDB内各节点的Redo(WAL)相关统计信息。

**表 1** GLOBALXC_FILE_REDO_IOSTAT字段

| **名称**  | **类型** | **描述**                                           |
| :-------- | :------- | :------------------------------------------------- |
| node_name | name     | 数据库进程名称。                                   |
| phywrts   | bigint   | 向wal buffer中写的次数。                           |
| phyblkwrt | bigint   | 向wal buffer中写的block的块数。                    |
| writetim  | bigint   | 向xlog文件中写操作的时间（单位: 微秒）。           |
| avgiotim  | bigint   | 平均写xlog的时间(writetim/phywrts)（单位: 微秒）。 |
| lstiotim  | bigint   | 最后一次写xlog的时间（单位: 微秒）。               |
| miniotim  | bigint   | 最小的写xlog时间（单位: 微秒）。                   |
| maxiowtm  | bigint   | 最大的写xlog时间（单位: 微秒）。                   |
