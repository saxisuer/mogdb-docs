---
title: 表 4
summary: 表 4
author: Guo Huan
date: 2021-04-19
---

**表 4** 事务锁等待事件列表<a id="表4"> </a>

| wait_event类型   | 类型描述              |
| :--------------- | :-------------------- |
| relation         | 对表加锁。            |
| extend           | 对表扩展空间时加锁。  |
| partition        | 对分区表加锁。        |
| partition_seq    | 对分区表的分区加锁。  |
| page             | 对表页面加锁。        |
| tuple            | 对页面上的tuple加锁。 |
| transactionid    | 对事务ID加锁。        |
| virtualxid       | 对虚拟事务ID加锁。    |
| object           | 加对象锁。            |
| cstore_freespace | 对列存空闲空间加锁。  |
| userlock         | 加用户锁。            |
| advisory         | 加advisory锁。        |
