---
title: 高可用管理
summary: 高可用管理
author: Liu Xu
date: 2021-03-04
---

# 高可用管理

## 简介

MogHA是云和恩墨基于MogDB同步及异步复制自研的一款企业级高可用产品。主要针对服务器宕机、实例宕机等多种情况，使数据库的故障持续时间从分钟级降到秒级，确保系统业务的持续运行，期间客户无感知。

MogHA高可用表示在一主一备或者一主多备情况下 ，当主机发生故障宕机，用户可以手动或自动实现主备切换，使备机升为主机接管主机业务，从而降低数据库的不可服务时间，避免影响业务。

**基本概念**

- 主备

    主备通指数据库数据级别主备，即主库实例与备库实例，并不共享数据文件，而是各自拥有独立的数据文件，主备之间通过数据操作日志进行数据同步的结构。

    主库允许读写操作，而备库禁止写操作，但可以读数据，备库通过即时回放操作日志，保证数据视图延迟不会超过特定区间（通常为最新的数据事务）。

- 物理复制

  数据库存在物理复制，物理复制指的是，复制日志为redo，为数据块变更记录，其变更保证主备之间数据文件一致，最大化保护数据。

- 逻辑复制

    逻辑复制为逻辑操作，主备之间数据相等，但数据文件不相同。

    相比较物理复制，逻辑复制更灵活。

- 数据库切换（以单套HA架构为例）

    HA主体架构如下图所示:

    ![cNh2QS.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/MogHA-management-2.png)

    该图是一个HA组，即一主一备的简化版HA架构。其中包括:

  - Agent: MogHA会为每个机器部署agent，用于维护HA相关操作。
  - vip: 指虚拟ip地址，通过虚拟网卡的方式挂载到主机，提供对外服务，当所在机器宕机，可以在其他机器重新挂载，避免应用修改数据库配置，降低数据库的故障持续时间。主要用于主备服务器之间的切换。
  - 仲裁: 目前出于简化配置考虑，设置为主备数据库所在业务子网的网关，作为网络隔离的判断依据之一。当备库需要判断主库是否可以ping通的情况时，会去连带ping该节点，如果仲裁不通，认为属于网络问题，不会触发主备切换。

- 部署模式

    MogHA支持两种部署模式，包括Lite模式和Full模式。

    **Lite模式**

  - 仅允许在主库和本机房同步备启动ha服务
  - 支持单次切换
  - 完成切换后需要人工参与事后处理
  - 设置新的同步备库（设置sync names变量）
  - 在新的同步备库启动HA服务
  - 不会对数据库配置做任何变更

    **Full模式**

  - 需要在所有实例都启动ha服务

    同步备宕机后，自动提升异步备为同步备

    允许人工不参与情况下的连续切换

    会变更数据库高可用相关配置（异步备提升同步备）

- 部署运维

    ![image-20210407104511881](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/MogHA-management-3.png)

    MogHA通过systemd管理服务。systemd通过supervosord管理web与heartbeat两个进程。

    Web进程主要用于组件内部通讯（通过来源访问控制，仅允许一套主备内相互访问）。Heartbeat心跳进程主要用于实际的检查，HA操作等。

<br/>

## 拓扑架构

MogHA高可用产品最多支持一主八备的部署方式，下面主要以一主六备部署架构为例进行说明。

一主两同步三异步一级联的两地三中心的数据库集群架构，保证在每个数据库中心都有至少两台数据库节点，同城双机房的数据实时同步预防单机房故障导致数据丢失或业务不可用，异地机房做数据容灾，实现数据多副本，集群架构如下图所示:

![image-20210407105301980](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/MogHA-management-4.png)

在主机房部署三台数据库节点，分别是主库、同步备库及异步备库，同步备库和异步备库上游节点都是主库，当主库有数据变更，需要同步备库数据落盘后才会执行事务commit操作，相比同步备库，异步备库允许数据有一定的延时落盘，当主库发生故障时，同步备库与主库数据是一致的，优先切换同步备库为新主库，数据无丢失。当原主库无法快速恢复时，需要修改新主库的同步配置参数synchronous_standby_names并执行reload操作,将异步备库切换成同步备库，保证整个集群中有两个同步备，且每个机房都不会出现单点。

HA工具监控集群中各个节点的状态，当主节点状态发生异常，可以快速的对主库进行切换，保证整个数据库集群的可用性，结合数据库连接（JDBC）的 IP列表配置，在不干预应用程序的情况下，可以自动判断集群哪个是主库，哪个是备库。

<br/>

## 部署与安装

**前提条件**

- 数据库已经部署完成

- 操作系统为x86红旗7.6版本

- python3已经安装完成

**操作步骤**

1. 检查关闭防火墙，实际环境可按端口配置。

2. 使用ntp或chronyd校对主库和备库的时间。

3. 配置用户的sudo权限。

    ```bash
    chmod +w /etc/sudoers
    which ifconfig
    /usr/sbin/ifconfig
    vi /etc/sudoers
    omm   ALL=(ALL)    NOPASSWD: /usr/sbin/ifconfig
    chmod -w /etc/sudoers
    ```

4. 准备Python运行环境。

    创建/home/omm/ha/venv/bin下的 python 和 python3软链接:

    ```bash
    ln -s /usr/bin/python3.7 /home/omm/ha/venv/bin/python
    ln -s /usr/bin/python3.7 /home/omm/ha/venv/bin/python3
    ```

5. 修改配置文件

    a. 修改数据目录下postgresql.conf配置文件，按要求修改监听地址。

    ```
    listen_addresses=’*’
    ```

    b. 在每个节点上，将replconninfo1和以下几行的端口改为26009，26008，26007，26009，26008，26007后，重启集群。

    c. 在pg_hba.conf里加上访问服务器的ip端

    例如: host     all   all   21.0.21.23/32      md5

    d. 将ha与venv放到/home/omm/ha下，目录结构如下:

    ```bash
    /home/omm/ha/ha/node.conf
                    env.sh
                    supervisord.conf
                    mogha.service
    /home/omm/ha/venv/bin/python
                          python3
    /usr/lib/systemd/system/mogha.service
    ```

    e. 修改/home/omm/ha/ha下的node.conf。

    ```bash
    [config]
    heartbeat_interval=3                                 #HA心跳间隔(s)
    primary_lost_timeout=10                              #主库丢失最大时间(s)
    primary_lonely_timeout=10                            #主库孤独检查最大时间(s)
    double_primary_timeout=10                            #双主检查最大时间(s)
    agent_port=8081                                      #ha的web端口
    db_port=26000                                         #数据库服务端口
    db_user=omm                                          #数据库的操作系统用户
    db_datadir=/data/dn1                                 #数据目录
    primary_info=/home/omm/ha/ha/primary_info.json     #主库的json数据地址，默认放HA目录下
    standby_info=/home/omm/ha/ha/standby_info.json     #备库的json数据地址，默认放HA目录下
    taskset=True
    [meta]                                          #元数据库
    ha_name=ms1
    host=192.168.2.1
    port=26000
    db=monitordb
    user=monitor
    password=monitor
    schema=public
    [host1]                                            #节点一信息,一般为主库
    ip=192.168.122.201
    heartbeat_ips=192.168.100.201
    [host2]                                            #节点二信息
    ip=192.168.122.202
    heartbeat_ips=192.168.100.202
    [host3]                                             #节点三信息
    ip=192.168.122.205
    heartbeat_ips=192.168.100.205
    [host4]                                             #节点四信息
    ip=192.168.122.206
    heartbeat_ips=192.168.100.206
    [zone1]                                            #主机房
    vip=192.168.122.211
    arping=192.168.122.1
    ping_list=192.168.122.1
    hosts=host1,host2
    [zone2]                                             #备机房
    vip=192.168.122.212
    arping=192.168.122.1
    ping_list=192.168.122.1
    hosts=host3
    cascades=host4                                      #级联库
    ```

    f. 修改/home/omm/ha/ha下的env.sh。

    ```bash
    export GAUSSHOME=/home/postgres/openGauss
    export PGDATA=$GAUSSHOME/data
    export LD_LIBRARY_PATH=$GAUSSHOME/lib
    ```

    g. 修改/home/omm/ha/ha下的supervisord.conf。

    ```bash
    [supervisord]
    logfile=/tmp/mogha_supervisord.log ;日志文件，默认是 $CWD/supervisord.log
    logfile_maxbytes=50MB        ;日志文件大小，超出会rotate，默认 50MB，如果设成0，表示不限制大小
    logfile_backups=10           ;日志文件保留备份数量默认10，设为0表示不备份
    loglevel=info                ;日志级别，默认info，其它: debug,warn,trace
    pidfile=/tmp/mogha_supervisord.pid ;pid 文件

    nodaemon=true               ;是否在前台启动，默认是false，即以 daemon 的方式启动
    minfds=1024                  ;可以打开的文件描述符的最小值，默认 1024
    minprocs=200                 ;可以打开的进程数的最小值，默认 200
    [program:web]
    command=/home/omm/ha/venv/bin/python  /home/omm/ha/ha/main.py --config /home/omm/ha/ha/node.conf --web
    autostart=true
    startsecs=10
    autorestart=true
    startretries=3
    user=omm
    redirect_stderr=true
    stdout_logfile_maxbytes=20MB
    stdout_logfile_backups = 20
    stdout_logfile=/home/omm/ha/ha/mogha_web.log
    environment=PYTHONUNBUFFERED=1,GAUSSHOME=/opt/gaussdb/app,PGDATA=/opt/gaussdb/data/db1,LD_LIBRARY_PATH=/opt/gaussdb/app/lib:/opt/mogdb/tools/lib:/opt/mogdb/tools/script/gspylib/clib
    directory=/home/omm/ha/ha/
    [program:heartbeat]
    command=/home/omm/ha/venv/bin/python  /home/omm/ha/ha/main.py --config /home/omm/ha/ha/node. conf --heartbeat
    autostart=true
    startsecs=10
    autorestart=true
    startretries=3
    user=omm
    redirect_stderr=true
    stdout_logfile_maxbytes=20MB
    stdout_logfile_backups = 20
    stdout_logfile=/home/omm/ha/ha/mogha_heartbeat.log
    environment=GAUSSHOME=/opt/gaussdb/app,PGDATA=/opt/gaussdb/data/db1,LD_LIBRARY_PATH=/opt/gaussdb/app/lib:/opt/mogdb/tools/lib:/opt/mogdb/tools/script/gspylib/clib
    directory=/home/omm/ha/ha
    ```

    h. 修改/home/omm/ha/ha下的mogha.service。

    ```bash
    [Unit]
    Description=The doufu python message queue server
    After=network.target remote-fs.target nss-lookup.target

    [Service]
    Environment=GAUSSHOME=/gauss/openGauss/app_101
    Environment=PGDATA=/gaussdata/openGauss/db1
    Environment=LD_LIBRARY_PATH=/gauss/openGauss/app_101/lib:/gauss/openGauss/om/lib:/gauss/openGauss/om/script/gspylib/clib:
    Type=simple
    User=omm
    WorkingDirectory=/home/omm/ha/ha
    ExecStart=/home/omm/ha/venv/bin/supervisord -c /home/omm/ha/ha/supervisord.conf
    KillSignal=SIGTERM
    TimeoutStopSec=5
    KillMode=process
    PrivateTmp=false
    [Install]
    WantedBy=multi-user.target
    ```

    i. 使用root权限把配置好的mogha.service拷贝到/usr/lib/systemd/system/下。

6. 启动/关闭MogHA。

    a. 需要在所有的节点上使用root用户执行如下操作:

    ```bash
    su - root
    systemctl [start|stop|restart] mogha
    ```

    b. 检查每个节点的log是否显示正常。

    ```bash
    tail -f /home/omm/ha/ha/mogha_web.log
    tail -f /home/omm/ha/ha/mogha_heartbeat.log
    ```

    c. 将ha设置为开机自启。

    ```bash
    su - root
    systemctl enable mogha
    ```

<br/>

## 卸载MogHA

**操作步骤**

1. 以操作系统用户omm登录每个数据库节点。

2. 删除节点上的venv和ha文件。

3. 切换到root用户。

4. 删除每个节点上的mogha.service文件。
