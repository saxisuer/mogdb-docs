---
title: 主备管理
summary: 主备管理
author: Guo Huan
date: 2021-03-11
---

# 主备管理

## 操作场景

MogDB在运行过程中，数据库管理员可能需要手工对数据库节点做主备切换。例如发现数据库节点主备failover后需要恢复原有的主备角色，或怀疑硬件故障需要手动进行主备切换。级联备机不能直接转换为主机，只能先通过switchover或者failover成为备机，然后再切换为主机。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 主备切换为维护操作，确保MogDB状态正常，所有业务结束后，再进行切换操作。
> - 在开启极致RTO时，不支持级联备机。级联备机因为极致RTO开启情况下，备机不支持连接无法同步数据。

<br/>

## 操作步骤

1. 以操作系统用户omm登录数据库任意节点，执行如下命令，查看主备情况。

    ```bash
    gs_om -t status --detail
    ```

2. 以操作系统用户omm登录准备切换为主节点的备节点，执行如下命令。

    ```bash
    gs_ctl switchover -D /home/omm/cluster/dn1/
    ```

    /home/omm/cluster/dn1/为备数据库节点的数据目录。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:** 对于同一数据库，上一次主备切换未完成，不能执行下一次切换。对于业务正在操作时，发起switchover，可能主机的线程无法停止导致switchover显示超时，实际后台仍然在运行，等主机线程停止后，switchover即可完成。比如在主机删除一个大的分区表时，可能无法响应switchover发起的信号。

3. switchover成功后，执行如下命令记录当前主备机器信息。

    ```bash
    gs_om -t refreshconf
    ```

<br/>

## 示例

将数据库节点备实例切换为主实例。

1.查询数据库状态。

```bash
gs_om -t status --detail
[   Cluster State   ]

cluster_state   : Normal
redistributing  : No
current_az      : AZ_ALL

[  Datanode State   ]

node               node_ip         instance                       state            | node               node_ip         instance                       state
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1  pekpopgsci00234 10.244.60.70    6001 /home/omm/cluster/dn1 P Primary Normal | 2  pekpopgsci00238 10.244.61.81    6002 /home/omm/cluster/dn1 S Standby Normal
```

2.登录备节点，进行主备切换。另外，switchover级联备机后，级联备机切换为备机，原备机将为级联备。

```bash
gs_ctl switchover -D /home/omm/cluster/dn1/
[2020-06-17 14:28:01.730][24438][][gs_ctl]: gs_ctl switchover ,datadir is -D "/home/omm/cluster/dn1"
[2020-06-17 14:28:01.730][24438][][gs_ctl]: switchover term (1)
[2020-06-17 14:28:01.768][24438][][gs_ctl]: waiting for server to switchover............
[2020-06-17 14:28:11.175][24438][][gs_ctl]: done
[2020-06-17 14:28:11.175][24438][][gs_ctl]: switchover completed (/home/omm/cluster/dn1)
```

3.保存数据库主备机器信息。

```bash
gs_om -t refreshconf
Generating dynamic configuration file for all nodes.
Successfully generated dynamic configuration file.
```

<br/>

## 错误排查

如果switchover过程中出现失败，请根据日志文件中的日志信息排查错误，参见日志参考。

<br/>

## 异常处理

异常判断标准如下:

- 业务压力下，主备实例切换时间长，这种情况不需要处理。

- 其他备机正在build的情况下，主机需要发送日志到备机后，才能降备，导致主备切换时间长。这种情况不需要处理，但应尽量避免build过程中进行主备切换。

- 切换过程中，因网络故障、磁盘满等原因造成主备实例连接断开，出现双主现象时，此时请参考如下步骤修复。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **警告:** 出现双主状态后，请按如下步骤恢复成正常的主备状态。否则可能会造成数据丢失。

1. 执行以下命令查询数据库当前的实例状态。

    ```bash
    gs_om -t status --detail
    ```

    若查询结果显示两个实例的状态都为Primary，这种状态为异常状态。

2. 确定降为备机的节点，在节点上执行如下命令关闭服务。

    ```bash
    gs_ctl stop -D /home/omm/cluster/dn1/
    ```

3. 执行以下命令，以standby模式启动备节点。

    ```bash
    gs_ctl start -D /home/omm/cluster/dn1/ -M standby
    ```

4. 保存数据库主备机器信息。

    ```bash
    gs_om -t refreshconf
    ```

5. 查看数据库状态，确认实例状态恢复。
