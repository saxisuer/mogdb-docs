---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 检查和清理日志

日志是检查系统运行及故障定位的关键手段。建议按月度例行查看操作系统日志及数据库的运行日志。同时，随着时间的推移，日志的增加会占用较多的磁盘空间。建议按月度清理数据库的运行日志。

<br/>

## 检查操作系统日志

建议按月检查操作系统日志，排除操作系统运行异常隐患。

执行如下命令查看操作系统日志文件。

```
vim /var/log/messages
```

关注其中近一个月出现的kernel、error、fatal等字样，根据系统报警信息进行处理。

<br/>

## 检查MogDB运行日志

数据库运行时，某些操作在执行过程中可能会出现错误，数据库依然能够运行。但是此时数据库中的数据可能已经发生不一致的情况。建议按月检查MogDB运行日志，及时发现隐患。

**前提条件**

- 收集日志的主机网络通畅且未宕机，数据库安装用户互信正常。
- 日志收集工具依赖操作系统工具如gstack，如果未安装该工具，则提示错误后，跳过该收集项。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 执行如下命令收集数据库日志。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59"
    ```

    20160616 01:01为日志的开始时间，20160616 23:59为日志的结束时间。

3. 根据2的界面输出提示，进入相应的日志收集目录，解压收集的日志，并检查数据库日志。

    以下以日志收集路径"/opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158.tar.gz"为例进行操作。

    ```
    tar -xvzf /opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158.tar.gz
    cd /opt/mogdb/tmp/gaussdba_mppdb/collector_20160726_105158
    ```

**示例**

- 以-begin-time与-end-time为参数执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59"
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected  files
    All results are stored in /tmp/gaussdba_mppdb/collector_20160616_175615.tar.gz.
    ```

- 以-begin-time，-end-time与-h为参数执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -h plat2
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected  files
    All results are stored in /tmp/gaussdba_mppdb/collector_20160616_190225.tar.gz.
    ```

- 以-begin-time，-end-time与-f为参数执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -f /opt/software/mogdb/output
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected  files
    All results are stored in /opt/software/mogdb/output/collector_20160616_190511.tar.gz.
    ```

- 以-begin-time，-end-time与-keyword为参数执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" --keyword="os"
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected files.
    All results are stored in /tmp/gaussdba_mppdb/collector_20160616_190836.tar.gz.
    ```

- 以-begin-time，-end-time与-o为参数执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -o /opt/software/mogdb/output
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected files.
    All results are stored in /opt/software/mogdb/output/collector_20160726_113711.tar.gz.
    ```

- 以-begin-time，-end-time与-l为参数（文件名必须以.log为后缀）执行gs_collector命令。

    ```bash
    gs_collector --begin-time="20160616 01:01" --end-time="20160616 23:59" -l /opt/software/mogdb/logfile.log
    ```

    当显示类似如下信息表示日志已经归档。

    ```
    Successfully collected files.
    All results are stored in /opt/software/mogdb/output/collector_20160726_113711.tar.gz.
  ```

<br/>

## 清理运行日志

数据库运行过程中会产生大量运行日志，占用大量的磁盘空间，建议清理过期日志文件，只保留一个月的日志。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 清理日志。

    a. 将超过1个月的日志备份到其他磁盘。

    b. 进入日志存放目录。

    ```
    cd $GAUSSLOG
    ```

    c. 进入相应的子目录，使用如下方式删除1个月之前产生的日志。

    ```
    rm 日志名称
    ```

    日志文件的命名格式为"mogdb-年-月-日_HHMMSS"。
