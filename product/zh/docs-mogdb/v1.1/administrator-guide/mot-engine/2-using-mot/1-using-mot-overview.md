---
title: MOT使用概述
summary: MOT使用概述
author: Zhang Cuiping
date: 2021-03-04
---

# MOT使用概述

MOT作为MogDB的一部分自动部署。有关如何计算和规划所需的内存和存储资源以维持工作负载的说明，请参阅**MOT准备**。参考**MOT部署**了解MOT中所有的配置，以及服务器优化的非必须选项。

使用MOT的方法非常简单。MOT命令的语法与基于磁盘的表的语法相同，并支持大多数标准，如PostgreSQL SQL、DDL和DML命令和功能，如存储过程。只有MOT中的创建和删除表语句与MogDB中基于磁盘的表的语句不同。您可以参考**MOT使用**了解这两个简单命令的说明，如何将基于磁盘的表转换为MOT，如何使用查询原生编译和PREPARE语句获得更高的性能，以及了解外部工具支持和MOT引擎的限制。

**MOT管理**介绍了如何进行数据库维护，以及监控和分析日志和错误报告。最后，**MOT样例TPC-C基准**介绍了如何执行标准TPC-C基准测试。

- 阅读以下内容了解如何使用MOT：

  ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/using-mot-overview-1.png)
