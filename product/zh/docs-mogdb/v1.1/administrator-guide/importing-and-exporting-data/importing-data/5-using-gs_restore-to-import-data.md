---
title: 使用gs_restore命令导入数据
summary: 使用gs_restore命令导入数据
author: Guo Huan
date: 2021-03-04
---

# 使用gs_restore命令导入数据

<br/>

## 操作场景

gs_restore是MogDB数据库提供的与gs_dump配套的导入工具。通过该工具，可将gs_dump导出的文件导入至数据库。gs_restore支持导入的文件格式包含自定义归档格式、目录归档格式和tar归档格式。

gs_restore具备如下两种功能。

- 导入至数据库

  如果指定了数据库，则数据将被导入到指定的数据库中。其中，并行导入必须指定连接数据库的密码。

- 导入至脚本文件

  如果未指定导入数据库，则创建包含重建数据库所需的SQL语句脚本，并将其写入至文件或者标准输出。该脚本文件等效于gs_dump导出的纯文本格式文件。

gs_restore工具在导入时，允许用户选择需要导入的内容，并支持在数据导入前对等待导入的内容进行排序。

<br/>

## 操作步骤

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> gs_restore默认是以追加的方式进行数据导入。为避免多次导入造成数据异常，在进行导入时，建议选择使用"-c" 和"-e"参数。"-c"表示在重新创建数据库对象前，清理（删除）已存在于将要还原的数据库中的数据库对象；"-e"表示当发送SQL语句到数据库时如果出现错误请退出，默认状态下会继续，且在导入后会显示一系列错误信息。

1. 以操作系统用户omm登录数据库主节点。

2. 使用gs_restore命令，从mogdb整个数据库内容的导出文件中，将数据库的所有对象的定义导入到backupdb。

    ```
    gs_restore -W Bigdata@123 -U jack /home/omm/backup/MPPDB_backup.tar -p 8000 -d backupdb -s -e -c
    ```

    **表 1** 常用参数说明

    | 参数 | 参数说明| 举例           |
    | :----- | :--------------------| :------------- |
    | -U   | 连接数据库的用户名。        | -U jack        |
    | -W   | 指定用户连接的密码。<br/>- 如果主机的认证策略是trust，则不会对数据库管理员进行密码验证，即无需输入-W选项；<br/>- 如果没有-W选项，并且不是数据库管理员，会提示用户输入密码。 | -W Bigdata@123 |
    | -d   | 连接数据库dbname，并直接将数据导入到该数据库中。       | -d backupdb    |
    | -p   | 指定服务器所监听的TCP端口或本地Unix域套接字后缀，以确保连接。                | -p 8000        |
    | -e   | 当发送SQL语句到数据库时如果出现错误，则退出。默认状态下会忽略错误任务并继续执行导入，且在导入后会显示一系列错误信息。 | -              |
    | -c   | 在重新创建数据库对象前，清理（删除）已存在于将要导入的数据库中的数据库对象。  | -              |
    | -s   | 只导入模式定义，不导入数据。当前的序列值也不会被导入。 | -              |

    其他参数说明请参见《工具参考》中"服务端工具 > gs_restore"章节。

<br/>

## 示例

示例一: 执行gs_restore，导入指定MPPDB_backup.dmp文件（自定义归档格式）中mogdb数据库的数据和对象定义。

```bash
gs_restore -W Bigdata@123 backup/MPPDB_backup.dmp -p 8000 -d backupdb
gs_restore[2017-07-21 19:16:26]: restore operation successful
gs_restore: total time: 13053  ms
```

示例二: 执行gs_restore，导入指定MPPDB_backup.tar文件（tar归档格式）中mogdb数据库的数据和对象定义。

```bash
gs_restore backup/MPPDB_backup.tar -p 8000 -d backupdb
gs_restore[2017-07-21 19:21:32]: restore operation successful
gs_restore[2017-07-21 19:21:32]: total time: 21203  ms
```

示例三: 执行gs_restore，导入指定MPPDB_backup目录文件（目录归档格式）中mogdb数据库的数据和对象定义。

```bash
gs_restore backup/MPPDB_backup -p 8000 -d backupdb
gs_restore[2017-07-21 19:26:46]: restore operation successful
gs_restore[2017-07-21 19:26:46]: total time: 21003  ms
```

示例四: 执行gs_restore，将mogdb数据库的所有对象的定义导入至backupdb数据库。导入前，mogdb存在完整的定义和数据，导入后，backupdb数据库只存在所有对象定义，表没有数据。

```bash
gs_restore -W Bigdata@123 /home/omm/backup/MPPDB_backup.tar -p 8000 -d backupdb -s -e -c
gs_restore[2017-07-21 19:46:27]: restore operation successful
gs_restore[2017-07-21 19:46:27]: total time: 32993  ms
```

示例五: 执行gs_restore，导入MPPDB_backup.dmp文件中PUBLIC模式的所有定义和数据。在导入时会先删除已经存在的对象，如果原对象存在跨模式的依赖则需手工强制干预。

```bash
gs_restore backup/MPPDB_backup.dmp -p 8000 -d backupdb -e -c -n PUBLIC
gs_restore: [archiver (db)] Error while PROCESSING TOC:
gs_restore: [archiver (db)] Error from TOC entry 313; 1259 337399 TABLE table1 gaussdba
gs_restore: [archiver (db)] could not execute query: ERROR:  cannot drop table table1 because other objects depend on it
DETAIL:  view t1.v1 depends on table table1
HINT:  Use DROP ... CASCADE to drop the dependent objects too.
Command was: DROP TABLE public.table1;
```

手工删除依赖，导入完成后再重新创建。

```bash
gs_restore backup/MPPDB_backup.dmp -p 8000 -d backupdb -e -c -n PUBLIC
gs_restore[2017-07-21 19:52:26]: restore operation successful
gs_restore[2017-07-21 19:52:26]: total time: 2203  ms
```

示例六: 执行gs_restore，导入MPPDB_backup.dmp文件中hr模式下表hr.staffs的定义。在导入之前，hr.staffs表不存在。

```bash
gs_restore backup/MPPDB_backup.dmp -p 8000 -d backupdb -e -c -s -n hr -t hr.staffs
gs_restore[2017-07-21 19:56:29]: restore operation successful
gs_restore[2017-07-21 19:56:29]: total time: 21000  ms
```

示例七: 执行gs_restore，导入MPPDB_backup.dmp文件中hr模式下表hr.staffs的数据。在导入之前，hr.staffs表不存在数据。

```bash
gs_restore backup/MPPDB_backup.dmp -p 8000 -d backupdb -e -a -n hr -t hr.staffs
gs_restore[2017-07-21 20:12:32]: restore operation successful
gs_restore[2017-07-21 20:12:32]: total time: 20203  ms
```

示例八: 执行gs_restore，导入指定表hr.staffs的定义。在导入之前，hr.staffs表的数据是存在的。

```sql
human_resource=# select * from hr.staffs;
 staff_id | first_name  |  last_name  |  email   |    phone_number    |      hire_date      | employment_id |  salary  | commission_pct | manager_id | section_id
----------+-------------+-------------+----------+--------------------+---------------------+---------------+----------+----------------+------------+------------
      200 | Jennifer    | Whalen      | JWHALEN  | 515.123.4444       | 1987-09-17 00:00:00 | AD_ASST       |  4400.00 |                |        101 |         10
      201 | Michael     | Hartstein   | MHARTSTE | 515.123.5555       | 1996-02-17 00:00:00 | MK_MAN        | 13000.00 |                |        100 |         20

gsql -d human_resource -p 8000

gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-12-08 02:59:43 commit 2143 last mr 131
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

human_resource=# drop table hr.staffs CASCADE;
NOTICE:  drop cascades to view hr.staff_details_view
DROP TABLE

gs_restore -W Bigdata@123 /home/omm/backup/MPPDB_backup.tar -p 8000 -d human_resource -n hr -t staffs -s -e
restore operation successful
total time: 904  ms

human_resource=# select * from hr.staffs;
 staff_id | first_name | last_name | email | phone_number | hire_date | employment_id | salary | commission_pct | manager_id | section_id
----------+------------+-----------+-------+--------------+-----------+---------------+--------+----------------+------------+------------
(0 rows)
```

示例九: 执行gs_restore，导入staffs和areas两个指定表的定义和数据。在导入之前，staffs和areas表不存在。

```sql
human_resource=# \d
                                 List of relations
 Schema |        Name        | Type  |  Owner   |             Storage
--------+--------------------+-------+----------+----------------------------------
 hr     | employment_history | table | omm | {orientation=row,compression=no}
 hr     | employments        | table | omm | {orientation=row,compression=no}
 hr     | places             | table | omm | {orientation=row,compression=no}
 hr     | sections           | table | omm | {orientation=row,compression=no}
 hr     | states             | table | omm | {orientation=row,compression=no}
(5 rows)

gs_restore -W Bigdata@123 /home/mogdb/backup/MPPDB_backup.tar -p 8000 -d human_resource -n hr -t staffs -n hr -t areas
restore operation successful
total time: 724  ms

human_resource=# \d
                                 List of relations
 Schema |        Name        | Type  |  Owner   |             Storage
--------+--------------------+-------+----------+----------------------------------
 hr     | areas              | table | omm | {orientation=row,compression=no}
 hr     | employment_history | table | omm | {orientation=row,compression=no}
 hr     | employments        | table | omm | {orientation=row,compression=no}
 hr     | places             | table | omm | {orientation=row,compression=no}
 hr     | sections           | table | omm | {orientation=row,compression=no}
 hr     | staffs             | table | omm | {orientation=row,compression=no}
 hr     | states             | table | omm | {orientation=row,compression=no}
(7 rows)

human_resource=# select * from hr.areas;
 area_id |       area_name
---------+------------------------
       4 | Middle East and Africa
       1 | Europe
       2 | Americas
       3 | Asia
(4 rows)
```

示例十: 执行gs_restore，导入hr的模式，包含模式下的所有对象定义和数据。

```bash
gs_restore -W Bigdata@123  /home/omm/backup/MPPDB_backup1.sql -p 8000 -d backupdb -n hr -e -c
restore operation successful
total time: 702  ms
```

示例十一: 执行gs_restore，同时导入hr和hr1两个模式，仅导入模式下的所有对象定义。

```bash
gs_restore -W Bigdata@123 /home/omm/backup/MPPDB_backup2.dmp -p 8000 -d backupdb -n hr -n hr1 -s
restore operation successful
total time: 665  ms
```

示例十二: 执行gs_restore，将human_resource数据库导出文件进行解密并导入至backupdb数据库中。

```sql
mogdb=# create database backupdb;
CREATE DATABASE

gs_restore /home/omm/backup/MPPDB_backup.tar -p 8000 -d backupdb --with-key=1234567812345678
restore operation successful
total time: 23472  ms

gsql -d backupdb -p 8000 -r

gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-12-08 02:59:43 commit 2143 last mr 131
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

backupdb=# select * from hr.areas;
 area_id |       area_name
---------+------------------------
       4 | Middle East and Africa
       1 | Europe
       2 | Americas
       3 | Asia
(4 rows)
```

示例十三: 用户user1不具备将导出文件中数据导入至数据库backupdb的权限，而角色role1具备该权限，要实现将文件数据导入数据库backupdb，可以在导出命令中设置-role角色为role1，使用role1的权限，完成导出目的。

```sql
human_resource=# CREATE USER user1 IDENTIFIED BY "1234@abc";
CREATE ROLE

gs_restore -U user1 -W 1234@abc /home/omm/backup/MPPDB_backup.tar -p 8000 -d backupdb --role role1 --rolepassword abc@1234
restore operation successful
total time: 554  ms

gsql -d backupdb -p 8000 -r

gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2020-12-08 02:59:43 commit 2143 last mr 131
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

backupdb=# select * from hr.areas;
 area_id |       area_name
---------+------------------------
       4 | Middle East and Africa
       1 | Europe
       2 | Americas
       3 | Asia
(4 rows)
```
