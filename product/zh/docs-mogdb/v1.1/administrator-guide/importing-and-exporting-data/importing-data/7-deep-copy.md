---
title: 深层复制
summary: 深层复制
author: Guo Huan
date: 2021-03-04
---

# 深层复制

数据导入后，如果需要修改表的分区键、或者将行存表改列存、添加PCK（Partial Cluster Key）约束等场景下，可以使用深层复制的方式对表进行调整。深层复制是指重新创建表，然后使用批量插入填充表的过程。

Mog提供了三种深层复制的方式供用户选择。

<br/>

## 使用CREATE TABLE执行深层复制

该方法使用CREATE TABLE语句创建原始表的副本，将原始表的数据填充至副本并重命名副本，完成原始表的复制。

在创建新表时，可以指定表以及列属性，比如主键。

**操作步骤**

执行如下步骤对表customer_t进行深层复制。

1. 使用CREATE TABLE语句创建表customer_t的副本customer_t_copy。

    ```sql
    CREATE TABLE customer_t_copy
    ( c_customer_sk             integer,
      c_customer_id             char(5),
      c_first_name              char(6),
      c_last_name               char(8)
    ) ;
    ```

2. 使用INSERT INTO…SELECT语句向副本填充原始表中的数据。

    ```sql
    INSERT INTO customer_t_copy (SELECT * FROM customer_t);
    ```

3. 删除原始表。

    ```sql
    DROP TABLE customer_t;
    ```

4. 使用ALTER TABLE语句将副本重命名为原始表名称。

    ```sql
    ALTER TABLE customer_t_copy RENAME TO customer_t;
    ```

<br/>

## 使用CREATE TABLE LIKE执行深层复制

该方法使用CREATE TABLE LIKE语句创建原始表的副本，将原始表的数据填充至副本并重命名副本，完成原始表的复制。该方法不继承父表的主键属性，您可以使用ALTER TABLE语句来添加它们。

**操作步骤**

1. 使用CREATE TABLE LIKE语句创建表customer_t的副本customer_t_copy。

    ```sql
    CREATE TABLE customer_t_copy (LIKE customer_t);
    ```

2. 使用INSERT INTO…SELECT语句向副本填充原始表中的数据。

    ```sql
    INSERT INTO customer_t_copy (SELECT * FROM customer_t);
    ```

3. 删除原始表。

    ```sql
    DROP TABLE customer_t;
    ```

4. 使用ALTER TABLE语句将副本重命名为原始表名称。

    ```sql
    ALTER TABLE customer_t_copy RENAME TO customer_t;
    ```

<br/>

## 通过创建临时表并截断原始表来执行深层复制

该方法使用CREATE TABLE AS语句创建原始表的临时表，然后截断原始表并从临时表填充它完成原始表的深层复制。

在新建表需要保留父表的主键属性，或如果父表具有依赖项的情况下，建议使用此方法。

**操作步骤**

1. 使用CREATE TABLE AS语句创建表customer_t的临时表副本customer_t_temp。

    ```sql
    CREATE TEMP TABLE customer_t_temp AS SELECT * FROM customer_t;
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    > 与使用永久表相比，使用临时表可以提高性能，但存在丢失数据的风险。临时表只在当前会话可见，本会话结束后将自动删除。如果数据丢失是不可接受的，请使用永久表。

2. 截断当前表customer_t。

    ```sql
    TRUNCATE customer_t;
    ```

3. 使用INSERT INTO…SELECT语句从副本中向原始表中填充数据。

    ```sql
    INSERT INTO customer_t (SELECT * FROM customer_t_temp);
    ```

4. 删除临时表副本customer_t_temp。

    ```sql
    DROP TABLE customer_t_temp;
    ```
