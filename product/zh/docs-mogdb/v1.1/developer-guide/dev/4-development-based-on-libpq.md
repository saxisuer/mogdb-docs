---
title: 基于libpq开发
summary: 基于libpq开发
author: Guo Huan
date: 2021-04-27
---

# 基于libpq开发

MogDB未对此接口在应用程序开发场景下的使用做验证。因此对使用此接口做应用程序开发存在的风险未知，故不推荐用户使用此套接口做应用程序开发。推荐用户使用ODBC或JDBC接口来替代。
