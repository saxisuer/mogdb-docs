---
title: 概述
summary: 概述
author: Guo Huan
date: 2021-04-26
---

# 概述

ODBC（Open Database Connectivity，开放数据库互连）是由Microsoft公司基于X/OPEN CLI提出的用于访问数据库的应用程序编程接口。应用程序通过ODBC提供的API与数据库进行交互，增强了应用程序的可移植性、扩展性和可维护性。

ODBC的系统结构参见[图1](#ODBC)。

**图 1**ODBC系统机构<a id="ODBC"> </a>

![ODBC系统机构](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-based-on-odbc-overview-1.png)

MogDB目前在以下环境中提供对ODBC3.5的支持。

**表 1** ODBC支持平台

| 操作系统                                           | 平台     |
| :------------------------------------------------- | :------- |
| CentOS 6.4/6.5/6.6/6.7/6.8/6.9/7.0/7.1/7.2/7.3/7.4 | x86_64位 |
| CentOS 7.6                                         | ARM64位  |
| EulerOS 2.0 SP2/SP3                                | x86_64位 |
| EulerOS 2.0 SP8                                    | ARM64位  |

UNIX/Linux系统下的驱动程序管理器主要有unixODBC和iODBC，在这选择驱动管理器unixODBC-2.3.0作为连接数据库的组件。

Windows系统自带ODBC驱动程序管理器，在控制面板->管理工具中可以找到数据源（ODBC）选项。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 当前数据库ODBC驱动基于开源版本，对于tinyint、smalldatetime、nvarchar2类型，在获取数据类型的时候，可能会出现不兼容。
