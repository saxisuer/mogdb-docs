---
title: 修改操作系统配置
summary: 修改操作系统配置
author: Zhang Cuiping
date: 2021-04-2
---

# 修改操作系统配置

## 关闭防火墙，selinux

```
systemctl disable firewalld.service
systemctl stop firewalld.service
setenforce=0
sed -i '/^SELINUX=/c'SELINUX=disabled /etc/selinux/config
```

## 设置字符集参数

将各数据库节点的字符集设置为相同的字符集，可以在/etc/profile文件中添加"export LANG=XXX"（XXX为Unicode编码）。

说明：可暂时不设置

## 设置时区和时间

将各数据库节点的时区设置为相同时区，可以将/usr/share/zoneinfo/目录下的时区文件拷贝为/etc/localtime文件。

cp /usr/share/zoneinfo/$地区/$时区 /etc/localtime使用date -s命令将各主机的时间设置为统一时间，举例如下。

```
date -s Mon May 11 16:42:11 CST 2020
```

## 关闭swap交换内存

在各数据库节点上，使用swapoff -a命令将交换内存关闭。

```
swapoff -a
```

说明：内存较大时关闭交换内存（建议32G以上）

## 设置网卡MTU值

使用如下命令将各数据库节点的网卡MTU值设置为相同大小。对于X86，MTU值推荐1500；对于ARM，MTU值推荐8192。

```
ifconfig 网卡编号 mtu 值
```
