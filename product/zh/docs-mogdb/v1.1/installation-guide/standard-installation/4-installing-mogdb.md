---
title: 安装MogDB
summary: 安装MogDB
author: Zhang Cuiping
date: 2021-04-2
---

# 安装MogDB

## 创建XML配置文件

安装MogDB前需要创建XML文件。XML文件包含部署MogDB的服务器信息、安装路径、IP地址以及端口号等。用于告知MogDB如何部署。用户需根据不同场配置对应的XML文件。

<br/>

**参数说明**

<table>  <tr>  <td>  <b>实例类型</b></td>   <td> <b>参数</b> </td>   <td> <b>说明</b></td>   </tr>   <tr>  <td rowspan="10">  整体信息</td>    </tr>   <tr>  <td>  clusterName</td>   <td> MogDB名称。</td>   </tr>   <tr>  <td>  nodeNames</td>   <td> MogDB中主机名称。</td>   </tr>   <tr>  <td>  backIp1s</td>   <td> 主机在后端存储网络中的IP地址（内网IP）。所有MogDB主机使用后端存储网络通讯。</td>   </tr>   <tr>  <td>  gaussdbAppPath</td>   <td> MogDB程序安装目录。此目录应满足如下要求：<br />- 磁盘空间&gt;1GB<br />- 与数据库所需其它路径相互独立，没有包含关系。</td>   </tr>   <tr>  <td>  gaussdbLogPath</td>   <td> MogDB运行日志和操作日志存储目录。此目录应满足如下要求：<br />- 磁盘空间建议根据主机上的数据库节点数规划。数据库节点预留1GB空间的基础上，再适当预留冗余空间。<br />- 与MogDB所需其它路径相互独立，没有包含关系。<br /> 此路径可选。不指定的情况下，MogDB安装时会默认指定“$GAUSSLOG/安装用户名”作为日志目录。 </td>   </tr>   <tr>  <td>  tmpdbPath</td>   <td> 数据库临时文件存放目录。<br /> 若不配置tmpdbPath，默认存放在/opt/mogdb/tools/perfadm_db目录下。 </td>   </tr>   <tr>  <td>  gaussdbToolPath</td>   <td> MogDB系统工具目录，主要用于存放互信工具等。此目录应满足如下要求：<br />- 磁盘空间&gt;100MB <br />- 固定目录，与数据库所需其它目录相互独立，没有包含关系。<br /> 此目录可选。不指定的情况下，MogDB安装时会默认指定“/opt/mogdb/tools”作为数据库系统工具目录。 </td>   </tr>   <tr>  <td>  corePath</td>   <td> MogDB core文件的指定目录。</td>   </tr>   <tr>  <td>  clusterType</td>   <td> MogDB类型，MogDB拓扑类型；可选字段。<br /> “single-inst”表示单机一主多备部署形态。 </td>   </tr>   </table>

## 示例

### 单节点配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1" />
        <PARAM name="backIp1s" value="192.168.0.11"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>

<!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

### 一主一备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1,node2" />
        <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1"/>
        </DEVICE>

        <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000002">
            <PARAM name="name" value="node2"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.12"/>
            <PARAM name="sshIp1" value="192.168.0.12"/>
    </DEVICE>
    </DEVICELIST>
</ROOT>
```

说明：替换主机名和IP，其他参数默认即可。

### 一主二备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1,node2,node3" />
        <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12, 192.168.0.13"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1, node3,/mogdb/data/db1"/>
        </DEVICE>

        <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000002">
            <PARAM name="name" value="node2"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.12"/>
            <PARAM name="sshIp1" value="192.168.0.12"/>
        </DEVICE>
        <!--node3上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000002">
            <PARAM name="name" value="node3"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.13"/>
            <PARAM name="sshIp1" value="192.168.0.13"/>
    </DEVICE>
    </DEVICELIST>
</ROOT>
```

说明：替换主机名和IP，其他参数默认即可。/mogdb/data/db1为数据存储路径，可根据需要替换

## 环境初始化

**操作步骤**

1. 以root用户登录待安装MogDB的任意主机，并按规划创建存放安装包的目录。

   ```
   mkdir -p /opt/software/mogdb
   chmod 755 -R /opt/software
   ```

2. 将安装包“MogDB-1.1.0-CentOS-all-x86_64.tar.gz”和配置文件“clusterconfig.xml”都上传至上一步所创建的目录中。

3. 在安装包所在的目录下，解压安装包。

   ```
   cd /opt/software/mogdb
   tar -xf  MogDB-1.1.0-CentOS-all-x86_64.tar.gz
   tar -xf  MogDB-1.1.0-CentOS-64bit-om.tar.gz
   tar -xf  MogDB-1.1.0-CentOS-64bit.tar.bz2
   ```

4. 进入到工具脚本存放目录下。

   ```
   cd /opt/software/mogdb/script
   ```

5. 为确保openssl版本正确，执行预安装前请加载安装包中lib库。执行命令如下，其中{packagePath}为用户安装包放置的路径，本示例中为/opt/software/mogdb。

   ```
   export LD_LIBRARY_PATH={packagePath}/script/gspylib/clib:$LD_LIBRARY_PATH
   ```

6. 为确保成功安装，检查 hostname 与 /etc/hostname 是否一致。预安装过程中，会对hostname进行检查。

7. openEuler 需要注释 #sysctl -w vm.min_free_kbytes=112640 &&gt; /dev/null  /etc/profile.d/performance.sh

8. CentOS系统需要安装python3

   ```
   wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
   mkdir -p /usr/local/python3
   tar -zxvf Python-3.6.5.tgz
   cd Python-3.6.5
   ./configure --prefix=/usr/local/python3 --enable-shared CFLAGS=-fPIC && make && make install
   ln -s /usr/local/python3/bin/python3 /usr/bin/python3
   ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
   find / -name libpython3.6m.so.1.0
   cp libpython3.6m.so.1.0 /usr/lib64
   ```

## 初始化脚本

```
/opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml

Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Distributing package.
Begin to distribute package to tool path.
Successfully distribute package to tool path.
Begin to distribute package to package path.
Successfully distribute package to package path.
Successfully distributed package.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Setting SCTP service.
Successfully set SCTP service.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting Cgroup.
Successfully set Cgroup.
Set ARM Optimization.
Successfully set ARM Optimization.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```

说明：运行过程中需要输入相关密码，需保证复杂度要求。（大小写，字符，数字）Enmo@123

## 执行安装

```
cd /opt/software/mogdb/script
chmod -R 755 /opt/software/mogdb/script
chown -R omm:dbgrp /opt/software/mogdb/script
su - omm
/opt/software/mogdb/script/gs_install -X /opt/software/mogdb/clusterconfig.xml

Parsing the configuration file.
Check preinstall on every node.
Successfully checked preinstall on every node.
Creating the backup directory.
Successfully created the backup directory.
begin deploy..
Installing the cluster.
begin prepare Install Cluster..
Checking the installation environment on all nodes.
begin install Cluster..
Installing applications on all nodes.
Successfully installed APP.
begin init Instance..
encrypt ciper and rand files for database.
Please enter password for database:
Please repeat for database:
begin to create CA cert files
The sslcert will be generated in /opt/mogdb/cluster/app/share/sslcert/om
Cluster installation is completed.
Configuring.
Deleting instances from all nodes.
Successfully deleted instances from all nodes.
Checking node configuration on all nodes.
Initializing instances on all nodes.
Updating instance configuration on all nodes.
Check consistence of memCheck and coresCheck on DN nodes.
Successful check consistence of memCheck and coresCheck on all nodes.
Configuring pg_hba on all nodes.
Configuration is completed.
Successfully started cluster.
Successfully installed application.
```

说明：在执行过程中，用户需根据提示输入数据库的密码，密码具有一定的复杂度，为保证用户正常使用该数据库，请记住输入的数据库密码。

## 修改初始密码

```
[omm@ecs-saving-0006 ~]$ gsql -d postgres -p26000 -r
gsql ((MogDB 1.1.0 build 5be05d82) compiled at 2021-03-09 15:31:32 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.
postgres=# \d
ERROR:  Please use "ALTER ROLE user_name IDENTIFIED BY 'password' REPLACE 'old password';" to modify the initial password of user omm before operation!
postgres=# ALTER ROLE omm IDENTIFIED BY 'Enmo@123' REPLACE 'old password ';
ALTER ROLE
postgres=# \copyright
MogDB Kernel Database Management System
Copyright (c) Yunhe Enmo (Beijing) Information Technology Co., Ltd. Copyright © 2009-2020 , All rights reserved.
```

## 更改同步节点

默认备库为异步，如果想配置同步需在主库做如下配置。

```
[omm@ecs-saving-0006 ~]$ sed -i "/synchronous_standby_names/synchronous_standby_names = 'dn_6002'" /mogdb/data/db1/postgresql.conf
[omm@ecs-saving-0006 ~]$ gs_om -t restart
```

dn_6002值比较特殊，他的值为固定值，主库为dn_6001,备库1为dn_6002,备库2为dn_6003,依次顺延。

'dn_6002,dn_6003'  表示备库1，2为同步节点。

替换相应的数据存储路径。
