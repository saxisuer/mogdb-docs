---
title: 容器化安装
summary: 容器化安装
author: Guo Huan
date: 2021-06-01
---

# 容器化安装

本文档主要介绍MogDB容器版单实例及主备安装过程。MogDB容器版本不支持MogHA工具、OM工具，目前最多支持一主一备。

<br/>

## 单实例安装

### 准备工作

MogDB支持的架构和操作系统版本如下:

- x86-64 CentOS 7.6
- ARM64 openEuler 20.03 LTS

在安装容器版MogDB之前，您需要先安装Docker。

Docker是一个开源的应用容器引擎，基于Go语言并遵从Apache2.0协议开源。Docker可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的Linux机器上，也可以实现虚拟化。

请前往[Docker](https://docs.docker.com/)网站根据您的操作系统选择对应的方法完成安装。

### 安装步骤

1. 启动Docker服务。

2. 根据您的系统架构输入以下命令之一获取最新版MogDB镜像文件:

   x86-64架构的机器：

   ```
   docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   ARM64架构的机器：

   ```
   mydocker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_arm
   ```

   以x86-64架构的机器为例：

   ![c3XvSP.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-12.png)

3. 输入以下命令为 MogDB 创建运行目录，下文以"mymogdb"为例：

   ```
   mkdir /mymogdb
   ```

4. 继续输入以下命令创建一个新的容器，将容器命名为"mymogdb"，以启动 MogDB 实例：

   ```
   docker run --name mymogdb  -d -e GS_PASSWORD=Secretpassword@123   -v /mymogdb:/var/lib/opengauss  -p 15432:5432  swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
   ```

   > 注意:MogDB 的默认监听启动在容器内的 5432 端口上，如果想要从容器外部访问数据库，则需要在 *docker run* 的时候指定 *-p* 参数。比如以上命令将允许使用 15432 端口访问容器数据库。容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在 *docker run* 的时候指定 *-v* 参数来实现。比如以上命令将会指定将 MogDB 的所有数据文件存储在宿主机的/mymogdb 下。此外，如果使用 podman，会有目标路径检查，因此需要预先创建目标路径（步骤 4）。

5. 进入容器终端：

   ```
   docker exec -it mymogdb /bin/bash
   ```

   至此，MogDB 容器版单实例安装完成。

### 使用 MogDB

安装完成后，输入以下命令切换到 omm 用户，即可通过外部 gsql 进行数据库访问以正常体验 MogDB:

```
su - omm
gsql -d postgres
```

![c3jMTJ.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-13.png)

### 环境变量

为了更灵活地使用 MogDB 镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

**GS_PASSWORD**

使用 MogDB 镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了 MogDB 数据库的超级用户 omm 以及测试用户 mogdb 的密码。MogDB 安装时默认会创建 omm 超级用户，该用户名暂时无法修改。测试用户 mogdb 是在 entrypoint.sh 中自定义创建的用户。

MogDB 镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

MogDB 的密码有复杂度要求，密码长度 8 个字符以上，必须同时包含大写字母、小写字母、数字、以及特殊符号（特殊符号仅包含( \# ? ! @ $ % ^ & * \- )，并且 ! $ & 需要用转义符"\\"进行转义。

**GS_NODENAME**

指定数据库节点名称，默认为 mogdb。

**GS_USERNAME**

指定数据库连接用户名，默认为 mogdb。

**GS_PORT**

指定数据库端口，默认为 5432。

<br/>

## 主备安装

### 准备工作

如需进行MogDB容器版主备搭建，请先完成上文"单实例安装"中的步骤。

### 操作步骤

1. 切换到root用户：

   ```
   su - root
   ```

2. 创建自定义网络，创建容器便于使用的固定IP：

   ```
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. 创建主库容器：

   ```
   docker run --name mogdb_master \
   --network myNetwork --ip 172.18.0.10 --privileged=true \
   --hostname mogdb_master --detach \
   --env GS_PORT=6432 \
   --env OG_SUBNET=172.18.0.0/16 \
   --env GS_PASSWORD=Enmo@1234 \
   --env NODE_NAME=mogdb_master \
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" \
   swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd -M primary
   ```

   ![docker-installation-1](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-1.png)

4. 创建备库容器：

   ```
    docker run --name mogdb_slave_one \
    --network myNetwork --ip 172.18.0.11 --privileged=true \
    --hostname mogdb_slave_one --detach \
    --env GS_PORT=6432 \
    --env OG_SUBNET=172.18.0.0/16 \
    --env GS_PASSWORD=Enmotech@1234 \
    --env NODE_NAME=mogdb_slave_one \
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" \
    enmotech/opengauss:1.0.1 -M standby
   ```

    ![docker-installation-2](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-2.png)

5. 查询主库角色状态：

   ```
   docker exec -it mogdb_master bash
   su - omm
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-3](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-3.png)

6. 查询备库角色状态：

   ```
   docker exec -it mogdb_slave_one bash
   su - omm
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-4](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-4.png)

   > 说明：从上面主库Senders信息和备库Receiver可以看到主备状态正常。

### 读写测试

主库写测试：

```
gsql -p6432
create table test(ID int);
insert into test values(1);
```

![docker-installation-5](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-5.png)

备库读测试：

```
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-6](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-6.png)

> 说明：命令结果显示主库可写，备库可读但不可写。

### 切换测试

将mogdb_slave_one切换为主库，mogdb_master切换为备库。

mogdb_slave_one执行switchover：

```
gs_ctl switchover -D /var/lib/opengauss/data/
```

![docker-installation-7](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-7.png)

mogdb_slave_one查询状态：

```
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-8](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-8.png)

mogdb_master查询状态：

```
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-9](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-9.png)

可以看到mogdb_master变为备库，mogdb_slave_one变为主库，切换成功。

### 数据读写验证

切换后的主库mogdb_slave_one做写入验证：

```
gsql -p6432
select * from test;
insert into test values(2);
```

![docker-installation-10](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-10.png)

切换后的备库mogdb_master做读取验证：

```
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-11](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-11.png)

可以看到，原备库切换为主库后可写，原主库切换为备库后可读但不可写。

<br/>

## 后续步骤

MogDB 容器版不支持 MogHA 工具、OM 工具，仅用于测试使用，目前最多支持一主一备。MogDB 企业版包含 MogHA 组件，最多支持一主八备。容器版与企业版基础功能完全一致，在生产环境中建议使用 MogDB 企业版。
