---
title: 容器化MogDB安装
summary: 容器化MogDB安装
author: Guo Huan
date: 2021-03-30
---

# 容器化MogDB安装

本文档主要介绍MogDB容器版安装过程。MogDB容器版本不支持 MogHA工具、OM工具，目前最多支持一主一备。

<br/>

## 准备工作

MogDB支持的架构和操作系统版本如下:

- x86-64 CentOS 7.6
- ARM64 openEuler 20.03 LTS

在安装容器版MogDB之前，您需要先安装Docker。

Docker是一个开源的应用容器引擎，基于Go语言并遵从Apache2.0协议开源。Docker可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的Linux机器上，也可以实现虚拟化。

请前往[Docker](https://docs.docker.com/)网站根据您的操作系统选择对应的方法完成安装。

<br/>

## 安装步骤

1. 启动Docker服务。

2. 根据您的系统架构输入以下命令之一获取最新版MogDB镜像文件:

    x86-64架构的机器:

    ```bash
    docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
    ```

    ARM64架构的机器:

    ```bash
    docker pull swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_arm
    ```

    以x86-64架构的机器为例：  ![c3XvSP.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-12.png)

3. 输入以下命令为MogDB创建运行目录，下文以"mymogdb"为例:

    ```bash
    mkdir /mymogdb
    ```

4. 继续输入以下命令创建一个新的容器，将容器命名为"mymogdb"，以启动MogDB实例:

    ```bash
    docker run --name mymogdb  -d -e GS_PASSWORD=Secretpassword@123  \
     -v /mymogdb:/var/lib/opengauss  \
     -p 15432:5432  swr.cn-east-3.myhuaweicloud.com/enmotech/mogdb:1.1.0_amd
    ```

    > 注意:MogDB 的默认监听启动在容器内的 5432 端口上，如果想要从容器外部访问数据库，则需要在 *docker run* 的时候指定 *-p* 参数。比如以上命令将允许使用 15432 端口访问容器数据库。容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在 *docker run* 的时候指定 *-v* 参数来实现。比如以上命令将会指定将 MogDB 的所有数据文件存储在宿主机的/mymogdb 下。此外，如果使用 podman，会有目标路径检查，因此需要预先创建目标路径（步骤 4）。

5. 进入容器终端：

    ```bash
    docker exec -it mymogdb /bin/bash
    ```

    至此，MogDB容器版安装完成。

<br/>

## 使用MogDB

安装完成后，输入以下命令切换到omm用户，即可通过外部gsql进行数据库访问以正常体验MogDB:

```bash
su - omm
gsql -d postgres
```

![c3jMTJ.png](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-13.png)

<br/>

## 环境变量

为了更灵活地使用MogDB镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

**GS_PASSWORD**

使用MogDB镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了MogDB数据库的超级用户omm以及测试用户mogdb的密码。MogDB安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户mogdb是在entrypoint.sh中自定义创建的用户。

MogDB镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

MogDB的密码有复杂度要求，密码长度8个字符以上，必须同时包含大写字母、小写字母、数字、以及特殊符号（特殊符号仅包含( \# ? ! @ $ % ^ & * \- )，并且 ! $ & 需要用转义符"\\"进行转义。

**GS_NODENAME**

指定数据库节点名称，默认为mogdb。

**GS_USERNAME**

指定数据库连接用户名，默认为mogdb。

**GS_PORT**

指定数据库端口，默认为5432。

<br/>

## 后续步骤

MogDB容器版不支持 MogHA工具、OM工具，仅用于测试使用，目前最多支持一主一备。MogDB企业版包含MogHA组件，最多支持一主八备。容器版与企业版基础功能完全一致，在生产环境中建议使用MogDB企业版。
