---
title: 动态语句
summary: 动态语句
author: Guo Huan
date: 2021-03-04
---

# 动态语句

## 执行动态查询语句

介绍执行动态查询语句。MogDB提供EXECUTE IMMEDIATE和OPEN FOR两种方式实现动态查询。EXECUTE IMMEDIATE是通过动态执行SELECT语句，OPEN FOR是结合了游标的使用。当需要将查询的结果保存在一个数据集用于提取时，可使用OPEN FOR实现动态查询。

**EXECUTE IMMEDIATE**

语法图请参见[图1](#图1)。

**图 1** EXECUTE IMMEDIATE dynamic_select_clause::=<a id="图1"> </a>

![EXECUTE-IMMEDIATE-dynamic_select_clause](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-1.png)

using_clause子句的语法图参见[图2](#图2)。

**图 2** using_clause::=<a id="图2"> </a>

![using_clause](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-2.png)

对以上语法格式的解释如下:

- define_variable：用于指定存放单行查询结果的变量。

- USING IN bind_argument：用于指定存放传递给动态SQL值的变量，即在dynamic_select_string中存在占位符时使用。

- USING OUT bind_argument：用于指定存放动态SQL返回值的变量。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
    >
    > - 查询语句中，into和out不能同时存在；
    > - 占位符命名以“:”开始，后面可跟数字、字符或字符串，与USING子句的bind_argument一一对应；
    > - bind_argument只能是值、变量或表达式，不能是表名、列名、数据类型等数据库对象，即不支持使用bind_argument为动态SQL语句传递模式对象。如果存储过程需要通过声明参数传递数据库对象来构造动态SQL语句（常见于执行DDL语句时），建议采用连接运算符“||”拼接dynamic_select_clause；
    > - 动态PL/SQL块允许出现重复的占位符，即相同占位符只能与USING子句的一个bind_argument按位置对应。

**OPEN FOR**

动态查询语句还可以使用OPEN FOR打开动态游标来执行。

语法参见[图3](#图3)。

**图 3** open_for::=<a id="图3"> </a>

![open_for](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-3.png)

参数说明:

- cursor_name：要打开的游标名。
- dynamic_string：动态查询语句。
- USING value：在dynamic_string中存在占位符时使用。

游标的使用请参考[游标](1-11-cursors.md)。

## 执行动态非查询语句

**语法**

语法请参见[图4](#noselect::=)。

**图 4** noselect::=<a id="noselect::="> </a>

![noselect](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-4.png)

using_clause子句的语法参见[图5](#using_clause::=)。

**图 5** using_clause::=<a id="using_clause::="> </a>

![using_clause-0](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-5.png)

对以上语法格式的解释如下：

USING IN bind_argument用于指定存放传递给动态SQL值的变量，在dynamic_noselect_string中存在占位符时使用，即动态SQL语句执行时，bind_argument将替换相对应的占位符。要注意的是，bind_argument只能是值、变量或表达式，不能是表名、列名、数据类型等数据库对象。如果存储过程需要通过声明参数传递数据库对象来构造动态SQL语句（常见于执行DDL语句时），建议采用连接运算符“||”拼接dynamic_select_clause。另外，动态语句允许出现重复的占位符，相同占位符只能与唯一一个bind_argument按位置一一对应。

**示例**

```sql
--创建表
MogDB=# CREATE TABLE sections_t1
(
   section       NUMBER(4) ,
   section_name  VARCHAR2(30),
   manager_id    NUMBER(6),
   place_id      NUMBER(4)
);

--声明变量
MogDB=# DECLARE
   section       NUMBER(4) := 280;
   section_name  VARCHAR2(30) := 'Info support';
   manager_id    NUMBER(6) := 103;
   place_id      NUMBER(4) := 1400;
   new_colname   VARCHAR2(10) := 'sec_name';
BEGIN
--执行查询
    EXECUTE IMMEDIATE 'insert into sections_t1 values(:1, :2, :3, :4)'
       USING section, section_name, manager_id,place_id;
--执行查询（重复占位符）
    EXECUTE IMMEDIATE 'insert into sections_t1 values(:1, :2, :3, :1)'
       USING section, section_name, manager_id;
--执行ALTER语句（建议采用"||"拼接数据库对象构造DDL语句）
    EXECUTE IMMEDIATE 'alter table sections_t1 rename section_name to ' || new_colname;
END;
/

--查询数据
MogDB=# SELECT * FROM sections_t1;

--删除表
MogDB=# DROP TABLE sections_t1;
```

## 动态调用存储过程

动态调用存储过程必须使用匿名的语句块将存储过程或语句块包在里面，使用EXECUTE IMMEDIATE…USING语句后面带IN、OUT来输入、输出参数。

**语法**

语法请参见[图6](#call_procedure::=)。

**图 6** call_procedure::=<a id="call_procedure::="> </a>

![call_procedure](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-6.png)

using_clause子句的语法参见[图7](#using_clause)。

**图 7** using_clause::=<a id="using_clause"> </a>

![using_clause-1](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-7.png)

对以上语法格式的解释如下：

- CALL procedure_name：调用存储过程。
- [:placeholder1，:placeholder2，…]：存储过程参数占位符列表。占位符个数与参数个数相同。
- USING [IN|OUT|IN OUT] bind_argument：用于指定存放传递给存储过程参数值的变量。bind_argument前的修饰符与对应参数的修饰符一致。

## 动态调用匿名块

动态调用匿名块是指在动态语句中执行匿名块，使用EXECUTE IMMEDIATE…USING语句后面带IN、OUT来输入、输出参数。

**语法**

语法请参见[图8](#call_anonymous_block::=)。

**图 8** call_anonymous_block::=<a id="call_anonymous_block::="> </a>

![call_anonymous_block](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-8.png)

using_clause子句的语法参见[图9](#using_clause::)。

**图 9** using_clause::=<a id="using_clause::"> </a>

![using_clause-2](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/dynamic-statements-9.png)

对以上语法格式的解释如下：

- 匿名块程序实施部分，以BEGIN语句开始，以END语句停顿，以一个分号结束。
- USING [IN|OUT|IN OUT] bind_argument，用于指定存放传递给存储过程参数值的变量。bind_argument前的修饰符与对应参数的修饰符一致。
- 匿名块中间的输入输出参数使用占位符来指明，要求占位符个数与参数个数相同，并且占位符所对应参数的顺序和USING中参数的顺序一致。
- 目前MogDB在动态语句调用匿名块时，EXCEPTION语句中暂不支持使用占位符进行输入输出参数的传递。
