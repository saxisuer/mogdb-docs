---
title: 静态分区裁剪
summary: 静态分区裁剪
author: Guo Huan
date: 2022-06-14
---

# 静态分区裁剪

MogDB主要根据静态谓词确定何时使用静态裁剪。

如果在解析时MogDB可以识别访问了哪个连续的分区集合，则执行计划中的**Selected Partitions:**列显示正在访问的分区的开始值和结束值。对于任何其他分区裁剪情况（包括动态裁剪），MogDB都会显示**Selected Partitions:**。

静态分区裁剪可通过[EXPLAIN VERBOSE](../../../reference-guide/sql-syntax/EXPLAIN.md)语句查看执行计划。如下示例查询中，prune_tt01表有四个分区，查询计划中显示SeqScan支持扫描的分区为3和4，从而确定分区1和2已经在查询计划中裁剪掉。

```sql
MogDB=# \d+ prune_tt01
                      Table "public.prune_tt01"
 Column |  Type   | Modifiers | Storage | Stats target | Description 
--------+---------+-----------+---------+--------------+-------------
 a      | integer |           | plain   |              | 
 b      | integer |           | plain   |              | 
Indexes:
    "index_prune_tt01" btree (a) LOCAL TABLESPACE pg_default
Partition By RANGE(a)
Number of partitions: 4 (View pg_partition to check each partition range.)
Has OIDs: no
Options: orientation=row, compression=no
```

```sql
MogDB=# explain verbose select * from prune_tt01 where a>12;
                                             QUERY PLAN                                             
----------------------------------------------------------------------------------------------------
 Partition Iterator  (cost=13.80..27.75 rows=716 width=8)
   Output: a, b
   Iterations: 2
   Selected Partitions:  3..4
   ->  Partitioned Bitmap Heap Scan on public.prune_tt01  (cost=13.80..27.75 rows=716 width=8)
         Output: a, b
         Recheck Cond: (prune_tt01.a > 12)
         ->  Partitioned Bitmap Index Scan on index_prune_tt01  (cost=0.00..13.62 rows=716 width=0)
               Index Cond: (prune_tt01.a > 12)
(9 rows)
```

<br/>

静态分区裁剪的详细支持范围如下表所示。

**表1** 静态分区裁剪的详细支持范围

| 序号 | 约束名称         | 约束范围                                                     |
| ---- | ---------------- | ------------------------------------------------------------ |
| 1    | 分区表类型       | Range分区、List分区、Hash分区                                |
| 2    | 分区表达式类型   | - 分区约束为单个表达式，如`a >12;`<br/>- 分区约束为BOOL表达式，如`a > 2 and a < 12;`<br/>- 分区约束为数组，如`a in (2, 3);`<br/>- 分区约束为常量表达式，如`1 = 1;`<br/>- 分区约束为`Is (NOT)NULL`表达式，如`a IS NULL` |
| 3    | 分区表达式操作符 | - Range分区表支持：`=`、`>`、`>=`、`<`、`<=`五种操作符<br/>- List分区和Hash分区只支持`=`操作符 |
| 4    | 分区表达式参数   | 一侧为分区键，另一侧为常量，例如：`a > 12`                   |
| 5    | 二级分区         | Range、List和Hash三种分区表的组合，例如：Range-List等等      |
| 6    | 分区裁剪结果显示 | Explain verbose显示裁剪分区链表                              |
