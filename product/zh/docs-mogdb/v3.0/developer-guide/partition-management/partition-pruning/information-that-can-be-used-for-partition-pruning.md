---
title: 可用于分区裁剪的信息
summary: 可用于分区裁剪的信息
author: Guo Huan
date: 2022-06-14
---

# 可用于分区裁剪的信息

可以对分区列执行分区裁剪。

当您在范围或列表分区列上使用范围、`LIKE`、等式和`IN`列表谓词，以及在哈希分区列上使用等式和`IN`列表谓词时，MogDB会裁剪分区。

例如示例1中，表prune_tt01在列a上按范围分区。

MogDB使用分区列上的谓词来执行分区裁剪，如下所示。

**示例1 创建带有分区裁剪的表**

```sql
CREATE TABLE prune_tt01(a int, b int)
PARTITION BY RANGE(a)
(
  PARTITION prune_tt01_p1 VALUES LESS THAN(5),
  PARTITION prune_tt01_p2 VALUES LESS THAN(10),
  PARTITION prune_tt01_p3 VALUES LESS THAN(15),
  PARTITION prune_tt01_p4 VALUES LESS THAN(MAXVALUE)
);
INSERT INTO prune_tt01 VALUES (generate_series(1, 20), generate_series(1,20));
CREATE INDEX index_prune_tt01 ON prune_tt01 USING btree(a) LOCAL;

select * from prune_tt01 where a > 8 ;
```