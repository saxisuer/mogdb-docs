---
title: 何时使用列表分区
summary: 何时使用列表分区
author: Guo Huan
date: 2022-06-14
---

# 何时使用列表分区

列表分区可基于离散值将行显式映射到分区。

在示例1中，按区域分析帐户的客户经理可有效利用分区裁剪。

**示例1 创建具有列表分区的表**

```sql
CREATE TABLE accounts
( id             NUMBER
, account_number NUMBER
, customer_id    NUMBER
, branch_id      NUMBER
, region         VARCHAR(2)
, status         VARCHAR2(1)
)
PARTITION BY LIST (region)
( PARTITION p_northwest VALUES ('OR', 'WA')
, PARTITION p_southwest VALUES ('AZ', 'UT', 'NM')
, PARTITION p_northeast VALUES ('NY', 'VM', 'NJ')
, PARTITION p_southeast VALUES ('FL', 'GA')
, PARTITION p_northcentral VALUES ('SD', 'WI')
, PARTITION p_southcentral VALUES ('OK', 'TX')
);
```