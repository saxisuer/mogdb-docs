---
title: 存储过程
summary: 存储过程
author: Guo Huan
date: 2021-03-04
---

# 存储过程

商业规则和业务逻辑可以通过程序存储在MogDB中，这个程序就是存储过程。

存储过程是SQL和PL/pgSQL的组合。存储过程使执行商业规则的代码可以从应用程序中移动到数据库。从而，代码存储一次能够被多个程序使用。

存储过程的创建及调用办法请参考[CREATE PROCEDURE](../reference-guide/sql-syntax/CREATE-PROCEDURE.md)。

[用户自定义函数](user-defined-functions.md)一节所提到的PL/pgSQL语言创建的函数与存储过程的应用方法相通。具体介绍请参看[PL/pgSQL-SQL过程语言](../developer-guide/plpgsql/1-1-plpgsql-overview.md)章节，除非特别声明，否则其中内容通用于存储过程和用户自定义函数。
