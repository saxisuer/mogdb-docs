---
title: PQerrorMessage
summary: PQerrorMessage
author: Guo Huan
date: 2021-05-17
---

# PQerrorMessage

## 功能描述

返回连接上的错误信息。

## 原型

```
char* PQerrorMessage(const PGconn* conn);
```

## 参数

**表 1**

| **关键字** | **参数说明** |
| :--------- | :----------- |
| conn       | 连接句柄。   |

## 返回值

char类型指针。

## 示例

请参见[示例](../../libpq-example.md)章节。
