---
title: PQexecParamsBatch
summary: PQexecParamsBatch
author: Guo Huan
date: 2021-05-17
---

# PQexecParamsBatch

## 功能描述

执行一个批量绑定参数的命令。

## 原型

```
PGresult* PQexecParamsBatch(PGconn* conn,
                            const char* command,
                            int nParams,
                            int nBatch,
                            const Oid* paramTypes,
                            const char* const* paramValues,
                            const int* paramLengths,
                            const int* paramFormats,
                            int resultFormat);
```

## 参数

**表 1** PQexecParamsBatch参数

| **关键字**   | **参数说明**                   |
| :----------- | :----------------------------- |
| conn         | 连接句柄。                     |
| command      | SQL文本串。                    |
| nParams      | 绑定参数的个数。               |
| nBatch       | 批量操作数。                   |
| paramTypes   | 绑定参数类型。                 |
| paramValues  | 绑定参数的值。                 |
| paramLengths | 参数长度。                     |
| paramFormats | 参数格式（文本或二进制）。     |
| resultFormat | 返回结果格式（文本或二进制）。 |

## 返回值

PGresult类型指针。
