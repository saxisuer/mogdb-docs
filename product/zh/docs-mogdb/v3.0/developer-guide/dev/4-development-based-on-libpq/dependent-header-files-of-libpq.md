---
title: libpq使用依赖的头文件
summary: libpq使用依赖的头文件
author: Guo Huan
date: 2022-04-26
---

# libpq使用依赖的头文件

使用libpq的前端程序必须包括头文件libpq-fe.h并且必须与libpq库链接。
