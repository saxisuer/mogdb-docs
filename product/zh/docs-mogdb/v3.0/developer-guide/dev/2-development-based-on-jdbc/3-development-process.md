---
title: 开发流程
summary: 开发流程
author: Guo Huan
date: 2021-04-26
---

# 开发流程

**图 1** 采用JDBC开发应用程序的流程

![采用JDBC开发应用程序的流程](https://cdn-mogdb.enmotech.com/docs-media/mogdb/developer-guide/development-process-1.png)
