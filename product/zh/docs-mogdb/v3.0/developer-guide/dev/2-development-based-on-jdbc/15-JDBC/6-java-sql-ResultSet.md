---
title: java.sql.ResultSet
summary: java.sql.ResultSet
author: Guo Huan
date: 2021-05-17
---

# java.sql.ResultSet

java.sql.ResultSet是执行结果集接口。

**表 1** 对java.sql.ResultSet的支持情况

| 方法名                                                       | 返回值类型        | 支持JDBC 4 |
| :----------------------------------------------------------- | :---------------- | :--------- |
| absolute(int row)                                            | Boolean           | Yes        |
| afterLast()                                                  | void              | Yes        |
| beforeFirst()                                                | void              | Yes        |
| cancelRowUpdates()                                           | void              | Yes        |
| clearWarnings()                                              | void              | Yes        |
| close()                                                      | void              | Yes        |
| deleteRow()                                                  | void              | Yes        |
| findColumn(String columnLabel)                               | int               | Yes        |
| first()                                                      | Boolean           | Yes        |
| getArray(int columnIndex)                                    | Array             | Yes        |
| getArray(String columnLabel)                                 | Array             | Yes        |
| getAsciiStream(int columnIndex)                              | InputStream       | Yes        |
| getAsciiStream(String columnLabel)                           | InputStream       | Yes        |
| getBigDecimal(int columnIndex)                               | BigDecimal        | Yes        |
| getBigDecimal(String columnLabel)                            | BigDecimal        | Yes        |
| getBinaryStream(int columnIndex)                             | InputStream       | Yes        |
| getBinaryStream(String columnLabel)                          | InputStream       | Yes        |
| getBlob(int columnIndex)                                     | Blob              | Yes        |
| getBlob(String columnLabel)                                  | Blob              | Yes        |
| getBoolean(int columnIndex)                                  | Boolean           | Yes        |
| getBoolean(String columnLabel)                               | Boolean           | Yes        |
| getByte(int columnIndex)                                     | byte              | Yes        |
| getBytes(int columnIndex)                                    | byte[]            | Yes        |
| getByte(String columnLabel)                                  | byte              | Yes        |
| getBytes(String columnLabel)                                 | byte[]            | Yes        |
| getCharacterStream(int columnIndex)                          | Reader            | Yes        |
| getCharacterStream(String columnLabel)                       | Reader            | Yes        |
| getClob(int columnIndex)                                     | Clob              | Yes        |
| getClob(String columnLabel)                                  | Clob              | Yes        |
| getConcurrency()                                             | int               | Yes        |
| getCursorName()                                              | String            | Yes        |
| getDate(int columnIndex)                                     | Date              | Yes        |
| getDate(int columnIndex, Calendar cal)                       | Date              | Yes        |
| getDate(String columnLabel)                                  | Date              | Yes        |
| getDate(String columnLabel, Calendar cal)                    | Date              | Yes        |
| getDouble(int columnIndex)                                   | double            | Yes        |
| getDouble(String columnLabel)                                | double            | Yes        |
| getFetchDirection()                                          | int               | Yes        |
| getFetchSize()                                               | int               | Yes        |
| getFloat(int columnIndex)                                    | float             | Yes        |
| getFloat(String columnLabel)                                 | float             | Yes        |
| getInt(int columnIndex)                                      | int               | Yes        |
| getInt(String columnLabel)                                   | int               | Yes        |
| getLong(int columnIndex)                                     | long              | Yes        |
| getLong(String columnLabel)                                  | long              | Yes        |
| getMetaData()                                                | ResultSetMetaData | Yes        |
| getObject(int columnIndex)                                   | Object            | Yes        |
| getObject(int columnIndex, Class&lt;T&gt; type)              | &lt;T&gt; T       | Yes        |
| getObject(int columnIndex, Map&lt;String,Class&lt;?&gt;&gt; map) | Object            | Yes        |
| getObject(String columnLabel)                                | Object            | Yes        |
| getObject(String columnLabel, Class&lt;T&gt; type)           | &lt;T&gt; T       | Yes        |
| getObject(String columnLabel, Map&lt;String,Class&lt;?&gt;&gt; map) | Object            | Yes        |
| getRow()                                                     | int               | Yes        |
| getShort(int columnIndex)                                    | short             | Yes        |
| getShort(String columnLabel)                                 | short             | Yes        |
| getSQLXML(int columnIndex)                                   | SQLXML            | Yes        |
| getSQLXML(String columnLabel)                                | SQLXML            | Yes        |
| getStatement()                                               | Statement         | Yes        |
| getString(int columnIndex)                                   | String            | Yes        |
| getString(String columnLabel)                                | String            | Yes        |
| getNString(int columnIndex)                                  | String            | Yes        |
| getNString(String columnLabel)                               | String            | Yes        |
| getTime(int columnIndex)                                     | Time              | Yes        |
| getTime(int columnIndex, Calendar cal)                       | Time              | Yes        |
| getTime(String columnLabel)                                  | Time              | Yes        |
| getTime(String columnLabel, Calendar cal)                    | Time              | Yes        |
| getTimestamp(int columnIndex)                                | Timestamp         | Yes        |
| getTimestamp(int columnIndex, Calendar cal)                  | Timestamp         | Yes        |
| getTimestamp(String columnLabel)                             | Timestamp         | Yes        |
| getTimestamp(String columnLabel, Calendar cal)               | Timestamp         | Yes        |
| getType()                                                    | int               | Yes        |
| getWarnings()                                                | SQLWarning        | Yes        |
| insertRow()                                                  | void              | Yes        |
| isAfterLast()                                                | Boolean           | Yes        |
| isBeforeFirst()                                              | Boolean           | Yes        |
| isClosed()                                                   | Boolean           | Yes        |
| isFirst()                                                    | Boolean           | Yes        |
| isLast()                                                     | Boolean           | Yes        |
| last()                                                       | Boolean           | Yes        |
| moveToCurrentRow()                                           | void              | Yes        |
| moveToInsertRow()                                            | void              | Yes        |
| next()                                                       | Boolean           | Yes        |
| previous()                                                   | Boolean           | Yes        |
| refreshRow()                                                 | void              | Yes        |
| relative(int rows)                                           | Boolean           | Yes        |
| rowDeleted()                                                 | Boolean           | Yes        |
| rowInserted()                                                | Boolean           | Yes        |
| rowUpdated()                                                 | Boolean           | Yes        |
| setFetchDirection(int direction)                             | void              | Yes        |
| setFetchSize(int rows)                                       | void              | Yes        |
| updateArray(int columnIndex, Array x)                        | void              | Yes        |
| updateArray(String columnLabel, Array x)                     | void              | Yes        |
| updateAsciiStream(int columnIndex, InputStream x, int length) | void              | Yes        |
| updateAsciiStream(String columnLabel, InputStream x, int length) | void              | Yes        |
| updateBigDecimal(int columnIndex, BigDecimal x)              | void              | Yes        |
| updateBigDecimal(String columnLabel, BigDecimal x)           | void              | Yes        |
| updateBinaryStream(int columnIndex, InputStream x, int length) | void              | Yes        |
| updateBinaryStream(String columnLabel, InputStream x, int length) | void              | Yes        |
| updateBoolean(int columnIndex, boolean x)                    | void              | Yes        |
| updateBoolean(String columnLabel, boolean x)                 | void              | Yes        |
| updateByte(int columnIndex, byte x)                          | void              | Yes        |
| updateByte(String columnLabel, byte x)                       | void              | Yes        |
| updateBytes(int columnIndex, byte[] x)                       | void              | Yes        |
| updateBytes(String columnLabel, byte[] x)                    | void              | Yes        |
| updateCharacterStream(int columnIndex, Reader x, int length) | void              | Yes        |
| updateCharacterStream(String columnLabel, Reader reader, int length) | void              | Yes        |
| updateDate(int columnIndex, Date x)                          | void              | Yes        |
| updateDate(String columnLabel, Date x)                       | void              | Yes        |
| updateDouble(int columnIndex, double x)                      | void              | Yes        |
| updateDouble(String columnLabel, double x)                   | void              | Yes        |
| updateFloat(int columnIndex, float x)                        | void              | Yes        |
| updateFloat(String columnLabel, float x)                     | void              | Yes        |
| updateInt(int columnIndex, int x)                            | void              | Yes        |
| updateInt(String columnLabel, int x)                         | void              | Yes        |
| updateLong(int columnIndex, long x)                          | void              | Yes        |
| updateLong(String columnLabel, long x)                       | void              | Yes        |
| updateNull(int columnIndex)                                  | void              | Yes        |
| updateNull(String columnLabel)                               | void              | Yes        |
| updateObject(int columnIndex, Object x)                      | void              | Yes        |
| updateObject(int columnIndex, Object x, int scaleOrLength)   | void              | Yes        |
| updateObject(String columnLabel, Object x)                   | void              | Yes        |
| updateObject(String columnLabel, Object x, int scaleOrLength) | void              | Yes        |
| updateRow()                                                  | void              | Yes        |
| updateShort(int columnIndex, short x)                        | void              | Yes        |
| updateShort(String columnLabel, short x)                     | void              | Yes        |
| updateSQLXML(int columnIndex, SQLXML xmlObject)              | void              | Yes        |
| updateSQLXML(String columnLabel, SQLXML xmlObject)           | void              | Yes        |
| updateString(int columnIndex, String x)                      | void              | Yes        |
| updateString(String columnLabel, String x)                   | void              | Yes        |
| updateTime(int columnIndex, Time x)                          | void              | Yes        |
| updateTime(String columnLabel, Time x)                       | void              | Yes        |
| updateTimestamp(int columnIndex, Timestamp x)                | void              | Yes        |
| updateTimestamp(String columnLabel, Timestamp x)             | void              | Yes        |
| wasNull()                                                    | Boolean           | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 一个Statement不能有多个处于"open"状态的ResultSet。
> - 用于遍历结果集(ResultSet)的游标(Cursor)在被提交后不能保持“open”的状态。
