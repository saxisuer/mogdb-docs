---
title: 加载驱动
summary: 加载驱动
author: Guo Huan
date: 2021-04-26
---

# 加载驱动

在创建数据库连接之前，需要先加载数据库驱动程序。

加载驱动有两种方法:

- 在代码中创建连接之前任意位置隐含装载: Class.forName("org.opengauss.Driver");

- 在JVM启动时参数传递: java -Djdbc.drivers=org.opengauss.Driver jdbctest

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    > 上述jdbctest为测试用例程序的名称。
