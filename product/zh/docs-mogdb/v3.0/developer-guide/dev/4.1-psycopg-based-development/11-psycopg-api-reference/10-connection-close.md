---
title: connection.close()
summary: connection.close()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.close()

## 功能描述

此方法关闭数据库连接。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 此方法关闭数据库连接，并不自动调用commit()。如果只是关闭数据库连接而不调用commit()方法，那么所有更改将会丢失。

## 原型

```
connection.close()
```

## 参数

无

## 返回值

无

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。