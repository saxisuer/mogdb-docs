---
title: connection.commit()
summary: connection.commit()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.commit()

## 功能描述

此方法将当前挂起的事务提交到数据库。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 默认情况下，Psycopg在执行第一个命令之前打开一个事务：如果不调用commit()，任何数据操作的效果都将丢失。

## 原型

```
connection.commit()
```

## 参数

无

## 返回值

无

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。