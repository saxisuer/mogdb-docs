---
title: connection.cursor()
summary: connection.cursor()
author: Zhang Cuiping
date: 2021-10-11
---

# connection.cursor()

## 功能描述

此方法用于返回新的cursor对象。

## 原型

```
cursor(name=None, cursor_factory=None, scrollable=None, withhold=False)
```

## 参数

**表 1** connection.cursor参数

| **关键字**     | **参数说明**                       |
| :------------- | :--------------------------------- |
| name           | cursor名称，默认为None。           |
| cursor_factory | 用于创造非标准cursor，默认为None。 |
| scrollable     | 设置SCROLL选项，默认为None。       |
| withhold       | 设置HOLD选项，默认为False。        |

## 返回值

cursor对象（用于整个数据库使用Python编程的cursor）。

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。