---
title: cursor.close()
summary: cursor.close()
author: Zhang Cuiping
date: 2021-10-11
---

# cursor.close()

## 功能描述

此方法关闭当前连接的游标。

## 原型

```
cursor.close()
```

## 参数

无

## 返回值

无

## 示例

请参见[示例：常用操作](../../../../developer-guide/dev/4.1-psycopg-based-development/10.1-example-common-operations.md)。