---
title: 基于Psycopg开发
summary: 基于Psycopg开发
author: Zhang Cuiping
date: 2021-10-11
---

# 基于Psycopg开发

Psycopg是一种用于执行SQL语句的PythonAPI，可以为PostgreSQL、MogDB数据库提供统一访问接口，应用程序可基于它进行数据操作。Psycopg2是对libpq的封装，主要使用C语言实现，既高效又安全。它具有客户端游标和服务器端游标、异步通信和通知、支持“COPY TO/COPY FROM”功能。支持多种类型Python开箱即用，适配PostgreSQL数据类型；通过灵活的对象适配系统，可以扩展和定制适配。Psycopg2兼容Unicode和Python 3。

MogDB数据库提供了对Psycopg2特性的支持，并且支持psycopg2通过SSL模式链接。

**表 1** Psycopg支持平台

| 操作系统    | 平台     |
| :---------- | :------- |
| EulerOS 2.5 | x86_64位 |
| EulerOS 2.8 | ARM64位  |
