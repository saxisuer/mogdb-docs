---
title: 连接数据库
summary: 连接数据库
author: Zhang Cuiping
date: 2021-10-11
---

# 连接数据库

1. 使用psycopg2.connect函数获得connection对象。
2. 使用connection对象创建cursor对象。