---
title: 连接数据库
summary: 连接数据库
author: Zhang Cuiping
date: 2021-10-11
---

# 连接数据库（SSL方式）

用户通过psycopy2连接MogDB服务器时，可以通过开启SSL加密客户端和服务器之间的通讯。在使用SSL时，默认用户已经获取了服务端和客户端所需要的证书和私钥文件，关于证书等文件的获取请参考Openssl相关文档和命令。

1. 使用*.ini文件（python的configparser包可以解析这种类型的配置文件）保存数据库连接的配置信息。

2. 在连接选项中添加SSL连接相关参数：sslmode，sslcert，sslkey，sslrootcert。

   - sslmode：可选项见[表1](#table1.1)。

   - sslcert：客户端证书路径。

   - sslkey：客户端密钥路径。

   - sslrootcert：根证书路径。

3. 使用psycopg2.connect函数获得connection对象。

4. 使用connection对象创建cursor对象。

**表 1** sslmode的可选项及其描述 <a id=table1.1> </a>

| sslmode     | 是否会启用SSL加密 | 描述                                                         |
| :---------- | :---------------- | :----------------------------------------------------------- |
| disable     | 否                | 不适用SSL安全连接。                                          |
| allow       | 可能              | 如果数据库服务器要求使用，则可以使用SSL安全加密连接，但不验证数据库服务器的真实性。 |
| prefer      | 可能              | 如果数据库支持，那么首选使用SSL连接，但不验证数据库服务器的真实性。 |
| require     | 是                | 必须使用SSL安全连接，但是只做了数据加密，而并不验证数据库服务器的真实性。 |
| verify-ca   | 是                | 必须使用SSL安全连接。                                        |
| verify-full | 是                | 必须使用SSL安全连接，目前MogDB 暂不支持。           |