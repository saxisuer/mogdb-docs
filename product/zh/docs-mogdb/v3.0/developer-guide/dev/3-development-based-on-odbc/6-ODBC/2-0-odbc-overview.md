---
title: 说明
summary: 说明
author: Guo Huan
date: 2021-05-17
---

# 说明

ODBC接口是一套提供给用户的API函数，本节将对部分常用接口做具体描述，若涉及其他接口可参考[msdn](https://msdn.microsoft.com/en-us/library/windows/desktop/ms714177(v=vs.85).aspx)中“ODBC Programmer's Reference”项的相关内容。
