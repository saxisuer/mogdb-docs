---
title: SQLFetch
summary: SQLFetch
author: Guo Huan
date: 2021-05-17
---

# SQLFetch

## 功能描述

从结果集中取下一个行集的数据，并返回所有被绑定列的数据。

## 原型

```
SQLRETURN SQLFetch(SQLHSTMT    StatementHandle);
```

## 参数

**表 1** SQLFetch参数

| **关键字**      | **参数说明**                       |
| :-------------- | :--------------------------------- |
| StatementHandle | 语句句柄，通过SQLAllocHandle获得。 |

## 返回值

- SQL_SUCCESS：表示调用正确。
- SQL_SUCCESS_WITH_INFO：表示会有一些警告信息。
- SQL_ERROR：表示比较严重的错误，如：内存分配失败、建立连接失败等。
- SQL_NO_DATA：表示SQL语句不返回结果集。
- SQL_INVALID_HANDLE：表示调用无效句柄。其他API的返回值同理。
- SQL_STILL_EXECUTING：表示语句正在执行。

## 注意事项

当调用SQLFetch函数返回SQL_ERROR或SQL_SUCCESS_WITH_INFO时，通过调用SQLGetDiagRec函数，并将HandleType和Handle参数设置为SQL_HANDLE_STMT和StatementHandle，可得到一个相关的SQLSTATE值，通过SQLSTATE值可以查出调用此函数的具体信息。

## 示例

参见：[示例](2-23-Examples.md)
