---
title: SQLFreeConnect
summary: SQLFreeConnect
author: Guo Huan
date: 2021-05-17
---

# SQLFreeConnect

在ODBC 3.x版本中，ODBC 2.x的函数SQLFreeConnect已被SQLFreeHandle代替。有关详细信息请参阅[SQLFreeHandle](2-15-SQLFreeHandle.md)。
