---
title: 发布记录
summary: 发布记录
author: Bin.Liu
date: 2022-06-17
---

# 发布记录

> MacOS 如果遇到已损坏,请执行
>
> sudo xattr -r -d com.apple.quarantine /Applications/Mogeaver.app

## 22.2.1

2022-09-30

- [mogeaver-ce-22.2.1-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.2.1-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.2.1-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.2.1-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.2.1-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.2.1/mogeaver-ce-22.2.1-win32.win32.x86_64.zip)

同步DBeaver代码

## 22.1.5

2022-08-22

- [mogeaver-ce-22.1.5-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.1.5-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.1.5-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.1.5-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.1.5-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.1.5/mogeaver-ce-22.1.5-win32.win32.x86_64.zip)

### Features

- Support dbms_output.enable

### Fix

- 打开脚本编辑器部分SQL导致Hang死问题
- Create Schema Issue
- 其他使用问题

## 22.1.1

2022-06-30

- [mogeaver-ce-22.1.1-linux.gtk.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-linux.gtk.aarch64.tar.gz)
- [mogeaver-ce-22.1.1-linux.gtk.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-linux.gtk.x86_64.tar.gz)
- [mogeaver-ce-22.1.1-macosx.cocoa.aarch64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-macosx.cocoa.aarch64.tar.gz)
- [mogeaver-ce-22.1.1-macosx.cocoa.x86_64.tar.gz](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-macosx.cocoa.x86_64.tar.gz)
- [mogeaver-ce-22.1.1-win32.win32.x86_64.zip](https://cdn-mogdb.enmotech.com/mogeaver/22.1.1/mogeaver-ce-22.1.1-win32.win32.x86_64.zip)

### Features

- 支持创建数据库选择兼容模式
- 支持MogDB角色（role）
- 支持Large序列 (MogDB 3.0)
- 支持分区查看功能 （暂未支持在界面中创建分区表）
- 支持Package（暂未实现类似Oracle的go to source code功能）
- 支持JOB
- 打包了debug功能，支持DBE_PLDEBUGGER服务端调试。
- 支持BLOB，CLOB的修改和查看
