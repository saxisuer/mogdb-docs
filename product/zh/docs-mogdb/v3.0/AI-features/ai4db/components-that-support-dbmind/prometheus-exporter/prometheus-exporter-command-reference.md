---
title: 命令参考
summary: 命令参考
author: Guo Huan
date: 2022-05-06
---

# 命令参考

reprocessing-exporter的使用帮助详情：

```
gs_dbmind component reprocessing_exporter --help
usage:  [-h] [--disable-https] [--ssl-keyfile SSL_KEYFILE]
        [--ssl-certfile SSL_CERTFILE]
        [--web.listen-address WEB.LISTEN_ADDRESS]
        [--web.listen-port WEB.LISTEN_PORT]
        [--collector.config COLLECTOR.CONFIG] [--log.filepath LOG.FILEPATH]
        [--log.level {debug,info,warn,error,fatal}] [--version]
        prometheus_host prometheus_port

Reprocessing Exporter: A re-processing module for metrics stored in the
Prometheus server.

positional arguments:
  prometheus_host       from which host to pull data
  prometheus_port       the port to connect to the Prometheus host

optional arguments:
  -h, --help            show this help message and exit
  --disable-https       disable Https schema
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --collector.config COLLECTOR.CONFIG
                        according to the content of the yaml file for metric
                        collection
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above.
                        Valid levels: [debug, info, warn, error, fatal]
  --version             show program's version number and exit
```

**表 1** reprocessing-exporter的命令行参数详情表

| 参数                 | 参数说明                               | 取值范围                        |
| :------------------- | :------------------------------------- | :------------------------------ |
| prometheus_host      | Prometheus-server的IP地址              | -                               |
| prometheus_port      | Prometheus-server的服务侦听端口号      | 1024-65535                      |
| -h, --help           | 帮助选项                               | -                               |
| --disable-https      | 禁用Https协议                          | -                               |
| --ssl-keyfile        | Https协议使用的证书私钥文件路径        | -                               |
| --ssl-certfile       | Https协议使用的证书文件路径            | -                               |
| --web.listen-address | 该exporter服务的绑定IP                 | -                               |
| --web.listen-port    | 该exporter服务的侦听端口               | 1024-65535                      |
| --collector.config   | 显性指定的待采集指标配置文件路径       | -                               |
| --log.filepath       | 日志文件保存路径，默认保存在当前目录下 | -                               |
| --log.level          | 日志文件的打印级别，默认为INFO级别     | debug, info, warn, error, fatal |
| --version            | 显示版本信息                           | -                               |

openGauss-exporter的使用帮助详情：

```
gs_dbmind component opengauss_exporter --help
usage:  [-h] --url URL [--config CONFIG] [--constant-labels CONSTANT_LABELS]
        [--web.listen-address WEB.LISTEN_ADDRESS]
        [--web.listen-port WEB.LISTEN_PORT]
        [--web.telemetry-path WEB.TELEMETRY_PATH] [--disable-cache]
        [--disable-settings-metrics] [--disable-statement-history-metrics]
        [--disable-https] [--ssl-keyfile SSL_KEYFILE]
        [--ssl-certfile SSL_CERTFILE] [--parallel PARALLEL]
        [--log.filepath LOG.FILEPATH]
        [--log.level {debug,info,warn,error,fatal}] [--version]

openGauss Exporter (DBMind): Monitoring for MogDB.

optional arguments:
  -h, --help            show this help message and exit
  --url URL             MogDB database target url.
  --config CONFIG       path to config dir or file.
  --constant-labels CONSTANT_LABELS
                        a list of label=value separated by comma(,).
  --web.listen-address WEB.LISTEN_ADDRESS
                        address on which to expose metrics and web interface
  --web.listen-port WEB.LISTEN_PORT
                        listen port to expose metrics and web interface
  --web.telemetry-path WEB.TELEMETRY_PATH
                        path under which to expose metrics.
  --disable-cache       force not using cache.
  --disable-settings-metrics
                        not collect pg_settings.yml metrics.
  --disable-statement-history-metrics
                        not collect statement-history metrics (including slow
                        queries).
  --disable-https       disable Https schema
  --ssl-keyfile SSL_KEYFILE
                        set the path of ssl key file
  --ssl-certfile SSL_CERTFILE
                        set the path of ssl certificate file
  --parallel PARALLEL   not collect pg_settings.yml metrics.
  --log.filepath LOG.FILEPATH
                        the path to log
  --log.level {debug,info,warn,error,fatal}
                        only log messages with the given severity or above.
                        Valid levels: [debug, info, warn, error, fatal]
  --version             show program's version number and exit
```

**表 2** openGauss-exporter的命令行参数详情表

| 参数                                | 参数说明                                                     | 取值范围                        |
| :---------------------------------- | :----------------------------------------------------------- | :------------------------------ |
| --url                               | MogDB server的连接地址，例如 postgres://user:pwd@host:port/dbname | -                               |
| --constant-labels                   | 常量标签，将采集到的指标项中强行添加该标签列表               | 1024-65535                      |
| -h, --help                          | 帮助选项                                                     | -                               |
| --disable-https                     | 禁用Https协议                                                | -                               |
| --ssl-keyfile                       | Https协议使用的证书私钥文件路径                              | -                               |
| --ssl-certfile                      | Https协议使用的证书文件路径                                  | -                               |
| --web.listen-address                | 该exporter服务的绑定IP                                       | -                               |
| --web.listen-port                   | 该exporter服务的侦听端口                                     | 1024-65535                      |
| --web.telemetry-path                | 该exporter采集指标的URI地址，默认为 /metrics                 | -                               |
| --config                            | 显性指定的待采集指标配置文件路径                             | -                               |
| --log.filepath                      | 日志文件保存路径，默认保存在当前目录下                       | -                               |
| --log.level                         | 日志文件的打印级别，默认为INFO级别                           | debug, info, warn, error, fatal |
| --version                           | 显示版本信息                                                 | -                               |
| --disable-cache                     | 禁止使用缓存                                                 | -                               |
| --disable-settings-metrics          | 禁止采集pg_settings表的值                                    | -                               |
| --disable-statement-history-metrics | 禁止收集statement_history表中的慢SQL信息                     | -                               |
| --parallel                          | 连接到MogDB的数据库连接池的大小                              | 正整数                          |