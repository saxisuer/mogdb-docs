---
title: 环境部署
summary: 环境部署
author: Guo Huan
date: 2022-05-06
---

# 环境部署

1. 通过命令行参数启动对应的exporter进程，同时在本地机器创建侦听端口号。

2. 在Promethues的服务器端修改配置文件prometheus.yml，将启动的exporter信息添加进去，例如：

   ```
    scrape_configs:
    ...
      - job_name: 'opengauss_exporter'
        static_configs:
        - targets: ['127.0.0.1:9187']
    ...
   ```

其中，提供的exporter组件默认采用Https通信协议，因此需要用户默认提供ssl证书和秘钥文件，并通过--ssl-keyfile以及--ssl-certfile提供。若用户不希望使用Https协议，则可以通过--disable-https选项禁用该模式。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> 由于MogDB默认模式下的通信加密协议与PostgreSQL不兼容，故导致通过PyPI源安装的基于PostgreSQL编译的Python驱动psycopg2-binary默认无法连接至MogDB数据库。
> 因此，需要用户自行编译psycopg2或修改GUC参数进行适配。也可通过openGauss官方网站下载基于openGauss编译的psycopg2（官方网站仅提供部分Python版本的编译包，一般为Python3.6版本，需要用户鉴别)。
>
> - 官方openGauss Python驱动下载地址为：
>   [https://opengauss.org/zh/download/](https://opengauss.org/zh/download/)
> - 关于Python驱动的适配问题，可参考openGauss官方操作指南：
>   [https://mp.weixin.qq.com/s/2TobUQKtw0N9sBpMZJr6zw](https://mp.weixin.qq.com/s/2TobUQKtw0N9sBpMZJr6zw)
