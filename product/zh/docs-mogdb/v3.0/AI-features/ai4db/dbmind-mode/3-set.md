---
title: set子命令
summary: set子命令
author: Guo Huan
date: 2022-05-06
---

# set子命令

该命令用于修改配置文件dbmind.conf中的参数值，与用户手动修改配置文件dbmind.conf一般无差异。例如修改配置目录confpath中的配置文件dbmind.conf中TSDB配置部分，host参数的值，并将其设置为127.0.0.1。则可通过下述命令实现：

```
gs_dbmind set TSDB host 127.0.0.1 -c confpath
```

在修改上述普通参数时，与手动修改配置文件dbmind.conf无差异。但是，当用户想要修改密码项时，则必须通过该命令才可以实现修改，否则通过用户手动修改的配置文件无法实现对明文密码的加密，例如：

```
gs_dbmind set METADATABASE password xxxxxx -c confpath
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该命令对于字符串是大小写敏感的，如果输错则可能出现执行过程错误。

## 命令参考

用户可以通过--help选项获得该模式的帮助信息，例如：

```
gs_dbmind set --help
```

```
usage:  set [-h] -c DIRECTORY section option target

positional arguments:
  section               which section (case sensitive) to set
  option                which option to set
  target                the parameter target to set

optional arguments:
  -h, --help            show this help message and exit
  -c DIRECTORY, --conf DIRECTORY
                        set the directory of configuration files
```

**表 1** 模块命令行参数说明：python dbmind/ set xxx

| 参数       | 参数说明             | 取值范围 |
| :--------- | :------------------- | :------- |
| -h, --help | 帮助命令             | -        |
| -c，--conf | 配置文件目录confpath | -        |
| section    | 设置区               | -        |
| option     | 设置项               | -        |
| target     | 设置值               | -        |
