---
title: 命令参考
summary: 命令参考
author: Guo Huan
date: 2022-05-06
---

# 命令参考

**表 1** gs_dbmind component slow_query_diagnosis 命令行说明

| 参数             | 参数说明                       | 取值范围                               |
| :--------------- | :----------------------------- | :------------------------------------- |
| -h, --help       | 帮助命令                       | -                                      |
| action           | 动作参数                       | - show：结果展示<br/>- clean：清理结果 |
| -c，--conf       | 配置目录                       | -                                      |
| --query          | 慢SQL文本                      | *                                      |
| --start-time     | 显示开始时间的时间戳，单位毫秒 | 非负整数                               |
| --end-time       | 显示结束时间的时间戳，单位毫秒 | 非负整数                               |
| --retention-days | 清理天数级结果                 | 非负实数                               |