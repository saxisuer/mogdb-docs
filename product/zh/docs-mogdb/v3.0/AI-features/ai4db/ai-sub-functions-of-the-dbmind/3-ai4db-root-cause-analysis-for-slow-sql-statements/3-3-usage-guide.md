---
title: 使用指导
summary: 使用指导
author: Guo Huan
date: 2022-05-06
---

# 使用指导

假设用户已经初始化配置文件目录confpath，则可以通过下述命令实现本特性的功能：

- 仅启动慢SQL诊断功能（输出Top3根因），启动命令如下（更多用法参考对service子命令的说明）：

  ```
  gs_dbmind service start -c confpath --only-run slow_query_diagnosis
  ```

- 用户交互式慢SQL诊断，命令如下：

  ```
  gs_dbmind component slow_query_diagnosis show -c confpath --query SQL --start-time timestamps0 --end-time timestamps1
  ```

- 用户手动清理历史预测结果，命令如下：

  ```
  gs_dbmind component slow_query_diagnosis clean -c confpath --retention-days DAYS
  ```

- 停止已启动的服务，命令如下：

  ```
  gs_dbmind service stop -c confpath
  ```
