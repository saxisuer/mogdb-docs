---
title: 概述
summary: 概述
author: Guo Huan
date: 2022-05-06
---

# 概述

本功能名为Predictor，是基于机器学习且具有在线学习能力的查询时间预测工具。通过不断学习数据库内收集的历史执行信息，实现计划的执行时间预测功能。

本特性需要拉起python进程AIEngine，用于模型的训练和推理。

该功能所在目录为$**GAUSSHOME**/bin/dbmind/components/predictor。由于该模块中某些功能涉及到相对复杂的搭建，因此，需要用户切换到该目录中寻找对应文件，并按照本章说明进行部署。
