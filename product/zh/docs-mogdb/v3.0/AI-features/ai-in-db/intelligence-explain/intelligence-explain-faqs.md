---
title: 常见问题处理
summary: 常见问题处理
author: Guo Huan
date: 2022-05-06
---

# 常见问题处理

## AI Engine配置问题

- **AiEngine启动失败**：请检查ip地址，端口是否可用；CA证书路径是否存在。
- **发起请求AiEngine无响应**：请检查通信双方CA证书是否一致。
- **训练，测试场景失败**：请检查模型文件保存路径是否存在；训练预测文件是否在正确下载。
- **更换AiEngine-IP地址**：按照[证书生成](intelligence-explain-environment-deployment.md#证书生成)步骤重新生成证书，在生成证书及密钥中替换成相应的IP地址即可。

## 数据库内部报错问题

问题：AiEngine链接失败。

```
ERROR:  AI engine connection failed.
CONTEXT:  referenced column: model_train_opt
```

处理方法：检查AIEngine是否正常拉起或重启AIEngine；检查通信双方CA证书是否一致；检查模型配置信息中的ip和端口是否匹配；

问题：模型不存在。

```
ERROR:  OPT_Model not found for model name XXX
CONTEXT:  referenced column: track_model_train_opt
```

处理方法：检查[GS_OPT_MODEL](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_OPT_MODEL.md)表中是否存在执行语句中“model_name”对应的模型；使用预测功能报错时，检查模型是否已被训练；
