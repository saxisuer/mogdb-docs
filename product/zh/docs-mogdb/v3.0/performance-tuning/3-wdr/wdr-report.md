---
title: 查看WDR报告
summary: 查看WDR报告
author: Guo Huan
date: 2022-04-23
---

# 查看WDR报告

## Database Stat

Database Stat列名称及描述如下表所示。

**表 1** Database Stat报表主要内容

| 列名称         | 描述                                                 |
| :------------- | :--------------------------------------------------- |
| DB Name        | 数据库名称。                                         |
| Backends       | 连接到该数据库的后端数。                             |
| Xact Commit    | 此数据库中已经提交的事务数。                         |
| Xact Rollback  | 此数据库中已经回滚的事务数。                         |
| Blks Read      | 在这个数据库中读取的磁盘块的数量。                   |
| Blks Hit       | 高速缓存中已经发现的磁盘块的次数。                   |
| Tuple Returned | 顺序扫描的行数。                                     |
| Tuple Fetched  | 随机扫描的行数。                                     |
| Tuple Inserted | 通过数据库查询插入的行数。                           |
| Tuple Updated  | 通过数据库查询更新的行数。                           |
| Tup Deleted    | 通过数据库查询删除的行数。                           |
| Conflicts      | 由于数据库恢复冲突取消的查询数量。                   |
| Temp Files     | 通过数据库查询创建的临时文件数量。                   |
| Temp Bytes     | 通过数据库查询写入临时文件的数据总量。               |
| Deadlocks      | 在该数据库中检索的死锁数。                           |
| Blk Read Time  | 通过数据库后端读取数据文件块花费的时间，以毫秒计算。 |
| Blk Write Time | 通过数据库后端写入数据文件块花费的时间，以毫秒计算。 |
| Stats Reset    | 重置当前状态统计的时间。                             |

## Load Profile

Load Profile指标名称及描述如下表所示。

**表 2** Load Profile报表主要内容

| 指标名称                  | 描述                             |
| :------------------------ | :------------------------------- |
| DB Time(us)               | 作业运行的elapse time总和。      |
| CPU Time(us)              | 作业运行的CPU时间总和。          |
| Redo size(blocks)         | 产生的WAL的大小（块数）。        |
| Logical read (blocks)     | 表或者索引文件的逻辑读（块数）。 |
| Physical read (blocks)    | 表或者索引的物理读（块数）。     |
| Physical write (blocks)   | 表或者索引的物理写（块数）。     |
| Read IO requests          | 表或者索引的读次数。             |
| Write IO requests         | 表或者索引的写次数。             |
| Read IO (MB)              | 表或者索引的读大小（MB）。       |
| Write IO (MB)             | 表或者索引的写大小（MB）。       |
| Logons                    | 登录次数。                       |
| Executes (SQL)            | SQL执行次数。                    |
| Rollbacks                 | 回滚事务数。                     |
| Transactions              | 事务数。                         |
| SQL response time P95(us) | 95%的SQL的响应时间。             |
| SQL response time P80(us) | 80%的SQL的响应时间。             |

## Instance Efficiency Percentages

Instance Efficiency Percentages指标名称及描述如下表所示。

**表 3** Instance Efficiency Percentages报表主要内容

| 指标名称          | 描述                                            |
| :---------------- | :---------------------------------------------- |
| Buffer Hit %      | Buffer Pool命中率。                             |
| Effective CPU %   | CPU time占DB time的比例。                       |
| WalWrite NoWait % | 访问WAL Buffer的event次数占总wait event的比例。 |
| Soft Parse %      | 软解析的次数占总的解析次数的比例。              |
| Non-Parse CPU %   | 非parse的时间占执行总时间的比例。               |

## Top 10 Events by Total Wait Time

Top 10 Events by Total Wait Time列名称及描述如下表所示。

**表 4** Top 10 Events by Total Wait Time报表主要内容

| 列名称              | 描述                   |
| :------------------ | :--------------------- |
| Event               | Wait Event名称。       |
| Waits               | wait次数。             |
| Total Wait Time(us) | 总wait时间（微秒）。   |
| Avg Wait Time(us)   | 平均wait时间（微秒）。 |
| Type                | Wait Event类别。       |

## Wait Classes by Total Wait Time

Wait Classes by Total Wait Time列名称及描述如下表所示。

**表 5** Wait Classes by Total Wait Time报表主要内容

| 列名称              | 描述                                                         |
| :------------------ | :----------------------------------------------------------- |
| Type                | Wait Event类别名称：<br/>- STATUS。<br/>- LWLOCK_EVENT。<br/>- LOCK_EVENT。<br/>- IO_EVENT。 |
| Waits               | Wait次数。                                                   |
| Total Wait Time(us) | 总Wait时间（微秒）。                                         |
| Avg Wait Time(us)   | 平均Wait时间（微秒）。                                       |

## Host CPU

Host CPU列名称及描述如下表所示。

**表 6** Host CPU报表主要内容

| 列名称             | 描述                           |
| :----------------- | :----------------------------- |
| Cpus               | CPU数量。                      |
| Cores              | CPU核数。                      |
| Sockets            | CPU Sockets数量。              |
| Load Average Begin | 开始snapshot的Load Average值。 |
| Load Average End   | 结束snapshot的Load Average值。 |
| %User              | 用户态在CPU时间上的占比。      |
| %System            | 内核态在CPU时间上的占比。      |
| %WIO               | Wait IO在CPU时间上的占比。     |
| %Idle              | 空闲时间在CPU时间上的占比。    |

## IO Profile

IO Profile指标名称及描述如下表所示。

**表 7** IO Profile指标表主要内容

| 指标名称          | 描述                |
| :---------------- | :------------------ |
| Database requests | Database IO次数。   |
| Database (MB)     | Database IO数据量。 |
| Database (blocks) | Database IO数据块。 |
| Redo requests     | Redo IO次数。       |
| Redo (MB)         | Redo IO量。         |

## Memory Statistics

Memory Statistics指标名称及描述如下表所示。

**表 8** Memory Statistics报表主要内容

| 指标名称            | 描述                         |
| :------------------ | :--------------------------- |
| shared_used_memory  | 已经使用共享内存大小（MB）。 |
| max_shared_memory   | 最大共享内存（MB）。         |
| process_used_memory | 进程已经使用内存（MB）。     |
| max_process_memory  | 最大进程内存（MB）。         |

## Time Model

Time Model名称及描述如下表所示。

**表 9** Time Model报表主要内容

| 名称                | 描述                                                         |
| :------------------ | :----------------------------------------------------------- |
| DB_TIME             | 所有线程端到端的墙上时间（WALL TIME）消耗总和（单位：微秒）。 |
| EXECUTION_TIME      | 消耗在执行器上的时间总和（单位：微秒)。                      |
| PL_EXECUTION_TIME   | 消耗在PL/SQL执行上的时间总和（单位：微秒）。                 |
| CPU_TIME            | 所有线程CPU时间消耗总和（单位：微秒）。                      |
| PLAN_TIME           | 消耗在执行计划生成上的时间总和（单位：微秒）。               |
| REWRITE_TIME        | 消耗在查询重写上的时间总和（单位：微秒）。                   |
| PL_COMPILATION_TIME | 消耗在SQL编译上的时间总和（单位：微秒）。                    |
| PARSE_TIME          | 消耗在SQL解析上的时间总和（单位：微秒）。                    |
| NET_SEND_TIME       | 消耗在网络发送上的时间总和（单位：微秒）。                   |
| DATA_IO_TIME        | 消耗在数据读写上的时间总和（单位：微秒）。                   |

## SQL Statistics

SQL Statistics列名称及描述如下表所示。

**表 10** SQL Statistics报表主要内容

| 列名称                | 描述                                           |
| :-------------------- | :--------------------------------------------- |
| Unique SQL Id         | 归一化的SQL ID。                               |
| Node Name             | 节点名称。                                     |
| User Name             | 用户名称。                                     |
| Tuples Read           | 访问的元组数量。                               |
| Calls                 | 调用次数。                                     |
| Min Elapse Time(us)   | 最小执行时间（us）。                           |
| Max Elapse Time(us)   | 最大执行时间（us）。                           |
| Total Elapse Time(us) | 总执行时间（us）。                             |
| Avg Elapse Time(us)   | 平均执行时间（us）。                           |
| Returned Rows         | SELECT返回行数。                               |
| Tuples Affected       | Insert/Update/Delete行数。                     |
| Logical Read          | Buffer逻辑读次数。                             |
| Physical Read         | Buffer物理读次数。                             |
| CPU Time(us)          | CPU时间（us）。                                |
| Data IO Time(us)      | IO上的时间花费（us）。                         |
| Sort Count            | 排序执行的次数。                               |
| Sort Time(us)         | 排序执行的时间（us）。                         |
| Sort Mem Used(KB)     | 排序过程中使用的work memory大小（KB）。        |
| Sort Spill Count      | 排序过程中，若发生落盘，写文件的次数。         |
| Sort Spill Size(KB)   | 排序过程中，若发生落盘，使用的文件大小（KB）。 |
| Hash Count            | hash执行的次数。                               |
| Hash Time(us)         | hash执行的时间（us）。                         |
| Hash Mem Used(KB)     | hash过程中使用的work memory大小（KB）。        |
| Hash Spill Count      | hash过程中，若发生落盘，写文件的次数。         |
| Hash Spill Size(KB)   | hash过程中，若发生落盘，使用的文件大小（KB）。 |
| SQL Text              | 归一化SQL字符串。                              |

## Wait Events

Wait Events列名称及描述如下表所示。

**表 11** Wait Events报表主要内容

| 列名称               | 描述                                                         |
| :------------------- | :----------------------------------------------------------- |
| Type                 | Wait Event类别名称：<br/>- STATUS。<br/>- LWLOCK_EVENT。<br/>- LOCK_EVENT。<br/>- IO_EVENT。 |
| Event                | Wait Event名称。                                             |
| Total Wait Time (us) | 总Wait时间（us）。                                           |
| Waits                | 总Wait次数。                                                 |
| Failed Waits         | Wait失败次数。                                               |
| Avg Wait Time (us)   | 平均Wait时间（us）。                                         |
| Max Wait Time (us)   | 最大Wait时间（us）。                                         |

## Cache IO Stats

Cache IO Stats包含User table和User index两张表，列名称及描述如下所示。

### User table IO activity ordered by heap blks hit ratio

**表 12** User table IO activity ordered by heap blks hit ratio报表主要内容

| 列名称               | 描述                                          |
| :------------------- | :-------------------------------------------- |
| DB Name              | Database名称。                                |
| Schema Name          | Schema名称。                                  |
| Table Name           | Table名称。                                   |
| %Heap Blks Hit Ratio | 此表的Buffer Pool命中率。                     |
| Heap Blks Read       | 该表中读取的磁盘块数。                        |
| Heap Blks Hit        | 此表缓存命中数。                              |
| Idx Blks Read        | 表中所有索引读取的磁盘块数。                  |
| Idx Blks Hit         | 表中所有索引命中缓存数。                      |
| Toast Blks Read      | 此表的TOAST表读取的磁盘块数（如果存在）。     |
| Toast Blks Hit       | 此表的TOAST表命中缓冲区数（如果存在）。       |
| Tidx Blks Read       | 此表的TOAST表索引读取的磁盘块数（如果存在）。 |
| Tidx Blks Hit        | 此表的TOAST表索引命中缓冲区数（如果存在）。   |

### User index IO activity ordered by idx blks hit ratio

**表 13** User index IO activity ordered by idx blks hit ratio报表主要内容

| 列名称              | 描述                     |
| :------------------ | :----------------------- |
| DB Name             | Database名称。           |
| Schema Name         | Schema名称。             |
| Table Name          | Table名称。              |
| Index Name          | Index名称。              |
| %Idx Blks Hit Ratio | Index的命中率。          |
| Idx Blks Read       | 所有索引读取的磁盘块数。 |
| Idx Blks Hit        | 所有索引命中缓存数。     |

## Utility status

Utility status包含Replication slot和Replication stat两张表，列名称及描述如下所示。

### Replication slot

**表 14** Replication slot报表主要内容

| 列名称        | 描述                     |
| :------------ | :----------------------- |
| Slot Name     | 复制节点名。             |
| Slot Type     | 复制节点类型。           |
| DB Name       | 复制节点数据库名称。     |
| Active        | 复制节点状态。           |
| Xmin          | 复制节点事务标识。       |
| Restart Lsn   | 复制节点的Xlog文件信息。 |
| Dummy Standby | 复制节点假备。           |

### Replication stat

**表 15** Replication stat报表主要内容

| 列名称                   | 描述                   |
| :----------------------- | :--------------------- |
| Thread Id                | 线程的PID。            |
| Usesys Id                | 用户系统ID。           |
| Usename                  | 用户名称。             |
| Application Name         | 应用程序。             |
| Client Addr              | 客户端地址。           |
| Client Hostname          | 客户端主机名。         |
| Client Port              | 客户端端口。           |
| Backend Start            | 程序起始时间。         |
| State                    | 日志复制状态。         |
| Sender Sent Location     | 发送端发送日志位置。   |
| Receiver Write Location  | 接收端write日志位置。  |
| Receiver Flush Location  | 接收端flush日志位置。  |
| Receiver Replay Location | 接收端replay日志位置。 |
| Sync Priority            | 同步优先级。           |
| Sync State               | 同步状态。             |

## Object stats

Object stats包含User Tables stats、User index stats和Bad lock stats三张表，列名称及描述如下所示。

### User Tables stats

**表 16** User Tables stats报表主要内容

| 列名称            | 描述                                                |
| :---------------- | :-------------------------------------------------- |
| DB Name           | Database名称。                                      |
| Schema            | Schema名称。                                        |
| Relname           | Relation名称。                                      |
| Seq Scan          | 此表发起的顺序扫描数。                              |
| Seq Tup Read      | 顺序扫描抓取的活跃行数。                            |
| Index Scan        | 此表发起的索引扫描数。                              |
| Index Tup Fetch   | 索引扫描抓取的活跃行数。                            |
| Tuple Insert      | 插入行数。                                          |
| Tuple Update      | 更新行数。                                          |
| Tuple Delete      | 删除行数。                                          |
| Tuple Hot Update  | HOT更新行数（即没有更新所需的单独索引）。           |
| Live Tuple        | 估计活跃行数。                                      |
| Dead Tuple        | 估计死行数。                                        |
| Last Vacuum       | 最后一次此表是手动清理的（不计算VACUUM FULL）时间。 |
| Last Autovacuum   | 上次被autovacuum守护进程清理的时间。                |
| Last Analyze      | 上次手动分析这个表的时间。                          |
| Last Autoanalyze  | 上次被autovacuum守护进程分析的时间。                |
| Vacuum Count      | 这个表被手动清理的次数（不计算VACUUM FULL）。       |
| Autovacuum Count  | 这个表被autovacuum清理的次数。                      |
| Analyze Count     | 这个表被手动分析的次数。                            |
| Autoanalyze Count | 这个表被autovacuum守护进程分析的次数。              |

### User index stats

**表 17** User index stats报表主要内容

| 列名称            | 描述                                     |
| :---------------- | :--------------------------------------- |
| DB Name           | Database名称。                           |
| Schema            | Schema名称。                             |
| Relname           | Relation名称。                           |
| Index Relname     | Index名称。                              |
| Index Scan        | 索引上开始的索引扫描数。                 |
| Index Tuple Read  | 通过索引上扫描返回的索引项数。           |
| Index Tuple Fetch | 通过使用索引的简单索引扫描抓取的表行数。 |

### Bad lock stats

**表 18** Bad lock stats报表主要内容

| 列名称        | 描述               |
| :------------ | :----------------- |
| DB Id         | 数据库的OID。      |
| Tablespace Id | 表空间的OID。      |
| Relfilenode   | 文件对象ID。       |
| Fork Number   | 文件类型。         |
| Error Count   | 失败计数。         |
| First Time    | 第一次发生时间。   |
| Last Time     | 最近一次发生时间。 |

## Configuration settings

Configuration settings列名称及描述如下表所示。

**表 19** Configuration settings报表主要内容

| 列名称        | 描述                           |
| :------------ | :----------------------------- |
| Name          | GUC名称。                      |
| Abstract      | GUC描述。                      |
| Type          | 数据类型。                     |
| Curent Value  | 当前值。                       |
| Min Value     | 合法最小值。                   |
| Max Value     | 合法最大值。                   |
| Category      | GUC类别。                      |
| Enum Values   | 如果是枚举值，列举所有枚举值。 |
| Default Value | 数据库启动时参数默认值。       |
| Reset Value   | 数据库重置时参数默认值。       |

## SQL Detail

SQL Detail列名称及描述如下表所示。

**表 20** SQL Detail报表主要内容

| 列名称        | 描述                               |
| :------------ | :--------------------------------- |
| Unique SQL Id | 归一化SQL ID。                     |
| User Name     | 用户名称。                         |
| Node Name     | 节点名称。Node模式下不显示该字段。 |
| SQL Text      | 归一化SQL文本。                    |
