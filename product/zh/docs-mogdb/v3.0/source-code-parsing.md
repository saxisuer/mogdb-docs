---
title: 源码解析
summary: 源码解析
author: Guo Huan
date: 2022-02-07
---

# 源码解析

本系列文章针对openGauss开源数据库的源码进行分模块解析，可以作为内核开发者了解openGauss数据库并基于openGauss进行数据库开发的参考教程，也可以作为广大高校计算机专业“数据库设计”课程的参考教材。

您可以点击以下链接跳转至墨天轮专栏查看本系列文章。

## openGauss简介

[openGauss简介（上）](https://www.modb.pro/db/47895)

[openGauss简介（下）](https://www.modb.pro/db/48291)

## openGauss开发快速入门

[openGauss开发快速入门（上）](https://www.modb.pro/db/48657)

[openGauss开发快速入门（中）](https://www.modb.pro/db/48870)

[openGauss开发快速入门（下）](https://www.modb.pro/db/49118)

## 公共组件源码解析

[公共组件源码解析（上）](https://www.modb.pro/db/49242)

[公共组件源码解析（下）](https://www.modb.pro/db/49711)

## 存储引擎源码解析

[存储引擎源码解析（一）](https://www.modb.pro/db/51541)

[存储引擎源码解析（二）](https://www.modb.pro/db/53926)

[存储引擎源码解析（三）](https://www.modb.pro/db/57991)

[存储引擎源码解析（四）](https://www.modb.pro/db/64255)

[存储引擎源码解析（五）](https://www.modb.pro/db/77848)

## 事务机制源码解析

[事务机制源码解析（一）](https://www.modb.pro/db/78553)

[事务机制源码解析（二）](https://www.modb.pro/db/79534)

[事务机制源码解析（三）](https://www.modb.pro/db/81991)

## SQL引擎源码解析

[SQL引擎源码解析（一）](https://www.modb.pro/db/86068)

[SQL引擎源码解析（二）](https://www.modb.pro/db/100257)

## 执行器解析

[执行器解析（一）](https://www.modb.pro/db/103591)

[执行器解析（二）](https://www.modb.pro/db/106997)

[执行器解析（三）](https://www.modb.pro/db/111770)

## AI技术

[AI技术（一）：自调优](https://www.modb.pro/db/162007)

[AI技术（二）：慢SQL发现](https://www.modb.pro/db/171668)

[AI技术（三）：智能索引推荐](https://www.modb.pro/db/210235)

[AI技术（四）：指标采集、预测与异常检测](https://www.modb.pro/db/245341)

[AI技术（五）：AI查询时间预测](https://www.modb.pro/db/251405)

[AI技术（六）：DeepSQL](https://www.modb.pro/db/329691)

## 安全管理源码解析

[安全管理源码解析（一）](https://www.modb.pro/db/335331)

[安全管理源码解析（二）](https://www.modb.pro/db/337160)

[安全管理源码解析（三）](https://www.modb.pro/db/378249)