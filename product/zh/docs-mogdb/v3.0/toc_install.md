<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->

# MogDB Documentation 3.0

## 安装指南

+ 安装准备
  + [环境要求](/installation-guide/installation-preparation/environment-requirement.md)
  + [操作系统配置](/installation-guide/installation-preparation/os-configuration.md)
+ [容器化安装](/installation-guide/docker-installation/docker-installation.md)
+ [PTK方式安装](/installation-guide/ptk-based-installation.md)
+ OM方式安装
  + [安装概述](/installation-guide/om-installation/installation-overview.md)
  + [获取安装包](/installation-guide/om-installation/obtaining-installation-package.md)
  + [极简安装](/installation-guide/om-installation/simple-installation.md)
  + [标准安装](/installation-guide/om-installation/standard-installation.md)
  + [安装验证](/installation-guide/om-installation/verifying-installation.md)
  + [卸载MogDB](/installation-guide/om-installation/uninstallation.md)
+ [手动安装](/installation-guide/manual-installation.md)
+ [推荐参数设置及新建测试库](/installation-guide/recommended-parameter-settings.md)