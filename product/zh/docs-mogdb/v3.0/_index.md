---
title: Introduction
summary: SELECT
author: Zhang Cuiping
date: 2022-08-07
---

<CustomContainer title="MogDB" subtitle="云和恩墨基于openGauss开源数据库打造，安稳易用的企业级关系型数据库。您可以在这里查看概念介绍、操作指南、应用开发、参考等产品文档。">

<CustomGroup label="了解">

[MogDB简介](./overview.md)

[特性描述](./characteristic-description/characteristic-description-overview.md)

[发布说明](./about-mogdb/mogdb-new-feature/release-note.md)

</CustomGroup>

<CustomGroup label="试用">

[MogDB实训环境](./quick-start/mogdb-playground.md)

</CustomGroup>

<CustomGroup label="部署">

[使用PTK安装MogDB](./installation-guide/ptk-based-installation.md)

[MogDB in Container](./installation-guide/docker-installation/docker-installation.md)

[MogDB on Kubernetes](../../docs-mogdb-stack/v1.0/quick-start.md)

</CustomGroup>

<CustomGroup label="开发">

[基于JDBC开发](./developer-guide/dev/2-development-based-on-jdbc/3-development-process.md)

[数据导入/导出](./administrator-guide/importing-and-exporting-data/importing-data/1-import-modes.md)

</CustomGroup>

<CustomGroup label="运维">

[日常运维](./administrator-guide/routine-maintenance/0-starting-and-stopping-mogdb.md)

[备份与恢复](./administrator-guide/br/1-1-br.md)

[升级指南](./administrator-guide/upgrade-guide.md)

[故障诊断指南](./common-faults-and-identification/common-fault-locating-methods.md)

[使用MogDB Stack](../../docs-mogdb-stack/v1.0/overview.md)

[MogHA高可用管理](../../docs-mogha/v2.3/overview.md)

[MogDB命令集](./reference-guide/tool-reference/tool-overview.md)

</CustomGroup>

<CustomGroup label="调优">

[系统优化指南](./performance-tuning/1-system/1-optimizing-os-parameters.md)

[SQL优化指南](./performance-tuning/2-sql/1-query-execution-process.md)

[WDR解读指南](./performance-tuning/3-wdr/wdr-snapshot-schema.md)

[TPCC测试优化指南](./performance-tuning/TPCC-performance-tuning-guide.md)

</CustomGroup>

<CustomGroup label="工具">

[PTK部署运维工具](../../docs-ptk/v0.4/overview.md)

[MTK异构数据库迁移](../../docs-mtk/v2.0/overview.md)

[SCA兼容性分析](../../docs-sca/v5.1/overview.md)

[MVD数据一致性校验](../../docs-mvd/v2.4/overview.md)

[MDB异构数据同步](../../docs-mdb/v1.0/overview.md)

[Mogeaver图形化工具](./mogeaver/mogeaver-overview.md)

</CustomGroup>

</CustomContainer>
