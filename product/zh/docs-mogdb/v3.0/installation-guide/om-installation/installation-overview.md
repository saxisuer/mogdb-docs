---
title: 安装概述
summary: 安装概述
author: Zhang Cuiping
date: 2021-04-02
---

# 安装概述

MogDB支持单机和一主多备两种部署方式。单机部署时，可在一台主机部署多个数据库实例，但为了数据安全，不建议用户这样部署。一主多备部署支持一台主机和最少一台备机，最多可配置8台备机。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**：
>
> - 通过OM方式安装时，只允许在单台物理机部署一个数据库系统。如果您需要在单台物理机部署多个数据库系统，建议您采用[PTK方式安装](../ptk-based-installation.md)。
>
> - MogDB支持PTK方式安装和OM方式安装。当前MogDB仅针对官方PTK安装方式产生的相关问题提供技术支持。