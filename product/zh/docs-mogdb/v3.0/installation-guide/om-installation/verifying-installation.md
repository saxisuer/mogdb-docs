---
title: 安装验证
summary: 安装验证
author: Zhang Cuiping
date: 2021-04-2
---

# 安装验证

通过MogDB提供的gs_om工具可以完成数据库状态检查。

**前提条件**

MogDB数据库已安装。

**操作步骤**

1. 以omm用户身份登录服务器。

2. 执行如下命令检查数据库状态是否正常，"cluster_state "显示"Normal"表示数据库可正常使用。

    ```bash
    gs_om -t status
    ```

3. 数据库安装完成后，默认生成名称为postgres的数据库。第一次连接数据库时可以连接到此数据库。

    其中postgres为需要连接的数据库名称，15400为数据库主节点的端口号，即XML配置文件中的dataPortBase的值。请根据实际情况替换。

    ```bash
    gsql -d postgres -p 15400
    ```
