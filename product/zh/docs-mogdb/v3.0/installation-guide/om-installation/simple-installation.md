---
title: 极简安装
summary: 极简安装
author: Zhang Cuiping
date: 2021-06-12
---

# 极简安装

## 单节点安装

本节以openEuler系统为例描述如何安装单节点。

1. 创建相关目录、用户、组，并授权。

   ```bash
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. 解压极简版安装包到安装目录。

   ```bash
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.gz -C /opt/software/mogdb/
   ```

3. 进入解压后目录下的simpleInstall。

   ```bash
   cd /opt/software/mogdb/simpleInstall
   ```

4. 执行install.sh脚本安装MogDB。

   ```bash
   sh install.sh -w Enmo@123 &&source ~/.bashrc
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - -w：初始化数据库密码（gs_initdb指定），安全需要必须设置。
   >
   > - -p：指定的MogDB端口号，如不指定，默认为5432。
   >
   > - -h|--help：打印使用说明。
   >
   > - 安装后，数据库的名称为sgnode（gs_initdb指定）。
   >
   > - 执行时，如果出现报错“the maximum number of SEMMNI is not correct, the current SEMMNI is xxx. Please check it.”，请使用有root权限的用户执行如下命令。
   >
   >   ```bash
   >   sysctl -w kernel.sem="250 85000 250 330"
   >   ```
   >
   > - 安装后，数据库目录安装路径/opt/software/mogdb/data/single_node，其中/opt/software/mogdb为解压包路径，data/single_node为新创建的数据库节点目录。
   >
   > - 安装完成后，默认生成名称为postgres的数据库。第一次连接数据库时可以连接到此数据库。

5. 安装执行完成后，使用ps和gs_ctl查看进程是否正常。

   ```bash
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/single_node
   ```

   执行ps命令，显示类似如下信息：

   ```bash
   omm      24209 11.9  1.0 1852000 355816 pts/0  Sl   01:54   0:33 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/single_node
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   执行gs_ctl命令，显示类似如下信息：

   ```bash
   gs_ctl query ,datadir is /opt/software/mogdb/data/single_node
   HA state:
       local_role                     : Normal
       static_connections             : 0
       db_state                       : Normal
       detail_information             : Normal

   Senders info:
       No information

    Receiver info:
   No information
   ```

6. 使用数据库

   ```bash
   -- 配置PATH
   echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
   source ~/.bash_profile
   -bash: ulimit: open files: cannot modify limit: Operation not permitted
   （默认设置fd可用大小超过系统设置，可以忽略该设置）

   -- 登录数据库
   gsql -d postgres -p 5432 -r
   gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.

   MogDB=#
   ```

## 一主一备节点安装

本节以openEuler系统为例描述如何安装一主一备节点。

1. 创建相关目录、用户、组，并授权。

   ```bash
   groupadd dbgrp -g 2000
   useradd omm -g 2000 -u 2000
   mkdir -p /opt/software/mogdb
   chown -R omm:dbgrp /opt/software/mogdb
   ```

2. 解压MogDB压缩包到安装目录。

   ```bash
   su - omm
   cd /opt/software/mogdb/
   tar -jxf MogDB-x.x.x-openEuler-64bit.tar.gz -C /opt/software/mogdb/
   ```

3. 进入解压后目录下的simpleInstall。

   ```bash
   cd /opt/software/mogdb/simpleInstall
   ```

4. 执行install.sh脚本安装MogDB。

   ```bash
   sh install.sh -w Enmo@123  --multinode
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - -w：初始化数据库密码（gs_initdb指定），安全需要必须设置。
   > - -p：指定的MogDB主节点端口号，默认5432。备节点端口号会使用主端口号+200，默认5632。
   > - --multinode：用来区分是单节点还是一主一备安装。
   > - -h|--help：打印使用说明。
   > - 安装后，数据库的主节点名称为nodename1，备节点名称为nodename2。
   > - 安装后，数据库主节点目录安装路径/opt/software/mogdb/data/master，备节点目录安装路径为/opt/software/mogdb/data/slave，其中/opt/software/mogdb为解压包路径，data/master(slave)为新创建的数据库节点目录。

5. 安装执行完成后，使用ps和gs_ctl查看进程是否正常。

   ```bash
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/master
   ```

   执行ps命令，显示类似如下信息：

   ```bash
   omm  4879 11.8 1.1 2082452 373832 pts/0  Sl   14:26   8:29 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/data/master -M primary
   omm  5083  1.1  0.9 1819988 327200 pts/0  Sl   14:26   0:49 /opt/software/mogdb/bin/mogdb -D /opt/software/mogdb/data/slave -M standby
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   执行gs_ctl命令，显示类似如下信息：

   ```bash
   gs_ctl query ,datadir is /opt/software/mogdb/data/master
   HA state:
       local_role                     : Primary
       static_connections             : 1
       db_state                       : Normal
       detail_information             : Normal
   
   Senders info:
       sender_pid                     : 5165
       local_role                     : Primary
       peer_role                      : Standby
       peer_state                     : Normal
       state                          : Streaming
       sender_sent_location           : 0/4005148
       sender_write_location          : 0/4005148
       sender_flush_location          : 0/4005148
       sender_replay_location         : 0/4005148
       receiver_received_location     : 0/4005148
       receiver_write_location        : 0/4005148
       receiver_flush_location        : 0/4005148
       receiver_replay_location       : 0/4005148
       sync_percent                   : 100%
       sync_state                     : Sync
       sync_priority                  : 1
       sync_most_available            : Off
       channel                        : 10.244.44.52:27001-->10.244.44.52:35912
   
    Receiver info:
   No information
   ```

6. 使用数据库

   ```bash
   -- 配置PATH
   echo "PATH=/opt/software/mogdb/bin:\$PATH" >> /home/omm/.bash_profile
   source ~/.bash_profile
   -bash: ulimit: open files: cannot modify limit: Operation not permitted
   （默认设置fd可用大小超过系统设置，可以忽略该设置）
   
   -- 登录数据库
   gsql -d postgres -p 5432 -r
   gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.
   
   MogDB=#
   ```