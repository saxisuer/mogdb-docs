---
title: mog_xlogdump使用说明
summary: mog_xlogdump使用说明
author: Guo Huan
date: 2021-11-15
---

# mog_xlogdump使用说明

## 简介

mog_xlogdump是云和恩墨独立开发的wal日志离线解析工具。主要用于在主备集群场景中，数据库永久性宕机且无法恢复的情况下，反向解析无法启动的数据库，然后在集群中恢复wal日志尾部没有同步的数据。

<br/>

## 研发背景

在MogDB主备高可用集群一主多备，使用异步逻辑复制场景中，当主库宕机其事务提交完时，该事务操作的数据已经被写入wal日志中。主库宕机后由于无法发送给备库，备库会产生不完整的数据段日志。因此在主库宕机后，备库与主库之间的数据存在数据丢失、逻辑没有对齐的情况。所以其后备库组成的主备集群和实际业务中数据存在数据丢失的风险。

在主库恢复期间，后备库组成的集群会有业务数据写入，此时即时主库恢复，主库的wal日志末尾不完整的数据段的段号、起始和结束位置与备库中已经产生不一致现象，也会导致主库最后宕机时丢失的数据无法恢复到备库中。

<br/>

## 使用场景

MogDB高可用集群中，当主库宕机时，在walbuffer写满一定比例时，或者checkpoint，或者commit时，触发walbuffer写入wal日志。由于数据库宕机，逻辑同步的WalSender线程停止发送日志，备库接收到不完整的数据段wal日志。此时则需要用flashback工具读取主库的wal日志中的数据块，并解码出对应数据操作的SQL语句，供DBA分析数据是否有价值，并恢复到备库中。

<br/>

## 原理说明

该工具使用的两个机制，一个是wal日志的头部解析，另一个是逻辑复制机制。

实现步骤主要分三步：

1. 读取wal日志文件，解析头部。

2. 依次读取数据，并反解码数据。

3. 根据数据不同的数据类型，回调不同类型的函数输出。

<br/>

## 支持解析的表类型

目前支持分区表和普通表。

<br/>

## 支持解析的数据类型

bool、bytea、char、name、int8、int2、int、text、oid、tid、xid、cid、xid32、clob、float4、float8、money、inet、varchar、numeric、int4;

> 注意：由于mog_xlogdump是离线wal解析工具，所以暂时不支持需要用toast数据的大数据类型（clob等），下个版本将会支持toast表文件离线解析功能。

<br/>

## 安装方法

访问[MogDB官网下载页面](https://www.mogdb.io/downloads/mogdb)下载对应版本的工具包，将工具放在MogDB安装路径的bin目录下即可。如下图所示，toolkits-xxxxxx.tar.gz即为包含mog_xlogdump的工具包。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/open-source-components-3.png)

<br/>

## 使用说明

mog_xlogdump是用于解析并显示MogDB 2.1及之后版本的Wal日志的工具。旨在帮助DBA分析与Debugging数据库问题的辅助工具。

mog_xlogdump解析工具暂时不支持列存表。（研究发现列存表会在cstore模式下生成两张对应的CUDesc和delta表，CUDesc是列存表的元数据，delta是列存表的临时表，是张行存表，这两张表（CUDesc，delta）都会写在wal日志中，虽然wal日志能解析出对应的delta表，但该表受表属性阈值deltarow_threshold控制 ，默认100，即小于100条数据才会写在delta表中，大于阈值就会直接写到cu文件里）

> 注意：列存表要写delta表，需要postgres.conf中开启参数enable_delta_store = on。

<br/>

## 使用方式

```
mog_xlogdump [OPTION]... [STARTSEG [ENDSEG]]
```

<br/>

## 选项说明

- -b, --bkp-details

  输出文件块的详细信息。（默认显示block的id、rel、fork、blk、lastlsn，该参数会显示Block Image）

- -B, --bytea_output

  指定bytea类型解码输出的显示格式，有二进制和字符两种格式

- -c --connectinfo

  指定链接字符串URL，例如`postgres://user:password@ip:port/dbname`

- -e, --end=RECPTR

  指定解析wal日志的结束位置，LSN号

- -f, --follow

  表示当指定的wal日志解析到结尾时，继续往下一个文件解析

- -n, --limit=N

  指定输出数据记录的条数

- -o, --oid=OID

  指定反解码表的OID

- -p, --path=PATH

  指定wal日志存放目录

- -R, --Rel=Relation

  指定反解码表的数据类型

- -r, --rmgr=RMGR

  只显示资源管理器生成的记录内容

- -s, --start=RECPTR

  指定解析wal日志的开始位置，LSN号

- -T, --CTimeZone_ENV

  指定时域，默认的是UTC。

- -t, --timeline=TLI

  指定开始读取wal日志的timeline

- -V, --version

  显示版本号

- -w, --write-FPW

  显示全页写的信息，配合-b使用

- -x, --xid=XID

  输出指定事务id的记录

- -z, --stats

  输出插入记录的统计

- -v, --verbose

  显示细节信息

- -?, --help

  显示帮助信息并退出

<br/>

## 用例一

### 使用场景

当主库宕机且无法恢复，备库可以正常链接，此时主库末发的wal日志里面可能包含有上万张表数据的操作，而mog_xlogdump工具需要根据指定的-s、-e（起始和结束的lsn位置），把所有表的数据操作都解析出来。

### 使用方法

```
mog_xlogdump -c '备库链接串' -s '起始lsn' -e '结束lsn 主库wal日志文件
```

### 解析设置

注：主要是为了让wal日志中记录旧数据，即update操作修改前的数据元组，和delete操作删除的数据。

1. 把数据库配置文件postgres.conf里的**wal_level**设置为**logical**级别。
2. 对表进行修改: `alter table table_name replica identity full;`

### 结果展示

以json格式输出wal日志数据解析结果。tuple展示格式为 `’列名’:’数据’`

```json
{'table_name':'xxx','schema_name':'yyy','action':'insert','tuple':{'name':'xx','id':'ss'}}
```

### 示例

![fe1b12d080accfb9e54f857e79baebc](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/mog_xlogdump-1.png)

红框中是根据解析设置后才会解析出的旧数据，如果没有设置则解析不到update和delete的旧数据。

其中-c后面的备机链接URL为`postgres://test:Test123456@172.16.0.44:5003/postgres`

- postgres://为链接串标记头部

- test为链接用户名

- Test123456为链接用户的密码

- 172.16.0.44为备机ip地址

- 5003为备机链接端口

- postgres为链接备机的数据库名

- db_p/pg_xlog/000000010000000000000004为主机的wal日志文件

 <br/>

## 用例二

### 使用场景

当主库宕机且无法恢复，且备库可以正常链接，此时用户可能只关注库中的几张表（个别表），mog_xlogdump工具可以根据参数-o、-R来解析指定oid的表数据。比如-o指定表的oid，-R指定表的字段类型。

### 使用方法

创建表，写入数据并修改，用mog_xlogdump工具反解析Wal日志。

```sql
create table t2(id int, money money,inet inet,bool bool,numeric numeric ,text text);
insert into t2 values(1, 24.241, '192.168.255.132', true, 3.1415926, 'ljfsodfo29892ifj');
insert into t2 values(2, 928.8271, '10.255.132.101', false, 3.1415926, 'vzvzcxwf2424@');
update t2 set id=111, money=982.371 where id =2;
delete from t2 where id=1;

postgres=# select * from t2;
 id | money |   inet    | bool | numeric |    text
----+---------+-----------------+------+-----------+------------------
 1 | $24.24 | 192.168.255.132 | t  | 3.1415926 | ljfsodfo29892ifj
 2 | $928.83 | 10.255.132.101 | f  | 3.1415926 | vzvzcxwf2424@
(2 rows)

postgres=# update t2 set id=111, money=982.371 where id =2;
Postgres=# delete from t2 where id=1;
postgres=# select * from t2;
 id |  money  |   inet   | bool | numeric |   text
-----+-------------+----------------+------+-----------+---------------
 111 | $982,371.00 | 10.255.132.101 | f  | 3.1415926 | vzvzcxwf2424@

(1 rows)
```

### 解析设置

与用例一相同，设置wal_level和alter table等两个设置。

### 结果展示

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/mog_xlogdump-2.png)

```json
./mog_xlogdump -o 16394 -R int,money,inet,bool,numeric,text ./db_p/pg_xlog/000000010000000000000004
'insert','tuple':{'(null)':'1','(null)':'$24.24','(null)':'192.168.255.132','(null)':true,'(null)':'3.1415926','(null)':'ljfsodfo29892ifj'}}
'insert','tuple':{'(null)':'2','(null)':'$928.83','(null)':'10.255.132.101','(null)':false,'(null)':'3.1415926','(null)':'vzvzcxwf2424@'}}
'update','old_tuple':{'(null)':'2','(null)':'$928.83','(null)':'10.255.132.101','(null)':false,'(null)':'3.1415926','(null)':'vzvzcxwf2424@'},'new_tuple':{'(null)':'111','(null)':'$982,371.00','(null)':'10.255.132.101','(null)':false,'(null)':'3.1415926','(null)':'vzvzcxwf2424@'}}
'delete','tuple':{'(null)':'1','(null)':'$24.24','(null)':'192.168.255.132','(null)':true,'(null)':'3.1415926','(null)':'ljfsodfo29892ifj'}}
```

> 注意：由于输出格式的更改，表名、模式名和列名是依据-c链接字符串去备机上查询获取，但由于原来的-o、-R指定表oid和字段类型的方式是完全离线式，所以无法获取表名、模式名、列名这些信息，所以再用-o、-R来离线解析，不显示表名、模式名，列名显示为null。

```
mog_xlogdump -o 表OID -R 表的数据类型 -s 解析起始的LSN -e 解析结束的LSN Wal日志文件
```

该工具也保留了pg_xlogdump原始的功能。

<br/>

## 用例三

### 使用场景

当用户想看wal日志头部数据内容时，或者统计wal日志一些相关信息，用mog_xlogdump原始功能。

### 使用方法

使用一（头部信息）：

```
./mog_xlogdump -n 10 wal日志文件
```

-n 10 表示只显示10条数据。

使用二（统计信息）：

```
./mog_xlogdump -z wal日志文件
```

### 结果展示

结果一

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/mog_xlogdump-3.png)

结果二

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/reference-guide/mog_xlogdump-4.png)
