---
title: MogDB 3.0.0
summary: MogDB 3.0.0
author: Guo Huan
date: 2022-05-30
---

# MogDB 3.0.0

## 1. 版本说明

MogDB 3.0.0版本于2022年6月30日发布。3.0.0版本基于2.1版本进一步增强，并合入了openGauss 3.0.0版本的新增特性。

<br/>

## 2. 新增特性

### 2.1 集成openGauss 3.0.0版本新特性

- [行存转向量化](../../characteristic-description/high-performance/12-row-store-execution-to-vectorized-execution.md)
- [延迟进入最大可用模式](../../characteristic-description/high-availability/11-delaying-entering-the-maximum-availability-mode.md)
- [并行逻辑解码](../../characteristic-description/high-availability/12-parallel-logical-decoding.md)
- [CM（Cluster Manager）](../../reference-guide/tool-reference/unified-database-management-tool.md)
- [Global Syscache](../../characteristic-description/high-availability/15-global-syscache.md)
- [发布订阅](../../characteristic-description/enterprise-level-features/16-publication-subscription.md)
- [外键锁增强](../../characteristic-description/enterprise-level-features/17-foreign-key-lock-enhancement.md)
- [支持ANY权限管理](../../reference-guide/sql-syntax/GRANT.md)
- [DBMind组件化](../../AI-features/ai4db/ai4db-autonomous-database-o&m.md)
- [库内AI算法支持XGBoost、multiclass和PCA](../../AI-features/db4ai/native-db4ai-engine.md)
- [支持使用中间件ShardingSphere构建分布式数据库](../../characteristic-description/middleware/distributed-database-capability.md)

### 2.2 自治事务异步提交

- 将事务执行和事务日志落盘拆分为由两个不同线程执行，避免事务等待阶段Session worker线程闲置，进一步提高了worker线程的利用率，在保障数据完整性的基础上进一步提升了数据库性能。
- 自治事务异步提交可以使数据库整体性能提升20%~50%，TPCC压测下tpmC稳定提升36%。

**相关页面**：[事务异步提交](../../characteristic-description/enterprise-level-features/19-transaction-async-submit.md)

### 2.3 索引创建并行控制

- 在创建索引时，可以指定参数控制并行度，使索引创建更加高效灵活。

**相关页面**：[索引创建并行控制](../../characteristic-description/enterprise-level-features/23-index-creation-parallel-control.md)

### 2.4 动态分区裁剪

- 在静态分区裁剪基础上，增加动态分区裁剪功能。
- 能够针对绑定变量查询，子查询以及NestLoop参数化查询进行分区裁剪，进一步提升分区查询性能。

**相关页面**：[动态分区裁剪](../../characteristic-description/enterprise-level-features/21-dynamic-partition-pruning.md)

### 2.5 Copy导入优化

- 利用CPU SSE4.2指令集，在COPY导入阶段利用SIMD特性提升COPY导入的性能。

**相关页面**：[COPY导入优化](../../characteristic-description/enterprise-level-features/20-copy-import-optimization.md)

### 2.6 SQL运行状态观测

- 对Session级别SQL运行状态进行收集执行计划树并动态采样执行算子。
- 显著提升用户在进行慢SQL查询性能问题定位时的效率。

**相关页面**：[SQL运行状态观测](../../characteristic-description/enterprise-level-features/22-sql-running-status-observation.md)

### 2.7 Brin Index

- 数据块范围索引，相比于精准的BTREE索引，BRIN INDEX提供了一个以较小空间消耗获得一个相对较快查询速度的平衡。该索引适用于数据与物理位置相关的列，具有创建索引快，占用空间小以及顺序扫描较快等优势。
- 该特性兼容PostgreSQL 。

**相关页面**：[BRIN索引](../../characteristic-description/enterprise-level-features/24-brin-index.md)

### 2.8 Bloom Index

- Bloom索引是基于Bloom Filter实现的一种索引结构。适用于表中有很多列，并且查询可能使用任意列组合的场景。对于传统的索引，如 B+-tree 在这种场景下可能需要建很多个索引来覆盖可能的查询条件，导致索引占用空间很大，且影响插入和更新的性能， 而这种场景只需要建单个Bloom 索引就可以应对。
- 该特性兼容PostgreSQL。

**相关页面**：[BLOOM索引](../../characteristic-description/enterprise-level-features/25-bloom-index.md)

### 2.9 PostGIS

PostGis是一个空间数据库系统，能够提供空间对象、空间索引、空间操作函数和空间操作符等空间信息服务功能，可用于： 

- 大气科学、海洋科学、地质学、环境科学、交通管理等，处理和分析复杂的空间数据，并进行制图； 
- 移动通信、移动定位、移动互联网； 
- 城市管理、灾害响应、资源开发等方面；

**相关页面**：[PostGIS](../../reference-guide/oracle-plugins/postgis-extension/postgis-overview.md)

### 2.10 Oracle兼容增强

通过集成whale插件，兼容以下Oracle函数和Package: 

**函数** 

- INSTRB
- NLS_CHARSET_ID
- NLS_CHARSET_NAME
- NLS_LOWER
- NLS_UPPER
- ORA_HASH
- REMAINDER
- REPLACE
- SHOW
- SHOW_PARAMETER
- TO_TIMESTAMP
- TO_YMINTERVAL
- TZ_OFFSET
- NULLIF
- Ratio_to_report 

**Package** 

- dbms_lock
- dbms_job
- dbms_utility
- dbms_random
- dbms_output
- dbms_application_info
- dbms_metadata

**相关页面**：[Oracle兼容增强](../../characteristic-description/application-development-interfaces/MogDB-Oracle-compatibility.md)

### 2.11 MySQL兼容增强

通过集成dolphin插件，兼容以下MySQL语法和函数等：

- Timestamp On Update
- SQL Mode（Strice Mode， Full group by）
- 用户锁
- Insert函数 

**相关页面**：[MySQL兼容增强](../../characteristic-description/application-development-interfaces/MogDB-MySQL-compatibility.md)

### 2.12 OM故障诊断能力增强

通过增加收集系统信息，参数及测试结果差异化比较以及增加调试信息等过程增强故障诊断能力，进而帮助用户或调试人员更有效地进行问题定位。

该功能通过以下的工具增强来达成：

- gstrace增强：通过增加模块切换（component switch）来获得更有针对性的执行路径，用于提升debug效率。
- gs_check增强：原有的场景检查基础上，实现检测结果保存，以及对不同时间做的两个检测结果进行差异比较。
- gs_watch：当MogDB发生故障时，使用此工具收集OS信息、日志信息以及配置文件等信息，来定位问题。
- gs_gucquery：实现MogDB GUC值自动收集整理导出和差异比较。

**相关页面**：[故障诊断](../../characteristic-description/maintainability/fault-diagnosis.md)

<br/>

## 3. 修复缺陷

### 3.1 集成openGauss 3.0.0版本修复缺陷

- [I4VUXG](https://gitee.com/opengauss/openGauss-server/issues/I4VUXG?from=project-issue)  修复unlogged table 数据丢失问题
- [I4SF5P](https://gitee.com/opengauss/openGauss-server/issues/I4SF5P?from=project-issue)  release版本编译安装数据库，且dblink模块编译安装后，create extension dblink导致数据库core
- [I4S74D](https://gitee.com/opengauss/openGauss-server/issues/I4S74D?from=project-issue)  使用Jmeter工具向行存压缩表插入数据，数据量1G以上时必现失败（5/5），compresstype=2
- [I4N81J](https://gitee.com/opengauss/openGauss-server/issues/I4N81J?from=project-issue)  update/delete操作无法同步到订阅端
- [I4YPJQ](https://gitee.com/opengauss/openGauss-server/issues/I4YPJQ?from=project-issue)  Inserting varchar constant into MOT table using JDBC fails
- [I4PF6G](https://gitee.com/opengauss/openGauss-server/issues/I4PF6G?from=project-issue)  外键锁增强-2.0.0.灰度升级至2.2.0不提交，执行tpcc失败
- [I4WPD1](https://gitee.com/opengauss/openGauss-server/issues/I4WPD1?from=project-issue)  简化安装模块获取安装包后解压openGauss-2.1.0-CentOS-64bit.tar.bz2缺少simpleinstall目录，无法执行极简安装
- [I4L268](https://gitee.com/opengauss/openGauss-server/issues/I4L268?from=project-issue)  分区表多次truncate后，再进行vacuum freeze pg\_partition，系统表pg\_partition索引不准确
- [I3HZJN](https://gitee.com/opengauss/openGauss-server/issues/I3HZJN?from=project-issue)  copy命令DATE\_FORMAT缺少时分秒时，未按格式复制
- [I4HUXD](https://gitee.com/opengauss/openGauss-server/issues/I4HUXD?from=project-issue)  jsonb类型查询报错
- [I4QDN9](https://gitee.com/opengauss/openGauss-server/issues/I4QDN9?from=project-issue)  select 1.79E +308\*2,cume\_dist\(\) over\(order by 1.0E128\*1.2\)返回超出范围
- [I4PAVO](https://gitee.com/opengauss/openGauss-server/issues/I4PAVO?from=project-issue)  start with connect by record子查询识别失败
- [I4UY9A](https://gitee.com/opengauss/openGauss-server/issues/I4UY9A?from=project-issue)  opengauss列表分区创建default分区失败
- [I4W3UB](https://gitee.com/opengauss/openGauss-server/issues/I4W3UB?from=project-issue)  创建并使用自定义类型创建视图，重命名该自定义类型后，无法获取视图定义
- [I4WRMX](https://gitee.com/opengauss/openGauss-server/issues/I4WRMX?from=project-issue)  重启数据库且enable\_stmt\_track参数关闭时，查询statement\_history表记录应该无记录，实际有记录，statement\_history表的数据未清空
- [I4WOBH](https://gitee.com/opengauss/openGauss-server/issues/I4WOBH?from=project-issue)  GUC设置pagewriter\_sleep为360000后恢复默认值2000，重启库失败

### 3.2 MogDB 3.0.0版本修复缺陷

- update view在某些场景下可能导致数据库 coredump
- 二级分区表创建local索引表在某些场景下可能导致数据库coredump
- connect by语句在某些场景下可能导致数据库coredump 

<br/>

## 4. 兼容性

本版本支持以下操作系统及CPU架构组合：

| 操作系统              | CPU架构                                       |
| --------------------- | --------------------------------------------- |
| CentOS 7.x            | X86_64（Intel，AMD，海光，兆芯）              |
| Redhat 7.x            | X86_64（Intel，AMD，海光，兆芯）              |
| openEuler 20.03LTS    | ARM（鲲鹏）、X86_64（Intel，AMD，海光，兆芯） |
| 银河麒麟V10           | ARM（鲲鹏）、X86_64（Intel，AMD，海光，兆芯） |
| 统信UOS V20-D / V20-E | ARM（鲲鹏）、X86_64（Intel，AMD，海光，兆芯） |
| 统信UOS V20-A         | X86_64（Intel，AMD，海光，兆芯）              |

## 5. 已知问题

在分区表使用PBE场景下，动态分区裁剪小概率可能会导致core dump，规避方案为暂时禁用resource track，也即设置enable_resource_track=off。该参数已经纳入推荐参数设置。