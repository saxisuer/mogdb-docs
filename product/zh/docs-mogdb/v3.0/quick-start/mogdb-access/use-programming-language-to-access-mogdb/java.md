---
title: 使用Java访问MogDB
summary: 使用Java访问MogDB
author: Guo Huan
date: 2022-03-25
---

# 使用Java访问MogDB

JDBC（Java Database Connectivity，Java数据库连接）是一种用于执行SQL语句的Java API，可以为多种关系数据库提供统一访问接口，应用程序可基于它操作数据。MogDB数据库提供了对JDBC 4.0特性的支持。

关于使用Java访问MogDB数据库的详细说明请参考《开发者指南》中[基于JDBC开发](../../../developer-guide/dev/2-development-based-on-jdbc/1-development-based-on-jdbc-overview.md)章节的内容。

驱动下载：[openGauss社区下载页-openGauss Connectors-JDBC_*.*.*](https://opengauss.org/zh/download/)
