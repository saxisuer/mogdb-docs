---
title: pgcli
summary: pgcli
author: Guo Huan
date: 2022-02-16
---

# pgcli

pgcli是一个具有自动补全以及高亮显示语法功能的Postgres命令行工具，同样适用于连接MogDB。

pgcli Github页面：[https://github.com/dbcli/pgcli](https://github.com/dbcli/pgcli)

<br/>

## 安装pgcli

```
sudo pip3 install pgcli
```

适用于不同操作系统的详细安装步骤请参考pgcli官网页面：[http://pgcli.com](http://pgcli.com/)

安装完成后可以使用help命令查看帮助信息。

```
[root@mogdb-kernel-0004 ~]# pgcli --help
Usage: pgcli [OPTIONS] [DBNAME] [USERNAME]

Options:
  -h, --host TEXT            Host address of the postgres database.
  -p, --port INTEGER         Port number at which the postgres instance is
                             listening.
  -U, --username TEXT        Username to connect to the postgres database.
  -u, --user TEXT            Username to connect to the postgres database.
  -W, --password             Force password prompt.
  -w, --no-password          Never prompt for password.
  --single-connection        Do not use a separate connection for completions.
  -v, --version              Version of pgcli.
  -d, --dbname TEXT          database name to connect to.
  --pgclirc FILE             Location of pgclirc file.
  -D, --dsn TEXT             Use DSN configured into the [alias_dsn] section
                             of pgclirc file.
  --list-dsn                 list of DSN configured into the [alias_dsn]
                             section of pgclirc file.
  --row-limit INTEGER        Set threshold for row limit prompt. Use 0 to
                             disable prompt.
  --less-chatty              Skip intro on startup and goodbye on exit.
  --prompt TEXT              Prompt format (Default: "\u@\h:\d> ").
  --prompt-dsn TEXT          Prompt format for connections using DSN aliases
                             (Default: "\u@\h:\d> ").
  -l, --list                 list available databases, then exit.
  --auto-vertical-output     Automatically switch to vertical output mode if
                             the result is wider than the terminal width.
  --warn [all|moderate|off]  Warn before running a destructive query.
  --help                     Show this message and exit.
```

<br/>

## 连接MogDB示例

通过`su - omm`命令切换到omm用户，然后执行`pgcli -d postgres -p 26000`即可连接MogDB数据库。

```
[root@mogdb-kernel-0004 ~]# su - omm
Last login: Wed Feb 16 15:31:37 CST 2022 on pts/1
[omm@mogdb-kernel-0004 ~]$ pgcli -d postgres -p 26000
Server: PostgreSQL 9.2.4
Version: 3.3.1
Home: http://pgcli.com
postgres> \l
+-----------+---------+----------+-------------+-------------+-------------------+
| Name      | Owner   | Encoding | Collate     | Ctype       | Access privileges |
|-----------+---------+----------+-------------+-------------+-------------------|
| postgres  | omm     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | <null>            |
| template0 | omm     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm            |
|           |         |          |             |             | omm=CTc/omm       |
| template1 | omm     | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm            |
|           |         |          |             |             | omm=CTc/omm       |
+-----------+---------+----------+-------------+-------------+-------------------+
SELECT 3
Time: 0.020s
postgres>
```