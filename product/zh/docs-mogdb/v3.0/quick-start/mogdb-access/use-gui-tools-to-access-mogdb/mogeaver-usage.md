---
title: 使用Mogeaver访问MogDB
summary: 使用Mogeaver访问MogDB
author: Guo Huan
date: 2022-06-27
---

# 使用Mogeaver访问MogDB

本文档介绍如何通过图形化工具Mogeaver访问MogDB数据库。

<br/>

## 工具介绍

Mogeaver [moˈgi:və(r) ] 基于流行的开源图形化工具[DBeaver](https://dbeaver.io/)，在严格遵循DBeaver Community Edition 的[ASL](https://dbeaver.io/product/dbeaver_license.txt)开源协议基础上，进行了二次开发和封装，支持对于MogDB数据库的图形化开发及管理，支持通过图形化的方式创建、修改、调试数据库内的存储过程、自定义函数及包。

<br/>

## 下载安装

[发布记录](../../../mogeaver/mogeaver-release-notes.md)

<br/>

> MacOS 如果遇到已损坏,请执行
>
> sudo xattr -r -d com.apple.quarantine /Applications/Mogeaver.app

## 配置连接串

以下步骤以Windows版本为例。

打开mogeaver程序后，点击“**数据库 -> 新建数据库连接**”。

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-2.png)

在弹出的窗口中选择MogDB数据库，并点击“**Next**”。

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-1.png)

填写要连接的数据库信息，然后点击“**Finish**”即可建立数据库连接。

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-3.png)

<br/>

## 界面展示

连接成功后的界面如下所示。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-6.png)

<br/>

## 后续步骤

如需了解更多使用说明，请点击“**帮助 -> Help Contents**”，或点击以下页面在线浏览官方文档：[https://dbeaver.com/docs/wiki/](https://dbeaver.com/docs/wiki/)。

 ![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/quick-start/mogeaver-5.png)
