<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 3.0

## 故障诊断

+ [常见故障定位手段](/common-faults-and-identification/common-fault-locating-methods.md)
+ 常见故障定位案例
  + core问题定位
    + [磁盘满故障引起的core问题](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/1-core-dump-occurs-due-to-full-disk-space.md)
    + [GUC参数log_directory设置不正确引起的core问题](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/2-core-dump-occurs-due-to-incorrect-settings-of-guc-parameter-log-directory.md)
    + [开启RemoveIPC引起的core问题](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/3-core-dump-occurs-when-removeipc-is-enabled.md)
    + [x86下安装完成后发生coredump](/common-faults-and-identification/common-fault-locating-cases/1-core-fault-locating/4-core-dump-occurs-after-installation-on-x86.md)
  + 权限/会话/数据类型问题定位
    + [强制结束指定的问题会话](/common-faults-and-identification/common-fault-locating-cases/13-forcibly-terminating-a-session.md)
    + [不同用户查询同表显示数据不同](/common-faults-and-identification/common-fault-locating-cases/19-different-data-is-displayed.md)
    + [业务运行时整数转换错](/common-faults-and-identification/common-fault-locating-cases/22-an-error-occurs-during-integer-conversion.md)
  + 服务/高可用/并发问题定位
    + [备机处于need repair(WAL)状态问题](/common-faults-and-identification/common-fault-locating-cases/3-standby-node-in-the-need-repair-state.md)
    + [服务启动失败](/common-faults-and-identification/common-fault-locating-cases/5-service-startup-failure.md)
    + [switchover操作时，主机降备卡住](/common-faults-and-identification/common-fault-locating-cases/9-primary-node-is-hung-in-demoting.md)
    + [高并发报错”too many clients already”或无法创建线程](/common-faults-and-identification/common-fault-locating-cases/23-too-many-clients-already.md)
  + 表/分区表问题定位
    + [VACUUM FULL一张表后，表文件大小无变化](/common-faults-and-identification/common-fault-locating-cases/17-table-size-does-not-change.md)
    + [执行修改表分区操作时报错](/common-faults-and-identification/common-fault-locating-cases/18-an-error-is-reported-when-the-table-partition-is-modified.md)
  + 文件系统/磁盘/内存问题定位
    + [在XFS文件系统中，使用du命令查询数据文件大小大于文件实际大小](/common-faults-and-identification/common-fault-locating-cases/7-after-you-run-the-du-command.md)
    + [在XFS文件系统中，出现文件损坏](/common-faults-and-identification/common-fault-locating-cases/8-file-is-damaged-in-the-xfs-file-system.md)
    + [内存不足问题](/common-faults-and-identification/common-fault-locating-cases/4-insufficient-memory.md)
    + [出现“Error:No space left on device”提示](/common-faults-and-identification/common-fault-locating-cases/6-error-no-space-left-on-device-is-displayed.md)
    + [TPCC运行时，注入磁盘满故障，TPCC卡住的问题](/common-faults-and-identification/common-fault-locating-cases/2-when-the-tpcc-is-running.md)
    + [磁盘空间达到阈值，数据库只读](/common-faults-and-identification/common-fault-locating-cases/10-disk-space-usage-reaches-the-threshold.md)
  + SQL问题定位
    + [执行 SQL 语句时，提示 Lock wait timeout](/common-faults-and-identification/common-fault-locating-cases/16-lock-wait-timeout-is-displayed.md)
    + [分析查询语句是否被阻塞](/common-faults-and-identification/common-fault-locating-cases/14-analyzing-whether-a-query-statement-is-blocked.md)
    + [分析查询效率异常降低的问题](/common-faults-and-identification/common-fault-locating-cases/15-low-query-efficiency.md)
    + [分析查询语句长时间运行的问题](/common-faults-and-identification/common-fault-locating-cases/11-slow-response-to-a-query-statement.md)
    + [分析查询语句运行状态](/common-faults-and-identification/common-fault-locating-cases/12-analyzing-the-status-of-a-query-statement.md)
  + 索引问题定位
    + [修改索引时只调用索引名提示索引不存在](/common-faults-and-identification/common-fault-locating-cases/20-when-a-user-specifies-only-an-index-name.md)
    + [重建索引失败](/common-faults-and-identification/common-fault-locating-cases/21-reindexing-fails.md)
    + [btree 索引故障情况下应对策略](/common-faults-and-identification/common-fault-locating-cases/24-b-tree-index-faults.md)