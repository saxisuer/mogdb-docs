---
title: 分布式数据库能力
summary: 分布式数据库能力
author: Guo Huan
date: 2022-05-10
---

# 分布式数据库能力

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

基于分布式中间件shardingsphere使MogDB具备分布式数据库能力。使用16个鲲鹏920（128核）节点组网（1\*shardingsphere-proxy、7\*shardingsphere-jdbc、8\*MogDB）时，完美sharding性能>1000万tpmc。

## 客户价值

通过中间件构建逻辑上无资源限制的分布式数据库。

## 特性描述

通过shardingsphere中间件的分库分表能力，使多个MogDB数据库可以在逻辑上组成一个更大的数据库，同时具备分布式事务和弹性伸缩的能力，使用方式与MogDB数据库并无不同。

## 特性增强

无。

## 特性约束

无。

## 依赖关系

shardingsphere中间件。