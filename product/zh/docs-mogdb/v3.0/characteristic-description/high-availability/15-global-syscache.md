---
title: 支持global syscache
summary: 支持global syscache
author: Guo Huan
date: 2022-05-10
---

# 支持global syscache

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

全局系统缓存（Global SysCache）是系统表数据的全局缓存和本地缓存。原理如图1所示。

**图 1** Global SysCache原理图

 ![Global-SysCache原理图](https://cdn-mogdb.enmotech.com/docs-media/mogdb/characteristic-description/Global-SysCache.png)

## 客户价值

全局系统缓存特性可以降低数据库进程的缓存内存占用，提升数据库的并发扩展能力。

## 特性描述

全局系统缓存特性指将系统缓存与会话解耦，绑定到线程上，结合线程池特性达到降低内存占用的目的，同时结合全局缓存，提升缓存命中率，保持性能稳定。

## 特性增强

支持更高的并发查询。

## 特性约束

- 设置enable_global_syscache为on。建议设置enable_thread_pool参数为on。
- 当DB数较多，且阈值global_syscache_threshold较小时，内存控制无法正常工作，性能会劣化。
- 不支持分布式时序相关的任务，这些任务的内存控制与性能不受GSC特性的影响。
- wal_level设置为minimal或者archive时，备机的查询性能会下降，会退化为短连接。

## 依赖关系

该特性降内存能力依赖于线程池特性。