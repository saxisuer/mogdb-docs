---
title: 支持备机build备机
summary: 支持备机build备机
author: Guo Huan
date: 2022-05-10
---

# 支持备机build备机

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

备机build备机加快备机故障的恢复。 减小主机io和带宽压力。

## 客户价值

当业务压力过大时，从主机build备机会对主机的资源造成影响，导致主机性能下降、build变慢的情况。使用备机build备机不会对主机业务造成影响。

## 特性描述

使用gs_ctl命令可以指定对应的备机去build需要修复的备机。具体操作可参考《参考指南》中的“[gs_ctl](../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md)”章节。

## 特性增强

无。

## 特性约束

只支持备机build备机，只能使用指定ip和port的方式做build，同时在build前应确保需要修复备机的日志比发送数据的备机的日志落后。

## 依赖关系

无。