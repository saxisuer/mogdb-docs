---
title: 全文索引
summary: 全文索引
author: Guo Huan
date: 2022-05-07
---

# 全文索引

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

MogDB中提供的全文索引功能可以对文档进行预处理，并且可以使后续的搜索更快速。

## 客户价值

MogDB全文索引功能提供了查询可读性文档的能力，并且通过查询相关度将结果进行排序。

## 特性描述

构建全文索引的预处理过程包括：

- 将文档解析成token。

  为每个文档标记不同类别的token是非常有必要的，例如：数字、文字、复合词、电子邮件地址，这样就可以做不同的处理。原则上token的类别依赖于具体的应用，但对于大多数的应用来说，可以使用一组预定义的token类。

- 将token转换为词素。

  词素像token一样是一个字符串，但它已经标准化处理，这样同一个词的不同形式是一样的。例如，标准化通常包括：将大写字母折成小写字母、删除后缀（如英语中的s或者es）。这将允许通过搜索找到同一个词的不同形式，不需要繁琐地输入所有可能的变形样式。同时，这一步通常会删除停用词。这些停用词通常因为太常见而对搜索无用。（总之，token是文档文本的原片段，而词素被认为是有用的索引和搜索词。）MogDB使用词典执行这一步，且提供了各种标准的词典。

- 保存搜索优化后的预处理文档。

  比如，每个文档可以呈现为标准化词素的有序组合。伴随词素，通常还需要存储词素位置信息以用于邻近排序。因此文档包含的查询词越密集其排序越高。词典能够对token如何标准化做到细粒度控制。使用合适的词典，可以定义不被索引的停用词。

## 特性增强

无。

## 特性约束

MogDB的全文检索功能当前限制约束是：

- 每个分词长度必须小于2KB。
- tsvector结构（分词+位置）的长度必须小于1MB。
- tsvector的位置值必须大于0，且小于等于16,383。
- 每个分词在文档中位置数必须小于256，若超过将舍弃后面的位置信息。

## 依赖关系

无。