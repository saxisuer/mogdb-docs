---
title: 全局临时表
summary: 全局临时表
author: Guo Huan
date: 2022-05-07
---

# 全局临时表

## 可获得性

本特性自MogDB 1.1.0 版本开始引入。

## 特性简介

临时表顾名思义是不保证持久化的表，其生命周期一般跟session或者事务绑定，可以方便用于表达处理过程中的一些临时数据存放，加速查询。

## 客户价值

提升临时表的表达能力和易用性。

## 特性描述

全局临时表的元数据对所有会话可见，会话结束后元数据继续存在。会话与会话之间的用户数据、索引和统计信息相互隔离，每个会话只能看到和更改自己提交的数据。

全局临时表有两种模式：一种是基于会话级别的（ON COMMIT PRESERVE ROWS），当会话结束时自动清空用户数据；一种是基于事务级别的（ON COMMIT DELETE ROWS）, 当执行commit或rollback时自动清空用户数据。建表时如果没有指定ON COMMIT选项，则缺省为会话级别。与本地临时表不同，全局临时表建表时可以指定非pg_temp_开头的schema。

## 特性增强

在本地临时表的基础上增加了全局临时表的处理。

## 特性约束

- 不支持并行扫描
- 不支持temp tablespace
- 不支持partition
- 不支持GIST索引
- 不支持User-defined统计信息pg_statistic_ext
- 不支持ON COMMIT DROP
- 不支持hash bucket 聚簇存储
- 不支持列存

## 依赖关系

无。