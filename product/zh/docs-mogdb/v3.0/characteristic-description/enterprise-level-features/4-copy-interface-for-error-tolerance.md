---
title: Copy接口支持容错机制
summary: Copy接口支持容错机制
author: Guo Huan
date: 2022-05-07
---

# Copy接口支持容错机制

## 可获得性

本特性自MogDB 1.1.0版本开始引入。

## 特性简介

支持将Copy过程中的部分错误导入到指定的错误表中，并且保持Copy过程不被中断。

## 客户价值

提升Copy功能的可用性和易用性，提升对于源数据格式异常等常见错误的容忍性和鲁棒性。

## 特性描述

MogDB提供用于创建函数的封装好的Copy错误表，并允许用户在使用Copy From指令时指定容错选项，使得Copy From语句在执行过程中部分解析、数据格式、字符集等相关的报错不会报错中断事务、而是被记录至错误表中，使得在Copy From的目标文件即使有少量数据错误也可以完成入库操作。用户随后可以在错误表中对相关的错误进行定位以及进一步排查。

## 特性增强

无。

## 特性约束

支持容错的具体错误种类请参见《管理指南》中“使用COPY FROM STDIN导入数据 > [处理错误表](../../administrator-guide/importing-and-exporting-data/importing-data/3-running-the-COPY-FROM-STDIN-statement-to-import-data.md#处理错误表)”章节。

## 依赖关系

无。