---
title: 动态分区裁剪
summary: 动态分区裁剪
author: Guo Huan
date: 2022-06-17
---

# 动态分区裁剪

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

本特性主要实现如下4个功能：

1. 支持NEST LOOP进行分区裁剪。

2. 支持绑定变量进行分区裁剪。

3. 支持子查询进行分区裁剪。

4. 支持通过EXPLAIN ANALYZE查看动态分区裁剪结果。

## 客户价值

本特性主要对分区裁剪特性进行优化，引入动态分区裁剪，同时支持通过EXPLAIN ANALYZE查看分区裁剪结果。在SQL执行阶段裁剪掉不需要的分区，再扫描分区表，从而提升分区表的查询性能。

## 相关页面

[静态分区裁剪](../../developer-guide/partition-management/partition-pruning/static-partition-pruning.md)、[动态分区裁剪](../../developer-guide/partition-management/partition-pruning/dynamic-partition-pruning.md)
