---
title: In-place Update存储引擎
summary: In-place Update存储引擎
author: Guo Huan
date: 2022-05-07
---

# In-place Update存储引擎

## 可获得性

本特性自MogDB 2.1.0 版本开始引入。

## 特性简介

In-place Update存储引擎（原地更新），是MogDB内核新增的一种存储模式。MogDB内核此前的版本使用的行存储引擎是Append Update（追加更新）模式。追加更新对于业务中的增、删以及HOT(HeapOnly Tuple) Update(即同一页面内更新)有很好的表现，但对于跨数据页面的非HOT UPDATE场景，垃圾回收不够高效，Ustore存储引擎可很好解决上述问题。

## 客户价值

In-place Update存储引擎可有效的降低多次更新元组后占用存储空间问题。

## 特性描述

新增的In-place update存储引擎很好的解决了Append update存储引擎空间膨胀，元组较大的劣势，高效回滚段的设计是In-place update存储引擎的基础。

## 特性增强

无。

## 特性约束

无。

## 依赖关系

无。