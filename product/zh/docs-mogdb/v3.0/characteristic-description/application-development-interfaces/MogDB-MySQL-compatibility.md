---
title: MySQL兼容增强
summary: MySQL兼容增强
author: Zhang Cuiping
date: 2022-06-21
---

# MySQL兼容增强

## 可获得性

本特性自MogDB 3.0.0版本开始引入。

## 特性简介

本特性主要从以下三方面增强MogDB与MySQL的兼容性。同时，支持兼容Insert语法，Insert into可以简写为insert。

支持用户锁，允许用户通过sql加自定义的锁，可以让多个程序之间完成加锁相关的交互过程，使得客户端从任何位置访问都可以得到一致性的锁视图。

支持建表插入数据时默认记录插入当前时间；更新数据时，如果未指定更新时间，默认显示数据变更的当前时间。

支持设置会话级SQL模式，允许运行时变更、全局变更以及会话内变更。

## 客户价值

通过设置用户锁，对数据、数据结构或者某些字符串进行保护，避免会话之间相互干扰，保证了信息的一致性和安全性。解决了用户业务数据写入与修改时，记录其操作时间戳的问题。通过设置sql模式，可以解决早期版本遗留问题与后期版本的兼容性。

## 相关页面

[CREATE TABLE](../../reference-guide/sql-syntax/CREATE-TABLE.md)， [ALTER TABLE](../../reference-guide/sql-syntax/ALTER-TABLE.md)， [INSERT](../../reference-guide/sql-syntax/INSERT.md)，[咨询锁函数](../../reference-guide/functions-and-operators/24-system-management-functions/7-advisory-lock-functions.md)，[SQL模式](../../reference-guide/guc-parameters/SQL-mode.md)，[dolphin插件](../../reference-guide/oracle-plugins/dolphin.md)