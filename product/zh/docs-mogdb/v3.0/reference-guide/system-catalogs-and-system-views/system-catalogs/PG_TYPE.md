---
title: PG_TYPE
summary: PG_TYPE
author: Guo Huan
date: 2021-04-19
---

# PG_TYPE

PG_TYPE系统表存储数据类型的相关信息。

**表 1** PG_TYPE字段

| 名称           | 类型         | 描述                                                         |
| :------------- | :----------- | :----------------------------------------------------------- |
| oid            | oid          | 行标识符（隐含属性，必须明确选择）。                         |
| typname        | name         | 数据类型名称。                                               |
| typnamespace   | oid          | 包含这个类型的名称空间的OID。                                |
| typowner       | oid          | 该类型的所有者。                                             |
| typlen         | smallint     | 对于定长类型是该类型内部表现形式的字节数目。对于变长类型是负数。<br/>-1表示一种"变长"（有长度字属性的数据）。<br/>-2表示这是一个NULL结尾的C字符串。 |
| typbyval       | boolean      | 指定内部传递这个类型的数值时是传值（该值为true）还是传引用（该值为false）。如果该类型的TYPLEN不是1，2，4，8， TYPBYVAL最好为false。变长类型通常是传引用。即使TYPLEN允许传值，TYPBYVAL也可以为false。 |
| typtype        | "char"       | - 对于基础类型是b。<br/>- 对于复合类型是c（比如，一个表的行类型）。<br/>- 对于域类型是d。<br/>- 对于伪类型是p。<br/>参见typrelid和typbasetype。 |
| typcategory    | "char"       | 是数据类型的模糊分类，可用于解析器做为数据转换的依据。       |
| typispreferred | boolean      | 如果为真，则数据符合TYPCATEGORY所指定的转换规则时进行转换。  |
| typisdefined   | boolean      | 如果定义了类型则为真，如果是一种尚未定义的类型的占位符则为假。如果为假，则除了该类型名称，名称空间和OID之外没有可靠的信息。 |
| typdelim       | "char"       | 当分析数组输入时，分隔两个此类型数值的字符请注意该分隔符是与数组元素数据类型相关联的，而不是和数组数据类型关联。 |
| typrelid       | oid          | 如果是复合类型（请参见typtype），则这个字段指向pg_class中定义该表的行。对于自由存在的复合类型，pg_class记录并不表示一个表，但是总需要它来查找该类型连接的pg_attribute记录。对于非复合类型为零。 |
| typelem        | oid          | 如果不为0，则它标识pg_type里面的另外一行。当前类型可以当做一个产生类型为typelem的数组来描述。一个"真正的"数组类型是变长的（typlen= -1），但是一些定长的（typlen &gt; 0）类型也拥有非零的typelem（比如name和point）。如果一个定长类型拥有一个typelem ，则他的内部形式必须是typelem数据类型的某个数目的个数值，不能有其他数据。变长数组类型有一个该数组子过程定义的头（文件）。 |
| typarray       | oid          | 如果不为0，则表示在pg_type中有对应的类型记录。               |
| typinput       | regproc      | 输入转换函数（文本格式）。                                   |
| typoutput      | regproc      | 输出转换函数（文本格式）。                                   |
| typreceive     | regproc      | 输入转换函数（二进制格式），如果没有则为0。                  |
| typsend        | regproc      | 输出转换函数（二进制格式），如果没有则为0。                  |
| typmodin       | regproc      | 输入类型修改符函数，如果为0，则不支持。                      |
| typmodout      | regproc      | 输出类型修改符函数，如果为0，则不支持。                      |
| typanalyze     | regproc      | 自定义的ANALYZE函数，如果使用标准函数，则为0。               |
| typalign       | "char"       | 当存储此类型的数值时要求的对齐性质。它应用于磁盘存储以及该值在PostgreSQL内部的大多数形式。如果数值是连续存放的，比如在磁盘上以完全的裸数据的形式存放时，则先在此类型的数据前填充空白，这样它就可以按照要求的界限存储。对齐引用是该序列中第一个数据的开头。可能的值包含: <br/>- c = char对齐，也就是不需要对齐。<br/>- s = short对齐（在大多数机器上是2字节）<br/>- i = int对齐（在大多数机器上是4字节）<br/>- d = double对齐（在大多数机器上是8字节，但不一定是全部）<br/>须知: <br/>对于在系统表里使用的类型，在pg_type里定义的尺寸和对齐必须和编译器在一个表示表的一行的结构里的布局一样。 |
| typstorage     | "char"       | 指明一个变长类型（那些有typlen = -1）是否准备好应付非常规值，以及对这种属性的类型的缺省策略是什么。可能的值包含: <br/>- p: 数值总是以简单方式存储。<br/>- e: 数值可以存储在一个"次要"关系中（如果该关系有这么一个，请参见pg_class.reltoastrelid）。<br/>- m: 数值可以以内联的压缩方式存储。<br/>- x: 数值可以以内联的压缩方式或者在"次要"表里存储。<br/>须知: <br/>m域也可以移到从属表里存储，但只是最后的解决方法（e和x域先移走）。 |
| typenotnull    | boolean      | 该类型是否存在NOTNULL约束。目前只用于域。                    |
| typbasetype    | oid          | 如果这是一个衍生类型（请参见typtype），则该标识作为这个类型的基础的类型。如果不是衍生类型则为零。 |
| typtypmod      | integer      | 域使用typtypmod记录要作用到它们的基础类型上的typmod（如果基础类型不使用typmod则为-1）。如果这种类型不是域，则为-1。 |
| typndims       | integer      | 如果一个域是数组，则typndims是数组维数的数值（也就是说，typbasetype是一个数组类型；域的typelem将匹配基本类型的typelem）。非域非数组域为零。 |
| typcollation   | oid          | 指定类型的排序规则。如果为0，则表示不支持排序。              |
| typdefaultbin  | pg_node_tree | 如果为非NULL，则它是该类型缺省表达式的nodeToString() 表现形式。目前这个字段只用于域。 |
| typdefault     | text         | 如果某类型没有相关缺省值，则取值是NULL。<br/>- 如果typdefaultbin为非NULL，则typdefault必须包含一个typdefaultbin代表的缺省表达式。<br/>- 如果typdefaultbin为NULL但typdefault不是，typdefault则是该类型缺省值的外部表现形式，可以把它作为该类型的输入，转换器生成一个常量。 |
| typacl         | aclitem[]    | 访问权限。                                                   |
