---
title: PG_REPLICATION_ORIGIN
summary: PG_REPLICATION_ORIGIN
author: Guo Huan
date: 2022-05-12
---

# PG_REPLICATION_ORIGIN

PG_REPLICATION_ORIGIN系统表包含所有已创建的复制源，该表为全局共享表，即在每个节点上只有一份pg_replication_origin，而不是每个数据库一份。

**表 1** PG_REPLICATION_ORIGIN字段

| 名称    | 类型 | 描述                               |
| :------ | :--- | :--------------------------------- |
| roident | oid  | 一个集群范围内唯一的复制源标识符。 |
| roname  | text | 外部的由用户定义的复制源名称。     |