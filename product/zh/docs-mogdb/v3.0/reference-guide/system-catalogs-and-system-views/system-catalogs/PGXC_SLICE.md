---
title: PGXC_SLICE
summary: PGXC_SLICE
author: Guo Huan
date: 2022-05-13
---

# PGXC_SLICE

PGXC_SLICE表是针对range范围分布和list分布创建的系统表，用来记录分布具体信息，当前不支持range interval自动扩展分片，不过在系统表中预留。

PGXC_SLICE系统表仅在分布式场景下有具体含义，MogDB只能查询表定义。

**表 1** PGXC_SLICE字段

| 名称            | 类型    | 描述                                                         |
| :-------------- | :------ | :----------------------------------------------------------- |
| relname         | name    | 表名或者分片名，通过type区分                                 |
| type            | char    | 当为’t’时relname是表名，当为’s’时relname是分片的名字         |
| strategy        | char    | ‘r’：为range分布表<br/>‘l’：为list分布表<br/>后续interval分片会扩展该值 |
| relid           | oid     | 该tuple隶属的分布表oid                                       |
| referenceoid    | oid     | 所参考分布表的oid,用于slice reference建表语法                |
| sindex          | int     | 当为list分布表时，表示当前boundary在某个分片内的位置         |
| interval        | text[]  | 预留字段                                                     |
| transitboundary | text[]  | 预留字段                                                     |
| transitno       | int     | 预留字段                                                     |
| nodeoid         | oid     | 当relname为分片名时，表示该分片的数据存放在哪个DN上，nodeoid表示这个DN的oid |
| boundaries      | text[]  | 当relname为分片名时，对应该分片的边界值                      |
| specified       | boolean | 当前分片对应的DN是否是用户在DDL中显示指定的                  |
| sliceorder      | int     | 用户定义分片的顺序                                           |