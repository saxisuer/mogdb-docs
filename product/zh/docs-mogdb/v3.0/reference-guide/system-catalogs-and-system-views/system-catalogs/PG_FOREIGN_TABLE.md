---
title: PG_FOREIGN_TABLE
summary: PG_FOREIGN_TABLE
author: Guo Huan
date: 2021-04-19
---

# PG_FOREIGN_TABLE

PG_FOREIGN_TABLE系统表存储外部表的辅助信息。

**表 1** PG_FOREIGN_TABLE字段

| 名称        | 类型    | 描述                                                         |
| :---------- | :------ | :----------------------------------------------------------- |
| ftrelid     | oid     | 外部表的ID。                                                 |
| ftserver    | oid     | 外部表的所在服务器。                                         |
| ftwriteonly | Boolean | 外部表是否可写。取值如下：<br />- t（true）：表示可写<br />- f（false）：表示不可写 |
| ftoptions   | text[]  | 外部表的可选项，具体参考CREATE FOREIGN TABLE语法说明。       |
