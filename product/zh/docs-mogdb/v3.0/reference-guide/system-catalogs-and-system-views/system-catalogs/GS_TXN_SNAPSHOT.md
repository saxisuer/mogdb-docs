---
title: GS_TXN_SNAPSHOT
summary: GS_TXN_SNAPSHOT
author: Zhang Cuiping
date: 2021-10-11
---

# GS_TXN_SNAPSHOT

GS_TXN_SNAPSHOT是“时间戳-CSN”映射表，周期性采样，并维护适当的时间范围，用于估算范围内的时间戳对应的CSN值。

**表 1** GS_TXN_SNAPSHOT字段

| 名称        | 类型        | 描述           |
| :---------- | :---------- | :------------- |
| snptime     | timestamptz | 快照捕获时间   |
| snpxmin     | bigint      | 快照xmin       |
| snpcsn      | bigint      | 快照csn        |
| snpsnapshot | TEXT        | 快照序列化文本 |