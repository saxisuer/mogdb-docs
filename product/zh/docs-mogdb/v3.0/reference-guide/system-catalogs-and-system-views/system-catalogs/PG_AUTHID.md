---
title: PG_AUTHID
summary: PG_AUTHID
author: Guo Huan
date: 2021-04-19
---

# PG_AUTHID

PG_AUTHID系统表存储有关数据库认证标识符（角色）的信息。角色把"用户"的概念包含在内。一个用户实际上就是一个rolcanlogin标志被设置的角色。任何角色（不管rolcanlogin设置与否）都能够把其他角色作为成员。

MogDB中只有一份pg_authid，不是每个数据库有一份。需要有系统管理员权限才可以访问此系统表。

**表 1** PG_AUTHID字段

| 名称             | 类型                     | 描述                                                         |
| :--------------- | :----------------------- | :----------------------------------------------------------- |
| oid              | oid                      | 行标识符（隐含属性，必须明确选择）。                         |
| rolname          | name                     | 角色名称。                                                   |
| rolsuper         | boolean                  | 角色是否是拥有最高权限的初始系统管理员。<br />- t（true）：表示是。<br />- f（false）：表示不是。 |
| rolinherit       | boolean                  | 角色是否自动继承其所属角色的权限。<br />- t（true）：表示自动继承。<br />- f（false）：表示不自动继承。 |
| rolcreaterole    | boolean                  | 角色是否可以创建更多角色。<br />- t（true）：表示可以。<br />- f（false）：表示不可以。 |
| rolcreatedb      | boolean                  | 角色是否可以创建数据库。<br />- t（true）：表示可以。<br />- f（false）：表示不可以。 |
| rolcatupdate     | boolean                  | 角色是否可以直接更新系统表。只有usesysid=10的初始系统管理员拥有此权限。其他用户无法获得此权限。<br />- t（true）：表示可以。<br />- f（false）：表示不可以。 |
| rolcanlogin      | boolean                  | 角色是否可以登录，也就是说，这个角色可以给予会话认证标识符。<br />- t（true）：表示可以。<br />- f（false）：表示不可以。 |
| rolreplication   | boolean                  | 角色是否具有复制权限。<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
| rolauditadmin    | boolean                  | 角色是否具有审计管理员权限：<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
| rolsystemadmin   | boolean                  | 角色是否具有系统管理员权限：<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
| rolconnlimit     | integer                  | 对于可以登录的角色，限制其最大并发连接数量。<br/>-1 表示没有限制。 |
| rolpassword      | text                     | 口令(可能是加密的)，如果没有口令，则为NULL。                 |
| rolvalidbegin    | timestamp with time zone | 帐户的有效开始时间，如果没有开始时间，则为NULL。             |
| rolvaliduntil    | timestamp with time zone | 帐户的有效结束时间，如果没有结束时间，则为NULL。             |
| rolrespool       | name                     | 用户所能够使用的resource pool。                              |
| roluseft         | boolean                  | 角色是否可以操作外表。<br />- t（true）：表示可以。<br />- f（false）：表示不可以。 |
| rolparentid      | oid                      | 用户所在组用户的OID。                                        |
| roltabspace      | text                     | 用户数据表的最大空间限额。                                   |
| rolkind          | char                     | 特殊用户种类，包括私有用户、普通用户。                       |
| rolnodegroup     | oid                      | 该字段不支持。                                               |
| roltempspace     | text                     | 用户临时表的最大空间限额，单位为KB。                         |
| rolspillspace    | text                     | 用户执行作业时下盘数据的最大空间限额，单位为KB。             |
| rolexcpdata      | text                     | 用户可以设置的查询规则（当前未使用）。                       |
| rolmonitoradmin  | boolean                  | 角色是否具有监控管理员权限：<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
| roloperatoradmin | boolean                  | 角色是否具有运维管理员权限：<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
| rolpolicyadmin   | boolean                  | 角色是否具有安全策略管理员权限：<br />- t（true）：表示有。<br />- f（false）：表示没有。 |
