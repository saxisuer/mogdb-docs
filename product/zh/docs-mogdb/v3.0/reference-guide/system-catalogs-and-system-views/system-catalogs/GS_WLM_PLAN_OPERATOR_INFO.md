---
title: GS_WLM_PLAN_OPERATOR_INFO
summary: GS_WLM_PLAN_OPERATOR_INFO
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_PLAN_OPERATOR_INFO

GS_WLM_PLAN_OPERATOR_INFO系统表显示执行作业结束后计划算子级的相关的记录。此数据是从内核中转储到系统表中的数据。

**表 1** GS_WLM_PLAN_OPERATOR_INFO的字段<a id="GS_WLM_PLAN_OPERATOR_INFO的字段"> </a>

| 名称            | 类型    | 描述                                       |
| :-------------- | :------ | :----------------------------------------- |
| datname         | name    | 收集计划信息所在的database名。             |
| queryid         | bigint  | 语句执行使用的内部query_id。               |
| plan_node_id    | integer | 查询对应的执行计划的plan node id。         |
| startup_time    | bigint  | 该算子处理第一条数据的开始时间。           |
| total_time      | bigint  | 该算子到结束时候总的执行时间(ms)。         |
| actual_rows     | bigint  | 实际执行的行数信息。                       |
| max_peak_memory | integer | 当前算子在数据库节点上的最大内存峰值(MB)。 |
| query_dop       | integer | 当前算子执行时的并行度。                   |
| parent_node_id  | integer | 当前算子的父节点node id。                  |
| left_child_id   | integer | 当前算子的左孩子节点node id。              |
| right_child_id  | integer | 当前算子的右孩子节点node id。              |
| operation       | text    | 当前算子进行的操作名称。                   |
| orientation     | text    | 当前算子的对齐方式。                       |
| strategy        | text    | 当前算子操作的实现方法。                   |
| options         | text    | 当前算子操作的选择方式。                   |
| condition       | text    | 当前算子操作的过滤条件。                   |
| projection      | text    | 当前算子的映射关系。                       |
