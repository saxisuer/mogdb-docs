---
title: PG_USER_STATUS
summary: PG_USER_STATUS
author: Guo Huan
date: 2021-04-19
---

# PG_USER_STATUS

PG_USER_STATUS系统表提供了访问数据库用户的状态。需要有系统管理员权限才可以访问此系统表。

**表 1** PG_USER_STATUS字段

| 名称            | 类型                     | 描述 |
| :-------------- | :----------------------- | :---------------------- |
| oid | oid | 行标识符（隐含字段，必须明确选择）。 |
| roloid          | oid                      | 角色的标识。          |
| failcount       | integer                  | 尝试失败次数。        |
| locktime        | timestamp with time zone | 角色被锁定的时间点。  |
| rolstatus       | smallint                 | 角色的状态。<br/>- 0: 正常状态。<br/>- 1: 由于登录失败次数超过阈值被锁定了一定的时间。<br/>- 2: 被管理员锁定。 |
| permspace       | bigint                   | 角色已经使用的永久表存储空间大小。                           |
| tempspace       | bigint                   | 角色已经使用的临时表存储空间大小。                           |
| passwordexpired | smallint                 | 密码是否失效。<br/>- 0: 密码有效。<br/>- 1: 密码失效。       |
