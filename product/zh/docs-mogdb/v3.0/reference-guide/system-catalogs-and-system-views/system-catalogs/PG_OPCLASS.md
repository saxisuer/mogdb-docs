---
title: PG_OPCLASS
summary: PG_OPCLASS
author: Guo Huan
date: 2021-04-19
---

# PG_OPCLASS

PG_OPCLASS系统表定义索引访问方法操作符类。

每个操作符类为一种特定数据类型和一种特定索引访问方法定义索引字段的语义。一个操作符类本质上指定一个特定的操作符族适用于一个特定的可索引的字段数据类型。索引的字段实际可用的族中的操作符集是接受字段的数据类型作为它们的左边的输入的那个。

**表 1** PG_OPCLASS字段

| 名称         | 类型    | 引用             | 描述                                        |
| :----------- | :------ | :--------------- | :------------------------------------------ |
| oid          | oid     | -                | 行标识符（隐含属性，必须明确选择）。        |
| opcmethod    | oid     | PG_AM.oid        | 操作符类所服务的索引访问方法。              |
| opcname      | name    | -                | 这个操作符类的名称。                        |
| opcnamespace | oid     | PG_NAMESPACE.oid | 这个操作符类的名称空间。                    |
| opcowner     | oid     | PG_AUTHID.oid    | 操作符类属主。                              |
| opcfamily    | oid     | PG_OPFAMILY.oid  | 包含该操作符类的操作符族。                  |
| opcintype    | oid     | PG_TYPE.oid      | 操作符类索引的数据类型。                    |
| opcdefault   | boolean | -                | 如果操作符类是opcintype的缺省，则为真。     |
| opckeytype   | oid     | PG_TYPE.oid      | 索引数据的类型，如果和opcintype相同则为零。 |

一个操作符类的opcmethod必须匹配包含它的操作符族的opfmethod。
