---
title: GS_PACKAGE
summary: GS_PACKAGE
author: Guo Huan
date: 2022-05-12
---

# GS_PACKAGE

GS_PACKAGE系统表记录PACKAGE内的信息。

**表 1** GS_PACKAGE字段

| 名称           | 类型    | 描述                                 |
| :------------- | :------ | :----------------------------------- |
| oid            | oid     | 行标识符（隐含属性，必须明确选择）。 |
| pkgnamespace   | oid     | package所属schema。                  |
| pkgowner       | oid     | package的所属者。                    |
| pkgname        | name    | package的名字。                      |
| pkgspecsrc     | text    | package specification的内容。        |
| pkgbodydeclsrc | text    | package body的内容。                 |
| pkgbodyinitsrc | text    | package init的内容。                 |
| pkgacl         | aclitem | 访问权限。                           |
| pkgsecdef      | boolean | package是否是定义者权限。            |