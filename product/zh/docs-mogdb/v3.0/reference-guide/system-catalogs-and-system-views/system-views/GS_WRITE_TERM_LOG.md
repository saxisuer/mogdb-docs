---
title: GS_WRITE_TERM_LOG
summary: GS_WRITE_TERM_LOG
author: Guo Huan
date: 2022-05-12
---

# GS_WRITE_TERM_LOG

写入一条日志记录DN当前的term值。备DN返回false，主DN写入成功后返回true。