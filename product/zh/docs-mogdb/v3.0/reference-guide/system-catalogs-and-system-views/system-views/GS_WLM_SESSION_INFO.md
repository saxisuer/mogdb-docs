---
title: GS_WLM_SESSION_INFO
summary: GS_WLM_SESSION_INFO
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_SESSION_INFO

GS_WLM_SESSION_INFO视图显示数据库实例执行作业结束后的负载管理记录。查询该视图需要sysadmin权限。

具体的字段请参考[GS_WLM_SESSION_HISTORY](GS_WLM_SESSION_HISTORY.md)表1字段中的信息。