---
title: PG_COMM_RECV_STREAM
summary: PG_COMM_RECV_STREAM
author: Guo Huan
date: 2021-06-07
---

# PG_COMM_RECV_STREAM

PG_COMM_RECV_STREAM视图展示节点上所有的通信库接收流状态。

**表 1** PG_COMM_RECV_STREAM字段

| 名称        | 类型    | 描述                                                         |
| :---------- | :------ | :----------------------------------------------------------- |
| node_name   | text    | 节点名称。                                                   |
| local_tid   | bigint  | 使用此通信流的线程ID。                                       |
| remote_name | text    | 连接对端节点名称。                                           |
| remote_tid  | bigint  | 连接对端线程ID。                                             |
| idx         | integer | 通信对端DN在本DN内的标识编号。                               |
| sid         | integer | 通信流在物理连接中的标识编号。                               |
| tcp_sock    | integer | 通信流所使用的tcp通信socket。                                |
| state       | text    | 通信流当前的状态。<br />- UNKNOWN：表示当前逻辑连接状态未知。<br />- READY：表示逻辑连接已就绪。 <br />- RUN：表示逻辑连接发送报文正常。<br />- HOLD：表示逻辑连接发送报文等待中。<br />- CLOSED：表示关闭逻辑连接。<br />- TO_CLOSED：表示将会关闭逻辑连接。 |
| query_id    | bigint  | 通信流对应的debug_query_id编号。                             |
| pn_id       | integer | 通信流所执行查询的plan_node_id编号。                         |
| send_smp    | integer | 通信流所执行查询send端的smpid编号。                          |
| recv_smp    | integer | 通信流所执行查询recv端的smpid编号。                          |
| recv_bytes  | bigint  | 通信流接收的数据总量，单位Byte。                             |
| time        | bigint  | 通信流当前生命周期使用时长，单位ms。                         |
| speed       | bigint  | 通信流的平均接收速率，单位Byte/s。                           |
| quota       | bigint  | 通信流当前的通信配额值，单位Byte。                           |
| buff_usize  | bigint  | 通信流当前缓存的数据大小，单位Byte。                         |
