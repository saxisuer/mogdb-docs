---
title: GS_SESSION_TIME
summary: GS_SESSION_TIME
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_TIME

GS_SESSION_TIME视图用于统计会话线程的运行时间信息，及各执行阶段所消耗时间。

**表 1** GS_SESSION_TIME字段

| 名称      | 类型    | 描述                    |
| :-------- | :------ | :---------------------- |
| sessid    | text    | 线程标识+线程启动时间。 |
| stat_id   | integer | 统计编号。              |
| stat_name | text    | 会话类型名称。          |
| value     | bigint  | 会话值。                |
