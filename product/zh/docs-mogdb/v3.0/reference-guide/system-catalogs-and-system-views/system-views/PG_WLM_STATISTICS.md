---
title: PG_WLM_STATISTICS
summary: PG_WLM_STATISTICS
author: Guo Huan
date: 2022-05-13
---

# PG_WLM_STATISTICS

PG_WLM_STATISTICS视图显示作业结束后或已被处理异常后的负载管理相关信息。查询该视图需要sysadmin权限。

**表 1** PG_WLM_STATISTICS字段

| 名称               | 类型    | 描述                                                         |
| :----------------- | :------ | :----------------------------------------------------------- |
| statement          | text    | 执行了异常处理的语句。                                       |
| block_time         | bigint  | 语句执行前的阻塞时间。                                       |
| elapsed_time       | bigint  | 语句的实际执行时间。                                         |
| total_cpu_time     | bigint  | 语句执行异常处理时数据库实例上CPU使用的总时间。              |
| qualification_time | bigint  | 语句检查倾斜率的时间周期。                                   |
| cpu_skew_percent   | integer | 语句在执行异常处理时数据库实例上CPU使用的倾斜率。            |
| control_group      | text    | 语句执行异常处理时所使用的Cgroups。                          |
| status             | text    | 语句执行异常处理后的状态，包括：<br/>- pending：执行前预备状态。<br/>- running：执行进行状态。<br/>- finished：执行正常结束。<br/>- abort：执行异常终止。 |
| action             | text    | 语句执行的异常处理动作，包括：<br/>- abort：执行终止操作。<br/>- adjust：执行Cgroups调整操作，目前只有降级操作。<br/>- finish：正常结束。 |
