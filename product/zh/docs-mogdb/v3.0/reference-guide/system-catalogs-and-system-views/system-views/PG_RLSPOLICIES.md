---
title: PG_RLSPOLICIES
summary: PG_RLSPOLICIES
author: Guo Huan
date: 2021-04-19
---

# PG_RLSPOLICIES

PG_RLSPOLICIES视图提供查询行级访问控制策略。

**表 1** PG_RLSPOLICIES字段

| 名称             | 类型   | 描述                                                       |
| :--------------- | :----- | :--------------------------------------------------------- |
| schemaname       | name   | 行级访问控制策略作用的表对象所属模式名称。                 |
| tablename        | name   | 行级访问控制策略作用的表对象名称。                         |
| policyname       | name   | 行级访问控制策略名称。                                     |
| policypermissive | text   | 行级访问控制策略属性。                                     |
| policyroles      | name[] | 行级访问控制策略影响的用户列表，不指定表示影响所有的用户。 |
| policycmd        | text   | 行级访问控制策略影响的SQL操作。                            |
| policyqual       | text   | 行级访问控制策略的表达式。                                 |
