---
title: GS_WLM_REBUILD_USER_RESOURCE_POOL
summary: GS_WLM_REBUILD_USER_RESOURCE_POOL
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_REBUILD_USER_RESOURCE_POOL

该视图用于在当前连接节点上重建内存中用户的资源池信息，无输出。只是用于资源池信息缺失或者错乱时用作补救措施。查询该视图需要sysadmin权限。

**表 1** GS_WLM_REBUILD_USER_RESOURCE_POOL的字段

| 名称                              | 类型    | 描述                                             |
| :-------------------------------- | :------ | :----------------------------------------------- |
| gs_wlm_rebuild_user_resource_pool | boolean | 重建内存中用户资源池信息结果。t为成功，f为失败。 |
