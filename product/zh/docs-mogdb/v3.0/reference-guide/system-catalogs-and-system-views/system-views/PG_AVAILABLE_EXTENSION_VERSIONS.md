---
title: PG_AVAILABLE_EXTENSION_VERSIONS
summary: PG_AVAILABLE_EXTENSION_VERSIONS
author: Guo Huan
date: 2021-04-19
---

# PG_AVAILABLE_EXTENSION_VERSIONS

PG_AVAILABLE_EXTENSION_VERSIONS视图显示数据库中某些特性的扩展版本信息。

**表 1** PG_AVAILABLE_EXTENSION_VERSIONS字段

| 名称        | 类型    | 描述                                                       |
| :---------- | :------ | :--------------------------------------------------------- |
| name        | name    | 扩展名。                                                   |
| version     | text    | 版本名。                                                   |
| installed   | Boolean | 如果这个扩展的版本是当前已经安装了的则为真。               |
| superuser   | Boolean | 如果只允许系统管理员安装这个扩展则为真。                   |
| relocatable | Boolean | 如果扩展可以重新加载到另一个模式则为真。                   |
| schema      | name    | 扩展必须安装到的模式名，如果部分或全部可重新定位则为NULL。 |
| requires    | name[]  | 先决条件扩展的名称，如果没有则为NULL。                     |
| comment     | text    | 扩展的控制文件中的评论字符串。                             |
