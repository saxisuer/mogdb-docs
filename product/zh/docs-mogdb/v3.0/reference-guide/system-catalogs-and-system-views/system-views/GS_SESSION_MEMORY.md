---
title: GS_SESSION_MEMORY
summary: GS_SESSION_MEMORY
author: Guo Huan
date: 2021-04-19
---

# GS_SESSION_MEMORY

GS_SESSION_MEMORY视图统计Session级别的内存使用情况，包含执行作业在数据节点上Postgres线程分配的所有内存。当GUC参数enable_memory_limit的值为off时，本视图不可用。

**表 1** GS_SESSION_MEMORY字段

| 名称     | 类型    | 描述                                               |
| :------- | :------ | :---------------------- |
| sessid   | text    | 线程启动时间+线程标识。                            |
| init_mem | integer | 当前正在执行作业进入执行器前已分配的内存，单位MB。 |
| used_mem | integer | 当前正在执行作业已分配的内存，单位MB。             |
| peak_mem | integer | 当前正在执行作业已分配的内存峰值，单位MB。         |
