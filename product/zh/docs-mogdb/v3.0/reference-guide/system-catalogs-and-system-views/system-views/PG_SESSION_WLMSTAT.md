---
title: PG_SESSION_WLMSTAT
summary: PG_SESSION_WLMSTAT
author: Guo Huan
date: 2022-05-13
---

# PG_SESSION_WLMSTAT

PG_SESSION_WLMSTAT视图显示当前用户执行作业正在运行时的负载管理相关信息。查询该视图需要sysadmin权限。

**表 1** PG_SESSION_WLMSTAT字段

| 名称             | 类型    | 描述                                                         |
| :--------------- | :------ | :----------------------------------------------------------- |
| datid            | oid     | 连接后端的数据库OID。                                        |
| datname          | name    | 连接后端的数据库名称。                                       |
| threadid         | bigint  | 后端线程ID。                                                 |
| sessionid        | bigint  | 会话ID。                                                     |
| processid        | integer | 后端线程的pid。                                              |
| usesysid         | oid     | 登录后端的用户OID。                                          |
| appname          | text    | 连接到后端的应用名。                                         |
| usename          | name    | 登录到该后端的用户名。                                       |
| priority         | bigint  | 语句所在Cgroups的优先级。                                    |
| attribute        | text    | 语句的属性：<br/>- Ordinary：语句发送到数据库后被解析前的默认属性。<br/>- Simple：简单语句。<br/>- Complicated：复杂语句。<br/>- Internal：数据库内部语句。<br/>- Unknown：未知。 |
| block_time       | bigint  | 语句当前为止的pending的时间，单位s。                         |
| elapsed_time     | bigint  | 语句当前为止的实际执行时间，单位s。                          |
| total_cpu_time   | bigint  | 语句在上一时间周期内的数据库实例上CPU使用的总时间，单位s。   |
| cpu_skew_percent | integer | 语句在上一时间周期内的数据库实例上CPU使用的倾斜率。          |
| statement_mem    | integer | 语句执行使用的statement_mem，预留字段。                      |
| active_points    | integer | 语句占用的资源池并发点数。                                   |
| dop_value        | integer | 语句的从资源池中获取的dop值。                                |
| control_group    | text    | 该字段不支持。                                               |
| status           | text    | 语句当前的状态，包括：<br/>- pending：执行前状态。<br/>- running：执行进行状态。<br/>- finished：执行正常结束。（当enqueue字段为StoredProc或Transaction时，仅代表语句中的部分作业已经执行完毕，该状态会持续到该语句完全执行完毕。）<br/>- aborted：执行异常终止。<br/>- active：非以上四种状态外的正常状态。<br/>- unknown：未知状态。 |
| enqueue          | text    | 该字段不支持。                                               |
| resource_pool    | name    | 语句当前所在的资源池。                                       |
| query            | text    | 该后端的最新查询。如果state状态是active（活的），此字段显示当前正在执行的查询。所有其他情况表示上一个查询。 |
| is_plana         | boolean | 该字段不支持。                                               |
| node_group       | text    | 该字段不支持。                                               |
