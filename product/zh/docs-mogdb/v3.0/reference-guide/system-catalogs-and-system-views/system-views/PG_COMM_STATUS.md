---
title: PG_COMM_STATUS
summary: PG_COMM_STATUS
author: Guo Huan
date: 2021-06-07
---

# PG_COMM_STATUS

PG_COMM_STATUS视图展示节点的通信库状态。

**表 1** PG_COMM_STATUS字段

| 名称             | 类型    | 描述                                        |
| :--------------- | :------ | :------------------------------------------ |
| node_name        | text    | 节点名称。                                  |
| rxpck_rate       | integer | 节点通信库接收速率，单位Byte/s。            |
| txpck_rate       | integer | 节点通信库发送速率，单位Byte/s。            |
| rxkbyte_rate     | bigint  | bigint节点通信库接收速率，单位KByte/s。     |
| txkbyte_rate     | bigint  | bigint节点通信库发送速率，单位KByte/s。     |
| buffer           | bigint  | cmailbox的buffer大小。                      |
| memkbyte_libcomm | bigint  | libcomm进程通信内存大小，单位Byte。         |
| memkbyte_libpq   | bigint  | libpq进程通信内存大小，单位Byte。           |
| used_pm          | integer | postmaster线程实时使用率。                  |
| used_sflow       | integer | gs_sender_flow_controller线程实时使用率。   |
| used_rflow       | integer | gs_receiver_flow_controller线程实时使用率。 |
| used_rloop       | integer | 多个gs_receivers_loop线程中高的实时使用率。 |
| stream           | integer | 当前使用的逻辑连接总数。                    |
