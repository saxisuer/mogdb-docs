---
title: PG_PREPARED_STATEMENTS
summary: PG_PREPARED_STATEMENTS
author: Guo Huan
date: 2021-04-19
---

# PG_PREPARED_STATEMENTS

PG_PREPARED_STATEMENTS视图显示当前会话所有可用的预备语句。

**表 1** PG_PREPARED_STATEMENTS字段

| 名称            | 类型                     | 描述                                                         |
| :-------------- | :----------------------- | :----------------------------------------------------------- |
| name            | text                     | 预备语句的标识符。                                           |
| statement       | text                     | 创建该预备语句的查询字符串。对于从SQL创建的预备语句而言是客户端提交的PREPARE语句；对于通过前/后端协议创建的预备语句而言是预备语句自身的文本。 |
| prepare_time    | timestamp with time zone | 创建该预备语句的时间戳。                                     |
| parameter_types | regtype[]                | 该预备语句期望的参数类型，以regtype类型的数组格式出现。与该数组元素相对应的OID可以通过把regtype转换为oid值得到。 |
| from_sql        | Boolean                  | - 如果该预备语句是通过PREPARE语句创建的则为true。<br/>- 如果是通过前/后端协议创建的则为false。 |
