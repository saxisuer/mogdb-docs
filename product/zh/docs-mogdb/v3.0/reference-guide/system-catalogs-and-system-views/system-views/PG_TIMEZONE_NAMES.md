---
title: PG_TIMEZONE_NAMES
summary: PG_TIMEZONE_NAMES
author: Guo Huan
date: 2021-04-19
---

# PG_TIMEZONE_NAMES

PG_TIMEZONE_NAMES视图提供了显示了所有能够被SET TIMEZONE识别的时区名及其缩写、UTC偏移量、是否夏时制。

**表 1** PG_TIMEZONE_NAMES字段

| 名称       | 类型     | 描述                                            |
| :--------- | :------- | :---------------------------------------------- |
| name       | text     | 时区名。                                        |
| abbrev     | text     | 时区名缩写。                                    |
| utc_offset | interval | 相对于UTC的偏移量。                             |
| is_dst     | Boolean  | 如果当前正处于夏令时范围则为TRUE，否则为FALSE。 |
