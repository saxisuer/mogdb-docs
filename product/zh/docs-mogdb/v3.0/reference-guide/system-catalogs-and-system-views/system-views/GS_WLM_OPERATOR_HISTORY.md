---
title: GS_WLM_OPERATOR_HISTORY
summary: GS_WLM_OPERATOR_HISTORY
author: Guo Huan
date: 2022-05-13
---

# GS_WLM_OPERATOR_HISTORY

GS_WLM_OPERATOR_HISTORY视图显示的是当前用户当前数据库主节点上执行作业结束后的算子的相关记录。查询该视图需要sysadmin权限。

记录的数据同[GS_WLM_OPERATOR_INFO](../../../reference-guide/system-catalogs-and-system-views/system-catalogs/GS_WLM_OPERATOR_INFO.md)表1。