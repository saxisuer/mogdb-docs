---
title: DV_SESSION_LONGOPS
summary: DV_SESSION_LONGOPS
author: Guo Huan
date: 2022-05-12
---

# DV_SESSION_LONGOPS

DV_SESSION_LONGOPS视图存储当前正在执行的操作的进度。该视图需要授权访问。

**表 1** DV_SESSION_LONGOPS字段

| 名称      | 类型    | 描述                                         |
| :-------- | :------ | :------------------------------------------- |
| sid       | bigint  | 当前正在执行的后台进程的OID。                |
| serial#   | integer | 当前正在执行的后台进程的序号，在MogDB中为0。 |
| sofar     | integer | 目前完成的工作量，在MogDB中为空。            |
| totalwork | integer | 工作总量，在MogDB中为空。                    |