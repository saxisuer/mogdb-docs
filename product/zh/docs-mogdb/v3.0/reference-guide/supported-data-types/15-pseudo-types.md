---
title: 伪类型
summary: 伪类型
author: Guo Huan
date: 2021-04-06
---

# 伪类型

MogDB数据类型中包含一系列特殊用途的类型，这些类型按照类别被称为伪类型。伪类型不能作为字段的数据类型，但是可以用于声明函数的参数或者结果类型。

当一个函数不仅是简单地接受并返回某种SQL数据类型的情况下伪类型是很有用的。[表1](#biao1)列出了所有的伪类型。

**表 1** 伪类型 <a id="biao1"></a>

| 名称             | 描述                                           |
| :--------------- | :--------------------------------------------- |
| any              | 表示函数接受任何输入数据类型。                 |
| anyelement       | 表示函数接受任何数据类型。                     |
| anyarray         | 表示函数接受任意数组数据类型。                 |
| anynonarray      | 表示函数接受任意非数组数据类型。               |
| anyenum          | 表示函数接受任意枚举数据类型。                 |
| anyrange         | 表示函数接受任意范围数据类型。                 |
| cstring          | 表示函数接受或者返回一个空结尾的C字符串。      |
| internal         | 表示函数接受或者返回一种服务器内部的数据类型。 |
| language_handler | 声明一个过程语言调用句柄返回language_handler。 |
| fdw_handler      | 声明一个外部数据封装器返回fdw_handler。        |
| record           | 标识函数返回一个未声明的行类型。               |
| trigger          | 声明一个触发器函数返回trigger。                |
| void             | 表示函数不返回数值。                           |
| opaque           | 一个已经过时的类型，以前用于所有上面这些用途。 |

声明用C编写的函数（不管是内置的还是动态装载的）都可以接受或者返回任何这样的伪数据类型。当伪类型作为参数类型使用时，用户需要保证函数的正常运行。

用过程语言编写的函数只能使用实现语言允许的伪类型。目前，过程语言都不允许使用作为参数类型的伪类型，并且只允许使用void和record作为结果类型。一些多态的函数还支持使用anyelement、anyarray、anynonarray anyenum和anyrange类型。

伪类型internal用于声明那种只能在数据库系统内部调用的函数，他们不能直接在SQL查询里调用。如果函数至少有一个internal类型的参数，则不能从SQL里调用他。建议不要创建任何声明返回internal的函数，除非他至少有一个internal类型的参数。

示例:

```sql
--创建表
MogDB=# create table t1 (a int);

--插入两条数据
MogDB=# insert into t1 values(1),(2);

--创建函数showall()。
MogDB=# CREATE OR REPLACE FUNCTION showall() RETURNS SETOF record
AS $$ SELECT count(*) from t1; $$
LANGUAGE SQL;

--调用函数showall()。
MogDB=# SELECT showall();
 showall
---------
 (2)
(1 row)

--删除函数。
MogDB=# DROP FUNCTION showall();

--删除表
MogDB=# drop table t1;
```
