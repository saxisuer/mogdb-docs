---
title: PREDICT BY
summary: PREDICT BY
author: Zhang Cuiping
date: 2021-11-01
---

# PREDICT BY

## 功能描述

利用完成训练的模型进行推测任务。

## 注意事项

调用的模型名称在系统表gs_model_warehouse中可查看到。

## 语法格式

```ebnf+diagram
PredictBy ::= PREDICT BY model_name [ (FEATURES attribute [, attribute] ) ];
```

## 参数说明

- model_name

  用于推测任务的模型名称。

  取值范围: 字符串，需要符合标识符的命名规则。

- attribute

  推测任务的输入特征列名。

  取值范围: 字符串，需要符合标识符的命名规则。

## 示例

```
SELECT id, PREDICT BY price_model (FEATURES size,lot), price
FROM houses;
```

## 相关链接

[CREATE MODEL](CREATE-MODEL.md)，[DROP MODEL](DROP-MODEL.md)
