---
title: DROP TRIGGER
summary: DROP TRIGGER
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TRIGGER

## 功能描述

删除触发器。

## 注意事项

只有触发器的所有者可以执行DROP TRIGGER操作，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
DropTrigger ::= DROP TRIGGER [ IF EXISTS ] trigger_name ON table_name [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的触发器不存在，则发出一个notice而不是抛出一个错误。

- **trigger_name**

  要删除的触发器名称。

  取值范围: 已存在的触发器。

- **table_name**

  要删除的触发器所在的表名称。

  取值范围: 已存在的含触发器的表。

- **CASCADE | RESTRICT**

  - CASCADE：级联删除依赖此触发器的对象。
  - RESTRICT：如果有依赖对象存在，则拒绝删除此触发器。此选项为缺省值。

## 示例

请参见CREATE TRIGGER的示例。

## 相关链接

[CREATE TRIGGER](CREATE-TRIGGER.md)，[ALTER TRIGGER](ALTER-TRIGGER.md)，[ALTER TABLE](ALTER-TABLE.md)
