---
title: DROP TABLESPACE
summary: DROP TABLESPACE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TABLESPACE

## 功能描述

删除一个表空间。

## 注意事项

- 只有表空间所有者或者被授予了表空间DROP权限的用户有权限执行DROP TABLESPACE命令，系统管理员默认拥有此权限。
- 在删除一个表空间之前，表空间里面不能有任何数据库对象，否则会报错。
- DROP TABLESPACE不支持回滚，因此，不能出现在事务块内部。
- 执行DROP TABLESPACE操作时，如果有另外的会话执行\db查询操作，可能会由于tablespace事务的原因导致查询失败，请重新执行\db查询操作。
- 如果执行DROP TABLESPACE失败，需要再次执行一次DROP TABLESPACE IF EXISTS。

## 语法格式

```ebnf+diagram
DropTablespace ::= DROP TABLESPACE [ IF EXISTS ] tablespace_name;
```

## 参数说明

- **IF EXISTS**

  如果指定的表空间不存在，则发出一个notice而不是抛出一个错误。

- **tablespace_name**

  表空间的名称。

  取值范围: 已存在的表空间的名称。

## 示例

请参见CREATE TABLESPACE的示例。

## 相关链接

[ALTER TABLESPACE](ALTER-TABLESPACE.md)， [CREATE TABLESPACE](CREATE-TABLESPACE.md)

## 优化建议

- drop database

  不支持在事务中删除database。
