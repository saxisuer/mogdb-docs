---
title: DROP AUDIT POLICY
summary: DROP AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# DROP AUDIT POLICY

## 功能描述

删除一个审计策略。

## 注意事项

只有poladmin，sysadmin或初始用户才能进行此操作。

## 语法格式

```ebnf+diagram
DropAuditPolicy ::= DROP AUDIT POLICY [IF EXISTS] policy_name;
```

## 参数说明

policy_name

审计策略名称，需要唯一，不可重复。

取值范围: 字符串，要符合标识符的命名规范。

## 示例

请参考CREATE AUDIT POLICY的示例。

## 相关链接

[ALTER AUDIT POLICY](ALTER-AUDIT-POLICY.md)，[CREATE AUDIT POLICY](CREATE-AUDIT-POLICY.md)。
