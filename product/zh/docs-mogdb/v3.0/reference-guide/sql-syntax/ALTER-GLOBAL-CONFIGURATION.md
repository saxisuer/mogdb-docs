---
title: ALTER GLOBAL CONFIGURATION
summary: ALTER GLOBAL CONFIGURATION
author: Guo Huan
date: 2022-05-16
---

# ALTER GLOBAL CONFIGURATION

## 功能描述

新增、修改系统表gs_global_config，增加key-value值。

## 注意事项

- 仅支持数据库初始用户运行此命令。
- 不支持创建修改关键字为weak_password。

## 语法格式

```ebnf+diagram
AlterGlobalConfiguration ::= ALTER GLOBAL CONFIGURATION with( { paraname = value } [, ...] );
```

## 参数说明

- paraname
  
  参数名称，text类型。

- value
  
  参数值，text类型。

## 示例

```sql
--修改系统表gs_global_config的参数值。
MogDB=# select * from GS_GLOBAL_CONFIG;
    name     | value 
-------------+-------
 buckets_len | 16384
(1 row)

MogDB=# ALTER GLOBAL CONFIGURATION with (buckets_len=16383);
ALTER GLOBAL CONFIGURATION

MogDB=# select * from GS_GLOBAL_CONFIG;
    name     | value 
-------------+-------
 buckets_len | 16383
(1 row)
```