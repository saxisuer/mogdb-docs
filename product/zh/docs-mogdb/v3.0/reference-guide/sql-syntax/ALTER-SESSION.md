---
title: ALTER SESSION
summary: ALTER SESSION
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SESSION

## 功能描述

ALTER SESSION命令用于定义或修改那些对当前会话有影响的条件或参数。修改后的会话参数会一直保持，直到断开当前会话。

## 注意事项

- 如果执行SET TRANSACTION之前没有执行START TRANSACTION，则事务立即结束，命令无法显示效果。
- 可以用START TRANSACTION里面声明所需要的transaction_mode(s)的方法来避免使用SET TRANSACTION。

## 语法格式

- 设置会话的事务参数。

  ```ebnf+diagram
  AlterSession ::= ALTER SESSION SET [ SESSION CHARACTERISTICS AS ] TRANSACTION
      { ISOLATION LEVEL { READ COMMITTED } | { READ ONLY  | READ WRITE } } [, ...] ;
  ```

- 设置会话的其他运行时参数。

  ```ebnf+diagram
  AlterSession ::= ALTER SESSION SET
      {{config_parameter { { TO  | =  }  { value | DEFAULT }
        | FROM CURRENT }}
        | TIME ZONE time_zone
        | CURRENT_SCHEMA schema
        | NAMES encoding_name
        | ROLE role_name PASSWORD 'password'
        | SESSION AUTHORIZATION { role_name PASSWORD 'password' | DEFAULT }
        | XML OPTION { DOCUMENT | CONTENT }
      } ;
  ```

## 参数说明

修改会话涉及到的参数说明请参见SET语法中的参数说明。

## 示例

```sql
-- 创建模式ds。
MogDB=# CREATE SCHEMA ds;

--设置模式搜索路径。
MogDB=# SET SEARCH_PATH TO ds, public;

--设置日期时间风格为传统的POSTGRES风格（日在月前）。
MogDB=# SET DATESTYLE TO postgres, dmy;

--设置当前会话的字符编码为UTF8。
MogDB=# ALTER SESSION SET NAMES 'UTF8';

--设置时区为加州伯克利。
MogDB=# SET TIME ZONE 'PST8PDT';

--设置时区为意大利。
MogDB=# SET TIME ZONE 'Europe/Rome';

--设置当前模式。
MogDB=# ALTER SESSION SET CURRENT_SCHEMA TO tpcds;

--设置XML OPTION为DOCUMENT。
MogDB=# ALTER SESSION SET XML OPTION DOCUMENT;

--创建角色joe，并设置会话的角色为joe。
MogDB=# CREATE ROLE joe WITH PASSWORD 'xxxxxxxxx';
MogDB=# ALTER SESSION SET SESSION AUTHORIZATION joe PASSWORD 'xxxxxxxxx';

--切换到默认用户。
mogdb=> ALTER SESSION SET SESSION AUTHORIZATION default;

--删除ds模式。
MogDB=# DROP SCHEMA ds;

--删除joe。
MogDB=# DROP ROLE joe;
```

## 相关链接

[SET](SET.md)
