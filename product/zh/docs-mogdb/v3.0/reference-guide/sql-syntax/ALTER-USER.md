---
title: ALTER USER
summary: ALTER USER
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER USER

## 功能描述

修改数据库用户的属性。

## 注意事项

ALTER USER中修改的会话参数只针对指定的用户，且在下一次会话中有效。

## 语法格式

- 修改用户的权限等信息。

  ```ebnf+diagram
  AlterUser ::= ALTER USER user_name [ [ WITH ] option [ ... ] ];
  ```

  其中option子句为。

  ```ebnf+diagram
  option ::= { CREATEDB | NOCREATEDB }
      | { CREATEROLE | NOCREATEROLE }
      | { INHERIT | NOINHERIT }
      | { AUDITADMIN | NOAUDITADMIN }
      | { SYSADMIN | NOSYSADMIN }
      | { MONADMIN | NOMONADMIN}
      | { OPRADMIN | NOOPRADMIN}
      | { POLADMIN | NOPOLADMIN}
      | { USEFT | NOUSEFT }
      | { LOGIN | NOLOGIN }
      | { REPLICATION | NOREPLICATION }
      | { INDEPENDENT | NOINDEPENDENT}
      | { VCADMIN | NOVCADMIN}
      | {PERSISTENCE | NOPERSISTENCE}
      | CONNECTION LIMIT connlimit
      | [ ENCRYPTED | UNENCRYPTED ] PASSWORD { 'password' [EXPIRED] | DISABLE | EXPIRED }
      | [ ENCRYPTED | UNENCRYPTED ] IDENTIFIED BY { 'password' [ REPLACE 'old_password' | EXPIRED ] | DISABLE }
      | VALID BEGIN 'timestamp'
      | VALID UNTIL 'timestamp'
      | RESOURCE POOL 'respool'
      | PERM SPACE 'spacelimit'
      | PGUSER
  ```

- 修改用户名。

  ```ebnf+diagram
  AlterUser ::= ALTER USER user_name
      RENAME TO new_name;
  ```

- 锁定或解锁。

  ```ebnf+diagram
  AlterUser ::= ALTER USER user_name
      ACCOUNT { LOCK | UNLOCK };
  ```

- 修改与用户关联的指定会话参数值。

  ```ebnf+diagram
  AlterUser ::= ALTER USER user_name
      SET configuration_parameter { { TO | = } { value | DEFAULT } | FROM CURRENT };
  ```

- 重置与用户关联的指定会话参数值。

  ```ebnf+diagram
  AlterUser ::= ALTER USER user_name
      RESET { configuration_parameter | ALL };
  ```

## 参数说明

- **user_name**

  现有用户名。

  取值范围: 已存在的用户名。

- **new_password**

    新密码。

    密码规则如下：

  - 不能与当前密码相同。
  - 密码默认不少于8个字符。
  - 不能与用户名及用户名倒序相同。
  - 至少包含大写字母（A-Z），小写字母（a-z），数字（0-9），非字母数字字符（限定为~!@#$%^&*()-_=+\|[{}];:,&lt;.&gt;/?）四类字符中的三类字符。

    取值范围: 字符串。

- **old_password**

  旧密码。

- **ACCOUNT LOCK | ACCOUNT UNLOCK**

  - ACCOUNT LOCK：锁定帐户，禁止登录数据库。
  - ACCOUNT UNLOCK：解锁帐户，允许登录数据库。

- **PGUSER**

  当前版本不允许修改用户的PGUSER属性。

其他参数请参见[CREATE ROLE](CREATE-ROLE.md)和[ALTER ROLE](ALTER-ROLE.md)的参数说明。

## 示例

请参考[CREATE USER](CREATE-USER.md)的示例。

## 相关链接

[CREATE ROLE](CREATE-ROLE.md)、[CREATE USER](CREATE-USER.md)、[DROP USER](DROP-USER.md)
