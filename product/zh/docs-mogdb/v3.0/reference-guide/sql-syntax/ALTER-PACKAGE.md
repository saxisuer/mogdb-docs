---
title: ALTER PACKAGE
summary: ALTER PACKAGE
author: Guo Huan
date: 2022-05-16
---

# ALTER PACKAGE

## 功能描述

修改PACKAGE的属性。

## 注意事项

目前仅支持ALTER PACKAGE OWNER功能，系统管理员默认拥有该权限，有以下权限约束：

- 当前用户必须是该PACKAGE的所有者或者系统管理员，且该用户是新所有者角色的成员。

## 语法格式

- 修改PACKAGE的所属者。

  ```ebnf+diagram
  AlterPackage ::= ALTER PACKAGE package_name OWNER TO new_owner;
  ```

## 参数说明

- **package_name**

  要修改的PACKAGE名称。

  取值范围：已存在的PACKAGE名，仅支持修改单个PACKAGE。

- **new_owner**

  PACKAGE的新所有者。要修改函数的所有者，新所有者必须拥有该PACKAGE所属模式的CREATE权限。

  取值范围：已存在的用户角色。

## 示例

请参见[CREATE PACKAGE](CREATE-PACKAGE.md)中的示例。

## 相关链接

[CREATE PACKAGE](CREATE-PACKAGE.md)、[DROP PACKAGE](DROP-PACKAGE.md)