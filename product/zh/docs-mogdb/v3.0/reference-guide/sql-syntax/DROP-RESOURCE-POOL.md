---
title: DROP RESOURCE POOL
summary: DROP RESOURCE POOL
author: Zhang Cuiping
date: 2021-11-01
---

# DROP RESOURCE POOL

## 功能描述

删除一个资源池。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  如果某个角色已关联到该资源池，无法删除。

## 注意事项

只要用户对当前数据库有DROP权限，就可以删除资源池。

## 语法格式

```ebnf+diagram
DropResourcePool ::= DROP RESOURCE POOL [ IF EXISTS ] pool_name;
```

## 参数说明

- **IF EXISTS**

  如果指定的资源池不存在，发出一个notice而不是抛出一个错误。

- **pool_name**

  已创建过的资源池名称。

  取值范围: 字符串，要符合标识符的命名规范。

## 示例

请参见CREATE RESOURCE POOL的示例。

## 相关链接

[ALTER RESOURCE POOL](ALTER-RESOURCE-POOL.md)，[CREATE RESOURCE POOL](CREATE-RESOURCE-POOL.md)
