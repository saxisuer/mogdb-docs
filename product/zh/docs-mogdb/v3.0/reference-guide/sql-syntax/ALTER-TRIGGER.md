---
title: ALTER TRIGGER
summary: ALTER TRIGGER
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER TRIGGER

## 功能描述

修改触发器定义。

>![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**:
>
>目前只支持修改名称。

## 注意事项

只有触发器所在表的所有者可以执行ALTER TRIGGER操作，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
AlterTrigger ::= ALTER TRIGGER trigger_name ON table_name RENAME TO new_name;
```

## 参数说明

- **trigger_name**

  要修改的触发器名称。

  取值范围: 已存在的触发器。

- **table_name**

  要修改的触发器所在的表名称。

  取值范围: 已存在的含触发器的表。

- **new_name**

  修改后的新名称。

  取值范围: 符合标识符命名规范的字符串，最大长度不超过63个字符，且不能与所在表上其他触发器同名。

## 示例

请参见[CREATE TRIGGER](CREATE-TRIGGER.md)的示例。

## 相关链接

[CREATE TRIGGER](CREATE-TRIGGER.md)，[DROP TRIGGER](DROP-TRIGGER.md)，[ALTER TABLE](ALTER-TRIGGER.md)
