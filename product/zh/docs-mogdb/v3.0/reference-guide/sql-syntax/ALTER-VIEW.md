---
title: ALTER VIEW
summary: ALTER VIEW
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER VIEW

## 功能描述

ALTER VIEW更改视图的各种辅助属性。（如果用户是更改视图的查询定义，要使用CREATE OR REPLACE VIEW。）

## 注意事项

只有视图的所有者或者被授予了视图ALTER权限的用户才可以执行ALTER VIEW命令，系统管理员默认拥有该权限。针对所要修改属性的不同，对其还有以下权限约束：

- 修改视图的模式，当前用户必须是视图的所有者或者系统管理员，且要有新模式的CREATE权限。
- 修改视图的所有者，当前用户必须是视图的所有者或者系统管理员，且该用户必须是新所有者角色的成员，并且此角色必须有视图所在模式的CREATE权限。

## 语法格式

- 设置视图列的默认值。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      ALTER [ COLUMN ] column_name SET DEFAULT expression;
  ```

- 取消列视图列的默认值。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      ALTER [ COLUMN ] column_name DROP DEFAULT;
  ```

- 修改视图的所有者。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      OWNER TO new_owner;
  ```

- 重命名视图。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      RENAME TO new_name;
  ```

- 设置视图的所属模式。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      SET SCHEMA new_schema;
  ```

- 设置视图的选项。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      SET ( { view_option_name [ = view_option_value ] } [, ... ] );
  ```

- 重置视图的选项。

  ```ebnf+diagram
  AlterView ::= ALTER VIEW [ IF EXISTS ] view_name
      RESET ( view_option_name [, ... ] );
  ```

## 参数说明

- **IF EXISTS**

  使用这个选项，如果视图不存在时不会产生错误，仅有会有一个提示信息。

- **view_name**

  视图名称，可以用模式修饰。

  取值范围: 字符串，符合标识符命名规范。

- **column_name**

  可选的名称列表，视图的字段名。如果没有给出，字段名取自查询中的字段名。

  取值范围: 字符串，符合标识符命名规范。

- **SET/DROP DEFAULT**

  设置或删除一个列的缺省值，该参数暂无实际意义。

- **new_owner**

  视图新所有者的用户名称。

- **new_name**

  视图的新名称。

- **new_schema**

  视图的新模式。

- **view_option_name [ = view_option_value ]**

  该子句为视图指定一个可选的参数。

  目前view_option_name支持的参数仅有security_barrier，当VIEW试图提供行级安全时，应使用该参数。

  取值范围: Boolean类型，TRUE、FALSE。

## 示例

```sql
--创建一个由c_customer_sk小于150的内容组成的视图。
MogDB=# CREATE VIEW tpcds.customer_details_view_v1 AS
    SELECT * FROM tpcds.customer
    WHERE c_customer_sk < 150;

--修改视图名称。
MogDB=# ALTER VIEW tpcds.customer_details_view_v1 RENAME TO customer_details_view_v2;

--修改视图所属schema。
MogDB=# ALTER VIEW tpcds.customer_details_view_v2 SET schema public;

--删除视图。
MogDB=# DROP VIEW public.customer_details_view_v2;
```

## 相关链接

[CREATE VIEW](CREATE-VIEW.md), [DROP VIEW](DROP-VIEW.md)
