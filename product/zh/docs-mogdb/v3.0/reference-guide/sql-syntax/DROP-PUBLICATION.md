---
title: DROP PUBLICATION
summary: DROP PUBLICATION
author: Guo Huan
date: 2022-05-16
---

# DROP PUBLICATION

## **功能描述**

从数据库中删除一个现有的发布。

## **注意事项**

发布只能被其属主或系统管理员删除。

## **语法格式**

```ebnf+diagram
DropPublication ::= DROP PUBLICATION [ IF EXISTS ] name [ CASCADE | RESTRICT ]
```

## **参数说明**

- **IF EXISTS**

  如果发布不存在，不要抛出一个错误，而是发出一个提示。

- **name**

  现有发布的名称。

- **CASCADE|RESTRICT**

  当前这些关键词没有任何作用，因为发布没有依赖关系。

## 示例

请参见[CREATE PUBLICATION](CREATE-PUBLICATION.md)中的示例。

## 相关链接

[ALTER PUBLICATION](ALTER-PUBLICATION.md)、[CREATE PUBLICATION](CREATE-PUBLICATION.md)
