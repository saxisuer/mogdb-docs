---
title: DROP TYPE
summary: DROP TYPE
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TYPE

## 功能描述

删除一个用户定义的数据类型。

## 注意事项

只有类型的所有者或者被授予了类型DROP权限的用户有权限执行DROP TYPE命令，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
DropType ::= DROP TYPE [ IF EXISTS ] name [, ...] [ CASCADE | RESTRICT ]
```

## 参数说明

- **IF EXISTS**

  如果指定的类型不存在，那么发出一个notice而不是抛出一个错误。

- **name**

  要删除的类型名(可以有模式修饰)。

- **CASCADE**

  级联删除依赖该类型的对象(比如字段、函数、操作符等)

  **RESTRICT**

  如果有依赖对象，则拒绝删除该类型（缺省行为）。

## 示例

请参考[CREATE TYPE](CREATE-TYPE.md)的示例。

## 相关链接

[CREATE TYPE](CREATE-TYPE.md)，[ALTER TYPE](ALTER-TYPE.md)
