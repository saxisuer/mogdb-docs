---
title: CREATE AUDIT POLICY
summary: CREATE AUDIT POLICY
author: Zhang Cuiping
date: 2021-06-07
---

# CREATE AUDIT POLICY

## 功能描述

创建统一审计策略。

## 注意事项

只有poladmin，sysadmin或初始用户能进行此操作。

需要开启安全策略开关，即设置GUC参数enable_security_policy=on，审计策略才可以生效。

## 语法格式

```ebnf+diagram
CreateAuditPolicy ::= CREATE AUDIT POLICY [ IF NOT EXISTS ] policy_name { { privilege_audit_clause | access_audit_clause } [ filter_group_clause ] [ ENABLE | DISABLE ] };
```

- privilege_audit_clause：

  ```ebnf+diagram
  privilege_audit_clause ::= PRIVILEGES { DDL | ALL } [ ON LABEL ( resource_label_name [, ... ] ) ]
  ```

- access_audit_clause：

  ```ebnf+diagram
  access_audit_clause ::= ACCESS { DML | ALL } [ ON LABEL ( resource_label_name [, ... ] ) ]
  ```

- filter_group_clause：

  ```ebnf+diagram
  filter_group_clause ::= FILTER ON { ( FILTER_TYPE ( filter_value [, ... ] ) ) [, ... ] }
  ```

## 参数说明

- **policy_name**

  审计策略名称，需要唯一，不可重复；

  取值范围: 字符串，要符合标识符的命名规范。

- **DDL**

  指的是针对数据库执行如下操作时进行审计，目前支持：CREATE、ALTER、DROP、ANALYZE、COMMENT、GRANT、REVOKE、SET、SHOW、LOGIN_ANY、LOGIN_FAILURE、LOGIN_SUCCESS、LOGOUT。

- **ALL**

  指的是上述DDL支持的所有对数据库的操作。

- **resource_label_name**

  资源标签名称。

- **DML**

  指的是针对数据库执行如下操作时进行审计，目前支持：SELECT、COPY、DEALLOCATE、DELETE、EXECUTE、INSERT、PREPARE、REINDEX、TRUNCATE、UPDATE。

- **FILTER_TYPE**

  描述策略过滤的条件类型，包括IP | APP | ROLES。

- **filter_value**

  指具体过滤信息内容。

- **ENABLE|DISABLE**

  可以打开或关闭统一审计策略。若不指定ENABLE|DISABLE，语句默认为ENABLE。

## 示例

```sql
--创建dev_audit和bob_audit用户。
MogDB=# CREATE USER dev_audit PASSWORD 'dev@1234';
MogDB=# CREATE USER bob_audit password 'bob@1234';

--创建一个表tb_for_audit
MogDB=# CREATE TABLE tb_for_audit(col1 text, col2 text, col3 text);

--创建资源标签
MogDB=# CREATE RESOURCE LABEL adt_lb0 add TABLE(tb_for_audit);

--对数据库执行create操作创建审计策略
MogDB=# CREATE AUDIT POLICY adt1 PRIVILEGES CREATE;

--对数据库执行select操作创建审计策略
MogDB=# CREATE AUDIT POLICY adt2 ACCESS SELECT;

--仅审计记录用户dev_audit和bob_audit在执行针对adt_lb0资源进行的create操作数据库创建审计策略
MogDB=# CREATE AUDIT POLICY adt3 PRIVILEGES CREATE ON LABEL(adt_lb0) FILTER ON ROLES(dev_audit, bob_audit);

--仅审计记录用户dev_audit和bob_audit,客户端工具为psql和gsql，IP地址为'10.20.30.40', '127.0.0.0/24'，在执行针对adt_lb0资源进行的select、insert、delete操作数据库创建审计策略。
MogDB=# CREATE AUDIT POLICY adt4 ACCESS SELECT ON LABEL(adt_lb0), INSERT ON LABEL(adt_lb0), DELETE FILTER ON ROLES(dev_audit, bob_audit), APP(psql, gsql), IP('10.20.30.40', '127.0.0.0/24');
```

## 相关链接

[ALTER AUDIT POLICY](ALTER-AUDIT-POLICY.md)，[DROP AUDIT POLICY](DROP-AUDIT-POLICY.md)。
