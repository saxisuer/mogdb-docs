---
title: ALTER SYSTEM KILL SESSION
summary: ALTER SYSTEM KILL SESSION
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SYSTEM KILL SESSION

## 功能描述

ALTER SYSTEM KILL SESSION命令用于结束一个会话。

## 注意事项

无。

## 语法格式

```ebnf+diagram
AlterSystemKillSession ::= ALTER SYSTEM KILL SESSION 'session_sid, serial' [ IMMEDIATE ];
```

## 参数说明

- **session_sid, serial**

  会话的SID和SERIAL（获取方法请参考示例）。

- **IMMEDIATE**

  表明会话将在命令执行后立即结束。

## 示例

```sql
--查询会话信息。
MogDB=#
SELECT sa.sessionid AS sid,0::integer AS serial#,ad.rolname AS username FROM pg_stat_get_activity(NULL) AS sa
LEFT JOIN pg_authid ad ON(sa.usesysid = ad.oid)WHERE sa.application_name <> 'JobScheduler';
       sid       | serial# | username
-----------------+---------+----------
 140131075880720 |       0 | omm
 140131025549072 |       0 | omm
 140131073779472 |       0 | omm
 140131071678224 |       0 | omm
 140131125774096 |       0 |
 140131127875344 |       0 |
 140131113629456 |       0 |
 140131094742800 |       0 |
(8 rows)

--结束SID为140131075880720的会话。
MogDB=#  ALTER SYSTEM KILL SESSION '140131075880720,0' IMMEDIATE;
```
