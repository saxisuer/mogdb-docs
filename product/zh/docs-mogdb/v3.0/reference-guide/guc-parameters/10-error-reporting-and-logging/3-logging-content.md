---
title: 记录日志的内容
summary: 记录日志的内容
author: Zhang Cuiping
date: 2021-04-20
---

# 记录日志的内容

## debug_print_parse

**参数说明**: 用于控制打印解析树结果。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启打印结果的功能。
- off表示关闭打印结果的功能。

**默认值**: off

## debug_print_rewritten

**参数说明**: 用于控制打印查询重写结果。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启打印结果的功能。
- off表示关闭打印结果的功能。

**默认值**: off

## debug_print_plan

**参数说明**: 用于设置是否将查询的执行计划打印到日志中。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启打印结果的功能。
- off表示关闭打印结果的功能。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> - 只有当日志的级别为log及以上时，debug_print_parse、debug_print_rewritten和debug_print_plan的调试信息才会输出。当这些选项打开时，调试信息只会记录在服务器的日志中，而不会输出到客户端的日志中。通过设置client_min_messages和log_min_messages参数可以改变日志级别。
> - 在打开debug_print_plan开关的情况下需尽量避免调用gs_encrypt_aes128及gs_decrypt_aes128函数，避免敏感参数信息在日志中泄露的风险。同时建议用户在打开debug_print_plan开关生成的日志中对gs_encrypt_aes128及gs_decrypt_aes128函数的参数信息进行过滤后再提供给外部维护人员定位，日志使用完成后请及时删除。

## debug_pretty_print

**参数说明**: 设置此选项对debug_print_parse、debug_print_rewritten和debug_print_plan产生的日志进行缩进，会生成易读但比设置为off时更长的输出格式。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示进行缩进。
- off表示不进行缩进。

**默认值**: on

## log_checkpoints

**参数说明**: 控制在服务器日志中记录检查点和重启点的信息。打开此参数时，服务器日志消息包含涉及检查点和重启点的统计量，其中包含需要写的缓存区的数量及写入所花费的时间等。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打开此参数时，服务器日志消息包含涉及检查点和重启点的统计量。
- off表示关闭此参数时，服务器日志消息包含不涉及检查点和重启点的统计量。

**默认值**: off

## log_connections

**参数说明**: 控制记录客户端的连接请求信息。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> 有些客户端程序（例如gsql），在判断是否需要口令的时候会尝试连接两次，因此日志消息中重复的"connection receive"（收到连接请求)并不意味着一定是问题。

**取值范围**: 布尔型

- on表示记录信息。
- off表示不记录信息。

**默认值**: off

## log_disconnections

**参数说明**: 控制记录客户端结束连接信息。

该参数属于BACKEND类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示记录信息。
- off表示不记录信息。

**默认值**: off

## log_duration

**参数说明**: 控制记录每个已完成SQL语句的执行时间。对使用扩展查询协议的客户端、会记录语法分析、绑定和执行每一步所花费的时间。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- 设置为off，该选项与log_min_duration_statement的不同之处在于log_min_duration_statement强制记录查询文本。
- 设置为on并且log_min_duration_statement大于零，记录所有持续时间，但是仅记录超过阈值的语句。这可用于在高负载情况下搜集统计信息。

**默认值**: on

## log_error_verbosity

**参数说明**: 控制服务器日志中每条记录的消息写入的详细度。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- terse输出不包括DETAIL、HINT、QUERY及CONTEXT错误信息的记录。
- verbose输出包括SQLSTATE错误代码、源代码文件名、函数名及产生错误所在的行号。
- default输出包括DETAIL、HINT、QUERY及CONTEXT错误信息的记录，不包括SQLSTATE错误代码 、源代码文件名、函数名及产生错误所在的行号。

**默认值**: default

## log_hostname

**参数说明**: 选项关闭状态下，连接消息日志只显示正在连接主机的IP地址。打开此选项同时可以记录主机名。由于解析主机名可能需要一定的时间，可能影响数据库的性能。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示可以同时记录主机名。
- off表示不可以同时记录主机名。

**默认值**: on

## log_line_prefix

**参数说明**: 控制每条日志信息的前缀格式。日志前缀类似于printf风格的字符串，在日志的每行开头输出。用以%为开头的“转义字符”代替[表1](#转义字符表)中的状态信息。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**表 1** 转义字符表<a id="转义字符表"> </a>

| <span style="white-space:nowrap;">转义字符</span> | 效果|
| -----------------------| ------------------------------- |
| %a                                                       | 应用程序名称。                                               |
| %u                                                       | 用户名。                                                     |
| %d                                                       | 数据库名。                                                   |
| %r                                                       | 远端主机名或者IP地址以及远端端口，在不启动log_hostname时显示IP地址及远端端口。 |
| %h                                                       | 远端主机名或者IP地址，在不启动log_hostname时只显示IP地址。   |
| %p                                                       | 线程ID。                                                     |
| %t                                                       | 时间戳（没有毫秒）。                                         |
| %m                                                       | 带毫秒的时间戳。                                             |
| %n                                                       | 表示指定错误从哪个节点上报的。                               |
| %i                                                       | 命令标签：会话当前执行的命令类型。                           |
| %e                                                       | SQLSTATE错误码。                                             |
| %c                                                       | 会话ID，详见说明。                                           |
| %l                                                       | 每个会话或线程的日志编号，从1开始。                          |
| %s                                                       | 进程启动时间。                                               |
| %v                                                       | 虚拟事务ID（backendID/ localXID）。                          |
| %x                                                       | 事务ID（0表示没有分配事务ID）。                              |
| %q                                                       | 不产生任何输出。如果当前线程是后端线程，忽略这个转义序列，继续处理后面的转义序列；如果当前线程不是后端线程，忽略这个转义序列和它后面的所有转义序列。 |
| %S                                                       | 会话ID。                                                     |
| %%                                                       | 字符% 。                                                     |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> 转义字符%c打印一个独一无二的会话ID，由两个4字节的十六进制数组成，通过字符"."分开。这两个十六进制数分别表示进程的启动时间及进程编号，所以%c也可以看作是保存打印这些名目的途径的空间。比如，从pg_stat_activity中产生会话ID，可以用下面的查询：
>
> ```sql
> SELECT to_hex(EXTRACT(EPOCH FROM backend_start)::integer) || '.' ||
>        to_hex(pid)
> FROM pg_stat_activity;
> ```

- 当log_line_prefix设置为非空值时，请将其最后一个字符作为一个独立的段，以此来直观地与后续的日志进行区分，也可以使用一个标点符号。
- Syslog生成自己的时间戳及进程ID信息，所以当登录日志时，不需要包含这些转义字符。

**取值范围**: 字符串

**默认值**: %m %c %d %p %a %x %n %e

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：%m %c %d %p %a %x %n %e表示在日志开头附加会话开始时间戳、会话ID、数据库名、线程ID、应用程序名、事务ID、报错节点、SQLSTATE错误码。

## log_lock_waits

**参数说明**: 当一个会话的等待获得一个锁的时间超过[deadlock_timeout](../../../reference-guide/guc-parameters/16-lock-management.md#deadlock_timeout)的值时，此选项控制在数据库日志中记录此消息。这对于决定锁等待是否会产生一个坏的行为是非常有用的。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示记录此信息。
- off表示不记录此信息。

**默认值**: off

## log_statement

**参数说明**: 控制记录SQL语句。对于使用扩展查询协议的客户端，记录接收到执行消息的事件和绑定参数的值（内置单引号要双写）。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> 即使log_statement设置为all，包含简单语法错误的语句也不会被记录，因为仅在完成基本的语法分析并确定了语句类型之后才记录日志。在使用扩展查询协议的情况下，在执行阶段之前（语法分析或规划阶段）同样不会记录。将log_min_error_statement设为ERROR或更低才能记录这些语句。

**取值范围**: 枚举类型

- none表示不记录语句。
- ddl表示记录所有的数据定义语句，比如CREATE、ALTER和DROP语句。
- mod表示记录所有DDL语句，还包括数据修改语句INSERT、UPDATE、DELETE、TRUNCATE和COPY FROM。
- all表示记录所有语句，PREPARE、EXECUTE和EXPLAIN ANALYZE语句也同样被记录。

**默认值**: none

## log_temp_files

**参数说明**: 控制记录临时文件的删除信息。临时文件可以用来排序、哈希及临时查询结果。当一个临时文件被删除时，将会产生一条日志消息。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为-1，最大值2147483647，单位KB。

- 正整数表示只记录比log_temp_files设定值大的临时文件的删除信息。
- 值0表示记录所有的临时文件的删除信息。
- 值-1表示不记录任何临时文件的删除信息。

**默认值**: -1

## log_timezone

**参数说明**: 设置服务器写日志文件时使用的时区。与[TimeZone](../../../reference-guide/guc-parameters/15-default-settings-of-client-connection/2-zone-and-formatting.md#timezone)不同，这个值是数据库范围的，针对所有连接到本数据库的会话生效。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，可查询视图PG_TIMEZONE_NAMES获得。

**默认值**: PRC

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：gs_initdb进行相应系统环境设置时会对默认值进行修改。

## logging_module

**参数说明**: 用于设置或者显示模块日志在服务端的可输出性。该参数属于会话级参数，不建议通过gs_guc工具来设置。

该参数属于USERSET类型参数，设置请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置的方法进行设置。

**取值范围**: 字符串

**默认值**: 所有模块日志在服务端是不输出的，可由SHOW logging_module查看。为ALL,on(),off(DFS、GUC、ORC、SLRU、MEM_CTL、AUTOVAC、CACHE、ADIO、SSL、TBLSPC、WLM、EXECUTOR、OPFUSION、VEC_EXECUTOR、LLVM、OPT、OPT_REWRITE、OPT_JOIN、OPT_AGG、OPT_SUBPLAN、OPT_SETOP、OPT_SKEW、UDF、COOP_ANALYZE、WLMCP、ACCELERATE、PLANHINT、SNAPSHOT、XACT、HANDLE、CLOG、EC、REMOTE、CN_RETRY、PLSQL、TEXTSEARCH、SEQ、REDO、FUNCTION、PARSER、INSTR、INCRE_CKPT、DBL_WRT、RTO、HEARTBEAT)。

**设置方法**：首先，可以通过SHOW logging_module来查看哪些模块是支持可控制的。例如，查询输出结果为：

```sql
MogDB=# show logging_module;
logging_module
----------------------------------------------------
ALL,on(),off(DFS,GUC,ORC,SLRU,MEM_CTL,AUTOVAC,CACHE,ADIO,SSL,TBLSPC,WLM,EXECUTOR,VEC_EXECUTOR,LLVM,OPT,OPT_REWRITE,OPT_JOIN,OPT_AGG,OPT_SUBPLAN,OPT_SETOP,OPT_SKEW,UDF,COOP_ANALYZE,WLMCP,ACCELERATE,T,PLANHINT,SNAPSHOT,XACT,HANDLE,CLOG,EC,REMOTE,CN_RETRY,PLSQL,TEXTSEARCH,SEQ,REDO,FUNCTION,PARSER,INSTR,INCRE_CKPT,DBL_WRT,RTO,HEARTBEAT)
(1 row)
```

支持可控制的模块使用大写来标识，特殊标识ALL用于对所有模块日志进行设置。可以使用on/off来控制模块日志的输出。设置SSL模块日志为可输出，使用如下命令：

```sql
MogDB=# set logging_module='on(SSL)';
SET
MogDB=# show logging_module;
logging_module
---------------------------------------------------------------------
ALL,on(SSL),off(DFS,GUC,ORC,SLRU,MEM_CTL,AUTOVAC,CACHE,ADIO,TBLSPC,WLM,EXECUTOR,VEC_EXECUTOR,LLVM,OPT,OPT_REWRITE,OPT_JOIN,OPT_AGG,OPT_SUBPLAN,OPT_SETOP,OPT_SKEW,UDF,COOP_ANALYZE,WLMCP,ACCELERATE,,PLANHINT,SNAPSHOT,XACT,HANDLE,CLOG,EC,REMOTE,CN_RETRY,PLSQL,TEXTSEARCH,SEQ,REDO,FUNCTION,PARSER,INSTR,INCRE_CKPT,DBL_WRT,RTO,HEARTBEAT,COMM_IPC,COMM_PARAM)
(1 row)
```

可以看到模块SSL的日志输出被打开。

ALL标识是相当于一个快捷操作，即对所有模块的日志可输出进行开启或关闭。

```sql
MogDB=# set logging_module='off(ALL)';
SET
MogDB=# show logging_module;
logging_module
-------------------------------------
ALL,on(),off(DFS,GUC,ORC,SLRU,MEM_CTL,AUTOVAC,CACHE,ADIO,SSL,TBLSPC,WLM,EXECUTOR,VEC_EXECUTOR,LLVM,OPT,OPT_REWRITE,OPT_JOIN,OPT_AGG,OPT_SUBPLAN,OPT_SETOP,OPT_SKEW,UDF,COOP_ANALYZE,WLMCP,ACCELERATE,PLANHINT,SNAPSHOT,XACT,HANDLE,CLOG,EC,REMOTE,CN_RETRY,PLSQL,TEXTSEARCH,SEQ,REDO,FUNCTION,PARSER,INSTR,INCRE_CKPT,DBL_WRT,RTO,HEARTBEAT)
(1 row)

MogDB=# set logging_module='on(ALL)';
SET
MogDB=# show logging_module;
logging_module
-----------------------------------------
ALL,on(DFS,GUC,ORC,SLRU,MEM_CTL,AUTOVAC,CACHE,ADIO,SSL,TBLSPC,WLM,EXECUTOR,VEC_EXECUTOR,LLVM,OPT,OPT_REWRITE,OPT_JOIN,OPT_AGG,OPT_SUBPLAN,OPT_SETOP,OPT_SKEW,UDF,COOP_ANALYZE,WLMCP,ACCELERATE,PLANHINT,SNAPSHOT,XACT,HANDLE,CLOG,EC,REMOTE,CN_RETRY,PLSQL,TEXTSEARCH,SEQ,REDO,FUNCTION,PARSER,INSTR,INCRE_CKPT,DBL_WRT,RTO,HEARTBEAT),off()
(1 row)
```

**依赖关系**：该参数依赖于log_min_messages参数的设置

## opfusion_debug_mode

**参数说明**: 用于调试简单查询是否进行查询优化。设置成log级别可以在数据库节点的执行计划中看到没有查询优化的具体原因。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- off表示不打开该功能。
- log表示打开该功能，可以在数据库节点的执行计划中看到没有查询优化的具体原因。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> 提供在log中显示语句没有查询优化的具体原因，需要将参数设置成log级别，log_min_messages设置成debug4级别，logging_module设置'on(OPFUSION)'，注意log内容可能会比较多，尽可能在调优期间执行少量作业使用。

**默认值**: off

## enable_debug_vacuum

**参数说明**: 允许输出一些与VACUUM相关的日志，便于定位VACUUM相关问题。开发人员专用，不建议普通用户使用。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示开启此日志开关。
- off/false表示关闭此日志开关。

**默认值**: off
