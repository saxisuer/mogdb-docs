---
title: MogDB事务
summary: MogDB事务
author: Zhang Cuiping
date: 2021-04-20
---

# MogDB事务

介绍MogDB事务隔离、事务只读、最大prepared事务数、维护模式目的参数设置及取值范围等内容。

## transaction_isolation

**参数说明**: 设置当前事务的隔离级别。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，只识别以下字符串，大小写空格敏感:

- serializable: MogDB中等价于REPEATABLE READ。
- read committed: 只能读取已提交的事务的数据（缺省），不能读取到未提交的数据。
- repeatable read: 仅能读取事务开始之前提交的数据，不能读取未提交的数据以及在事务执行期间由其它并发事务提交的修改。
- default: 设置为default_transaction_isolation所设隔离级别。

**默认值**: read committed

## transaction_read_only

**参数说明**: 设置当前事务是只读事务。

该参数在数据库恢复过程中或者在备机里，固定为on；否则，固定为default_transaction_read_only的值。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示设置当前事务为只读事务。
- off表示该事务可以是非只读事务。

**默认值**: off

## xc_maintenance_mode

**参数说明**: 设置系统进入维护模式。

该参数属于SUSET类型参数，仅支持表[GUC参数分类](30-appendix.md)中的方式三进行设置。

**取值范围**: 布尔型

- on表示该功能启用。
- off表示该功能被禁用。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
>
> 谨慎打开这个开关，避免引起MogDB数据不一致。

**默认值**: off

## allow_concurrent_tuple_update

**参数说明**: 设置是否允许并发更新。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示该功能启用。
- off表示该功能被禁用。

**默认值**: on

## transaction_deferrable

**参数说明**: 指定是否允许一个只读串行事务延迟执行，使其不会执行失败。该参数设置为on时，当一个只读事务发现读取的元组正在被其他事务修改，则延迟该只读事务直到其他事务修改完成。该参数为预留参数，该版本不生效。与该参数类似的还有一个default_transaction_deferrable，设置它来指定一个事务是否允许延迟。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许执行。
- off表示不允许执行。

**默认值**: off

## enable_show_any_tuples

**参数说明**: 该参数只有在只读事务中可用，用于分析。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on/true表示表中元组的所有版本都会可见。
- off/false表示表中元组的所有版本都不可见。

**默认值**: off

## replication_type

**参数说明**: 标记当前HA模式是单主机模式、主备从模式还是一主多备模式。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

该参数用户不能自己去设置参数值。

**取值范围**: 0~2

- 2 表示单主机模式，此模式无法扩展备机。
- 1 表示使用一主多备模式，全场景覆盖，推荐使用。
- 0 表示主备从模式，目前此模式暂不支持。

**默认值**: 1

## pgxc_node_name

**参数说明**: 指定节点名称。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

在备机请求主机进行日志复制时，如果application_name参数没有设置，那么该参数会被用来作为备机在主机上的流复制槽名字。该流复制槽的命名方式为 “该参数值\_备机ip\_备机port”。其中，备机ip和备机port取自replconninfo参数中指定的备机ip和端口号。该流复制槽最大长度为61个字符，如果拼接后的字符串超过该长度，则会使用截断后的pgxc_node_name进行拼接，以保证流复制槽名字长度小于等于61个字符。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **注意：** 此参数修改后会导致连接数据库实例失败，不建议进行修改。

**取值范围**: 字符串

**默认值**: 当前节点名称

## enable_defer_calculate_snapshot

**参数说明**: 延迟计算快照的xmin和oldestxmin，执行1000个事务或者间隔1s才触发计算，设置为on时可以在高负载场景下减少计算快照的开销，但是会导致oldestxmin推进较慢，影响垃圾元组回收，设置为off时xmin和oldestxmin可以实时推进，但是会增加计算快照时的开销。

该参数属于SIGHUP类型参数，改请参考[GUC参数分类](30-appendix.md)进行设置

**取值范围**: 布尔型。

- on表示延迟计算快照xmin和oldestxmin。
- off表示实时计算快照xmin和oldestxmin。

**默认值**: on

## async_submit

**参数说明**：可以在session级别控制是否使用事务异步提交。该开关仅在“[enable_thread_pool](32-thread-pool.md#enable_thread_pool) = on”和“[synchronous_commit](../../reference-guide/guc-parameters/6-write-ahead-log/1-settings.md#synchronous_commit)”不为“off”时有效。

**取值范围**：布尔型。

- on：表示打开事务异步提交，该session上所有事务提交将异步完成。

- off：该session上所有事务按照原有的逻辑提交。

**默认值**：off