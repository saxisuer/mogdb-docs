---
title: 连接池参数
summary: 连接池参数
author: Zhang Cuiping
date: 2021-04-20
---

# 连接池参数

当使用连接池访问数据库时，在系统运行过程中，数据库连接是被当作对象存储在内存中的，当用户需要访问数据库时，并非建立一个新的连接，而是从连接池中取出一个已建立的空闲连接来使用。用户使用完毕后，数据库并非将连接关闭，而是将连接放回连接池中，以供下一个请求访问使用。

## pooler_maximum_idle_time

**参数说明**: Pooler链接自动清理功能使用，当链接池中链接空闲时间超过所设置值时，会触发自动清理机制，清理各节点的空闲链接数到minimum_pool_size。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  此参数在该版本不生效。

该参数属于USERSET类型参数，请参考[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为INT_MAX，最小单位为分钟

**默认值**: 1h（即60min）

## minimum_pool_size

**参数说明**: Pooler链接自动清理功能使用，自动清理后各pooler链接池对应节点的链接数最小剩余量，当参数设置为0时，可以关闭pooler链接自动清理功能。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  此参数在该版本不生效。

该参数属于USERSET类型参数，请参考[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为1，最大值为65535

**默认值**: 200

## cache_connection

**参数说明**: 是否回收连接池的连接。

该参数属于SIGHUP类型参数，请参考表[GUC参数设置方式](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示回收连接池的连接。
- off表示不回收连接池的连接。

**默认值**: on
