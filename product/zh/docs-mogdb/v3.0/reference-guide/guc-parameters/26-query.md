---
title: Query
summary: Query
author: Zhang Cuiping
date: 2021-04-20
---

# Query

## instr_unique_sql_count

**参数说明**: 控制系统中unique sql信息实时收集功能。配置为0表示不启用unique sql信息收集功能。

该值由大变小将会清空系统中原有的数据重新统计；从小变大不受影响。

当系统中产生的unique sql条目数量大于instr_unique_sql_count时，若开启了unique sql自动淘汰，则系统会按unique sql的更新时间由远到近自动淘汰一定比例的条目，使得新产生的unique sql信息可以继续被统计。若没有开启自动淘汰，则系统产生的新的unique sql信息将不再被统计。

在x86平台集中式部署下，硬件配置规格为32核CPU/256GB内存，使用Benchmark SQL 5.0工具测试性能，开关此参数性能影响约3%。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~2147483647

**默认值**: 100

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：**
>
> - 在开启自动淘汰的情况下，如果该值设置的较小，可能会导致系统频繁的进行自动淘汰，有可能会影响数据库系统性能，所以实际场景中建议不要将该值设置的过小，建议值为200000。
>
> - 在开启自动淘汰的情况下，如果该值设置的较大（例如38347922），清理过程中可能会引发大内存问题而无法清理。

## instr_unique_sql_track_type

**参数说明**: unique sql记录SQL方式。

该参数属于INTERNAL类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

top：只记录顶层SQL。

**默认值**: top

## enable_instr_rt_percentile

**参数说明**: 是否开启计算系统中80%和95%的SQL响应时间的功能。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on：表示打开SQL响应时间信息计算功能。
- off：表示关闭SQL响应时间信息计算功能。

**默认值**: on

## percentile

**参数说明**: SQL响应时间百分比信息，后台计算线程根据设置的值计算相应的百分比信息。

该参数属于INTERNAL类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。

**默认值**: 80,95

## instr_rt_percentile_interval

**参数说明**: SQL响应时间信息计算间隔，SQL响应时间信息计算功能打开后，后台计算线程每隔设置的时间进行一次计算。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～3600，单位为秒。

**默认值**: 10s

## enable_instr_cpu_timer

**参数说明**: 是否捕获SQL执行的cpu时间消耗 。

在x86平台集中式部署下，硬件配置规格为32核CPU/256GB内存，使用Benchmark SQL 5.0工具测试性能，开关此参数性能影响约3.5%。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on：表示捕获SQL执行的cpu时间消耗。
- off：表示不捕获SQL执行的cpu时间消耗。

**默认值**: on

## enable_stmt_track

**参数说明**: 控制是否启用Full /Slow SQL特性。

在x86平台集中式部署下，硬件配置规格为32核CPU/256GB内存，使用Benchmark SQL 5.0工具测试性能，开关此参数性能影响约1.2%。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on：表示开启Full /Slow SQL捕获。
- off：表示关闭Full /Slow SQL捕获。

**默认值**: on

## track_stmt_session_slot

**参数说明**: 设置一个session缓存的最大的全量/慢SQL的数量，超过这个数量，新的语句执行将不会被跟踪，直到落盘线程将缓存语句落盘，留出空闲的空间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ 2147483647

**默认值**: 1000

## track_stmt_details_size

**参数说明**: 设置单语句可以收集的最大的执行事件的大小（byte）。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ 100000000

**默认值**: 4096

## track_stmt_retention_time

**参数说明**: 组合参数，控制全量/慢SQL记录的保留时间。以60秒为周期读取该参数，并执行清理超过保留时间的记录，仅sysadmin用户可以访问。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符型

该参数分为两部分，形式为'full sql retention time, slow sql retention time'

full sql retention time为全量SQL保留时间，取值范围为0 ~ 86400

slow sql retention time为慢SQL的保留时间，取值范围为0 ~ 604800

**默认值**: 3600,604800

## track_stmt_stat_level

**参数说明**: 控制语句执行跟踪的级别。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置，不区分英文字母大小写。

**取值范围**: 字符型

该参数分为两部分，形式为'full sql stat level, slow sql stat level'

full sql stat level为全量SQL跟踪级别，取值范围为OFF、L0、L1、L2

slow sql stat level为慢SQL的跟踪级别，取值范围为OFF、L0、L1、L2

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  若全量SQL跟踪级别值为非OFF时，当前SQL跟踪级别值为全量SQL和慢SQL的较高级别（L2 &gt; L1 &gt; L0），L1在L0的基础上记录了执行计划，L2在L1的基础上记录了锁的详细信息，详情请参考[STATEMENT_HISTORY](../../reference-guide/system-catalogs-and-system-views/system-catalogs/STATEMENT_HISTORY.md)。

**默认值**: OFF,L0

## enable_auto_clean_unique_sql

**参数说明**: 当系统中产生的unique sql条目数量大于等于instr_unique_sql_count时，是否启用unique sql自动淘汰功能。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 由于快照有部分信息是来源于unique sql，所以开启自动淘汰的情况下，在生成wdr报告时，如果选择的起始快照和终止快照跨过了淘汰发生的时间，会导致无法生成wdr报告。
