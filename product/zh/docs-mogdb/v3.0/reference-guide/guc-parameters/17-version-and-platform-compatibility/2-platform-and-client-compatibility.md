---
title: 平台和客户端兼容性
summary: 平台和客户端兼容性
author: Zhang Cuiping
date: 2021-04-20
---

# 平台和客户端兼容性

很多平台都使用数据库系统，数据库系统的对外兼容性给平台提供了很大的方便。

## convert_string_to_digit

**参数说明**: 设置隐式转换优先级，是否优先将字符串转为数字。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示优先将字符串转为数字。
- off表示不优先将字符串转为数字。

**默认值**: on

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 该参数调整会修改内部数据类型转换规则，导致不可预期的行为，请谨慎操作。

## nls_timestamp_format

**参数说明**: 设置时间戳默认格式。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: DD-Mon-YYYY HH:MI:SS.FF AM

## max_function_args

**参数说明**: 函数参数最大个数。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

**取值范围**: 整型

**默认值**: 8192

## transform_null_equals

**参数说明**: 控制表达式expr = NULL（或NULL = expr）当做expr IS NULL处理。如果expr得出NULL值则返回真，否则返回假。

- 正确的SQL标准兼容的expr = NULL总是返回NULL（未知）。
- Microsoft Access里的过滤表单生成的查询使用expr = NULL来测试空值。打开这个选项，可以使用该接口来访问数据库。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示控制表达式expr = NULL（或NULL = expr）当做expr IS NULL处理。
- off表示不控制，即expr = NULL总是返回NULL（未知）。

**默认值**: off

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  新用户经常在涉及NULL的表达式上语义混淆，故默认值设为off。

## support_extended_features

**参数说明**: 控制是否支持数据库的扩展特性。

该参数属于POSTMASTER类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示支持数据库的扩展特性。
- off表示不支持数据库的扩展特性。

**默认值**: off

## sql_compatibility

**参数说明**: 控制数据库的SQL语法和语句行为同哪一个主流数据库兼容。

该参数属于INTERNAL类型参数，用户无法修改，只能查看。

**取值范围**: 枚举型

- A表示同O数据库兼容。
- B表示同MY数据库兼容。
- C表示同TD数据库兼容。
- PG表示同POSTGRES数据库兼容。

**默认值**: A

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 该参数只能在执行[CREATE DATABASE](../../../reference-guide/sql-syntax/CREATE-DATABASE.md)命令创建数据库的时候设置。
> - 在数据库中，该参数只能是确定的一个值，要么始终设置为A，要么始终设置为B，请勿任意改动，否则会导致数据库行为不一致。

## behavior_compat_options

**参数说明**: 数据库兼容性行为配置项，该参数的值由若干个配置项用逗号隔开构成。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: 空

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 当前只支持[表1](#jianrongxing)。
> - 配置多个兼容性配置项时，相邻配置项用逗号隔开，例如：set behavior_compat_options='end_month_calculate,display_leading_zero';

**表 1** 兼容性配置项 <a id="jianrongxing"> </a>

| **兼容性配置项**                | **兼容性行为控制**                                           |
| :------------------------------ | :----------------------------------------------------------- |
| display_leading_zero            | 浮点数显示配置项。<br />- 不设置此配置项时，对于-1\~0和0\~1之间的小数，不显示小数点前的0。比如，0.25显示为.25。<br />- 设置此配置项时，对于-1\~0和0\~1之间的小数，显示小数点前的0。比如，0.25显示为0.25。 |
| end_month_calculate             | add_months函数计算逻辑配置项。<br />假定函数add_months的两个参数分别为param1和param2，param1的月份和param2的和为月份result。<br />- 不设置此配置项时，如果param1的日期（Day字段）为月末，并且param1的日期（Day字段）比result月份的月末日期小，计算结果中的日期字段（Day字段）和param1的日期字段保持一致。比如，<br />`MogDB=# select add_months('2018-02-28',3) from sys_dummy; add_months ---------------------- 2018-05-28 00:00:00 (1 row)`<br />- 设置此配置项时，如果param1的日期（Day字段）为月末，并且param1的日期（Day字段）比result月份的月末日期比小，计算结果中的日期字段（Day字段）和result的月末日期保持一致。比如，<br />`MogDB=# select add_months('2018-02-28',3) from sys_dummy; add_months ---------------------- 2018-05-31 00:00:00 (1 row)` |
| compat_analyze_sample           | analyze采样行为配置项。<br />设置此配置项时，会优化analyze的采样行为，主要体现在analyze时全局采样会更精确的控制在3万条左右，更好的控制analyze时DBnode端的内存消耗，保证analyze性能的稳定性。 |
| bind_schema_tablespace          | 绑定模式与同名表空间配置项。<br />如果存在与模式名sche_name相同的表空间名，那么如果设置search_path为sche_name， default_tablespace也会同步切换到sche_name。 |
| bind_procedure_searchpath       | 未指定模式名的数据库对象的搜索路径配置项。<br />在存储过程中如果不显示指定模式名，会优先在存储过程所属的模式下搜索。<br />如果找不到，则有两种情况：<br />- 若不设置此参数，报错退出。<br />- 若设置此参数，按照search_path中指定的顺序继续搜索。如果还是找不到，报错退出。 |
| correct_to_number               | 控制to_number()结果兼容性的配置项。<br />若设置此配置项，则to_number()函数结果与pg11保持一致，否则默认与O db保持一致。 |
| unbind_dive_bound               | 控制对整数除法的结果进行范围校验。<br />若设置此配置项，则不需要对除法结果做范围校验，例如，INT_MIN/(-1)可以得到输出结果为INT_MAX+1，反之，则会因为超过结果大于INT_MAX而报越界错误。 |
| return_null_string              | 控制函数lpad()和rpad()结果为空字符串”的显示配置项。<br />- 不设置此配置项时，空字符串显示为NULL。<br />`MogDB=# select length(lpad('123',0,'*')) from sys_dummy; length -----------------------  (1 row)*`<br />- 设置此配置项时，空字符串显示为”。<br />`MogDB=# select length(lpad('123',0,'*')) from sys_dummy; length ----------------------- 0 (1 row)` |
| compat_concat_variadic          | 控制函数concat()和concat_ws()对variadic类型结果兼容性的配置项。<br />若设置此配置项，当concat函数参数为variadic类型时，保留a db和Teradata兼容模式下不同的结果形式；否则默认a db和Teradata兼容模式下结果相同，且与a db保持一致。由于MY无variadic类型，所以该选项对MY无影响。 |
| merge_update_multi              | 控制在使用MERGE INTO … WHEN MATCHED THEN UPDATE（参考[MERGE INTO](../../../reference-guide/sql-syntax/MERGE-INTO.md)） 和INSERT … ON DUPLICATE KEY UPDATE（参考[INSERT](../../../reference-guide/sql-syntax/INSERT.md)）时，当目标表中一条目标数据与多条源数据冲突时UPDATE行为。<br />若设置此配置项，当存在上述场景时，该冲突行将会多次执行UPDATE；否则（默认）报错，即MERGE或INSERT操作失败。 |
| hide_tailing_zero               | numeric显示配置项。不设置此项时，numeric按照指定精度显示。设置此项时，隐藏小数点后的末尾0。<br />`set behavior_compat_options='hide_tailing_zero'; select cast(123.123 as numeric(15,10)); numeric ---- 123.123 (1 row)` |
| rownum_type_compat              | 控制ROWNUM的类型，ROWNUM默认类型为INT8，设置此参数后，ROWNUM类型变更为NUMERIC类型。 |
| aformat_null_test               | 控制rowtype类型判空逻辑,设置此项时，对于rowtype is not null判断，当一行数据有一列不为空的时候返回ture。否则，对于rowtype is not null判断，当一行数据所有列不为空的时候返回ture. |
| aformat_regexp_match            | 控制正则表达式函数的匹配行为。<br/>设置此项，且sql_compatibility参数的值为A或B时，正则表达式的 flags 参数支持的选项含义有变更：<br/>1.  “.”默认不能匹配 '\n' 字符。<br/>2. flags 中包含n选项时，“.”能够匹配 '\n' 字符。 <br/>3. regexp_replace(source, pattern replacement) 函数替换所有匹配的子串。<br/>4. regexp_replace(source, pattern, replacement, flags) 在 flags值为” 或者null时，返回值为null。<br/>否则，正则表达式的 flags 参数支持的选项含义：<br/>1.  “.”默认能匹配 '\n' 字符。<br/>2. flags 中的 n 选项表示按照多行模式匹配。 <br/>3. regexp_replace(source, pattern replacement) 函数仅替换第一个匹配到的子串。<br/>4. regexp_replace(source, pattern, replacement, flags) 在 flags值为” 或者null时，返回值为替换后的字符串。 |
| compat_cursor                   | 控制隐式游标状态兼容行为。设置此项，且兼容O，隐式游标状态（SQL%FOUND、SQL%NOTFOUND、SQL%ISOPNE、SQL%ROWCOUNT）由原先的仅在当前执行的函数有效，拓展到包括本函数调用的子函数有效。 |
| proc_outparam_override          | 控制存储过程出参的重载行为，打开该参数后，对于存储过程只有out出参部分不同的情况下，也可以正常调用。 |
| proc_implicit_for_loop_variable | 控制存储过程中FOR_LOOP查询语句行为设置此项时，在FOR rec IN query LOOP语句中，若rec已经定义，不会复用已经定义的rec变量，而且重新建立一个新的变量。否则，会复用已经定义的rec变量，不会建立新的变量。 |
| allow_procedure_compile_check   | 控制存储过程中select语句和open cursor语句的编译检查设置此项时，在存储过程中执行select语句、open cursor for语句、cursor%rowtype语句、for rec in语句时，若查询的表不存在，则无法创建创建存储过程，不支持trigger函数的编译检查，若查询的表存在，则成功创建存储过程。 |
| char_coerce_compat              | 控制char(n)类型向其它变长字符串类型转换时的行为。默认情况下char(n)类型转换其它变长字符串类型时会省略尾部的空格，开启该参数后，转换时不再省略尾部的空格，并且在转换时如果char(n)类型的长度超过其它变长字符串类型时将会报错。该参数仅在sql_compatibility参数的值为A时生效。 |

## plpgsql.variable_conflict

**参数说明**: 设置同名的存储过程变量和表的列的使用优先级。

该参数属于USERSET类型参数，仅支持表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法3进行设置。

**取值范围**: 字符串

- error表示遇到存储过程变量和表的列名同名则编译报错。
- use_variable表示存储过程变量和表的列名同名则优先使用变量。
- use_column表示存储过程变量和表的列名同名则优先使用列名。

**默认值**: error

## td_compatible_truncation

**参数说明**: 控制是否开启与Teradata数据库相应兼容的特征。该参数在用户连接上与TD兼容的数据库时，可以将参数设置成为on（即超长字符串自动截断功能启用），该功能启用后，在后续的insert语句中，对目标表中char和varchar类型的列插入超长字符串时，会按照目标表中相应列定义的最大长度对超长字符串进行自动截断。保证数据都能插入目标表中，而不是报错。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  超长字符串自动截断功能不适用于insert语句包含外表的场景。如果向字符集为字节类型编码（SQL_ASCII、LATIN1等）的数据库中插入多字节字符数据（如汉字等)，且字符数据跨越截断位置，这种情况下，按照字节长度自动截断，自动截断后会在尾部产生非预期结果。如果用户有对于截断结果正确性的要求，建议用户采用UTF8等能够按照字符截断的输入字符集作为数据库的编码集。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示启动超长字符串自动截断功能。
- off表示停止超长字符串自动截断功能。

**默认值**: off

## lastval_supported

**参数说明**: 控制是否可以使用lastval函数。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示支持lastval函数，同时nextval函数不支持下推。
- off表示不支持lastval函数，同时nextval函数可以下推。

**默认值**: off
