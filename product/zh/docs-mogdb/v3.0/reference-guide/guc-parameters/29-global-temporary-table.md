---
title: 全局临时表
summary: 全局临时表
author: Zhang Cuiping
date: 2021-04-20
---

# 全局临时表

## max_active_global_temporary_table

**参数说明**: 全局临时表功能开关，控制是否可以创建全局临时表。

该参数属于USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ 1000000

- 0：全局临时表功能关闭。
- &gt; 0：全局临时表功能打开。

**默认值**: 1000

## vacuum_gtt_defer_check_age

**参数说明**: vacuum执行后检查全局临时表relfrozenxid与普通表的差异。如果全局临时表relfrozenxid落后超过指定参数值，就产生WARNING。一般不用修改。

该参数USERSET类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0 ~ 1000000

**默认值**: 10000
