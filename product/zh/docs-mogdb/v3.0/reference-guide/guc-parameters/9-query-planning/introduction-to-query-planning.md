---
title: 介绍
summary: 介绍
author: Zhang Cuiping
date: 2021-04-20
---

# 介绍

介绍查询优化器方法配置、开销常量、规划算法以及一些配置参数。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> 优化器中涉及的两个参数：
>
> - INT_MAX数据类型INT的最大值，其值为2147483647。
> - DBL_MAX数据类型FLOAT的最大值。
