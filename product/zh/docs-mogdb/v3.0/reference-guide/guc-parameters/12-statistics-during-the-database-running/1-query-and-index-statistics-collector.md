---
title: 查询和索引统计收集器
summary: 查询和索引统计收集器
author: Zhang Cuiping
date: 2021-04-20
---

# 查询和索引统计收集器

查询和索引统计收集器负责收集数据库系统运行中的统计数据，如在一个表和索引上进行了多少次插入与更新操作、磁盘块的数量和元组的数量、每个表上最近一次执行清理和分析操作的时间等。可以通过查询系统视图pg_stats和pg_statistic查看统计数据。下面的参数设置服务器范围内的统计收集特性。

## track_activities

**参数说明**: 控制收集每个会话中当前正在执行命令的统计数据。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启收集功能。
- off表示关闭收集功能。

**默认值**: on

## track_counts

**参数说明**: 控制收集数据库活动的统计数据。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启收集功能。
- off表示关闭收集功能。

**默认值**: on

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：在AutoVacuum自动清理进程中选择清理的数据库时，需要数据库的统计数据，故默认值设为on。
>

## track_io_timing

**参数说明**: 控制收集数据库I/O调用时序的统计数据。I/O时序统计数据可以在pg_stat_database中查询。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启收集功能，开启时，收集器会在重复地去查询当前时间的操作系统，这可能会引起某些平台的重大开销，故默认值设置为off。
- off表示关闭收集功能。

**默认值**: off

## track_functions

**参数说明**: 控制收集函数的调用次数和调用耗时的统计数据。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：当SQL语言函数设置为调用查询的“内联”函数时，不管是否设置此选项，这些SQL语言函数无法被追踪到。
>

**取值范围**: 枚举类型

- pl表示只追踪过程语言函数。
- all表示追踪SQL语言函数。
- none表示关闭函数追踪功能。

**默认值**: none

## track_activity_query_size

**参数说明**: 设置用于跟踪每一个活动会话的当前正在执行命令的字节数。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，100～102400

**默认值**: 1024

## stats_temp_directory

**参数说明**: 设置存储临时统计数据的目录。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：将其设置为一个基于RAM的文件系统目录会减少实际的I/O开销并可以提升其性能。
>

**取值范围**: 字符串

**默认值**: pg_stat_tmp

## track_thread_wait_status_interval

**参数说明**: 用来定期收集thread状态信息的时间间隔。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0～1天，单位为min。

**默认值**: 30min

## enable_save_datachanged_timestamp

**参数说明**: 确定是否收集insert/update/delete, exchange/truncate/drop partition操作对表数据改动的时间。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示允许收集相关操作对表数据改动的时间。
- off表示禁止收集相关操作对表数据改动的时间。

**默认值**: on

## track_sql_count

**参数说明**: 控制对每个会话中当前正在执行的SELECT、INSERT、UPDATE、DELETE、MERGE INTO语句进行计数的统计数据。

在x86平台集中式部署下，硬件配置规格为32核CPU/256GB内存，使用Benchmark SQL 5.0工具测试性能，开关此参数性能影响约0.8%。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启计数功能。
- off表示关闭计数功能。

**默认值**: on

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**：
>
> - track_sql_count参数受track_activities约束：
>   - track_activities开启而track_sql_count关闭时，如果查询了gs_sql_count视图，日志中将会有WARNING提示track_sql_count是关闭的；
>   - track_activities和track_sql_count同时关闭，那么此时日志中将会有两条WARNING，分别提示track_activities是关闭的和track_sql_count是关闭的；
>   - track_activities关闭而track_sql_count开启，此时日志中将仅有WARNING提示track_activities是关闭。
> - 当参数关闭时，查询视图的结果为0行。
