---
title: DCF参数设置
summary: DCF参数设置
author: Zhang Cuiping
date: 2021-11-08
---

# DCF参数设置

## enable_dcf

**参数说明**: 是否开启DCF模式，该参数不允许修改。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型，on/off。on表示当前安装部署方式为DCF模式，off表示当前安装部署方式为非DCF模式。

**默认值**: off

## dcf_ssl

**参数说明**: 是否开启SSL，重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型，on/off。on表示使用SSL，off表示不使用SSL。

**默认值**: on

## dcf_config

**参数说明**: 用户安装时自定义配置信息，该参数不允许修改。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**默认值**: 字符串，安装时用户自定义配置

## dcf_data_path

**参数说明**: DCF数据路径，该参数不允许修改。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**默认值**: 字符串，DN数据目录下的dcf_data目录

## dcf_log_path

**参数说明**: DCF日志路径，该参数不允许修改。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**默认值**: 字符串，DN数据目录下的dcf_log目录

## dcf_node_id

**参数说明**: DCF所在DN节点ID，用户安装时自定义，该参数不允许修改。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**默认值**: 整型，安装时用户自定义配置

## dcf_max_workers

**参数说明**: DCF回调函数线程个数。如果节点数量超过7个，需要增加这个参数的数值（比如增加到40），否则可能会出现主节点一直处于promoting状态，主备节点日志不推进的状态。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，10~262143

**默认值**: 10

## dcf_truncate_threshold

**参数说明**: DN对DCF日志进行truncate的门限阈值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~2147483647

**默认值**: 100000

## dcf_election_timeout

**参数说明**: DCF leader和follower选举超时时间。选举超时时间数值依赖于当前DN之间的网络状况，在超时时间较小且网络极差的情形下，会有超时选举发生，待网络恢复选举恢复正常。建议根据当前网络状态合理设置超时时间。对DCF节点时钟的约束：DCF节点间最大时钟差异小于选举超时时间的一半。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位s，1~600

**默认值**: 3

## dcf_enable_auto_election_priority

**参数说明**: DCF优先级选主是否允许内部自动调整优先级值。0表示不允许，1表示允许内部自动调整。

该参数属于SIGHUP类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~1

**默认值**: 1

## dcf_election_switch_threshold

**参数说明**: DCF防频繁切主门限。推荐根据用户业务可接受的最大故障时间配置。

该参数属于SIGHUP类型参数，请参考[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位s，0~2147483647

**默认值**: 0

## dcf_run_mode

**参数说明**: DCF选举模式，0表示自动选举模式，2表示去使能选举模式。目前去使能选举模式只限定少数派恢复场景使用，修改会导致数据库实例不可用。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型，0、2

**默认值**: 0

## dcf_log_level

**参数说明**: DCF日志级别。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

- **关闭日志**：“NONE”，NONE表示关闭日志打印，不能与以下日志级别混合使用。

- **开启日志：“**RUN_ERR|RUN_WAR|RUN_INF|DEBUG_ERR|DEBUG_WAR|DEBUG_INF|TRACE|PROFILE|OPER”

  日志级别可以从上述字符串中选取字符串并使用竖线组合使用，不能配置空串。

**默认值**: “RUN_ERR|RUN_WAR|DEBUG_ERR|OPER|RUN_INF|PROFILE”

## dcf_log_backup_file_count

**参数说明**: DCF运行日志备份保留个数。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~100

**默认值**: 10

## dcf_max_log_file_size

**参数说明**: DCF运行日志单个文件最大大小。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，1~1000

**默认值**: 10

## dcf_socket_timeout

**参数说明**: DCF通信模块连接socket超时时间，参数重启生效。对于网络环境比较差的环境，若配置很小的超时时间，可能会导致建链不成功，此时需要适当增大此值。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位ms，10~600000

**默认值**: 5000

## dcf_connect_timeout

**参数说明**: DCF通信模块建立连接超时时间，参数重启生效。对于网络环境比较差的环境，若配置很小的超时时间，可能会导致建链不成功，此时需要适当增大此值。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位ms，10~600000

**默认值**: 60000

## dcf_mec_fragment_size

**参数说明**: DCF通信模块fragment大小，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位KB，32~10240

**默认值**: 64

## dcf_stg_pool_max_size

**参数说明**: DCF存储模内存池最大值，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，32~2147483647

**默认值**: 2048

## dcf_stg_pool_init_size

**参数说明**: DCF存储模块内存池最小值，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，32~2147483647

**默认值**: 32

## dcf_mec_pool_max_size

**参数说明**: DCF通信模块内存池最大值，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，32~2147483647

**默认值**: 200

## dcf_flow_control_disk_rawait_threshold

**参数说明**: DCF流控功能的磁盘等待阈值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位us，0~2147483647

**默认值**: 100000

## dcf_flow_control_net_queue_message_num_threshold

**参数说明**: DCF流控功能的网络队列消息数阈值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~2147483647

**默认值**: 1024

## dcf_flow_control_cpu_threshold

**参数说明**: DCF CPU流控阈值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位百分比，0~2147483647

**默认值**: 100

## dcf_mec_batch_size

**参数说明**: DCF通信批量消息数，数值为0时，DCF会根据网络以及写入数据量自适应调整，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~1024

**默认值**: 0

## dcf_mem_pool_max_size

**参数说明**: DCF内存最大值，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，32~2147483647

**默认值**: 2048

## dcf_mem_pool_init_size

**参数说明**: DCF内存初始化大小，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，单位MB，32~2147483647

**默认值**: 32

## dcf_compress_algorithm

**参数说明**: DCF运行日志传输压缩算法，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型

- 0表示不压缩
- 1表示ZSTD压缩算法
- 2表示LZ4压缩算法

**默认值**: 0

## dcf_compress_level

**参数说明**: DCF日志传输压缩级别，参数重启生效，此参数生效前提必须配置有效的压缩算法，即设置合法的dcf_compress_algorithm参数。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~22

若不开启压缩，配置的压缩级别将不生效。

**默认值**: 1

## dcf_mec_channel_num

**参数说明**: DCF通信通道数量，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~64

**默认值**: 1

## dcf_rep_append_thread_num

**参数说明**: DCF日志复制线程数量，参数重启生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~1000

**默认值**: 2

## dcf_mec_agent_thread_num

**参数说明**: DCF通信工作线程数量，参数重启生效。dcf_mec_agent_thread_num值建议不少于2\*节点数\*dcf_mec_channel_num。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~1000

**默认值**: 10

## dcf_mec_reactor_thread_num

**参数说明**: DCF使用reactor线程数量，参数重启生效。dcf_mec_reactor_thread_num与dcf_mec_agent_thread_num比例建议1：40。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~100

**默认值**: 1

## dcf_log_file_permission

**参数说明**: DCF运行日志文件属性，参数重启生效，参数安装阶段配置，后续不支持修改。若用户需要支持同组的其他用户访问日志，首先需要所有的父目录都支持同组的其他用户也能访问。即若参数dcf_log_path_permission配置为750，dcf_log_file_permission只能为600或者640。若参数dcf_log_path_permission配置为700，dcf_log_file_permission只能为600。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举型，600、640

**默认值**: 600

## dcf_log_path_permission

**参数说明**: DCF运行日志目录属性，参数重启生效，参数安装阶段配置，后续不支持修改。若用户需要支持同组的其他用户访问日志路径，需选择参数750，否则选择700。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举型，700、750

**默认值**: 700
