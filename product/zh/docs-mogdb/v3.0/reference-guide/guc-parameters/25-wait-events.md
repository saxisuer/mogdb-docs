---
title: 等待事件
summary: 等待事件
author: Zhang Cuiping
date: 2021-04-20
---

# 等待事件

## enable_instr_track_wait

**参数说明**: 是否开启等待事件信息实时收集功能。

在x86平台集中式部署下，硬件配置规格为32核CPU/256GB内存，使用Benchmark SQL 5.0工具测试性能，开关此参数性能影响约1.4%。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on：表示打开等待事件信息收集功能。
- off：表示关闭等待事件信息收集功能。

**默认值**: on
