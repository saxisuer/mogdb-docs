---
title: 基于开销的清理延迟
summary: 基于开销的清理延迟
author: Zhang Cuiping
date: 2021-04-20
---

# 基于开销的清理延迟

这个特性的目的是允许管理员减少VACUUM和ANALYZE语句在并发活动的数据库上的I/O影响。比如，像VACUUM和ANALYZE这样的维护语句并不需要迅速完成，并且不希望他们严重干扰系统执行其他的数据库操作。基于开销的清理延迟为管理员提供了一个实现这个目的手段。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
> 有些清理操作会持有关键的锁，这些操作应该尽快结束并释放锁。所以MogDB的机制是，在这类操作过程中，基于开销的清理延迟不会发生作用。为了避免在这种情况下的长延时，实际的开销限制取下面两者之间的较大值:
>
> - vacuum_cost_delay\*accumulated_balance/vacuum_cost_limit
> - vacuum_cost_delay\*4

**背景信息**

在[ANALYZE | ANALYSE](../../../reference-guide/sql-syntax/ANALYZE-ANALYSE.md)和[VACUUM](../../../reference-guide/sql-syntax/VACUUM.md)语句执行过程中，系统维护一个内部的记数器，跟踪所执行的各种I/O操作的近似开销。如果积累的开销达到了vacuum_cost_limit声明的限制，则执行这个操作的进程将睡眠vacuum_cost_delay指定的时间。然后它会重置记数器然后继续执行。

这个特性是缺省关闭的。如需开启，需要把vacuum_cost_delay变量设置为一个非零值。

## vacuum_cost_delay

**参数说明**: 指定开销超过vacuum_cost_limit的值时，进程睡眠的时间。

要注意在许多系统上，睡眠的有效分辨率是10毫秒。因此把vacuum_cost_delay设置为一个不是10的整数倍的数值与将它设置为下一个10的整数倍作用相同。

此参数一般设置较小，常见的设置是10或20毫秒。调整此特性资源占用率时，最好是调整其他参数，而不是此参数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~100，正数值表示打开基于开销的清理延迟特性；0表示关闭基于开销的清理延迟特性。

**默认值**: 0

## vacuum_cost_page_hit

**参数说明**: 清理一个在共享缓存里找到的缓冲区的预计开销。它代表锁住缓冲池、查找共享的Hash表、扫描页面内容的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~10000。

**默认值**: 1

## vacuum_cost_page_miss

**参数说明**: 清理一个要从磁盘上读取的缓冲区的预计开销。它代表锁住缓冲池、查找共享Hash表、从磁盘读取需要的数据块、扫描它的内容的开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~10000。

**默认值**: 10

## vacuum_cost_page_dirty

**参数说明**: 清理修改一个原先是干净的块的预计开销。它代表把一个脏的磁盘块再次刷新到磁盘上的额外开销。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~1000

**默认值**: 20

## vacuum_cost_limit

**参数说明**: 设置清理进程休眠的开销限制。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~10000。

**默认值**: 200
