---
title: 预留参数
summary: 预留参数
author: Zhang Cuiping
date: 2021-11-08
---

# 预留参数

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  下列参数为预留参数，该版本不生效。

acce_min_datasize_per_thread

cstore_insert_mode

enable_constraint_optimization

enable_hadoop_env

enable_hdfs_predicate_pushdown

enable_orc_cache

schedule_splits_threshold

backend_version

undo_zone_count

version_retention_age
