---
title: SQL模式
summary: SQL模式
author: Zhang Cuiping
date: 2022-06-21
---

# SQL模式

当了避免数据库出现非法数据影响变更周期或者代码迁移过程中避免修改老代码时，可以设置以下参数实现。

## sql_mode

**参数说明**：设置SQL模式。

**取值范围**：string 或者null

- **sql_mode_strict**：如果数据不合法或缺失指定值时则会报错

  > 在非sql_mode_strict模式下支持以下四种场景：
  >
  > - 插入不符合当前列类型的值时,进行数据转换，分俩种：insert into table values(…) 和insert into table select …
  >
  >   主要涉及到各种数据类型之间的互相转换，目前涉及的类型有tinyint（tinyint由于数据范围和mysql有差别，暂时不
  >   考虑）,smallint,int,bigint,float,double,numeric,clob,char和varchar
  >
  > - 插入的列值长度超过此列所限定的长度时,赋予该列最大或最小值（涉及的类型有tinyint,smallint,int,bigint,float,double,numeric,clob,char和
  >   varchar）
  > - insert时，属性是非空且没有默认值的列，且没有在insert的列表中，则为其添加默认值（涉及的类型有tinyint,smallint,int,bigint,float,double,numeric,clob,char和
  >   varchar）
  > - 支持对属性是非空且没有默认值的列显式插入default（涉及的类型有tinyint,smallint,int,bigint,float,double,numeric,clob,char和
  >   varchar）

- **sql_mode_full_group**：不允许非group by的列在select列中（除非是作为聚合函数的参数）

  > 注意：如果select列表中的列没有使用聚合函数，也没有出现在group by子句，则会报错。

**默认值**： **sql_mode_strict**， **sql_mode_full_group**