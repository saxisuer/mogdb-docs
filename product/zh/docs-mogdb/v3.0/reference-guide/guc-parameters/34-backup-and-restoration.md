---
title: 备份恢复
summary: 备份恢复
author: Zhang Cuiping
date: 2021-11-08
---

# 备份恢复

## operation_mode

**参数说明**: 标示系统进入备份恢复模式。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示在备份恢复过程中。
- off表示不在备份恢复过程中。

**默认值**: off

## enable_cbm_tracking

**参数说明**: 当使用roach执行数据库实例的全量和增量备份时需要开启此参数，如果关闭会导致备份失败。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示追踪功能开启。
- off表示追踪功能关闭。

**默认值**: off

## hadr_max_size_for_xlog_receiver

**参数说明**: 该参数为异地容灾参数，表示灾备数据库实例中实例获取obs端日志和本地回放日志的最大允许差距，若差距大于此值时停止获取obs端日志。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中方式对应设置方法进行设置。

**修改建议：**该参数的取值应和本地磁盘大小相关，建议设置为磁盘大小的50%。

**取值范围**: 整型，0~2147483647

**默认值**: 256GB
