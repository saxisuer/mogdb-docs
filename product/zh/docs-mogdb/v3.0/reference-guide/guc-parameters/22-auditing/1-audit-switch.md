---
title: 审计开关
summary: 审计开关
author: Zhang Cuiping
date: 2021-04-20
---

# 审计开关

## audit_enabled

**参数说明**: 控制审计进程的开启和关闭。审计进程开启后，将从管道读取后台进程写入的审计信息，并写入审计文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示启动审计功能。
- off表示关闭审计功能。

**默认值**: on

## audit_directory

**参数说明**: 审计文件的存储目录。一个相对于数据目录data的路径，可自行指定。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: pg_audit。如果使用om工具部署MogDB，则审计日志路径为"$GAUSSLOG/pg_audit/实例名称"。

## audit_data_format

**参数说明**: 审计日志文件的格式。当前仅支持二进制格式。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: binary

## audit_rotation_interval

**参数说明**: 指定创建一个新审计日志文件的时间间隔。当现在的时间减去上次创建一个审计日志的时间超过了此参数值时，服务器将生成一个新的审计日志文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~INT_MAX/60，单位为min。

**默认值**: 1d

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
> 请不要随意调整此参数，否侧可能会导致audit_resource_policy无法生效，如果需要控制审计日志的存储空间和时间，请使用[audit_resource_policy](#audit_resource_policy)、[audit_space_limit](#audit_space_limit)和[audit_file_remain_time](#audit_file_remain_time)参数进行控制。

## audit_rotation_size

**参数说明**: 指定审计日志文件的最大容量。当审计日志消息的总量超过此参数值时，服务器将生成一个新的审计日志文件。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024~1048576，单位为KB。

**默认值**: 10MB

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
> 请不要随意调整此参数，否侧可能会导致audit_resource_policy无法生效，如果需要控制审计日志的存储空间和时间，请使用audit_resource_policy、audit_space_limit和audit_file_remain_time参数进行控制。

## audit_resource_policy

**参数说明**: 控制审计日志的保存策略，以空间还是时间限制为优先策略。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示采用空间优先策略，最多存储[audit_space_limit](#audit_space_limit)大小的日志。
- off表示采用时间优先策略，最少存储[audit_file_remain_time](#audit_file_remain_time)长度时间的日志。

**默认值**: on

## audit_file_remain_time

**参数说明**: 表示需记录审计日志的最短时间要求，该参数在[audit_resource_policy](#audit_resource_policy)为off时生效。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~730，单位为day，0表示无时间限制。

**默认值**: 90

## audit_space_limit

**参数说明**: 审计文件占用的磁盘空间总量。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1024KB~1024GB，单位为KB。

**默认值**: 1GB

## audit_file_remain_threshold

**参数说明**: 审计目录下审计文件个数的最大值。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~1048576

**默认值**: 1048576

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**：
> 请尽量保证此参数为1048576，并不要随意调整此参数，否则可能会导致audit_resource_policy无法生效，如果需要控制审计日志的存储空间和时间，请使用audit_resource_policy、audit_space_limit和audit_file_remain_time参数进行控制。

## audit_thread_num

**参数说明**: 审计线程的个数。

该参数属于POSTMASTER类型参数，请参考[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，1~48

**默认值**: 1

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 当audit_dml_state开关打开且在高性能场景下，建议增大此参数保证审计消息可以被及时处理和记录。
