---
title: 用户和权限审计
summary: 用户和权限审计
author: Zhang Cuiping
date: 2021-04-20
---

# 用户和权限审计

## audit_login_logout

**参数说明**: 这个参数决定是否审计MogDB用户的登录（包括登录成功和登录失败）、注销。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~7。

- 0表示关闭用户登录、注销审计功能。
- 1表示只审计用户登录成功。
- 2表示只审计用户登录失败。
- 3表示只审计用户登录成功和失败。
- 4表示只审计用户注销。
- 5表示只审计用户注销和登录成功。
- 6表示只审计用户注销和登录失败。
- 7表示审计用户登录成功、失败和注销。

**默认值**: 7

## audit_database_process

**参数说明**: 该参数决定是否对MogDB的启动、停止、切换和恢复进行审计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭MogDB启动、停止、恢复和切换审计功能。
- 1表示开启MogDB启动、停止、恢复和切换审计功能。

**默认值**: 1

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> MogDB启动时DN执行备升主流程，因此DN启动时审计日志中类型为system_switch。

## audit_user_locked

**参数说明**: 该参数决定是否审计MogDB用户的锁定和解锁。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭用户锁定和解锁审计功能。
- 1表示开启审计用户锁定和解锁功能。

**默认值**: 1

## audit_user_violation

**参数说明**: 该参数决定是否审计用户的越权访问操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭用户越权操作审计功能。
- 1表示开启用户越权操作审计功能。

**默认值**: 0

## audit_grant_revoke

**参数说明**: 该参数决定是否审计MogDB用户权限授予和回收的操作。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0、1。

- 0表示关闭审计用户权限授予和回收功能。
- 1表示开启审计用户权限授予和回收功能。

**默认值**: 1
