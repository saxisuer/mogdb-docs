---
title: 时间和日期处理函数和操作符
summary: 时间和日期处理函数和操作符
author: Zhang Cuiping
date: 2021-04-20
---

# 时间和日期处理函数和操作符

## 时间日期操作符

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **警告:**
> 用户在使用时间和日期操作符时，对应的操作数请使用明确的类型前缀修饰，以确保数据库在解析操作数的时候能够与用户预期一致，不会产生用户非预期的结果。
> 比如下面示例没有明确数据类型就会出现异常错误。
>
> ```sql
> SELECT date '2001-10-01' - '7' AS RESULT;
> ```

**表 1** 时间和日期操作符

| 操作符 | 示例                                                         |
| ------ | ------------------------------------------------------------ |
| +      | MogDB=# SELECT date '2001-09-28' + integer '7' AS RESULT; <br/>result <br/>-----------------<br/>2001-10-05 <br/>(1 row) |
|        | MogDB=# SELECT date '2001-09-28' + interval '1 hour' AS RESULT;<br/>result<br/> ----------------------------- <br/>2001-09-28 01:00:00<br/> (1 row) |
|        | MogDB=# SELECT date '2001-09-28' + time '03:00' AS RESULT; <br/>result<br/> -------------------------------<br/> 2001-09-28 03:00:00<br/> (1 row) |
|        | MogDB=# SELECT interval '1 day' + interval '1 hour' AS RESULT;<br/>result <br/>---------------------- <br/>1 day 01:00:00<br/> (1 row) |
|        | MogDB=# SELECT timestamp '2001-09-28 01:00' + interval '23 hours' AS RESULT; <br/>result <br/>------------------------------ <br/>2001-09-29 00:00:00<br/> (1 row) |
|        | MogDB=# SELECT time '01:00' + interval '3 hours' AS RESULT;<br/>result<br/> ------------- <br/>04:00:00<br/> (1 row) |
| -      | MogDB=# SELECT date '2001-10-01' - date '2001-09-28' AS RESULT; <br/>result<br/> ---------- <br/>3days <br/>(1 row) |
|        | MogDB=# SELECT date '2001-10-01' - integer '7' AS RESULT;<br/>result <br/>-------------------------------<br/> 2001-09-24 00:00:00<br/> (1 row) |
|        | MogDB=# SELECT date '2001-09-28' - interval '1 hour' AS RESULT; <br/>result <br/>--------------------------------<br/> 2001-09-27 23:00:00<br/> (1 row) |
|        | MogDB=# SELECT time '05:00' - time '03:00' AS RESULT;<br/> result <br/>------------- <br/>02:00:00<br/> (1 row) |
|        | MogDB=# SELECT time '05:00' - interval '2 hours' AS RESULT;<br/>result<br/> ------------- <br/>03:00:00<br/> (1 row) |
|        | MogDB=# SELECT timestamp '2001-09-28 23:00' - interval '23 hours' AS RESULT; <br/>result <br/>------------------------------- <br/>2001-09-28 00:00:00 <br/>(1 row) |
|        | MogDB=# SELECT interval '1 day' - interval '1 hour' AS RESULT;<br/>result <br/>------------- <br/>23:00:00<br/> (1 row) |
|        | MogDB=# SELECT timestamp '2001-09-29 03:00' - timestamp '2001-09-27 12:00' AS RESULT;<br/>result <br/>--------------------- <br/>1 day 15:00:00 <br/>(1 row) |
| *      | MogDB=# SELECT 900 * interval '1 second' AS RESULT;<br/>result <br/>--------------<br/> 00:15:00<br/> (1 row) |
|        | MogDB=# SELECT 21 * interval '1 day' AS RESULT;<br/>result <br/>-------------<br/> 21 days <br/>(1 row) |
|        | MogDB=# SELECT double precision '3.5' * interval '1 hour' AS RESULT;<br/>result <br/>--------------<br/> 03:30:00<br/> (1 row) |
| /      | MogDB=# SELECT interval '1 hour' / double precision '1.5' AS RESULT; <br/>result <br/>------------- <br/>00:40:00<br/> (1 row) |

## 时间/日期函数

- age(timestamp, timestamp)

  描述：将两个参数相减，并以年、月、日作为返回值。若相减值为负，则函数返回亦为负，入参可以都带timezone或都不带timezone。

  返回值类型：interval

  示例：

  ```sql
  MogDB=# SELECT age(timestamp '2001-04-10', timestamp '1957-06-13');
             age
  -------------------------
   43 years 9 mons 27 days
  (1 row)
  ```

- age(timestamp)

  描述：当前时间和参数相减，入参可以带或者不带timezone。

  返回值类型：interval

  示例：

  ```sql
  MogDB=# SELECT age(timestamp '1957-06-13');
             age
  -------------------------
   60 years 2 mons 18 days
  (1 row)
  ```

- clock_timestamp()

  描述：实时时钟的当前时间戳。

  返回值类型：timestamp with time zone

  示例：

  ```sql
  MogDB=# SELECT clock_timestamp();
          clock_timestamp
  -------------------------------
   2017-09-01 16:57:36.636205+08
  (1 row)
  ```

- current_date

  描述：当前日期。

  返回值类型：date

  示例：

  ```sql
  MogDB=# SELECT current_date;
      date
  ------------
   2017-09-01
  (1 row)
  ```

- current_time

  描述：当前时间。

  返回值类型：time with time zone

  示例：

  ```sql
  MogDB=# SELECT current_time;
         timetz
  --------------------
   16:58:07.086215+08
  (1 row)
  ```

- current_timestamp

  描述：当前日期及时间。

  返回值类型：timestamp with time zone

  示例：

  ```sql
  MogDB=# SELECT current_timestamp;
         pg_systimestamp
  ------------------------------
   2017-09-01 16:58:19.22173+08
  (1 row)
  ```

- systimestamp <a id = 'systimestamp'> </a>

  描述：返回数据库所在系统的系统日期，包括分数秒和时区。

  返回值类型：TIMESTAMP WITH TIME ZONE

  示例：

  ```sql
  MogDB=# select systimestamp;
  pg_systimestamp
  -----------------------------
  2021-12-24 14:34:24.6903+08
  (1 row)
  ```

- date_part(text, timestamp)

  描述：获取日期/时间值中子域的值，例如年或者小时的值。等效于extract(field from timestamp)。

  timestamp类型：abstime、date、interval、reltime、time with time zone、time without time zone、timestamp with time zone、timestamp without time zone。

  返回值类型：double precision

  示例：

  ```sql
  MogDB=# SELECT date_part('hour', timestamp '2001-02-16 20:38:40');
   date_part
           20
  (1 row)
  ```

- date_part(text, interval)

  描述：获取日期/时间值中子域的值。获取月份值时，如果月份值大于12，则取与12的模。等效于extract(field from timestamp)。

  返回值类型：double precision

  示例：

  ```sql
  MogDB=# SELECT date_part('month', interval '2 years 3 months');
   date_part
  -----------
           3
  (1 row)
  ```

- date_trunc(text, timestamp)

  描述：截取到参数text指定的精度。

  返回值类型：interval、timestamp with time zone、timestamp without time zone

  示例：

  ```sql
  MogDB=# SELECT date_trunc('hour', timestamp  '2001-02-16 20:38:40');
       date_trunc
  ---------------------
   2001-02-16 20:00:00
  (1 row)
  ```

- trunc(timestamp)

  描述：默认按天截取。

  示例：

  ```sql
  MogDB=# SELECT trunc(timestamp  '2001-02-16 20:38:40');                                                                                                                                                                   trunc
  ---------------------
  2001-02-16 00:00:00
  (1 row)
  ```

- daterange(arg1, arg2)

    描述：获取时间边界信息。arg1和arg2的类型为date。

    返回值类型：daterange

    示例：

    ```sql
    MogDB=# select daterange('2000-05-06','2000-08-08');
            daterange
    -------------------------
     [2000-05-06,2000-08-08)
    (1 row)
    ```

- daterange(arg1, arg2, text)

    描述：获取时间边界信息。arg1和arg2的类型为date，text类型为text。

    返回值类型：daterange

    示例：

    ```sql
    MogDB=# select daterange('2000-05-06','2000-08-08','[]');
            daterange
    -------------------------
     [2000-05-06,2000-08-09)
    (1 row)
    ```

- extract(field from timestamp)

  描述：获取小时的值。

  返回值类型：double precision

  示例：

  ```sql
  MogDB=# SELECT extract(hour from timestamp '2001-02-16 20:38:40');
   date_part
  -----------
          20
  (1 row)
  ```

- extract(field from interval)

  描述：获取月份的值。如果大于12，则取与12的模。

  返回值类型：double precision

  示例：

  ```sql
  MogDB=# SELECT extract(month from interval '2 years 3 months');
   date_part
  -----------
           3
  (1 row)
  ```

- isfinite(date)

  描述：测试是否为有效日期。

  返回值类型：Boolean

  示例：

  ```sql
  MogDB=# SELECT isfinite(date '2001-02-16');
   isfinite
  ----------
   t
  (1 row)
  ```

- isfinite(timestamp)

  描述：测试判断是否为有效时间。

  返回值类型：Boolean

  示例：

  ```sql
  MogDB=# SELECT isfinite(timestamp '2001-02-16 21:28:30');
   isfinite
  ----------
   t
  (1 row)
  ```

- isfinite(interval)

  描述：测试是否为有效区间。

  返回值类型：Boolean

  示例：

  ```sql
  MogDB=# SELECT isfinite(interval '4 hours');
   isfinite
  ----------
   t
  (1 row)
  ```

- justify_days(interval)

  描述：将时间间隔以月（30天为一月）为单位。

  返回值类型：interval

  示例：

  ```sql
  MogDB=# SELECT justify_days(interval '35 days');
   justify_days
  --------------
   1 mon 5 days
  (1 row)
  ```

- justify_hours(interval)

  描述：将时间间隔以天（24小时为一天）为单位。

  返回值类型：interval

  示例：

  ```sql
  MogDB=# SELECT JUSTIFY_HOURS(INTERVAL '27 HOURS');
   justify_hours
  ----------------
   1 day 03:00:00
  (1 row)
  ```

- justify_interval(interval)

  描述：结合justify_days和justify_hours，调整interval。

  返回值类型：interval

  示例：

  ```sql
  MogDB=# SELECT JUSTIFY_INTERVAL(INTERVAL '1 MON -1 HOUR');
   justify_interval
  ------------------
   29 days 23:00:00
  (1 row)
  ```

- localtime

  描述：当前时间。

  返回值类型：time

  示例：

  ```sql
  MogDB=# SELECT localtime AS RESULT;
       result
  ----------------
   16:05:55.664681
  (1 row)
  ```

- localtimestamp

  描述：当前日期及时间。

  返回值类型：timestamp

  示例：

  ```sql
  MogDB=# SELECT localtimestamp;
           timestamp
  ----------------------------
   2017-09-01 17:03:30.781902
  (1 row)
  ```

- now()

  描述：当前日期及时间。

  返回值类型：timestamp with time zone

  示例：

  ```sql
  MogDB=# SELECT now();
                now
  -------------------------------
   2017-09-01 17:03:42.549426+08
  (1 row)
  ```

- timenow

    描述：当前日期及时间。

    返回值类型：timestamp with time zone

    示例：

    ```sql
    MogDB=# select timenow();
            timenow
    ------------------------
     2020-06-23 20:36:56+08
    (1 row)
    ```

- numtodsinterval(num, interval_unit)

  描述：将数字转换为interval类型。num为numeric类型数字，interval_unit为固定格式字符串（'DAY' | 'HOUR' | 'MINUTE' | 'SECOND'）。

  可以通过设置参数IntervalStyle为a，兼容该函数interval输出格式。

  示例：

  ```sql
  MogDB=# SELECT numtodsinterval(100, 'HOUR');
   numtodsinterval
  -----------------
   100:00:00
  (1 row)

  MogDB=# SET intervalstyle = a;
  SET
  MogDB=# SELECT numtodsinterval(100, 'HOUR');
          numtodsinterval
  -------------------------------
   +000000004 04:00:00.000000000
  (1 row)
  ```

- pg_sleep(seconds)

  描述：服务器线程延迟时间，单位为秒。

  返回值类型：void

  示例：

  ```sql
  MogDB=# SELECT pg_sleep(10);
   pg_sleep
  ----------

  (1 row)
  ```

- statement_timestamp()

  描述：当前日期及时间。

  返回值类型：timestamp with time zone

  示例：

  ```sql
  MogDB=# SELECT statement_timestamp();
        statement_timestamp
  -------------------------------
   2017-09-01 17:04:39.119267+08
  (1 row)
  ```

- sysdate

  描述：当前日期及时间。

  返回值类型：timestamp

  示例：

  ```sql
  MogDB=# SELECT sysdate;
         sysdate
  ---------------------
   2017-09-01 17:04:49
  (1 row)
  ```

- timeofday()

  描述：当前日期及时间（像clock_timestamp，但是返回时为text）。

  返回值类型：text

  示例：

  ```sql
  MogDB=# SELECT timeofday();
                timeofday
  -------------------------------------
   Fri Sep 01 17:05:01.167506 2017 CST
  (1 row)
  ```

- transaction_timestamp()

  描述：当前日期及时间，与current_timestamp等效。

  返回值类型：timestamp with time zone

  示例：

  ```sql
  MogDB=# SELECT transaction_timestamp();
       transaction_timestamp
  -------------------------------
   2017-09-01 17:05:13.534454+08
  (1 row)
  ```

- add_months(d,n)

  描述：用于计算时间点d再加上n个月的时间。

  返回值类型：timestamp

  示例：

  ```sql
  MogDB=# SELECT add_months(to_date('2017-5-29', 'yyyy-mm-dd'), 11) FROM dual;
       add_months
  ---------------------
   2018-04-29 00:00:00
  (1 row)
  ```

- last_day(d)

  描述：用于计算时间点d当月最后一天的时间。

  返回值类型：timestamp

  示例：

  ```sql
  MogDB=# select last_day(to_date('2017-01-01', 'YYYY-MM-DD')) AS cal_result;
       cal_result
  ---------------------
   2017-01-31 00:00:00
  (1 row)
  ```

- next_day(x,y)

  描述：用于计算时间点x开始的下一个星期几（y）的时间。

  返回值类型：timestamp

  示例：

  ```sql
  MogDB=# select next_day(timestamp '2017-05-25 00:00:00','Sunday')AS cal_result;
       cal_result
  ---------------------
   2017-05-28 00:00:00
  (1 row)
  ```

- tinterval(abstime, abstime )

    描述：用两个绝对时间创建时间间隔。

    返回值类型：tinterval

    示例：

    ```sql
    MogDB=# call tinterval(abstime 'May 10, 1947 23:59:12', abstime 'Mon May  1 00:30:30 1995');
                          tinterval
    -----------------------------------------------------
     ["1947-05-10 23:59:12+09" "1995-05-01 00:30:30+08"]
    (1 row)
    ```

- tintervalend(tinterval)

    描述：返回tinteval的结束时间。

    返回值类型：abstime

    示例：

    ```sql
    MogDB=# select tintervalend('["Sep 4, 1983 23:59:12" "Oct4, 1983 23:59:12"]');
          tintervalend
    ------------------------
     1983-10-04 23:59:12+08
    (1 row)
    ```

- tintervalrel(tinterval)

    描述：计算并返回tinterval的相对时间。

    返回值类型：reltime

    示例：

    ```sql
    MogDB=# select tintervalrel('["Sep 4, 1983 23:59:12" "Oct4, 1983 23:59:12"]');
     tintervalrel
    --------------
     1 mon
    (1 row)
    ```

- to_timestamp

    描述：将入参的字符串的时间转换成不带时区的TIMESTAMP。

    返回值类型：timestamp，输出格式由GUC变量 nls_timestamp_format控制

    示例：

    ```sql
    MogDB=# SELECT to_timestamp('1997/Feb/16 --> 08:14:30', 'YYYY/Mon/DD --> HH:MI:SS');
        to_timestamp     
    ---------------------
     1997-02-16 08:14:30
    (1 row)
    ```

- to_yminterval

    描述：将入参转换为包含年月的interval。

    返回值类型：interval，输出格式由GUC变量IntervalStyle控制

    示例：

    ```sql
    MogDB=# SELECT TO_YMINTERVAL('15-11');
      to_yminterval   
    ------------------
     15 years 11 mons
    (1 row)
    
    MogDB=# SELECT TO_YMINTERVAL('P15Y11M');
      to_yminterval   
    ------------------
     15 years 11 mons
    (1 row)
    ```

- TZ_OFFSET(var) 

    描述：返回入参地址的时区偏移量。

    返回值类型：interval

    示例：

    ```sql
    MogDB=# SELECT TZ_OFFSET('Asia/Hong_Kong') FROM dual;
     tz_offset 
    -----------
     08:00:00
    (1 row)
    
    MogDB=# SELECT TZ_OFFSET('+14:00') FROM dual;
     tz_offset 
    -----------
     14:00
    (1 row)
    ```

- smalldatetime_ge

    描述：判断是否第一个参数大于等于第二个参数。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean

- smalldatetime_cmp

    描述：对比smalldatetime是否相等。

    参数：smalldatetime, smalldatetime

    返回值类型：integer

- smalldatetime_eq

    描述：对比smalldatetime是否相等。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean。

- smalldatetime_gt

    描述：判断是否第一个参数大于第二个参数。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean

- smalldatetime_hash

    描述：计算timestamp对应的哈希值。

    参数：smalldatetime

    返回值类型：integer

- smalldatetime_in

    描述：输入timestamp。

    参数：cstring, oid, integer

    返回值类型：smalldatetime

- smalldatetime_larger

    描述：返回较大的timestamp。

    参数：smalldatetime, smalldatetime

    返回值类型：smalldatetime

- smalldatetime_le

    描述：判断是否第一个参数小于等于第二个参数。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean

- smalldatetime_lt

    描述：判断是否第一个参数小于第二个参数。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean

- smalldatetime_ne

    描述：比较两个timestamp是否不相等。

    参数：smalldatetime, smalldatetime

    返回值类型：boolean

- smalldatetime_out

    描述：timestamp转换为外部形式。

    参数：smalldatetime

    返回值类型：cstring

- smalldatetime_send

    描述：timestamp转换为二进制格式。

    参数：smalldatetime

    返回值类型：bytea

- smalldatetime_smaller

    描述：返回较小的一个smalldatetime。

    参数：smalldatetime, smalldatetime

    返回值类型：smalldatetime

- smalldatetime_to_abstime

    描述：smalldatetime转换为abstime。

    参数：smalldatetime

    返回值类型：abstime

- smalldatetime_to_time

    描述：smalldatetime转换为time。

    参数：smalldatetime

    返回值类型：time without time zone

- smalldatetime_to_timestamp

    描述：smalldatetime转换为timestamp。

    参数：smalldatetime

    返回值类型：timestamp without time zone

- smalldatetime_to_timestamptz

    描述：smalldatetime转换为timestamptz。

    参数：smalldatetime

    返回值类型：timestamp with time zone

- smalldatetime_to_varchar2

    描述：smalldatetime转换为varchar2。

    参数：smalldatetime

    返回值类型：character varying

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：**
    >
    > 获取当前时间有多种方式，请根据实际业务从场景选择合适的接口：
    >
    > 1. 以下接口按照当前事务的开始时刻返回值：
    >
    >    ```sql
    >    CURRENT_DATE CURRENT_TIME CURRENT_TIMESTAMP CURRENT_TIME(precision) CURRENT_TIMESTAMP(precision) LOCALTIME LOCALTIMESTAMP LOCALTIME(precision) LOCALTIMESTAMP(precision)
    >    ```
    >
    >    其中CURRENT_TIME和CURRENT_TIMESTAMP传递带有时区的值；LOCALTIME和LOCALTIMESTAMP传递的值不带时区。CURRENT_TIME、CURRENT_TIMESTAMP、LOCALTIME和 LOCALTIMESTAMP可以有选择地接受一个精度参数， 该精度导致结果的秒域被园整为指定小数位。如果没有精度参数，结果将被给予所能得到的全部精度。
    >
    >    因为这些函数全部都按照当前事务的开始时刻返回结果，所以它们的值在事务运行的整个期间内都不改变。 我们认为这是一个特性：目的是为了允许一个事务在“当前”时间上有一致的概念， 这样在同一个事务里的多个修改可以保持同样的时间戳。
    >
    > 2. 以下接口返回当前语句开始时间：
    >
    >    ```sql
    >    transaction_timestamp() statement_timestamp() now()
    >    ```
    >
    >    其中transaction_timestamp()等价于CURRENT_TIMESTAMP，但是其命名清楚地反映了它的返回值。statement_timestamp()返回当前语句的开始时刻（更准确的说是收到 客户端最后一条命令的时间）。statement_timestamp()和transaction_timestamp()在一个事务的第一条命令期间返回值相同，但是在随后的命令中却不一定相同。
    >
    >    now()等效于transaction_timestamp()。
    >
    > 3. 以下接口返回函数被调用时的真实当前时间：
    >
    >    ```sql
    >    clock_timestamp() timeofday()
    >    ```
    >
    >    clock_timestamp()返回真正的当前时间，因此它的值甚至在同一条 SQL 命令中都会变化。timeofday()和clock_timestamp()相似，timeofday()也返回真实的当前时间，但是它的结果是一个格式化的text串，而不是timestamp with time zone值。

## TIMESTAMPDIFF

- **TIMESTAMPDIFF(unit , timestamp_expr1, timestamp_expr2)**

timestampdiff函数是计算两个日期时间之间(timestamp_expr2-timestamp_expr1)的差值，并以unit形式返回结果。timestamp_expr1，timestamp_expr2必须是一个timestamp、timestamptz、date类型的值表达式。unit表示的是两个日期差的单位。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> 该函数仅在MogDB兼容MY类型时（即dbcompatibility = 'B'）有效，其他类型不支持该函数。

- year

  年份。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(YEAR, '2018-01-01', '2020-01-01');
   timestamp_diff
  ----------------
                2
  (1 row)
  ```

- quarter

  季度。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(QUARTER, '2018-01-01', '2020-01-01');
   timestamp_diff
  ----------------
                8
  (1 row)
  ```

- month

  月份。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(MONTH, '2018-01-01', '2020-01-01');
   timestamp_diff
  ----------------
               24
  (1 row)
  ```

- week

  星期。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(WEEK, '2018-01-01', '2020-01-01');
   timestamp_diff
  ----------------
              104
  (1 row)
  ```

- day

  天。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(DAY, '2018-01-01', '2020-01-01');
   timestamp_diff
  ----------------
              730
  (1 row)
  ```

- hour

  小时。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(HOUR, '2020-01-01 10:10:10', '2020-01-01 11:11:11');
   timestamp_diff
  ----------------
                1
  (1 row)

  ```

- minute

  分钟。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(MINUTE, '2020-01-01 10:10:10', '2020-01-01 11:11:11');
   timestamp_diff
  ----------------
               61
  (1 row)

  ```

- second

  秒。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(SECOND, '2020-01-01 10:10:10', '2020-01-01 11:11:11');
   timestamp_diff
  ----------------
             3661
  (1 row)

  ```

- microseconds

  秒域（包括小数部分）乘以1,000,000。

  ```sql
  MogDB=#  SELECT TIMESTAMPDIFF(MICROSECOND, '2020-01-01 10:10:10.000000', '2020-01-01 10:10:10.111111');
   timestamp_diff
  ----------------
           111111
  (1 row)

  ```

- timestamp_expr含有时区

    ```sql
    MogDB=# SELECT TIMESTAMPDIFF(HOUR,'2020-05-01 10:10:10-01','2020-05-01 10:10:10-03');
     timestamp_diff
    ----------------
                  2
    (1 row)
    ```

## EXTRACT

- **EXTRACT**(field **FROM** source)

  extract函数从日期或时间的数值里抽取子域，比如年、小时等。source必须是一个timestamp、time或interval类型的值表达式（类型为date的表达式转换为timestamp，因此也可以用）。field是一个标识符或者字符串，它指定从源数据中抽取的域。extract函数返回类型为double precision的数值。field的取值范围如下所示。

- century

  世纪。

  第一个世纪从0001-01-01 00:00:00 AD开始。这个定义适用于所有使用阳历的国家。没有0世纪，直接从公元前1世纪到公元1世纪。

  示例：

  ```sql
  MogDB=# SELECT EXTRACT(CENTURY FROM TIMESTAMP '2000-12-16 12:21:13');
   date_part
  -----------
          20
  (1 row)
  ```

- day

  - 如果source为timestamp，表示月份里的日期（1-31）。

    ```sql
    MogDB=# SELECT EXTRACT(DAY FROM TIMESTAMP '2001-02-16 20:38:40');
     date_part
    -----------
            16
    (1 row)
    ```

  - 如果source为interval，表示天数。

    ```sql
    MogDB=# SELECT EXTRACT(DAY FROM INTERVAL '40 days 1 minute');
     date_part
    -----------
            40
    (1 row)
    ```

- decade

  年份除以10。

  ```sql
  MogDB=# SELECT EXTRACT(DECADE FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
         200
  (1 row)
  ```

- dow

  每周的星期几，星期天（0）到星期六（6）。

  ```sql
  MogDB=# SELECT EXTRACT(DOW FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
           5
  (1 row)
  ```

- doy

  一年的第几天（1~365/366）。

  ```sql
  MogDB=# SELECT EXTRACT(DOY FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
          47
  (1 row)
  ```

- epoch

  - 如果source为timestamp with time zone，表示自1970-01-01 00:00:00-00 UTC以来的秒数（结果可能是负数）；

    如果source为date和timestamp，表示自1970-01-01 00:00:00-00当地时间以来的秒数；

    如果source为interval，表示时间间隔的总秒数。

    ```sql
    MogDB=# SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '2001-02-16 20:38:40.12-08');
      date_part
    --------------
     982384720.12
    (1 row)
    ```

    ```sql
    MogDB=# SELECT EXTRACT(EPOCH FROM INTERVAL '5 days 3 hours');
     date_part
    -----------
        442800
    (1 row)
    ```

  - 将epoch值转换为时间戳的方法。

    ```sql
    MogDB=# SELECT TIMESTAMP WITH TIME ZONE 'epoch' + 982384720.12 * INTERVAL '1 second' AS RESULT;
              result
    ---------------------------
     2001-02-17 12:38:40.12+08
    (1 row)
    ```

- hour

  小时域（0-23）。

  ```sql
  MogDB=# SELECT EXTRACT(HOUR FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
          20
  (1 row)
  ```

- isodow

  一周的第几天（1-7）。

  星期一为1，星期天为7。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
  > 除了星期天外，都与dow相同。

  ```sql
  MogDB=# SELECT EXTRACT(ISODOW FROM TIMESTAMP '2001-02-18 20:38:40');
   date_part
  -----------
           7
  (1 row)
  ```

- isoyear

  日期中的ISO 8601标准年（不适用于间隔）。

  每个带有星期一开始的周中包含1月4日的ISO年，所以在年初的1月或12月下旬的ISO年可能会不同于阳历的年。详细信息请参见后续的week描述。

  ```sql
  MogDB=# SELECT EXTRACT(ISOYEAR FROM DATE '2006-01-01');
   date_part
  -----------
        2005
  (1 row)
  ```

  ```sql
  MogDB=# SELECT EXTRACT(ISOYEAR FROM DATE '2006-01-02');
   date_part
  -----------
        2006
  (1 row)
  ```

- microseconds

  秒域（包括小数部分）乘以1,000,000。

  ```sql
  MogDB=# SELECT EXTRACT(MICROSECONDS FROM TIME '17:12:28.5');
   date_part
  -----------
    28500000
  (1 row)
  ```

- millennium

  千年。

  20世纪（19xx年）里面的年份在第二个千年里。第三个千年从2001年1月1日零时开始。

  ```sql
  MogDB=# SELECT EXTRACT(MILLENNIUM FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
           3
  (1 row)
  ```

- milliseconds

  秒域（包括小数部分）乘以1000。请注意它包括完整的秒。

  ```sql
  MogDB=# SELECT EXTRACT(MILLISECONDS FROM TIME '17:12:28.5');
   date_part
  -----------
       28500
  (1 row)
  ```

- minute

  分钟域（0-59）。

  ```sql
  MogDB=# SELECT EXTRACT(MINUTE FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
          38
  (1 row)
  ```

- month

  如果source为timestamp，表示一年里的月份数（1-12）。

  ```sql
  MogDB=# SELECT EXTRACT(MONTH FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
           2
  (1 row)
  ```

  如果source为interval，表示月的数目，然后对12取模（0-11）。

  ```sql
  MogDB=# SELECT EXTRACT(MONTH FROM INTERVAL '2 years 13 months');
   date_part
  -----------
           1
  (1 row)
  ```

- quarter

  该天所在的该年的季度（1-4）。

  ```sql
  MogDB=# SELECT EXTRACT(QUARTER FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
           1
  (1 row)
  ```

- second

  秒域，包括小数部分（0-59）。

  ```sql
  MogDB=# SELECT EXTRACT(SECOND FROM TIME '17:12:28.5');
   date_part
  -----------
        28.5
  (1 row)
  ```

- timezone

  与UTC的时区偏移量，单位为秒。正数对应UTC东边的时区，负数对应UTC西边的时区。

- timezone_hour

  时区偏移量的小时部分。

- timezone_minute

  时区偏移量的分钟部分。

- week

  该天在所在的年份里是第几周。ISO 8601定义一年的第一周包含该年的一月四日（ISO-8601 的周从星期一开始）。换句话说，一年的第一个星期四在第一周。

  在ISO定义里，一月的头几天可能是前一年的第52或者第53周，十二月的后几天可能是下一年第一周。比如，2005-01-01是2004年的第53周，而2006-01-01是2005年的第52周，2012-12-31是2013年的第一周。建议isoyear字段和week一起使用以得到一致的结果。

  ```sql
  MogDB=# SELECT EXTRACT(WEEK FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
           7
  (1 row)
  ```

- year

  年份域。

  ```sql
  MogDB=# SELECT EXTRACT(YEAR FROM TIMESTAMP '2001-02-16 20:38:40');
   date_part
  -----------
        2001
  (1 row)
  ```

## date_part

date_part函数是在传统的Ingres函数的基础上制作的（该函数等效于SQL标准函数extract）：

- **date_part('field', source)**

这里的field参数必须是一个字符串，而不是一个名称。有效的field与extract一样，详细信息请参见[EXTRACT](#EXTRACT)。

示例：

```sql
MogDB=# SELECT date_part('day', TIMESTAMP '2001-02-16 20:38:40');
 date_part
-----------
        16
(1 row)
MogDB=# SELECT date_part('hour', INTERVAL '4 hours 3 minutes');
 date_part
-----------
         4
(1 row)
```

表2显示了可以用于格式化日期和时间值的模版。

**表 2** 用于日期/时间格式化的模式<a id="用于"> </a>

| 类别     | 模式                  | 描述                                                         |
| -------- | --------------------- | ------------------------------------------------------------ |
| 小时     | HH                    | 一天的小时数（01-12）                                        |
|          | HH12                  | 一天的小时数（01-12）                                        |
|          | HH24                  | 一天的小时数（00-23）                                        |
| 分钟     | MI                    | 分钟（00-59）                                                |
| 秒       | SS                    | 秒（00-59）                                                  |
|          | FF                    | 微秒（000000-999999）                                        |
|          | SSSSS                 | 午夜后的秒（0-86399）                                        |
| 上、下午 | AM或A.M.              | 上午标识                                                     |
|          | PM或P.M.              | 下午标识                                                     |
| 年       | Y,YYY                 | 带逗号的年（4和更多位）                                      |
|          | SYYYY                 | 公元前四位年                                                 |
|          | YYYY                  | 年（4和更多位）                                              |
|          | YYY                   | 年的后三位                                                   |
|          | YY                    | 年的后两位                                                   |
|          | Y                     | 年的最后一位                                                 |
|          | IYYY                  | ISO年（4位或更多位）                                         |
|          | IYY                   | ISO年的最后三位                                              |
|          | IY                    | ISO年的最后两位                                              |
|          | I                     | ISO年的最后一位                                              |
|          | RR                    | 年的后两位（可在21世纪存储20世纪的年份）                     |
|          | RRRR                  | 可接收4位年或两位年。若是两位，则和RR的返回值相同，若是四位，则和YYYY相同。 |
|          | BC或B.C.<br/>AD或A.D. | 纪元标识。BC（公元前），AD（公元后）。                       |
| 月       | MONTH                 | 全长大写月份名（空白填充为9字符）                            |
|          | MON                   | 大写缩写月份名（3字符）                                      |
|          | MM                    | 月份数（01-12）                                              |
|          | RM                    | 罗马数字的月份（I-XII ；I=JAN）（大写）                      |
| 天       | DAY                   | 全长大写日期名（空白填充为9字符）                            |
|          | DY                    | 缩写大写日期名（3字符）                                      |
|          | DDD                   | 一年里的日（001-366）                                        |
|          | DD                    | 一个月里的日（01-31）                                        |
|          | D                     | 一周里的日（1-7 ；周日是 1）                                 |
| 周       | W                     | 一个月里的周数（1-5）（第一周从该月第一天开始）              |
|          | WW                    | 一年里的周数（1-53）（第一周从该年的第一天开始）             |
|          | IW                    | ISO一年里的周数（第一个星期四在第一周里）                    |
| 世纪     | CC                    | 世纪（2位）（21 世纪从 2001-01-01 开始）                     |
| 儒略日   | J                     | 儒略日（自公元前 4712 年 1 月 1 日来的天数）                 |
| 季度     | Q                     | 季度                                                         |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：**
>
> 上表中RR计算年的规则如下：
>
> - 输入的两位年份在00~49之间：
>   
>   当前年份的后两位在00~49之间，返回值年份的前两位和当前年份的前两位相同；
>   
>   当前年份的后两位在50~99之间，返回值年份的前两位是当前年份的前两位加1。
>   
> - 输入的两位年份在50~99之间：
>   
>   当前年份的后两位在00\~49之间，返回值年份的前两位是当前年份的前两位减1；
>   
>   当前年份的后两位在50~99之间，返回值年份的前两位和当前年份的前两位相同。
