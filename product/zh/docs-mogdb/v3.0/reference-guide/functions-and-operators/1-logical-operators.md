---
title: 逻辑操作符
summary: 逻辑操作符
author: Zhang Cuiping
date: 2021-04-20
---

# 逻辑操作符

常用的逻辑操作符有AND、OR和NOT，他们的运算结果有三个值，分别为TRUE、FALSE和NULL，其中NULL代表未知。他们运算优先级顺序为: NOT&gt;AND&gt;OR。

运算规则请参见[表1](#运算规则表)，表中的a和b代表逻辑表达式。

**表 1** 运算规则表<a id="运算规则表"> </a>

| a     | b     | a **AND** b的结果 | a **OR** b的结果 | **NOT** a的结果 |
| :---- | :---- | :---------------- | :--------------- | :-------------- |
| TRUE  | TRUE  | TRUE              | TRUE             | FALSE           |
| TRUE  | FALSE | FALSE             | TRUE             | FALSE           |
| TRUE  | NULL  | NULL              | TRUE             | FALSE           |
| FALSE | FALSE | FALSE             | FALSE            | TRUE            |
| FALSE | NULL  | FALSE             | NULL             | TRUE            |
| NULL  | NULL  | NULL              | NULL             | NOT NULL        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
> 操作符AND和OR具有交换性，即交换左右两个操作数，不影响其结果。
