---
title: 数组函数和操作符
summary: 数组函数和操作符
author: Zhang Cuiping
date: 2021-04-20
---

# 数组函数和操作符

## 数组操作符

- =

  描述: 两个数组是否相等

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1.1,2.1,3.1]::int[] = ARRAY[1,2,3] AS RESULT ;
   result
  --------
   t
  (1 row)
  ```

- &lt;&gt;

  描述: 两个数组是否不相等

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,2,3] <> ARRAY[1,2,4] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &lt;

  描述: 一个数组是否小于另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,2,3] < ARRAY[1,2,4] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &gt;

  描述: 一个数组是否大于另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,4,3] > ARRAY[1,2,4] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &lt;=

  描述: 一个数组是否小于或等于另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,2,3] <= ARRAY[1,2,3] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &gt;=

  描述: 一个数组是否大于或等于另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,4,3] >= ARRAY[1,4,3] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- @&gt;

  描述: 一个数组是否包含另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,4,3] @> ARRAY[3,1] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &lt;@

  描述: 一个数组是否被包含于另一个数组

  示例:

  ```sql
  MogDB=# SELECT ARRAY[2,7] <@ ARRAY[1,7,4,2,6] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- &&

  描述: 一个数组是否和另一个数组重叠（有共同元素）

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,4,3] && ARRAY[2,1] AS RESULT;
   result
  --------
   t
  (1 row)
  ```

- ||

  描述: 数组与数组进行连接

  示例:

  ```sql
  MogDB=# SELECT ARRAY[1,2,3] || ARRAY[4,5,6] AS RESULT;
      result
  ---------------
   {1,2,3,4,5,6}
  (1 row)
  ```

  ```sql
  MogDB=# SELECT ARRAY[1,2,3] || ARRAY[[4,5,6],[7,8,9]] AS RESULT;
            result
  ---------------------------
   {{1,2,3},{4,5,6},{7,8,9}}
  (1 row)
  ```

- ||

  描述: 元素与数组进行连接

  示例:

  ```sql
  MogDB=# SELECT 3 || ARRAY[4,5,6] AS RESULT;
    result
  -----------
   {3,4,5,6}
  (1 row)
  ```

- ||

  描述: 数组与元素进行连接

  示例:

  ```sql
  MogDB=# SELECT ARRAY[4,5,6] || 7 AS RESULT;
    result
  -----------
   {4,5,6,7}
  (1 row)
  ```

数组比较是使用默认的B-tree比较函数对所有元素逐一进行比较的。多维数组的元素按照行顺序进行访问。如果两个数组的内容相同但维数不等，决定排序顺序的首要因素是维数。

## 数组函数

- array_append(anyarray, anyelement)

  描述: 向数组末尾添加元素，只支持一维数组。

  返回类型: anyarray

  示例:

  ```sql
  MogDB=# SELECT array_append(ARRAY[1,2], 3) AS RESULT;
   result
  ---------
   {1,2,3}
  (1 row)
  ```

- array_prepend(anyelement, anyarray)

  描述: 向数组开头添加元素，只支持一维数组。

  返回类型: anyarray

  示例:

  ```sql
  MogDB=# SELECT array_prepend(1, ARRAY[2,3]) AS RESULT;
   result
  ---------
   {1,2,3}
  (1 row)
  ```

- array_cat(anyarray, anyarray)

  描述: 连接两个数组，支持多维数组。

  返回类型: anyarray

  示例:

  ```sql
  MogDB=# SELECT array_cat(ARRAY[1,2,3], ARRAY[4,5]) AS RESULT;
     result
  -------------
   {1,2,3,4,5}
  (1 row)

  MogDB=# SELECT array_cat(ARRAY[[1,2],[4,5]], ARRAY[6,7]) AS RESULT;
         result
  ---------------------
   {{1,2},{4,5},{6,7}}
  (1 row)
  ```

- array\_union\(anyarray, anyarray\)

    描述：连接两个数组，只支持一维数组。

    返回类型：anyarray

    示例：

    ```
    MogDB=# SELECT array_union(ARRAY[1,2,3], ARRAY[3,4,5]) AS RESULT;
       result    
    -------------
     {1,2,3,3,4,5}
    (1 row)
    ```

- array\_union\_distinct\(anyarray, anyarray\)

    描述：连接两个数组，并去重，只支持一维数组。

    返回类型：anyarray

    示例：

    ```
    MogDB=# SELECT array_union_distinct(ARRAY[1,2,3], ARRAY[3,4,5]) AS RESULT;
       result    
    -------------
     {1,2,3,4,5}
    (1 row)
    ```
    
- array_intersect(anyarray, anyarray)

    描述：两个数组取交集，只支持一维数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_intersect(ARRAY[1,2,3], ARRAY[3,4,5]) AS RESULT;
       result
    -------------
     {3}
    (1 row)
    ```

- array_intersect_distinct(anyarray, anyarray)

    描述：两个数组取交集，并去重，只支持一维数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_intersect_distinct(ARRAY[1,2,2], ARRAY[2,2,4,5]) AS RESULT;
       result
    -------------
     {2}
    (1 row)
    ```

- array_except(anyarray, anyarray)

    描述：两个数组取差，只支持一维数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_except(ARRAY[1,2,3], ARRAY[3,4,5]) AS RESULT;
       result
    -------------
     {1,2}
    (1 row)
    ```

- array_except_distinct(anyarray, anyarray)

    描述：两个数组取差，并去重，只支持一维数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_except_distinct(ARRAY[1,2,2,3], ARRAY[3,4,5]) AS RESULT;
       result
    -------------
     {1,2}
    (1 row)
    ```

- array_ndims(anyarray)

  描述: 返回数组的维数。

  返回类型: int

  示例:

  ```sql
  MogDB=# SELECT array_ndims(ARRAY[[1,2,3], [4,5,6]]) AS RESULT;
   result
  --------
        2
  (1 row)
  ```

- array_dims(anyarray)

  描述: 返回数组各个维度中的低位下标值和高位下标值。

  返回类型: text

  示例:

  ```sql
  MogDB=# SELECT array_dims(ARRAY[[1,2,3], [4,5,6]]) AS RESULT;
     result
  ------------
   [1:2][1:3]
  (1 row)
  ```

- array_length(anyarray, int)

  描述: 返回指定数组维度的长度。int为指定数组维度。

  返回类型: int

  示例:

  ```sql
  MogDB=# SELECT array_length(array[1,2,3], 1) AS RESULT;
   result
  --------
        3
  (1 row)

  MogDB=# SELECT array_length(array[[1,2,3],[4,5,6]], 2) AS RESULT;
   result
  --------
        3
  (1 row)
  ```

- array_lower(anyarray, int)

  描述: 返回指定数组维数的下界。int为指定数组维度。

  返回类型: int

  示例:

  ```sql
  MogDB=# SELECT array_lower('[0:2]={1,2,3}'::int[], 1) AS RESULT;
   result
  --------
        0
  (1 row)
  ```

- array_upper(anyarray, int)

  描述: 返回指定数组维数的上界。int为指定数组维度。

  返回类型: int

  示例:

  ```sql
  MogDB=# SELECT array_upper(ARRAY[1,8,3,7], 1) AS RESULT;
   result
  --------
        4
  (1 row)
  ```

- array_remove(anyarray, anyelement)

    描述：移除数组中的所有指定元素。仅支持一维数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_remove(ARRAY[1,8,8,7], 8) AS RESULT;
     result
    --------
     {1,7}
    (1 row)
    ```

- array_to_string(anyarray, text [, text])

  描述: 使用第一个text作为数组的新分隔符，使用第二个text替换数组值为null的值。

  返回类型: text

  示例:

  ```sql
  MogDB=# SELECT array_to_string(ARRAY[1, 2, 3, NULL, 5], ',', '*') AS RESULT;
    result
  -----------
   1,2,3,*,5
  (1 row)
  ```

- array_delete(anyarray)

    描述：清空数组中的元素并返回一个同类型的空数组。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_delete(ARRAY[1,8,3,7]) AS RESULT;
     result
    --------
     {}
    (1 row)
    ```

- array\_deleteidx\(anyarray, int\)

    描述：从数组中删除指定下标的元素并返回剩余元素组成的数组。

    返回类型：anyarray

    示例：

    ```
    MogDB=# SELECT array_deleteidx(ARRAY[1,2,3,4,5], 1) AS RESULT;
      result
    -----------
     {2,3,4,5}
    (1 row)
    ```

- array_extendnull(anyarray, int)

    描述：往数组尾部添加指定个数的null空元素。

    返回类型：anyarray

    示例：

    ```sql
    MogDB=# SELECT array_extendnull(ARRAY[1,8,3,7],1) AS RESULT;
        result
    --------------
    {1,8,3,7,null}
    (1 row)
    ```

- array\_trim\(anyarray, int\)

    描述：从数组尾部删除指定个数个元素。

    返回类型：anyarray

    示例：

    ```
    MogDB=# SELECT array_trim(ARRAY[1,8,3,7],1) AS RESULT;
     result
    ---------
     {1,8,3}
    (1 row)
    ```

- array_exists(anyarray, int)

    描述：检查第二个参数是否是数组的合法下标。

    返回类型：boolean

    示例：

    ```sql
    MogDB=# SELECT array_exists(ARRAY[1,8,3,7],1) AS RESULT;
     result
    --------
     t
    (1 row)
    ```

- array_next(anyarray, int)

    描述：根据第二个入参返回数组中指定下标元素的下一个元素的下标。

    返回类型：int

    示例：

    ```sql
    MogDB=# SELECT array_next(ARRAY[1,8,3,7],1) AS RESULT;
     result
    --------
          2
    (1 row)
    ```

- array_prior(anyarray, int)

    描述：根据第二个入参返回数组中指定下标元素的上一个元素的下标。

    返回类型：int

    示例：

    ```sql
    MogDB=# SELECT array_prior(ARRAY[1,8,3,7],2) AS RESULT;
     result
    --------
      1
    (1 row)
    ```

- string_to_array(text, text [, text])

  描述: 使用第二个text指定分隔符，使用第三个可选的text作为NULL值替换模板，如果分隔后的子串与第三个可选的text完全匹配，则将其替换为NULL。

  返回类型: text[]

  示例:

  ```sql
  MogDB=# SELECT string_to_array('xx~^~yy~^~zz', '~^~', 'yy') AS RESULT;
      result
  --------------
   {xx,NULL,zz}
  (1 row)
  MogDB=# SELECT string_to_array('xx~^~yy~^~zz', '~^~', 'y') AS RESULT;
     result
  ------------
   {xx,yy,zz}
  (1 row)
  ```

- unnest(anyarray)

  描述: 扩大一个数组为一组行。

  返回类型: setof anyelement

  示例:

  ```sql
  MogDB=# SELECT unnest(ARRAY[1,2]) AS RESULT;
   result
  --------
        1
        2
  (2 rows)
  ```

在string_to_array中，如果分隔符参数是NULL，输入字符串中的每个字符将在结果数组中变成一个独立的元素。如果分隔符是一个空白字符串，则整个输入的字符串将变为一个元素的数组。否则输入字符串将在每个分隔字符串处分开。

在string_to_array中，如果省略null字符串参数或为NULL，将字符串中没有输入内容的子串替换为NULL。

在array_to_string中，如果省略null字符串参数或为NULL，运算中将跳过在数组中的任何null元素，并且不会在输出字符串中出现。
