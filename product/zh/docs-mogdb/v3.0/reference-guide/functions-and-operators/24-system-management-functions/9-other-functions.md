---
title: 其它函数
summary: 其它函数
author: Zhang Cuiping
date: 2021-04-20
---

# 其它函数

- plan_seed()

  描述：获取前一次查询语句的seed值（内部使用）。

  返回值类型：int

- pg_stat_get_env()

  描述：获取当前节点的环境变量信息，仅sysadmin和monitor admin可以访问。

  返回值类型：record

  示例：

  ```sql
  MogDB=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/GaussDB_Kernel_TRUNK/install,/data1/GaussDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  描述：清理节点上无人使用的全局计划缓存。

  返回值类型：bool

- pg_catalog.plancache_status()

  描述：显示节点上全局计划缓存的信息，函数返回信息和GLOBAL_PLANCACHE_STATUS一致。

  返回值类型：record

- textlen(text)

  描述：提供查询text的逻辑长度的方法。

  返回值类型：int

- threadpool_status()

  描述：显示线程池中工作线程及会话的状态信息。

  返回值类型：record

- get_local_active_session()

  描述：提供当前节点保存在内存中的历史活跃session状态的采样记录。

  返回值类型：record

- pg_stat_get_thread()

    描述：提供当前节点下所有线程的状态信息，sysadmin和monitor admin用户可以查看所有线程信息，普通用户查看本用户的线程信息。

    返回值类型：record

- pg_stat_get_sql_count()

    描述：提供当前节点中用户执行的SELECT/UPDATE/INSERT/DELETE/MERGE INTO语句的计数结果，sysadmin和monitor admin用户可以查看所有用户的信息，普通用户查看本用户的统计信息。

    返回值类型：record

- pg_stat_get_data_senders()

  描述：提供当前活跃的数据复制发送线程的详细信息。

  返回值类型：record

- get_wait_event_info()

    描述：提供wait event事件的具体信息。

    返回值类型：record

- generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring)

  描述：基于两个snapshot生成系统诊断报告。需要在postgres库下执行，默认初始化用户或monadmin用户可以访问。只可在系统库中查询到结果，用户库中无法查询。

  返回值类型：record

  **表 1** generate_wdr_report 参数说明

  | 参数          | 说明                                                         | 取值范围                                                     |
  | ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | begin_snap_id | 生成某段时间内性能诊断报告的开始snapshotid。                 | -                                                            |
  | end_snap_id   | 结束snapshot的id，默认end_snap_id大于begin_snap_id。         | -                                                            |
  | report_type   | 指定生成report的类型。                                       | - summary<br/>- detail<br/>- all，即同时包含summary 和 detail。 |
  | report_scope  | 指定生成report的范围。                                       | - cluster：数据库级别的信息<br/>- node：节点级别的信息       |
  | node_name     | 在report_scope指定为node时，需要把该参数指定为对应节点的名称。（节点名称可以执行select * from pg_node_env;查询）。<br/>在report_scope为cluster时，该值可以省略或者指定为空或NULL。 | - cluster：省略/空/NULL<br/>- node： MogDB中的节点名称       |

- create_wdr_snapshot()

  描述：手工生成系统诊断快照，该函数需要sysadmin权限。

  返回值类型：text

- kill_snapshot()

    描述：kill后台的WDR snapshot线程，调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。

    返回值类型：void

- capture_view_to_json(text,integer)

    描述：将视图的结果存入GUC: perf_directory所指定的目录，如果is_crossdb为1，则表示对于所有的database都会访问一次view；如果is_crossdb为0，则表示仅对当前database进行一次视图访问。该函数只有sysadmin和monitor admin用户可以执行。

    返回值类型：int

- reset_unique_sql

  描述：用来清理数据库节点内存中的Unique SQL（需要sysadmin权限）。

  返回值类型：bool

  **表 2** reset_unique_sql参数说明

  | 参数        | 类型 | 描述                                                         |
  | :---------- | :--- | :----------------------------------------------------------- |
  | scope       | text | 清理范围类型：<br/>- 'GLOBAL'：清理所有的节点，如果是'GLOBAL'，则只可以为主节点执行此函数。<br/>- 'LOCAL'：清理本节点。 |
  | clean_type  | text | - 'BY_USERID'：按用户ID来进行清理Unique SQL。<br/>- 'BY_CNID'：按主节点的ID来进行清理Unique SQL。<br/>- 'ALL'：全部清理。 |
  | clean_value | int8 | 具体清理type对应的清理值。                                   |

  > **须知**:
  >
  > - scope的取值GLOBAL和LOCAL针对分布式，对于MogDB而言两者意义相同，均表示清理本节点。
  > - clean_type的值BY_CNID仅针对分布式，对于MogDB无效。

- wdr_xdb_query(db_name_str text, query text)

  描述：提供本地跨数据库执行query的能力。例如: 在连接到mogdb库时, 访问test库下的表。

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  返回值类型：record

- pg_wlm_jump_queue(pid int)

  描述：调整任务到数据库主节点队列的最前端。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- gs_wlm_switch_cgroup(pid int, cgroup text)

  描述：调整作业的优先级到新控制组。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

  描述：将线程tid的MemoryContextName内存上下文信息记录到“\$GAUSSLOG/pg_log/${node_name}/dumpmem”目录下的“threadid_timestamp.log”文件中。其中threadid可通过视图GS_SESSION_MEMORY_DETAIL中的sessid后获得。在正式发布的版本中仅接受MemoryContextName为空串（两个单引号表示输入为空串，即”）的输入，此时会记录所有的内存上下文信息，否则不会有任何操作。对供内部开发人员和测试人员调试用的DEBUG版本，可以指定需要统计的MemoryContextName，此时会将该Context所有的内存使用情况记录到指定文件。该函数需要管理员权限的用户才能执行。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- pg_shared_memctx_detail(MemoryContextName text)

  描述：将MemoryContextName内存上下文信息记录到“\$GAUSSLOG/pg_log/${node_name}/dumpmem”目录下的“threadid_timestamp.log”文件中。该函数功能仅在DEBUG版本中供内部开发人员和测试人员调试使用，在正式发布版本中调用该函数不会有任何操作。该函数需要管理员权限的用户才能执行。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- local_bgwriter_stat()

  描述：显示本实例的bgwriter线程刷页信息，候选buffer链中页面个数，buffer淘汰信息。

  返回值类型：record

- local_candidate_stat()

    描述：显示本实例的候选buffer链中页面个数，buffer淘汰信息，包含normal buffer pool和segment buffer pool。

    返回值类型：record

- local_ckpt_stat()

  描述：显示本实例的检查点信息和各类日志刷页情况。

  返回值类型：record

- local_double_write_stat()

  描述：显示本实例的双写文件的情况。

  返回值类型：record

  **表 3** local_double_write_stat参数说明

  | 参数                  | 类型 | 描述                                                         |
  | :-------------------- | :--- | :----------------------------------------------------------- |
  | node_name             | text | 实例名称。                                                   |
  | curr_dwn              | int8 | 当前双写文件的序列号。                                       |
  | curr_start_page       | int8 | 当前双写文件恢复起始页面。                                   |
  | file_trunc_num        | int8 | 当前双写文件复用的次数。                                     |
  | file_reset_num        | int8 | 当前双写文件写满后发生重置的次数。                           |
  | total_writes          | int8 | 当前双写文件总的I/O次数。                                    |
  | low_threshold_writes  | int8 | 低效率写双写文件的I/O次数（一次I/O刷页数量少于16页面）。     |
  | high_threshold_writes | int8 | 高效率写双写文件的I/O次数（一次I/O刷页数量多于一批，421个页面）。 |
  | total_pages           | int8 | 当前刷页到双写文件区的总的页面个数。                         |
  | low_threshold_pages   | int8 | 低效率刷页的页面个数。                                       |
  | high_threshold_pages  | int8 | 高效率刷页的页面个数。                                       |
  | file_id               | int8 | 当前双写文件的id号。                                         |

- local_single_flush_dw_stat()

  描述：显示本实例的单页面淘汰双写文件的情况。

  返回值类型：record

- local_pagewriter_stat()

  描述：显示本实例的刷页信息和检查点信息。

  返回值类型：record

- local_redo_stat()

  描述：显示本实例的备机的当前回放状态。

  返回值类型：record

  备注：返回的回放状态主要包括当前回放位置，回放最小恢复点位置等信息。

- local_recovery_status()

  描述：显示本实例的主机和备机的日志流控信息。

  返回值类型：record

- gs_wlm_node_recover(boolean isForce)

  描述：获取当前内存中记录的TopSQL查询语句级别相关统计信息，当传入的参数不为0时，会将这部分信息从内存中清理掉。

  返回值类型：record

- gs_wlm_node_clean(cstring nodename)

  描述：动态负载管理节点故障后做数据清理操作。该函数只有管理员用户可以执行，属于数据库实例管理模块调用的，不建议用户直接调用。该视图在集中式和单机环境上不支持。

  返回值类型：bool

- gs_cgroup_map_ng_conf(group name)

  描述：读取指定逻辑数据库的cgroup配置文件。

  返回值类型：record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  描述：切换指定会话的控制组。

  返回值类型：record

- hdfs_fdw_handler()

  描述：用于外表重写功能，定义外表时需要定义的函数。

  返回值类型：record

- hdfs_fdw_validator(text[], oid)

  描述：用于外表重写功能，定义外表时需要定义的函数。

  返回值类型：record

- comm_client_info()

  描述：用于查询单个节点活跃的客户端连接信息。

  返回值类型：setof record

- pg_sync_cstore_delta(text)

  描述：同步指定列存表的delta表表结构，使其与列存表主表一致。

  返回值类型：bigint

- pg_sync_cstore_delta()

  描述：同步所有列存表的delta表表结构，使其与列存表主表一致。

  返回值类型：bigint

- pg_get_flush_lsn()

  描述：返回当前节点flush的xlog位置。

  返回值类型：text

- pg_get_sync_flush_lsn()

  描述：返回当前节点多数派flush的xlog位置。

  返回值类型：text

- gs_create_log_tables()

  描述：用于创建运行日志和性能日志的外表和视图。

  返回值类型：void

  示例：

  ```sql
  MogDB=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述：获取数据库级的全量SQL(Full SQL)信息。只可在系统库中查询到结果，用户库中无法查询。

  返回值类型：record

  **表 4** dbe_perf.get_global_full_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | :-------------- | :----------------------- | :---------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述：获取数据库级的慢SQL(Slow SQL)信息。只可在系统库中查询到结果，用户库中无法查询。

  返回值类型：record

  **表 5** dbe_perf.get_global_slow_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | :-------------- | :----------------------- | :---------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- statement_detail_decode(detail text, format text, pretty boolean)

  描述：解析全量/慢SQL语句中的details字段的信息。只可在系统库中查询到结果，用户库中无法查询。

  返回值类型：text

  **表 6** statement_detail_decode参数说明

  | 参数   | 类型    | 描述                                                         |
  | :----- | :------ | :----------------------------------------------------------- |
  | detail | text    | SQL语句产生的事件的集合（不可读）。                          |
  | format | text    | 解析输出格式，取值为 plaintext。                             |
  | pretty | boolean | 当format为plaintext时，是否以优雅的格式展示：true表示通过“\n”分隔事。false表示通过“，”分隔事件。 |

- get_prepared_pending_xid

    描述：当恢复完成时，返回nextxid。

    参数：nan

    返回值类型：text

- pg_clean_region_info

    描述：清理regionmap。

    参数：nan

    返回值类型：character varying

- pg_get_delta_info

    描述：从单个dn获取delta info。

    参数：rel text, schema_name text

    返回值类型：part_name text, live_tuple bigint, data_size bigint, blocknum bigint

- pg_get_replication_slot_name

    描述：获取slot name。

    参数：nan

    返回值类型：text

- pg_get_running_xacts

    描述：获取运行中的xact。

    参数：nan

    返回值类型：handle integer, gxid xid, state tinyint, node text, xmin xid, vacuum boolean, timeline bigint, prepare_xid xid, pid bigint, next_xid xid

- pg_get_variable_info

    描述：获取共享内存变量cache。

    参数：nan

    返回值类型：node_name text, nextOid oid, nextXid xid, oldestXid xid, xidVacLimit xid, oldestXidDB oid, lastExtendCSNLogpage xid, startExtendCSNLogpage xid, nextCommitSeqNo xid, latestCompletedXid xid, startupMaxXid xid

- pg_get_xidlimit

    描述：从共享内存获取事物id信息。

    参数：nan

    返回值类型：nextXid xid, oldestXid xid, xidVacLimit xid, xidWarnLimit xid, xidStopLimit xid, xidWrapLimit xid, oldestXidDB oid

- get_global_user_transaction()

  描述：返回所有节点上各用户的事务相关信息。

  返回值类型：node_name name, usename name, commit_counter bigint, rollback_counter bigint, resp_min bigint, resp_max bigint, resp_avg bigint, resp_total bigint, bg_commit_counter bigint, bg_rollback_counter bigint, bg_resp_min bigint, bg_resp_max bigint, bg_resp_avg bigint, bg_resp_total bigint

- pg_collation_for

  描述：返回入参字符串对应的排序规则。

  参数：any（如果是常量必须进行显式类型转换）

  返回值类型：text

- pgxc_unlock_for_sp_database(name Name)

  描述：释放指定数据库锁。

  参数：数据库名

  返回值类型：布尔

- pgxc_lock_for_sp_database(name Name)

  描述：对指定的数据库加锁。

  参数：数据库名

  返回值类型：布尔

- copy_error_log_create()

  描述：创建COPY FROM容错机制所需要的错误表（public.pgxc_copy_error_log）。

  返回值类型：Boolean

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 此函数会尝试创建public.pgxc_copy_error_log表，表的详细信息请参见表6。
  > - 在relname列上创建B-tree索引，并REVOKE ALL on public.pgxc_copy_error_log FROM public对错误表进行权限控制（与COPY语句权限一致）。
  > - 由于尝试创建的public.pgxc_copy_error_log定义是一张行存表，因此数据库实例上必须支持行存表的创建才能够正常运行此函数，并使用后续的COPY容错功能。需要特别注意的是，enable_hadoop_env这个GUC参数开启后会禁止在数据库实例内创建行存表（MogDB默认为off）。
  > - 此函数自身权限为Sysadmin及以上（与错误表、COPY权限一致）。
  > - 若创建前public.pgxc_copy_error_log表已存在或者copy_error_log_relname_idx索引已存在，则此函数会报错回滚。

  **表 7** 错误表public.pgxc_copy_error_log信息 <a id="cuowubiao"></a>

  | 列名称    | 类型                     | 描述                                         |
  | :-------- | :----------------------- | :------------------------------------------- |
  | relname   | character varying        | 表名称。以模式名.表名形式显示。              |
  | begintime | timestamp with time zone | 出现数据格式错误的时间。                     |
  | filename  | character varying        | 出现数据格式错误的数据源文件名。             |
  | lineno    | bigint                   | 在数据源文件中，出现数据格式错误的行号。     |
  | rawrecord | text                     | 在数据源文件中，出现数据格式错误的原始记录。 |
  | detail    | text                     | 详细错误信息。                               |

- dynamic_func_control(scope text, function_name text, action text, “{params}” text[])

    描述：动态开启内置的功能，当前仅支持动态开启全量SQL。

    返回值类型：record

    **表 8** dynamic_func_control参数说明

    | 参数          | 类型   | 描述                                                         |
    | :------------ | :----- | :----------------------------------------------------------- |
    | scope         | text   | 动态开启功能的范围，当前仅支持'LOCAL'。                      |
    | function_name | text   | 功能的名称，当前仅支持'STMT'。                               |
    | action        | text   | 当function_name为'STMT'时，action仅支持TRACK/UNTRACK/LIST/CLEAN：<br/>- TRACK - 开始记录归一化SQL的全量SQL信息。<br/>- UNTRACK - 取消记录归一化SQL的全量SQL信息。<br/>- LIST - 列取当前TRACK的归一化SQL的信息。<br/>- CLEAN - 清理记录当前归一化SQL的信息。 |
    | params        | text[] | 当function_name为'STMT'时，对应不同的action时，对应的params设置如下：<br/>- TRACK - '{“归一化SQLID”, “L0/L1/L2”}'<br/>- UNTRACK - '{“归一化SQLID”}'<br/>- LIST - '{}'<br/>- CLEAN - '{}' |

- gs_parse_page_bypath(path text, blocknum bigint, relation_type text, read_memory boolean)

    描述：用于解析指定表页面，并返回存放解析内容的路径。

    返回值类型：text

    备注：必须是系统管理员或运维管理员才能执行此函数。

    **表 9** gs_parse_page_bypath参数说明

    | 参数          | 类型    | 描述                                                         |
    | :------------ | :------ | :----------------------------------------------------------- |
    | path          | text    | - 对于普通表或段页式表，相对路径为：tablespace name/database oid/表的relfilenode(物理文件名)。例如：base/16603/16394。<br/>- 表文件的相对路径可以通过pg_relation_filepath(table_name text)查找。<br/>- 合法的path格式列举：<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
    | blocknum      | bigint  | -1 所有block的信息<br/>0- MaxBlockNumber 对应block的信息     |
    | relation_type | text    | - heap(astore 表)<br/>- uheap(ustore 表)<br/>- btree_index(BTree 索引)<br/>- ubtree_index(UBTree 索引)<br/>- segment(段页式) |
    | read_memory   | boolean | - false，从磁盘文件解析。<br/>- true，首先尝试从共享缓冲区中解析该页面；如果共享缓冲区中不存在，则从磁盘文件解析。 |

- gs_xlogdump_lsn(start_lsn text, end_lsn text)

    描述：用于解析指定lsn范围之内的XLOG日志，并返回存放解析内容的路径。可以通过pg_current_xlog_location()获取当前XLOG位置。

    返回值类型：text

    参数：LSN起始位置，LSN结束位置

    备注：必须是系统管理员或运维管理员才能执行此函数。

- gs_xlogdump_xid(c_xid xid)

    描述：用于解析指定xid的XLOG日志，并返回存放解析内容的路径。可以通过txid_current()获取当前事务ID。

    参数：事务ID

    返回值类型：text

    备注：必须是系统管理员或运维管理员才能执行此函数。

- gs_xlogdump_tablepath(path text, blocknum bigint, relation_type text)

    描述：用于解析指定表页面对应的日志，并返回存放解析内容的路径。

    返回值类型：text

    备注：必须是系统管理员或运维管理员才能执行此函数。

    **表 10** gs_xlogdump_tablepath参数说明

    | 参数          | 类型   | 描述                                                         |
    | :------------ | :----- | :----------------------------------------------------------- |
    | path          | text   | - 对于普通表或段页式表，相对路径为：tablespace name/database oid/表的relfilenode(物理文件名)。例如：base/16603/16394。<br/>- 表文件的相对路径可以通过pg_relation_filepath(table_name text)查找。<br/>- 合法的path格式列举：<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
    | blocknum      | bigint | -1 所有block的信息<br/>0- MaxBlockNumber 对应block的信息     |
    | relation_type | text   | - heap(astore 表)<br/>- uheap(ustore 表)<br/>- btree_index(BTree 索引)<br/>- ubtree_index(UBTree 索引)<br/>- segment(段页式) |

- gs_xlogdump_parsepage_tablepath(path text, blocknum bigint, relation_type text, read_memory boolean)

    描述：用于解析指定表页面和表页面对应的日志，并返回存放解析内容的路径。可以看做一次执行gs_parse_page_bypath和gs_xlogdump_tablepath。该函数执行的前置条件是表文件存在。如果想查看已删除的表的相关日志，请直接调用gs_xlogdump_tablepath。

    返回值类型：text

    备注：必须是系统管理员或运维管理员才能执行此函数。

    **表 11** gs_xlogdump_parsepage_tablepath参数说明

    | 参数          | 类型    | 描述                                                         |
    | :------------ | :------ | :----------------------------------------------------------- |
    | path          | text    | - 对于普通表或段页式表，相对路径为：tablespace name/database oid/表的relfilenode(物理文件名)；例如：base/16603/16394<br/>- 表文件的相对路径可以通过pg_relation_filepath(table_name text)查找。<br/>- 合法的path格式列举：<br/>  - global/relNode<br/>  - base/dbNode/relNode<br/>  - pg_tblspc/spcNode/version_dir/dbNode/relNode |
    | blocknum      | bigint  | -1 所有block的信息<br/>0- MaxBlockNumber 对应block的信息     |
    | relation_type | text    | - heap(astore 表)<br/>- uheap(ustore 表)<br/>- btree_index(BTree 索引)<br/>- ubtree_index(UBTree 索引)<br/>- segment(段页式) |
    | read_memory   | boolean | - false，从磁盘文件解析<br/>- true，首先尝试从共享缓冲区中解析该页面；如果共享缓冲区中不存在，则从磁盘文件解析 |

- gs_index_verify(Oid oid, uint32:wq blkno)

    描述：用于校验UBtree索引页面或者索引树上key的顺序是否正确。

    返回值类型：record

    **表 12** gs_index_verify参数说明

    | 参数  | 类型   | 描述                                                         |
    | :---- | :----- | :----------------------------------------------------------- |
    | oid   | Oid    | 索引文件relfilenode,可以通过select relfilenode from pg_class where relname='name'查询，其中name表示对应的索引文件名字。 |
    | blkno | uint32 | 0 ，表示检验整个索引树上所有页面。<br/>大于0，表示校验页面编码等于blkno的索引页面。 |

- gs_index_recycle_queue(Oid oid, int type, uint32 blkno)

    描述：用于解析UBtree索引回收队列信息。

    返回值类型：record

    **表 13** gs_index_recycle_queue参数说明

    | 参数  | 类型   | 描述                                                         |
    | :---- | :----- | :----------------------------------------------------------- |
    | oid   | Oid    | 索引文件relfilenode,可以通过select relfilenode from pg_class where relname='name'查询，其中name表示对应的索引文件名字。 |
    | type  | int    | 0，表示解析整个待回收队列。<br/>1，表示解析整个空页队列。<br/>2，表示解析单个页面。 |
    | blkno | uint32 | 回收队列页面编号，该参数只有在type=2的时候有效，blkno有效取值范围为1~4294967294。 |

- gs_stat_wal_entrytable(int64 idx)

    描述：用于输出xlog中预写日志插入状态表的内容。

    返回值类型：record

    **表 14** gs_stat_wal_entrytable参数说明

    | 参数类型 | 参数名 | 类型   | 描述                                                         |
    | :------- | :----- | :----- | :----------------------------------------------------------- |
    | 输入参数 | idx    | int64  | -1：查询数组所有元素。<br/>0-最大值：具体某个数组元素内容。  |
    | 输出参数 | idx    | uint64 | 记录对应数组中的下标。                                       |
    | 输出参数 | endlsn | uint64 | 记录的LSN标签。                                              |
    | 输出参数 | lrc    | int32  | 记录对应的LRC。                                              |
    | 输出参数 | status | uint32 | 标识当前entry对应的xlog是否已经完全拷贝到wal buffer中：<br/>0：非COPIED<br/>1：COPIED |

- gs_walwriter_flush_position()

    描述：输出预写日志的刷新位置。

    返回值类型：record

    **表 15** gs_walwriter_flush_position参数说明

    | 参数类型 | 参数名                  | 类型   | 描述                                                         |
    | :------- | :---------------------- | :----- | :----------------------------------------------------------- |
    | 输出参数 | last_flush_status_entry | int32  | Xlog flush上一个刷盘的tblEntry下标索引。                     |
    | 输出参数 | last_scanned_lrc        | int32  | Xlog flush上一次扫描到的最后一个tblEntry记录的LRC。          |
    | 输出参数 | curr_lrc                | int32  | WALInsertStatusEntry状态表中LRC最新的使用情况，该LRC表示下一个Xlog记录写入时在WALInsertStatusEntry对应的LRC值。 |
    | 输出参数 | curr_byte_pos           | uint64 | Xlog记录写入WAL文件，最新分配的位置，下一个xlog记录插入点。  |
    | 输出参数 | prev_byte_size          | uint32 | 上一个xlog记录的长度。                                       |
    | 输出参数 | flush_result            | uint64 | 当前全局xlog刷盘的位置。                                     |
    | 输出参数 | send_result             | uint64 | 当前主机上xlog发送位置。                                     |
    | 输出参数 | shm_rqst_write_pos      | uint64 | 共享内存中记录的XLogCtl中LogwrtRqst请求的write位置。         |
    | 输出参数 | shm_rqst_flush_pos      | uint64 | 共享内存中记录的XLogCtl中LogwrtRqst请求的flush位置。         |
    | 输出参数 | shm_result_write_pos    | uint64 | 共享内存中记录的XLogCtl中LogwrtResult的write位置。           |
    | 输出参数 | shm_result_flush_pos    | uint64 | 共享内存中记录的XLogCtl中LogwrtResult的flush位置。           |
    | 输出参数 | curr_time               | text   | 当前时间。                                                   |

- gs_walwriter_flush_stat(int operation)

    描述：用于统计预写日志write与sync的次数频率与数据量，以及xlog文件的信息。

    返回值类型：record

    **表 16** gs_walwriter_flush_stat参数说明

    | 参数类型 | 参数名                       | 类型   | 描述                                                         |
    | :------- | :--------------------------- | :----- | :----------------------------------------------------------- |
    | 输入参数 | operation                    | int    | -1：关闭统计开关(默认状态为关闭)。<br/>0：打开统计开关。<br/>1：查询统计信息。2：重置统计信息。 |
    | 输出参数 | write_times                  | uint64 | Xlog调用write接口的次数。                                    |
    | 输出参数 | sync_times                   | uint64 | Xlog调用sync接口次数。                                       |
    | 输出参数 | total_xlog_sync_bytes        | uint64 | Backend线程请求写入xlog总量统计值。                          |
    | 输出参数 | total_actual_xlog_sync_bytes | uint64 | 调用sync接口实际刷盘的xlog总量统计值。                       |
    | 输出参数 | avg_write_bytes              | uint32 | 每次调用XLogWrite接口请求写的xlog量。                        |
    | 输出参数 | avg_actual_write_bytes       | uint32 | 实际每次调用write接口写的xlog量。                            |
    | 输出参数 | avg_sync_bytes               | uint32 | 平均每次请求sync的xlog量。                                   |
    | 输出参数 | avg_actual_sync_bytes        | uint32 | 实际每次调用sync刷盘xlog量。                                 |
    | 输出参数 | total_write_time             | uint64 | 调用write操作总时间统计(单位：us)。                          |
    | 输出参数 | total_sync_time              | uint64 | 调用sync操作总时间统计(单位：us)。                           |
    | 输出参数 | avg_write_time               | uint32 | 每次调用write接口平均时间(单位：us)。                        |
    | 输出参数 | avg_sync_time                | uint32 | 每次调用sync接口平均时间(单位：us)。                         |
    | 输出参数 | curr_init_xlog_segno         | uint64 | 当前最新创建的xlog段文件编号。                               |
    | 输出参数 | curr_open_xlog_segno         | uint64 | 当前正在写的xlog段文件编号。                                 |
    | 输出参数 | last_reset_time              | text   | 上一次重置统计信息的时间。                                   |
    | 输出参数 | curr_time                    | text   | 当前时间。                                                   |

- gs_comm_proxy_thread_status()

    描述：用于在集群配置用户态网络的场景下，代理通信库comm_proxy收发数据包统计

    参数：nan

    返回值类型：record

    >**说明**:
    >
    >- 此函数的查询仅在集中式环境开始部署用户态网络，且comm_proxy_attr参数中enable_dfx配置为true的条件下显示具体信息。其他场景报错不支持查询。
