---
title: Undo系统函数
summary: Undo系统函数
author: Guo Huan
date: 2021-10-28
---

# Undo系统函数

- gs_undo_meta(type, zoneId, location)

  描述：Undo各模块元信息。

  参数说明：

  - type(元信息类型)

    0表示Undo Zone(Record) 对应的元信息。

    1表示Undo Zone(Transaction Slot) 对应的元信息。

    2表示Undo Space(Record) 对应的元信息。

    3表示Undo Space(Transaction Slot) 对应的元信息。

  - zoneId(undo zone编号)

    -1表示所有undo zone的元信息。

    0-1024*1024表示对应zoneid的元信息。

  - location(读取位置）

    0表示从当前内存中读取。

    1表示从物理文件中读取。

  返回值类型：record

- gs_undo_translot(location, zoneId)

  描述：Undo事务槽信息。

  参数说明：

  - location(读取位置)

    0表示从当前内存中读取。

    1表示从物理文件中读取。

  - zoneId(undo zone编号)

    -1表示所有undo zone的元信息。

    0-1024*1024表示对应zoneId的元信息。

  返回值类型：record

- gs_stat_undo()

  描述：Undo统计信息。

  返回值类型：record

  **表 1** gs_stat_undo参数说明

  | 参数类型 | 参数名                  | 类型   | 描述                                                         |
  | :------- | :---------------------- | :----- | :----------------------------------------------------------- |
  | 输出参数 | curr_used_zone_count    | uint32 | 当前使用的Undo zone数量。                                    |
  | 输出参数 | top_used_zones          | text   | 前三个使用量最大的Undo zone信息，格式输出为：(zoneId1:使用大小，zoneId2:使用大小，zoneId3:使用大小)。 |
  | 输出参数 | curr_used_undo_size     | uint32 | 当前使用的Undo总空间大小，单位为MB。                         |
  | 输出参数 | undo_threshold          | uint32 | 为guc参数undo_space_limit_size * 80%计算的结果,单位为MB。    |
  | 输出参数 | oldest_xid_in_undo      | uint64 | 当前Undo空间回收到的事务xid(小于该xid事务产生的Undo记录都已经被回收)。 |
  | 输出参数 | oldest_xmin             | uint64 | 最老的活跃事务。                                             |
  | 输出参数 | total_undo_chain_len    | int64  | 所有访问过的Undo链总长度。                                   |
  | 输出参数 | max_undo_chain_len      | int64  | 最大访问过的Undo链长度。                                     |
  | 输出参数 | create_undo_file_count  | uint32 | 创建的Undo文件数量统计。                                     |
  | 输出参数 | discard_undo_file_count | uint32 | 删除的Undo文件数量统计。                                     |

- gs_undo_record(undoptr)

  描述：Undo记录解析。

  参数说明：

  - undoptr(undo记录指针)

  返回值类型：record
