---
title: 逻辑复制函数
summary: 逻辑复制函数
author: Zhang Cuiping
date: 2021-04-20
---

# 逻辑复制函数

- pg_create_logical_replication_slot('slot_name', 'plugin_name')

  描述：创建逻辑复制槽。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - plugin_name

    插件名称。

    取值范围：字符串，当前支持mppdb_decoding。

  返回值类型：name, text

  备注：第一个返回值表示slot_name，第二个返回值表示该逻辑复制槽解码的起始LSN位置。调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。此函数目前只支持在主机调用。

- pg_create_physical_replication_slot('slot_name', 'isDummyStandby')

  描述：创建新的物理复制槽。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - isDummyStandby

    是否是从从备连接主机创建的复制槽。

    类型：bool。

  返回值类型：name, text

  备注：调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。目前默认不支持主备从部署模式。

- pg_drop_replication_slot('slot_name')

  描述：删除流复制槽。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  返回值类型：void

  备注：调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。此函数目前只支持在主机调用。

- pg_logical_slot_peek_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述：解码并不推进流复制槽（下次解码可以再次获取本次解出的数据）。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - LSN

    日志的LSN，表示只解码小于等于此LSN的日志。

    取值范围：字符串（LSN，格式为xlogid/xrecoff），如'1/2AAFC60'。为NULL时表示不对解码截止的日志位置做限制。

  - upto_nchanges

    解码条数（包含begin和commit）。假设一共有三条事务，分别包含3、5、7条记录，如果upto_nchanges为4，那么会解码出前两个事务共8条记录。解码完第二条事务时发现解码条数记录大于等于upto_nchanges，会停止解码。

    取值范围：非负整数。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  LSN和upto_nchanges中任一参数达到限制，解码都会结束。

  - options：此项为可选参数，由一系列options_name和options_value一一对应组成。

    - include-xids

      解码出的data列是否包含xid信息。

      取值范围：0或1，默认值为1。

      - 0：设为0时，解码出的data列不包含xid信息。
      - 1：设为1时，解码出的data列包含xid信息。

    - skip-empty-xacts

      解码时是否忽略空事务信息。

      取值范围：0或1，默认值为0。

      - 0：设为0时，解码时不忽略空事务信息。
      - 1：设为1时，解码时会忽略空事务信息。

    - include-timestamp

      解码信息是否包含commit时间戳。

      取值范围：0或1，默认值为0。

      - 0：设为0时，解码信息不包含commit时间戳。
      - 1：设为1时，解码信息包含commit时间戳。

    - only-local

      是否仅解码本地日志。

      取值范围：0或1，默认值为1。

      - 0：设为0时，解码非本地日志和本地日志。
      - 1：设为1时，仅解码本地日志。

    - force-binary

      是否以二进制格式输出解码结果。

      取值范围：0，默认值为0。

      - 0：设为0时，以文本格式输出解码结果。

    - white-table-list

      白名单参数，包含需要进行解码的schema和表名。

      取值范围：包含白名单中表名的字符串，不同的表以','为分隔符进行隔离；使用'*'来模糊匹配所有情况；schema名和表名间以'.'分割，不允许存在任意空白符。例：

      select * from pg_logical_slot_peek_changes('slot1', NULL, 4096, 'white-table-list', 'public.t1,public.t2');

  返回值类型：text, xid, text

  备注：函数返回解码结果，每一条解码结果包含三列，对应上述返回值类型，分别表示LSN位置、xid和解码内容。

  调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。

- pg_logical_slot_get_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述：解码并推进流复制槽。

  参数说明：与pg_logical_slot_peek_changes一致。

  备注：调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。此函数目前只支持在主机调用。

- pg_logical_slot_peek_binary_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述：以二进制格解码且不推进流复制槽（下次解码可以再次获取本次解出的数据）。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - LSN

    日志的LSN，表示只解码小于等于此LSN的日志。

    取值范围：字符串（LSN，格式为xlogid/xrecoff），如'1/2AAFC60'。为NULL时表示不对解码截止的日志位置做限制。

  - upto_nchanges

    解码条数（包含begin和commit）。假设一共有三条事务，分别包含3、5、7条记录，如果upto_nchanges为4，那么会解码出前两个事务共8条记录。解码完第二条事务时发现解码条数记录大于等于upto_nchanges，会停止解码。

    取值范围：非负整数。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  LSN和upto_nchanges中任一参数达到限制，解码都会结束。

  - options：此项为可选参数，由一系列options_name和options_value一一对应组成。

    - include-xids

      解码出的data列是否包含xid信息。

      取值范围：0或1，默认值为1。

      - 0：设为0时，解码出的data列不包含xid信息。
      - 1：设为1时，解码出的data列包含xid信息。

    - skip-empty-xacts

      解码时是否忽略空事务信息。

      取值范围：0或1，默认值为0。

      - 0：设为0时，解码时不忽略空事务信息。
      - 1：设为1时，解码时会忽略空事务信息。

    - include-timestamp

      解码信息是否包含commit时间戳。

      取值范围：0或1，默认值为0。

      - 0：设为0时，解码信息不包含commit时间戳。
      - 1：设为1时，解码信息包含commit时间戳。

    - only-local

      是否仅解码本地日志。

      取值范围：0或1，默认值为1。

      - 0：设为0时，解码非本地日志和本地日志。
      - 1：设为1时，仅解码本地日志。

    - force-binary

      是否以二进制格式输出解码结果。

      取值范围：0或1，默认值为0，均以二进制格式输出结果。

    - white-table-list

      白名单参数，包含需要进行解码的schema和表名。

      取值范围：包含白名单中表名的字符串，不同的表以','为分隔符进行隔离；使用'*'来模糊匹配所有情况；schema名和表名间以'.'分割，不允许存在任意空白符。例：

      select * from pg_logical_slot_peek_binary_changes('slot1', NULL, 4096, 'white-table-list', 'public.t1,public.t2');

  返回值类型：text, xid, bytea

  备注：函数返回解码结果，每一条解码结果包含三列，对应上述返回值类型，分别表示LSN位置、xid和二进制格式的解码内容。调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。

- pg_logical_slot_get_binary_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述：以二进制格式解码并推进流复制槽。

  参数说明：与pg_logical_slot_peek_binary_changes一致。

  备注：调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。

- pg_replication_slot_advance ('slot_name', 'LSN')

  描述：直接推进流复制槽到指定LSN，不输出解码结果。

  参数说明：

  - slot_name

    流复制槽名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - LSN

    推进到的日志LSN位置，下次解码时只会输出提交位置比该LSN大的事务结果。如果输入的LSN比当前流复制槽记录的推进位置还要小，则直接返回；如果输入的LSN比当前最新物理日志LSN还要大，则推进到当前最新物理日志LSN。

    取值范围：字符串（LSN，格式为xlogid/xrecoff）。

  返回值类型：name, text

  备注：返回值分别对应slot_name和实际推进至的LSN。调用该函数的用户需要具有SYSADMIN权限或具有REPLICATION权限或继承了内置角色gs_role_replication的权限。此函数目前只支持在主机调用。

- pg_logical_get_area_changes('LSN_start', 'LSN_end', upto_nchanges, 'decoding_plugin', 'xlog_path', 'options_name', 'options_value')

  描述：没有ddl的前提下，指定lsn区间进行解码，或者指定xlog文件进行解码。

  约束条件如下：

  1. 调用接口时，日志级别wal_level=logical，且只有在wal_level=logical期间产生的日志文件才能被解析，如果使用的xlog文件为非logical级别，则解码内容没有对应的值和类型，无其他影响。
  2. xlog文件只能被完全同构的dn的某个副本解析，确保可以找到数据对应的元信息，且没有DDL操作和VACUUM FULL。
  3. 用户可以找到需要解析的xlog。
  4. 用户需要注意一次不要读入过多xlog文件，推荐一次一个，一个xlog文件估测占用内存为xlog文件大小的2~3倍。
  5. 无法解码扩容前的xlog文件。

  参数说明：

  - LSN_start

    指定开始解码的lsn。

    取值范围：字符串（LSN，格式为xlogid/xrecoff），如'1/2AAFC60'。为NULL时表示不对解码截止的日志位置做限制。

  - LSN_end

    指定解码结束的lsn。

    取值范围：字符串（LSN，格式为xlogid/xrecoff），如'1/2AAFC60'。为NULL时表示不对解码截止的日志位置做限制。

  - upto_nchanges

    解码条数（包含begin和commit）。假设一共有三条事务，分别包含3、5、7条记录，如果upto_nchanges为4，那么会解码出前两个事务共8条记录。解码完第二条事务时发现解码条数记录大于等于upto_nchanges，会停止解码。

    取值范围：非负整数。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  LSN和upto_nchanges中任一参数达到限制，解码都会结束。

  - decoding_plugin

  解码插件，指定解码内容输出格式的so插件。

  取值范围：提供mppdb_decoding和sql_decoding两个解码插件。

  - xlog_path

  解码插件，指定解码文件的xlog绝对路径，文件级别

  取值范围：NULL 或者 xlog文件绝对路径的字符串。

  - options：此项为可选参数，由一系列options_name和options_value一一对应组成，可以缺省，详见pg_logical_slot_peek_changes。

  示例：

  ```
  MogDB=# SELECT pg_current_xlog_location();
   pg_current_xlog_location
  --------------------------
   0/E62E238
  (1 row)

  MogDB=# create table t1 (a int primary key,b int,c int);
  NOTICE:  CREATE TABLE / PRIMARY KEY will create implicit index "t1_pkey" for table "t1"
  CREATE TABLE
  MogDB=# insert into t1 values(1,1,1);
  INSERT 0 1
  MogDB=# insert into t1 values(2,2,2);
  INSERT 0 1

  MogDB=# select data from pg_logical_get_area_changes('0/E62E238',NULL,NULL,'sql_decoding',NULL);
   location  |  xid  |                       data
  -----------+-------+---------------------------------------------------
   0/E62E8D0 | 27213 | COMMIT (at 2022-01-26 15:08:03.349057+08) 3020226
   0/E6325F0 | 27214 | COMMIT (at 2022-01-26 15:08:07.309869+08) 3020234
  ……
  ```

- pg_get_replication_slots()

  描述：获取复制槽列表。

  返回值类型：text，text，text，oid，boolean，xid，xid，text，boolean

  示例：

  ```
  MogDB=# select * from pg_get_replication_slots();
   slot_name |     plugin     | slot_type | datoid | active | xmin | catalog_xmin | restart_lsn | dummy_standby
  -----------+----------------+-----------+--------+--------+------+--------------+-------------+---------------
   wkl001    | mppdb_decoding | logical   |  15914 | f      |      |      2079556 | 4/1B81D920  | f
   dn_6002   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   dn_6004   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   dn_6003   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
   gfslot001 | mppdb_decoding | logical   |  15914 | f      |      |      2412553 | 4/A54B2428  | f
  (5 rows)
  ```

- gs_get_parallel_decode_status()

  描述：监控各个解码线程的读取日志队列和解码结果队列的长度，以便定位并行解码性能瓶颈。

  返回值类型：text, int, text, text

  示例：

  ```
  MogDB=# select * from gs_get_parallel_decode_status();
   slot_name | parallel_decode_num |                     read_change_queue_length                     |                    decode_change_queue_length
  -----------+---------------------+------------------------------------------------------------------+------------------------------------------------------------------
   slot1     |                   3 | queue0: 33, queue1: 36, queue2: 1017                             | queue0: 1011, queue1: 1008, queue2: 27
   slot2     |                   5 | queue0: 452, queue1: 1017, queue2: 233, queue3: 585, queue4: 183 | queue0: 754, queue1: 188, queue2: 972, queue3: 620, queue4: 1022
  (2 rows)
  ```

  备注：返回值的slot_name代表复制槽名，parallel_decode_num代表该复制槽的并行解码线程数，read_change_queue_length列出了每个解码线程读取日志队列的当前长度，decode_change_queue_length列出了每个解码线程解码结果队列的当前长度。

- pg_replication_origin_create (node_name)

  描述：用给定的外部名称创建一个复制源，并且返回分配给它的内部ID。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    待创建的复制源的名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  返回值类型：oid

- pg_replication_origin_drop (node_name)

  描述：删除一个以前创建的复制源，包括任何相关的重放进度。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    待删除的复制源的名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

- pg_replication_origin_oid (node_name)

  描述：根据名称查找复制源并返回内部ID。如果没有发现这样的复制源，则抛出错误。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    要查找的复制源的名称

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  返回值类型：oid

- pg_replication_origin_session_setup (node_name)

  描述：将当前会话标记为从给定的原点回放，从而允许跟踪回放进度。只能在当前没有选择原点时使用。使用pg_replication_origin_session_reset 命令来撤销。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    复制源名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

- pg_replication_origin_session_reset ()

  描述：取消pg_replication_origin_session_setup()的效果。

  备注：调用该函数的用户需要具有SYSADMIN权限。

- pg_replication_origin_session_is_setup ()

  描述：如果在当前会话中选择了复制源则返回真。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  返回值类型：boolean

- pg_replication_origin_session_progress (flush)

  描述：返回当前会话中选择的复制源的重放位置。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - flush

    决定对应的本地事务是否被确保已经刷入磁盘。

    取值范围：boolean

  返回值类型：LSN

- pg_replication_origin_xact_setup (origin_lsn, origin_timestamp)

  描述：将当前事务标记为重放在给定LSN和时间戳上提交的事务。只能在使用pg_replication_origin_session_setup选择复制源时调用。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - origin_lsn

    复制源回放位置。

    取值范围：LSN

  - origin_timestamp

    事务提交时间。

    取值范围：timestamp with time zone

- pg_replication_origin_xact_reset ()

  描述：取消pg_replication_origin_xact_setup()的效果。

  备注：调用该函数的用户需要具有SYSADMIN权限。

- pg_replication_origin_advance (node_name, lsn)

  描述：

  将给定节点的复制进度设置为给定的位置。这主要用于设置初始位置，或在配置更改或类似的变更后设置新位置。

  注意：这个函数的使用不当可能会导致不一致的复制数据。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    已有复制源名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - lsn

    复制源回放位置。

    取值范围：LSN

- pg_replication_origin_progress (node_name, flush)

  描述：返回给定复制源的重放位置。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  参数说明：

  - node_name

    复制源名称。

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  - flush

    决定对应的本地事务是否被确保已经刷入磁盘。

    取值范围：boolean

- pg_show_replication_origin_status()

  描述：获取复制源的复制状态。

  备注：调用该函数的用户需要具有SYSADMIN权限。

  返回值类型：

  - local_id：oid，复制源id。
  - external_id：text，复制源名称。
  - remote_lsn：LSN，复制源的lsn位置。
  - local_lsn：LSN，本地的lsn位置。

- pg_get_publication_tables(pub_name)

  描述：根据发布的名称，返回对应发布要发布的表的relid列表

  参数说明：

  - pub_name

    已存在的发布名称

    取值范围：字符串，不支持除字母，数字，以及（_?-.）以外的字符。

  返回值类型：relid列表

- pg_stat_get_subscription(sub_oid oid) → record

  描述：

  输入订阅的oid，返回订阅的状态信息。

  参数说明：

- subid

  订阅的oid。

  取值范围：oid

  返回值类型：

  - relid：oid，表的oid。
  - pid：thread_id，后台apply/sync线程的thread id。
  - received_lsn：pg_lsn，从发布端接收到的最近的lsn。
  - last_msg_send_time：timestamp，最近发布端发送消息的时间。
  - last_msg_receipt_time：timestamp，最新订阅端收到消息的时间。
  - latest_end_lsn：pg_lsn，最近一次收到保活消息时发布端的lsn。
  - latest_end_time：timstamp，最近一次收到保活消息的时间。
