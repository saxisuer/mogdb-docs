---
title: GAUSS-04401 - GAUSS-04500
summary: GAUSS-04401 - GAUSS-04500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-04401 - GAUSS-04500

<br/>

## GAUSS-04401 - GAUSS-04410

<br/>

GAUSS-04401: "The local index %u on the partition %u not exist."

SQLSTATE: 42704

错误原因: 可能由于drop partition和drop index并发，在drop index的时候可能对应index已经不存在了。

解决办法: 调整并发控制

GAUSS-04403: "Can only access temp objects of the current session."

SQLSTATE: 0A000

错误原因: 访问了其他session的临时对象。

解决办法: 只允许访问本session的临时对象，不支持访问其他session的临时对象。

GAUSS-04404: "cache lookup failed for role %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04405: "temp relation is invalid because of cluster resizing"

SQLSTATE: 0A000

错误原因: 扩容/缩容后原有临时表失效。

解决办法:

1. 退出当前session，再重新连接并创建临时表
2. 使用DROP SCHEMA %s, %s CASCADE来删除临时schema，再重建临时表。

GAUSS-04407: "Not allowed to insert into relation pg_auth_history."

SQLSTATE: XX000

错误原因: pg_auth_history用于审计帐户密码的变更。只有当pg_authid中的帐户密码发生变化时才记录相应的变动。如果认为变更，将影响审计。

解决办法: 禁止使用insert …into…语句人为向pg_auth_history中插入记录。

GAUSS-04408: "do not support create non-temp table like temp table"

SQLSTATE: 0A000

错误原因: 创建普通表时使用了like临时表语法，不支持这样使用。

解决办法: 不支持创建非临时表like临时表，如果要使用like临时表语法，请将要创建的表也指定为临时表。

<br/>

## GAUSS-04411 - GAUSS-04420

<br/>

GAUSS-04411: "Multi-column combined informational constraint is forbidden."

SQLSTATE: 0A000

错误原因: 不支持多列组合信息约束。

解决办法: 在一个列上建立信息约束。

GAUSS-04414: "The number of %s distribute key can not exceed 1"

SQLSTATE: 42601

错误原因: 非hash分布的表的分布列不能超过1。

解决办法: 修改建表语句，指定一个列作为分布列。

GAUSS-04415: "Compatibility args %s is invalid"

SQLSTATE: 22023

错误原因: 指定的兼容行参数不对。

解决办法: 指定参数为'ORA'或者'TD'。

GAUSS-04417: "Alias '%s' reference with window function included is not supported."

SQLSTATE: 42703

错误原因: 别名引用，被引用的表达式不能包含window function。

解决办法: 修改sql语句，不支持该用法。

GAUSS-04418: "Alias '%s' reference with volatile function included is not supported."

SQLSTATE: 42703

错误原因: 别名引用，被引用的表达式不能包含volatile function。

解决办法: 修改sql语句，不支持该用法。

GAUSS-04419: "Alias '%s' is ambiguous."

SQLSTATE: 42703

错误原因: 使用别名重复。

解决办法: 更换别名，避免别名重复。

<br/>

## GAUSS-04421 - GAUSS-04430

<br/>

GAUSS-04422: "%u/%u/%u invalid bcm meta buffer %u"

SQLSTATE: XX000

错误原因: 无效的BCM文件的meta页面。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04424: "Vector aggregation does not support this distinct clause in aggregate function"

SQLSTATE: XX000

错误原因: aggregation向量化执行引擎初始化时，聚合函数不支持distinct子句。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04425: "Grant not supported for TEMP and non-TEMP objects together"

SQLSTATE: 0A000

错误原因: Grant操作不支持临时对象和非临时对象一起出现。

解决办法: 将要执行的语句拆分成两句，将临时对象和非临时对象分开执行。

GAUSS-04426: "Explicit prepare transaction is not supported."

SQLSTATE: 0A000

错误原因: 不支持显式prepare。由于用户输入了显式prepare语句。

解决办法: 不输入显式prepare语句。

GAUSS-04427: "Explicit commit prepared transaction is not supported."

SQLSTATE: 0A000

错误原因: 不支持显式commit prepared。由于用户输入了显式commit prepared语句。

解决办法: 不输入显式commit prepared语句。

GAUSS-04428: "Explicit rollback prepared transaction is not supported."

SQLSTATE: 0A000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04429: "LOCK not supported for TEMP and non-TEMP objects together"

SQLSTATE: 0A000

错误原因: LOCK操作不支持临时对象和非临时对象一起出现。

解决办法: 将要执行的语句拆分成两句，将临时对象和非临时对象分开执行。

<br/>

## GAUSS-04431 - GAUSS-04440

<br/>

GAUSS-04432: "invalid set size for BipartiteMatch"

SQLSTATE: XX000

错误原因: 输入参数错误，系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04433: "AIO Completer io_getevents() failed: error %d ."

SQLSTATE: XX000

错误原因: 操作系统函数io_getevents()执行失败。

解决办法: 检测操作系统内核函数是否大于Linux 2.6.23，确认操作系统安装了libaio库。

GAUSS-04434: "parent of GROUPING is not VecAgg node"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04435: "parent of GROUPINGID is not VecAgg node"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04436: "Unsupported alternative subPlan expression in vector engine"

SQLSTATE: 0A000

错误原因: alternative表达式没有实现向量化。

解决办法: 修改SQL语句。

GAUSS-04437: "Unsupported rowexpr expression in vector engine"

SQLSTATE: 0A000

错误原因: rowexpr表达式没有向量化。

解决办法: 修改SQL语句。

GAUSS-04438: "unsupported vector sub plan type %d"

SQLSTATE: XX000

错误原因: 用了向量化执行引擎不支持的Subplan类型，报错提示不支持。

解决办法: 修改SQL语句。

GAUSS-04439: "Unsupported state in vec sort agg"

SQLSTATE: 20000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04440: "Failed to connect hdfs."

SQLSTATE: XX000

错误原因: 连接HDFS失败。

解决办法: 请检查CN日志，根据这条报错可以搜索到具体报错原因。

<br/>

## GAUSS-04441 - GAUSS-04450

<br/>

GAUSS-04441: "Unsupport store format, only support ORC format for DFS table."

SQLSTATE: XX000

错误原因: 系统内部错误。HDFS表检测数据格式，发现不是ORC格式。

解决办法: 修改数据格式。

GAUSS-04442: "'INSERT' is not supported by the type of relation."

SQLSTATE: D0011

错误原因: 该表不支持insert操作。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04443: "'DELETE' is not supported by the type of relation."

SQLSTATE: D0011

错误原因: 该表不支持delete操作。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04444: "'UPDATE' is not supported by the type of relation."

SQLSTATE: D0011

错误原因: 该表不支持update操作。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04449: "parent of GROUPING is not Agg node"

SQLSTATE: 42P27

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04450: "parent of GROUPINGID is not Agg node"

SQLSTATE: 42P27

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04461 - GAUSS-04470

<br/>

GAUSS-04468: "corrupt during reset shared hash table '%s'"

SQLSTATE: XX000

错误原因: 重置共享hash表过程中内存崩溃。

解决办法: 节点重启，无须额外处理。

GAUSS-04469: "terminating SyncLocalXactsWithGTM process due to administrator command"

SQLSTATE: 57P01

错误原因: 主降备或者系统退出过程中SyncLocalXactsWithGTM被停止。

解决办法: 无须额外处理。

<br/>

## GAUSS-04471 - GAUSS-04480

<br/>

GAUSS-04471: "fallocate(fd=%d, amount=%d, offset=%ld),write count(%d), errno(%d), maybe you use adio without XFS filesystem, if you really want do this,please turn off GUC parameter enable_fast_allocate"

SQLSTATE: XX000

错误原因: 快速分配数据文件块失败。

解决办法: 确认文件系统是XFS，若不是请关闭enable_fast_allocate配置项。

GAUSS-04472: "FileAsyncRead, file access failed %d"

SQLSTATE: XX000

错误原因: 在异步IO的模式下，服务器读取数据文件前，需要检测到文件状态异常。

解决办法: 请确保数据文件状态正常，若数据文件异常，请从备机或备份数据中恢复。

GAUSS-04474: "FileAsyncWrite, file access failed %d"

SQLSTATE: XX000

错误原因: 异步IO写数据文件前检测到文件状态异常。

解决办法: 请确保数据文件状态正常，若数据文件异常，请从备机或备份数据中恢复。

GAUSS-04476: "FileAccess() FAILED %d"

SQLSTATE: XX000

错误原因: 异步IO读写数据文件前检测到文件状态异常。

解决办法: 请确保数据文件状态正常，若数据文件异常，请从备机或备份数据中恢复。

GAUSS-04479: "fallocate(fd=%d, amount=%u, offset=%u),write count(%d), errno(%d), maybe you use adio without XFS filesystem, if you really want do this,please turn off GUC parameter enable_fast_allocate"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-04481 - GAUSS-04490

<br/>

GAUSS-04481: "Require scratch buflist to reorder writes."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04483: "CheckIOState, find an error in async write"

SQLSTATE: XX000

错误原因:  VACUUM FULL行存表产生写IO错误。

解决办法: 检测磁盘是否发生故障是否需要更新磁盘后可尝试重新执行。

GAUSS-04484: "InProgressBuf not null: id %d flags %u, buf: id %d flags %u"

SQLSTATE: 无

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04485: "posix_memalign fails, The alignment argument was not a power of two, or was not a multiple of sizeof(void *)"

SQLSTATE: XX000

错误原因: 系统内部错误。操作系统内存不足或者申请内存对齐参数不为2的幂次方或者指针参数异常。

解决办法: 操作系统释放部分内存后，可尝试重新执行。

<br/>

## GAUSS-04491 - GAUSS-04500

<br/>

GAUSS-04492: "redundant options."

SQLSTATE: XX000

错误原因: 冗余的选项。

解决办法: 检查DDL语句中的选项是否有重复。

GAUSS-04494: "max_dop value can't be %ld."

SQLSTATE: 22003

错误原因: max_dop的值无效。

解决办法: 设置max_dop的值必须大于等于1。

GAUSS-04495: "invalid value '%s' for parameter '%s'"

SQLSTATE: 22023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-04496: "memory_limit size value can't be %d."

SQLSTATE: 22003

错误原因: memory_limit的数值无效。

解决办法: memory_limit的数值必须是正数。
