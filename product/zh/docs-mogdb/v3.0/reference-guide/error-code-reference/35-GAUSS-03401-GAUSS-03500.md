---
title: GAUSS-03401 - GAUSS-03500
summary: GAUSS-03401 - GAUSS-03500
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03401 - GAUSS-03500

<br/>

## GAUSS-03401 - GAUSS-03410

<br/>

GAUSS-03402: "hash_redo: unimplemented"

SQLSTATE: XX000

错误原因: 哈希索引不支持的redo。

解决办法: 属于功能不支持，请检查使用方式和场景。

GAUSS-03404: "index row size %lu exceeds hash maximum %lu"

SQLSTATE: 54000

错误原因: 元组的大小超过一个页的大小。

解决办法: 内部错误。检查是否使用了toast。

GAUSS-03405: "failed to add index item to '%s'"

SQLSTATE: XX000

错误原因: 向哈希索引页插入元组失败。

解决办法: 内部错误，请联系技术支持工程师提供技术支持。

GAUSS-03407: "hash AM does not use P_NEW"

SQLSTATE: XX000

错误原因: 哈希索引不支持外部扩页。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03408: "access to noncontiguous page in hash index '%s'"

SQLSTATE: XX000

错误原因: 哈希索引扩的页必须连续，不支持非连续扩页。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03409: "unexpected hash relation size: %u, should be %u"

SQLSTATE: XX000

错误原因: 哈希索引扩页失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03410: "cannot initialize non-empty hash index '%s'"

SQLSTATE: XX000

错误原因: 不能初始化非空哈希表。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03411 - GAUSS-03420

<br/>

GAUSS-03411: "scan in progress on supposedly new bucket"

SQLSTATE: XX000

错误原因: 在新的bucket上有scan操作。

解决办法: 等scan操作结束后，再尝试执行本操作。

GAUSS-03412: "could not get lock on supposedly new bucket"

SQLSTATE: XX000

错误原因: 新bucket上的锁被占用。

解决办法: 尝试再次执行。

GAUSS-03413: "invalid overflow block number %u"

SQLSTATE: XX000

错误原因: 超过hash表的内部最大页号。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03414: "firstfreebit found no free bit"

SQLSTATE: XX000

错误原因: 没有找到free bit。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03415: "invalid overflow bit number %u"

SQLSTATE: XX000

错误原因: 超过最大bitmap页数。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03416: "out of overflow pages in hash index '%s'"

SQLSTATE: 54000

错误原因: hash索引支持容量达到上限。

解决办法: 请重建索引。

GAUSS-03417: "missing support function %d(%u,%u) for index '%s'"

SQLSTATE: XX000

错误原因: 不支持该哈希函数。

解决办法: 使用其他的哈希函数。

GAUSS-03418: "index '%s' contains unexpected zero page at block %u"

SQLSTATE: XX002

错误原因: 索引检测到空页。

解决办法: 请重建索引。

GAUSS-03419: "index '%s' contains corrupted page at block %u"

SQLSTATE: XX002

错误原因: 索引检测到坏页。

解决办法: 请重建索引。

GAUSS-03420: "index '%s' is not a hash index"

SQLSTATE: XX002

错误原因: 该索引不属于哈希索引。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03421 - GAUSS-03430

<br/>

GAUSS-03421: "index '%s' has wrong hash version"

SQLSTATE: XX002

错误原因: 版本信息错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03423: "lost saved point in index"

SQLSTATE: XX000

错误原因: 索引中的页面缺失。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03424: "could not find additional pending pages for same heap tuple"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03425: "index row size %lu exceeds maximum %lu for index '%s'"

SQLSTATE: 54000

错误原因: 元组的大小超过一个页的大小。

解决办法: 内部错误，属于预防性报错。检查是否使用了toast机制。

GAUSS-03426: "failed to add item to index page in '%s'"

SQLSTATE: XX000

错误原因: 页面插入元组失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03427: "failed to add item to index root page"

SQLSTATE: XX000

错误原因: 向root页面插入元组失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03428: "Lost path"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03429: "ginmerge: unimplemented"

SQLSTATE: XX000

错误原因: 不支持gin索引merge。

解决办法: 属于功能不支持，请检查使用方式和场景。

GAUSS-03430: "unexpected searchMode: %d"

SQLSTATE: XX000

错误原因: searchMode错误。

解决办法: 检查所设置的searchmode。

<br/>

## GAUSS-03431 - GAUSS-03440

<br/>

GAUSS-03431: "old GIN indexes do not support whole-index scans nor searches for nulls"

SQLSTATE: 0A000

错误原因: 低版本的gin索引不支持，whole-index scan或null的搜索。

解决办法: 在新版本上重新创建gin索引。

GAUSS-03432: "GIN does not support mark/restore"

SQLSTATE: XX000

错误原因: Gin索引不支持记录或者恢复scan position。

解决办法: 属于功能不支持，请检查使用方式。

GAUSS-03433: "failed to add item to index page in %u/%u/%u"

SQLSTATE: XX000

错误原因: 页面插入元组失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03434: "failed to add item to index page"

SQLSTATE: XX000

错误原因: 页面插入元组失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03438: "ginarrayextract requires three arguments"

SQLSTATE: XX000

错误原因: 该函数需要3个参数。

解决办法: 检查函数ginarrayextract_2args的使用方法。

GAUSS-03439: "ginqueryarrayextract: unknown strategy number: %d"

SQLSTATE: XX000

错误原因: 使用了错误的strategy。

解决办法: 检查函数ginqueryarrayextract的使用方法，检查第三个参数。

<br/>

## GAUSS-03441 - GAUSS-03450

<br/>

GAUSS-03441: "column is not in index"

SQLSTATE: XX000

错误原因: 索引中没有该属性列。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03442: "system catalog scans with lossy index conditions are not implemented"

SQLSTATE: XX000

错误原因: 内部功能不支持。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03443: "cannot do ordered scan on index '%s', because it is being reindexed"

SQLSTATE: XX000

错误原因: reindex的过程中不支持order scan。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03444: "invalid %s regproc"

SQLSTATE: XX000

错误原因: 非法的regproc。

解决办法: 内部错误，检查regproc。

GAUSS-03445: "multixact_redo: unknown op code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03451 - GAUSS-03460

<br/>

GAUSS-03453: "Xid is invalid."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03454: "current transaction with handle: (%d:%u) have a valid xid: %lu already"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03456: "cannot commit a transaction that deleted files but has no xid"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03457: "cannot commit transaction %lu, it was already aborted"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03458: "maximum number of committed subtransactions (%d) exceeded"

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03459: "cannot abort transaction %lu, it was already committed"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03461 - GAUSS-03470

<br/>

GAUSS-03461: "cannot PREPARE a transaction that has exported snapshots"

SQLSTATE: 0A000

错误原因: 两阶段事务不能作用于快照。

解决办法: 请关闭两阶段事务。

GAUSS-03462: "CleanupTransaction: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03463: "StartTransactionCommand: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03464: "CommitTransactionCommand: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03465: "%s cannot run inside a transaction block"

SQLSTATE: 25001

错误原因: 事务内不能执行如下SQL(eg: CREATE TABLESPACE，COMMIT PREPARED)。

解决办法: 请不要在事务内执行如下SQL(eg: CREATE TABLESPACE，COMMIT PREPARED)。

GAUSS-03466: "%s cannot run inside a subtransaction"

SQLSTATE: 25001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03467: "%s cannot be executed from a function or multi-command string"

SQLSTATE: 25001

错误原因: 函数不能执行如下SQL(eg: CREATE TABLESPACE，COMMIT PREPARED)。

解决办法: 请不要针对函数执行如下SQL(eg: CREATE TABLESPACE，COMMIT PREPARED)。

GAUSS-03468: "cannot prevent transaction chain"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03469: "%s can only be used in transaction blocks"

SQLSTATE: 25005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03470: "BeginTransactionBlock: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03471 - GAUSS-03480

<br/>

GAUSS-03471: "EndTransactionBlock: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03472: "UserAbortTransactionBlock: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03473: "DefineSavepoint: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03474: "no such savepoint"

SQLSTATE: 3B001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03475: "ReleaseSavepoint: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03476: "RollbackToSavepoint: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03477: "BeginInternalSubTransaction: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03478: "ReleaseCurrentSubTransaction: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03479: "RollbackAndReleaseCurrentSubTransaction: unexpected state %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03480: "invalid transaction block state: %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03481 - GAUSS-03490

<br/>

GAUSS-03481: "cannot have more than 2^32-1 subtransactions in a transaction"

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03482: "PopTransaction with no parent"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03483: "xact_redo: unknown op code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03485: "cannot assign TransactionIds during recovery"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03487: "Falling back to local Xid. Was = %lu, now is = %lu"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03491 - GAUSS-03500

<br/>

GAUSS-03492: "cannot assign OIDs during recovery"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03493: "Open file %s failed. %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03494: "Close file %s failed. %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03495: "seek file %s failed. %s"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03496: "clog_redo: unknown op code %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03498: "recovery is in progress"

SQLSTATE: 55000

错误原因: 系统处于前滚阶段，不能执行WAL控制函数。

解决办法: 请等待系统启动完毕后，重新执行WAL控制函数。

GAUSS-03500: "WAL level not sufficient for creating a restore point"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
