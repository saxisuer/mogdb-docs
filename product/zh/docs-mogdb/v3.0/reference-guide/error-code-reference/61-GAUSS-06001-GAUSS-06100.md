---
title: GAUSS-06001 - GAUSS-06100
summary: GAUSS-06001 - GAUSS-06100
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-06001 - GAUSS-06100

<br/>

## GAUSS-06001 - GAUSS-06010

<br/>

GAUSS-06001: "password encryption failed"

SQLSTATE: 28P01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06002: "cache lookup failed for role with oid %u"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06003: "%s is not supported for using here, just support template0"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06005: "could not drop database while ddl delay function is enabled"

SQLSTATE: 55006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06006: "database '%s' is used by a logical decoding slot"

SQLSTATE: 55006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06007: "Database '%s' is being accessed by other users. You can stop all connections by command: 'clean connection to all force for database XXXX;' or wait for the sessions to end by querying view: 'pg_stat_activity'."

SQLSTATE: 55006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06008: "dbase_redo: unknown op code %hhu"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06009: "Not support alter cross-database private pbject, please switch to '%s' and run this command"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06010: "permission denied to create foreign table in security mode"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06011 - GAUSS-06020

<br/>

GAUSS-06011: "Failed to create foreign table '%s'."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06012: "The schema 'dbe_perf' doesn't allow to drop"

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06013: "The schema 'snapshot' doesn't allow to drop"

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06014: "The schema '%s' doesn't allow to rename"

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06015: "It is not supported to rename schema '%s' which includes timeseries table '%s'."

SQLSTATE: 42939

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06016: "column/timeseries store unsupport constraint '%s'"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06017: " period must smaller than ttl."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06018: "It is unsupported to create row/cstore non-temporary/non-unlogged table in hadoop enviroment."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06019: "It is unsupported to create unlogged table and temporary table on DFS tablespace."

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06020: "It is unsupported to create table with to group option on DFS tablespace."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06021 - GAUSS-06030

<br/>

GAUSS-06021: "It is unsupported to create foreign table with to group option."

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06022: "Only support hash/replication distribution for dfs table."

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06023: "Partition can not be created on DFS tablespace.Only table-level tablespace can be DFS.DFS table only support partition strategy '%s' feature."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06024: " unsupported persistency for timeseries table."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06025: "Only support hash distribution for timeseries table."

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06026: "kvtype of '%s' must be defined when using timeseries."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06027: "TIMESERIES must have one and only one time column."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06028: "type '%s' is not supported in timeseries store"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06029: "Cannot use orientation is timeseries when enable_tsdb is off."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06030: "Only support one partition Key."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06031 - GAUSS-06040

<br/>

GAUSS-06031: "type '%s' does not exist."

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06032: " Partition Key must be of type TIMESTAMP(TZ) when using ttl or period."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06033: " Partition Key must be of kv type TSTAG."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06034: "ON COMMIT only support PRESERVE ROWS or DELETE ROWS option"

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06035: "Unsupport the dfs table in this version."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06036: "Only support one partial cluster key for dfs/cstore table."

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06037: "hash bucket table not supported in current version!"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06038: "Local OID column not supported in column/timeseries store tables."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06039: "column constraint on postgres foreign tables are not supported"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06040: "relation type column on postgres foreign tables are not supported"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06041 - GAUSS-06050

<br/>

GAUSS-06041: "The table %s do not support hash bucket"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06042: "Invalid Oid while setting new relfilenode for ts aux table."

SQLSTATE: 42602

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06043: "NOT-SUPPORT: Not support TRUNCATE multiple objects different nodegroup"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06044: "rel %s can not truncate during redis."

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06045: "column '%s' has a kvtype parameter conflict"

SQLSTATE: 42804

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06046: "RENAME SEQUENCE is not yet supported."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06047: "cannot find timeseries aux table (or index). timeseries table oid: %u"

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06048: "This ALTER command is not support in timeseries store."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06049: "alter row table tablespace cannot run inside a transaction block"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06050: "type '%s' is not supported in DFS table."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06051 - GAUSS-06060

<br/>

GAUSS-06051: "It's not supported to add column with default value for timeseries tables."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06052: "It's not supported to alter table add column default with nextval expression."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06053: "It is not supported on DFS table. The detailed reasons are the followings:"

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06054: "cannot drop TSTime column '%s' from timeseries table"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06055: "unrecognized table type: %d"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06056: "cannot alter column type to '%s'"

SQLSTATE: 42P16

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06057: "PSort %u should depend on only one index relation but not %ld."

SQLSTATE: OP002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06059: "no perm space is available for the targeted owner"

SQLSTATE: 53000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06060: "no perm space is available for the targeted user group"

SQLSTATE: 53000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06061 - GAUSS-06070

<br/>

GAUSS-06061: "no temp space is available for the targeted owner"

SQLSTATE: 53000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06062: "no temp space is available for the targeted user group"

SQLSTATE: 53000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06063: "can not specify 'PARTITION FOR (value,,,)' for 'MOVE PARTITION CLAUSE'"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06064: "invalid partition node type in 'MOVE PARTITION CLAUSE'"

SQLSTATE: XX004

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06065: "fail to remote read page, data corrupted in network"

SQLSTATE: XX001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06066: "invalid page in block %u of relation %s"

SQLSTATE: XX001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06067: "unknown type %u for ALTER TABLE ROW LEVEL SECURITY"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06068: "cache lookup failed for relation '%s'"

SQLSTATE: 29P01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06069: "unexpected identity type %u"

SQLSTATE: XX006

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06070: "cannot use non-unique index '%s' as replica identity"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06071 - GAUSS-06080

<br/>

GAUSS-06071: "cannot use expression index '%s' as replica identity"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06072: "cannot use partial index '%s' as replica identity"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06073: "cannot use invalid index '%s' as replica identity"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06074: "internal column %d in unique index '%s'"

SQLSTATE: 55000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06075: "index '%s' cannot be used as replica identity because column '%s' is nullable"

SQLSTATE: 42809

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06076: "The obs foreign partition table cannot support on text, csv, carbondata format."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06077: "The obs foreign table has column type bytea, cannot support on orc format."

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06078: "Invalid option '%s'"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06079: "There's dependent sequence, but ALTER SEQUENCE SET SCHEMA is not yet supported."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06080: "can't add partition bacause the relation %s has unusable local index"

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06081 - GAUSS-06090

<br/>

GAUSS-06081: "can't merge partition bacause partition %s has unusable local index"

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06082: "ALTER TABLE EXCHANGE requires both ordinary table and partitioned table to have the same hashbucket option(on or off)"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06083: "the kv storage type of column mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06084: "cache lookup failed for relaton %u"

SQLSTATE: 29P01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06085: "can't split partition bacause relation %s has unusable local index"

SQLSTATE: 42P17

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06086: "the partition oid(%u) of partition name (%s) is not found in partitioned table(%u)."

SQLSTATE: P0002

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06087: "Un-supported feature"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06088: "Cannot find a valid job_id."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06089: "cannot find timeseries aux table (or index). search name: %s"

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06090: "Unable to create partition policy when enable_tsdb is off."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-06091 - GAUSS-06100

<br/>

GAUSS-06091: "%s does not have partition !"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06092: "cannot create partiton policy for aux table."

SQLSTATE: OP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06093: "Unable to alter partition policy when enable_tsdb is off."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06094: "The function's %s %u dismatch the type %u"

SQLSTATE: 42804

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06095: "The index table does not support verify on cascade mode."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06096: "Non-table objects do not support verify."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06097: "The hdfs table does not support verify."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06098: "The temporary/unlog table does not support verify."

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06099: "The important catalog table %s.%s corrupts, the node is %s, please fix it."

SQLSTATE: XX001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-06100: "pg_class entry for relid %u vanished during updating TotalRows"

SQLSTATE: 2F000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
