---
title: 工具一览表
summary: 工具一览表
author: Zhang Cuiping
date: 2021-06-07
---

# 工具一览表

MogDB提供客户端和服务器端应用程序（工具），帮助用户更好地维护MogDB，提供的所有工具如[表1](#gongjuyilanbiao)所示。工具位于安装数据库服务器的\$GPHOME/script和$GAUSSHOME/bin路径下。

**表 1** 工具一览表 <a id="gongjuyilanbiao"> </a>

<table>
    <tr>
        <th>分类</th>
        <th>工具名称</th>
        <th>简介</th>
    </tr>
    <tr>
        <td>客户端工具</td>
        <td>gsql</td>
        <td>gsql是MogDB提供在命令行下运行的数据库连接工具，可以通过此工具连接服务器并对其进行操作和维护，除了具备操作数据库的基本功能，gsql还提供了若干高级特性，便于用户使用。</td>
    </tr>
    <tr>
        <td rowspan="13">服务端工具</td>
        <td>gs_cgroup</td>
        <td>gs_cgroup是MogDB提供的负载管理工具。负责创建默认控制组、创建用户自定义控制组、删除用户自定义控制组、更新用户自定义组的资源配额和资源限额、显示控制组配置文件内容、显示控制组树形结构和删除用户的所有控制组。</td>
    <tr/>
    <tr>
        <td>gs_check</td>
        <td>gs_check改进增强，统一化当前系统中存在的各种检查工具，例如gs_check，gs_checkos等，帮助用户在MogDB运行过程中，全量的检查MogDB运行环境、操作系统环境、网络环境及数据库执行环境，也有助于在MogDB重大操作之前对各类环境进行全面检查，有效保证操作执行成功。</td>
    </tr>
    <tr>
        <td>gs_checkos</td>
        <td>gs_checkos用于检查操作系统、控制参数、磁盘配置等内容，并对系统控制参数、I/O配置、网络配置和THP服务等信息进行配置。</td>
    </tr>
    <tr>
        <td>gs_checkperf</td>
        <td>gs_checkperf工具可定期对MogDB级别（主机CPU占用率、MogDB CPU占用率、I/O使用情况等）、节点级别（CPU使用情况、内存使用情况、I/O使用情况）、会话/进程级别（CPU使用情况、内存使用情况、I/O使用情况）、SSD性能（写入、读取性能）进行检查，让用户了解MogDB的负载情况，采取对应的改进措施。</td>
    </tr>
    <tr>
        <td>gs_collector</td>
        <td>gs_collector在MogDB发生故障时，收集OS信息、日志信息以及配置文件信息，来定位问题。</td>
    </tr>
    <tr>
        <td>gs_dump</td>
        <td>gs_dump是一款用于导出数据库相关信息的工具，支持导出完整一致的数据库对象（数据库、模式、表、视图等）数据，同时不影响用户对数据库的正常访问。</td>
    </tr>
    <tr>
        <td>gs_dumpall</td>
        <td>gs_dumpall是一款用于导出数据库相关信息的工具，支持导出完整一致的MogDB数据库所有数据，同时不影响用户对数据库的正常访问。</td>
    </tr>
    <tr>
        <td>gs_guc</td>
        <td>gs_guc用于设置MogDB配置文件（“postgresql.conf”、“pg_hba.conf”）中的参数，配置文件中参数的默认值是单机的配置模式，您可以使用gs_guc来设置适合的参数值。</td>
    </tr>
    <tr>
        <td>gs_encrypt</td>
        <td>gs_encrypt是一款用于为输入的明文字符串进行加密操作。</td>
    </tr>
    <tr>
        <td>gs_om</td>
        <td>MogDB提供了gs_om工具帮助对MogDB进行维护，包括启动MogDB、停止MogDB、查询MogDB状态、生成静态配置文件、刷新动态配置文件、SSL证书替换、启停kerberos认证、显示帮助信息和显示版本号信息的功能。</td>
    </tr>
    <tr>
        <td>gs_restore</td>
        <td>gs_restore是MogDB提供的针对gs_dump导出数据的导入工具。通过此工具可由gs_dump生成的导出文件进行导入。</td>
    </tr>
    <tr>
        <td>gs_ssh</td>
        <td>MogDB提供了gs_ssh工具帮助用户在MogDB各节点上执行相同的命令。</td>
    </tr>
</table>