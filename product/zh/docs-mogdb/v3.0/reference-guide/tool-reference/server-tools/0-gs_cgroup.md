---
title: gs_cgroup
summary: gs_cgroup
author: Zhang Cuiping
date: 2021-11-17
---

# gs_cgroup

## 背景信息

集群环境下做批量任务处理时，多任务复杂性，会导致不同机器间的负载差距很大。为了充分利用集群资源，负载管理变得尤为重要。gs_cgroup是MogDB提供的负载管理工具。负责创建默认控制组、创建用户自定义控制组、删除用户自定义控制组、更新用户自定义组的资源配额和资源限额、显示控制组配置文件内容、显示控制组树形结构和删除用户的所有控制组。

gs_cgroup工具为使用数据库的操作系统用户创建Cgroups配置文件，并且在操作系统中生成用户设定的Cgroups。同时为用户提供增加、删除Cgroups、更新Cgroups资源配额、设定Cgroups的CPU或IO限额、设定异常处理阈值及相应操作等服务。此工具只负责当前操作系统节点的Cgroups操作，使用时需在各个节点上调用相同命令语句进行统一配置。

这里假设读者已经了解了负载管理的相关原理，具体请参考《开发者指南》中“资源负载管理”章节。

## 使用示例

- 使用普通用户或数据库管理员执行命令。

1. 前置条件: 需设置GAUSSHOME环境变量为数据库安装目录；且root用户已创建普通用户默认的控制组。

2. 创建控制组及设置对应的资源配额，以便在数据库中运行作业时，指定到此控制组，使用此控制组管理的资源；通常数据库管理员为每个数据库用户创建Class组。

    1. 创建Class控制组和Workload控制组。

        ```bash
        gs_cgroup -c -S class1 -s 40
        ```

        创建当前用户新的Class Cgroups命名为`class1`，资源配额为总Class的40%。

        ```bash
        gs_cgroup -c -S class1 -G grp1 -g 20
        ```

        创建当前用户新的“class1” Cgroups下属的Workload控制组，命名为`grp1`，资源配额为`class1`Cgroups的20%。

    2. 删除grp1控制组和Class控制组。

        ```bash
        gs_cgroup -d -S class1 -G grp1
        ```

        删除当前用户已建的`grp1`Cgroups。

        ```bash
        gs_cgroup -d -S class1
        ```

        删除当前用户已建的`class1`Cgroups。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
    > 如果删除Class控制组，则Workload控制组也被删除。

3. 更新已创建控制组的资源配额。

    1. 更新动态资源配额

        ```bash
        gs_cgroup -u -S class1 -G grp1 -g 30
        ```

        更新当前用户的class1 Cgroups下属grp1 Cgroups资源配额为class1 Cgroups的30%。

    2. 更新限制资源配额

        ```bash
        gs_cgroup --fixed -u -S class1 -G grp1 -g 30
        ```

    更新当前用户的class1 Cgroups下属grp1 Cgroups限制使用CPU核数范围占上一级class1可用核数的30%。

4. 更新Gaussdb的CPU核数范围。

    ```bash
    gs_cgroup -u -T Gaussdb -f 0-20
    ```

    更新Gaussdb进程使用的CPU核数为0-20。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
    > -f参数只适用于对Gaussdb设置核数范围。对于其他各控制组，如需设置核数范围，需要使用-fixed参数。

5. 设置异常处理信息（class1:grp1组需存在）。

    1. 设置组class1:grp1下的作业阻塞到1200秒或执行2400秒时执行终止动作：

        ```bash
        gs_cgroup -S class1 -G grp1 -E "blocktime=1200,elapsedtime=2400" -a
        ```

    2. 设置组class1:grp1下的作业下盘数据量达到256MB或大表广播数据量达到100MB时执行终止动作：

        ```bash
        gs_cgroup -S class1 -G grp1 -E "spillsize=256,broadcastsize=100" -a
        ```

    3. 设置组class1下的作业在所有数据库节点上CPU总时间到达100s时执行降级动作：

        ```bash
        gs_cgroup -S class1 -E "allcputime=100" --penalty
        ```

    4. 设置组class1下的作业在所有数据库节点上执行时间到达2400秒，倾斜率达到90时执行降级动作:

        ```bash
        gs_cgroup -S class1 -E "qualificationtime=2400,cpuskewpercnt=90"
        ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
    > 给控制组设置异常处理信息，需要确保对应的控制组已经创建。指定多个阈值时用“，”分隔，不指定任何动作时默认为“penalty”操作。

6. 设置控制组使用的核数范围。

    设置组class1:grp1的核数范围占Class控制组的20%

    ```bash
    gs_cgroup -S class1 -G grp1 -g 20 --fixed -u
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
    > Class或Workload核数范围必须通过指定--fixed参数设置。

7. 回退上一个步骤。

    ```bash
    gs_cgroup --recover
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
    > --recover只支持对Class控制组和Workload控制组的增删改操作进行回退，且只支持回退一次操作。

8. 查看已创建的控制组信息。

    1. 查看配置文件中控制组信息。

        ```bash
        gs_cgroup -p
        ```

        控制组配置信息

        ```bash
        gs_cgroup -p

        Top Group information is listed:
        GID:   0 Type: Top    Percent(%): 1000( 50) Name: Root                  Cores: 0-47
        GID:   1 Type: Top    Percent(%):  833( 83) Name: Gaussdb:omm           Cores: 0-20
        GID:   2 Type: Top    Percent(%):  333( 40) Name: Backend               Cores: 0-20
        GID:   3 Type: Top    Percent(%):  499( 60) Name: Class                 Cores: 0-20

        Backend Group information is listed:
        GID:   4 Type: BAKWD  Name: DefaultBackend   TopGID:   2 Percent(%): 266(80) Cores: 0-20
        GID:   5 Type: BAKWD  Name: Vacuum           TopGID:   2 Percent(%):  66(20) Cores: 0-20

        Class Group information is listed:
        GID:  20 Type: CLASS  Name: DefaultClass     TopGID:   3 Percent(%): 166(20) MaxLevel: 1 RemPCT: 100 Cores: 0-20
        GID:  21 Type: CLASS  Name: class1           TopGID:   3 Percent(%): 332(40) MaxLevel: 2 RemPCT:  70 Cores: 0-20

        Workload Group information is listed:
        GID:  86 Type: DEFWD  Name: grp1:2           ClsGID:  21 Percent(%):  99(30) WDLevel:  2 Quota(%): 30 Cores: 0-5

        Timeshare Group information is listed:
        GID: 724 Type: TSWD   Name: Low              Rate: 1
        GID: 725 Type: TSWD   Name: Medium           Rate: 2
        GID: 726 Type: TSWD   Name: High             Rate: 4
        GID: 727 Type: TSWD   Name: Rush             Rate: 8

        Group Exception information is listed:
        GID:  20 Type: EXCEPTION Class: DefaultClass
        PENALTY: QualificationTime=1800 CPUSkewPercent=30

        GID:  21 Type: EXCEPTION Class: class1
        PENALTY: AllCpuTime=100 QualificationTime=2400 CPUSkewPercent=90

        GID:  86 Type: EXCEPTION Group: class1:grp1:2
        ABORT: BlockTime=1200 ElapsedTime=2400
        ```

        上述示例查看到的控制组配置信息如下表所示。

        **表 1** 控制组配置信息

        | GID  | 类型            | 名称                                                         | Percent（%）信息                                             | 特定信息                                                     |
        | :--- | :-------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
        | 0    | Top控制组       | Root                                                         | 1000代表总的系统资源为1000份。括号中的50代表IO资源的50%。MogDB不通过控制组对IO资源做控制，因此下面其他控制组信息中仅涉及CPU配额情况。 | -                                                            |
        | 1    |                 | mogdb:omm                                                    | 系统中只运行一套数据库程序，mogdb:omm控制组默认配额为833，数据库程序和非数据库程序的比值为（833:167=5:1）。 | -                                                            |
        | 2    |                 | Backend                                                      | Backend和Class括号中的40和60，代表Backend占用mogdb:dbuser控制组40%的资源，Class占用mogdb:dbuser控制组60%的资源。 | -                                                            |
        | 3    |                 | Class                                                        |                                                              | -                                                            |
        | 4    | Backend控制组   | DefaultBackend                                               | 括号中的80和20代表DefaultBackend和Vacuum占用Backend控制组80%和20%的资源。 | TopGID：代表Top类型控制组中Backend组的GID，即2。             |
        | 5    |                 | Vacuum                                                       |                                                              |                                                              |
        | 20   | Class控制组     | DefaultClass                                                 | DefaultClass和class1的20和40代表占Class控制组20%和40%的资源。因为当前只有两个Class组，所有它们按照20:40的比例分配Class控制组499的系统配额，则分别为166和332。 | - TopGID：代表DefaultClass和class1所属的上层控制（Top控制组中的Class组）的GID，即3。<br/>- MaxLevel：Class组当前含有的Workload组的最大层次，DefaultClass没有Workload Cgroup，其数值为1。<br/>- RemPCT：代表Class组分配Workload组后剩余的资源百分比。如class1中剩余的百分比为70。 |
        | 21   |                 | class1                                                       |                                                              |                                                              |
        | 86   | Workload控制组  | grp1:2（该名称由Workload Cgroup Name和其在class中的层级组成，它是class1的第一个Workload组，层级为2，每个Class组最多10层Workload Cgroup。） | 根据设置，其占class1的百分比为30，则为332*30%=99。           | - ClsGID：代表Workload控制组所属的上层控制组（class1控制组）的GID。<br/>- WDLevel：代表当前Workload Cgroup在对应的Class组所在的层次。 |
        | 724  | Timeshare控制组 | Low                                                          | -                                                            | Rate：代表Timeshare中的分配比例，Low最少为1，Rush最高为8。这四个Timeshare组的资源配比为Rush:High:Medium:Low=8:4:2:1。 |
        | 725  |                 | Medium                                                       | -                                                            |                                                              |
        | 726  |                 | High                                                         | -                                                            |                                                              |
        | 727  |                 | Rush                                                         | -                                                            |                                                              |

    2. 查看操作系统中树形结构的控制组信息

        `gs_cgroup -P`显示控制组树形结构信息，其中shares代表操作系统中CPU资源的动态资源配额`cpu.shares`的数值，cpus代表操作系统中CPUSET资源的动态资源限额`cpuset.cpus`的数值，指的是该控制组能够使用的核数范围。

        ```bash
        gs_cgroup -P
        Mount Information:
        cpu:/dev/cgroup/cpu
        blkio:/dev/cgroup/blkio
        cpuset:/dev/cgroup/cpuset
        cpuacct:/dev/cgroup/cpuacct

        Group Tree Information:
        - Gaussdb:wangrui (shares: 5120, cpus: 0-20, weight: 1000)
                - Backend (shares: 4096, cpus: 0-20, weight: 400)
                        - Vacuum (shares: 2048, cpus: 0-20, weight: 200)
                        - DefaultBackend (shares: 8192, cpus: 0-20, weight: 800)
                - Class (shares: 6144, cpus: 0-20, weight: 600)
                        - class1 (shares: 4096, cpus: 0-20, weight: 400)
                                - RemainWD:1 (shares: 1000, cpus: 0-20, weight: 100)
                                        - RemainWD:2 (shares: 7000, cpus: 0-20, weight: 700)
                                                - Timeshare (shares: 1024, cpus: 0-20, weight: 500)
                                                        - Rush (shares: 8192, cpus: 0-20, weight: 800)
                                                        - High (shares: 4096, cpus: 0-20, weight: 400)
                                                        - Medium (shares: 2048, cpus: 0-20, weight: 200)
                                                        - Low (shares: 1024, cpus: 0-20, weight: 100)
                                        - grp1:2 (shares: 3000, cpus: 0-5, weight: 300)
                                - TopWD:1 (shares: 9000, cpus: 0-20, weight: 900)
                        - DefaultClass (shares: 2048, cpus: 0-20, weight: 200)
                                - RemainWD:1 (shares: 1000, cpus: 0-20, weight: 100)
                                        - Timeshare (shares: 1024, cpus: 0-20, weight: 500)
                                                - Rush (shares: 8192, cpus: 0-20, weight: 800)
                                                - High (shares: 4096, cpus: 0-20, weight: 400)
                                                - Medium (shares: 2048, cpus: 0-20, weight: 200)
                                                - Low (shares: 1024, cpus: 0-20, weight: 100)
                                - TopWD:1 (shares: 9000, cpus: 0-20, weight: 900)
        ```

## 参数说明

- -a [--abort]

  对满足设定的异常阈值的作业执行终止动作。

- -b pct

  Backend Cgroups占用Top Backend资源的百分比，需同时指定`-B backendname`参数。

  取值范围

  - 为1 ~ 99。在不进行设置的情况下，默认CPU配额设置为Vacuum控制组占20%，DefaultBackend控制组占80%。Vacuum和DefaultBackend控制组配额之和设置应小于100%。

- -B name

  Backend Cgroups名称，仅可指定`-u`参数来更新此Cgroups的资源配额。

  `-b percent`和`-B backendname`参数共同作用来控制数据库后备线程的资源比例。

  取值范围: 字符串，最长为64个字节。

- -c

  创建Cgroups并指定其标识。

  普通用户指定`-c`和`-S classname`可以创建新的Class Cgroups，如果再指定`-G groupname`则创建属于Class Cgroups的Workload Cgroups，新的Workload Cgroups位于Class Cgroups的最底层（4层是最底层）。

- -d

  删除Cgroups及标识。

  普通用户指定`-d`和`-S classname`可以删除已有的Class Cgroups，如果再指定`-G groupname`则删除属于Class Cgroups的Workload Cgroups，并将附属的线程重置到`DefaultClass:DefaultWD:1`Cgroups中；若删除的Workload Cgroups位于Class Cgroups的高层（1层是最高层），则需调整低层的Cgroups的层级，重建新的Cgroups附属的线程加载到新的Cgroups。

- -E data

  设定异常阈值，目前阈值包括：`blocktime`、`elapsedtime`、`allcputime`、`spillsize`、`broadcastsize`以及`qualificationtime`和`cpuskewpercent`，指定多个阈值时用“，”分隔。参数值0表示取消设置，设置不合法的值时会提示。

  **表 2** 异常阈值类型

  | 异常阈值类型      | 说明                                                         | 取值范围（0表示取消设置） | 支持的异常操作 |
  | :---------------- | :----------------------------------------------------------- | :------------------------ | :------------- |
  | blocktime         | 作业的阻塞时间，单位秒。包括全局并发排队以及局部并发排队的总时间。 | 0~UINT_MAX                | abort          |
  | elapsedtime       | 作业的已被执行时间，单位秒。从开始执行到当前所消耗的时间。   | 0~UINT_MAX                | abort          |
  | allcputime        | 作业在所有数据库节点上执行时所耗费的CPU总时间，单位秒。      | 0~UINT_MAX                | abort，penalty |
  | cpuskewpercent    | 作业在数据库节点上执行时的CPU时间的倾斜率，依赖于qualificationtime的设置。 | 0~100                     | abort，penalty |
  | qualificationtime | 检查作业执行cpu倾斜率的间隔时间，单位秒，需同cpuskewpercent一起设置。 | 0~UINT_MAX                | none           |
  | spillsize         | 作业在数据库节点上下盘的数据量，单位MB。                     | 0~UINT_MAX                | abort          |
  | broadcastsize     | 作业在数据库节点上算子大表广播数据量，单位MB。               | 0~UINT_MAX                | abort          |

- -h [--help]

  显示命令帮助信息。

- -H

  用户环境中`$GAUSSHOME`信息。

  取值范围: 字符串，最长为1023个字符。

- -f

  设置Gaussdb控制组使用的核数范围，范围必须是a-b或a的形式。其他控制组可以使用-fixed进行设置核数范围。

- --fixed

  设置控制组使用的核数范围比例占上一层级的百分比或者设置IO资源。

  设置核数范围比例时--fixed设置核数范围与'-s' '-g' '-t' '-b' 一起使用。

  核数比例范围0-100，同一层级的核数比例总和小于或者等于100，0代表核数与上一层级相同，对于所有的控制组，CPU限额默认设置为0。-f和--fixed不能同时设置。设置-fixed之后，-f设置的范围自动失效。设置的比例以quota值在-p中进行显示。

  设置IO资源配额时，与'-R' '-r' '-W' '-w'一起使用。

- -g pct

  指定Workload Cgroups的资源占用`Class Cgroups`资源的百分比，需同时指定`-G groupname`参数；用于创建`-c`或更新`-u`Workload Cgroups。

  取值范围为1 ~ 99。默认Workload控制组CPU配额设置为20%。各Workload控制组配额之和应小于99%。

- -G name

  指定Workload Cgroups的名称，需同时指定`-S classname`参数来表示该group属于哪个Class Cgroups；可以连同`-c`参数创建新的Cgroups、`-d`参数删除此Cgroups及`-u`更新此Cgroups的资源配额；需要注意，此名称不可是Timeshare Cgroups的默认名称，如`Low`、`Medium”`、`High`或`Rush`。

  如果用户自己创建Workload控制组，指定名称时不能指定带 ':' 的名称。不能创建同名控制组。

  取值范围: 字符串，最长为28个字节。

- -N [--group] name

  可以将组名简写成class:wg。

- -p

  显示Cgroups配置文件的信息。

- -P

  显示Cgroups树形结构信息。

- --penalty

  对满足设定的异常阈值的作业执行降级动作，如果没有设定任何操作，则该操作将为默认操作。

- -r data

  仅用于更新I/O资源读数据上限，用于设置`blkio.throttle.read_bps_device`的数值；为字符串类型，该字符串由`major:minor value`构成，其中major为要访问的磁盘的主设备号，minor为要访问的磁盘的次设备号，value为设备每秒读操作次数上限数值，取值范围为0 ~ ULONG_MAX，其中取值0用来初始化此字段为空；需和“-u”参数及Cgroups名称一同使用；如果Class Cgroups和Workload Cgroups的名称同时指定，则只应用到Workload Cgroups。

  取值范围: 字符串，最长为32个字符。

- -R data

  仅用于更新IO资源每秒读操作次数上限，用于设置`blkio.throttle.read_iops_device`的数值；取值信息同`-r`参数；需和`-u`参数及Cgroups名称一同使用；如果Class Cgroups和Workload Cgroups的名称同时指定，则只应用到Workload Cgroups。

  取值范围: 字符串，最长为32个字符。

- --recover

  仅用于回退Class控制组和Workload控制组的增删改操作，且只能回退一步。

- --revert

  恢复控制组为默认状态。

- -D mpoint

  指定的挂载点，默认的挂载点`/dev/cgroup/subsystem`。

- -m

  挂载cgroup。

- -M

  取消挂载cgroup。

- -U

  数据库用户名称。

- --refresh

  刷新控制组状态。

- -s pct

  指定Class Cgroups的资源占用`Top Class Cgroups`资源的百分比，需同时指定`-S classname`参数；用于创建`-c`或更新`-u`Class Cgroups。

  取值范围为1-99。默认Class控制组的CPU配额设置为20%，R6C10版本中，Class控制组的CPU配额设置为40%，升级过程中，不会对此配额做更新。新创建的Class控制组的CPU配额以及默认的DefaultClass之和应小于100%。

- -S name

  指定Class group的名称；可以连同`-c`参数创建新的Cgroups、`-d`参数删除此Cgroups及`-u`更新此Cgroups的资源配额。创建子Class名称不能带‘：’。

  取值范围: 字符串，最长为31个字节。

- -t percent

  指定Top Cgroups（Root、Gaussdb:omm、Backend和Class Cgroups）占用资源的百分比，需同时指定“-T name”参数。若指定“-T Root”所在的Cgroups，其在Cgroups配置文件中显示的名称为“Root”，此数值代表的含义是blkio.weight值的百分比，最小值为10%，CPU资源配额如cpu.shares的数值不可修改；若指定“Gaussdb:omm” Cgroups，则表示占用整个系统CPU资源的百分比（可根据Root Cgroups的默认CPU配额1024得出该组的cpu.shares数值，此数值默认当前系统仅含有1套数据库环境），对于IO资源配额为1000，不再变化;若指定“Class”或“Backend”Cgroups，则表示资源占用“Gaussdb”Cgroups资源的百分比。

  取值范围为1 ~ 99。默认Class控制组配额为60%， Backend控制组配额为40%。修改Class控制组配额，同时会自动更新Backend控制组配额，使两者之和保持100%。

- -T name

  指定Top Cgroups的名称。

  取值范围: 字符串，最长为64个字节。

- -u

  更新Cgroups。

- -V [--version]

  显示gs_cgroup工具的版本信息。

- -w data

  仅用于更新I/O资源每秒写数据上限，用于设置`blkio.throttle.write_bps_device`的数值。取值信息同`-r`参数，需和`-u`参数及Cgroups名称一同使用。如果Class Cgroups和Workload Cgroups的名称同时指定，则只应用到Workload Cgroups。

  取值范围: 字符串，最长为32个字符。

- -W data

  仅用于更新IO资源每秒写操作次数上限，用于设置`blkio.throttle.write_iops_device`的数值。取值信息同`-r`参数，需和`-u`参数及Cgroups名称一同使用。如果Class Cgroups和Workload Cgroups的名称同时指定，则只应用到Workload Cgroups。

  取值范围: 字符串，最长为32个字符。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  对于磁盘major:minor的编号获取，可以通过下面方式。如获取/mpp目录对应的磁盘编号：
>
> ```bash
> df
> Filesystem      1K-blocks      Used  Available Use% Mounted on
> /dev/sda1       524173248  41012784  456534008   9% /
> devtmpfs         66059264       236   66059028   1% /dev
> tmpfs            66059264        88   66059176   1% /dev/shm
> /dev/sdb1      2920486864 135987592 2784499272   5% /data
> /dev/sdc1      2920486864  24747868 2895738996   1% /data1
> /dev/sdd1      2920486864  24736704 2895750160   1% /mpp
> /dev/sde1      2920486864  24750068 2895736796   1% /mpp1
> ls -l /dev/sdd
> brw-rw---- 1 root disk 8, 48 Feb 26 11:20 /dev/sdd
> ```
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 这里一定要查sdd的磁盘号，不能是sdd1的磁盘号。否则执行时会报错。 如果更新IO的限额配置信息超过了可允许的最大配置字串，则新的更新不存储在配置文件中。如当前设置的字串长度为96，更新IO的磁盘数量大于8个，则有可能超出字串限制，更新成功，但是不存储在配置文件中。
