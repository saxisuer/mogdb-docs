---
title: gs_gucquery
summary: gs_gucquery
author: Zhang Cuiping
date: 2022-06-22
---

# gs_gucquery

## 功能介绍

gs_gucquery工具可自动收集、整理、导出MogDB GUC值，并进行差异比较。

## 语法

- 导出GUC值

  ```bash
  gs_gucquery -t query [-o OUTPUTFILE] [-p PORT]
  ```

- 比较GUC值

  ```bash
  gs_gucquery -t compare --source-file=OUTPUTFILE --target-file=OUTPUTFILE --diff-file=OUTPUTDIR
  ```

## 参数说明

| 参数          | 说明                                                         |
| ------------- | ------------------------------------------------------------ |
| -t            | 指定gs_gucquery命令的类型。<br />取值范围：query或者compare  |
| --source-file | 指定被比较文件（前一次GUC值导出的结果）路径，需包含文件名。  |
| --target-file | 指定比较文件（本次GUC值导出的结果）路径，需包含文件名。      |
| --diff-file   | 指定差异比较结果的输出路径。                                 |
| -o            | 指定收集GUC值文件的导出路径。<br />如果不指定-o参数，则会在屏幕上输出结果。 |
| -p            | 指定需要导出GUC值所对应MogDB服务器的端口号。                 |

## 示例

1. 导出MogDB GUC值

   ```bash
   gs_gucquery -t query -o /data/gucRes
   ```

2. 比较MogDB GUC值

   ```bash
   gs_gucquery -t compare --source-file=/data/gucRes/GucQuery_20220203091229.csv --target-file /data/gucRes/GucQuery_20220204101330.csv --diff-file /data/gucRes/diffRes
   ```
