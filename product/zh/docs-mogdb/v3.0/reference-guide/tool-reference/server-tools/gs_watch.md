---
title: gs_watch
summary: gs_watch
author: Zhang Cuiping
date: 2022-06-22
---

# gs_watch

## 功能介绍

gs_watch工具可监测MogDB进程。当gs_watch开启时，MogDB进程发生了故障导致崩溃。gs_watch会自动触发调用gs_collector，对系统状态进行收集。

## 语法

- 启动自动触发

  ```bash
  gs_watch -t start [-o OUTDIR] [--pid=PID]
  ```

- 关闭自动触发

  ```bash
  gs_watch -t stop [--pid=PID]
  ```

## 参数说明

| 参数  | 说明                                                         |
| ----- | ------------------------------------------------------------ |
| -t    | 指定gs_watch命令的类型。<br />取值范围：start或者stop        |
| -o    | 指定系统崩溃时，gs_collector收集结果的导出路径。<br />注意：gs_collector如果不指定-o参数，则检查结果会默认以压缩包的形式输出到$GAUSSLOG路径中。 |
| --pid | 指定进程ID。<br />如果启动了多个MogDB进程，需要设置该参数来指定需要watch或停止watch的MogDB进程。否则会报错。 |

## 示例

1. 启动自动触发

   ```bash
   gs_watch -t start -o /opt/mogdb/data --pid=23550
   ```

2. 关闭自动触发

   ```bash
   gs_watch -t stop --pid=23550
   ```

## 相关命令

[gs_collector](4-gs_collector.md)