---
title: gs_uninstall
summary: gs_uninstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_uninstall

## 背景信息

MogDB提供了gs_uninstall工具来帮助完成MogDB的卸载。

## 语法

- 卸载MogDB

  ```bash
  gs_uninstall [--delete-data] [-L] [-l LOGFILE]
  ```

- 显示帮助信息

  ```bash
  gs_uninstall -? | --help
  ```

- 显示版本号信息

  ```bash
  gs_uninstall -V | --version
  ```

## 参数说明

- --delete-data

  删除数据文件。

- -L

  只卸载本地主机。如果MogDB内某主机做单点卸载后，MogDB不能再做全量卸载。

- -l

  指定日志文件名及可访问的绝对路径。在内部会自动给日志名添加一个时间戳。

  - 当既不明确指定-l，又不在XML文件中配置gaussdbLogPath时，默认值为“$GAUSSLOG/om/gs_uninstall-YYYY-MM-DD_hhmmss.log”。

  - 当不明确指定-l，但在XML文件中配置了gaussdbLogPath时，默认值为“gaussdbLogPath/用户名/om/gs_uninstall-YYYY-MM-DD_hhmmss.log”。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  由于在执行gs_uninstall时，系统会自动删除MogDB相关目录。因此建议用户通过该参数指定日志文件到非MogDB相关路径。

- -?, --help

  显示帮助信息。

- -V, --version

  显示版本号信息。

## 示例

使用数据库用户执行gs_uninstall脚本进行卸载MogDB。

```
gs_uninstall --delete-data
Checking uninstallation.
Successfully checked uninstallation.
Stopping the cluster.
Successfully stopped the cluster.
Successfully deleted instances.
Uninstalling application.
Successfully uninstalled application.
Uninstallation succeeded.
```

## 相关命令

[gs_install](6-gs_install.md)，[gs_postuninstall](7-gs_postuninstall.md)
