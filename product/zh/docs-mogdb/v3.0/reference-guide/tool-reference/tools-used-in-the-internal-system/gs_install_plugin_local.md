---
title: gs_install_plugin_local
summary: gs_install_plugin_local
author: Guo Huan
date: 2022-01-18
---

# gs_install_plugin_local

## 背景信息

MogDB提供了gs_install_plugin_local工具用于安装插件，目前支持安装的插件包括pg_repack、pg_trgm、dblink、wal2json、orafce、pg_bulkload、pg_prewarm等。本工具适用于单机部署的MogDB。

## 语法

```shell
gs_install_plugin_local [-h] [-p] [--all] [--force] [--plugins] [--PLUGIN_NAME] [--debug]
```

## 参数说明

- -h, --help

  显示帮助信息

- -p

  指定plugins安装包位置，默认在$GPHOME/script/static寻找plugins安装包

- --all

  安装全部插件

- --force

  指定覆盖旧的插件

- --plugins

  可安装多个插件，--plugins后跟插件名

- --PLUGIN_NAME

  指定安装某一个插件，如：--dblink

- --debug

  输出debug信息

> **注意**：在不指定安装某一个插件的情况下，默认安装全部插件

## 支持安装的插件列表

| 插件名      | 用途                                                         |
| ----------- | ------------------------------------------------------------ |
| dblink      | 提供跨数据库访问能力                                         |
| pg_trgm     | 提供函数和操作符测定字母数字文本基于三元模型匹配的相似性， 还有支持快速搜索相似字符串的索引操作符类 |
| wal2json    | 将逻辑日志文件输出为 JSON 格式                               |
| pg_repack   | 在线重建表和索引的扩展。它会在数据库中建立一个和需要清理的目标表一样的临时表，将目标表中的数据 COPY 到临时表，并在临时表上建立与目标表一样的索引，然后通过重命名的方式用临时表替换目标表。 |
| orafce      | 用于 Oracle 迁移到 MogDB 的兼容性                            |
| pg_bulkload | 用于MogDB的高速数据加载工具，相比copy命令。最大的优势就是速度，优在跳过shared buffer、wal buffer直接写文件。 |
| pg_prewarm  | 可以用于在系统重启时，手动加载经常访问的表到操作系统的cache或MogDB的shared buffer，从而减少检查系统重启对应用的影响 |
| PostGIS     | 空间数据处理插件，提供支持空间数据管理、数量测量与几何拓扑分析的能力，支持空间对象、空间索引、空间操作函数和操作符。 |
| whale       | Oracle兼容性插件，可以支持Oracle的特定函数和package。（语法及存储过程还处于开发中） |
| dolphin     | MySQL兼容性插件，可以支持MySQL的特定语法与行为，如MySQL中，sql mode未开启strict mode的对非空列的处理等特性。 |

## 安装示例

1. 插件包放置位置

   数据库未安装情况下，进入数据库安装目录下的script文件夹：

   ```shell
   cd /opt/software/mogdb/script/
   ```

   数据库已经安装的情况下

   ```shell
   su - omm
   cd $GPHOME/script/
   ```

2. 创建static文件夹：

    ```shell
    mkdir -p static
    ```

    > 注意：文件夹名称请勿更改。

3. 访问[MogDB官网下载页面](https://www.mogdb.io/downloads/mogdb)，根据您的操作系统及CPU架构下载对应版本的插件，并将插件放入static文件夹下。

4. 设置权限，例如：

    ```shell
    chmod +x Plugins-3.0.1-CentOS-x86_64.tar.gz
    ```

5. 安装插件

   - **安装全部插件**

      当不通过 `-p` 指定插件安装包路径时，默认在 `$GPHOME/script/static` 目录下获取

      ```shell
      # 不指定-p
      gs_install_plugin_local --all
      # gs_install_plugin_local 等价于  gs_install_plugin_local --all
      ```

      输出如下：

      ```shell
      [omm@hostname ~]$ gs_install_plugin_local  --all
      SUCCESS: pg_trgm
      SUCCESS: dblink
      SUCCESS: orafce
      SUCCESS: wal2json
      SUCCESS: pg_repack
      SUCCESS: pg_bulkload
      SUCCESS: pg_prewarm
      SUCCESS: dolphin
      SUCCESS: whale
      SUCCESS: postgis
      ```

   - **指定插件安装包路径安装**

      ```shell
      # 指定-p 和 --force
      gs_install_plugin_local -p /opt/mogdb/tools/script/static/Plugins-3.0.1-CentOS-x86_64.tar.gz --all --force
      ```

      输出如下：

      ```shell
      [omm@hostname ~]$ gs_install_plugin_local -p /opt/mogdb/tools/script/static/Plugins-3.0.1-CentOS-x86_64.tar.gz --all --force
      SUCCESS: pg_trgm
      SUCCESS: dblink
      SUCCESS: orafce
      SUCCESS: wal2json
      SUCCESS: pg_repack
      SUCCESS: pg_bulkload
      SUCCESS: pg_prewarm
      SUCCESS: dolphin
      SUCCESS: whale
      SUCCESS: postgis
      ```

   - **安装部分插件**

      ```shell
      gs_install_plugin_local --plugins dblink pg_trgm
      ```

      输出如下：

      这里因为之前安装了全部插件，所以要是重新进行插件安装的话，检测到之前的插件文件存在，会提示 `Warning`，默认不会覆盖掉已安装的插件，若需覆盖已安装的插件，需指定 `--force` 参数

      ```shell
      [omm@hostname ~]$ gs_install_plugin_local --plugins dblink pg_trgm
      Warning: (plugin: pg_trgm) file /opt/mogdb/app/lib/postgresql/pg_trgm.so already exists
      Warning: (plugin: pg_trgm) file /opt/mogdb/app/share/postgresql/extension/pg_trgm.control already exists
      Warning: (plugin: pg_trgm) file /opt/mogdb/app/share/postgresql/extension/pg_trgm--1.0.sql already exists
      Warning: (plugin: pg_trgm) file /opt/mogdb/app/share/postgresql/extension/pg_trgm--unpackaged--1.0.sql already exists
      SUCCESS: pg_trgm
      Warning: (plugin: dblink) file /opt/mogdb/app/lib/postgresql/dblink.so already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink.control already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink--1.0.sql already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink--unpackaged--1.0.sql already exists
      SUCCESS: dblink
      ```

   - **安装单个插件**

      ```shell
      gs_install_plugin_local --dblink --force
      ```

      输出如下：

      ```shell
      [omm@hostname ~]$ gs_install_plugin_local --dblink --force
      SUCCESS: dblink
      ```

   - **调试模式**

      ```shell
      gs_install_plugin_local --dblink --debug
      ```

      输出如下：

      ```shell
      [omm@hostname ~]$ gs_install_plugin_local --dblink --debug
      use xml file: 
      use plugin package file: /opt/mogdb/tools/script/static/Plugins-*.tar.gz
      local mode is True
      deconpress plugin package to /opt/mogdb/tools/script/static/plugins/
      make common dirs
      Warning: (plugin: dblink) file /opt/mogdb/app/lib/postgresql/dblink.so already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink.control already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink--1.0.sql already exists
      Warning: (plugin: dblink) file /opt/mogdb/app/share/postgresql/extension/dblink--unpackaged--1.0.sql already exists
      SUCCESS: dblink
      ```

## 相关页面

[orafce](../../../reference-guide/oracle-plugins/orafce-user-guide.md)、[dblink](../../../reference-guide/oracle-plugins/dblink-user-guide.md)、[pg_repack](../../../reference-guide/oracle-plugins/pg_repack-user-guide.md)、[pg_trgm](../../../reference-guide/oracle-plugins/pg_trgm-user-guide.md)、[wal2json](../../../reference-guide/oracle-plugins/wal2json-user-guide.md)、[pg_bulkload](../../../reference-guide/oracle-plugins/pg_bulkload-user-guide.md)、[pg_prewarm](../../../reference-guide/oracle-plugins/pg_prewarm-user-guide.md)、[PostGIS](../../../reference-guide/oracle-plugins/postgis-extension/postgis-overview.md)、[whale](../../../reference-guide/oracle-plugins/whale.md)、[dolphin](../../../reference-guide/oracle-plugins/dolphin.md)。