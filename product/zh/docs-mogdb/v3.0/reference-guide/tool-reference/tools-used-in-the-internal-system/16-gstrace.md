---
title: gstrace
summary: gstrace
author: Zhang Cuiping
date: 2021-06-07
---

# gstrace

## 功能介绍

gstrace是MogDB提供的用来跟踪内核代码执行路径、记录内核数据结构、分析代码性能的工具。gstrace允许用户指定一个或多个模块和函数进行追踪。Trace的有限点位和数据在版本中被固化，无法动态添加和删除。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **警告：**
>
> 1. 对内核dump指定内存变量的数据用于诊断分析，不存在直接指定任意寄存器或者内存地址的行为。读取的内存地址均是在开发阶段硬编码，没有任意地读取或者修改寄存器或内存的操作。
> 2. Trace点可能涉及敏感数据，收集trace信息前需要同用户协商，授权和许可后方可收集。
> 3. MogDB不会在敏感信息上打点，不会TRACE和用户相关的数据。
> 4. Trace仅用于诊断目的，开启trace将对性能产生一定影响，影响的大小视负载的高低，trace的模块而不同。
> 5. Trace工具的权限为0700，仅限于数据库用户读、写和执行。
>
> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明：** 如果进程异常终止，/dev/shm/ 目录下将会有gstrace_trace_cfg_*残留，可以手动清除。

## 语法

- 模块开关

  ```bash
  gstrace [start|stop|config|dump|detailcodepath|analyze] [-p <port>][-s <BUFFER_SIZE>] [-f <DUMP_FILE>] [-o <OUTPUT_FILE>] [-t <STEP_SIZE>] [-q <COMP_FUNC_PAIRS>]
  ```

  注意：由于语法定义中使用了‘,’，‘.’，‘&’作为分隔符，因此定义模块名时不可包括这些符号。除此之外，-q参数不能和原有的内部trace -m参数同时定义，因为两个选项都会影响trace结果，当同时定义时，会返回对应错误信息。

- 连接执行状态导出

  ```bash
  gstrace dump_usess -p <port> -o <OUTPUT_FILE>
  gstrace analyze_usess -f <INPUT_FILE> -o <OUTPUT_FILE>
  ```

## 参数说明

- 模块开关参数

| 参数              | 说明                                                         |
| :---------------- | :----------------------------------------------------------- |
| start             | 开始记录trace。                                              |
| stop              | 停止trace，释放存储信息的共享内存trace buffer。注意：trace buffer中已捕获trace信息会丢失。 |
| config            | 显示trace的配置信息。                                        |
| dump              | 将共享内存中的trace信息写入指定文件。若没有启动trace将报错。 |
| detail            | 将dump出来的二进制文件解析成文本文件，显示trace点的线程、时间信息。 |
| codepath          | 提取dump文件中的函数调用信息，按照调用栈的方式显示。         |
| analyze           | 统计各个函数的执行次数、总耗时、平均耗时、最长耗时、最短耗时。 |
| -p PORT           | 指定启动trace功能的实例侦听的端口号。                        |
| -f DUMP_FILE      | 指定dump导出的trace文件。                                    |
| -o OUTPUT_FILE    | 指定写入的文件。                                             |
| -t STEP_SIZE      | 指定分片分析的时间跨度（秒）,可选。将生成单独的{OUTPUT_FILE}.step文件。 |
| -m MASK           | 指定哪些模块、函数需要追踪，如果参数为空，则追踪添加trace接口的所有模块的所有函数。<br />格式：[comp…][ALL].[func…][ALL]<br />描述：<br />- comp 是模块列表，使用‘,’分隔，例如：executer,mogdb。也可以使用序号，例如：executer,2。<br />- func 是function列表，使用‘,’ 分隔。 例如：sql_execute_ddl, sql_execute_dcl。也可以使用序号指代模块中第几个function，例如：2。<br />- 设置成ALL表示所有模块或所有function被trace。<br />示例：<br />- executer,mogdb.ALL ：所有定义定义在executer和mogdb模块下的function被trace。<br />- executer,mogdb.sql_execute_ddl ：定义在executer 下的Function sql_execute_ddl被trace. mogdb模块被忽略，因为下面没有该function 。<br />- executer,mogdb.1 ：定义在模块executer和mogdb下的第一个function被trace。<br />ALL.1,2 ：定义在所有模块下的第一，二个function被trace。 |
| -s BUFFER_SIZE    | 指定用于trace功能的共享内存大小，默认为1G。如果指定的BUFFER_SIZE小于最小值2048，系统将自动调整为最小值。如果指定的BUFFER_SIZE不是2^N^（二的N次方），则向下对齐2^N^；例如：指定BUFFER_SIZE=3072，由于2^11^<3072<2^12^，系统将调整为2048。 |
| -q COMP_FUNC_PAIR | 指定哪些模块、函数需要追踪。参数不得为空。<br />使用语法格式及描述：<br /><COMP_FUNC_PAIRS> -> <COMP_FUNC_PAIR>[,<COMP_FUNC_PAIR>]\*<br />- COMP\_FUNC_PAIRS由一组及以上的模块与函数组合组成<br /><br /><COMP\_FUNC\_PAIR> -> <COMP\_NAME>.<FUNC\_NAMES><br />- COMP_FUNC_PAIR代表一组模块与函数组合<br />- COMP\_NAME 代表一个模块名<br /><br /><FUNC_NAMES> -> ALL\|[<FUNC_NAME>[&<FUNC_NAME>]*]<br />\- FUNC_NAMES 可由用户指定为ALL 或者一个及以上的函数名。由于FUNC_NAMES一定是与COMP_NAME组合，因此，ALL代表追踪以COMP_NAME为名的模块的所有函数；而如果用户指定的是一个及以上的函数名，则是追踪以COMP_NAME为名的模块中的这几个指定FUNC_NAME对应的函数名<br />\- FUNC_NAME 代表一个函数<br />示例：<br />\- “access.StartTransaction”：追踪一个模块及一个对应函数<br />\- “executer.ExecutorStart&ExecInitExpr”： 追踪一个模块及多个对应函数<br />\- “executer.ExecutorStart,tcop.PortalStart&PortalRun”： 追踪多个模块及多个对应函数<br />\- “executer.ALL,access.ALL”：使用ALL关键字追踪所有executor和access两个模块中的全部函数<br />注意：如果需要追踪所有模块及函数，不使用-q参数即可。举例：gstrace start -p 8000 |
| COMP_NAME         | 使用语法格式及描述：指定模块名称。                           |
| FUNC_NAME         | <COMP_FUNC_PAIRS> -> <COMP_FUNC_PAIR>[,<COMP_FUNC_PAIR>]* 指定函数名称。 |

- 连接执行状态导出参数

| 参数          | 说明                                                         |
| ------------- | ------------------------------------------------------------ |
| dump_usess    | 表示导出连接相关状态数据信息。<br />采用原始二进制格式。     |
| analyze_usess | 表示分析生成可阅读文件。<br />采用文本可阅读格式。           |
| -p            | 指定控制MogDB服务器端口。                                    |
| -o            | 指定输出的文件，可包含路径。输出文件需要不存在，否则报错。   |
| -f            | 指定输入的文件，可包含路径。输入文件必须存在且符合格式要求，否则报错。 |

## 示例

1. 启动trace

   ```bash
   gstrace start -p 8000
   ```

2. 停止trace

   ```bash
   gstrace stop -p 8000
   ```

3. 查看trace配置

   ```bash
   gstrace config -p 8000
   ```

4. dump trace

   ```bash
   gstrace dump -p 8000 -o /data/8000.dump
   ```

5. 解析dump detail信息

   ```bash
   gstrace detail -f /data/8000.dump -o /data/8000.detail
   ```

6. 解析dump codepath

   ```bash
   gstrace codepath -f /data/8000.dump -o /data/8000.codepath
   ```

7. 分析全局性能

   ```bash
   gstrace analyze -f /data/8000.dump -o /data/8000.perf
   ```

8. 分析分片性能

   ```bash
   gstrace analyze -f /data/8000.dump -o /data/8000.perf -t 1
   ```

9. 跟踪executer模块的全部函数

   ```bash
   gstrace start -p 7654 -q executer.ALL
   ```

10. 跟踪rewrite模块中的全部函数以及optimizer模块的create_plan()和query_planner()函数

    ```bash
    gstrace start -p 7654 -q "rewrite.ALL,optimizer.create_plan&query_planner"
    ```

11. 导出获得二进制user_session文件

    ```bash
    gstrace dump_usess -p 7654 -o /home/mogdbuser/dump1.bin
    ```

12. 创建可阅读json版本的user_session信息文件

    ```bash
    gstrace analyze_usess -f /home/mogdbuser/dump1.bin -o /home/mogdbuser/analyze1.json
    ```