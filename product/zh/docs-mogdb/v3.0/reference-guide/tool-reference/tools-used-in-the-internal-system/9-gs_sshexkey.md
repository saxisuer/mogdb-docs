---
title: gs_sshexkey
summary: gs_sshexkey
author: Zhang Cuiping
date: 2021-06-07
---

# gs_sshexkey

## 背景信息

MogDB在安装过程中，需要在MogDB中的节点间执行命令，传送文件等操作。因此，安装前需要确保互信是连通的。MogDB提供了gs_sshexkey工具来帮助用户建立互信。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知** ：
>
> - root用户互信可能会存在安全隐患，因此建议用户在使用完成后，立即删除各主机上root用户的互信。
> - 企业版安装场景下，出于安全考虑，预安装完成后会自动删除改工具。 使用gs_sshexkey工具建立互信成功之后，会在用户目录下面生成gaussdb_tmp目录，该目录用于存放ssh-agent进程文件gauss_socket_tmp和加密私钥的密码短语文件（随机生成32位密码短语，通过encrypt工具加密），同时用户下会拉起两个ssh-agent进程，此进程用于挂载私钥自动输入对应的密码短语。进程拉起完毕之后，需要source用户的环境变量，才能使互信功能生效。

## 前提条件

- 确保ssh服务打开。

- 确保ssh端口不会被防火墙关闭。

- 确保xml文件中各主机名称和IP配置正确。

- 确保所有机器节点间网络畅通。

- 如果为普通用户建立互信，需要提前在各主机创建相同用户并设置密码。

- 执行命令前，需要检查是否所有集群节点都支持expect命令，如果不支持请自行下载expect工具，建议使用expect version 5.45。

- 如果各主机安装并启动了SELinux服务，需要确保/root和/home目录安全上下文为默认值（root目录：system_u:object_r:home_root_t:s0，home目录：system_u:object_r:admin_home_t:s0）或者关闭掉SELinux服务。

  检查系统SELinux状态的方法：执行命令getenforce，如果返回结果是Enforcing ，说明SELinux安装并启用。

  检查目录安全上下文的命令：

  ```bash
  ls -ldZ  /root | awk '{print $4}'
  ```

  ```bash
  ls -ldZ  /home | awk '{print $4}'
  ```

  恢复目录安全上下文命令：

  ```bash
  restorecon -r -vv /home/
  ```

  ```bash
  restorecon -r -vv /root/
  ```

## 语法

- 建立互信

  ```bash
  gs_sshexkey -f HOSTFILE  [...] [--skip-hostname-set] [-l LOGFILE]
  ```

- 显示帮助信息

  ```bash
  gs_sshexkey -? | --help
  ```

- 显示版本号信息

  ```bash
  gs_sshexkey -V | --version
  ```

## 参数说明

- -f

  主机列表，列出所有需要建立互信主机的IP。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  确保hostfile文件中只配置主机IP（每行只写一个IP)，不包含其它信息。

- -l

  指定日志文件的保存路径。

  取值范围: 任意存在的可访问的绝对路径。

- --skip-hostname-set

  是否将“-f”参数文件中IP与其hostname的映射关系写入“/etc/hosts”文件中。默认写入，如果指定该参数则不写入。普通用户无权限编辑/etc/hosts文件，故有无指定该参数，普通用户均不生成映射关系。

- --no-deduplicate

  是否在配置互信修改/etc/hosts文件的时候，去除/etc/hosts文件中已经配置过的主机信息，默认是去重复。

- -?, --help

  显示帮助信息。

- -V, --version

  显示版本号信息。

## 示例

如下是为root用户建立互信的示例。仅支持交互式输入密码。

- 用户密码相同情况下，交互式模式使用以下命令建立互信。

  ```bash
  gs_sshexkey -f /opt/software/hostfile
  Please enter password for current user[root].
  Password:
  Checking network information.
  All nodes in the network are Normal.
  Successfully checked network information.
  Creating SSH trust.
  Creating the local key file.
  Appending local ID to authorized_keys.
  Successfully appended local ID to authorized_keys.
  Updating the known_hosts file.
  Successfully updated the known_hosts file.
  Appending authorized_key on the remote node.
  Successfully appended authorized_key on all remote node.
  Checking common authentication file content.
  Successfully checked common authentication content.
  Distributing SSH trust file to all node.
  Successfully distributed SSH trust file to all node.
  Verifying SSH trust on all hosts.
  Successfully verified SSH trust on all hosts.
  Successfully created SSH trust.
  ```
