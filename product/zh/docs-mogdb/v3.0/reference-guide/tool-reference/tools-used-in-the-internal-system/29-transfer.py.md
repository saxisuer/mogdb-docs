---
title: transfer.py
summary: transfer.py
author: Zhang Cuiping
date: 2021-11-17
---

# transfer.py

## 功能介绍

在非单节点模式下，可以通过transfer.py工具将全文检索所使用的词典文件和PostGIS插件所需的文件发送到其他节点。

## 前提条件

- 执行该工具前，需确保在$GAUSSHOME目录下存在以下四个文件，否则会报错，错误码为52200。
  - script/gspylib/common/DbClusterInfo.py。
  - script/gspylib/common/Common.py。
  - script/gspylib/common/GaussLog.py。
  - script/gspylib/threads/SshTool.py。
- $GPHOME环境变量配置正确。

## 语法

- 将sourcefile发送到所有节点的destinationpath路径。

  ```bash
  transfer.py 1 sourcefile destinationpath
  ```

- 将sourcefile发送到pgxc_node_name的standby节点的相同路径下。

  ```bash
  transfer.py 2 sourcefile pgxc_node_name
  ```

- 显示帮助信息。

  ```bash
  transfer.py -? | --help
  ```

## 参数说明

- OPTION取值如下所示：

  - 1：将当前文件发送到所有节点的目标文件路径下。
  - 2：将当前文件发送到目标节点的standby节点的相同路径下。

- sourcefile

  被发送的源文件路径。

- destinationpath

  目标文件路径。

- pgxc_node_name

  目标节点名。
