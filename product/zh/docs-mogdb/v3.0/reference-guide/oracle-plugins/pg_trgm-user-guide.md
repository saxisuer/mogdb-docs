---
title: pg_trgm使用手册
summary: pg_trgm使用手册
author: Guo Huan
date: 2021-11-29
---

# pg_trgm

## pg_trgm简介

pg_trgm模块提供函数和操作符测定字母数字文本基于三元模型匹配的相似性，还有支持快速搜索相似字符串的索引操作符类。

<br/>

## pg_trgm安装

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

<br/>

## pg_trgm使用

1. sql端执行：

   ```sql
   -- 创建pg_trgm拓展
   create extension pg_trgm;
   ```

2. 创建一张表插入一百万条数据。

   ```sql
   create table trgm_test(id int, name varchar);
   insert into trgm_test select generate_series(1,1000000),md5(random()::name);
   ```

3. 不使用pg_trgm进行查询。

   ```sql
   explain analyze select * from trgm_test where name like '%69089%';
   ```

   ```sql
   select * from trgm_test where name like '%69089%';
   ```

4. 创建索引。

   ```sql
   create index idx_trgm_test_1 on trgm_test using gin(name gin_trgm_ops);
   ```

   ```sql
   explain analyze select * from trgm_test where name like '%ad44%';
   ```

   ```sql
   select * from trgm_test where name like '%305696%';
   ```
