---
title: pg_prewarm使用手册
summary: pg_prewarm使用手册
author: Guo Huan
date: 2021-11-29
---

# pg_prewarm

## pg_prewarm简介

pg_prewarm模块提供一种方便的方法把关系数据载入到操作系统缓冲区缓存或者MogDB缓冲区缓存。

<br/>

## pg_prewarm安装

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

<br/>

## pg_prewarm加载模式

- mode：加载模式，可选项有“prefetch”、“read”、“buffer”，默认为“buffer”
- prefetch：异步地将数据预加载到操作系统缓存
- read：最终结果跟prefetch一样，但它是同步方式，支持所有平台
- buffer：将数据预加载到数据库缓存

<br/>

## pg_prewarm创建及使用

1. 创建pg_prewarm。

   ```sql
   create extension pg_prewarm; 
   ```

2. 创建测试表。

   ```sql
   create table test_pre (id int4,name character varying(64),creat_time timestamp(6) without time zone); 
   ```

3. 插入数据。

   ```sql
   insert into test_pre select generate_series(1,100000),generate_series(1,100000)||  '_pre',clock_timestamp(); 
   ```

4. 查看数据表大小。

   ```sql
   MogDB=# select pg_size_pretty(pg_relation_size('test_pre'));
   pg_size_pretty
   ----------------
   5136 kB
   (1 row)
   ```

5. 加载数据到数据库缓存。这里可以看到pg_prewarm把所有数据分为了642块。

   ```sql
   MogDB=# select pg_prewarm('test_pre','buffer');
    pg_prewarm
   ------------
        642
   (1 row)
   ```

6. 查块大小。

   ```sql
   MogDB=# select current_setting('block_size');
    current_setting
   -----------------
   8192
   (1 row)
   ```

   MogDB默认每个数据块的大小为8KB。

   ```sql
   MogDB=# select 642*8; 
    ?column?
   ----------
     5136
   (1 row)
   ```