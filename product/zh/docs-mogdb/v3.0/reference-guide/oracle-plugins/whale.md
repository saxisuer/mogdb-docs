---
title: whale
summary: whale
author: Zhang Cuiping
date: 2022-06-27
---

# whale

## 简介

whale是MogDB针对Oracle的兼容插件包，新增了Oracle函数以及package等功能。其中新增函数15个，例如instrb，nls_charset_id，nls_charset_name，nls_lower等。新增Oracle包7个，分别为dbms_random，dbms_output，dbms_lock，dbms_application_info，dbms_metadata，dbms_job，dbms_utility。 

> **注意**：whale插件和orafce插件不能同时使用，后续补丁版本将解决该问题。

## 安装whale

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

## 使用whale

导入whale插件功能

```sql
CREATE EXTENSION whale;
```

> 注意：创建完此extension并提交后，所有已存在的连接需要重新连接服务端才能不指定whale schema时自动找到whale下的函数。

### 函数

目前支持的oracle函数包含instrb，nls_charset_id，nls_charset_name，nls_lower，nls_upper， ora_hash， remainder，replace，show，show_parameter，to_timestamp， to_yminterval，tz_offset，nullif，ratio_to_report。

### Package

用户常用的函数大都存储在Package中。可以将自定义的常用函数封装到Package中，使用时指定Package来调用对应函数。

Package一般用于存储过程，包含dbms_random，dbms_output，dbms_lock，dbms_application_info，dbms_metadata，dbms_job，dbms_utility。

#### DBMS_RANDOM

DBMS_RANDOM提供内置随机数生成器。主要包含如下内置接口：

- DBMS_RANDOM.INITIALIZE(val IN BINARY_INTEGER)：使用val值初始化包。

  ```sql
  select DBMS_RANDOM.INITIALIZE(101);
  ```

- DBMS_RANDOM.NORMAL：函数返回标准正态分布中的随机数。

  ```sql
  select DBMS_RANDOM.NORMAL();
  ```

- DBMS_RANDOM.RANDOM：它生成并返回一个随机数, 范围[-2^^31, 2^^31) 的整数。

  ```sql
  select DBMS_RANDOM.RANDOM();s
  ```

- DBMS_RANDOM.SEED：过程重置用于生成随机数的seed。

  ```
  select DBMS_RANDOM.SEED(1);
  ```

- DBMS_RANDOM.STRING(opt IN CHAR,len IN NUMBER)RETURN VARCHAR2：该函数生成并返回一个随机字符串。

  ```sql
  select DBMS_RANDOM.STRING('A', 10);
  select DBMS_RANDOM.STRING('x', 10);
  ```

  生成一个指定模式指定位数的随机字符串，可选模式opt如下：
  'u'或'U'，仅返回大写字母
  'l'或'L'，仅返回小写字母
  'a'或'A'，返回大小写字母混合
  'x'或'X'，返回大写字母和数字混合
  'p'或'P'，返回任意可显示字符

- DBMS_RANDOM.TERMINATE：此过程已弃用。尽管当前受支持，但不应使用它。当用户完成包时将调用它。
- DBMS_RANDOM.VALUE RETURN NUMBER：返回一个[0, 1)的随机数，小数点右侧有15位。

#### DBMS_OUTPUT 

DBMS_OUTPUT 包使您能够从存储过程、包和触发器发送消息。DBMS_OUTPUT 包主要包括如下的内置接口：

> **注意**：
>
> - 使用范围，只适用于存储过程，不适用于gsql。
>
> - 不支持 set serveroutput on;  
>
> - 不支持 set serveroutput off;

- DBMS_OUTPUT.ENABLE(buffer_size IN INTEGER DEFAULT 20000)：设置buff_size,最大值1000000，最小值2000。使用DBMS_OUTPUT之前，必须执行DBMS_OUTPUT.ENABLE。

- DBMS_OUTPUT.GET_LINE(line INOUT text, status INOUT INTEGER)：此过程从缓冲区中检索行数组。

- DBMS_OUTPUT.GET_LINES(lines INOUT text[], numlines INOUT INTEGER)：此过程从缓冲区中检索行数组。

  **说明**：DBMS_OUTPUT.GET_LINE和DBMS_OUTPUT.GET_LINES之后，缓冲区清空，获取的数据为空。

- DBMS_OUTPUT.NEW_LINE：此过程放置一个空格。
- DBMS_OUTPUT.PUT(item IN VARCHAR2)：此过程在缓冲区中放置部分行。
- DBMS_OUTPUT.PUT_LINE(item IN VARCHAR2)：此过程在缓冲区中放置一行。

- DBMS_OUTPUT.disable ()：销毁申请的空间。

- DBMS_OUTPUT.数据类型：DBMS_OUTPUT package内置了CHARARR数据类型。在package外，可以DBMS_OUTPUT.CHARARR来使用该类型。DBMS_OUTPUT package暂时不支持DBMSOUTPUT_LINESARRAY数据类型。

#### DBMS_LOCK

DBMS_LOCK 包为 Oracle 锁管理服务提供了一个接口。

dbms_lock.sleep:pg_sleep()

#### DBMS_APPLICATION_INFO

DBMS_APPLICATION_INFO包用来记录数据库中正在执行的模块或事务的名称，以供以后跟踪各种模块的性能和调试时使用。

DBMS_APPLICATION_INFO包主要包括如下的内置接口：

- DBMS_APPLICATION_INFO.READ_CLIENT_INFO (client_info OUT VARCHAR2)：此过程读取当前会话的 client_info 字段的值。

- DBMS_APPLICATION_INFO.READ_MODULE (module_name OUT VARCHAR2, action_name OUT VARCHAR2)：此过程读取当前会话的模块和操作字段的值。
- DBMS_APPLICATION_INFO.SET_CLIENT_INFO (client_info IN VARCHAR2)：此过程提供有关客户端应用程序的附加信息。
- DBMS_APPLICATION_INFO.SET_MODULE ( module_name IN VARCHAR2, action_name IN VARCHAR2)：此过程设置当前应用程序或模块的名称。

- DBMS_APPLICATION_INFO.SET_ACTION (action_name IN VARCHAR2)：此过程设置当前模块下当前动作的名称。

#### DBMS_METADATA

DBMS_METADATA 包提供了一种从数据库字典中检索元数据作为 XML 或创建 DDL 并提交 XML 以重新创建对象的方法。

dbms_metadata.get_ddl()

#### DBMS_JOB

DBMS_JOB 包调度和管理作业队列中的作业。

DBMS_JOB包主要包括如下的内置接口：

- DBMS_JOB.BROKEN( job    int8, broken   bool, next_date timestamp default sysdate)：此过程设置损坏的标志。Broken 表示是否finish job; 终止job时，job状态job_status设置为 ‘d’。
- DBMS_JOB.CHANGE( job     int8, what     text, next_date  timestamp, "interval" text, instance  int4    default null, force    bool    default false)：此过程更改用户可以在job中设置的任何字段。
  - CHANGE的what，next_date，interval 至少有一个参数不为空。如果参数为空，则不修改参数。
  - instance：本参数在dbms_job中无用。
  - force：本参数在dbms_job中无用。

- DBMS_JOB.INSTANCE( job    int8, instance int4, force   bool  default false)()：此过程不做任何操作。

- DBMS_JOB.INTERVAL( job      int8, "interval"  text)：此过程更改job运行的频率。

- DBMS_JOB.NEXT_DATE( job     int8, next_date  timestamp)：此过程更改job下次运行的时间。

  **说明**：一般需要把next_date设置为当前系统时间之后。如果把next_date设置为当前系统时间之后，就会立即执行任务，并且把next_date设置为当前系统时间+interval。

- DBMS_JOB.REMOVE(job int8)：此过程从job队列中删除job。
- DBMS_JOB.RUN( job   int8, force boolean default false)：此过程运行job。Force参数无用。

- DBMS_JOB.SUBMIT( job     out int8, what     in text, next_date  in timestamp default sysdate, "interval" in text    default null, no_parse   in bool    default false, instance  in int4    default null, force    in bool    default false)：提交一个新的job。no_parse，instance，force：这个3个参数不使用。如果使用，有提示信息。
- DBMS_JOB.USER_EXPORT(job IN int8, mycall OUT text)：生成调用文本以重新创建给定的job。
- DBMS_JOB.WHAT( job  int8, what  text)：此过程更改现有job的功能，并替换其环境。如果what不存在，则报错。

#### DBMS_UTILITY

DBMS_UTILITY包用于数据类型处理和计算。dbms_utility包主要包括如下内置接口：

- DBMS_UTILITY.CANONICALIZE( name     IN  text, canon_name  OUT text, canon_len  IN  int4)：此过程规范化给定的字符串。 该过程处理单个保留字或关键字（例如“table”），并去除单个标识符的空格，以便“table”变为 TABLE。

- DBMS_UTILITY.COMMA_TO_TABLE( list     IN  text, tablen   OUT  int4, tab     OUT  text[])：此过程将逗号分隔的名称列表转换为名称的 PL/SQL 表。

- DBMS_UTILITY.TABLE_TO_COMMA(tab IN text[], tablen OUT int4, val OUT text)：此过程将名称的 PL/SQL 表转换为以逗号分隔的名称列表。
- DBMS_UTILITY.DB_VERSION(INOUT version text, INOUT compatibility text)：此过程返回数据库的版本信息。
- DBMS_UTILITY.EXEC_DDL_STATEMENT(IN parse_string  text)：此过程执行 parse_string 中的 DDL 语句。

当前DBMS_UTILITY package支持INSTANCE_RECORD，DBLINK_ARRAY，INDEX_TABLE_TYPE，INSTANCE_TABLE，LNAME_ARRAY，NAME_ARRAY，NUMBER_ARRAY和UNCL_ARRAY数据类型。

## 示例

### 函数

- 调用INSTRB函数

  ```sql
  MogDB=# select INSTRB('123456123', '123', 4);
   instrb 
  --------
        7
  (1 row)
  ```
  
- 调用NLS_CHARSET_ID函数

  ```sql
  MogDB=# SELECT NLS_CHARSET_ID('gbk');
   nls_charset_id 
  ----------------
                6
  (1 row)
  ```
  
- dbms_random提供内置随机数生成器

  ```sql
  MogDB=# select DBMS_RANDOM.VALUE(1, '100');
        value       
  ------------------
   92.4730090592057
  ```

### package

下面以dbms_random包为例：

```sql
MogDB=# select DBMS_RANDOM.VALUE();
       value       
-------------------
 0.482205999083817
(1 row)
```
