---
title: DBlink使用手册
summary: DBlink使用手册
author: Guo Huan
date: 2021-11-29
---

# dblink

## dblink简介

dblink用于在远程数据库中执行查询，通常是一个`SELECT`，但是也可以是任意返回行的SQL语句。

当给出两个`text`参数时，首先将第一个作为持久连接的名称进行查找；如果找到，则在该连接上执行命令。如果未找到，则将第一个参数视为与`dblink_connect`一样的连接信息字符串，并且仅在此命令期间进行指示的连接。

<br/>

## dblink安装

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

<br/>

## dblink使用

### sql端创建拓展

```sql
create extension dblink;
```

### 检查dblink是否创建成功

```
\dx
```

### 连接远端数据库执行查询操作

```sql
select * from dblink('dbname=postgres host=127.0.0.1 port=12345 user=test password=Test123456'::text, 'select * from dblink_test'::text)t(id int, name varchar);
```

### 创建连接

```sql
select dblink_connect('dblink_conn','hostaddr=127.0.0.1 port=12345 dbname=postgres user=test password=Test123456');
```

### 数据库表操作

（视图不支持查询操作）

```sql
select dblink_exec('dblink_conn', 'create table ss(id int, name int)');
select dblink_exec('dblink_conn', 'insert into ss values(2,1)');
select dblink_exec('dblink_conn', 'update ss set name=2 where id=1');
select dblink_exec('dblink_conn', 'delete from ss where id=1');
```

### 解除连接

```sql
select dblink_disconnect('dblink_conn')
```
