---
title: PostGIS使用
summary: PostGIS使用
author: Guo Huan
date: 2022-04-29
---

# PostGIS使用

## 下载PostGIS插件

访问[MogDB官网下载页面](https://www.mogdb.io/downloads/mogdb/)下载PostGIS插件并安装。

## 安装PostGIS插件

请参见[gs_install_plugin](../../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

## 创建Extension

使用CREATE EXTENSION命令分别创建postgis、postgis_raster以及postgis_topology：

```sql
MogDB=# CREATE EXTENSION postgis;
```

```sql
MogDB=# CREATE EXTENSION postgis_raster;
```

```sql
MogDB=# set behavior_compat_options='bind_procedure_searchpath';
CREATE EXTENSION postgis_topology;
```

## 使用Extension

PostGIS Extension函数调用格式为：

```sql
MogDB=# SELECT GisFunction (Param1, Param2,......);
```

其中GisFunction为函数名，Param1、Param2等为函数参数名。下列SQL语句展示PostGIS的简单使用，对于各函数的具体使用，请参考[《PostGIS-2.4.2用户手册》](https://download.osgeo.org/postgis/docs/postgis-2.4.2.pdf)。

示例1：几何表的创建。

```sql
MogDB=# CREATE TABLE cities ( id integer, city_name varchar(50) );
MogDB=# SELECT AddGeometryColumn('cities', 'position', 4326, 'POINT', 2);
```

示例2：几何数据的插入。

```sql
MogDB=# INSERT INTO cities (id, position, city_name) VALUES (1,ST_GeomFromText('POINT(-9.5 23)',4326),'CityA');
MogDB=# INSERT INTO cities (id, position, city_name) VALUES (2,ST_GeomFromText('POINT(-10.6 40.3)',4326),'CityB');
MogDB=# INSERT INTO cities (id, position, city_name) VALUES (3,ST_GeomFromText('POINT(20.8 30.3)',4326), 'CityC');
```

示例3：计算三个城市间任意两个城市距离。

```sql
MogDB=# SELECT p1.city_name,p2.city_name,ST_Distance(p1.position,p2.position) FROM cities AS p1, cities AS p2 WHERE p1.id > p2.id;
```

## 删除Extension

在MogDB中删除PostGIS Extension的方法如下所示：

```sql
MogDB=# DROP EXTENSION postgis [CASCADE];
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif)**说明**:  如果Extension被其它对象依赖（如创建的几何表），需要加入CASCADE（级联)关键字，删除所有依赖对象。

若要完全删除PostGIS Extension，则需由omm用户使用gs_om工具移除PostGIS及其依赖的动态链接库，格式如下：

```bash
gs_om -t postgis -m rmlib
```
