---
title: pg_bulkload使用手册
summary: pg_bulkload使用手册
author: Guo Huan
date: 2021-11-29
---

# pg_bulkload

## pg_bulkload简介

pg_bulkload是一种用于MogDB的高速数据加载工具，相比copy命令速度更快，能够跳过shared buffer、wal buffer直接写文件。

<br/>

## 安装pg_bulkload

请参见[gs_install_plugin](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin.md)或[gs_install_plugin_local](../../reference-guide/tool-reference/tools-used-in-the-internal-system/gs_install_plugin_local.md)。

<br/>

## pg_bulkload使用

```bash
pg_bulkload --help
gsql -p 5432 postgres -r
```

```sql
CREATE EXTENSION pg_bulkload;
create table test_bulkload(id int, name varchar(128));
```

创建一个txt文件，写10W条数据：

```bash
seq 100000| awk '{print $0"|bulkload"}' > bulkload_output.txt
```

<br/>

### 使用参数

文件创建成功，执行如下命令：

```bash
pg_bulkload -i ./bulkload_output.txt -O test_bulkload -l test_bulkload.log -p 5432 -o "TYPE=csv" -o "DELIMITER=|" -d postgres -U hlv
```

连接数据库，查看数据是否导入成功：

```sql
select count(1) from test_bulkload;
```

<br/>

### 使用控制文件

在使用控制文件进行数据导入之前，需要先清空之前表中导入的数据。

编写.ctl文件

```bash
INPUT=/vdb/MogDB-server/dest/bulkload_output.txt
LOGFILE = /vdb/MogDB-server/dest/test_bulkload.log
LIMIT = INFINITE
PARSE_ERRORS = 0
CHECK_CONSTRAINTS = NO
TYPE = CSV
SKIP = 5  (该参数设置跳过几行)
DELIMITER = |
QUOTE = "\""
ESCAPE = "\""
OUTPUT = test_bulkload
MULTI_PROCESS = NO
WRITER = DIRECT
DUPLICATE_ERRORS = 0
ON_DUPLICATE_KEEP = NEW
TRUNCATE = YES
```

执行命令：

```bash
pg_bulkload ./lottu.ctl -d postgres -U hlv
```
