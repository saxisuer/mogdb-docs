---
title: 常量与宏
summary: 常量与宏
author: Zhang Cuiping
date: 2021-05-17
---

# 常量与宏

MogDB支持的常量和宏请参见[表1](#constants)。

**表 1** 常量和宏 <a id="constants"> </a>

| 参数            | 描述                 | 示例              |
| :------------------ | :-------------- | :----------------------------------------- |
| CURRENT_CATALOG | 当前数据库                             | `MogDB=# SELECT CURRENT_CATALOG; current_database ------------------- postgres (1 row)` |
| CURRENT_ROLE    | 当前用户                               | `MogDB=# SELECT CURRENT_ROLE; current_user ------------------- omm (1 row)` |
| CURRENT_SCHEMA  | 当前数据库模式                         | `MogDB=# SELECT CURRENT_SCHEMA; current_schema ------------------- public (1 row)` |
| CURRENT_USER    | 当前用户                               | `MogDB=# SELECT CURRENT_USER; current_user ------------------- omm (1 row)` |
| LOCALTIMESTAMP  | 当前会话时间（无时区）                 | `MogDB=# SELECT LOCALTIMESTAMP;         timestamp ------------------- 2015-10-10 15:37:30.968538 (1 row)` |
| NULL            | 空值                                   | -                                                            |
| SESSION_USER    | 当前系统用户                           | `MogDB=# SELECT SESSION_USER; session_user ------------------- omm (1 row)` |
| SYSDATE         | 当前系统日期                           | `MogDB=# SELECT SYSDATE; sysdate ------------------- 2015-10-10 15:48:53 (1 row)` |
| USER            | 当前用户，此用户为CURRENT_USER的别名。 | `MogDB=# SELECT USER; current_user ------------------- omm (1 row)`      |
