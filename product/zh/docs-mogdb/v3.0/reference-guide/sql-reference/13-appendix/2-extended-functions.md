---
title: 扩展函数
summary: 扩展函数
author: Zhang Cuiping
date: 2021-05-18
---

# 扩展函数

下表列举了MogDB中支持的扩展函数，不作为商用特性交付，仅供参考。

<table>
    <tr>
        <th>分类</th>
        <th>函数名称</th>
        <th>描述</th>
    </tr>
    <tr>
        <td rowspan=2>访问权限查询函数</td>
        <td>has_sequence_privilege(user, sequence, privilege)</td>
        <td>指定用户是否有访问序列的权限</td>
    </tr>
    <tr>
        <td>has_sequence_privilege(sequence, privilege)</td>
        <td>当前用户是否有访问序列的权限</td>
    </tr>
    <tr>
        <td rowspan=2>触发器函数</td>
        <td>pg_get_triggerdef(oid)</td>
        <td>为触发器获取CREATE [ CONSTRAINT ] TRIGGER命令</td>
    </tr>
    <tr>
        <td>pg_get_triggerdef(oid, boolean)</td>
        <td>为触发器获取CREATE [ CONSTRAINT ] TRIGGER命令</td>
    </tr>
</table>
