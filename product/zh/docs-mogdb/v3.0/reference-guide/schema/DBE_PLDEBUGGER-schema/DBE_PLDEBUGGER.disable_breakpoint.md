---
title: DBE_PLDEBUGGER.disable_breakpoint
summary: DBE_PLDEBUGGER.disable_breakpoint
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEBUGGER.disable_breakpoint

debug端调试过程中，调用disable_breakpoint禁用已被激活的断点。

**表 1** disable_breakpoint入参和返回值列表

| 名称         | 类型       | 描述     |
| :----------- | :--------- | :------- |
| breakpointno | IN integer | 断点编号 |
| result       | OUT bool   | 是否成功 |