---
title: DBE_PLDEBUGGER.delete_breakpoint
summary: DBE_PLDEBUGGER.delete_breakpoint
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.delete_breakpoint

debug端调试过程中，调用delete_breakpoint删除已有的断点。

**表 1** delete_breakpoint入参和返回值列表

| 名称         | 类型       | 描述       |
| :----------- | :--------- | :--------- |
| breakpointno | IN integer | 断点编号。 |
| result       | OUT bool   | 是否成功。 |
