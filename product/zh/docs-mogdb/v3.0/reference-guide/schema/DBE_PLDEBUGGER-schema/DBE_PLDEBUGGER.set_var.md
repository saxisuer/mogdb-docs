---
title: DBE_PLDEBUGGER.set_var
summary: DBE_PLDEBUGGER.set_var
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.set_var

将指定的调试的存储过程中最上层栈上的变量修改为入参的取值。如果存储过程中包含同名的变量，set_var只支持第一个变量值的设置。

**表 1** set_var入参和返回值列表

| 名称     | 类型        | 描述             |
| :------- | :---------- | :--------------- |
| var_name | IN text     | 变量名。         |
| value    | IN text     | 修改值。         |
| result   | OUT boolean | 结果，是否成功。 |
