---
title: DBE_PLDEBUGGER.abort
summary: DBE_PLDEBUGGER.abort
author: Guo Huan
date: 2021-11-15
---

# DBE_PLDEBUGGER.abort

令server端执行的存储过程报错跳出。返回值表示是否成功发送abort。

函数原型为：

```sql
DBE_PLDEBUGGER.abort()
RETURN boolean;
```

**表 1** abort 返回值列表

| 名称  | 类型        | 描述             |
| :---- | :---------- | :--------------- |
| abort | OUT boolean | 表示成功或失败。 |
