---
title: DBE_PLDEVELOPER
summary: DBE_PLDEVELOPER
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER

DBE_PLDEVELOPER下系统表用于记录PLPGSQL包、函数及存储过程编译过程中需要记录的信息。
