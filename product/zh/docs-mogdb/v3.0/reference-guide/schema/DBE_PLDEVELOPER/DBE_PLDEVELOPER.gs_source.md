---
title: DBE_PLDEVELOPER.gs_source
summary: DBE_PLDEVELOPER.gs_source
author: Guo Huan
date: 2022-05-31
---

# DBE_PLDEVELOPER.gs_source

用于记录PLPGSQL对象（存储过程、函数、包、包体）编译相关信息，具体内容见下列字段描述。

打开plsql_show_all_error参数后，会把成功或失败的PLPGSQL对象编译信息记录在此表中，如果关闭plsql_show_all_error参数则只会将正确的编译相关信息插入此表中。

**表 1** DBE_PLDEVELOPER.gs_source字段

| **名称** | **类型** | **描述**                                              |
| -------- | -------- | ----------------------------------------------------- |
| id       | oid      | 对象的ID。                                            |
| owner    | bigint   | 对象创建用户ID。                                      |
| nspid    | oid      | 对象的模式ID。                                        |
| name     | name     | 对象名。                                              |
| type     | text     | 对象类型（procedure/function/package/package body）。 |
| status   | boolean  | 是否创建成功。                                        |
| src      | text     | 对象创建的原始语句。                                  |
