---
title: _PG_FOREIGN_TABLES
summary: _PG_FOREIGN_TABLES
author: Guo Huan
date: 2021-06-23
---

# _PG_FOREIGN_TABLES

存储所有的定义在本数据库的外部表信息。只显示当前用户有权访问的外部表信息。该视图只有sysadmin权限可以查看。

**表 1** _PG_FOREIGN_TABLES字段

| **名称**                 | **类型**                          | **描述**                                         |
| ------------------------ | --------------------------------- | ------------------------------------------------ |
| foreign_table_catalog    | information_schema.sql_identifier | 外部表所在的数据库名称（永远是当前数据库）。     |
| foreign_table_schema     | name                              | 外部表的schema名称。                             |
| foreign_table_name       | name                              | 外部表的名称。                                   |
| ftoptions                | text[]                            | 外部表的可选项。                                 |
| foreign_server_catalog   | information_schema.sql_identifier | 外部服务器所在的数据库名称（永远是当前数据库）。 |
| foreign_server_name      | information_schema.sql_identifier | 外部服务器的名称。                               |
| authorization_identifier | information_schema.sql_identifier | 所有者的角色名称。                               |
