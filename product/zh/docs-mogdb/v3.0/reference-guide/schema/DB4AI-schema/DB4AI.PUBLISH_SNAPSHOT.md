---
title: DB4AI.PUBLISH_SNAPSHOT
summary: DB4AI.PUBLISH_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PUBLISH_SNAPSHOT

PUBLISH_SNAPSHOT是DB4AI特性用于发布快照的接口函数。通过语法PUBLISH SNAPSHOT调用。

**表 1** DB4AI.PUBLISH_SNAPSHOT入参和返回值列表

| 参数     | 类型                    | 描述                         |
| :------- | :---------------------- | :--------------------------- |
| i_schema | IN NAME                 | 快照存储的模式名字，默认值是当前用户或者PUBLIC |
| i_name   | IN NAME                 | 快照名称                                       |
| res      | OUT db4ai.snapshot_name | 结果                                           |
