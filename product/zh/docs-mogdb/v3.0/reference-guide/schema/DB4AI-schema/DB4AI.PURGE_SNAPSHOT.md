---
title: DB4AI.PURGE_SNAPSHOT
summary: DB4AI.PURGE_SNAPSHOT
author: Guo Huan
date: 2021-11-15
---

# DB4AI.PURGE_SNAPSHOT

PURGE_SNAPSHOT是DB4AI特性用于删除快照的接口函数。通过语法PURGE SNAPSHOT调用。

**表 1** DB4AI.PURGE_SNAPSHOT入参和返回值列表

| 参数     | 类型                    | 描述               |
| :------- | :---------------------- | :----------------- |
| i_schema | IN NAME                 | 快照存储的模式名字 |
| i_name   | IN NAME                 | 快照名称           |
| res      | OUT db4ai.snapshot_name | 结果               |
