---
title: WAIT_EVENTS
summary: WAIT_EVENTS
author: Guo Huan
date: 2021-04-19
---

# WAIT_EVENTS

WAIT_EVENTS显示当前节点的event的等待相关的统计信息。具体事件信息请参见[PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md)表2 等待状态列表、表3 轻量级锁等待事件列表、表4 IO等待事件列表和表5 事务锁等待事件列表。关于每种事务锁对业务的影响程度，请参考[LOCK](../../../../reference-guide/sql-syntax/LOCK.md)语法小节的详细描述。

**表 1** WAIT_EVENTS字段

| **名称**        | **类型**                 | **描述**                     |
| :-------------- | :----------------------- | :--------------------------- |
| nodename        | text                     | 数据库进程名称。             |
| type            | text                     | event类型。                  |
| event           | text                     | event名称。                  |
| wait            | bigint                   | 等待次数。                   |
| failed_wait     | bigint                   | 失败的等待次数。             |
| total_wait_time | bigint                   | 总等待时间（单位: 微秒）。   |
| avg_wait_time   | bigint                   | 平均等待时间（单位: 微秒）。 |
| max_wait_time   | bigint                   | 最大等待时间（单位: 微秒）。 |
| min_wait_time   | bigint                   | 最小等待时间（单位: 微秒）。 |
| last_updated    | timestamp with time zone | 最后一次更新该事件的时间。   |
