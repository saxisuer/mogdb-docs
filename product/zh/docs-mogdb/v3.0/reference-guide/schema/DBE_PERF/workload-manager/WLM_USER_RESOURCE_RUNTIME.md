---
title: WLM_USER_RESOURCE_RUNTIME
summary: WLM_USER_RESOURCE_RUNTIME
author: Guo Huan
date: 2021-04-19
---

# WLM_USER_RESOURCE_RUNTIME

WLM_USER_RESOURCE_RUNTIME视图显示所有用户资源使用情况，需要使用管理员用户进行查询。此视图在GUC参数“use_workload_manager”为“on”时才有效。

**表 1** WLM_USER_RESOURCE_RUNTIME字段

| 名称              | 类型    | 描述                              |
| :---------------- | :------ | :-------------------------------- |
| username          | name    | 用户名。                                                     |
| used_memory       | integer | 正在使用的内存大小，单位MB。                                 |
| total_memory      | integer | 可以使用的内存大小，单位MB。值为0表示未限制最大可用内存，其限制取决于数据库最大可用内存。 |
| used_cpu          | integer | 正在使用的CPU核数。                                          |
| total_cpu         | integer | 在该机器节点上，用户关联控制组的CPU核数总和。                |
| used_space        | bigint  | 已使用的存储空间大小，单位KB。                               |
| total_space       | bigint  | 可使用的存储空间大小，单位KB，值为-1表示未限制最大存储空间。 |
| used_temp_space   | bigint  | 已使用的临时空间大小（预留字段，暂未使用），单位KB。         |
| total_temp_space  | bigint  | 可使用的临时空间大小（预留字段，暂未使用），单位KB，值为-1表示未限制最大临时存储空间。 |
| used_spill_space  | bigint  | 已使用的下盘空间大小（预留字段，暂未使用），单位KB。         |
| total_spill_space | bigint  | 可使用的下盘空间大小（预留字段，暂未使用），单位KB，值为-1表示未限制最大下盘空间。 |
