---
title: GLOBAL_USER_TRANSACTION
summary: GLOBAL_USER_TRANSACTION
author: Guo Huan
date: 2021-11-15
---

# GLOBAL_USER_TRANSACTION

GLOBAL_USER_TRANSACTION用来统计全局用户执行的事务信息。

**表 1** GLOBAL_USER_TRANSACTION字段

| **名称**            | **类型** | **描述**                             |
| :------------------ | :------- | :----------------------------------- |
| node_name           | name     | 节点名称。                           |
| username            | name     | 用户的名称。                         |
| commit_counter      | bigint   | 用户事务commit数量。                 |
| rollback_counter    | bigint   | 用户事务rollback数量。               |
| resp_min            | bigint   | 用户事务最小响应时间（单位：微秒）。 |
| resp_max            | bigint   | 用户事务最大响应时间（单位：微秒）。 |
| resp_avg            | bigint   | 用户事务平均响应时间（单位：微秒）。 |
| resp_total          | bigint   | 用户事务总响应时间（单位：微秒）。   |
| bg_commit_counter   | bigint   | 后台事务commit数量。                 |
| bg_rollback_counter | bigint   | 后台事务rollback数量。               |
| bg_resp_min         | bigint   | 后台事务最小响应时间（单位：微秒）。 |
| bg_resp_max         | bigint   | 后台事务最大响应时间（单位：微秒）。 |
| bg_resp_avg         | bigint   | 后台事务平均响应时间（单位：微秒）。 |
| bg_resp_total       | bigint   | 后台事务总响应时间（单位：微秒）。   |
