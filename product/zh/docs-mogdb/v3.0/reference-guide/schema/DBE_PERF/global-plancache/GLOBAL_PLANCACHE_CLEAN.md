---
title: GLOBAL_PLANCACHE_CLEAN
summary: GLOBAL_PLANCACHE_CLEAN
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_PLANCACHE_CLEAN

GLOBAL_PLANCACHE_CLEAN视图用于清理所有节点上无人使用的全局计划缓存。返回值为Boolean类型。
