---
title: STATIO_ALL_SEQUENCES
summary: STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_ALL_SEQUENCES

STATIO_ALL_SEQUENCES视图包含数据库中每个序列的每一行，显示特定序列关于I/O的统计。

**表 1** STATIO_ALL_SEQUENCES字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| relid      | oid      | 序列OID。                |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | bigint   | 从序列中读取的磁盘块数。 |
| blks_hit   | bigint   | 序列中缓存命中数。       |
