---
title: STATIO_USER_SEQUENCES
summary: STATIO_USER_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_USER_SEQUENCES

STATIO_USER_SEQUENCE视图显示当前节点的命名空间中所有用户关系表类型为序列的IO状态信息。

**表 1** STATIO_USER_SEQUENCE字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| relid      | oid      | 序列OID。                |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | bigint   | 从序列中读取的磁盘块数。 |
| blks_hit   | bigint   | 序列中缓存命中数。       |
