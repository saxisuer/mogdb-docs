---
title: SUMMARY_STATIO_ALL_SEQUENCES
summary: SUMMARY_STATIO_ALL_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_ALL_SEQUENCES

SUMMARY_STATIO_ALL_SEQUENCES视图包含MogDB内汇聚的数据库中每个序列的每一行,显示特定序列关于I/O的统计。

**表 1** SUMMARY_STATIO_ALL_SEQUENCES字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | numeric  | 从序列中读取的磁盘块数。 |
| blks_hit   | numeric  | 序列中缓存命中数。       |
