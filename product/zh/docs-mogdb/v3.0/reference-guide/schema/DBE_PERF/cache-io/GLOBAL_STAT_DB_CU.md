---
title: GLOBAL_STAT_DB_CU
summary: GLOBAL_STAT_DB_CU
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_DB_CU

GLOBAL_STAT_DB_CU视图用于查询MogDB，每个数据库的CU命中情况。可以通过pg_stat_reset()进行清零。

**表 1** GLOBAL_STAT_DB_CU字段

| **名称**      | **类型** | **描述**         |
| :------------ | :------- | :--------------- |
| node_name1    | text     | 数据库进程名称。 |
| db_name       | text     | 数据库名。       |
| mem_hit       | bigint   | 内存命中次数。   |
| hdd_sync_read | bigint   | 硬盘同步读次数。 |
| hdd_asyn_read | bigint   | 硬盘异步读次数。 |
