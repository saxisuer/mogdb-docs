---
title: SESSION_TIME
summary: SESSION_TIME
author: Guo Huan
date: 2021-04-19
---

# SESSION_TIME

用于统计当前节点会话线程的运行时间信息，及各执行阶段所消耗时间。

**表 1** SESSION_TIME字段

| **名称**  | **类型** | **描述**                |
| :-------- | :------- | :---------------------- |
| sessid    | text     | 线程启动时间+线程标识。 |
| stat_id   | integer  | 统计编号。              |
| stat_name | text     | 会话类型名称。          |
| value     | bigint   | 会话值。                |
