---
title: LOCAL_THREADPOOL_STATUS
summary: LOCAL_THREADPOOL_STATUS
author: Guo Huan
date: 2021-04-19
---

# LOCAL_THREADPOOL_STATUS

LOCAL_THREADPOOL_STATUS视图显示线程池下工作线程及会话的状态信息。该视图仅在线程池开启（enable_thread_pool = on）时生效。

**表 1** LOCAL_THREADPOOL_STATUS字段<a id="LOCAL_THREADPOOL_STATUS字段"> </a>

| 名称            | 类型    | 描述                                                         |
| :-------------- | :------ | :----------------------------------------------------------- |
| node_name       | text    | 数据库进程名称。                                             |
| group_id        | integer | 线程池组ID。                                                 |
| bind_numa_id    | integer | 该线程池组绑定的NUMA ID。                                    |
| bind_cpu_number | integer | 该线程池组绑定的CPU信息。如果未绑定CPU，该值为NULL。         |
| listener        | integer | 该线程池组的Listener线程数量。                               |
| worker_info     | text    | 线程池中worker线程相关信息，包括以下信息: <br/>- default: 该线程池组中的初始线程数量。<br/>- new: 该线程池组中新增线程的数量。<br/>- expect: 该线程池组中预期线程的数量。<br/>- actual: 该线程池组中实际线程的数量。<br/>- idle: 该线程池组中空闲线程的数量。<br/>- pending: 该线程池组中等待线程的数量。 |
| session_info    | text    | 线程池中会话相关信息，包括以下信息: <br/>- total: 该线程池组中所有的会话数量。<br/>- waiting: 该线程池组中等待调度的会话数量。<br/>- running: 该线程池中正在执行的会话数量。<br/>- idle: 该线程池组中空闲的会话数量。 |
| stream_info     | text    | 线程池中stream线程相关信息，包括以下信息：<br/>- total: 该线程池组中所有的stream线程数量。<br/>- running: 该线程池中正在执行的stream线程数量。<br/>- idle: 该线程池组中空闲的stream线程数量 |
| committer_info  | text    | 线程池中committer线程相关信息，包括以下信息：<br/>- total: 该线程池组中所有的committer线程数量。<br/>- running: 该线程池中正在执行的committer线程数量。<br/>- idle: 该线程池组中空闲的committer线程数量 |
