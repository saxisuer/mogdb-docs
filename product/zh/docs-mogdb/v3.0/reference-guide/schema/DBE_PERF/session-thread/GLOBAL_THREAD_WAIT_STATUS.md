---
title: GLOBAL_THREAD_WAIT_STATUS
summary: GLOBAL_THREAD_WAIT_STATUS
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_THREAD_WAIT_STATUS

通过该视图可以检测所有节点上工作线程（backend thread）以及辅助线程（auxiliary thread）的阻塞等待情况。具体事件信息请参见[PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md)表2 等待状态列表、表3 轻量级锁等待事件列表、表4 IO等待事件列表和表5 事务锁等待事件列表。

通过GLOBAL_THREAD_WAIT_STATUS视图，可以查看MogDB全局各个节点上所有SQL语句产生的线程之间的调用层次关系，以及各个线程的阻塞等待状态，从而更容易定位hang以及类似现象的原因。

GLOBAL_THREAD_WAIT_STATUS视图和THREAD_WAIT_STATUS视图列定义完全相同，这是由于GLOBAL_THREAD_WAIT_STATUS视图本质是到MogDB中各个节点上查询THREAD_WAIT_STATUS视图汇总的结果。

**表 1** GLOBAL_THREAD_WAIT_STATUS字段

| **名称**    | **类型** | **描述**                                                     |
| :---------- | :------- | :----------------------------------------------------------- |
| node_name   | text     | 数据库进程名称。                                             |
| db_name     | text     | 数据库名称。                                                 |
| thread_name | text     | 线程名称。                                                   |
| query_id    | bigint   | 查询ID，对应debug_query_id。                                 |
| tid         | bigint   | 当前线程的线程号。                                           |
| sessionid   | bigint   | session的ID。                                                |
| lwtid       | integer  | 当前线程的轻量级线程号。                                     |
| psessionid  | bigint   | streaming线程的父线程。                                      |
| tlevel      | integer  | streaming线程的层级。                                        |
| smpid       | integer  | 并行线程的ID。                                               |
| wait_status | text     | 当前线程的等待状态。等待状态的详细信息请参见[PG_THREAD_WAIT_STATUS](../../../../reference-guide/system-catalogs-and-system-views/system-views/PG_THREAD_WAIT_STATUS.md)表2 等待状态列表。 |
| wait_event  | text     | 如果wait_status是acquire lock、acquire lwlock、wait io三种类型，此列描述具体的锁、轻量级锁、IO的信息。否则是空。 |
