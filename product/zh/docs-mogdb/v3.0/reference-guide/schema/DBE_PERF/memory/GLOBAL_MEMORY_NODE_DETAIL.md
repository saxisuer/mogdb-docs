---
title: GLOBAL_MEMORY_NODE_DETAIL
summary: GLOBAL_MEMORY_NODE_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_MEMORY_NODE_DETAIL

显示当前MogDB中所有正常节点下的内存使用情况。

**表 1** GLOBAL_MEMORY_NODE_DETAIL字段

| **名称**     | **类型** | **描述**                      |
| :----------- | :------- | :---------------------------|
| nodename     | text     | 数据库进程名称。                        |
| memorytype   | text     | 内存使用的名称。<br/>- max_process_memory: MogDB实例所占用的内存大小。<br/>- process_used_memory: 进程所使用的内存大小。<br/>- max_dynamic_memory: 最大动态内存。<br/>- dynamic_used_memory: 已使用的动态内存。<br/>- dynamic_peak_memory: 内存的动态峰值。<br/>- dynamic_used_shrctx: 最大动态共享内存上下文。<br/>- dynamic_peak_shrctx: 共享内存上下文的动态峰值。<br/>- max_shared_memory: 最大共享内存。<br/>- shared_used_memory: 已使用的共享内存。<br/>- max_cstore_memory: 列存所允许使用的最大内存。<br/>- cstore_used_memory: 列存已使用的内存大小。<br/>- max_sctpcomm_memory: sctp通信所允许使用的最大内存。<br/>- sctpcomm_used_memory: sctp通信已使用的内存大小。<br/>- sctpcomm_peak_memory: sctp通信的内存峰值。<br/>- other_used_memory: 其他已使用的内存大小。<br/>- gpu_max_dynamic_memory: GPU最大动态内存。<br/>- gpu_dynamic_used_memory: GPU已使用的动态内存。<br/>- gpu_dynamic_peak_memory: GPU内存的动态峰值。<br/>- pooler_conn_memory: 链接池申请内存计数。<br/>- pooler_freeconn_memory: 链接池空闲连接的内存计数。<br/>- storage_compress_memory: 存储模块压缩使用的内存大小。<br/>- udf_reserved_memory: UDF预留的内存大小。 |
| memorymbytes | integer  | 内存使用的大小，单位为MB。            |
