---
title: STATEMENT_COMPLEX_HISTORY_TABLE
summary: STATEMENT_COMPLEX_HISTORY_TABLE
author: Guo Huan
date: 2021-04-19
---

# STATEMENT_COMPLEX_HISTORY_TABLE

STATEMENT_COMPLEX_HISTORY_TABLE系统表显示数据库主节点执行作业结束后的负载管理记录。此数据是从内核中转储到系统表中的数据。具体的字段请参考[GS_SESSION_MEMORY_DETAIL](../../../../reference-guide/system-catalogs-and-system-views/system-views/GS_SESSION_MEMORY_DETAIL.md)表1。
