---
title: 通过INSERT语句直接写入数据
summary: 通过INSERT语句直接写入数据
author: Guo Huan
date: 2021-03-04
---

# 通过INSERT语句直接写入数据

用户可以通过以下方式执行INSERT语句直接向MogDB数据库写入数据:

- 使用MogDB数据库提供的客户端工具向MogDB数据库写入数据。

    请参见向表中插入数据。

- 通过JDBC/ODBC驱动连接数据库执行INSERT语句向MogDB数据库写入数据。

    详细内容请参见连接数据库。

MogDB数据库支持完整的数据库事务级别的增删改操作。INSERT是最简单的一种数据写入方式，这种方式适合数据写入量不大，并发度不高的场景。
