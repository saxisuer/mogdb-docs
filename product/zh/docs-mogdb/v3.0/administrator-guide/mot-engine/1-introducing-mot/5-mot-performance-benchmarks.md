---
title: MOT性能基准
summary: MOT性能基准
author: Zhang Cuiping
date: 2021-03-04
---

# MOT性能基准

我们的性能测试是基于业界和学术界通用的TPC-C基准。

测试使用了BenchmarkSQL（请参见[MOT样例TPC-C基准](../../../administrator-guide/mot-engine/2-using-mot/6-mot-sample-tpcc-benchmark.md)），并且使用交互式SQL命令而不是存储过程来生成工作负载。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  使用存储过程方法可能会产生更高的性能结果，因为它需要大大减少网络往返和数据库封装SQL处理周期。

评估MogDB MOT性能和磁盘性能的所有测试都使用了同步日志记录和在MOT中优化的group-commit=on版本。

最后我们进行了额外测试，评估MOT快速采集大量数据的能力，并将其作为中间层数据采集解决方案的替代方案。

2020年6月完成全部测试。

下面是各种类型的MOT性能基准。

<br/>

## MOT硬件

本次测试使用的服务器满足10GbE组网和以下配置：

- 基于Arm64/鲲鹏920的2路服务器，型号为TaiShan 2280 v2（128核），800GB RAM，1TB NVMe盘。服务器详细规格参见[此页面](https://e.huawei.com/cn/products/servers/taishan-server/taishan-2480-v2)。操作系统为openEuler。
- 基于Arm64/鲲鹏960的4路服务器，型号为TaiShan 2480 v2（256核），512GB RAM，1TB NVMe盘。服务器详细规格参见[此页面](https://e.huawei.com/cn/products/servers/taishan-server/taishan-2480-v2)。操作系统为openEuler。
- 戴尔x86服务器，2路英特尔至强金牌6154 CPU @ 3Ghz，18核（超线程开启时共72核），1TB RAM，1TB SSD。操作系统为CentOS 7.6。
- x86超微服务器，8路英特尔（R）至强（R） CPU E7-8890 v4 @ 2.20GHz，24核（超线程开启共384核），1TB RAM，1.2 TB SSD（希捷1200 SSD 200GB，SAS 12Gb/s）。操作系统为Ubuntu 16.04.2 LTS。
- 华为x86服务器，4路英特尔（R）至强（R） CPU E7-8890 v4 @ 2.2Ghz（超线程开启共96核），512GB RAM，SSD 2TB。操作系统为CentOS 7.6。

<br/>

## MOT测试总结

MOT比磁盘表性能提升2.5至4.1倍，在Arm/鲲鹏256核服务器上达到480万tpmC。测试结果清楚表明MOT在扩展和利用所有硬件资源方面的卓越能力。随着CPU槽位和服务器核数增加，性能会随之跃升。

MOT在Arm/鲲鹏架构下最高可达3万tpmC/核，在x86架构下最高可达4万tpmC/核。

由于持久性机制更高效，MOT中的复制开销在Arm/鲲鹏主备高可用场景下为7%，在x86服务器中为2%。而磁盘表的开销在Arm/鲲鹏中为20%，在x86中为15%。

最终，MOT延迟降低2.5倍，TPC-C事务响应速度提升2至7倍。

<br/>

## MOT高吞吐量

MOT高吞吐量测试结果如下。

<br/>

### Arm/鲲鹏2路128核

- **性能**

  下图是华为Arm/鲲鹏2路128核服务器TPC-C基准测试的结果。

  一共进行了四类测试：

  - MOT和MogDB基于磁盘的表各进行了2次测试。
  - 其中两项测试是在单节点（无高可用性）上执行，这意味着没有向备节点执行复制。其余两个测试在主备节点（有高可用性）上执行，即写入主节点的数据被复制到备节点。

  MOT用橙色表示，基于磁盘的表用蓝色表示。

  **图 1** Arm/鲲鹏2路128核性能基准

  ![Arm-鲲鹏2路128核性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-1.png)

  结果表明：

  - 正如预期的那样，在所有情况下，MOT的性能明显高于基于磁盘的表。
  - 单节点：MOT性能为380万tpmC，而基于磁盘的表为150万tpmC。
  - 主备节点：MOT性能为350万tpmC，而基于磁盘的表为120万tpmC。
  - 相比单节点（无高可用性，无复制），在有复制需求的生产级（高可用性）服务器（主备节点）上，使用MOT的好处更显著。
  - 同在主备高可用场景下，MOT复制开销：Arm/鲲鹏为7%，x86为2%；而基于磁盘的表复制开销：Arm/鲲鹏为20%；x86为15%。

- **单CPU核性能**

  下图是华为Arm/鲲鹏服务器2路128核的单核TPC-C基准性能/吞吐量测试结果。同样地，一共进行了四类测试：

  **图 2** Arm/鲲鹏2路128核的单核性能标杆

  ![Arm-鲲鹏2路128核的单核性能标杆](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-2.png)

  结果表明，正如预期的那样，在所有情况下，MOT的单核性能明显高于基于磁盘的表。相比单节点（无高可用性，无复制），在有复制需求的生产级（高可用性）服务器（主备节点）上，使用MOT的好处更显著。

<br/>

### Arm/鲲鹏4路256核

下面通过单连接数的tpmC来展示MOT出色的并发控制性能。

**图 3** Arm/鲲鹏4路256核性能基准

![Arm-鲲鹏4路256核性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-3.png)

结果表明，随着核数增多，性能也显著提高，在768核时性能达到480万tpmC的峰值。

<br/>

### x86服务器

- **8路384核**

下面通过比较基于磁盘的表和MOT之间单连接数的tpmC，来展示MOT出色的并发控制性能。本次测试以8路384核x86服务器为例。橙色表示MOT的结果。

**图 4** 8路384核x86服务器性能基准

![8路384核x86服务器性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-4.png)

结果表明，在386核服务器上，MOT的性能明显优于基于磁盘的表，并且单核性能非常高，达到300万tpmC/核。

- **4路96核**

在4路96核服务器上，MOT实现了390万tpmC。下图展示了高效MOT的单核性能达到4万tpmC/核。

**图 5** 4路96核服务器性能基准

![4路96核服务器性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-5.png)

<br/>

## MOT低延迟

以下是在Arm/鲲鹏两路服务器（128核）上进行测试的结果。单位为毫秒（ms）。

**图 1** 低延迟（90th%）性能基准

![低延迟（90th-）性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-6.png)

MOT的平均事务速度为2.5倍，MOT延迟为10.5ms，而基于磁盘的表延迟为23至25ms。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  计算平均数时，已考虑TPC-C的5个事务分布占比。有关更多信息，请参阅[MOT样例TPC-C基准](../../../administrator-guide/mot-engine/2-using-mot/6-mot-sample-tpcc-benchmark.md)中关于TPC-C事务的说明。

**图 2** 低延迟（90th%，事务平均）性能基准

![低延迟（90th-事务平均）性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-7.png)

<br/>

## MOT恢复时间目标（RTO）和冷启动时间

### 高可用RTO

MOT完全集成到MogDB中，包括支持主备部署的高可用场景。WAL重做日志的复制机制将把复制更改到数据库备节点并使用备节点进行重放。

如果故障转移事件发生，无论是由于计划外的主节点故障还是由于计划内的维护事件，备节点都会迅速活跃。恢复和重放WAL重做日志以及启用连接所需的时间也称为恢复时间目标（RTO）。

**MogDB（包括MOT）的RTO小于10秒。**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  灾难发生后必须恢复业务流程，避免导致连续性中断相关的不可接受的后果，而RTO表示的就是这段流程的持续时间和业务级别。换句话说，RTO就是在回答这个问题：在通知业务流程中断后，需要多长时间才能恢复？

另外，从[MOT高吞吐量](#mot高吞吐量)的MOT中可以看出，在Arm/鲲鹏架构下，主从高可用场景复制开销仅为7%，在x86架构下仅为2%，而基于磁盘的表复制开销为2%。Arm/鲲鹏机型为20%，x86机型为15%。

<br/>

### 冷启动恢复时间

冷启动恢复时间是指系统从停止模式到能够完全运行所需的时间。在内存数据库中，这包括将所有数据和索引加载到内存中的时间，因此它取决于数据大小、硬件带宽和软件算法能否高效地处理这些数据。

MOT测试使用40 GB/s的ARM磁盘测试，可以在100 GB/s的时间内加载数据库。MOT的索引非持久化，因此它们是在冷启动时创建的。实际加载的数据加索引大小约多50%。因此，可以转换为MOT冷启动时间的数据和索引容量为40秒内150GB，或225 GB/分钟（3.75 GB/秒）。

冷启动过程和从磁盘加载数据到MOT所需时间如下图所示。

**图 1** 冷启动时间性能基准

![冷启动时间性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-8.png)

- 数据库大小：加载整个数据库（每数据库GB）的总时间由蓝色线条和左侧的Y轴“时间（秒）”表示。
- 吞吐量：数据库每秒GB吞吐量由橙色线和右侧的Y轴“吞吐量GB/秒”表示。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  测试过程中表现的性能与SSD硬件的带宽非常接近。因此，可以在不同的平台上实现更高（或更低）的性能。

<br/>

## MOT资源利用率

在4路96核512GB RAM的x86服务器上测试的资源利用率如下所示。MOT能够高效持续消耗几乎所有可用的CPU资源。例如，192核390万tpmC的CPU利用率几乎达到100%。

- tmpC：每分钟完成的TPC-C事务数以橙色条柱和左侧的Y轴“tpmC”表示。
- CPU利用率（%）：CPU利用率由蓝色线条和右侧的Y轴“CPU%”表示。

**图 1** 资源利用率性能基准

![资源利用率性能基准](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/mot-performance-benchmarks-9.png)

<br/>

## MOT数据采集速度

该测试模拟海量物联网、云端或移动端接入的实时数据流，快速持续地把海量数据注入到数据库。

- 本次测试涉及大量数据采集，具体如下：
  - 1000万行数据由500个线程发送，2000轮，每个insert命令有10条记录（行），每条记录占200字节。
  - 客户端和数据库位于不同的机器上。 数据库服务器为2路72核x86服务器。
- 性能结果
  - 吞吐量：10000个记录/核，或2MB/核。
  - 延迟：2.8ms每10条记录批量插入（包括客户端-服务器组网）。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-caution.gif) **注意：** 预计MOT将针对这一场景进行多项额外的甚至重大的性能改进。更多关于大规模数据流和数据采集的信息，请参阅[MOT应用场景](4-mot-usage-scenarios.md)。
