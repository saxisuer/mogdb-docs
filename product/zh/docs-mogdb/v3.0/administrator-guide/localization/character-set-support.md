---
title: 字符集支持
summary: 字符集支持
author: Guo Huan
date: 2022-07-26
---

# 字符集支持

MogDB支持以各种字符集（也称为编码）存储文本，包含单字节字符集，比如ISO-8859系列，和多字节字符集，比如EUC（扩展Unix编码）、UTF-8、Mule国际编码。所有支持的字符集都可以被客户端透明地使用，但有一些不支持在服务器内使用（即作为服务器端的编码）。缺省字符集是在使用`gs_initdb`初始化数据库集群时选择的。创建数据库时可以重写此缺省值。因此，您可以创建多个具有不同字符集的数据库。

但是，存在一个重要的限制，每个数据库的字符集必须与该数据库的`LC_CTYPE`（字符类别）以及 `LC_COLLATE`（字符串排序顺序）区域设置相兼容。对于`C`或者 `POSIX`区域，允许使用任意字符集， 但对于其他区域则只有设置某一个字符集时能正常运行。（不过在Windows上，UTF-8编码可用于任何区域。）

## 支持的字符集

表1显示了可以在MogDB中使用的字符集。

**表1 MogDB字符集**

| 名称             | 描述                        | 语言                       | 服务器端是否支持 | 字节数/字符 | 别名                                          |
| ---------------- | --------------------------- | -------------------------- | ---------------- | ----------- | --------------------------------------------- |
| `BIG5`           | 大五码                      | 繁体中文                   | 否               | 1-2         | `WIN950`, `Windows950`                        |
| `EUC_CN`         | 扩展UNIX代码-CN             | 简体中文                   | 是               | 1-3         |                                               |
| `EUC_JP`         | 扩展UNIX代码-JP             | 日文                       | 是               | 1-3         |                                               |
| `EUC_JIS_2004`   | 扩展UNIX代码-JP, JIS X 0213 | 日文                       | 是               | 1-3         |                                               |
| `EUC_KR`         | 扩展UNIX代码-KR             | 韩文                       | 是               | 1-3         |                                               |
| `EUC_TW`         | 扩展UNIX代码-TW             | 繁体中文，台湾             | 是               | 1-3         |                                               |
| `GB18030`        | 国标码                      | 中文                       | 是               | 1-2         |                                               |
| `GBK`            | 扩展国标码                  | 简体中文                   | 是               | 1-2         | `WIN936`, `Windows936`                        |
| `ISO_8859_5`     | ISO 8859-5, ECMA 113        | 拉丁/西里尔语              | 是               | 1           |                                               |
| `ISO_8859_6`     | ISO 8859-6, ECMA 114        | 拉丁/阿拉伯语              | 是               | 1           |                                               |
| `ISO_8859_7`     | ISO 8859-7, ECMA 118        | 拉丁/希腊语                | 是               | 1           |                                               |
| `ISO_8859_8`     | ISO 8859-8, ECMA 121        | 拉丁/希伯莱语              | 是               | 1           |                                               |
| `JOHAB`          | JOHAB                       | 韩语                       | 否               | 1-3         |                                               |
| `KOI8R`          | KOI8-R                      | 西里尔语(俄国)             | 是               | 1           | `KOI8`                                        |
| `KOI8U`          | KOI8-U                      | 西里尔语(乌克兰)           | 是               | 1           |                                               |
| `LATIN1`         | ISO 8859-1, ECMA 94         | 西欧语                     | 是               | 1           | `ISO88591`                                    |
| `LATIN2`         | ISO 8859-2, ECMA 94         | 中欧语                     | 是               | 1           | `ISO88592`                                    |
| `LATIN3`         | ISO 8859-3, ECMA 94         | 南欧语                     | 是               | 1           | `ISO88593`                                    |
| `LATIN4`         | ISO 8859-4, ECMA 94         | 北欧语                     | 是               | 1           | `ISO88594`                                    |
| `LATIN5`         | ISO 8859-9, ECMA 128        | 土耳其语                   | 是               | 1           | `ISO88599`                                    |
| `LATIN6`         | ISO 8859-10, ECMA 144       | 日耳曼语                   | 是               | 1           | `ISO885910`                                   |
| `LATIN7`         | ISO 8859-13                 | 波罗的海语                 | 是               | 1           | `ISO885913`                                   |
| `LATIN8`         | ISO 8859-14                 | 凯尔特语                   | 是               | 1           | `ISO885914`                                   |
| `LATIN9`         | ISO 8859-15                 | 带有欧洲语系和语调的LATIN1 | 是               | 1           | `ISO885915`                                   |
| `LATIN10`        | ISO 8859-16, ASRO SR 14111  | 罗马尼亚语                 | 是               | 1           | `ISO885916`                                   |
| `MULE_INTERNAL`  | Mule internal code          | 多语种Emacs                | 是               | 1-4         |                                               |
| `SJIS`           | Shift JIS                   | 日语                       | 否               | 1-2         | `Mskanji`, `ShiftJIS`, `WIN932`, `Windows932` |
| `SHIFT_JIS_2004` | Shift JIS, JIS X 0213       | 日语                       | 否               | 1-2         |                                               |
| `SQL_ASCII`      | unspecified (see text)      | **任意**                   | 是               | 1           |                                               |
| `UHC`            | Unified Hangul Code         | 韩语                       | 否               | 1-2         | `WIN949`, `Windows949`                        |
| `UTF8`           | Unicode, 8-bit              | **全部**                   | 是               | 1-4         | `Unicode`                                     |
| `WIN866`         | Windows CP866               | 西里尔语                   | 是               | 1           | `ALT`                                         |
| `WIN874`         | Windows CP874               | 泰国语                     | 是               | 1           |                                               |
| `WIN1250`        | Windows CP1250              | 中欧语                     | 是               | 1           |                                               |
| `WIN1251`        | Windows CP1251              | 西里尔语                   | 是               | 1           | `WIN`                                         |
| `WIN1252`        | Windows CP1252              | 西欧语                     | 是               | 1           |                                               |
| `WIN1253`        | Windows CP1253              | 希腊语                     | 是               | 1           |                                               |
| `WIN1254`        | Windows CP1254              | 土耳其语                   | 是               | 1           |                                               |
| `WIN1255`        | Windows CP1255              | 希伯来语                   | 是               | 1           |                                               |
| `WIN1256`        | Windows CP1256              | 阿拉伯语                   | 是               | 1           |                                               |
| `WIN1257`        | Windows CP1257              | 波罗的语                   | 是               | 1           |                                               |
| `WIN1258`        | Windows CP1258              | 越南语                     | 是               | 1           | `ABC`, `TCVN`, `TCVN5712`, `VSCII`            |

并非所有API都支持上面列出的编码。`SQL_ASCII`设置与其它设置不同。如果服务器字符集是`SQL_ASCII`，服务器根据ASCII标准解析0-127的字节值，而字节值为128-255的则当作未解析的字符。如果设置为`SQL_ASCII`就不会有编码转换。因此，这个设置基本不用来声明所使用的编码，因为此声明会忽略编码。在大多数情况下，如果使用了任何非ASCII数据，那么我们不建议使用`SQL_ASCII`，因为MogDB无法转换或者校验非ASCII字符。

## 设置字符集

`gs_initdb`可以为一个MogDB集群定义缺省字符集（编码），详细说明请参见[gs_initdb](../../reference-guide/tool-reference/tools-used-in-the-internal-system/5-gs_initdb.md)。

您也可以通过SQL语句在数据库创建时指定非缺省编码，但是指定的编码必须与所选的区域相兼容：

```sql
CREATE DATABASE chinese WITH ENCODING 'UTF8' LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8' TEMPLATE=template0;
```

注意上述命令声明拷贝`template0`数据库。当拷贝任何其他数据库时，来自源数据库的编码和区域设置不能被改变，因为可能导致数据损坏。参阅[CREATE DATABASE](../../reference-guide/sql-syntax/CREATE-DATABASE.md)获取更多信息。

数据库的编码存储在`pg_database`系统表中。您可以使用`gsql`的`-l`选项或`\l` 命令列出这些编码。

```bash
$ gsql -l
                              List of databases
   Name    | Owner | Encoding |   Collate   |    Ctype    | Access privileges
-----------+-------+----------+-------------+-------------+-------------------
 chinese   | omm   | UTF8     | en_US.UTF8  | en_US.UTF8  |
 mogdb     | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 mogila    | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 postgres  | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm           +
           |       |          |             |             | omm=CTc/omm
 template1 | omm   | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/omm           +
           |       |          |             |             | omm=CTc/omm
(6 rows)
```

> **注意**：在大多数现代操作系统中，MogDB可以通过`LC_CTYPE`的设置决定使用哪种字符集，并且强制只使用匹配的数据库编码。在旧的操作系统上您需要确保使用所选区域所期望的编码。如果设置不正确很可能导致与区域相关的操作表现异常，比如排序。
>
> 当`LC_CTYPE`不是`C`或者`POSIX`时，MogDB也允许超级用户创建使用`SQL_ASCII`编码的数据库。如上所述，`SQL_ASCII`不强制要求存储在数据库中的数据具有任何特定的编码，因此会存在出现区域相关错误行为的风险。我们不建议使用这样的设置组合，未来版本也许会完全禁止。

## 服务器和客户端之间的自动字符集转换

MogDB支持在服务器和客户端之间的自动字符集转换。转换信息存储在系统表`pg_conversion`中。MogDB自带一些预定义的转换，如表2所示。

**表2 客户端/服务器字符集转换**

| 服务器端字符集  | 可用的客户端字符集                                           |
| --------------- | ------------------------------------------------------------ |
| `BIG5`          | **不支持做服务器端编码**                                     |
| `EUC_CN`        | **EUC_CN**, `MULE_INTERNAL`, `UTF8`                          |
| `EUC_JP`        | **EUC_JP**, `MULE_INTERNAL`, `SJIS`, `UTF8`                  |
| `EUC_KR`        | **EUC_KR**, `MULE_INTERNAL`, `UTF8`                          |
| `EUC_TW`        | **EUC_TW**, `BIG5`, `MULE_INTERNAL`, `UTF8`                  |
| `GB18030`       | **不支持做服务器端编码**                                     |
| `GBK`           | **不支持做服务器端编码**                                     |
| `ISO_8859_5`    | **ISO_8859_5**, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN866`, `WIN1251` |
| `ISO_8859_6`    | **ISO_8859_6**, `UTF8`                                       |
| `ISO_8859_7`    | **ISO_8859_7**, `UTF8`                                       |
| `ISO_8859_8`    | **ISO_8859_8**, `UTF8`                                       |
| `JOHAB`         | **JOHAB**, `UTF8`                                            |
| `KOI8R`         | **KOI8R**, `ISO_8859_5`, `MULE_INTERNAL`, `UTF8`, `WIN866`, `WIN1251` |
| `KOI8U`         | **KOI8U**, `UTF8`                                            |
| `LATIN1`        | **LATIN1**, `MULE_INTERNAL`, `UTF8`                          |
| `LATIN2`        | **LATIN2**, `MULE_INTERNAL`, `UTF8`, `WIN1250`               |
| `LATIN3`        | **LATIN3**, `MULE_INTERNAL`, `UTF8`                          |
| `LATIN4`        | **LATIN4**, `MULE_INTERNAL`, `UTF8`                          |
| `LATIN5`        | **LATIN5**, `UTF8`                                           |
| `LATIN6`        | **LATIN6**, `UTF8`                                           |
| `LATIN7`        | **LATIN7**, `UTF8`                                           |
| `LATIN8`        | **LATIN8**, `UTF8`                                           |
| `LATIN9`        | **LATIN9**, `UTF8`                                           |
| `LATIN10`       | **LATIN10**, `UTF8`                                          |
| `MULE_INTERNAL` | **MULE_INTERNAL**, `BIG5`, `EUC_CN`, `EUC_JP`, `EUC_KR`, `EUC_TW`, `ISO_8859_5`, `KOI8R`, `LATIN1` to `LATIN4`, `SJIS`, `WIN866`, `WIN1250`, `WIN1251` |
| `SJIS`          | **不支持做服务器端编码**                                     |
| `SQL_ASCII`     | **任意（不会发生编码转换）**                                 |
| `UHC`           | **不支持做服务器端编码**                                     |
| `UTF8`          | **所有支持的编码**                                           |
| `WIN866`        | **WIN866**, `ISO_8859_5`, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN1251` |
| `WIN874`        | **WIN874**, `UTF8`                                           |
| `WIN1250`       | **WIN1250**, `LATIN2`, `MULE_INTERNAL`, `UTF8`               |
| `WIN1251`       | **WIN1251**, `ISO_8859_5`, `KOI8R`, `MULE_INTERNAL`, `UTF8`, `WIN866` |
| `WIN1252`       | **WIN1252**, `UTF8`                                          |
| `WIN1253`       | **WIN1253**, `UTF8`                                          |
| `WIN1254`       | **WIN1254**, `UTF8`                                          |
| `WIN1255`       | **WIN1255**, `UTF8`                                          |
| `WIN1256`       | **WIN1256**, `UTF8`                                          |
| `WIN1257`       | **WIN1257**, `UTF8`                                          |
| `WIN1258`       | **WIN1258**, `UTF8`                                          |

要打开自动字符集转换功能，您必须键入想在客户端使用的字符集（编码）。 可以通过以下几种方法来实现。

- 用gsql里的`\encoding`命令。 `\encoding`允许动态修改客户端编码。 比如，把编码改变为`SJIS`，键入：

  ```sql
  \encoding SJIS
  ```

- 使用[libpq函数](../../developer-guide/dev/4-development-based-on-libpq/2-libpq/1-database-connection-control-functions/1-database-connection-control-functions-overview.md)控制客户端编码。

- 使用`SET client_encoding TO`语句。使用下面的SQL命令设置客户端编码：

  ```bash
  SET CLIENT_ENCODING TO 'value';
  ```

  您也可以使用标准SQL语法`SET NAMES`：

  ```bash
  SET NAMES 'value';
  ```

  查询当前客户端编码：

  ```bash
  SHOW client_encoding;
  ```

  重置默认编码：

  ```bash
  RESET client_encoding;
  ```

- 使用`PGCLIENTENCODING`。如果在客户端环境里定义了 `PGCLIENTENCODING`环境变量，那么在与服务器进行连接时将自动选择这个客户端编码。此编码可以用上文介绍的方法重写。

- 使用[client_encoding](../../reference-guide/guc-parameters/15-default-settings-of-client-connection/2-zone-and-formatting.md#client_encoding)配置变量。 如果在`client_encoding`里设置了该变量， 那么在与服务器建立了连接之后，将自动选定这个客户端编码。此设置可以用上文介绍的方法重写。

如果无法进行特定的字符转换，比如，所选的服务器编码是`EUC_JP`而客户端是`LATIN1`，那么部分日文字符无法转换，此时将会报错。

如果客户端字符集定义成了`SQL_ASCII`， 那么编码转换会被禁用，无论服务器字符集设置如何。和服务器一样，除非您的业务环境全部使用ASCII数据，否则不建议使用`SQL_ASCII`。

## 进一步阅读

您可以查阅以下资料了解各种类型的编码系统。

- *CJKV Information Processing: Chinese, Japanese, Korean & Vietnamese Computing*

  包含`EUC_JP`, `EUC_CN`, `EUC_KR`, `EUC_TW`的详细说明。

- [http://www.unicode.org/](http://www.unicode.org/)

  Unicode主页。

- RFC 3629

  UTF-8（8-bit UCS/Unicode转换格式）的定义。
