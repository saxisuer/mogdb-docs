---
title: gsql客户端连接
summary: gsql客户端连接
author: Guo Huan
date: 2021-12-2
---

# gsql客户端连接

## 确认连接信息

客户端工具通过数据库主节点连接数据库。因此连接前，需获取数据库主节点所在服务器的IP地址及数据库主节点的端口号信息。

1. 以操作系统用户omm登录数据库主节点。

2. 使用"gs_om -t status --detail"命令查询MogDB各实例情况。

   ```bash
   gs_om -t status --detail

   [  Datanode State   ]

   node                 node_ip         instance                state
   ---------------------------------------------------------------------------------
   1  mogdb-kernel-0005 172.16.0.176    6001 /mogdb/data/db1 P Primary Normal
   ```

   如上部署了数据库主节点实例的服务器IP地址为172.16.0.176。数据库主节点数据路径为"/mogdb/data/db1"。

3. 确认数据库主节点的端口号。

   在步骤2查到的数据库主节点数据路径下的postgresql.conf文件中查看端口号信息。示例如下:

   ```bash
   cat /mogdb/data/db1/postgresql.conf | grep port

   port = 26000    # (change requires restart)
   #comm_sctp_port = 1024   # Assigned by installation (change requires restart)
   #comm_control_port = 10001  # Assigned by installation (change requires restart)
         # supported by the operating system:
         # e.g. 'localhost=10.145.130.2 localport=12211 remotehost=10.145.130.3 remoteport=12212, localhost=10.145.133.2 localport=12213 remotehost=10.145.133.3 remoteport=12214'
         # e.g. 'localhost=10.145.130.2 localport=12311 remotehost=10.145.130.4 remoteport=12312, localhost=10.145.133.2 localport=12313 remotehost=10.145.133.4 remoteport=12314'
         #   %r = remote host and port
   alarm_report_interval = 10
   support_extended_features=true
   ```

   第一行中的26000为数据库主节点的端口号。

<br/>

## 安装gsql客户端

在客户端机器上，上传客户端工具包并配置gsql的执行环境变量。

1. 以任意用户登录客户端机器。

2. 创建"/opt/mogdb/tools"目录。

   ```bash
   mkdir /opt/mogdb/tools
   ```

3. 获取软件安装包中的"MogDB-x.x.x-openEuler-64bit-tools.tar.gz"上传到"/opt/mogdb/tools"路径下。

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - 软件包相对位置为安装时所放位置，根据实际情况填写。
   > - 不同的操作系统，工具包文件名称会有差异。请根据实际的操作系统类型选择对应的工具包。

4. 解压文件。

   ```bash
   cd /opt/mogdb/tools
   tar -zxvf MogDB-x.x.x-openEuler-64bit-tools.tar.gz
   ```

5. 设置环境变量。

   打开"~/.bashrc"文件。

   ```bash
   vi ~/.bashrc
   ```

   在其中输入如下内容后，使用":wq!"命令保存并退出。

   ```bash
   export PATH=/opt/mogdb/tools/bin:$PATH
   export LD_LIBRARY_PATH=/opt/mogdb/tools/lib:$LD_LIBRARY_PATH
   ```

6. 使环境变量配置生效。

   ```bash
   source ~/.bashrc
   ```

<br/>

## 使用gsql连接数据库

<br/>

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 缺省情况下，客户端连接数据库后处于空闲状态时会根据参数session_timeout的默认值自动断开连接。如果要关闭超时设置，设置参数session_timeout为0即可。

<br/>

### 本地连接数据库

1. 以操作系统用户omm登录数据库服务器。

2. 连接数据库。

   数据库安装完成后，默认生成名称为postgres的数据库。第一次连接数据库时可以连接到此数据库。

   执行如下命令连接数据库。

   ```bash
   gsql -d postgres -p 26000
   ```

   其中postgres为需要连接的数据库名称，26000为数据库主节点的端口号。请根据实际情况替换。

   连接成功后，系统显示类似如下信息:

   ```sql
   gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.

   postgres=#
   ```

   omm用户是管理员用户，因此系统显示"postgres=#"。若使用普通用户身份登录和连接数据库，系统显示"postgres=>"。

   "Non-SSL connection"表示未使用SSL方式连接数据库。如果需要高安全性时，请使用SSL连接。

3. 退出数据库。

   ```sql
   postgres=# \q
   ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 使用omm用户在本地连接数据库，无需输入密码，这是由于在pg_hba.conf文件中默认设置了本机允许trust方式连接。
> - 更多关于客户端认证方式的信息，请参见[客户端接入认证](../../security-guide/security/1-client-access-authentication.md)章节。

<br/>

## 远程连接数据库

<br/>

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 由于安全性限制，数据库初始用户omm无法进行远程连接。

<br/>

### 通过gs_guc配置白名单（更新pg_hba.conf文件）

1. 以操作系统用户omm登录数据库主节点。

2. 配置客户端认证方式，允许客户端以"jack"用户连接到本机，远程连接禁止使用"omm"用户（即数据库初始化用户）。

   例如，下面示例中配置允许IP地址为172.16.0.245的客户端访问本机。

   ```bash
   gs_guc set -N all -I all -h "host all jack 172.16.0.245/24 sha256"
   ```

   **注意：**

   - 使用"jack"用户前，需先本地连接数据库，并在数据库中使用如下语句建立"jack"用户:

     ```sql
     postgres=# CREATE USER jack PASSWORD 'Test@123';
     ```

   - -N all表示MogDB的所有主机。

   - -I all表示主机的所有实例。

   - -h表示指定需要在"pg_hba.conf"增加的语句。

   - all表示允许客户端连接到任意的数据库。

   - jack表示连接数据库的用户。

   - 172.16.0.245/24表示只允许IP地址为172.16.0.245的主机连接。此处的IP地址不能为MogDB内的IP，在使用过程中，请根据用户的网络进行配置修改。24表示子网掩码为1的位数，即255.255.255.0。

   - sha256表示连接时jack用户的密码使用sha256算法加密。

   这条命令在数据库主节点实例对应的"pg_hba.conf"文件中添加了一条规则，用于对连接数据库主节点的客户端进行鉴定。

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > + 更多关于客户端认证方式的信息，请参见[客户端接入认证](../../security-guide/security/1-client-access-authentication.md)章节。

3. 连接数据库。

   数据库安装完成后，默认生成名称为postgres的数据库。第一次连接数据库时可以连接到此数据库。

   ```bash
   gsql -d postgres -h 172.16.0.176 -U jack -p 26000 -W Test@123
   ```

   postgres为需要连接的数据库名称，172.16.0.176为数据库主节点所在的服务器IP地址，jack为连接数据库的用户，26000为数据库主节点的端口号，Test@123为连接数据库用户jack的密码。
