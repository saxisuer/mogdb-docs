---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 检查MogDB健康状态

## 检查办法

通过MogDB提供的gs_check工具可以开展MogDB健康状态检查。

**注意事项**

- 扩容新节点检查只能在root用户下执行，其他场景都必须在omm用户下执行。
- 必须指定-i或-e参数，-i会检查指定的单项，-e会检查对应场景配置中的多项。
- 如果-i参数中不包含root类检查项或-e场景配置列表中没有root类检查项，则不需要交互输入root权限的用户及其密码。
- 可使用-skip-root-items跳过检查项中包含的root类检查，以免需要输入root权限用户及密码。
- 检查扩容新节点与现有节点之间的一致性，在现有节点执行gs_check命令指定-hosts参数进行检查，其中hosts文件中需要写入新节点ip。

**操作步骤**

方式1:

1. 以操作系统用户omm登录数据库主节点。

2. 执行如下命令对MogDB数据库状态进行检查。

    ```bash
    gs_check -i CheckClusterState
    ```

    其中，-i指定检查项，注意区分大小写。格式: -i CheckClusterState、-i CheckCPU或-i CheckClusterState，CheckCPU。

    取值范围为所有支持的检查项名称，详细列表请参见“参考指南 > 工具参考 > 服务端工具 > [gs_check](../../reference-guide/tool-reference/server-tools/1-gs_check.md)”中的“表 1 MogDB状态检查表”，用户可以根据需求自己编写新检查项。

方式2:

1. 以操作系统用户omm登录数据库主节点。

2. 执行如下命令对MogDB数据库进行健康检查。

    ```bash
    gs_check -e inspect
    ```

    其中，-e指定场景名，注意区分大小写。格式: -e inspect或-e upgrade。

    取值范围为所有支持的巡检场景名称，默认列表包括: inspect（例行巡检）、upgrade（升级前巡检）、install（安装）、binary_upgrade（就地升级前巡检）、health（健康检查巡检）、slow_node（节点）、longtime（耗时长巡检），用户可以根据需求自己编写场景。

MogDB巡检的主要作用是在MogDB运行过程中，检查整个MogDB状态是否正常，或者重大操作前（升级、扩容），确保MogDB满足操作所需的环境条件和状态条件。详细的巡检项目和场景请参见《MogDB 工具参考》中"服务端工具 > [gs_checkos](../../reference-guide/tool-reference/server-tools/2-gs_checkos.md) > MogDB状态检查表"。

**示例**

执行单项检查结果:

```bash
perfadm@lfgp000700749:/opt/huawei/perfadm/tool/script> gs_check -i CheckCPU
Parsing the check items config file successfully
Distribute the context file to remote hosts successfully
Start to health check for the cluster. Total Items:1 Nodes:3

Checking...               [=========================] 1/1
Start to analysis the check result
CheckCPU....................................OK
The item run on 3 nodes.  success: 3

Analysis the check result successfully
Success. All check items run completed. Total:1  Success:1  Failed:0
For more information please refer to /opt/mogdb/tools/script/gspylib/inspection/output/CheckReport_201902193704661604.tar.gz
```

本地执行结果:

```bash
perfadm@lfgp000700749:/opt/huawei/perfadm/tool/script> gs_check -i CheckCPU -L

2017-12-29 17:09:29 [NAM] CheckCPU
2017-12-29 17:09:29 [STD] 检查主机CPU占用率，如果idle 大于30%并且iowait 小于 30%.则检查项通过，否则检查项不通过
2017-12-29 17:09:29 [RST] OK

2017-12-29 17:09:29 [RAW]
Linux 4.4.21-69-default (lfgp000700749)  12/29/17  _x86_64_

17:09:24        CPU     %user     %nice   %system   %iowait    %steal     %idle
17:09:25        all      0.25      0.00      0.25      0.00      0.00     99.50
17:09:26        all      0.25      0.00      0.13      0.00      0.00     99.62
17:09:27        all      0.25      0.00      0.25      0.13      0.00     99.37
17:09:28        all      0.38      0.00      0.25      0.00      0.13     99.25
17:09:29        all      1.00      0.00      0.88      0.00      0.00     98.12
Average:        all      0.43      0.00      0.35      0.03      0.03     99.17
```

执行场景检查结果:

```bash
[perfadm@SIA1000131072 Check]$ gs_check -e inspect
Parsing the check items config file successfully
The below items require root privileges to execute:[CheckBlockdev CheckIOrequestqueue CheckIOConfigure CheckCheckMultiQueue CheckFirewall CheckSshdService CheckSshdConfig CheckCrondService CheckBootItems CheckFilehandle CheckNICModel CheckDropCache]
Please enter root privileges user[root]:root
Please enter password for user[root]:
Please enter password for user[root] on the node[10.244.57.240]:
Check root password connection successfully
Distribute the context file to remote hosts successfully
Start to health check for the cluster. Total Items:57 Nodes:2

Checking...               [                         ] 21/57
Checking...               [=========================] 57/57
Start to analysis the check result
CheckClusterState...........................OK
The item run on 2 nodes.  success: 2

CheckDBParams...............................OK
The item run on 1 nodes.  success: 1

CheckDebugSwitch............................OK
The item run on 2 nodes.  success: 2

CheckDirPermissions.........................OK
The item run on 2 nodes.  success: 2

CheckReadonlyMode...........................OK
The item run on 1 nodes.  success: 1

CheckEnvProfile.............................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
GAUSSHOME        /usr1/mogdb/app
LD_LIBRARY_PATH  /usr1/mogdb/app/lib
PATH             /usr1/mogdb/app/bin


CheckBlockdev...............................OK
The item run on 2 nodes.  success: 2

CheckCurConnCount...........................OK
The item run on 1 nodes.  success: 1

CheckCursorNum..............................OK
The item run on 1 nodes.  success: 1

CheckPgxcgroup..............................OK
The item run on 1 nodes.  success: 1

CheckDiskFormat.............................OK
The item run on 2 nodes.  success: 2

CheckSpaceUsage.............................OK
The item run on 2 nodes.  success: 2

CheckInodeUsage.............................OK
The item run on 2 nodes.  success: 2

CheckSwapMemory.............................OK
The item run on 2 nodes.  success: 2

CheckLogicalBlock...........................OK
The item run on 2 nodes.  success: 2

CheckIOrequestqueue.....................WARNING
The item run on 2 nodes.  warning: 2
The warning[host240,host157] value:
On device (vdb) 'IO Request' RealValue '256' ExpectedValue '32768'
On device (vda) 'IO Request' RealValue '256' ExpectedValue '32768'

CheckMaxAsyIOrequests.......................OK
The item run on 2 nodes.  success: 2

CheckIOConfigure............................OK
The item run on 2 nodes.  success: 2

CheckMTU....................................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
1500

CheckPing...................................OK
The item run on 2 nodes.  success: 2

CheckRXTX...................................NG
The item run on 2 nodes.  ng: 2
The ng[host240,host157] value:
NetWork[eth0]
RX: 256
TX: 256


CheckNetWorkDrop............................OK
The item run on 2 nodes.  success: 2

CheckMultiQueue.............................OK
The item run on 2 nodes.  success: 2

CheckEncoding...............................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
LANG=en_US.UTF-8

CheckFirewall...............................OK
The item run on 2 nodes.  success: 2

CheckKernelVer..............................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
3.10.0-957.el7.x86_64

CheckMaxHandle..............................OK
The item run on 2 nodes.  success: 2

CheckNTPD...................................OK
host240: NTPD service is running, 2020-06-02 17:00:28
host157: NTPD service is running, 2020-06-02 17:00:06


CheckOSVer..................................OK
host240: The current OS is centos 7.6 64bit.
host157: The current OS is centos 7.6 64bit.


CheckSysParams..........................WARNING
The item run on 2 nodes.  warning: 2
The warning[host240,host157] value:
Warning reason: variable 'net.ipv4.tcp_retries1' RealValue '3' ExpectedValue '5'.
Warning reason: variable 'net.ipv4.tcp_syn_retries' RealValue '6' ExpectedValue '5'.

CheckTHP....................................OK
The item run on 2 nodes.  success: 2

CheckTimeZone...............................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
+0800

CheckCPU....................................OK
The item run on 2 nodes.  success: 2

CheckSshdService............................OK
The item run on 2 nodes.  success: 2

Warning reason: UseDNS parameter is not set; expected: no

CheckCrondService...........................OK
The item run on 2 nodes.  success: 2

CheckStack..................................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
8192

CheckSysPortRange...........................OK
The item run on 2 nodes.  success: 2

CheckMemInfo................................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
totalMem: 31.260929107666016G

CheckHyperThread............................OK
The item run on 2 nodes.  success: 2

CheckTableSpace.............................OK
The item run on 1 nodes.  success: 1

CheckSysadminUser...........................OK
The item run on 1 nodes.  success: 1


CheckGUCConsistent..........................OK
All DN instance guc value is consistent.

CheckMaxProcMemory..........................OK
The item run on 1 nodes.  success: 1

CheckBootItems..............................OK
The item run on 2 nodes.  success: 2

CheckHashIndex..............................OK
The item run on 1 nodes.  success: 1

CheckPgxcRedistb............................OK
The item run on 1 nodes.  success: 1

CheckNodeGroupName..........................OK
The item run on 1 nodes.  success: 1

CheckTDDate.................................OK
The item run on 1 nodes.  success: 1

CheckDilateSysTab...........................OK
The item run on 1 nodes.  success: 1

CheckKeyProAdj..............................OK
The item run on 2 nodes.  success: 2

CheckProStartTime.......................WARNING
host157:
STARTED COMMAND
Tue Jun  2 16:57:18 2020 /usr1/dmuser/dmserver/metricdb1/server/bin/mogdb --single_node -D /usr1/dmuser/dmb1/data -p 22204
Mon Jun  1 16:15:15 2020 /usr1/mogdb/app/bin/mogdb -D /usr1/mogdb/data/dn1 -M standby


CheckFilehandle.............................OK
The item run on 2 nodes.  success: 2

CheckRouting................................OK
The item run on 2 nodes.  success: 2

CheckNICModel...............................OK
The item run on 2 nodes.  success: 2  (consistent)
The success on all nodes value:
version: 1.0.1
model: Red Hat, Inc. Virtio network device


CheckDropCache..........................WARNING
The item run on 2 nodes.  warning: 2
The warning[host240,host157] value:
No DropCache process is running

CheckMpprcFile..............................NG
The item run on 2 nodes.  ng: 2
The ng[host240,host157] value:
There is no mpprc file

Analysis the check result successfully
Failed. All check items run completed. Total:57   Success:50   Warning:5   NG:2
For more information please refer to /usr1/mogdb/tool/script/gspylib/inspection/output/CheckReport_inspect611.tar.gz
```

<br/>

## 异常处理

如果发现检查结果异常，可以根据以下内容进行修复。

**表 1** 检查MogDB运行状态

<table>
    <tr>
        <td>检查项    </td>
        <td>  异常状态  </td>
        <td> 处理方法   </td>
     </tr>
    <tr>
        <td rowspan="2"> CheckClusterState（检查MogDB状态）   </td>
        <td>  MogDB未启动或MogDB实例未启动  </td>
        <td>  使用以下命令启动MogDB及实例。<br />gs_om -t start  </td>
     </tr>
    <tr>
        <td>  MogDB状态异常或MogDB实例异常  </td>
        <td>  检查各主机、实例状态，根据状态信息进行排查。<br />gs_check -i CheckClusterState </td>
     </tr>
    <tr>
        <td>  CheckDBParams（检查数据库参数）  </td>
        <td>  数据库参数错误  </td>
        <td>   通过gs_guc工具修改数据库参数为指定值。 </td>
     </tr>
    <tr>
        <td>  CheckDebugSwitch（检查调试日志）  </td>
        <td>  日志级别不正确  </td>
        <td>  使用gs_guc工具将log_min_messages改为指定内容。  </td>
     </tr>
    <tr>
        <td>  CheckDirPermissions（检查目录权限）  </td>
        <td>  路径权限错误  </td>
        <td>   修改对应目录权限为指定数值（750/700）。<br />chmod 750 DIR </td>
     </tr>
    <tr>
        <td>  CheckReadonlyMode（检查只读模式）  </td>
        <td>  只读模式被打开  </td>
        <td> 确认数据库节点所在磁盘使用率未超阈值（默认60%）且未在执行其他运维操作。<br />gs_check -i CheckDataDiskUsage ps ux<br />使用gs_guc工具关闭MogDB只读模式。<br />gs_guc reload -N all -I all -c 'default_transaction_read_only = off'     </td>
     </tr>
    <tr>
        <td> CheckEnvProfile（检查环境变量）   </td>
        <td>  环境变量不一致  </td>
        <td> 重新执行前置更新环境变量信息。   </td>
     </tr>
    <tr>
        <td>  CheckBlockdev（检查磁盘预读块）  </td>
        <td>  磁盘预读块大小不为16384  </td>
        <td> 使用gs_checkos设置预读块大小为16384KB，并写入自启动文件。<br />gs_checkos -i B3  </td>
     </tr>
    <tr>
        <td> CheckCursorNum（检查游标数）   </td>
        <td>   检查游标数失败 </td>
        <td>  检查数据库能否正常连接，MogDB状态是否正常。  </td>
     </tr>
    <tr>
        <td>  CheckPgxcgroup（检查重分布状态） </td>
        <td>  有未完成重分布的pgxc_group表  </td>
        <td>  继续完成扩容或缩容的数据重分布操作。<br />gs_expand、gs_shrink </td>
     </tr>
    <tr>
        <td> CheckDiskFormat（检查磁盘配置）   </td>
        <td>  各节点磁盘配置不一致  </td>
        <td>  将各节点的磁盘规格改为相同。  </td>
     </tr>
    <tr>
        <td> CheckSpaceUsage（检查磁盘空间使用率）   </td>
        <td>  磁盘可用空间不足  </td>
        <td> 清理或扩展对应目录所在的磁盘。   </td>
     </tr>
    <tr>
        <td>   CheckInodeUsage（检查磁盘索引使用率） </td>
        <td>  磁盘可用索引不足  </td>
        <td>  清理或扩展对应目录所在的磁盘。  </td>
     </tr>
    <tr>
        <td>  CheckSwapMemory（检查交换内存）  </td>
        <td>  交换内存大于物理内存  </td>
        <td>  将交换内存调小或关闭。  </td>
     </tr>
    <tr>
        <td>  CheckLogicalBlock（检查磁盘逻辑块）  </td>
        <td>  磁盘逻辑块大小不为512  </td>
        <td>  使用gs_checkos修改磁盘逻辑块大小为512KB，并写入开机自启动文件。<br />gs_checkos -i B4  </td>
     </tr>
    <tr>
        <td>  CheckIOrequestqueue（检查IO请求）  </td>
        <td>  IO请求值不为32768  </td>
        <td>  使用gs_checkos设置IO请求值为32768，并写入开机自启动文件。<br />gs_checkos -i B4  </td>
     </tr>
    <tr>
        <td>  CheckCurConnCount（检查当前连接数）  </td>
        <td>  当前连接数超过最大连接数的90%  </td>
        <td>  断开未使用的数据库主节点连接。  </td>
     </tr>
    <tr>
        <td>  CheckMaxAsyIOrequests（检查最大异步请求）  </td>
        <td>  最大异步请求值小于104857600或当前节点数据库实例数乘以1048576  </td>
        <td>  使用gs_checkos设置最大异步请求值为104857600和当前节点数据库实例数乘以1048576中的最大值。<br />gs_checkos -i B4  </td>
     </tr>
    <tr>
        <td> CheckMTU（检查MTU值）   </td>
        <td>  MTU值不一致  </td>
        <td> 设置各节点的MTU一致为1500或8192。<br />ifconfig eth* MTU 1500  </td>
     </tr>
      <tr>
        <td>  CheckIOConfigure（检查IO配置）  </td>
        <td>  IO配置不是deadline  </td>
        <td>   使用gs_checkos设置IO配置为deadline，并写入开机自启动文件。<br />gs_checkos -i B4 </td>
     </tr>
      <tr>
        <td>   CheckRXTX（检查RXTX值） </td>
        <td>  网卡RX/TX值不是4096  </td>
        <td>  使用checkos设置MogDB使用的物理网卡RX/TX值为4096。<br />gs_checkos -i B5  </td>
     </tr>
      <tr>
        <td>   CheckPing（检查网络通畅） </td>
        <td>    存在MogDB IP无法ping通</td>
        <td>  检查异常ip间网络设置和状态、防火墙状态。  </td>
     </tr>
      <tr>
        <td> CheckNetWorkDrop（检查网络丢包率）   </td>
        <td> 网络通信丢包率高于1%   </td>
        <td>  检查对应IP间网络负载、状态。  </td>
     </tr>
      <tr>
        <td> CheckMultiQueue（检查网卡多队列）   </td>
        <td>   未开启网卡多队列并未将网卡中断绑定到不同CPU核心 </td>
        <td>  开启网卡多队列并将网卡队列中断绑定到不同的CPU核心。  </td>
     </tr>
      <tr>
        <td>  CheckEncoding（检查编码格式）  </td>
        <td>  各节点编码格式不一致  </td>
        <td>  在/etc/profile中写入一致的编码信息。<br />echo "export LANG=XXX" >> /etc/profile  </td>
     </tr>
      <tr>
        <td>  CheckActQryCount（检查归档模式）  </td>
        <td>  启用归档模式，归档目录不在主数据库节点目录下  </td>
        <td>    关闭归档模式或者将归档目录设置在主数据库节点目录下。</td>
     </tr>
      <tr>
        <td>  CheckFirewall（检查防火墙）  </td>
        <td>  防火墙未关闭  </td>
        <td>  关闭防火墙服务。<br />systemctl disable firewalld.service   </td>
     </tr>
      <tr>
        <td>  CheckKernelVer（检查内核版本）  </td>
        <td> 节点间的内核版本不一致  </td>
        <td>    </td>
     </tr>
      <tr>
        <td>  CheckMaxHandle（检查最大文件句柄数）  </td>
        <td>  最大文件句柄数小于1000000  </td>
        <td>  设置91-nofile.conf/90-nofile.conf最大文件句柄数软硬限制为1000000。<br />gs_checkos -i B2  </td>
     </tr>
      <tr>
        <td>  CheckNTPD（检查时间同步服务）  </td>
        <td>  NTPD服务未开启或时间误差超过一分钟  </td>
        <td>   开启NTPD服务并设置时钟一致。 </td>
     </tr>
      <tr>
        <td>   CheckSysParams（检查操作系统参数） </td>
        <td>  操作系统参数设置不满足要求  </td>
        <td>  使用gs_checkos进行参数设置或手动设置。<br />gs_checkos -i B1 vim /etc/sysctl.conf  </td>
     </tr>
      <tr>
        <td>   CheckTHP（检查THP服务） </td>
        <td>  THP服务未开启  </td>
        <td>   使用gs_checkos设置THP服务。<br />gs_checkos -i B6</td>
     </tr>
      <tr>
        <td>  CheckTimeZone（检查时区）  </td>
        <td> 时区不一致   </td>
        <td>  设置各节点为同一时区。<br />`cp /usr/share/zoneinfo/$地区/$时区 /etc/localtime`  </td>
     </tr>
      <tr>
        <td>   CheckCPU（检查CPU） </td>
        <td>   CPU占用过高或IO等待过高 </td>
        <td>   进行CPU配置升级或磁盘性能升级。</td>
     </tr>
      <tr>
        <td>  CheckSshdService（检查SSHD服务）  </td>
        <td> 未开启SSHD服务   </td>
        <td>  启动SSHD服务并写入开机自启动文件。<br />service sshd start echo "server sshd start" >> initFile  </td>
     </tr>
      <tr>
        <td> CheckSshdConfig（检查SSHD配置）   </td>
        <td>  SSHD服务配置错误  </td>
        <td> 设置SSHD服务，<br />PasswordAuthentication=no; MaxStartups=1000; UseDNS=yes; ClientAliveInterval=10800/ClientAliveInterval=0<br />并重启服务: <br />server sshd start   </td>
     </tr>
      <tr>
        <td>  CheckCrondService（检查Crond服务）  </td>
        <td> Crond服务未启动   </td>
        <td>  安装Crond服务并启用。</td>
     </tr>
      <tr>
        <td>   CheckStack（检查堆栈大小） </td>
        <td>   堆栈大小小于3072 </td>
        <td>   使用gs_checkos设置为3072并重启堆栈值过小进程。<br /> gs_checkos -i B2 </td>
     </tr>
      <tr>
        <td>   CheckSysPortRange（检查系统端口设置） </td>
        <td>  系统ip端口不在预期范围内或MogDB端口在系统ip端口内  </td>
        <td>  设置系统ip端口范围参数到26000-65535之中；设置MogDB端口在系统ip端口范围外。<br />vim /etc/sysctl.conf  </td>
     </tr>
      <tr>
        <td>  CheckMemInfo（检查内存信息）  </td>
        <td>  各节点内存大小不一致  </td>
        <td>  使用相同规格的物理内存。</td>
     </tr>
      <tr>
        <td>  CheckHyperThread（检查超线程）  </td>
        <td>  未开启CPU超线程  </td>
        <td>  开启CPU超线程。</td>
     </tr>
      <tr>
        <td> CheckTableSpace（检查表空间）   </td>
        <td>  表空间路径和MogDB路径存在嵌套或表空间路径相互存在嵌套  </td>
        <td>  将表空间数据迁移到路径合法的表空间中。</td>
     </tr>
</table>

## 状态查询

**背景信息**

MogDB支持查看整个MogDB的状态，通过查询结果确认MogDB或者单个主机的运行状态是否正常。

**前提条件**

MogDB已经启动。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令查询MogDB状态：

   ```bash
   gs_om -t status --detail
   ```

   MogDB状态显示结果的参数说明请参见表1。

   若要查询某主机上的实例状态，请在命令中增加“-h”项。示例如下：

   ```bash
   gs_om -t status -h plat2
   ```

   其中，plat2为待查询主机的名称。

**参数说明**

**表 1** 节点角色说明

| 字段          | 字段含义                               | 字段值                                                       |
| :------------ | :------------------------------------- | :----------------------------------------------------------- |
| cluster_state | MogDB状态。显示整个MogDB是否运行正常。 | - Normal：表示MogDB可用，且数据有冗余备份。所有进程都在运行，主备关系正常。<br/>- Unavailable：表示MogDB不可用。<br/>- Degraded：表示MogDB可用，但存在故障的数据库节点、数据库主节点实例。 |
| node          | 主机名称                               | 表示该实例所在的主机名称。多AZ时会显示AZ编号。               |
| node_ip       | 主机IP                                 | 表示该实例所在的主机IP。                                     |
| instance      | 实例ID                                 | 表示该实例的ID。                                             |
| state         | 实例角色                               | - Normal：表示单主机实例。<br/>- Primary：表示实例为主实例。<br/>- Standby：表示实例为备实例。<br/>- Cascade Standby：表示实例为级联备实例。<br/>- Pending：表示该实例在仲裁阶段。<br/>- Unknown：表示实例状态未知。<br/>- Down：表示实例处于宕机状态。<br/>- Abnormal：表示节点处于异常状态。<br/>- Manually stopped：表示节点已经被手动停止。 |

每个角色也存在不同的状态，例如启动，连接等，其各个状态说明如下：

**表 2** 节点状态说明

| 状态           | 字段含义                                                     |
| :------------- | :----------------------------------------------------------- |
| Normal         | 表示节点启动正常                                             |
| Need repair    | 当前节点需要修复                                             |
| Starting       | 节点正在启动中                                               |
| Wait promoting | 节点正等待升级中，例如备机向主机发送升级请求后，正在等待主机回应时的状态 |
| Promoting      | 备节点正在升级为主节点的状态                                 |
| Demoting       | 节点正在降级中，如主机正在降为备机中                         |
| Building       | 备机启动失败，需要重建                                       |
| Catchup        | 备节点正在追赶主节点                                         |
| Coredump       | 节点程序崩溃                                                 |
| Unknown        | 节点状态未知                                                 |

当节点出现Need repair状态时，可能需要对该节点进行重建使其恢复正常，通常情况下，节点重建原因说明如下：

**表3** 节点重建原因说明

| 状态                  | 字段含义                                              |
| :-------------------- | :---------------------------------------------------- |
| Normal                | 表示节点启动正常                                      |
| WAL segment removed   | 主机日志WAL日志不存在，备机日志比主机日志新           |
| Disconnect            | 备机不能连接主机                                      |
| Version not matched   | 主备二进制版本不一致                                  |
| Mode not matched      | 主备角色不匹配，例如两个备机互联                      |
| System id not matched | 主备数据库系统id不一致，主备双机要求System ID必须一致 |
| Timeline not matched  | 日志时间线不一致                                      |
| Unknown               | 其他原因                                              |

**示例**

查看MogDB详细状态信息，含实例状态信息。

```bash
gs_om -t status --detail
[   Cluster State   ]

cluster_state   : Normal
redistributing  : No
current_az      : AZ_ALL

[  Datanode State   ]

    node             node_ip         port      instance                                   state
-----------------------------------------------------------------------------------------------------
1  pekpopgsci00235  10.244.62.204    5432      6001 /opt/mogdb/cluster/data/dn1   P Primary Normal
2  pekpopgsci00238  10.244.61.81     5432      6002 /opt/mogdb/cluster/data/dn1   S Standby Normal
```
