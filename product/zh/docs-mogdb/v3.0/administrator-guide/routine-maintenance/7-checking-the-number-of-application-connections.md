---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 检查应用连接数

如果应用程序与数据库的连接数超过最大值，则新的连接无法建立。建议每天检查连接数，及时释放空闲的连接或者增加最大连接数。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 使用如下命令连接数据库。

    ```bash
    gsql -d mogdb -p 8000
    ```

    mogdb为需要连接的数据库名称，8000为数据库主节点的端口号。

    连接成功后，系统显示类似如下信息:

    ```bash
    gsql ((MogDB x.x.x build 56189e20) compiled at 2022-01-07 18:47:53 commit 0 last mr  )
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.

    MogDB=#
    ```

3. 执行如下SQL语句查看连接数。

    ```sql
    MogDB=# SELECT count(*) FROM (SELECT pg_stat_get_backend_idset() AS backendid) AS s;
    ```

    显示类似如下的信息，其中2表示当前有两个应用连接到数据库。

    ```sql
    count
    -------
         2
    (1 row)
    ```

4. 查看现有最大连接数。

    ```sql
    MogDB=# SHOW max_connections;
    ```

    显示信息如下，其中200为现在的最大连接数。

    ```sql
     max_connections
    -----------------
     200
    (1 row)
    ```

<br/>

## 异常处理

如果显示的连接数接近数据库的最大连接数max_connections，则需要考虑清理现有连接数或者增加新的连接数。

1. 执行如下SQL语句，查看state字段等于idle，且state_change字段长时间没有更新过的连接信息。

    ```sql
    MogDB=# SELECT * FROM pg_stat_activity where state='idle' order by state_change;
    ```

    显示类似如下的信息:

    ```sql
     datid | datname  |       pid       | usesysid | usename  | application_name |  client_addr
     | client_hostname | client_port |         backend_start         | xact_start |          quer
    y_start          |         state_change          | waiting | enqueue | state | resource_pool
    |                    query
    -------+----------+-----------------+----------+----------+------------------+---------------
    -+-----------------+-------------+-------------------------------+------------+--------------
    -----------------+-------------------------------+---------+---------+-------+---------------
    +----------------------------------------------
     13626 | mogdb | 140390162233104 |       10 | gaussdba |                  |
     |                 |          -1 | 2016-07-15 14:08:59.474118+08 |            | 2016-07-15 14
    :09:04.496769+08 | 2016-07-15 14:09:04.496975+08 | f       |         | idle  | default_pool
    | select count(group_name) from pgxc_group;
     13626 | mogdb | 140390132872976 |       10 | gaussdba | cn_5002          | 10.180.123.163
     |                 |       48614 | 2016-07-15 14:11:16.014871+08 |            | 2016-07-15 14
    :21:17.346045+08 | 2016-07-15 14:21:17.346095+08 | f       |         | idle  | default_pool
    | SET SESSION AUTHORIZATION DEFAULT;RESET ALL;
    (2 rows)
    ```

2. 释放空闲的连接数。

    查看每个连接，并与此连接的使用者确认是否可以断开连接，或执行如下SQL语句释放连接。其中，pid为上一步查询中空闲连接所对应的pid字段值。

    ```sql
    MogDB=# SELECT pg_terminate_backend(140390132872976);
    ```

    显示类似如下的信息:

    ```sql
    MogDB=# SELECT pg_terminate_backend(140390132872976);
     pg_terminate_backend
    ----------------------
     t
    (1 row)
    ```

    如果没有可释放的连接，请执行下一步。

3. 增加最大连接数。

    ```bash
    gs_guc set -D /mogdb/data/dbnode -c "max_connections= 800"
    ```

    其中800为新修改的连接数。

4. 重启数据库服务使新的设置生效。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
    > 重启MogDB操作会导致用户执行操作中断，请在操作之前规划好合适的执行窗口。

    ```bash
    gs_om -t stop && gs_om -t start
    ```
