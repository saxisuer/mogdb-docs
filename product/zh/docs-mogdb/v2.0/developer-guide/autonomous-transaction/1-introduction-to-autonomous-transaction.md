---
title: 介绍
summary: 介绍
author: Zhang Cuiping
date: 2021-05-10
---

# 介绍

自治事务（Autonomous Transaction）将一个主事务分割成几个子事务，在执行完子事务以后再继续执行主事务。子事务是独立于主事务的，子事务中的ROLLBACK和COMMIT操作只会影响子事务中的DML操作；同样，主事务中的ROLLBACK和COMMIT操作只会影响主事务中的DML操作，而不会影响子事务中的操作。在子事务中已经COMMIT的操作，不会被主事务中的ROLLBACK撤销。

自治事务在函数或存储过程中定义，用PRAGMA AUTONOMOUS_TRANSACTION关键字来声明
