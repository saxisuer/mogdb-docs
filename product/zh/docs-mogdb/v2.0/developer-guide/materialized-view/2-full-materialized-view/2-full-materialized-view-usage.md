---
title: 使用
summary: 使用
author: Liuxu
date: 2021-05-21
---

# 使用

## 语法格式

- 创建全量物化视图

  ```sql
  CREATE MATERIALIZED VIEW [ view_name ] AS { query_block }; 
  ```

- 全量刷新物化视图

  ```sql
  REFRESH MATERIALIZED VIEW [ view_name ];
  ```

- 删除物化视图

  ```sql
  DROP MATERIALIZED VIEW [ view_name ];
  ```

- 查询物化视图

  ```sql
  SELECT * FROM [ view_name ];
  ```

## 示例

```sql
--准备数据。
mogdb=# CREATE TABLE t1(c1 int, c2 int);
mogdb=# INSERT INTO t1 VALUES(1, 1);
mogdb=# INSERT INTO t1 VALUES(2, 2);

--创建全量物化视图。
mogdb=# CREATE MATERIALIZED VIEW mv AS select count(*) from t1;

--查询物化视图结果。
mogdb=# SELECT * FROM mv;
 count 
-------
     2
(1 row)

--向物化视图中基表插入数据。
mogdb=# INSERT INTO t1 VALUES(3, 3);

--对全量物化视图做全量刷新。
mogdb=# REFRESH MATERIALIZED VIEW mv;

--查询物化视图结果。
mogdb=# SELECT * FROM mv;
 count 
-------
     3
(1 row)

--删除物化视图。
mogdb=# DROP MATERIALIZED VIEW mv;
```