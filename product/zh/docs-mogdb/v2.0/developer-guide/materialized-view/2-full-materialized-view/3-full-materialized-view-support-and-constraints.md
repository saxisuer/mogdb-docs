---
title: 支持和约束
summary: 支持和约束
author: Liuxu
date: 2021-05-21
---

# 支持和约束

## 支持场景

- 通常全量物化视图所支持的查询范围与CREATE TABLE AS语句一致。
- 全量物化视图上支持创建索引。
- 支持analyze、explain。

## 不支持场景

物化视图不支持增删改操作，只支持查询语句。
