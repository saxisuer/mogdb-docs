---
title: javax.sql.ConnectionPoolDataSource
summary: javax.sql.ConnectionPoolDataSource
author: Guo Huan
date: 2021-05-17
---

# javax.sql.ConnectionPoolDataSource

javax.sql.ConnectionPoolDataSource是数据源连接池接口。

**表 1** 对javax.sql.ConnectionPoolDataSource的支持情况

| 方法名                                           | 返回值类型       | 支持JDBC 4 |
| :----------------------------------------------- | :--------------- | :--------- |
| getLoginTimeout()                                | int              | Yes        |
| getLogWriter()                                   | PrintWriter      | Yes        |
| getPooledConnection()                            | PooledConnection | Yes        |
| getPooledConnection(String user,String password) | PooledConnection | Yes        |
| setLoginTimeout(int seconds)                     | void             | Yes        |
| setLogWriter(PrintWriter out)                    | void             | Yes        |
