---
title: java.sql.ResultSetMetaData
summary: java.sql.ResultSetMetaData
author: Guo Huan
date: 2021-05-17
---

# java.sql.ResultSetMetaData

java.sql.ResultSetMetaData是对ResultSet对象相关信息的具体描述。

**表 1**  对java.sql.ResultSetMetaData的支持情况

| 方法名                        | 返回值类型 | 支持JDBC 4 |
| :---------------------------- | :--------- | :--------- |
| getColumnCount()              | int        | Yes        |
| getColumnName(int column)     | String     | Yes        |
| getColumnType(int column)     | int        | Yes        |
| getColumnTypeName(int column) | String     | Yes        |
