---
title: java.sql.CallableStatement
summary: java.sql.CallableStatement
author: Guo Huan
date: 2021-05-17
---

# java.sql.CallableStatement

java.sql.CallableStatement是存储过程执行接口。

**表 1** 对java.sql.CallableStatement的支持情况

| 方法名                                             | 返回值类型 | 支持JDBC 4 |
| :------------------------------------------------- | :--------- | :--------- |
| registerOutParameter(int parameterIndex, int type) | void       | Yes        |
| wasNull()                                          | Boolean    | Yes        |
| getString(int parameterIndex)                      | String     | Yes        |
| getBoolean(int parameterIndex)                     | Boolean    | Yes        |
| getByte(int parameterIndex)                        | byte       | Yes        |
| getShort(int parameterIndex)                       | short      | Yes        |
| getInt(int parameterIndex)                         | int        | Yes        |
| getLong(int parameterIndex)                        | long       | Yes        |
| getFloat(int parameterIndex)                       | float      | Yes        |
| getDouble(int parameterIndex)                      | double     | Yes        |
| getBigDecimal(int parameterIndex)                  | BigDecimal | Yes        |
| getBytes(int parameterIndex)                       | byte[]     | Yes        |
| getDate(int parameterIndex)                        | Date       | Yes        |
| getTime(int parameterIndex)                        | Time       | Yes        |
| getTimestamp(int parameterIndex)                   | Timestamp  | Yes        |
| getObject(int parameterIndex)                      | Object     | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 不允许含有OUT参数的语句执行批量操作。
> - 以下方法是从java.sql.Statement继承而来：close，execute，executeQuery，executeUpdate，getConnection，getResultSet，getUpdateCount，isClosed，setMaxRows , setFetchSize。
> - 以下方法是从java.sql.PreparedStatement继承而来：addBatch，clearParameters，execute，executeQuery，executeUpdate，getMetaData，setBigDecimal，setBoolean，setByte，setBytes，setDate，setDouble，setFloat，setInt，setLong，setNull，setObject，setString，setTime，setTimestamp 。
