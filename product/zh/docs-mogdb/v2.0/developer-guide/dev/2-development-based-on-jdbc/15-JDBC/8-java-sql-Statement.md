---
title: java.sql.Statement
summary: java.sql.Statement
author: Guo Huan
date: 2021-05-17
---

# java.sql.Statement

java.sql.Statement是SQL语句接口。

**表 1** 对java.sql.Statement的支持情况

| 方法名                       | 返回值类型 | 支持JDBC 4 |
| :--------------------------- | :--------- | :--------- |
| close()                      | void       | Yes        |
| execute(String sql)          | Boolean    | Yes        |
| executeQuery(String sql)     | ResultSet  | Yes        |
| executeUpdate(String sql)    | int        | Yes        |
| getConnection()              | Connection | Yes        |
| getResultSet()               | ResultSet  | Yes        |
| getQueryTimeout()            | int        | Yes        |
| getUpdateCount()             | int        | Yes        |
| isClosed()                   | Boolean    | Yes        |
| setQueryTimeout(int seconds) | void       | Yes        |
| setFetchSize(int rows)       | void       | Yes        |
| cancel()                     | void       | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 通过setFetchSize可以减少结果集在客户端的内存占用情况。它的原理是通过将结果集打包成游标，然后分段处理，所以会加大数据库与客户端的通信量，会有性能损耗。
> - 由于数据库游标是事务内有效，所以，在设置setFetchSize的同时，需要将连接设置为非自动提交模式，setAutoCommit(false)。同时在业务数据需要持久化到数据库中时，在连接上执行提交操作。
