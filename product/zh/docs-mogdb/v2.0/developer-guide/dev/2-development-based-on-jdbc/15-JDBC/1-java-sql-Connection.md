---
title: java.sql.Connection
summary: java.sql.Connection
author: Guo Huan
date: 2021-05-17
---

# java.sql.Connection

java.sql.Connection是数据库连接接口。

**表 1**  对java.sql.Connection接口的支持情况

| 方法名                                  | 返回值类型        | 支持JDBC 4 |
| :-------------------------------------- | :---------------- | :--------- |
| close()                                 | void              | Yes        |
| commit()                                | void              | Yes        |
| createStatement()                       | Statement         | Yes        |
| getAutoCommit()                         | Boolean           | Yes        |
| getClientInfo()                         | Properties        | Yes        |
| getClientInfo(String name)              | String            | Yes        |
| getTransactionIsolation()               | int               | Yes        |
| isClosed()                              | Boolean           | Yes        |
| isReadOnly()                            | Boolean           | Yes        |
| prepareStatement(String sql)            | PreparedStatement | Yes        |
| rollback()                              | void              | Yes        |
| setAutoCommit(boolean autoCommit)       | void              | Yes        |
| setClientInfo(Properties properties)    | void              | Yes        |
| setClientInfo(String name,String value) | void              | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
> 接口内部默认使用自动提交模式，若通过setAutoCommit(false)关闭自动提交，将会导致后面执行的语句都受到显式事务包裹，数据库中不支持事务中执行的语句不能在此模式下执行。
