---
title: java.sql.ResultSet
summary: java.sql.ResultSet
author: Guo Huan
date: 2021-05-17
---

# java.sql.ResultSet

java.sql.ResultSet是执行结果集接口。

**表 1** 对java.sql.ResultSet的支持情况

| 方法名                            | 返回值类型 | 支持JDBC 4 |
| :-------------------------------- | :--------- | :--------- |
| findColumn(String columnLabel)    | int        | Yes        |
| getBigDecimal(int columnIndex)    | BigDecimal | Yes        |
| getBigDecimal(String columnLabel) | BigDecimal | Yes        |
| getBoolean(int columnIndex)       | Boolean    | Yes        |
| getBoolean(String columnLabel)    | Boolean    | Yes        |
| getByte(int columnIndex)          | byte       | Yes        |
| getBytes(int columnIndex)         | byte[]     | Yes        |
| getByte(String columnLabel)       | byte       | Yes        |
| getBytes(String columnLabel)      | byte[]     | Yes        |
| getDate(int columnIndex)          | Date       | Yes        |
| getDate(String columnLabel)       | Date       | Yes        |
| getDouble(int columnIndex)        | double     | Yes        |
| getDouble(String columnLabel)     | double     | Yes        |
| getFloat(int columnIndex)         | float      | Yes        |
| getFloat(String columnLabel)      | float      | Yes        |
| getInt(int columnIndex)           | int        | Yes        |
| getInt(String columnLabel)        | int        | Yes        |
| getLong(int columnIndex)          | long       | Yes        |
| getLong(String columnLabel)       | long       | Yes        |
| getShort(int columnIndex)         | short      | Yes        |
| getShort(String columnLabel)      | short      | Yes        |
| getString(int columnIndex)        | String     | Yes        |
| getString(String columnLabel)     | String     | Yes        |
| getTime(int columnIndex)          | Time       | Yes        |
| getTime(String columnLabel)       | Time       | Yes        |
| getTimestamp(int columnIndex)     | Timestamp  | Yes        |
| getTimestamp(String columnLabel)  | Timestamp  | Yes        |
| isAfterLast()                     | Boolean    | Yes        |
| isBeforeFirst()                   | Boolean    | Yes        |
| isFirst()                         | Boolean    | Yes        |
| next()                            | Boolean    | Yes        |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 一个Statement不能有多个处于"open"状态的ResultSet。
> - 用于遍历结果集(ResultSet)的游标(Cursor)在被提交后不能保持“open”的状态。
