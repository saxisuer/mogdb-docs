---
title: javax.sql.PooledConnection
summary: javax.sql.PooledConnection
author: Guo Huan
date: 2021-05-17
---

# javax.sql.PooledConnection

javax.sql.PooledConnection是由连接池创建的连接接口。

**表 1** 对javax.sql.PooledConnection的支持情况

| 方法名                                                       | 返回值类型 | 支持JDBC 4 |
| :----------------------------------------------------------- | :--------- | :--------- |
| addConnectionEventListener (ConnectionEventListener listener) | void       | Yes        |
| close()                                                      | void       | Yes        |
| getConnection()                                              | Connection | Yes        |
| removeConnectionEventListener (ConnectionEventListener listener) | void       | Yes        |
| addStatementEventListener (StatementEventListener listener)  | void       | Yes        |
| removeStatementEventListener (StatementEventListener listener) | void       | Yes        |
