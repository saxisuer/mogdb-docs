---
title: 连接数据库
summary: 连接数据库
author: Guo Huan
date: 2021-04-26
---

# 连接数据库

在创建数据库连接之后，才能使用它来执行SQL语句操作数据。

<br/>

## 函数原型

JDBC提供了三个方法，用于创建数据库连接。

- DriverManager.getConnection(String url);
- DriverManager.getConnection(String url, Properties info);
- DriverManager.getConnection(String url, String user, String password);

<br/>

## 参数

**表 1** 数据库连接参数

| 参数     | 描述        |
| :------- | :-------------- |
| url      | postgresql.jar数据库连接描述符。格式如下: <br />- jdbc:postgresql:database<br />- jdbc:postgresql://host/database<br />- jdbc:postgresql://host:port/database<br />- jdbc:postgresql://host:port/database?param1=value1&param2=value2<br />- jdbc:postgresql://host1:port1,host2:port2/database?param1=value1&param2=value2<br />说明:<br />- database为要连接的数据库名称。<br />- host为数据库服务器名称或IP地址。<br />连接MogDB的机器与MogDB不在同一网段时，host指定的IP地址应为Manager界面上所设的coo.cooListenIp2（应用访问IP）的取值。<br />由于安全原因，数据库主节点禁止MogDB内部其他节点无认证接入。如果要在MogDB内部访问数据库主节点，请将JDBC程序部署在数据库主节点所在机器，host使用“127.0.0.1”。否则可能会出现"FATAL: Forbid remote connection with trust method!"错误。<br />建议业务系统单独部署在MogDB外部，否则可能会影响数据库运行性能。<br />缺省情况下，连接服务器为localhost。<br />- port为数据库服务器端口。<br />缺省情况下，会尝试连接到5432端口的database。<br />- param为参数名称，即数据库连接属性。<br />参数可以配置在URL中，以“?”开始配置，以“=”给参数赋值，以“&”作为不同参数的间隔。也可以采用info对象的属性方式进行配置，详细示例会在本节给出。<br />- value为参数值，即数据库连接属性值。<br />连接时需配置connectTimeout，socketTimeout，如果未配置，默认为0，即不会超时。在DN与客户端出现网络故障时，客户端一直未收到DN侧ACK确认报文，会启动超时重传机制，不断的进行重传。当超时时间达到系统默认的600s后才会报超时错误，这也就会导致RTO时间很高。<br />- 连接时需配置connectTimeout，socketTimeout，如果未配置，默认为0，即不会超时。在DN与客户端出现网络故障时，客户端一直未收到DN侧ACK确认报文，会启动超时重传机制，不断的进行重传。当超时时间达到系统默认的600s后才会报超时错误，这也就会导致RTO时间很高。 |
| info     | 数据库连接属性（所有属性大小写敏感）。常用的属性如下: <br />- PGDBNAME: String类型。表示数据库名称。（URL中无需配置该参数，自动从URL中解析）<br />- PGHOST: String类型。主机IP地址。详细示例见下。<br />- PGPORT: Integer类型。主机端口号。详细示例见下。<br />- user: String类型。表示创建连接的数据库用户。<br />- password: String类型。表示数据库用户的密码。<br />- enable_ce：String类型。其中enable_ce=1表示JDBC支持密态等值查询。<br />- loggerLevel: String类型。目前支持3种级别: OFF、DEBUG、TRACE。设置为OFF关闭日志，设置为DEBUG和TRACE记录的日志信息详细程度不同。<br />- loggerFile: String类型。Logger输出的文件名。需要显示指定日志文件名，若未指定目录则生成在客户端运行程序目录。<br />- allowEncodingChanges: Boolean类型。设置该参数值为“true”进行字符集类型更改，配合characterEncoding=CHARSET设置字符集，二者使用“&”分隔。<br />- currentSchema: String类型。在search-path中指定要设置的schema。<br />- hostRecheckSeconds: Integer类型。JDBC尝试连接主机后会保存主机状态: 连接成功或连接失败。在hostRecheckSeconds时间内保持可信，超过则状态失效。缺省值是10秒。<br />- ssl: Boolean类型。以SSL方式连接。<br />ssl=true可支持NonValidatingFactory通道和使用证书的方式: <br />1、NonValidatingFactory通道需要配置用户名和密码，同时将SSL设置为true。<br />2、配置客户端证书、密钥、根证书，将SSL设置为true。<br />- sslmode: String类型。SSL认证方式。取值范围为: require、verify-ca、verify-full。<br />    - require只尝试SSL连接，如果存在CA文件，则应设置成verify-ca的方式验证。<br />    - verify-ca只尝试SSL连接，并且验证服务器是否具有由可信任的证书机构签发的证书。<br />    - verify-full只尝试SSL连接，并且验证服务器是否具有由可信任的证书机构签发的证书，以及验证服务器主机名是否与证书中的一致。<br />- sslcert: String类型。提供证书文件的完整路径。客户端和服务端证书的类型为End Entity。<br />- sslkey: String类型。提供密钥文件的完整路径。使用时将客户端证书转换为DER格式: <br />*openssl pkcs8 -topk8 -outform DER -in client.key -out client.key.pk8 -nocrypt*<br />- sslrootcert: String类型。SSL根证书的文件名。根证书的类型为CA。<br />- sslpassword: String类型。提供给ConsoleCallbackHandler使用。<br />- sslpasswordcallback: String类型。SSL密码提供者的类名。缺省值: org.postgresql.ssl.jdbc4.LibPQFactory.ConsoleCallbackHandler。<br />- sslfactory: String类型。提供的值是SSLSocketFactory在建立SSL连接时用的类名。<br />- sslfactoryarg: String类型。此值是上面提供的sslfactory类的构造函数的可选参数（不推荐使用）。<br />- sslhostnameverifier: String类型。主机名验证程序的类名。接口实现javax.net.ssl.HostnameVerifier，默认使用org.postgresql.ssl.PGjdbcHostnameVerifier。<br />- loginTimeout: Integer类型。指建立数据库连接的等待时间。超时时间单位为秒。<br />- connectTimeout: Integer类型。用于连接服务器操作的超时值。如果连接到服务器花费的时间超过此值，则连接断开。超时时间单位为秒，值为0时表示已禁用，timeout不发生。<br />- socketTimeout: Integer类型。用于socket读取操作的超时值。如果从服务器读取所花费的时间超过此值，则连接关闭。超时时间单位为秒，值为0时表示已禁用，timeout不发生。<br />- cancelSignalTimeout: Integer类型。发送取消消息本身可能会阻塞，此属性控制用于取消命令的"connect超时"和"socket超时"。超时时间单位为秒，默认值为10秒。<br />- tcpKeepAlive: Boolean类型。启用或禁用TCP保活探测功能。默认为false。<br />- logUnclosedConnections: Boolean类型。客户端可能由于未调用Connection对象的close()方法而泄漏Connection对象。最终这些对象将被垃圾回收，并且调用finalize()方法。如果调用者自己忽略了此操作，该方法将关闭Connection。<br />- assumeMinServerVersion: String类型。客户端会发送请求进行float精度设置。该参数设置要连接的服务器版本，如assumeMinServerVersion=9.0，可以在建立时减少相关包的发送。<br />- ApplicationName: String类型。设置正在使用连接的JDBC驱动的名称。通过在数据库主节点上查询pg_stat_activity表可以看到正在连接的客户端信息，JDBC驱动名称显示在application_name列。缺省值为PostgreSQL JDBC Driver。<br />- connectionExtraInfo: Boolean类型。表示驱动是否上报当前驱动的部署路径、进程属主用户到数据库。取值范围: true或false，默认值为false。设置connectionExtraInfo为true，JDBC驱动会将当前驱动的部署路径、进程属主用户、url连接配置信息上报到数据库中，记录在connection_info参数里；同时可以在PG_STAT_ACTIVITY中查询到。<br />- autosave: String类型。共有3种: “always”, “never”, “conservative”。如果查询失败，指定驱动程序应该执行的操作。在autosave=always模式下，JDBC驱动程序在每次查询之前设置一个保存点，并在失败时回滚到该保存点。在autosave=never模式（默认）下，无保存点。在autosave=conservative模式下，每次查询都会设置保存点，但是只会在"statement XXX无效"等情况下回滚并重试。<br />- protocolVersion: Integer类型。连接协议版本号，目前仅支持3。注意:设置该参数时将采用md5加密方式，需要同步修改数据库的加密方式: gs_guc set -N all -I all -c "password_encryption_type=1" ，重启MogDB生效后需要创建用md5方式加密口令的用户。同时修改pg_hba.conf，将客户端连接方式修改为md5。用新建用户进行登录（不推荐）。<br />说明：<br />MD5加密算法安全性低，存在安全风险，建议使用更安全的加密算法。<br />- prepareThreshold: Integer类型。控制parse语句何时发送。默认值是5。第一次parse一个SQL比较慢，后面再parse就会比较快，因为有缓存了。如果一个会话连续多次执行同一个SQL，在达到prepareThreshold次数以上时，JDBC将不再对这个SQL发送parse命令。<br />- preparedStatementCacheQueries: Integer类型。确定每个连接中缓存的查询数，默认情况下是256。若在prepareStatement()调用中使用超过256个不同的查询，则最近最少使用的查询缓存将被丢弃。0表示禁用缓存。<br />- preparedStatementCacheSizeMiB: Integer类型。确定每个连接可缓存的最大值（以兆字节为单位），默认情况下是5。若缓存了超过5MB的查询，则最近最少使用的查询缓存将被丢弃。0表示禁用缓存。<br />- databaseMetadataCacheFields: Integer类型。默认值是65536。指定每个连接可缓存的最大值。"0"表示禁用缓存。<br />- databaseMetadataCacheFieldsMiB: Integer类型。默认值是5。每个连接可缓存的最大值，单位是MB。"0"表示禁用缓存。<br />- stringtype: String类型，可选字段为: false, "unspecified", "varchar"。设置通过setString()方法使用的PreparedStatement参数的类型，如果stringtype设置为VARCHAR（默认值），则这些参数将作为varchar参数发送给服务器。若stringtype设置为unspecified，则参数将作为untyped值发送到服务器，服务器将尝试推断适当的类型。<br />- batchMode: Boolean类型。用于确定是否使用batch模式连接。默认值为on，表示开启batch模式。<br />- fetchsize: Integer类型。用于设置数据库连接所创建statement的默认fetchsize。默认值为0，表示一次获取所有结果<br />- reWriteBatchedInserts: Boolean类型。批量导入时，该参数设置为on，可将N条插入语句合并为一条: insert into TABLE_NAME values(values1, …, valuesN), …, (values1, …, valuesN);使用该参数时，需设置batchMode=off。<br />- unknownLength: Integer类型，默认为Integer.MAX_VALUE。某些postgresql类型（例如TEXT）没有明确定义的长度，当通过ResultSetMetaData.getColumnDisplaySize和ResultSetMetaData.getPrecision等函数返回关于这些类型的数据时，此参数指定未知长度类型的长度。<br />- defaultRowFetchSize: Integer类型。确定一次fetch在ResultSet中读取的行数。限制每次访问数据库时读取的行数可以避免不必要的内存消耗，从而避免OutOfMemoryException。缺省值是0，这意味着ResultSet中将一次获取所有行。没有负数。<br />- binaryTransfer: Boolean类型。使用二进制格式发送和接收数据，默认值为"false"。<br />- binaryTransferEnable: String类型。启用二进制传输的类型列表，以逗号分隔。OID编号和名称二选一，例如binaryTransferEnable=Integer4_ARRAY,Integer8_ARRAY。比如: OID名称为BLOB，编号为88，可以如下配置: <br />binaryTransferEnable=BLOB 或 binaryTransferEnable=88<br />- binaryTransferDisEnable: String类型。禁用二进制传输的类型列表，以逗号分隔。OID编号和名称二选一。覆盖binaryTransferEnable的设置。<br />- blobMode: String类型。用于设置setBinaryStream方法为不同类型的数据赋值，设置为on时表示为blob类型数据赋值，设置为off时表示为bytea类型数据赋值，默认为on。<br />- socketFactory: String类型。用于创建与服务器socket连接的类的名称。该类必须实现了接口"javax.net.SocketFactory"，并定义无参或单String参数的构造函数。<br />- socketFactoryArg: String类型。此值是上面提供的socketFactory类的构造函数的可选参数，不推荐使用。<br />- receiveBufferSize: Integer类型。该值用于设置连接流上的SO_RCVBUF。<br />- sendBufferSize: Integer类型。该值用于设置连接流上的SO_SNDBUF。<br />- preferQueryMode: String类型。共有4种: “extended”, “extendedForPrepared”, “extendedCacheEverything”和 “simple”。用于指定执行查询的模式，simple模式会excute，不parse和bind；extended模式会bind和excute；extendedForPrepared模式为prepared statement扩展使用；extendedCacheEverything模式会缓存每个statement。<br />- targetServerType: String类型。 该参数识别主备数据节点是通过查询URL连接串中，数据节点是否允许写操作来实现的，默认为”any”。共有四种：”any”，”master”，”slave”，”preferSlave”：<br />  - master则尝试连接到URL连接串中的主节点，如果找不到就抛出异常。<br />  - slave则尝试连接到URL连接串中的备节点，如果找不到就抛出异常。<br />  - preferSlave则尝试连接到URL连接串中的备数据节点（如果有可用的话），否则连接到主数据节点。<br />  - any则尝试连接URL连接串中的任何一个数据节点。<br />- priorityServers：Integer类型。此值用于指定url上配置的前n个节点作为主集群被优先连接。默认值为null。该值为数字，大于0，且小于url上配置的DN数量。<br />例如：jdbc:postgresql://host1:port1,host2:port2,host3:port3,host4:port4,/database?priorityServers=2。即表示host1与host2为主集群节点，host3与host4为容灾集群节点。<br />- forceTargetServerSlave：Boolean类型。此值用于控制是否开启强制连接备机功能，并在集群发生主备切换时，禁止已存在的连接在升主备机上继续使用。默认值为false，表示不开启强制连接备机功能。true，表示开启强制连接备机功能。 |

<br/>

## 示例

```java
//以下代码将获取数据库连接操作封装为一个接口，可通过给定用户名和密码来连接数据库。
public static Connection getConnect(String username, String passwd)
    {
        //驱动类。
        String driver = "org.postgresql.Driver";
        //数据库连接描述符。
        String sourceURL = "jdbc:postgresql://10.10.0.13:8000/postgres";
        Connection conn = null;

        try
        {
            //加载驱动。
            Class.forName(driver);
        }
        catch( Exception e )
        {
            e.printStackTrace();
            return null;
        }

        try
        {
             //创建连接。
            conn = DriverManager.getConnection(sourceURL, username, passwd);
            System.out.println("Connection succeed!");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return conn;
    };
// 以下代码将使用Properties对象作为参数建立连接
public static Connection getConnectUseProp(String username, String passwd)
    {
        //驱动类。
        String driver = "org.postgresql.Driver";
        //数据库连接描述符。
        String sourceURL = "jdbc:postgresql://10.10.0.13:8000/postgres?";
        Connection conn = null;
        Properties info = new Properties();

        try
        {
            //加载驱动。
            Class.forName(driver);
        }
        catch( Exception e )
        {
            e.printStackTrace();
            return null;
        }

        try
        {
             info.setProperty("user", username);
             info.setProperty("password", passwd);
             //创建连接。
             conn = DriverManager.getConnection(sourceURL, info);
             System.out.println("Connection succeed!");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return conn;
    };
```
