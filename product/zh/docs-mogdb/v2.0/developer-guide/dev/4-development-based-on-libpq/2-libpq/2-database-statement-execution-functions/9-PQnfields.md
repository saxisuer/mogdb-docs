---
title: PQnfields
summary: PQnfields
author: Guo Huan
date: 2021-05-17
---

# PQnfields

## 功能描述

返回查询结果中每一行的列（域）数。

## 原型

```
int PQnfields(const PGresult *res);
```

## 参数

**表 1** PQnfields参数

| **关键字** | **参数说明**   |
| :--------- | :------------- |
| res        | 操作结果句柄。 |

## 返回值

int类型数字。

## 示例

参见：示例
