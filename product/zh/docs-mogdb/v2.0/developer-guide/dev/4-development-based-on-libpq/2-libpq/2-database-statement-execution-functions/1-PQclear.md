---
title: PQclear
summary: PQclear
author: Guo Huan
date: 2021-05-17
---

# PQclear

## 功能描述

释放与PGresult相关联的存储空间，任何不再需要的查询结果都应该用PQclear释放掉。

## 原型

```
void PQclear(PGresult *res);
```

## 参数

**表 1** PQclear参数

| **关键字** | **参数说明**             |
| :--------- | :----------------------- |
| res        | 包含查询结果的对象指针。 |

## 注意事项

PGresult不会自动释放，当提交新的查询时它并不消失，甚至断开连接后也不会。要删除它，必须调用PQclear，否则则会有内存泄漏。

## 示例

请参见示例章节。
