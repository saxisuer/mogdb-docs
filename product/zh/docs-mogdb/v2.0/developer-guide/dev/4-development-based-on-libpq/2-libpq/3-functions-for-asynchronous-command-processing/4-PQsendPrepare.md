---
title: PQsendPrepare
summary: PQsendPrepare
author: Guo Huan
date: 2021-05-17
---

# PQsendPrepare

## 功能描述

发送一个请求，创建一个给定参数的预备语句，而不等待结束。

## 原型

```
int PQsendPrepare(PGconn *conn,
                  const char *stmtName,
                  const char *query,
                  int nParams,
                  const Oid *paramTypes);
```

## 参数

**表 1** PQsendPrepare参数

| **关键字** | **参数说明**             |
| :--------- | :----------------------- |
| conn       | 指向包含链接的对象指针。 |
| stmtName   | 需要执行的*stmt*名称。   |
| query      | 需要执行的查询字符串。   |
| nParams    | 参数个数。               |
| paramTypes | 声明参数类型的数组。     |

## 返回值

int：执行结果为1表示成功，0表示失败，失败原因存到conn-&gt;errorMessage中。

## 注意事项

该函数为PQprepare的异步版本：如果能够分派请求，则返回1，否则返回0。调用成功后，调用PQgetResult判断服务端是否成功创建了preparedStatement。函数的参数与PQprepare一样处理。与PQprepare一样，它也不能在2.0协议的连接上工作。

## 示例

请参见示例章节。
