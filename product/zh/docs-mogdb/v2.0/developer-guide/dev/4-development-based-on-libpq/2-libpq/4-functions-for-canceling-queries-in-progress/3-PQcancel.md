---
title: PQcancel
summary: PQcancel
author: Guo Huan
date: 2021-05-17
---

# PQcancel

## 功能描述

要求服务器放弃处理当前命令。

## 原型

```
int PQcancel(PGcancel *cancel, char *errbuf, int errbufsize);
```

## 参数

**表 1** PQcancel参数

| **关键字** | **参数说明**                   |
| :--------- | :----------------------------- |
| cancel     | 指向包含cancel信息的对象指针。 |
| errbuf     | 出错保存错误信息的buffer。     |
| errbufsize | 保存错误信息的buffer大小。     |

## 返回值

int：执行结果为1表示成功，0表示失败，失败原因存到errbuf中。

## 注意事项

- 成功发送并不保证请求将产生任何效果。如果取消有效，当前命令将提前终止并返回错误结果。如果取消失败（例如，因为服务器已经处理完命令），无返回结果。
- 如果errbuf是信号处理程序中的局部变量，则可以安全地从信号处理程序中调用PQcancel。就PQcancel而言，PGcancel对象是只读的，因此它也可以从一个线程中调用，这个线程与操作PGconn对象线程是分离的。

## 示例

请参见示例章节。
