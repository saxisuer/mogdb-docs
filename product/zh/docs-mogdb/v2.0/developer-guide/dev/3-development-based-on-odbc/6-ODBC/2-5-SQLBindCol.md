---
title: SQLBindCol
summary: SQLBindCol
author: Guo Huan
date: 2021-05-17
---

# SQLBindCol

## 功能描述

将应用程序数据缓冲区绑定到结果集的列中。

## 原型

```
SQLRETURN SQLBindCol(SQLHSTMT       StatementHandle,
                     SQLUSMALLINT   ColumnNumber,
                     SQLSMALLINT    TargetType,
                     SQLPOINTER     TargetValuePtr,
                     SQLLEN     BufferLength,
                     SQLLEN     *StrLen_or_IndPtr);
```

## 参数

**表 1**  SQLBindCol参数

| **关键字**       | **参数说明**                                                 |
| :--------------- | :----------------------------------------------------------- |
| StatementHandle  | 语句句柄。                                                   |
| ColumnNumber     | 要绑定结果集的列号。起始列号为0，以递增的顺序计算列号，第0列是书签列。若未设置书签页，则起始列号为1。 |
| TargetType       | 缓冲区中C数据类型的标识符。                                  |
| TargetValuePtr   | **输出参数**：指向与列绑定的数据缓冲区的指针。SQLFetch函数返回这个缓冲区中的数据。如果此参数为一个空指针，则StrLen_or_IndPtr是一个有效值。 |
| BufferLength     | TargetValuePtr指向缓冲区的长度，以字节为单位。               |
| StrLen_or_IndPtr | **输出参数**：缓冲区的长度或指示器指针。若为空值，则未使用任何长度或指示器值。 |

## 返回值

- SQL_SUCCESS：表示调用正确。
- SQL_SUCCESS_WITH_INFO：表示会有一些警告信息。
- SQL_ERROR：表示比较严重的错误，如：内存分配失败、建立连接失败等。
- SQL_INVALID_HANDLE：表示调用无效句柄。其他API的返回值同理。

## 注意事项

当SQLBindCol返回SQL_ERROR或SQL_SUCCESS_WITH_INFO时，通过调用SQLGetDiagRec函数，并将HandleType和Handle参数设置为SQL_HANDLE_STMT和StatementHandle，可得到一个相关的SQLSTATE值，通过SQLSTATE值可以查出调用此函数的具体信息。

## 示例

参见：ODBC -  示例
