---
title: SQLExecDirect
summary: SQLExecDirect
author: Guo Huan
date: 2021-05-17
---

# SQLExecDirect

## 功能描述

使用参数的当前值，执行一条准备好的语句。对于一次只执行一条SQL语句，SQLExecDirect是最快的执行方式。

## 原型

```
SQLRETURN SQLExecDirect(SQLHSTMT         StatementHandle,
                        SQLCHAR         *StatementText,
                        SQLINTEGER       TextLength);
```

## 参数

**表 1** SQLExecDirect参数

| **关键字**      | **参数说明**                              |
| :-------------- | :---------------------------------------- |
| StatementHandle | 语句句柄，通过SQLAllocHandle获得。        |
| StatementText   | 要执行的SQL语句。不支持一次执行多条语句。 |
| TextLength      | StatementText的长度。                     |

## 返回值

- SQL_SUCCESS：表示调用正确。
- SQL_SUCCESS_WITH_INFO：表示会有一些警告信息。
- SQL_NEED_DATA：在执行SQL语句前没有提供足够的参数。
- SQL_ERROR：表示比较严重的错误，如：内存分配失败、建立连接失败等。
- SQL_INVALID_HANDLE：表示调用无效句柄。其他API的返回值同理。
- SQL_STILL_EXECUTING：表示语句正在执行。
- SQL_NO_DATA：表示SQL语句不返回结果集。

## 注意事项

当调用SQLExecDirect函数返回SQL_ERROR或SQL_SUCCESS_WITH_INFO时，通过调用SQLGetDiagRec函数，并将HandleType和Handle参数设置为SQL_HANDLE_STMT和StatementHandle，可得到一个相关的SQLSTATE值，通过SQLSTATE值可以查出调用此函数的具体信息。

## 示例

参见：ODBC - 示例
