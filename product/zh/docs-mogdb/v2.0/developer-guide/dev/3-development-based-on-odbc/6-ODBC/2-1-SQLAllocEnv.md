---
title: SQLAllocEnv
summary: SQLAllocEnv
author: Guo Huan
date: 2021-05-17
---

# SQLAllocEnv

在ODBC 3.x版本中，ODBC 2.x的函数SQLAllocEnv已被SQLAllocHandle代替。有关详细信息请参阅SQLAllocHandle。
