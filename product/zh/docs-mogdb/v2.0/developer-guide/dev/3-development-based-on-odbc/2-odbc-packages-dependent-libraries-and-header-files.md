---
title: ODBC包及依赖的库和头文件
summary: ODBC包及依赖的库和头文件
author: Guo Huan
date: 2021-04-26
---

# ODBC包及依赖的库和头文件

<br/>

## Linux下的ODBC包

从发布包中获取，包名为[openGauss-x.x.x-ODBC.tar.gz](https://opengauss.org/zh/download/)。Linux环境下，开发应用程序要用到unixODBC提供的头文件（sql.h、sqlext.h等）和库libodbc.so。这些头文件和库可从unixODBC-2.3.0的安装包中获得。
