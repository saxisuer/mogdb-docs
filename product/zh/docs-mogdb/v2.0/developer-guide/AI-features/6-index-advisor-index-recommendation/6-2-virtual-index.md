---
title: 虚拟索引
summary: 虚拟索引
author: Guo Huan
date: 2021-05-19
---

# 虚拟索引

虚拟索引功能支持用户在数据库中直接进行操作，本功能将模拟真实索引的建立，避免真实索引创建所需的时间和空间开销，用户基于虚拟索引，可通过优化器评估该索引对指定查询语句的代价影响。

本功能涉及的系统函数接口如下表所示：

**表 1** 虚拟索引功能的接口

| 函数名               | 参数                 | 功能                             |
| :------------------- | :------------------- | :------------------------------- |
| hypopg_create_index  | 创建索引语句的字符串 | 创建虚拟索引。                   |
| hypopg_display_index | 无                   | 显示所有创建的虚拟索引信息。     |
| hypopg_drop_index    | 索引的oid            | 删除指定的虚拟索引。             |
| hypopg_reset_index   | 无                   | 清除所有虚拟索引。               |
| hypopg_estimate_size | 索引的oid            | 估计指定索引创建所需的空间大小。 |

本功能涉及的GUC参数如下：

**表 2** 虚拟索引功能的GUC参数

| 参数名            | 功能                 | 默认值 |
| :---------------- | :------------------- | :----- |
| enable_hypo_index | 是否开启虚拟索引功能 | off    |

<br/>

## 使用步骤

1. 使用函数hypopg_create_index创建虚拟索引。例如：

   ```sql
   mogdb=> select * from hypopg_create_index('create index on bmsql_customer(c_w_id)');
    indexrelid |              indexname
   ------------+-------------------------------------
        329726 | <329726>btree_bmsql_customer_c_w_id
   (1 row)
   ```

2. 开启GUC参数enable_hypo_index，该参数控制数据库的优化器进行EXPLAIN时是否考虑创建的虚拟索引。通过对特定的查询语句执行explain，用户可根据优化器给出的执行计划评估该索引是否能够提升该查询语句的执行效率。例如：

   ```sql
   mogdb=> set enable_hypo_index = on;
   SET
   ```

   开启GUC参数前，执行EXPLAIN + 查询语句：

   ```sql
   mogdb=> explain SELECT c_discount from bmsql_customer where c_w_id = 10;
                                 QUERY PLAN
   ----------------------------------------------------------------------
    Seq Scan on bmsql_customer  (cost=0.00..52963.06 rows=31224 width=4)
      Filter: (c_w_id = 10)
   (2 rows)
   ```

   开启GUC参数后，执行EXPLAIN + 查询语句：

   ```sql
   mogdb=> explain SELECT c_discount from bmsql_customer where c_w_id = 10;
                                                       QUERY PLAN
   ------------------------------------------------------------------------------------------------------------------
    [Bypass]
    Index Scan using <329726>btree_bmsql_customer_c_w_id on bmsql_customer  (cost=0.00..39678.69 rows=31224 width=4)
      Index Cond: (c_w_id = 10)
   (3 rows)
   ```

   通过对比两个执行计划可以观察到，该索引预计会降低指定查询语句的执行代价，用户可考虑创建对应的真实索引。

3. （可选）使用函数hypopg_display_index展示所有创建过的虚拟索引。例如：

   ```sql
   mogdb=> select * from hypopg_display_index();
                    indexname                  | indexrelid |     table      |      column
   --------------------------------------------+------------+----------------+------------------
    <329726>btree_bmsql_customer_c_w_id        |     329726 | bmsql_customer | (c_w_id)
    <329729>btree_bmsql_customer_c_d_id_c_w_id |     329729 | bmsql_customer | (c_d_id, c_w_id)
   (2 rows)
   ```

4. （可选）使用函数hypopg_estimate_size估计虚拟索引创建所需的空间大小（单位：字节）。例如：

   ```sql
   mogdb=> select * from hypopg_estimate_size(329730);
    hypopg_estimate_size
   ----------------------
                15687680
   (1 row)
   ```

5. 删除虚拟索引。

   使用函数hypopg_drop_index删除指定oid的虚拟索引。例如：

   ```sql
   mogdb=> select * from hypopg_drop_index(329726);
    hypopg_drop_index
   -------------------
    t
   (1 row)
   ```

   使用函数hypopg_reset_index一次性清除所有创建的虚拟索引。例如：

   ```sql
   mogdb=> select * from hypopg_reset_index();
    hypopg_reset_index
   --------------------

   (1 row)
   ```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 执行EXPLAIN ANALYZE不会涉及虚拟索引功能。
> - 创建的虚拟索引是数据库实例级别的，各个会话（session)之间可共享设置，关闭会话后虚拟索引仍可存在，但是重启数据库后将被清空。
