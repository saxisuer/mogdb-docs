---
title: 概述
summary: 概述
author: Guo Huan
date: 2021-05-19
---

# 概述

数据库DeepSQL特性实现DB4AI功能，即在数据库内实现AI算法，以更好的支撑大数据的快速分析和计算。这里提供了一整套基于SQL的机器学习、数据挖掘以及统计学的算法，用户可以直接使用SQL进行机器学习工作。Deep SQL能够抽象出端到端从数据到模型的研发过程，配合底层的引擎及自动优化，具备基础SQL知识的技术人员即可完成大部分的机器学习模型训练及预测任务。整个分析和处理都运行在数据库引擎中，用户可以直接分析和处理数据库内的数据，不需要在数据库和其它平台之间进行数据传递，避免在多个环境之间进行不必要地数据移动。

DeepSQL是对MogDB DB4AI能力的增强。DeepSQL将常用的机器学习算法封装为SQL语句，支持60多个常用算法。其中包括回归算法（例如线性回归，逻辑回归，随机森林等）、分类算法（比如KNN等）、聚类算法（比如K-means）等。除了基础的机器学习算法之外，还包括图相关的算法，比如最短路径，图形直径等等算法；此外还支持数据处理（比如PCA），稀疏向量，统计学常用算法（比如协方差，Pearson系数计算等），训练集测试集分割方法，交叉验证方法等。

**表 1** 支持的机器学习算法 - 回归类算法

| 算法中文名称    | 算法英文名称                        | 应用场景                                                     |
| :-------------- | :---------------------------------- | :----------------------------------------------------------- |
| 逻辑回归        | Logistic Regression                 | 例如寻找某疾病的危险因素，金融商业机构需要对企业进行评估等。<br/>预测：根据模型预测不同的自变量情况下某病或某情况的发生概率。<br/>判别：实际上跟预测类似，也是根据模型判断某人属于某病或属于某种情况的概率有多大，即判断某人有多大可能是属于某病。 |
| Cox比例风险回归 | Cox Proportional Hazards Regression | 该模型以生存结局和生存时间为因变量，可同时分析众多因素对生存期的影响，能分析带有截尾生存时间的资料，且不要求估计资料的生存分布类型。由于上述优良性质，该模型自问世以来，在医学类研究中得到广泛的应用，是迄今生存分析中应用最多的多因素分析方法。 |
| 弹性网络回归    | Elastic Net Regularization          | 弹性回归是岭回归和套索回归的混合技术，它同时使用 L2 和 L1 正则化。当有多个相关的特征时，套索回归很可能随机选择其中一个，而弹性回归很可能都会选择。 |
| 广义线性模型    | Generalized Linear Models           | 在一些实际问题中，变量间的关系并不都是线性的，这种情况就应该用曲线去进行拟合。 |
| 边际效应        | Marginal Effects                    | 提供边际效应的计算。                                         |
| 多类回归        | Multinomial Regression              | 如果目标类别数超过两个，这时就需要使用多类回归，如疗效可能是“无效”，“显效”，“痊愈”三类。 |
| 序数回归        | Ordinal Regression                  | 在统计学中，序数回归是一种用于预测序数变量的回归分析，即其值存在于任意范围内的变量，不同值之间的度量距离也不同。它可以被认为是介于回归和分类之间的一类问题。例如，病情的分级（1、2、3、4级），症状的感觉分级（不痛、微痛、较痛和剧痛），对药物剂量反应的分级（无效、微效、中效和高效）等等。不同级别之间的差异不一定相等，如不痛与微痛的差值不一定等于较痛与剧痛的差值。 |
| 聚类方差        | Clustered Variance                  | Clustered Variance模块调整聚类的标准误差。例如，将一个数据集合复制100次，不应该增加参数估计的精度，但是在符合独立同分布假设（Independent Identically Distributed，IID）下执行这个过程实际上会提高精度。 |
| 稳健方差        | Robust Variance                     | Robust Variance模块中的函数用于计算线性回归、逻辑回归、多类逻辑回归和Cox比例风险回归的稳健方差（Huber-White估计）。它们可用于计算具有潜在噪声异常值的数据集中数据的差异。 |
| 支持向量机      | Support Vector Machines(SVM)        | 用于文本和超文本的分类、图像分类，比起传统的查询优化方案，支持向量机能够获取明显更高的搜索准确度。这同样也适用于图像分割系统。 |
| 线性回归        | Linear Regression                   | 应用广泛，例如经济学、金融学等。                             |

**表 2** 支持的机器学习算法 - 其他监督学习

| 算法名称（中文） | 算法名称（英文）               | 应用场景                                                     |
| :--------------- | :----------------------------- | :----------------------------------------------------------- |
| 决策树           | Decision Tree                  | 最为广泛的归纳推理算法之一，处理类别型或连续型变量的分类预测问题，可以用图形和if-then的规则表示模型，可读性较高。 |
| 随机森林         | Random Forest                  | 随机森林是一类专门为决策树分类器设计的组合方法。它组合多棵决策树作出的预测。 |
| 条件随机场       | Conditional Random Field (CRF) | 条件随机场（CRF）是一种判别的，无向概率的图形模型。线性链CRF是一种特殊类型的CRF，它假定当前状态仅取决于先前的状态。在分词、词性标注和命名实体识别等序列标注任务中取得了很好的效果。 |
| 朴素贝叶斯       | Naive Bayes                    | 通过计算概率来进行分类，可以用来处理多分类问题，比如：垃圾邮件过滤器。 |
| 神经网络         | Neural Networks                | 拥有广泛的应用场景，譬如语音识别、图像识别、机器翻译等等。在模式识别的领域中算是标准监督学习算法，并在计算神经学中，持续成为被研究的课题。MLP已被证明是一种通用的函数近似方法，可以被用来拟合复杂的函数或解决分类问题。 |
| k临近算法        | k-Nearest Neighbors            | K近邻分类方法通过计算每个训练样例到待分类样品的距离，取和待分类样品距离最近的K个训练样例，K个样品中哪个类别的训练样例占多数，则待分类元组就属于哪个类别。<br/>可用于：文字识别，面部识别，基因模式识别，客户流失预测、欺诈侦测。 |

**表 3** 支持的机器学习算法 - 数据处理类算法

| 算法名称（中文） | 算法名称（英文）               | 应用场景                                                     |
| :--------------- | :----------------------------- | :----------------------------------------------------------- |
| 数组操作         | Array Operations               | 数组、向量操作运算，包括基础的加减乘除、幂运算、开方、cos、sin、绝对值、方差等。 |
| 主成成分分析     | Dimensionality Reduction (PCA) | 降维，计算主成分。                                           |
| 变量编码         | Encoding Categorical Variables | 当前支持one-hot和dummy编码技术。<br/>当需要用一组特定的预测变量与其它预测变量组作比较时，通常使用哑编码（dummy coding），与之比较的变量组称为参照组。One-hot编码与哑编码类似，两者的区别是前者为每种分类值建立数字类型的0/1指示列。在每行数据中（对应一个数据点），只有一个分类编码列的值可以为1。 |
| 矩阵操作         | Matrix Operations              | 运用矩阵分解，将大型矩阵分解成简单矩阵的乘积形式，则可大大降低计算的难度以及计算量。矩阵加减乘除、最值、均值、求秩、求逆、矩阵分解(QR，LU，Cholesky)，特征提取。 |
| 规范化和距离函数 | Norms and Distance Functions   | 求范数，余弦相似度，向量间距离。                             |
| 稀疏向量         | Sparse Vectors                 | 实现稀疏向量类型，如果向量中重复值较多，可以用来压缩储存节省空间。 |
| 透视图           | Pivot                          | 透视表或枢轴表，通常用来实现OLAP或报表系统中一类常见的行列转置需求。pivot函数能够对一个表中存储的数据执行基本行转列操作，并将汇总后的结果输出到另一个表中。使行列转置操作变得更为简单与灵活。 |
| 模式匹配         | Path                           | 是在一系列行上执行常规模式匹配，并提取有关模式匹配的有用信息。有用的信息可以是简单的匹配计数或更多涉及的内容，如聚合或窗口函数。 |
| 会话             | Sessionize                     | 会话化功能对包括事件序列的数据集执行面向时间的会话重建。定义的不活动时段表示一个会话的结束和下一个会话的开始。<br/>可以用于：网络分析，网络安全，制造，财务和运营分析。 |
| 共轭梯度法       | Conjugate gradient             | 求解系数矩阵为对称正定矩阵的线性方程组的数值解的方法。       |
| 词干提取         | Stemming                       | 词干提取简单说就是找出单词中的词干部分，场景比如: 搜索引擎建立网页主题概念。在英文网站优化作用明显，对其他语言有借鉴意义。 |
| 训练集测试集分割 | Train-Test Split               | 分割数据集，把一份数据集划分成训练集和测试集，train的部分用于训练，test部分用于验证。 |
| 交叉验证         | Cross Validation               | 交叉验证。                                                   |
| 预测指标         | Prediction Metrics             | 用于评估模型预测的质量，包括均方误差，AUC值、混淆矩阵、修正R方等用于评价模型的函数。 |
| 小批量预处理     | Mini-Batch Preprocessor        | 把数据打包成小份进行训练，优点是它可以比随机梯度下降（默认MADlib优化器）表现更好，会更快更平滑的收敛。 |

**表 4** 支持的机器学习算法 - 图类

| 算法名称（中文）   | 算法名称（英文）                      | 应用场景                                                     |
| :----------------- | :------------------------------------ | :----------------------------------------------------------- |
| 所有对间最短路径   | All Pairs Shortest Path (APSP)        | 所有对最短路径（APSP）算法找到所有顶点对之间的最短路径的长度（总和权重），使得路径边缘的权重之和最小化。 |
| 广度优先算法       | Breadth-First Search                  | 广度优先算法遍历路径。                                       |
| 超链接诱导主题搜索 | Hyperlink-Induced Topic Search (HITS) | HITS算法输出每个节点的authority评分和hub评分，其中authority评分给出页面内容的分数，hub评估出连接到其他页面的分数。 |
| 平均路径长度       | Average Path Length                   | 此函数计算每对顶点之间的最短路径的平均值。平均路径长度基于“可到达的目标顶点”，因此它忽略了未连接的顶点之间的无限长度路径。 |
| 中心性             | Closeness Centrality                  | 接近度度量是和的倒数，平均值的倒数，以及到所有可到达目标顶点（不包括源顶点）的最短距离的倒数之和。 |
| 图表直径           | Graph Diameter                        | 直径被定义为图中所有最短路径中最长的。                       |
| 入度出度           | In-Out Degree                         | 计算图中每个点的入度出度，入度指指向此点的边的数量，出度指此点指向其他点的边的数量。 |
| 网页排名           | PageRank                              | 给定图形，给定图形，PageRank算法输出概率分布，该概率分布表示随机遍历图形的人将到达任何特定顶点的可能性PageRank算法输出概率分布，该概率分布表示随机遍历图形的人将到达任何特定顶点的可能性。 |
| 单源最短路径       | Single Source Shortest Path (SSSP)    | 给定图形和源顶点，单源最短路径（SSSP）算法找到从源顶点到图中的每个其他顶点的路径，使得路径边缘的权重之和最小化（每条边权值非负）。 |
| 弱连通分量         | Weakly Connected Component            | 给定有向图，弱连通分量（WCC）是原始图的子图，其中所有顶点通过某个路径彼此连接，忽略边的方向。在无向图的情况下，弱连通分量也是强连通分量。该模块还包括许多在WCC输出上运行的辅助函数。 |

**表 5** 支持的机器学习算法 - 时间序列

| 算法名称（中文）           | 算法名称（英文）                                      | 应用场景                                                     |
| :------------------------- | :---------------------------------------------------- | :----------------------------------------------------------- |
| 差分整合移动平均自回归模型 | Autoregressive Integrated Moving Average model(ARIMA) | 时间序列预测，用于理解和预测一系列数据的未来值。<br/>比如：国际航空旅客数据，预测旅客人数。 |

**表 6** 支持的机器学习算法 - 采样

| 算法名称（中文） | 算法名称（英文）    | 应用场景                                                     |
| :--------------- | :------------------ | :----------------------------------------------------------- |
| 采样函数         | sample              | 抽样。                                                       |
| 分层抽样         | Stratified Sampling | 分层随机抽样，又称类型随机抽样，它是先将总体各单位按一定标准分成各种类型（或层）；然后根据各类型单位数与总体单位数的比例，确定从各类型中抽取样本单位的数量；最后，按照随机原则从各类型中抽取样本。 |
| 对称抽样         | Balanced Sampling   | 一些分类算法仅在每个类中的样本数大致相同时才最佳地执行。高度偏斜的数据集在许多领域中是常见的（例如，欺诈检测），因此重新采样以抵消这种不平衡可以产生更好的决策边界。 |

**表 7** 支持的机器学习算法 - 统计学

| 算法名称（中文） | 算法名称（英文）                 | 应用场景                                                     |
| :--------------- | :------------------------------- | :----------------------------------------------------------- |
| 汇总统计函数     | Summary                          | 生成任何数据表的摘要统计信息。                               |
| 协方差和相关系数 | Correlation and Covariance       | 描述性统计，求Pearson系数，相关系数，另一个输出协方差。了解数据从统计学上反映的量的特征，以便我们更好地认识这些将要被挖掘的数据。 |
| 统计频率算法     | CountMin (Cormode-Muthukrishnan) | 统计一个实时的数据流中元素出现的频率，并且准备随时回答某个元素出现的频率，不需要的精确的计数。 |
| 基数估计算法     | FM (Flajolet-Martin)             | 获取指定列中的不同值的数量。 找出这个数字集合中不重复的数字的个数。 |
| 最频繁值         | MFV (Most Frequent Values)       | 计算频繁值的场景。                                           |
| 假设检验         | Hypothesis Tests                 | 包含F-test，chi2-test等。                                    |
| 概率函数         | Probability Functions            | 概率函数模块为各种概率分布提供累积分布，密度、质量和分位数函数。 |

**表 8** 支持的机器学习算法 - 其他算法

| 算法名称（中文） | 算法名称（英文）                  | 应用场景                                                     |
| :--------------- | :-------------------------------- | :----------------------------------------------------------- |
| k-聚类算法       | K-means                           | 聚类场景。                                                   |
| 隐含狄利克雷分布 | Latent Dirichlet Allocation (LDA) | LDA 在主题模型中占有非常重要的地位，常用来文本分类。         |
| 关联规则算法     | Apriori Algorithm                 | 关联规则算法，关联规则挖掘的目标是发现数据项集之间的关联关系。比如经典的“啤酒和尿布”。 |
