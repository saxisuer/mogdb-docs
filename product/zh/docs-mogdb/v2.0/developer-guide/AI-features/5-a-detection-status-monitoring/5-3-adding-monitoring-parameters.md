---
title: 添加监控参数
summary: 添加监控参数
author: Guo Huan
date: 2021-05-19
---

# 添加监控参数

<br/>

## 编写指标数据获取接口

下面以io_read为例，在task目录的metric_task.py中编写如下io_read指标获取函数：

```python
def io_read():
     child1 = subprocess.Popen(['pidstat', '-d'], stdout=subprocess.PIPE, shell=False)
     child2 = subprocess.Popen(['grep', 'gaussd[b]'], stdin=child1.stdout, stdout=subprocess.PIPE, shell=False)
     result = child2.communicate()
     if not result[0]:
         return 0.0
     else:
         return result[0].split()[3].decode('utf-8')
```

<br/>

## 添加指标监控参数

在task目录下的metric_task.conf中添加io_read的section：

```python
[io_read]
minimum = 30
maximum = 100
data_period = 1H
forecast_interval = 2H
forecast_period = 30M
```

<br/>

## 重启服务

命令参考获取帮助

```python
重启agent：
    本地服务器:
        python main.py stop --role agent
        python main.py stop --role agent
    远程服务器：
        python main.py start --user USER --host HOST --project-path PROJECT_PATH --role agent
        python main.py stop --user USER --host HOST --project-path PROJECT_PATH --role agent
重启server：
    本地服务器:
        python main.py stop --role server
        python main.py stop --role server
    远程服务器：
        python main.py start --user USER --host HOST --project-path PROJECT_PATH --role server
        python main.py stop --user USER --host HOST --project-path PROJECT_PATH --role server
重启monitor：
    本地服务器:
        python main.py stop --role monitor
        python main.py stop --role monitor
    远程服务器：
        python main.py start --user USER --host HOST --project-path PROJECT_PATH --role monitor
        python main.py stop --user USER --host HOST --project-path PROJECT_PATH --role monitor
```
