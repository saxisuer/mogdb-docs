---
title: 概述
summary: 概述
author: Guo Huan
date: 2021-05-19
---

# 概述

anomaly_detection是MogDB集成的、可以用于数据库指标采集、预测以及异常监控与诊断的AI工具，是dbmind套间中的一个组件。支持采集的信息包括IO_Read、IO_Write、CPU_Usage、Memory_Usage以及数据库所占磁盘空间等。anomaly_detection可以同时监控多个指标，并预测每个指标未来的变化趋势，当发现某个指标在未来某段时间或者某个时刻会超出人工设置的阈值，该工具会通过日志进行报警。

anomaly_detection由agent和detector两部分组成。

agent和MogDB数据库环境部署在同一个服务器上，agent模块主要有两个作用。一个是定时采集数据库指标数据，并将采集到的数据存放到缓冲队列中；另一个作用是将缓冲队列中数据定时发送到detector端。

detector模块基于http或https和agent模块通信，因此它可以部署到任何可以与agent端进行通信的服务器上，该模块主要主要有两个作用。一个是接受agent端发送的数据，并将收集到的数据缓存在本地；另外一个作用是基于收集到的数据库指标数据，对该指标的未来变化趋势进行预测和异常报警。
