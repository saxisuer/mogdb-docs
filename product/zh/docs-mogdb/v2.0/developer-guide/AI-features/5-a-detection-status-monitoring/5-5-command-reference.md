---
title: 命令参考
summary: 命令参考
author: Guo Huan
date: 2021-05-19
---

# 命令参考

**表 1** 命令行参数

| 参数              | 参数说明                            | 取值范围                                                     |
| :---------------- | :---------------------------------- | :----------------------------------------------------------- |
| mode              | 指定运行模式                        | start,stop,forecast,show_metrics,deploy                      |
| -user             | 远程服务器用户                      |                                                              |
| -host             | 远程服务器IP                        |                                                              |
| -project-path     | 远程服务器anomaly_detection项目路径 |                                                              |
| -role             | 启动角色选择                        | agent，server，monitor                                       |
| -metric-name      | 指标名称                            |                                                              |
| -forecast-periods | 未来预测周期                        | 整数+时间单位，如'100S'，'3H'<br/>时间单位包括（S(second),M(minute),H(hour),D(day),W(week)） |
| -forecast-method  | 预测方法                            | auto_arima， fbprophet                                       |
| -save-path        | 预测结果存放地址                    |                                                              |
| -version, -v      | 返回当前工具版本号                  |                                                              |
