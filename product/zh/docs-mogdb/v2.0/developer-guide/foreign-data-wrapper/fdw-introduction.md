---
title: 介绍
summary: 介绍
author: Zhang Cuiping
date: 2021-05-17
---

# 介绍

MogDB的fdw实现的功能是各个MogDB数据库及远程数据库之间的跨库操作。目前支持的远程数据库类型包括PostgreSQL/openGauss/MogDB（postgres_fdw）。Oracle和MySQL/MariaDB的fdw会在近期推出。
