---
title: MOT样例PGBench基准
summary: MOT样例PGBench基准
author: Zhang Cuiping
date: 2021-03-04
---

# MOT样例PGBench基准

pgbench是一个用于在PostgreSQL数据库中运行基准测试的简单程序。pgbench在多个并发的数据库会话中反复运行一系列相同的SQL命令，并计算事务执行的平均速率（每秒执行的事务个数）。 pgbench默认测试的是一种基于TPC-B的松散的测试，即一个事务中包括5个SELECT,UPDATE和INSERT语句。

为了支持MogDB MOT的乐观并发控制(OCC)，在标准pgbench中进行了几个小的更改。增强的实用程序作为MogDB代码的一部分发布在**contrib**文件夹下。

pgbench基准测试(26/08/2020)显示，MOT引擎的性能是Disk-engine的2.4-2.8倍，既提高了TPS，也降低了延迟。

- 硬件测试：2插槽72核x86服务器(Intel Xeon Gold 6154 CPU @ 3GHz)
- 测试参数：事务=TPCB;缩放系数= 100;查询协议=已准备

<br/>

## 如何使用MOT运行pgbench

1. 将数据加载到MOT，需要使用“-m”标记，如下例所示：

   ```bash
   ./pgbench -h <server_ip> -p <server_port> -U <user> -W <password> -i -m -s 100
   ```

2. 为了利用MOT JIT功能的最大性能，建议使用如下示例所示的准备模式：

   ```bash
   ./pgbench -h <server_ip> -p <server_port> -U <user> -W <password> -c 30 -j 30 -T 10 -M prepared
   ```
