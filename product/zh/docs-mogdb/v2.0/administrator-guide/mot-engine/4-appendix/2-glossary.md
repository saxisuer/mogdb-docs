---
title: 术语表
summary: 术语表
author: Zhang Cuiping
date: 2021-05-18
---

# 术语表

| 缩略语 | 定义描述                                                     |
| :----- | :----------------------------------------------------------- |
| 2PL    | 2阶段锁（2-Phase Locking）                                   |
| ACID   | 原子性（Atomicity），一致性（Consistency），隔离性（Isolation），持久性（Durability） |
| AP     | 分析处理（Analytical Processing）                            |
| Arm    | 高级RISC机器（Advanced RISC Machine），x86的替代硬件架构。   |
| CC     | 并发控制（Concurrency Control）                              |
| CPU    | 中央处理器（Central Processing Unit）                        |
| DB     | 数据库（Database）                                           |
| DBA    | 数据库管理员（Database Administrator）                       |
| DBMS   | 数据库管理系统（DataBase Management System）                 |
| DDL    | 数据定义语言（Data Definition Language）数据库模式管理语言   |
| DML    | 数据修改语言（Data Modification Language）                   |
| ETL    | 提取、转换、加载或遇时锁定（Extract, Transform, Load or Encounter Time Locking） |
| FDW    | 外部数据封装（Foreign Data Wrapper）                         |
| GC     | 垃圾收集器（Garbage Collector）                              |
| HA     | 高可用性（High Availability）                                |
| HTAP   | 事务分析混合处理（Hybrid Transactional-Analytical Processing） |
| IoT    | 物联网（Internet of Things）                                 |
| IM     | 内储存（In-Memory）                                          |
| IMDB   | 内储存数据库（In-Memory Database）                           |
| IR     | 源代码的中间表示（Intermediate Representation），用于编译和优化 |
| JIT    | 准时（Just In Time）                                         |
| JSON   | JavaScript对象表示法（JavaScript Object Notation）           |
| KV     | 键值（Key Value）                                            |
| LLVM   | 低级虚拟机（Low-Level Virtual Machine），指编译代码或IR查询  |
| M2M    | 机对机（Machine-to-Machine）                                 |
| ML     | 机器学习（Machine Learning）                                 |
| MM     | 主内存（Main Memory）                                        |
| MO     | 内存优化（Memory Optimized）                                 |
| MOT    | 内存优化表存储引擎（SE），读作/em/ /oh/ /tee/                |
| MVCC   | 多版本并发控制（Multi-Version Concurrency Control）          |
| NUMA   | 非一致性内存访问（Non-Uniform Memory Access）                |
| OCC    | 乐观并发控制（Optimistic Concurrency Control）               |
| OLTP   | 在线事务处理（On-Line Transaction Processing），多用户在线交易类业务 |
| PG     | PostgreSQL                                                   |
| RAW    | 写后读校验（Reads-After-Writes）                             |
| RC     | 返回码（Return Code）                                        |
| RTO    | 目标恢复时间（Recovery Time Objective）                      |
| SE     | 存储引擎（Storage Engine）                                   |
| SQL    | 结构化查询语言（Structured Query Language）                  |
| TCO    | 总体拥有成本（Total Cost of Ownership）                      |
| TP     | 事务处理（Transactional Processing）                         |
| TPC-C  | 一种联机事务处理基准                                         |
| Tpm-C  | 每分钟事务数-C. TPC-C基准的性能指标，用于统计新订单事务。    |
| TVM    | 微小虚拟机（Tiny Virtual Machine）                           |
| TSO    | 分时选项（Time Sharing Option）                              |
| UDT    | 自定义类型                                                   |
| WAL    | 预写日志（Write Ahead Log）                                  |
| XLOG   | 事务日志的PostgreSQL实现（WAL，如上文所述）                  |
