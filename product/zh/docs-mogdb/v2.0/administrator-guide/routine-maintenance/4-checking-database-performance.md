---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 检查数据库性能

## 检查办法

通过MogDB提供的性能统计工具gs_checkperf可以对硬件性能进行检查。

**前提条件**

- MogDB运行状态正常。
- 运行在数据库之上的业务运行正常。

**操作步骤**

1. 以操作系统用户omm登录数据库主节点。

2. 执行如下命令对MogDB数据库进行性能检查。

    ```bash
    gs_checkperf
    ```

具体的性能统计项目请参见“参考指南 > 工具参考 > 服务端工具 > [gs_checkperf](../../reference-guide/tool-reference/server-tools/3-gs_checkperf.md)”中的“表1 性能检查项”。

**示例**

以简要格式在屏幕上显示性能统计结果。

```bash
gs_checkperf -i pmk -U omm
Cluster statistics information:
    Host CPU busy time ratio                     :    1.43       %
    MPPDB CPU time % in busy time                :    1.88       %
    Shared Buffer Hit ratio                      :    99.96      %
    In-memory sort ratio                         :    100.00     %
    Physical Reads                               :    4
    Physical Writes                              :    25
    DB size                                      :    70         MB
    Total Physical writes                        :    25
    Active SQL count                             :    2
    Session count                                :    3
```

<br/>

## 异常处理

使用gs_checkperf工具检查MogDB性能状态后，如果发现检查结果发现异常，可以根据以下内容进行修复。

**表 1** 检查MogDB级别性能状态

| 异常状态          | 处理方法           |
| ----------------- | -------------------------------------------------- |
| 主机CPU占有率高   | 1、更换和增加高性能的CPU。<br />2、使用top命令查看系统哪些进程的CPU占有率高，然后使用kill命令关闭没有使用的进程。<br />*top*                                                              |
| MogDB CPU占有率高 | 1、更换和增加高性能的CPU。<br />2、使用top命令查看数据库哪些进程的CPU占有率高，然后使用kill命令关闭没有使用的进程。<br />*top*<br />3、使用gs_expand工具扩容，增加新的主机均衡CPU占有率。 |
| 共享内存命中率低  | 1、扩大内存。<br />2、使用如下命令查看操作系统配置文件/etc/sysctl.conf，调大共享内存kernel.shmmax值。<br />*vim /etc/sysctl.conf*                                                         |
| 内存中排序比率低  | 扩大内存。                                                                                                                                                                                |
| I/O、磁盘使用率高 | 1、更换高性能的磁盘。<br />2、调整数据布局，尽量将I/O请求较合理的分配到所有物理磁盘中。<br />3、全库进行VACUUM FULL操作。<br />*vacuum full;*<br />4、进行磁盘整理。<br />5、降低并发数。 |
| 事务统计          | 查询pg_stat_activity系统表，将不必要的连接断开。（登录数据库后查询: postgres=# \d+ pg_stat_activity;）                                                                                    |

**表 2** 检查节点级别性能状态

| 异常状态           | 处理方法                                                                                                                               |
| ------------------ | --------------------------------------- |
| CPU占有率高        | 1、更换和增加高性能的CPU。<br />2、使用top命令查看系统哪些进程的CPU占有率高，然后使用kill命令关闭没有使用的进程。<br />*top*           |
| 内存使用率过高情况 | 扩大或清理内存。                                                                                                                       |
| I/O使用率过高情况  | 1、更换高性能的磁盘。<br />2、进行磁盘清理。<br />3、尽可能用内存的读写代替直接磁盘I/O，使频繁访问的文件或数据放入内存中进行操作处理。 |

**表 3** 会话/进程级别性能状态

| 异常状态                     | 处理方法        |
| ---------------------------- | --------------------------------- |
| CPU、内存、I/O使用率过高情况 | 查看哪个进程占用CPU/内存高或I/O使用率高，若是无用的进程，则kill掉，否则排查具体原因。例如SQL执行占用内存大，查看是否SQL语句需要优化。 |

**表 4** SSD性能状态

| 异常状态        | 处理方法                                    |
| --------------- | -------------------------------------------- |
| SSD读写性能故障 | 使用以下命令查看SSD是否有故障，排查具体故障原因。<br />*gs_checkperf -i SSD -U omm* |
