---
title: 日常运维
summary: 日常运维
author: Zhang Cuiping
date: 2021-03-04
---

# 例行维护表

为了保证数据库的有效运行，数据库必须在插入/删除操作后，基于客户场景，定期做VACUUM FULL和ANALYZE，更新统计信息，以便获得更优的性能。

**相关概念**

使用VACUUM、VACUUM FULL和ANALYZE命令定期对每个表进行维护，主要有以下原因:

- VACUUM FULL可回收已更新或已删除的数据所占据的磁盘空间，同时将小数据文件合并。
- VACUUM对每个表维护了一个可视化映射来跟踪包含对别的活动事务可见的数组的页。一个普通的索引扫描首先通过可视化映射来获取对应的数组，来检查是否对当前事务可见。若无法获取，再通过堆数组抓取的方式来检查。因此更新表的可视化映射，可加速唯一索引扫描。
- VACUUM可避免执行的事务数超过数据库阈值时，事务ID重叠造成的原有数据丢失。
- ANALYZE可收集与数据库中表内容相关的统计信息。统计结果存储在系统表PG_STATISTIC中。查询优化器会使用这些统计数据，生成最有效的执行计划。

**操作步骤**

1. 使用VACUUM或VACUUM FULL命令，进行磁盘空间回收。

    - **VACUUM**:

    对表执行VACUUM操作

    ```sql
    postgres=# VACUUM customer;
    ```

    ```sql
    VACUUM
    ```

    可以与数据库操作命令并行运行。（执行期间，可正常使用的语句: SELECT、INSERT、UPDATE和DELETE。不可正常使用的语句: ALTER TABLE）。

    对表分区执行VACUUM操作

    ```sql
    postgres=# VACUUM customer_par PARTITION ( P1 );
    ```

    ```sql
    VACUUM
    ```

   - **VACUUM FULL**:

    ```sql
    postgres=# VACUUM FULL customer;
    ```

    ```sql
    VACUUM
    ```

    需要向正在执行的表增加排他锁，且需要停止其他所有数据库操作。

2. 使用ANALYZE语句更新统计信息。

    ```sql
    postgres=# ANALYZE customer;
    ```

    ```sql
    ANALYZE
    ```

    使用ANALYZE VERBOSE语句更新统计信息，并输出表的相关信息。

    ```sql
    postgres=# ANALYZE VERBOSE customer;
    ```

    ```sql
    ANALYZE
    ```

    也可以同时执行VACUUM ANALYZE命令进行查询优化。

    ```sql
    postgres=# VACUUM ANALYZE customer;
    ```

    ```sql
    VACUUM
    ```

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** VACUUM和ANALYZE会导致I/O流量的大幅增加，这可能会影响其他活动会话的性能。因此，建议通过"vacuum_cost_delay"参数设置《开发者指南》中"GUC参数说明 > 资源消耗 > 基于开销的清理延迟"。

3. 删除表

    ```sql
    postgres=# DROP TABLE customer;
    postgres=# DROP TABLE customer_par;
    postgres=# DROP TABLE part;
    ```

    当结果显示为如下信息，则表示删除成功。

    ```sql
    DROP TABLE
    ```

**维护建议**

- 定期对部分大表做VACUUM FULL，在性能下降后为全库做VACUUM FULL，目前暂定每月做一次VACUUM FULL。
- 定期对系统表做VACUUM FULL，主要是PG_ATTRIBUTE。
- 启用系统自动清理进程（AUTOVACUUM）自动执行VACUUM和ANALYZE，回收被标识为删除状态的记录空间，并更新表的统计数据。
