---
title: 更新表中数据
summary: 更新表中数据
author: Guo Huan
date: 2021-03-04
---

# 更新表中数据

<br/>

## 使用DML命令更新表

MogDB支持标准的数据库操作语言（DML）命令，对表进行更新。

<br/>

### 操作步骤

假设存在表customer_t，表结构如下:

```sql
CREATE TABLE customer_t
( c_customer_sk             integer,
  c_customer_id             char(5),
  c_first_name              char(6),
  c_last_name               char(8)
) ;
```

可以使用如下DML命令对表进行数据更新。

- 使用INSERT向表中插入数据。

  - 向表customer_t中插入一行。

    ```sql
    INSERT INTO customer_t (c_customer_sk, c_customer_id, c_first_name,c_last_name) VALUES (3769, 5, 'Grace','White');
    ```

  - 向表customer_t中插入多行数据。

    ```sql
    INSERT INTO customer_t (c_customer_sk, c_customer_id, c_first_name,c_last_name) VALUES
    (6885, 1, 'Joes', 'Hunter'),
    (4321, 2, 'Lily','Carter'),
    (9527, 3, 'James', 'Cook'),
    (9500, 4, 'Lucy', 'Baker');
    ```

    更多关于INSERT的使用方法，请参见向表中插入数据。

- 使用UPDATE更新表中数据。修改字段c_customer_id值为0。

    ```sql
    UPDATE customer_t SET c_customer_id = 0;
    ```

    更多关于UPDATE的使用方法，请参见UPDATE。

- 使用DELETE删除表中的行。

    可以使用WHERE子句指定需要删除的行，若不指定即删除表中所有的行，只保留数据结构。

    ```sql
    DELETE FROM customer_t WHERE c_last_name = 'Baker';
    ```

    更多关于DELETE的使用方法，请参见DELETE。

- 使用TRUNCATE命令快速从表中删除所有的行。

    ```sql
    TRUNCATE TABLE customer_t;
    ```

    更多关于TRUNCATE的使用方法，请参见TRUNCATE。

    删除表时，DELETE语句每次删除一行数据而TRUNCATE语句是通过释放表存储的数据页来删除数据，使用TRUNCATE语句比使用DELETE语句更加快速。

    使用DELETE语句删除表时，仅删除数据，不释放存储空间。使用TRUNCATE语句删除表时，删除数据且释放存储空间。

<br/>

## 使用合并方式更新和插入数据

在用户需要将一个表中所有的数据或大量的数据添加至现有表的场景下，MogDB提供了MERGE INTO语句通过两个表合并的方式高效地将新数据添加到现有表。

MERGE INTO语句将目标表和源表中数据针对关联条件进行匹配，若关联条件匹配时对目标表进行UPDATE，关联条件不匹配时对目标表执行INSERT。此方法可以很方便地用来将两个表合并执行UPDATE和INSERT，避免多次执行。

<br/>

### 前提条件

进行MERGE INTO操作的用户需要同时拥有目标表的UPDATE和INSERT权限，以及源表的SELECT权限。

<br/>

### 操作步骤

1. 创建源表products，并插入数据。

    ```sql
    mogdb=# CREATE TABLE products
    ( product_id INTEGER,
      product_name VARCHAR2(60),
      category VARCHAR2(60)
    );

    mogdb=# INSERT INTO products VALUES
    (1502, 'olympus camera', 'electrncs'),
    (1601, 'lamaze', 'toys'),
    (1666, 'harry potter', 'toys'),
    (1700, 'wait interface', 'books');
    ```

2. 创建目标表newproducts,并插入数据。

    ```sql
    mogdb=# CREATE TABLE newproducts
    ( product_id INTEGER,
      product_name VARCHAR2(60),
      category VARCHAR2(60)
    );

    mogdb=# INSERT INTO newproducts VALUES
    (1501, 'vivitar 35mm', 'electrncs'),
    (1502, 'olympus ', 'electrncs'),
    (1600, 'play gym', 'toys'),
    (1601, 'lamaze', 'toys'),
    (1666, 'harry potter', 'dvd');
    ```

3. 使用MERGE INTO 语句将源表products的数据合并至目标表newproducts。

    ```sql
    MERGE INTO newproducts np
    USING products p
    ON (np.product_id = p.product_id )
    WHEN MATCHED THEN
      UPDATE SET np.product_name = p.product_name, np.category = p.category
    WHEN NOT MATCHED THEN
      INSERT VALUES (p.product_id, p.product_name, p.category) ;
    ```

    上述语句中使用的参数说明，请见[表1](#MERGE INTO语句参数说明)。更多信息，请参见MERGE INTO。

    **表 1** MERGE INTO语句参数说明<a id="MERGE INTO语句参数说明"> </a>

    | 参数                 | 说明| 举例 |
    | :--------------- | :------------- | :--------------|
    | INTO 子句            | 指定需要更新或插入数据的目标表。<br/>目标表支持指定别名。 | **取值:**newproducts np<br/>**说明:**名为newproducts，别名为np的目标表。          |
    | USING子句            | 指定源表。源表支持指定别名。                       | **取值:**products p<br/>**说明:**名为products，别名为p的源表。                   |
    | ON子句               | 指定目标表和源表的关联条件。<br/>关联条件中的字段不支持更新。  | **取值:**np.product_id = p.product_id<br/>**说明:**指定的关联条件为，目标表newproducts的product_id字段和源表products的product_id字段相等。                      |
    | WHEN MATCHED子句     | 当源表和目标表中数据针对关联条件可以匹配上时，选择WHEN MATCHED子句进行UPDATE操作。<br/>- 仅支持指定一个WHEN MATCHED子句。<br/>- WHEN MATCHED子句可缺省，缺省时，对于满足ON子句条件的行，不进行任何操作。<br/>- 若目标表中存在分布列，则该列不支持更新。                | **取值:**WHEN MATCHED THEN UPDATE SET np.product_name = p.product_name, np.category = p.category<br/>**说明:**当满足ON子句条件时，将目标表newproducts的product_name、category字段的值替换为源表products相对应字段的值。 |
    | WHEN NOT MATCHED子句 | 当源表和目标表中数据针对关联条件无法匹配时，选择WHEN NOT MATCHED子句进行INSERT操作。<br/>- 仅支持指定一个WHEN NOT MATCHED子句。<br/>- WHEN NOT MATCHED子句可缺省。<br/>- 不支持INSERT子句中包含多个VALUES。<br/>- WHEN MATCHED和WHEN NOT MATCHED子  句顺序可以交换，可以缺省其中一个，但不能同时缺省。 | **取值:**WHEN NOT MATCHED THENINSERT VALUES (p.product_id, p.product_name, p.category)<br/>**说明:**将源表products中，不满足ON子句条件的行插入目标表newproducts。 |

4. 查询合并后的目标表newproducts。

    ```sql
    SELECT * FROM newproducts;
    ```

   返回信息如下:

    ```sql
     product_id |  product_name  | category
    ------------+----------------+-----------
          1501 | vivitar 35mm   | electrncs
          1502 | olympus camera | electrncs
          1666 | harry potter   | toys
          1600 | play gym       | toys
          1601 | lamaze         | toys
          1700 | wait interface | books
    (6 rows)
    ```
