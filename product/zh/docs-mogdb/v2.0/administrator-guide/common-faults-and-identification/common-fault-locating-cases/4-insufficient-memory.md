---
title: 内存不足问题
summary: 内存不足问题
author: Guo Huan
date: 2021-05-24
---

# 内存不足问题

## 问题现象

客户端或日志里出现错误: memory usage reach the max_dynamic_memory。

## 原因分析

出现内存不足可能因GUC参数max_process_memory值设置较小相关，该参数限制一个MogDB实例可用最大内存。

## 处理分析

通过工具gs_guc适当调整max_process_memory参数值。注意需重启实例生效。
