---
title: 分析查询语句长时间运行的问题
summary: 分析查询语句长时间运行的问题
author: Guo Huan
date: 2021-05-24
---

# 分析查询语句长时间运行的问题

## 问题现象

系统中部分查询语句运行时间过长。

## 原因分析

- 查询语句较为复杂，需要长时间运行。
- 查询语句阻塞。

## 处理办法

1. 以操作系统用户omm登录主机。

2. 使用如下命令连接数据库。

   ```bash
   gsql -d mogdb -p 8000
   ```

   postgres为需要连接的数据库名称，8000为端口号。

3. 查看系统中长时间运行的查询语句。

   ```sql
   SELECT timestampdiff(minutes, query_start, current_timestamp) AS runtime, datname, usename, query FROM pg_stat_activity WHERE state != 'idle' ORDER BY 1 desc;
   ```

   查询会返回按执行时间长短从大到小排列的查询语句列表。第一条结果就是当前系统中执行时间长的查询语句。

   如果当前系统较为繁忙，可以使用[TIMESTAMPDIFF](../../../reference-guide/functions-and-operators/8-date-and-time-processing-functions-and-operators.md)函数通过限制current_timestamp和query_start大于某一阈值查看执行时间超过此阈值的查询语句。timestampdiff的第一个参数为时间差单位。例如，执行超过2分钟的查询语句可以通过如下语句查询。

   ```sql
   SELECT query FROM pg_stat_activity WHERE timestampdiff(minutes, query_start, current_timestamp) > 2;
   ```

4. 分析长时间运行的查询语句状态。

   - 如果查询语句处于正常状态，则等待其执行完毕。
   - 如果查询语句阻塞，请参见[分析查询语句是否被阻塞处理](14-analyzing-whether-a-query-statement-is-blocked.md)。
