---
title: 磁盘满故障引起的core问题
summary: 磁盘满故障引起的core问题
author: Guo Huan
date: 2021-05-24
---

# 磁盘满故障引起的core问题

## 问题现象

TPCC运行时，注入磁盘满故障，数据库进程mogdb core掉，如下图所示。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/administrator-guide/core-dump-occurs-due-to-full-disk-space.png)

## 原因分析

数据库本身机制，在磁盘满时，xlog日志无法进行写入，通过panic日志退出程序。

## 处理办法

外部监控磁盘使用状况，定时进行清理磁盘。
