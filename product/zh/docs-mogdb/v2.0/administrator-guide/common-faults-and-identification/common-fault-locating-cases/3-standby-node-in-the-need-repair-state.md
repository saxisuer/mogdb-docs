---
title: 备机处于need repair(WAL)状态问题
summary: 备机处于need repair(WAL)状态问题
author: Guo Huan
date: 2021-05-24
---

# 备机处于need repair(WAL)状态问题

## 问题现象

MogDB备机出现Standby Need repair(WAL)故障。

## 原因分析

因网络故障、磁盘满等原因造成主备实例连接断开，主备日志不同步，导致数据库在启动时异常。

## 处理分析

通过gs_ctl build -D 命令对故障节点进行重建，具体的操作方法请参见MogDB工具参考中的[build参数](../../../reference-guide/tool-reference/tools-used-in-the-internal-system/4-gs_ctl.md#build)。
