---
title: 产品FAQs
summary: 产品FAQs
author: Guo Huan
date: 2021-09-26
---

# 产品FAQs

**Q1：“极致RTO”、“并行恢复”与“备机可读”的关系是什么？**

极致RTO、并行恢复、备机可读配置分别如下：

- 极致RTO配置：recovery_parse_workers 并行恢复时解析XLOG线程的个数 recovery_parallelism 并行恢复线程的实际个数 recovery_parse_workers大于1时是极致RTO恢复，同时需要配置recovery_parallelism大于1，才会实现并行恢复。
- 并行恢复配置：recovery_max_workers 并行恢复的最大线程个数 recovery_parallelism 并行恢复线程的实际个数 recovery_max_workers大于1时是并行恢复，同时需要配置recovery_parallelism大于1，才会实现并行恢复。
- 备机可读配置：hot_standby 备机热备，在恢复时支持读操作。 hot_standby为true时是备机可读。

并行恢复是文件级别的并行REDO，极致RTO是数据块级别的并行恢复。并行恢复和备机可读是兼容的，极致RTO和备机可读是不兼容的。 配置上面参数时，代码中有检查（CheckExtremeRtoGUCConflicts），如果同时配置了recovery_parse_workers大于1和hot_standby会报错。

**Q2：极致RTO场景下，备机不能读，那该如何选择主备切换的候选主节点？**

备机只有极致RTO情况下不能读，在串行恢复、并行恢复情况下，备机都是可读的。如果使用极致RTO，即在极致RTO情况下，当前只能配置为同步方式，然后随机选择一个当做主机即可（配置为同步方式后所有节点数据是一样的）。

**Q3：模板数据库的作用是什么？模板数据库中包含哪些表？**

模板数据库提供了一个快速创建数据库的手段，创建数据库时指定**TEMPLATE**参数，即可通过复制模板数据库创建数据库。

模板数据库中没有用户表，可通过系统表PG_DATABASE查看模板数据库属性。

**Q4：MogDB支持物理复制时，备机是按照Page并行回放日志吗？**

MogDB有两种并行恢复模式，一种是以文件为粒度，一种是以页面为粒度。

**Q5：MogDB是不是分布式数据库？**

不是。MogDB目前仅仅是单机主备模式，不具备分布式数据库特征。

但是如果对于分库分表，请求路由等非Native分布式需求，MogDB前端可以架设pgpool-II或者Shardingsphere等分布式中间件，来提供相应的功能。

**Q6：MogDB的适用场景有哪些？**

和pgsql受众类似。MogDB数据库可应用于电商、金融、O2O、电信CRM/计费等大并发、大数据量、

 以联机事务处理为主的交易型应用场景和工业监控、远程控制、智慧城市能力延展、智能家居、车联网等物联网应用场景。

不仅具备关系型数据库的基本功能，更针对企业级应用场景和特性打造高可用、监控等增强功能

**Q7：MogDB可以部署在x86服务器上吗？硬件推荐配置是怎样的？**

MogDB可以安装在x86服务器上（AMD64芯片架构）。具体信息请参考[硬件环境要求](../installation-guide/simplified-installation-process/1-installation-preparation.md#yingjianhuanjing)。

**Q8：MogDB可以部署在鲲鹏服务器上吗？硬件推荐配置是怎样的？**

MogDB可以部署在鲲鹏服务器上（ARM芯片架构），硬件推荐配置同x86一样。

实际上，MogDB在鲲鹏服务器上的性能更优于x86服务器，因此我们推荐客户选择国产芯片服务器。

**Q9：MogDB是免费的吗？MogDB的收费模式是怎样的？**

MogDB不是免费的（openGauss是免费的）。MogDB目前面向企业用户，不向个人开放，具体定价规则请咨询销售体系。

**Q10：MogDB创建数据库时的兼容数据库设置具体指什么？对兼容不同数据库的数据类型都做了哪些处理？**

MogDB创建数据库时可以指定DBCOMPATIBILITY参数以兼容不同数据库，但是这里的兼容只是非常有限的数据存储结构的兼容，并不会支持比如SQL语法等等的兼容。

目前有三种设定，分别是Oracle、MySQL和PostgreSQL。默认兼容Oracle。

- 在Oracle兼容性下，数据库将空字符串作为NULL处理，数据类型DATE会被替换为TIMESTAMP(0) WITHOUT TIME ZONE。
- 在MySQL兼容性下，将字符串转换成整数类型时，如果输入不合法，会将输入转换为0，而其它兼容性则会报错。
- 在PG兼容性下，CHAR和VARCHAR以字符为计数单位，其它兼容性以字节为计数单位。
  例如，对于UTF-8字符集，CHAR(3)在PG兼容性下能存放3个中文字符，而在其它兼容性下只能存放1个中文字符。

如需创建兼容PG数据库，语法格式：CREATE DATABASE dbname DBCOMPATIBILITY='PG';

参数说明：

DBCOMPATIBILITY [ = ] compatibilty_type

指定兼容的数据库的类型。取值范围: A、B、C、PG。分别表示兼容Oracle、MySQL、Teradata和PostgreSQL。但是C目前已经放弃支持。因此常用的取值是A、B、PG。

**Q11：MogDB在传统PostgreSQL的基础上做了什么重要改进？MogDB优势是什么？**

在技术实现层面

- 最大可用模式most_available_sync（同步备机故障时，主机事务不因同步备机故障而被阻塞）
- xid不可耗尽
- 流复制环境自动创建物理复制槽
- 增量检查点
- 双写double write等等

在生态层面

- 基于openGauss魔改，依托于openGauss的强大研发团队。
- 完全自主可控。
- 有完整的生态工具。（PTK，MTK，RWT，MIT，SCA，BRM）-部分工具还在研发中。
- 社区活跃，资料丰富。

**Q12：对于 Intel 处理器，MogDB的性能情况如何呢？**

| **测试项**       | **数据量** | **并发数** | **CPU平均使用率** | **IOPS** | **IO延时** | **WAL数量** | **tpmC** | **测试时长** |
| ---------------- | ---------- | ---------- | ----------------- | -------- | ---------- | ----------- | -------- | ------------ |
| 单节点           | 100G       | 500        | 29.39%            | 6.50K    | 1.94.ms    | 3974        | 520896.3 | 10分钟       |
| 一主一同步一异步 | 100G       | 500        | 30.4%             | 5.31K    | 453.2us    | 3944        | 519993.5 | 10分钟       |
| 一主两同步       | 100G       | 500        | 26.35%            | 7.66K    | 531.9us    | 3535        | 480842.2 | 10分钟       |

**Q13：MogDB支持部署在哪些操作系统上？会有兼容性问题吗？**

MogDB支持部署在以下操作系统：

- ARM：
  - openEuler 20.3LTS（推荐采用此操作系统）
  - 麒麟V10
- X86：
  - openEuler 20.3LTS
  - CentOS 7.6
  - Asianux 7.6

不同的操作系统和CPU架构会有兼容性问题。

**Q14：MogDB和Oracle在MVCC的实现上有什么区别？**

Oracle：基于SCN，块级别，循环undo segment实现，支持闪回功能，存在大事务回滚、快照过旧ORA-01555问题。
MogDB：基于事务编号txid，行级别，无需undo，大事务可瞬间回滚，存在数据块（block page）空间及性能消耗问题

**Q15：MogDB的增量检查点优势在哪里？**

增量检查点会小批量的分阶段的滚筒式的去进行脏页刷盘，同时更新lsn信息，回收不需要的xlog日志,避免了全量刷脏页对磁盘造成冲击，影响性能

**Q16：MogDB如何屏蔽或优化原生PostgreSQL中遇到的XID用尽问题？**

MogDB将transactionid由int32改为了int64，64位的xid永远不可能耗尽，虽然xid改为了64位，但是过期的xid依旧需要freeze清理，只是永远不用担心会发生xid回卷宕机的风险。

**Q17：MogDB用户权限和Oracle有什么不同？**

Oracle schema和user是一个概念，MogDB则不是。MogDB中，用户创建的所有对象都被创建在指定的schema（或namespace）中。其他用户可能拥有、也可能不拥有访问这些对象的权限，甚至都不可以在对应的schema中创建对象。用户（或角色）是全局对象，不是定义在数据库中，而是定义在实例的级别。schema是用户在指定的数据库中创建的，其中包含数据库对象。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/faqs/product-faqs-1.png)

**Q18：MogDB中double write与MySQL二次commit有什么区别？**

实现原理与MySQL机制相同，其目的是为了保证数据的强一致性，防止因为坏块导致数据丢失，从而导致数据无法恢复。

**Q19：MogDB主备和Oracle Data Guard有什么异同点？**

备库都可以做为查询库分担主库压力

**Q20：物理备份可以使用并行吗？物理备份可以远程执行吗？远程执行物理备份的时候数据文件是通过什么方式传输到执行备份的机器上？**

gs_probackup支持并行备份。可以远程执行，可以通过archive文件或者stream协议传输到目标机器。

**Q21：在物理备份的时候，设置xlog-method=stream或者fetch有什么不同？**

设置xlog传输方式。没有设置该参数的情况下，默认-xlog-method=stream。在备份中包括所需的预写式日志文件（WAL文件）。这包括所有在备份期间产生的预写式日志。fetch方式在备份末尾收集预写式日志文件。因此，有必要把wal_keep_segments参数设置得足够高，这样在备份末尾之前日志不会被移除。如果在要传输日志时它已经被轮转，备份将失败并且是不可用的。stream方式在备份被创建时流传送预写式日志。这将开启一个到服务器的第二连接并且在运行备份时并行开始流传输预写式日志。因此，它将使用最多两个由max_wal_senders参数配置的连接。只要客户端能保持接收预写式日志，使用这种模式不需要在主控机上保存额外的预写式日志。
