---
title: 高可用FAQs
summary: 高可用FAQs
author: Guo Huan
date: 2021-09-26
---

# 高可用FAQs

**Q1：MogHA组件做了什么事情？**

独⽴的HA套件MogHA，主动应对服务器宕机、实例宕机等多种情况，使数据库的故障持续时间从分钟级降到秒级。

**Q2：MogDB HA的架构是怎样的？**

当前⽀持⼀台主机和最少⼀台备机，备机最多8台（与openGauss最新发版保持同步）。

多节点版基于openGauss做了改良处理，由⼀主多备组成，最⾼⽀持8个备库。多节点版的RPO为0，RTO⼩于10s，即当主节点实例故障时，从节点可以在10s之内转换为主节点并提供服务，同时能够保证数据不丢失。

**Q3：MogDB常见部署架构是怎样的？**

MogDB支持三种节点主库、备库、级联库，其中备库和级联库加起来最多支持8个，即1主4备4级联或1主8备，某客户生产架构为1主5备1级联，如下图。

![img](https://cdn-mogdb.enmotech.com/docs-media/mogdb/faqs/high-availability-faqs-1.png)

**Q4：MogDB的RPO/RTO怎样？高可用性可以达到99.999%吗？**

RPO=0，RTO<10s，SLA=99.999%。
