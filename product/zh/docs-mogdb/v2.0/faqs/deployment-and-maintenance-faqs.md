---
title: 部署运维FAQs
summary: 部署运维FAQs
author: Guo Huan
date: 2021-09-26
---

# 部署运维FAQs

<br/>

## 初始化环境FAQs

**Q1: `clusterconfig.xml`示例配置文件需要修改哪些参数？**

需要修改机器名和IP地址。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="mogdb-kernel-0002" />
        <PARAM name="backIp1s" value="172.16.0.245"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/opt/mogdb/mogdb_log/omm" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tool" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>

<!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- mogdb-kernel-0002上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="mogdb-kernel-0002"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="172.16.0.245"/>
            <PARAM name="sshIp1" value="172.16.0.245"/>

            <!--dbnode-->
            <PARAM name="dataNum" value="1"/>
            <PARAM name="dataPortBase" value="26000"/>
            <PARAM name="dataNode1" value="/mogdb/data/db1"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

<br/>

## 初始化脚本FAQs

**Q2: 当提示配置文件参数值与系统环境变量值不相符怎么办？**

```bash
Failed to check the preinstall repeat condition.gaussdbToolPath [/opt/enmo/wisery] is not same with environment [/opt/mogdb/tool]
```

将`clusterconfig.xml`文件里`gaussdbToolPath`的值改为`/opt/mogdb/tool`。

> 说明：
>
> - 出现此类问题，主要是因为系统之前安装过MogDB，环境可能未清理干净，所以会产生配置文件参数值与系统环境变量值不相符的情况，将导致执行失败。将`clusterconfig.xml`配置文件中报错参数的值改为和环境变量文件参数值相同即可。
> - 在`omm`用户下执行`vi ~/.bashrc`可查看环境变量。

**Q3: 当提示无权限执行该命令，怎么办？**

```bash
Fixing server package owner.
[GAUSS-51400] : Failed to execute the command: su - omm -c 'cd '/opt/software/mogdb/''. Error:
-bash: line 0: cd: /opt/software/mogdb/: Permission denied
```

执行`chmod 755 -R /opt/software`为该目录赋予权限后，再重新执行脚本初始化命令。

**Q4: 当符号链接创建失败怎么办？**

```bash
In:failed to create symbolic link 'usr/bin/python3': File exists
```

将`usr/bin/`目录下的`python3`删掉，重新执行`In`命令。

**Q5: 当提示主机名不在集群中时怎么办？**

```bash
GAUSS-51619: "The host name [host-172-16-0-195] is not in the cluster."
```

这是因为机器实际的名称是`host-172-16-0-195`，而不是`mogdb-kernel-0006`，所以需要将`clusterconfig.xml`里的节点名称改为`host-172-16-0-195`。

**Q6: 当出现如下报错时，怎么办？**

```bash
[GAUSS-50202]:The /opt/mogdb must be empty. Or user [omm] has write permission to directory /opt/mogdb. Because it will create symbolic link [/opt/mogdb/app] to install path [/opt/mogdb/app_e0e9f58d] in gs_install process with this user.
```

在`omm`用户下，执行`vi ~/.bashrc`，将环境变量清空，重新执行初始化脚本即可成功。

**Q7: 当出现如下有关依赖包的报错时，怎么办？**

```
/opt/software/mogdb/bin/mogdb: error while loading shared libraries: libnsl.so.1: cannot open shared object file: No such file or directory
no data was returned by command ""/opt/software/mogdb/bin/mogdb" -V"
The program "mogdb" is needed by gs_initdb but was not found in the
same directory as "/opt/software/mogdb/bin/gs_initdb".
Check your installation.
```

重新安装依赖包libnsl*即可。

<br/>

## 安装FAQs

**Q8: 当提示集群已安装怎么办？**

```bash
[GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc;python3 '/opt/mogdb/tool/script/local/CheckInstall.py' -U omm:dbgrp -R /opt/mogdb/app -l /opt/mogdb/mogdb_log/omm/omm/om/gs_local.log -X /opt/software/mogdb/clusterconfig.xml.Error:
Checking old installation.
[GAUSS-51806] : The cluster has been installed.
```

执行`vi ~/.bashrc`查看环境变量，将`GAUSS_ENV=2`改为`GAUSS_ENV=1`，重新执行安装命令即可。

**Q9: 当执行安装命令报如下错误怎么办？**

```bash
[GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc;python3 '/opt/mogdb/tool/script/local/CheckInstall.py' -U omm:dbgrp -R /opt/mogdb/app -l /opt/mogdb/mogdb_log/omm/omm/om/gs_local.log -X /opt/software/mogdb/clusterconfig.xml.Error:
Checking old installation.
Successfully checked old installation.
Checking SHA256.
Successfully checked SHA256.
Checking kernel parameters.
Successfully checked kernel parameters.
Checking directory.
[GAUSS-50202] : The /opt/mogdb/app_b75b585a must be empty.
```

在`root`用户下，进入`/opt/mogdb/app_b75b585a`目录，执行`rm -rf *`将目录下所有文件删除，重新执行安装命令即可。

**Q10: 当错误提示socket被占用怎么办？**

```bash
[GAUSS-50200] : The socket file already exists. Port:26000.
```

将`clusterconfig.xml`文件里的`dataPortBase`值改为一个可用的端口即可。

<br/>

## 部署后FAQs

**gs_om -t start启动数据库失败："在标准安装后，通过hostnamectl set修改主机名"**

**适用于：**

* MogDB 1.1.0

* MogDB 2.0.0

* MogDB 2.0.1

* MogDB 单机数据库

本文档中的信息适用于x86平台。

**症状**：

在标准安装结束后（单机），通过`hostnamectl set`修改主机名，执行`gs_om -t start`命令数据库启动失败。

例如：

1. 在`root`用户下通过`hostnamectl set`将主机名`mogdb-kernel-0005`修改为`www.mogdb.com`。

   ```bash
   [root@mogdb-kernel-0005 ~]# hostnamectl
      Static hostname: mogdb-kernel-0005
            Icon name: computer-vm
              Chassis: vm
           Machine ID: de03a5f3bf5a4b7e9df939c9dc5b428d
              Boot ID: 828bfbe948ed4de7aff2a059103a0eaa
       Virtualization: kvm
     Operating System: CentOS Linux 7 (Core)
          CPE OS Name: cpe:/o:centos:centos:7
               Kernel: Linux 3.10.0-1160.15.2.el7.x86_64
         Architecture: x86-64
   [root@mogdb-kernel-0005 ~]# id
   uid=0(root) gid=0(root) groups=0(root)
   [root@mogdb-kernel-0005 ~]# hostname
   mogdb-kernel-0005
   [root@mogdb-kernel-0005 ~]# hostnamectl set-hostname www.mogdb.com
   ```

2. 在任意用户下执行如下命令查看是否主机名修改成功。

   ```bash
   [root@mogdb-kernel-0005 ~]# hostname
   www.mogdb.com
   ```

3. 在`omm`用户下执行如下命令停止数据库。

   ```bash
   [root@mogdb-kernel-0005 ~]# su - omm
   Last login: Tue Jul 27 11:18:16 CST 2021 on pts/1
   [omm@www ~]$ gs_ctl stop -D /mogdb/data/db1
   [2021-07-27 11:19:20.283][32166][][gs_ctl]: gs_ctl stopped ,datadir is /mogdb/data/db1
   waiting for server to shut down.... done
   server stopped
   ```

4. 在`omm`用户下执行如下命令启动数据库。

   ```bash
   [omm@www ~]$ gs_om -t start

   Starting cluster.
   =========================================

   =========================================
   [GAUSS-53600]: Can not start the database, the cmd is source /home/omm/.bashrc; python3 '/opt/enmo/wisequery/script/local/StartInstance.py' -U omm -R /opt/mogdb/app -t 300 --security-mode=off,  Error:
   [GAUSS-51400] : Failed to execute the command: source /home/omm/.bashrc; python3 '/opt/enmo/wisequery/script/local/StartInstance.py' -U omm -R /opt/mogdb/app -t 300 --security-mode=off. Error:
   [FAILURE] mogdb-kernel-0005
   ```

**目的：**

在标准安装结束后（单机），通过`modip.py`脚本修改主机名，执行`gs_om -t start`命令，数据库启动成功。

**故障解决步骤：**

1. 以`omm`用户创建`modip.py`脚本文件。

   ```python
   [omm@www ~]$ cd
   [omm@www ~]$ pwd
   /home/omm
   [omm@www ~]$ vi modip.py

   # -*- coding:utf-8 -*-

   import sys
   ##sys.path.append(r'$GPHOME/script/')
   sys.path.append(r'/opt/enmo/wisequery/script/')
   from gspylib.common.DbClusterInfo import dbClusterInfo

   oldIP = '172.16.0.176'
   newIP = '172.16.0.176'
   oldnodename = 'mogdb-kernel-0005'
   newnodename = 'www.mogdb.com'
   ##Output file directory
   tmpDir = r'/home/omm'
   dbuser = 'omm'

   c1=dbClusterInfo()
   c1.initFromStaticConfig(dbuser)

   for node in c1.dbNodes:
       if node.backIps[0] == oldIP:
           node.backIps[0] = newIP
       if len(node.virtualIp):
           if node.virtualIp[0] == oldIP:
               node.virtualIp[0] = newIP
       if node.name == oldnodename:
           node.name = newnodename
       if node.sshIps[0] == oldIP:
           node.sshIps[0] = newIP
       for dn in node.datanodes:
           if dn.listenIps[0] == oldIP:
               dn.listenIps[0] = newIP
           if dn.haIps[0] == oldIP:
               dn.haIps[0] = newIP
           for peerdn in dn.peerInstanceInfos:
               if len(peerdn.peerHAIPs):
                   if peerdn.peerHAIPs[0] == oldIP:
                       peerdn.peerHAIPs[0] = newIP
   for node in c1.dbNodes:
       staticConfigPath_dn = "%s/cluster_static_config_%s" % (tmpDir, node.name)
       c1.saveToStaticConfig(staticConfigPath_dn, node.id)
       print(staticConfigPath_dn)
   ```

   > **说明**: 需根据实际情况，修改主机IP地址和主机名。例如，在本示例中，主机IP地址为`172.16.0.176`，旧主机名为`mogdb-kernel-0005`，新主机名为`www.mogdb.com`。

2. 执行如下命令生成集群静态配置文件。

   ```bash
   [omm@www ~]$ python3 modip.py
   ```

   回显如下即执行成功。

   ```bash
   /home/omm/cluster_static_config_www.mogdb.com
   ```

3. 执行如下命令替换源文件。

   ```bash
   [omm@www ~]$ mv /opt/mogdb/app/bin/cluster_static_config /opt/mogdb/app/bin/cluster_static_config_mogdb.com

   [omm@www ~]$ cp /home/omm/cluster_static_config_www.mogdb.com /opt/mogdb/app/bin/cluster_static_config
   ```

4. 执行如下命令启动数据库。

   ```bash
   [omm@www ~]$ gs_om -t start

   Starting cluster.
   =========================================

   [SUCCESS] www.mogdb.com

   2021-07-26 16:20:42.938 60fe7056.1 [unknown] 139667408496384 [unknown] 0 dn_6001 01000  0 [BACKEND] WARNING: could not create any HA TCP/IP sockets
   =========================================

   Successfully started.
   ```
