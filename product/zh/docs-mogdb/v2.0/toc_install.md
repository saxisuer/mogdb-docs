<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# MogDB Documentation 2.0.0

## Documentation List

+ 关于MogDB
  + [MogDB简介](/overview.md)
  + [MogDB与openGauss](/about-mogdb/MogDB-and-openGauss.md)
  + [MogDB新特性](/about-mogdb/MogDB-1.0-new-feature.md)
  + [功能迭代计划](/about-mogdb/function-iteration-plan.md)
+ 快速入门
  + [容器化单实例安装](/quick-start/container-based-single-instance-installation.md)
  + [单实例安装](/quick-start/installation-on-a-single-node.md)
  + [gsql客户端连接](/quick-start/using-the-gsql-client-for-connection.md)
+ 测试报告
  + 高可用测试报告
    + [数据库服务器异常测试报告](/test-report/ha/1-database-server-exception-testing.md)
    + [网络异常测试报告](/test-report/ha/2-network-exception-testing.md)
    + [日常维护测试报告](/test-report/ha/3-routine-maintenance-testing.md)
    + [服务异常测试报告](/test-report/ha/4-service-exception-testing.md)
  + 性能测试报告
    + [性能测试简述](/test-report/performance/1-performance-test-overview.md)
    + [MogDB  on 鲲鹏性能测试报告](/test-report/performance/2-mogdb-on-kunpeng-performance-test-report.md)
    + [MogDB  on x86性能测试报告](/test-report/performance/3-mogdb-on-x86-performance-test-report.md)
+ 安装指南
  + [安装概述](/installation-guide/standard-installation/1-installation-overview.md)
  + 安装准备
    + [了解安装流程](/installation-guide/standard-installation/2-installation-process.md)
    + [获取安装包](/installation-guide/standard-installation/3-obtaining-and-verifying-an-installation-package.md)
    + [准备软硬件安装环境](/installation-guide/standard-installation/4-preparing-software-and-hardware-installation-environment.md)
    + [了解安装用户及用户组](/installation-guide/standard-installation/5-installation-user-and-user-group.md)
  + 安装MogDB
    + 标准安装
      + [单实例安装](/installation-guide/standard-installation/6-installation-on-a-single-node.md)
      + 主备安装
        + [创建XML配置文件](/installation-guide/standard-installation/7-creating-XML-configuration-file.md)
        + [初始化安装环境](/installation-guide/standard-installation/8-initializing-installation-environment.md)
        + [执行安装](/installation-guide/standard-installation/9-installation.md)
        + [初始化数据库](/installation-guide/standard-installation/10-initializing-database.md)
        + [（可选）设置备机可读](/installation-guide/standard-installation/11-optional-setting-the-standby-node-to-readable.md)
    + 容器化安装
      + [单实例安装](/installation-guide/docker-installation/1-container-based-single-instance-installation.md)
      + [主备安装](/installation-guide/docker-installation/2-container-based-primary-standby-installation.md)
  + [安装验证](/installation-guide/standard-installation/12-verifying-installation.md)
  + [卸载MogDB](/installation-guide/standard-installation/13-uninstallation.md)
