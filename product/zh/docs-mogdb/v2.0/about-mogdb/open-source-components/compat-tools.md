---
title: compat-tools
summary: compat-tools
author: Zhang Cuiping
date: 2021-07-14
---

# compat-tools

## 组件获取

<https://gitee.com/enmotech/compat-tools>

<br/>

## 介绍

本项目是一个兼容工具集合，旨在为从其他异构数据库，迁移到 MogDB 之后的系统，创建必要的函数，以及系统视图的兼容。为后续的系统运维与应用改造提供便利。

脚本中带有版本控制，直接运行脚本时，会根据如下三种情况进行处理：

1. 若待创建对象在目标数据库中不存在，则直接进行创建
2. 若待创建对象版本高于目标数据库中的对象版本，则进行升级重建
3. 若待创建对象版本不高于目标数据库中的对象版本，则跳过创建

<br/>

## 软件架构

SQL文件：

* runMe.sql: 总调度脚本
* Oracle_Views.sql: Oracle数据库兼容性数据字典及视图
* Oracle_Functions.sql: Oracle数据库兼容性函数
* Oracle_Packages.sql: Oracle数据库管理包兼容
* MySQL_Views.sql: MySQL数据库兼容性数据字典及视图 //TODO
* MySQL_Functions.sql: MySQL数据库兼容性函数
* DB2_Functions.sql: DB2数据库兼容性函数

<br/>

## 使用说明

直接在MogDB数据库中执行sql文本。

> 注意：以下所有操作需要在当前脚本所在的目录中执行。

### 创建所有兼容性对象

创建所有兼容性对象，包括：视图，函数，管理包等。

```
# 本地用户，默认库登录
gsql -f runMe.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限）
gsql -h 127.0.0.1 -p 5432 -U test -d postgres -f runMe.sql
```

### 创建单一类兼容对象

以下以 Oracle 兼容性视图为例，其他兼容性对象脚本，也可以以此方式创建。

```
# 本地用户，默认库登录
gsql -f Oracle_Views.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限）
gsql -h 127.0.0.1 -p 5432 -U test -d postgres -f Oracle_Views.sql
```

<br/>

## 结果说明

脚本运行结束后，会自动输出以下两类内容：

1. 本次脚本中涉及到的兼容对象，以及对应的操作内容

   ```
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- Compat Object List:
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:     | type      | name                                             | version | language | operation           |
   gsql:Oracle_Functions.sql:1035: NOTICE:     |-----------|--------------------------------------------------|---------|----------|---------------------|
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | months_between(timestamptz,timestamptz)          | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | sys_guid()                                       | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | aggregate | wm_concat(text)                                  | 1.0     | internal | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | nvl2(anyelement,anyelement,anyelement)           | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | trunc(timestamp,text)                            | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | trunc(timestamptz,text)                          | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | nanvl(numeric,numeric)                           | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_substr(text,text,int4)                    | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_substr(text,text,int4,int4,text,int4)     | 2.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_instr(text,text,int4,int4,int4,text,int4) | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_replace(text,text,text,int4,int4,text)    | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_count(text,text,int4,text)                | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | replace(text,text)                               | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | ora_hash(anyelement,int4,int4)                   | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | show(text)                                       | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | show_parameter(text)                             | 1.0     | sql      | Skip due to version |
   ```

2. 本次脚本中内置的测试用例的运行汇总

   确认所有测试用例都是最新执行，并且没有类型为 `FAILED` 的测试用例即可。

   ```
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- Function Test Result:
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:     | result_type | case_count | start_time                 | complete_time              |
   gsql:Oracle_Functions.sql:1035: NOTICE:     |-------------|------------|----------------------------|----------------------------|
   gsql:Oracle_Functions.sql:1035: NOTICE:     | PASSED      |        387 | 2021-11-25 09:42:20.016619 | 2021-11-25 09:42:52.127892 |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | NULL        |          6 | 2021-11-25 09:42:20.016619 | 2021-11-25 09:42:20.016619 |
   ```

### 查询已创建的兼容对象及版本

```sql
select * from compat_tools.compat_version;
```

### 查询测试用例详情

```sql
select * from compat_tools.compat_testing;
```

<br/>

## Oracle 兼容视图

Oracle_Views.sql 包含如下几类 Oracle 兼容性视图：

### 管理视图： ALL_\*, DBA_\*, USER_\*

> ALL\_ 视图当前与 DBA\_ 视图一致，并未做权限检验，后期版本将会考虑权限校验。

* [DBA,ALL,USER]_CATALOG
* [DBA,ALL,USER]_CONS_COLUMNS
* [DBA,ALL,USER]_CONSTRAINTS
* [DBA,ALL,USER]_DATA_FILES
* [DBA,ALL,USER]_DEPENDENCIES
* [DBA,ALL,USER]_IND_COLUMNS
* [DBA,ALL,USER]_IND_PARTITIONS
* [DBA,ALL,USER]_IND_STATISTICS
* [DBA,ALL,USER]_INDEX_USAGE
* [DBA,ALL,USER]_INDEXES
* [DBA,ALL,USER]_OBJECTS
* [DBA,ALL,USER]_PART_INDEXES
* [DBA,ALL,USER]_PART_TABLES
* [DBA,ALL,USER]_PROCEDURES
* [DBA,ALL,USER]_SEGMENTS
* [DBA,ALL,USER]_SEQUENCES
* [DBA,ALL,USER]_SOURCE
* [DBA,ALL,USER]_SYNONYMS
* [DBA,ALL,USER]_TAB_COL_STATISTICS
* [DBA,ALL,USER]_TAB_COLS
* [DBA,ALL,USER]_TAB_COLUMNS
* [DBA,ALL,USER]_TAB_COMMENTS
* [DBA,ALL,USER]_TAB_MODIFICATIONS
* [DBA,ALL,USER]_TAB_PARTITIONS
* [DBA,ALL,USER]_TAB_STATISTICS
* [DBA,ALL,USER]_TABLES
* [DBA,ALL,USER]_TABLESPACES
* [DBA,ALL,USER]_TRIGGER_COLS
* [DBA,ALL,USER]_TRIGGERS
* [DBA,ALL,USER]_TYPES
* [DBA,ALL,USER]_VIEWS
* [DBA,ALL,USER]_JOBS
* [DBA,ALL,USER]_JOBS_RUNNING
* [DBA,ALL]_USERS
* DBA_SOURCE_ALL
* NLS_DATABASE_PARAMETERS
* NLS_INSTANCE_PARAMETERS
* NLS_SESSION_PARAMETERS

### 其他短名称视图

* DICTIONARY
* DICT
* COLS
* IND
* OBJ
* TAB
* DUAL (For 2.0 and above)

### 动态性能视图： GV,V

* [GV,V]$DATAFILE
* [GV,V]$LOCK
* [GV,V]$PARAMETER
* [GV,V]$PARAMETER_VALID_VALUES
* [GV,V]$SESSION
* [GV,V]$SESSTAT
* [GV,V]$SPPARAMETER
* [GV,V]$SYSSTAT
* [GV,V]$TABLESPACE
* [GV,V]$VERSION
* [GV,V]$NLS_PARAMETERS
* [GV,V]$NLS_VALID_VALUES

### 权限统一查询视图

* DBA_DETAIL_PRIVILEGES
* DBA_ALL_PRIVILEGES
* DBA_ALL_PRIVILEGES_SQL

<br/>

## Oracle 兼容函数

### 字符函数

* regexp_substr(text,text,int4)
* regexp_substr(text,text,int4,int4,text,int4) -- 注意： 最后一个参数 p_subexpr 不支持
* regexp_instr(text,text,int4,int4,text,int4)
* regexp_replace(text,text,text,int4,int4,text)
* regexp_count(text,text,int4,text)
* replace(text,text)

### 时间函数

* months_between(timestamp, timestamp)
* trunc(timestamp, text)
* trunc(timestamptz, text)
* round(timestamptz,text)

### 数字函数

* nanvl(numeric,numeric)

### 聚合函数

* wm_concat(text)

### NULL 值相关函数

* nullif(anyelement, anyelement)
* nvl2(anyelement, anyelement, anyelement)

### 其他函数

* sys_guid()
* ora_hash(anyelement,bigint,bigint)
* show(text) -- 提供类似 Oracle 中 show xxx 查看相关参数的功能
* show_parameter(text) -- 等同于 show(text)
* dump(anynonarray)

<br/>

## Oracle 管理包

### DBMS_METADATA

* get_ddl(object_type, object_name, schema_name)

### DBMS_OUTPUT

* enable(size)
* disable()
* get_line(line, status)
* get_lines(lines, numlines)
* new_line()
* put(text)
* put_line(text)

### DBMS_RANDOM

* initialize(int4) -- deprecated in Oracle
* normal()
* random() -- deprecated in Oracle
* seed(int4)
* seed(text)
* string(char,int4)
* terminate() -- deprecated in Oracle
* value()
* value(numeric,numeric)

关于如何使用DBMS_RANDOM在MogDB数据库中生成随机数（数字、字符串和日期），请参见[DBMS_RANDOM](DBMS-RANDOM.md)。

### DBMS_JOB

- broken(int8,bool,timestamp)
- change(int8,text,timestamp,text,int4,bool)
- instance(int8,int4,bool)
- interval(int8,text)
- next_date(int8,timestamp)
- remove(int8)
- run(int8,bool)
- submit(int8,text,timestamp,text,bool,int4,bool)
- user_export(int8,text)
- what(int8,text)

<br/>

## MySQL 兼容函数

### NULL 相关函数

> 注意： ifnull 效果等同于通用函数 coalesce，在条件允许的情况下，建议修改 SQL 使用 coalesce 函数，该函数几乎在各个数据库中均支持

- ifnull(text,text)
- ifnull(numeric,numeric)
- ifnull(timestamp,timestamp)
- ifnull(timestamptz,timestamptz)
- isnull(text) -- 注意： MogDB 中如果创建数据库时，使用默认的 A 兼容模式，那么 '' 空字符串也会被识别为 NULL 值
- isnull(numeric)
- isnull(timestamptz)

### 条件控制函数

- if(bool,text,text)
- if(bool,numeric,numeric)
- if(bool,timestamptz,timestamptz)

### 字符函数

- find_in_set(text,text)
- find_in_set(text,text[])
- field(text,text[])
- elt(int4,text[])
- strcmp(text,text)
- insert(text,int8,int8,text)
- lcase(text)
- ucase(text)
- space(int4)
- mid(text,int8,int8)
- locate(text,text,int4)
- to_base64(text)
- from_base64(text)

### 数字函数

- field(numeric,numeric[])
- log10(numeric)
- log10(float8)
- rand(int4)

### 时间函数

- unix_timestamp(timestamp)
- unix_timestamp(timestamptz)
- from_unixtime(int8)
- from_unixtime(numeric)
- from_unixtime(numeric,text)
- to_days(timestamp)
- to_seconds(timestamp)
- to_seconds(timestamptz)
- timediff(timestamptz,timestamptz)
- time_to_sec(time)
- sec_to_time(int4)
- date_format(timestamp, text)
- date_format(timestamptz, text)
- timestampdiff(text,timestamptz,timestamptz) -- 注意： 使用必须带 pg_catalog. 前缀Schema，第一个单位参数必须使用单引号包裹
- str_to_date(text,text) -- 由于 MySQL 与 MogDB 中时间类型的差异，本兼容函数并非完全一致性的兼容，详细请查阅对应函数尾部的用例描述部分

### 其他函数

- uuid() -- 提供基于 随机值+时间戳 的哈希计算 uuid
- uuid_to_bin(uuid, int4)
- bin_to_uuid(bytea, int4)

<br/>

## DB2 兼容函数

### NULL 相关函数

> 注意： value 效果等同于通用函数 coalesce，在条件允许的情况下，建议修改 SQL 使用 coalesce 函数，该函数几乎在各个数据库中均支持

- value(text,text)
- value(numeric,numeric)
- value(timestamp,timestamp)
- value(timestamptz,timestamptz)

### 字符函数

- posstr(text,text)
- locate_in_string(text,text,int4,int4,text)
- regexp_match_count(text,text,int4,text,text)

### 时间函数

- year (text)
- year (timestamptz)
- year (interval)
- month (text)
- month (timestamptz)
- month (interval)
- quarter (timestamptz)
- week (timestamptz)
- day (text)
- day (timestamptz)
- day (interval)
- hour (text)
- hour (timestamptz)
- hour (interval)
- minute (text)
- minute (timestamptz)
- minute (interval)
- second (text)
- second (timestamptz)
- second (interval)
- days (timestamptz)
- dayofyear (timestamptz)
- dayofweek (timestamptz)
- dayofweek_iso (timestamptz)
- dayname (timestamptz)
- monthname (timestamptz)
- midnight_seconds (timestamptz)
- next_day (timestamptz,text,text)
- next_month (timestamptz)
- next_quarter (timestamptz)
- next_week (timestamptz)
- next_year (timestamptz)
- last_day (timestamptz)
- first_day (timestamptz)
- this_month (timestamptz)
- this_quarter (timestamptz)
- this_week (timestamptz)
- this_year (timestamptz)
- days_between (timestamptz,timestamptz)
- years_between (timestamptz,timestamptz)
- ymd_between (timestamptz,timestamptz)
