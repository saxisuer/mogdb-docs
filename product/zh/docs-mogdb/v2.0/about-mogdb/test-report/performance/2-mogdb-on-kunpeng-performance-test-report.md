---
title: MogDB on鲲鹏性能测试
summary: MogDB on鲲鹏性能测试
author: Liu Xu
date: 2021-03-04
---

# MogDB on鲲鹏性能测试

## 测试目的

本文主要讲述了对MogDB 2.0.0 版本on鲲鹏进行的单节点和一主多备（同步备、异步备）的测试。

<br/>

## 测试环境

### 环境配置

| 类别     | 服务端配置            | 客户端配置    | 数量  |
| -------- | --------------------- | ------------- | ----- |
| CPU      | Kunpeng 920           | Kunpeng 920   | 128   |
| 内存     | DDR4,2933MT/s         | DDR4,2933MT/s | 2048G |
| 硬盘     | Nvme 3.5T             | Nvme 3T       | 4个   |
| 文件系统 | Xfs                   | Xfs           | 4     |
| OS       | openEuler 20.03 (LTS) | Kylin V10     |       |
| 数据库   | MogDB 1.1.0安装包     |               |       |

<br/>

### 测试工具

| 工具名称        | 用途                 |
| ------------- | ---------------------------------|
| Benchmarksql5.0 | BenchmarkSQL是对OLTP数据库主流测试标准TPC-C的开源JAVA实现,用于对数据库事务处理能力的评估测试。 |

<br/>

## 测试步骤

### MogDB数据库端操作

1. 获取数据库安装包

2. 安装数据库

3. 创建tpcc测试用户和数据库

    ```bash
    create user [username] identified by 'passwd';
    grant [origin user] to [username];
    create database [dbname];
    ```

4. 停止数据库修改数据库配置文件postgresql.conf数据库，在末尾添加配置参数（单节点测试追加参数如下）

    ```bash
    max_connections = 4096
    allow_concurrent_tuple_update = true
    audit_enabled = off
    checkpoint_segments = 1024
    cstore_buffers =16MB
    enable_alarm = off
    enable_codegen = false
    enable_data_replicate = off
    full_page_writes  = off
    max_files_per_process = 100000
    max_prepared_transactions = 2048
    shared_buffers = 350GB
    use_workload_manager = off
    wal_buffers = 1GB
    work_mem = 1MB
    log_min_messages  = FATAL
    transaction_isolation = 'read committed'
    default_transaction_isolation = 'read committed'
    synchronous_commit = on
    fsync  = on
    maintenance_work_mem = 2GB
    vacuum_cost_limit = 2000
    autovacuum = on
    autovacuum_mode = vacuum
    autovacuum_max_workers = 5
    autovacuum_naptime = 20s
    autovacuum_vacuum_cost_delay =10
    xloginsert_locks = 48
    update_lockwait_timeout =20min
    enable_mergejoin = off
    enable_nestloop = off
    enable_hashjoin = off
    enable_bitmapscan = on
    enable_material = off
    wal_log_hints = off
    log_duration = off
    checkpoint_timeout = 15min
    enable_save_datachanged_timestamp =FALSE
    enable_thread_pool = on
    thread_pool_attr =  '812,4,(cpubind:0-27,32-59,64-91,96-123)'
    enable_double_write = on
    enable_incremental_checkpoint = on
    enable_opfusion = on
    advance_xlog_file_num = 10
    numa_distribute_mode = 'all'
    track_activities = off
    enable_instr_track_wait = off
    enable_instr_rt_percentile = off
    track_counts =on
    track_sql_count = off
    enable_instr_cpu_timer = off
    plog_merge_age = 0
    session_timeout = 0
    enable_instance_metric_persistent = off
    enable_logical_io_statistics = off
    enable_user_metric_persistent =off
    enable_xlog_prune = off
    enable_resource_track = off
    instr_unique_sql_count = 0
    enable_beta_opfusion = on
    enable_beta_nestloop_fusion = on
    autovacuum_vacuum_scale_factor = 0.02
    autovacuum_analyze_scale_factor = 0.1
    client_encoding = UTF8
    lc_messages = en_US.UTF-8
    lc_monetary = en_US.UTF-8
    lc_numeric = en_US.UTF-8
    lc_time = en_US.UTF-8
    modify_initial_password = off
    ssl = off
    enable_memory_limit = off
    data_replicate_buffer_size = 16384
    max_wal_senders = 8
    log_line_prefix = '%m %u %d %h %p %S'
    vacuum_cost_limit = 10000
    max_process_memory = 12582912
    recovery_max_workers = 1
    recovery_parallelism = 1
    explain_perf_mode = normal
    remote_read_mode = non_authentication
    enable_page_lsn_check = off
    pagewriter_sleep = 100
    ```

<br/>

### benchmarksql端操作

1. 修改配置文件

    进入benchmarksql安装目录下，找到run目录下的配置文件[config file]

    ```text
    db=postgres

    driver=org.postgresql.Driver

    conn=jdbc:postgresql://[ip:port]/tpcc?prepareThreshold=1&batchMode=on&fetchsize=10

    user=[user]

    password=[passwd]

    warehouses=1000

    loadWorkers=80

    terminals=812

    //To run specified transactions per terminal- runMins must equal zero

    runTxnsPerTerminal=0

    //To run for specified minutes- runTxnsPerTerminal must equal zero

    runMins=30

    //Number of total transactions per minute

    limitTxnsPerMin=0

    //Set to true to run in 4.x compatible mode. Set to false to use the

    //entire configured database evenly.

    terminalWarehouseFixed=false  #true

    //The following five values must add up to 100

    //The default percentages of 45, 43, 4, 4 & 4 match the TPC-C spec

    newOrderWeight=45

    paymentWeight=43

    orderStatusWeight=4

    deliveryWeight=4

    stockLevelWeight=4

    // Directory name to create for collecting detailed result data.

    // Comment this out to suppress.

    //resultDirectory=my_result_%tY-%tm-%td_%tH%tM%tS

    //osCollectorScript=./misc/os_collector_linux.py

    //osCollectorInterval=1

    //osCollectorSSHAddr=tpcc@127.0.0.1

    //osCollectorDevices=net_eth0 blk_sda blk_sdg blk_sdh blk_sdi blk_sdj
    ```

2. 运行runDataBuild.sh生成数据

    ```
    ./runDatabaseBuild.sh [config file]
    ```

3. 运行runBenchmark.sh测试数据库

    ```
    ./runBenchmark.sh [config file]
    ```

<br/>

### 操作系统配置

1. 修改操作系统内核PAGESIZE（仅EulerOS需执行此步骤）

    **安装kernel-4.19.36-1.aarch64.rpm (*)**

    ```
    # rpm -Uvh --force --nodeps kernel-4.19.36-1.aarch64.rpm

    *: 此文件是基于linux 4.19.36的内核包，获取路径如下:

    10.44.133.121 (root/Huawei12#$)

    /data14/xy_packages/kernel-4.19.36-1.aarch64.rpm
    ```

    **修改操作系统内核配置文件中root的启动项**

    ```
    # vim /boot/efi/EFI/euleros/grubenv  //修改前备份grubenv文件

    # GRUB Environment Block

    saved_entry=EulerOS (4.19.36) 2.0 (SP8)   -- 此处修改为4.19.36
    ```

<br/>

### 文件系统

1. 修改xfs文件系统blocksize为8K

    \# 下面命令查看当前挂载的nvme盘

    ```
    # df -h | grep nvme
    /dev/nvme0n1         3.7T  2.6T  1.2T  69% /data1
    /dev/nvme1n1         3.7T  1.9T  1.8T  51% /data2
    /dev/nvme2n1         3.7T  2.2T  1.6T  59% /data3
    /dev/nvme3n1         3.7T  1.4T  2.3T  39% /data4

    xfs_info命令可以查看nvme盘的信息

    # xfs_info /data1
    ```

2. 针对需要修改的磁盘，备份所需的数据

3. 重新格式化磁盘

    ```
    以/dev/nvme0n1盘，加载点为/data1为例，相关参考命令如下

    umount /data1

    mkfs.xfs -b size=8192 /dev/nvme0n1 -f

    mount /dev/nvme0n1 /data1
    ```

<br/>

## 测试项及结论

### 测试结果汇总

| 测试项   | 数据量 | 并发数 | CPU平均使用率 | IOPS   | IO延时   | WAL数量 | tpmC       | 测试时长 |
| ------ | ------ | ------ | --------- | ------ | -------- | ------- | ---------- | -------- |
| 单节点   | 100G   | 500    | 77.49%        | 17.96K | 819.05us | 13260   | 1567226.12 | 10分钟   |
| 一主一备 | 100G   | 500    | 57.64%        | 5.31K  | 842.78us | 13272   | 1130307.87 | 10分钟   |
| 一主两备 | 100G   | 500    | 60.77%        | 5.3K   | 821.66us | 14324   | 1201560.28 | 10分钟   |

<br/>

### 单节点

- tpmC

    ![tpmC](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-1.png)

- 系统数据

    ![系统数据](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-2.png)

<br/>

### 一主一备

- tpmC

    ![tpmC](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-3.png)

- 系统数据

    ![系统数据](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-4.png)

<br/>

### 一主两备

- tpmC

    ![tpmC](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-5.png)

- 系统数据

    ![系统数据](https://cdn-mogdb.enmotech.com/docs-media/mogdb/about-mogdb/mogdb-on-kunpeng-6.png)
