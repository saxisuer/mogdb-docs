---
title: 功能迭代计划
summary: 功能迭代计划
author: Guo Huan
date: 2021-04-14
---

# 功能迭代计划

本文档主要介绍MogDB 2.0.1的功能迭代计划。

<table>
    <tr>
        <th>产品组件修改</th>
        <th>未合并的内容</th>
    </tr>
    <tr>
        <td rowspan="4">MogHA</td>
        <td>新增支持主库实例自动重启</td>
    </tr>
    <tr>
        <td>新增支持数据库cpu使用限额</td>
    </tr>
    <tr>
        <td>新增支持HA切换自定义操作</td>
    </tr>
    <tr>
        <td>新增配套WEB界面，api等，方便小规模部署使用（EA阶段）</td>
    </tr>
    <tr>
        <td rowspan="10">MogDB Server</td>
        <td>支持主库设置为全局只读，可用于网络隔离情况下的防脑裂处理</td>
    </tr>
    <tr>
        <td>支持pg trgm插件（全文索引插件）</td>
    </tr>
    <tr>
        <td>支持自定义操作符，自定义操作符类</td>
    </tr>
    <tr>
        <td>降低数据库指标采集的资源消耗</td>
    </tr>
    <tr>
        <td>分区创建语法优化</td>
    </tr>
     <tr>
        <td>优化对CJK（东亚字符集）的排序支持，针对大数据量排序有更好的表现</td>
    </tr>
     <tr>
        <td>支持基于xid的闪回查询</td>
    </tr>
     <tr>
        <td>二级分区（sub partition)</td>
    </tr>
     <tr>
        <td>citus 插件支持， 横向扩展的分布式能力</td>
    </tr>
     <tr>
        <td>在线重建索引（online reindex), 重建索引不锁表</td>
    </tr>
    <tr>
        <td rowspan="2">MogDB Client</td>
        <td>go客户端：新增支持go的sha256加密算法支持</td>
    </tr>
    <tr>
        <td>python客户端：新增支持sha256加密算法，支持主库自动识别</td>
    </tr>
    <tr>
        <td rowspan="4">MogDB plugin</td>
        <td>wal2json 插件兼容支持 用于现在的逻辑日志导出以及异构复制</td>
    </tr>
    <tr>
        <td>pg_dirtyread 插件支持 用于特殊情况下的数据恢复和回查</td>
    </tr>
    <tr>
        <td>walminer 插件支持 用于在线的wal日志解析（不依赖逻辑日志）</td>
    </tr>
    <tr>
        <td>db_link 插件支持，用于从MogDB连接PostgreSQL数据库</td>
    </tr>
</table>
