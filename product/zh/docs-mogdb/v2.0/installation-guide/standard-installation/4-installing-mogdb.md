---
title: 安装MogDB
summary: 安装MogDB
author: Zhang Cuiping
date: 2021-04-2
---

# 安装MogDB

## 创建XML配置文件

安装MogDB前需要创建XML文件。XML文件包含部署MogDB的服务器信息、安装路径、IP地址以及端口号等。用于告知MogDB如何部署。用户需根据不同场配置对应的XML文件。

<br/>

**参数说明**

<table>  <tr>  <td>  <b>实例类型</b></td>   <td> <b>参数</b> </td>   <td> <b>说明</b></td>   </tr>   <tr>  <td rowspan="10">  整体信息</td>    </tr>   <tr>  <td>  clusterName</td>   <td> MogDB名称。</td>   </tr>   <tr>  <td>  nodeNames</td>   <td> MogDB中主机名称。</td>   </tr>   <tr>  <td>  backIp1s</td>   <td> 主机在后端存储网络中的IP地址（内网IP）。所有MogDB主机使用后端存储网络通讯。</td>   </tr>   <tr>  <td>  gaussdbAppPath</td>   <td> MogDB程序安装目录。此目录应满足如下要求：<br />- 磁盘空间&gt;1GB<br />- 与数据库所需其它路径相互独立，没有包含关系。</td>   </tr>   <tr>  <td>  gaussdbLogPath</td>   <td> MogDB运行日志和操作日志存储目录。此目录应满足如下要求：<br />- 磁盘空间建议根据主机上的数据库节点数规划。数据库节点预留1GB空间的基础上，再适当预留冗余空间。<br />- 与MogDB所需其它路径相互独立，没有包含关系。<br /> 此路径可选。不指定的情况下，MogDB安装时会默认指定“$GAUSSLOG/安装用户名”作为日志目录。 </td>   </tr>   <tr>  <td>  tmpdbPath</td>   <td> 数据库临时文件存放目录。<br /> 若不配置tmpdbPath，默认存放在/opt/mogdb/tools/perfadm_db目录下。 </td>   </tr>   <tr>  <td>  gaussdbToolPath</td>   <td> MogDB系统工具目录，主要用于存放互信工具等。此目录应满足如下要求：<br />- 磁盘空间&gt;100MB <br />- 固定目录，与数据库所需其它目录相互独立，没有包含关系。<br /> 此目录可选。不指定的情况下，MogDB安装时会默认指定“/opt/mogdb/tools”作为数据库系统工具目录。 </td>   </tr>   <tr>  <td>  corePath</td>   <td> MogDB core文件的指定目录。</td>   </tr>   <tr>  <td>  clusterType</td>   <td> MogDB类型，MogDB拓扑类型；可选字段。<br /> “single-inst”表示单机一主多备部署形态。 </td>   </tr>   </table>

### 示例

#### 单节点配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1" />
        <PARAM name="backIp1s" value="192.168.0.11"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>

<!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1"/>
        </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。

#### 一主一备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1,node2" />
        <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1"/>
        </DEVICE>

        <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000002">
            <PARAM name="name" value="node2"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.12"/>
            <PARAM name="sshIp1" value="192.168.0.12"/>
    </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。

#### 一主二备配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ROOT>
    <!-- MogDB整体信息 -->
    <CLUSTER>
        <PARAM name="clusterName" value="dbCluster" />
        <PARAM name="nodeNames" value="node1,node2,node3" />
        <PARAM name="backIp1s" value="192.168.0.11,192.168.0.12, 192.168.0.13"/>
        <PARAM name="gaussdbAppPath" value="/opt/mogdb/app" />
        <PARAM name="gaussdbLogPath" value="/var/log/mogdb" />
        <PARAM name="gaussdbToolPath" value="/opt/mogdb/tools" />
        <PARAM name="corePath" value="/opt/mogdb/corefile"/>
        <PARAM name="clusterType" value="single-inst"/>
    </CLUSTER>
    <!-- 每台服务器上的节点部署信息 -->
    <DEVICELIST>
        <!-- node1上的节点部署信息 -->
        <DEVICE sn="1000001">
            <PARAM name="name" value="node1"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.11"/>
            <PARAM name="sshIp1" value="192.168.0.11"/>

        <!--dbnode-->
        <PARAM name="dataNum" value="1"/>
        <PARAM name="dataPortBase" value="26000"/>
        <PARAM name="dataNode1" value="/mogdb/data/db1,node2,/mogdb/data/db1, node3,/mogdb/data/db1"/>
        </DEVICE>

        <!-- node2上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000002">
            <PARAM name="name" value="node2"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.12"/>
            <PARAM name="sshIp1" value="192.168.0.12"/>
    </DEVICE>
        <!--node3上的节点部署信息，其中“name”的值配置为主机名称 -->
        <DEVICE sn="1000003">
            <PARAM name="name" value="node3"/>
            <PARAM name="azName" value="AZ1"/>
            <PARAM name="azPriority" value="1"/>
            <!-- 如果服务器只有一个网卡可用，将backIP1和sshIP1配置成同一个IP -->
            <PARAM name="backIp1" value="192.168.0.13"/>
            <PARAM name="sshIp1" value="192.168.0.13"/>
    </DEVICE>
    </DEVICELIST>
</ROOT>
```

> 说明：替换主机名和IP，其他参数默认即可。/mogdb/data/db1为数据存储路径，可根据需要替换。

<br/>

## 下载安装包

安装前可访问[MogDB下载页面](https://www.mogdb.io/downloads/mogdb)下载适用于您当前操作系统和CPU的安装包。以CentOS系统为例，各安装包及功能介绍见下表。

| **安装包**                            | **功能**                                                     |
| ------------------------------------- | ------------------------------------------------------------ |
| MogDB-2.0.3-CentOS-all-x86_64.tar     | 包含所有安装包所有文件                                       |
| MogDB-2.0.3-CentOS-64bit.tar.bz2      | （主备）MogDB主备（Datanode）。负责存储业务数据、执行数据查询任务以及向客户端返回执行结果。MogDB实例包含主、备两种类型，支持一主多备。建议将主、备MogDB实例分散部署在不同的物理节点中。 |
| MogDB-2.0.3-CentOS-64bit.sha256       | MogDB-2.0.3-CentOS-64bit.tar.bz2的sha256的hash值             |
| MogDB-2.0.3-CentOS-64bit-Libpq.tar.gz | C程序访问MogDB数据库的驱动                                   |
| MogDB-2.0.3-CentOS-64bit-om.tar.gz    | 运维管理模块（Operation  Manager）。提供数据库日常运维、配置管理的管理接口、工具。 |
| MogDB-2.0.3-CentOS-64bit-om.sha256    | MogDB-2.0.3-CentOS-64bit-om.tar.gz的sha256的hash值           |
| MogDB-2.0.3-CentOS-64bit-tools.tar.gz | 客户端驱动（Client  Driver）。负责接收来自应用的访问请求，并向应用返回执行结果。客户端驱动负责与MogDB实例通信，发送应用的SQL命令，接收MogDB实例的执行结果 |
| upgrade_sql.tar.gz                    | 升级所需要的SQL                                              |
| upgrade_sql.sha256                    | upgrade_sql.tar.gz的sha256的hash值                           |

<br/>

## 环境初始化

下文以CentOS 7.6系统为例介绍安装过程。

**操作步骤**

1. 以root用户执行以下命令安装所需的基础环境包，以避免后续安装过程中因系统采用最小化安装或者安装包不全导致的报错。

   ```bash
   yum install -y bzip2 libaio-devel flex bison ncurses-devel glibc-devel libxml2-devel patch redhat-lsb-core unzip gcc gcc-c++ perl openssl-devel libffi-devel libtool zlib-devel
   ```

2. 以root用户登录待安装MogDB的任意主机，并按规划创建存放安装包的目录，下文以`/opt/software/mogdb`为例。

   ```bash
   mkdir -p /opt/software/mogdb
   chmod 755 -R /opt/software
   ```

3. 进入`/opt/software/mogdb`，将安装包及配置文件`clusterconfig.xml`上传至此目录。若未创建配置文件，可通过`vi clusterconfig.xml`新建并编辑配置文件。

   ```bash
   cd /opt/software/mogdb
   ```

   > 说明：
   > 在允许访问外网的环境中，可通过`wget`命令直接下载适用于当前操作系统和CPU的安装包：
   >
   > ```bash
   > wget -c https://cdn-mogdb.enmotech.com/mogdb-media/2.0.3/MogDB-2.0.3-CentOS-x86_64.tar
   > ```

4. 在安装包所在的目录下，依次解压安装包。

   ```bash
   tar -xvf  MogDB-2.0.3-CentOS-x86_64.tar
   tar -xvf  MogDB-2.0.3-CentOS-64bit-om.tar.gz
   tar -xf  MogDB-2.0.3-CentOS-64bit.tar.bz2
   ```

5. 进入到工具脚本存放目录下。

   ```bash
   cd /opt/software/mogdb/script
   ```

6. 为确保openssl版本正确，执行预安装前请加载安装包中lib库。执行命令如下，其中`{packagePath}`为用户安装包放置的路径，本示例中为`/opt/software/mogdb`。

   ```bash
   export LD_LIBRARY_PATH={packagePath}/script/gspylib/clib:$LD_LIBRARY_PATH
   ```

7. 为确保成功安装，请执行如下命令检查主机名称是否一致。预安装过程中，会对hostname进行检查。

   ```bash
   hostname
   vi /etc/hostname
   ```

8. 若您使用的为openEuler系统，需打开`performance.sh`文件注释`#sysctl -w vm.min_free_kbytes=112640 &> /dev/null`

   ```bash
   vi /etc/profile.d/performance.sh
   ```

   完成后跳过第9步即可。

9. CentOS系统需要安装python3，请依次执行以下命令。

   注意：CentOS系统中进行标准安装，请务必安装Python 3.6.x版本，目前不支持Python更高版本。

   ```bash
   wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tgz
   mkdir -p /usr/local/python3
   tar -zxvf Python-3.6.5.tgz
   cd Python-3.6.5
   ./configure --prefix=/usr/local/python3 --enable-shared CFLAGS=-fPIC && make && make install
   ln -s /usr/local/python3/bin/python3 /usr/bin/python3
   ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
   ```

   > 说明：
   >
   > * 如遇`zipimport.ZipImportError: can't decompress data; zlib not available`报错，请执行`yum -y install zlib*`安装解压缩类库，之后重新安装。
   >
   > * 如遇`find:'/run/user/1000/gvfs':权限不够`的报错，可忽略。
   >

<br/>

## 初始化脚本

执行以下命令以初始化脚本：

```bash
/opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /opt/software/mogdb/clusterconfig.xml
```

回显如下：

```bash
Parsing the configuration file.
Successfully parsed the configuration file.
Installing the tools on the local node.
Successfully installed the tools on the local node.
Distributing package.
Begin to distribute package to tool path.
Successfully distribute package to tool path.
Begin to distribute package to package path.
Successfully distribute package to package path.
Successfully distributed package.
Installing the tools in the cluster.
Successfully installed the tools in the cluster.
Checking hostname mapping.
Successfully checked hostname mapping.
Checking OS version.
Successfully checked OS version.
Creating cluster's path.
Successfully created cluster's path.
Setting SCTP service.
Successfully set SCTP service.
Set and check OS parameter.
Setting OS parameters.
Successfully set OS parameters.
Set and check OS parameter completed.
Preparing CRON service.
Successfully prepared CRON service.
Preparing SSH service.
Successfully prepared SSH service.
Setting user environmental variables.
Successfully set user environmental variables.
Configuring alarms on the cluster nodes.
Successfully configured alarms on the cluster nodes.
Setting the dynamic link library.
Successfully set the dynamic link library.
Setting Cgroup.
Successfully set Cgroup.
Set ARM Optimization.
Successfully set ARM Optimization.
Setting finish flag.
Successfully set finish flag.
Preinstallation succeeded.
```

> 说明：
>
> - 运行过程中会创建omm用户，输入yes，然后输入相关密码，需保证复杂度要求，密码应包括大小写、字符、数字，例如Enmo@123。
>
> - 如遇以下错误，则表示Python版本过高。
>
>   ```
>   [root@localhost mogdb]# /opt/software/mogdb/script/gs_preinstall -U omm -G dbgrp -X /mogdb/etc/mogdb_clusterconfig.xml
>   Failed to execute cmd: rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so' && rm -rf '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' && cp -r '/opt/software/mogdb/script/gspylib/os/./../../../lib /psutil/_psutil_posix.so_3.9' '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_posix.so' . Error:
>   cp: cannot stat '/opt/software/mogdb/script/gspylib/os/./../../../lib/psutil/_psutil_linux.so_3.9': No such file or directory
>   ```

<br/>

## 执行安装

依次执行以下命令安装MogDB：

```bash
su - omm
gs_install -X /opt/software/mogdb/clusterconfig.xml --gsinit-parameter="--locale=en_US.UTF-8" --gsinit-parameter="--encoding=UTF-8"
```

回显如下：

```bash
Parsing the configuration file.
Check preinstall on every node.
Successfully checked preinstall on every node.
Creating the backup directory.
Successfully created the backup directory.
begin deploy..
Installing the cluster.
begin prepare Install Cluster..
Checking the installation environment on all nodes.
begin install Cluster..
Installing applications on all nodes.
Successfully installed APP.
begin init Instance..
encrypt ciper and rand files for database.
Please enter password for database:
Please repeat for database:
begin to create CA cert files
The sslcert will be generated in /opt/mogdb/cluster/app/share/sslcert/om
Cluster installation is completed.
Configuring.
Deleting instances from all nodes.
Successfully deleted instances from all nodes.
Checking node configuration on all nodes.
Initializing instances on all nodes.
Updating instance configuration on all nodes.
Check consistence of memCheck and coresCheck on DN nodes.
Successful check consistence of memCheck and coresCheck on all nodes.
Configuring pg_hba on all nodes.
Configuration is completed.
Successfully started cluster.
Successfully installed application.
```

> 说明：在执行过程中，用户需根据提示输入数据库的密码，密码具有一定的复杂度，为保证用户正常使用该数据库，请记住输入的数据库密码。

<br/>

## 连接数据库

安装完成后，可使用omm用户通过“gsql -d postgres -p 26000 -r”命令连接MogDB数据库，其中“-p 26000”为数据库端口号，请根据实际情况替换。键入“\copyright”可查看版权信息。

```sql
[omm@ecs-saving-0006 ~]$ gsql -d postgres -p 26000 -r
gsql ((MogDB x.x.x build f892ccb7) compiled at 2021-07-09 16:12:59 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

postgres=# \copyright
MogDB Kernel Database Management System
Copyright (c) Yunhe Enmo (Beijing) Information Technology Co., Ltd. Copyright © 2009-2021 , All rights reserved.
```

<br/>

## 更改同步节点

默认备库为异步，如果想配置同步需在主库做如下配置。

```bash
[omm@ecs-saving-0006 ~]$ sed -i "/synchronous_standby_names/synchronous_standby_names = 'dn_6002'" /mogdb/data/db1/postgresql.conf
[omm@ecs-saving-0006 ~]$ gs_om -t restart
```

dn_6002值比较特殊，其值为固定值，主库为dn_6001，备库1为dn_6002，备库2为dn_6003，依次顺延。

'dn_6002,dn_6003'  表示备库1，2为同步节点。

请替换相应的数据存储路径。
