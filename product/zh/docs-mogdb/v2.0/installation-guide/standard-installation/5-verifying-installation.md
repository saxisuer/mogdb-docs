---
title: 安装验证
summary: 安装验证
author: Zhang Cuiping
date: 2021-04-2
---

# 安装验证

通过MogDB提供的gs_om工具可以完成数据库状态检查。

**前提条件**

MogDB数据库已安装。

**操作步骤**

1. 以omm用户身份登录服务器。

2. 执行如下命令检查数据库状态是否正常，"cluster_state "显示"Normal"表示数据库可正常使用。

    ```
    gs_om -t status
    ```

3. 数据库安装完成后，默认生成名称为postgres的数据库。第一次连接数据库时可以连接到此数据库。

    其中postgres为需要连接的数据库名称，26000为数据库主节点的端口号，即XML配置文件中的dataPortBase的值。请根据实际情况替换。

    ```
    gsql -d postgres -p 26000
    ```

    连接成功后，系统显示类似如下信息表示数据库连接成功。

    ```
    gsql ((MogDB x.x.x build 5be05d82) compiled at 2021-05-28 02:59:43 commit 2143 last mr 131
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.
    ```
