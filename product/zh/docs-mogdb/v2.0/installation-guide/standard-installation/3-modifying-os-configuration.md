---
title: 修改操作系统配置
summary: 修改操作系统配置
author: Zhang Cuiping
date: 2021-04-2
---

# 修改操作系统配置

## 关闭操作系统防火墙

为了在防火墙开启的状态下，确保MogDB的正常使用。用户需要将同MogDB相关的服务、协议、IP以及端口添加到MogDB各主机的防火墙白名单中。

以openEuler操作系统为例，假设MogDB信息如[表1 MogDB信息](#mogdb)所示。

**表 1** MogDB信息 <a id="mogdb"></a>

| 主机名称 | 内部IP       | 外部IP       |
| :------- | :----------- | :----------- |
| plat1    | 192.168.0.11 | 10.10.0.11   |
| plat2    | 192.168.0.12 | 10.10.0.12   |
| plat3    | 192.168.0.13 | 10.10.0.13   |
| plat4    | 192.168.0.14 | 10.10.0.14   |
| 管理网络 | -            | 10.10.64.236 |

**操作步骤**

目前仅支持在防火墙关闭的状态下进行安装。

1. 修改/etc/selinux/config文件中的“SELINUX”值为“disabled”。<a id="1"> </a>

   a. 使用VIM打开config文件。

   ```
   vim /etc/selinux/config
   ```

   b. 修改“SELINUX”的值“disabled”，执行:wq保存并退出修改。

   ```
   SELINUX=disabled
   ```

2. 重新启动操作系统。

   ```
   reboot
   ```

3. 检查防火墙是否关闭。

   ```
   systemctl status firewalld
   ```

   若防火墙状态显示为active (running)，则表示防火墙未关闭，请执行步骤[4](#4)；

   若防火墙状态显示为inactive (dead)，则无需再关闭防火墙。

4. 关闭防火墙并禁止开机重启。<a id="4"> </a>

   ```
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   ```

5. 在其他主机上重复步骤[1](#1)到步骤[4](#4)。

## 设置字符集参数

将各数据库节点的字符集设置为相同的字符集，可以在/etc/profile文件中添加“export LANG=XXX”（XXX为Unicode编码）。

```
vim /etc/profile
```

## 设置时区和时间

将各数据库节点的时区设置为相同时区，可以将/usr/share/zoneinfo/目录下的时区文件拷贝为/etc/localtime文件。

```
cp /usr/share/zoneinfo/$地区/$时区 /etc/localtime
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
> `\$地区/$时区`为需要设置时区的信息，例如：Asia_Shanghai。

使用date -s命令将各主机的时间设置为统一时间，举例如下。

```
date -s "Sat Sep 27 16:00:07 CST 2020"
```

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  可以通过date命令查询主机时区。

## （可选）关闭swap交换内存

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  关闭swap交换内存是为了保障数据库的访问性能，避免把数据库的缓冲区内存淘汰到磁盘上。 如果服务器内存比较小，内存过载时，可打开swap交换内存保障正常运行。

在各数据库节点上，使用swapoff -a命令将交换内存关闭。

```
swapoff -a
```

## 设置网卡MTU值

将各数据库节点的网卡MTU值设置为相同大小。对于x86，MTU值推荐1500；对于ARM，MTU值推荐8192。

```
ifconfig 网卡编号 mtu 值
```

## 关闭RemoveIPC

在各数据库节点上，关闭RemoveIPC。CentOS操作系统无该参数，可以跳过该步骤。

**操作步骤**

1. 修改/etc/systemd/logind.conf文件中的“RemoveIPC”值为“no”。<a id="01"> </a>

   a. 使用VIM打开logind.conf文件。

   ```
   vim  /etc/systemd/logind.conf
   ```

   b. 修改“RemoveIPC”值为“no”。

   ```
   RemoveIPC=no
   ```

2. 修改/usr/lib/systemd/system/systemd-logind.service文件中的“RemoveIPC”值为“no”。

   a. 使用VIM打开systemd-logind.service文件。

   ```
   vim /usr/lib/systemd/system/systemd-logind.service
   ```

   b. 修改“RemoveIPC”值为“no”。

   ```
   RemoveIPC=no
   ```

3. 重新加载配置参数。

   ```
   systemctl daemon-reload
   systemctl restart systemd-logind
   ```

4. 检查修改是否生效。<a id="04"> </a>

   ```
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

5. 在其他主机上重复步骤[1](#01)到步骤[4](#04)。

## 设置root用户远程登录

在MogDB安装时需要root帐户远程登录访问权限，本章介绍如何设置使用root用户远程登录。

1. 修改PermitRootLogin配置，允许用户远程登录。

   a. 打开sshd_config文件。

   ```
   vim /etc/ssh/sshd_config
   ```

   b. 修改权限配置，可以使用以下两种方式实现：

   - 注释掉“PermitRootLogin no”。

     ```
     #PermitRootLogin no
     ```

   - 将“PermitRootLogin”改为“yes”。

     ```
     PermitRootLogin yes
     ```

   c. 执行**:wq**保存并退出编辑页面。

2. 修改Banner配置，去掉连接到系统时，系统提示的欢迎信息。欢迎信息会干扰安装时远程操作的返回结果，影响安装正常执行。

   a. 编辑sshd_config文件。

   ```
   vim /etc/ssh/sshd_config
   ```

   b. 修改Banner配置，注释掉“Banner”所在的行。

   ```
   #Banner XXXX
   ```

   c. 执行**:wq**保存并退出编辑页面。

3. 使用如下命令使设置生效。

   ```
   systemctl restart sshd.service
   ```

4. 以root用户身份重新登录。

   ```
   ssh xxx.xxx.xxx.xxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > xxx.xxx.xxx.xxx为安装MogDB环境的IP地址。

## 动态关闭使用透明大页

echo never &gt; /sys/kernel/mm/transparent_hugepage/enabled

例如 openEuler：

```bash
/root~ # cat /etc/system-release
openEuler release 20.03 (LTS)
/etc/default~ # echo never > /sys/kernel/mm/transparent_hugepage/enabled
/etc/default~ # cat /sys/kernel/mm/transparent_hugepage/enabled
always madvise [never]
```

成功关闭后，显示为 [never]

关闭透明大页可以减少宿主机操作系统的开销，以便获得更稳定的性能。
