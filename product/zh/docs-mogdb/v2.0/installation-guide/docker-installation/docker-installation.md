---
title: 容器化安装
summary: 容器化安装
author: Guo Huan
date: 2021-06-01
---

# 容器化安装

本文档主要介绍MogDB容器版安装过程。MogDB容器版本不支持 MogHA工具、OM工具，目前最多支持一主八备。<br/>由于考虑尽可能缩减容器镜像的大小，以方便快速下载和部署，从2.0版本开始，容器化MogDB在容器里运行的操作系统在x86和ARM架构下分别运行在Ubuntu和Debian之上。<br/>
x86-64架构的MogDB容器运行在Ubuntu 18.04操作系统中。<br/>
ARM64架构的MogDB容器运行在Debian 10 操作系统中。

<br/>

## 单实例安装

### 准备工作

容器化MogDB的安装与宿主机的操作系统无关，能够运行容器引擎的所有操作系统均可支持，比如Linux，Windows，macOS。
在安装容器版MogDB之前，您需要先安装容器运行时环境，比如Docker。

Docker是一个开源的应用容器引擎，基于Go语言并遵从Apache2.0协议开源。Docker可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的Linux机器上，也可以实现虚拟化。

Docker Desktop下载地址：<https://www.docker.com/products/docker-desktop>

<br/>

### 安装步骤

1. 启动Docker服务。

2. 获取最新版MogDB镜像文件：

   ```shell
   docker pull swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```
   
3. 输入以下命令为MogDB创建运行目录，下文以“mogdb”为例：

   ```bash
   mkdir /mogdb
   ```

4. 继续输入以下命令创建一个新的容器，将容器命名为“mogdb”，以启动MogDB实例：

   ```shell
   docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123  -v /mogdb:/var/lib/opengauss  -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
   ```

   对于Windows系统而言：

   - 如果Docker使用Hyper-V做引擎，请依次执行以下命令，在Hyper-V中创建卷的逻辑对象mogdata，然后创建容器：

     ```shell
     docker volume create mogdata

     docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v mogdata:/var/lib/opengauss -p 15453:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
     ```

   - 如果Docker启用WSL 2做引擎，请执行以下命令创建容器：

     ```bash
     docker run --name mogdb --privileged=true -d -e GS_PASSWORD=Enmo@123 -v C:\mogdb:/var/lib/opengauss -p 15432:5432  swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3
     ```

   > 注意：
   >
   > - MogDB的默认监听启动在容器内的5432端口上，如果想要从容器外部访问数据库，则需要在`docker run`的时候指定`-p`参数。比如以上命令将允许使用15432端口访问容器数据库。
   >- 容器一旦被删除，容器内的所有数据和配置也均会丢失，而从镜像重新运行一个容器的话，则所有数据又都是呈现在初始化状态，因此对于数据库容器来说，为了防止因为容器的消亡或者损坏导致的数据丢失，需要进行持久化存储数据的操作。通过在`docker run`的时候指定`-v`参数来实现。比如以上命令将会指定将MogDB的所有数据文件存储在宿主机的`/mogdb`下。如果使用podman，会有目标路径检查，需要预先创建宿主机目标路径（步骤4）。

5. 进入容器终端：

   ```bash
   docker exec -it mogdb bash
   ```

   至此，MogDB容器版单实例安装完成。

<br/>

### 使用MogDB

安装完成并进入容器后，通过`su - omm`切换为omm用户，即可通过gsql进行数据库访问以正常体验MogDB各项功能：

```bash
root@384ac97543bd:/# su - omm
omm@384ac97543bd:~$ gsql -d postgres
gsql ((MogDB x.x.x build f892ccb7) compiled at 2021-07-09 16:12:59 commit 0 last mr  )
Non-SSL connection (SSL connection is recommended when requiring high-security)
Type "help" for help.

postgres=#
```

<br/>

### 环境变量

为了更灵活地使用MogDB镜像，可以设置额外的参数。未来我们会扩充更多的可控制参数，当前版本支持以下变量的设定。

**GS_PASSWORD**

使用MogDB镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了MogDB数据库的超级用户omm以及测试用户mogdb的密码。MogDB安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户mogdb是在entrypoint.sh中自定义创建的用户。

MogDB镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

MogDB的密码有复杂度要求，密码长度8个字符以上，必须同时包含大写字母、小写字母、数字、以及特殊符号（特殊符号仅包含( \# ? ! @ $ % ^ & * \- )，并且 ! $ & 需要用转义符"\\"进行转义。

**GS_NODENAME**

指定数据库节点名称，默认为mogdb。

**GS_USERNAME**

指定数据库连接用户名，默认为mogdb。

**GS_PORT**

指定数据库端口，默认为5432。

<br/>

## 主备安装

### 准备工作

如需进行MogDB容器版主备搭建，请先完成上文“单实例安装”中的步骤。

### 操作步骤

1. 切换到root用户：

   ```bash
   su - root
   ```

2. 创建自定义网络，创建容器便于使用的固定IP：

   ```bash
   docker network create --subnet=172.18.0.0/16 myNetwork
   ```

3. 创建主库容器：

   ```bash
   docker run --name mogdb_master \
   --network myNetwork --ip 172.18.0.10 --privileged=true \
   --hostname mogdb_master --detach \
   --env GS_PORT=6432 \
   --env OG_SUBNET=172.18.0.0/16 \
   --env GS_PASSWORD=Enmo@1234 \
   --env NODE_NAME=mogdb_master \
   --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.10 localport=6439  localservice=6432 remotehost=172.18.0.11 remoteport=6439 remoteservice=6432 '\n" \
   swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3 -M primary
   ```

4. 创建备库容器：

   ```bash
    docker run --name mogdb_slave_one \
    --network myNetwork --ip 172.18.0.11 --privileged=true \
    --hostname mogdb_slave_one --detach \
    --env GS_PORT=6432 \
    --env OG_SUBNET=172.18.0.0/16 \
    --env GS_PASSWORD=Enmotech@1234 \
    --env NODE_NAME=mogdb_slave_one \
    --env REPL_CONN_INFO="replconninfo1 = 'localhost=172.18.0.11 localport=6439  localservice=6432 remotehost=172.18.0.10 remoteport=6439 remoteservice=6432 '\n" \
    swr.cn-north-4.myhuaweicloud.com/mogdb/mogdb:2.0.3 -M standby
   ```

5. 查询主库角色状态：

   ```bash
   docker exec -it mogdb_master bash
   gs_ctl query -D /var/lib/opengauss/data/
   ```

    ![docker-installation-3](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-3.png)

6. 查询备库角色状态：

   ```bash
   docker exec -it mogdb_slave_one bash
   gs_ctl query -D /var/lib/opengauss/data/
   ```

   ![docker-installation-4](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-4.png)

   > 说明：从上面主库Senders信息和备库Receiver可以看到主备状态正常。

### 读写测试

主库写测试：

```bash
gsql -p6432
create table test(ID int);
insert into test values(1);
```

![docker-installation-5](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-5.png)

备库读测试：

```bash
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-6](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-6.png)

> 说明：命令结果显示主库可写，备库可读但不可写。

### 切换测试

将mogdb_slave_one切换为主库，mogdb_master切换为备库。

mogdb_slave_one执行switchover：

```bash
gs_ctl switchover -D /var/lib/opengauss/data/
```

![docker-installation-7](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-7.png)

mogdb_slave_one查询状态：

```bash
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-8](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-8.png)

mogdb_master查询状态：

```bash
gs_ctl query -D /var/lib/opengauss/data/
```

![docker-installation-9](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-9.png)

可以看到mogdb_master变为备库，mogdb_slave_one变为主库，切换成功。

### 数据读写验证

切换后的主库mogdb_slave_one做写入验证：

```bash
gsql -p6432
select * from test;
insert into test values(2);
```

![docker-installation-10](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-10.png)

切换后的备库mogdb_master做读取验证：

```bash
gsql -p6432
select * from test;
delete from test;
```

![docker-installation-11](https://cdn-mogdb.enmotech.com/docs-media/mogdb/installation-guide/docker-installation-11.png)

可以看到，原备库切换为主库后可写，原主库切换为备库后可读但不可写。

<br/>

## 后续步骤

MogDB容器版不支持MogHA工具、OM工具，仅用于测试使用，目前最多支持一主八备。MogDB企业版包含MogHA组件。容器版与企业版基础功能完全一致，在生产环境中建议使用MogDB企业版。
