---
title: 单节点安装
summary: 单节点安装
author: Zhang Cuiping
date: 2021-06-11
---

# 单节点安装

## 前提条件

- 已完成用户组和普通用户的创建。
- 所有服务器操作系统和网络均正常运行。
- 普通用户必须有数据库包解压路径、安装路径的读、写和执行操作权限，并且安装路径必须为空。
- 普通用户对下载的MogDB压缩包有执行权限。
- 安装前请检查指定的MogDB端口是否被占用，如果被占用请更改端口或者停止当前使用端口进程。

## 操作步骤

本节以openEuler系统为例描述如何安装单节点。

1. 使用普通用户登录到MogDB包安装的主机，解压MogDB压缩包到安装目录。

   ```bash
   tar -jxf MogDB-2.0.3-openEuler-64bit.tar.bz2 -C /opt/software/mogdb
   ```

2. 假定解压包的路径为/opt/software/mogdb,进入解压后目录下的simpleInstall。

   ```bash
   cd /opt/software/mogdb/simpleInstall
   ```

3. 执行sh install.sh -w &lt;login password&gt;命令安装MogDB，例如sh install.sh -w Aqz@179。

   ```bash
   sh install.sh  -w xxxx
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
   >
   > - -w：初始化数据库密码（gs_initdb指定）,安全需要必须设置。密码应包括大小写、字符、数字。
   > - -p：指定的MogDB端口号, 如不指定，默认为5432。
   > - -h|-help 打印使用说明。
   > - 安装后，数据库的名称为sgnode。
   > - 安装后，数据库目录安装路径/opt/software/mogdb/data/single_node,其中/opt/software/mogdb为解压包路径，data/single_node为新创建的数据库节点目录。

4. 安装执行完成后，使用ps和gs_ctl查看进程是否正常。

   ```bash
   ps ux | grep mogdb
   gs_ctl query -D /opt/software/mogdb/data/single_node
   ```

   执行ps命令，显示类似如下信息：

   ```
   omm      24209 11.9  1.0 1852000 355816 pts/0  Sl   01:54   0:33 /opt/software/mogdb/bin/gaussdb -D /opt/software/mogdb/single_node
   omm      20377  0.0  0.0 119880  1216 pts/0    S+   15:37   0:00 grep --color=auto mogdb
   ```

   执行gs_ctl命令，显示类似如下信息：

   ```
   gs_ctl query ,datadir is /opt/software/mogdb/data/single_node
   HA state:
       local_role                     : Normal
       static_connections             : 0
       db_state                       : Normal
       detail_information             : Normal
   
   Senders info:
       No information
   
    Receiver info:
   No information
   ```
