---
title: 安装准备
summary: 安装准备
author: Zhang Cuiping
date: 2021-06-11
---

# 安装准备

本章详细介绍MogDB极简安装的环境准备和配置，极简安装包括单节点安装和一主一备节点安装，请在安装之前仔细阅读本章的内容。如果已完成本章节的配置，请忽略。

## 获取安装包

云和恩墨MogDB网站下载页面提供了安装包的获取方式。

安装包下载链接：<https://www.mogdb.io/downloads/mogdb>

**操作步骤**

本节以openEuler系统为例描述如何获取安装包。

1. 上传下载好的标准安装包，并解压以获得极简安装包。

   ```bash
   tar -xvf MogDB-2.0.3-openEuler-arm64.tar
   ```

   > 说明：
   >
   > + 网站上提供标准安装包MogDB-2.0.3-openEuler-arm64.tar，解压标准安装包后得到的MogDB-2.0.3-openEuler-64bit.tar.bz2即为极简安装包。
   > + 在允许访问外网的环境中可通过wget命令直接下载标准安装包：
   >
   >   ```bash
   >   wget -c https://cdn-mogdb.enmotech.com/mogdb-media/2.0.3/MogDB-2.0.3-openEuler-arm64.tar
   >   ```
   >
   >   然后执行tar命令解压即可。

2. 检查安装包。

   解压安装包，检查安装目录及文件是否齐全。在安装包所在目录执行以下命令：

   ```
   tar -jxf MogDB-2.0.3-openEuler-64bit.tar.bz2
   ls -1b
   ```

   执行ls命令，显示类似如下信息：

   ```bash
   total 90296
   drwx------ 3 root root     4096 Mar 31 21:18 bin
   drwx------ 3 root root     4096 Mar 31 21:18 etc
   drwx------ 3 root root     4096 Mar 31 21:18 include
   drwx------ 4 root root     4096 Mar 31 21:18 jre
   drwx------ 5 root root     4096 Mar 31 21:18 lib
   -rw------- 1 root root 92427499 Apr  1 09:43 MogDB-2.0.3-openEuler-64bit.tar.bz2
   drwx------ 5 root root     4096 Mar 31 21:18 share
   drwx------ 2 root root     4096 Mar 31 21:18 simpleInstall
   -rw------- 1 root root       32 Mar 31 21:18 version.cfg
   ```

## 准备软硬件安装环境

本章节描述安装前需要进行的环境准备。

### 软硬件环境要求

介绍MogDB的软硬件环境要求。建议部署MogDB的各服务器具有等价的软硬件配置。

**硬件环境要求**

[表1 硬件环境要求](#yingjianhuanjing)列出了MogDB服务器应具备的最低硬件要求。在实际产品中，硬件配置的规划需考虑数据规模及所期望的数据库响应速度。请根据实际情况进行规划。

**表 1** 硬件环境要求 <a id="yingjianhuanjing"> </a>

| 项目     | 配置描述                                                     |
| :------- | :----------------------------------------------------------- |
| 内存     | 功能调试建议32GB以上。<br />性能测试和商业部署时，单实例部署建议128GB以上。<br />复杂的查询对内存的需求量比较高，在高并发场景下，可能出现内存不足。此时建议使用大内存的机器，或使用负载管理限制系统的并发。 |
| CPU      | 功能调试最小1×8 核 2.0GHz。<br />性能测试和商业部署时，建议1×16核 2.0GHz。<br />CPU超线程和非超线程两种模式都支持。<br />说明：<br />个人开发者最低配置2核4G，推荐配置4核8G。<br />目前，MogDB支持鲲鹏920处理器，海光3000、5000、7000系列处理器，兆芯KH30000、KH20000、ZX-C+系列处理器和基于X86_64通用PC服务器的CPU。 |
| 硬盘     | 用于安装MogDB的硬盘需最少满足如下要求：<br />- 至少1GB用于安装MogDB的应用程序。<br />- 每个主机需大约300MB用于元数据存储。<br />- 预留70%以上的磁盘剩余空间用于数据存储。<br />建议系统盘配置为Raid1，数据盘配置为Raid5，且规划4组Raid5数据盘用于安装MogDB。有关Raid的配置方法在本手册中不做介绍。请参考硬件厂家的手册或互联网上的方法进行配置，其中Disk Cache Policy一项需要设置为Disabled，否则机器异常掉电后有数据丢失的风险。<br />MogDB支持使用SSD盘作为数据库的主存储设备，支持SAS接口和NVME协议的SSD盘，以RAID的方式部署使用。 |
| 网络要求 | 300兆以上以太网。<br />建议网卡设置为双网卡冗余bond。有关网卡冗余bond的配置方法在本手册中不做介绍。请参考硬件厂商的手册或互联网上的方法进行配置。 |

**软件环境要求**

**表 2** 软件环境要求

| 软件类型      | 配置描述                                                     |
| :------------ | :----------------------------------------------------------- |
| Linux操作系统 | ARM：<br />- openEuler 20.3LTS（推荐采用此操作系统）<br />- 麒麟V10<br />X86：<br />- openEuler 20.3LTS<br />- CentOS 7.6 |
| rdtscp指令集（x86）  | 执行`lscpu | grep rdtscp`命令查看是否支持rdtscp指令集。                        |
| 工具          | bzip2                                                        |

**软件依赖要求**

MogDB的软件依赖要求如[表3 软件依赖要求](#ruanjianyilai)所示。

建议使用上述操作系统安装光盘或者源中，下列依赖软件的默认安装包，若不存在下列软件，可参看软件对应的建议版本。

**表 3** 软件依赖要求 <a id="ruanjianyilai"> </a>

| 所需软件                      | 建议版本                  |
| :---------------------------- | :------------------------ |
| libaio-devel                  | 建议版本：0.3.109-13      |
| flex                          | 要求版本：2.5.31 以上     |
| bison                         | 建议版本：2.7-4           |
| ncurses-devel                 | 建议版本：5.9-13.20130511 |
| glibc-devel                   | 建议版本：2.17-111        |
| patch                         | 建议版本：2.7.1-10        |
| redhat-lsb-core               | 建议版本：4.1             |
| readline-devel                | 建议版本：7.0-13          |
| libnsl（openeuler+x86环境中） | 建议版本 ：2.28-36        |
| openSSH                       | 建议版本 ：8.4p1          |

### 修改操作系统配置

**关闭操作系统防火墙**

为了在防火墙开启的状态下，确保MogDB的正常使用。用户需要将同MogDB相关的服务、协议、IP以及端口添加到MogDB主机的防火墙白名单中。

以openEuler操作系统为例，假设MogDB信息如[表 4](#biao4)所示。

**表 4** MogDB信息 <a id="biao4"></a>

| 主机名称 | 内部IP       | 外部IP       |
| :------- | :----------- | :----------- |
| plat1    | 192.168.0.11 | 10.10.0.11   |
| 管理网络 | -            | 10.10.64.236 |

目前仅支持在防火墙关闭的状态下进行安装。

1. 修改SELinux运行模式，临时关闭防火墙。

    ```bash
    setenforce 0
    ```

2. 修改/etc/selinux/config文件中的“SELINUX“值为“disabled“。

   a. 使用VIM打开config文件。

   ```bash
   vim /etc/selinux/config
   ```

   b. 修改“SELINUX“的值“disabled“，执行**:wq**保存并退出修改。

   ```bash
   SELINUX=disabled
   ```

3. （可选）重新启动操作系统。

   ```bash
   reboot
   ```

   以下步骤为永久性关闭防火墙。

4. 检查防火墙是否关闭。

   ```bash
   systemctl status firewalld
   ```

   若防火墙状态显示为active (running)，则表示防火墙未关闭，请执行[5](#5)；

   若防火墙状态显示为inactive (dead)，则无需再关闭防火墙。

5. 关闭防火墙。 <a id="5"></a>

   ```bash
   systemctl disable firewalld.service
   systemctl stop firewalld.service
   ```

6. 在其他主机上重复步骤1到步骤5。

**设置字符集参数**

将各数据库节点的字符集设置为相同的字符集，可以在/etc/profile文件中添加”export LANG=XXX”（XXX为Unicode编码）。

```bash
vim /etc/profile
```

**设置时区和时间**

在各数据库节点上，确保时区和时间一致。

1. 执行如下命令检查各数据库节点时间和时区是否一致。如果各数据库节点时间和时区不一致区，请执行[步骤2](#2)~[步骤3](#3)。

   ```bash
   date
   ```

2. 使用如下命令将各数据库节点/usr/share/zoneinfo/目录下的时区文件拷贝为/etc/localtime文件。 <a id="2"></a>

   ```bash
   cp /usr/share/zoneinfo/$地区/$时区 /etc/localtime
   ```

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  *\$地区/\$时区*为需要设置时区的信息，例如：Asia/Shanghai。

3. 使用**date -s**命令将各数据库节点的时间设置为统一时间，举例如下。<a id="3"></a>

   ```bash
   date -s "Sat Sep 27 16:00:07 CST 2020"
   ```

**(可选）关闭swap交换内存**

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  关闭swap交换内存是为了保障数据库的访问性能，避免把数据库的缓冲区内存淘汰到磁盘上。 如果服务器内存比较小，内存过载时，可打开swap交换内存保障正常运行。

在各数据库节点上，使用swapoff -a命令将交换内存关闭。

```bash
swapoff -a
```

**关闭RemoveIPC**

在各数据库节点上，关闭RemoveIPC。CentOS操作系统默认为关闭，可以跳过该步骤。

1. 修改/etc/systemd/logind.conf文件中的“RemoveIPC“值为“no“。 <a id="buzhou1"></a>

   a. 使用VIM打开logind.conf文件。

   ```bash
   vim  /etc/systemd/logind.conf
   ```

   b. 修改“RemoveIPC“的值“no“。

   ```bash
   RemoveIPC=no
   ```

2. 修改/usr/lib/systemd/system/systemd-logind.service文件中的“RemoveIPC“值为“no“。

   a. 使用VIM打开systemd-logind.service文件。

   ```bash
   vim /usr/lib/systemd/system/systemd-logind.service
   ```

   b. 修改“RemoveIPC“的值“no“。

   ```bash
   RemoveIPC=no
   ```

3. 重新加载配置参数。

   ```bash
   systemctl daemon-reload
   systemctl restart systemd-logind
   ```

4. 检查修改是否生效。

   ```bash
   loginctl show-session | grep RemoveIPC
   systemctl show systemd-logind | grep RemoveIPC
   ```

5. 分别打开limits.conf和sysctl.conf配置文件，增加如下对应内容。<a id="buzhou5"></a>

   ```bash
   vim /etc/security/limits.conf
   #limits.conf配置文件增加如下内容：
   omm            soft    nofile          1000000
   omm            hard    nofile          1000000
   vim /etc/sysctl.conf
   #sysctl.conf配置文件增加如下内容：
   kernel.sem = 250 32000 100 999
   ```

6. 在其他主机上重复[步骤1](#buzhou1)到[步骤5](#buzhou5)。
