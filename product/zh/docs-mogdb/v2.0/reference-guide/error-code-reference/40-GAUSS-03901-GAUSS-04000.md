---
title: GAUSS-03901 - GAUSS-04000
summary: GAUSS-03901 - GAUSS-04000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03901 - GAUSS-04000

<br/>

## GAUSS-03901 - GAUSS-03910

<br/>

GAUSS-03903: "Resource Pool '%s': object already defined"

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03906: "Can not alter default_pool"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03907: "cache lookup failed for pg_resource_pool %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03909: "Can not drop default_pool"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03910: "cannot drop resource pool '%s' because other objects depend on it"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03911 - GAUSS-03920

<br/>

GAUSS-03912: "Workload Group '%s': object already defined"

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03915: "Can not alter default_group"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03916: "cache lookup failed for pg_workload_group %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03917: "must be system admin to remove workload groups "

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03918: "Can not drop default_group"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03919: "cannot drop workload group '%s' because other objects depend on it"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03920: "must be system admin to create application workload group mapping"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03921 - GAUSS-03930

<br/>

GAUSS-03921: "Application Workload Group Mapping '%s': object can not create"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03922: "Application Workload Group Mapping '%s': object already defined"

SQLSTATE: 42710

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03924: "must be system admin to change application workload group mapping"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03925: "Application Workload Group Mapping '%s': object not defined"

SQLSTATE: 42704

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03926: "Can not alter default_application"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03927: "cache lookup failed for pg_app_workloadgroup_mapping %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03928: "must be system admin to remove application workload group mapping."

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03929: "Can not drop default_application"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03931 - GAUSS-03940

<br/>

GAUSS-03935: "user mapping not found for '%s'"

SQLSTATE: 42704

错误原因: 初始化Foreign data wrapper出现问题。

解决办法: 请检查使用中的gds、hdfs外表以及roach、obs设置是否正确。若不正确，请联系技术支持工程师提供技术支持。

GAUSS-03936: "foreign-data wrapper handler function %u did not return an FdwRoutine struct"

SQLSTATE: XX000

错误原因: 初始化Foreign data wrapper出现问题。

解决办法: 请检查使用中的gds、hdfs外表以及roach、obs设置是否正确。若不正确，请联系技术支持工程师提供技术支持。

GAUSS-03937: "foreign-data wrapper '%s' has no handler"

SQLSTATE: 55000

错误原因: 初始化Foreign data wrapper出现问题。

解决办法: 请检查使用中的gds、hdfs外表以及roach、obs设置是否正确。若否，请联系技术支持工程师提供技术支持。

GAUSS-03940: "unexpected delimiter"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03941 - GAUSS-03950

<br/>

GAUSS-03941: "unexpected end of line or lexeme"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03943: "unexpected end of line"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03944: "thesaurus sample word '%s' isn't recognized by subdictionary (rule %d)"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03945: "thesaurus sample word '%s' is a stop word (rule %d)"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03946: "thesaurus substitute word '%s' is a stop word (rule %d)"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03947: "thesaurus substitute word '%s' isn't recognized by subdictionary (rule %d)"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03948: "thesaurus substitute phrase is empty (rule %d)"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03949: "multiple DictFile parameters"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03950: "multiple Dictionary parameters"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03951 - GAUSS-03960

<br/>

GAUSS-03951: "unrecognized Thesaurus parameter: '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03952: "missing DictFile parameter"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03953: "missing Dictionary parameter"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03954: "forbidden call of thesaurus or nested call"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03955: "invalid text search configuration file name '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03957: "text search parser does not support headline creation"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03958: "word is too long to be indexed"

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03959: "unrecognized synonym parameter: '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03960: "missing Synonyms parameter"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03961 - GAUSS-03970

<br/>

GAUSS-03962: "multiple AffFile parameters"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03963: "multiple StopWords parameters"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03964: "unrecognized Ispell parameter: '%s'"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03965: "missing AffFile parameter"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03966: "internal error in RS_isRegis: state %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03967: "invalid regis pattern: '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03968: "internal error in RS_compile: state %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03971 - GAUSS-03980

<br/>

GAUSS-03971: "syntax error"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03972: "unrecognized state in parse_affentry: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03974: "could not open affix file '%s': %m"

SQLSTATE: F0000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03977: "multiple Accept parameters"

SQLSTATE: 22023

错误原因: 输入了多个Accept参数。

解决办法: 检查参数。

GAUSS-03978: "unrecognized simple dictionary parameter: '%s'"

SQLSTATE: 22023

错误原因: 输入了错误的dictionary参数。

解决办法: 检查参数。

GAUSS-03979: "conversion from wchar_t to server encoding failed: %m"

SQLSTATE: 22021

错误原因: 将某个wchar_t字符转换成服务器的编码方式失败。

解决办法: 检查待转换的字符。

GAUSS-03980: "unrecognized headline parameter: '%s'"

SQLSTATE: 22023

错误原因: 对于HighlightAll命令输入了错误的参数。

解决办法: 对于HighlightAll参数只能是1、on、true、t、y和yes。

<br/>

## GAUSS-03981 - GAUSS-03990

<br/>

GAUSS-03981: "MinWords should be less than MaxWords"

SQLSTATE: 22023

错误原因: 输入的min_words比max_words大。

解决办法: 调整min_words和max_words。

GAUSS-03982: "MinWords should be positive"

SQLSTATE: 22023

错误原因: 输入的min_words小于0。

解决办法: 调整min_words为正数。

GAUSS-03983: "ShortWord should be >= 0"

SQLSTATE: 22023

错误原因: 输入的ShortWord小于0。

解决办法: 调整ShortWord大于等于0。

GAUSS-03984: "MaxFragments should be >= 0"

SQLSTATE: 22023

错误原因: 输入的MaxFragments小于0。

解决办法: 调整MaxFragments大于等于0。

GAUSS-03985: "Invalid formatter options '%s'"

SQLSTATE: XX000

错误原因: 数据格式不合法。

解决办法: 请检查数据格式，再次执行操作。

GAUSS-03986: "locations can not use different protocols"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03987: "invalid file header location '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03988: "Loading mode '%s' not recognized"

SQLSTATE: XX000

错误原因: 不可识别的模式。

解决办法: 请使用normal，shared，private模式，再次执行操作。

GAUSS-03989: "LOAD format '%s' not recognized"

SQLSTATE: 22023

错误原因: 不可识别的模式。

解决办法: 请使用text，csv，fixed模式，再次执行操作。

<br/>

## GAUSS-03991 - GAUSS-04000

<br/>

GAUSS-03993: "location '%s' is invalid"

SQLSTATE: 42000

错误原因: 路径格式不正确。

解决办法: 请检查确保路径正确后，再次执行操作。

GAUSS-03994: "SHARED mode can not use location '%s'"

SQLSTATE: 42601

错误原因: SHARED模式不支持使用远程路径。

解决办法: 请替换为本地路径后，再次执行操作。

GAUSS-03995: "PRIVATE mode can not use location '%s'"

SQLSTATE: 42601

错误原因: PRIVATE模式不支持使用远程路径。

解决办法: 请替换为本地路径后，再次执行操作。

GAUSS-03996: "Normal mode can not use location '%s'"

SQLSTATE: 42601

错误原因: NORMAL模式不支持使用远程路径。

解决办法: 请替换为本地路径后，再次执行操作。

GAUSS-03997: "can not specify multiple local locations"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03998: "HEADER needs FILEHEADER specification in WRITE ONLY foreign table"

SQLSTATE: 42601

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03999: "can not scan a WRITE ONLY foreign table"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
