---
title: GAUSS-00901 - GAUSS-01000
summary: GAUSS-00901 - GAUSS-01000
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-00901 - GAUSS-01000

<br/>

## GAUSS-00901 - GAUSS-00910

<br/>

GAUSS-00901: "tables in ALTER TABLE EXCHANGE PARTITION must have the same column/row storage"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表必有相同的行/列存储。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的行列存储，否则不能执行。

GAUSS-00902: "tables in ALTER TABLE EXCHANGE PARTITION must have the same type of compress"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表必有相同的压缩类型。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的压缩类型，否则不能执行。

GAUSS-00903: "tables in ALTER TABLE EXCHANGE PARTITION must have the same number of columns"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表必有相同的列数。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列数，否则不能执行。

GAUSS-00904: "column name mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列名不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列名，否则不能执行。

GAUSS-00905: "column type or size mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列类型及大小不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列类型和大小，否则不能执行。

GAUSS-00906: "column not null constraint mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列不能为空限制不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表列是否同时有不能为空限制，否则不能执行。

GAUSS-00907: "column default constraint mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列默认限制不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表列是否有相同的默认限制，否则不能执行。

GAUSS-00908: "column collation mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行表的列排序不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列排序，否则不能执行。

GAUSS-00909: "column storage mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列存储不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列存储，否则不能执行。

GAUSS-00910: "the type of column compress mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列压缩类型不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的压缩类型，否则不能执行。

<br/>

## GAUSS-00911 - GAUSS-00920

<br/>

GAUSS-00911: "constraint mismatch in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列限制不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列限制，否则不能执行。

GAUSS-00912: "distribute mismatch for tables in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表列分布不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的列分布，否则不能执行。

GAUSS-00913: "tables in ALTER TABLE EXCHANGE PARTITION must have the same number of indexs"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表索引数目不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的索引数目，否则不能执行。

GAUSS-00914: "index mismatch for tables in ALTER TABLE EXCHANGE PARTITION"

SQLSTATE: 0A000

错误原因: ALTER TABLE EXCHANGE PARTITION语句执行的表索引不匹配。

解决办法: 建议检查ALTER TABLE EXCHANGE PARTITION语句执行的表是否有相同的表索引，否则不能执行。

GAUSS-00915: "some rows in table do not qualify for specified partition"

SQLSTATE: 0A000

错误原因: SPLIT PARTITION操作执行时，表中的一些行不符合指定的分区。

解决办法: 请检查表中不符合指定分区的行，查明原因。

GAUSS-00916: "the number of resulting partitions must be more than one"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到结果数目小于2个，表明分割分区失败。

解决办法: 无分割点(split point)，分割分区失败，建议检查失败原因。

GAUSS-00917: "the bound of the first resulting partition is too low"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到的第一结果分区边界过低，分割分区失败。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00918: "the bound of resulting partition '%s' is too low"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到的结果分区边界过低，分割分区失败。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00919: "the bound of the last resulting partition is not equal with specified partition bound"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到的最后一个结果分区边界与指定分区边界不等，分割分区失败。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00920: "number of boundary items NOT EQUAL to number of partition keys"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到的边界项数目与分区键值数目不同，分割分区失败。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

<br/>

## GAUSS-00921 - GAUSS-00930

<br/>

GAUSS-00921: "split point is too low"

SQLSTATE: 0A000

错误原因: SPLIT PARTITION操作得到的分割点应介于前分区与后分区之间，实际得到的分割点小于前分区。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00922: "split point is too high"

SQLSTATE: 0A000

错误原因: SPLIT PARTITION操作得到的分割点应介于前分区与后分区之间，实际得到的分割点大于后分区。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00923: "resulting partition '%s' name conflicts with that of an existing partition"

SQLSTATE: 42710

错误原因: SPLIT PARTITION操作得到的分区名称与已有分区名冲突，该分割分区操作不能执行。

解决办法: 建议修改结果分区名称。

GAUSS-00924: "number of boundary items NOT EQUAL to number of partition keys"

SQLSTATE: 42601

错误原因: SPLIT PARTITION操作得到的边界项数目与分区键值数目不同，分割分区失败。

解决办法: 分割分区操作失败，建议重新检查SPLIT PARTITION操作。

GAUSS-00925: "cache lookup failed for toast table: %u"

SQLSTATE: XX000

错误原因: 没有找到对应的toast表。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

GAUSS-00926: "cache lookup failed for cuDesc table: %u"

SQLSTATE: XX000

错误原因: 没有找到对应的cuDesc表。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

GAUSS-00927: "cache lookup failed for delta table: %u"

SQLSTATE: XX000

错误原因: 没有找到对应的delta表。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-00931 - GAUSS-00940

<br/>

GAUSS-00935: "cache lookup failed for relation %u"

SQLSTATE: 02000

错误原因: ALTER TABLE SET TABLESPACE操作，关系不存在。

解决办法: 无法找到关系表，建议更改本操作。

GAUSS-00936: "SQL function cannot return shell type %s"

SQLSTATE: 42P13

错误原因: SQL函数不支持返回共享类型的变量。

解决办法: 修改SQL函数，不使用共享类型变量作为返回值。

GAUSS-00937: "SQL function cannot accept shell type %s"

SQLSTATE: 42P13

错误原因: SQL函数不支持共享类型的变量作为函数参数。

解决办法: 修改SQL函数，不使用共享类型变量作为函数参数。

GAUSS-00938: "type %s does not exist"

SQLSTATE: 42704

错误原因: 所提示的数据类型不存在。

解决办法: 请检查函数或存储过程，确定参数的数据类型是否正确。

GAUSS-00940: "VARIADIC parameter must be the last input parameter"

SQLSTATE: 42P13

错误原因: VARIADIC参数必须放在输入参数的最后位置。

解决办法: 修改函数参数列表，将VARIADIC参数放在输入参数的最后位置。

<br/>

## GAUSS-00941 - GAUSS-00950

<br/>

GAUSS-00941: "VARIADIC parameter must be an array"

SQLSTATE: 42P13

错误原因: VARIADIC参数必须是数组类型。

解决办法: 修改函数参数，使用数组类型变量作为VARIADIC参数。

GAUSS-00942: "parameter name '%s' used more than once"

SQLSTATE: 42P13

错误原因: 在参数列表中多次使用了同一个参数名称。

解决办法: 修改函数参数，使用不同的参数名称。

GAUSS-00943: "only input parameters can have default values"

SQLSTATE: 42P13

错误原因: 只有输入参数才能有默认值。

解决办法: 修改函数参数，只对输入参数定义默认值。

GAUSS-00944: "cannot use table references in parameter default value"

SQLSTATE: 42P10

错误原因: 不能在参数的默认值中使用变量（如表、列、CurrentOf表达式等）。

解决办法: 修改函数参数，不使用变量作为默认值。

GAUSS-00945: "cannot use subquery in parameter default value"

SQLSTATE: 0A000

错误原因: 不能在参数的默认值中使用子查询。

解决办法: 修改函数参数，不使用子查询作为默认值。

GAUSS-00946: "cannot use aggregate function in parameter default value"

SQLSTATE: 42803

错误原因: 不能在参数的默认值中使用聚集函数。

解决办法: 修改函数参数，不使用聚集函数作为默认值。

GAUSS-00947: "cannot use window function in parameter default value"

SQLSTATE: 42P20

错误原因: 不能在参数的默认值中使用窗口函数。

解决办法: 修改函数参数，不使用窗口函数作为默认值。

GAUSS-00948: "invalid volatility '%s'"

SQLSTATE: XX000

错误原因: 函数volatile类型非法。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

GAUSS-00949: "no function body specified"

SQLSTATE: 42P13

错误原因: 没有定义函数体。

解决办法: 请检查函数，确保定义了函数体。

GAUSS-00950: "no language specified"

SQLSTATE: 42P13

错误原因: 没有定义函数的语言类型（如C/plpgsql/sql）。

解决办法: 检查函数定义，确保定义了语言类型（如C/plpgsql/sql）。

<br/>

## GAUSS-00951 - GAUSS-00960

<br/>

GAUSS-00951: "COST must be positive"

SQLSTATE: 22023

错误原因: COST必须是正数。

解决办法: 检查函数定义，确保COST定义为正数。

GAUSS-00952: "ROWS must be positive"

SQLSTATE: 22023

错误原因: ROWS必须还正数。

解决办法: 检查函数定义，确保ROWS定义为正数。

GAUSS-00953: "only one AS item needed for language '%s'"

SQLSTATE: 42P13

错误原因: 只需要一个AS子句。

解决办法: 检查函数定义，确保只有一个AS子句。

GAUSS-00954: "language '%s' does not exist"

SQLSTATE: 42704

错误原因: 语言不存在。

解决办法: 检查函数定义，确保函数的语言类型是数据库支持的(如language plpgsql/language sql等)。

GAUSS-00955: "only system admin can define a leakproof function"

SQLSTATE: 42501

错误原因: 只有系统管理员来能定义leakproof函数。

解决办法: 使用系统管理员帐户来创建leakproof函数。

GAUSS-00957: "function result type cannot be a view."

SQLSTATE: 42P13

错误原因: 函数返回结果类型不能是视图。

解决办法: 检查函数定义，不使用视图作为函数返回结果的类型。

GAUSS-00958: "ROWS is not applicable when function does not return a set"

SQLSTATE: 22023

错误原因: 如果不是返回结果集的函数，不能指定ROWS。

解决办法: 检查函数定义，对返回结果集的函数才指定ROWS。

GAUSS-00959: "cache lookup failed for pg_aggregate tuple for function %u"

SQLSTATE: XX000

错误原因: 未在pg_aggregate中找到对应的聚集函数。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

GAUSS-00960: "function %u doesn't return OPAQUE"

SQLSTATE: XX000

错误原因: 函数返回类型不是OPAQUE。

解决办法: 请检查对应的函数的返回类型是否正确。

<br/>

## GAUSS-00961 - GAUSS-00970

<br/>

GAUSS-00962: "source data type %s is a pseudo-type"

SQLSTATE: 42809

错误原因: 源数据类型不能为pseudo类型。

解决办法: 检查要创建的CAST，确保源数据类型不是pseudo类型。

GAUSS-00963: "target data type %s is a pseudo-type"

SQLSTATE: 42809

错误原因: 目标数据类型不能为pseudo类型。

解决办法: 检查要创建的CAST，确保目标数据类型不是pseudo类型。

GAUSS-00964: "must be owner of type %s or type %s"

SQLSTATE: 42501

错误原因: 必须是源类型和目标类型的所有者。

解决办法: 检查SQL语句，确保是源类型和目标类型的所有者。

GAUSS-00965: "cast function must take one to three arguments"

SQLSTATE: 42P17

错误原因: CAST函数必须指定1到3个参数。

解决办法: 检查CAST函数，确保有1到3个参数。

GAUSS-00966: "argument of cast function must match or be binary-coercible from source data type"

SQLSTATE: 42P17

错误原因: CAST函数的参数必须和源数据类型匹配或二进制兼容。

解决办法: 检查CAST函数，确保参数和源数据类型匹配或二进制兼容。

GAUSS-00967: "second argument of cast function must be type integer"

SQLSTATE: 42P17

错误原因: CAST函数的第2个参数必须是整数类型。

解决办法: 检查CAST函数，确保第2个参数是整数类型。

GAUSS-00968: "third argument of cast function must be type Boolean"

SQLSTATE: 42P17

错误原因: CAST函数的第3个参数必须是布尔类型。

解决办法: 检查CAST函数，确保第3个参数是布尔类型。

GAUSS-00969: "return data type of cast function must match or be binary-coercible to target data type"

SQLSTATE: 42P17

错误原因: "return data type of cast function must match or be binary-coercible to target data typeCAST函数的返回数据类型必须和目标数据类型匹配或二进制兼容"。

解决办法: "check the CAST function and make sure that return data type of cast function match or be binary-coercible to target data type检查CAST函数，确保返回数据类型和目标数据类型匹配或二进制兼容"。

GAUSS-00970: "cast function must not be volatile"

SQLSTATE: 42P17

错误原因: "cast function must not be volatileCAST函数的属性不能是volatile的"。

解决办法: "check the CAST function and make sure that cast function not be volatile检查CAST函数，确保它的属性不是volatile的"。

<br/>

## GAUSS-00971 - GAUSS-00980

<br/>

GAUSS-00971: "cast function must not be an aggregate function"

SQLSTATE: 42P17

错误原因: 函数不能是聚集函数。

解决办法: 检查CAST函数，确保它不是聚集函数。

GAUSS-00972: "cast function must not be a window function"

SQLSTATE: 42P17

错误原因: 函数不能是窗口函数。

解决办法: 检查CAST函数，确保他不是窗口函数。

GAUSS-00973: "cast function must not return a set"

SQLSTATE: 42P17

错误原因: 函数不能返回结果集。

解决办法: 检查CAST函数，确保它不返回结果集"。

GAUSS-00974: "must be system admin to create a cast WITHOUT FUNCTION"

SQLSTATE: 42501

错误原因: 必须是系统管理员才能指定CAST函数的WITHOUT FUNCTION属性。

解决办法: 若要在创建CAST函数时指定WITHOUT FUNCTION属性，必须使用管理员帐户来执行创建语句。

GAUSS-00975: "source and target data types are not physically compatible"

SQLSTATE: 42P17

错误原因: 源数据类型和目标数据类型不是物理兼容的（物理兼容是指: 这两种数据类型在pg_type系统表中的typlen，typbyval，typalign都相同）。

解决办法: 创建一个CAST时，保证源数据类型和目标数据类型是物理兼容的（物理兼容是指: 这两种数据类型在pg_type系统表中的typlen，typbyval，typalign都相同）。

GAUSS-00976: "composite data types are not binary-compatible"

SQLSTATE: 42P17

错误原因: 复合数据类型不是二进制兼容的，因此不能进行转换。

解决办法: 创建CAST时，请勿使用复合数据类型作为源数据类型或目标数据类型。

GAUSS-00977: "enum data types are not binary-compatible"

SQLSTATE: 42P17

错误原因: 枚举数据类型不是二进制兼容的，因此不能进行转换。

解决办法: 创建CAST时，请勿使用枚举数据类型作为源数据类型或目标数据类型。

GAUSS-00978: "array data types are not binary-compatible"

SQLSTATE: 42P17

错误原因: 数组类型不是二进制兼容的，因此不能进行转换。

解决办法: 创建CAST时，请勿使用数组类型作为源数据类型或目标数据类型。

GAUSS-00979: "domain data types must not be marked binary-compatible"

SQLSTATE: 42P17

错误原因: "domain data types must not be marked binary-compatibledomain数据类型不是二进制兼容的，因此不能进行转换。

解决办法: 创建CAST时，请勿使用domain数据类型作为源数据类型或目标数据类型。

GAUSS-00980: "source data type and target data type are the same"

SQLSTATE: 42P17

错误原因: 源数据类型和目标数据类型相同。

解决办法: 创建CAST时，保证源数据类型和目标数据类型不同。

<br/>

## GAUSS-00981 - GAUSS-00990

<br/>

GAUSS-00981: "unrecognized CoercionContext: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00982: "cast from type %s to type %s already exists"

SQLSTATE: 42710

错误原因: 从源数据类型到目标数据类型的CAST已经存在。

解决办法: 从源数据类型到目标数据类型的CAST已经存在，无需再创建。

GAUSS-00983: "cast from type %s to type %s does not exist"

SQLSTATE: 42704

错误原因: 从源数据类型到目标数据类型的CAST不存在。

解决办法: 检查pg_cast系统表，看这个CAST是否存在，入不存在，可以进行创建，或重写sql语句来避免使用这个CAST。

GAUSS-00984: "could not find tuple for cast %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00985: "function '%s' already exists in schema '%s'"

SQLSTATE: 42723

错误原因: 目标模式中的这个函数已存在。

解决办法: 检查目标模式，确定是否已创建了同名函数。

GAUSS-00986: "failed to change schema dependency for function '%s'"

SQLSTATE: XX000

错误原因: 更改函数的模式依赖未成功。

解决办法: 系统表异常，请联系技术支持工程师提供技术支持。

GAUSS-00987: "no inline code specified"

SQLSTATE: 42601

错误原因: 匿名块中没有指定内联语句（匿名块为内联执行，因此其中的语句都是内联语句）。

解决办法: 检查匿名块语法，确保其中有内联语句（匿名块为内联执行，因此其中的语句都是内联语句）。

GAUSS-00988: "language '%s' does not support inline code execution"

SQLSTATE: 0A000

错误原因: 此语言不支持内联执行。

解决办法: 使用普通函数替代或使用其他语言。

GAUSS-00989: "source encoding '%s' does not exist"

SQLSTATE: 42704

错误原因: 转换中的源编码不存在。

解决办法: 检查源编码，确保其在系统中存在。

GAUSS-00990: "destination encoding '%s' does not exist"

SQLSTATE: 42704

错误原因: 转换中的目标编码不存在。

解决办法: 检查目标编码，确保其在系统中存在。

<br/>

## GAUSS-00991 - GAUSS-01000

<br/>

GAUSS-00991: "encoding conversion function %s must return type 'void'"

SQLSTATE: 42P17

错误原因: 编码转换函数只能返回void。

解决办法: 检查编码转换函数，确保其返回void。

GAUSS-00992: "cache lookup failed for conversion %u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00994: "extra data after last expected column"

SQLSTATE: 22P04

错误原因: 数据文件中的列的个数超过表中列的个数。

解决办法: 检查数据文件列的个数与表定义是否一致。

GAUSS-00995: "field position covers previous field"

SQLSTATE: 22P04

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-00998: "%s"

SQLSTATE: 42000

错误原因: 语法错误。

解决办法: 检查sql语句的语法是否正确。

GAUSS-01000: "type '%s' already exists"

SQLSTATE: 42710

错误原因: 数据类型已存在。

解决办法: 检查pg_type系统表，查看此类型是否已存在。
