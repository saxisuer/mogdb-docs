---
title: GAUSS-03601 - GAUSS-03700
summary: GAUSS-03601 - GAUSS-03700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-03601 - GAUSS-03700

<br/>

## GAUSS-03601 - GAUSS-03610

<br/>

GAUSS-03603: "number of index columns (%d) exceeds limit (%d)"

SQLSTATE: 54011

错误原因: 索引元组的数目超过了上限32个。

解决办法: 减少需要建立行存索引的列数目。

GAUSS-03604: "index row requires %lu bytes, maximum size is %lu"

SQLSTATE: 54000

错误原因: 单条索引元组的长度超过了一个页的大小，主要是由索引列数据过长引起的。

解决办法: 不可对超长的列数据建立索引。

GAUSS-03605: "column '%s' cannot be applied %s compress mode"

SQLSTATE: 42P16

错误原因: 列不支持所提示的压缩模式。

解决办法: 请正确选择压缩列、模式。

GAUSS-03606: "unsupported format code: %d"

SQLSTATE: 22023

错误原因: 不支持的类型。

解决办法: 请检查列的类型，再次执行操作。

GAUSS-03607: "invalid attnum: %d"

SQLSTATE: XX000

错误原因: 运行过程中出现了不存在的系统列。

解决办法: 若是由用户发起的系统列查询，需要检测是否支持该系统列的查询；否则为内部运行错误，请联系技术支持工程师提供技术支持。

GAUSS-03608: "number of columns (%d) exceeds limit (%d)"

SQLSTATE: 54011

错误原因: 列的数量达到上限。

解决办法: 请减少列的数量，再次执行操作。

GAUSS-03609: "Tuple does not match the descriptor"

SQLSTATE: XX001

错误原因: 元组的列数目与实际数据的列数目不一致。

解决办法: 运行时错误，请联系技术支持工程师提供技术支持。

GAUSS-03610: "cannot extract system attribute from virtual tuple"

SQLSTATE: XX000

错误原因: 内部错误: 无法从虚元组中抽取出系统列来。

解决办法: 运行时错误，请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03611 - GAUSS-03620

<br/>

GAUSS-03611: "cannot extract system attribute from minimal tuple"

SQLSTATE: XX000

错误原因: 内部错误: 无法从虚元组中抽取出系统列来。

解决办法: 运行时错误，请联系技术支持工程师提供技术支持。

GAUSS-03613: "user-defined relation parameter types limit exceeded"

SQLSTATE: 54000

错误原因: 关系参数类型定义达到上限。

解决办法: 请重新定义关系参数类型。

GAUSS-03614: "unsupported option type"

SQLSTATE: XX000

错误原因: 表定义的选项中出现了不支持的数据类型，支持的有效数据类型包括布尔类、整数、浮点数和字符串。

解决办法: 修正创建表时可选项对应的数据类型为有效类型。

GAUSS-03615: "RESET must not include values for parameters"

SQLSTATE: 42601

错误原因: RESET不支持指定参数值。

解决办法: 请检查RESET命令，再次执行命令重置缺省值。

GAUSS-03616: "unrecognized parameter namespace '%s'"

SQLSTATE: 22023

错误原因: 不可识别的参数命名空间。

解决办法: 请检查参数命名空间是否存在，再次执行操作。

GAUSS-03617: "unrecognized parameter '%s'"

SQLSTATE: 22023

错误原因: 不可识别的参数。

解决办法: 请检查参数是否存在，再次执行操作。

GAUSS-03618: "parameter '%s' specified more than once"

SQLSTATE: 22023

错误原因: 参数指定多于一次。

解决办法: 请检查参数使用，再次执行操作。

GAUSS-03619: "invalid value for boolean option '%s': %s"

SQLSTATE: XX000

错误原因: BOOL类型项数据不合法。

解决办法: 请检查BOOL类型项数据的合法性，再次执行操作。

GAUSS-03620: "invalid value for integer option '%s': %s"

SQLSTATE: XX000

错误原因: INT类型项数据不合法。

解决办法: 请检查INT类型项数据的合法性，再次执行操作。

<br/>

## GAUSS-03621 - GAUSS-03630

<br/>

GAUSS-03621: "value %s out of bounds for option '%s'"

SQLSTATE: XX000

错误原因: 数据值不在范围内。

解决办法: 请检查数据取值范围，再次执行操作。

GAUSS-03622: "invalid value for floating point option '%s': %s"

SQLSTATE: XX000

错误原因: FLOAT类型项数据不合法。

解决办法: 请检查FLOAT类型项数据，再次执行操作。

GAUSS-03623: "unsupported reloption type %d"

SQLSTATE: XX000

错误原因: 表定义的选项中出现了不支持的数据类型，支持的有效数据类型包括布尔类、整数、浮点数和字符串。

解决办法: 修正创建表时可选项对应的数据类型为有效类型。

GAUSS-03624: "unrecognized reloption type %c"

SQLSTATE: XX000

错误原因: 表定义的选项中出现了不支持的数据类型，支持的有效数据类型包括布尔类、整数、浮点数和字符串。

解决办法: 修正创建表时可选项对应的数据类型为有效类型。

GAUSS-03625: "reloption '%s' not found in parse table"

SQLSTATE: XX000

错误原因: 创建表时指定了不存在的表选项。

解决办法: 询表支持的有效选项并修正。

GAUSS-03626: "Invalid string for 'ORIENTATION' option"

SQLSTATE: 22023

错误原因: ORIENTATION项的参数不合法。

解决办法: 请检查ORIENTATION项参数的合法性，再次执行操作。

GAUSS-03627: "Invalid string for 'COMPRESSION' option"

SQLSTATE: 22023

错误原因: COMPRESSION项的参数不合法。

解决办法: 请检查COMPRESSION项参数的合法性，再次执行操作。

GAUSS-03628: "unrecognized StrategyNumber: %d"

SQLSTATE: XX000

错误原因: 使用的strategy错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03629: "missing oprcode for operator %u"

SQLSTATE: XX000

错误原因: 非法的oprcode。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03630: "btree index keys must be ordered by attribute"

SQLSTATE: XX000

错误原因: 索引的属性列小于1。

解决办法: 检查B-tree索引是否创建正确。

<br/>

## GAUSS-03631 - GAUSS-03640

<br/>

GAUSS-03631: "multiple active vacuums for index '%s'"

SQLSTATE: XX000

错误原因: 一个索引上同时有多个VACUUM操作。

解决办法: 避免同时对一个b-tree索引做多个VACUUM操作。

GAUSS-03632: "out of btvacinfo slots"

SQLSTATE: XX000

错误原因: VACUUM操作数量超过最大值。

解决办法: 等其他VACUUM操作执行完成后再支持该操作。

GAUSS-03633: "index '%s' is not a btree"

SQLSTATE: XX002

错误原因: 该节点不是B-tree索引的root。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03635: "no live root page found in index '%s'"

SQLSTATE: XX000

错误原因: B-tree索引没有root节点。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03636: "root page %u of index '%s' has level %u, expected %u"

SQLSTATE: XX000

错误原因: 索引的root页有错误的level信息。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03637: "failed to re-find parent key in index '%s' for deletion target page %u"

SQLSTATE: XX000

错误原因: 删除的过程中找不到父节点。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03638: "not enough stack items"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03639: "left link changed unexpectedly in block %u of index '%s'"

SQLSTATE: XX000

错误原因: left link的页编号被修改。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03640: "right sibling's left-link doesn't match: block %u links to %u instead of expected %u in index '%s'"

SQLSTATE: XX000

错误原因: B-tree索引结构错误，右兄弟的左连接不匹配。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03641 - GAUSS-03650

<br/>

GAUSS-03641: "failed to delete rightmost child %u of block %u in index '%s'"

SQLSTATE: XX000

错误原因: 删除节点失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03642: "right sibling %u of block %u is not next child %u of block %u in index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03643: "fell off the end of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03644: "missing support function %d(%u,%u) for attribute %d of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03646: "could not find left sibling of block %u in index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03647: "btree level %u not found in index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03648: "invalid scan direction: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03649: "duplicate key value violates unique constraint '%s'"

SQLSTATE: 23505

错误原因: 插入唯一索引主键冲突。

解决办法: 请检查数据冲突或更改索引为非唯一索引，再次执行操作。

GAUSS-03650: "failed to re-find tuple within index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03651 - GAUSS-03660

<br/>

GAUSS-03651: "failed to add new item to block %u in index '%s'"

SQLSTATE: XX000

错误原因: 插入记录到特定索引页失败。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03652: "failed to add hikey to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03653: "failed to add hikey to the left sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03655: "failed to add new item to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03656: "failed to add old item to the left sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03657: "failed to add old item to the right sibling while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03658: "could not find a feasible split point for index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03659: "failed to re-find parent key in index '%s' for split pages %u/%u"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03660: "failed to add leftkey to new root page while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03661 - GAUSS-03670

<br/>

GAUSS-03661: "failed to add rightkey to new root page while splitting block %u of index '%s'"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03662: "failed to add item to the index page"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03663: "index compare error, both are NULL"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03664: "_bt_restore_page: cannot add item to page"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03665: "btree_insert_redo: failed to add item"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03666: "failed to add new item to left page after split"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03667: "failed to add high key to left page after split"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03670: "btree_xlog_cleanup: left block unfound"

SQLSTATE: XX000

错误原因: left block查找不到。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03671 - GAUSS-03680

<br/>

GAUSS-03671: "btree_xlog_cleanup: right block unfound"

SQLSTATE: XX000

错误原因: right block查找不到。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03672: "btree_xlog_cleanup: _bt_pagedel failed"

SQLSTATE: XX000

错误原因: b tree页面删除失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03673: "unlogged GiST indexes are not supported"

SQLSTATE: 0A000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03674: "concurrent GiST page split was incomplete"

SQLSTATE: XX000

错误原因: split不完整。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03675: "index '%s' contains an inner tuple marked as invalid"

SQLSTATE: XX000

错误原因:  内部索引包含无效元组。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03676: "failed to re-find parent of a page in index '%s', block %u"

SQLSTATE: XX000

错误原因: 查找父节点页失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03677: "numberOfAttributes %d > %d"

SQLSTATE: XX000

错误原因: 索引表的属性列超过最大值。

解决办法: 减少创建索引的属性列数。

GAUSS-03678: "invalid GiST tuple found on leaf page"

SQLSTATE: XX000

错误原因: GIST页错误。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03679: "GiST only supports forward scan direction"

SQLSTATE: XX000

错误原因: 只支持forward方向。

解决办法: 内部错误。检查扫描方向，第二个参数。

<br/>

## GAUSS-03681 - GAUSS-03690

<br/>

GAUSS-03683: "invalid value for 'buffering' option"

SQLSTATE: 22023

错误原因: BufferingOption的选项非: on、off和auto。

解决办法: 内部错误。检查BufferingOption选项。

GAUSS-03685: "failed to re-find parent for block %u"

SQLSTATE: XX000

错误原因: 查找父节点页失败。

解决办法: 内部错误，属于预防性报错机制。请联系技术支持工程师提供技术支持。

GAUSS-03687: "gistmerge: unimplemented"

SQLSTATE: XX000

错误原因: gist不支持的merge。

解决办法: 属于功能不支持，请检查使用方式和场景。

GAUSS-03688: "failed to add item to GiST index page, size %d bytes"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03690: "GiST does not support mark/restore"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-03691 - GAUSS-03700

<br/>

GAUSS-03691: "inconsistent point values"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03692: "unknown strategy number: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03693: "unrecognized strategy number: %d"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03694: "index '%s' is not an SP-GiST index"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03695: "desired SPGiST tuple size is too big"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03696: "SP-GiST inner tuple size %lu exceeds maximum %lu"

SQLSTATE: 54000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03697: "SPGiST inner tuple header field is too small"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03698: "some but not all node labels are null in SPGiST inner tuple"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-03699: "failed to add item of size %u to SPGiST index page"

SQLSTATE: XX000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
