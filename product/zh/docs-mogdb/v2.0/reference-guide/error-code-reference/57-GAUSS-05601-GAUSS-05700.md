---
title: GAUSS-05601 - GAUSS-05700
summary: GAUSS-05601 - GAUSS-05700
author: Zhang Cuiping
date: 2021-03-11
---

# GAUSS-05601 - GAUSS-05700

<br/>

## GAUSS-05601 - GAUSS-05610

<br/>

GAUSS-05601: "only system/monitor admin can get user statistics info"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05602: "report params is null"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05603: "report type can not = %s"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05604: "report scope can not = %s"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05605: "dashTitle or tableTitle is null"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05606: "out of the Contents"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05607: "no available data for report"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05608: "dashboard title is not assigned"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05609: "table title is not assigned"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05610: "query is null"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05611 - GAUSS-05620

<br/>

GAUSS-05611: "list is null, can not free"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05612: "table is not null when overturn table"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05613: "no this type of report_scope"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05614: "calc trx diff count failed!"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05615: "set the snapshotid"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05616: "snapshot id is invalid"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05617: "Instance reset time is different"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05618: "invalid report type, should be %s or %s or %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05619: "invalid report scope, should be %s or %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05620: "invalid report node name."

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05621 - GAUSS-05630

<br/>

GAUSS-05621: "The 3rd argument 'report_type' and 4th argument 'report_scope' should not be null"

SQLSTATE: 22004

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05622: "Superuser privilege is neended to generate report"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05623: "out of memory of current node."

SQLSTATE: 53200

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05624: "could not establish connection"

SQLSTATE: 08001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05625: "Superuser privilege is need to operate wdr_xdb_query"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05626: "wrong number of arguments"

SQLSTATE: 54023

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05627: "connection '%s' not available"

SQLSTATE: 08003

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05628: "could not send query: %s"

SQLSTATE: 02002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05629: "failed to set single-row mode for dblink query"

SQLSTATE: 02002

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05630: "remote query result rowtype does not match the specified FROM clause rowtype"

SQLSTATE: 42804

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05631 - GAUSS-05640

<br/>

GAUSS-05631: "only system admin can kill snapshot thread"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05632: "kill snapshot thread failed %s"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05633: "kill snapshot thread failed, exceeds MAX_RETRY_COUNT(%d)"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05634: "set lockwait_timeout failed: %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05635: "Superuser privilege is need to operate snapshot"

SQLSTATE: 42501

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05636: "WDR snapshot request can not be accepted, please retry later"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05637: "Cannot respond to WDR snapshot request"

SQLSTATE: OP001

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05638: "query is NULL"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05639: "this query can not get datum values"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05640: "create sequence failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05641 - GAUSS-05650

<br/>

GAUSS-05641: "update snapshot end time stamp filled"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05642: "wdr_snapshot_interval is 0"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05643: "invalid query: %s"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05644: "clean table of snap_%s is failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05645: "WDR snapshot analyze table:%s not exist"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05646: "insert into tables_snap_timestamp start time stamp is failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05647: "insert into snap_%s is failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05648: "update tables_snap_timestamp end time stamp is failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05649: "clean snapshot id %lu is failed in snapshot table"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05650: "clean snapshot id %lu is failed in tables_snap_timestamp table"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05651 - GAUSS-05660

<br/>

GAUSS-05651: "query or the tablename is null when snapshot create stat table"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05652: "can not create snapshot stat table"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05653: "create WDR snapshot data table failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05654: "spilt str can not null"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05655: "insert into tables_snap_timestamp start time stamp failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05656: "create index failed"

SQLSTATE: 22000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05657: "analyze table, connection failed: %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05658: "snapshot thread SPI_connect failed: %s"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05659: "The length of str: %d is less than %d"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05660: "Array length %d or %d is less than %d"

SQLSTATE: XX000

错误原因:  系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05661 - GAUSS-05670

<br/>

GAUSS-05661: "[CapView] OOM in capture view"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05662: "[CapView] could not write to view perf file: %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05663: "[CapView] could not ftell json file :%m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05664: "[CapView] pls check database name and view name!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05665: "[CapView] view name is too long!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05666: "[CapView] view name can not contain '('!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05667: "[CapView] only system admin can capure view"

SQLSTATE: 42501

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05668: "[CapView] in capture_view_to_json proc, view_name or is_all_db can not by null"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05669: "[CapView] in capture_view_to_json proc, is_all_db can only be 0 or 1"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05670: "[CapView] SPI_connect failed: %s"

SQLSTATE: SP001

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05671 - GAUSS-05680

<br/>

GAUSS-05671: "[CapView] json file can not by NULL"

SQLSTATE: XX005

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05672: "[CapView] calc realpath failed"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05673: "[CapView] could not open log file '%s': %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05674: "[CapView] could not chmod view json file '%s': %m"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05675: "[CapView] calc localtime failed"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05676: "[CapView] invalid query"

SQLSTATE: 22000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05677: "The encryption key can not be empty!"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05678: "The encryption key must be shorter than 16 bytes!"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05679: "The decryption key can not be empty!"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05680: "Decode the cipher text failed or the ciphertext is too short!"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05681 - GAUSS-05690

<br/>

GAUSS-05681: "Encrypt OBS AK/SK failed."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05682: "Encrypt OBS AK/SK internal error"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05683: "Decrypt OBS AK/SK failed."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05684: "Decrypt OBS AK/SK internal error."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05685: "Failed to get OBS certificate file."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05686: "No key file obsserver.key.cipher"

SQLSTATE: 58P01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05687: "aes128EncryptSpeedFailed!"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05688: "Failed to get EC certificate file: get env GAUSSHOME failed."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05689: "No key file datasource.key.cipher"

SQLSTATE: 58P01

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05690: "Encrypt EC internal error: dest cipher length is too short."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

<br/>

## GAUSS-05691 - GAUSS-05700

<br/>

GAUSS-05691: "Decrypt EC internal error: dest plain length is too short."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05692: "it's an encrypted cluster, but parameter not initialized!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05693: "encrypt failed, return code is %u!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05694: "encrypt failed after retry three times, error code is %u!"

SQLSTATE: 无

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05695: "decrypt failed, return code is %u!"

SQLSTATE: 22026

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05696: "decrypt failed after retry three times, error code is %u!"

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05697: "transparent_encrypt_kms_url and transparent_encrypt_kms_region should not be empty when transparent encryption enabled."

SQLSTATE: 22023

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05698: "Failed to get ak/sk for transparent encryption."

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05699: "Failed to fork subprocess to get DEK for transparent encryption. Failure command is: [%s]"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。

GAUSS-05700: "Failed to get DEK for transparent encryption. Failure command is [%s], error message is [%s]"

SQLSTATE: 39000

错误原因: 系统内部错误。

解决办法: 请联系技术支持工程师提供技术支持。
