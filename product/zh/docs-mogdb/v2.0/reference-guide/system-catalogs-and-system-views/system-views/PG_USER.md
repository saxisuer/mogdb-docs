---
title: PG_USER
summary: PG_USER
author: Guo Huan
date: 2021-04-19
---

# PG_USER

PG_USER视图提供了访问数据库用户的信息，默认只有初始化用户和具有sysadmin属性的用户可以查看，其余用户需要赋权后才可以查看。

**表 1** PG_USER字段

| 名称            | 类型                     | 描述                                                         |
| :-------------- | :----------------------- | :----------------------------- |
| usename         | name                     | 用户名。                                                     |
| usesysid        | oid                      | 此用户的ID。                                                 |
| usecreatedb     | Boolean                  | 用户是否可以创建数据库。                                     |
| usesuper        | Boolean                  | 用户是否是拥有最高权限的初始系统管理员。                     |
| usecatupd       | Boolean                  | 用户是否可以直接更新系统表。只有usesysid=10的初始系统管理员拥有此权限。其他用户无法获得此权限。 |
| userepl         | Boolean                  | 用户是否可以复制数据流。                                     |
| passwd          | text                     | 密文存储后的用户口令，始终为\*\*\*\*\*\*\*\*。               |
| valbegin        | timestamp with time zone | 帐户的有效开始时间；如果没有设置有效开始时间，则为NULL。     |
| valuntil        | timestamp with time zone | 帐户的有效结束时间；如果没有设置有效结束时间，则为NULL。     |
| respool         | name                     | 用户所在的资源池。                                           |
| parent          | oid                      | 父用户OID。                                                  |
| spacelimit      | text                     | 永久表存储空间限额。                                         |
| tempspacelimit  | text                     | 临时表存储空间限额。                                         |
| spillspacelimit | text                     | 算子落盘空间限额。                                           |
| useconfig       | text[]                   | 运行时配置参数的会话缺省。                                   |
| nodegroup       | name                     | 用户关联的逻辑MogDB名称，如果该用户没有管理MogDB，则该字段为空。 |
