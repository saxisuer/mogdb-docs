---
title: GS_TOTAL_MEMORY_DETAIL
summary: GS_TOTAL_MEMORY_DETAIL
author: Guo Huan
date: 2021-04-19
---

# GS_TOTAL_MEMORY_DETAIL

GS_TOTAL_MEMORY_DETAIL视图统计当前数据库节点使用内存的信息，单位为MB。

**表 1** GS_TOTAL_MEMORY_DETAIL字段

| 名称         | 类型    | 描述                                                         |
| :----------- | :------ | :----------------------------------------------------------- |
| nodename     | text    | 节点名称。                                                   |
| memorytype   | text    | 内存类型，包括以下几种: <br/>- max_process_memory: MogDB实例所占用的内存大小。<br/>- process_used_memory: MogDB进程所使用的内存大小。<br/>- max_dynamic_memory: 最大动态内存。<br/>- dynamic_used_memory: 已使用的动态内存。<br/>- dynamic_peak_memory: 内存的动态峰值。<br/>- dynamic_used_shrctx: 最大动态共享内存上下文。<br/>- dynamic_peak_shrctx: 共享内存上下文的动态峰值。<br/>- max_shared_memory: 最大共享内存。<br/>- shared_used_memory: 已使用的共享内存。<br/>- max_cstore_memory: 列存所允许使用的最大内存。<br/>- cstore_used_memory: 列存已使用的内存大小。<br/>- max_sctpcomm_memory: 通信库所允许使用的最大内存。<br/>- sctpcomm_used_memory: 通信库已使用的内存大小。<br/>- sctpcomm_peak_memory: 通信库的内存峰值。<br/>- other_used_memory: 其他已使用的内存大小。 |
| memorymbytes | integer | 内存类型分配内存的大小。                                     |
