---
title: PG_NODE_ENV
summary: PG_NODE_ENV
author: Guo Huan
date: 2021-04-19
---

# PG_NODE_ENV

PG_NODE_ENV视图提供获取当前节点的环境变量信息。

**表 1** PG_NODE_ENV字段

| 名称          | 类型    | 描述                 |
| :------------ | :------ | :------------------- |
| node_name     | text    | 当前节点的名称。     |
| host          | text    | 当前节点的主机名称。 |
| process       | integer | 当前节点的进程号。   |
| port          | integer | 当前节点的端口号。   |
| installpath   | text    | 当前节点的安装目录。 |
| datapath      | text    | 当前节点的数据目录。 |
| log_directory | text    | 当前节点的日志目录。 |
