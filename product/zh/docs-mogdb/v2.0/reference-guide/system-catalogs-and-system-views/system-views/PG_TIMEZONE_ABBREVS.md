---
title: PG_TIMEZONE_ABBREVS
summary: PG_TIMEZONE_ABBREVS
author: Guo Huan
date: 2021-04-19
---

# PG_TIMEZONE_ABBREVS

PG_TIMEZONE_ABBREVS视图提供了显示了所有可用的时区信息。

**表 1** PG_TIMEZONE_ABBREVS字段

| 名称       | 类型     | 描述                                            |
| :--------- | :------- | :---------------------------------------------- |
| abbrev     | text     | 时区名缩写。                                    |
| utc_offset | interval | 相对于UTC的偏移量。                             |
| is_dst     | Boolean  | 如果当前正处于夏令时范围则为TRUE，否则为FALSE。 |
