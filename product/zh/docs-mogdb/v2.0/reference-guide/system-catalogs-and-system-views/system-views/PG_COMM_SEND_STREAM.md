---
title: PG_COMM_SEND_STREAM
summary: PG_COMM_SEND_STREAM
author: Guo Huan
date: 2021-06-07
---

# PG_COMM_SEND_STREAM

PG_COMM_SEND_STREAM视图展示单个DN上所有的通信库发送流状态。

**表 1** PG_COMM_SEND_STREAM字段

| 名称        | 类型    | 描述                                          |
| :---------- | :------ | :-------------------------------------------- |
| node_name   | text    | 节点名称。                                    |
| local_tid   | bigint  | 使用此通信流的线程ID。                        |
| remote_name | text    | 连接对端节点名称。                            |
| remote_tid  | bigint  | 连接对端线程ID。                              |
| idx         | integer | 通信对端DN在本DN内的标识编号。                |
| sid         | integer | 通信流在物理连接中的标识编号。                |
| tcp_sock    | integer | 通信流所使用的tcp通信socket。                 |
| state       | text    | 通信流当前的状态。                            |
| query_id    | bigint  | 通信流对应的debug_query_id编号。              |
| pn_id       | integer | 通信流所执行查询的plan_node_id编号。          |
| send_smp    | integer | 通信流所执行查询send端的smpid编号。           |
| recv_smp    | integer | 通信流所执行查询recv端的smpid编号。           |
| send_bytes  | bigint  | 通信流发送的数据总量，单位Byte。              |
| time        | bigint  | 通信流当前生命周期使用时长，单位ms。          |
| speed       | bigint  | 通信流的平均发送速率，单位Byte/s。            |
| quota       | bigint  | 通信流当前的通信配额值，单位Byte。            |
| wait_quota  | bigint  | 通信流等待quota值产生的额外时间开销，单位ms。 |
