---
title: GS_CLUSTER_RESOURCE_INFO
summary: GS_CLUSTER_RESOURCE_INFO
author: Guo Huan
date: 2021-06-07
---

# GS_CLUSTER_RESOURCE_INFO

GS_CLUSTER_RESOURCE_INFO视图显示的是所有DN资源的汇总信息。该视图需要设置enable_dynamic_workload=on才能查询，并且该视图不支持在DN执行。查询该视图需要sysadmin权限。

**表 1** GS_CLUSTER_RESOURCE_INFO字段

| 名称          | 类型    | 描述                     |
| :------------ | :------ | :----------------------- |
| min_mem_util  | integer | DN最小内存使用率。       |
| max_mem_util  | integer | DN最大内存使用率。       |
| min_cpu_util  | integer | DN最小CPU使用率。        |
| max_cpu_util  | integer | DN最大CPU使用率。        |
| min_io_util   | integer | DN最小IO使用率。         |
| max_io_util   | integer | DN最大IO使用率。         |
| used_mem_rate | integer | 物理节点最大内存使用率。 |
