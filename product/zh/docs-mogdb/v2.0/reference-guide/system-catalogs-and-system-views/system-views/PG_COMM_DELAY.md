---
title: PG_COMM_DELAY
summary: PG_COMM_DELAY
author: Guo Huan
date: 2021-06-07
---

# PG_COMM_DELAY

PG_COMM_DELAY视图展示单个DN的通信库时延状态。

**表 1** PG_COMM_DELAY字段

| 名称        | 类型    | 描述                                                         |
| :---------- | :------ | :----------------------------------------------------------- |
| node_name   | text    | 节点名称。                                                   |
| remote_name | text    | 连接对端节点名称。                                           |
| remote_host | text    | 连接对端IP地址。                                             |
| stream_num  | integer | 当前物理连接使用的stream逻辑连接数量。                       |
| min_delay   | integer | 当前物理连接一分钟内探测到的最小时延，单位微秒。说明：负数结果无效，请重新等待时延状态更新后再执行。 |
| average     | integer | 当前物理连接一分钟内探测时延的平均值，单位微秒。             |
| max_delay   | integer | 当前物理连接一分钟内探测到的最大时延，单位微秒。             |
