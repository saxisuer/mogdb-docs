---
title: PG_GROUP
summary: PG_GROUP
author: Guo Huan
date: 2021-04-19
---

# PG_GROUP

PG_GROUP视图查看数据库认证角色及角色之间的成员关系。

**表 1** PG_GROUP字段

| 名称     | 类型  | 描述                                   |
| :------- | :---- | :------------------------------------- |
| groname  | name  | 组的名称。                             |
| grosysid | oid   | 组的ID。                               |
| grolist  | oid[] | 一个数组，包含这个组里面所有角色的ID。 |
