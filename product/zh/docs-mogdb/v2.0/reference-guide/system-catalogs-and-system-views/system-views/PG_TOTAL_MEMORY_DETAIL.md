---
title: PG_TOTAL_MEMORY_DETAIL
summary: PG_TOTAL_MEMORY_DETAIL
author: Guo Huan
date: 2021-06-07
---

# PG_TOTAL_MEMORY_DETAIL

PG_TOTAL_MEMORY_DETAIL视图显示某个数据库节点内存使用情况。

**表 1** PG_TOTAL_MEMORY_DETAIL字段

| 名称         | 类型    | 描述                       |
| :----------- | :------ | :------------------------- |
| nodename     | text    | 节点名称。                 |
| memorytype   | text    | 内存的名称。               |
| memorymbytes | integer | 内存使用的大小，单位为MB。 |
