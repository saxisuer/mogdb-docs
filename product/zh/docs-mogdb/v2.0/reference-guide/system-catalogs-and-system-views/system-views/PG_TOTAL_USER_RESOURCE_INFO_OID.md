---
title: PG_TOTAL_USER_RESOURCE_INFO_OID
summary: PG_TOTAL_USER_RESOURCE_INFO_OID
author: Guo Huan
date: 2021-04-19
---

# PG_TOTAL_USER_RESOURCE_INFO_OID

PG_TOTAL_USER_RESOURCE_INFO_OID视图显示所有用户资源使用情况，需要使用管理员用户进行查询。此视图在参数use_workload_manager为on时才有效。

**表 1** PG_TOTAL_USER_RESOURCE_INFO_OID字段

| 名称              | 类型             | 描述                                                         |
| :---------------- | :--------------- | :----------------------------------------------------------- |
| userid            | oid              | 用户ID。                                                     |
| used_memory       | integer          | 正在使用的内存大小，单位MB。                                 |
| total_memory      | integer          | 可以使用的内存大小，单位MB。值为0表示未限制最大可用内存，其限制取决于数据库最大可用内存。 |
| used_cpu          | double precision | 正在使用的CPU核数。                                          |
| total_cpu         | integer          | 在该机器节点上，用户关联控制组的CPU核数总和。                |
| used_space        | bigint           | 已使用的存储空间大小，单位KB。                               |
| total_space       | bigint           | 可使用的存储空间大小，单位KB，值为-1表示未限制最大存储空间。 |
| used_temp_space   | bigint           | 已使用的临时空间大小，单位KB                                 |
| total_temp_space  | bigint           | 可使用的临时空间总大小，单位KB，值为-1表示未限制。           |
| used_spill_space  | bigint           | 已使用的下盘空间大小。单位KB。                               |
| total_spill_space | bigint           | 可使用的下盘空间总大小，单位KB，值为-1表示未限制。           |
| read_kbytes       | bigint           | 读磁盘数据量，单位KB。                                       |
| write_kbytes      | bigint           | 写磁盘数据量，单位KB。                                       |
| read_counts       | bigint           | 读磁盘次数。                                                 |
| write_counts      | bigint           | 写磁盘次数。                                                 |
| read_speed        | double precision | 读磁盘速率，单位B/ms。                                       |
| write_speed       | double precision | 写磁盘速率，单位B/ms。                                       |
