---
title: PG_STAT_ALL_INDEXES
summary: PG_STAT_ALL_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STAT_ALL_INDEXES

PG_STAT_ALL_INDEXES视图将包含当前数据库中的每个索引行，显示访问特定索引的统计。

索引可以通过简单的索引扫描或"位图"索引扫描进行使用。位图扫描中几个索引的输出可以通过AND或者OR规则进行组合， 因此当使用位图扫描的时候，很难将独立堆行抓取与特定索引进行组合， 因此，一个位图扫描增加pg_stat_all_indexes.idx_tup_read使用索引计数，并且增加pg_stat_all_tables.idx_tup_fetch表计数，但不影响pg_stat_all_indexes.idx_tup_fetch。

**表 1** PG_STAT_ALL_INDEXES字段

| 名称          | 类型   | 描述                                       |
| :------------ | :----- | :----------------------------------------- |
| relid         | oid    | 这个索引的表的OID。                        |
| indexrelid    | oid    | 索引的OID。                                |
| schemaname    | name   | 索引的模式名。                             |
| relname       | name   | 索引的表名。                               |
| indexrelname  | name   | 索引名。                                   |
| idx_scan      | bigint | 索引上开始的索引扫描数。                   |
| idx_tup_read  | bigint | 通过索引上扫描返回的索引项数。             |
| idx_tup_fetch | bigint | 通过使用索引的简单索引扫描抓取的活表行数。 |
