---
title: GS_MASKING
summary: GS_MASKING
author: Guo Huan
date: 2021-06-07
---

# GS_MASKING

GS_MASKING视图显示所有已配置的动态脱敏策略信息。需要有系统管理员或安全策略管理员权限才可以访问此视图。

| 名称           | 类型    | 描述                     |
| :------------- | :------ | :----------------------- |
| polname        | name    | 脱敏策略名称。           |
| polenabled     | boolean | 脱敏策略开关。           |
| maskaction     | name    | 脱敏函数。               |
| labelname      | name    | 脱敏函数作用的标签名称。 |
| masking_object | text    | 脱敏数据库资源对象。     |
| filter_name    | text    | 过滤条件的逻辑表达式。   |
