---
title: GS_AUDITING_ACCESS
summary: GS_AUDITING_ACCESS
author: Guo Huan
date: 2021-06-07
---

# GS_AUDITING_ACCESS

GS_AUDITING_ACCESS视图显示对数据库DML相关操作的所有审计信息。需要有系统管理员或安全策略管理员权限才可以访问此视图。

**表 1** GS_AUDITING_ACCESS字段

| 名称          | 类型    | 描述                                                        |
| :------------ | :------ | :---------------------------------------------------------- |
| polname       | name    | 策略名称，需要唯一，不可重复。                              |
| pol_type      | text    | 审计策略类型，值为‘access’。                                |
| polenabled    | boolean | 用来表示策略启动开关。                                      |
| access_type   | name    | DML数据库操作相关类型。例如SELECT、INSERT、DELETE等。       |
| label_name    | name    | 资源标签名称。对应系统表gs_auditing_policy中的polname字段。 |
| access_object | text    | 用来描述数据库资产的路径。                                  |
| filter_name   | text    | 过滤条件的逻辑字符串。                                      |
