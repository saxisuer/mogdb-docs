---
title: PG_SETTINGS
summary: PG_SETTINGS
author: Guo Huan
date: 2021-04-19
---

# PG_SETTINGS

PG_SETTINGS视图显示数据库运行时参数的相关信息。

**表 1** PG_SETTINGS字段

| 名称       | 类型    | 描述                                                         |
| :--------- | :------ | :----------------------------------------------------------- |
| name       | text    | 参数名称。                                                   |
| setting    | text    | 参数当前值。                                                 |
| unit       | text    | 参数的隐式结构。                                             |
| category   | text    | 参数的逻辑组。                                               |
| short_desc | text    | 参数的简单描述。                                             |
| extra_desc | text    | 参数的详细描述。                                             |
| context    | text    | 设置参数值的上下文，包括internal，postmaster，sighup，backend，superuser，user。 |
| vartype    | text    | 参数类型，包括bool，enum，integer，real，string。            |
| source     | text    | 参数的赋值方式。                                             |
| min_val    | text    | 参数最小值。如果参数类型不是数值型，那么该字段值为null。     |
| max_val    | text    | 参数最大值。如果参数类型不是数值型，那么该字段值为null。     |
| enumvals   | text[]  | enum类型参数合法值。如果参数类型不是enum型，那么该字段值为null。 |
| boot_val   | text    | 数据库启动时参数默认值。                                     |
| reset_val  | text    | 数据库重置时参数默认值。                                     |
| sourcefile | text    | 设置参数值的配置文件。如果参数不是通过配置文件赋值，那么该字段值为null。 |
| sourceline | integer | 设置参数值的配置文件的行号。如果参数不是通过配置文件赋值，那么该字段值为null。 |
