---
title: PG_STATIO_USER_INDEXES
summary: PG_STATIO_USER_INDEXES
author: Guo Huan
date: 2021-04-19
---

# PG_STATIO_USER_INDEXES

PG_STATIO_USER_INDEXES视图显示命名空间中所有用户关系表索引的IO状态信息。

**表 1** PG_STATIO_USER_INDEXES字段

| 名称          | 类型   | 描述                     |
| :------------ | :----- | :----------------------- |
| relid         | oid    | 索引的表的OID。          |
| indexrelid    | oid    | 该索引的OID。            |
| schemaname    | name   | 该索引的模式名。         |
| relname       | name   | 该索引的表名.            |
| indexrelname  | name   | 索引名称。               |
| idx_blks_read | bigint | 从索引中读取的磁盘块数。 |
| idx_blks_hit  | bigint | 索引命中缓存数。         |
