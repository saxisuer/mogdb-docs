---
title: GS_WLM_CGROUP_INFO
summary: GS_WLM_CGROUP_INFO
author: Guo Huan
date: 2021-06-07
---

# GS_WLM_CGROUP_INFO

GS_WLM_CGROUP_INFO视图显示当前执行作业的控制组的信息。

**表 1** GS_WLM_CGROUP_INFO字段

| 名称         | 类型     | 描述                      |
| :----------- | :------- | :------------------------ |
| cgoup_name   | text     | 控制组的名称。            |
| priority     | interger | 作业的优先级。            |
| usage_pecent | interger | 控制组占用的百分比。      |
| shares       | bigint   | 控制组分配的CPU资源配额。 |
| cpuacct      | bigint   | CPU配额分配。             |
| cpuset       | text     | CPU限额分配。             |
| relpath      | text     | 控制组的相对路径。        |
| valid        | text     | 该控制组是否有效。        |
| node_group   | text     | 逻辑集群名称。            |
