---
title: PG_INDEX
summary: PG_INDEX
author: Guo Huan
date: 2021-04-19
---

# PG_INDEX

PG_INDEX系统表存储索引的一部分信息，其他的信息大多数在PG_CLASS中。

**表 1** PG_INDEX字段

| 名称           | 类型         | 描述                                                         |
| :------------- | :----------- | :----------------------------------------------------------- |
| indexrelid     | oid          | 这个索引在pg_class里的记录的OID。                            |
| indrelid       | oid          | 使用这个索引的表在pg_class里的记录的OID。                    |
| indnatts       | smallint     | 索引中的字段数目。                                           |
| indnkeyatts    | smallint     | 索引中作为KEY字段的数目。                                    |
| indisunique    | Boolean      | 如果为真，这是个唯一索引。                                   |
| indisprimary   | Boolean      | 如果为真，该索引代表该表的主键。这个字段为真的时候indisunique总是为真。 |
| indisexclusion | Boolean      | 如果为真，该索引支持排他约束。                               |
| indimmediate   | Boolean      | 如果为真，在插入数据时会立即进行唯一性检查。                 |
| indisclustered | Boolean      | 如果为真，则该表最后在这个索引上建了簇。                     |
| indisusable    | Boolean      | 如果为真，该索引对insert/select可用。                        |
| indisvalid     | Boolean      | 如果为真，则该索引可以用于查询。如果为假，则该索引可能不完整，仍然必须在INSERT/UPDATE操作时进行更新，不过不能安全的用于查询。如果是唯一索引，则唯一属性也将不为真。 |
| indcheckxmin   | Boolean      | 如果为true，查询不能使用索引，直到pg_index此行的xmin低于其快照的TransactionXmin，因为该表可能包含它们能看到的不兼容行断开的HOT链。 |
| indisready     | Boolean      | 如果为真，表示此索引对插入数据是可用的，否则，在插入或修改数据时忽略此索引。 |
| indkey         | int2vector   | 这是一个包含indnatts值的数组，这些数组值表示这个索引所建立的表字段。比如一个值为1 3的意思是第一个字段和第三个字段组成这个索引键字。这个数组里的零表明对应的索引属性是在这个表字段上的一个表达式，而不是一个简单的字段引用。 |
| indcollation   | oidvector    | 索引用到的各列的ID。                                         |
| indclass       | oidvector    | 对于索引键字里面的每个字段，这个字段都包含一个指向所使用的操作符类的OID，参阅pg_opclass获取细节。 |
| indoption      | int2vector   | 存储列前标识位，该标识位是由索引的访问方法定义。             |
| indexprs       | pg_node_tree | 表达式树（以nodeToString()形式表现）用于那些非简单字段引用的索引属性。它是一个列表，个数与INDKEY中的零值个数相同。如果所有索引属性都是简单的引用，则为空。 |
| indpred        | pg_node_tree | 部分索引断言的表达式树（以nodeToString()的形式表现）。如果不是部分索引，则是空字符串。 |
| indisreplident | Boolean      | 如果为真，则此索引的列成为逻辑解码的解码列。                 |
