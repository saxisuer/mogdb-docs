---
title: GS_CLIENT_GLOBAL_KEYS_ARGS
summary: GS_CLIENT_GLOBAL_KEYS_ARGS
author: Guo Huan
date: 2021-04-19
---

# GS_CLIENT_GLOBAL_KEYS_ARGS

GS_CLIENT_GLOBAL_KEYS_ARGS系统表记录密态等值特性中客户端加密主密钥相关元数据信息，每条记录对应客户端加密主密钥的一个键值对信息。

**表 1** GS_CLIENT_GLOBAL_KEYS_ARGS字段

| 名称          | 类型  | 描述                                          |
| :------------ | :---- | :-------------------------------------------- |
| global_key_id | oid   | 客户端加密主密钥(cmk)oid。                    |
| function_name | name  | 值为encryption。                              |
| key           | name  | 客户端加密主密钥(cmk)的元数据信息对应的名称。 |
| value         | bytea | 客户端加密主密钥(cmk)的元数据信息名称的值。   |
