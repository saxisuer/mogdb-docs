---
title: GS_MATVIEW
summary: GS_MATVIEW
author: Guo Huan
date: 2021-06-07
---

# GS_MATVIEW

GS_MATVIEW系统表提供了关于数据库中每一个物化视图的信息。

**表 1** GS_MATVIEW字段

| 名称        | 类型      | 描述                                                         |
| :---------- | :-------- | :----------------------------------------------------------- |
| matviewid   | oid       | 物化视图的oid。                                              |
| mapid       | oid       | 物化视图map表的oid，map表为物化视图关联表，与物化视图一一对应。 |
| ivm         | boolean   | 物化视图的类型，t为增量物化视图，f为全量物化视图。           |
| needrefresh | boolean   | 物化视图是否需要刷新，t表示有增量数据，需要刷新。            |
| refreshtime | timestamp | 物化视图上一次刷新时间，若未刷新则为null。                   |
