---
title: PG_ATTRDEF
summary: PG_ATTRDEF
author: Guo Huan
date: 2021-04-19
---

# PG_ATTRDEF

PG_ATTRDEF系统表存储列的默认值。

**表 1** PG_ATTRDEF字段

| 名称    | 类型         | 描述                                 |
| :------ | :----------- | :----------------------------------- |
| oid     | oid          | 行标识符（隐藏属性，必须明确选择）。 |
| adrelid | oid          | 该列的所属表。                       |
| adnum   | smallint     | 该列的数目。                         |
| adbin   | pg_node_tree | 字段缺省值的内部表现形式。           |
| adsrc   | text         | 可读的缺省值的内部表现形式。         |
