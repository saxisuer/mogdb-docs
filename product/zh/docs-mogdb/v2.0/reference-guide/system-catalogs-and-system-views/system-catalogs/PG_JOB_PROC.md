---
title: PG_JOB_PROC
summary: PG_JOB_PROC
author: Guo Huan
date: 2021-04-19
---

# PG_JOB_PROC

PG_JOB_PROC系统表对应PG_JOB表中每个任务的作业内容（包括: PL/SQL代码块、匿名块）。将存储过程信息独立出来，如果放到PG_JOB中，被加载到共享内存的时候，会占用不必要的空间，所以在使用的时候再进行查询获取。

**表 1** PG_JOB_PROC字段

| 名称    | 类型    | 描述                                 |
| :------ | :------ | :----------------------------------- |
| oid     | oid     | 行标识符（隐藏属性，必须明确选择）。 |
| job_oid | integer | 关联pg_job表中的job_id。             |
| what    | text    | 作业内容。                           |
