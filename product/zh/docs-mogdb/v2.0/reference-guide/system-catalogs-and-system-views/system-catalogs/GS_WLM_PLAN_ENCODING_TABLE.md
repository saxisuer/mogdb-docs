---
title: GS_WLM_PLAN_ENCODING_TABLE
summary: GS_WLM_PLAN_ENCODING_TABLE
author: Guo Huan
date: 2021-04-19
---

# GS_WLM_PLAN_ENCODING_TABLE

GS_WLM_PLAN_ENCODING_TABLE系统表显示计划算子级的编码信息，为机器学习模型的提供包括startup time, total time, peak memory, rows等标签值的训练、预测集。

**表 1** GS_WLM_PLAN_ENCODING_TABLE的字段

| 名称           | 类型    | 描述                                       |
| :------------- | :------ | :----------------------------------------- |
| queryid        | bigint  | 语句执行使用的内部query_id。               |
| plan_node_id   | integer | 查询对应的执行计划的plan node id。         |
| parent_node_id | integer | 当前算子的父节点node id。                  |
| startup_time   | bignit  | 该算子处理第一条数据的开始时间。           |
| total_time     | bigint  | 该算子到结束时候总的执行时间(ms)。         |
| rows           | bigint  | 当前算子执行的行数信息。                   |
| peak_memory    | integer | 当前算子在数据库节点上的最大内存峰值(MB)。 |
| encode         | text    | 当前计划算子的编码信息。                   |
