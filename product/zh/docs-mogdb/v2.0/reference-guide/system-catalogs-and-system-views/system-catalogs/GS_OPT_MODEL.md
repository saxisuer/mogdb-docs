---
title: GS_OPT_MODEL
summary: GS_OPT_MODEL
author: Guo Huan
date: 2021-04-19
---

# GS_OPT_MODEL

GS_OPT_MODEL是启用AiEngine执行计划时间预测功能时的数据表，记录机器学习模型的配置、训练结果、功能、对应系统函数、训练历史等相关信息。

**表 1** GS_OPT_MODEL字段

| 名称          | 类型     | 描述                                                         |
| :------------ | :------- | :----------------------------------------------------------- |
| template_name | name     | 机器学习模型的模板名，决定训练和预测调用的函数接口，目前只实现了rlstm，方便后续扩展。 |
| model_name    | name     | 模型的实例名，每个模型对应aiEngine在线学习进程中的一套参数、训练日志、模型系数。此列需为unique。 |
| datname       | name     | 该模型所服务的database名，每个模型只针对单个database。此参数决定训练时所使用的数据。 |
| ip            | name     | AiEngine端所部署的host ip地址。                              |
| port          | integer  | AiEngine端所监听的端口号。                                   |
| max_epoch     | integer  | 模型每次训练的迭代次数上限。                                 |
| learning_rate | real     | 模型训练的学习速率，推荐缺省值1。                            |
| dim_red       | real     | 模型特征维度降维系数。                                       |
| hidden_units  | integer  | 模型隐藏层神经元个数。如果训练发现模型长期无法收敛，可以适量提升本参数。 |
| batch_size    | integer  | 模型每次迭代时一个batch的大小，尽量设为大于等于训练数据总量的值，加快模型的收敛速度。 |
| feature_size  | integer  | [不需设置] 模型特征的长度，用于触发重新训练，模型训练后该参数自动更新。 |
| available     | boolean  | [不需设置]标识模型是否收敛。                                 |
| Is_training   | boolean  | [不需设置]标识模型是否正在训练。                             |
| label         | "char"[] | 模型的目标任务: <br/>- S: startup time<br/>- T: total time<br/>- R: rows<br/>- M: peak memory<br/>目前受模型性能限制，推荐{S, T}或{R}。 |
| max           | bigint[] | [不需设置]标识模型各任务标签的最大值，用于触发重新训练。     |
| acc           | real[]   | [不需设置]标识模型个任务的准确率。                           |
| description   | text     | 模型注释。                                                   |
