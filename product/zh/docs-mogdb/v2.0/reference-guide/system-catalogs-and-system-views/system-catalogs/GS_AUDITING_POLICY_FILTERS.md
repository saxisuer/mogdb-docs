---
title: GS_AUDITING_POLICY_FILTERS
summary: GS_AUDITING_POLICY_FILTERS
author: Guo Huan
date: 2021-06-07
---

# GS_AUDITING_POLICY_FILTERS

GS_AUDITING_POLICY_FILTERS系统表记录统一审计相关的过滤策略相关信息，每条记录对应一个设计策略。需要有系统管理员或安全策略管理员权限才可以访问此系统表。

**表 1** GS_AUDITING_POLICY_FILTERS字段

| 名称            | 类型      | 描述                                          |
| :-------------- | :-------- | :-------------------------------------------- |
| filtertype      | name      | 过滤类型。目前值仅为logical_expr。            |
| labelname       | name      | 名称。目前值仅为logical_expr。                |
| policyoid       | oid       | 对应审计策略系统表GS_AUDITING_POLICY中的oid。 |
| modifydate      | timestamp | 创建或修改的最新时间戳。                      |
| logicaloperator | text      | 过滤条件的逻辑字符串。                        |
