---
title: OPERATOR_HISTORY
summary: OPERATOR_HISTORY
author: Guo Huan
date: 2021-04-19
---

# OPERATOR_HISTORY

OPERATOR_HISTORY视图显示的是当前用户数据库主节点上执行作业结束后的算子的相关记录。记录的数据同[附录-表6](../../../reference-guide/DBE_PERF/appendices/table-6.md)。
