---
title: SUMMARY_WORKLOAD_TRANSACTION
summary: SUMMARY_WORKLOAD_TRANSACTION
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_WORKLOAD_TRANSACTION

显示MogDB内汇聚的负载事务信息。

**表 1** SUMMARY_WORKLOAD_TRANSACTION字段

| **名称**            | **类型** | **描述**                             |
| :------------------ | :------- | :----------------------------------- |
| workload            | name     | 负载的名称。                         |
| commit_counter      | numeric  | 用户事务commit数量。                 |
| rollback_counter    | numeric  | 用户事务rollback数量。               |
| resp_min            | bigint   | 用户事务最小响应时间（单位: 微秒）。 |
| resp_max            | bigint   | 用户事务最大响应时间（单位: 微秒）。 |
| resp_avg            | bigint   | 用户事务平均响应时间（单位: 微秒）。 |
| resp_total          | numeric  | 用户事务总响应时间（单位: 微秒）。   |
| bg_commit_counter   | numeric  | 后台事务commit数量。                 |
| bg_rollback_counter | numeric  | 后台事务rollback数量。               |
| bg_resp_min         | bigint   | 后台事务最小响应时间（单位: 微秒）。 |
| bg_resp_max         | bigint   | 后台事务最大响应时间（单位: 微秒）。 |
| bg_resp_avg         | bigint   | 后台事务平均响应时间（单位: 微秒）。 |
| bg_resp_total       | numeric  | 后台事务总响应时间（单位: 微秒）。   |
