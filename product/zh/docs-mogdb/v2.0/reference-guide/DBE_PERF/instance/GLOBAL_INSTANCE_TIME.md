---
title: GLOBAL_INSTANCE_TIME
summary: GLOBAL_INSTANCE_TIME
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_INSTANCE_TIME

提供MogDB中所有正常节点下的各种时间消耗信息(时间类型见instance_time视图)。

**表 1** GLOBAL_INSTANCE_TIME字段

| **名称**  | **类型** | **描述**               |
| :-------- | :------- | :--------------------- |
| node_name | name     | 数据库进程的名称。     |
| stat_id   | integer  | 统计编号。             |
| stat_name | text     | 类型名称。             |
| value     | bigint   | 时间值（单位: 微秒）。 |
