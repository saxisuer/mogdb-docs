---
title: GLOBAL_STAT_SESSION_CU
summary: GLOBAL_STAT_SESSION_CU
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STAT_SESSION_CU

GLOBAL_STAT_SESSION_CU用于查询MogDB各个节点，当前运行session的CU命中情况。session退出相应的统计数据会清零。MogDB重启后，统计数据也会清零。

**表 1** GLOBAL_STAT_SESSION_CU字段

| **名称**      | **类型** | **描述**         |
| :------------ | :------- | :--------------- |
| mem_hit       | integer  | 内存命中次数。   |
| hdd_sync_read | integer  | 硬盘同步读次数。 |
| hdd_asyn_read | integer  | 硬盘异步读次数。 |
