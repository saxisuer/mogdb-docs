---
title: STATIO_SYS_SEQUENCES
summary: STATIO_SYS_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# STATIO_SYS_SEQUENCES

STATIO_SYS_SEQUENCES显示命名空间中所有系统表为序列的IO状态信息。

**表 1** STATIO_SYS_SEQUENCES字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| relid      | oid      | 序列OID。                |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | bigint   | 从序列中读取的磁盘块数。 |
| blks_hit   | bigint   | 序列中缓存命中数。       |
