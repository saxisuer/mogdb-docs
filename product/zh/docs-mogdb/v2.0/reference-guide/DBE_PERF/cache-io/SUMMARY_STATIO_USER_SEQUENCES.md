---
title: SUMMARY_STATIO_USER_SEQUENCES
summary: SUMMARY_STATIO_USER_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_USER_SEQUENCES

SUMMARY_STATIO_USER_SEQUENCES视图显示MogDB内汇聚的命名空间中所有用户关系表类型为序列的IO状态信息。

**表 1** SUMMARY_STATIO_USER_SEQUENCES字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | numeric  | 从序列中读取的磁盘块数。 |
| blks_hit   | numeric  | 序列中缓存命中数。       |
