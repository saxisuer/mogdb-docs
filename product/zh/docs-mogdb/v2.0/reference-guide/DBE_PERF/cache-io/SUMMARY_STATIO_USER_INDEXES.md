---
title: SUMMARY_STATIO_USER_INDEXES
summary: SUMMARY_STATIO_USER_INDEXES
author: Guo Huan
date: 2021-04-19
---

# SUMMARY_STATIO_USER_INDEXES

SUMMARY_STATIO_USER_INDEXES视图显示MogDB内汇聚的命名空间中所有用户关系表索引的IO状态信息。

**表 1** SUMMARY_STATIO_USER_INDEXES字段

| **名称**      | **类型** | **描述**                 |
| :------------ | :------- | :----------------------- |
| schemaname    | name     | 该索引的模式名。         |
| relname       | name     | 该索引的表名。           |
| indexrelname  | name     | 索引名称。               |
| idx_blks_read | numeric  | 从索引中读取的磁盘块数。 |
| idx_blks_hit  | numeric  | 索引命中缓存数。         |
