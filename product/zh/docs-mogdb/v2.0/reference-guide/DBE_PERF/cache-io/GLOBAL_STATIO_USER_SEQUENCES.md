---
title: GLOBAL_STATIO_USER_SEQUENCES
summary: GLOBAL_STATIO_USER_SEQUENCES
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_STATIO_USER_SEQUENCES

GLOBAL_STATIO_USER_SEQUENCES视图显示各节点的命名空间中所有用户关系表类型为序列的IO状态信息。

**表 1** GLOBAL_STATIO_USER_SEQUENCES字段

| **名称**   | **类型** | **描述**                 |
| :--------- | :------- | :----------------------- |
| node_name  | name     | 数据库进程名称。         |
| relid      | oid      | 序列OID。                |
| schemaname | name     | 序列中模式名。           |
| relname    | name     | 序列名。                 |
| blks_read  | bigint   | 从序列中读取的磁盘块数。 |
| blks_hit   | bigint   | 序列中缓存命中数。       |
