---
title: GLOBAL_REPLICATION_STAT
summary: GLOBAL_REPLICATION_STAT
author: Guo Huan
date: 2021-04-19
---

# GLOBAL_REPLICATION_STAT

GLOBAL_REPLICATION_STAT视图用于获得各节点描述日志同步状态信息，如发起端发送日志位置，收端接收日志位置等。

**表 1** GLOBAL_REPLICATION_STAT字段

| **名称**                 | **类型**                 | **描述**                                                  |
| :----------------------- | :----------------------- | :-------------------------------------------------------- |
| node_name                | name                     | 数据库进程名称。                                          |
| pid                      | bigint                   | 线程的PID。                                               |
| usesysid                 | oid                      | 用户系统ID。                                              |
| usename                  | name                     | 用户名。                                                  |
| application_name         | text                     | 程序名称。                                                |
| client_addr              | inet                     | 客户端地址。                                              |
| client_hostname          | text                     | 客户端名。                                                |
| client_port              | integer                  | 客户端端口。                                              |
| backend_start            | timestamp with time zone | 程序启动时间。                                            |
| state                    | text                     | 日志复制的状态: <br/>- 追赶状态。<br/>- 一致的流状态。    |
| sender_sent_location     | text                     | 发送端发送日志位置。                                      |
| receiver_write_location  | text                     | 接收端write日志位置。                                     |
| receiver_flush_location  | text                     | 接收端flush日志位置。                                     |
| receiver_replay_location | text                     | 接收端replay日志位置。                                    |
| sync_priority            | integer                  | 同步复制的优先级（0表示异步）。                           |
| sync_state               | text                     | 同步状态: <br/>- 异步复制<br/>- 同步复制<br/>- 潜在同步者 |
