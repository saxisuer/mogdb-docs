---
title: DDL语法一览表
summary: DDL语法一览表
author: Zhang Cuiping
date: 2021-05-17
---

# DDL语法一览表

DDL（Data Definition Language数据定义语言），用于定义或修改数据库中的对象。如：表、索引、视图等。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
> MogDB不支持数据库主节点不完整时进行DDL操作。例如：MogDB中有1个数据库主节点故障时执行新建数据库、表等操作都会失败。

## 定义数据库

数据库是组织、存储和管理数据的仓库，而数据库定义主要包括：创建数据库、修改数据库属性，以及删除数据库。所涉及的SQL语句，请参考[表1](#databasedefinition)。

**表 1** 数据库定义相关SQL <a id="databasedefinition"></a>

| 功能           | 相关SQL         |
| :------------- | :-------------- |
| 创建数据库     | CREATE DATABASE |
| 修改数据库属性 | ALTER DATABASE  |
| 删除数据库     | DROP DATABASE   |

## 定义模式

模式是一组数据库对象的集合，主要用于控制对数据库对象的访问。所涉及的SQL语句，请参考[表2](#modedefinition)。

**表 2** 模式定义相关SQL <a id="modedefinition"> </a>

| 功能         | 相关SQL       |
| :----------- | :------------ |
| 创建模式     | CREATE SCHEMA |
| 修改模式属性 | ALTER SCHEMA  |
| 删除模式     | DROP SCHEMA   |

## 定义表空间

表空间用于管理数据对象，与磁盘上的一个目录对应。所涉及的SQL语句，请参考[表3](#tablespacedefi)。

**表 3** 表空间定义相关SQL <a id="tablespacedefi"> </a>

| 功能           | 相关SQL           |
| :------------- | :---------------- |
| 创建表空间     | CREATE TABLESPACE |
| 修改表空间属性 | ALTER TABLESPACE  |
| 删除表空间     | DROP TABLESPACE   |

## 定义表

表是数据库中的一种特殊数据结构，用于存储数据对象以及对象之间的关系。所涉及的SQL语句，请参考[表4](#tabledefi)。

**表 4** 表定义相关SQL <a id="tabledefi"> </a>

| 功能       | 相关SQL      |
| :--------- | :----------- |
| 创建表     | CREATE TABLE |
| 修改表属性 | ALTER TABLE  |
| 删除表     | DROP TABLE   |

## 定义分区表

分区表是一种逻辑表，数据是由普通表存储的，主要用于提升查询性能。所涉及的SQL语句，请参考[表5](#partitiondefi)。

**表 5** 分区表定义相关SQL <a id="partitiondefi"> </a>

| 功能           | 相关SQL                |
| :------------- | :--------------------- |
| 创建分区表     | CREATE TABLE PARTITION |
| 创建分区       | ALTER TABLE PARTITION  |
| 修改分区表属性 | ALTER TABLE PARTITION  |
| 删除分区       | ALTER TABLE PARTITION  |
| 删除分区表     | DROP TABLE             |

## 定义索引

索引是对数据库表中一列或多列的值进行排序的一种结构，使用索引可快速访问数据库表中的特定信息。所涉及的SQL语句，请参考[表6](#indexdefi)。

**表 6** 索引定义相关SQL <a id="indexdefi"> </a>

| 功能         | 相关SQL      |
| :----------- | :----------- |
| 创建索引     | CREATE INDEX |
| 修改索引属性 | ALTER INDEX  |
| 删除索引     | DROP INDEX   |
| 重建索引     | REINDEX      |

## 定义存储过程

存储过程是一组为了完成特定功能的SQL语句集，经编译后存储在数据库中，用户通过指定存储过程的名称并给出参数（如果该存储过程带有参数）来执行它。所涉及的SQL语句，请参考[表7](#storageprocess)。

**表 7** 存储过程定义相关SQL <a id="storageprocess"> </a>

| 功能         | 相关SQL          |
| :----------- | :--------------- |
| 创建存储过程 | CREATE PROCEDURE |
| 删除存储过程 | DROP PROCEDURE   |

## 定义函数

在MogDB中，它和存储过程类似，也是一组SQL语句集，使用上没有差别。所涉及的SQL语句，请参考[表8](#functiondefi)。

**表 8** 函数定义相关SQL <a id="functiondefi"> </a>

| 功能         | 相关SQL         |
| :----------- | :-------------- |
| 创建函数     | CREATE FUNCTION |
| 修改函数属性 | ALTER FUNCTION  |
| 删除函数     | DROP FUNCTION   |

## 定义视图

视图是从一个或几个基本表中导出的虚表，可用于控制用户对数据访问，请参考[表9](#viewdefi)。

**表 9** 视图定义相关SQL <a id="viewdefi"> </a>

| 功能     | 相关SQL     |
| :------- | :---------- |
| 创建视图 | CREATE VIEW |
| 删除视图 | DROP VIEW   |

## 定义游标

为了处理SQL语句，存储过程进程分配一段内存区域来保存上下文联系。游标是指向上下文区域的句柄或指针。借助游标，存储过程可以控制上下文区域的变化，请参考[表10](#cursordefi)。

**表 10** 游标定义相关SQL <a id="cursordefi"></a>

| 功能             | 相关SQL |
| :--------------- | :------ |
| 创建游标         | CURSOR  |
| 移动游标         | MOVE    |
| 从游标中提取数据 | FETCH   |
| 关闭游标         | CLOSE   |
