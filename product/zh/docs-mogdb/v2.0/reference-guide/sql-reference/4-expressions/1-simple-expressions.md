---
title: 简单表达式
summary: 简单表达式
author: Zhang Cuiping
date: 2021-05-17
---

# 简单表达式

## 逻辑表达式

逻辑表达式的操作符和运算规则，请参见逻辑操作符。

## 比较表达式

常用的比较操作符，请参见操作符。

除比较操作符外，还可以使用以下句式结构：

- BETWEEN操作符

  a BETWEEN x AND y等效于a &gt;= x AND a &lt;= y

  a NOT BETWEEN x AND y等效于a &lt; x OR a &gt; y

- 检查一个值是不是null，可使用：

  expression IS NULL

  expression IS NOT NULL

  或者与之等价的句式结构，但不是标准的：

  expression ISNULL

  expression NOTNULL

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
  > 不要写expression=NULL或expression&lt;&gt;(!=)NULL，因为NULL代表一个未知的值，不能通过该表达式判断两个未知值是否相等。

## 示例

```sql
mogdb=# SELECT 2 BETWEEN 1 AND 3 AS RESULT;
 result
----------
 t
(1 row)

mogdb=# SELECT 2 >= 1 AND 2 <= 3 AS RESULT;
 result
----------
 t
(1 row)

mogdb=# SELECT 2 NOT BETWEEN 1 AND 3 AS RESULT;
 result
----------
 f
(1 row)

mogdb=# SELECT 2 < 1 OR 2 > 3 AS RESULT;
 result
----------
 f
(1 row)

mogdb=# SELECT 2+2 IS NULL AS RESULT;
 result
----------
 f
(1 row)

mogdb=# SELECT 2+2 IS NOT NULL AS RESULT;
 result
----------
 t
(1 row)

mogdb=# SELECT 2+2 ISNULL AS RESULT;
 result
----------
 f
(1 row)

mogdb=# SELECT 2+2 NOTNULL AS RESULT;
 result
----------
 t
(1 row)

mogdb=# SELECT 2+2 IS DISTINCT FROM NULL AS RESULT;
 result
----------
 t
(1 row)

mogdb=# SELECT 2+2 IS NOT DISTINCT FROM NULL AS RESULT;
 result
----------
 f
(1 row)
```

## 伪列

### ROWNUM

ROWNUM是一个伪列， 它返回一个数字表示从查询中获取结果的行编号，第一行的ROWNUM为1， 第二行的为2，依此类推。ROWNUM的返回类型为BIGINT。ROWNUM可以用于限制查询返回的总行数， 例如下面查询用于从Students表中返回最多10条记录。

```sql
select * from Students where rownum <= 10;
```
