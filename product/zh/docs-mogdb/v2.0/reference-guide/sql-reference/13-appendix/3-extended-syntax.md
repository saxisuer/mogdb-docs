---
title: 扩展语法
summary: 扩展语法
author: Zhang Cuiping
date: 2021-05-18
---

# 扩展语法

MogDB提供的扩展语法如下。

**表 1** 扩展SQL语法

| 类别                                                         | 语法关键字                                                   | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| 创建表（CREATE TABLE）                                       | **INHERITS ( parent_table [, … ] )**                         | 是否支持继承表。                                             |
| table_constraint:<br />**EXCLUDE USING index_method** index_parameters [ WHERE ( predicate ) ] \| **FOREIGN KEY ( column_name [, … ] ) REFERENCES reftable [ ( refcolumn [, … ] ) ]**<br />**[ MATCH FULL \| MATCH PARTIAL \| MATCH SIMPLE ] [ ON DELETE action ] [ ON UPDATE action ]** | 不支持用EXCLUDE **USING index_method** 为表创建排除约束。    |                                                              |
| 修改表（ALTER TABLE）                                        | **TO { GROUP groupname \| NODE ( nodename [, … ] ) }**       | 修改表数据分布的数据节点列表。                               |
| **DELETE NODE ( nodename [, … ] )**                          | 删除表数据分布的数据节点。                                   |                                                              |
| 加载模块                                                     | **CREATE EXTENSION**                                         | 把一个新的模块（例如DBLINK）加载进当前数据库中。             |
| **ALTER EXTENSION**                                          | 修改已加载的模块。                                           |                                                              |
| **DROP EXTENSION**                                           | 删除已加载的模块。                                           |                                                              |
| 聚集函数                                                     | **CREATE AGGREGATE**                                         | 定义一个新的聚集函数。                                       |
| **ALTER AGGREGATE**                                          | 修改一个聚集函数的定义。                                     |                                                              |
| **DROP AGGREGATE**                                           | 删除一个现存的聚集函数。                                     |                                                              |
| 操作符                                                       | **CREATE OPERATOR**                                          | 定义一个新的操作符。                                         |
| **ALTER OPERATOR**                                           | 修改一个操作符的定义。                                       |                                                              |
| **DROP OPERATOR**                                            | 从数据库中删除一个现存的操作符。                             |                                                              |
| 操作符类                                                     | **CREATE OPERATOR CLASS**                                    | 定义一个新的操作符类。                                       |
| **ALTER OPERATOR CLASS**                                     | 修改一个操作符类的定义。                                     |                                                              |
| **DROP OPERATOR CLASS**                                      | 删除一个现有操作符类。                                       |                                                              |
| 操作符族                                                     | **CREATE OPERATOR FAMILY**                                   | 定义一个新的操作符族。                                       |
| **ALTER OPERATOR FAMILY**                                    | 修改一个操作符族的定义。                                     |                                                              |
| **DROP OPERATOR FAMILY**                                     | 删除一个已有的操作符族。                                     |                                                              |
| 文本检索解析器                                               | **CREATE TEXT SEARCH PARSER**                                | 创建一个新的文本检索解析器。                                 |
| **ALTER TEXT SEARCH PARSER**                                 | 修改文本检索解析器。                                         |                                                              |
| **DROP TEXT SEARCH PARSER**                                  | 删除现有的文本检索解析器。                                   |                                                              |
| 文本检索模板                                                 | **CREATE TEXT SEARCH TEMPLATE**                              | 创建一个新的文本检索模板。                                   |
| **ALTER TEXT SEARCH TEMPLATE**                               | 修改文本检索模板。                                           |                                                              |
| **DROP TEXT SEARCH TEMPLATE**                                | 删除现有的文本检索模板。                                     |                                                              |
| 排序规则                                                     | **CREATE COLLATION**                                         | 创建排序规则。<br />排序规则使得用户可以定义数据在列级，甚至是操作级的排序规则和字符分类行为。 |
| **ALTER COLLATION**                                          | 修改排序规则。                                               |                                                              |
| **DROP COLLATION**                                           | 删除排序规则。                                               |                                                              |
| 生成一个通知                                                 | **NOTIFY**                                                   | NOTIFY命令发送带有可选的“有效载荷”字符串的通知给先前执行监听数据库中指定的同一渠道的每一个客户端。 |
| 监听一个通知                                                 | **LISTEN**                                                   | 给当前会话注册一个监听器。                                   |
| 停止监听通知信息                                             | **UNLISTEN**                                                 | 清除本次会话注册的所有监听器。                               |
| 加载或重新加载一个共享库文件                                 | **LOAD**                                                     | 加载一个共享库文件到数据库服务器的地址空间。                 |
| 释放一个数据库的会话资源                                     | **DISCARD**                                                  | 释放与此数据库会话的相关资源。                               |
| 过程语言                                                     | **CREATE LANGUAGE**                                          | 注册一个新的语言。                                           |
| **ALTER LANGUAGE**                                           | 修改一个过程语言的定义。                                     |                                                              |
| **DROP LANGUAGE**                                            | 删除一个过程语言。                                           |                                                              |
| 域                                                           | **CREATE DOMAIN**                                            | 创建一个新的域。                                             |
| **ALTER DOMAIN**                                             | 修改现有域的定义。                                           |                                                              |
| **DROP DOMAIN**                                              | 删除一个域。                                                 |                                                              |
| 编码转换                                                     | **CREATE CONVERSION**                                        | 定义字符集之间的转换。                                       |
| **ALTER CONVERSION**                                         | 修改一个编码转换的定义。                                     |                                                              |
| **DROP CONVERSION**                                          | 删除一个先前定义过的编码转换。                               |                                                              |
| 类型转换                                                     | **CREATE CAST**                                              | 定义一个新的转换。一个转换说明如何在两个类型之间进行转换。   |
| **DROP CAST**                                                | 删除一个先前定义过的类型转换。                               |                                                              |
| 创建游标                                                     | CURSOR name [ BINARY ] [ **INSENSITIVE** ] [ [ NO ] **SCROLL** ] [ **WITH HOLD** ] FOR query | INSENSITIVE：<br />这个关键字没有什么作用，只在为了兼容SQL标准。<br />SCROLL：声明该游标可以用于反向检索。<br />WITH HOLD:声明该游标可以在创建它的事务成功提交后继续使用。 |
| 移动游标                                                     | MOVE **BACKWARD**                                            | 反向移动游标，需与SCROLL结合才能使用。                       |
