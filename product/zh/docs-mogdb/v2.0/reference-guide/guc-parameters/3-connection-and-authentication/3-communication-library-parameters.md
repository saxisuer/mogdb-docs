---
title: 通信库参数
summary: 通信库参数
author: Zhang Cuiping
date: 2021-04-20
---

# 通信库参数

本节介绍通信库相关的参数设置及取值范围等内容。

## tcp_keepalives_idle

**参数说明**: 在支持TCP_KEEPIDLE套接字选项的系统上，设置发送活跃信号的间隔秒数。不设置发送保持活跃信号，连接就会处于闲置状态。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 如果操作系统不支持TCP_KEEPIDLE选项 ，这个参数的值必须为0。
> - 在通过Unix域套接字进行的连接的操作系统上，这个参数将被忽略。

**取值范围**: 0-3600，单位为s。

**默认值**: 60

## tcp_keepalives_interval

**参数说明**: 在支持TCP_KEEPINTVL套接字选项的操作系统上，以秒数声明在重新传输之间等待响应的时间。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 0-180，单位为s。

**默认值**: 30

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 如果操作系统不支持TCP_KEEPINTVL选项，这个参数的值必须为0。
> - 在通过Unix域套接字进行的连接的操作系统上，这个参数将被忽略。

## tcp_keepalives_count

**参数说明**: 在支持TCP_KEEPCNT套接字选项的操作系统上，设置MogDB服务端在断开与客户端连接之前可以等待的保持活跃信号个数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 如果操作系统不支持TCP_KEEPCNT选项，这个参数的值必须为0。
> - 在通过Unix域套接字进行连接的操作系统上，这个参数将被忽略。

**取值范围**: 0-100，其中0表示MogDB未收到客户端反馈的保持活跃信号则立即断开连接。

**默认值**: 20

## comm_tcp_mode

**参数说明**: 通信库使用TCP或SCTP协议建立数据通道的切换开关，重启MogDB生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> SCTP协议的连接不再提供支持，为了保持兼容，提供此参数的接口，但此参数会在设置过程中强制改为on。

**取值范围**: 布尔型，设置为on表示使用TCP模式连接数据库节点。

**默认值**: on

## comm_sctp_port

**参数说明**: TCP代理通信库或SCTP通信库使用的TCP或SCTP协议侦听端口，负责侦听数据报文通道。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> MogDB部署时会自动分配此端口号，请不要轻易修改此参数，如端口号配置不正确会导致数据库通信失败。

**取值范围**: 整型，最小值为0，最大值为65535。

**默认值**: 7000

## comm_control_port

**参数说明**: TCP代理通信库或SCTP通信库使用的TCP协议侦听端口。

该参数属于INTERNAL类型参数，为固定参数，用户无法修改此参数，只能查看。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> MogDB部署时会自动分配此端口号，请不要轻易修改此参数，如端口号配置不正确会导致数据库通信失败。

**取值范围**: 整型，最小值为0，最大值为65535。

**默认值**: 7001

## comm_max_receiver

**参数说明**: TCP代理通信库或SCTP通信库内部接收线程数量。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为1，最大值为50。

**默认值**: 4

## comm_quota_size

**参数说明**: TCP代理通信库或SCTP通信库最大可连续发送包总大小。使用1GE网卡时，建议取较小值，推荐设置为20KB~40KB。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为2048000，默认单位为KB。

**默认值**: 1MB

## comm_usable_memory

**参数说明**: 单个数据库节点内TCP代理通信库或SCTP通信库缓存最大可使用内存。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 此参数需根据环境内存及部署方式具体配置，过大会造成OOM，过小会降低TCP代理通信库或SCTP通信库性能。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为100*1024，最大值为INT_MAX/2，默认单位为KB。

**默认值**: 4000MB

## comm_memory_pool

**参数说明**: 单个数据库节点内TCP代理通信库或SCTP通信库可使用内存池资源的容量大小。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 此参数需根据实际业务情况做调整，若通信库使用内存小，可设置该参数数值较小，反之设置数值较大。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为100*1024，最大值为INT_MAX/2，默认单位为KB。

**默认值**: 2000MB

## comm_memory_pool_percent

**参数说明**: 单个数据库节点内TCP代理通信库或SCTP通信库可使用内存池资源的百分比，用于自适应负载预留通信库通信消耗的内存大小。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 此参数需根据实际业务情况做调整，若通信库使用内存小，可设置该参数数值较小，反之设置数值较大。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为100。

**默认值**: 0

## comm_no_delay

**参数说明**: 是否使用通信库连接的NO_DELAY属性，重启MogDB生效。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 如果MogDB出现因每秒接收数据包过多导致的丢包时，需设置为off，以便小包合并成大包发送，减少数据包总数。

**默认值**: off

## comm_debug_mode

**参数说明**: TCP代理通信库或SCTP通信库debug模式开关，该参数设置是否打印通信层详细日志。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 设置为on时，打印日志量较大，会增加额外的overhead并降低数据库性能，仅在调试时打开。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打印通信库详细debug日志。
- off表示不打印通信库详细debug日志。

**默认值**: off

## comm_ackchk_time

**参数说明**: 无数据包接收情况下，该参数设置通信库服务端主动ACK触发时长。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，最小值为0，最大值为20000，单位为毫秒。取值为0表示关闭此功能。

**默认值**: 2000

## comm_timer_mode

**参数说明**: TCP代理通信库或SCTP通信库timer模式开关，该参数设置是否打印通信层各阶段时间桩。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 设置为on时，打印日志量较大，会增加额外的overhead并降低数据库性能，仅在调试时打开。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打印通信库详细时间桩日志。
- off表示不打印通信库详细时间桩日志。

**默认值**: off

## comm_stat_mode

**参数说明**: TCP代理通信库或SCTP通信库stat模式开关，该参数设置是否打印通信层的统计信息。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 设置为on时，打印日志量较大，会增加额外的overhead并降低数据库性能，仅在调试时打开。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示打印通信库统计信息日志。
- off表示不打印通信库统计信息日志。

**默认值**: off
