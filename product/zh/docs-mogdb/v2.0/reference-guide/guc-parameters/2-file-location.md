---
title: 文件位置
summary: 文件位置
author: Zhang Cuiping
date: 2021-04-20
---

# 文件位置

数据库安装后会自动生成三个配置文件（postgresql.conf、pg_hba.conf和pg_ident.conf），并统一存放在数据目录（data）下。用户可以使用本节介绍的方法修改配置文件的名称和存放路径。

修改任意一个配置文件的存放目录时，postgresql.conf里的data_directory参数必须设置为实际数据目录（data）。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 考虑到配置文件修改一旦出错对数据库的影响很大，不建议安装后再修改本节的配置文件。

## data_directory

**参数说明**: 设置MogDB的数据目录（data目录）。此参数可以通过如下方式指定。

- 在安装MogDB时指定。
- 该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，长度大于0

**默认值**: 安装时指定，如果在安装时不指定，则默认不初始化数据库。

## config_file

**参数说明**: 设置主服务器配置文件名称（postgresql.conf）。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置，不支持使用表[GUC参数设置方式](30-appendix.md)中的方式四进行修改。

**取值范围**: 字符串，长度大于0

**默认值**: postgresql.conf(实际安装可能带有绝对目录)

## hba_file

**参数说明**: 设置基于主机认证（HBA）的配置文件（pg_hba.conf）。此参数只能在配置文件postgresql.conf中指定。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: pg_hba.conf(实际安装可能带有绝对目录)

## ident_file

**参数说明**: 设置用于客户端认证的配置文件的名称（pg_ident.conf）。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: pg_ident.conf(实际安装可能带有绝对目录)

## external_pid_file

**参数说明**: 声明可被服务器管理程序使用的额外PID文件。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 这个参数只能在数据库服务重新启动后生效。

**取值范围**: 字符串

**默认值**: 空
