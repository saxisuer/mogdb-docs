---
title: 主服务器
summary: 主服务器
author: Zhang Cuiping
date: 2021-04-20
---

# 主服务器

## synchronous_standby_names

**参数说明**: 潜在同步复制的备机名称列表，每个名称用逗号分隔。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 当前连接的同步备机是列表中的第一个名称。如果当前同步备机失去连接，则它会立即更换下一个优先级更高的备机，并将此备机的名称放入列表中。
> - 备机名称可以通过设置环境变量PGAPPNAME指定。

**取值范围**: 字符串。当取值为*，表示匹配任意提供同步复制的备机名称。支持按如下格式配置：

- ANY **num_sync (standby_name** [, …])
- [FIRST] **num_sync (standby_name** [, …])
- **standby_name** [, …]

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - 其中 **num_sync** 是事务需要等待其回复的同步复制的备机的数量，**standby_name** 是备机的名称，FIRST以及ANY指定从所列服务器中选取同步复制的备机的策略
>
> - ANY N (node1,node2,…) 表示在括号内任选N个主机名称作为同步复制的备机名称列表。例如，ANY 1 (node1,node2) 表示在node1和node2中任选一个作为同步复制的备机名称。
>
> - FIRST N (node1,node2,…)表示在括号内按出现顺序的先后作为优先级选择前N个主机名称作为同步复制的备机名称列表。例如，FIRST 1 (node1,node2)表示选择node1作为同步复制的备机名称。
>
> - node1,node2,…和FIRST 1 (node1,node2,…) 具有的含义相同
>
> - 若使用gs_guc工具设置该参数，需要如下设置：
>
>   ```
>   gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY NODE 1(dn_instanceId1, dn_instanceId2)'";
>   ```
>
> 或者：
>
> ```
>  gs_guc reload -Z datanode -N @NODE_NAME@ -D @DN_PATH@ -c "synchronous_standby_names='ANY 1(AZ1, AZ2)'";
> ```

**默认值：***

## most_available_sync

**参数说明**: 在有同步备机故障时，主机事务不因同步备机故障而被阻塞。比如有两个同步备机，一个故障，另一个正常，这个时候主机事务只会等好的这个同步备，而不被故障的同步备所阻塞； 再比如走quroum协议时，一主三同步备，配置ANY 2(node1,node2,node3)，当node1、node3故障，node2正常时，主机业务同样不被阻塞。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示在有同步备机故障时，不阻塞主机。
- off表示在有同步备机故障时，阻塞主机。

**默认值**: off

## enable_stream_replication

**参数说明**: 控制主备、主从是否进行数据和日志同步。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 此参数属于性能测试参数，用于测试带有备机和不带备机的性能参数。关闭参数后，不能进行切换、故障等异常场景测试，否则会出现主备从不一致的情况。
> - 此参数属于受控参数，不建议正常业务场景下关闭此参数。

**取值范围**: 布尔型

- on表示打开主备、主从同步。
- off表示关闭主备、主从同步。

**默认值**: on

## enable_mix_replication

**参数说明**: 控制主备、主从之间WAL日志及数据复制的方式。

该参数属于INTERNAL类型参数，默认值为off，不允许外部修改。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> 此参数目前不允许正常业务场景下改变其值，即关闭WAL日志、数据页混合复制模式。

**取值范围**: 布尔型

- on表示打开WAL日志、数据页混合复制模式。
- off表示关闭WAL日志、数据页混合复制模式。

**默认值**: off

## vacuum_defer_cleanup_age

**参数说明**: 指定VACUUM使用的事务数，VACUUM会延迟清除无效的行存表记录，延迟的事务个数通过vacuum_defer_cleanup_age进行设置。即VACUUM和VACUUM FULL操作不会立即清理刚刚被删除元组。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～1000000，值为0表示不延迟。

**默认值**: 0

## data_replicate_buffer_size

**参数说明**: 发送端与接收端传递数据页时，队列占用内存的大小。此参数会影响主备之间复制的缓冲大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，4096~1072693248，单位为KB。

**默认值**: 16MB（即16448KB）

## walsender_max_send_size

**参数说明**: 设置主机端日志或数据发送缓冲区的大小。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，8~INT_MAX，单位为KB。

**默认值**: 8M（即8192KB）

## enable_data_replicate

**参数说明**: 当数据库在数据导入行存表时，主机与备机的数据同步方式可以进行选择。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示导入数据行存表时主备数据采用数据页的方式进行同步。当replication_type参数为1时，不允许设置为on，如果此时用guc工具设置成on，会强制改为off。
- off表示导入数据行存表时主备数据采用日志（Xlog）方式进行同步。

**默认值**: off

## ha_module_debug

**参数说明**: 用于查看数据复制时具体数据块的复制状态日志。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示日志中将打印数据复制时每个数据块的状态。
- off表示日志中不打印数据复制时每个数据块的状态。

**默认值**: off

## enable_incremental_catchup

**参数说明**: 控制主备之间数据追赶（catchup）的方式。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示备机catchup时用增量catchup方式，即从从备本地数据文件扫描获得主备差异数据文件列表，进行主备之间的catchup。
- off表示备机catchup时用全量catchup方式，即从主机本地所有数据文件扫描获得主备差异数据文件列表，进行主备之间的catchup。

**默认值**: on

## wait_dummy_time

**参数说明**: 同时控制增量数据追赶（catchup）时，MogDB主备从按顺序启动时等待从备启动的最长时间以及等待从备发回扫描列表的最长时间。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，范围1~INT_MAX，单位为秒

**默认值**: 300

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> 单位只能设置为秒。

## catchup2normal_wait_time

**参数说明**: 单同步备机情况下，控制备机数据追赶（catchup）阻塞主机的最长时间。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，范围-1~10000，单位为毫秒。

- -1表示主机阻塞直到备机数据追赶完成。
- 0表示备机数据追赶时始终不阻塞主机。
- 其余值表示备机数据追赶时阻塞主机的最长时间。例如，取值5000，表示当备机数据追赶完成时间还剩5s时，阻塞主机等待其完成。

**默认值**: -1

## sync_config_strategy

**参数说明**: 主机和备机、备机和级联备之间配置文件的同步策略。

该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- all_node: 主机配置为all_node时，表示允许主机向所有备机主动同步配置文件；备机配置为all_node时，表示允许当前备机向其主机发送同步请求，允许当前备机向其所有级联备主动同步配置文件；级联备配置为all_node时，表示允许当前级联备向其备机发送同步请求。
- only_sync_node: 主机配置为only_sync_node时，表示仅允许主机向所有同步备机主动同步配置文件；备机配置为only_sync_node时，表示允许当前备机向其主机发送同步请求，不允许当前备机向其所有级联备主动同步配置文件；级联备配置为only_sync_node时，表示允许当前级联备向其备机发送同步请求。
- none_node: 主机配置为none_node时，表示不允许主机向任何备机主动同步配置文件；备机配置为none_node时，表示不允许当前备机向其主机发送同步请求，不允许当前备机向其所有级联备主动同步配置文件；级联备配置为none_node时，表示不允许当前级联备向其备机发送同步请求。

**默认值：**all_node

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
>
> - 在一个包含了主机、备机和级联备的MogDB集群中，主机相对于备机是发送端，备机相对于主机是接收端，备机相对于级联备是发送端，级联备相对于备机是接收端。
> - 发送端主动向接收端同步配置文件、接收端请求发送端同步配置文件是两个独立的事件，均会使得配置文件同步。若不希望配置文件同步，则需要将集群中所有节点的sync_config_strategy参数配置为none_node；若仅希望主机与同步备机同步配置文件，则需要将主机的sync_config_strategy参数配置为only_sync_node，其余节点配置为none_node；若希望所有节点同步配置文件，则需要将所有节点的sync_config_strategy参数配置为all_node。目前暂不支持自定义指定任意节点间的同步策略。
> - 配置参数同步的具体表现为，发送端发送配置文件，对接收端配置文件中的对应参数直接覆盖。若设置了配置文件需要同步的策略，则修改接收端配置参数后，发送端会立刻覆盖接收端的配置参数，使得接收端修改不生效。
> - 即使设置了配置文件需要同步的策略，仍有部分配置参数不会被同步。它们是： “application_name”, “archive_command”, “audit_directory”, “available_zone”, “comm_control_port”, “comm_sctp_port”, “listen_addresses”, “log_directory”, “port”, “replconninfo1”, “replconninfo2”, “replconninfo3”, “replconninfo4”, “replconninfo5”, “replconninfo6”, “replconninfo7”, “replconninfo8”, “ssl”, “ssl_ca_file”, “ssl_cert_file”, “ssl_ciphers”, “ssl_crl_file”, “ssl_key_file”, “ssl_renegotiation_limit”, “ssl_cert_notify_time”, “synchronous_standby_names”, “local_bind_address”, “perf_directory”, “query_log_directory”, “asp_log_directory”, “streaming_router_port”, “enable_upsert_to_merge”, “archive_dest”, “recovery_min_apply_delay”, “sync_config_strategy”。
