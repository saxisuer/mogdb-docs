---
title: 区域和格式化
summary: 区域和格式化
author: Zhang Cuiping
date: 2021-04-20
---

# 区域和格式化

介绍时间格式设置的相关参数。

## DateStyle <a id="DateStyle"> </a>

**参数说明**: 设置日期和时间值的显示格式，以及有歧义的输入值的解析规则。

这个变量包含两个独立的加载部分: 输出格式声明（ISO、Postgres、SQL、German）和输入输出的年/月/日顺序（DMY、MDY、YMD）。这两个可以独立设置或者一起设置。关键字Euro和European等价于DMY；关键字US、NonEuro、NonEuropean等价于MDY 。

该参数属于USERSET类型参数，请**参考表GUC参数设置方式**中对应设置方法进行设置。

**取值范围**: 字符串

**默认值**: ISO, MDY

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> gs_initdb会将这个参数初始化成与[lc_time](#lc_time)一致的值。

**设置建议:**优先推荐使用ISO格式。Postgres、SQL和German均采用字母缩写的形式来表示时区，例如"EST、WST、CST"等。这些缩写可同时指代不同的时区，比如CST可同时代表美国中部时间(Central Standard Time (USA) UT-6:00)、澳大利亚中部时间(Central Standard Time (Australia) UT+9:30)、中国标准时间(China Standard Time UT+8:00)、古巴标准时间(Cuba Standard Time UT-4:00)。这种情况下在时区转化时可能会得不到正确的结果，从而引发其他问题。

## IntervalStyle

**参数说明**: 设置区间值的显示格式。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- sql_standard表示产生与SQL标准规定匹配的输出。
- postgres表示产生与PostgreSQL 8.4版本相匹配的输出，当[DateStyle](#DateStyle)参数被设为ISO时。
- postgres_verbose表示产生与PostgreSQL 8.4版本相匹配的输出，当[DateStyle](#DateStyle)参数被设为non_ISO时。
- iso_8601表示产生与在ISO 8601中定义的"格式与代号"相匹配的输出。
- a表示与numtodsinterval函数相匹配的输出结果，详细请参考numtodsinterval。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> IntervalStyle参数也会影响不明确的间隔输入的说明。

**默认值**: postgres

## TimeZone

**参数说明**: 设置显示和解释时间类型数值时使用的时区。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，可查询视图PG_TIMEZONE_NAMES获得。

**默认值**: PRC

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> gs_initdb将设置一个与其系统环境一致的时区值。

## timezone_abbreviations

**参数说明**: 设置服务器接受的时区缩写值。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，可查询视图pg_timezone_names获得。

**默认值**: Default

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> Default表示通用时区的缩写, 适合绝大部分情况。但也可设置其他诸如 'Australia' 和 'India' 等用来定义特定的安装。而设置除此之外的时区缩写, 需要在建数据库之前通过相应的配置文件进行设置。

## extra_float_digits

**参数说明**: 这个参数为浮点数值调整显示的数据位数，浮点类型包括float4、float8 以及几何数据类型。参数值加在标准的数据位数上（FLT_DIG或DBL_DIG中合适的）。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，-15～3

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 设置为3，表示包括部分关键的数据位。这个功能对转储那些需要精确恢复的浮点数据特别有用。
> - 设置为负数，表示消除不需要的数据位。

**默认值**: 0

## client_encoding

**参数说明**: 设置客户端的字符编码类型。

请根据前端业务的情况确定。尽量客户端编码和服务器端编码一致，提高效率。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 兼容PostgreSQL所有的字符编码类型。其中UTF8表示使用数据库的字符编码类型。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用命令locale -a查看当前系统支持的区域和相应的编码格式，并可以选择进行设置。
> - 默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。
> - 参数建议保持默认值，不建议通过gs_guc工具或其他方式直接在postgresql.conf文件中设置client_encoding参数，即使设置也不会生效，以保证MogDB内部通信编码格式一致。

**默认值**: UTF8

**推荐值:**SQL_ASCII/UTF8

## lc_messages

**参数说明**: 设置信息显示的语言。

- 可接受的值是与系统相关的。

- 在一些系统上，这个区域范畴并不存在，不过仍然允许设置这个变量，只是不会有任何效果。同样，也有可能是所期望的语言的翻译信息不存在。在这种情况下，用户仍然能看到英文信息。

  该参数属于SUSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用命令locale -a查看当前系统支持的区域和相应的编码格式，并可以选择进行设置。
> - 默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。

**默认值**: C

## lc_monetary

**参数说明**: 设置货币值的显示格式，影响to_char之类的函数的输出。可接受的值是系统相关的。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用命令locale -a查看当前系统支持的区域和相应的编码格式，并可以选择进行设置。
> - 默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。

**默认值**: C

## lc_numeric

**参数说明**: 设置数值的显示格式，影响to_char之类的函数的输出。可接受的值是系统相关的。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用命令locale -a查看当前系统支持的区域和相应的编码格式，并可以选择进行设置。
> - 默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。

**默认值**: C

## lc_time

**参数说明**: 设置时间和区域的显示格式，影响to_char之类的函数的输出。可接受的值是系统相关的。

该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
>
> - 使用命令locale -a查看当前系统支持的区域和相应的编码格式，并可以选择进行设置。
> - 默认情况下，gs_initdb会根据当前的系统环境初始化此参数，通过locale命令可以查看当前的配置环境。

**默认值**: C

## default_text_search_config

**参数说明**: 设置全文检索的配置信息。

如果设置为不存在的文本搜索配置时将会报错。如果default_text_search_config对应的文本搜索配置被删除，需要重新设置default_text_search_config，否则会报设置错误。

- 其被文本搜索函数使用，这些函数并没有一个明确指定的配置。

- 当与环境相匹配的配置文件确定时，gs_initdb会选择一个与环境相对应的设置来初始化配置文件。

  该参数属于USERSET类型参数，请参考表[GUC参数设置方式](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:**
> MogDB支持pg_catalog.english，pg_catalog.simple两种配置。

**默认值**: pg_catalog.english
