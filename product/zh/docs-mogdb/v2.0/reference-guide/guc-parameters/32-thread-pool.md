---
title: 线程池
summary: 线程池
author: Zhang Cuiping
date: 2021-06-07
---

# 线程池

## enable_thread_pool

**参数说明**: 控制是否使用线程池功能。该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on表示开启线程池功能。
- off表示不开启线程池功能。

**默认值**: off

## thread_pool_attr

**参数说明**: 用于控制线程池功能的详细属性，该参数仅在enable_thread_pool打开后生效。该参数属于POSTMASTER类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串，长度大于0

该参数分为3个部分，'thread_num, group_num, cpubind_info'，这3个部分的具体含义如下：

- thread_num：线程池中的线程总数，取值范围是0~4096。其中0的含义是数据库根据系统CPU core的数量来自动配置线程池的线程数，如果参数值大于0，线程池中的线程数等于thread_num。
- group_num：线程池中的线程分组个数，取值范围是0~64。其中0的含义是数据库根据系统NUMA组的个数来自动配置线程池的线程分组个数，如果参数值大于0，线程池中的线程组个数等于group_num。
- cpubind_info：线程池是否绑核的配置参数。可选择的配置方式有集中：1. '(nobind)' ，线程不做绑核；2. '(allbind)'，利用当前系统所有能查询到的CPU core做线程绑核；3. '(nodebind: 1, 2)'，利用NUMA组1,2中的CPU core进行绑核；4. '(cpubind: 0-30)'，利用0-30号CPU core进行绑核。该参数不区分大小写。

**默认值**: '16, 2, (nobind)'
