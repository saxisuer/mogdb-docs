---
title: 基因查询优化器
summary: 基因查询优化器
author: Zhang Cuiping
date: 2021-04-20
---

# 基因查询优化器

介绍基因查询优化器相关的参数。基因查询优化器（GEQO）是一种启发式的查询规划算法。这个算法减少了对复杂查询规划的时间，而且生成规划的开销有时也小于正常的详尽的查询算法。

## geqo

**参数说明**: 控制基因查询优化的使用。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 通常情况下在执行过程中不要关闭，geqo_threshold变量提供了更精细的控制GEQO的方法。

**取值范围**: 布尔型

- on表示使用。
- off表示不使用。

**默认值**: on

## geqo_threshold

**参数说明**: 如果执行语句的数量超过设计的FROM的项数，则会使用基因查询优化来执行查询。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
>
> - 对于简单的查询，通常用详尽搜索方法，当涉及多个表的查询的时候，用GEQO可以更好的管理查询。
> - 一个FULL OUTER JOIN构造仅作为一个FROM项。

**取值范围**: 整型，2～INT_MAX。

**默认值**: 12

## geqo_effort

**参数说明**: 控制GEQO在规划时间和规划质量之间的平衡。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> geqo_effort实际上并没有直接做任何事情，只是用于计算其他影响GEQO的变量的默认值。如果愿意，可以手工设置其他参数。

**取值范围**: 整型，1～10。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 比默认值大的数值增加了查询规划的时间，但是也增加了选中有效查询的几率。

**默认值**: 5

## geqo_pool_size

**参数说明**: 控制GEQO使用池的大小，也就是基因全体中的个体数量。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 至少是2，且有用的值一般在100到1000之间。设置为0，表示使用系统自适应方式，MogDB会基于geqo_effort和表的个数选取合适的值。

**默认值**: 0

## geqo_generations

**参数说明**: 控制GEQO使用的算法的迭代次数。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～INT_MAX。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 必须至少是1，且有用的值介于100和1000之间。如果设置为0，则基于geqo_pool_size选取合适的值。

**默认值**: 0

## geqo_selection_bias

**参数说明**: 控制GEQO的选择性偏好，即就是一个种群中的选择性压力。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，1.5～2.0。

**默认值**: 2

## geqo_seed

**参数说明**: 控制GEQO使用的随机数生产器的初始化值，用来从顺序连接在一起的查询空间中查找随机路径。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

**取值范围**: 浮点型，0.0～1.0。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> 不同的值会改变搜索的连接路径，从而影响了所找路径的优劣。

**默认值**: 0
