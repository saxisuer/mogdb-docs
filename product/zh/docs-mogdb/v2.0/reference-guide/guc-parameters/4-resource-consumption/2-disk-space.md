---
title: 磁盘空间
summary: 磁盘空间
author: Zhang Cuiping
date: 2021-04-20
---

# 磁盘空间

介绍与磁盘空间相关的参数，用于限制临时文件所占用的磁盘空间。

## sql_use_spacelimit

**参数说明**: 限制单个SQL在单个数据库节点上，触发落盘操作时，落盘文件的空间大小，管控的空间包扩普通表、临时表以及中间结果集落盘占用的空间。

该参数属于USERSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置

**取值范围**: 整型，-1~2147483647，单位为KB。其中-1表示没有限制。

**默认值: -1**

## temp_file_limit

**参数说明**: 限制一个会话中，触发下盘操作时，单个下盘文件的空间大小。例如一次会话中，排序和哈希表使用的临时文件，或者游标占用的临时文件。

此设置为会话级别的下盘文件控制。

该参数属于SUSET类型参数，请参考表[GUC参数分类](../../../reference-guide/guc-parameters/30-appendix.md)中对应设置方法进行设置。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知:**
> SQL查询执行时使用的临时表空间不在此限制。

**取值范围**: 整型，-1~2147483647，单位为KB。其中-1表示没有限制。

**默认值**: -1
