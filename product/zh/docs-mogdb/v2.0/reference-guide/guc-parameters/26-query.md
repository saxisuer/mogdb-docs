---
title: Query
summary: Query
author: Zhang Cuiping
date: 2021-04-20
---

# Query

## instr_unique_sql_count

**参数说明**: 控制系统中unique sql信息实时收集功能。配置为0表示不启用unique sql信息收集功能。

该值由大变小将会清空系统中原有的数据重新统计；从小变大不受影响。

当系统中产生的unique sql信息大于instr_unique_sql_count时，系统产生的unique sql信息不被统计。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0~INT_MAX

**默认值**: 100

## instr_unique_sql_track_type

**参数说明**: unique sql记录SQL方式。

该参数属于INTERNAL类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 枚举类型

- top: 只记录顶层SQL。

**默认值**: top

## enable_instr_rt_percentile

**参数说明**: 是否开启计算系统中80%和95%的SQL响应时间的功能

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on: 表示打开sql响应时间信息计算功能。
- off: 表示关闭sql响应时间信息计算功能。

**默认值**: on

## percentile

**参数说明**: sql响应时间百分比信息，后台计算线程根据设置的值计算相应的百分比信息。

该参数属于INTERNAL类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 字符串。

**默认值**: 80,95

## instr_rt_percentile_interval

**参数说明**: sql响应时间信息计算间隔，sql响应时间信息计算功能打开后，后台计算线程每隔设置的时间进行一次计算。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 整型，0～3600（秒）。

**默认值**: 10s

## enable_instr_cpu_timer

**参数说明**: 是否捕获sql执行的cpu时间消耗 。

该参数属于SIGHUP类型参数，请参考表[GUC参数分类](30-appendix.md)中对应设置方法进行设置。

**取值范围**: 布尔型

- on: 表示捕获sql执行的cpu时间消耗。
- off: 表示不捕获sql执行的cpu时间消耗。

**默认值**: on
