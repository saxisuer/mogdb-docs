---
title: AI特性函数
summary: AI特性函数
author: Zhang Cuiping
date: 2021-04-20
---

# AI特性函数

- gs_index_advise(text)

  描述：针对单条查询语句推荐索引。

  参数：SQL语句字符串

  返回值类型：record

  示例请参见单query索引推荐。

- hypopg_create_index(text)

  描述：创建虚拟索引。

  参数：创建索引语句的字符串

  返回值类型：record

  示例请参见虚拟索引。

- hypopg_display_index()

  描述：显示所有创建的虚拟索引信息。

  参数：无

  返回值类型：record

  示例请参见虚拟索引。

- hypopg_drop_index(oid)

  描述：删除指定的虚拟索引。

  参数：索引的oid

  返回值类型：bool

  示例请参见虚拟索引。

- hypopg_reset_index()

  描述：清除所有虚拟索引。

  参数：无

  返回值类型：无

  示例请参见虚拟索引。

- hypopg_estimate_size(oid)

  描述：估计指定索引创建所需的空间大小。

  参数：索引的oid

  返回值类型：int8

  示例请参见虚拟索引。

- check_engine_status(ip text, port text)

  描述：测试给定的ip和port上是否有predictor engine提供服务。

  参数：predictor engine的ip地址和端口号。

  返回值类型：text

  示例请参见使用指导。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该函数当前版本不可用。

- encode_plan_node(optname text, orientation text, strategy text, options text, dop int8, quals text, projection text)

  描述：对入参的计划算子信息进行编码。

  参数：计划算子信息。

  返回值类型：text。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该函数为内部功能调用函数，不建议用户直接使用。

- model_train_opt(template text, model text)

  描述：训练给定的查询性能预测模型。

  参数：性能预测模型的模板名和模型名。

  返回值类型：tartup_time_accuracy FLOAT8, total_time_accuracy FLOAT8, rows_accuracy FLOAT8, peak_memory_accuracy FLOAT8

  示例请参见使用指导。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该函数当前版本不可用。

- track_model_train_opt(ip text, port text)

  描述：返回给定ip和port predictor engine的训练日志地址。

  参数：predictor engine的ip地址和端口号。

  返回值类型：text

  示例请参见使用指导。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  该函数当前版本不可用。

- encode_feature_perf_hist(datname text)

  描述：将目标数据库已收集的历史计划算子进行编码。

  参数：数据库名。

  返回值类型：queryid bigint, plan_node_id int, parent_node_id int, left_child_id int, right_child_id int, encode text, startup_time bigint, total_time bigint, rows bigint, peak_memory int

  示例请参见使用指导。

- gather_encoding_info(datname text)

  描述：调用encode_feature_perf_hist，将编码好的数据进行持久化保存。

  参数：数据库名。

  返回值类型：int

  示例请参见使用指导。
