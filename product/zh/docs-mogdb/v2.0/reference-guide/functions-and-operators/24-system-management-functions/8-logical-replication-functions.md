---
title: 逻辑复制函数
summary: 逻辑复制函数
author: Zhang Cuiping
date: 2021-04-20
---

# 逻辑复制函数

- pg_create_logical_replication_slot('slot_name', 'plugin_name')

  描述: 创建逻辑复制槽。

  参数说明:

  - slot_name

    流复制槽名称。

    取值范围: 字符串，不支持除字母，数字,以及（_?-.）以外的字符。

  - plugin_name

    插件名称。

    取值范围: 字符串，当前支持mppdb_decoding。

  返回值类型: name, text

  备注: 第一个返回值表示slot_name，第二个返回值表示该逻辑复制槽解码的起始LSN位置。

- pg_create_physical_replication_slot('slot_name', 'isDummyStandby')

  描述: 创建新的物理复制槽。

  参数说明:

  - slot_name

    流复制槽名称。

    取值范围: 字符串，不支持除字母，数字,以及（_?-.）以外的字符。

  - isDummyStandby

    是否是从从备连接主机创建的复制槽。

    类型: bool。

    返回值类型: name, text

- pg_drop_replication_slot('slot_name')

  描述: 删除流复制槽。

  参数说明:

  - slot_name

    流复制槽名称。

    取值范围: 字符串，不支持除字母，数字,以及（_?-.）以外的字符。

  返回值类型: void

- pg_logical_slot_peek_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述: 解码并不推进流复制槽（下次解码可以再次获取本次解出的数据）。

  参数说明:

  - slot_name

    流复制槽名称。

    取值范围: 字符串，不支持除字母，数字,以及（_?-.）以外的字符。

  - LSN

    日志的LSN，表示只解码小于等于此LSN的日志。

    取值范围: 字符串（LSN，格式为xlogid/xrecoff），如'1/2AAFC60'。为NULL时表示不对解码截止的日志位置做限制。

  - upto_nchanges

    解码条数（包含begin和commit）。假设一共有三条事务，分别包含3、5、7条记录，如果upto_nchanges为4，那么会解码出前两个事务共8条记录。解码完第二条事务时发现解码条数记录大于等于upto_nchanges，会停止解码。

    取值范围: 非负整数。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明:** LSN和upto_nchanges中任一参数达到限制，解码都会结束。

  - options: 此项为可选参数。

    - include-xids

        解码出的data列是否包含xid信息。

        取值范围: 0或1，默认值为1。

      - 0: 设为0时，解码出的data列不包含xid信息。
      - 1: 设为1时，解码出的data列包含xid信息。

    - skip-empty-xacts

        解码时是否忽略空事务信息。

        取值范围: 0或1，默认值为0。

      - 0: 设为0时，解码时不忽略空事务信息。
      - 1: 设为1时，解码时会忽略空事务信息。

    - include-timestamp

        解码信息是否包含commit时间戳。

        取值范围: 0或1，默认值为0。

      - 0: 设为0时，解码信息不包含commit时间戳。
      - 1: 设为1时，解码信息包含commit时间戳。

  返回值类型: text, uint, text 备注: 函数返回解码结果，每一条解码结果包含三列，对应上述返回值类型，分别表示LSN位置、xid和解码内容。

- pg_logical_slot_get_changes('slot_name', 'LSN', upto_nchanges, 'options_name', 'options_value')

  描述: 解码并推进流复制槽。

  参数说明: 与pg_logical_slot_peek_changes一致，详细内容请参见 pg_logical_slot_peek_ch…。

- pg_replication_slot_advance ('slot_name', 'LSN')

  描述: 直接推进流复制槽到指定LSN，不输出解码结果。

  参数说明:

  - slot_name

    流复制槽名称。

    取值范围: 字符串，不支持除字母，数字,以及（_?-.）以外的字符。

  - LSN

    推进到的日志LSN位置，下次解码时只会输出提交位置比该LSN大的事务结果。如果输入的LSN比当前流复制槽记录的推进位置还要小，则直接返回；如果输入的LSN比当前最新物理日志LSN还要大，则推进到当前最新物理日志LSN。

    取值范围: 字符串（LSN，格式为xlogid/xrecoff）。

  返回值类型: name, text

  备注: 返回值分别对应slot_name和实际推进至的LSN。

- pg_get_replication_slots

  描述: 获取复制槽列表。

  返回值类型: text，text，text，oid，boolean，xid，xid，text，boolean

  示例:

  ```sql
    mogdb=# select * from pg_get_replication_slots();
     slot_name |     plugin     | slot_type | datoid | active | xmin | catalog_xmin | restart_lsn | dummy_standby
    -----------+----------------+-----------+--------+--------+------+--------------+-------------+---------------
     wkl001    | mppdb_decoding | logical   |  15914 | f      |      |      2079556 | 4/1B81D920  | f
     dn_6002   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
     dn_6004   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
     dn_6003   |                | physical  |      0 | t      |      |              | 8/7CB63BD8  | f
     gfslot001 | mppdb_decoding | logical   |  15914 | f      |      |      2412553 | 4/A54B2428  | (5 rows)
  ```
