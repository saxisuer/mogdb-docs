---
title: 其它函数
summary: 其它函数
author: Zhang Cuiping
date: 2021-04-20
---

# 其它函数

- plan_seed()

  描述：获取前一次查询语句的seed值（内部使用）。

  返回值类型：int

- pg_stat_get_env()

  描述：获取当前节点的环境变量信息，仅sysadmin和monitor admin可以访问。

  返回值类型：record

  示例：

  ```sql
  mogdb=# select pg_stat_get_env();
                                                                                pg_stat_get_env
  ---------------------------------------------------------------------------------------------------------------------------------------
   (coordinator1,localhost,144773,49100,/data1/GaussDB_Kernel_TRUNK/install,/data1/GaussDB_Kernel_TRUNK/install/data/coordinator1,pg_log)
  (1 row)
  ```

- pg_catalog.plancache_clean()

  描述：清理节点上无人使用的全局计划缓存。

  返回值类型：bool

- pg_catalog.plancache_status()

  描述：显示节点上全局计划缓存的信息，函数返回信息和GLOBAL_PLANCACHE_STATUS一致。

  返回值类型：record

- textlen()

  描述：提供查询text的逻辑长度的方法。

  返回值类型：int

- threadpool_status()

  描述：显示线程池中工作线程及会话的状态信息。

  返回值类型：record

- get_local_active_session()

  描述：提供当前节点保存在内存中的历史活跃session状态的采样记录。

  返回值类型：record

- pg_stat_get_thread()

  描述：提供当前节点下所有线程的状态信息。

  返回值类型：record

- pg_stat_get_sql_count()

  描述：提供当前节点中所有用户执行的SELECT/UPDATE/INSERT/DELETE/MERGE INTO语句的计数结果。

  返回值类型：record

- pg_stat_get_data_senders()

  描述：提供当前活跃的数据复制发送线程的详细信息。

  返回值类型：record

- generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring)

  描述：基于两个snapshot生成系统诊断报告。需要在mogdb库下执行，默认初始化用户或monadmin用户可以访问，V500R001C20SPC002及其之前的版本初始化用户或sysadmin用户可以访问。

  返回值类型：record

  **表 1** generate_wdr_report 参数说明

  | 参数          | 说明                                                         | 取值范围                                        |
  | ------------- | ------------------------------------------------------------ | ----------------------------------------------- |
  | begin_snap_id | 生成某段时间内性能诊断报告的开始snapshotid。                 | -                                               |
  | end_snap_id   | 结束snapshot的id，默认end_snap_id大于begin_snap_id。         | -                                               |
  | report_type   | 指定生成report的类型。                                       | summarydetailall，即同时包含summary 和 detail。 |
  | report_scope  | 指定生成report的范围。                                       | cluster：数据库级别的信息node：节点级别的信息   |
  | node_name     | 在report_scope指定为node时，需要把该参数指定为对应节点的名称。(节点名称可以执行select * from pg_node_env;查询)。在report_scope为cluster时，该值可以省略或者指定为空或NULL。 | cluster：省略/空/NULLnode： MogDB中的节点名称   |

- create_wdr_snapshot()

  描述：手工生成系统诊断快照，该函数需要sysadmin权限。

  返回值类型：text

- reset_unique_sql

  描述：用来清理数据库节点内存中的Unique SQL（需要sysadmin权限）。

  返回值类型：bool

  **表 2** reset_unique_sql参数说明

  | 参数        | 类型 | 描述                                                         |
  | :---------- | :--- | :----------------------------------------------------------- |
  | scope       | text | 清理范围类型：'GLOBAL'：清理所有的节点，如果是'GLOBAL'，则只可以为主节点执行此函数。'LOCAL'：清理本节点。 |
  | clean_type  | text | 'BY_USERID'：按用户ID来进行清理Unique SQL。'BY_CNID'：按主节点的ID来进行清理Unique SQL。'ALL'：全部清理。 |
  | clean_value | int8 | 具体清理type对应的清理值。                                   |

- wdr_xdb_query(db_name_str text, query text)

  描述：提供本地跨数据库执行query的能力。例如: 在连接到mogdb库时, 访问test库下的表。

  ```sql
  select col1 from wdr_xdb_query('dbname=test','select col1 from t1') as dd(col1 int);
  ```

  返回值类型：record

- pg_wlm_jump_queue(pid int)

  描述：调整任务到数据库主节点队列的最前端。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- gs_wlm_switch_cgroup(pid int, cgroup text)

  描述：调整作业的优先级到新控制组。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- pv_session_memctx_detail(threadid tid, MemoryContextName text)

  描述：将线程tid的MemoryContextName内存上下文信息记录到“$GAUSSLOG/pg_log/${node_name}/dumpmem”目录下的“threadid_timestamp.log”文件中。其中threadid可通过视图GS_SESSION_MEMORY_DETAIL中的sessid后获得。在正式发布的版本中仅接受MemoryContextName为空串（两个单引号表示输入为空串，即”）的输入，此时会记录所有的内存上下文信息，否则不会有任何操作。对供内部开发人员和测试人员调试用的DEBUG版本，可以指定需要统计的MemoryContextName，此时会将该Context所有的内存使用情况记录到指定文件。该函数需要管理员权限的用户才能执行。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- pg_shared_memctx_detail(MemoryContextName text)

  描述：将MemoryContextName内存上下文信息记录到“$GAUSSLOG/pg_log/${node_name}/dumpmem”目录下的“threadid_timestamp.log”文件中。该函数功能仅在DEBUG版本中供内部开发人员和测试人员调试使用，在正式发布版本中调用该函数不会有任何操作。该函数需要管理员权限的用户才能执行。

  返回值类型：boolean

  - true：成功。
  - false：失败。

- local_bgwriter_stat()

  描述：显示本实例的bgwriter线程刷页信息，候选buffer链中页面个数，buffer淘汰信息。

  返回值类型：record

- local_ckpt_stat()

  描述：显示本实例的检查点信息和各类日志刷页情况。

  返回值类型：record

- local_double_write_stat()

  描述：显示本实例的双写文件的情况。

  返回值类型：record

- local_single_flush_dw_stat()

  描述：显示本实例的单页面淘汰双写文件的情况。

  返回值类型：record

- local_pagewriter_stat()

  描述：显示本实例的刷页信息和检查点信息。

  返回值类型：record

- local_redo_stat()

  描述：显示本实例的备机的当前回放状态。

  返回值类型：record

  备注：返回的回放状态主要包括当前回放位置，回放最小恢复点位置等信息。

- local_recovery_status()

  描述：显示本实例的主机和备机的日志流控信息。

  返回值类型：record

- gs_wlm_node_recover(boolean isForce)

  描述：获取当前内存中记录的TopSQL查询语句级别相关统计信息，当传入的参数不为0时，会将这部分信息从内存中清理掉。

  返回值类型：record

- gs_cgroup_map_ng_conf(group name)

  描述：读取指定逻辑数据库的cgroup配置文件。

  返回值类型：record

- gs_wlm_switch_cgroup(sess_id int8, cgroup name)

  描述：切换指定会话的控制组。

  返回值类型：record

- hdfs_fdw_handler()

  描述：用于外表重写功能，定义外表时需要定义的函数。

  返回值类型：record

- hdfs_fdw_validator(text[], oid)

  描述：用于外表重写功能，定义外表时需要定义的函数。

  返回值类型：record

- comm_client_info()

  描述：用于查询单个节点活跃的客户端连接信息。

  返回值类型：setof record

- pg_sync_cstore_delta(text)

  描述：同步指定列存表的delta表表结构，使其与列存表主表一致。

  返回值类型：bigint

- pg_sync_cstore_delta()

  描述：同步所有列存表的delta表表结构，使其与列存表主表一致。

  返回值类型：bigint

- pg_get_flush_lsn()

  描述：返回当前节点flush的xlog位置。

  返回值类型：text

- pg_get_sync_flush_lsn()

  描述：返回当前节点多数派flush的xlog位置。

  返回值类型：text

- gs_create_log_tables()

  描述：用于创建运行日志和性能日志的外表和视图。

  返回值类型：void

  示例：

  ```sql
  mogdb=# select gs_create_log_tables();
   gs_create_log_tables
  ----------------------

  (1 row)
  ```

- dbe_perf.get_global_full_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述：获取数据库级的全量SQL(Full SQL)信息。

  返回值类型：record

  **表 3** dbe_perf.get_global_full_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | :-------------- | :----------------------- | :---------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- dbe_perf.get_global_slow_sql_by_timestamp(start_timestamp timestamp with time zone, end_timestamp timestamp with time zone)

  描述：获取数据库级的慢SQL(Slow SQL)信息。

  返回值类型：record

  **表 4** dbe_perf.get_global_slow_sql_by_timestamp参数说明

  | 参数            | 类型                     | 描述                          |
  | :-------------- | :----------------------- | :---------------------------- |
  | start_timestamp | timestamp with time zone | SQL启动时间范围的开始时间点。 |
  | end_timestamp   | timestamp with time zone | SQL启动时间范围的结束时间点。 |

- statement_detail_decode(detail text, format text, pretty boolean)

  描述：解析全量/慢SQL语句中的details字段的信息。

  返回值类型：text

  **表 5** statement_detail_decode参数说明

  | 参数   | 类型    | 描述                                                         |
  | :----- | :------ | :----------------------------------------------------------- |
  | detail | text    | SQL语句产生的事件的集合(不可读)。                            |
  | format | text    | 解析输出格式，取值为 plaintext。                             |
  | pretty | boolean | 当format为plaintext时，是否以优雅的格式展示：true表示通过“\n”分隔事。false表示通过“，”分隔事件。 |

- get_global_user_transaction()

  描述：返回所有节点上各用户的事务相关信息。

  返回值类型：node_name name, usename name, commit_counter bigint, rollback_counter bigint, resp_min bigint, resp_max bigint, resp_avg bigint, resp_total bigint, bg_commit_counter bigint, bg_rollback_counter bigint, bg_resp_min bigint, bg_resp_max bigint, bg_resp_avg bigint, bg_resp_total bigint

- pg_collation_for

  描述：返回入参字符串对应的排序规则。

  参数：any（如果是常量必须进行显式类型转换）

  返回值类型：text

- pgxc_unlock_for_sp_database(name Name)

  描述：释放指定数据库锁。

  参数：数据库名

  返回值类型：布尔

- pgxc_lock_for_sp_database(name Name)

  描述：对指定的数据库加锁。

  参数：数据库名

  返回值类型：布尔

- copy_error_log_create()

  描述：创建COPY FROM容错机制所需要的错误表（public.pgxc_copy_error_log）。

  返回值类型：Boolean

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 此函数会尝试创建public.pgxc_copy_error_log表，表的详细信息请参见[表6](#cuowubiao)。
  > - 在relname列上创建B-tree索引，并REVOKE ALL on public.pgxc_copy_error_log FROM public对错误表进行权限控制（与COPY语句权限一致）。
  > - 由于尝试创建的public.pgxc_copy_error_log定义是一张行存表，因此数据库实例上必须支持行存表的创建才能够正常运行此函数，并使用后续的COPY容错功能。需要特别注意的是，enable_hadoop_env这个GUC参数开启后会禁止在数据库实例内创建行存表（MogDB默认为off）。
  > - 此函数自身权限为Sysadmin及以上（与错误表、COPY权限一致）。
  > - 若创建前public.pgxc_copy_error_log表已存在或者copy_error_log_relname_idx索引已存在，则此函数会报错回滚。

  **表 6** 错误表public.pgxc_copy_error_log信息 <a id="cuowubiao"></a>

  | 列名称    | 类型                     | 描述                                         |
  | :-------- | :----------------------- | :------------------------------------------- |
  | relname   | character varying        | 表名称。以模式名.表名形式显示。              |
  | begintime | timestamp with time zone | 出现数据格式错误的时间。                     |
  | filename  | character varying        | 出现数据格式错误的数据源文件名。             |
  | lineno    | bigint                   | 在数据源文件中，出现数据格式错误的行号。     |
  | rawrecord | text                     | 在数据源文件中，出现数据格式错误的原始记录。 |
  | detail    | text                     | 详细错误信息。                               |
