---
title: 客户端工具
summary: 客户端工具
author: Zhang Cuiping
date: 2021-06-07
---

# 客户端工具

数据库在部署成功后，需要通过一些工具来便捷地连接数据库，对数据库进行各种操作和调试。MogDB提供了一些数据库连接工具。通过这些工具可以方便地连接数据库并对数据库进行数据操作。

## gsql

gsql是MogDB提供在命令行下运行的数据库连接工具，可以通过此工具连接服务器并对其进行操作和维护，除了具备操作数据库的基本功能，gsql还提供了若干高级特性，便于用户使用。

### gsql概述

**基本功能**

- **连接数据库：**详细操作请参见《管理指南》中“日常运维 &gt; [gsql客户端连接](../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md)”章节。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  gsql创建连接时，数据库默认设置会有5分钟超时时间。如果在这个时间内，数据库未正确地接受连接并对身份进行认证，gsql将超时退出。 针对此问题，可以参考[常见问题处理](#常见问题处理)。

- **执行SQL语句：**支持交互式地键入并执行SQL语句，也可以执行一个文件中指定的SQL语句。

- **执行元命令：**元命令可以帮助管理员查看数据库对象的信息、查询缓存区信息、格式化SQL输出结果，以及连接到新的数据库等。元命令的详细说明请参见[元命令参考](#元命令参考)。

**高级特性**

gsql的高级特性如[表1](#gsql)所示。

**表 1** gsql高级特性 <a id="gsql"> </a>

| 特性名称           | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| 变量               | gsql提供类似于Linux的shell命令的变量特性，可以使用gsql的原命令\set设置一个变量，格式如下：<br />`\set varname value`<br />要删除一个变量请使用如下方式：<br />`\unset varname`<br />说明：<br />- 变量只是简单的名称/值对，这里的值可以是任意长度。<br />- 变量名称必须由字母（包括非拉丁字母）、数字和下划线组成，且对大小写敏感。<br />- 如果使用\set varname的格式（不带第二个参数），则只是设置这个变量而没有给变量赋值。<br />- 可以使用不带参数的\set来显示所有变量的值。<br />变量的示例和详细说明请参见**变量**。 |
| SQL代换            | 利用gsql的变量特性，可以将常用的SQL语句设置为变量，以简化操作。<br />SQL代换的示例和详细说明请参见**SQL代换**。 |
| 自定义提示符       | gsql使用的提示符支持用户自定义。可以通过修改gsql预留的三个变量PROMPT1、PROMPT2、PROMPT3来改变提示符。<br />这三个变量的值可以用户自定义，也可以使用gsql预定义的值。详细请参见**提示符**。 |
| 命令自动补齐       | 根据MogDB语法规则，gsql支持使用Tab键进行命令的自动补齐，当编译时指定了选项-with-readline，且客户端连接时指定“-r”参数，此功能被打开。例如，crea后键入Tab，gsql会将其补齐为create。<br />说明：<br />- 支持数据库SQL关键字如select、create、table等。<br />- 支持表名、视图名等自定义标识符的补齐。<br />- 元命令选项'S'、'+'不支持自动补齐。<br />- 对表进行补齐时，只有前缀是”pg_“才会补齐系统表。<br />- 不支持建表时字段类型的补齐。<br />- select后不支持任何补齐。<br />- 不支持常量与宏的自动补齐。<br />- select * from a,b… 不支持第二个开始表的自动补齐, insert into t1 (col1, col2, …) 不支持第二个列的自动补齐。<br />- 不支持create tablespace语句with以及with后参数的自动补齐。<br />- 创建索引不支持local、global的自动补齐，修改索引不支持rebuild自动补齐。<br />- set语句仅支持自动补全user和superuser级别的参数。<br />- 不支持if exists的自动补齐。<br />- 不支持表名.列名的自动补齐，如alter sequence <name> owned by tableName.colName，owned by。<br />- 不支持自定义操作符自动补齐。<br />- 使用复制粘贴这种方式输入命令，如果粘贴的命令里面有TAB键有可能会使输入命令的格式错乱，无法正常执行。 |
| 客户端操作历史记录 | gsql支持客户端操作历史记录，当客户端连接时指定“-r”参数，此功能被打开。可以通过\set设置记录历史的条数，例如，\set HISTSIZE 50，将记录历史的条数设置为50，\set HISTSIZE 0，不记录历史。<br />说明：<br />- 客户端操作历史记录条数默认设置为32条，最多支持记录500条。当客户端交互式输入包含中文字符时，只支持UTF-8 的编码环境。<br />- 出于安全考虑，将包含PASSWORD、IDENTIFIED敏感词的记录识别为敏感信息，不会记录到历史信息中，即不能通过上下翻回显。 |

- 变量

  可以使用gsql元命令\set设置一个变量。例如把变量foo的值设置为bar：

  ```
  mogdb=# \set foo bar
  ```

  要引用变量的值，在变量前面加冒号。例如查看变量的值：

  ```
  mogdb=# \echo :foo
  bar
  ```

  这种变量的引用方法适用于正规的SQL语句和元命令。

  gsql预定义了一些特殊变量，同时也规划了变量的取值。为了保证和后续版本最大限度地兼容，请避免以其他目的使用这些变量。所有特殊变量见[表2](#teshubian)。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 所有特殊变量都由大写字母、数字和下划线组成。
  > - 要查看特殊变量的默认值，请使用元命令**\echo :**_varname_（例如\echo :DBNAME）。

  **表 2** 特殊变量设置 <a id="teshubian"> </a>

  | 变量              | 设置方法                                          | 变量说明                                                     |
  | :---------------- | :------------------------------------------------ | :----------------------------------------------------------- |
  | DBNAME            | `\set DBNAME *dbname*`                            | 当前连接的数据库的名称。每次连接数据库时都会被重新设置。     |
  | ECHO              | `\set ECHO all | queries`                         | - 如果设置为all，只显示查询信息。设置为all等效于使用gsql连接数据库时指定-a参数。 <br />- 如果设置为queries，显示命令行和查询信息。等效于使用gsql连接数据库时指定-e参数。 |
  | ECHO_HIDDEN       | `\set ECHO_HIDDEN  on | off | noexec`             | 当使用元命令查询数据库信息（例如\dg）时，此变量的取值决定了查询的行为：<br />- 设置为on，先显示元命令实际调用的查询语句，然后显示查询结果。等效于使用gsql连接数据库时指定-E参数。<br />- 设置为off，则只显示查询结果。<br />- 设置为noexec，则只显示查询信息，不执行查询操作。 |
  | ENCODING          | `\set ENCODING   encoding`                        | 当前客户端的字符集编码。                                     |
  | FETCH_COUNT       | `\set FETCH_COUNT variable`                       | - 如果该变量的值为大于0的整数，假设为n，则执行SELECT语句时每次从结果集中取n行到缓存并显示到屏幕。<br />- 如果不设置此变量，或设置的值小于等于0，则执行SELECT语句时一次性把结果都取到缓存。<br />说明：<br />设置合理的变量值，将减少内存使用量。一般来说，设为100到1000之间的值比较合理。 |
  | HOST              | `\set HOST hostname`                              | 已连接的数据库主机名称。                                     |
  | IGNOREEOF         | `\set IGNOREEOF variable`                         | - 若设置此变量为数值，假设为10，则在gsql中输入的前9次EOF字符（通常是Ctrl+D组合键）都会被忽略，在第10次按Ctrl+D才能退出gsql程序。<br />- 若设置此变量为非数值，则缺省为10。<br />- 若删除此变量，则向交互的gsql会话发送一个EOF终止应用。 |
  | LASTOID           | `\set LASTOID oid`                                | 最后影响的oid值，即为从一条INSERT或lo_import命令返回的值。此变量只保证在下一条SQL语句的结果显示之前有效。 |
  | ON_ERROR_ROLLBACK | `\set  ON_ERROR_ROLLBACK  on | interactive | off` | - 如果是on，当一个事务块里的语句产生错误的时候，这个错误将被忽略而事务继续。<br />- 如果是interactive，这样的错误只是在交互的会话里忽略。<br />- 如果是off（缺省），事务块里一个语句生成的错误将会回滚整个事务。on_error_rollback-on模式是通过在一个事务块的每个命令前隐含地发出一个SAVEPOINT的方式工作的，在发生错误的时候回滚到该事务块。 |
  | ON_ERROR_STOP     | `\set ON_ERROR_STOP on | off`                     | - on：命令执行错误时会立即停止，在交互模式下，gsql会立即返回已执行命令的结果。<br />- off（缺省）：命令执行错误时将会跳过错误继续执行。 |
  | PORT              | `\set PORT port`                                  | 正连接数据库的端口号。                                       |
  | USER              | `\set USER username`                              | 当前用于连接的数据库用户。                                   |
  | VERBOSITY         | `\set VERBOSITY   terse | default | verbose`      | 这个选项可以设置为值terse、default、verbose之一以控制错误报告的冗余行。<br />- terse：仅返回严重且主要的错误文本以及文本位置（一般适合于单行错误信息）。<br />- default：返回严重且主要的错误文本及其位置，还包括详细的错误细节、错误提示（可能会跨越多行）。<br />- verbose：返回所有的错误信息。 |

- SQL代换

  像元命令的参数一样，gsql变量的一个关键特性是可以把gsql变量替换成正规的SQL语句。此外，gsql还提供为变量更换新的别名或其他标识符等功能。使用SQL代换方式替换一个变量的值可在变量前加冒号。例如：

  ```
  mogdb=# \set foo 'HR.areaS'
  mogdb=# select * from :foo;
   area_id |       area_name
  ---------+------------------------
         4 | Middle East and Africa
         3 | Asia
         1 | Europe
         2 | Americas
  (4 rows)
  ```

  执行以上命令，将会查询HR.areaS表。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 变量的值是逐字复制的，甚至可以包含不对称的引号或反斜杠命令。所以必须保证输入的内容有意义。

- 提示符

  通过[表3](#tishifu)的三个变量可以设置gsql的提示符，这些变量是由字符和特殊的转义字符所组成。

  **表 3** 提示符变量 <a id="tishifu"> </a>

  | 变量    | 描述                                                         | 示例                                                         |
  | :------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
  | PROMPT1 | gsql请求一个新命令时使用的正常提示符。<br />PROMPT1的默认值为：<br />`%/%R%#` | 使用变量PROMPT1切换提示符：<br />- 提示符变为[local]：<br />`mogdb=> \set PROMPT1 %M [local:/tmp/gaussdba_mppdb]`<br />- 提示符变为name：<br />`mogdb=> \set PROMPT1 name name`<br />- 提示符变为=：<br />`mogdb=> \set PROMPT1 %R =` |
  | PROMPT2 | 在一个命令输入期待更多输入时（例如，查询没有用一个分号结束或者引号不完整）显示的提示符。 | 使用变量PROMPT2显示提示符：<br />`mogdb=# \set PROMPT2 TEST mogdb=# select * from HR.areaS TEST; area_id |       area_name ---+-------       1 | Europe       2 | Americas       4 | Middle East and Africa       3 | Asia (4 rows))` |
  | PROMPT3 | 当执行COPY命令，并期望在终端输入数据时（例如，COPY FROM STDIN），显示提示符。 | 使用变量PROMPT3显示COPY提示符：<br />`mogdb=#  \set PROMPT3 '>>>>' mogdb=#  copy HR.areaS from STDIN; Enter data to be copied followed by a newline. End with a backslash and a period on a line by itself. >>>>1 aa >>>>2 bb >>>>.` |

  提示符变量的值是按实际字符显示的，但是，当设置提示符的命令中出现“%”时，变量的值根据“%”后的字符，替换为已定义的内容，已定义的提示符请参见[表4](#yidingyi)。

  **表 4** 已定义的替换 <a id="yidingyi"> </a>

  | 符号     | 符号说明                                                     |
  | :------- | :----------------------------------------------------------- |
  | %M       | 主机的全名（包含域名），若连接是通过Unix域套接字进行的，则全名为[local]，若Unix域套接字不是编译的缺省位置，就是[local:/dir/name]。 |
  | %m       | 主机名删去第一个点后面的部分。若通过Unix域套接字连接，则为[local]。 |
  | %&gt;    | 主机正在侦听的端口号。                                       |
  | %n       | 数据库会话的用户名。                                         |
  | %/       | 当前数据库名称。                                             |
  | %~       | 类似 %/，如果数据库是缺省数据库时输出的是波浪线\~。           |
  | %#       | 如果会话用户是数据库系统管理员，使用#，否则用&gt;。          |
  | %R       | - 对于PROMPT1通常是“=”，如果是单行模式则是“^”，如果会话与数据库断开（如果\connect失败可能发生）则是“!”。<br />- 对于PROMPT2该序列被“ -”、单引号、双引号或“\$”(取决于gsql是否等待更多的输入：查询没有终止、正在一个 / … / 注释里、正在引号或者美元符扩展里)代替。 |
  | %x       | 事务状态：<br />- 如果不在事务块里，则是一个空字符串。<br />- 如果在事务块里，则是“*”。<br />- 如果在一个失败的事务块里则是“!”。<br />- 如果无法判断事务状态时为“?”（比如没有连接）。 |
  | %digits  | 指定字节值的字符将被替换到该位置。                           |
  | %:name   | gsql变量“name”的值。                                         |
  | %command | command的输出，类似于使用“^”替换。                           |
  | %[ … %]  | 提示可以包含终端控制字符，这些字符可以改变颜色、背景、提示文本的风格、终端窗口的标题。例如，<br />`mogdb=>\set PROMPT1 '%[%033[1;33;40m%]%n@%/%R%[%033[0m%]%#'`<br />这个句式的结果是在VT100兼容的可显示彩色的终端上的一个宽体（1;）黑底黄字（33;40）。 |

  **环境变量**

  **表 5** 与gsql相关的环境变量

  | 名称                       | 描述                                                         |
  | :------------------------- | :----------------------------------------------------------- |
  | COLUMNS                    | 如果\set columns为0，则由此参数控制wrapped格式的宽度。这个宽度用于决定在自动扩展的模式下，是否要把宽输出模式变成竖线的格式。 |
  | PAGER                      | 如果查询结果无法在一页显示，它们就会被重定向到这个命令。可以用\pset命令关闭分页器。典型的是用命令more或less来实现逐页查看。缺省值是平台相关的。<br />说明：<br />less的文本显示，受系统环境变量LC_CTYPE影响。 |
  | PSQL_EDITOR                | \e和\ef命令使用环境变量指定的编辑器。变量是按照列出的先后顺序检查的。在Unix系统上默认的编辑工具是vi。 |
  | EDITOR                     |                                                              |
  | VISUAL                     |                                                              |
  | PSQL_EDITOR_LINENUMBER_ARG | 当\e和\ef带上一行数字参数使用时，这个变量指定的命令行参数用于向编辑器传递起始行数。像Emacs或vi这样的编辑器，这只是个加号。如果选项和行号之间需要空白，在变量的值后加一个空格。例如：<br />`PSQL_EDITOR_LINENUMBER_ARG = '+' PSQL_EDITOR_LINENUMBER_ARG='-line '`<br />Unix系统默认的是+。 |
  | PSQLRC                     | 用户的.gsqlrc文件的交互位置。                                |
  | SHELL                      | 使用!命令跟shell执行的命令是一样的效果。                     |
  | TMPDIR                     | 存储临时文件的目录。缺省是/tmp。                             |

### 使用指导

**前提条件**

连接数据库时使用的用户需要具备访问数据库的权限。

**背景信息**

使用gsql命令可以连接远程数据库服务。连接远程数据库服务时，需要在服务器上设置允许远程连接，详细操作请参见《管理指南》中“日常运维 &gt; gsql客户端连接 &gt; [使用gsql远程连接](../../administrator-guide/routine-maintenance/using-the-gsql-client-for-connection.md#远程连接数据库)”章节。

**操作步骤**

1. 使用gsql连接到MogDB服务器。

   gsql工具使用-d参数指定目标数据库名、-U参数指定数据库用户名、-h参数指定主机名、-p参数指定端口号信息。

   > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  若未指定数据库名称，则使用初始化时默认生成的数据库名称；若未指定数据库用户名，则默认使用当前操作系统用户作为数据库用户名；当某个值没有前面的参数（-d、-U等）时，若连接的命令中没有指定数据库名（-d）则该参数会被解释成数据库名；如果已经指定数据库名（-d）而没有指定数据库用户名（-U）时，该参数则会被解释成数据库用户名。

   示例1，使用omm用户连接到本机mogdb数据库的15400端口。

   ```
   gsql -d mogdb -p 15400
   ```

   示例2，使用jack用户连接到远程主机mogdb数据库的15400端口。

   ```
   gsql -h 10.180.123.163 -d mogdb -U jack -p 15400
   ```

   示例3，参数mogdb和omm不属于任何选项时，分别被解释成了数据库名和用户名。

   ```
   gsql mogdb omm -p 15400
   ```

   **等效于**

   ```
   gsql -d mogdb -U omm -p 15400
   ```

   详细的gsql参数请参见[命令参考](#命令参考)。

2. 执行SQL语句。

   以创建数据库human_staff为例。

   ```
   CREATE DATABASE human_staff;
   ```

   通常，输入的命令行在遇到分号的时候结束。如果输入的命令行没有错误，结果就会输出到屏幕上。

3. 执行gsql元命令。

   以列出MogDB中所有的数据库和描述信息为例。

   ```sql
   mogdb=#  \l
                                   List of databases
         Name      |  Owner   | Encoding  | Collate | Ctype |   Access privileges
   ----------------+----------+-----------+---------+-------+-----------------------
    human_resource | omm | SQL_ASCII | C       | C     |
    postgres       | omm | SQL_ASCII | C       | C     |
    template0      | omm | SQL_ASCII | C       | C     | =c/omm         +
                   |          |           |         |       | omm=CTc/omm
    template1      | omm | SQL_ASCII | C       | C     | =c/omm          +
                   |          |           |         |       | omm=CTc/omm
    human_staff    | omm | SQL_ASCII | C       | C     |
   (5 rows)
   ```

   更多gsql元命令请参见[元命令参考](#元命令参考)。

**示例**

首先要创建一个表空间EXAMPLE：

```sql
mogdb=# CREATE TABLESPACE EXAMPLE RELATIVE LOCATION 'tablespace1/tablespace_1';
CREATE TABLESPACE
```

表空间创建成功后，创建schema HR：

```sql
mogdb=# CREATE schema HR;
CREATE SCHEMA
```

以把一个查询分成多行输入为例。注意提示符的变化：

```sql
mogdb=# CREATE TABLE HR.areaS(
mogdb(# area_ID   NUMBER,
mogdb(# area_NAME VARCHAR2(25)
mogdb(# )tablespace EXAMPLE;
CREATE TABLE
```

查看表的定义：

```sql
mogdb=# \d HR.areaS
               Table "hr.areas"
  Column   |         Type          | Modifiers
-----------+-----------------------+-----------
 area_id   | numeric               |
 area_name | character varying(25) |
Tablespace: "example"
```

向HR.areaS表插入四行数据：

```sql
mogdb=# INSERT INTO HR.areaS (area_ID, area_NAME) VALUES (1, 'Europe');
INSERT 0 1
mogdb=# INSERT INTO HR.areaS (area_ID, area_NAME) VALUES (2, 'Americas');
INSERT 0 1
mogdb=# INSERT INTO HR.areaS (area_ID, area_NAME) VALUES (3, 'Asia');
INSERT 0 1
mogdb=# INSERT INTO HR.areaS (area_ID, area_NAME) VALUES (4, 'Middle East and Africa');
INSERT 0 1
```

切换提示符：

```sql
mogdb=# \set PROMPT1 '%n@%m %~%R%#'
omm@[local] mogdb=#
```

查看表：

```sql
omm@[local] mogdb=#SELECT * FROM HR.areaS;
 area_id |       area_name
---------+------------------------
       1 | Europe
       4 | Middle East and Africa
       2 | Americas
       3 | Asia
(4 rows)
```

可以用\pset命令以不同的方法显示表：

```sql
omm@[local] mogdb=#\pset border 2
Border style is 2.
omm@[local] mogdb=#SELECT * FROM HR.areaS;
+---------+------------------------+
| area_id |       area_name        |
+---------+------------------------+
|       1 | Europe                 |
|       2 | Americas               |
|       3 | Asia                   |
|       4 | Middle East and Africa |
+---------+------------------------+
(4 rows)
omm@[local] mogdb=#\pset border 0
Border style is 0.
omm@[local] mogdb=#SELECT * FROM HR.areaS;
area_id       area_name
------- ----------------------
      1 Europe
      2 Americas
      3 Asia
      4 Middle East and Africa
(4 rows)
```

使用元命令：

```sql
omm@[local] mogdb=#\a \t \x
Output format is unaligned.
Showing only tuples.
Expanded display is on.
omm@[local] mogdb=#SELECT * FROM HR.areaS;
area_id|2
area_name|Americas

area_id|1
area_name|Europe

area_id|4
area_name|Middle East and Africa

area_id|3
area_name|Asia
omm@[local] mogdb=#
```

### 获取帮助

**操作步骤**

- 连接数据库时，可以使用如下命令获取帮助信息。

  ```sql
  gsql --help
  ```

  显示如下帮助信息：

  ```sql
  ......
  Usage:
    gsql [OPTION]... [DBNAME [USERNAME]]

  General options:
    -c, --command=COMMAND    run only single command (SQL or internal) and exit
    -d, --dbname=DBNAME      database name to connect to (default: "mogdb")
    -f, --file=FILENAME      execute commands from file, then exit
  ......
  ```

- 连接到数据库后，可以使用如下命令获取帮助信息。

  ```
  help
  ```

  显示如下帮助信息：

  ```
  You are using gsql, the command-line interface to gaussdb.
  Type:  \copyright for distribution terms
         \h for help with SQL commands
         \? for help with gsql commands
         \g or terminate with semicolon to execute query
         \q to quit
  ```

**任务示例**

1. 使用如下命令连接数据库。

   ```sql
   gsql -d mogdb -p 15400
   ```

   mogdb为需要连接的数据库名称，15400为数据库主节点的端口号。

   连接成功后，系统显示类似如下信息：

   ```sql
   gsql ((MogDB 2.0.1 build f892ccb7) compiled at 2021-07-09 16:12:59 commit 0 last mr )
   Non-SSL connection (SSL connection is recommended when requiring high-security)
   Type "help" for help.

   mogdb=#
   ```

2. 查看gsql的帮助信息。具体执行命令请参见[表6 使用gsql联机帮助](#shiyong)。

   **表 6** 使用gsql联机帮助 <a id="shiyong"> </a>

   | 描述                         | 示例                                                         |
   | :--------------------------- | :----------------------------------------------------------- |
   | 查看版权信息                 | \copyright                                                   |
   | 查看MogDB支持的SQL语句的帮助 | 查看MogDB支持的SQL语句的帮助<br />例如，查看MogDB支持的所有SQL语句：<br />`mogdb=# \h Available help:  ABORT  ALTER APP WORKLOAD GROUP … …`<br />例如，查看CREATE DATABASE命令的参数可使用下面的命令：<br />`mogdb=# \help CREATE DATABASE Command:     CREATE DATABASE Description: create a new database Syntax: CREATE DATABASE database_name     [ [ WITH ] {[ OWNER [=] user_name ]|           [ TEMPLATE [=] template ]|           [ ENCODING [=] encoding ]|           [ LC_COLLATE [=] lc_collate ]|           [ LC_CTYPE [=] lc_ctype ]|           [ DBCOMPATIBILITY [=] compatibility_type ]|           [ TABLESPACE [=] tablespace_name ]|           [ CONNECTION LIMIT [=] connlimit ]}[…] ];` |
   | 查看gsql命令的帮助           | 例如，查看gsql支持的命令：<br />`mogdb=# \? General  \copyright             show FusionInsight LibrA usage and distribution terms  \g [FILE] or ;         execute query (and send results to file or |pipe)  \h(\help) [NAME]              help on syntax of SQL commands, * for all commands  \parallel [on [num]|off] toggle status of execute (currently off)  \q                     quit gsql … …` |

### 命令参考

详细的gsql参数请参见[表7 常用参数](#changyong)、[表8 输入和输出参数](#shuruheshuchu)、[表9 输出格式参数](#shuchu)和[表10 连接参数](#lianjie)。

**表 7** 常用参数 <a id="changyong"> </a>

| 参数                            | 参数说明                                                     | 取值范围                                         |
| :------------------------------ | :----------------------------------------------------------- | :----------------------------------------------- |
| -c, -command=COMMAND            | 声明gsql要执行一条字符串命令然后退出。                       | -                                                |
| -d, -dbname=DBNAME              | 指定想要连接的数据库名称。                                   | 字符串。                                         |
| -f, -file=FILENAME              | 使用文件作为命令源而不是交互式输入。gsql将在处理完文件后结束。如果FILENAME是-（连字符），则从标准输入读取。<br />说明：<br />环境场景：一主一备（8U32G）<br />实测：读取200M数据文件时，耗时约为8分钟21秒；<br />读取500M数据文件时，耗时约为18分41秒。<br />建议：文件读取的时间随文件数据量逐渐增加，如果文件太大，中间异常时需要重新读取，同时会导致系统OS的IO过载，建议文件大小控制在500M左右。 | 绝对路径或相对路径，且满足操作系统路径命名规则。 |
| -l, -list                       | 列出所有可用的数据库，然后退出。                             | -                                                |
| -v, -set, -variable=NAME=VALUE  | 设置gsql变量NAME为VALUE。<br />变量的示例和详细说明请参见**变量**。 | -                                                |
| -X, -no-gsqlrc                  | 不读取启动文件（系统范围的gsqlrc或者用户的~/.gsqlrc都不读取）。<br />说明：<br />启动文件默认为~/.gsqlrc，或通过PSQLRC环境变量指定。 | -                                                |
| -1 (“one”), -single-transaction | 当gsql使用-f选项执行脚本时，会在脚本的开头和结尾分别加上START TRANSACTION/COMMIT用以把整个脚本当作一个事务执行。这将保证该脚本完全执行成功，或者脚本无效。<br />说明：<br />如果脚本中已经使用了START TRANSACTION，COMMIT，ROLLBACK，则该选项无效。 | -                                                |
| -?, -help                       | 显示关于gsql命令行参数的帮助信息然后退出。                   | -                                                |
| -V, -version                    | 打印gsql版本信息然后退出。                                   | -                                                |
| -C, -enable-client-encryption   | 当使用-C参数连接本地数据库或者远程连接数据库时，可通过该选项打开全密态数据库开关。 | -                                                |

**表 8** 输入和输出参数 <a id="shuruheshuchu"> </a>

| 参数                   | 参数说明                                                     | 取值范围                                         |
| :--------------------- | :----------------------------------------------------------- | :----------------------------------------------- |
| -a, -echo-all          | 在读取行时向标准输出打印所有内容。<br />注意：<br />使用此参数可能会暴露部分SQL语句中的敏感信息，如创建用户语句中的password信息等，请谨慎使用。 | -                                                |
| -e, -echo-queries      | 把所有发送给服务器的查询同时回显到标准输出。<br />注意：<br />使用此参数可能会暴露部分SQL语句中的敏感信息，如创建用户语句中的password信息等，请谨慎使用。 | -                                                |
| -E, -echo-hidden       | 回显由\d和其他反斜杠命令生成的实际查询。                     | -                                                |
| -k, -with-key=KEY      | 使用gsql对导入的加密文件进行解密。<br />须知：<br />- 对于本身就是shell命令中的关键字符如单引号（'）或双引号（”），Linux shell会检测输入的单引号（'）或双引号（”）是否匹配。如果不匹配，shell认为用户没有输入完毕，会一直等待用户输入，从而不会进入到gsql程序。<br />- 不支持解密导入存储过程和函数。不支持解密导入存储过程和函数。 | -                                                |
| -L, -log-file=FILENAME | 除了正常的输出源之外，把所有查询输出记录到文件FILENAME中。<br />注意：<br />- 使用此参数可能会暴露部分SQL语句中的敏感信息，如创建用户语句中的password信息等，请谨慎使用。<br />- 此参数只保留查询结果到相应文件中，主要目标是为了查询结果能够更好更准确地被其他调用者（例如自动化运维脚本）解析；而不是保留gsql运行过程中的相关日志信息。 | 绝对路径或相对路径，且满足操作系统路径命名规则。 |
| -m, -maintenance       | 允许在两阶段事务恢复期间连接MogDB。<br />说明：<br />该选项是一个开发选项，禁止用户使用，只限专业技术人员使用，功能是：使用该选项时，gsql可以连接到备机，用于校验主备机数据的一致性。 | -                                                |
| -n, -no-libedit        | 关闭命令行编辑。                                             | -                                                |
| -o, -output=FILENAME   | 将所有查询输出重定向到文件FILENAME。                         | 绝对路径或相对路径，且满足操作系统路径命名规则。 |
| -q, -quiet             | 安静模式，执行时不会打印出额外信息。                         | 缺省时gsql将打印许多其他输出信息。               |
| -s, -single-step       | 单步模式运行。意味着每个查询在发往服务器之前都要提示用户，用这个选项也可以取消执行。此选项主要用于调试脚本。<br />注意：<br />使用此参数可能会暴露部分SQL语句中的敏感信息，如创建用户语句中的password信息等，请谨慎使用。 | -                                                |
| -S, -single-line       | 单行运行模式，这时每个命令都将由换行符结束，像分号那样。     | -                                                |

**表 9** 输出格式参数 <a id="shuchu"> </a>

| 参数                         | 参数说明                                                     | 取值范围             |
| :--------------------------- | :----------------------------------------------------------- | :------------------- |
| -A, -no-align                | 切换为非对齐输出模式。                                       | 缺省为对齐输出模式。 |
| -F, -field-separator=STRING  | 设置域分隔符（默认为“\|”）。                                 | -                    |
| -H, -html                    | 打开HTML格式输出。                                           | -                    |
| -P, -pset=VAR[=ARG]          | 在命令行上以\pset的风格设置打印选项。<br />说明：<br />这里必须用等号而不是空格分隔名称和值。例如，把输出格式设置为LaTeX，可以键入-P format=latex | -                    |
| -R, -record-separator=STRING | 设置记录分隔符。                                             | -                    |
| -r                           | 开启在客户端操作中可以进行编辑的模式。                       | 缺省为关闭。         |
| -t, -tuples-only             | 只打印行。                                                   | -                    |
| -T, -table-attr=TEXT         | 允许声明放在HTML table标签里的选项。<br />使用时请搭配参数“-H,-html”，指定为HTML格式输出。 | -                    |
| -x, -expanded                | 打开扩展表格式模式。                                         | -                    |
| -z, -field-separator-zero    | 设置非对齐输出模式的域分隔符为空。<br />使用时请搭配参数“-A, -no-align”，指定为非对齐输出模式。 | -                    |
| -0, -record-separator-zero   | 设置非对齐输出模式的记录分隔符为空。<br />使用时请搭配参数“-A, -no-align”，指定为非对齐输出模式。 | -                    |

**表 10** 连接参数 <a id="lianjie"> </a>

| 参数                   | 参数说明                                                     | 取值范围                                                     |
| :--------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| -h, -host=HOSTNAME     | 指定正在运行服务器的主机名或者Unix域套接字的路径。           | 如果省略主机名，gsql将通过Unix域套接字与本地主机的服务器相联，或者在没有Unix域套接字的机器上，通过TCP/IP与localhost连接。 |
| -p, -port=PORT         | 指定数据库服务器的端口号。<br />可以通过port参数修改默认端口号。 | 默认为5432。                                                 |
| -U, -username=USERNAME | 指定连接数据库的用户。<br />说明：<br />- 通过该参数指定用户连接数据库时，需要同时提供用户密码用以身份验证。您可以通过交换方式输入密码，或者通过-W参数指定密码。<br />- 用户名中包含有字符\$，需要在字符$前增加转义字符才可成功连接数据库。 | 字符串。默认使用与当前操作系统用户同名的用户。               |
| -W, -password=PASSWORD | 当使用-U参数连接远端数据库时，可通过该选项指定密码。<br />说明：<br />数据库主节点所在服务器后连接本地数据库主节点实例时，默认使用trust连接，会忽略此参数。<br />用户密码中包含特殊字符“\”和”`“时，需要增加转义字符才可成功连接数据库。<br />如果用户未输入该参数，但是数据库连接需要用户密码，这时将出现交互式输入，请用户输入当前连接的密码。该密码最长长度为999字节，受限于GUC参数password_max_length的最大值。 | 符合密码复杂度要求。                                         |

### 元命令参考

介绍使用MogDB数据库命令行交互工具登录数据库后，gsql所提供的元命令。所谓元命令就是在gsql里输入的任何以不带引号的反斜杠开头的命令。

**注意事项**

- 一个gsql元命令的格式是反斜杠后面紧跟一个动词，然后是任意参数。参数命令动词和其他参数以任意个空白字符间隔。
- 要在参数里面包含空白，必须用单引号把它引起来。要在这样的参数里包含单引号，可以在前面加一个反斜杠。任何包含在单引号里的内容都会被进一步进行类似C语言的替换：\n（新行）、\t（制表符）、\b（退格）、\r（回车）、\f（换页）、\digits（八进制表示的字符）、\xdigits（十六进制表示的字符）。
- 用”“包围的内容被当做一个命令行传入shell。该命令的输出（删除了结尾的新行）被当做参数值。
- 如果不带引号的参数以冒号（:）开头，它会被当做一个gsql变量，并且该变量的值最终会成为真正的参数值。
- 有些命令以一个SQL标识的名称（比如一个表）为参数。这些参数遵循SQL语法关于双引号的规则：不带双引号的标识强制转换成小写，而双引号保护字母不进行大小写转换，并且允许在标识符中使用空白。在双引号中，成对的双引号在结果名称中分析成一个双引号。比如，FOO”BAR”BAZ解析成fooBARbaz；而”A weird”“name”解析成A weird”name。
- 对参数的分析在遇到另一个不带引号的反斜杠时停止。这里会认为是一个新的元命令的开始。特殊的双反斜杠序列（\\）标识参数的结尾并将继续分析后面的SQL语句（如果存在）。这样SQL和gsql命令可以自由的在一行里面混合。但是在任何情况下，一条元命令的参数不能延续超过行尾。

**元命令**

元命令的详细说明请参见下表。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 以下命令中所提到的FILE代表文件路径。此路径可以是绝对路径（如/home/gauss/file.txt），也可以是相对路径（file.txt，file.txt会默认在用户执行gsql命令所在的路径下创建）。

**表 11** 一般的元命令

| 参数                      | 参数说明                                                     | 取值范围                                                     |
| :------------------------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| \copyright                | 显示MogDB的版本和版权信息。                                  | -                                                            |
| \g [FILE] or ;            | 执行查询（并将结果发送到文件或管道）。                       | -                                                            |
| \h(\help) [NAME]          | 给出指定SQL语句的语法帮助。                                  | 如果没有给出NAME，gsql将列出可获得帮助的所有命令。如果NAME是一个星号（*），则显示所有SQL语句的语法帮助。 |
| \parallel [on [num]\|off] | 控制并发执行开关。<br /> - on：打开控制并发执行开关，且最大并发数为num。<br />- off：关闭控制并发执行开关。<br />说明：<br />- 不支持事务中开启并发执行以及并发中开启事务。<br />- 不支持\d这类元命令的并发。<br />- 并发select返回结果混乱问题，此为客户可接受，core、进程停止响应不可接受。<br />- 不推荐在并发中使用set语句，否则导致结果与预期不一致。<br />- 不支持创建临时表！如需使用临时表，需要在开启parallel之前创建好，并在parallel内部使用。parallel内部不允许创建临时表。<br />- \parallel执行时最多会启动num个独立的gsql进程连接服务器。<br />- \parallel中所有作业的持续时间不能超过session_timeout，否则可能会导致并发执行过程中断连。 | num的默认值：1024。<br />须知：<br />- 服务器能接受的最大连接数受max_connection及当前已有连接数限制。<br />- 设置num时请考虑服务器当前可接受的实际连接数合理指定。 |
| \q                        | 退出gsql程序。在一个脚本文件里，只在脚本终止的时候执行。     | -                                                            |

**表 12** 查询缓存区元命令

| 参数                  | 参数说明                                                     |
| :-------------------- | :----------------------------------------------------------- |
| \e [FILE] [LINE]      | 使用外部编辑器编辑查询缓冲区（或者文件）。                   |
| \ef [FUNCNAME [LINE]] | 使用外部编辑器编辑函数定义。如果指定了LINE（即行号），则光标会指到函数体的指定行。 |
| \p                    | 打印当前查询缓冲区到标准输出。                               |
| \r                    | 重置（或清空）查询缓冲区。                                   |
| \w FILE               | 将当前查询缓冲区输出到文件。                                 |

**表 13** 输入/输出元命令

| 参数                                                         | 参数说明                                                     |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| \copy { table [ ( column_list ) ] \| ( query ) } { from \| to } { filename \| stdin \| stdout \| pstdin \| pstdout } [ with ] [ binary ] [ oids ] [ delimiter [ as ] 'character' ] [ null [ as ] 'string' ] [ csv [ header ] [ quote [ as ] 'character' ] [ escape [ as ] 'character' ] [ force quote column_list \| * ] [ force not null column_list ] ] | 在任何psql客户端登录数据库成功后可以执行导入导出数据， 这是一个运行SQL COPY命令的操作，但不是读取或写入指定文件的服务器，而是读取或写入文件，并在服务器和本地文件系统之间路由数据。 这意味着文件的可访问性和权限是本地用户的权限，而不是服务器的权限，并且不需要数据库初始化用户权限。<br />说明：<br />\COPY只适合小批量，格式良好的数据导入，不会对非法字符进行预处理，也无容错能力。导入数据应优先选择COPY。 |
| \echo [STRING]                                               | 把字符串写到标准输出。                                       |
| \i FILE                                                      | 从文件FILE中读取内容，并将其当作输入，执行查询。             |
| \i+ FILE KEY                                                 | 执行加密文件中的命令。                                       |
| \ir FILE                                                     | 和\i类似，只是相对于存放当前脚本的路径。                     |
| \ir+ FILE KEY                                                | 和\i+类似，只是相对于存放当前脚本的路径。                    |
| \o [FILE]                                                    | 把所有的查询结果发送到文件里。                               |
| \qecho [STRING]                                              | 把字符串写到查询结果输出流里。                               |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  [表14 显示信息元命令](#xianshi)中的选项S表示显示系统对象，PATTERN表示显示对象附加的描述信息。用来指定要被显示的对象名称。

**表 14** 显示信息元命令 <a id="xianshi"> </a>

| 参数                                                         | 参数说明                                                     | 取值范围                                                     | 示例                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| \d[S+]                                                       | 列出当前search_path中模式下所有的表、视图和序列。当search_path中不同模式存在同名对象时，只显示search_path中位置靠前模式下的同名对象。 | -                                                            | 列出当前search_path中模式下所有的表、视图和序列。<br />`mogdb=# \d` |
| \d[S+] NAME                                                  | 列出指定表、视图和索引的结构。                               | -                                                            | 假设存在表a，列出指定表a的结构。<br />`mogdb=#  \dtable+ a`  |
| \d+ [PATTERN]                                                | 列出所有表、视图和索引。                                     | 如果声明了PATTERN，只显示名称匹配PATTERN的表、视图和索引。   | 列出所有名称以f开头的表、视图和索引。`mogdb=# \d+ f`         |
| \da[S] [PATTERN]                                             | 列出所有可用的聚集函数，以及它们操作的数据类型和返回值类型。 | 如果声明了PATTERN，只显示名称匹配PATTERN的聚集函数。         | 列出所有名称以f开头可用的聚集函数，以及它们操作的数据类型和返回值类型。<br />`mogdb=# \da f` |
| \db[+] [PATTERN]                                             | 列出所有可用的表空间。                                       | 如果声明了PATTERN，只显示名称匹配PATTERN的表空间。           | 列出所有名称以p开头的可用表空间。<br />`mogdb=# \db p*`      |
| \dc[S+] [PATTERN]                                            | 列出所有字符集之间的可用转换。                               | 如果声明了PATTERN，只显示名称匹配PATTERN的转换。             | 列出所有字符集之间的可用转换。<br /           |
| \dC[+] [PATTERN]                                             | 列出所有类型转换。                                           | 如果声明了PATTERN，只显示名称匹配PATTERN的转换。             | 列出所有名称以c开头的类型转换。<br />`mogdb=# \dC c`         |
| \dd[S] [PATTERN]                                             | 显示所有匹配PATTERN的描述。                                  | 如果没有给出参数，则显示所有可视对象。“对象”包括：聚集、函数、操作符、类型、关系(表、视图、索引、序列、大对象)、规则。 | 列出所有可视对象。<br />`mogdb=# \dd`                        |
| \ddp [PATTERN]                                               | 显示所有默认的使用权限。                                     | 如果指定了PATTERN，只显示名称匹配PATTERN的使用权限。         | 列出所有默认的使用权限。<br />`mogdb=# \ddp`                 |
| \dD[S+] [PATTERN]                                            | 列出所有可用域。                                             | 如果声明了PATTERN，只显示名称匹配PATTERN的域。               | 列出所有可用域。<br />`mogdb=# \dD`                          |
| \ded[+] [PATTERN]                                            | 列出所有的Data Source对象。                                  | 如果声明了PATTERN，只显示名称匹配PATTERN的对象。             | 列出所有的Data Source对象。<br />`mogdb=# \ded`              |
| \det[+] [PATTERN]                                            | 列出所有的外部表。                                           | 如果声明了PATTERN，只显示名称匹配PATTERN的表。               | 列出所有的外部表。<br />`mogdb=# \det`                       |
| \des[+] [PATTERN]                                            | 列出所有的外部服务器。                                       | 如果声明了PATTERN，只显示名称匹配PATTERN的服务器。           | 列出所有的外部服务器。<br />`mogdb=# \des`                   |
| \deu[+] [PATTERN]                                            | 列出用户映射信息。                                           | 如果声明了PATTERN，只显示名称匹配PATTERN的信息。             | 列出用户映射信息。<br />`mogdb=# \deu`                       |
| \dew[+] [PATTERN]                                            | 列出封装的外部数据。                                         | 如果声明了PATTERN，只显示名称匹配PATTERN的数据。             | 列出封装的外部数据。<br />`mogdb=# \dew`                     |
| \df[antw][S+] [PATTERN]                                     | 列出所有可用函数，以及它们的参数和返回的数据类型。a代表聚集函数，n代表普通函数，t代表触发器，w代表窗口函数。 | 如果声明了PATTERN，只显示名称匹配PATTERN的函数。             | 列出所有可用函数，以及它们的参数和返回的数据类型。<br />`mogdb=# \df` |
| \dF[+] [PATTERN]                                             | 列出所有的文本搜索配置信息。                                 | 如果声明了PATTERN，只显示名称匹配PATTERN的配置信息。         | 列出所有的文本搜索配置信息。<br />`mogdb=# \dF+`             |
| \dFd[+] [PATTERN]                                            | 列出所有的文本搜索字典。                                     | 如果声明了PATTERN，只显示名称匹配PATTERN的字典。             | 列出所有的文本搜索字典。<br />`mogdb=# \dFd`                 |
| \dFp[+] [PATTERN]                                            | 列出所有的文本搜索分析器。                                   | 如果声明了PATTERN，只显示名称匹配PATTERN的分析器。           | 列出所有的文本搜索分析器。<br />`mogdb=# \dFp`               |
| \dFt[+] [PATTERN]                                            | 列出所有的文本搜索模板。                                     | 如果声明了PATTERN，只显示名称匹配PATTERN的模板。             | 列出所有的文本搜索模板。<br />`mogdb=# \dFt`                 |
| \dg[+] [PATTERN]                                             | 列出所有数据库角色。<br />说明：<br />因为用户和群组的概念被统一为角色，所以这个命令等价于\du。为了和以前兼容，所以保留两个命令。 | 如果指定了PATTERN，只显示名称匹配PATTERN的角色。             | 列出名称为‘j_e’所有数据库角色。<br />`mogdb=# \dg j?e`       |
| \dl                                                          | \lo_list的别名，显示一个大对象的列表。                       | -                                                            | 列出所有的大对象。<br />`mogdb=# \dl`                        |
| \dL[S+] [PATTERN]                                            | 列出可用的程序语言。                                         | 如果指定了PATTERN，只列出名称匹配PATTERN的语言。             | 列出可用的程序语言。<br />`mogdb=# \dL`                      |
| \dn[S+] [PATTERN]                                            | 列出所有的模式（名称空间）。                                 | 如果声明了PATTERN，只列出名称匹配PATTERN的模式名。缺省时，只列出用户创建的模式。 | 列出所有名称以d开头的模式以及相关信息。<br />`mogdb=#  \dn+ d*` |
| \do[S] [PATTERN]                                             | 列出所有可用的操作符，以及它们的操作数和返回的数据类型。     | 如果声明了PATTERN，只列出名称匹配PATTERN的操作符。缺省时，只列出用户创建的操作符。 | 列出所有可用的操作符，以及它们的操作数和返回的数据类型。<br />`mogdb=# \do` |
| \dO[S+] [PATTERN]                                            | 列出排序规则。                                               | 如果声明了PATTERN，只列出名称匹配PATTERN的规则。缺省时，只列出用户创建的规则。 | 列出排序规则。<br />`mogdb=# \dO`                            |
| \dp [PATTERN]                                                | 列出一列可用的表、视图以及相关的权限信息。<br />\dp显示结果如下：<br />`rolename=xxxx/yyyy  -赋予一个角色的权限`<br /<br />xxxx表示赋予的权限，yyyy表示授予这个权限的角色。权限的参数说明请参见**表 权限的参数说明**。 | 如果指定了PATTERN，只列出名称匹配PATTERN的表、视图。         | 列出一列可用的表、视图以及相关的权限信息。<br />`mogdb=# \dp` |
| \drds [PATTERN1 [PATTERN2]]                                  | 列出所有修改过的配置参数。这些设置可以是针对角色的、针对数据库的或者同时针对两者的。PATTERN1和PATTERN2表示要列出的角色PATTERN和数据库PATTERN。 | 如果声明了PATTERN，只列出名称匹配PATTERN的规则。缺省或指定*时，则会列出所有设置。 | 列出mogdb数据库所有修改过的配置参数。<br />`mogdb=# \drds * mogdb` |
| \dT[S+] [PATTERN]                                            | 列出所有的数据类型。                                         | 如果指定了PATTERN，只列出名称匹配PATTERN的类型。             | 列出所有的数据类型。<br />`mogdb=# \dT`                      |
| \du[+] [PATTERN]                                             | 列出所有数据库角色。<br />说明：<br />因为用户和群组的概念被统一为角色，所以这个命令等价于\dg。为了和以前兼容，所以保留两个命令。 | 如果指定了PATTERN，则只列出名称匹配PATTERN的角色。           | 列出所有数据库角色。<br />`mogdb=# \du`                      |
| \dE[S+] [PATTERN]\di[S+] [PATTERN]\ds[S+] [PATTERN]\dt[S+] [PATTERN]\dv[S+] [PATTERN] | 这一组命令，字母E，i，s，t和v分别代表着外部表，索引，序列，表和视图。可以以任意顺序指定其中一个或者它们的组合来列出这些对象。例如：\dit列出所有的索引和表。在命令名称后面追加+，则每一个对象的物理尺寸以及相关的描述也会被列出。 | 如果指定了PATTERN，只列出名称匹配该PATTERN的对象。默认情况下只会显示用户创建的对象。通过PATTERN或者S修饰符可以把系统对象包括在内。 | 列出所有的索引和视图。<br />`mogdb=# \div`                   |
| \dx[+] [PATTERN]                                             | 列出安装数据库的扩展信息。                                   | 如果指定了PATTERN，则只列出名称匹配PATTERN的扩展信息。       | 列出安装数据库的扩展信息。<br />`mogdb=# \dx`                |
| \l[+]                                                        | 列出服务器上所有数据库的名称、所有者、字符集编码以及使用权限。 | -                                                            | 列出服务器上所有数据库的名称、所有者、字符集编码以及使用权限。<br />`mogdb=#  \l` |
| \sf[+] FUNCNAME                                              | 显示函数的定义。<br />说明：<br />对于带圆括号的函数名，需要在函数名两端添加双引号，并且在双引号后面加上参数类型列表。参数类型列表两端添加圆括号。 | -                                                            | 假设存在函数function_a和函数名带圆括号的函数func()name，列出函数的定义。<br />`mogdb=# \sf function_a mogdb=# \sf “func()name”(argtype1, argtype2)` |
| \z [PATTERN]                                                 | 列出数据库中所有表、视图和序列，以及它们相关的访问特权。     | 如果给出任何pattern ，则被当成一个正则表达式，只显示匹配的表、视图、序列。 | 列出数据库中所有表、视图和序列，以及它们相关的访问特权。<br />`mogdb=# \z` |

**表 15** 权限的参数说明

| 参数    | 参数说明                                                     |
| :------ | :----------------------------------------------------------- |
| r       | SELECT：允许对指定的表、视图读取数据。                       |
| w       | UPDATE：允许对指定表更新字段。                               |
| a       | INSERT：允许对指定表插入数据。                               |
| d       | DELETE：允许删除指定表中的数据。                             |
| D       | TRUNCATE：允许清理指定表中的数据。                           |
| x       | REFERENCES：允许创建外键约束。由于当前不支持外键，所以该参数暂不生效。 |
| t       | TRIGGER：允许在指定表上创建触发器。                          |
| X       | EXECUTE：允许使用指定的函数，以及利用这些函数实现的操作符。  |
| U       | USAGE：<br />- 对于过程语言，允许用户在创建函数时，指定过程语言。<br />- 对于模式，允许访问包含在指定模式中的对象。<br />- 对于序列，允许使用nextval函数。 |
| C       | CREATE：<br />- 对于数据库，允许在该数据库里创建新的模式。<br />- 对于模式，允许在该模式中创建新的对象。<br />- 对于表空间，允许在其中创建表，以及允许创建数据库和模式的时候把该表空间指定为其缺省表空间。 |
| c       | CONNECT：允许用户连接到指定的数据库。                        |
| T       | TEMPORARY：允许创建临时表。                                  |
| arwdDxt | ALL PRIVILEGES：一次性给指定用户/角色赋予所有可赋予的权限。  |
| *       | 给前面权限的授权选项。                                       |

**表 16** 格式化元命令

| 参数               | 参数说明                                                     |
| :----------------- | :----------------------------------------------------------- |
| \a                 | 对齐模式和非对齐模式之间的切换。                             |
| \C [STRING]        | 把正在打印的表的标题设置为一个查询的结果或者取消这样的设置。 |
| \f [STRING]        | 对于不对齐的查询输出，显示或者设置域分隔符。                 |
| \H                 | - 若当前模式为文本格式，则切换为HTML输出格式。<br />- 若当前模式为HTML格式，则切换回文本格式。 |
| \pset NAME [VALUE] | 设置影响查询结果表输出的选项。NAME的取值见[表17 可调节的打印选项](#ketiaojie)。 |
| \t [on\|off]       | 切换输出的字段名的信息和行计数脚注。                         |
| \T [STRING]        | 指定在使用HTML输出格式时放在table标签里的属性。如果参数为空，不设置。 |
| \x [on\|off\|auto] | 切换扩展行格式。                                             |

**表 17** 可调节的打印选项 <a id="ketiaojie"> </a>

| 选项                  | 选项说明                                                     | 取值范围                                                     |
| :-------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| border                | value必须是一个数字。通常这个数字越大，表的边界就越宽线就越多，但是这个取决于特定的格式。 | - 在HTML格式下，取值范围为大于0的整数。<br />- 在其他格式下，取值范围: <br />  - 0：无边框<br />  - 1：内部分隔线<br />  - 2：台架 |
| expanded (或x)        | 在正常和扩展格式之间切换。                                   | - 当打开扩展格式时，查询结果用两列显示，字段名称在左、数据在右。这个模式在数据无法放进通常的”水平”模式的屏幕时很有用。<br />- 在正常格式下，当查询输出的格式比屏幕宽时，用扩展格式。正常格式只对aligned和wrapped格式有用。 |
| fieldsep              | 声明域分隔符来实现非对齐输出。这样就可以创建其他程序希望的制表符或逗号分隔的输出。要设置制表符域分隔符，键入\pset fieldsep '\t'。缺省域分隔符是 '\|' (竖条符)。 | -                                                            |
| fieldsep_zero         | 声明域分隔符来实现非对齐输出到零字节。                       | -                                                            |
| footer                | 用来切换脚注。                                               | -                                                            |
| format                | 设置输出格式。允许使用唯一缩写（这意味着一个字母就够了）。   | 取值范围: <br />- unaligned：写一行的所有列在一条直线上中，当前活动字段分隔符分隔。<br />- aligned：此格式是标准的，可读性最好的文本输出。<br />- wrapped：类似aligned，但是包装跨行的宽数据值，使其适应目标字段的宽度输出。<br />- html：把表输出为可用于文档里的对应标记语言。输出不是完整的文档。<br />- latex：把表输出为可用于文档里的对应标记语言。输出不是完整的文档。<br />- troff-ms：把表输出为可用于文档里的对应标记语言。输出不是完整的文档。 |
| null                  | 打印一个字符串，用来代替一个null值。                         | 缺省是什么都不打印，这样很容易和空字符串混淆。               |
| numericlocale         | 切换分隔小数点左边的数值的区域相关的分组符号。               | - on：显示指定的分隔符。<br />- off：不显示分隔符。<br />忽略此参数，显示默认的分隔符。 |
| pager                 | 控制查询和gsql帮助输出的分页器。如果设置了环境变量PAGER，输出将被指向到指定程序，否则使用系统缺省。 | - on：当输出到终端且不适合屏幕显示时，使用分页器。<br />- off：不使用分页器。<br />- always：当输出到终端无论是否符合屏幕显示时，都使用分页器。 |
| recordsep             | 声明在非对齐输出格式时的记录分隔符。                         | -                                                            |
| recordsep_zero        | 声明在非对齐输出到零字节时的记录分隔符。                     | -                                                            |
| tableattr（或T）      | 声明放在html输出格式中HTML table标签的属性（例如：cellpadding或bgcolor）。注意：这里可能不需要声明border，因为已经在\pset border里用过了。如果没有给出value，则不设置表的属性。 | -                                                            |
| title                 | 为随后打印的表设置标题。这个可以用于给输出一个描述性标签。如果没有给出value，不设置标题。 | -                                                            |
| tuples_only （或者t） | 在完全显示和只显示实际的表数据之间切换。完全显示将输出像列头、标题、各种脚注等信息。在tuples_only模式下，只显示实际的表数据。 | -                                                            |

**表 18** 连接元命令

| 参数                                           | 参数说明                                                     | 取值范围                         |
| :--------------------------------------------- | :----------------------------------------------------------- | :------------------------------- |
| \c[onnect] [DBNAME\|- USER\|- HOST\|- PORT\|-] | 连接到一个新的数据库（当前数据库为mogdb）。当数据库名称长度超过63个字节时，默认前63个字节有效，连接到前63个字节对应的数据库，但是gsql的命令提示符中显示的数据库对象名仍为截断前的名称。<br />说明：<br />重新建立连接时，如果切换数据库登录用户，将可能会出现交互式输入，要求输入新用户的连接密码。该密码最长长度为999字节，受限于GUC参数password_max_length的最大值。 | -                                |
| \encoding [ENCODING]                           | 设置客户端字符编码格式。                                     | 不带参数时，显示当前的编码格式。 |
| \conninfo                                      | 输出当前连接的数据库的信息。                                 | -                                |

**表 19** 操作系统元命令

| 参数                 | 参数说明                                                     | 取值范围                                         |
| :------------------- | :----------------------------------------------------------- | :----------------------------------------------- |
| \cd [DIR]            | 切换当前的工作目录。                                         | 绝对路径或相对路径，且满足操作系统路径命名规则。 |
| \setenv NAME [VALUE] | 设置环境变量NAME为VALUE，如果没有给出VALUE值，则不设置环境变量。 | -                                                |
| \timing [on\|off]    | 以毫秒为单位显示每条SQL语句的执行时间。                      | - on表示打开显示。<br />- off表示关闭显示。      |
| ! [COMMAND]          | 返回到一个单独的Unix shell或者执行Unix命令COMMAND。          | -                                                |

**表 20** 变量元命令

| 参数                | 参数说明                                                     |
| :------------------ | :----------------------------------------------------------- |
| \prompt [TEXT] NAME | 提示用户用文本格式来指定变量名称。                           |
| \set [NAME [VALUE]] | 设置内部变量NAME为VALUE或者如果给出了多于一个值，设置为所有这些值的连接结果。如果没有给出第二个参数，只设变量不设值。<br />有一些常用变量被gsql特殊对待，它们是一些选项设置，通常所有特殊对待的变量都是由大写字母组成(可能还有数字和下划线)。 [表21 \set常用命令](#set)是一个所有特殊对待的变量列表。 |
| \unset NAME         | 不设置（或删除）gsql变量名。                                 |

**表 21** \set常用命令 <a id="set"> </a>

| 名称                     | 命令说明                                                     | 取值范围                                         |
| :----------------------- | :----------------------------------------------------------- | :----------------------------------------------- |
| \set VERBOSITY value     | 这个选项可以设置为值default， verbose，terse之一以控制错误报告的冗余行。 | value取值范围: default， verbose，terse          |
| \set ON_ERROR_STOP value | 如果设置了这个变量，脚本处理将马上停止。如果该脚本是从另外一个脚本调用的，那个脚本也会按同样的方式停止。如果最外层的脚本不是从一次交互的gsql会话中调用的而是用-f选项调用的，gsql将返回错误代码3，以示这个情况与致命错误条件的区别(错误代码为1)。 | value取值范围为：on/off，true/false，yes/no，1/0 |
| \set RETRY [retry_times] | 用于控制是否开启语句出错场景下的重试功能，参数retry_times用来指定最大重试次数，缺省值为5，取值范围为5-10。当重试功能已经开启时，再次执行\set RETRY可以关闭该功能。<br />使用配置文件retry_errcodes.conf列举需要重试的错误码列表，该文件和gsql可执行程序位于同一级目录下。该配置文件为系统配置，非用户定义，不允许用户直接修改。<br />当前支持以下出错场景的重试：<br />- YY001：TCP通信错误，Connection reset by peer<br />- YY002：TCP通信错误，Connection reset by peer<br />- YY003：锁超时，Lock wait timeout…/wait transaction xxx sync time exceed xxx<br />- YY004：TCP通信错误，Connection timed out<br />- YY005：SET命令发送失败，ERROR SET query<br />- YY006：内存申请失败，memory is temporarily unavailable<br />- YY007：通信库错误，Memory allocate error<br />- YY008：通信库错误，No data in buffer<br />- YY009：通信库错误，Close because release memory<br />- YY010：通信库错误，TCP disconnect<br />- YY011：通信库错误，SCTP disconnect<br />- YY012：通信库错误，Stream closed by remote<br />- YY013：通信库错误，Wait poll unknown error<br />- YY014,YY015,53200,08006,08000,57P01,XX003,XX009等<br />同时，出错时gsql会查询数据库节点的连接状态，当状态异常时会sleep 1分钟再进行重试，能够覆盖大部分主备切换场景下的出错重试。<br />说明：<br />1. 不支持事务块中的语句错误重试；<br />2. 不支持通过ODBC、JDBC接口查询的出错重试；<br />3. 含有unlogged表的sql语句，不支持节点故障后的出错重试；<br />4. gsql客户端本身出现的错误，不在重跑考虑范围之内； | retry_times取值范围为：5-10                      |

**表 22** 大对象元命令

| 参数     | 参数说明                                                     |
| :------- | :----------------------------------------------------------- |
| \lo_list | 显示一个目前存储在该数据库里的所有MogDB大对象和提供给他们的注释。 |

**PATTERN**

很多\d命令都可以用一个PATTERN参数来指定要被显示的对象名称。在最简单的情况下，PATTERN正好就是该对象的准确名称。在PATTERN中的字符通常会被变成小写形式（就像在SQL名称中那样），例如\dt FOO将会显示名为foo的表。就像在SQL名称中那样，把PATTERN放在双引号中可以阻止它被转换成小写形式。如果需要在一个PATTERN中包括一个真正的双引号字符，则需要把它写成两个相邻的双引号，这同样是符合SQL引用标识符的规则。例如，\dt “FOO”“BAR”将显示名为FOO”BAR（不是foo”bar）的表。和普通的SQL名称规则不同，不能只在PATTERN的一部分周围放上双引号，例如\dt FOO”FOO”BAR将会显示名为fooFOObar的表。

不使用PATTERN参数时，\d命令会显示当前schema搜索路径中可见的全部对象,这等价于用\*作为PATTERN。所谓对象可见是指可以直接用名称引用该对象，而不需要用schema来进行限定。要查看数据库中所有的对象而不管它们的可见性，可以把*.*用作PATTERN。

如果放在一个PATTERN中，\*将匹配任意字符序列（包括空序列），而?会匹配任意的单个字符（这种记号方法就像 Unix shell 的文件名PATTERN一样）。例如，\dt int\*会显示名称以int开始的表。但是如果被放在双引号内，\*和?就会失去这些特殊含义而变成普通的字符。

包含一个点号（.）的PATTERN被解释为一个schema名称模式后面跟上一个对象名称模式。例如，\dt foo*.*bar会显示名称以foo开始的schema中所有名称包括bar的表。如果没有出现点号，那么模式将只匹配当前schema搜索路径中可见的对象。同样，双引号内的点号会失去其特殊含义并且变成普通的字符。

高级用户可以使用字符类等正则表达式记法，如[0-9]可以匹配任意数字。所有的正则表达式特殊字符都按照POSIX正则表达式所说的工作。以下字符除外：

- .会按照上面所说的作为一种分隔符。
- \*会被翻译成正则表达式记号.*。
- ?会被翻译成.。
- $则按字面意思匹配。

根据需要，可以通过书写?,(`R+|`),(`R|`)和`R?`来分别模拟PATTERN字符`.`,`R+|`和`R?`。\$不需要作为一个正则表达式字符，因为PATTERN必须匹配整个名称，而不是像正则表达式的常规用法那样解释（换句话说，$会被自动地追加到PATTERN上）。如果不希望该PATTERN的匹配位置被固定，可以在开头或者结尾写上\*。注意在双引号内，所有的正则表达式特殊字符会失去其特殊含义并且按照其字面意思进行匹配。另外，在操作符名称PATTERN中（即\do的PATTERN参数），正则表达式特殊字符也按照字面意思进行匹配。

### 常见问题处理

**连接性能问题**

- 开启了log_hostname，但是配置了错误的DNS导致的连接性能问题。

  在连接上数据库，通过“show log_hostname”语句，检查数据库中是否开启了log_hostname参数。

  如果开启了相关参数，那么数据库内核会通过DNS反查客户端所在机器的主机名。这时如果数据库主节点配置了不正确的/不可达的DNS服务器，那么会导致数据库建立连接过程较慢。此参数的更多信息，详见《参考指南》中“GUC参数说明 &gt; 错误报告和日志 &gt; [记录日志的内容](../../reference-guide/guc-parameters/10-error-reporting-and-logging/3-logging-content.md)”章节中关于“log_hostname”的描述。

- 数据库内核执行初始化语句较慢导致的性能问题。

  此种情况定位较难，可以尝试使用Linux的跟踪命令：strace。

  ```
  strace gsql -U MyUserName -W MyPassWord -d mogdb -h 127.0.0.1 -p 23508 -r -c '\q'
  ```

  此时便会在屏幕上打印出数据库的连接过程。比如较长时间停留在下面的操作上：

  ```
  sendto(3, "Q\0\0\0\25SELECT VERSION()\0", 22, MSG_NOSIGNAL, NULL, 0) = 22
  poll([{fd=3, events=POLLIN|POLLERR}], 1, -1) = 1 ([{fd=3, revents=POLLIN}])
  ```

  此时便可以确定是数据库执行”SELECT VERSION()“语句较慢。

  在连接上数据库后，便可以通过执行“explain performance select version()”语句来确定初始化语句执行较慢的原因。更多信息，详见《性能优化指南》中“SQL优化指南 &gt; [SQL执行计划介绍](../../performance-tuning/2-sql/2-introduction-to-the-sql-execution-plan.md)”章节。

  另外还有一种场景不太常见：由于数据库主节点所在机器的磁盘满或故障，此时所查询等受影响，无法进行用户认证，导致连接过程挂起，表现为假死。解决此问题清理数据库主节点的数据盘空间便可。

- TCP连接创建较慢问题。

  此问题可以参考上面的初始化语句较慢排查的做法，通过strace跟踪，如果长时间停留在：

  ```
  connect(3, {sa_family=AF_FILE, path="/home/test/tmp/gaussdb_llt1/.s.PGSQL.61052"}, 110) = 0
  ```

  或者

  ```
  connect(3, {sa_family=AF_INET, sin_port=htons(61052), sin_addr=inet_addr("127.0.0.1")}, 16) = -1 EINPROGRESS (Operation now in progress)
  ```

  那么说明客户端与数据库端建立物理连接过慢，此时应当检查网络是否存在不稳定、网络吞吐量太大的问题。

**创建连接故障**

- gsql: could not connect to server: No route to host

  此问题一般是指定了不可达的地址或者端口导致的。请检查-h参数与-p参数是否添加正确。

- gsql: FATAL: Invalid username/password,login denied.

  此问题一般是输入了错误的用户名和密码导致的，请联系数据库管理员，确认用户名和密码的正确性。

- gsql: FATAL: Forbid remote connection with trust method!

  数据库由于安全问题，禁止远程登录时使用trust模式。这时需要修改pg_hba.conf里的连接认证信息。具体的设置信息请参见：《安全指南》中“数据库安全管理 &gt; 客户端接入认证 &gt; [配置文件参考](../../security-guide/security/1-client-access-authentication.md#配置文件参考)”章节。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  请不要修改pg_hba.conf中MogDB主机的相关设置，否则可能导致数据库功能故障。建议业务应用部署在MogDB之外，而非MogDB内部。

- 连接数据库，添加“-h 127.0.0.1”可以连接，去掉后无法连接问题。

  通过执行SQL语句“show unix_socket_directory”检查数据库主节点使用的Unix套接字目录，是否与shell中的环境变量$PGHOST一致。

  如果检查结果不一致，那么修改PGHOST环境变量到GUC参数unix_socket_directory指向的目录便可。

  关于unix_socket_directory的更多信息，详见《参考指南》中“GUC参数说明 &gt; 连接和认证 &gt; [连接设置](../../reference-guide/guc-parameters/3-connection-and-authentication/1-connection-settings.md)”章节中的说明。

- The “libpq.so” loaded mismatch the version of gsql, please check it.

  此问题是由于环境中使用的libpq.so的版本与gsql的版本不匹配导致的，请通过“ldd gsql”命令确认当前加载的libpq.so的版本，并通过修改LD_LIBRARY_PATH环境变量来加载正确的libpq.so。

- gsql: symbol lookup error: xxx/gsql: undefined symbol: libpqVersionString

  此问题是由于环境中使用的libpq.so的版本与gsql的版本不匹配导致的（也有可能是环境中存在PostgreSQL的libpq.so），请通过“ldd gsql”命令确认当前加载的libpq.so的版本，并通过修改LD_LIBRARY_PATH环境变量来加载正确的libpq.so。

- gsql: connect to server failed: Connection timed out

  Is the server running on host “xx.xxx.xxx.xxx” and accepting TCP/IP connections on port xxxx?

  此问题是由于网络连接故障造成。请检查客户端与数据库服务器间的网络连接。如果发现从客户端无法PING到数据库服务器端，则说明网络连接出现故障。请联系网络管理人员排查解决。

  ```
  ping -c 4 10.10.10.1
  PING 10.10.10.1 (10.10.10.1) 56(84) bytes of data.
  From 10.10.10.1: icmp_seq=2 Destination Host Unreachable
  From 10.10.10.1 icmp_seq=2 Destination Host Unreachable
  From 10.10.10.1 icmp_seq=3 Destination Host Unreachable
  From 10.10.10.1 icmp_seq=4 Destination Host Unreachable
  --- 10.10.10.1 ping statistics ---
  4 packets transmitted, 0 received, +4 errors, 100% packet loss, time 2999ms
  ```

- gsql: FATAL: permission denied for database “mogdb”

  DETAIL: User does not have CONNECT privilege.

  此问题是由于用户不具备访问该数据库的权限，可以使用如下方法解决。

  1. 使用管理员用户dbadmin连接数据库。

     ```
     gsql -d mogdb -U dbadmin -p 15400
     ```

  2. 赋予该用户访问数据库的权限。

     GRANT CONNECT ON DATABASE mogdb TO user1;

     > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  实际上，常见的许多错误操作也可能产生用户无法连接上数据库的现象。如用户连接的数据库不存在，用户名或密码输入错误等。这些错误操作在客户端工具也有相应的提示信息。
     >
     > ```
     > gsql -d mogdb -p 15400
     > gsql: FATAL:  database "mogdb" does not exist
     > gsql -d mogdb -U user1 -W gauss@789 -p 15400
     > gsql: FATAL:  Invalid username/password,login denied.
     > ```

- gsql: FATAL: sorry, too many clients already，active/non-active: 2/10/3.

  此问题是由于系统连接数量超过了[表23 查看会话连接数](#chakan)最大连接数量。请联系数据库DBA进行会话连接数管理，释放无用会话。

  关于查看用户会话连接数的方法如。

  会话状态可以在视图PG_STAT_ACTIVITY中查看。无用会话可以使用函数pg_terminate_backend进行释放。

  ```
  select datid,pid,state from pg_stat_activity;
  ```

  ```
   datid |       pid       | state
  -------+-----------------+--------
  13205 | 139834762094352 | active
  13205 | 139834759993104 | idle
  (2 rows)
  ```

  其中pid的值即为该会话的线程ID。根据线程ID结束会话。

  ```
  SELECT PG_TERMINATE_BACKEND(139834759993104);
  ```

  显示类似如下信息，表示结束会话成功。

  ```
  PG_TERMINATE_BACKEND
  ----------------------
   t
  (1 row)
  ```

  **表 23** 查看会话连接数 <a id="chakan"> </a>

  | 描述                               | 命令                                                         |
  | :--------------------------------- | :----------------------------------------------------------- |
  | 查看指定用户的会话连接数上限。     | 执行如下命令查看连接到指定用户USER1的会话连接数上限。其中-1表示没有对用户user1设置连接数的限制。<br />`SELECT ROLNAME,ROLCONNLIMIT FROM PG_ROLES WHERE ROLNAME='user1'; rolname | rolconnlimit ---+----- user1    |           -1 (1 row)` |
  | 查看指定数据库的会话连接数上限。   | 执行如下命令查看连接到指定数据库mogdb的会话连接数上限。其中-1表示没有对数据库mogdb设置连接数的限制。<br />`SELECT DATNAME,DATCONNLIMIT FROM PG_DATABASE WHERE DATNAME='mogdb'; datname  | datconnlimit ----+----- mogdb |           -1 (1 row)` |
  | 查看指定数据库已使用的会话连接数。 | 执行如下命令查看指定数据库mogdb上已使用的会话连接数。其中，1表示数据库mogdb上已使用的会话连接数。<br />`SELECT COUNT(*) FROM PG_STAT_ACTIVITY WHERE DATNAME='mogdb'; count ----     1 (1 row)` |

- gsql: wait xxx.xxx.xxx.xxx:xxxx timeout expired

  gsql在向数据库发起连接的时候，会有5分钟超时机制，如果在这个超时时间内，数据库未能正常的对客户端请求进行校验和身份认证，那么gsql会退出当前会话的连接过程，并报出如上错误。

  一般来说，此问题是由于连接时使用的-h参数及-p参数指定的连接主机及端口有误（即错误信息中的xxx部分），导致通信故障；极少数情况是网络故障导致。要排除此问题，请检查数据库的主机名及端口是否正确。

- gsql: could not receive data from server: Connection reset by peer.

  同时，检查数据库主节点日志中出现类似如下日志“ FATAL: cipher file “/data/dbnode/server.key.cipher” has group or world access”，一般是由于数据目录或部分关键文件的权限被误操作篡改导致。请参照其他正常实例下的相关文件权限，修改回来便可。

- gsql: FATAL: GSS authentication method is not allowed because XXXX user password is not disabled.

  目标数据库主节点的pg_hba.conf里配置了当前客户端IP使用”gss”方式来做认证，该认证算法不支持用作客户端的身份认证，请修改到”sha256”后再试。配置方法见《安全指南》中“数据库安全管理 &gt; 客户端接入认证 &gt; [配置文件参考](../../security-guide/security/1-client-access-authentication.md#配置文件参考)”章节。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
  >
  > - 请不要修改pg_hba.conf中MogDB主机的相关设置，否则可能导致数据库功能故障。
  > - 建议业务应用部署在MogDB之外，而非MogDB内部。

**其他故障**

- 出现因“总线错误”（Bus error）导致的core dump或异常退出

  一般情况下出现此种问题，是进程运行过程中加载的共享动态库（在Linux为.so文件）出现变化；或者进程二进制文件本身出现变化，导致操作系统加载机器的执行码或者加载依赖库的入口发生变化，操作系统出于保护目的将进程杀死，产生core dump文件。

  解决此问题，重试便可。同时请尽可能避免在升级等运维操作过程中，在MogDB内部运行业务程序，避免升级时因替换文件产生此问题。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  此故障的core dump文件的可能堆栈是dl_main及其子调用，它是操作系统用来初始化进程做共享动态库加载的。如果进程已经初始化，但是共享动态库还未加载完成，严格意义上来说，进程并未完全启动。
