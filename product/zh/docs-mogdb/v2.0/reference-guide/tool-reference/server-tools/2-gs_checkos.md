---
title: gs_checkos
summary: gs_checkos
author: Zhang Cuiping
date: 2021-06-07
---

# gs_checkos

## 背景信息

gs_checkos工具来帮助检查操作系统、控制参数、磁盘配置等内容，并对系统控制参数、I/O配置、网络配置和THP服务等信息进行配置。

## 前提条件

- 当前的硬件和网络环境正常。
- 各主机间root互信状态正常。
- 只能使用root用户执行gs_checkos命令。

## 语法

- 检查操作系统信息

  ```
  gs_checkos -i ITEM [-f HOSTFILE] [-h HOSTNAME] [-X XMLFILE] [--detail] [-o OUTPUT] [-l LOGFILE]
  ```

- 显示帮助信息

  ```
  gs_checkos -? | --help
  ```

- 显示版本号信息

  ```
  gs_checkos -V | --version
  ```

## 参数说明

- -i

  列表编号。格式：-i A、-i B1、-i A1 -i A2或-i A1,A2。

  取值范围: A1…A14、B1…B8

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  A1…A14 表示只检查操作系统参数，并不设置。 B1…B8 表示将参数系统参数设置为期望值。 A和B不能同时输入。

  详细的检查项请参见[表1](#gs_checkos)。

- -f

  主机名称列表文件。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  -f和-h参数不能同时使用。

  取值范围: 主机名称的列表。

- -h

  指定需要检查的主机名称，可以同时指定多个主机，主机之间使用“,”分割。

  取值范围: MogDB的主机名称。如果不指定主机，则检查当前主机。

- -X

  MogDBXML配置文件。

- -detail

  显示检查结果详情。

- -o

  指定OS检查报告输出到指定的文件。

  不指定则将检查结果输出到屏幕上。

- -l

  指定日志文件及存放路径。

  默认值：/tmp/gs_checkos/gs_checkos-YYYY-MM-DD_hhmmss.log。

- -?, -help

  显示帮助信息。

- -V, -version

  显示版本号信息。

**表 1** 操作系统检查项 <a id="gs_checkos"> </a>

| 编号 | 检查项                                                    | 检查项详细说明                                               |
| :--- | :-------------------------------------------------------- | :----------------------------------------------------------- |
| A1   | 操作系统版本（OS version status）                         | 操作系统版本检查：保证主机平台是支持的SUSE11、SUSE12、RHEL和CentOS平台中的一种。同时保证MogDB各个主机的平台是属于同一混编范围的。 |
| A2   | 内核版本（Kernel version status）                         | 内核版本检查。                                               |
| A3   | Unicode状态（Unicode status）                             | 字符集设置检查：保证MogDB各个主机的字符集一致。              |
| A4   | 时区状态（Time zone status）                              | 时区时间设置检查：保证MogDB各个主机的时区状态一致。          |
| A5   | 交换内存状态（Swap memory status）                        | Swap分区检查：各个主机的swap分区大小必须小于等于各主机内存总大小。 |
| A6   | 系统控制参数（System control parameters status）          | 内核参数检查：保证当前内核中的参数值和默认的期望值保持一致。详细的内核参数请参见**操作系统参数**。 |
| A7   | 文件系统配置状态（File system configuration status）      | 系统文件句柄检查：系统文件句柄的参数值必须大于等于100万。线程堆栈大小检查：线程堆栈大小大于等于3072KB进程最大可用虚拟内存：系统进程最大可用虚拟内存为unlimited。 |
| A8   | 磁盘配置状态（Disk configuration status）                 | 硬盘挂载参数检查：格式为xfs的硬盘，其挂载格式必须为：”rw,noatime,inode64,allocsize=16m”。 |
| A9   | 预读块大小状态（Pre-read block size status）              | 预读参数检查：预读参数期望值为16384。                        |
| A10  | IO调度状态（IO scheduler status）                         | IO调度策略检查：IO调度策略的方式为deadline。                 |
| A11  | 网卡配置状态（Network card configuration status）         | 万兆以下网卡参数检查：确保网卡mtu=1500。万兆及以上网卡参数检查：当网卡为非绑定模式，同时网卡类型为万兆时，确保mtu=1500，rx/tx &gt;=4096。当网卡为绑定模式时，需保证绑定的每个网卡类型均为万兆，然后再确保mtu=1500，rx/tx&gt;=4096。MogDB周边设备（例如交换机等）的网卡也要设置成与MogDB部署的主机一致的值。 |
| A12  | 时间一致性（Time consistency status）                     | 时间一致性检查：确认ntp服务启动，MogDB各个主机的系统时间误差不超过60s。 |
| A13  | 防火墙状态（Firewall service status）                     | 防火墙检查： 确认防火墙的状态为关闭状态。                    |
| A14  | THP服务（THP service status）                             | THP服务检查：确认THP服务状态为关闭状态。                     |
| B1   | 设置系统控制参数（Set system control parameters）         | 内核参数设置：当实际值不满足检查条件期望时，对结果为Abnormal项参数进行修改设置；对Warning项不进行设置，由用户根据实际环境设置。 |
| B2   | 设置文件系统配置值（Set file system configuration value） | 系统文件句柄设置：当系统文件句柄的参数值小于100万时，对其进行修改设置。线程堆栈大小设置：线程堆栈大小小于3072KB时，对其进行设置。进程最大可用虚拟内存设置：系统进程最大可用虚拟内存不为unlimited时对其进行修改设置。 |
| B3   | 设置预读块大小值（Set pre-read block size value）         | 硬盘预读参数设置：当系统的实际值小于16384时，对其进行修改设置。 |
| B4   | 设置IO调度值（Set IO scheduler value）                    | IO配置项的设置：当系统的实际值不为deadline时，对其进行修改设置。 |
| B5   | 设置网卡配置值（Set network card configuration value）    | 万兆网卡参数设置：对不满足万兆网卡的RX、TX参数进行设置，不对MTU参数进行设置。 |
| B6   | 设置THP服务（Set THP service）                            | THP服务设置：如果THP服务启动，将该服务关闭。                 |
| B7   | 设置欧拉系统属性（Set RemoveIPC value）                   | 欧拉系统属性检查：检查设置欧拉系统文件/usr/lib/systemd/system/systemd-logind.service，/etc/systemd/logind.conf中配置项RemoveIPC属性值是否为no，当不为no时，对其进行修改设置。 |
| B8   | 设置sshd服务应用pam模块（Set Session Process）            | 远程设备继承系统默认资源：修改/etc/pam.d/sshd服务文件，添加配置项session required pam_limits.so，来控制用户使用的资源。 |

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
>
> - A6选项检查标准来自配置文件check_list.conf下[/etc/sysctl.conf]、[SUGGEST:/etc/sysctl.conf]域： 若[/etc/sysctl.conf]下参数值与系统实际参数值不同，A6检查提示Abnormal，可以使用B1参数进行设置。 若[SUGGEST:/etc/sysctl.conf]下参数值与系统实际参数值不同，A6检查提示Warning，B1参数不会进行设置，需根据实际情况进行手动设置。
> - A7选项检查系统文件句柄标准来自配置文件check_list.conf下[/etc/security/limits.conf]域open file参数，可以使用B2参数进行设置。
> - A11选项检查万兆网卡参数”MTU”、”RX”、”TX”标准来自配置文件check_list.conf下[/sbin/ifconfig]域，可以使用B5参数对RX、TX进行设置，MTU需用户手动设置。
> - 使用gs_checkos设置的内核参数值和文件句柄参数值，需重启新会话生效。

## 操作系统参数

**表 2** 操作系统参数

| 参数名称                                 | 参数说明                                                     | 推荐取值                     |
| :--------------------------------------- | :----------------------------------------------------------- | :--------------------------- |
| net.ipv4.tcp_max_tw_buckets              | 表示同时保持TIME_WAIT状态的TCP/IP连接最大数量。如果超过所配置的取值，TIME_WAIT将立刻被释放并打印警告信息。 | 10000                        |
| net.ipv4.tcp_tw_reuse                    | 允许将TIME-WAIT状态的sockets重新用于新的TCP连接。0表示关闭。1表示开启。 | 1                            |
| net.ipv4.tcp_tw_recycle                  | 表示开启TCP连接中TIME-WAIT状态sockets的快速回收。0表示关闭。1表示开启。 | 1                            |
| net.ipv4.tcp_keepalive_time              | 表示当keepalive启用的时候，TCP发送keepalive消息的频度。      | 30                           |
| net.ipv4.tcp_keepalive_probes            | 在认定连接失效之前，发送TCP的keepalive探测包数量。这个值乘以tcp_keepalive_intvl之后决定了一个连接发送了keepalive之后可以有多少时间没有回应。 | 9                            |
| net.ipv4.tcp_keepalive_intvl             | 当探测没有确认时，重新发送探测的频度。                       | 30                           |
| net.ipv4.tcp_retries1                    | 在连接建立过程中TCP协议最大重试次数。                        | 5                            |
| net.ipv4.tcp_syn_retries                 | TCP协议SYN报文最大重试次数。                                 | 5                            |
| net.ipv4.tcp_synack_retries              | TCP协议SYN应答报文最大重试次数。                             | 5                            |
| net.sctp.path_max_retrans                | SCTP协议最大重试次数。                                       | 10                           |
| net.sctp.max_init_retransmits            | SCTP协议INIT报文最大重试次数。                               | 10                           |
| net.sctp.association_max_retrans         | SCTP协议单个逻辑连接最大重试次数。                           | 10                           |
| net.sctp.hb_interval                     | SCTP协议心跳检测包重传间隔。                                 | 30000                        |
| net.ipv4.tcp_retries2                    | 控制内核向已经建立连接的远程主机重新发送数据的次数，低值可以更早的检测到与远程主机失效的连接，因此服务器可以更快的释放该连接。发生“connection reset by peer”时可以尝试调大该值规避问题。 | 12                           |
| vm.overcommit_memory                     | 控制在做内存分配的时候，内核的检查方式。0：表示系统会尽量精确计算当前可用的内存。1：表示不作检查直接返回成功。2：内存总量×vm.overcommit_ratio/100＋SWAP的总量，如果申请空间超过此数值则返回失败。内核默认是2过于保守，推荐设置为0，如果系统压力大可以设置为1。 | 0                            |
| net.sctp.sndbuf_policy                   | SCTP发送缓冲区分配原则。0为按连接。1为按耦联。               | 0                            |
| net.sctp.rcvbuf_policy                   | SCTP接收缓冲区分配原则。0为按连接。1为按耦联。               | 0                            |
| net.sctp.sctp_mem                        | 内核SCTP协议栈的最大可用内存，分无压力，有压力，和压力大三个区间，压力大时会丢包，单位为页面。 | 94500000 915000000 927000000 |
| net.sctp.sctp_rmem                       | 内核SCTP协议栈的接收总可用内存，分无压力，有压力和压力大三个区间，压力大时会丢包，单位为页面。 | 8192 250000 16777216         |
| net.sctp.sctp_wmem                       | 内核SCTP协议栈的发送总可用内存，分无压力，有压力和压力大三个区间，压力大时会丢包，单位为页面。 | 8192 250000 16777216         |
| net.ipv4.tcp_rmem                        | TCP协议接收端缓冲区的可用内存大小。分无压力，有压力，和压力大三个区间，单位为页面。 | 8192 250000 16777216         |
| net.ipv4.tcp_wmem                        | TCP协议发送端缓冲区的可用内存大小。分无压力，有压力，和压力大三个区间，单位为页面。 | 8192 250000 16777216         |
| net.core.wmem_max                        | socket发送端缓冲区大小的最大值。                             | 21299200                     |
| net.core.rmem_max                        | socket接收端缓冲区大小的最大值。                             | 21299200                     |
| net.core.wmem_default                    | socket发送端缓冲区大小的默认值。                             | 21299200                     |
| net.core.rmem_default                    | socket接收端缓冲区大小的默认值。                             | 21299200                     |
| net.ipv4.ip_local_port_range             | 物理机可用临时端口范围。                                     | 26000-65535                  |
| kernel.sem                               | 内核信号量参数设置大小。                                     | 250 6400000 1000 25600       |
| vm.min_free_kbytes                       | 保证物理内存有足够空闲空间，防止突发性换页。                 | 系统总内存的5%               |
| net.core.somaxconn                       | 定义了系统中每一个端口最大的监听队列的长度，这是个全局的参数。 | 65535                        |
| net.ipv4.tcp_syncookies                  | 当出现SYN等待队列溢出时，启用cookies来处理，可防范少量SYN攻击。0表示关闭SYN Cookies。1表示开启SYN Cookies。 | 1                            |
| net.sctp.addip_enable                    | SCTP动态地址重置支持开关0表示关闭。1表示开启。               | 0                            |
| net.core.netdev_max_backlog              | 在每个网络接口接收数据包的速率比内核处理这些包的速率快时，允许送到队列的数据包的最大数目。 | 65535                        |
| net.ipv4.tcp_max_syn_backlog             | 记录的那些尚未收到客户端确认信息的连接请求的最大值。         | 65535                        |
| net.ipv4.tcp_fin_timeout                 | 系统默认的超时时间。                                         | 60                           |
| kernel.shmall                            | 内核可用的共享内存总量。                                     | 1152921504606846720          |
| kernel.shmmax                            | 内核参数定义单个共享内存段的最大值。                         | 18446744073709551615         |
| net.ipv4.tcp_sack                        | 启用有选择的应答，通过有选择地应答乱序接受到的报文来提高性能，让发送者只发送丢失的报文段（对于广域网来说）这个选项应该启用，但是会增加对CPU的占用。0表示关闭。1表示开启 | 1                            |
| net.ipv4.tcp_timestamps                  | TCP时间戳（会在TCP包头增加12节），以一种比重发超时更精确的方式（参考RFC 1323）来启用对RTT的计算，启用可以实现更好的性能。0表示关闭。1表示开启 | 1                            |
| vm.extfrag_threshold                     | 系统内存不够用时，linux会为当前系统内存碎片情况打分，如果超过vm.extfrag_threshold的值，kswapd就会触发memory compaction。所以这个值设置的接近1000，说明系统在内存碎片的处理倾向于把旧的页换出，以符合申请的需要，而设置接近0，表示系统在内存碎片的处理倾向做memory compaction。 | 500                          |
| vm.overcommit_ratio                      | 系统使用绝不过量使用内存的算法时，系统整个内存地址空间不得超过swap+RAM值的此参数百分比，当vm.overcommit_memory=2时此参数生效。 | 90                           |
| /sys/module/sctp/parameters/no_checksums | SCTP协议是否关闭checksum。                                   | 0                            |
| MTU                                      | 节点网卡最大传输单元。OS默认值为1500，调整为8192可以提升SCTP协议数据收发的性能。 | 8192                         |

## 文件系统参数

- soft nofile

  说明：soft nofile表示软限制，用户使用的文件句柄数量可以超过该限制，但是如果超过会有告警信息。

  推荐取值：1000000

- hard nofile

  说明：hard nofile表示硬限制，是一个严格的限制，用户使用的文件句柄数量一定不能超过该设置。

  推荐取值：1000000

- stack size

  说明：线程堆栈大小。

  推荐值：3072

## 示例

使用如下命令检查操作系统参数。

```
gs_checkos -i A -h plat1 -X /opt/software/MogDB/clusterconfig.xml --detail -o /var/log/checkos
Performing operation system check/set. Output the result to the file /var/log/checkos.
Operation system check/set is completed.
Total numbers:14. Abnormal numbers:0. Warning number:1.
```

查看操作系统参数检查结果。

```
vim /var/log/checkos
Checking items:
    A1. [ OS version status ]                                   : Normal
    A2. [ MogDB version status ]                               : Normal
    A3. [ Unicode status ]                                      : Normal
    A4. [ Time zone status ]                                    : Normal
    A5. [ Swap memory status ]                                  : Normal
    A6. [ System control parameters status ]                    : Normal
    A7. [ File system configuration status ]                    : Normal
    A8. [ Disk configuration status ]                           : Normal
    A9. [ Pre-read block size status ]                          : Normal
    A10.[ IO scheduler status ]                                 : Normal
    A11.[ Network card configuration status ]                   : Normal
    A12.[ Time consistency status ]                             : Warning
    A13.[ Firewall service status ]                             : Normal
    A14.[ THP service status ]                                  : Normal
```
