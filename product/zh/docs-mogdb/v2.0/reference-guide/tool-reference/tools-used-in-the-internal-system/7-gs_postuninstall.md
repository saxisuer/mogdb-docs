---
title: gs_postuninstall
summary: gs_postuninstall
author: Zhang Cuiping
date: 2021-06-07
---

# gs_postuninstall

## 背景信息

MogDB提供了gs_postuninstall工具来帮助清理准备MogDB环境阶段所做配置，使得卸载后的环境得到清理。

## 前提条件

- MogDB卸载执行成功。
- root用户互信可用。
- 只能使用root用户执行gs_postuninstall命令。

## 语法

- MogDB清理用户、用户组以及虚拟IP清理

  ```
  gs_postuninstall -U USER -X XMLFILE [-L] [--delete-user] [--delete-group] [-l LOGFILE]
  ```

- 显示帮助信息

  ```
  gs_postuninstall -? | --help
  ```

- 显示版本号信息

  ```
  gs_postuninstall -V | --version
  ```

## 参数说明

- -U

  运行MogDB的操作系统用户名。

  取值范围: 字符串，要符合标识符的命名规范。

- -X

  MogDB配置文件路径。

  取值范围: xml文件的存储路径。

- -L

  只清理本主机的环境。

  如果MogDB内某主机做单机环境清理后，MogDB不能再做全量环境清理。

- -delete-user

  删除-U参数指定的操作系统用户。

  如果在Redhat环境下，且用户名与用户组名相同，选择此项必须指定-delete-group参数。

- -delete-group

  删除操作系统用户所在的用户组（选择此选项必须指定-delete-user参数）。

- -l

指定日志文件名及路径。在内部会自动给日志名添加一个时间戳。

当既不明确指定-l，又不在XML文件中配置gaussdbLogPath时，默认值为：“/var/log/gaussdb/om/gs_local-YYYY-MMDD_hhmmss.log”。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:  由于在执行gs_postuninstall后，系统会自动删除MogDB相关目录（包含$GAUSSLOG目录）。因此建议用户通过该参数指定日志文件到非MogDB相关路径。

- -?, -help

  显示帮助信息。

- -V, -version

  显示版本号信息。

## 示例

清理主机的环境

```
gs_postuninstall -U omm  -X /opt/software/MogDB/clusterconfig.xml --delete-user
Parsing the configuration file.
Successfully parsed the configuration file.
Check log file path.
Successfully checked log file path.
Checking unpreinstallation.
Successfully checked unpreinstallation.
Deleting the instance's directory.
Successfully deleted the instance's directory.
Deleting the installation directory.
Successfully deleted the installation directory.
Deleting the temporary directory.
Successfully deleted the temporary directory.
Deleting remote OS user.
Successfully deleted remote OS user.
Deleting software packages and environmental variables of other nodes.
Successfully deleted software packages and environmental variables of other nodes.
Deleting logs of other nodes.
Successfully deleted logs of other nodes.
Deleting software packages and environmental variables of the local node.
Successfully deleted software packages and environmental variables of the local nodes.
Deleting local OS user.
Successfully deleted local OS user.
Deleting local node's logs.
Successfully deleted local node's logs.
Successfully cleaned environment.
```
