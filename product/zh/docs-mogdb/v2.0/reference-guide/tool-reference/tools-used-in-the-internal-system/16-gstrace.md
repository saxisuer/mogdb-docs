---
title: gstrace
summary: gstrace
author: Zhang Cuiping
date: 2021-06-07
---

# gstrace

## 功能介绍

gstrace是MogDB提供的用来跟踪内核代码执行路径，记录内核数据结构，分析代码性能的工具。Trace的有限点位和数据在版本中被固化，无法动态添加和删除。

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-warning.gif) **警告**:

1. 对内核dump指定内存变量的数据用于诊断分析，不存在直接指定任意寄存器或者内存地址的行为。读取的内存地址均是在开发阶段硬编码，没有任意地读取或者修改寄存器或内存的操作。
2. Trace点可能涉及敏感数据，收集trace信息前需要同用户协商，授权和许可后方可收集。
3. MogDB不会在敏感信息上打点，不会TRACE和用户相关的数据。
4. Trace仅用于诊断目的，开启trace将对性能产生一定影响，影响的大小视负载的高低，trace的模块而不同。
5. Trace工具的权限为0700，仅限于数据库用户读、写和执行。

![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:

如果进程异常终止，/dev/shm/ 目录下将会有gstrace_trace_cfg_*残留，可以手动清除。

## 语法

```
gstrace [start|stop|config|dump|detailcodepath|analyze] [-p <port>][-s <BUFFER_SIZE>] [-f <DUMP_FILE>] [-o <OUTPUT_FILE>] [-t <STEP_SIZE>]
```

## 参数说明

**表 1** gs_trace参数说明

| 参数           | 说明                                                         |
| :------------- | :----------------------------------------------------------- |
| start          | 开始记录trace                                                |
| stop           | 停止trace，释放存储信息的共享内存trace buffer。注意：trace buffer中已捕获trace信息会丢失。 |
| config         | 显示trace的配置信息                                          |
| dump           | 将共享内存中的trace信息写入指定文件。若没有启动trace将报错。 |
| detail         | 将dump出来的二进制文件解析成文本文件，显示trace点的线程、时间信息。 |
| codepath       | 提取dump文件中的函数调用信息，按照调用栈的方式显示。         |
| analyze        | 统计各个函数的执行次数、总耗时、平均耗时、最长耗时、最短耗时 |
| -p PID         | 指定启动trace功能的实例进程号                                |
| -f DUMP_FILE   | 指定dump导出的trace文件                                      |
| -o OUTPUT_FILE | 指定写入的文件                                               |
| -t STEP_SIZE   | 指定分片分析的时间跨度（秒）,可选。将生成单独的{OUTPUT_FILE}.step文件。 |
| -s BUFFER_SIZE | 指定用于trace功能的共享内存大小，默认为1G。如果指定的BUFFER_SIZE小于最小值2048，系统将自动调整为最小值。如果指定的BUFFER_SIZE不是2\^N（二的N次方），则向下对齐2\^N；例如：指定BUFFER_SIZE=3072，由于2\^11<3072<2^12，系统将调整为2048。 |

## 示例

1. 启动trace

   ```
   gstrace start -p 207787
   ```

2. 停止trace

   ```
   gstrace stop -p 207787
   ```

3. 查看trace配置

   ```
   gstrace config -p 207787
   ```

4. dump trace

   ```
   gstrace dump -p 207787 -o /data/207787.dump
   ```

5. 解析dump detail信息

   ```
   gstrace detail -f /data/207787.dump -o /data/207787.detail
   ```

6. 解析dump codepath

   ```
   gstrace codepath -f /data/207787.dump -o /data/207787.codepath
   ```

7. 分析全局性能

   ```
   gstrace analyze -f /data/207787.dump -o /data/207787.perf
   ```

8. 分析分片性能

   ```
   gstrace analyze -f /data/207787.dump -o /data/207787.perf -t 1
   ```
