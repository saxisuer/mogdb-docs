---
title: klist
summary: klist
author: Zhang Cuiping
date: 2021-06-07
---

# klist

## 功能介绍

为MogDB认证服务Kerberos提供列出凭证缓存中的用户和票据能力。

## 参数说明

Kerberos工具为开源第三方提供，具体参数说明请参考Kerberos官方文档：<https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>
