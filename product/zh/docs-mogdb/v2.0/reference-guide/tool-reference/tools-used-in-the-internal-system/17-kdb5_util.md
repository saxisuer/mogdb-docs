---
title: kdb5_util
summary: kdb5_util
author: Zhang Cuiping
date: 2021-06-07
---

# kdb5_util

## 功能介绍

为MogDB认证服务Kerberos提供数据库管理操作的工具, 可以新建、销毁、导入和导出数据库。

## 参数说明

Kerberos工具为开源第三方提供，具体参数说明请参考Kerberos官方文档：<https://web.mit.edu/kerberos/krb5-1.17/doc/admin/admin_commands/index.html>
