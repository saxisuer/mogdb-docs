---
title: INFORMATION_SCHEMA_CATALOG_NAME
summary: INFORMATION_SCHEMA_CATALOG_NAME
author: Guo Huan
date: 2021-06-23
---

# INFORMATION_SCHEMA_CATALOG_NAME

用来显示当前所在的database的名称。

**表 1** INFORMATION_SCHEMA_CATALOG_NAME字段

| **名称**     | **类型**                          | **描述**           |
| :----------- | :-------------------------------- | :----------------- |
| catalog_name | information_schema.sql_identifier | 当前database的名称 |
