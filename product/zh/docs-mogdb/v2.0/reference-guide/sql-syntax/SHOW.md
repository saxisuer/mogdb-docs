---
title: SHOW
summary: SHOW
author: Zhang Cuiping
date: 2021-05-18
---

# SHOW

## 功能描述

SHOW将显示当前运行时参数的数值。

## 注意事项

无。

## 语法格式

```ebnf+diagram
Show ::= SHOW
  {
    configuration_parameter |
    CURRENT_SCHEMA |
    TIME ZONE |
    TRANSACTION ISOLATION LEVEL |
    SESSION AUTHORIZATION |
    ALL
  };
```

## 参数说明

显示变量的参数请参见RESET的参数说明。

## 示例

```sql
--显示 timezone 参数值。
mogdb=# SHOW timezone;

--显示所有参数。
mogdb=# SHOW ALL;
```
