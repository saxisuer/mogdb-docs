---
title: DROP DATABASE
summary: DROP DATABASE
author: Zhang Cuiping
date: 2021-05-10
---

# DROP DATABASE

## 功能描述

删除一个数据库。

## 注意事项

- 只有数据库所有者或者被授予了数据库DROP权限的用户有权限执行DROP DATABASE命令，系统管理员默认拥有此权限。
- 不能对系统默认安装的三个数据库（POSTGRES、TEMPLATE0和TEMPLATE1）执行删除操作，系统做了保护。如果想查看当前服务中有哪几个数据库，可以用gsql的\l命令查看。
- 如果有用户正在与要删除的数据库连接，则删除操作失败。
- 不能在事务块中执行DROP DATABASE命令。
- 如果执行DROP DATABASE失败，事务回滚，需要再次执行一次DROP DATABASE IF EXISTS。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: DROP DATABASE一旦执行将无法撤销，请谨慎使用。

## 语法格式

```ebnf+diagram
DropDatabase ::= DROP DATABASE [ IF EXISTS ] database_name ;
```

## 参数说明

- **IF EXISTS**

  如果指定的数据库不存在，则发出一个notice而不是抛出一个错误。

- **database_name**

  要删除的数据库名称。

  取值范围: 字符串，已存在的数据库名称。

## 示例

请参见CREATE DATABASE的示例。

## 相关链接

CREATE DATABASE

## 优化建议

- drop database

  不支持在事务中删除database。
