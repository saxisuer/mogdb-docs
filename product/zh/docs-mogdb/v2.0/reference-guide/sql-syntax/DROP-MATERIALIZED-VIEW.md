---
title: DROP MATERIALIZED VIEW
summary: DROP MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# DROP MATERIALIZED VIEW

## 功能描述

强制删除数据库中已有的物化视图。

## 注意事项

只有物化视图的所有者有权限执行DROP MATERIALIZED VIEW命令，系统管理员默认拥有此权限。

## 语法格式

```ebnf+diagram
DropMaterializedView ::= DROP MATERIALIZED VIEW [ IF EXISTS ] mv_name [, ...] [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的物化视图不存在，则发出一个notice而不是抛出一个错误。

- **mv_name**

  要删除的物化视图名称。

- **CASCADE | RESTRICT**

  - CASCADE：级联删除依赖此物化视图的对象。
  - RESTRICT：如果有依赖对象存在，则拒绝删除此物化视图。此选项为缺省值。

## 示例

```sql
--删除名为my_mv的物化视图。
mogdb=# DROP MATERIALIZED VIEW my_mv;
```
