---
title: ALTER GROUP
summary: ALTER GROUP
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER GROUP

## 功能描述

修改一个用户组的属性。

## 注意事项

ALTER GROUP是ALTER ROLE的别名，非SQL标准语法，不推荐使用，建议用户直接使用ALTER ROLE替代。

## 语法格式

- 向用户组中添加用户。

  ```ebnf+diagram
  AlterGroup ::= ALTER GROUP group_name
      ADD USER user_name [, ... ];
  ```

- 从用户组中删除用户。

  ```ebnf+diagram
  AlterGroup ::= ALTER GROUP group_name
      DROP USER user_name [, ... ];
  ```

- 修改用户组的名称。

  ```ebnf+diagram
  AlterGroup ::= ALTER GROUP group_name
      RENAME TO new_name;
  ```

## 参数说明

请参考ALTER ROLE的参数说明。

## 示例

```sql
--向用户组中添加用户。
mogdb=# ALTER GROUP super_users ADD USER lche, jim;

--从用户组中删除用户。
mogdb=# ALTER GROUP super_users DROP USER jim;

--修改用户组的名称。
mogdb=# ALTER GROUP super_users RENAME TO normal_users;
```
