---
title: ALTER ROLE
summary: ALTER ROLE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER ROLE

## 功能描述

修改角色属性。

## 注意事项

无。

## 语法格式

- 修改角色的权限。

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name [ [ WITH ] option [ ... ] ];
  ```

  其中权限项子句option为：

  ```ebnf+diagram
  option ::= {CREATEDB | NOCREATEDB}
      | {CREATEROLE | NOCREATEROLE}
      | {INHERIT | NOINHERIT}
      | {AUDITADMIN | NOAUDITADMIN}
      | {SYSADMIN | NOSYSADMIN}
      | {USEFT | NOUSEFT}
      | {LOGIN | NOLOGIN}
      | {REPLICATION | NOREPLICATION}
      | {INDEPENDENT | NOINDEPENDENT}
      | {VCADMIN | NOVCADMIN}
      | CONNECTION LIMIT connlimit
      | [ ENCRYPTED | UNENCRYPTED ] PASSWORD 'password' [EXPIRED]
      | [ ENCRYPTED | UNENCRYPTED ] IDENTIFIED BY 'password' [ REPLACE 'old_password' | EXPIRED ]
      | [ ENCRYPTED | UNENCRYPTED ] PASSWORD { 'password' | DISABLE | EXPIRED }
      | [ ENCRYPTED | UNENCRYPTED ] IDENTIFIED BY { 'password' [ REPLACE 'old_password' ] | DISABLE }
      | VALID BEGIN 'timestamp'
      | VALID UNTIL 'timestamp'
      | RESOURCE POOL 'respool'
      | PERM SPACE 'spacelimit'
      | ACCOUNT { LOCK | UNLOCK }
      | PGUSER
  ```

- 修改角色的名称。

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name
      RENAME TO new_name;
  ```

- 设置角色的配置参数。

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name [ IN DATABASE database_name ]
      SET configuration_parameter {{ TO | = } { value | DEFAULT } | FROM CURRENT};
  ```

- 重置角色的配置参数。

  ```ebnf+diagram
  AlterRole ::= ALTER ROLE role_name
      [ IN DATABASE database_name ] RESET {configuration_parameter|ALL};
  ```

## 参数说明

- **role_name**

  现有角色名。

  取值范围: 已存在的用户名。

- **IN DATABASE database_name**

  表示修改角色在指定数据库上的参数。

- **SET configuration_parameter**

  设置角色的参数。ALTER ROLE中修改的会话参数只针对指定的角色，且在下一次该角色启动的会话中有效。

  取值范围:

  configuration_parameter和value的取值请参见**SET**。

  DEFAULT：表示清除configuration_parameter参数的值，configuration_parameter参数的值将继承本角色新产生的SESSION的默认值。

  FROM CURRENT：取当前会话中的值设置为configuration_parameter参数的值。

- **RESET configuration_parameter/ALL**

  清除configuration_parameter参数的值。与SET configuration_parameter TO DEFAULT的效果相同。

  取值范围: ALL表示清除所有参数的值。

- **ACCOUNT LOCK | ACCOUNT UNLOCK**

  - ACCOUNT LOCK：锁定帐户，禁止登录数据库。
  - ACCOUNT UNLOCK：解锁帐户，允许登录数据库。

- **PGUSER**

  当前版本不允许修改角色的PGUSER属性

其他参数请参见CREATE ROLE的参数说明。

## 示例

请参见CREATE ROLE的示例。
