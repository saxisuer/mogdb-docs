---
title: DO
summary: DO
author: Zhang Cuiping
date: 2021-05-10
---

# DO

## 功能描述

执行匿名代码块。

代码块被看做是没有参数的一段函数体，返回值类型是void。它的解析和执行是同一时刻发生的。

## 注意事项

- 程序语言在使用之前，必须通过命令CREATE LANGUAGE安装到当前的数据库中。 plpgsql是默认的安装语言，其它语言安装时必须指定。
- 如果语言是不受信任的，用户必须有使用程序语言的USAGE权限，或者是系统管理员。

## 语法格式

```ebnf+diagram
Do ::= DO [ LANGUAGE lang_name ] code;
```

## 参数说明

- **lang_name**

  用来解析代码的程序语言的名称，如果缺省，默认的语言是plpgsql。

- **code**

  程序语言代码可以被执行的。程序语言必须指定为字符串才行。

## 示例

```sql
--创建用户webuser。
mogdb=# CREATE USER webuser PASSWORD 'xxxxxx';

--授予用户webuser对模式tpcds下视图的所有操作权限。
mogdb=# DO $$DECLARE r record;
BEGIN
    FOR r IN SELECT c.relname table_name,n.nspname table_schema FROM pg_class c,pg_namespace n
             WHERE c.relnamespace = n.oid AND n.nspname = 'tpcds' AND relkind IN ('r','v')
    LOOP
        EXECUTE 'GRANT ALL ON ' || quote_ident(r.table_schema) || '.' || quote_ident(r.table_name) || ' TO webuser';
    END LOOP;
END$$;


--删除用户webuser。
mogdb=# DROP USER webuser CASCADE;
```
