---
title: ALTER DIRECTORY
summary: ALTER DIRECTORY
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER DIRECTORY

## 功能描述

对directory属性进行修改。

## 注意事项

- 目前只支持修改directory属主。
- 属主仅允许是sysadmin权限用户，不允许赋予普通用户。

## 语法格式

```ebnf+diagram
AlterDirectory ::= ALTER DIRECTORY directory_name
    OWNER TO new_owner;
```

## 参数描述

**directory_name**

需要修改的目录名称，范围为已经存在的目录名称。

## 示例

```sql
--创建目录。
mogdb=# CREATE OR REPLACE DIRECTORY  dir  as '/tmp/';

--修改目录的owner。
mogdb=# ALTER DIRECTORY dir OWNER TO system;

--删除目录。
mogdb=# DROP DIRECTORY dir;
```
