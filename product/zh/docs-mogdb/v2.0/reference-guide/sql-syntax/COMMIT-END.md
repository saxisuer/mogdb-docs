---
title: COMMIT | END
summary: COMMIT | END
author: Zhang Cuiping
date: 2021-05-10
---

# COMMIT | END

## 功能描述

通过COMMIT或者END可完成提交事务的功能，即提交事务的所有操作。

## 注意事项

执行COMMIT这个命令的时候，命令执行者必须是该事务的创建者或系统管理员，且创建和提交操作可以不在同一个会话中。

## 语法格式

```ebnf+diagram
CommitEnd ::= { COMMIT | END } [ WORK | TRANSACTION ] ;
```

## 参数说明

- **COMMIT | END**

  提交当前事务，让所有当前事务的更改为其他事务可见。

- **WORK | TRANSACTION**

  可选关键字，除了增加可读性没有其他任何作用。

## 示例

```sql
--创建表。
mogdb=# CREATE TABLE tpcds.customer_demographics_t2
(
    CD_DEMO_SK                INTEGER               NOT NULL,
    CD_GENDER                 CHAR(1)                       ,
    CD_MARITAL_STATUS         CHAR(1)                       ,
    CD_EDUCATION_STATUS       CHAR(20)                      ,
    CD_PURCHASE_ESTIMATE      INTEGER                       ,
    CD_CREDIT_RATING          CHAR(10)                      ,
    CD_DEP_COUNT              INTEGER                       ,
    CD_DEP_EMPLOYED_COUNT     INTEGER                       ,
    CD_DEP_COLLEGE_COUNT      INTEGER
)
WITH (ORIENTATION = COLUMN,COMPRESSION=MIDDLE)
;

--开启事务。
mogdb=# START TRANSACTION;

--插入数据。
mogdb=# INSERT INTO tpcds.customer_demographics_t2 VALUES(1,'M', 'U', 'DOCTOR DEGREE', 1200, 'GOOD', 1, 0, 0);
mogdb=# INSERT INTO tpcds.customer_demographics_t2 VALUES(2,'F', 'U', 'MASTER DEGREE', 300, 'BAD', 1, 0, 0);

--提交事务，让所有更改永久化。
mogdb=# COMMIT;

--查询数据。
mogdb=# SELECT * FROM tpcds.customer_demographics_t2;

--删除表tpcds.customer_demographics_t2。
mogdb=# DROP TABLE tpcds.customer_demographics_t2;
```
