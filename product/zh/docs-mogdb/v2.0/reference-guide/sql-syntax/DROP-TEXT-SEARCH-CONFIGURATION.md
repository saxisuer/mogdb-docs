---
title: DROP TEXT SEARCH CONFIGURATION
summary: DROP TEXT SEARCH CONFIGURATION
author: Zhang Cuiping
date: 2021-05-18
---

# DROP TEXT SEARCH CONFIGURATION

## 功能描述

删除已有文本搜索配置。

## 注意事项

要执行这个命令，用户必须是该配置的所有者。

## 语法格式

```ebnf+diagram
DropTextSearchConfiguration ::= DROP TEXT SEARCH CONFIGURATION [ IF EXISTS ] name [ CASCADE | RESTRICT ];
```

## 参数说明

- **IF EXISTS**

  如果指定的文本搜索配置不存在，那么发出一个notice而不是抛出一个错误。

- **name**

  要删除的文本搜索配置名称（可有模式修饰）。

- **CASCADE**

  级联删除依赖文本搜索配置的对象。

- **RESTRICT**

  若有任何对象依赖文本搜索配置则拒绝删除它。这是默认情况。

## 示例

请参见CREATE TEXT SEARCH CONFIGURATION的示例。
