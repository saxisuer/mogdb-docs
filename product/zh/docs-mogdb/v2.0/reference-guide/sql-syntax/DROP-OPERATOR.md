---
title: DROP OPERATOR
summary: DROP OPERATOR
author: Zhang Cuiping
date: 2021-06-07
---

# DROP OPERATOR

## 功能描述

删除一个操作符。

## 注意事项

DROP OPERATOR语句从数据库中删除一个现存的操作符。 要执行这个命令，你必须是操作符所有者。

## 语法格式

```ebnf+diagram
DropOperator ::= DROP OPERATOR [ IF EXISTS ] name ( { left_type | NONE } , { right_type | NONE } ) [ CASCADE | RESTRICT ]
```

## 参数说明

- **IF EXISTS**

  如果指定的操作符不存在，那么发出一个 notice 而不是抛出一个错误。

- **name**

  一个现存的操作符的名字(可以有模式修饰)。

- **left_type**

  该操作符左操作数的类型。如果没有则写NONE。

- **right_type**

  该操作符右操作数的类型。如果没有则写NONE。

- **CASCADE**

  级联删除依赖于此操作符的所有对象。

- **RESTRICT**

  如果有任何依赖对象则拒绝删除此操作符。这个是缺省项。

## 示例

将用于integer的幂操作符a^b删除：

```sql
DROP OPERATOR ^ (integer, integer);
```

为类型bit删除左单目位操作符~b：

```sql
DROP OPERATOR ~ (none, bit);
```

删除用于bigint的阶乘x!：

```sql
DROP OPERATOR ! (bigint, none);
```

## 兼容性

SQL 标准里没有DROP OPERATOR语句