---
title: COMMIT PREPARED
summary: COMMIT PREPARED
author: Zhang Cuiping
date: 2021-05-10
---

# COMMIT PREPARED

## 功能描述

提交一个早先为两阶段提交准备好的事务。

## 注意事项

- 该功能仅在维护模式(GUC参数xc_maintenance_mode为on时)下可用。该模式谨慎打开，一般供维护人员排查问题使用，一般用户不应使用该模式。
- 命令执行者必须是该事务的创建者或系统管理员，且创建和提交操作可以不在同一个会话中。
- 事务功能由数据库自动维护，不应显式使用事务功能。

## 语法格式

```ebnf+diagram
CommitPrepared ::= COMMIT PREPARED transaction_id ;
```

```ebnf+diagram
CommitPrepared ::= COMMIT PREPARED transaction_id WITH CSN;
```

## 参数说明

- **transaction_id**

  待提交事务的标识符。它不能和任何当前预备事务已经使用了的标识符同名。

- **CSN(commit sequence number)**

  待提交事务的序列号。它是一个64位递增无符号数。

## 示例

```sql
--提交标识符为的trans_test的事务。
mogdb=# COMMIT PREPARED 'trans_test';
```
