---
title: ALTER SEQUENCE
summary: ALTER SEQUENCE
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SEQUENCE

## 功能描述

修改一个现有的序列的参数。

## 注意事项

- 只有序列的所有者或者被授予了序列ALTER权限的用户才能执行ALTER SEQUENCE命令，系统管理员默认拥有该权限。但要修改序列的所有者，当前用户必须是该序列的所有者或者系统管理员，且该用户是新所有者角色的成员。
- 当前版本仅支持修改拥有者、归属列和最大值。若要修改其他参数，可以删除重建，并用Setval函数恢复当前值。
- ALTER SEQUENCE MAXVALUE不支持在事务、函数和存储过程中使用。
- 修改序列的最大值后，会清空该序列在所有会话的cache。
- ALTER SEQUENCE会阻塞nextval、setval、currval和lastval的调用、

## 语法格式

- 修改序列归属列

  ```ebnf+diagram
  AlterSequence ::= ALTER SEQUENCE [ IF EXISTS ] name
      [MAXVALUE maxvalue | NO MAXVALUE | NOMAXVALUE]
      [ OWNED BY { table_name'.'column_name | NONE } ]
  ```

- 修改序列的拥有者

  ```ebnf+diagram
  AlterSequence ::= ALTER SEQUENCE [ IF EXISTS ] name OWNER TO new_owner;
  ```

## 参数说明

- name

  将要修改的序列名称。

- IF EXISTS

  当序列不存在时使用该选项不会出现错误消息，仅有一个通知。

- OWNED BY

  将序列和一个表的指定字段进行关联。这样，在删除那个字段或其所在表的时候会自动删除已关联的序列。

  如果序列已经和表有关联后，使用这个选项后新的关联关系会覆盖旧的关联。

  关联的表和序列的所有者必须是同一个用户，并且在同一个模式中。

  使用OWNED BY NONE将删除任何已经存在的关联。

- new_owner

  序列新所有者的用户名。用户要修改序列的所有者，必须是新角色的直接或者间接成员，并且那个角色必须有序列所在模式上的CREATE权限。

## 示例

```sql
--创建一个名为serial的递增序列，从101开始。
mogdb=# CREATE SEQUENCE serial START 101;

--创建一个表,定义默认值。
mogdb=# CREATE TABLE T1(C1 bigint default nextval('serial'));

--将序列serial的归属列变为T1.C1。
mogdb=# ALTER SEQUENCE serial OWNED BY T1.C1;

--删除序列和表。
mogdb=# DROP SEQUENCE serial cascade;
mogdb=# DROP TABLE T1;
```
