---
title: ABORT
summary: ABORT
author: Zhang Cuiping
date: 2021-05-17
---

# ABORT

## 功能描述

回滚当前事务并且撤销所有当前事务中所做的更改。

作用等同于ROLLBACK，早期SQL有用ABORT，现在推荐使用ROLLBACK。

## 注意事项

在事务外部执行ABORT语句不会影响事务的执行，但是会抛出一个NOTICE信息。

## 语法格式

```ebnf+diagram
Abort ::= ABORT [ WORK | TRANSACTION ];
```

## 参数说明

**WORK | TRANSACTION**

可选关键字，除了增加可读性没有其他任何作用。

## 示例

```sql
--创建表customer_demographics_t1。
mogdb=# CREATE TABLE customer_demographics_t1
(
    CD_DEMO_SK                INTEGER               NOT NULL,
    CD_GENDER                 CHAR(1)                       ,
    CD_MARITAL_STATUS         CHAR(1)                       ,
    CD_EDUCATION_STATUS       CHAR(20)                      ,
    CD_PURCHASE_ESTIMATE      INTEGER                       ,
    CD_CREDIT_RATING          CHAR(10)                      ,
    CD_DEP_COUNT              INTEGER                       ,
    CD_DEP_EMPLOYED_COUNT     INTEGER                       ,
    CD_DEP_COLLEGE_COUNT      INTEGER
)
WITH (ORIENTATION = COLUMN,COMPRESSION=MIDDLE)
;

--插入记录。
mogdb=# INSERT INTO customer_demographics_t1 VALUES(1920801,'M', 'U', 'DOCTOR DEGREE', 200, 'GOOD', 1, 0,0);

--开启事务。
mogdb=# START TRANSACTION;

--更新字段值。
mogdb=# UPDATE customer_demographics_t1 SET cd_education_status= 'Unknown';

--终止事务，上面所执行的更新会被撤销掉。
mogdb=# ABORT;

--查询数据。
mogdb=# SELECT * FROM customer_demographics_t1 WHERE cd_demo_sk = 1920801;
cd_demo_sk | cd_gender | cd_marital_status | cd_education_status  | cd_purchase_estimate | cd_credit_rating | cd_dep_count | cd_dep_employed_count | cd_dep_college_count
------------+-----------+-------------------+----------------------+----------------------+------------------+--------------+-----------------------+----------------------
    1920801 | M         | U                 | DOCTOR DEGREE        |                  200 | GOOD             |            1 |                     0 |                    0
(1 row)

--删除表。
mogdb=# DROP TABLE customer_demographics_t1;
```
