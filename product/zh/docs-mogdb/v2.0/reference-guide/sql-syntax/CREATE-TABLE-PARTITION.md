---
title: CREATE TABLE PARTITION
summary: CREATE TABLE PARTITION
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE TABLE PARTITION

## 功能描述

创建分区表。分区表是把逻辑上的一张表根据某种方案分成几张物理块进行存储，这张逻辑上的表称之为分区表，物理块称之为分区。分区表是一张逻辑表，不存储数据，数据实际是存储在分区上的。

常见的分区方案有范围分区（Range Partitioning）、间隔分区（Interval Partitioning）、哈希分区（Hash Partitioning）、列表分区（List Partitioning）、数值分区（Value Partition）等。目前行存表支持范围分区、间隔分区、哈希分区、列表分区，列存表仅支持范围分区。

范围分区是根据表的一列或者多列，将要插入表的记录分为若干个范围，这些范围在不同的分区里没有重叠。为每个范围创建一个分区，用来存储相应的数据。

范围分区的分区策略是指记录插入分区的方式。目前范围分区仅支持范围分区策略。

范围分区策略：根据分区键值将记录映射到已创建的某个分区上，如果可以映射到已创建的某一分区上，则把记录插入到对应的分区上，否则给出报错和提示信息。这是最常用的分区策略。

间隔分区是一种特殊的范围分区，相比范围分区，新增间隔值定义，当插入记录找不到匹配的分区时，可以根据间隔值自动创建分区。

间隔分区只支持基于表的一列分区，并且该列只支持TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE数据类型。

间隔分区策略：根据分区键值将记录映射到已创建的某个分区上，如果可以映射到已创建的某一分区上，则把记录插入到对应的分区上，否则根据分区键值和表定义信息自动创建一个分区，然后将记录插入新分区中，新创建的分区数据范围等于间隔值。

哈希分区是根据表的一列，为每个分区指定模数和余数，将要插入表的记录划分到对应的分区中，每个分区所持有的行都需要满足条件：分区键的值除以为其指定的模数将产生为其指定的余数。

哈希分区策：根据分区键值将记录映射到已创建的某个分区上，如果可以映射到已创建的某一分区上，则把记录插入到对应的分区上，否则返回报错和提示信息。

列表分区是根据表的一列，将要插入表的记录通过每一个分区中出现的键值划分到对应的分区中，这些键值在不同的分区里没有重叠。为每组键值创建一个分区，用来存储相应的数据。

列表分区策略：根据分区键值将记录映射到已创建的某个分区上，如果可以映射到已创建的某一分区上，则把记录插入到对应的分区上，否则给出报错和提示信息。

分区可以提供若干好处：

- 某些类型的查询性能可以得到极大提升。特别是表中访问率较高的行位于一个单独分区或少数几个分区上的情况下。分区可以减少数据的搜索空间，提高数据访问效率。
- 当查询或更新一个分区的大部分记录时，连续扫描那个分区而不是访问整个表可以获得巨大的性能提升。
- 如果需要大量加载或者删除的记录位于单独的分区上，则可以通过直接读取或删除那个分区以获得巨大的性能提升，同时还可以避免由于大量DELETE导致的VACUUM超载（仅范围分区）。

## 注意事项

- 有限地支持唯一约束和主键约束，即唯一约束和主键约束的约束键必须包含所有分区键。
- 目前哈希分区和列表分区仅支持单列构建分区键，暂不支持多列构建分区键。
- 只需要有间隔分区表的INSERT权限，往该表INSERT数据时就可以自动创建分区。

## 语法格式

```ebnf+diagram
CreateTablePartition ::= CREATE TABLE [ IF NOT EXISTS ] partition_table_name
( [
    { column_name data_type [ COLLATE collation ] [ column_constraint [ ... ] ]
    | table_constraint
    | LIKE source_table [ like_option [...] ] }[, ... ]
] )
    [ WITH ( {storage_parameter = value} [, ... ] ) ]
    [ COMPRESS | NOCOMPRESS ]
    [ TABLESPACE tablespace_name ]
     PARTITION BY {
        {RANGE (partition_key) [ INTERVAL ('interval_expr') [ STORE IN (tablespace_name [, ... ] ) ] ] ( partition_less_than_item [, ... ] )} |
        {RANGE (partition_key) [ INTERVAL ('interval_expr') [ STORE IN (tablespace_name [, ... ] ) ] ] ( partition_start_end_item [, ... ] )} |
        {LIST | HASH (partition_key) (PARTITION partition_name [VALUES (list_values_clause)] opt_table_space )}
    } [ { ENABLE | DISABLE } ROW MOVEMENT ];
```

- 列约束column_constraint：

  ```ebnf+diagram
  column_constraint ::= [ CONSTRAINT constraint_name ]
  { NOT NULL |
    NULL |
    CHECK ( expression ) |
    DEFAULT default_expr |
    UNIQUE index_parameters |
    PRIMARY KEY index_parameters |
    REFERENCES reftable [ ( refcolumn ) ] [ MATCH FULL | MATCH PARTIAL | MATCH SIMPLE ]
        [ ON DELETE action ] [ ON UPDATE action ] }
  [ DEFERRABLE | NOT DEFERRABLE | INITIALLY DEFERRED | INITIALLY IMMEDIATE ]
  ```

- 表约束table_constraint：

  ```ebnf+diagram
  table_constraint ::= [ CONSTRAINT constraint_name ]
  { CHECK ( expression ) |
    UNIQUE ( column_name [, ... ] ) index_parameters |
    PRIMARY KEY ( column_name [, ... ] ) index_parameters |
    FOREIGN KEY ( column_name [, ... ] ) REFERENCES reftable [ ( refcolumn [, ... ] ) ]
        [ MATCH FULL | MATCH PARTIAL | MATCH SIMPLE ] [ ON DELETE action ] [ ON UPDATE action ] }
  [ DEFERRABLE | NOT DEFERRABLE | INITIALLY DEFERRED | INITIALLY IMMEDIATE ]
  ```

- like选项like_option：

  ```ebnf+diagram
  like_option ::= { INCLUDING | EXCLUDING } { DEFAULTS | CONSTRAINTS | INDEXES | STORAGE | COMMENTS | RELOPTIONS| ALL }
  ```

- 索引存储参数index_parameters：

  ```ebnf+diagram
  index_parameters ::= [ WITH ( {storage_parameter = value} [, ... ] ) ]
  [ USING INDEX TABLESPACE tablespace_name ]
  ```

- partition_less_than_item：

  ```ebnf+diagram
  partition_less_than_item ::= PARTITION partition_name VALUES LESS THAN ( { partition_value | MAXVALUE } ) [TABLESPACE tablespace_name]
  ```

- partition_start_end_item：

  ```ebnf+diagram
  partition_start_end_item ::= PARTITION partition_name {
          {START(partition_value) END (partition_value) EVERY (interval_value)} |
          {START(partition_value) END ({partition_value | MAXVALUE})} |
          {START(partition_value)} |
          {END({partition_value | MAXVALUE})}
  } [TABLESPACE tablespace_name]
  ```

## 参数说明

- **IF NOT EXISTS**

  如果已经存在相同名称的表，不会抛出一个错误，而会发出一个通知，告知表关系已存在。

- **partition_table_name**

  分区表的名称。

  取值范围: 字符串，要符合标识符的命名规范。

- **column_name**

  新表中要创建的字段名。

  取值范围: 字符串，要符合标识符的命名规范。

- **data_type**

  字段的数据类型。

- **COLLATE collation**

  COLLATE子句指定列的排序规则（该列必须是可排列的数据类型）。如果没有指定，则使用默认的排序规则。排序规则可以使用“select * from pg_collation;”命令从pg_collation系统表中查询，默认的排序规则为查询结果中以default开始的行。

- **CONSTRAINT constraint_name**

    列约束或表约束的名称。可选的约束子句用于声明约束，新行或者更新的行必须满足这些约束才能成功插入或更新。

    定义约束有两种方法：

  - 列约束：作为一个列定义的一部分，仅影响该列。
  - 表约束：不和某个列绑在一起，可以作用于多个列。

- **LIKE source_table [ like_option … ]**

    LIKE子句声明一个表，新表自动从这个表里面继承所有字段名及其数据类型和非空约束。

    和INHERITS不同，新表与原来的表之间在创建动作完毕之后是完全无关的。在源表做的任何修改都不会传播到新表中，并且也不可能在扫描源表的时候包含新表的数据。

    字段缺省表达式只有在声明了INCLUDING DEFAULTS之后才会包含进来。缺省是不包含缺省表达式的，即新表中所有字段的缺省值都是NULL。

    非空约束将总是复制到新表中，CHECK约束则仅在指定了INCLUDING CONSTRAINTS的时候才复制，而其他类型的约束则永远也不会被复制。此规则同时适用于表约束和列约束。

    和INHERITS不同，被复制的列和约束并不使用相同的名称进行融合。如果明确的指定了相同的名称或者在另外一个LIKE子句中，将会报错。

  - 如果指定了INCLUDING INDEXES，则源表上的索引也将在新表上创建，默认不建立索引。
  - 如果指定了INCLUDING STORAGE，则拷贝列的STORAGE设置也将被拷贝，默认情况下不包含STORAGE设置。
  - 如果指定了INCLUDING COMMENTS，则源表列、约束和索引的注释也会被拷贝过来。默认情况下，不拷贝源表的注释。
  - 如果指定了INCLUDING RELOPTIONS，则源表的存储参数（即源表的WITH子句）也将拷贝至新表。默认情况下，不拷贝源表的存储参数。
  - INCLUDING ALL包含了INCLUDING DEFAULTS、INCLUDING CONSTRAINTS、INCLUDING INDEXES、INCLUDING STORAGE、INCLUDING COMMENTS、INCLUDING PARTITION和INCLUDING RELOPTIONS的内容。

- **WITH ( storage_parameter [= value] [, … ] )**

    这个子句为表或索引指定一个可选的存储参数。参数的详细描述如下所示：

  - FILLFACTOR

    一个表的填充因子（fillfactor）是一个介于10和100之间的百分数。100（完全填充）是默认值。如果指定了较小的填充因子，INSERT操作仅按照填充因子指定的百分率填充表页。每个页上的剩余空间将用于在该页上更新行，这就使得UPDATE有机会在同一页上放置同一条记录的新版本，这比把新版本放置在其他页上更有效。对于一个从不更新的表将填充因子设为100是最佳选择，但是对于频繁更新的表，选择较小的填充因子则更加合适。该参数对于列存表没有意义。

    取值范围: 10~100

  - ORIENTATION

    决定了表的数据的存储方式。

    取值范围:

    - COLUMN：表的数据将以列式存储。

    - ROW（缺省值）：表的数据将以行式存储。

    > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: orientation不支持修改。

  - COMPRESSION

    - 列存表的有效值为LOW/MIDDLE/HIGH/YES/NO，压缩级别依次升高，默认值为LOW。
    - 行存表不支持压缩。

  - MAX_BATCHROW

    指定了在数据加载过程中一个存储单元可以容纳记录的最大数目。该参数只对列存表有效。

    取值范围: 10000~60000，默认60000。

  - PARTIAL_CLUSTER_ROWS

    指定了在数据加载过程中进行将局部聚簇存储的记录数目。该参数只对列存表有效。

    取值范围: 大于等于MAX_BATCHROW，建议取值为MAX_BATCHROW的整数倍数。

  - DELTAROW_THRESHOLD

    预留参数。该参数只对列存表有效。

    取值范围: 0～9999

- **COMPRESS / NOCOMPRESS**

  创建一个新表时，需要在创建表语句中指定关键字COMPRESS，这样，当对该表进行批量插入时就会触发压缩特性。该特性会在页范围内扫描所有元组数据，生成字典、压缩元组数据并进行存储。指定关键字NOCOMPRESS则不对表进行压缩。行存表不支持压缩。

  缺省值为NOCOMPRESS，即不对元组数据进行压缩。

- **TABLESPACE tablespace_name**

  指定新表将要在tablespace_name表空间内创建。如果没有声明，将使用默认表空间。

- **PARTITION BY RANGE(partition_key)**

  创建范围分区。partition_key为分区键的名称。

  （1）对于从句是VALUES LESS THAN的语法格式：

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 对于从句是VALUE LESS THAN的语法格式，范围分区策略的分区键最多支持4列。

  该情形下，分区键支持的数据类型为：SMALLINT、INTEGER、BIGINT、DECIMAL、NUMERIC、REAL、DOUBLE PRECISION、CHARACTER VARYING(n)、VARCHAR(n)、CHARACTER(n)、CHAR(n)、CHARACTER、CHAR、TEXT、NVARCHAR2、NAME、TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE。

  （2）对于从句是START END的语法格式：

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 对于从句是START END的语法格式，范围分区策略的分区键仅支持1列。

  该情形下，分区键支持的数据类型为：SMALLINT、INTEGER、BIGINT、DECIMAL、NUMERIC、REAL、DOUBLE PRECISION、TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE。

  （3）对于指定了INTERVAL子句的语法格式：

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 对于指定了INTERVAL子句的语法格式，范围分区策略的分区键仅支持1列。

  该情形下，分区键支持的数据类型为：TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE。

- **PARTITION partition_name VALUES LESS THAN ( { partition_value | MAXVALUE } )**

  指定各分区的信息。partition_name为范围分区的名称。partition_value为范围分区的上边界，取值依赖于partition_key的类型。MAXVALUE表示分区的上边界，它通常用于设置最后一个范围分区的上边界。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
  >
  > - 每个分区都需要指定一个上边界。
  > - 分区上边界的类型应当和分区键的类型一致。
  > - 分区列表是按照分区上边界升序排列的，值较小的分区位于值较大的分区之前。

- **PARTITION partition_name {START (partition_value) END (partition_value) EVERY (interval_value)} |** {START (partition_value) END (partition_value|MAXVALUE)} | {START(partition_value)} | **{END (partition_value | MAXVALUE)**}

  指定各分区的信息，各参数意义如下：

  - partition_name：范围分区的名称或名称前缀，除以下情形外（假定其中的partition_name是p1），均为分区的名称。
    - 若该定义是START+END+EVERY从句，则语义上定义的分区的名称依次为p1_1, p1_2, …。例如对于定义“PARTITION p1 START(1) END(4) EVERY(1)”，则生成的分区是：[1, 2), [2, 3) 和 [3, 4)，名称依次为p1_1, p1_2和p1_3，即此处的p1是名称前缀。
    - 若该定义是第一个分区定义，且该定义有START值，则范围（MINVALUE, START）将自动作为第一个实际分区，其名称为p1_0，然后该定义语义描述的分区名称依次为p1_1, p1_2, …。例如对于完整定义“PARTITION p1 START(1), PARTITION p2 START(2)”，则生成的分区是：(MINVALUE, 1), [1, 2) 和 [2, MAXVALUE)，其名称依次为p1_0, p1_1和p2，即此处p1是名称前缀，p2是分区名称。这里MINVALUE表示最小值。
  - partition_value：范围分区的端点值（起始或终点），取值依赖于partition_key的类型，不可是MAXVALUE。
  - interval_value：对[START，END) 表示的范围进行切分，interval_value是指定切分后每个分区的宽度，不可是MAXVALUE；如果（END-START）值不能整除以EVERY值，则仅最后一个分区的宽度小于EVERY值。
  - MAXVALUE：表示最大值，它通常用于设置最后一个范围分区的上边界。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
  >
  > 1. 在创建分区表若第一个分区定义含START值，则范围（MINVALUE，START）将自动作为实际的第一个分区。
  > 2. START END语法需要遵循以下限制：
  >    - 每个partition_start_end_item中的START值（如果有的话，下同）必须小于其END值；
  >    - 相邻的两个partition_start_end_item，第一个的END值必须等于第二个的START值； - 每个partition_start_end_item中的EVERY值必须是正向递增的，且必须小于（END-START）值；
  >    - 每个分区包含起始值，不包含终点值，即形如：[起始值，终点值)，起始值是MINVALUE时则不包含；
  >    - 一个partition_start_end_item创建的每个分区所属的TABLESPACE一样；
  >    - partition_name作为分区名称前缀时，其长度不要超过57字节，超过时自动截断；
  >    - 在创建、修改分区表时请注意分区表的分区总数不可超过最大限制（32767）；
  > 3. 在创建分区表时START END与LESS THAN语法不可混合使用。
  > 4. 即使创建分区表时使用START END语法，备份（gs_dump）出的SQL语句也是VALUES LESS THAN语法格式。

- **INTERVAL ('interval_expr') [ STORE IN (tablespace_name [, … ] ) ]**

  间隔分区定义信息。

  - interval_expr：自动创建分区的间隔，例如：1 day、1 month。
  - STORE IN (tablespace_name [, … ] )：指定存放自动创建分区的表空间列表，如果有指定，则自动创建的分区从表空间列表中循环选择使用，否则使用分区表默认的表空间。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 列存表不支持间隔分区。

- **PARTITION BY LIST(partition_key)**

  创建列表分区。partition_key为分区键的名称。

  - 对于partition_key，列表分区策略的分区键仅支持1列。
  - 对于从句是VALUES (list_values_clause)的语法格式，list_values_clause中包含了对应分区存在的键值，推荐每个分区的键值数量不超过64个。

  分区键支持的数据类型为：INT1、INT2、INT4、INT8、NUMERIC、VARCHAR(n)、CHAR、BPCHAR、NVARCHAR2、TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE。分区个数不能超过64个。

- **PARTITION BY HASH(partition_key)**

  创建哈希分区。partition_key为分区键的名称。

  对于partition_key，哈希分区策略的分区键仅支持1列。

  分区键支持的数据类型为：INT1、INT2、INT4、INT8、NUMERIC、VARCHAR(n)、CHAR、BPCHAR、TEXT、NVARCHAR2、TIMESTAMP[(p)] [WITHOUT TIME ZONE]、TIMESTAMP[(p)] [WITH TIME ZONE]、DATE。分区个数不能超过64个。

- **{ ENABLE | DISABLE } ROW MOVEMENT**

  行迁移开关。

  如果进行UPDATE操作时，更新了元组在分区键上的值，造成了该元组所在分区发生变化，就会根据该开关给出报错信息，或者进行元组在分区间的转移。

  取值范围:

  - ENABLE（缺省值）：行迁移开关打开。
  - DISABLE：行迁移开关关闭。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**: 列表/哈希分区表暂不支持ROW MOVEMENT。

- **NOT NULL**

  字段值不允许为NULL。ENABLE用于语法兼容，可省略。

- **NULL**

  字段值允许NULL ，这是缺省。

  这个子句只是为和非标准SQL数据库兼容。不建议使用。

- **CHECK (condition) [ NO INHERIT ]**

  CHECK约束声明一个布尔表达式，每次要插入的新行或者要更新的行的新值必须使表达式结果为真或未知才能成功，否则会抛出一个异常并且不会修改数据库。

  声明为字段约束的检查约束应该只引用该字段的数值，而在表约束里出现的表达式可以引用多个字段。

  用NO INHERIT标记的约束将不会传递到子表中去。

  ENABLE用于语法兼容，可省略。

- **DEFAULT default_expr**

  DEFAULT子句给字段指定缺省值。该数值可以是任何不含变量的表达式(不允许使用子查询和对本表中的其他字段的交叉引用)。缺省表达式的数据类型必须和字段类型匹配。

  缺省表达式将被用于任何未声明该字段数值的插入操作。如果没有指定缺省值则缺省值为NULL 。

- **UNIQUE index_parameters**

  **UNIQUE ( column_name [, … ] ) index_parameters**

  UNIQUE约束表示表里的一个字段或多个字段的组合必须在全表范围内唯一。

  对于唯一约束，NULL被认为是互不相等的。

- **PRIMARY KEY index_parameters**

  **PRIMARY KEY ( column_name [, … ] ) index_parameters**

  主键约束声明表中的一个或者多个字段只能包含唯一的非NULL值。

  一个表只能声明一个主键。

- **DEFERRABLE | NOT DEFERRABLE**

  这两个关键字设置该约束是否可推迟。一个不可推迟的约束将在每条命令之后马上检查。可推迟约束可以推迟到事务结尾使用SET CONSTRAINTS命令检查。缺省是NOT DEFERRABLE。目前，UNIQUE约束和主键约束可以接受这个子句。所有其他约束类型都是不可推迟的。

- **INITIALLY IMMEDIATE | INITIALLY DEFERRED**

  如果约束是可推迟的，则这个子句声明检查约束的缺省时间。

  - 如果约束是INITIALLY IMMEDIATE（缺省），则在每条语句执行之后就立即检查它；
  - 如果约束是INITIALLY DEFERRED ，则只有在事务结尾才检查它。

  约束检查的时间可以用SET CONSTRAINTS命令修改。

- **USING INDEX TABLESPACE tablespace_name**

  为UNIQUE或PRIMARY KEY约束相关的索引声明一个表空间。如果没有提供这个子句，这个索引将在default_tablespace中创建，如果default_tablespace为空，将使用数据库的缺省表空间。

## 示例

- 示例1：创建范围分区表tpcds.web_returns_p1，含有8个分区，分区键为integer类型。 分区的范围分别为：wr_returned_date_sk&lt; 2450815，2450815&lt;= wr_returned_date_sk&lt; 2451179，2451179&lt;=wr_returned_date_sk&lt; 2451544，2451544 &lt;= wr_returned_date_sk&lt; 2451910，2451910 &lt;= wr_returned_date_sk&lt; 2452275，2452275 &lt;= wr_returned_date_sk&lt; 2452640，2452640 &lt;= wr_returned_date_sk&lt; 2453005，wr_returned_date_sk&gt;=2453005。

  ```sql
  --创建表tpcds.web_returns。
  mogdb=# CREATE TABLE tpcds.web_returns
  (
      W_WAREHOUSE_SK            INTEGER               NOT NULL,
      W_WAREHOUSE_ID            CHAR(16)              NOT NULL,
      W_WAREHOUSE_NAME          VARCHAR(20)                   ,
      W_WAREHOUSE_SQ_FT         INTEGER                       ,
      W_STREET_NUMBER           CHAR(10)                      ,
      W_STREET_NAME             VARCHAR(60)                   ,
      W_STREET_TYPE             CHAR(15)                      ,
      W_SUITE_NUMBER            CHAR(10)                      ,
      W_CITY                    VARCHAR(60)                   ,
      W_COUNTY                  VARCHAR(30)                   ,
      W_STATE                   CHAR(2)                       ,
      W_ZIP                     CHAR(10)                      ,
      W_COUNTRY                 VARCHAR(20)                   ,
      W_GMT_OFFSET              DECIMAL(5,2)
  );
  --创建分区表tpcds.web_returns_p1。
  mogdb=# CREATE TABLE tpcds.web_returns_p1
  (
      WR_RETURNED_DATE_SK       INTEGER                       ,
      WR_RETURNED_TIME_SK       INTEGER                       ,
      WR_ITEM_SK                INTEGER               NOT NULL,
      WR_REFUNDED_CUSTOMER_SK   INTEGER                       ,
      WR_REFUNDED_CDEMO_SK      INTEGER                       ,
      WR_REFUNDED_HDEMO_SK      INTEGER                       ,
      WR_REFUNDED_ADDR_SK       INTEGER                       ,
      WR_RETURNING_CUSTOMER_SK  INTEGER                       ,
      WR_RETURNING_CDEMO_SK     INTEGER                       ,
      WR_RETURNING_HDEMO_SK     INTEGER                       ,
      WR_RETURNING_ADDR_SK      INTEGER                       ,
      WR_WEB_PAGE_SK            INTEGER                       ,
      WR_REASON_SK              INTEGER                       ,
      WR_ORDER_NUMBER           BIGINT                NOT NULL,
      WR_RETURN_QUANTITY        INTEGER                       ,
      WR_RETURN_AMT             DECIMAL(7,2)                  ,
      WR_RETURN_TAX             DECIMAL(7,2)                  ,
      WR_RETURN_AMT_INC_TAX     DECIMAL(7,2)                  ,
      WR_FEE                    DECIMAL(7,2)                  ,
      WR_RETURN_SHIP_COST       DECIMAL(7,2)                  ,
      WR_REFUNDED_CASH          DECIMAL(7,2)                  ,
      WR_REVERSED_CHARGE        DECIMAL(7,2)                  ,
      WR_ACCOUNT_CREDIT         DECIMAL(7,2)                  ,
      WR_NET_LOSS               DECIMAL(7,2)
  )
  WITH (ORIENTATION = COLUMN,COMPRESSION=MIDDLE)
  PARTITION BY RANGE(WR_RETURNED_DATE_SK)
  (
          PARTITION P1 VALUES LESS THAN(2450815),
          PARTITION P2 VALUES LESS THAN(2451179),
          PARTITION P3 VALUES LESS THAN(2451544),
          PARTITION P4 VALUES LESS THAN(2451910),
          PARTITION P5 VALUES LESS THAN(2452275),
          PARTITION P6 VALUES LESS THAN(2452640),
          PARTITION P7 VALUES LESS THAN(2453005),
          PARTITION P8 VALUES LESS THAN(MAXVALUE)
  );

  --从示例数据表导入数据。
  mogdb=# INSERT INTO tpcds.web_returns_p1 SELECT * FROM tpcds.web_returns;

  --删除分区P8。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 DROP PARTITION P8;

  --增加分区WR_RETURNED_DATE_SK介于2453005和2453105之间。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 ADD PARTITION P8 VALUES LESS THAN (2453105);

  --增加分区WR_RETURNED_DATE_SK介于2453105和MAXVALUE之间。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 ADD PARTITION P9 VALUES LESS THAN (MAXVALUE);

  --删除分区P8。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 DROP PARTITION FOR (2453005);

  --分区P7重命名为P10。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 RENAME PARTITION P7 TO P10;

  --分区P6重命名为P11。
  mogdb=# ALTER TABLE tpcds.web_returns_p1 RENAME PARTITION FOR (2452639) TO P11;

  --查询分区P10的行数。
  mogdb=# SELECT count(*) FROM tpcds.web_returns_p1 PARTITION (P10);
   count
  --------
   0
  (1 row)

  --查询分区P1的行数。
  mogdb=# SELECT COUNT(*) FROM tpcds.web_returns_p1 PARTITION FOR (2450815);
   count
  --------
   0
  (1 row)
  ```

- 示例2：创建范围分区表tpcds.web_returns_p2，含有8个分区，分区键类型为integer类型，其中第8个分区上边界为MAXVALUE。

  八个分区的范围分别为： wr_returned_date_sk&lt; 2450815，2450815&lt;= wr_returned_date_sk&lt; 2451179，2451179&lt;=wr_returned_date_sk&lt; 2451544，2451544 &lt;= wr_returned_date_sk&lt; 2451910，2451910 &lt;= wr_returned_date_sk&lt; 2452275，2452275 &lt;= wr_returned_date_sk&lt; 2452640，2452640 &lt;= wr_returned_date_sk&lt; 2453005，wr_returned_date_sk&gt;=2453005。

  分区表tpcds.web_returns_p2的表空间为example1；分区P1到P7没有声明表空间，使用采用分区表tpcds.web_returns_p2的表空间example1；指定分区P8的表空间为example2。

  假定数据库节点的数据目录/pg_location/mount1/path1，数据库节点的数据目录/pg_location/mount2/path2，数据库节点的数据目录/pg_location/mount3/path3，数据库节点的数据目录/pg_location/mount4/path4是dwsadmin用户拥有读写权限的空目录。

  ```sql
  mogdb=# CREATE TABLESPACE example1 RELATIVE LOCATION 'tablespace1/tablespace_1';
  mogdb=# CREATE TABLESPACE example2 RELATIVE LOCATION 'tablespace2/tablespace_2';
  mogdb=# CREATE TABLESPACE example3 RELATIVE LOCATION 'tablespace3/tablespace_3';
  mogdb=# CREATE TABLESPACE example4 RELATIVE LOCATION 'tablespace4/tablespace_4';

  mogdb=# CREATE TABLE tpcds.web_returns_p2
  (
      WR_RETURNED_DATE_SK       INTEGER                       ,
      WR_RETURNED_TIME_SK       INTEGER                       ,
      WR_ITEM_SK                INTEGER               NOT NULL,
      WR_REFUNDED_CUSTOMER_SK   INTEGER                       ,
      WR_REFUNDED_CDEMO_SK      INTEGER                       ,
      WR_REFUNDED_HDEMO_SK      INTEGER                       ,
      WR_REFUNDED_ADDR_SK       INTEGER                       ,
      WR_RETURNING_CUSTOMER_SK  INTEGER                       ,
      WR_RETURNING_CDEMO_SK     INTEGER                       ,
      WR_RETURNING_HDEMO_SK     INTEGER                       ,
      WR_RETURNING_ADDR_SK      INTEGER                       ,
      WR_WEB_PAGE_SK            INTEGER                       ,
      WR_REASON_SK              INTEGER                       ,
      WR_ORDER_NUMBER           BIGINT                NOT NULL,
      WR_RETURN_QUANTITY        INTEGER                       ,
      WR_RETURN_AMT             DECIMAL(7,2)                  ,
      WR_RETURN_TAX             DECIMAL(7,2)                  ,
      WR_RETURN_AMT_INC_TAX     DECIMAL(7,2)                  ,
      WR_FEE                    DECIMAL(7,2)                  ,
      WR_RETURN_SHIP_COST       DECIMAL(7,2)                  ,
      WR_REFUNDED_CASH          DECIMAL(7,2)                  ,
      WR_REVERSED_CHARGE        DECIMAL(7,2)                  ,
      WR_ACCOUNT_CREDIT         DECIMAL(7,2)                  ,
      WR_NET_LOSS               DECIMAL(7,2)
  )
  TABLESPACE example1
  PARTITION BY RANGE(WR_RETURNED_DATE_SK)
  (
          PARTITION P1 VALUES LESS THAN(2450815),
          PARTITION P2 VALUES LESS THAN(2451179),
          PARTITION P3 VALUES LESS THAN(2451544),
          PARTITION P4 VALUES LESS THAN(2451910),
          PARTITION P5 VALUES LESS THAN(2452275),
          PARTITION P6 VALUES LESS THAN(2452640),
          PARTITION P7 VALUES LESS THAN(2453005),
          PARTITION P8 VALUES LESS THAN(MAXVALUE) TABLESPACE example2
  )
  ENABLE ROW MOVEMENT;

  --以like方式创建一个分区表。
  mogdb=# CREATE TABLE tpcds.web_returns_p3 (LIKE tpcds.web_returns_p2 INCLUDING PARTITION);

  --修改分区P1的表空间为example2。
  mogdb=# ALTER TABLE tpcds.web_returns_p2 MOVE PARTITION P1 TABLESPACE example2;

  --修改分区P2的表空间为example3。
  mogdb=# ALTER TABLE tpcds.web_returns_p2 MOVE PARTITION P2 TABLESPACE example3;

  --以2453010为分割点切分P8。
  mogdb=# ALTER TABLE tpcds.web_returns_p2 SPLIT PARTITION P8 AT (2453010) INTO
  (
          PARTITION P9,
          PARTITION P10
  );

  --将P6，P7合并为一个分区。
  mogdb=# ALTER TABLE tpcds.web_returns_p2 MERGE PARTITIONS P6, P7 INTO PARTITION P8;

  --修改分区表迁移属性。
  mogdb=# ALTER TABLE tpcds.web_returns_p2 DISABLE ROW MOVEMENT;
  --删除表和表空间。
  mogdb=# DROP TABLE tpcds.web_returns_p1;
  mogdb=# DROP TABLE tpcds.web_returns_p2;
  mogdb=# DROP TABLE tpcds.web_returns_p3;
  mogdb=# DROP TABLESPACE example1;
  mogdb=# DROP TABLESPACE example2;
  mogdb=# DROP TABLESPACE example3;
  mogdb=# DROP TABLESPACE example4;
  ```

- 示例3：START END语法创建、修改Range分区表。

  假定/home/omm/startend_tbs1，/home/omm/startend_tbs2，/home/omm/startend_tbs3，/home/omm/startend_tbs4是omm用户拥有读写权限的空目录。

  ```sql
  -- 创建表空间
  mogdb=# CREATE TABLESPACE startend_tbs1 LOCATION '/home/omm/startend_tbs1';
  mogdb=# CREATE TABLESPACE startend_tbs2 LOCATION '/home/omm/startend_tbs2';
  mogdb=# CREATE TABLESPACE startend_tbs3 LOCATION '/home/omm/startend_tbs3';
  mogdb=# CREATE TABLESPACE startend_tbs4 LOCATION '/home/omm/startend_tbs4';

  -- 创建临时schema
  mogdb=# CREATE SCHEMA tpcds;
  mogdb=# SET CURRENT_SCHEMA TO tpcds;

  -- 创建分区表，分区键是integer类型
  mogdb=# CREATE TABLE tpcds.startend_pt (c1 INT, c2 INT)
  TABLESPACE startend_tbs1
  PARTITION BY RANGE (c2) (
      PARTITION p1 START(1) END(1000) EVERY(200) TABLESPACE startend_tbs2,
      PARTITION p2 END(2000),
      PARTITION p3 START(2000) END(2500) TABLESPACE startend_tbs3,
      PARTITION p4 START(2500),
      PARTITION p5 START(3000) END(5000) EVERY(1000) TABLESPACE startend_tbs4
  )
  ENABLE ROW MOVEMENT;

  -- 查看分区表信息
  mogdb=# SELECT relname, boundaries, spcname FROM pg_partition p JOIN pg_tablespace t ON p.reltablespace=t.oid and p.parentid='tpcds.startend_pt'::regclass ORDER BY 1;
     relname   | boundaries |    spcname
  -------------+------------+---------------
   p1_0        | {1}        | startend_tbs2
   p1_1        | {201}      | startend_tbs2
   p1_2        | {401}      | startend_tbs2
   p1_3        | {601}      | startend_tbs2
   p1_4        | {801}      | startend_tbs2
   p1_5        | {1000}     | startend_tbs2
   p2          | {2000}     | startend_tbs1
   p3          | {2500}     | startend_tbs3
   p4          | {3000}     | startend_tbs1
   p5_1        | {4000}     | startend_tbs4
   p5_2        | {5000}     | startend_tbs4
   startend_pt |            | startend_tbs1
  (12 rows)

  -- 导入数据，查看分区数据量
  mogdb=# INSERT INTO tpcds.startend_pt VALUES (GENERATE_SERIES(0, 4999), GENERATE_SERIES(0, 4999));
  mogdb=# SELECT COUNT(*) FROM tpcds.startend_pt PARTITION FOR (0);
   count
  -------
       1
  (1 row)

  mogdb=# SELECT COUNT(*) FROM tpcds.startend_pt PARTITION (p3);
   count
  -------
     500
  (1 row)

  -- 增加分区: [5000, 5300), [5300, 5600), [5600, 5900), [5900, 6000)
  mogdb=# ALTER TABLE tpcds.startend_pt ADD PARTITION p6 START(5000) END(6000) EVERY(300) TABLESPACE startend_tbs4;

  -- 增加MAXVALUE分区: p7
  mogdb=# ALTER TABLE tpcds.startend_pt ADD PARTITION p7 END(MAXVALUE);

  -- 重命名分区p7为p8
  mogdb=# ALTER TABLE tpcds.startend_pt RENAME PARTITION p7 TO p8;

  -- 删除分区p8
  mogdb=# ALTER TABLE tpcds.startend_pt DROP PARTITION p8;

  -- 重命名5950所在的分区为：p71
  mogdb=# ALTER TABLE tpcds.startend_pt RENAME PARTITION FOR(5950) TO p71;

  -- 分裂4500所在的分区[4000, 5000)
  mogdb=# ALTER TABLE tpcds.startend_pt SPLIT PARTITION FOR(4500) INTO(PARTITION q1 START(4000) END(5000) EVERY(250) TABLESPACE startend_tbs3);

  -- 修改分区p2的表空间为startend_tbs4
  mogdb=# ALTER TABLE tpcds.startend_pt MOVE PARTITION p2 TABLESPACE startend_tbs4;

  -- 查看分区情形
  mogdb=# SELECT relname, boundaries, spcname FROM pg_partition p JOIN pg_tablespace t ON p.reltablespace=t.oid and p.parentid='tpcds.startend_pt'::regclass ORDER BY 1;
     relname   | boundaries |    spcname
  -------------+------------+---------------
   p1_0        | {1}        | startend_tbs2
   p1_1        | {201}      | startend_tbs2
   p1_2        | {401}      | startend_tbs2
   p1_3        | {601}      | startend_tbs2
   p1_4        | {801}      | startend_tbs2
   p1_5        | {1000}     | startend_tbs2
   p2          | {2000}     | startend_tbs4
   p3          | {2500}     | startend_tbs3
   p4          | {3000}     | startend_tbs1
   p5_1        | {4000}     | startend_tbs4
   p6_1        | {5300}     | startend_tbs4
   p6_2        | {5600}     | startend_tbs4
   p6_3        | {5900}     | startend_tbs4
   p71         | {6000}     | startend_tbs4
   q1_1        | {4250}     | startend_tbs3
   q1_2        | {4500}     | startend_tbs3
   q1_3        | {4750}     | startend_tbs3
   q1_4        | {5000}     | startend_tbs3
   startend_pt |            | startend_tbs1
  (19 rows)

  -- 删除表和表空间
  mogdb=# DROP SCHEMA tpcds CASCADE;
  mogdb=# DROP TABLESPACE startend_tbs1;
  mogdb=# DROP TABLESPACE startend_tbs2;
  mogdb=# DROP TABLESPACE startend_tbs3;
  mogdb=# DROP TABLESPACE startend_tbs4;
  ```

- 示例4：创建间隔分区表sales，初始包含2个分区，分区键为DATE类型。 分区的范围分别为：time_id &lt; '2019-02-01 00:00:00'，

  '2019-02-01 00:00:00' &lt;= time_id &lt; '2019-02-02 00:00:00' 。

  ```sql
  --创建表sales
  mogdb=# CREATE TABLE sales
  (prod_id NUMBER(6),
   cust_id NUMBER,
   time_id DATE,
   channel_id CHAR(1),
   promo_id NUMBER(6),
   quantity_sold NUMBER(3),
   amount_sold NUMBER(10,2)
  )
  PARTITION BY RANGE (time_id)
  INTERVAL('1 day')
  ( PARTITION p1 VALUES LESS THAN ('2019-02-01 00:00:00'),
    PARTITION p2 VALUES LESS THAN ('2019-02-02 00:00:00')
  );

  -- 数据插入分区p1
  mogdb=# INSERT INTO sales VALUES(1, 12, '2019-01-10 00:00:00', 'a', 1, 1, 1);

  -- 数据插入分区p2
  mogdb=# INSERT INTO sales VALUES(1, 12, '2019-02-01 00:00:00', 'a', 1, 1, 1);

  -- 查看分区信息
  mogdb=# SELECT t1.relname, partstrategy, boundaries FROM pg_partition t1, pg_class t2 WHERE t1.parentid = t2.oid AND t2.relname = 'sales' AND t1.parttype = 'p';
   relname | partstrategy |       boundaries
  ---------+--------------+-------------------------
   p1      | r            | {"2019-02-01 00:00:00"}
   p2      | r            | {"2019-02-02 00:00:00"}
  (2 rows)

  -- 插入数据没有匹配的分区，新创建一个分区，并将数据插入该分区
  -- 新分区的范围为 '2019-02-05 00:00:00' <= time_id < '2019-02-06 00:00:00'
  mogdb=# INSERT INTO sales VALUES(1, 12, '2019-02-05 00:00:00', 'a', 1, 1, 1);

  -- 插入数据没有匹配的分区，新创建一个分区，并将数据插入该分区
  -- 新分区的范围为 '2019-02-03 00:00:00' <= time_id < '2019-02-04 00:00:00'
  mogdb=# INSERT INTO sales VALUES(1, 12, '2019-02-03 00:00:00', 'a', 1, 1, 1);

  -- 查看分区信息
  mogdb=# SELECT t1.relname, partstrategy, boundaries FROM pg_partition t1, pg_class t2 WHERE t1.parentid = t2.oid AND t2.relname = 'sales' AND t1.parttype = 'p';
   relname | partstrategy |       boundaries
  ---------+--------------+-------------------------
   sys_p1  | i            | {"2019-02-06 00:00:00"}
   sys_p2  | i            | {"2019-02-04 00:00:00"}
   p1      | r            | {"2019-02-01 00:00:00"}
   p2      | r            | {"2019-02-02 00:00:00"}
  (4 rows)

  ```
