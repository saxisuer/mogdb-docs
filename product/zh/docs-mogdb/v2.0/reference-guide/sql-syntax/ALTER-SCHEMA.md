---
title: ALTER SCHEMA
summary: ALTER SCHEMA
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER SCHEMA

## 功能描述

修改模式属性。

## 注意事项

只有模式的所有者或者被授予了模式ALTER权限的用户有权限执行ALTER SCHEMA命令，系统管理员默认拥有此权限。但要修改模式的所有者，当前用户必须是该模式的所有者或者系统管理员，且该用户是新所有者角色的成员。

## 语法格式

- 修改模式的名称。

  ```ebnf+diagram
  AlterSchema ::= ALTER SCHEMA schema_name
      RENAME TO new_name;
  ```

- 修改模式的所有者。

  ```ebnf+diagram
  AlterSchema ::= ALTER SCHEMA schema_name
      OWNER TO new_owner;
  ```

## 参数说明

- **schema_name**

  现有模式的名称。

  取值范围: 已存在的模式名。

- **RENAME TO new_name**

  修改模式的名称。非系统管理员要改变模式的名称，该用户必须在此数据库上有CREATE权限。

  new_name：模式的新名称。

  取值范围: 字符串，要符合标识符命名规范。

- **OWNER TO new_owner**

  修改模式的所有者。非系统管理员要改变模式的所有者，该用户还必须是新的所有角色的直接或间接成员， 并且该成员必须在此数据库上有CREATE权限。

  new_owner：模式的新所有者。

  取值范围: 已存在的用户名/角色名。

## 示例

```sql
--创建模式ds。
mogdb=# CREATE SCHEMA ds;

--将当前模式ds更名为ds_new。
mogdb=# ALTER SCHEMA ds RENAME TO ds_new;

--创建用户jack。
mogdb=# CREATE USER jack PASSWORD 'xxxxxx';

--将DS_NEW的所有者修改为jack。
mogdb=# ALTER SCHEMA ds_new OWNER TO jack;

--删除用户jack和模式ds_new。
mogdb=# DROP SCHEMA ds_new;
mogdb=# DROP USER jack;
```
