---
title: CREATE SCHEMA
summary: CREATE SCHEMA
author: Zhang Cuiping
date: 2021-05-10
---

# CREATE SCHEMA

## 功能描述

创建模式。

访问命名对象时可以使用模式名作为前缀进行访问，如果无模式名前缀，则访问当前模式下的命名对象。创建命名对象时也可用模式名作为前缀修饰。

另外，CREATE SCHEMA可以包括在新模式中创建对象的子命令，这些子命令和那些在创建完模式后发出的命令没有任何区别。如果使用了AUTHORIZATION子句，则所有创建的对象都将被该用户所拥有。

## 注意事项

- 只要用户对当前数据库有CREATE权限，就可以创建模式。
- 系统管理员在普通用户同名schema下创建的对象，所有者为schema的同名用户（非系统管理员）。

## 语法格式

- 根据指定的名称创建模式。

  ```ebnf+diagram
  CreateSchema ::= CREATE SCHEMA schema_name
      [ AUTHORIZATION user_name ] [ schema_element [ ... ] ];
  ```

- 根据用户名创建模式。

  ```ebnf+diagram
  CreateSchema ::= CREATE SCHEMA AUTHORIZATION user_name [ schema_element [ ... ] ];
  ```

## 参数说明

- **schema_name**

  模式名称。

  > ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-notice.gif) **须知**:
  > 模式名不能和当前数据库里其他的模式重名。
  > 模式的名称不可以“pg_”开头。

  取值范围: 字符串，要符合标识符的命名规范。

- **AUTHORIZATION user_name**

  指定模式的所有者。当不指定schema_name时，把user_name当作模式名，此时user_name只能是角色名。

  取值范围: 已存在的用户名/角色名。

- **schema_element**

  在模式里创建对象的SQL语句。目前仅支持CREATE TABLE、CREATE VIEW、CREATE INDEX、CREATE PARTITION、CREATE SEQUENCE、CREATE TRIGGER、GRANT子句。

  子命令所创建的对象都被AUTHORIZATION子句指定的用户所拥有。

> ![img](https://cdn-mogdb.enmotech.com/docs-media/icon/icon-note.gif) **说明**:
> 如果当前搜索路径上的模式中存在同名对象时，需要明确指定引用对象所在的模式。可以通过命令SHOW SEARCH_PATH来查看当前搜索路径上的模式。

## 示例

```sql
--创建一个角色role1。
mogdb=# CREATE ROLE role1 IDENTIFIED BY 'xxxxxx';

-- 为用户role1创建一个同名schema，子命令创建的表films和winners的拥有者为role1。
mogdb=# CREATE SCHEMA AUTHORIZATION role1
     CREATE TABLE films (title text, release date, awards text[])
     CREATE VIEW winners AS
     SELECT title, release FROM films WHERE awards IS NOT NULL;

--删除schema。
mogdb=# DROP SCHEMA role1 CASCADE;
--删除用户。
mogdb=# DROP USER role1 CASCADE;
```
