---
title: DROP RESOURCE LABEL
summary: DROP RESOURCE LABEL
author: Zhang Cuiping
date: 2021-06-07
---

# DROP RESOURCE LABEL

## 功能描述

删除资源标签。

## 注意事项

只有poladmin，sysadmin或初始用户才能执行此操作。

## 语法格式

```ebnf+diagram
DropResourceLabel ::= DROP RESOURCE LABEL [IF EXISTS] policy_name[, ...]*;
```

## 参数说明

**label_name**

资源标签名称；

取值范围: 字符串，要符合标识符的命名规范。

## 示例

```sql
--删除一个资源标签。
mogdb=# DROP RESOURCE LABEL IF EXISTS res_label1;

--删除一组资源标签。
mogdb=# DROP RESOURCE LABEL IF EXISTS res_label1, res_label2, res_label3;
```
