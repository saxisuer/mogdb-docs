---
title: REFRESH MATERIALIZED VIEW
summary: REFRESH MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-18
---

# REFRESH MATERIALIZED VIEW

## 功能描述

REFRESH MATERIALIZED VIEW 完全替换一个 物化视图的内容。旧的内容会被抛弃。

## 注意事项

- 如果指定了 WITH DATA（或者作为默认值），支持查询将被执行以提供新的数据。如果指定了WITH NO DATA，则不会生成新数据。
- 如果希望数据在产生时排序，必须在支持查询中使用ORDER BY子句。

## 语法格式

- 查询数据

```ebnf+diagram
  RefreshMaterializedView ::= REFRESH MATERIALIZED VIEW mv_name [ WITH [ NO ] DATA ];
```

## 参数说明

- **mv_name**

要刷新的物化视图的名称。

## 示例

```sql
--这个命令将使用物化视图order_summary定义中的查询来替换该物化视图的内容：
mogdb=# REFRESH MATERIALIZED VIEW order_summary;
```
