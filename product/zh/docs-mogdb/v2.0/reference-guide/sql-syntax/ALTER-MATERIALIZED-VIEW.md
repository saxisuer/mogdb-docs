---
title: ALTER MATERIALIZED VIEW
summary: ALTER MATERIALIZED VIEW
author: Zhang Cuiping
date: 2021-05-17
---

# ALTER MATERIALIZED VIEW

## 功能描述

更改一个现有物化视图的多个辅助属性。

可用于ALTER MATERIALIZED VIEW的语句形式和动作是ALTER TABLE的一个子集，并且在用于物化视图时具有相同的含义。

## 注意事项

- 只有物化视图的所有者有权限执行ALTER TMATERIALIZED VIEW命令，系统管理员默认拥有此权限。
- 不支持更改物化视图结构。

## 语法格式

- 修改物化视图的定义。

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      OWNER TO new_owner;
  ```

- 修改物化视图的列。

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME [ COLUMN ] column_name TO new_column_name;
  ```

- 重命名物化视图。

  ```ebnf+diagram
  AlterMaterializedView ::= ALTER MATERIALIZED VIEW [ IF EXISTS ] mv_name
      RENAME TO new_name;
  ```

## 参数说明

- **mv_name**

  一个现有物化视图的名称，可以用模式修饰。

  取值范围: 字符串，符合标识符命名规范。

- **column_name**

  一个新的或者现有的。

  取值范围: 字符串，符合标识符命名规范。

- **new_column_name**

  一个现有列的新名称。

- **new_owner**

  该物化视图的新拥有者的用户名。

- **new_name**

  该物化视图的新名称。

## 示例

```sql
--把物化视图foo重命名为bar。
mogdb=# ALTER MATERIALIZED VIEW foo RENAME TO bar;
```
