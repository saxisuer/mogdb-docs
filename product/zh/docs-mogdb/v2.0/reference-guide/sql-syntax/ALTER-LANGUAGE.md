---
title: ALTER LANGUAGE
summary: ALTER LANGUAGE
author: Zhang Cuiping
date: 2021-06-07
---

# ALTER LANGUAGE

## 功能描述

修改一个过程语言的定义

## 语法格式

```ebnf+diagram
AlterLanguage ::= ALTER [ PROCEDURAL ] LANGUAGE name RENAME TO new_name ALTER [ PROCEDURAL ] LANGUAGE name OWNER TO new_owner;
```

## 参数说明

- **name**

  语言的名字。

- **new_name**

  语言的新名字。

- **new_owner**

  语言的新的所有者。

## 兼容性

SQL标准里没有ALTER LANGUAGE语句。