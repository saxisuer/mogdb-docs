---
title: ROLLBACK PREPARED
summary: ROLLBACK PREPARED
author: Zhang Cuiping
date: 2021-05-18
---

# ROLLBACK PREPARED

## 功能描述

取消一个先前为两阶段提交准备好的事务。

## 注意事项

- 该功能仅在维护模式（GUC参数xc_maintenance_mode为on时）下可用。该模式谨慎打开，一般供维护人员排查问题使用，一般用户不应使用该模式。
- 要想回滚一个预备事务，必须是最初发起事务的用户，或者是系统管理员。
- 事务功能由数据库自动维护，不应显式使用事务功能。

## 语法格式

```ebnf+diagram
RollbackPrepared ::= ROLLBACK PREPARED transaction_id ;
```

## 参数说明

**transaction_id**

待提交事务的标识符。它不能和任何当前预备事务已经使用了的标识符同名。
