---
title: JSON类型
summary: JSON类型
author: Guo Huan
date: 2021-04-06
---

# JSON类型

JSON数据类型可以用来存储JSON（JavaScript Object Notation）数据。数据可以存储为text，但是JSON数据类型更有利于检查每个存储的数值是可用的JSON值。

JSON类型相关的支持函数请参见JSON函数。
