---
title: WDR解读指南
summary: WDR解读指南
author: Zhang Cuiping
date: 2021-03-11
---

# WDR解读指南

WDR Snasphot在启动后（打开参数enable_wdr_snapshot）,会在用户表空间"pg_default"，数据库"postgres"下新建schema "snapshot"，用于持久化WDR快照数据。默认初始化用户或monadmin用户可以访问Snapshot Schema。

根据参数wdr_snapshot_retention_days来自动管理快照的生命周期。

<br/>

## WDR Snapshot 原信息表

<br/>

### SNAPSHOT.SNAPSHOT

SNAPSHOT表记录当前系统中存储的WDR 快照数据的索引信息，开始，结束时间。

**表 1** SNAPSHOT表属性

| 名称        | 类型      | 描述                | 示例                          |
| :---------- | :-------- | :------------------ | :---------------------------- |
| snapshot_id | bigint    | WDR快照序号。       | 1                             |
| start_ts    | timestamp | WDR快照的开始时间。 | 2019-12-28 17:11:27.423742+08 |
| end_ts      | timestamp | WDR快照的结束时间。 | 2019-12-28 17:11:43.67726+08  |

<br/>

### SNAPSHOT.TABLES_SNAP_TIMESTAMP

TABLES_SNAP_TIMESTAMP表记录所有存储的WDR snapshot中数据库，表对象，数据采集的开始，结束时间。

**表 2** TABLES_SNAP_TIMESTAMP表属性

| 名称        | 类型      | 描述                         | 示例                          |
| :---------- | :-------- | :--------------------------- | :---------------------------- |
| snapshot_id | bigint    | WDR快照序号。                | 1                             |
| db_name     | text      | WDR snapshot对应的database。 | tpcc1000                      |
| tablename   | text      | WDR snasphot对应的table。    | snap_xc_statio_all_indexes    |
| start_ts    | timestamp | WDR快照的开始时间。          | 2019-12-28 17:11:27.425849+08 |
| end_ts      | timestamp | WDR快照的结束时间。          | 2019-12-28 17:11:27.707398+08 |

<br/>

## WDR Snapshot 数据表

WDR Snapshot数据表命名原则:  snap_{源数据表}。

WDR Snapshot数据表来源为"DBE_PERF Schema"下所有的视图。

<br/>

## WDR Snapshot生成性能报告

基于WDR Snapshot数据表汇总、统计，生成性能报告。

**前提条件**

WDR Snasphot在启动后（即参数enable_wdr_snapshot为on时）， 且快照数量大于等于2时可以生成报告。

**操作步骤**

1. 执行如下命令查询已经生成的快照，以获取快照的snapshot_id。

    ```sql
    select * from snapshot.snapshot;
    ```

2. （可选）执行如下命令手动创建快照。数据库中只有一个快照或者需要查询在当前时段数据库监控的监控数据，可以选择手动执行快照操作,该命令需要用户具有sysadmin权限。

    ```sql
    select create_wdr_snapshot();
    ```

3. 执行如下步骤生成性能报告。

    a. 执行如下命令生成格式化性能报告文件。

    ```sql
    \a \t \o 服务器文件路径
    ```

    上述命令涉及参数说明如下:

    - \\a: 切换非对齐模式。
    - \\t: 切换输出的字段名的信息和行计数脚注。
    - \\o: 把所有的查询结果发送至服务器文件里。
    - 服务器文件路径: 生成性能报告文件存放路径。用户需要拥有此路径的读写权限。

    b. 执行如下命令将查询到的信息写入性能报告中。

    ```sql
    select generate_wdr_report(begin_snap_id bigint, end_snap_id bigint, report_type cstring, report_scope cstring, node_name cstring);
    ```

    命令中涉及的参数说明如下。

    **表 3** generate_wdr_report函数参数说明

    | 参数          | 说明             | 取值范围   |
    | :------------ | :---------------|-----------|
    | begin_snap_id | 查询时间段开始的snapshot的id（表snapshot.snaoshot中的snapshot_id）。                                                                                                          | -                                                                           |
    | end_snap_id   | 查询时间段结束snapshot的id。默认end_snap_id大于begin_snap_id（表snapshot.snaoshot中的snapshot_id）。                                                                          | -                                                                           |
    | report_type   | 指定生成report的类型。例如，summary/detail/all。                                                                                                                              | summary: 汇总数据。<br />detail: 明细数据。<br />all: 包含summary和detail。 |
    | report_scope  | 指定生成report的范围，可以为cluster或者node。                                                                                                                                 | cluster: 数据库级别的信息。<br />node: 节点级别的信息。                     |
    | node_name     | 在report_scope指定为node时，需要把该参数指定为对应节点的名称。(节点名称可以执行select * from pg_node_env;查询)。在report_scope为cluster时，该值可以指定为省略、空或者为NULL。 | node: MogDB中的节点名称。<br />cluster: 省略、空或者NULL。                  |

    c.执行如下命令关闭输出选项及格式化输出命令。

    ```
    \o \a \t
    ```

4. 根据需要查看WDR报告内容。

   **表 4** WDR报表主要内容

   | 项目                                               | 描述                                                         |
   | :------------------------------------------------- | :----------------------------------------------------------- |
   | Database Stat（数据库范围）                        | 数据库维度性能统计信息：事务，读写，行活动，写冲突，死锁等。 |
   | Load Profile（数据库范围）                         | 数据库维度的性能统计信息：CPU时间，DB时间，逻辑读/物理读，IO性能，登入登出，负载强度，负载性能表现等。 |
   | Instance Efficiency Percentages（数据库/节点范围） | 数据库级或者节点Buffer Hit（缓冲命中率），Effective CPU（CPU使用率），WalWrite NoWait（获取Wal Buffer成功率），Soft Parse（软解析率），Non-parse CPU（CPU非解析时间比例）。 |
   | Top 10 Events by Total Wait Time（节点范围）       | 最消耗时间的事件。                                           |
   | Wait Classes by Total Wait Time（节点范围）        | 最消耗时间的等待事件分类。                                   |
   | Host CPU（节点范围）                               | 主机CPU消耗。                                                |
   | Memory Statistics（节点范围）                      | 内核内存使用分布。                                           |
   | Object stats（节点范围）                           | 表，索引维度的性能统计信息。                                 |
   | Database Configuration（节点范围）                 | 节点配置。                                                   |
   | SQL Statistics（节点范围）                         | SQL语句各个维度性能统计：端到端时间，行活动，缓存命中，CPU消耗，时间消耗细分。 |
   | SQL Detail（节点范围）                             | SQL语句文本详情。                                            |

**示例**

```sql
--查询已经生成的快照。
mogdb=# select * from snapshot.snapshot;
 snapshot_id |           start_ts            |            end_ts
-------------+-------------------------------+-------------------------------
           1 | 2020-09-07 10:20:36.763244+08 | 2020-09-07 10:20:42.166511+08
           2 | 2020-09-07 10:21:13.416352+08 | 2020-09-07 10:21:19.470911+08
(2 rows)

--生成格式化性能报告wdrTestNode.html。
mogdb=# \a \t \o /home/om/wdrTestNode.html
Output format is unaligned.
Showing only tuples.

--向性能报告wdrTestNode.html中写入数据。
mogdb=# select generate_wdr_report(1, 2, 'all', 'node', 'dn');

--关闭性能报告wdrTestNode.html。
mogdb=# \o

--生成格式化性能报告wdrTestCluster.html。
mogdb=# \o /home/om/wdrTestCluster.html

--向格式化性能报告wdrTestCluster.html中写入数据。
mogdb=# select generate_wdr_report(1, 2, 'all', 'cluster');

--关闭性能报告wdrTestCluster.html。
mogdb=# \o \a \t
Output format is aligned.
Tuples only is off.
```
