---
title: Configuration Samples
summary: Configuration Samples
author: Yao Qian
date: 2021-11-09
---

# 配置示例

## 本地单实例最简配置

```yaml
global:
  cluster_name: cluster_1
  user: omm
  db_port: 26000
  base_dir: /opt/mogdb

db_servers:
  - host: 127.0.0.1
```

## 远程单实例定制化配置

```yaml
global:
  cluster_name: cluster_2
  user: mogdb
  db_port: 26000
  # 数据库密码加密后填写，加密方式 ptk encrypt <your_password>
  # 密码强度需 8~16 位，包含大小写和特殊字符
  db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U=
  base_dir: /opt/mogdb
  # 自定义数据目录
  data_dir: /data/mogdb_data

db_servers:
  - host: "192.168.100.100"  # 替换目标机器IP
    # gs_initdb 的参数定制化
    gs_initdb_opts:
      - --encoding=UTF-8
      - --dbcompatibility=A
    # 数据库 postgresql.conf 中的参数定制化
    db_conf:
      wal_level: logical
      most_available_sync: on
    ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa
```

## 单机多实例配置

> 单机多实例部署方式，仅用于**个人测试或学习**，由于这种部署方式集群内存在多个数据库实例共用IP，所以PTK的部分功能是受限的。
> 单机多实例部署，必须将单机上不同的数据库实例指定不同的系统用户，端口以及安装目录，通过不同用户来隔离实例的环境变量。
> 单机上不同实例的监听端口，至少需间隔 50 以上，因为数据库内部会依赖监听端口以上的部分端口进行集群内部通信

```yaml
global:
  cluster_name: cluster_3

db_servers:
  - host: 127.0.0.1
    user: mogdb1
    db_port: 26000
    base_dir: /home/mogdb1
  - host: 127.0.0.1
    user: mogdb2
    db_port: 27000
    base_dir: /home/mogdb2
```

## 一主多备配置

### 不同服务器的SSH配置相同

```yaml
global:
  cluster_name: cluster_4
  user: mogdb
  db_port: 26000
  base_dir: /opt/mogdb
  # 仅需在 global 里配置一份 ssh 信息即可
  ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa

db_servers:
  - host: 192.168.100.100
    role: primary
  - host: 192.168.100.101
    role: standby
  - host: 192.168.100.102
    role: standby
```

### 不同服务器SSH配置不同

```yaml
global:
  cluster_name: cluster_5
  user: mogdb
  db_port: 26000
  base_dir: /opt/mogdb

db_servers:
  - host: 192.168.100.100
    role: primary
    ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa
  - host: 192.168.100.101
    role: standby
    ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa
  - host: 192.168.100.102
    role: standby
    ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa
```

## 无法直连数据库服务器，需通过跳板机访问的配置

不管是 global 级别还是 db_servers 级别的 ssh_option 都是支持一层跳板机登录的配置

```yaml
global:
  cluster_name: cluster_6
  user: mogdb
  db_port: 26000
  base_dir: /opt/mogdb
  ssh_option:
      port: 22
      user: root
      # 密码和密钥文件填写任何一个即可
      password: ""  # ptk encrypt <ssh_password>
      key_file: ~/.ssh/id_rsa
      # 这部分配置跳板机的连接信息，上面是目标机器
      proxy:
        host: 192.168.200.100  # 跳板机IP
        port: 22
        user: root
        password: ""  # ptk encrypt <proxy_ssh_password>
        key_file: ~/.ssh/id_rsa  

db_servers:
  - host: 192.168.100.100
    role: primary
  - host: 192.168.100.101
    role: standby
  - host: 192.168.100.102
    role: standby
```
