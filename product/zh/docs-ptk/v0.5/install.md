---
title: 软件安装
summary: 软件安装
author: Yao Qian
date: 2022-05-30
---

## 安装 PTK

PTK 作为一个部署管理工具，并不需要和数据库实例安装到一起，我们推荐使用一个中控管理机器来安装PTK，然后通过该中控机去部署管理数据库实例到其他服务器。

### 在线安装

> 注意：
> 命令行安装方式暂不支持 Windows 系统

PTK 的在线安装过程非常简单，无论是 MacOS 还是 Linux 系统，执行以下指令即可自动安装成功：

```shell
curl --proto '=https' --tlsv1.2 -sSf https://cdn-mogdb.enmotech.com/ptk/install.sh | sh
```

该指令会自动将 PTK 安装在 `$HOME/.ptk` 目录下，该目录以后会作为 PTK 工具的工作目录，
其产生的缓存文件、数据文件、以及集群的配置信息，备份信息等文件均会存储到该目录下。
同时，安装指令会自动将 `$HOME/.ptk/bin` 的路径加入到对应 SHELL Profile 文件的 PATH 环境变量中，
这样在用户登录到服务器后就可以直接使用 `ptk` 指令了。

### 离线安装

如果你需要安装的服务器无法直接访问外网或者是需要在 Windows 系统上使用 PTK 工具，此时可以通过手动方式进行安装。

请按照以下操作步骤进行安装：

第一步：先在个人电脑上下载你所要安装的机器的对应安装包，以下是不同系统架构的最新版的安装包下载链接:

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

第二步：下载好安装包以后，通过内网将安装包拷贝到目标服务器上解压；

第三步：解压后会得到一个名为 `ptk` 的二进制文件，该文件即为 PTK 的可执行文件，可以根据个人偏好，将该文件移动到合适的目录（推荐 `$HOME/.ptk/bin/` 目录，需手动创建），然后将所在所在目录添加到 PATH 环境变量中即可；

### 升级 PTK

如果是离线方式安装的，重新下载最新的安装包后替换覆盖服务器上的二进制文件即可升级到最新版。

如果是在线方式安装的，执行以下指令即可自动升级：

```shell
ptk self upgrade
```
