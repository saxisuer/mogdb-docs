---
title: "ptk candidate cpu"
summary: ptk candidate cpu
author: ptk
date: 2022-9-14
---

## ptk candidate cpu

列出支持的CPU型号名称

## 语法

```shell
ptk candidate cpu [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出含有 `cpu model` 字段的表格
