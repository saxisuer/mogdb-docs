---
title: "ptk env"
summary: ptk env
author: ptk
date: 2022-06-28
---

## ptk env

打印 PTK 支持的环境变量和值

## 语法

```shell
ptk env [key1 key2 ... keyN] [flags]
```

## 支持的环境变量

### PTK_HOME

PTK 元数据的根目录，默认是安装用户家目录下 `.ptk` 文件夹

### PTK_DATA_DIR

PTK 存放所管理的集群信息的目录，默认为 `$PTK_HOME/data` 文件夹

### PTK_SSH_CONNECT_TIMEOUT

SSH 建立连接时的超时时间，默认 1 分钟。

值类型为字符串，支持的单位： h(小时)/m(分钟)/s(秒)

### PTK_CMD_EXECUTE_TIMEOUT

PTK 内部在运行每条 Shell 指令时的超时时间，默认 10 分钟。

值类型为字符串，支持的单位： h(小时)/m(分钟)/s(秒)

### PTK_LOG_PATH

运行日志输出文件路径，当配置了该环境变量时，PTK 运行时会在输出到标准输出的同时向该文件中写入日志。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

以键值对的形式显示环境变量的值

示例：

```shell
PTK_HOME="/root/.ptk"
PTK_DATA_DIR="/root/.ptk/data"
PTK_SSH_CONNECT_TIMEOUT="1m0s"
PTK_CMD_EXECUTE_TIMEOUT="10m0s"
PTK_LOG_PATH=""
```
