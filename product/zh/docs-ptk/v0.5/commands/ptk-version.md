---
title: "ptk version"
summary: ptk version
author: ptk
date: 2022-06-28
---

## ptk version

打印 PTK 的版本信息

## 语法

```shell
ptk version [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

版本信息类似如下：

```text
PTK Version: v0.2.0
Go Version: go1.17.1
Build Date: 2022-06-28T07:28:33Z
Git Hash: df42c22
```

里面包含 PTK 的版本，编译时的 Golang 版本，构建时间以及Git commit 信息
