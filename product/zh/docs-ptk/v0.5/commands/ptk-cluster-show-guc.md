---
title: "ptk cluster show-guc"
summary: ptk cluster show-guc
author: ptk
date: 2022-08-01
---

# ptk cluster show-guc

查看数据库参数信息

## 语法

ptk cluster show-guc [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -k, --key strings

- 指定要查看参数名
- 数据类型：字符串
- 可以多次指定不同参数名
