---
title: "ptk cluster switchover"
summary: ptk cluster switchover
author: ptk
date: 2022-08-01
---

# ptk cluster switchover

查看数据库参数信息

## 语法

ptk cluster switchover [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -H, --host string

- 指定要提升为主库的实例IP
- 数据类型：字符串
