---
title: "ptk cluster show-hba"
summary: ptk cluster show-hba
author: ptk
date: 2022-08-01
---

# ptk cluster show-hba

查看数据库 pg_hba 信息

## 语法

ptk cluster show-hba [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -H, --host string

- 指定要查看的实例IP
- 数据类型：字符串
- 默认查看主库的配置
