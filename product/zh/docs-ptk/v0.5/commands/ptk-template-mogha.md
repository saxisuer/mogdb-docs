---
title: "ptk template mogha"
summary: ptk template mogha
author: ptk
date: 2022-08-01
---

# ptk template mogha

生成 MogHA 配置

## 语法

ptk template mogha [flags]

## 选项

### -n, --cluster-name string

- 集群名称
- 数据类型：字符串

### -o, --output string

- 指定输出文件
- 数据类型：字符串

### --port int

- 指定 MogHA 监听的端口
- 数据类型：整型
- 默认值：8081
