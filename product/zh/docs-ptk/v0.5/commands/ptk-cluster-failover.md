---
title: "ptk cluster failover"
summary: ptk cluster failover
author: ptk
date: 2022-08-01
---

# ptk cluster failover

查看数据库参数信息

## 语法

ptk cluster failover [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -H, --host string

- 指定要failover的实例IP
- 数据类型：字符串
