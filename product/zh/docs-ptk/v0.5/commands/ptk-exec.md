---
title: "ptk exec"
summary: ptk exec
author: ptk
date: 2022-11-09
---

## ptk exec

分发shell指令或脚本

## 语法

```shell
ptk exec [flags]
```

## 选项

### -c, --command string

- 要执行的 shell 指令
- 数据类型：字符串

### -s, --script string

- 要执行的本地 shell 脚本文件路径
- 数据类型：字符串

### -f, --file string

- 指定一个配置文件，该配置文件可以是还未安装的集群
- 数据类型：字符串

### -n, --name string

- 已经安装的集群名称
- 数据类型：字符串

### -H, --host stringArray

- 指定集群内具体机器的IP执行指令
- 数据类型：字符串，可多次指定
- 默认在全部机器上执行

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

分块输出每个机器的执行结果