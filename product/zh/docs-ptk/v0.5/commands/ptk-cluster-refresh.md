---
title: "ptk cluster refresh"
summary: ptk cluster refresh
author: ptk
date: 2022-08-01
---

# ptk cluster refresh

更新集群配置信息

## 语法

ptk cluster refresh [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### --replace-ip stringArray

- 替换 db_server IP映射字符串, 格式为: old_ip=new_ip
- 数据类型：字符串
- 可以多次指定
