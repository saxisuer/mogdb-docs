---
title: "ptk gen-om-xml"
summary: ptk  gen-om-xml
author: ptk
date: 2022-06-28
---

## ptk  gen-om-xml

生成 gs_om 使用的 XML 配置文件

## 语法

```shell
ptk gen-om-xml -f CONFIG_FILE [flags]
```

## 选项

### -o, --output string

- 指定输出的文件路径
- 数据类型：字符串
- 默认值: `config_[timestamp].xml`， timestamp 为时间戳

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
