---
title: "ptk cluster install-mogha"
summary: ptk cluster install-mogha
author: ptk
date: 2022-08-01
---

## ptk cluster install-mogha

安装MogDB插件

## 语法

ptk cluster -n CLUSTER_NAME install-mogha [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -d, --install-dir string

- 指定 MogHA 安装目录
- 数据类型：字符串

### -p, --mogha-pkg string

- 指定 MogHA 本地安装包路径
- 数据类型：字符串

### -c, --node-conf string

- 指定 MogHA 配置文件路径
- 数据类型：字符串

### --skip-start

- 不启动 MogHA 服务
- 数据类型：布尔值
