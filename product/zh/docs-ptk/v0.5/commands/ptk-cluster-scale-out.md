---
title: "ptk cluster scale-out"
summary: ptk cluster scale-out
author: ptk
date: 2022-08-01
---

## ptk cluster scale-out

对一个MogDB集群进行扩容

## 语法

```shell
ptk cluster -n CLUSTER_NAME scale-out -c scale-out.yaml  [--skip-check-distro] [--skip-check-os] [--skip-create-user] [--skip-rollback]

```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -c, --config string

- 扩缩容配置文件路径
- 数据类型：字符串

### --cpu string

- 指定 CPU 厂商类型，通过 ptk candidate cpu 查看支持的列表
- 数据类型：字符串

### --default-guc

- 不自动优化 GUC 参数，使用数据库默认参数
- 数据类型：布尔值

### --skip-check-distro

- 跳过检查系统发行版
- 数据类型：布尔值

### --skip-check-os

- 跳过检查系统
- 数据类型：布尔值

### --skip-create-user

- 跳过创建用户
- 数据类型：布尔值

### --skip-rollback

- 扩容的失败的节点不做回滚操作
- 数据类型：布尔值

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

操作日志
