---
title: "ptk cluster scale-in"
summary: ptk cluster scale-in
author: ptk
date: 2022-08-01
---

## ptk cluster scale-in

对一个MogDB集群进行缩容

## 语法

```shell
ptk cluster -n CLUSTER_NAME scale-in  -H HOST_IP [--stop-db] [--clear-user] [--clear-dir] [--clear-env] [-t 120]

```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -H, --host strings

- 要移除的机器IP
- 数据类型：字符串
- 可以多次指定

### --force

- 如果无法通过ssh连接目标主机，强制从集群升级节点的配置中删除目标实例
- 数据类型： 布尔值

### --skip-clear-user

- 不删除系统用户
- 数据类型：布尔值

### --skip-clear-db

- 不清理数据库相对目录
- 数据类型：布尔值

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

操作日志
