---
title: "ptk cluster modify-comment"
summary: ptk cluster modify-comment
author: ptk
date: 2022-08-01
---

## ptk cluster modify-comment

修改集群备注信息

## 语法

ptk cluster -n CLUSTER_NAME modify-comment [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### --comment string

- 新的备注信息
- 数据类型：字符串
