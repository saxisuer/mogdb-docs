---
title: PTK 使用文档
summary: PTK 使用文档
author: Yao Qian
date: 2022-05-30
---

## PTK 简介

PTK (Provisioning Toolkit)是一款针对 MogDB 数据库开发的软件安装和运维工具，旨在帮助用户更便捷地安装部署MogDB数据库。

如果用户想要运行 MogDB 或者 MogDB 的相关组件时，仅需要执行一行命令即可实现。

## 使用场景

- 开发人员快速启动多个本地MogDB环境
- 用户通过PTK快速安装部署MogDB
- DBA日常运维使用
- 第三方运维平台集成

## 推荐部署架构

PTK 的定位是一个多集群管理软件，作为一个中控机的角色，通过 SSH 的方式远程管理多个数据库集群，所以，我们推荐您将PTK部署在单独的一台控制服务器上，然后将数据库部署到数据库专属的服务器上，如下图。当然，PTK也支持本地部署，您可以根据您的机器情况自行安排安装位置。

```
                           +-----------+
                           |    PTK    |
                           +-----------+
                 /---ssh-----/   |    \---ssh----\
                /               ssh               \
               /                 |                 \
        +-----------+      +-----------+      +-----------+
        |   MogDB   |      |   MogDB   |      |   MogDB   |
        +-----------+      +-----------+      +-----------+
```

## 通过 PTK 支持安装 MogDB 的操作系统

> 最新列表请通过 `ptk candidate os` 查看

| id       | os                                  | tested version(s)
----------|-------------------------------------|----------------------------
|  1007010 | CentOS Linux 7 (Core) (x86_64)      | 7.6.1810 (Core)
|  1008010 | Centos 8 (x86_64)                   | 8.0.1905 (Core)
|  1008020 | Centos 8 (arm64)                    | 8.0.1905 (Core)
|  1120010 | openEuler 20 (x86_64)               | 20.03 LTS
|  1120020 | openEuler 20 (arm64)                | 20.03 LTS
|  1122010 | openEuler 22 (x86_64)               | 22.03 LTS
|  1122020 | openEuler 22 (arm64)                | 22.03 LTS
|  1210010 | Kylin V10 (x86_64)                  | V10 (Tercel)
|  1210020 | Kylin V10 (arm64)                   | V10 (Tercel)
|  1320010 | UOS 20 A (x86_64)                   | 1002a/1020a/1050a
|  1320020 | UOS 20 A (arm64)                    | 1050a (kongzi)
|  1420010 | UOS 20 D/E (x86_64)                 | 1040d (fou)
|  1420020 | UOS 20 D/E (arm64)                  | 1040d (fou)
|  1520010 | Ubuntu 20 (x86_64)                  | 20.04.3 LTS (Focal Fossa)
|  1522010 | Ubuntu 22 (x86_64)                  | 22.04 (Jammy Jellyfish)
|  1607010 | Red Hat Enterprise Linux 7 (x86_64) | 7.5 (Maipo)
|  1608010 | Red Hat Enterprise Linux 8 (x86_64) | 8.5 (Ootpa)
|  1702010 | EulerOS 2 (x86_64)                  | 2.0 (SP3)
|  1702020 | EulerOS 2 (arm64)                   | 2.0 (SP3)
|  1812010 | SLES 12SP5 (x86_64)                 | 12SP5
|  1907010 | Oracle Linux 7 (x86_64)             | 7.9 (Maipo)
|  1908010 | Oracle Linux 8 (x86_64)             | 8.6 (Ootpa)
|  2008010 | Rocky Linux 8 (x86_64)              | 8.5 (Green Obsidian)
|  2107010 | NeoKylin V7 (x86_64)                | V7Update6
|  2222010 | FusionOS 22 (x86_64)                | 22.0.2
|  2222020 | FusionOS 22 (arm64)                 | 22.0.2

*更多操作系统适配测试中...*
