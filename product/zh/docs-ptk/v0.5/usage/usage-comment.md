---
title: Cluster Comment
summary: Cluster Comment
author: Yao Qian
date: 2022-09-15
---

# 集群备注

PTK 在安装数据库集群时，支持通过 `--comment` 参数来指定一段针对该集群的备注信息。

安装成功后，备注信息会在 ls 查看集群列表时显示。

## 修改备注信息

当需要修改某个集群的备注信息时，可通过如下执行进行修改：

```shell
ptk cluster -n CLUSTER_NAME modify-comment --comment "new comment"
```
