---
title: Scale
summary: Scale
author: Yao Qian
date: 2022-07-30
---

# 数据库扩缩容

## 什么是扩缩容？

扩缩容是指在某个已存在的数据库集群上，对集群进行增加数据库节点或减少数据库节点的操作，以满足不同的业务需求场景

术语:

- **扩容**：指通过一系列操作，在原有集群上，增加数据库节点。
- **缩容**：指通过一系列操作，在原有集群上，减少数据库节点。

## 前提

本文假设你已有一个 MogDB 数据库集群，可以是单机或多节点。且集群名为 `CLUSTER_NAME`

扩容时，请确保数据库版本，配置和系统环境一致，避免出现不兼容问题

> 数据库集群安装请参考: [安装数据库](/install.md)

## 扩容

### 查看帮助文档

```
# ptk cluster scale-out -h
对一个MogDB集群进行扩容

Usage:
  ptk cluster scale-out [flags]

Examples:
ptk cluster -n <cluster_name> scale-out -c scale-out.yaml

Flags:
  -y, --assumeyes           自动对所有提问回复Yes
  -c, --config string       扩缩容配置文件路径
      --cpu string          指定CPU型号名
                            可通过 'ptk candidate cpu' 来查看支持的CPU型号名列表
      --default-guc         不自动优化 GUC 参数，使用数据库默认参数
  -h, --help                help for scale-out
      --skip-check-distro   跳过检查系统发行版
      --skip-check-os       跳过系统检查
      --skip-create-user    跳过创建用户
      --skip-rollback       安装失败时跳过回滚操作
```

#### 扩容原理

ptk 在扩容时，会在当前集群内随机选择一个节点，将节点上的应用目录和工具目录等静态文件进行打包，复制到目标机器上后，解压到对应目录下。
然后使用内核工具初始化一个新的数据目录出来，最后将集群的配置按照新集群的拓扑整体刷新。

注意，扩容时采用的是逐个节点扩容的模式，当扩容至某个节点失败时，立即停止继续扩容，按照已完成的集群进行刷新配置。

### 创建扩容的配置文件

ptk 的 template 子命令中包含 scale-out 的子命令，可以生成一个基础的扩容模板配置文件

```
ptk template scale-out > scale-out.yaml
```

此时已创建一个扩容的配置文件 scale-out.yaml，请按需修改该配置文件。

下面对扩容模板内容进行一下介绍:

```yaml
# 新增的数据库服务器列表，支持的字段和安装时相同
db_servers:
  - host: "replace host ip here"
    # 角色仅支持 "standby" (默认) 或者 "cascade_standby"
    role: standby
    ssh_option:
      port: 22
      user: root
      password: "encrypted ssh password by ptk"

# CM 组件服务器列表
# 如果扩容前的集群安装了CM组件，则需要指定扩容后的CM服务器列表，
# 一般情况下 CM 服务器的列表和数据库的相同即可
# 但如果在新的服务器上仅扩容数据库或者仅扩容CM组件，两个列表内的机器列表可以不一致
cm_servers:
  - host: "replace host ip here"
```

### 扩容支持情况表

|  原集群情况    |   扩容条件   |   支持情况   |   解决方案   |
| ---- | ---- | ---- | ---- |
|   db1,db2   |   db3   |   支持   ||
|   db1,db2   |   cm1,cm2   |   不支持   | 卸载集群指定 --install-cm 参数重新装 |
|   db1+cm1,db2+cm2,db3+cm3   |   db4+cm4   |   支持   ||
|   db1+cm1,db2+cm2,db3+cm3   |   db4   |   支持   ||
|   db1+cm1,db2+cm2,db3+cm3   |   cm4   |   支持   ||
|   db1+cm1,db2+cm2,db3   |   cm3   |   不支持   | 缩容 db3, 扩容 db3+cm3 |
|   db1+cm1,db2+cm2,cm3   |   db3   |   不支持   | 缩容 cm3, 扩容 db3+cm3 |

### 扩容指令

```shell
ptk cluster -n CLUSTER_NAME scale-out -c scale-out.yaml 
```

## 缩容

### 查看帮助

```
# ptk cluster scale-in -h

对一个MogDB集群进行缩容

Usage:
  ptk cluster scale-in [flags]

Examples:
ptk cluster -n <cluster_name> scale-in -H <IP1> -H <IP2>

Flags:
  -y, --assumeyes          自动对所有提问回复Yes
      --force              如果无法通过ssh连接目标主机，强制从集群升级节点的配置中删除目标实例
  -h, --help               help for scale-in
  -H, --host stringArray   要删除的目标IP
      --skip-clear-db      不清除目标数据库的目录
      --skip-clear-user    不删除目标数据库的系统用户
```

### 缩容指令

```shell
ptk cluster -n CLUSTER_NAME scale-in -H IP1 -H IP2
```

会将IP为 `IP1` 和 `IP2` 的数据库节点从集群中移除。

**缩容的部分说明**：

- 关于一主多备多级联的缩容

假设集群拓扑结构为

```
         standby1 —— cascade_standby1
       /
primary 
       \
         standby2 —— cascade_standby2
```

缩容删除standby1时，最终拓扑结构变为

```
         standby1   cascade_standby1
                  /
primary          /
       \        /
         standby2 —— cascade_standby2
```

此时standby1不在集群中，而是独立出来，与原集群没有任何关联。

也就是说，如果被删除的是备库，且该备备库下有级联节点，且除了被删的备库之外，还有其他备库。会为其下的级联备库随机选一个未被删除的备库连接。

- 关于一主一备一级联的缩容

假设集群拓扑结构为

```
primary —— standby1 —— cascade_standby1
```

缩容删除standby1时，最终拓扑结构变为

```
primary    standby1    cascade_standby1    
```

此时3个节点直接没有任何关联，均独立。

### 缩容支持情况表

> 注意：缩容仅支持按照 IP 的节点级别整体摘除

|  原集群情况    |   缩容条件   |   支持情况   |   解决方案   |
| ------------ | ------------ | ------------ | ------------ |
|   db1,db2,db3               |   db3   |   支持   ||
|   db1+cm1,db2+cm2,db3+cm3   |   db3+cm3(同一个IP)   |   支持   ||
|   db1+cm1,db2+cm2,db3+cm3   |   db3   |   不支持   | 缩容 db3+cm3，扩 cm3 |
|   db1+cm1,db2+cm2,db3+cm3   |   cm3   |   不支持   | 缩容 db3+cm3，扩 db3 |
|   db1+cm1,db2+cm2,db3   |   db3   |   支持   | |
|   db1+cm1,db2+cm2,cm3   |   cm3   |   支持   | |
