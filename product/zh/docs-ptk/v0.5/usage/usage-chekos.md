---
title: CheckOS
summary: CheckOS
author: Yao Qian
date: 2022-07-30
---

# 系统检查

在安装数据库之前，你需要先对要安装数据库的服务器进行一下系统参数以及软件依赖等检查，确保安装过程可以顺利执行。

## 检查项列表

PTK 内置了 `checkos` 的指令来辅助你进行检查操作，在检查过程中，PTK 会对服务器上以下这些模块进行检查：

| 类别编号 | 类别         | 检查项                        | 说明                 |
| -------- | ------------ | ----------------------------- | -------------------- |
| A1       | 系统版本     | Check_OS_Version              | 检查系统版本         |
| A2       | 内核版本     | Check_Kernel_Version          | 检查内核版本         |
| A3       | 字符集       | Check_Unicode                 | 检查字符集           |
| A4       | 时区         | Check_TimeZone                | 检查时区             |
| A5       | 内存交换区   | Check_Swap_Memory_Configure   | 检查 swap 内存配置   |
| A6       | sysctl参数   | Check_SysCtl_Parameter        | 检查 sysctl 参数     |
| A7       | 文件系统     | Check_FileSystem_Configure    | 检查文件系统配置     |
| A8       | 磁盘配置     | Check_Disk_Configure          | 检查磁盘配置         |
| A9       | 预读块设置   | Check_BlockDev_Configure      | 检查块设备配置       |
|          |              | Check_Logical_Block           | 检查逻辑块           |
| A10      | IO调度       | Check_IO_Request              | 检查 IO 请求参数     |
|          |              | Check_Asynchronous_IO_Request | 检查异步 IO 请求参数 |
|          |              | Check_IO_Configure            | 检查 IO 配置         |
| A11      | 网络配置     | Check_Network_Configure       | 检查网络配置         |
| A12      | 时钟一致性   | Check_Time_Consistency        | 检查时钟一致性       |
| A13      | 防火墙配置   | Check_Firewall_Service        | 检查防火墙配置       |
| A14      | 透明大页配置 | Check_THP_Service             | 检查透明大页配置     |
| A15      | 依赖包     | Check_Dependent_Package        | 检查数据库系统安装依赖 |
| A16      | CPU指令集   | Check_CPU_Instruction_Set        | 检查CPU指令集 |
| A17      | 端口状态   | Check_Port        | 检查数据库端口是否被占用 |

在使用 PTK 进行系统检查时，需指定集群配置文件（`-f`）以及指定要检查的内容（`-i`），如果不知道要检查的内容，默认会检查以上表格内的所有项目。

如果需要针对个别类别进行检查，可以通过以逗号连接类别编号来进行指定。

## 示例

```shell
ptk checkos -i A1,A2,A3   # 要检查多个状态，请按以下格式输入项目：“-i A1,A2,A3”。
ptk checkos -i A          # 检查全部检查项
ptk checkos -i A --detail # 加上--detail 会显示详细信息
ptk checkos --gen-warning-fix # 输出 root-fix 脚本的时候，将 warning 级别的检查项也生成修复指令
```

## 检查结果分类

PTK 的检查结果分为四个等级：

- **OK**：符合预期值，满足安装需求
- **Warning**: 不符合预期值，但可以满足安装需求
- **Abnormal**: 不满足安装要求，可能会导致安装过程失败，需根据 PTK 给出的修改建议脚本，人工执行修改
- **ExecuteError**: 执行错误，该等级表示 PTK 在检查时执行命令失败，可能为用户环境工具缺失或内部BUG，需根据实际错误提示进行修正

在安装数据库之前，需确保所有的检查项都在 **OK** 或 **Warning** 级别，如果存在 **Abnormal** 的检查结果，在安装时会报错退出。

## 更多参数

更新参数的使用说明请通过 `ptk checkos -h` 查看

```shell
Usage:
  ptk checkos [flags]

Flags:
      --detail                 打印每个检查项的详细信息
  -f, --file string            指定集群配置文件
      --gen-warning-fix        输出 abnormal 和 warning 项目的修复语句，默认仅输出 abnormal 的项目修复语句
  -h, --help                   help for checkos
  -i, --item string            指定检查项编号，多个编号间使用英文逗号分隔 (default "A")
      --only-abnormal-detail   仅显示Abnormal级别的信息
  -o, --output string          指定结果输出文件路径
```
