---
title: Config
summary: Config
author: Yao Qian
date: 2022-07-30
---

# 创建配置文件

## 手动配置

在安装数据库之前，需通过 PTK 来创建一个配置文件，用于描述期望的数据库集群拓扑结构。

如果要安装单实例数据库，可以通过以下命令生成配置文件：

```shell
ptk template -l > config.yaml
```

如果要安装多实例集群，则可以通过以下命令生成配置文件：

```shell
ptk template > config.yaml
```

运行后会在当前文件夹内生成 config.yaml 文件，用户需修改文件内 db_server 相关的字段。

如果存在密码字段，用户需通过 [ptk encrypt](../commands/ptk-encrypt.md) 指令加密后写入配置文件。

> 注意：
> 如果您希望 PTK 使用非 root 用户来连接目标机器操作, 请确保该用户在目标机器上具有免密执行 sudo 的权限，
> 配置方式：可在目标机器上的 /etc/sudoers 文件中新增如下一行（USERNAME 替换为SSH连接用户名） ：
> `[USERNAME]  ALL=(ALL)  NOPASSWD:ALL`

## 交互式创建

PTK 也支持通过交互式方式创建配置文件，只需执行如下命令:

```shell
ptk template create
```

执行后，会进入交互式终端，用户只需根据提示的问题，逐个填写即可。

注意，在填写 db_server 时，SSH 的链接信息如果相同可重复使用。

当交互式终端退出后，会自动在当前文件夹下创建一个 config.yaml 的文件。

### 交互式创建示例

默认情况下，大部分字段可使用系统生成的默认值，回车即可，密码字段需用户手动输入。

下面是创建一主一备的配置文件的示例，假设主在本机，备机IP为 `192.168.1.100`

```shell
$ ptk template create
请输入集群名称: (default: cluster_igiltry)
请输入系统用户名: (default: omm)
请输入用户组名: (default: omm)
请输入数据库初始密码(需8到16位)
请再次输入数据库密码确认:
请输入数据库监听端口，取值范围 [1024~65535]: (default: 26000)
请输入CM sever的监听端口, 取值范围[1024~65535]: (default: 15300)
请输入数据库要安装的目录(需确保为空): (default: /opt/mogdb)
接下来, 我们来添加一些数据库实例服务器
================db server 1================
请输入服务器IP(仅支持IPv4):  127.0.0.1
请选择数据库角色:
  1: primary
  2: standby
  3: cascade_standby
请输入选项编号:  1
请输出数据库的 available zone 名称: (default: AZ1)
================ end ================
Do you want to add another db server?[Y|Yes](default=N) y
================db server 2================
请输入服务器IP(仅支持IPv4):  192.168.1.100
请选择数据库角色:
  1: primary
  2: standby
  3: cascade_standby
请输入选项编号:  2
请输出数据库的 available zone 名称: (default: AZ1)
[SSH] 请输入 SSH 登录用户: (default: root)
[SSH] 请输入 SSH 端口: (default: 22)
[SSH] 请选择验证方式:
  1: Password
  2: KeyFile
请输入选项编号:  1
[SSH] 请输入 SSH 登录密码:
================ end ================
Do you want to add another db server?[Y|Yes](default=N) n
Generate /Users/vimiix/Home/enmotech/ptk/ptk_repo/config.yaml successfully
```

## 配置文件字段说明

我们以下面这个完整的配置文件为例说明：

> yaml 中，如果是字符串类型默认无需加引号，如果字符串之中包含空格或特殊字符，需要放在引号之中

```yaml
global:
  # # 集群名称，用户自定义
  cluster_name: c1
  # # 运行数据的系统用户
  user: omm
  # # 系统用户组，默认和用户名同名
  group: omm
  # # 系统用户密码，PTK在创建用户时设置，可不填。若填写需通过 ptk encrypt 加密
  # user_password: ""
  # # 数据库密码，需通过 ptk encrypt 加密。若不填写，在安装时会通过交互式填写
  db_password: ""
  # # 数据库端口
  db_port: 26000
  # # 数据库的安装目录
  # # PTK 会在该目录下分别创建应用目录，数据目录，工具目录，日志目录以及临时目录
  base_dir: /opt/mogdb
  # # gs_initdb 工具支持参数列表，安装时会追加到指令中
  gs_initdb_opts:
  - --encoding=UTF-8
  - --dbcompatibility=A
  # # CM 组件的配置，该值在启用CM组件时生效
  cm_option:
    # # CM 的安装目录，不填写默认会在 base_dir 目录下创建 cm 目录
    # dir: /opt/mogdb/cm
    # # CM Server 端口
    cm_server_port: 15300
    # # 虚拟IP，该值需CM支持的版本才生效
    db_service_vip: ""
    # # CM Server 配置，会更新到 cm_server.conf 中
    cm_server_conf:
      # instance_heartbeat_timeout: 2
      # ...
    # # CM Agent 配置，会更新到 cm_agent.conf 中
    cm_agent_conf:
      # log_max_size: 10240
      # ...

db_servers:
  - host: 10.0.1.100
    # # 数据库端口
    db_port: 26000
    # # 数据库角色，支持的选项: primary, standby, cascade_standby
    role: primary
    # # 可用区名称，该值在启用CM组件时生效
    az_name: AZ1
    # # 可用区优先级，值越小优先级越高，最小 1，该值在启用CM组件时生效
    az_priority: 1
    # # 级联备的上游备库IP，仅级联备时必填
    upstream_host:
    # # postgresql.conf 中支持的参数
    db_conf:
      # replication_type: 1
    # # 连接服务器的SSH配置
    # ssh_option:
    #    port: 22
    #    user: root
    #    password: 
# cm 服务器列表，如果不在这里列出，在安装时会在每个 db_server 的机器上部署
# 注意此处配置不意味着安装，需在安装时指定 --install-cm 参数才会生效
cm_servers:
  - host: 10.0.1.100
    # # CM Server 端口，默认为 global.cm_option.cm_server_port
    port: 15300 
    # # ssh 配置信息，如果 db_server 有相同机器配置过，则无需在此处配置
    ssh_option:
```

整个配置文件分为两个部分：`global` 和 `db_servers`

- **global**: 定义集群内所有实例公用的信息及缺省值
- **db_servers**: 实例列表，每个元素代表一个实例信息

### global 配置

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
cluster_name | 集群名称 | 字符串 | `cluster_` 前缀，随机生成| 是
user | 操作系统用户 | 字符串| omm | 否
group | 操作系统用户组 | 字符串| omm | 否
user_password | 操作系统用户密码 | 字符串 | 空 | 否 | 需通过 encrypt 指令加密后填写
db_password | 数据库密码 | 字符串| 空 | 否 | 需通过 encrypt 指令加密后填写，若不填写，安装过程也可以填写
db_port | 数据库监听端口 | 整型| 26000 | 否|
base_dir | 数据库安装的基础目录 | 字符串| /opt/mogdb | 否 | 若填写该字段，app_dir, log_dir, data_dir, tool_dir, cm_dir, tmp_dir 可不填写，自动放置到该字段指定的目录下
app_dir | 数据库应用目录 | 字符串| /opt/mogdb/app | 否|
log_dir | 数据库日志存放目录 | 字符串 | /opt/mogdb/log | 否|
data_dir | 数据库数据存放目录 | 字符串 | /opt/mogdb/data | 否|
tool_dir | 数据库工具存放目录 | 字符串| /opt/mogdb/tool | 否|
tmp_dir | 数据库临时文件存放目录 | 字符串 | /opt/mogdb/tmp | 否|
core_file_dir | 系统 corefile 路径 | 字符串 | 空 | 否 | 若填写会修改系统 `kernel.core_pattern` 的值
gs_initdb_opts | 初始化数据库时 gs_initdb 工具的参数 | 字符串列表 | 否 | 一个元素设置一个参数，如果未设置 `--locale` 参数，PTK 默认设置 `--no-locale`
ssh_option | SSH连接认证信息 | [ssh_option](#ssh_option) | 否 | 实例如果需要远程连接，但实例级别的 ssh_option 未配置时会使用 global 内的该字段
cm_option | CM组件配置参数 | [cm_option](#cm_option) | 否 | 该字段在安装时指定 `--install-cm` 时生效

### db_servers 配置

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
host | 实例IP | 字符串 | 空 | 是 |
db_port | 实例端口 | 整型 | 26000 | 否 | 若未填写，使用 global 内的配置
ha_ips | 实例日志复制备用IP列表 | 字符串列表 | 空 | 否 |
ha_port | 日志复制备用端口 | 整型 | 26001 | 否 |
role | 数据库实例角色 | 字符串 | 空 | 否 | 可选值： `primary`, `standby`, `cascade_standby`, 若不填写，集群内随机选择一个实例设置主，其余为备库
upstream_host | 级联备上游备库IP | 字符串 | 空 | 级联备时必填 |
az_name | 可用区名 | 字符串 | AZ1 | 否 |
az_priority | 可用区优先级 | 整型 | 1 | 数值越小优先级越高
xlog_dir | xlog 日志目录 | 字符串 | 空 | 默认会放到 log_dir 的 pg_log 下，若配置该字段，不能为数据目录的子目录，否则启动报错
db_conf | 数据库guc 参数配置 | 字典 | 空 | 字典键为 postgresql.conf 中的参数
ssh_option | 连接实例时的ssh 连接认证信息 | [ssh_option](#ssh_option) | 否 | 若未设置使用 global 内的配置

### cm_servers 配置

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
host | 实例IP | 字符串 | 空 | 是 |
port | CM Server端口 | 整型 | 15300 | 否 | 若未填写，使用 global.cm_option.cm_server_port 的值
ssh_option | 连接实例时的ssh 连接认证信息 | [ssh_option](#ssh_option) | 否 | 若未设置使用 global 内的配置或db_servers 中相同IP的配置

### cm_option

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
dir | CM 安装目录 | 字符串 | $base_dir/cm | 是 | 
cm_server_port | CM server 监听端口 | 整型 | 15300 | 否| 
db_service_vip | 虚拟IP（需CM支持） | 字符串 | 空 | 否 | 
cm_server_conf | CM sever 配置参数 | 字典| 空 | 否 | 该字段下字典的键支持任何 cm_server 的参数，PTK 会将值设置到 cm_server.conf 文件中
cm_agent_conf | CM agent 配置参数 | 字典 | 空 | 否 | 该字段下字典的键支持任何 cm_agent 的参数，PTK 会将值设置到 cm_agent.conf 文件中

### ssh_option

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
host | 机器IP | 字符串 | 实例的IP | 否 | 如果配置为 proxy 跳板机时，该字段必填
port | ssh 连接端口 | 整型 | 22 | 否 |
user | ssh 连接用户 | 字符串 | root | 否 | 如果连接的用户为非 root 时，需确保该用户具有免密执行 sudo 的权限
password | ssh 连接密码 | 字符串 | 空 | 否 | 需通过 encrypt 指令加密后填写
key_file | ssh 连接密钥文件 | 字符串 | 空 | 否 |
passphrase | ssh 连接密钥文件的口令 | 字符串 | 空 | 否 |
conn_timeout | ssh 连接超时时间 | Duration | 1m | 否 |
exec_timeout | 执行命令超时时间 | Duration | 10m | 否 |
proxy | 跳板机配置 | ssh_option | 空 | 否 | 仅支持跳一次连接
