---
title: Operation
summary: Operation
author: Yao Qian
date: 2022-07-30
---

# 数据库操作

## 查看集群列表

当数据库集群安装成功后，可以通过 `ptk ls` 来查看当前用户已经安装过的集群列表（由于 PTK 的元数据存储于 `$HOME/.ptk` 目录下，所以仅能查看当前用户安装的，无法查看其他用户安装的集群列表）。

示例：

```shell
$ ptk ls
cluster_name|  id  |         addr          | user |       data_dir        |          db_version          |     create_time     | comment
------------------+------+-----------------------+------+-----------------------+------------------------------+---------------------+----------
     c1     | 6001 | 192.168.100.100:26000 | ptk1 | /home/ptk1/mogdb/data | MogDB 3.0.3 (build 23ba838d) | 2022-11-09 15:01:34 |
```

可以在输出表中查看如下信息：

- cluster_name: 集群名称
- id: 安装时给实例分配的ID
- addr: 实例地址列表
- user: 运行的系统用户
- data_dir: 实例的数据目录
- db_version: 数据库版本信息
- create_time: 集群创建时间
- comment: 集群备注

集群安装成功后，在之后管理集群操作时，使用 `-n` 参数指定集群名称来管理集群。

## 查看集群状态

可以通过 `cluster status` 指令查看集群状态

```shell
# ptk cluster -n c1 status

[   Cluster State   ]
database_version            : MogDB 3.0.3 (build 23ba838d)
cluster_name                : c1
cluster_state               : Normal

[  Datanode State   ]
cluster_name |   id  |       ip        | port  | user | instance | db_role | state
-------------+-------+-----------------+-------+------+----------+---------+---------
   c1        |  6001 | 192.168.122.101 | 26000 | ptk  | dn_6001  | primary | Normal
             |  6002 | 192.168.122.102 | 26000 | ptk  | dn_6002  | standby | Normal
```

## 启动集群

> 下面的集群操作以集群 `c1` 为例

在安装完数据库集群后，PTK 默认会将数据库集群启动。

如果安装时指定了 `--skip-launch-db` 的话，数据库会处于停止状态。

此时可通过 `cluster start` 指令启动集群，需指定集群的集群名称。

示例：

```shell
# ptk cluster -n c1 start
INFO[2022-08-02T11:40:48.728] Operating: Starting.
INFO[2022-08-02T11:40:48.728] =========================================
INFO[2022-08-02T11:40:48.784] starting host 192.168.122.101
INFO[2022-08-02T11:40:54.097] starting host (192.168.122.101,ptk) successfully
INFO[2022-08-02T11:40:54.097] starting host 192.168.122.102
INFO[2022-08-02T11:40:56.329] starting host (192.168.122.102,ptk) successfully
INFO[2022-08-02T11:40:56.613] waiting for check cluster state...
INFO[2022-08-02T11:41:01.861] =========================================
INFO[2022-08-02T11:41:01.861] Successfully started.
INFO[2022-08-02T11:41:01.861] Operation succeeded: Start.
```

同时，PTK 默认会启动集群内所有实例，PTK 也支持指定单个实例启动：

```shell
# ptk cluster -n c1 start -H 192.168.122.101
INFO[2022-08-02T11:50:04.442] Operating: Starting.
INFO[2022-08-02T11:50:04.442] =========================================
INFO[2022-08-02T11:50:06.692] starting host (192.168.122.101,ptk) successfully
```

更多启动参数请查看帮助文档：

```shell
# ptk cluster start -h
启动数据库实例或集群

Usage:
  ptk cluster start [flags]

Flags:
  -h, --help                   help for start
  -H, --host string            操作的实例IP
  -n, --name string            集群名称
      --security-mode string   是否使用安全模式启动数据库
                               可选项: on/off
      --time-out duration      启动超时时间 (default 10m0s)
```

## 停止集群

> 下面的集群操作以集群 `c1` 为例

如果想要停止数据库集群，可以通过 `cluster stop` 指令，默认会停止集群内所有实例：

```shell
# ptk cluster -n c1 stop
INFO[2022-08-02T11:49:40.685] Operating: Stopping.
INFO[2022-08-02T11:49:40.685] =========================================
INFO[2022-08-02T11:49:40.891] stopping host 192.168.122.102
INFO[2022-08-02T11:49:41.946] stopping host (192.168.122.102,ptk) successfully
INFO[2022-08-02T11:49:41.946] stopping host 192.168.122.101
INFO[2022-08-02T11:49:43.004] stopping host (192.168.122.101,ptk) successfully
INFO[2022-08-02T11:49:43.004] =========================================
INFO[2022-08-02T11:49:43.004] Successfully stoped.
INFO[2022-08-02T11:49:43.004] Operation succeeded: Stop.
```

如果想要停止集群内某个实例，可通过 `-H` 指定实例的IP

```shell
# ptk cluster -n c1 stop -H 192.168.122.101
INFO[2022-08-02T11:56:32.880] Operating: Stopping.
INFO[2022-08-02T11:56:32.881] =========================================
INFO[2022-08-02T11:56:34.154] stopping host (192.168.122.101,ptk) successfully
```

停止集群的更多参数，请查看帮助文档：

```shell
# ptk cluster stop -h
停止数据库实例或集群

Usage:
  ptk cluster stop [flags]

Flags:
  -h, --help                help for stop
  -H, --host string         操作的实例IP
  -n, --name string         集群名称
      --time-out duration   停止超时时间 (default 10m0s)
```

## 重启集群

> 下面的集群操作以集群 `c1` 为例

重启集群的操作，本质上是先停止数据库，再启动数据库的组合操作。

可通过 `cluster restart` 指令来实现：

```shell
# ptk cluster -n c1 restart
INFO[2022-08-02T11:59:31.037] Operating: Stopping.
INFO[2022-08-02T11:59:31.037] =========================================
INFO[2022-08-02T11:59:31.217] stopping host 192.168.122.102
INFO[2022-08-02T11:59:32.269] stopping host (192.168.122.102,ptk) successfully
INFO[2022-08-02T11:59:32.269] stopping host 192.168.122.101
INFO[2022-08-02T11:59:33.309] stopping host (192.168.122.101,ptk) successfully
INFO[2022-08-02T11:59:33.309] =========================================
INFO[2022-08-02T11:59:33.309] Successfully stoped.
INFO[2022-08-02T11:59:33.309] Operation succeeded: Stop.

INFO[2022-08-02T11:59:33.310] Operating: Starting.
INFO[2022-08-02T11:59:33.310] =========================================
INFO[2022-08-02T11:59:33.376] starting host 192.168.122.101
INFO[2022-08-02T11:59:35.583] starting host (192.168.122.101,ptk) successfully
INFO[2022-08-02T11:59:35.583] starting host 192.168.122.102
INFO[2022-08-02T11:59:36.787] starting host (192.168.122.102,ptk) successfully
INFO[2022-08-02T11:59:36.995] waiting for check cluster state...
INFO[2022-08-02T11:59:42.247] =========================================
INFO[2022-08-02T11:59:42.247] Successfully started.
INFO[2022-08-02T11:59:42.247] Operation succeeded: Start.
```

重启集群的更多参数，请查看帮助文档：

```shell
# ptk cluster restart -h
重启数据库实例或集群

Usage:
  ptk cluster restart [flags]

Flags:
  -h, --help                   help for restart
  -H, --host string            操作的实例IP
  -n, --name string            集群名称
      --security-mode string   是否使用安全模式启动数据库
                               可选项: on/off
      --time-out duration      启动超时时间 (default 10m0s)
```
