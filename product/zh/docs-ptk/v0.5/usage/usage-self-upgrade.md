---
title: Self upgrade
summary: Self upgrade
author: Yao Qian
date: 2022-07-30
---

# 升级PTK

## 查看帮助文档

```
$ ptk self -h

操作 PTK 自身安装包

Usage:
  ptk self [command]

Available Commands:
  upgrade     下载并自动安装PTK

Flags:
  -h, --help   help for self
```

### 在线升级

PTK 支持在有网络情况下，通过 `self upgrade` 指令将PTK升级到已经发布的最新版本，升级后通过 `ptk -v` 查看版本。

下面是升级示例：

```shell
$ ptk self upgrade 

INFO[2022-07-30T10:26:36.197] downloading ptk_darwin_arm64.tar.gz...
> ptk_darwin_arm64.tar.gz: 4.86 MiB / 4.86 MiB [-------------------------------------------------------------------] 100.00% 7.48 MiB p/s 900ms
INFO[2022-07-30T10:26:37.701] download successfully
INFO[2022-07-30T10:26:37.831] upgrade ptk successfully
```

### 离线包升级

首选，根据系统类型下载离线包（下面的链接始终返回最新版PTK安装包）：

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

下载好离线包以后，将离线包放置在PTK所在的服务器上PTK二进制目录后解压覆盖之前的二进制文件即可：

```shell
# which ptk
/root/.ptk/bin/ptk

# cd /root/.ptk/bin/
tar -xvf ptk_[os]_[arch]_.tar.gz
```

如果 ptk 版本大于 0.3, 可以通过 `-p` 参数本地升级：

```
# ptk self upgrade -p /path/to/ptk_[os]_[arch]_.tar.gz
```
