---
title: 发布记录
summary: 发布记录
author: Yao Qian
date: 2022-06-01
---

## latest 版本

> PTK 会根据用户反馈不定期更新发布修订版本, latest 版本链接会始终指向最新的版本

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## 发布记录

### v0.3.0

#### ChangeLog

- 支持扩缩容
- 支持插件安装
- 部分功能优化和Bug修复

#### 下载地址

- [ptk_0.3.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_linux_x86_64.tar.gz)
- [ptk_0.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_linux_arm64.tar.gz)
- [ptk_0.3.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_darwin_x86_64.tar.gz)
- [ptk_0.3.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_darwin_arm64.tar.gz)
- [ptk_0.3.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.3.0/ptk_0.3.0_windows_x86_64.tar.gz)

### v0.2.21

#### 下载地址

- [ptk_0.2.21_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_linux_x86_64.tar.gz)
- [ptk_0.2.21_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_linux_arm64.tar.gz)
- [ptk_0.2.21_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_darwin_x86_64.tar.gz)
- [ptk_0.2.21_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_darwin_arm64.tar.gz)
- [ptk_0.2.21_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.21/ptk_0.2.21_windows_x86_64.tar.gz)

### v0.1.0

- [ptk_0.1.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_x86_64.tar.gz)
- [ptk_0.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_arm64.tar.gz)
- [ptk_0.1.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_x86_64.tar.gz)
- [ptk_0.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_arm64.tar.gz)
- [ptk_0.1.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_windows_x86_64.tar.gz)
