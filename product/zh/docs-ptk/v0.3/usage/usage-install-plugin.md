---
title: Install Plugin
summary: Install Plugin
author: Yao Qian
date: 2022-07-30
---

# 插件安装

> MogDB 插件的使用请参考[相关文档](https://docs.mogdb.io/zh/mogdb/v3.0/dblink-user-guide)

PTK 插件安装会自动通过判断当前集群的版本，下载对应的插件包进行安装。

安装成功后，PTK 不会在数据库中创建 extension，需用户根据需要手动在数据库中参考插件安装指导进行创建。

## 查看帮助文档

```shell
# ptk cluster install-plugin -h
安装MogDB插件

Usage:
  ptk cluster install-plugin [flags]

Flags:
  -h, --help             help for install-plugin
  -H, --host strings     指定待安装的主机 IP，默认将安装到集群中的所有主机
  -n, --name string      集群名称
      --override         是否覆盖现有插件文件
  -p, --pkg string       指定插件包路径
  -P, --plugin strings   指定要安装的插件名称，默认会安装所有插件
```

## 安装全部插件

默认 PTK 会下载对应版本的安装包后安装全部插件到数据库，若需指定插件安装，可通过 `-P` 或 `--plugin` 指定。

安装示例：

```
# ptk cluster -n cluster_slirist install-plugin
INFO[2022-08-02T16:15:11.786] downloading Plugins-3.0.0-CentOS-x86_64.tar.gz...
> Plugins-3.0.0-CentOS-x86_64...: 70.71 MiB / 70.71 MiB [-----------------------------------------------] 100.00% 14.33 MiB p/s 5.1s
INFO[2022-08-02T16:15:17.629] download successfully
INFO[2022-08-02T16:15:17.651] scp file from /tmp/2451478859/Plugins-3.0.0-CentOS-x86_64.tar.gz to 192.168.122.101:/opt/mogdb/tool/Plugins-3.0.0-CentOS-x86_64.tar.gz  host=192.168.122.101
INFO[2022-08-02T16:15:17.651] scp file from /tmp/2451478859/Plugins-3.0.0-CentOS-x86_64.tar.gz to 192.168.122.102:/opt/mogdb/tool/Plugins-3.0.0-CentOS-x86_64.tar.gz  host=192.168.122.102
INFO[2022-08-02T16:15:17.781] mkdir /opt/mogdb/tool/plugins                 host=192.168.122.101
> upload Plugins-3.0.0-CentOS...: 70.71 MiB / 70.71 MiB [--------------------------------------------------------] 100.00% 124.92 MiB p/s 800ms
INFO[2022-08-02T16:15:18.437] mkdir /opt/mogdb/tool/plugins                 host=192.168.122.102
INFO[2022-08-02T16:15:18.606] decompress Plugins-3.0.0-CentOS-x86_64.tar.gz to dir /opt/mogdb/tool/plugins  host=192.168.122.102
INFO[2022-08-02T16:15:19.816] change /opt/mogdb/tool/plugins owner to vmx   host=192.168.122.101
INFO[2022-08-02T16:15:20.542] change /opt/mogdb/tool/plugins owner to vmx   host=192.168.122.102
INFO[2022-08-02T16:15:20.562] installing plugin [dblink] ...                host=192.168.122.101
INFO[2022-08-02T16:15:20.562] installing plugin [dolphin] ...               host=192.168.122.102
INFO[2022-08-02T16:15:20.627] plugin [dblink] installed successfully        host=192.168.122.101
INFO[2022-08-02T16:15:20.627] installing plugin [wal2json] ...              host=192.168.122.101
INFO[2022-08-02T16:15:20.668] plugin [wal2json] installed successfully      host=192.168.122.101
INFO[2022-08-02T16:15:20.668] installing plugin [pg_bulkload] ...           host=192.168.122.101
INFO[2022-08-02T16:15:20.725] plugin [dolphin] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:20.725] installing plugin [postgis] ...               host=192.168.122.102
INFO[2022-08-02T16:15:20.734] plugin [pg_bulkload] installed successfully   host=192.168.122.101
INFO[2022-08-02T16:15:20.734] installing plugin [pg_prewarm] ...            host=192.168.122.101
INFO[2022-08-02T16:15:20.771] plugin [pg_prewarm] installed successfully    host=192.168.122.101
INFO[2022-08-02T16:15:20.771] installing plugin [dolphin] ...               host=192.168.122.101
INFO[2022-08-02T16:15:20.881] plugin [dolphin] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:20.881] installing plugin [postgis] ...               host=192.168.122.101
INFO[2022-08-02T16:15:20.985] plugin [postgis] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:20.985] installing plugin [dblink] ...                host=192.168.122.102
INFO[2022-08-02T16:15:21.042] plugin [dblink] installed successfully        host=192.168.122.102
INFO[2022-08-02T16:15:21.042] installing plugin [wal2json] ...              host=192.168.122.102
INFO[2022-08-02T16:15:21.075] plugin [postgis] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:21.075] installing plugin [pg_trgm] ...               host=192.168.122.101
INFO[2022-08-02T16:15:21.112] plugin [wal2json] installed successfully      host=192.168.122.102
INFO[2022-08-02T16:15:21.112] installing plugin [pg_bulkload] ...           host=192.168.122.102
INFO[2022-08-02T16:15:21.117] plugin [pg_trgm] installed successfully       host=192.168.122.101
INFO[2022-08-02T16:15:21.117] installing plugin [orafce] ...                host=192.168.122.101
INFO[2022-08-02T16:15:21.160] plugin [orafce] installed successfully        host=192.168.122.101
INFO[2022-08-02T16:15:21.160] installing plugin [pg_repack] ...             host=192.168.122.101
INFO[2022-08-02T16:15:21.200] plugin [pg_repack] installed successfully     host=192.168.122.101
INFO[2022-08-02T16:15:21.200] installing plugin [whale] ...                 host=192.168.122.101
INFO[2022-08-02T16:15:21.208] plugin [pg_bulkload] installed successfully   host=192.168.122.102
INFO[2022-08-02T16:15:21.208] installing plugin [pg_prewarm] ...            host=192.168.122.102
INFO[2022-08-02T16:15:21.243] plugin [whale] installed successfully         host=192.168.122.101
INFO[2022-08-02T16:15:21.266] plugin [pg_prewarm] installed successfully    host=192.168.122.102
INFO[2022-08-02T16:15:21.266] installing plugin [pg_trgm] ...               host=192.168.122.102
INFO[2022-08-02T16:15:21.321] plugin [pg_trgm] installed successfully       host=192.168.122.102
INFO[2022-08-02T16:15:21.321] installing plugin [orafce] ...                host=192.168.122.102
INFO[2022-08-02T16:15:21.384] plugin [orafce] installed successfully        host=192.168.122.102
INFO[2022-08-02T16:15:21.384] installing plugin [pg_repack] ...             host=192.168.122.102
INFO[2022-08-02T16:15:21.444] plugin [pg_repack] installed successfully     host=192.168.122.102
INFO[2022-08-02T16:15:21.444] installing plugin [whale] ...                 host=192.168.122.102
INFO[2022-08-02T16:15:21.510] plugin [whale] installed successfully         host=192.168.122.102
INFO[2022-08-02T16:15:26.949] All done
INFO[2022-08-02T16:15:26.949] Time elapsed: 15s
```
