---
title: Config
summary: Config
author: Yao Qian
date: 2022-07-30
---

# 创建配置文件

## 手动配置

在安装数据库之前，需通过 PTK 来创建一个配置文件，用于描述期望的数据库集群拓扑结构。

如果要安装单实例数据库，可以通过以下命令生成配置文件：

```shell
ptk template -l > config.yaml
```

如果要安装多实例集群，则可以通过以下命令生成配置文件：

```shell
ptk template > config.yaml
```

运行后会在当前文件夹内生成 config.yaml 文件，用户需修改文件内 db_server 相关的字段。

如果存在密码字段，用户需通过 [ptk encrypt](../commands/ptk-encrypt.md) 指令加密后写入配置文件。

> 注意：
> 如果您希望 PTK 使用非 root 用户来连接目标机器操作, 请确保该用户在目标机器上具有免密执行 sudo 的权限，
> 配置方式：可在目标机器上的 /etc/sudoers 文件中新增如下一行（USERNAME 替换为SSH连接用户名） ：
> `[USERNAME]  ALL=(ALL)  NOPASSWD:ALL`

## 交互式创建

PTK 也支持通过交互式方式创建配置文件，只需执行如下命令:

```shell
ptk template create
```

执行后，会进入交互式终端，用户只需根据提示的问题，逐个填写即可。

注意，在填写 db_server 时，SSH 的链接信息如果相同可重复使用。

当交互式终端退出后，会自动在当前文件夹下创建一个 config.yaml 的文件。

### 交互式创建示例

默认情况下，大部分字段可使用系统生成的默认值，回车即可，密码字段需用户手动输入。

下面是创建一主一备的配置文件的示例，假设主在本机，备机IP为 `192.168.1.100`

```shell
$ ptk template create
请输入集群名称: (default: cluster_igiltry)
请输入系统用户名: (default: omm)
请输入用户组名: (default: omm)
请输入数据库初始密码(需8到16位)
请再次输入数据库密码确认:
请输入数据库监听端口，取值范围 [1024~65535]: (default: 26000)
请输入CM sever的监听端口, 取值范围[1024~65535]: (default: 15300)
请输入数据库要安装的目录(需确保为空): (default: /opt/mogdb)
接下来, 我们来添加一些数据库实例服务器
================db server 1================
请输入服务器IP(仅支持IPv4):  127.0.0.1
请选择数据库角色:
  1: primary
  2: standby
  3: cascade_standby
请输入选项编号:  1
请输出数据库的 available zone 名称: (default: AZ1)
================ end ================
Do you want to add another db server?[Y|Yes](default=N) y
================db server 2================
请输入服务器IP(仅支持IPv4):  192.168.1.100
请选择数据库角色:
  1: primary
  2: standby
  3: cascade_standby
请输入选项编号:  2
请输出数据库的 available zone 名称: (default: AZ1)
[SSH] 请输入 SSH 登录用户: (default: root)
[SSH] 请输入 SSH 端口: (default: 22)
[SSH] 请选择验证方式:
  1: Password
  2: KeyFile
请输入选项编号:  1
[SSH] 请输入 SSH 登录密码:
================ end ================
Do you want to add another db server?[Y|Yes](default=N) n
Generate /Users/vimiix/Home/enmotech/ptk/ptk_repo/config.yaml successfully
```

## 配置文件字段说明

我们以下面这个完整的配置文件为例说明：

> yaml 中，如果是字符串类型默认无需加引号，如果字符串之中包含空格或特殊字符，需要放在引号之中

```yaml
global:
  # # cluster name
  cluster_name:
  # # system user for running db
  user: omm
  # # system user group, same as username if not given
  group: omm
  # # system user password, use 'ptk encrypt' to encrypt it if not empty
  # user_password: ""
  # # database password, use 'ptk encrypt' to encrypt it if not empty
  # db_password: ""
  # # database port
  db_port: 26000
  # # cm_server port (Effective after mogdb3.0)
  # cm_server_port: 15300
  # # base directory for install MogDB server,
  # # if any of app_dir，data_dir，log_dir and tool_dir not config，
  # # PTK will create corresponding directory under base_dir
  base_dir: /opt/mogdb
  # # application directory
  # app_dir: /opt/mogdb/app
  # # log directory
  # log_dir: /opt/mogdb/log
  # # data directory
  # data_dir: /opt/mogdb/data
  # # tool directory
  # tool_dir: /opt/mogdb/tool
  # # cm directory (Effective after mogdb3.0)
  # cm_dir: /opt/mogdb/cm
  # cm_server_port: 15300
  # # temporary directory
  tmp_dir: "/tmp"
  # # corefile directory
  # core_file_dir: 
  # enable_cm: no
  # # gs_initdb custom options
  gs_initdb_opts:
  - --encoding=UTF-8
  - --dbcompatibility=A

db_servers:
  - host: 10.0.1.100
    # # database port
    db_port: 26000
    # # ip list for database replication, use host default
    # ha_ips:
    # - 10.0.2.100
    # # port for database replication, use db_port plus 1 default
    ha_port: 26001
    # # database role, options: primary, standby, cascade_standby
    role: primary
    # # available zone name
    az_name: AZ1
    # available zone priority
    az_priority: 1
    # # upstream host ip for cascade_standby instance
    upstream_host:
    # # parameters in postgresql.conf
    db_conf:
        replication_type: 1
    # # SSH connection config，use global.ssh_option if not config
    # ssh_option:
    #    port: 22
    #    user: root
    #    password: 
    #    key_file: 
    #    passphrase: 
    #    conn_timeout: 5s
    #    exec_timeout: 1m
    #    proxy:
    #      host: 
    #      user: 
    #      password: 
    #      key_file: 
    #      passphrase: 
    #      conn_timeout: 5s
    #      exec_timeout: 1m
```

整个配置文件分为两个部分：`global` 和 `db_servers`

- **global**: 定义集群内所有实例公用的信息
- **db_servers**: 实例列表，每个元素代表一个实例信息

### global 配置

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
cluster_name | 集群名称 | 字符串 | `cluster_` 前缀，随机生成| 是
user | 操作系统用户 | 字符串| omm | 否
group | 操作系统用户组 | 字符串| omm | 否
user_password | 操作系统用户密码 | 字符串 | 空 | 否 | 需通过 encrypt 指令加密后填写
db_password | 数据库密码 | 字符串| 空 | 否 | 需通过 encrypt 指令加密后填写，若不填写，安装过程也可以填写
db_port | 数据库监听端口 | 整型| 26000 | 否|
base_dir | 数据库安装的基础目录 | 字符串| /opt/mogdb | 否 | 若填写该字段，app_dir, log_dir, data_dir, tool_dir, cm_dir, tmp_dir 可不填写，自动放置到该字段指定的目录下
app_dir | 数据库应用目录 | 字符串| /opt/mogdb/app | 否|
log_dir | 数据库日志存放目录 | 字符串 | /opt/mogdb/log | 否|
data_dir | 数据库数据存放目录 | 字符串 | /opt/mogdb/data | 否|
tool_dir | 数据库工具存放目录 | 字符串| /opt/mogdb/tool | 否|
cm_dir | 数据库CM组件目录 | 字符串 | /opt/mogdb/cm | 否|
tmp_dir | 数据库临时文件存放目录 | 字符串 | /tmp | 否|
core_file_dir | 系统 corefile 路径 | 字符串 | 空 | 否 | 若填写会修改系统 `kernel.core_pattern` 的值
enable_cm | 是否安装 CM 组件 | 布尔值 | no | 否 |
cm_server_port | CM Server 监听端口 | 整型 | 15300 | 否 |
gs_initdb_opts | 初始化数据库时 gs_initdb 工具的参数 | 字符串列表 | 否 | 一个元素设置一个参数，如果未设置 `--locale` 参数，PTK 默认设置 `--no-locale`
ssh_option | SSH连接认证信息 | [ssh_option](#ssh_option) | 否 | 实例如果需要远程连接，但实例级别的 ssh_option 未配置时会使用 global 内的该字段

### db_servers 配置

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
host | 实例IP | 字符串 | 空 | 是 |
db_port | 实例端口 | 整型 | 26000 | 否 | 若未填写，使用 global 内的配置
ha_ips | 实例日志复制备用IP列表 | 字符串列表 | 空 | 否 |
ha_port | 日志复制备用端口 | 整型 | 26001 | 否 |
role | 数据库实例角色 | 字符串 | 空 | 否 | 可选值： `primary`, `standby`, `cascade_standby`, 若不填写，集群内随机选择一个实例设置主，其余为备库
upstream_host | 级联备上游备库IP | 字符串 | 空 | 级联备时必填 |
az_name | 可用区名 | 字符串 | AZ1 | 否 |
az_priority | 可用区优先级 | 整型 | 1 | 数值越小优先级越高
xlog_dir | xlog 日志目录 | 字符串 | 空 | 默认会放到 log_dir 的 pg_log 下，若配置该字段，不能为数据目录的子目录，否则启动报错
db_conf | 数据库guc 参数配置 | 字典 | 空 |
ssh_option | 连接实例时的ssh 连接认证信息 | [ssh_option](#ssh_option) | 否 | 若未设置使用 global 内的配置

### ssh_option

字段名 | 解释 | 值类型 | 默认值 |  是否必填 | 备注
------|-------|-----|------|------ | ------
host | 机器IP | 字符串 | 实例的IP | 否 | 如果配置为 proxy 跳板机时，该字段必填
port | ssh 连接端口 | 整型 | 22 | 否 |
user | ssh 连接用户 | 字符串 | root | 否 | 如果连接的用户为非 root 时，需确保该用户具有免密执行 sudo 的权限
password | ssh 连接密码 | 字符串 | 空 | 否 | 需通过 encrypt 指令加密后填写
key_file | ssh 连接密钥文件 | 字符串 | 空 | 否 |
passphrase | ssh 连接密钥文件的口令 | 字符串 | 空 | 否 |
conn_timeout | ssh 连接超时时间 | Duration | 1m | 否 |
exec_timeout | 执行命令超时时间 | Duration | 10m | 否 |
proxy | 跳板机配置 | ssh_option | 空 | 否 | 仅支持跳一次连接
