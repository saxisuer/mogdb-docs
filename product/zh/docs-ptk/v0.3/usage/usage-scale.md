---
title: Scale
summary: Scale
author: Yao Qian
date: 2022-07-30
---

# 数据库扩缩容

## 什么是扩缩容？

扩缩容是指在某个已存在的数据库集群上，对集群进行增加数据库节点或减少数据库节点的操作，以满足不同的业务需求场景

术语:

- **扩容**：指通过一系列操作，在原有集群上，增加数据库节点。
- **缩容**：指通过一系列操作，在原有集群上，减少数据库节点。

## 前提

本文假设你已有一个 MogDB 数据库集群，可以是单机或多节点。且集群名为 `CLUSTER_NAME`

> 数据库集群安装请参考: [安装数据库](/install.md)

## 扩容

### 查看帮助文档

```
# ptk cluster scale-out -h

对一个MogDB集群进行扩容

Usage:
  ptk cluster scale-out [flags]

Examples:
ptk cluster -n CLUSTER_NAME scale-out -c add.yaml [--force] [--skip-check-distro] [--skip-check-os] [--skip-create-user]

Flags:
  -c, --config string       扩缩容配置文件路径
      --default-guc         不自动优化 GUC 参数，使用数据库默认参数
      --force               如果扩缩容执行失败或被中断，可通过 --force 参数再次执行操作，PTK 会清理掉旧的脏数据后操作
      --gen-template        生成一个扩容模板配置
  -h, --help                help for scale-out
  -n, --name string         集群名称
      --skip-check-distro   跳过检查系统发行版
      --skip-check-os       跳过系统检查
      --skip-create-user    跳过创建用户
  -t, --timeout duration    操作超时时间 (default 10m0s)
```

### 创建扩容的配置文件

```
ptk cluster scale-out --gen-template > add.yaml
```

此时已创建一个扩容的配置文件 add.yaml。请按需修改该配置文件。

> 说明：此文件同安装数据库集群的配置文件中 db_server 相同。这是因为此功能为扩容，Global的相关配置保持与原集群一致，以更好的统一管理。

### 进行扩容操作

```
ptk cluster -n CLUSTER_NAME scale-out -c add.yaml 
```

**扩容的部分说明：**

- --force：**该参数仅在扩容过程中失败或被中断后再次进行扩容操作时使用，同时会清理Global配置下的相关目录。因此使用该参数时，请一定确保原集群Global的配置中的相关目录下没有私有重要数据，以免造成数据丢失！**

## 缩容

### 查看帮助

```
# ptk cluster scale-in -h

对一个MogDB集群进行缩容

Usage:
  ptk cluster scale-in [flags]

Examples:
ptk cluster -n CLUSTER_NAME scale-in  -H 10.0.0.1 [--clear-user] [--clear-dir] [--clear-env] [-t 120]

Flags:
      --clear-dir          清理数据库相对目录
      --clear-env          清理环境变量
      --clear-user         删除系统用户
  -h, --help               help for scale-in
  -H, --host stringArray   要移除的机器IP
  -n, --name string        集群名称
  -t, --timeout duration   操作超时时间 (default 5m0s)
```

### 进行缩容操作

```
ptk cluster -n CLUSTER_NAME scale-in -H ${DELETE_IP1} -H ${DELETE_IP2}
```

会将IP为 `DELETE_IP1` 和 `DELETE_IP2` 的数据库实例从集群中移除。

**缩容的部分说明**：

- 关于一主多备多级联的缩容

假设集群拓扑结构为

```
         standby1 —— cascade_standby1
       /
primary 
       \
         standby2 —— cascade_standby2
```

缩容删除standby1时，最终拓扑结构变为

```
         standby1   cascade_standby1
                  /
primary          /
       \        /
         standby2 —— cascade_standby2
```

此时standby1不在集群中，而是独立出来，与原集群没有任何关联。

也就是说，如果被删除的是备库，且该备备库下有级联节点，且除了被删的备库之外，还有其他备库。会为其下的级联备库随机选一个未被删除的备库连接。

- 关于一主一备一级联的缩容

假设集群拓扑结构为

```
primary —— standby1 —— cascade_standby1
```

缩容删除standby1时，最终拓扑结构变为

```
primary    standby1    cascade_standby1    
```

此时3个节点直接没有任何关联，均独立。