---
title: "ptk cluster"
summary: ptk cluster
author: ptk
date: 2022-06-28
---

## ptk cluster

数据库集群管理的操作

## 语法

```shell
ptk cluster [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 子命令

- [ptk cluster status](ptk-cluster-status.md): 查询集群或实例状态
- [ptk cluster start](ptk-cluster-start.md): 启动数据库集群或实例
- [ptk cluster stop](ptk-cluster-stop.md): 停止数据库集群或实例
- [ptk cluster restart](ptk-cluster-restart.md): 重启数据库集群或实例
- [ptk cluster scale-out](ptk-cluster-scale-out.md): 集群扩容
- [ptk cluster scale-in](ptk-cluster-scale-in.md): 集群缩容
- [ptk cluster install-plugin](ptk-cluster-install-plugin.md): 插件安装
