---
title: "ptk cluster install-plugin"
summary: ptk cluster install-plugin
author: ptk
date: 2022-08-01
---

## ptk cluster install-plugin

安装MogDB插件

## 语法

ptk cluster install-plugin [flags]

## 选项

### -H, --host strings

- 指定待安装的主机 IP
- 数据类型：字符串
- 默认将安装到集群中的所有主机
- 可以多次指定

### -n, --name string

- 集群名称
- 数据类型：字符串

### --override

- 是否覆盖现有插件文件
- 数据类型：布尔值

### -p, --pkg string

- 本地插件包路径，适用于离线安装
- 数据类型：字符串

### -P, --plugin strings

- 指定要安装的插件名称，默认会安装所有插件
- 数据类型：字符串
- 可以多次指定

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

操作日志
