---
title: "ptk candidate db"
summary: ptk candidate db
author: ptk
date: 2022-6-28
---

## ptk candidate db

列出支持安装的 MogDB 数据库版本列表

## 语法

```shell
ptk candidate db [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出含有 `software` 和 `version` 字段的表格
