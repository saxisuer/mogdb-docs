---
title: "ptk completion zsh"
summary: ptk completion zsh
author: ptk
date: 2022-06-28
---

## ptk completion zsh

为 zsh shell 生成自动补全脚本。

如果您的环境中还没有启用 shell 补全功能，那么您将需要
启用它。可以执行以下操作一次:

```shell
echo "autoload -U compinit;compinit" >> ~/.zshrc
```

在当前的 shell 会话中加载生效:

```shell
source <(ptk completion zsh); compdef _ptk ptk
```

要为每个新会话加载完成，需操作一次：

**Linux:**

```shell
ptk completion zsh > "${fpath[1]}/_ptk"
```

**macOS:**

```shell
ptk completion zsh > /usr/local/share/zsh/site-functions/_ptk
```

您需要启动一个新的 shell 才能使这个设置生效。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
