---
title: "ptk install"
summary: ptk install
author: ptk
date: 2022-6-28
---

## ptk install

基于给定的拓扑配置部署 MogDB 数据库集群

## 语法

```shell
ptk install -f CONFIG_YAML [flags]
```

## 选项

### -y, --assumeyes

- 自动对所有提问回复Yes
- 数据类型：布尔值
- 该选项默认关闭，在命令行中添加该参数使其生效
- 默认值：false

### --db-version string

- 指定数据库的版本（仅支持有网络情况下在线安装）
- 数据类型：字符串
- 可通过 'ptk candidate mogdb-server' 查看支持的 Mogdb Server 版本列表 
- 默认值："3.0.0"

### -e, --env strings

- 指定键值对形式的环境变量字符串，所有值将被添加到安装后系统用户环境变量中
- 数据类型：字符串列表
- 该选项支持多次指定，对应不同的值

### -p, --pkg string

- 数据库安装包的文件路径或者下载地址
- 数据类型：字符串
- 该选项默认为空，使用 `--db-version` 默认值对应的安装包。该选项支持本地磁盘文件路径，同时也支持网络下载链接(需包含 `http://` 或 `https://` 的完整链接)

### --no-cache

- 不使用本地缓存的安装包
- 数据类型：布尔值
- 该选项默认关闭，PTK 会在本地元数据中查找是否有缓存的已经下载好的安装包，如果存在就直接使用缓存的安装包进行安装
- 默认值：false

### --launch-db-timeout duration

- 启动数据库超时时间
- 数据类型：Duration
- 该选项指定了在安装完成后启动数据的超时时间，超过指定时长后，报超时错误退出，需用户手动查看数据库的启动状态或手动启动。
- Duration 类型支持的单位："h" 小时, "m" 分支, "s" 秒, "ms" 毫秒
- 默认值："1m"

### --pre-run string

- 安装数据库**之前**需要执行的bash脚本路径
- 数据类型：字符串
- 该选项提供了一个 pre-hook 功能，允许用户提供一个bash脚本，PTK将在部署数据库前在集群内每台机器上运行

### --post-run string

- 数据库**启动后**需要执行的bash脚本路径
- 数据类型：字符串
- 该选项提供了一个 post-hook 功能，允许用户提供一个bash脚本，PTK将在启动数据库成功后在集群内每台机器上运行

### --skip-check-distro

- 跳过检查操作系统发行版
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。一般用于测试场景，在PTK不支持的操作系统上测试数据库能够使用。
- 默认值：false

### --skip-check-os

- 跳过检查系统环境
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。当用户由于特殊原因无法直接修改系统参数时，PTK支持跳过检查进行安装。否则存在 abnormal 级别的检查结果时，安装过程会中支。
- 默认值：false

### --skip-create-user

- 跳过创建系统用户
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。当指定该选项时，需确保要安装的服务器上用户已经存在，否则会中止安装。当指定跳过创建用户时，PTK不自动创建互信，需用户自己创建SSH互信
- 默认值：false

### --install-cm

- 安装 CM 相关组件 (3节点以上生效)
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。cm 组件提供了一层高可用和运维的能力，当用户选择其他高可用组件时，PTK支持默认不安装CM组件，以普通方式通过 gs_ctl 启动主备数据库。该选项针对3节点以上的集群，3节点以下，不支持安装 CM 组件。注意：由于 PTK 是通过 gs_ctl 工具来管理数据库实例的，所以安装了CM组件后，启停管理功能暂时不支持。
- 默认值：false

### --skip-launch-db

- 安装完成后不启动数据库
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。需注意的是，当指定了该选项安完成后，再次手动启动后，需用户进行一下初始用户密码的修改，否则通过 [`ptk cluster status`](ptk-cluster-status.md) 查看集群状态时，可能会出现 `ERROR:  Please use "ALTER ROLE user_name PASSWORD 'password';" to set the password of user xx before other operation!` 的错误。
- 默认值：false

### --skip-rollback

- 安装过程出错后，不进行操作的回滚
- 数据类型：布尔值
- 该选项默认关闭，在命令行添加该选项生效。PTK 默认会在执行安装出错时，回滚掉之前的所有操作，以保证系统中不会有安装过程的遗留文件或配置。需注意的时，该操作也依赖网络，需SSH保持畅通，如果回滚过程断网或其他报错，回滚也会中止，需人工进行清理操作。
- 默认值：false

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

显示安装过程的日志信息，默认日志级别为 INFO，可以通过全局参数 `--log-level` 来修改日志级别。
最后输出安装状态的表格，表格包含下列字段：

- host: 服务器IP
- stage: 安装阶段
- status: 安装状态
- message: 安装结果信息

安装状态值：

- precheck_success: 预检查成功
- precheck_failed: 预检查失败
- initial_db_success: 初始化数据库成功
- initial_db_failed: 初始化数据库失败
- initial_cm_success: 初初始化 CM 组件成功
- initial_cm_failed: 初始化 CM 组件失败
- rollback_success: 回滚成功
- need_rollback_manually: 需手动回滚
- start_success: 启动数据库成功
- start_failed: 启动数据库失败
- need_start_manually: 需手动启动数据库
