---
title: "ptk completion powershell"
summary: ptk completion powershell
author: ptk
date: 2022-06-28
---

## ptk completion powershell

为 powershell 生成自动补全脚本。

在当前的shell会话中加载生效:

```powershell
ptk completion powershell | Out-String | Invoke-Expression
```

要为每个新会话实现补全操作，请将上述命令的输出添加到你的 powershell profile。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
