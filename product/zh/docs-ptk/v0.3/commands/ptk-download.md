---
title: "ptk candidate"
summary: ptk candidate
author: ptk
date: 2022-6-28
---

## ptk download

在线下载 MogDB 数据库安装包

## 语法

```shell
ptk candidate [flags]
```

## 选项

### -O，--os

- 指定系统ID（ID 可通过 [ptk candidate os](ptk-candidate-os.md) 指令查看）
- 数据类型：整型
- 若不指定，默认自动探测当前系统

### -V，--db-version

- 指定 MogDB 数据库版本（ 可通过 [ptk candidate db](ptk-candidate-db.md) 指令查看支持的版本列表）
- 数据类型：字符串
- 默认值：PTK发布时的最新版

### --output

- 下载到本地的文件路径
- 数据类型：字符串
- 默认值为空

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出文件下载进度日志
