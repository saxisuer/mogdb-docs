---
title: "ptk cluster scale-in"
summary: ptk cluster scale-in
author: ptk
date: 2022-08-01
---

## ptk cluster scale-in

对一个MogDB集群进行缩容

## 语法

```shell
ptk cluster -n CLUSTER_NAME scale-in  -H HOST_IP [--stop-db] [--clear-user] [--clear-dir] [--clear-env] [-t 120]

```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -H, --host strings

- 要移除的机器IP
- 数据类型：字符串
- 可以多次指定

### --stop-db

- 停止数据库
- 数据类型：布尔值

### --clear-user

- 删除系统用户
- 数据类型：布尔值

### --clear-env

- 清理环境变量
- 数据类型：布尔值

### --clear-dir

- 清理数据库相对目录
- 数据类型：布尔值

### -t, --timeout duration

- 执行超时时间
- 数据类型：Duration
- Duration 类型支持的单位："h" 小时, "m" 分支, "s" 秒, "ms"
- 默认值："5m"

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

操作日志
