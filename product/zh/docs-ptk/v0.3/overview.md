---
title: PTK 使用文档
summary: PTK 使用文档
author: Yao Qian
date: 2022-05-30
---

## PTK 简介

PTK (Provisioning Toolkit)是一款针对 MogDB 数据库开发的软件安装和运维工具，旨在帮助用户更便捷地安装部署MogDB数据库。

如果用户想要运行 MogDB 或者 MogDB 的相关组件时，仅需要执行一行命令即可实现。

## 使用场景

- 开发人员快速启动多个本地MogDB环境
- 用户通过PTK快速安装部署MogDB
- DBA日常运维使用
- 第三方运维平台集成

## 通过 PTK 支持安装 MogDB 的操作系统

> 最新列表请通过 `ptk candidate os` 查看

 id  |                 os                  |     tested version(s)
-----|-------------------------------------|----------------------------
   1 | CentOS 7 (x86_64)                   | 7.6.1810 (Core)
   2 | openEuler 20 (x86_64)               | 20.03 LTS
   3 | openEuler 20 (arm64)                | 20.03 LTS
   4 | openEuler 22 (x86_64)               | 22.03 LTS
   5 | openEuler 22 (arm64)                | 22.03 LTS
   6 | Kylin V10 (x86_64)                  | V10 (Tercel)
   7 | Kylin V10 (arm64)                   | V10 (Tercel)
   8 | UOS 20 A (x86_64)                   | 1002a/1020a/1050a
   9 | UOS 20 A (arm64)                    | 1050a (kongzi)
  10 | Ubuntu 20 (x86_64)                  | 20.04.3 LTS (Focal Fossa)
  11 | CentOS 8 (arm64)                    | 8.0.1905 (Core)
  12 | CentOS 8 (x86_64)                   | 8.0.1905 (Core)
  13 | Red Hat Enterprise Linux 7 (x86_64) | 7.5 (Maipo)
  14 | Red Hat Enterprise Linux 8 (x86_64) | 8.5 (Ootpa)
  15 | EulerOS 2 (x86_64)                  | 2.0 (SP3)
  16 | EulerOS 2 (arm64)                   | 2.0 (SP3)
  18 | SLES 12SP5 (x86_64)                 | 12SP5
  19 | Oracle Linux 7 (x86_64)             | 7.9 (Maipo)
  20 | Oracle Linux 8 (x86_64)             | 8.6 (Ootpa)
  21 | Rocky Linux 8 (x86_64)              | 8.5 (Green Obsidian)
  22 | NeoKylin V7 (x86_64)                | V7Update6
  23 | UOS 20 D/E (x86_64)                 | 1040d (fou)
  24 | UOS 20 D/E (arm64)                  | 1040d (fou)
  25 | Ubuntu 22 (x86_64)                  | 22.04 (Jammy Jellyfish)

*更多操作系统适配测试中...*
