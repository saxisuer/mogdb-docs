---
title: 快速上手
summary: 快速上手
author: Yao Qian
date: 2022-06-02
---

## 快速上手

本文介绍如何快速使用PTK安装 MogDB 数据库。

> PTK工具本身可以在多种操作系统中运行，支持Linux，macOS，Windows，但是由于 MogDB 目前仅支持 在Linux 系统中运行，因此需确保要运行MogDB数据库的服务器为 Linux 操作系统。MogDB支持的Linux操作系统种类和版本参看[通过 PTK 支持安装 MogDB 的操作系统](./overview.md#通过-ptk-支持安装-mogdb-的操作系统)。

### 1. 下载并安装 PTK

执行以下命令在线安装：

```shell
curl --proto '=https' --tlsv1.2 -sSf https://cdn-mogdb.enmotech.com/ptk/install.sh | sh
```

安装完成后会提示如下信息（根据运行PTK的 SHELL 类型不同，信息会有所差异）

```
info: downloading ptk package
Detected shell: bash
Shell profile:  /root/.bashrc
ptk has been added to PATH in /root/.bashrc
open a new terminal or source /root/.bashrc to active it
Installed path: /root/.ptk/bin/ptk
```

可以通过提示的 source 指令或打开一个新的终端窗口来使 PTK  PATH 环境变量生效。

以 bash 为例：

```shell
source $HOME/.bashrc
```

### 2. 通过PTK安装MogDB

#### 2.1 准备拓扑配置文件

PTK 安装需用户提供一个如下内容的配置文件 config.yaml

```yaml
# config.yaml
global:
    cluster_name: mogdb1
    user: omm
    group: omm
    base_dir: /opt/mogdb
db_servers:
    - host: 127.0.0.1
      db_port: 26000
```

如果全部使用默认值，则PTK 执行以下行为：

* 在本机安装数据库
* 运行数据库的操作系统用户为omm ，用户组名称也是omm，该用户没有默认密码；
* 数据库安装在 /opt/mogdb目录下，在该目录下会创建4个目录：app, data, log, tool，分别用于存储数据库软件、数据文件、数据库日志和数据库相关工具；
* 数据库监听端口为26000；

如果需要在其它服务器上安装MogDB，或者是安装主备架构的MogDB，则需要在config.yaml中[配置更多信息](./config.md)。详细信息可参考 [使用手册](./usage/usage-config.md)。

以下为安装一主一备环境的配置示例。

```yaml
# config.yaml
global:
    cluster_name: mogdb_cluster1
    user: omm
    group: omm
    base_dir: /opt/mogdb
db_servers:
    - host: 192.168.0.1
      db_port: 26000
      role: primary
      ssh_option:
        port: 22
        user: root
        password: [此处填写SSH登录密码]
    - host: 192.168.0.2
      db_port: 26000
      role: standby
      ssh_option:
        port: 22
        user: root
        password: [此处填写SSH登录密码]
```

#### 2.2对本机进行系统检查

```shell
ptk checkos -f config.yaml
```

确保输出的检查结果均为 `OK` 或者 `Warning` ，如果有 `Abnormal` 出现，需用户根据日志提示先修正系统参数。

#### 2.3 执行安装

```shell
ptk install -f config.yaml
```

默认会安装当前已经正式发布的 MogDB 最新版本，版本号可以在 MogDB 官网下载页面查询。安装过程中会提示用户输入数据库初始用户的密码，请用户自行记录并安全保存。PTK 自动完成所有安装操作后，会启动数据库实例。

PTK 也支持自定义安装包，可以通过手工下载安装包，指定安装。比如执行以下命令，将使用当前目录下的该安装包进行数据库安装。

```shell
ptk install -f config.yaml --pkg ./MogDB-3.0.0-openEuler-arm64.tar.gz
```

安装成功后，可以通过 `ptk ls` 来查看安装的实例信息。

### 3. 访问数据库

```
su - omm
gsql -d postgres -p 26000
```

## 探索更多

以上示例中的拓扑配置文件以最简化模板提供，方便用户快速体验 MogDB, 若想要更全面的描述集群拓扑，请访问[拓扑文件配置](./config.md)章节了解更多参数细节。
