---
title: PTK命令参考
summary: PTK命令参考
author: Yao Qian
date: 2022-06-28
---

## PTK

PTK是一款部署和管理MogDB数据库集群的命令行工具

## 语法

```shell
ptk [flags] <command> [args...]
```

## 全局选项

全局选项可以被应用到 PTK 下的任意子命令中

### -f, --file string

- 指定集群配置文件
- 数据类型：字符串
- 用户在安装数据库时需明确指定拓扑配置文件路径，该配置可以通过 [`ptk template`](commands/ptk-template.md) 子命令创建。

### --log-file string

- 运行日志输出到文件路径
- 数据类型：字符串
- 该选项默认为空，指定该选项时，PTK会在终端输出日志的同时输出日志到指定文件中。

### --log-format string

- 指定日志的输出格式
- 数据类型：字符串
- PTK 的日志输出类型目前仅支持 `text` 文本格式和 `json` JSON格式
- 默认值："text"

### --log-level string

- 指定输出日志的级别
- 数据类型：字符串
- 支持的日志级别有： [debug, info, warning, error, panic]
- 默认值："info"

### -v, --version

- 显示 PTK 的版本信息
- 数据类型：布尔值
- 默认值：false

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 更多子命令

* [ptk candidate](commands/ptk-candidate.md) - 打印 PTK 支持的软件版本列表
* [ptk checkos](commands/ptk-checkos.md) - 检查集群服务器系统依赖是否满足安装数据库需求
* [ptk encrypt](commands/ptk-encrypt.md) - 提供一个便捷的方式来加密您的文本或密码
* [ptk env](commands/ptk-env.md) - 打印 PTK 环境变量信息
* [ptk install](commands/ptk-install.md) - 基于给定的拓扑配置部署MogDB数据库集群
* [ptk ls](commands/ptk-ls.md) - 列出所有MogDB集群列表
* [ptk self](commands/ptk-self.md) - 操作 PTK 自身安装包
* [ptk cluster](commands/ptk-cluster.md) -数据库集群操作
* [ptk template](commands/ptk-template.md) - 打印配置模板
* [ptk uninstall](commands/ptk-uninstall.md) - 卸载 MogDB 数据库集群
* [ptk gen-om-xml](commands/ptk-gen-om-xml.md) - 生成 gs_om 的 XML 配置文件
* [ptk version](commands/ptk-version.md) - 打印 PTK 版本
* [ptk completion](commands/ptk-completion.md) - 为指定的 shell 生成自动补全脚本
