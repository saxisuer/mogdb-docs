---
title: "ptk completion fish"
summary: ptk completion fish
author: ptk
date: 2022-06-28
---

## ptk completion fish

为 fish 生成自动补全脚本。

在当前的 shell 会话中加载生效:

```shell
ptk completion fish | source
```

要为每个新会话加载生效，需执行一次:

```shell
ptk completion fish > ~/.config/fish/completions/ptk.fish
```

您需要启动一个新的 shell 才能使这个设置生效。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
