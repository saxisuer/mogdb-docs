---
title: "ptk uninstall"
summary: ptk uninstall
author: ptk
date: 2022-06-28
---

## ptk uninstall

卸载 MogDB 数据库集群

## 语法

```shell
ptk uninstall [flags]
```

## 选项

### -n, --name string

- 指定要卸载的集群名称
- 数据类型：字符串
- 该选项和全局选项 `-f` 需至少指定一个，来告诉 PTK 要卸载的目标集群

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 示例

```shell
# 通过配置文件卸载
ptk uninstall -f config.yaml

# 通过集群名称卸载
ptk uninstall --name CLUSTER_NAME
```

## 输出

输出卸载过程步骤日志
