---
title: "ptk completion bash"
summary: ptk completion bash
author: ptk
date: 2022-06-28
---

## ptk completion bash

为 bash shell 生成自动补全脚本。

这个脚本依赖于 'bash-completion' 包。

如果还没有安装，您可以通过操作系统的包管理器安装它。

在当前的 shell 会话中加载生效:

```shell
Source <(ptk completion bash)
````

要为每个新会话加载生效，执行一次:

**Linux:**

```shell
ptk completion bash > /etc/bash_completion.d/ptk
````

**macOS:**

```shell
ptk completion bash > /usr/local/etc/bash_completion.d/ ptk
```

您需要启动一个新的 shell 才能使这个设置生效。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
