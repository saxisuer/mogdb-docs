---
title: "ptk env"
summary: ptk env
author: ptk
date: 2022-06-28
---

## ptk env

打印 PTK 的环境变量信息

## 语法

```shell
ptk env [key1 key2 ... keyN] [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

以键值对的形式显示环境变量的值
