---
title: "ptk template create"
summary: ptk template create
author: ptk
date: 2022-6-28
---

## ptk template create

以交互的方式创建配置模板

## 语法

```shell
ptk template create [flags]
```

## 选项

### -o, --output string

- 指定用于存储模板的文件路径
- 数据类型：字符串
- 默认值："config.yaml"

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
