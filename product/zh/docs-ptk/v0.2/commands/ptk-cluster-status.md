---
title: "ptk cluster status"
summary: ptk cluster status
author: ptk
date: 2022-06-28
---

## ptk cluster status

查询数据库集群或实例的运行状态

## 语法

```shell
ptk cluster status -n string [flags]
```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -H, --host string

- 要操作的实例IP
- 数据类型：字符串
- 该选项是为了支持允许查询单个实例的状态，默认查询整个集群的所有实例状态

### --detail

- 是否显示详细的信息
- 数据类型：布尔值
- 默认值：false

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

分模块输出状态信息

- Cluster State: 集群状态
- Datanode State: 每个实例节点状态
- (Datanode Detail): 节点详细信息，默认不输出，指定 `--detail` 选项时显示
