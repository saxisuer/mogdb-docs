---
title: "ptk ls"
summary: ptk ls
author: ptk
date: 2022-06-28
---

## ptk ls

列出当前用户已经安装的所有数据库列表，PTK以使用用户作为区分，所以不同用户之间仅能看到自己安装的列表

## 语法

```shell
ptk ls [flags]
```

## 选项

### -n, --only-name

- 仅列出集群的名称
- 数据类型：布尔值
- 该选项默认关闭，显示集群的静态数据，在命令行中添加该选项使其生效。
- 默认值：false

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出集群列表的表格，表格包含以下字段：

- cluster_name: 集群名称
- instances: 集群内的实例列表
- user: 运行的操作系统用户
- data_dir: 实例的数据目录
- db_version: 数据库版本
