---
title: "ptk encrypt"
summary: ptk encrypt
author: ptk
date: 2022-6-28
---

## ptk encrypt

加密指定的文本或密码，主要用于配置文件中的密码字段，PTK 不推荐将明文存储在配置文件中，所以需通过该命令来加密后写入

## 语法

```shell
ptk encrypt TEXT1 [TEXT2 TEXT3...] [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

以键值对形式输出原文和密文，加密多个原文时，会以多行输出。
