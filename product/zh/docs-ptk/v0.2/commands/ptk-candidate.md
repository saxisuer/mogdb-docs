---
title: "ptk candidate"
summary: ptk candidate
author: ptk
date: 2022-6-28
---

## ptk candidate

打印 PTK 支持的软件版本列表，默认打印支持的 MogDB Server 版本列表 (同 [ptk candidate db](ptk-candidate-db.md))

## 语法

```shell
ptk candidate [flags] [command]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出含有 `software` 和 `version` 字段的表格

## 更多子命令

- [ptk candidate db](ptk-candidate-db.md) - 列出支持安装的 MogDB 数据库版本列表
- [ptk candidate os](ptk-candidate-os.md) - 列出支持安装 MogDB 的操作系统
