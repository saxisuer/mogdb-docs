---
title: "ptk completion"
summary: ptk completion
author: ptk
date: 2022-06-28
---

> 注意：该命令非必知的功能项，供专业人士参考。

## ptk completion

为指定的 shell 生成 ptk 的自动补全脚本。

有关如何使用生成的脚本的详细信息，请参阅每个子命令的帮助。

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 子命令

- [ptk completion bash](ptk-completion-bash.md): 生成bash的自动补全脚本
- [ptk completion fish](ptk-completion-fish.md): 生成fish的自动补全脚本
- [ptk completion powershell](ptk-completion-powershell.md): 生成powershell的自动补全脚本
- [ptk completion zsh](ptk-completion-zsh.md): 生成zsh的自动补全脚本
