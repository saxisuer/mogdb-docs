---
title: "ptk candidate os"
summary: ptk candidate os
author: ptk
date: 2022-6-28
---

## ptk candidate os

列出支持安装 MogDB 的操作系统

## 语法

```shell
ptk candidate os [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

输出含有 `id` 和 `os` 字段的表格
