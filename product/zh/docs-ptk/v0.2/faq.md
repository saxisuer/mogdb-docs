---
title: FAQ
summary: FAQ
author: Yao Qian
date: 2022-05-30
---

## FAQ

### checkos 检查 core_pattern 报错：can not combine with 'abrt-hook-ccpp'

> 使用 abrt 服务代理coredump 文件，有可能导致数据库core文件丢失或者数据库宕机的风险。所以在 PTK 里面都是禁止开启的。可通过修改 kernel.core_pattern 参数修复。
> sysctl -w kernel.core_pattern=/var/log/coredump/%e.%p.%u.%t.core

### PTK安装后，是否还支持 gs_om 操作？

> 可以。ptk 安装后的集群兼容 gs_om，生成了 gs_om 需要使用到的`cluster_static_config` 等文件。
> 注意：`gs_om` 的操作需要用户互信，PTK 安装后会自动建立，若未创建需用户自行配置。

### ptk 如何查看目前可支持安装的mogdb版本

> `ptk candidate db`

### `gs_ctl query` 查询的状态报错 `FATAL:  semctl(578486307, 7, SETVAL, 0) failed: Invalid argument`

> 请检查服务器 /etc/systemd/logind.conf 中 **RemoveIPC** 的值，修改为 `no` 后重启服务器

### ptk 如何查看目前可支持安装的操作系统

> `ptk candidate os`
