---
title: PTK 使用文档
summary: PTK 使用文档
author: Yao Qian
date: 2022-05-30
---

## PTK 简介

PTK (Provisioning Toolkit)是一款针对 MogDB 数据库开发的软件安装和运维工具，旨在帮助用户更便捷地安装部署MogDB数据库。

如果用户想要运行 MogDB 或者 MogDB 的相关组件时，仅需要执行一行命令即可实现。

## 使用场景

- 开发人员快速启动多个本地MogDB环境
- 用户通过PTK快速安装部署MogDB
- DBA日常运维使用
- 第三方运维平台集成

## 通过 PTK 支持安装 MogDB 的操作系统

> 最新列表请通过 `ptk candidate os` 查看

| 操作系统        | CPU架构      |
| ----------- | -------------- |
| CentOS 7 |x86_64|
| CentOS 8 |x86_64|
| CentOS 8 |arm64|
| EulerOS 2 |arm64|
| EulerOS 2 |x86_64|
| Kylin V10 |arm64|
| Kylin V10 |x86_64|
| NeoKylin V7 |x86_64|
| Oracle Linux Server 7 |x86_64|
| Oracle Linux Server 8 |x86_64|
| openEuler 20 |arm64|
| openEuler 20 |x86_64|
| openEuler 22 |arm64|
| openEuler 22 |x86_64|
| RedHat 7 |x86_64|
| Rocky Linux 7 |x86_64|
| Rocky Linux 8 |x86_64|
| SLES 12 |arm64|
| Ubuntu 18 |x86_64|
| UOS 20 | arm64|
| UOS 20 |x86_64|

*更多操作系统适配测试中...*
