<!-- markdownlint-disable MD007 -->
<!-- markdownlint-disable MD041 -->
# 文档

## PTK 文档目录

+ [关于PTK](/overview.md)
+ [快速上手](/quick-start.md)
+ [软件安装](/install.md)
+ [使用手册](/usage.md)
+ [拓扑文件配置](/config.md)
+ 命令介绍
  + [ptk](/command.md)
  + [ptk candidate](/commands/ptk-candidate.md)
    + [ptk candidate mogdb-server](/commands/ptk-candidate-mogdb-server.md)
    + [ptk candidate os](/commands/ptk-candidate-os.md)
  + [ptk checkos](/commands/ptk-checkos.md)
  + [ptk encrypt](/commands/ptk-encrypt.md)
  + [ptk env](/commands/ptk-env.md)
  + [ptk template](/commands/ptk-template.md)
    + [ptk template create](/commands/ptk-template-create.md)
  + [ptk install](/commands/ptk-install.md)
  + [ptk ls](/commands/ptk-ls.md)
  + [ptk cluster](/commands/ptk-cluster.md)
    + [ptk cluster start](/commands/ptk-cluster-start.md)
    + [ptk cluster stop](/commands/ptk-cluster-stop.md)
    + [ptk cluster status](/commands/ptk-cluster-status.md)
    + [ptk cluster restart](/commands/ptk-cluster-restart.md)
  + [ptk self](/commands/ptk-self.md)
    + [ptk self upgrade](/commands/ptk-self-upgrade.md)
  + [ptk uninstall](/commands/ptk-uninstall.md)
  + [ptk gen-om-xml](/commands/ptk-gen-om-xml.md)
  + [ptk version](/commands/ptk-version.md)
  + [ptk completion](/commands/ptk-completion.md)
+ [故障排查](/debug.md)
+ [FAQ](/faq.md)
+ [发布记录](/release.md)
