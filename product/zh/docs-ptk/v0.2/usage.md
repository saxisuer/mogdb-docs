---
title: 使用手册
summary: 使用手册
author: Yao Qian
date: 2022-05-30
---

## 查看帮助

当安装成功 PTK 以后，可以在终端运行 `ptk -h` 来查看帮助信息，关于命令参数的含义说明，请参考[命令介绍}](command.md)

## 查看版本

可以通过 `ptk -v` 来查看 PTK 版本信息，以下为示例：

```shell
$ ptk -v
PTK Version: v0.1.0
Go Version: go1.17.1
Build Date: 2022-05-27T09:32:14Z
Git Hash: 6aba9ea
```

从版本信息可以得知 PTK 的版本号，编译的 Golang 版本，构建时间以及构建时的哈希值。

## 创建拓扑配置文件

在安装数据库之前，需通过 PTK 来创建一个配置文件，用于描述期望的数据库集群拓扑结构。

如果要安装单实例数据库，可以通过以下命令生成配置文件：

```shell
ptk template -l > config.yaml
```

如果要安装多实例集群，则可以通过以下命令生成配置文件：

```shell
ptk template > config.yaml
```

运行后会在当前文件夹内生成 config.yaml 文件，用户需修改文件内服务 HOST 字段为实际的 IP，并且如果存在密码字段，用户需通过 [ptk encrypt](commands/ptk-encrypt.md) 指令加密后写入配置文件。

> 注意：
> 如果您希望 PTK 使用非 root 用户来连接目标机器操作, 请确保运行该用户具有免密的 sudo 权限，
> 可在目标机器上的 /etc/sudoers 文件中新增如下一行（USERNAME 替换为SSH连接用户名） ：
> `[USERNAME]  ALL=(ALL)  NOPASSWD:ALL`

另外，如果不确定配置文件中每个字段的含义，PTK 也支持通过交互式方式创建配置文件，只需执行如下命令:

```shell
ptk template create
```

## 系统检查

在安装数据库之前，你需要先对要安装数据库的服务器进行一下系统参数以及软件依赖等检查，确保安装过程可以顺利执行。

PTK 内置了 `checkos` 的指令来辅助你进行检查操作，在检查过程中，PTK 会对服务器上以下这些模块进行检查：

| 类别编号 | 类别         | 检查项                        | 说明                 |
| -------- | ------------ | ----------------------------- | -------------------- |
| A1       | 系统版本     | Check_OS_Version              | 检查系统版本         |
| A2       | 内核版本     | Check_Kernel_Version          | 检查内核版本         |
| A3       | 字符集       | Check_Unicode                 | 检查字符集           |
| A4       | 时区         | Check_TimeZone                | 检查时区             |
| A5       | 内存交换区   | Check_Swap_Memory_Configure   | 检查 swap 内存配置   |
| A6       | sysctl参数   | Check_SysCtl_Parameter        | 检查 sysctl 参数     |
| A7       | 文件系统     | Check_FileSystem_Configure    | 检查文件系统配置     |
| A8       | 磁盘配置     | Check_Disk_Configure          | 检查磁盘配置         |
| A9       | 预读块设置   | Check_BlockDev_Configure      | 检查块设备配置       |
|          |              | Check_Logical_Block           | 检查逻辑块           |
| A10      | IO调度       | Check_IO_Request              | 检查 IO 请求参数     |
|          |              | Check_Asynchronous_IO_Request | 检查异步 IO 请求参数 |
|          |              | Check_IO_Configure            | 检查 IO 配置         |
| A11      | 网络配置     | Check_Network_Configure       | 检查网络配置         |
| A12      | 时钟一致性   | Check_Time_Consistency        | 检查时钟一致性       |
| A13      | 防火墙配置   | Check_Firewall_Service        | 检查防火墙配置       |
| A14      | 透明大页配置 | Check_THP_Service             | 检查透明大页配置     |
| A15      | 依赖包     | Check_Dependent_Package        | 检查数据库系统安装依赖 |
| A16      | CPU指令集   | Check_CPU_Instruction_Set        | 检查CPU指令集 |
| A17      | 端口状态   | Check_Port        | 检查数据库端口是否被占用 |

在使用 PTK 进行系统检查时，需指定集群配置文件（`-f`）以及指定要检查的内容（`-i`），如果不知道要检查的内容，默认会检查以上表格内的所有项目。

如果需要针对个别类别进行检查，可以通过以逗号连接类别编号来进行指定。

使用示例：

```shell
ptk checkos -i A1,A2,A3   # 要检查多个状态，请按以下格式输入项目：“-i A1,A2,A3”。
ptk checkos -i A          # 检查全部检查项
ptk checkos -i A --detail # 加上--detail 会显示详细信息
```

PTK 的检查结果分为四个等级：

- **OK**：符合预期值，满足安装需求
- **Warning**: 不符合预期值，但可以满足安装需求
- **Abnormal**: 不满足安装要求，可能会导致安装过程失败，需根据 PTK 给出的修改建议脚本，人工执行修改
- **ExecuteError**: 执行错误，该等级表示 PTK 在检查时执行命令失败，可能为用户环境工具缺失或内部BUG，需根据实际错误提示进行修正

在安装数据库之前，需确保所有的检查项都在 **OK** 或 **Warning** 级别，如果存在 **Abnormal** 的检查结果，在安装时会报错退出。

## 数据库集群安装

当有了配置文件，并且系统检查也都通过后，就可以开始数据库安装了。

使用 PTK 安装数据库集群很简单，安装仅需输入以下指令即可：

```shell
$ ptk install -f config.yaml -y
```

`-f` 指定配置文件路径，`-y` 表示自动所有交互式问询回复 Yes。（注：当配置文件中没有配置数据库初始密码时，`-y` 无法跳过询问密码流程）

默认 PTK 会安装 PTK 发布时 MogDB 的最新版，可以通过 `ptk install -h` 查看 `--db-version` 的默认值。

如果你想要指定安装不同的版本或者想要安装自己编译的安装包，PTK 也是支持的。

PTK 安装指令提供了 `--db-version` 参数，来允许用户指定要安装的 MogDB 版本，例如：`ptk install -f config.yaml --db-version 2.0.1 -y` (支持的版本列表请查看 [MogDB 下载页](https://www.mogdb.io/downloads/mogdb/))

如果你希望安装自己编译的安装包或者社区的 openGauss 包，需通过 `-p|--pkg` 参数指定本地安装包路径或者网络下载地址。PTK 会自动下载后从PTK所在的机器分发到所有需要部署的机器上。

更多选项：

- `-e|--env` : 环境变量将被添加到安装后的数据库系统用户的环境变量中
- `--launch-db-timeout`: 启动数据库超时时间，默认 5 分钟。
- `--pre-run`: 支持添加安装前的一个钩子 shell 脚本路径，在集群安装前，自动在集群内所有机器上执行指定的脚本
- `--post-run`: 支持添加安装后的一个钩子 shell 脚本路径，在集群安装成功后，自动在集群内所有机器上执行指定的脚本
- `--skip-check-os`: 跳过系统检查
- `--skip-check-distro` : 跳过系统发行版检查，直接安装
- `--skip-create-user`: PTK 默认会自动创建用户，指定该参数可以跳过创建用户，当指定该参数时，服务器上用户必须已经存在
- `--skip-launch-db`: 安装完成后不启动数据库
- `--skip-rollback`: PTK 在安装过程中如果出现错误，默认会回滚之前的所有操作来清理环境，指定该参数可以禁止回滚操作。
- `--install-cm`: 安装数据库的同时，安装CM组件（仅适用于MogDB版本大于3.0.0且集群内实例数量大于3个）
- `--no-cache`: 不使用本地缓存的安装包，强制从网络重新下载

## 数据库集群卸载

> 注意：
>
> 操作卸载数据库集群之前，请反复确认当前集群是否确实可以卸载！

数据库集群的卸载仅需在 PTK 所在的服务器上执行 `ptk uninstall (-f CONFIG.YAML|--name CLUSTER_NAME)` 指令。

执行卸载时支持通过 config.yaml 指定集群，也可以通过集群名称来指定，使用集群名称来指定的前提是 `ptk ls` 可以正常查询到该集群。

在卸载前，PTK 会交互时的询问用户，以确认要删除的集群拓扑信息，确认是否要连带删除系统用户，以及确认是否要连带删除数据库数据。请在回答每一个问题时，确认你的回答，避免由于误操作导致不可恢复的数据丢失！

在 PTK 执行数据库卸载操作时，如果用户指定了删除数据目录，PTK 仅会删除数据目录，不会删除数据目录所在的父目录，需用户手动清理父目录。

## 数据库集群管理

### 查看集群列表

当数据库集群安装成功后，可以通过 `ptk ls` 来查看当前用户已经安装过的集群列表（由于 PTK 的元数据存储于 `$HOME/.ptk` 目录下，所以仅能查看当前用户安装的，无法查看其他用户安装的集群列表）。

```shell
$ ptk ls
  cluster_name  |     instances      |  user  |     data_dir
----------------+--------------------+--------+-------------------
  single        | 172.16.0.183:21000 | single | /opt/mogdb_single/data
  cluster       | 172.16.0.114:23000 | cluster| /opt/mogdb_cluster/data
                | 172.16.0.190:23000 |        |
```

可以在输出表中得到集群的名称，集群内的实例列表，运行的系统用户以及实例的数据目录。

一旦集群安装成功后，之后在卸载或管理操作时，PTK 支持使用 `-f` 指定配置文件来管理集群，同时也支持 `--name` 指定集群名称来管理集群。

### 启动/停止/重启

PTK 支持集群的整体启动和停止，同时也支持集群内单个实例的启停。

当启停集群时，需指定集群的集群名称或者集群的配置文件。通过集群名称管理集群时，必须该集群是通过 PTK 进行安装的，可以通过 `ptk ls` 查询到即可。

### 查询集群状态

如果想查看数据库集群的运行状态是否正常，可以通过 `status` 子命令查看。

例如：

```shell
$ ptk cluster status --name CLUSTER_NAME

[   Cluster State   ]

database_version  : MogDB-3.0.0
cluster_name      : test
cluster_state     : Normal
current_az        : AZ_ALL

[  Datanode State   ]

   id  |      ip      | port  | user  | instance | db_role |  state
-------+--------------+-------+-------+----------+---------+---------
  6001 | 172.16.0.100 | 26000 |  omm  | dn_6001  | primary | Normal
  6002 | 172.16.0.101 | 26000 |  omm  | dn_6002  | standby | Normal
  6003 | 172.16.0.102 | 26000 |  omm  | dn_6003  | standby | Normal
```

如果项查询更详细的主备复制状态信息，可以通过添加 `--detail` 参数。

## 安装数据库完整流程示例

> 以下示例中 **CLUSTER_NAME** 需替换为实际的集群名

### 创建配置文件

```shell
ptk template create -o config.yaml
```

### 检查服务器

```shell
ptk checkos -f config.yaml -i A
```

### 安装集群

```shell
ptk install -f config.yaml
```

### 查询集群状态

```shell
ptk cluster status --name CLUSTER_NAME

# 查看详细信息
ptk cluster status --name CLUSTER_NAME --detail
```

### 启停集群

```shell
# stop
ptk cluster stop --name CLUSTER_NAME

# stop a node
ptk cluster stop --name CLUSTER_NAME -H HOST_IP
# stop a node with mode(f:fast, i:immediate)
ptk cluster stop --name CLUSTER_NAME -H HOST_IP --mode f

# start
ptk cluster start --name CLUSTER_NAME

# start a node
ptk cluster start --name CLUSTER_NAME -H HOST_IP
# start a node with security-mode
ptk cluster start  --name CLUSTER_NAME -H HOST_IP --security-mode on
```

### 卸载集群

```shell
ptk uninstall --name CLUSTER_NAME
```