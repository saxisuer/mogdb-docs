---
title: 发布记录
summary: 发布记录
author: Yao Qian
date: 2022-06-01
---

## 下载最新版

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## 发布记录

### v0.2.13 (2022.7.8)

#### ChangeLog

- 新增 download 命令支持在线下载MogDB
- ptk candidate mogdb-server 简化为 ptk candidate db
- 修复 cm 集群启停失败问题
- 安装时预安装的检查项移动到 checkos 中
- 部分功能优化和Bug修复

#### 下载地址

- [ptk_0.2.13_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.13/ptk_0.2.13_linux_x86_64.tar.gz)
- [ptk_0.2.13_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.13/ptk_0.2.13_linux_arm64.tar.gz)
- [ptk_0.2.13_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.13/ptk_0.2.13_darwin_x86_64.tar.gz)
- [ptk_0.2.13_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.13/ptk_0.2.13_darwin_arm64.tar.gz)
- [ptk_0.2.13_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.13/ptk_0.2.13_windows_x86_64.tar.gz)

### v0.2.3 (2022.7.4)

#### ChangeLog

- 修复三节点集群状态显示错误
- 支持 oracle linux, rocky linux & neokylin
- 安装结果的表格中增加集群名称，用户和端口字段

#### 下载地址

- [ptk_0.2.3_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_linux_x86_64.tar.gz)
- [ptk_0.2.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_linux_arm64.tar.gz)
- [ptk_0.2.3_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_darwin_x86_64.tar.gz)
- [ptk_0.2.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_darwin_arm64.tar.gz)
- [ptk_0.2.3_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.3/ptk_0.2.3_windows_x86_64.tar.gz)

### v0.2.0 (2022.6.30)

#### ChangeLog

- 支持 MogDB 3.0.0 安装
- 支持 CM 组件安装
- 优化系统检查逻辑
- 配置文件中密码字段强制加密

#### 下载地址

- [ptk_0.2.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_linux_x86_64.tar.gz)
- [ptk_0.2.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_linux_arm64.tar.gz)
- [ptk_0.2.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_darwin_x86_64.tar.gz)
- [ptk_0.2.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_darwin_arm64.tar.gz)
- [ptk_0.2.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.2.0/ptk_0.2.0_windows_x86_64.tar.gz)

### v0.1.0 (2022.6.1)

- [ptk_0.1.0_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_x86_64.tar.gz)
- [ptk_0.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_linux_arm64.tar.gz)
- [ptk_0.1.0_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_x86_64.tar.gz)
- [ptk_0.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_darwin_arm64.tar.gz)
- [ptk_0.1.0_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/v0.1.0/ptk_0.1.0_windows_x86_64.tar.gz)
