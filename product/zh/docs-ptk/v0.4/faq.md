---
title: FAQ
summary: FAQ
author: Yao Qian
date: 2022-05-30
---

## FAQ

### 报错：can not combine with 'abrt-hook-ccpp'

使用 abrt(automatic bug report too) 服务代理 coredump 文件，有可能导致 core 文件丢失或者数据库宕机的风险。

*[什么是abrt服务](https://abrt.readthedocs.io/en/latest/howitworks.html)*

可通过修改 `kernel.core_pattern` 参数的值修复。

```shell
# 值根据需求可自定义
sysctl -w kernel.core_pattern=/var/log/coredump/%e.%p.%u.%t.core
```

### 是否需要预装 Python3？

需要，且版本需为 python3.6 或者 python 3.7，因为 om 包中提供的预编译好的 psutil 动态库目前仅支持这两个版本。

> ptk 和 mogdb 本身不依赖 python3，gs_om、gs_guc 等工具依赖，所以建议预装好，保证环境使用顺利。

### 能否在 PTK 未支持的操作系统版本上使用 PTK 进行数据库安装？

可以。

PTK 本身提供完整通用的安装流程，默认情况下，PTK 会针对客户系统自动判断需要下载的数据库安装包类型，但对于超出 PTK 支持的操作系统或版本，PTK 无法为用户决定应该使用哪个安装包，此时，用户可以通过 ptk install 提供的 `-p` 参数来指定安装包安装，但由于未测试过，不一定能够正常启动，这是很正常的。

用户在决定自己的系统应该尝试使用哪个安装包时，可以参考如下规则：

1. 优先判断 CPU 指令集架构

    ```shell
    uname -m
    ```

    `arm64/aarch64` 的情况考虑使用 openEuler arm64 安装包

    `x86_64` 继续判断

2. 其次根据内核 glibc 版本判断

    ```shell
    getconf GNU_LIBC_VERSION
    ```

    如果 glibc 版本小于等于 2.17，考虑使用 centos7 x86_64 的安装包，否则考虑使用 openEuler x86_64 的安装包

### PTK安装后，是否还支持 gs_om 操作？

支持。

PTK 安装的集群兼容 gs_om，会自动生成 gs_om 需要的静态文件。

> 注意：`gs_om` 的操作需要是节点之间的用户SSH互信，PTK 安装后会自动建立，若未创建需用户自行建立。

### 查询的状态报错 `FATAL:  semctl(578486307, 7, SETVAL, 0) failed: Invalid argument`

请检查服务器 /etc/systemd/logind.conf 中 **RemoveIPC** 的值是否为 `no`，修改后重启服务器

### PTK 如何查看目前可支持安装的mogdb版本？

执行 `ptk candidate db` 指令查看

### PTK 如何查看目前可支持安装的操作系统？

执行 `ptk candidate os` 指令查看

### 是否支持安装 CM 组件？

支持。

> 注意：CM 的使用目前强制要求 3 个实例及以上才可以启用。

ptk install 命令支持 `--install-cm` 参数，当指定了该参数时，当初始化完集群内所有节点时，会使用 cm_ctl 去启动集群（不安装 CM 时，使用 gs_ctl 工具）,使用 cm_ctl 启动的过程会涉及集群选主及主备构建的过程，所以一般耗时较长。

如果在 PTK 启动阶段指令超时。用户可登录到数据库服务器查看集群状态，因为在启动阶段，PTK的退出不中断 cm_ctl 后端的执行。

### PTK 必须和数据库在同一个服务器上吗？

不是的。

PTK 工具的设计时的定位是 “中控机” 的角色，因为 PTK 拥有管理多个数据库集群的能力，所以我们建议是 PTK 独立在一个管理服务器上安装，数据库在数据库专用的服务器集群内节点上安装。

当然，PTK 也是支持和数据库部署在同一个服务器上的，此时就是本地安装。
