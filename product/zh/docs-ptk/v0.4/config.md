---
title: Configuration File
summary: Configuration File
author: Yao Qian
date: 2021-09-14
---

# 配置文件

PTK 的配置文件采用 [YAML格式](./yaml-grammar.md)。

该文件包含介绍了安装集群所需的所有配置参数。

在安装数据库集群时，需用户提供一个描述集群拓扑的配置文件，该配置文件在 PTK 的大多数子命令中都会使用到。

PTK 提供了交互式创建配置文件的功能，通过运行下面的指令可以进入交互流程:

```shell
ptk template create
```

你只需要按照终端提示的步骤逐步完成配置，最终会在当前文件夹下生成 config.yaml 文件。
另外，如果想要不使用交互式创建，直接生成一个默认配置的文件，可以运行命令:

```shell
ptk template > config.yaml
```

运行后可以生成一个 config.yaml 的文件，需用户根据自己的实际情况配置里面的参数。

在非交互式修改配置时，配置中的系统用户密码 (user_password)，数据库密码 (db_password)，SSH密码 (ssh_option.password) 或密钥口令 (ssh_option.passphrase) 等敏感字段，

建议使用 PTK 提供的加密命令进行加密后填写：

```shell
ptk encrypt PASSWORD
```

---

## Config

Config 数据库集群拓扑配置信息

**示例**:

- simple config example

```yaml
# 集群级别的配置信息，其中 `db_port` 和 `ssh_option` 字段是 **可重用字段**
global:
    cluster_name: DefaultCluster # 集群名称，PTK管理的集群的唯一标识
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
# 集群内的数据库实例服务器配置信息
db_servers:
    - host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
    - host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
```

- complex config example

```yaml
# 集群级别的配置信息，其中 `db_port` 和 `ssh_option` 字段是 **可重用字段**
global:
    cluster_name: CustomCluster # 集群名称，PTK管理的集群的唯一标识
    user: omm # 运行数据库的操作系统用户名
    group: omm # 运行数据库的操作系统用户组
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
    base_dir: /opt/mogdb # 数据库安装的基础目录，如果 app_dir，data_dir，log_dir和tool_dir 参数中有未配置的，PTK 会自动在该目录下会创建对应的目录；
# 集群内的数据库实例服务器配置信息
db_servers:
    - host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.200
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: primary # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      az_name: BJ # Available Zone (可用区) 名称
      az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
      # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
      ssh_option:
        port: 22 # SSH 登录用户
        user: root # SSH 登录用户密码
        password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH 登录密钥文件路径
        conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
        exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
    - host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.201
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      az_name: BJ # Available Zone (可用区) 名称
      az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
      # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
      ssh_option:
        port: 22 # SSH 登录用户
        user: root # SSH 登录用户密码
        key_file: ~/.ssh/id_rsa # SSH 登录密钥文件口令
        conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
        exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
    - host: 10.1.1.102 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.202
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: cascade_standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      upstream_host: 10.1.1.101 # 当实例角色为 cascade_standby 时，该字段表示要跟随的上游备库 IP
      az_name: SH # Available Zone (可用区) 名称
      az_priority: 2 # 可用区使用的优先级，数值越小优先级越高
```

### global

**类型**: <a href="#global">Global</a>

**描述**: 集群级别的配置信息，其中 `db_port` 和 `ssh_option` 字段是 **可重用字段**<br /><br />**可重用字段**<br />  如果实例级别设置了该字段，则对应实例使用实例级别的值；<br />  如果实例级别未配置该字段，则继承 global 级别的字段的值。

**示例**:

- simple global example

```yaml
global:
    cluster_name: DefaultCluster # 集群名称，PTK管理的集群的唯一标识
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
```

- complex global example

```yaml
global:
    cluster_name: CustomCluster # 集群名称，PTK管理的集群的唯一标识
    user: omm # 运行数据库的操作系统用户名
    group: omm # 运行数据库的操作系统用户组
    db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
    base_dir: /opt/mogdb # 数据库安装的基础目录，如果 app_dir，data_dir，log_dir和tool_dir 参数中有未配置的，PTK 会自动在该目录下会创建对应的目录；
```

**新增于**: v0.1

### db_servers

**类型**: []<a href="#dbserver">DBServer</a>

**描述**: 集群内的数据库实例服务器配置信息

**示例**:

- simple db servers example

```yaml
db_servers:
    - host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
    - host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
```

- complex db servers example

```yaml
db_servers:
    - host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.200
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: primary # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      az_name: BJ # Available Zone (可用区) 名称
      az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
      # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
      ssh_option:
        port: 22 # SSH 登录用户
        user: root # SSH 登录用户密码
        password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH 登录密钥文件路径
        conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
        exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
    - host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.201
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      az_name: BJ # Available Zone (可用区) 名称
      az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
      # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
      ssh_option:
        port: 22 # SSH 登录用户
        user: root # SSH 登录用户密码
        key_file: ~/.ssh/id_rsa # SSH 登录密钥文件口令
        conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
        exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
    - host: 10.1.1.102 # 数据库实例服务器 IP (仅支持 IPv4)
      db_port: 27000 # 数据库端口
      # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
      ha_ips:
        - 10.1.1.202
      ha_port: 27001 # 数据库主备实例日志传输端口
      role: cascade_standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
      upstream_host: 10.1.1.101 # 当实例角色为 cascade_standby 时，该字段表示要跟随的上游备库 IP
      az_name: SH # Available Zone (可用区) 名称
      az_priority: 2 # 可用区使用的优先级，数值越小优先级越高
```

**新增于**: v0.1

---

## Global

Global 定义配置文件中全局参数

Appears in:

- <code><a href="#config">Config</a>.global</code>

**示例**:

- simple global example

```yaml
cluster_name: DefaultCluster # 集群名称，PTK管理的集群的唯一标识
db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
```

- complex global example

```yaml
cluster_name: CustomCluster # 集群名称，PTK管理的集群的唯一标识
user: omm # 运行数据库的操作系统用户名
group: omm # 运行数据库的操作系统用户组
db_password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入
base_dir: /opt/mogdb # 数据库安装的基础目录，如果 app_dir，data_dir，log_dir和tool_dir 参数中有未配置的，PTK 会自动在该目录下会创建对应的目录；
```

### cluster_name

**类型**: string

**描述**: 集群名称，PTK管理的集群的唯一标识

**示例**:

- cluster name example

```yaml
cluster_name: cluster1
```

**新增于**: v0.1

### user

**类型**: string

**描述**: 运行数据库的操作系统用户名<br />默认为："omm"

**示例**:

- os user example

```yaml
user: omm
```

**新增于**: v0.1

### group

**类型**: string

**描述**: 运行数据库的操作系统用户组<br />默认和操作系统用户同名

**示例**:

- os user group example

```yaml
group: omm
```

**新增于**: v0.1

### user_password

**类型**: string

**描述**: 运行数据库的操作系统用户密码<br />该字段不支持明文填写，需使用 ptk encrypt 加密<br />默认为空，不设置

**新增于**: v0.1

### db_password

**类型**: string

**描述**: 数据库的初始用户密码，若配置文件中未填写，会在安装时通过交互式要求输入<br />该字段不支持明文填写，需使用 ptk encrypt 加密

**新增于**: v0.1

### db_port

**类型**: int

**描述**: 数据库的运行端口<br />默认值: 26000

**示例**:

- db port example

```yaml
db_port: 26000
```

**新增于**: v0.1

### base_dir

**类型**: string

**描述**: 数据库安装的基础目录，如果 app_dir，data_dir，log_dir和tool_dir 参数中有未配置的，PTK 会自动在该目录下会创建对应的目录；<br />默认值: "/opt/mogdb"

**示例**:

- db base dir example

```yaml
base_dir: /opt/mogdb
```

**新增于**: v0.1

### app_dir

**类型**: string

**描述**: 数据库部署文件、启动脚本和配置文件的存放路径<br />默认值: "/opt/mogdb/app"

**示例**:

- db app dir example

```yaml
app_dir: /opt/mogdb/app
```

**新增于**: v0.1

### data_dir

**类型**: string

**描述**: 数据库数据存储目录<br />默认值: "/opt/mogdb/data"

**示例**:

- db data dir example

```yaml
data_dir: /opt/mogdb/data
```

**新增于**: v0.1

### log_dir

**类型**: string

**描述**: 数据库运行日志和操作日志存储目录<br />默认值: "/opt/mogdb/log"

**示例**:

- db log dir example

```yaml
log_dir: /opt/mogdb/log
```

**新增于**: v0.1

### tool_dir

**类型**: string

**描述**: 数据库工具目录<br />默认值: "/opt/mogdb/tool"

**示例**:

- db tool dir example

```yaml
tool_dir: /opt/mogdb/tool
```

**新增于**: v0.1

### tmp_dir

**类型**: string

**描述**: 数据库临时目录<br />默认值: "/tmp"

**示例**:

- db tmp dir example

```yaml
tmp_dir: /tmp
```

**新增于**: v0.1

### core_file_dir

**类型**: string

**描述**: 数据库 core dump 时，错误日志文件目录，设置后会修改系统配置参数 `kernel.core_pattern`<br />默认为空

**新增于**: v0.1

### cm_dir

**类型**: string

**描述**: [0.4 版本以上弃用，请使用 CMOption 字段] 数据库 cm数据目录（仅在数据库版本 3.0 及以上生效）

**示例**:

- db cm dir example

```yaml
cm_dir: /opt/mogdb/cm
```

**新增于**: v0.2

### cm_server_port

**类型**: int

**描述**: [0.4 版本以上弃用，请使用 CMOption 字段] CM Server端口号（仅在数据库版本 3.0 及以上生效）

**示例**:

- cm server port

```yaml
cm_server_port: 15300
```

**新增于**: v0.2

### ssh_option

**类型**: <a href="#sshoption">SSHOption</a>

**描述**: SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户<br />不设置时为本地执行

**新增于**: v0.1

### gs_initdb_opts

**类型**: []string

**描述**: 用户自定义配置 gs_initdb 初始化数据库时的参数。<br />特别地, 如果用户未配置 locale 参数, PTK 默认会设置 no-locale<br />支持的参数列表请查看 gs_initdb 工具[文档](https://docs.mogdb.io/zh/mogdb/v3.0/5-gs_initdb)

**示例**:

- gs_initdb options example

```yaml
gs_initdb_opts:
    - --locale=LOCALE
    - -E=UTF-8
```

**新增于**: v0.3

### cm_option

**类型**: <a href="#cmoption">CMOption</a>

**描述**: （选填）MogDB 高可用组件 CM 配置信息，若不使用 CM 组件，可不填写该字段<br /> 仅在数据库版本 3.0 及以上生效

**新增于**: v0.4

---

## CMOption

Appears in:

- <code><a href="#global">Global</a>.cm_option</code>

### dir

**类型**: string

**描述**: CM 安装目录

**示例**:

- db cm dir example

```yaml
dir: /opt/mogdb/cm
```

**新增于**: v0.4

### cm_server_port

**类型**: int

**描述**: cm server 监听端口

**示例**:

- cm server port

```yaml
cm_server_port: 15300
```

**新增于**: v0.4

### db_service_vip

**类型**: string

**描述**: 数据库提供服务的虚拟IP

**新增于**: v0.4

### cm_server_conf

**类型**: map[string]string

**描述**: cm_server.conf 中支持的配置参数， PTK 不对参数的准确性和有效性做校验

**示例**:

- cm server conf

```yaml
cm_server_conf:
    key: value
```

**新增于**: v0.4

### cm_agent_conf

**类型**: map[string]string

**描述**: cm_agent.conf 中支持的配置参数， PTK 不对参数的准确性和有效性做校验

**示例**:

- cm agent conf

```yaml
cm_agent_conf:
    key: value
```

**新增于**: v0.4

---

## DBServer

DBServer 集群内的数据库实例服务器配置信息

Appears in:

- <code><a href="#config">Config</a>.db_servers</code>

**示例**:

- simple db servers example

```yaml
- host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
- host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
```

- complex db servers example

```yaml
- host: 10.1.1.100 # 数据库实例服务器 IP (仅支持 IPv4)
  db_port: 27000 # 数据库端口
  # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
  ha_ips:
    - 10.1.1.200
  ha_port: 27001 # 数据库主备实例日志传输端口
  role: primary # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
  az_name: BJ # Available Zone (可用区) 名称
  az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
  # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
  ssh_option:
    port: 22 # SSH 登录用户
    user: root # SSH 登录用户密码
    password: pTk6LIBsPVplOpmxQToCPT9+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U= # SSH 登录密钥文件路径
    conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
    exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
- host: 10.1.1.101 # 数据库实例服务器 IP (仅支持 IPv4)
  db_port: 27000 # 数据库端口
  # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
  ha_ips:
    - 10.1.1.201
  ha_port: 27001 # 数据库主备实例日志传输端口
  role: standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
  az_name: BJ # Available Zone (可用区) 名称
  az_priority: 1 # 可用区使用的优先级，数值越小优先级越高
  # SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户
  ssh_option:
    port: 22 # SSH 登录用户
    user: root # SSH 登录用户密码
    key_file: ~/.ssh/id_rsa # SSH 登录密钥文件口令
    conn_timeout: 5m0s # SSH 执行单条命令时的超时时间
    exec_timeout: 5m0s # 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接
- host: 10.1.1.102 # 数据库实例服务器 IP (仅支持 IPv4)
  db_port: 27000 # 数据库端口
  # 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)
  ha_ips:
    - 10.1.1.202
  ha_port: 27001 # 数据库主备实例日志传输端口
  role: cascade_standby # 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库
  upstream_host: 10.1.1.101 # 当实例角色为 cascade_standby 时，该字段表示要跟随的上游备库 IP
  az_name: SH # Available Zone (可用区) 名称
  az_priority: 2 # 可用区使用的优先级，数值越小优先级越高
```

### host

**类型**: string

**描述**: 数据库实例服务器 IP (仅支持 IPv4)

**示例**:

- db host ip example

```yaml
host: 10.1.1.100
```

**新增于**: v0.1

### db_port

**类型**: int

**描述**: 数据库端口<br />默认为空，复用全局的端口配置 26000

**示例**:

- db port example

```yaml
db_port: 26000
```

**新增于**: v0.1

### ha_ips

**类型**: []string

**描述**: 数据库备用本机 IP 列表(注: 并非集群里其它节点的IP)

**示例**:

- db HA ip list example

```yaml
ha_ips:
    - 10.1.1.200
```

**新增于**: v0.1

### ha_port

**类型**: int

**描述**: 数据库主备实例日志传输端口<br />默认和数据库端口一致

**新增于**: v0.1

### role

**类型**: string

**描述**: 数据库实例角色，若不填，PTK 会随机选择一个实例作为主库，其余自动为备库

**可选值**: 

- primary
- standby
- cascade_standby

**示例**:

- db role example

```yaml
role: primary
```

**新增于**: v0.1

### upstream_host

**类型**: string

**描述**: 当实例角色为 cascade_standby 时，该字段表示要跟随的上游备库 IP

**示例**:

- cascade db upstream example

```yaml
upstream_host: 10.1.1.101
```

**新增于**: v0.1

### az_name

**类型**: string

**描述**: Available Zone (可用区) 名称<br />默认值："AZ1"

**示例**:

- db az example

```yaml
az_name: AZ1
```

**新增于**: v0.1

### az_priority

**类型**: int

**描述**: 可用区使用的优先级，数值越小优先级越高<br />默认值：1

**新增于**: v0.1

### xlog_dir

**类型**: string

**描述**: 数据库 Xlog 日志目录设置<br />默认为空，若需配置，值不能为数据目录的子目录

**新增于**: v0.1

### db_conf

**类型**: map[string]string

**描述**: 用户根据个性化需要配置数据库参数和值，该字段配置的内容将会在数据库启动前写入到 postgresql.conf 文件中

**示例**:

- db config example

```yaml
db_conf:
    key: value
```

**新增于**: v0.1

### ssh_option

**类型**: <a href="#sshoption">SSHOption</a>

**描述**: SSH 登录信息，登录用户需为 **root** 或者具有 sudo 权限的用户<br />不设置时为本地执行

**新增于**: v0.1

---

## SSHOption

SSHOption SSH 登录认证信息，支持密码和密钥两种方式，二者需至少设置一种

Appears in:

- <code><a href="#global">Global</a>.ssh_option</code>
- <code><a href="#dbserver">DBServer</a>.ssh_option</code>
- <code><a href="#sshoption">SSHOption</a>.proxy</code>

**示例**:

```yaml
host: 10.1.1.100
port: 22
user: root
key_file: ~/.ssh/id_rsa
```

### host

**类型**: string

**描述**: 要连接的目标服务器 IP<br />当该机器为跳板机时必填；<br />当为数据库所在机器时可以留空，会继承 db_server 的 IP<br />默认为空

**新增于**: v0.1

### port

**类型**: int

**描述**: SSH 服务端口<br />默认值：22

**新增于**: v0.1

### user

**类型**: string

**描述**: SSH 登录用户<br />默认值：root

**示例**:

- ssh user example

```yaml
user: root
```

**新增于**: v0.1

### password

**类型**: string

**描述**: SSH 登录用户密码<br />该字段不支持明文填写，需使用 ptk encrypt 加密后填写

**示例**:

- ssh password example

```yaml
password: pTk6MTIxOGMxOTk8QT1CPT4+PXlnYW1DdHpXb2hCX3c3UW0wWXVkNlZwMGRCakpxRXd1WWdwQ0xDUWVrb0U=
```

**新增于**: v0.1

### key_file

**类型**: string

**描述**: SSH 登录密钥文件路径<br />默认为空

**示例**:

- ssh key file example

```yaml
key_file: ~/.ssh/id_rsa
```

**新增于**: v0.1

### passphrase

**类型**: string

**描述**: SSH 登录密钥文件口令<br />该字段不支持明文填写，需使用 ptk encrypt 加密后填写<br />默认为空

**新增于**: v0.1

### conn_timeout

**类型**: Duration

**描述**: SSH 登录连接超时时间，支持单位 m(分),s(秒)<br />默认值: "1m"

**示例**:

- ssh connect timeout example

```yaml
conn_timeout: 1m
```

```yaml
conn_timeout: 30s
```

**新增于**: v0.1

### exec_timeout

**类型**: Duration

**描述**: SSH 执行单条命令时的超时时间<br />默认值: "10m"

**示例**:

- ssh execute timeout example

```yaml
exec_timeout: 1m
```

```yaml
exec_timeout: 30s
```

**新增于**: v0.1

### proxy

**类型**: <a href="#sshoption">SSHOption</a>

**描述**: 跳板服务器登录信息，如果目标服务器无法直连时，支持通过中间跳板机方式连接

**新增于**: v0.1
