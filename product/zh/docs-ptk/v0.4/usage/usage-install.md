---
title: Install
summary: Install
author: Yao Qian
date: 2022-07-30
---

# 数据库安装

## 查看帮助文档

当有了配置文件，并且系统检查也都通过后，就可以开始数据库安装了。

在操作数据库安装之前，建议先熟悉安装的一些常用参数，可通过 `ptk install -h` 查看：

```shell
基于给定的拓扑配置部署MogDB数据库集群

Usage:
  ptk install [flags]

Flags:
  -y, --assumeyes                    自动对所有提问回复Yes
      --db-version string            指定数据库安装包的版本(仅限在线安装)
                                     可通过 'ptk candidate db' 查看支持的 MogDB Server 版本列表 (default "3.0.0")
      --default-guc                  不自动优化 GUC 参数，使用数据库默认参数
  -e, --env strings                  环境变量将被添加到系统用户的配置文件中
  -h, --help                         help for install
      --install-cm                   安装CM组件
      --launch-db-timeout duration   启动数据库超时时间 (default 10m0s)
      --no-cache                     不使用本地缓存的安装包
  -p, --pkg string                   指定数据库安装包的文件路径或URL
      --post-run string              指定 Bash 脚本路径，将在部署成功后分发到每个服务器上运行
      --pre-run string               指定 Bash 脚本路径，将在执行部署数据库之前分发到每个服务器上运行
      --skip-check-distro            跳过系统发行版检查，直接安装
      --skip-check-os                跳过系统环境检查，直接安装
      --skip-create-user             跳过创建系统用户
      --skip-launch-db               跳过启动数据库
      --skip-rollback                安装失败时跳过回滚操作
```

### 安装

使用 PTK 安装数据库集群很简单，安装仅需输入以下指令即可：

```shell
$ ptk install -f config.yaml -y
```

`-f` 指定配置文件路径，`-y` 表示自动所有交互式问询回复 Yes。

> 注：当配置文件中没有配置数据库初始密码时，`-y` 无法跳过询问密码流程

默认 PTK 会自动从网络下载 MogDB 的最新版进行安装。

如果你想要指定安装不同的版本或者想要安装自己编译的安装包，PTK 也是支持的。

PTK 提供了 `--db-version` 参数，来允许用户指定要安装的 MogDB 版本，

假如想要安装 MogDB 2.1.1 版本：

```shell
ptk install -f config.yaml --db-version 2.1.1 -y
``` 

> 支持安装的数据库版本列表可通过 ptk candidate db 查看

如果你希望安装自己编译的安装包，需通过 `-p|--pkg` 参数指定本地安装包路径或者网络下载地址.

更多选项：

- `-e|--env` : 环境变量将被添加到安装后的数据库系统用户的环境变量中
- `--launch-db-timeout`: 启动数据库超时时间，默认 5 分钟。
- `--pre-run`: 支持添加安装前的一个钩子 shell 脚本路径，在集群安装前，自动在集群内所有机器上执行指定的脚本
- `--post-run`: 支持添加安装后的一个钩子 shell 脚本路径，在集群安装成功后，自动在集群内所有机器上执行指定的脚本
- `--skip-check-os`: 跳过系统检查
- `--skip-check-distro` : 跳过系统发行版检查，直接安装
- `--skip-create-user`: PTK 默认会自动创建用户，指定该参数可以跳过创建用户，当指定该参数时，服务器上用户必须已经存在
- `--skip-launch-db`: 安装完成后不启动数据库
- `--skip-rollback`: PTK 在安装过程中如果出现错误，默认会回滚之前的所有操作来清理环境，指定该参数可以禁止回滚操作。
- `--install-cm`: 安装数据库的同时，安装CM组件（仅适用于MogDB版本大于3.0.0且集群内实例数量大于3个）
- `--no-cache`: 不使用本地缓存的安装包，强制从网络重新下载