---
title: Cluster Install MogHA
summary: Cluster Install MogHA
author: Yao Qian
date: 2022-09-15
---

# MogHA 安装

PTK 支持基本的 MogHA 安装流程，需用户提供合法的 MogHA 配置文件。

> 当前版本中，PTK 不对 MogHA 的配置文件进行校验，直接将用户提交的文件部署到集群服务器，所以需用户确保其正确性。

MogHA 分为 lite 模式和非 lite 模式，lite 模式仅会在主库和同步备库部署，非 lite 模式会在集群内所有节点进行部署。

## 生成配置文件模板

PTK 提供了模板指令来帮助用户自动生成 MogHA 的配置文件：

```shell
ptk template mogha -n CLUSTER_NAME [-o node.conf] [--port 8081]
```

## 安装方式

```shell
ptk cluster -n CLUSTER_NAME install-mogha -d INSTALL_DIR -c node.conf [-p MOGHA_PACKAGE]
```
