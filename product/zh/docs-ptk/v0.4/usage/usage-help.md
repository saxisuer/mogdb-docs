---
title: 使用手册
summary: 使用手册
author: Yao Qian
date: 2022-05-30
---

## 查看帮助

当安装成功 PTK 以后，可以在终端运行 `ptk -h` 来查看帮助信息，关于命令参数的含义说明，请参考[命令介绍}](../command.md)

```
PTK是一款部署和管理MogDB数据库集群的命令行工具

Usage:
  ptk [flags] <command> [args...]
  ptk [command]

Available Commands:
  env         打印 PTK 加载的环境变量值
  gen-om-xml  生成 gs_om 的 XML 配置文件
  self        操作 PTK 自身安装包
  version     打印 PTK 版本
  help        打印PTK帮助信息
  completion  Generate the autocompletion script for the specified shell

Pre Install Commands:
  candidate   打印 PTK 支持的软件版本列表
  download    在线下载 MogDB 安装包
  checkos     检查集群服务器系统依赖是否满足安装数据库需求
  encrypt     提供一个便捷的方式来加密您的文本或密码
  template    打印配置模板

Install Commands:
  install     基于给定的拓扑配置部署MogDB数据库集群
  uninstall   卸载 MogDB 数据库集群

Post Install Commands:
  ls          列出所有MogDB集群列表
  cluster     数据库集群管理的操作

Experimental Commands:
  register    注册PTK内部类型满足特定需求

Flags:
  -h, --help                打印PTK帮助信息
      --log-file string     指定运行日志文件路径
      --log-format string   指定运行日志的输出格式, 可选项: [text, json] (default "text")
      --log-level string    指定运行日志级别, 可选项: [debug, info, warning, error, panic] (default "info")
  -v, --version             打印 PTK 版本

Use "ptk [command] --help" for more information about a command.
```

## 查看版本

可以通过 `ptk -v` 来查看 PTK 版本信息，以下为示例：

```shell
$ ptk -v
PTK Version: v0.4.0
Go Version: go1.17.1
Build Date: 2022-09-14T09:32:14Z
Git Hash: a9xa9e
```

从版本信息可以得知 PTK 的版本号，编译的 Golang 版本，构建时间以及构建时的哈希值。

## 更多

- [系统检查](./usage-chekos.md)
- [创建配置文件](./usage-config.md)
- [安装数据库](./usage-install.md)
- [操作数据库](./usage-operation.md)
- [扩缩容](./usage-scale.md)
- [安装插件](./usage-install-plugin.md)
- [卸载数据库](./usage-uninstall.md)
- [集群备注](./usage-comment.md)
- [升级PTK](./usage-self-upgrade.md)
