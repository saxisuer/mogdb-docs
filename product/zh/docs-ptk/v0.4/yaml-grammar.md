---
title: 附录：YAML语法
summary: 详细描述了YAML规范以及如何应用于PTK
author: Yao Qian
date: 2022-09-16
---

# 附录：YAML语法

PTK 采用 YAML 格式的配置文件，主要是因为语法简洁直观，数据结构方面的表达能力强，并且便于搭配注释进行描述。

> 注意：YAML 特点是使用空格来表达层次结构，所以在编写配置时需要注意缩进层次

本文就主要介绍 YAML 的语法，来帮助用户可以更好的理解 PTK 的配置文件格式。

## 标量和集合

根据 [YAML 规范](https://yaml.org/spec/1.2/spec.html)，有两种集合类型和很多标量类型。

两种集合类型是 map (字典)和 sequence (列表)：

```yaml
map:
  one: 1
  two: 2
  three: 3

sequence:
  - one
  - two
  - three
```

标量值是单个值，（与集合相反）

## YAML中的标量类型

如果整型或浮点型数字没有引号，通常被视为数字类型：

```yaml
count: 1
size: 2.34
```

但是如果被引号引起来，会被当做字符串：

```yaml
count: "1" # <-- string, not int
size: '2.34' # <-- string, not float
```

布尔函数也是如此：

```yaml
isGood: true   # bool
answer: "true" # string
```

空字符串是 `null` （不是 `nil`）。

注意 `port: "80"`是合法的YAML，可以通过模板引擎和YAML解释器传值，但是如果PTK希望`port`是整型，就会失败。

在一些场景中，可以使用YAML节点标签强制推断特定类型：

```yaml
coffee: "yes, please"
age: !!str 21
port: !!int "80"
```

如上所示，`!!str`告诉解释器`age`是一个字符串，即使它看起来像是整型。即使`port`被引号括起来，也会被视为int。

## YAML中的字符串

YAML中的大多数数据都是字符串。YAML有多种表示字符串的方法。本节解释这些方法并演示如何使用其中一些方法。

有三种单行方式声明一个字符串：

```yaml
way1: bare words
way2: "double-quoted strings"
way3: 'single-quoted strings'
```

单行样式必须在一行：

- 裸字没有引号，也没有转义，因此，必须小心使用字符。
- 双引号字符串可以使用`\`转义指定字符。比如，`"\"Hello\", she said"`。可以使用`\n`转义换行。
- 单引号字符串是字面意义的字符串，不用`\`转义，只有单引号`''`需要转义，转成单个`'`。

除了单行字符串，可以声明多行字符串：

```yaml
coffee: |
  Latte
  Cappuccino
  Espresso
```

上述会被当作`coffee`的字符串值，等同于`Latte\nCappuccino\nEspresso\n`。

注意在第一行`|`后面必须正确缩进。可以这样破坏上述示例：

```yaml
coffee: |
         Latte
  Cappuccino
  Espresso

```

由于`Latte`没有正确缩进，会遇到这样的错误：

```shell
Error parsing file: error converting YAML to JSON: yaml: line 7: did not find expected key
```

模板中，有时候为了避免上述错误，在多行文本中添加一个假的“第一行”会更加安全：

```yaml
coffee: |
  # Commented first line
         Latte
  Cappuccino
  Espresso

```

注意无论第一行是什么，都会保存在字符串的输出中。比如你要这样把文件内容注入到配置映射中，注释应该是读取该条目需要的类型。

### 控制多行字符串中的空格

在上述示例中，使用了 `|` 来表示多行字符串。但是注意字符串后面有一个尾随的`\n`。如果需要YAML处理器去掉末尾的换行符，在`|`
后面添加`-`：

```yaml
coffee: |-
  Latte
  Cappuccino
  Espresso
```

现在 `coffee`的值变成了： `Latte\nCappuccino\nEspresso` (没有末尾的`\n`)。

其他时候，可能希望保留尾随空格。可以使用 `|+`符号：

```yaml
coffee: |+
  Latte
  Cappuccino
  Espresso


another: value
```

现在`coffee`的值是 `Latte\nCappuccino\nEspresso\n\n\n`。

文本块中的缩进会被保留，也会保留换行符：

```yaml
coffee: |-
  Latte
    12 oz
    16 oz
  Cappuccino
  Espresso
```

上述示例中，`coffee`会变成 `Latte\n  12 oz\n  16 oz\nCappuccino\nEspresso`。

### 折叠多行字符串

有时您想在 YAML 中用多行表示一个字符串，但希望在解释时将其视为一个长行。这被称为"折叠"。要声明一个折叠块，使用 `>` 代替 `|`：

```yaml
coffee: >
  Latte
  Cappuccino
  Espresso


```

上面`coffee`的值是： `Latte Cappuccino Espresso\n`。 注意，除了最后一个换行符之外，所有的换行符都将转换成空格。
可以组合空格控制符和折叠字符标记 `>-` 来替换或取消所有的新行。

注意在折叠语法中，缩进文本将导致保留行。

```yaml
coffee: >-
  Latte
    12 oz
    16 oz
  Cappuccino
  Espresso
```

上述结果为：`Latte\n  12 oz\n  16 oz\nCappuccino Espresso`。注意空格和换行都保存下来了。

## YAML是JSON的超集

由于YAML是一个JSON的超集，任何合法的JSON文档 _都应该_ 是合法的YAML。

```json
{
  "coffee": "yes, please",
  "coffees": [
    "Latte", "Cappuccino", "Espresso"
  ]
}
```

上述json的另一种表述方式是：

```yaml
coffee: yes, please
coffees:
- Latte
- Cappuccino
- Espresso
```

而且两种可以混合（要小心）：

```yaml
coffee: "yes, please"
coffees: [ "Latte", "Cappuccino", "Espresso"]
```

所有这三个都应该解析为相同的内部表示形式。
