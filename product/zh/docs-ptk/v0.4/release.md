---
title: 发布记录
summary: 发布记录
author: Yao Qian
date: 2022-06-01
---

## 最新版本下载

> PTK 会根据用户反馈不定期更新发布修订版本, 推荐用户始终下载最新版本使用，

- MacOS ARM64: [ptk_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_arm64.tar.gz)
- MacOS X86:   [ptk_darwin_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_darwin_x86_64.tar.gz)
- Linux ARM64: [ptk_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_arm64.tar.gz)
- Linux X86:   [ptk_linux_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_linux_x86_64.tar.gz)
- Windows X86: [ptk_windows_x86_64.tar.gz](https://cdn-mogdb.enmotech.com/ptk/latest/ptk_windows_x86_64.tar.gz)

## 发布记录

### v0.4

#### 新功能

- cluster指令新增 failover/switchover 切换功能
- cluster指令新增 show-hba/show-guc/show-config 指令，用户可方便地查看集群配置信息
- cluster指令新增 MogHA 的安装功能
- cluster指令新增 shell 指令，提供快速运行shell命令或脚本到集群服务器的能力
- cluster指令新增 refesh 指令，便于用户更新集群配置，目前开放替换IP功能
- 新增 register 指令，允许用户自己注册系统类型
- candidate指令新增 cpu 指令，查看PTK支持的CPU架构列表
- 集群扩容新增 copy 模式
- 集群操作（启、停、查询）支持操作全部集群
- 支持 MogDB 3.0.1 以上CM两节点安装
- 支持飞腾CPU检测
- 兼容 openGauss 安装

#### 功能优化及Bug修复

- 优化执行过程中断信号的检测和处理
- 优化遇到信号量不足错误时的输出，给出修复建议
- 修复az优先级配置错误导致CM启动失败的问题
- 其他已知bug的修复

### v0.3

#### 新功能

- 支持扩缩容
- 支持插件安装
- 自动根据环境尝试修复动态库依赖
- 集群列表结果新增版本信息
- 新增安装包 md5 校验
- 支持 download 指令
- 新增集群备注功能

#### 功能优化及Bug修复

- 修复存在IB网卡时，网络检查的错误
- 修复 suse 修复脚本指令错误
- 其他已知问题修复及优化

### v0.2

#### 新功能

- 新增 candidate 指令
- 新增 cm 安装支持
- 系统检查自动生成修复脚本
- 增加默认设置推荐数据库参数

#### Bug修复

- 优化安装流程
- 修复checkos中多个检查项的问题
- 兼容多种压缩包格式自动探测
- 其他已知Bug修复

### v0.1

#### 新功能

- 实现系统检查
- 实现数据库集群的安装和卸载
- 实现集群管理功能：启动、停止、查询以及重启
- 实现密码加密功能
- 实现模板生成功能
- 适配不同操作系统安装
- 实现 ptk 自升级功能
