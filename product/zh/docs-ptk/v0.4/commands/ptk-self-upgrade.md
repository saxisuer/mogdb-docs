---
title: "ptk self upgrade"
summary: ptk self upgrade
author: ptk
date: 2022-06-28
---

## ptk self upgrade

下载并自动安装最新版的PTK

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

升级步骤日志，包含下载最新包并升级，最终显示 `upgrade ptk successfully` 就表示升级成功

可以通过 [`ptk version`](ptk-version.md) 查看升级后的版本
