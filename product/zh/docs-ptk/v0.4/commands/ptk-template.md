---
title: "ptk template"
summary: ptk template
author: ptk
date: 2022-6-28
---

## ptk template

打印配置模板，默认输出多实例的配置模板

## 语法

```shell
ptk template [flags]
```

## 选项

### -l, --local

- 生成本地单实例数据库的配置文件
- 数据类型：布尔值
- 改选项默认关闭，在命令中添加该选项生效
- 默认值：false

### -d, --base-dir string

- 指定数据库要安装的基目录
- 数据类型：字符串
- PTK会在该目录下自动分配数据库的数据目录，工具目录，应用目录等。
- 默认值："/opt/mogdb"

### -n, --cluster-name string

- 指定集群名称
- 数据类型：字符串
- 如果不传该参数，PTK默认会随机生成一个集群名称

### -u, --user string

- 运行数据库的系统用户名称
- 数据类型：字符串
- 默认值："omm"

### -g, --group string

- 运行数据库的系统用户组名称
- 数据类型：字符串
- 默认值：和 `-u` 指定的系统用户同名

### -p, --port int

- 数据库运行端口
- 数据类型：整型
- 默认值：26000

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

以键值对形式输出原文和密文，加密多个原文时，会以多行输出。

## 更多子命令

- [ptk template create][ptk-template-create.md]
