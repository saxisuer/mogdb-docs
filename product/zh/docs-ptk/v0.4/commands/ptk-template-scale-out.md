---
title: "ptk template scale-out"
summary: ptk template scale-out
author: ptk
date: 2022-08-01
---

# ptk template scale-out

生成集群扩容配置

## 语法

ptk template scale-out [flags]

## 选项

### -o, --output string

- 指定输出文件
- 数据类型：字符串
