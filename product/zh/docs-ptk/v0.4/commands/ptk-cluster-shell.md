---
title: "ptk cluster shell"
summary: ptk cluster shell
author: ptk
date: 2022-08-01
---

# ptk cluster shell

执行shell指令或脚本, 若不加参数进入终端模式

## 语法

ptk cluster shell [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串

### -c, --command string

- 要执行的 shell 指令
- 数据类型：字符串

### -H, --host stringArray

- 指定执行的机器IP，默认在集群内全部执行
- 数据类型：字符串
- 可多次指定

### -s, --script string

- 指定要执行的 shell 脚本路径
- 数据类型：字符串
