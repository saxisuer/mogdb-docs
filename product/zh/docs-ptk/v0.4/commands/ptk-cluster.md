---
title: "ptk cluster"
summary: ptk cluster
author: ptk
date: 2022-06-28
---

## ptk cluster

数据库集群管理的操作

## 语法

```shell
ptk cluster [flags]
```

## 选项

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。
