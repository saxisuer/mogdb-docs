---
title: "ptk cluster restart"
summary: ptk cluster restart
author: ptk
date: 2022-06-28
---

## ptk cluster restart

重启数据库集群或实例

## 语法

```shell
ptk cluster restart -n string [flags]
```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -H, --host string

- 要操作的实例IP
- 数据类型：字符串
- 该选项是为了支持允许查询单个实例的状态，默认查询整个集群的所有实例状态

### --security-mode string

- 是否使用安全模式启动数据库
- 数据类型：字符串
- 可选项：on/off
- 默认值：off

### --time-out duration

- 执行超时时间
- 数据类型：Duration
- Duration 类型支持的单位："h" 小时, "m" 分支, "s" 秒, "ms"
- 默认值："2m"

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

运行状态日志
