---
title: "ptk cluster scale-out"
summary: ptk cluster scale-out
author: ptk
date: 2022-08-01
---

## ptk cluster scale-out

对一个MogDB集群进行扩容

## 语法

```shell
ptk cluster -n CLUSTER_NAME scale-out -c add.yaml [--force] [--skip-check-distro] [--skip-check-os] [--skip-create-user]

```

## 选项

### -n, --name string

- 要操作的集群名称
- 数据类型：字符串

### -c, --config string

- 扩缩容配置文件路径
- 数据类型：字符串

### --default-guc

- 不自动优化 GUC 参数，使用数据库默认参数
- 数据类型：布尔值

### --force

- 如果扩缩容执行失败或被中断，可通过 --force 参数再次执行操作，PTK 会清理掉旧的脏数据后操作
- 数据类型：布尔值

### --gen-template

- 生成一个扩容模板的配置
- 数据类型：布尔值

### --skip-check-distro

- 跳过检查系统发行版
- 数据类型：布尔值

### --skip-check-os

- 跳过检查系统
- 数据类型：布尔值

### --skip-create-user

- 跳过创建用户
- 数据类型：布尔值

### -t, --timeout duration

- 执行超时时间
- 数据类型：Duration
- Duration 类型支持的单位："h" 小时, "m" 分支, "s" 秒, "ms"
- 默认值："10m"

### -h, --help

- 输出帮助信息。
- 数据类型：布尔值
- 该选项默认关闭，默认值为 false。在命令中添加该选项，并传入 true 值或不传值，均可开启此功能。

## 输出

操作日志
