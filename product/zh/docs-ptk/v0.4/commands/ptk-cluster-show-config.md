---
title: "ptk cluster show-config"
summary: ptk cluster show-config
author: ptk
date: 2022-08-01
---

# ptk cluster show-config

显示指定集群的拓扑配置信息

## 语法

ptk cluster show-hba [flags]

## 选项

### -n, --name string

- 集群名称
- 数据类型：字符串
