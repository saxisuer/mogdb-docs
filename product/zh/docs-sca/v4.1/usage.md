---
title: SCA 使用说明
summary: SCA 使用说明
author: Hong Rihua
date: 2021-11-12
---

# SCA 使用说明

## 部署步骤

采用 Python3 编写的脚本程序，且已打包为可执行程序，无需部署。

程序文件上传后，确认能正常通过网络连接数据库即可直接使用。

使用前建议仔细阅读使用文档，以及命令选项文档，确保使用的各个选项合理。

## 常用命令

注意：以下以 x86-64 架构下的 Linux 系统为例进行说明，其他 ARM 平台或 MacOS 系统需要修改对应的命令名称。

* 初始化资料库且进行兼容评估 (通常在第一次运行时使用，使用 SCA 自动创建的目标库)

```shell
./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password> -d <unzipped data directory>
```

* 只进行资料库初始化 (对于一个资料库只需要运行一次即可)

```shell
./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password>
```

* 只进行兼容评估 (通常在连接一个已初始化过的资料库进行数据分析)

```shell
./sca_linux_x86_64 -d <unzipped data directory>
```

* 只进行 SQL 模拟 (资料库已初始化)

```shell
./sca_linux_x86_64 -T S -d <unzipped data directory>
```

* 同时进行SQL兼容度与性能评估 (资料库已初始化)

```shell
./sca_linux_x86_64 -T IS -H <host> -P <port> -U <super-user> -E <super-password> -d <unzipped data directory> -t <target_db>
```

* Oracle 数据采集 (无需资料库)

```shell
# 作为测试用途，可添加如下参数加快 SQL 采集： -q 0.001 -Q 60 -m off
./sca_linux_x86_64 -T C -s SCOTT -t <target-db> -H <host> -P <port> -U <oracle-user> -E <oracle-password>
```

* MySQL 慢日志 SQL 兼容度分析 (资料库已初始化)

```shell
./sca_linux_x86_64 -T M -t <target-db> -H <host> -P <port> -d <report-directory> --slow-log=<slow-log-file>
```
