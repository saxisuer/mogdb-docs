---
title: SCA 介绍
summary: SCA 介绍
author: Hong Rihua
date: 2021-11-12
---

# SCA 介绍

SCA 是 SQL Compatible Analysis 的简称，工具采用 Python (Version 3) 语言编写。
SCA 工具是一款异构数据库迁移前的兼容度评估工具，可用于异构数据迁移前，评估源数据库中的实际业务 SQL 在目标库中是否执行，以及评估两款异构数据库中的 SQL 执行性能差异。
**适用于**： MogDB（以及其他基于 openGauss 的数据库）

SCA 服务程序可运行如下 3 种类型的任务：

1. `I [Inspection]`:  **兼容评估**，用于评估从 Oracle 到 MogDB 的对象以及业务 SQL 的兼容度情况
2. `S [Simulation]`:  **SQL模拟**，用于在 MogDB 保真环境中，模拟 Oracle 中业务 SQL 的执行情况
3. `C [Collection]`:  **数据采集**，用于采集 Oracle 元数据，主要包括对象结构数据，SQL执行数据等
4. `M [MySQL]`: **MySQL评估**，用于对 MySQL 中采集到的 slow-log 慢日志中的 SQL 进行兼容度评估

注意：

- 兼容评估，需要有完整的源库结构，推荐使用 MTK 工具进行源数据库结构迁移，也可使用 SCA 自动创建测试目标库结构，SCA 创建的目标库兼容性较差
- SQL模拟，需要有完整的源库数据，推荐使用 MTK 工具进行源数据库结构以及数据迁移

## 操作系统与平台支持

SCA 目前支持在如下操作系统和平台架构下运行：

1. Linux x86_64
2. Linux arm64
3. MacOS (MacOS 版本不支持 openGauss/MogDB 中的 SHA256 加密认证方式)

## 数据库支持

SCA 目前支持的源端与目标端数据库类型如下：

1. 源端： Oracle &gt;= 10.2
2. 目标端： MogDB/openGauss &gt;= 1.0    ( MacOS 暂不支持 SHA256 加密认证方式 )

**注意**： 平台支持，与数据库支持没有相关性，数据库可以运行在其他平台，如 Windows/AIX 等，只要从数据库到 SCA 运行主机网络与端口互通即可。
