---
title: 发布记录
summary: 发布记录
author: Hong Rihua
date: 2021-11-12
---

# 发布记录

## v4.1.0

### Feature

- 支持 MySQL 数据库基于 slow-log 和 general-log 的 SQL 兼容度分析
- 支持自定义 SQL 转换规则

## v4.0.0

### Feature

- 集成全新设计的离线分析报告
- 支持单个可执行程序的数据采集，兼容性分析与性能评估
- 支持 Linux 系统下的 openGauss/MogDB 数据库的 sha256 密码认证方式
