---
title: SCA 介绍
summary: SCA 介绍
author: Hong Rihua
date: 2022-03-07
---

# SCA 介绍

SCA 全称 SQL Compatible Analysis，是一款异构数据库迁移前的 SQL 兼容和性能评估工具。

可用于异构数据迁移前的兼容性评估，评估源数据库中的实际业务 SQL 在目标库中是否存在语法问题，以及评估两款异构数据库中的 SQL 实际执行的性能差异。

本工具目前支持三种源端数据库，包括： Oracle, MySQL, DB2。

**适用于**： MogDB（以及其他基于 openGauss 的数据库）

SCA 程序可运行如下 10 种类型的任务：

1. `I [Initialize]`:  **初始化**，用于初始化 SCA 资料库
2. `L [Apply License]`:  **申请 License**，用于 License 申请的任务
3. `OC [Oracle Collection]`:  **Oracle采集**，用于采集 Oracle 数据库中执行过的业务 SQL，需要长时间采集
4. `MC [MySQL Collection]`:  **MySQL采集**，用于采集 MySQL 数据库中执行过的业务 SQL，需提前配置慢日志，然后一次性采集
5. `DC [DB2 Collection]`:  **DB2采集**，用于采集 DB2 数据库中执行过的业务 SQL，需要长时间采集
6. `OI [Oracle Inspection]`:  **Oracle兼容评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
7. `MI [MySQL Inspection]`:  **MySQL兼容评估**，用于评估源端 MySQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
8. `DI [DB2 Inspection]`: **DB2兼容评估**，用于评估源端 DB2 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
9. `OS [Oracle Simulation]`: **Oracle性能评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的执行性能
10. `OIS [Oracle Inspection & Simulation]`: **Oracle兼容和性能评估**，等同于 OI + OS 两种任务同时进行

注意：

- 兼容评估，需要有完整的源库结构，推荐使用 MTK 工具进行源数据库结构迁移，也可使用 SCA 自动创建测试目标库结构，SCA 创建的目标库兼容性较差
- SQL模拟，需要有完整的源库数据和数据，推荐使用 MTK 工具进行源数据库结构以及数据迁移

## 操作系统与平台支持

SCA 目前支持在如下操作系统和平台架构下运行：

1. Linux x86_64
2. Linux arm64 (ARM 平台由于没有 DB2 客户端，故不支持 DB2 数据采集)
3. MacOS (MacOS 版本不支持 openGauss/MogDB 中的 SHA256 加密认证方式)

## 数据库支持

SCA 目前支持的源端与目标端数据库类型如下：

1. 源端： 
   - Oracle: 不低于 10.2
   - MySQL: 不低于 5.5
   - DB2: 不低于 11.5
   - File: MySQL slow-log

2. 目标端： 
   - MogDB/openGauss: 不低于 2.0
   - PostgreSQL: 不低于 13.0

**注意**： 平台支持，与数据库支持没有相关性，数据库可以运行在其他平台，如 Windows/AIX 等，只要从数据库到 SCA 运行主机网络与端口互通即可。
