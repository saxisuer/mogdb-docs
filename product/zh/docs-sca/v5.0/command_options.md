---
title: SCA 使用说明
summary: SCA 使用说明
author: Hong Rihua
date: 2022-03-07
---

# SCA 命令行选项

SCA 为纯命令行工具，所有功能均可通过单一的可执行程序完成。

## 通用选项

通用选项在三类任务中均有可能会使用。

### --help (帮助信息)

在使用分析程序前，建议仔细阅读帮助信息（命令： `./sca_linux_x86_64 --help`）:

```shell
Introduction: 
    SCA is the tool used to do SQL life-cycle inspector and simulation from other DB to MogDB.
    Now we support source databases of Oracle, MySQL and DB2.
    So there are 10 type of tasks:
        1. [  I] Init SCA repository
        2. [  L] Apply for License
        3. [ OC] Oracle Collection, collect Oracle basic information and executed SQLs
        4. [ MC] MySQL Collection, collect MySQL basic information and executed SQLs
        5. [ DC] DB2 Collection, collect DB2 basic information and executed SQLs
        6. [ OI] Oracle Inspection, used to do Oracle SQL compatible analysis
        7. [ MI] MySQL Inspection, used to do MySQL SQL compatible analysis
        8. [ DI] DB2 Inspection, used to do DB2 SQL compatible analysis
        9. [ OS] Oracle Simulation, used to do Oracle SQL performance analysis like Oracle build-in SPA tool
       10. [OIS] Oracle Inspection & Simulation, for both SQL compatible and performance analysis

Options: 
                          --[ Overall ]--
        --help            : Show help message
    -v, --version         : Show SCA version
    -T, --type            : Run type:
                          :        I = Init SCA repository
                          :        L = Apply for license
                          :     ----------------[Analysis: Target required]---------------
                          :       OI = Oracle Inspection, this is default type
                          :       MI = MySQL Inspection
                          :       DI = DB2 Inspection
                          :       OS = Oracle Simulation, just like Oracle SPA
                          :      OIS = Oracle Inspection & Simulation
                          :     ------------[Collection: No need of target DB]------------
                          :       OC = Oracle Collection
                          :       MC = MySQL Collection
                          :       DC = DB2 Collection
    -d, --data            : Unzipped data directory for analyzer, or directory for collection
    -D, --data-id         : Use data with data id existed in the repository
    -w, --workers         : Parallel workers for tasks, default: 10
    -x, --debug           : Enable debug mode
    -l, --logfile         : Output to both logfile (without progress-bar) and screen (with progress-bar)
    -L, --log-only        : Output to only logfile (without progress-bar)
    -F, --force           : Force mode in REPO Creation, drop old objects before create it
    -r, --report          : Final report file location, default in data directory with name 'report'
        --license         : License file, default is [./license.json]
        --sql-transformer : Regular rules for SQL transformation (for internal use)
                          : Format: [{"name": "xxx"
                          :           "source": "xxx",
                          :           "target": "xxx",
                          :           "comment": "xxx"}, ...]

                          --[ Repository Connection ]--
    -H, --repo-host       : Repository DB Server host address, default: 127.0.0.1
    -P, --repo-port       : Repository DB server port, default: 5432
    -N, --repo-name       : Repository database, default: sca_db
    -U, --repo-user       : Repository user, default: sca_repo
    -E, --repo-password   : Repository password, default: SCA@password
        --user            : Administrator used to create repository DB and user, default: mogdb
        --password        : Password for Administrator, default: mogdb

                          --[ Source & Target Connection ]--
    -h, --db-host         : Source & Target DB Server host address, default same as -H
    -p, --db-port         : Source & Target DB server port, default same as -P
    -n, --db-name         : Source & Target database, default same as -N
    -u, --db-user         : Source & Target user, default same as -U
    -e, --db-password     : Source & Target password, default same as -E
        --target-type     : Target database type in analysis tasks, default: MOGDB
                          : Valid type: ORACLE, MOGDB, OPENGAUSS, POSTGRESQL, MYSQL, DB2

                          --[ Collection Options ]--
    -q, --sql-days        : How many days for session sql data, default: 7
    -Q, --sql-interval    : SQL collect interval in seconds, default: 600
    -s, --schema-include  : Users/Schemas included in data collection, default: ''
    -S, --schema-exclude  : Users/Schemas excluded in data collection
                          : Default: <<depends on DB type>>
    -m, --enable-monitor  : Starting background monitor process in SQL Collection
                          : Valid values: 1/on/true/t = ENABLE, default: on
                          :               0/off/false/f = DISABLE
        --slow-log        : MySQL slow-log for client data collection
        --sql-csv         : SQL file in csv format for SQL Inspection (@todo)

Usage: 
    0. Apply for license
       ./sca_linux_x86_64 -T L
    1. Init repository (used for first running)
       ./sca_linux_x86_64 -T i -H <host> -P <port> -N <repo-database> -U <repo-user> -E <repo-password> --user <super_user> --password <super_password>
    2. Oracle data collection
       # Notice: "-q 0.001 -Q 60" means gather Session SQL only once
       #         "-m off" means do not monitor system status (CPU Idle and Disk Free)
       ./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n '<target-db>' -u <oracle-user> -e <oracle-password> -q 0.001 -Q 60 -m off
       ./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n '<target-db>' -u <oracle-user> -e <oracle-password>
    3. MySQL data collection using slow-log file
       ./sca_linux_x86_64 -T MC -d <report-directory> --slow-log=<slow-log-file>
    4. Oracle SQL compatible analysis (Required: Repository, Target DB)
       Note: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T OI -d <unzipped data directory>
    5. Oracle SQL performance simulation (Required: Repository, Target DB)
       Note: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T OS -d <unzipped data directory>
    6. MySQL SQL compatible analysis (Required: Repository, Target DB)
       Not e: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T MI -d <unzipped data directory> -H <host> -P <port> -n <target-db>
```

### -v, --version (查看版本)

查看当前 SCA 的版本信息：

```shell
hongyedba@localhost ~ % ./sca_linux_x86_64 -v
SCA version: 5.0.0
```

### -T, --type (任务类型)

默认值： OI

指定任务类型，目前 SCA 支持如下类型的任务：

1. `I [Initialize]`:  **初始化**，用于初始化 SCA 资料库
2. `L [Apply License]`:  **申请 License**，用于 License 申请的任务
3. `OC [Oracle Collection]`:  **Oracle采集**，用于采集 Oracle 数据库中执行过的业务 SQL，需要长时间采集
4. `MC [MySQL Collection]`:  **MySQL采集**，用于采集 MySQL 数据库中执行过的业务 SQL，需提前配置慢日志，然后一次性采集
5. `DC [DB2 Collection]`:  **DB2采集**，用于采集 DB2 数据库中执行过的业务 SQL，需要长时间采集
6. `OI [Oracle Inspection]`:  **Oracle兼容评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
7. `MI [MySQL Inspection]`:  **MySQL兼容评估**，用于评估源端 MySQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
8. `DI [DB2 Inspection]`: **DB2兼容评估**，用于评估源端 DB2 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
9. `OS [Oracle Simulation]`: **Oracle性能评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的执行性能
10. `OIS [Oracle Inspection & Simulation]`: **Oracle兼容和性能评估**，等同于 OI + OS 两种任务同时进行

### -d, --data (数据目录)

除了资料库初始化（-T I）之外的所有任务类型，都可以指定 -d 数据目录。

采集任务会将采集到的数据写入到 -d 指定的数据目录。

分析任务会从数据目录中读取数据，将数据插入到资料库表中，最终生成的报告结果默认也会写入到数据目录下。

### -D, --data-id (数据编号)

指定数据编号，然后直接从资料库读取指定编号的数据，而不是重新从数据目录中读取并加载数据。

指定 -D 选项后，会跳过加载数据的步骤，直接执行相关的分析任务。

### -w, --workers (并发度)

默认值： 10

指定用于运行任务的并发度大小，适量的并发度有助于提高各个任务的运行速度。

并发度用于： 文件数据加载到资料库，SQL兼容度评估，SQL复杂度评估，SQL性能模拟等操作中。

**注意**： 

* 在SQL模拟任务中，并发度越大，可能导致单条SQL的执行效率下降，需要依据实际生产环境的负载压力选取合理的并发度，通常设置为生产库的平均活跃会话数。

### -x, --debug (Debug模式)

开启 Debug 模式，不会影响正常的分析逻辑，但是会输出大量的日志，通常用于辅助分析程序本身的运行异常。

### -l, --logfile (日志文件)

指定程序日志文件，程序输出会同时输出到终端命令行与日志文件中。

### -L, --log-only (仅日志文件)

指定程序日志文件，程序输出会仅输出到日志文件中。

### -F, --force (强制模式)

是否启用强制模式，强制模式会在资料库初始化场景下生效。

在资料库初始化过程中，如果启用强制模式，会先删除资料库，然后重新创建。

### -r, --report (报告目录)

默认值： <DATA_DIR>/report

指定报告目录，这里的报告包括 `兼容评估` 报告和 `SQL模拟` 报告，都是 html 格式报告，可离线查看。

同时，对于 `兼容评估` 任务，还会产生 `sql_detail_list.csv` 文件，记录所有 SQL 的评估结果，以及可能存在的改写方案。

### --license (License文件)

默认值： ./license.json

License 文件的位置。

### --sql-transformer (SQL转换规则)

【高级用法】 指定从源库到目标库的 SQL 转换规则，使用正则匹配进行转换。

## 资料库选项

### -H, --repo-host (资料库IP地址)

默认值： 127.0.0.1

资料库数据库 IP 地址。

### -P, --repo-port (资料库端口)

默认值： 5432

资料库数据库端口。

### -N, --repo-name (资料库名称)

默认值： sca_db

资料库数据库名称。

### -U, --repo-user (资料库用户)

默认值： sca_repo

资料库数据库登录用户。

### -E, --repo-password (资料库密码)

默认值： SCA@password

资料库数据库登录密码。

### --user (管理员用户)

资料库管理员用户，用于初始化资料库过程中，创建资料库用户，和资料库数据库。

### --password (管路员密码)

资料库管理员用户登录密码，用于初始化资料库过程中，创建资料库用户，和资料库数据库。

## 源或目标库连接

### -h, --db-host (源或目标库IP地址)

源或目标库数据库 IP 地址，默认继承 -H 选项值。

### -p, --db-port (源或目标库端口)

源或目标库数据库端口，默认继承 -P 选项值。

### -n, --db-name (源或目标库名称)

源或目标库数据库名称，默认继承 -N 选项值。

在 SQL 分析任务中，指定目标库名称，通常是使用 MTK 或其他数据迁移工具，进行数据库结构和数据迁移时，在 MogDB 端创建的目标数据库。

需要注意的是，资料库用户默认需要对目标库有完全的操作权限，默认资料库是管理员权限。

* 在兼容评估中，只需要目标库有对象结构即可
* 在SQL模拟中，需要目标库既有对象结构，也有真实对等的全量生产数据，否则性能模拟的结果不具有参考意义

### -u, --db-user (源或目标库用户)

源或目标库数据库登录用户，默认继承 -U 选项值。

### -e, --db-password (源或目标库密码)

源或目标库数据库登录密码，默认继承 -E 选项值。

### --target-type (目标库类型)

指定分析任务总，目标数据库类型，默认为 MOGDB。

当前此参数并未完善，仅支持 MOGDB/POSTGRESQL。

## 数据采集选项

### -q, --sql-days (SQL采集天数)

指定 SQL 相关数据的采集总天数，由于从会话缓存 GV$SQLAREA 中采集已执行过的 SQL 数据，存在一定的概率漏采，可通过延长采集天数，减小漏采的概率。

默认会采集一周 7 天的 SQL 数据。

### -Q, --sql-interval (SQL采集间隔)

指定 SQL 相关数据的采集间隔（单位：秒），默认每 10 分钟采集一次。

每次采集到的 SQL 数据会和已采集的数据进行对比去重，避免重复数据太多导致的数据文件过大。

### -s, schema-include (Schema白名单)

指定数据采集的 Schema 白名单，即只采集白名单列表中列出的 Schema 相关数据。

### -S, schema-exclude (Schema黑名单)

指定数据采集的 Schema 黑名单，即不采集黑名单列表中列出的 Schema 相关数据。

默认会将 Oracle 系统用户列入 Schema 黑名单中。

### -m, enable-monitor (资源监控)

是否在后台启用资源监控子进程，默认启用。

资源监控子进程会定期（间隔 3 秒）查询当前服务器的 CPU 使用率，以及数据目录所在文件系统的剩余空间。

当 CPU 使用率高于 90%，或者文件系统剩余空间低于 100MB 时，监控子进程会触发信号，停止采集主进程，避免因为资源问题导致服务器故障。

### --slow-log (慢查询日志)

采集时，指定 MySQL 慢查询日志文件。

在采集程序无法访问目标 MySQL 数据库，以及不在 MySQL 服务器上运行采集程序时，可手动将目标库 MySQL 慢日志取出来，使用当前选项指定慢日志即可解析并生成对应的采集数据，可用于后续 MySQL SQL 兼容性评估任务。
