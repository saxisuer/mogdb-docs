---
title: DB2到MogDB评估
summary: DB2到MogDB评估
author: Hong Rihua
date: 2022-03-07
---

# DB2 到 MogDB 评估

## MogDB 环境准备

需要自行准备 MogDB/openGauss 数据库环境，用于资料库和目标库，资料库和目标库默认使用同一套环境。

## 对象结构迁移

- **结构迁移**: 需要在准备好的 MogDB/openGauss 环境中，创建目标库，并将源库中的数据结构迁移到新创建的目标库中。
- **数据迁移**: 如果想要进行 SQL 性能对比评估，那么除了迁移数据库结构之外，还需要迁移表数据，确保源 DB2 与目标 MogDB/openGauss 库数据规模一致，性能对比才有意义。

相关工具：

- **MTK**: 可使用 MTK 工具完成结构和数据迁移，具体详情请参考： [https://mogdb.io/mtk](https://mogdb.io/mtk)

## DB2 数据采集

### 所需权限

采集用户所需权限如下：

> dbadm

### 相关命令

```shell
./sca_linux_x86_64 -T DC -h <host> -p <port> -n <target-db> -u <db2-user> -e <db2-password>

# 命令选项说明：
# -h/p/t/u/e 指定连接的源端 DB2 库的连接方式
# 考虑到共享缓存中 SQL 内容不一定全面，采集会持续性增量采集，默认采集一周的数据。如果只是做功能性验证，可使用 -q 0.001 -Q 60 选项，即只进行一次 SQL 数据采集。
```

### 采集结果

采集完成后，会生成一个 zip 数据包，该数据包可以拷贝到目标库，并解压成数据目录。

后续兼容性分析，则依赖此数据包中的数据，且无需再次连接到源 DB2 数据库。

## 资料库初始化

```shell
# 使用具有管理员权限的 MogDB/openGauss 用户，进行资料库初始化
# 资料库初始化会默认创建名称为 sca_db 的资料库
# 可通过 -N 指定资料库名称，-U 指定资料库用户名，-E 指定资料库用户密码

./sca_linux_x86_64 -T i -H <host> -P <port> -N sca_db -U sca_repo -E 'SCA@password' --user <super-user> --password <super-password>
```

## 执行分析任务

### 兼容性分析

若只做 SQL 兼容性分析，则使用如下命令：

```shell
# 若资料库名称，用户名，密码非默认，则需要使用 -N, -U, -E 选项指定

./sca_linux_x86_64 -T DI -H <host> -P <port> -d <unzipped data directory>
```

分析完成后，会在 -d 指定的目录中生成 report，可将 report 离线下载后进行查看。

### 性能对比分析

若只做 SQL 性能对比分析，则使用如下命令：

```shell
# 若资料库名称，用户名，密码非默认，则需要使用 -N, -U, -E 选项指定

./sca_linux_x86_64 -T DS -H <host> -P <port> -d <unzipped data directory>
```

分析完成后，会在 -d 指定的目录中生成 report，可将 report 离线下载后进行查看。

### 兼容性分析 + 性能对比分析

若需要同时做 SQL 兼容性分析和 SQL 性能对比，则使用如下命令：

```shell
# 若资料库名称，用户名，密码非默认，则需要使用 -N, -U, -E 选项指定

./sca_linux_x86_64 -T DIS -H <host> -P <port> -d <unzipped data directory>
```

分析完成后，会在 -d 指定的目录中生成 report，可将 report 离线下载后进行查看。
