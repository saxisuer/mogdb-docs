---
title: SCA 结果说明
summary: SCA 结果说明
author: Hong Rihua
date: 2022-03-07
---

# SCA 结果说明

SCA 结果分为两类：

- **采集结果** : 源库（Oracle）中的数据采集结果，该结果会自动打包为一个 zip 文件，并在采集最后给出明确文件位置提示。
- **分析结果** : 在目标 MogDB/openGauss 中执行完分析后生成的分析报告，报告为单独一个文件夹，其中为离线 HTML 格式的报告文档，可任意进行拷贝传阅。

## 采集结果

数据采集结果会自动打包成 zip 数据包，默认存储在程序当前目录下。

采集完成之后的结果提示信息如下：

```
2022-02-15 19:20:40.301126 INFO [runMe.py:356] +==================== [ Summary Information ] ====================+
2022-02-15 19:20:40.301184 INFO [runMe.py:357] | Task Name                                  File Name  File Size |
2022-02-15 19:20:40.301222 INFO [runMe.py:358] | --------------------- ------------------------------ ---------- |
2022-02-15 19:20:40.301260 INFO [runMe.py:360] | SCA_SESSION_SQL              sca_sql_information.dat    3.65 KB |
2022-02-15 19:20:40.301294 INFO [runMe.py:360] | SCA_SESSION_SQL_PERF         sca_sql_performance.dat    3.29 KB |
2022-02-15 19:20:40.301326 INFO [runMe.py:360] | SCA_MYSQL_USER_HOST          sca_mysql_user_host.dat    1815  B |
2022-02-15 19:20:40.301357 INFO [runMe.py:360] | SCA_DATABASE                        sca_database.dat     163  B |
2022-02-15 19:20:40.301387 INFO [runMe.py:361] +=================================================================+

 >>> Final Result is:
 >>> ----------------------------------------------
 >>> /Users/hongyedba/Desktop/SCA_MySQL_test.zip
```

## 分析报告

兼容性分析与 SQL 性能模拟均会生成对应的分析报告，分析报告默认位于指定的数据目录中，也可以使用 `-r` 选项指定报告的输出目录。

### 报告入口

分析报告目录中，`index.html` 为报告的入口，点击该文件，使用默认浏览器（推荐使用Chrome）打开，即可查看分析报告。

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-1.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-2.png)

### SQL 兼容度汇总

SQL 兼容度汇总页面展示本次兼容度分析的相关结果数据，页面表格中按照用户名，程序名，模块名汇总，展示系统中采集到的所有 SQL，以及这些 SQL 在 MogDB 中的支持情况。

注意： 不同数据库中，该表格展示的内容也略有差异。

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-3.png)

### SQL 改写规则

SQL 改写规则页面展示本次分析中涉及到的 SQL 改写相关的规则信息。

其中使用情况字段展示该条规则的触发情况：

- Match 为规则在 SQL 中的命中数量
- Count 为规则匹配的 SQL 数量

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-4.png)

### SQL 复杂度分布

SQL 复杂度分布页面展示采集到的 SQL 的复杂度分布情况。
复杂度分布目前的评判标准如下：

1. SQL 涉及的表的数量，表数量越多，复杂度越高
2. SQL 中使用 connect by 语法的次数，使用 connect by 的次数越多，越有可能出现执行性能问题，对应 SQL 的复杂度越高
3. SQL 中使用自定义函数的数量，自定义函数中的逻辑复杂度不明，所以使用自定义函数越多，SQL 复杂度越高
4. SQL 在 Oracle 中的实际执行函数，执行耗时越高，则认为对应的 SQL 复杂度越高

最终每条 SQL 的复杂度则按照以上 4 个评判标准进行汇总。SQL 复杂度越高，在迁移后越需要关注 SQL 的执行性能，避免性能问题导致的业务故障。

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-5.png)

### SQL 性能对比（性能对比汇总）

SQL 性能对比汇总页面展示两个信息：

1. 性能对比的基础信息，性能对比的一些基础配置，以及对比时使用到的相关阈值设置
2. SQL 性能汇总，按照总体，提升，下降，不支持，超时等维度对 SQL 进行汇总，分析各类 SQL 对整体负载的影响

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-6.png)

### SQL 性能对比（Top by Workload/SQL, Timeout）

SQL 性能对比中 Top by Workload, Top by SQL, Timeout 等页面内容格式较为相似，以 Top by Workload 为例进行说明。

该列表展示影响最大的 100 条 SQL，其中 SQL FMS 字段为超链接，点击可进一步查看对应 SQL 的分析详情。

SQL 的性能影响有两个评估维度：

1. SQL 影响： 当前 SQL 在单条 SQL 执行情况下的性能变化影响的比例
2. 负载影响： 当前 SQL 参考其总执行次数，综合评估其对整个 SQL 负载的性能变化，以及对整体 SQL 性能的影响

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-7.png)

### SQL 性能对比（SQL 详情）

SQL详情页面展示如下几个方面的内容：

1. SQL 执行信息： SQL 在 Oracle 以及 MogDB 中的执行信息，其中 Oracle 中的执行信息来源于动态性能视图，MogDB 中的执行信息来源于实际 SQL 执行。
2. SQL 文本： SQL 在 Oracle 中的执行文本，以及在 MogDB 中的实际执行文本。
3. SQL 绑定变量： SQL 在 Oracle 中的绑定变量信息，该绑定变量信息会应用到 MogDB 中执行的 SQL 文本内，使其能在 MogDB 中真实的模拟业务执行。
4. Oracle 执行计划： SQL 在 Oracle 中的执行计划，该执行计划来源于动态性能视图。
5. MogDB 执行计划： SQL 在 MogDB 中的执行计划，该执行计划来源于实际执行，程序会自动对 MogDB 中的执行计划进行初步分析，标识出其中潜在的性能问题点。
6. MogDB 对象信息： SQL 中涉及到的对象在 MogDB 中的相关结构，以及统计信息。

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-8.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-9.png)

![img](https://cdn-mogdb.enmotech.com/docs-media/sca/result-10.png)

### 详细 SQL 支持列表

在报告的根目录中，`sql_detail_list.csv` 文件中记录了本次 SQL 兼容度评估涉及到的所有 SQL 的支持情况，以及部分 SQL 在评估过程中的自动化改写策略和改写后的 SQL 文本。

文件中的字段内容主要根据数据库的不同，略有差异。

#### Oracle

1. schema_name: 执行 SQL 的 Schema 信息，在 Oracle 中一般对应于用户
2. sql_type: SQL 类型，区分采集到的 SQL 是系统 SQL 还是用户的业务 SQL，取值为： USER, SYSTEM_CATALOG, SYSTEM_COMMAND
3. module: 执行 SQL 语句的客户端 module
4. action: 执行 SQL 语句的客户端 action
5. mogdb_error: MogDB 中的 SQL 执行错误码
6. mogdb_error_message: MogDB 中的 SQL 执行报错信息
7. sql_text: 原始 SQL 文本
8. transform_matched: SQL 满足的自动化改写规则
9. sql_rewrite: 经过自动化改写后的 SQL 文本

#### MySQL

1. schema_name: 执行 SQL 的 Schema 信息，在 MySQL 中一般对应于数据库
2. sql_type: SQL 类型，区分采集到的 SQL 是系统 SQL 还是用户的业务 SQL，取值为： USER, SYSTEM_CATALOG, SYSTEM_COMMAND
3. user_host: 执行 SQL 的 MySQL 客户端用户和对应的执行客户端主机信息
4. mysql_error: SQL 在 MySQL 中的执行情况，如果是 0 则表明 SQL 执行成功，非 0 表示在 MySQL 中执行报错
5. mogdb_error: MogDB 中的 SQL 执行错误码
6. mogdb_error_message: MogDB 中的 SQL 执行报错信息
7. sql_text: 原始 SQL 文本
8. transform_matched: SQL 满足的自动化改写规则
9. sql_rewrite: 经过自动化改写后的 SQL 文本

#### DB2

1. schema_name: 执行 SQL 的 Schema 信息
2. sql_type: SQL 类型，区分采集到的 SQL 是系统 SQL 还是用户的业务 SQL，取值为： USER, SYSTEM_CATALOG, SYSTEM_COMMAND
3. stmt_type: DB2 中记录的 SQL 语句类型
4. mogdb_error: MogDB 中的 SQL 执行错误码
5. mogdb_error_message: MogDB 中的 SQL 执行报错信息
6. sql_text: 原始 SQL 文本
7. transform_matched: SQL 满足的自动化改写规则
8. sql_rewrite: 经过自动化改写后的 SQL 文本
