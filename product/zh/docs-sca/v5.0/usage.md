---
title: SCA 使用说明
summary: SCA 使用说明
author: Hong Rihua
date: 2022-03-07
---

# SCA 使用说明

## 部署步骤

程序已打包为二进制可执行文件，无需额外的部署操作。

程序文件上传后，确认能正常通过网络连接数据库即可直接使用。

## 常用命令

注意：以下以 x86-64 架构下的 Linux 系统为例进行说明，其他 ARM 平台或 MacOS 系统需要修改对应的命令名称。

* 申请 License (软件第一次运行的时候，需要先联机申请 License)

```shell
# 运行过程中，需要输入用户邮箱
# 申请到的 License 数据会发送到输入的邮箱中。
# 将 License 数据拷贝出来，并写入 SCA 同目录中的 license.json 文件内即可
./sca_linux_x86_64 -T L
```

* 初始化资料库 (对于同一个目标MogDB数据库，只在第一次运行时需要初始化资料库)

```shell
./sca_linux_x86_64 -T i -H <host> -P <port> -N <repo-database> -U <repo-user> -E <repo-password> --user <super_user> --password <super_password>
```

* Oracle 数据采集 (无需资料库)

```shell
# 作为测试用途，可添加如下参数加快 SQL 采集： -q 0.001 -Q 60 -m off
./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n <target_db> -u <Oracle_user> -e <Oracle_password>
```

* 从指定 MySQL 慢日志采集

```shell
./sca_linux_x86_64 -T MC -d <data_directory> --slow-log=<slow_logfile>
```

* 从指定 MySQL 服务器进行自动采集

```shell
./sca_linux_x86_64 -T MC -d <data_directory> -h <host> -p <port> -n <target_db> -u <MySQL_user> -e <MySQL_password>
```

* 进行 Oracle 兼容评估 (资料库已初始化，且资料库登录信息均为默认值)

```shell
./sca_linux_x86_64 -T OI -d <unzipped data directory> -n <target_db>
```

* 只进行 SQL 模拟 (资料库已初始化，且资料库用户密码信息均为默认值)

```shell
./sca_linux_x86_64 -T OS -h <host> -p <port> -n <target_db> -d <unzipped data directory>
```

* 同时进行SQL兼容度与性能评估 (资料库已初始化)

```shell
./sca_linux_x86_64 -T OIS -h <host> -p <port> -n <target_db> -u <repo_user> -e <repo_password> -d <unzipped data directory>
```
