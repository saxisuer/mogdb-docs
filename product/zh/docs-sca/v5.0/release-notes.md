---
title: 发布记录
summary: 发布记录
author: Hong Rihua
date: 2022-03-07
---

# 发布记录

## v5.0.0

### Feature

- 代码逻辑重构，命令选项和使用方式有较大改变
- 支持 MySQL 连库采集，通过数据库表中的慢日志信息采集，或者通过服务器端本地慢日志采集
- 支持 DB2 数据的连库采集，采集方式与 Oracle 类似，需要长期运行，默认采集一周
- 支持 DB2 SQL 兼容性分析，前提是需要提前准备好目标 MogDB 数据库
- 支持 Oracle SQL 详情列表文件的导出 (sql_detail_list.csv)
- 调整 Oracle 数据采集逻辑，移除不必要的对象信息与系统配置信息
- 调整资料库结构，只保留源库基础信息，SQL兼容审核，SQL性能对比等相关的结构

## v4.1.0

### Feature

- 支持 MySQL 数据库基于 slow-log 和 general-log 的 SQL 兼容度分析
- 支持自定义 SQL 转换规则

## v4.0.0

### Feature

- 集成全新设计的离线分析报告
- 支持单个可执行程序的数据采集，兼容性分析与性能评估
- 支持 Linux 系统下的 openGauss/MogDB 数据库的 sha256 密码认证方式
