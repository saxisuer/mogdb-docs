---
title: 发布记录
summary: 发布记录
author: Zhang Cuiping
date: 2021-10-08
---

# 发布记录

## v4.0.0

### Feature

- 集成全新设计的离线分析报告
- 支持单个可执行程序的数据采集，兼容性分析与性能评估
- 支持 Linux 系统下的 openGauss/MogDB 数据库的 sha256 密码认证方式
