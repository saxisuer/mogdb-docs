---
title: SCA 使用说明
summary: SCA 使用说明
author: hongyedba
date: 2021-09-30
---

# SCA 命令行选项

SCA 为纯命令行工具，所有功能均可通过单一的可执行程序完成。

## 通用选项

通用选项在三类任务中均有可能会使用。

### -h (帮助信息)

在使用分析程序前，建议仔细阅读帮助信息（命令： `./sca_linux_x86_64 -h`）:

```shell
Introduction:
    SCA is the program used to do SQL life-cycle inspection and simulation from Oracle to MogDB.
    There are 3 types of workers:
        1. [I] Compatible inspection about objects and SQLs
        2. [S] SQL performance simulator
        3. [C] Oracle data collector

Options:
                         --[ Overall ]--
    -h, --help           : Show current help message
    -v, --version        : Show current server version
    -d, --data           : Unzipped data directory for analyzer, or dest location for collection
    -t, --target-db      : Target database for all type of running
    -T, --type           : Running Type:  I = Inspection, and automatic building target database, this is default
                         :                S = Simulation, like Oracle SPA, use existing target database
                         :               IO = Inspection Only, use existing target database
                         :               IS = Inspection & Simulation, use existing target database
                         :                C = Run as agent, for data collection
    -w, --workers        : Parallel workers in each tasks, default: 10
    -x, --debug          : Enable debug mode
    -H, --host           : Server host address or name, default: 121.36.15.2
    -P, --port           : Database server port, default: 55432
    -F, --force          : Force mode, drop old objects before create it
                         : In repository initialization, means drop old repository if exists
    -l, --logfile        : Write output to both logfile (without progress-bar) and screen (with progress-bar)
    -L, --log-only       : Write output to only logfile (without progress-bar)
    -r, --report         : Final report file location, default in data directory with name 'report'
    -R, --rewrite        : PLSQL rewrite scripts location, default in data directory with name 'rewrite'
                         --[ Repository ]--
    -i, --init           : Initial repository, server connection information needed
    -U, --suser          : [Init] Super user used to login database server, default: mogdb
    -E, --spassword      : [Init] Password for super user, default: mogdb
    -n, --database       : Repository database, default: sca_db
    -u, --user           : Repository user, default: sca_repo
    -e, --password       : Repository password, default: SCA@password
                         --[ Collection ]--
    -C, --catagory       : Collection catagory: A = all, M = object metadata, Q = sql data, default is A
    -q, --sql-days       : How many days for session sql data, default: 7
    -Q, --sql-interval   : SQL collection interval in seconds, default: 600
    -U, --suser          : Oracle user used for data colletion, default: sys
    -E, --spassword      : Password for Oracle login user, default: oracle
    -s, --schema-include : Users/Schemas included in data collection, default: ''
    -S, --schema-exclude : Users/Schemas excluded in data collection
                         : Default: SYS,SYSTEM,OUTLN,QS_ES,QS_WS,QS_OS,QS_CBADM,QS_CB,SH,HR,QS,QS_CS,ORACLE_OCM,TSMSYS,DIP,PM,OE,DBSNMP,APPQOSSYS,WMSYS,EXFSYS,CTXSYS,ANONYMOUS,XDB,XS$NULL,ORDDATA,SI_INFORMTN_SCHEMA,ORDPLUGINS,ORDSYS,MDSYS,OLAPSYS,MDDATA,SPATIAL_WFS_ADMIN_USR,SPATIAL_CSW_ADMIN_USR,SYSMAN,MGMT_VIEW,APEX_030200,APEX_040200,FLOWS_FILES,APEX_PUBLIC_USER,OWBSYS,OWBSYS_AUDIT,SCOTT,UNKNOWN,PUBLIC,ODM,ODM_MTR,WKSYS,WKPROXY,DMSYS,IX,AQ$_ORDERS_QUEUETABLE_V
    -m, --enable-monitor : Starting monitor process in backgroud, Valid values: 1/on/true/t = ENABLE, 0/off/false/f = DISABLE, default: on

Usage:
    1. Init repository and analyze data (used for first running)
       ./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password> -d <unzipped data directory>
    2. Just analyze data
       ./sca_linux_x86_64 -d <unzipped data directory>
    3. Just Init repository (just need ran once for a repository database)
       ./sca_linux_x86_64 -i -H <host> -P <port> -U <super-user> -E <super-password>
    4. Just do SQL performance simulator (repository database is ready)
       ./sca_linux_x86_64 -T S -d <unzipped data directory>
    5. Do Data collection as agent for test (repository database is not needed)
       # Notice: "-q 0.001 -Q 60" means gather Session SQL only once
       #         "-m off" means do not monitor system status (CPU Idle and Disk Free)
       ./sca_linux_x86_64 -T C -s SCOTT -t '<target-db>' -H <host> -P <port> -U <oracle-user> -E <oracle-password> -q 0.001 -Q 60 -m off
    6. Do Data collection as agent for regular use (repository database is not needed)
       ./sca_linux_x86_64 -T C -s SCOTT -t '<target-db>' -H <host> -P <port> -U <oracle-user> -E <oracle-password>
```

### -v (查看版本)

查看当前 SCA 的版本信息：

```shell
hongyedba@localhost ~ % ./sca_linux_x86_64 -v
Server version: 4.0.0
```

### -d (数据目录)

除了数据采集（-TC）之外的所有任务类型，都需要指定 -d 数据目录。

任务会从数据目录中读取数据，将数据插入到资料库表中，最终生成的报告结果默认也会写入到数据目录下。

### -t (目标库)

指定目标库名称，通常是使用 MTK 或其他数据迁移工具，进行数据库结构和数据迁移时，在 MogDB 端创建的目标数据库。

* 在兼容评估中，只需要目标库有对象结构即可
* 在SQL模拟中，需要目标库既有对象结构，也有真实对等的全量生产数据，否则性能模拟的结果不具有参考意义

### -T (任务类型)

指定任务类型。

目前 SCA 支持如下类型的任务：

1. `I [Inspection]`:  自动创建目标库结构，并进行 SQL 兼容性评估
2. `IO [Inspection Only]`:  仅做兼容性评估，需要配合 `-t` 选项指定目标库
3. `S [Simulation]`:  仅做性能模拟任务（类似 Oracle SPA），需要配合 `-t` 选项指定目标库
4. `IS [Inspection & Simulation]`:  同时执行兼容性评估与性能模拟任务，需要配合 `-t` 选项指定目标库
5. `C [Collection]`: 执行数据采集任务，作为采集客户端使用，用于从 Oracle 中采集所需的对象信息与会话 SQL 执行性能数据

### -w (并发度)

指定用于运行任务的并发度大小，适量的并发度有助于提高各个任务的运行速度，默认为10个并发。

并发度用于： 文件数据加载到资料库，SQL兼容度评估，SQL复杂度评估，SQL性能模拟等操作中。

**注意**：

* 在SQL模拟任务中，并发度越大，可能导致单条SQL的执行效率下降，需要依据实际生产环境的负载压力选取合理的并发度，通常设置为生产库的平均活跃会话数。

### -x (Debug模式)

开启 Debug 模式，不会影响正常的分析逻辑，但是会输出大量的日志，通常用于辅助分析程序本身的运行异常。

### -H (主机)

采集模式下，指定 Oracle 数据库服务器的 IP 地址。

其他模式下，指定 MogDB 数据库服务器的 IP 地址。

### -P (端口)

采集模式下，指定 Oracle 数据库端口。

其他模式下，指定 MogDB 数据库端口。

### -F (强制模式)

是否启用强制模式，强制模式会在资料库初始化场景下生效。

在资料库初始化过程中，如果启用强制模式，会先删除资料库，然后重新创建。

### -l (日志文件)

指定程序日志文件，程序输出会同时输出到终端命令行与日志文件中。

### -L (日志文件)

指定程序日志文件，程序输出会仅输出到日志文件中。

### -r (报告目录)

指定报告目录，这里的报告包括 `兼容评估` 报告和 `SQL模拟` 报告，都是 html 格式报告，可离线查看。

### -R (PLSQL改写目录)

指定 PL/SQL 改写后的存储过程脚本目录。

## 资料库选项

### -i (初始化资料库)

执行资料库初始化任务，资料库初始化通常只需要在第一次运行 `兼容评估` 或 `SQL模拟` 任务时运行。

在数据采集任务中，无需资料库。

### -U (超级用户)

在采集模式下，指定 Oracle 数据库管理员用户的用户名。

在资料库初始化时，指定超级用户名称。后续各个任务分析时，需要使用 `-u` 指定 MogDB 的连接用户。

### -E (超级用户密码)

在采集模式下，指定 Oracle 数据库管理员用户的登录密码。

在资料库初始化时，指定使用的超级用户密码。后续各个任务分析时，需要使用 `-e` 指定 MogDB 的连接用户密码。

### -n (资料库名)

资料库初始化时，指定资料库名称，默认不指定的话，则会使用 `sca_db` 作为资料库名。

### -u (资料库用户)

资料库初始化时，指定资料库用户名称，默认不指定的话，则会使用 `sca_repo` 作为资料库用户名称。

### -e (资料库密码)

资料库初始化时，指定资料库用户密码，默认不指定的话，则会使用 `SCA@password` 作为资料库用户密码。

任务分析时，该参数则用于指定由 `-u` 选项指定的用户的登录密码。

## 数据采集选项

### -C (采集类别)

采集的类别分为如下三类：

- `A` : 采集所有数据，包括数据库基础结构信息，对象信息，会话 SQL 信息，SQL 性能数据
- `M` : 采集数据库基础结构信息，以及对象信息
- `Q` : 采集会话 SQL 信息，以及 SQL 性能数据

### -q (SQL采集天数)

指定 SQL 相关数据的采集总天数，由于从会话缓存 GV$SQLAREA 中采集已执行过的 SQL 数据，存在一定的概率漏采，可通过延长采集天数，减小漏采的概率。

默认会采集一周 7 天的 SQL 数据。

### -Q (SQL采集间隔)

指定 SQL 相关数据的采集间隔（单位：秒），默认每 10 分钟采集一次。

每次采集到的 SQL 数据会和已采集的数据进行对比去重，避免重复数据太多导致的数据文件过大。

### -s (Schema白名单)

指定数据采集的 Schema 白名单，即只采集白名单列表中列出的 Schema 相关数据。

### -S (Schema黑名单)

指定数据采集的 Schema 黑名单，即不采集黑名单列表中列出的 Schema 相关数据。

默认会将 Oracle 系统用户列入 Schema 黑名单中。

### -m (资源监控)

是否在后台启用资源监控子进程，默认启用。

资源监控子进程会定期（间隔 3 秒）查询当前服务器的 CPU 使用率，以及数据目录所在文件系统的剩余空间。

当 CPU 使用率高于 90%，或者文件系统剩余空间低于 100MB 时，监控子进程会触发信号，停止采集主进程，避免因为资源问题导致服务器故障。
