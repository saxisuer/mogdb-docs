---
title: SCA 使用说明
summary: SCA 使用说明
author: Hong Rihua
date: 2022-03-07
---

# SCA 命令行选项

SCA 为纯命令行工具，所有功能均可通过单一的可执行程序完成。

## 通用选项

通用选项在各类任务中均有可能会使用。

### --help (帮助信息)

在使用分析程序前，建议仔细阅读帮助信息（命令： `./sca_linux_x86_64 --help`）:

```shell
Introduction:
    SCA is a tool used to do SQL life-cycle inspection and simulation
    when migration from heterogeneous database to MogDB.
    Source databases supported as below:
        1. Oracle     : SQL Inspection, Simulation
        2. DB2        : SQL Inspection
        3. MySQL      : SQL Inspection
        4. PostgreSQL : SQL Inspection
        5. Informix   : SQL Inspection
        6. SQL Server : SQL Inspection (SQL Server)

Options:
                          --[ Overall ]--
        --help            : Show help message
    -v, --version         : Show SCA version
    -T, --type            : Run type:
                          :        I = Init SCA repository
                          :        L = Apply for license
                          :     ----------------[Analysis: Target required]---------------
                          :       OI = Oracle Inspection, this is default type
                          :       MI = MySQL Inspection
                          :       DI = DB2 Inspection
                          :       PI = PostgreSQL Inspection
                          :       II = Informix Inspection
                          :       SI = SQL Server Inspection
                          :       OS = Oracle Simulation, just like Oracle SPA
                          :      OIS = Oracle Inspection & Simulation
                          :     ------------[Collection: No need of target DB]------------
                          :       OC = Oracle Collection
                          :       MC = MySQL Collection
                          :       DC = DB2 Collection
                          :       PC = PostgreSQL Collection
                          :       IC = Informix Collection
                          :       SC = SQL Server Collection
    -d, --data            : Unzipped data directory for analyzer, or directory for collection
    -D, --data-id         : Use data with data id existed in the repository
    -w, --workers         : Parallel workers for tasks, default: 10
    -x, --debug           : Enable debug mode
    -l, --logfile         : Output to both logfile (without progress-bar) and screen (with progress-bar)
    -L, --log-only        : Output to only logfile (without progress-bar)
    -F, --force           : Force mode in REPO Creation, drop old objects before create it
    -r, --report          : Final report file location, default in data directory with name 'report'
        --report-lang     : Report language, you can use this option to create multiple report
        --license         : License file, default is [./license.json]
        --sql-modified    : Modified SQL list used in simulation
        --sql-config      : SQL configuration file (for internal use)
        --sql-transformer : Regular rules for SQL transformation (for internal use)
                          : Format: [{"name": "xxx"
                          :           "source": "xxx",
                          :           "target": "xxx",
                          :           "comment": "xxx"}, ...]
        --steps           : Run given steps (with step id or name) in analysis tasks
                          : Valid in type:
                          :     OI/MI/DI/PI/OS/OIS
                          : Step valid in order:
                          :     1.load            : Load data from file to repository database
                          :     2.make_object     : Create source objects in target database
                          :     3.inspect         : Do SQL compatible analysis
                          :     4.rollback_object : Rollback source objects created by make_object
                          :     5.simulate        : Do SQL performance analysis
                          :     6.html_report     : Genarate summary report in HTML format
                          :     7.excel_report    : Genarate detail report in Excel format
                          : Option value for example:
                          :     --steps 'load'      : Just load data to repository database
                          :     --steps 'inspect-'  : Run steps after 'inspect' (Step 3-7)
                          :     --steps '-5'        : Run steps before 'simulate' (Step 1-5)
                          :     --steps '2-5'       : Run steps from 'make_object' to 'simulate' (Step 2-5)
        --upgrade         : Upgrade current binary SCA command

                          --[ Repository Connection ]--
    -H, --repo-host       : Repository DB Server host address, default: 127.0.0.1
    -P, --repo-port       : Repository DB server port, default: 5432
    -N, --repo-name       : Repository database, default: sca_db
    -U, --repo-user       : Repository user, default: sca_repo
    -E, --repo-password   : Repository password, default: SCA@password
        --user            : Administrator used to create repository DB and user, default: mogdb
        --password        : Password for Administrator, default: mogdb

                          --[ Source & Target Connection ]--
    -h, --db-host         : Source & Target DB Server host address, default same as -H
    -p, --db-port         : Source & Target DB server port, default same as -P
    -n, --db-name         : Source & Target database, default same as -N
    -u, --db-user         : Source & Target user, default same as -U
    -e, --db-password     : Source & Target password, default same as -E
        --target-type     : Target database type in analysis tasks, default: MOGDB
                          : Valid type: ORACLE, MOGDB, OPENGAUSS, POSTGRESQL, MYSQL, DB2

                          --[ Collection Options ]--
    -q, --sql-days        : How many days for session sql data, default: 7
    -Q, --sql-interval    : SQL collect interval in seconds, default: 600
    -s, --schema-include  : Users/Schemas included in data collection, default: ''
    -S, --schema-exclude  : Users/Schemas excluded in data collection
                          : Default: <<depends on DB type>>
    -m, --enable-monitor  : Starting background monitor process in SQL Collection
                          : Valid values: 1/on/true/t = ENABLE, default: on
                          :               0/off/false/f = DISABLE
        --slow-log        : MySQL slow-log for client data collection
        --ignore-bind-plan: Ingore SQL binds and plans in Oracle data collection
        --sql-csv         : SQL file in csv format for SQL Inspection (@todo)

Usage:
    0. Apply for license
       ./sca_linux_x86_64 -T L
    1. Init repository (used for first running)
       ./sca_linux_x86_64 -T i -H <host> -P <port> -N <repo-database> -U <repo-user> -E <repo-password> --user <super_user> --password <super_password>
    2. Oracle data collection
       # Notice: "-q 0.001 -Q 60" means gather Session SQL only once
       #         "-m off" means do not monitor system status (CPU Idle and Disk Free)
       ./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n '<target-db>' -u <oracle-user> -e <oracle-password> -q 0.001 -Q 60 -m off
       ./sca_linux_x86_64 -T OC -s SCOTT -h <host> -p <port> -n '<target-db>' -u <oracle-user> -e <oracle-password>
    3. MySQL data collection using slow-log file
       ./sca_linux_x86_64 -T MC -d <report-directory> --slow-log=<slow-log-file>
    4. Oracle SQL compatible analysis (Required: Repository, Target DB)
       Note: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T OI -d <unzipped data directory> -n <target_db>
    5. Oracle SQL performance simulation (Required: Repository, Target DB)
       Note: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T OS -d <unzipped data directory> -n <target_db>
    6. MySQL SQL compatible analysis (Required: Repository, Target DB)
       Not e: use [H/P/N/U/E] options to assign the repository
             use [h/p/n/u/e] options to assign the target database
       ./sca_linux_x86_64 -T MI -d <unzipped data directory> -h <host> -p <port> -n <target-db>
```

### -v, --version (查看版本)

查看当前 SCA 的版本信息：

```shell
hongyedba@localhost ~ % ./sca_linux_x86_64 -v
SCA version: 5.1.0
```

### -T, --type (任务类型)

默认值： OI

指定任务类型，目前 SCA 支持如下类型的任务：

* 程序运行相关的任务

  1. `I [Initialize]`:  **初始化**，用于初始化 SCA 资料库
  2. `L [Apply License]`:  **申请 License**，用于 License 申请的任务

* 采集相关的任务

  1. `OC [Oracle Collection]`:  **Oracle 采集**，用于采集 Oracle 数据库中执行过的业务 SQL，需要长时间采集
  2. `MC [MySQL Collection]`:  **MySQL 采集**，用于采集 MySQL 数据库中执行过的业务 SQL，需提前配置慢日志，然后一次性采集
  3. `DC [DB2 Collection]`:  **DB2 采集**，用于采集 DB2 数据库中执行过的业务 SQL，需要长时间采集
  4. `PC [PostgreSQL Collection]`:  **PostgreSQL 采集**，用于采集 PostgreSQL 数据库中执行过的业务 SQL（依赖于 pg_stat_statments 插件），需要长时间采集
  5. `IC [Informix Collection]`:  **Informix 采集**，用于采集 Informix 数据库中执行过的业务 SQL（需要先手动开启 global sql trace 功能），需要长时间采集
  6. `SC [SQL Server Collection]`:  **SQL Server 采集**，用于采集 SQL Server 数据库中执行过的业务 SQL，需要长时间采集

* 分析相关的任务

  1. `OI [Oracle Inspection]`:  **Oracle 兼容评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  2. `MI [MySQL Inspection]`:  **MySQL 兼容评估**，用于评估源端 MySQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  3. `DI [DB2 Inspection]`: **DB2 兼容评估**，用于评估源端 DB2 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  4. `PI [PostgreSQL Inspection]`: **PostgreSQL 兼容评估**，用于评估源端 PostgreSQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  5. `II [Informix Inspection]`: **Informix 兼容评估**，用于评估源端 Informix 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  6. `SI [SQL Server Inspection]`: **SQL Server 兼容评估**，用于评估源端 SQL Server 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  7. `OS [Oracle Simulation]`: **Oracle 性能评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的执行性能
  8. `OIS [Oracle Inspection & Simulation]`: **Oracle 兼容和性能评估**，等同于 OI + OS 两种任务同时进行

### -d, --data (数据目录)

除了资料库初始化（-T I）之外的所有任务类型，都可以指定 -d 数据目录。

采集任务会将采集到的数据写入到 -d 指定的数据目录。

分析任务会从数据目录中读取数据，将数据插入到资料库表中，最终生成的报告结果默认也会写入到数据目录下。

### -D, --data-id (数据编号)

指定数据编号，然后直接从资料库读取指定编号的数据，而不是重新从数据目录中读取并加载数据。

指定 -D 选项后，会跳过加载数据的步骤，直接执行相关的分析任务。

### -w, --workers (并发度)

默认值： 10

指定用于运行任务的并发度大小，适量的并发度有助于提高各个任务的运行速度。

并发度用于： 文件数据加载到资料库，SQL兼容度评估，SQL复杂度评估，SQL性能模拟等操作中。

**注意**：

* 在SQL模拟任务中，并发度越大，可能导致单条SQL的执行效率下降，需要依据实际生产环境的负载压力选取合理的并发度，通常设置为生产库的平均活跃会话数。

### -x, --debug (Debug模式)

开启 Debug 模式，不会影响正常的分析逻辑，但是会输出大量的日志，通常用于辅助分析程序本身的运行异常。

### -l, --logfile (日志文件)

指定程序日志文件，程序输出会同时输出到终端命令行与日志文件中。

### -L, --log-only (仅日志文件)

指定程序日志文件，程序输出会仅输出到日志文件中。

### -F, --force (强制模式)

是否启用强制模式，强制模式会在资料库初始化场景下生效。

在资料库初始化过程中，如果启用强制模式，会先删除资料库，然后重新创建。

### -r, --report (报告目录)

默认值： <DATA_DIR>/report

指定报告目录，这里的报告包括 `兼容评估` 报告和 `SQL模拟` 报告，都是 html 格式报告，可离线查看。

同时，对于 `兼容评估` 任务，还会产生 `sql_detail_list.csv` 文件，记录所有 SQL 的评估结果，以及可能存在的改写方案。

### --report-lang (报告语言)

默认值： zh_CN

指定报告的语言，SCA 内置的语言包括 zh_CN (中文) 和 en_US (英文) 两种，同时也支持自定义的语言文件，还支持同时生成不同语言的报告文件。

若要使用自定义语言，那么请参考如下步骤进行：

1. 下载示例中的本地化语言文件（每个版本的本地化语言配置文件可能略有不同，请务必使用程序同版本的语言配置文件）
2. 自定义翻译语言文件中各个变量的对应语言值
3. 将编辑好的自定义语言文件上传到 sca 程序当前目录中，并修改其名称为 Localization_<语言名>.toml，其中 <语言名> 是自定义的语言名称，大小写无关
4. 运行 sca 分析任务，指定命令选项 --report-lang='<语言名>'

### --license (License文件)

默认值： ./license.json

License 文件的位置。

### --sql-modified (自定义SQL改写)

SQL 性能模拟中，每次运行性能模拟均会产生不支持 SQL 列表文件（存储于自定义的报告目录中），名称为： Simulator_modified_list.csv。

该选项用于在 SQL 性能模拟任务中，手动修改部分不支持的 SQL ，以达到更多的 SQL 支持率，尽可能对比更多的 SQL 性能变化。

自定义 SQL 修改的流程如下：

1. 使用标准的流程完成 SQL 性能模拟任务（第一次性能模拟）
2. 打开报告目录中的 Simulator_modified_list.csv 文件，手动修改该文件中的SQL语句语法，使其能在目标数据库中正确执行
3. 在原有选项的前提下，增加 `-D <data-id> --sql-modified <repor-dir>/Simulator_modified_list.csv` 选项，即可重新对失败的 SQL 进行性能模拟
4. 继续检查步骤二和步骤三，直到最终性能模拟的 SQL 范围符合预期，取出最终的分析报告

### --sql-config (自定义SQL)

【高级用法】 指定各个步骤中使用的SQL命令，仅为内部紧急故障调试使用。

### --sql-transformer (SQL转换规则)

【高级用法】 指定从源库到目标库的 SQL 转换规则，使用正则匹配进行转换。

### --steps

在分析过程中，运行指定的步骤，可使用步骤名称或编号。

该选项在以下任务类型中生效： OI, MI, DI, PI, OS, OIS

有效的步骤编号与名称说明如下：

1. load            : 将数据载入到资料库
2. make_object     : 在目标数据库中创建源库对象
3. inspect         : 执行 SQL 兼容性分析
4. rollback_object : 回退 make_objects 步骤中创建的源库对象
5. simulate        : 执行 SQL 性能模拟分析
6. html_report     : 创建 HTML 格式的汇总报告
7. excel_report    : 创建 Excel 格式的详细报告

合法的使用示例如下：

* --steps 'load'      : 只执行数据入库步骤
* --steps 'inspect-'  : 执行 'inspect' 之后的各个步骤 (步骤 3-7)
* --steps '-5'        : 执行 'simulate' 之前的各个步骤 (步骤 1-5)
* --steps '2-5'       : 执行从 'make_object' 到 'simulate' 中的步骤 (步骤 2-5)

### --upgrade

在运行环境可联网的情况下，自动升级当前 SCA 可执行程序。

## 资料库选项

### -H, --repo-host (资料库IP地址)

默认值： 127.0.0.1

资料库数据库 IP 地址。

### -P, --repo-port (资料库端口)

默认值： 5432

资料库数据库端口。

### -N, --repo-name (资料库名称)

默认值： sca_db

资料库数据库名称。

### -U, --repo-user (资料库用户)

默认值： sca_repo

资料库数据库登录用户。

### -E, --repo-password (资料库密码)

默认值： SCA@password

资料库数据库登录密码。

### --user (管理员用户)

资料库管理员用户，用于初始化资料库过程中，创建资料库用户，和资料库数据库。

### --password (管路员密码)

资料库管理员用户登录密码，用于初始化资料库过程中，创建资料库用户，和资料库数据库。

## 源或目标库连接

### -h, --db-host (源或目标库IP地址)

源或目标库数据库 IP 地址，默认继承 -H 选项值。

### -p, --db-port (源或目标库端口)

源或目标库数据库端口，默认继承 -P 选项值。

### -n, --db-name (源或目标库名称)

源或目标库数据库名称，默认继承 -N 选项值。

在 SQL 分析任务中，指定目标库名称，通常是使用 MTK 或其他数据迁移工具，进行数据库结构和数据迁移时，在 MogDB 端创建的目标数据库。

需要注意的是，资料库用户默认需要对目标库有完全的操作权限，默认资料库是管理员权限。

* 在兼容评估中，只需要目标库有对象结构即可
* 在SQL模拟中，需要目标库既有对象结构，也有真实对等的全量生产数据，否则性能模拟的结果不具有参考意义

### -u, --db-user (源或目标库用户)

源或目标库数据库登录用户，默认继承 -U 选项值。

### -e, --db-password (源或目标库密码)

源或目标库数据库登录密码，默认继承 -E 选项值。

### --target-type (目标库类型)

指定分析任务总，目标数据库类型，默认为 MOGDB。

当前此参数并未完善，仅支持 MOGDB/POSTGRESQL。

## 数据采集选项

### -q, --sql-days (SQL采集天数)

指定 SQL 相关数据的采集总天数，由于从会话缓存 GV$SQLAREA 中采集已执行过的 SQL 数据，存在一定的概率漏采，可通过延长采集天数，减小漏采的概率。

默认会采集一周 7 天的 SQL 数据。

### -Q, --sql-interval (SQL采集间隔)

指定 SQL 相关数据的采集间隔（单位：秒），默认每 10 分钟采集一次。

每次采集到的 SQL 数据会和已采集的数据进行对比去重，避免重复数据太多导致的数据文件过大。

### -s, schema-include (Schema白名单)

指定数据采集的 Schema 白名单，即只采集白名单列表中列出的 Schema 相关数据。

### -S, schema-exclude (Schema黑名单)

指定数据采集的 Schema 黑名单，即不采集黑名单列表中列出的 Schema 相关数据。

默认会将 Oracle 系统用户列入 Schema 黑名单中。

### -m, enable-monitor (资源监控)

是否在后台启用资源监控子进程，默认启用。

资源监控子进程会定期（间隔 3 秒）查询当前服务器的 CPU 使用率，以及数据目录所在文件系统的剩余空间。

当 CPU 使用率高于 90%，或者文件系统剩余空间低于 100MB 时，监控子进程会触发信号，停止采集主进程，避免因为资源问题导致服务器故障。

### --slow-log (慢查询日志)

采集时，指定 MySQL 慢查询日志文件。

在采集程序无法访问目标 MySQL 数据库，以及不在 MySQL 服务器上运行采集程序时，可手动将目标库 MySQL 慢日志取出来，使用当前选项指定慢日志即可解析并生成对应的采集数据，可用于后续 MySQL SQL 兼容性评估任务。

### --ignore-bind-plan (忽略绑定变量与执行计划)

在 Oracle 数据采集过程中，忽略 SQL 绑定变量和执行计划数据的采集，忽略采集能避免由于绑定变量或执行计划解析而导致的 Oracle 字典查询 Bug，并加快数据采集时间，但可能导致后期数据分析的精度下降。
