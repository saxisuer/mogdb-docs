---
title: SCA 介绍
summary: SCA 介绍
author: Hong Rihua
date: 2022-03-07
---

# SCA 介绍

SCA 全称 SQL Compatible Analysis，是一款异构数据库迁移前的 SQL 兼容和性能评估工具。

可用于异构数据迁移前的兼容性评估，评估源数据库中的实际业务 SQL 在目标库中是否存在语法问题，以及评估两款异构数据库中的 SQL 实际执行的性能差异。

本工具目前支持五种源端数据库，包括： Oracle, MySQL, DB2, PostgreSQL, Informix, SQL Server。

**适用于**： MogDB（以及其他基于 openGauss 的数据库）

SCA 程序可运行如下 14 种类型的任务：

* 程序运行相关的任务

  1. `I [Initialize]`:  **初始化**，用于初始化 SCA 资料库
  2. `L [Apply License]`:  **申请 License**，用于 License 申请的任务

* 采集相关的任务

  1. `OC [Oracle Collection]`:  **Oracle 采集**，用于采集 Oracle 数据库中执行过的业务 SQL，需要长时间采集
  2. `MC [MySQL Collection]`:  **MySQL 采集**，用于采集 MySQL 数据库中执行过的业务 SQL，需提前配置慢日志，然后一次性采集
  3. `DC [DB2 Collection]`:  **DB2 采集**，用于采集 DB2 数据库中执行过的业务 SQL，需要长时间采集
  4. `PC [PostgreSQL Collection]`:  **PostgreSQL 采集**，用于采集 PostgreSQL 数据库中执行过的业务 SQL（依赖于 pg_stat_statments 插件），需要长时间采集
  5. `IC [Informix Collection]`:  **Informix 采集**，用于采集 Informix 数据库中执行过的业务 SQL（需要先手动开启 global sql trace 功能），需要长时间采集
  6. `SC [SQL Server Collection]`:  **SQL Server 采集**，用于采集 SQL Server 数据库中执行过的业务 SQL，需要长时间采集

* 分析相关的任务

  1. `OI [Oracle Inspection]`:  **Oracle 兼容评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  2. `MI [MySQL Inspection]`:  **MySQL 兼容评估**，用于评估源端 MySQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  3. `DI [DB2 Inspection]`: **DB2 兼容评估**，用于评估源端 DB2 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  4. `PI [PostgreSQL Inspection]`: **PostgreSQL 兼容评估**，用于评估源端 PostgreSQL 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  5. `II [Informix Inspection]`: **Informix 兼容评估**，用于评估源端 Informix 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  6. `SI [SQL Server Inspection]`: **SQL Server 兼容评估**，用于评估源端 SQL Server 中采集到的业务 SQL，在目标端 MogDB 中的实际兼容性
  7. `OS [Oracle Simulation]`: **Oracle 性能评估**，用于评估源端 Oracle 中采集到的业务 SQL，在目标端 MogDB 中的执行性能
  8. `OIS [Oracle Inspection & Simulation]`: **Oracle 兼容和性能评估**，等同于 OI + OS 两种任务同时进行

注意：

- 兼容评估，需要有完整的源库结构，推荐使用 MTK 工具进行源数据库结构迁移，测试用途下也可使用 SCA 自动创建测试目标库结构
- SQL 模拟，需要有完整的源库数据和数据，推荐使用 MTK 工具进行源数据库结构以及数据迁移

## 操作系统与平台支持

SCA 目前支持在如下操作系统和平台架构下运行：

1. Linux x86_64
2. Linux arm64 (ARM 平台由于没有 DB2/Informix 客户端，故不支持 DB2/Informix 数据采集)
3. MacOS (MacOS 版本不支持 openGauss/MogDB 中的 SHA256 加密认证方式)

## 数据库支持

SCA 目前支持的源端与目标端数据库类型如下：

1. 源端：
   - Oracle: 不低于 10.2
   - MySQL: 不低于 5.5
   - DB2: 不低于 11.5
   - PostgreSQL: 不低于 9
   - Informix: 不低于 14.10 (更低版本未做验证)
   - SQL Server: 不低于 10.0 (更低版本未做验证)
   - File: MySQL slow-log

2. 目标端：
   - MogDB/openGauss: 不低于 2.0
   - PostgreSQL: 不低于 13.0

**注意**： 平台支持，与数据库支持没有相关性，数据库可以运行在其他平台，如 Windows/AIX 等，只要从数据库到 SCA 运行主机网络与端口互通即可。
