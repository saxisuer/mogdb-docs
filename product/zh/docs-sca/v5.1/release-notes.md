---
title: 发布记录
summary: 发布记录
author: Hong Rihua
date: 2022-03-07
---

# 发布记录

## v5.4.0 (2022-11-13)

- [sca_linux_arm64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.4.0/sca_linux_arm64)
- [sca_linux_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.4.0/sca_linux_x86_64)
- [sca_macos_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.4.0/sca_macos_x86_64)
- [示例报告](https://cdn-mogdb.enmotech.com/sca/SCA_v5.4.0/sca_sample_report_v5.2.zip)
- [示例语言配置文件](https://cdn-mogdb.enmotech.com/sca/SCA_v5.4.0/Localization_en_US.toml)

### 功能

- 增加对 SQL Server 数据库的采集与 SQL 兼容性分析功能

### 改进

- 扩充资料库 sca_database.db_version 字段，以便存储 SQL Server 原始版本信息

### 修复

- 修复Oracle 10.2版本 PLSQL 采集时，DEFAULTD 字段缺失导致的报错

## v5.3.2 (2022-10-24)

- [sca_linux_arm64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.2/sca_linux_arm64)
- [sca_linux_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.2/sca_linux_x86_64)
- [sca_macos_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.2/sca_macos_x86_64)
- [示例报告](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.2/sca_sample_report_v5.2.zip)
- [示例语言配置文件](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.2/Localization_en_US.toml)

### 功能

- 增加 --report-lang 可以指定报告的语言，支持同时生成多种不同语言的报告

### 改进

- 增加大量 MySQL 相关的改写规则
- License 认证流程优化调整（使用 License v31），不再支持旧版本的 License 文件
- Oracle Excel 报告的 SQL详情 sheet 页中增加 SQL 复杂度字段
- 调整 Oracle 数据字典语句的判断标准，将 sys.|mdsys.|system. 的查询语句判定为字典查询

### 修复

- 修复 sca_sql_result 资料库表中的 mogdb_message 字段类型长度不足导致的报错
- 调整 Oracle 性能对比查询 SQL，修复部分逻辑问题

## v5.3.1 (2022-09-23)

- [sca_linux_arm64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.1/sca_linux_arm64)
- [sca_linux_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.1/sca_linux_x86_64)
- [sca_macos_x86_64](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.1/sca_macos_x86_64)
- [示例报告](https://cdn-mogdb.enmotech.com/sca/SCA_v5.3.1/sca_sample_report_v5.2.zip)

### 功能

- 增加 -d/--data 可以指定多个数据包的功能，针对同一个库的多次采集数据进行合并分析
- 支持在本地具有 gsql 命令的情况下，对目标库主动运行 compat-tools 脚本，以便增加兼容度

### 改进

- 日志输出更新，减少部分比必要的日志输出
- 部分代码逻辑微调，全局参数微调

### 修复

- 移除 openGauss/MogDB 中对 systimestamp 函数不带括号的改写规则

## v5.3.0 (2022-08-30)

### 功能

- 增加对 Informix 源库 SQL 与对象基础信息的采集功能，同步调整资料库结构
- 增加 "--upgrade" 命令行选项，支持工具的在线升级

### 改进

- PLSQL 对象信息增加 OBJECT_ID 字段，兼容源库中可能存在的同名函数重载
- PLSQL 重建逻辑调整，适配 Informix 数据库
- 优化 MySQL 慢日志采集逻辑，增加对较大的慢日志进行分析采集的效率
- 增加对绑定变量 DD/MM/YYYY HH24:MI:SS 格式的时间数据的识别
- 支持 Excel 中 SQL 改写部分的红色标识，并同步修改研发过程中发现的一些正则匹配规则问题
- 移除 Oracle 采集 SQL 中使用 row_number() 进行数据去重的逻辑，影响采集性能
- 改写规则调整，Oracle/DB2 大写转小写精确匹配

### 修复

- 修复部分 DB2 中自定义 SQL 解析命名不正常的问题
- 修复 12c 以后 PDB 库分析时，查询SQL详情时默认 CON_ID=0 查询不到数据的问题
- 修复指定 --slow-log 的情况下，无法采集 MySQL 数据的问题
- 修复目标库为原生 PG 库时，性能对比逻辑执行报错的问题
- 捕获openGauss/PG中check_sql中的非数据库异常，避免对整体分析任务的影响

## v5.2.3 (2022-06-20)

### 功能

暂无

### 改进

- 调整 Oracle 性能对比页面中，数据小数情况下的展示，优化浮点数展示，显示 0-1 之间小数点前的 0
- 调整 Oracle 采集过程中权限检查逻辑
- 优化 Oracle SQL 复杂度分析数据的查询 SQL 的性能

### 修复

- 修复 Oracle 数据采集中，Package 中 Procedure 无参数的数据异常问题

## v5.2.2 (2022-06-13)

### 功能

- 增加 "--steps" 选项，支持指定运行分析过程中的各个步骤
- 增加 "--sql-config" 选项，支持自定义各个任务中使用到的 SQL (高级功能，仅供内部使用)
- 增加 "--ignore-bind-plan" 选项，可屏蔽对 Oracle SQL 绑定变量和执行计划的采集，避免部分数据字典查询解析的 Bug
- 增加 Excel 版详细数据报告（内含分类汇总饼图，详细 SQL 列表以及各个改写规则涉及的 SQL 列表），并移除旧版本 csv 详细列表文件
- 支持 "-d" 直接指定 zip 文件，即 zip 文件或解压后的目录都可以执行

### 改进

- 对于报错信息为 "already exists" 结尾的 SQL，认为语法上已经支持，统一更新为已支持
- 更改规则名称，SCA 运行相关的改写统一使用 "SCA_" 开头，并在展示时进行相应的屏蔽
- 通过实际客户环境中的数据，对 Oracle SQL 改写规则进行大幅度的新增和修改，以达到更精确和广泛的匹配
- 移除 "user" 转换为 "current_user()" 的转换规则
- License 调整，增加 version,type 属性，feature 限制为只适用于 sca
- Oracle 采集：未探测到用户列表时，给出具体报错信息并退出采集
- 对象补全调整：Debug 模式下不进行已补全对象的清理工作，便于调试
- 对象补全模块中，统一创建 Schema，并增加对 SQL 中出现的序列的创建和回退
- 移除 stmt_transformer 函数中双引号内数据大小写翻转逻辑，大小写处理采用 POST 正则规则统一处理
- 优化对象重建过程中的大小写问题，增加对大小写混合名称的处理

### 修复

- 修复 Oracle 性能模拟过程中，rollback 影响 search_path 的问题
- 对象重建时，函数统一重建为 IMMUTABLE 函数，避免部分情况下函数属性导致的报错，例如: [42703] Alias "label" reference with volatile function included is not supported.
- 修复对象删除时对象名称未加双引号导致的报错
- 修复在未收集绑定变量信息的情况下，Oracle 绑定变量处理报错的问题
- 修复部分 Oracle 场景下，module, action 字段超长导致的 SQL 信息入库报错
- 修复缺失 control.dat 文件的情况下，未能导入字段定义与 PLSQL 定义的问题
- 修复 MySQL 版本数据获取异常，导致的对象信息采集报错的问题
- 修复 MySQL 仅从指定慢日志采集时，对象信息采集报错的问题
- 修复 force 模式下，SQL 兼容性评估进度条长度不对的问题
- 对象补全问题修复： Create schema 问题，进度数据查询问题，非 Package 函数创建错误的问题
- 增加无参数函数添加括号的规则及其测试用例，调整部分测试出现异常的规则，确保所有规则的测试用例都正常
- 绑定变量解析时，对字符型变量值，将其中的单引号替换为双单引号

## v5.2 (2022-05-10)

### 功能

- 增加 --sql-modified 选项，支持在 SQL 模拟中手动调整待执行的 SQL
- SQL 兼容度评估支持断点续跑，支持与强制模式联动

### 改进

- 在各种分析任务中，增加分析完成后的概要信息输出
- openGauss/MogDB 库模块中，增加 copy_db 函数的错误详细信息输出
- 调整函数 SQL_Normalize，支持按数据库区别的 SQL 规范化操作
- 调整对象补全逻辑的范围，只在有必要进行SQL兼容度评估的任务中执行
- 在 Oracle SQL 采集时，对于 FMS= 0 的 SQL，采用 Python 自定义函数计算 SQL_HASH 值，减少部分重复 DML 出现的概率

### 修复

- 修复数据中存在双引号，导致 csv 格式数据 copy 到数据库报错的问题
- 修复资料库中由于 NULL 值与空字符串判断，导致的一些异常
- 修复 Package_name 信息为空导致的兼容性分析报错
- 修复 Oracle SQL 改写时，关键词改写规则误判的问题 (V$VERSION => V$"VERSION")

## v5.1.0

### Features

- 增加源数据库支持： PostgreSQL，其 SQL 数据来源于 pg_stat_statements 插件采集
- 增加对象信息采集： 所有源库均会自动采集字段信息与 PLSQL 参数信息，并在 SQL 兼容性审核中自动创建使用

### Bugfixs

- 修复部分情况下，进度条输出会因队列问题导致异常的报错问题
- 修复 MySQL 采集过程中 performance_schema 未能识别为 SYSTEM_CATALOG 的问题
- 修复 MySQL 对象信息采集时，没有过滤系统内置对象的问题
- 修复替换 openGauss 驱动后，-D 选项由于类型绑定问题导致的报错
- 修复 License 验证时，未能验证 License 失效时间的问题
- 修复慢日志文件中，SQL 文本中含有 # 开头的行，导致解析报错的问题
- 修复 SQL 兼容度汇总页面中，对于源库为 Oracle 时，表格数据排序不正常的问题
- 修复因为 JS 问题导致的部分浏览器下离线报告无法展示的问题
- 修复 MySQL 慢日志采集时，解析不到 ID 数据导致的异常退出
- 修复 --debug 选项不识别，需要指定值的问题
- 修复部分场景下 Oracle 数据采集字符集报错的问题 ORA-29275 (字符集转换 + 拼接空字符串)
- 修复 Oracle 为源库的报告中，由于 GaussDB 资料库没有 median 函数，导致的复杂度数据查询报错

### Improvements

- 连接目标库之后统一设置 search_path 包含所有 schema，降低 SQL 兼容度分析进程中找不到表的概率
- 增加 SQL 改写规则，针对 Oracle 数据库增加了部分 SQL 改写规则
- csv 格式的详细 SQL 列表调整，使用中文表头和部分中文内容，增加支持类别字段，便于阅读和理解
- 调整 SQL 兼容度汇总页面数据逻辑，NULL 相关改写不算改写支持
- 调整 MySQL 数据获取逻辑，严格按照 log_output 中 FILE/TABLE 的设定获取数据
- 驱动及配套代码调整，对于 PostgreSQL 库使用 psycopg2 驱动，对于 openGauss 库使用 py_opengauss 驱动
- 移除部分冗余的 format_size 函数及其调用

## v5.0.0

### Feature

- 代码逻辑重构，命令选项和使用方式有较大改变
- 支持 MySQL 连库采集，通过数据库表中的慢日志信息采集，或者通过服务器端本地慢日志采集
- 支持 DB2 数据的连库采集，采集方式与 Oracle 类似，需要长期运行，默认采集一周
- 支持 DB2 SQL 兼容性分析，前提是需要提前准备好目标 MogDB 数据库
- 支持 Oracle SQL 详情列表文件的导出 (sql_detail_list.csv)
- 调整 Oracle 数据采集逻辑，移除不必要的对象信息与系统配置信息
- 调整资料库结构，只保留源库基础信息，SQL兼容审核，SQL性能对比等相关的结构

## v4.1.0

### Feature

- 支持 MySQL 数据库基于 slow-log 和 general-log 的 SQL 兼容度分析
- 支持自定义 SQL 转换规则

## v4.0.0

### Feature

- 集成全新设计的离线分析报告
- 支持单个可执行程序的数据采集，兼容性分析与性能评估
- 支持 Linux 系统下的 openGauss/MogDB 数据库的 sha256 密码认证方式
