---
title: SQL Server 到 MogDB 评估
summary: SQL Server 到 MogDB 评估
author: Hong Rihua
date: 2022-11-12
---

# SQL Server 到 MogDB 评估

## MogDB 环境准备

需要自行准备 MogDB/openGauss 数据库环境，用于资料库和目标库，资料库和目标库默认使用同一套环境。

## 对象结构迁移

- **结构迁移**: 需要在准备好的 MogDB/openGauss 环境中，创建目标库，并将源库中的数据结构迁移到新创建的目标库中。
- **数据迁移**: 如果想要进行 SQL 性能对比评估，那么除了迁移数据库结构之外，还需要迁移表数据，确保源 SQL Server 与目标 MogDB/openGauss 库数据规模一致，性能对比才有意义。

相关工具：

- **MTK**: 可使用 MTK 工具完成结构和数据迁移，具体详情请参考： [https://mogdb.io/mtk](https://mogdb.io/mtk)

## SQL Server 数据采集

目前 SQL Server 的数据采集是从缓存中持续采集 SQL，默认情况下，采集过程会定期（根据缓存中的 SQL 总跨越时间）进行一次采集，共采集一周的缓存 SQL 数据。

### 所需权限

采集用户所需权限如下（建议使用管理员用户）：

> VIEW SERVER STATE
>
> VIEW DEFINITION

### 相关命令

```shell
./sca_linux_x86_64 -T SC -h <host> -p <port> -n <source-sql-server-db> -u <sql-server-user> -e <sql-server-password>

# 命令选项说明：
# -h/p/t/u/e 指定连接的源端 SQL Server 库的连接方式
# 考虑到共享缓存中 SQL 内容不一定全面，采集会持续性增量采集，默认采集一周的数据。如果只是做功能性验证，可使用 -q 0.0001 选项，即只进行一次 SQL 数据采集。
```

### 采集结果

采集完成后，会生成一个 zip 数据包，该数据包可以拷贝到目标库，并解压成数据目录。

后续兼容性分析，则依赖此数据包中的数据，且无需再次连接到源 SQL Server 数据库。

## 资料库初始化

```shell
# 使用具有管理员权限的 MogDB/openGauss 用户，进行资料库初始化
# 资料库初始化会默认创建名称为 sca_db 的资料库
# 可通过 -N 指定资料库名称，-U 指定资料库用户名，-E 指定资料库用户密码

./sca_linux_x86_64 -T i -H <host> -P <port> -N sca_db -U sca_repo -E 'SCA@password' --user <super-user> --password <super-password>
```

## 执行分析任务

### 兼容性分析

SQL Server 数据库目前只支持 SQL 兼容性分析，命令如下：

```shell
# 若资料库名称，用户名，密码非默认，则需要使用 -N, -U, -E 选项指定
# 若目标库信息与资料库不同，则需要使用 -h, -p, -n, -u, -e 选项指定

./sca_linux_x86_64 -T SI -H <host> -P <port> -n <target-db> -d <unzipped data directory>
```

分析完成后，会在 -d 指定的目录中生成 report，可将 report 离线下载后进行查看。
