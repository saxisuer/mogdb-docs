---
title: "mtk check-table-data"
summary: mtk check-table-data
author: mtk
date: 2022-09-21
---
## mtk check-table-data

检查表数据是否存在异常数据

### Synopsis

检查表数据是否存在异常数据

1. date/datetime type contains zero data data

    - MySQL

        ```sql
        select * from table where (col_date >= '0000-00-00' and col_date < '0001-01-01')
        ```

2. varchar/varchar2/char/text type contains char(0),chr(0),\0,0x00,x'00'

    - MySQL

        ```sql
        select * from table where (col_varchar like CONCAT('%', char(0),'%')
        ```

    - DB2

        ```sql
        select * from table where (col_varchar like '%'||x'00'||'%')
        ```

    - Oracle

        ```sql
        select * from table where (col_varchar like '%'||chr(0)||'%')
        ```

3. Character set encoding
    - Oracle

        ```sql
        -- converts the column data to the encoding of the data character set,
        -- and then compares the length with the original data
        select * from table where ( LENGTHB(col_gbk_n) != LENGTHB(convert(col_gbk_n,'UTF8')) )
        ```

```bash
mtk check-table-data [flags]
```

### Options

```bash
  -h, --help   help for check-table-data
```

### Options inherited from parent commands

```bash
      --batchSize int            指定批量插入或Copy条数大小.
                                 有效值为 1-50000, 默认大小为 1000
                                 支持Oracle、PostgreSQL、openGauss、MySQL.
      --bufferSize int           指定批量插入、Copy缓存大小. 
                                 有效值为1-1024，默认为8 MB
                                 for postgres,openGauss,mysql.
      --caseSensitive int        SQL语句中的对象大小写参数.
                                 1 - 小写 
                                 2 - 大写 
                                 3 - 保持与源数据库相同.
      --channelCacheNum int      指定队列大小.
                                 有效值为 1-50000, 默认10000
  -c, --config string            设置MTK配置文件。支持json、yaml格式. [env MTK_CONFIG] (default "mtk.json")
      --cpBufferSize int         定义Copy命令中使用的缓存大小（以MB为单位）. 
                                 有效值为1-1024，默认批量大小为8 MB
                                 for Postgres,openGauss.
  -d, --debug                    设置调试模式.
                                 正常使用不需要此选项. [env MTK_DEBUG]
      --disableCollStatistics    禁用采集统计信息.
      --disableFKCons            禁用表外键约束同步
      --disableIgnoreCase        禁用查询忽略大小写
      --disableSelectPart        禁言分区并行查询
      --disableTableDataComp     禁用统计表行数对比.
      --enableSyncCompTabPro     启用同步表压缩属性
      --fetchSize int            指定一次行提取行数大小 
                                 有效值为 1-50000, 默认大小为 1000
                                 for Oracle. (default 1000)
      --file                     导出成文件
      --fileType string          指定导出的文件类型。
                                 支持csv、sql
      --noTerminalReport         终端不打印迁移报告概览
  -p, --parallel int             指定并行度. 
                                 并行度目前仅在并行迁移表数据和创建索引时有用. (default 1)
      --path string              指定要导出数据的文件的目录。
                                 默认值为 config.target.parameter.path。如果不配置，系统默认值./data
                                 命令值 > 配置值 > MTK 默认值
      --preRun                   预运行.
  -r, --reportFile string        设置 mtk 报告文件或目录。如果是文件，则使用文件目录。
                                 默认值为 ./report
                                 报告目录格式 [./report/reportYYYYMMDDHHMISS] ./report/report20210101121314
      --schemas string           迁移模式, 以逗号分隔. (schema1,schema2,db1)
      --seqLastNumAddNum int     指定序列最后值统一增加多少
      --tableSkip stringArray    指定表拆分条件,允许指定多少. 
                                 format schema.tableName
                                 --tableSkip MTK.TABLE01
                                 --tableSkip MTK.TABLE02
      --tableSplit stringArray   指定表拆分条件,允许指定多少. 
                                 format schema.tableName:where:where:where
                                 --tableSplit 'MTK.TABLE01: "ID">100 AND ID<=200: ID>200 AND ID<=300:ID>300'
                                 --tableSplit "MTK.TABLE02: \"ID\">100 AND ID<=200: ID>200 AND ID<=300:ID>300 AND COL1='1'"
      --tables string            迁移表, 以逗号分隔. (tab1,schema1.tab1)
```

### SEE ALSO

* [mtk](./mtk.md)     - 数据库迁移工具
