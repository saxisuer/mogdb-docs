---
title: "mtk"
summary: mtk
author: mtk
date: 2022-09-21
---
## mtk

数据库迁移工具

### Synopsis

MTK（数据库迁移工具）用于帮助您迁移数据库

```bash
mtk [flags]
```

### Examples

```
# init project
./mtk init-project -n ora2og

# Generate configuration file. 
# edit connection information and migration objects (tables/schemas). 
# the directory definition does not need to be modified
vi ora2og/config/mtk.json

# Run
./mtk -c ora2og/config/mtk.json

# Specify migration report
./mtk -c ora2og/config/mtk.json

# Specify debug mode
./mtk -c ora2og/config/mtk.json --debug

# Migrate to a file
./mtk -c ora2og/config/mtk.json --file

# Only Schema is migrated
./mtk -c ora2og/config/mtk.json --schemaOnly

# Only Data is migrated
./mtk -c ora2og/config/mtk.json --dataOnly

# Defining the migration table overrides the contents of the configuration file
./mtk -c ora2og/config/mtk.json --tables schema1.table1,schema2.table2

# Defining the migration schema overrides the contents of the configuration file
./mtk -c ora2og/config/mtk.json --schemas schema1,schema2

```

### Options

```bash
      --batchSize int            指定批量插入或Copy条数大小.
                                 有效值为 1-50000, 默认大小为 1000
                                 支持Oracle、PostgreSQL、openGauss、MySQL.
      --bufferSize int           指定批量插入、Copy缓存大小. 
                                 有效值为1-1024，默认为8 MB
                                 for postgres,openGauss,mysql.
      --caseSensitive int        SQL语句中的对象大小写参数.
                                 1 - 小写 
                                 2 - 大写 
                                 3 - 保持与源数据库相同.
      --channelCacheNum int      指定队列大小.
                                 有效值为 1-50000, 默认10000
  -c, --config string            设置MTK配置文件。支持json、yaml格式. [env MTK_CONFIG] (default "mtk.json")
      --cpBufferSize int         定义Copy命令中使用的缓存大小（以MB为单位）. 
                                 有效值为1-1024，默认批量大小为8 MB
                                 for Postgres,openGauss.
      --dataOnly                 仅迁移表数据
  -d, --debug                    设置调试模式.
                                 正常使用不需要此选项. [env MTK_DEBUG]
      --disableCollStatistics    禁用采集统计信息.
      --disableFKCons            禁用表外键约束同步
      --disableIgnoreCase        禁用查询忽略大小写
      --disableSelectPart        禁言分区并行查询
      --disableTableDataComp     禁用统计表行数对比.
      --fetchSize int            指定一次行提取行数大小 
                                 有效值为 1-50000, 默认大小为 1000
                                 for Oracle. (default 1000)
      --file                     导出成文件
      --fileType string          指定导出的文件类型。
                                 支持csv、sql
  -h, --help                     help for mtk
      --httpAddr string          设置MTK运行的Http服务端口 [env MTK_HTTP_ADDR]
      --logDir string            设置MTK运行的Http服务日志目录 [env MTK_LOG_DIR]
      --logfile string           设置MTK日志文件。默认值为reportFile目录.
      --noTerminalReport         终端不打印迁移报告概览
  -p, --parallel int             指定并行度. 
                                 并行度目前仅在并行迁移表数据和创建索引时有用. (default 1)
      --path string              指定要导出数据的文件的目录。
                                 默认值为 config.target.parameter.path。如果不配置，系统默认值./data
                                 命令值 > 配置值 > MTK 默认值
      --preRun                   预运行.
  -r, --reportFile string        设置 mtk 报告文件或目录。如果是文件，则使用文件目录。
                                 默认值为 ./report
                                 报告目录格式 [./report/reportYYYYMMDDHHMISS] ./report/report20210101121314
      --reportServer string      指定报告服务器 [env MTK_REPORT_SERVER]
      --schemaOnly               仅迁移模式DDL
      --schemas string           迁移模式, 以逗号分隔. (schema1,schema2,db1)
      --seqLastNumAddNum int     指定序列最后值统一增加多少
      --tableSkip stringArray    指定表拆分条件,允许指定多少. 
                                 format schema.tableName
                                 --tableSkip MTK.TABLE01
                                 --tableSkip MTK.TABLE02
      --tableSplit stringArray   指定表拆分条件,允许指定多少. 
                                 format schema.tableName:where:where:where
                                 --tableSplit 'MTK.TABLE01: "ID">100 AND ID<=200: ID>200 AND ID<=300:ID>300'
                                 --tableSplit "MTK.TABLE02: \"ID\">100 AND ID<=200: ID>200 AND ID<=300:ID>300 AND COL1='1'"
      --tables string            迁移表, 以逗号分隔. (tab1,schema1.tab1)
  -v, --version                  MTK Version
```

### SEE ALSO

* [mtk init-project](./mtk_init-project.md)     - 初始化 mtk 项目目录。目录将在当前目录下创建。
* [mtk config-check](./mtk_config-check.md)     - 检查配置文件是否有误
* [mtk config-gen](./mtk_config-gen.md)     - 交互式生成配置文件
* [mtk gen](./mtk_gen.md)     - 生成命令行文档，shell自动补全脚本
* [mtk license](./mtk_license.md)     - 管理许可证
* [mtk mig-tab-pre](./mtk_mig-tab-pre.md)     - 迁移数据库对象 Schema,Sequence,ObjectType,Domain,Queue,Wrapper,Server,UserMapping,Table,NickName,Rule
* [mtk mig-tab-data](./mtk_mig-tab-data.md)     - 迁移数据库对象 TableData,TableDataEstimate,CheckTableData
* [mtk mig-tab-post](./mtk_mig-tab-post.md)     - 迁移数据库对象 Index,Constraint,AlterSequence,TableDataCom,CollStatistics
* [mtk mig-tab-other](./mtk_mig-tab-other.md)     - 迁移数据库对象 DBLink,View,MaterializedView,Function,Procedure,Package,Trigger,Synonym
* [mtk mig-select](./mtk_mig-select.md)     - 迁移自定义查询语句到csv文件或目标表
* [mtk mig-selects](./mtk_mig-selects.md)     - 迁移自定义查询语句到csv文件或目标表
* [mtk show-schema](./mtk_show-schema.md)     - 查看源数据库方案.
* [mtk show-db-info](./mtk_show-db-info.md)     - 查看数据库基本信息.
* [mtk show-type](./mtk_show-type.md)     - 查看支持数据类型.
* [mtk show-table](./mtk_show-table.md)     - 查看Top n 表大小
* [mtk show-table-split](./mtk_show-table-split.md)     - 根据用户定义的并行度，并行拆分大表
* [mtk show-support-db](./mtk_show-support-db.md)     - 查看支持数据库版本.
* [mtk show-table-data-estimate](./mtk_show-table-data-estimate.md)     - 预估表数据迁移时间.
* [mtk report](./mtk_report.md)     - 转换MTK报告成sql脚本.
* [mtk sync-schema](./mtk_sync-schema.md)     - 迁移方案.
* [mtk sync-object-type](./mtk_sync-object-type.md)     - 迁移对象类型.
* [mtk sync-domain](./mtk_sync-domain.md)     - 迁移域.
* [mtk sync-sequence](./mtk_sync-sequence.md)     - 迁移序列.
* [mtk sync-queue](./mtk_sync-queue.md)     - 迁移队列表.
* [mtk sync-wrapper](./mtk_sync-wrapper.md)     - 迁移DB2 Wrapper.
* [mtk sync-server](./mtk_sync-server.md)     - 迁移DB2 Server.
* [mtk sync-user-mapping](./mtk_sync-user-mapping.md)     - 迁移DB2 User Mapping.
* [mtk sync-table](./mtk_sync-table.md)     - 迁移表.
* [mtk sync-nickname](./mtk_sync-nickname.md)     - 迁移DB2 Nickname.
* [mtk sync-table-data](./mtk_sync-table-data.md)     - 迁移表数据.
* [mtk sync-table-data-estimate](./mtk_sync-table-data-estimate.md)     - 预估表数据迁移时间.
* [mtk sync-index](./mtk_sync-index.md)     - 迁移索引.
* [mtk sync-constraint](./mtk_sync-constraint.md)     - 迁移约束.
* [mtk sync-view](./mtk_sync-view.md)     - 迁移视图.
* [mtk sync-mview](./mtk_sync-mview.md)     - 迁移物化视图.
* [mtk sync-trigger](./mtk_sync-trigger.md)     - 迁移触发器.
* [mtk sync-procedure](./mtk_sync-procedure.md)     - 迁移存储过程.
* [mtk sync-function](./mtk_sync-function.md)     - 迁移函数.
* [mtk sync-package](./mtk_sync-package.md)     - 迁移包.
* [mtk sync-synonym](./mtk_sync-synonym.md)     - 迁移同义词.
* [mtk sync-db-link](./mtk_sync-db-link.md)     - 迁移数据库链接.
* [mtk sync-rule](./mtk_sync-rule.md)     - 迁移规则.
* [mtk sync-table-data-com](./mtk_sync-table-data-com.md)     - 统计表行数对比.
* [mtk sync-alter-sequence](./mtk_sync-alter-sequence.md)     - 修改序列下一个值.
* [mtk sync-coll-statistics](./mtk_sync-coll-statistics.md)     - 采集统计信息.
* [mtk check-table-data](./mtk_check-table-data.md)     - 检查表数据是否存在异常数据
* [mtk show-drop-index](./mtk_show-drop-index.md)     - 生成删除索引语句
* [mtk show-drop-cons](./mtk_show-drop-cons.md)     - 生成删除约束语句
* [mtk version](./mtk_version.md)     - mtk version
* [mtk encrypt](./mtk_encrypt.md)     - 提供一种方便的方式来加密您的文本/密码
* [mtk convert-plsql](./mtk_convert-plsql.md)     - 转换PLSQL语句
* [mtk self](./mtk_self.md)     - MTK本身管理
