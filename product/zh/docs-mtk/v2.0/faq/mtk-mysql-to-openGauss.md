---
title: MTK 迁移MySQL到openGauss/MogDB
summary: MTK 迁移MySQL到openGauss/MogDB
author: Liu Xu
date: 2021-03-04
---

# 使用MTK迁移MySQL到openGauss/MogDB

## 支持版本

- 5.5
- 5.6
- 5.7
- 8.0

## 注意事项

### 空字符串和NULL值问题

- openGauss A模式下和MySQL不一样,需要处理数据 当列的属性为`NOT NULL`时转为 `" "`,允许为NULL时转为NULL
- openGauss PG模式下和MySQL一样, NULL != `''` 无需处理
- openGauss B模式下和MySQL一样, NULL != `''` 无需处理

数据库兼容类型。取值范围: A、B、C、PG。分别表示兼容Oracle、MySQL、Teradata和PostgreSQL。但是C目前已经放弃支持。因此常用的取值是A、B、PG。

  >说明：
  >
  > - A兼容性下，数据库将空字符串作为NULL处理，数据类型DATE会被替换为TIMESTAMP(0) WITHOUT TIME ZONE。
  > - 将字符串转换成整数类型时，如果输入不合法,B兼容性会将输入转换为0，而其它兼 容性则会报错。
  > - PG兼容性下，CHAR和VARCHAR以字符为计数单位，其它兼容性以字节为计数单位。例如，对于UTF-8字符集，CHAR(3)在PG兼容性下能存放3个中文字符，而在其它兼容性下只能存放1个中文字符。

### 表

- 自增列改写为默认值序列
- `CHARSET` 属性移除
- `ENGINE` 属性移除

#### 分区表

- openGauss 不支持 子分区表
- openGauss 不支持 Key 分区
- openGauss 不支持 分区键和分区键值为表达式
- range partition

  - days 函数 不支持
  - to_days

    MySQL

    ```sql
    CREATE TABLE mtk.range_part_t1 (
        prof_history_id BIGINT(20),
        person_id BIGINT(20) NOT NULL,
        organization_id BIGINT(20) NOT NULL,
        record_date DATETIME NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8 PARTITION BY RANGE (to_days(record_date))
    (
        PARTITION yr0 VALUES LESS THAN(736695),
        PARTITION yr7 VALUES LESS THAN(737060),
        PARTITION yr8 VALUES LESS THAN(737425),
        PARTITION yr9 VALUES LESS THAN(MAXVALUE)
    )
    ```

    openGauss

    ```sql
    CREATE TABLE mtk.range_part_t1 (
        prof_history_id BIGINT,
        person_id BIGINT NOT NULL,
        organization_id BIGINT NOT NULL,
        record_date TIMESTAMP NOT NULL
    ) PARTITION BY RANGE (record_date)
    (
        PARTITION yr0 VALUES LESS THAN('2017-01-01'),
        PARTITION yr7 VALUES LESS THAN('2018-01-01'),
        PARTITION yr8 VALUES LESS THAN('2019-01-01'),
        PARTITION yr9 VALUES LESS THAN(MAXVALUE)
    )
    ```

  - year

    MySQL

    ```sql
    CREATE TABLE mtk.range_part_t2 (
        prof_history_id BIGINT(20),
        person_id BIGINT(20) NOT NULL,
        organization_id BIGINT(20) NOT NULL,
        record_date DATETIME NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8 PARTITION BY RANGE (year(record_date))
    (
        PARTITION yr0 VALUES LESS THAN(2018),
        PARTITION yr7 VALUES LESS THAN(2019),
        PARTITION yr8 VALUES LESS THAN(2020),
        PARTITION yr9 VALUES LESS THAN(MAXVALUE)
    )
    ```

    openGauss

    ```sql
    CREATE TABLE mtk.range_part_t2 (
        prof_history_id BIGINT,
        person_id BIGINT NOT NULL,
        organization_id BIGINT NOT NULL,
        record_date TIMESTAMP NOT NULL
    ) PARTITION BY RANGE (record_date)
    (
        PARTITION yr0 VALUES LESS THAN('2018-01-01'),
        PARTITION yr7 VALUES LESS THAN('2019-01-01'),
        PARTITION yr8 VALUES LESS THAN('2020-01-01'),
        PARTITION yr9 VALUES LESS THAN(MAXVALUE)
    )
    ```

  - unix_timestamp

    MySQL

    ```sql
    CREATE TABLE mtk1.range_part_t3 (
        report_id INT(11) NOT NULL,
        report_status VARCHAR(20) NOT NULL,
        report_updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
    ) ENGINE=InnoDB DEFAULT CHARSET=UTF8 PARTITION BY RANGE (unix_timestamp(report_updated))
    (
        PARTITION p0 VALUES LESS THAN(1293811200),
        PARTITION p1 VALUES LESS THAN(1296489600),
        PARTITION p2 VALUES LESS THAN(1298908800),
        PARTITION p3 VALUES LESS THAN(1301587200),
        PARTITION p4 VALUES LESS THAN(1304179200),
        PARTITION p5 VALUES LESS THAN(1306857600),
        PARTITION p6 VALUES LESS THAN(1309449600),
        PARTITION p7 VALUES LESS THAN(1312128000),
        PARTITION p8 VALUES LESS THAN(1314806400),
        PARTITION p9 VALUES LESS THAN(MAXVALUE)
    )
    ```

    openGauss

    ```sql
    CREATE TABLE mtk1.range_part_t3 (
        report_id INTEGER NOT NULL,
        report_status VARCHAR(60) NOT NULL,
        report_updated TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
    ) PARTITION BY RANGE (report_updated)
    (
        -- 1293811200 ---> '2011-01-01 00:00:00'
        PARTITION p0 VALUES LESS THAN('2011-01-01 00:00:00'),
        -- 1296489600 ---> '2011-02-01 00:00:00'
        PARTITION p1 VALUES LESS THAN('2011-02-01 00:00:00'),
        -- 1298908800 ---> '2011-03-01 00:00:00'
        PARTITION p2 VALUES LESS THAN('2011-03-01 00:00:00'),
        -- 1301587200 ---> '2011-04-01 00:00:00'
        PARTITION p3 VALUES LESS THAN('2011-04-01 00:00:00'),
        -- 1304179200 ---> '2011-05-01 00:00:00'
        PARTITION p4 VALUES LESS THAN('2011-05-01 00:00:00'),
        -- 1306857600 ---> '2011-06-01 00:00:00'
        PARTITION p5 VALUES LESS THAN('2011-06-01 00:00:00'),
        -- 1309449600 ---> '2011-07-01 00:00:00'
        PARTITION p6 VALUES LESS THAN('2011-07-01 00:00:00'),
        -- 1312128000 ---> '2011-08-01 00:00:00'
        PARTITION p7 VALUES LESS THAN('2011-08-01 00:00:00'),
        -- 1314806400 ---> '2011-09-01 00:00:00'
        PARTITION p8 VALUES LESS THAN('2011-09-01 00:00:00'),
        PARTITION p9 VALUES LESS THAN(MAXVALUE)
    )
    ```

### 列

#### 列类型映射

| MySQL COLUMN_TYPE |     openGauss data_type     | data_length | data_precision | data_scale |   openGauss     ex_data_type   |
|-------------------|-----------------------------|-------------|----------------|------------|--------------------------------|
| bigint(20)        | bigint                      |             |             64 |          0 | bigint                         |
| char(1)           | character                   |           1 |                |            | character(1)                   |
| varchar(20)       | character varying           |          20 |                |            | character varying(20)          |
| varchar(4000)     | character varying           |        4000 |                |            | character varying(4000)        |
| time              | time                        |             |                |            | time                           |
| date              | date                        |             |                |            | date                           |
| year              | int                         |             |                |            | int                            |
| datetime          | timestamp without time zone |             |                |            | timestamp(0) without time zone |
| timestamp(6)      | timestamp with time zone    |             |                |            | timestamp with time zone       |
| decimal(10,0)     | numeric                     |             |             10 |          0 | numeric(10,0)                  |
| decimal(38,0)     | numeric                     |             |             38 |          0 | numeric(38,0)                  |
| decimal(65,30)    | numeric                     |             |             65 |         30 | numeric(65,30)                 |
| double            | DOUBLE PRECISION            |             |                |            | DOUBLE PRECISION               |
| double(10)        | numeric                     |             |             10 |          5 | numeric(10)                    |
| double(10,5)      | numeric                     |             |             10 |          5 | numeric(10,5)                  |
| blob              | bytea/blob                  |             |                |            | bytea/blob                     |
| tinyblob          | bytea/blob                  |             |                |            | bytea/blob                     |
| mediumblob        | bytea/blob                  |             |                |            | bytea/blob                     |
| longblob          | bytea/blob                  |             |                |            | bytea/blob                     |
| tinytext          | text                        |             |                |            | text                           |
| text              | text                        |             |                |            | text                           |
| mediumtext        | text                        |             |                |            | text                           |
| longtext          | text                        |             |                |            | text                           |
| binary(10)        | bytea                       |          10 |                |            | bytea                          |
| varbinary(10)     | bytea                       |          40 |                |            | bytea                          |
| enum              | enum type                   |             |                |            | enum type                      |
| [set](#set)       | varchar                     |             |                |            | varchar                        |
| Geometry          | varchar/Geometry            |             |                |            | varchar/Geometry               |
| GeometryCollection| varchar/Geometry            |             |                |            | varchar/Geometry               |
| GemoCollection    | varchar/Geometry            |             |                |            | varchar/Geometry               |
| LineString        | varchar/Geometry            |             |                |            | varchar/Geometry               |
| MultiPoint        | varchar/Geometry            |             |                |            | varchar/Geometry               |
| MultiPolygon      | varchar/Geometry            |             |                |            | varchar/Geometry               |
| Point             | varchar/Geometry            |             |                |            | varchar/Geometry               |
| Polygon           | varchar/Geometry            |             |                |            | varchar/Geometry               |
| MultilineString   | varchar/Geometry            |             |                |            | varchar/Geometry               |

- MySQL `bigint unsigned` 自增列迁移到 openGauss 需要注意序列大小
- 字段长度会根据迁移到openGauss的兼容模式进行扩充
- timestamp列默认值为 `0000-00-00 00:00:00` 修改为 `1970-01-01`

##### [Spatial Data Type](https://dev.mysql.com/doc/refman/8.0/en/spatial-types.html)

MTK 2.5.3 支持. 如果安装 `Postgis` 迁移为`geomotry`,否则为`varchar`.

支持以下类型:
    + Geometry
    + GeometryCollection
    + LineString
    + MultiPoint
    + MultiPolygon
    + Point
    + Polygon
    + MultilineString

#### Set

MySQL Set类型可以插入多个值，暂时迁移到openGauss varchar 类型

### 数据

- 时间列数据为 `0000-00-00 13:14:13` 这种数据在openGauss不支持

    可通过参数[mySQLSkipErrorDateTimeData](./../config/mtk-parameter.md#mysqlskiperrordatetimedata) or  [igErrorData](./../config/mtk-parameter.md#igerrordata) 忽略

#### 约束

- 在openGauss/MogDB端会自动改名
- 不支持子表的外键指向父表的非唯一索引

  ```sql
  CREATE TABLE `t1` (
  `id` int(11) NOT NULL,
  `uuid` binary(16) NOT NULL COMMENT 'UUID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uuid` (`uuid`) USING BTREE
  );
  CREATE TABLE `t2` (
  `c1_uuid` binary(16) NOT NUL,
  `c2_uuid` binary(16) NOT NULL,
  PRIMARY KEY (`c1_uuid`,`c2_uuid`),
  CONSTRAINT `FK_T1` FOREIGN KEY (`c2_uuid`) REFERENCES `t1` (`uuid`) ON DELETE RESTRICT ON UPDATE RESTRICT
  );
  -- FK_T1 约束不会迁移. 它指向的依赖是普通索引
  ```

#### 索引

- 在openGauss/MogDB端会自动改名
- 索引分区暂时不支持
