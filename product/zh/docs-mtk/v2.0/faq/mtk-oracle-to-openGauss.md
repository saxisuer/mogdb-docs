---
title: MTK 迁移Oracle到openGauss/MogDB
summary: MTK 迁移Oracle到openGauss/MogDB
author: Liu Xu
date: 2021-03-04
---

# 使用MTK迁移Oracle到openGauss/MogDB

## 支持版本

- 10.2+
- 11.2+
- 12c+

<br/>

MTK 使用 Golang 进行开发. 连接Oracle的驱动有以下选择.

|驱动名称|描述|
|---|---|
|[godror](https://github.com/godror/godror)  |cgo 需要Oracle客户端|
|[go-oci8](https://github.com/mattn/go-oci8)|cgo 需要Oracle客户端|
|[ora](https://github.com/rana/ora)         |cgo 需要Oracle客户端|
|[go-ora](https://github.com/sijms/go-ora)  |Pure go 不需要Oracle客户端|

CGO类型的驱动性能低于Go原生语言驱动,是因为架构限制,如godror驱动调用路径为 `Godror Driver->CGO Libraries->ODPI-C->OCI`. 一般性能瓶颈在`CGO Libraries`

> [efficient-fetching-of-data-from-oracle-database-in-golang](https://www.syntio.net/en/labs-musings/efficient-fetching-of-data-from-oracle-database-in-golang)
> Drivers based on CGO have a complex architecture, they wrap around an existing library which may also be another wrapper or technology. If we take a look at the Godror Golang driver, we’ll see that it’s a 4-level architecture driver:

MTK 新增参数 `pureDriver` 支持使用 `go-ora`驱动. 在启用此参数后MTK会自动判断在查询数据库表数据时是否使用 `go-ora` 驱动. 存在以下情况不会使用

- 表存在XML列类型
- 表存在Spatial类型
- 表存在UDT类型

在使用`go-ora`时`Number`类型的数据会存在异常数据情况,MTK内部进行`TO_CHAR(COL_NAME) AS COL_NAME`进行查询

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "127.0.0.1",
      "user": "system",
      "port": 1521,
      "password": "oracle",
      "dbName": "mtk",
      "dsn": "",
      "charset": "ZHS16GBK",
      "pureDriver": true
    },
    "parameter": {
      "debugTest": false
    }
  }
}
```

## Oracle迁移用户权限

迁移用户必须能访问`DBA_*`/`ALL_*`视图和查询迁移表权限.

- connect
- select any dictionary
- select any table
- select_catalog_role

> MTK 从 2.3.0 以后访问 `DBA_*`视图,不在查询`ALL_*`视图.

## MTK Type 和oracle type 对应

对应 `DBA_OBJECT` 里的 `OBJECT_TYPE`

| MTK        | Oracle               |
| ---------- | -------------------- |
| Schema     | User                 |
| ObjectType | TYPE/TYPE BODY       |
| Domain     |                      |
| CustomType |                      |
| Sequence   | SEQUENCE             |
| Queue      |                      |
| Table      | TABLE                |
| Constraint |                      |
| Index      | INDEX                |
| View       | VIEW                 |
| Trigger    | TRIGGER              |
| Procedure  | PROCEDURE            |
| Function   | FUNCTION             |
| Package    | PACKAGE/PACKAGE BODY |
| Synonym    | Synonym              |
| DBLink     |                      |
| Rule       |                      |

<br/>

## 注意事项

### 空字符串和NULL问题

Oracle 里 `''` = `NULL` 和 openGauss A兼容模式一致, 和`PG`兼容模式 `''` ! = `NULL` 不一样.

DBCOMPATIBILITY [ = ] compatibility_type

指定兼容的数据库的类型。取值范围: A、B、C、PG。分别表示兼容Oracle、MySQL、Teradata和PostgreSQL。但是C目前已经放弃支持。因此常用的取值是A、B、PG。

  >说明：
  >
  > - A兼容性下，数据库将空字符串作为NULL处理，数据类型DATE会被替换为TIMESTAMP(0) WITHOUT TIME ZONE。
  > - 将字符串转换成整数类型时，如果输入不合法,B兼容性会将输入转换为0，而其它兼容性则会报错。
  > - PG兼容性下，CHAR和VARCHAR以字符为计数单位，其它兼容性以字节为计数单位。例如，对于UTF-8字符集，CHAR(3)在PG兼容性下能存放3个中文字符，而在其它兼容性下只能存放1个中文字符。

### 序列

MogDB/openGauss 3.0.0 以后迁移成 Large Sequence.

MogDB/openGauss 3.0.0 之前如果值大于9223372036854775807会有警告信息。

### Type

- CREATE OR REPLACE TYPE TY_STR_SPLIT IS TABLE OF VARCHAR2 (4000)

    > MogDB/openGauss 3.0.0 Support
    >
    > MTK v2.4.2 Support

### 表

- IOT_OVERFLOW 不支持
- NESTED TABLE 不支持
- OBJECT TYPE TABLE

    > MogDB/openGauss 3.0.0 Support
    >
    > MTK v2.4.2 Support

    ```sql
    CREATE OR REPLACE TYPE address_obj as OBJECT(
          street VARCHAR2(20),
          city VARCHAR2(20),
          state CHAR(2),
          zip CHAR(5));
    /
    CREATE TABLE address_table OF ADDRESS_OBJ;
    ```

- 分区表

  - Hash Partition Table
  - List Partition Table
  - Range Partition Table
  - Sub Partition Table
  - System Partition table
  - interval Partition openGauss语法限制部分支持
    - Convert interval(NUMTOYMINTERVAL(1,'year')) --&gt; interval(1 year)

        |             Oracle             | openGauss |
        |--------------------------------|-----------|
        | NUMTOYMINTERVAL(1,'YEAR')      | 1 year    |
        | NUMTOYMINTERVAL ( 1, 'MONTH' ) | 1 MONTH   |
        | numtodsinterval ( 1, 'day' )   | 1 day     |

    - number interval                      -- invalid input syntax for type interval

- 物化视图的表不迁移,迁移物化视图

### Char 字段类型

如果存在字符集转换,需要注意Char类型字段数据.

- Oracle

```sql

SQL> create table tab_char(id number, col_char char(2));

Table created.

SQL> insert into tab_char values (1,'1');

1 row created.

SQL> select id,'"'||col_char||'"' ,col_char from tab_char;

    ID '"' C
---------- --- -
    1 "1 " 1
```

- openGauss/MogDB

由于从GBK字符集迁移到UTF8,字段长度会变大

```sql
MogDB=#create table tab_char(id bigint, col_char char(2));
CREATE TABLE
MogDB=#insert into tab_char values (1,'1');
INSERT 0 1
MogDB=#select id,concat('"',col_char,'"') ,col_char from tab_char;
 id | concat | col_char
----+--------+----------
  1 | "1  "   | 1              >> 多了个空格
(1 row)
```

### 列

#### 列类型转换

|            Oracle                         |          openGauss          |
|-------------------------------------------|-----------------------------|
| SMALLINT(number(*,0))                     | bigint                      |
| INTEGER(number(*,0))                      | bigint                      |
| NUMBER(8,5)                               | numeric(8,5)                |
| NUMBER(*,5)                               | numeric(38,5)               |
| NUMBER(38,127)                            | numeric(127)                |
| NUMBER(38,-64)                            | numeric(102)                |
| NUMBER                                    | numeric                     |
| DECIMAL(8,5)                              | numeric(8,5)                |
| NUMERIC(8,5)                              | numeric(8,5)                |
| REAL(float(64))                            | numeric                     |
| FLOAT(20)                                 | numeric                     |
| BINARY_FLOAT                              | numeric                     |
| BINARY_DOUBLE                             | numeric                     |
| CHAR(8)                                   | character(8)                |
| NCHAR(8)                                  | character(8)                |
| VARCHAR(8)                                | character varying(8)        |
| VARCHAR2(8)                               | character varying(8)        |
| NVARCHAR2(8)                              | character varying(8)        |
| DATE                                      | timestamp without time zone |
| TIMESTAMP                                 | timestamp without time zone |
| TIMESTAMP WITH LOCAL TIME ZONE            | timestamp with time zone    |
| TIMESTAMP WITH TIME ZONE                  | TIMESTAMP WITH TIME ZONE    |
| INTERVAL YEAR TO MONTH                    | interval                    |
| ITERVAL DAY TO SECOND                     | interval                    |
| LONG                                      | text                        |
| RAW(111)                                  | RAW(111)                    |
| LONG RAW                                  | bytea                       |
| CLOB                                      | text/CLOB                   |
| BLOB                                      | bytea/BLOB                  |
| NCLOB                                     | text/CLOB                   |
| BFILE                                     | Not Support                 |
| [User-Defined Types][UDT]                  | Not Support                 |
| [Any Types][AT]                           | Not Support                 |
| [URI Data Types][URI_Data_Types]          | Not Support                 |
| [URIFactory Package][URIFactory_Package]  | Not Support                 |
| SDO_GEOMETRY                              | Partially Supported <br/> See : [Spatial](#spatial)|
| ST_GEOMETRY                               | Partially Supported <br/> See : [Spatial](#spatial)|
| [SDO_TOPO_GEOMETRY][SDO_TOPO_GEOMETRY]    | Not Support                 |
| [SDO_GEORASTER][SDO_GEORASTER]            | Not Support                 |

[UDT]: https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-7CF27C66-9908-4C02-9401-06C2F2C4021C

[AT]: https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-5A8C5AC6-BC32-4D78-B0DE-037162106C72

[URI_Data_Types]: https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-6C9AC925-4E3F-476D-BB63-5A70CC12FC40

[URIFactory_Package]: <https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-1CA616C7-BFA7-4AFC-A199-7589DA049CB6>

[SDO_TOPO_GEOMETRY]: https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-66AF10E5-137D-444B-B9BC-89B2B340E278

[SDO_GEORASTER]: https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html#GUID-CFEFCFAC-4756-4B90-B88D-D89B861C1628

#### 列默认值

- sys_guid

    ```sql
    SQL> SELECT sys_guid() FROM dual;

    SYS_GUID()
    --------------------------------
    B2CBE5EE3AB4032BE053030011AC6BD3
    ```

    ```sql
    CREATE or replace FUNCTION pg_catalog.sys_guid() RETURNS varchar AS $$
    BEGIN
    RETURN md5(random()::text || clock_timestamp()::text);
    END;
    $$ LANGUAGE plpgsql;
    ```

- TO_CHAR(SYSTIMESTAMP, 'TZR') from dual;

#### Number

- number(38,127) --&gt; numeric(127, 127)

    当 p < s的时候，使用numeric(s, s)。此时，Oracle可以控制小数点后0的个数

    ```sql
    create table o_test3(value number(38,127));
    INSERT INTO o_test3 VALUES (00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012345678901234567890123456890123456789012345678901234567890123456789);
    SELECT * FROM o_test3;
    ```

- number(38,-64) --&gt; numeric(102)

    ```sql
    create table o_test4(value number(38,-64));
    INSERT INTO o_test4 VALUES(12345678901234567890123456789012345678901234567890123456789012345678900000000000000000000000000000000);
    INSERT INTO o_test4 VALUES(123456789012345678901234567890123456789012345678901234567890123456789000000000000000000000000000000000);
    INSERT INTO o_test4 VALUES(123456789012345678901234567890123456789012345678901234567890123456789000000000000000000000000000000001);
    SELECT * FROM o_test4;
    ```

#### Timestamp

- Timestamp
  - Oracle Timestamp 精度最大为9, openGauss/mogdb
- Timestamp with local time zone
    Oracle 会转换插入时间为数据库时区时间. openGauss/mogdb 使用 Timestamp local time zone
- Timestamp local time zone
    Oracle 不转换时区. openGauss/mogdb 暂时不支持. openGauss/mogdb 使用 Timestamp local time zone 会丢失原有客户端时区信息

#### 列名

openGauss数据库系统列:

- "CMAX"
- "CMIN"
- "CTID"
- "TABLEOID"
- "TID"
- "XMAX"
- "XMIN"

以上列名用户不能进行列名定义.

### 约束

- Oracle主键约束和表名一致,在openGauss会报错。程序自动后名称后面增加_PK。
- C - Check constraint on a table
- P - Primary key
- U - Unique key
- R - Referential integrity
- V - With check option, on a view           -- 不支持
- O - With read only, on a view              -- 不支持
- H - Hash expression
- F - Constraint that involves a REF column
- S - Supplemental logging                   -- 不支持

### 索引

- bitmap索引不支持

### 视图

- Select语句部分列别名问题

    ```sql
    select name name from dual      -- rewrite name as name
    ```

- Oracle TRANSLATE(xx USING NCHAR_CS) 函数

    ```sql
    select TRANSLATE(col1 USING NCHAR_CS) from (
        select 1 as col1 from dual;
    )
    ```

### 同义词

不迁移DBLink下的同义词

```sql
SELECT OWNER,SYNONYM_NAME,TABLE_OWNER,TABLE_NAME FROM DBA_SYNONYMS WHERE DB_LINK IS NULL;
```

### Spatial

迁移 Oracle Spatial 以下参数可用

- [enableConvertSrid](./../config/mtk-parameter.md#enableconvertsrid)
- [defaultSrid](./../config/mtk-parameter.md#defaultsrid)

迁移 Oracle Spatial 数据会查询**pg_extension**判断数据库是否安装**postgis**

- **postgis**安装则迁移为**geometry**
- **postgis**未安装则迁移为**text**

支持情况如下:

- [SDO_GTYPE][SDO_GTYPE]
  - [2,3,4]0[0-7] Support
  - [2,3,4]0[8-9] No Support
- [SDO_ETYPE][SDO_ETYPE]
  - 1, 2, 1003, 2003, 4,1005, 2005 Support
  - [2006,1007]: Not Supprt

[SDO_GTYPE]: https://docs.oracle.com/en/database/oracle/oracle-database/21/spatl/spatial-datatypes-metadata.html#GUID-4D84CE67-67E4-4A84-8255-C586E765A94B
[SDO_ETYPE]: https://docs.oracle.com/en/database/oracle/oracle-database/21/spatl/spatial-datatypes-metadata.html#GUID-270AE39D-7B83-46D0-9DD6-E5D99C045021

<br/>

### 其他问题

#### ORA-29275: partial multibyte character

- AL32UTF8 TO UTF8
  在 `source`-`parameter` 下配置参数[charAppendEmptyString](./../config/mtk-parameter.md#charappendemptystring) 为 true
- ZHS16GBK TO UTF8
  - 方法一
      在 `source`-`parameter` 下配置参数[charAppendEmptyString](./../config/mtk-parameter.md#charappendemptystring) 为 true
  - 方法二
      在 `source`和`target`下的 `connect` 下配置参数[clientCharset](./../config/mtk-config.md#clientcharset) 为 `GBK`

      在 `target`-`parameter` 下配置参数[igErrorData](./../config/mtk-parameter.md#igerrordata) 为 true

#### ORA-01805 msg:possible error in date/time operation

- 查询 timezone_file

    1. query oracle database server timezone_file version

        ```sql
        sqlplus "/ as sysdba"
        SQL> select * from v$timezone_file;

        FILENAME    VERSION     CON_ID
        -------------------- ---------- ----------
        timezlrg_34.dat        34    0
        ```

    2. query oracle client timezone_file version

        ```bash
        genezi -v
        Client Shared Library 64-bit - 19.3.0.0.0

        System name:  Darwin
        Release:  19.6.0
        Version:  Darwin Kernel Version 19.6.0: Thu Jun 18 20:49:00 PDT 2020; root:xnu-6153.141.1~1/RELEASE_X86_64
        Machine:  x86_64

        TIMEZONE INFORMATION
        --------------------
        Operating in Instant Client mode.

        Small timezone file = /opt/client_19/oracore/zoneinfo/timezone_32.dat
        Large timezone file = /opt/client_19/oracore/zoneinfo/timezlrg_32.dat
        ```

    如果客户端版本和服务器端版本不一致，请升级timezone_file.

    服务器端版 : timezlrg_34.dat

    客户端版本 : timezlrg_32.dat

- 解决方案:

    1. 安装Oracle客户端。
    2. 升级timezone_file。具体查看Updated DST Transitions and New Time Zones in Oracle RDBMS and OJVM Time Zone File Patches (Doc ID 412160.1)

        ```
        # for oracle Instant client
        # 设置环境变量ORA_TZFILE
        export ORA_TZFILE=timezlrg_34.dat
        # 从其他已安装补丁的机器复制过来.
        cp timezlrg_34.dat $ORACLE_HOME/oracore/zoneinfo/
        genezi -v
        Client Shared Library 64-bit - 19.3.0.0.0
        System name: Darwin
        Release:  19.6.0
        Version:  Darwin Kernel Version 19.6.0: Thu Jun 18 20:49:00 PDT 2020; root:xnu-6153.141.1~1/RELEASE_X86_64
        Machine:  x86_64
        TIMEZONE INFORMATION
        --------------------
        Operating in Instant Client mode.
        Time zone file timezlrg_34.dat set in ORA_TZFILE environment variable will be used.
        Small timezone file = /opt/client_19/oracore/zoneinfo/timezlrg_34.dat
        Large timezone file = /opt/client_19/oracore/zoneinfo/timezlrg_34.dat
        ```

#### Segmentation Fault on some requests handling ROWID

<https://github.com/oracle/python-cx_Oracle/issues/315>

Try setting the environment variable ORA_OCI_NO_OPTIMIZED_FETCH to the value 1 before you run the query to avoid the issue

#### 乱码问题

1. 存储过程里中文乱码.

    MTK报告里显示乱码. 基本出现在AL32UTF8的数据库里出现了非UTF8编码的数据. 如GBK编码

    ```sql
    -- 使用Oracle Dump函数进行确定
    -- 从DBA_SOURCE查询对应的SQL 文本
    select line,text,dump(text,1016),dump(CONVERT(text, 'AL32UTF8'),1016) from dba_source where name='xxx';

    /*
        -- 模拟故障
        SELECT
            dump(CONVERT('请求系统响应', 'ZHS16GBK'),1016), -- 正常UTF8编码数据转为GBK
            dump(CONVERT(CONVERT('请求系统响应', 'ZHS16GBK'),'AL32UTF8','ZHS16GBK'),1016) -- 正常UTF8编码数据转为GBK在转为UTF8
        FROM dual;
    */
    ```

2. invalid byte sequence for encoding

    此问题一般为源数据库存在乱码. 可以配置参数[igErrorData](./../config/mtk-parameter.md#igerrordata) 为 true,先跳过错误数据,然后在处理错误数据.

    找到报告里提示的错误数据文件. 参考如下操作

    > 此CSV通过DBeaver/Mogeaver 会正常导入,因为进行转码操作

    ```bash
    gsql ((openGauss 3.0.0 build 02c14696) compiled at 2022-04-01 18:29:12 commit 0 last mr  release)
    Non-SSL connection (SSL connection is recommended when requiring high-security)
    Type "help" for help.

    -- 使用CTAS创建临时表. 注意后面的表明修改为真实表明
    omm=# create table copy_table as select * from XXX.TABLE where 1=0;
    INSERT 0 0
    omm=# \COPY TABLE FROM 'xxx_table_0.csv' WITH csv header DELIMITER ',' encoding 'UTF8';
    ERROR:  invalid byte sequence for encoding "UTF8": 0xe7 0x31 0x31
    CONTEXT:  COPY TABLE, line 18750
    ```

    根据报错信息找到有问题行的,去源数据库查询数据,发现乱码请尝试修复.

    Oracle

    ```SQL
    select dump(col_name,1016) from table_name t WHERE t.col_name='xxx';
    ```

    openGauss/MogDB/PostgreSQL

    ```SQL
    select upper(encode(col_name::bytea, 'hex')) from table_name t WHERE t.col_name='xxx'
    ```

<br/>

## 部分SQL语句

### Sequence

#### 11G

```sql
SELECT SEQUENCE_OWNER,SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY,
 ORDER_FLAG,
 CYCLE_FLAG,
 CACHE_SIZE,
 LAST_NUMBER,
 LAST_NUMBER + CACHE_SIZE AS STARTVAL
FROM
 DBA_SEQUENCES S
WHERE 1=1
```

#### 12C+

```sql
SELECT SEQUENCE_OWNER,SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY,
 ORDER_FLAG,
 CYCLE_FLAG,
 CACHE_SIZE,
 LAST_NUMBER,
 LAST_NUMBER + CACHE_SIZE AS STARTVAL
FROM
 DBA_SEQUENCES S
WHERE
 NOT EXISTS (
 SELECT
  1
 FROM
  DBA_TAB_IDENTITY_COLS C
 WHERE
  S.SEQUENCE_OWNER = C.OWNER
 AND S.SEQUENCE_NAME = C.SEQUENCE_NAME ) AND SEQUENCE_OWNER='ORA_MTK'
```

### Type

```sql
SELECT
 o.object_type,
 s.owner,
 s.name,
 s.line,
 s.text,
 t.typecode,
 o.timestamp,
 h.lvl
FROM
 DBA_SOURCE s,
 DBA_TYPES t,
 DBA_OBJECTS o,
 (
 SELECT
  NAME,
  MAX(LEVEL) lvl
 FROM
  (
  SELECT
   *
  FROM
   DBA_DEPENDENCIES
  WHERE
   TYPE = 'TYPE'
   AND owner = 'ORA_MTK')
 CONNECT BY
  nocycle referenced_name = PRIOR name
 GROUP BY
  name) h
WHERE
 s.type = 'TYPE'
 AND o.object_type = 'TYPE'
 AND s.owner = 'ORA_MTK'
 AND h.name = s.name
 AND s.owner = t.owner
 AND s.name = t.type_name
 AND s.owner = o.owner
 AND s.name = o.object_name
 AND o.status = 'VALID'
UNION ALL
SELECT
 o.object_type,
 s.owner,
 s.name,
 s.line,
 s.text,
 t.typecode,
 o.timestamp,
 h.lvl
FROM
 DBA_SOURCE s,
 DBA_TYPES t,
 DBA_OBJECTS o,
 (
 SELECT
  NAME,
  MAX(LEVEL) lvl
 FROM
  (
  SELECT
   *
  FROM
   DBA_DEPENDENCIES
  WHERE
   TYPE = 'TYPE'
   AND owner = 'ORA_MTK')
 CONNECT BY
  nocycle referenced_name = PRIOR name
 GROUP BY
  name) h
WHERE
 s.type = 'TYPE BODY'
 AND o.object_type = 'TYPE BODY'
 AND s.owner = 'ORA_MTK'
 AND h.name = s.name
 AND s.owner = t.owner
 AND s.name = t.type_name
 AND s.owner = o.owner
 AND s.name = o.object_name
 AND o.status = 'VALID'
ORDER BY
 lvl,
 OWNER,
 NAME,
 object_type,
 LINE
```

### Queue

```sql
SELECT
 Q.OWNER,
 NAME,
 QUEUE_TYPE,
 Q.QUEUE_TABLE,
 QT.OBJECT_TYPE
FROM
 DBA_QUEUES Q,
 DBA_QUEUE_TABLES QT
WHERE
 Q.QUEUE_TABLE = QT.QUEUE_TABLE
 AND Q.OWNER = QT.OWNER AND
```

### Table

```sql
SELECT A.OWNER,A.TABLE_NAME,A.TABLESPACE_NAME,A.CLUSTER_NAME,A.IOT_NAME,A.LOGGING,A.PARTITIONED,A.IOT_TYPE,
  A.TEMPORARY,
  A.SECONDARY,
  A.NESTED,
  A.COMPRESSION,
  A.COMPRESS_FOR,
  B.COMMENTS,
  A.DURATION ,
  A.NUM_ROWS ,
  A.BLOCKS,
  A.AVG_ROW_LEN,
  A.TABLE_TYPE,
  A.TABLE_TYPE_OWNER
 FROM
 DBA_ALL_TABLES A,
 DBA_OBJECTS O,
 DBA_TAB_COMMENTS B
 WHERE NOT EXISTS (SELECT 1 FROM DBA_SNAPSHOTS S WHERE S.OWNER = A.OWNER AND S.TABLE_NAME=A.TABLE_NAME)
 AND A.OWNER=B.OWNER(+)
 AND A.TABLE_NAME=B.TABLE_NAME(+)
 AND A.OWNER=O.OWNER
 AND A.TABLE_NAME=O.OBJECT_NAME
 AND O.OBJECT_TYPE='TABLE'
```

### Column

#### 11G

```sql
SELECT
 /*+ RULE */
 C.OWNER,
 C.TABLE_NAME,
 C.COLUMN_NAME,
 COLUMN_ID,
 DATA_DEFAULT,
 DATA_TYPE,
 DATA_TYPE_OWNER,
 DATA_LENGTH,
 CHAR_LENGTH,
 DATA_PRECISION,
 DATA_SCALE,
 CHAR_USED,
 HIDDEN_COLUMN,
 VIRTUAL_COLUMN,
 NULLABLE,
 NULL COMMENTS,
 NULL AS IDENTITY_COLUMN
FROM
 DBA_TAB_COLS C
WHERE 1=1 AND HIDDEN_COLUMN='NO'  AND UPPER(C.OWNER) = 'ORA_MTK' AND (UPPER(C.TABLE_NAME) = 'T3' OR UPPER(C.TABLE_NAME) = 'T2') ORDER BY c.OWNER,c.TABLE_NAME,c.COLUMN_ID
```

#### 12C

```sql
SELECT
 /*+ RULE */
 C.OWNER,
 C.TABLE_NAME,
 C.COLUMN_NAME,
 COLUMN_ID,
 DATA_DEFAULT,
 DATA_TYPE,
 DATA_TYPE_OWNER,
 DATA_LENGTH,
 CHAR_LENGTH,
 DATA_PRECISION,
 DATA_SCALE,
 CHAR_USED,
 HIDDEN_COLUMN,
 VIRTUAL_COLUMN,
 NULLABLE,
 NULL COMMENTS,
 C.IDENTITY_COLUMN -- 12.1
FROM
 DBA_TAB_COLS C
WHERE 1=1 AND HIDDEN_COLUMN='NO'  AND UPPER(C.OWNER) = 'ORA_MTK' AND (UPPER(C.TABLE_NAME) = 'T3' OR UPPER(C.TABLE_NAME) = 'T2') ORDER BY c.OWNER,c.TABLE_NAME,c.COLUMN_ID
```

### Column Comment

```sql
SELECT c.owner, c.table_name, c.column_name, comments FROM DBA_COL_COMMENTS c WHERE c.comments IS NOT null
```

### Column Identity

12C+

```sql
SELECT
 C.OWNER,
 C.TABLE_NAME,
 C.COLUMN_NAME,
 C.SEQUENCE_NAME,
 C.GENERATION_TYPE,
 S.MIN_VALUE,
 S.MAX_VALUE,
 S.INCREMENT_BY,
 S.ORDER_FLAG,
 S.CYCLE_FLAG,
 S.CACHE_SIZE,
 S.LAST_NUMBER,
 S.LAST_NUMBER + S.CACHE_SIZE AS STARTVAL
FROM
 DBA_TAB_IDENTITY_COLS C,
 DBA_SEQUENCES S
WHERE
 S.SEQUENCE_OWNER = C.OWNER
 AND S.SEQUENCE_NAME = C.SEQUENCE_NAME
```

### CONSTRAINT

#### Constraint Basic Info

```sql
SELECT  C.OWNER,
 C.CONSTRAINT_NAME,
 C.CONSTRAINT_TYPE,
 C.TABLE_NAME,
 C.SEARCH_CONDITION,
 C.R_OWNER,
 C.R_CONSTRAINT_NAME,
 C.DELETE_RULE,
 C.STATUS,
 C.DEFERRABLE,
 C.DEFERRED,
 C.VALIDATED,
 C.INDEX_OWNER,
 C.INDEX_NAME
FROM
 DBA_CONSTRAINTS C
WHERE
 1 = 1 AND UPPER(C.OWNER) = 'ORA_MTK' AND (UPPER(C.TABLE_NAME) = 'T3' OR UPPER(C.TABLE_NAME) = 'T2')
UNION ALL
 SELECT
 R.OWNER,
 R.CONSTRAINT_NAME,
 R.CONSTRAINT_TYPE,
 R.TABLE_NAME,
 NULL AS SEARCH_CONDITION,
 R.R_OWNER,
 R.R_CONSTRAINT_NAME,
 R.DELETE_RULE,
 R.STATUS,
 R.DEFERRABLE,
 R.DEFERRED,
 R.VALIDATED,
 R.INDEX_OWNER,
 R.INDEX_NAME
FROM
 DBA_CONSTRAINTS C
 LEFT JOIN DBA_CONSTRAINTS R ON C.R_OWNER = R.OWNER AND C.R_CONSTRAINT_NAME = R.CONSTRAINT_NAME
WHERE
 1 = 1
 AND C.CONSTRAINT_TYPE='R'
 AND UPPER(C.OWNER) = 'ORA_MTK' AND (UPPER(C.TABLE_NAME) = 'T3' OR UPPER(C.TABLE_NAME) = 'T2')
```

#### Constraint Column

```sql
SELECT
 COLS.OWNER,
 COLS.CONSTRAINT_NAME,
 COLS.TABLE_NAME,
 COLS.COLUMN_NAME,
 COLS.POSITION ,
 TC.NULLABLE
FROM
 DBA_CONS_COLUMNS COLS ,
 DBA_TAB_COLS TC
WHERE
 COLS.TABLE_NAME = TC.TABLE_NAME
 AND COLS.OWNER = TC.OWNER
 AND COLS.COLUMN_NAME = TC.COLUMN_NAME
 AND UPPER(COLS.OWNER) = 'ORA_MTK' AND (UPPER(COLS.TABLE_NAME) = 'T3' OR UPPER(COLS.TABLE_NAME) = 'T2')
UNION
SELECT DISTINCT COLS.OWNER,
 COLS.CONSTRAINT_NAME,
 COLS.TABLE_NAME,
 COLS.COLUMN_NAME,
 COLS.POSITION ,
 TC.NULLABLE
FROM
 DBA_CONSTRAINTS CONS,
 DBA_CONSTRAINTS CONS_R,
 DBA_CONS_COLUMNS COLS,
 DBA_TAB_COLS TC
WHERE
 1 = 1
 AND CONS.R_OWNER = CONS_R.OWNER
 AND CONS.R_CONSTRAINT_NAME = CONS_R.CONSTRAINT_NAME
 AND CONS_R.TABLE_NAME = COLS.TABLE_NAME
 AND CONS_R.CONSTRAINT_NAME = COLS.CONSTRAINT_NAME
 AND CONS_R.OWNER = COLS.OWNER
 AND CONS_R.TABLE_NAME = TC.TABLE_NAME
 AND CONS_R.OWNER = TC.OWNER
 AND COLS.OWNER = TC.OWNER
 AND COLS.TABLE_NAME = TC.TABLE_NAME
 AND COLS.COLUMN_NAME = TC.COLUMN_NAME
 AND CONS.CONSTRAINT_TYPE ='R'
 AND UPPER(COLS.OWNER) = 'ORA_MTK' AND (UPPER(COLS.TABLE_NAME) = 'T3' OR UPPER(COLS.TABLE_NAME) = 'T2')
 ORDER BY OWNER,TABLE_NAME,CONSTRAINT_NAME,POSITION
```

#### Constraint Index

```sql
SELECT
 S.INDEX_NAME,
 S.TABLE_NAME,
 S.UNIQUENESS,
 S.INDEX_TYPE,
 S.TABLE_OWNER,
 S.PARTITIONED,
 S.TABLESPACE_NAME,
 Q.QUEUE_TABLE,
 S.JOIN_INDEX
FROM
 DBA_INDEXES S
LEFT OUTER JOIN DBA_QUEUE_TABLES Q ON
 Q.QUEUE_TABLE = S.TABLE_NAME
 AND Q.OWNER = S.TABLE_OWNER
WHERE EXISTS (
 SELECT
  AC.INDEX_NAME
 FROM
  DBA_CONSTRAINTS AC
 WHERE
   AC.CONSTRAINT_TYPE IN ('P', 'U')
  AND AC.TABLE_NAME = S.TABLE_NAME
  AND AC.OWNER = S.TABLE_OWNER
  AND AC.INDEX_NAME = S.INDEX_NAME
 )
 AND S.TEMPORARY = 'N'
 AND (FUNCIDX_STATUS IS NULL
 OR FUNCIDX_STATUS != 'DISABLED')
 AND UPPER(S.TABLE_OWNER) = 'ORA_MTK' AND UPPER(S.TABLE_NAME) = 'PROMOTIONS_VAR3'
```

#### Constraint Index Column

```sql
SELECT IC.INDEX_OWNER,IC.INDEX_NAME,IC.COLUMN_NAME,IE.COLUMN_EXPRESSION,IC.COLUMN_POSITION,IC.TABLE_NAME,IC.TABLE_OWNER,IC.DESCEND FROM DBA_IND_COLUMNS IC, DBA_IND_EXPRESSIONS IE WHERE IC.INDEX_OWNER = IE.INDEX_OWNER(+) AND IC.INDEX_NAME = IE.INDEX_NAME(+) AND IC.COLUMN_POSITION=IE.COLUMN_POSITION(+)
AND UPPER(IC.TABLE_OWNER) = 'ORA_MTK' AND UPPER(IC.TABLE_NAME) = 'PROMOTIONS_VAR3'
ORDER BY IC.TABLE_OWNER,IC.TABLE_NAME,IC.INDEX_NAME,IC.COLUMN_POSITION
```

### Index Column

#### Index Basic Info

```sql
SELECT
 S.INDEX_NAME,
 S.TABLE_NAME,
 S.UNIQUENESS,
 S.INDEX_TYPE,
 S.TABLE_OWNER,
 S.PARTITIONED,
 S.TABLESPACE_NAME,
 Q.QUEUE_TABLE,
 S.JOIN_INDEX
FROM
 DBA_INDEXES S
LEFT OUTER JOIN DBA_QUEUE_TABLES Q ON
 Q.QUEUE_TABLE = S.TABLE_NAME
 AND Q.OWNER = S.TABLE_OWNER
WHERE NOT EXISTS (
 SELECT
  AC.INDEX_NAME
 FROM
  DBA_CONSTRAINTS AC
 WHERE
   AC.CONSTRAINT_TYPE IN ('P', 'U')
  AND AC.TABLE_NAME = S.TABLE_NAME
  AND AC.OWNER = S.TABLE_OWNER
  AND AC.INDEX_NAME = S.INDEX_NAME
 )
 AND S.TEMPORARY = 'N'
 AND (FUNCIDX_STATUS IS NULL
 OR FUNCIDX_STATUS != 'DISABLED')
 AND UPPER(S.TABLE_OWNER) = 'ORA_MTK' AND UPPER(S.TABLE_NAME) = 'PROMOTIONS_VAR3'
```

#### Index Column

```sql
SELECT IC.INDEX_OWNER,IC.INDEX_NAME,IC.COLUMN_NAME,IE.COLUMN_EXPRESSION,IC.COLUMN_POSITION,IC.TABLE_NAME,IC.TABLE_OWNER,IC.DESCEND FROM DBA_IND_COLUMNS IC, DBA_IND_EXPRESSIONS IE WHERE IC.INDEX_OWNER = IE.INDEX_OWNER(+) AND IC.INDEX_NAME = IE.INDEX_NAME(+) AND IC.COLUMN_POSITION=IE.COLUMN_POSITION(+)
AND UPPER(IC.TABLE_OWNER) = 'ORA_MTK' AND UPPER(IC.TABLE_NAME) = 'PROMOTIONS_VAR3'
ORDER BY IC.TABLE_OWNER,IC.TABLE_NAME,IC.INDEX_NAME,IC.COLUMN_POSITION
```

### DBLink

```sql
SELECT OWNER, DB_LINK, USERNAME,HOST FROM DBA_DB_LINKS WHERE 1=1
```

### View

```sql
SELECT V.OWNER,V.VIEW_NAME,V.TEXT,V.TEXT_VC ,O.STATUS
FROM DBA_VIEWS V,DBA_OBJECTS O
WHERE V.OWNER = O.OWNER AND V.VIEW_NAME = O.OBJECT_NAME
```

### Materialized View

```sql
SELECT
 MV.OWNER,
 MV.MVIEW_NAME,
 MV.BUILD_MODE,
 MV.REFRESH_METHOD,
 MV.REFRESH_MODE,
 MV.REWRITE_ENABLED,
 MV.QUERY,
 O.STATUS,
 R.INTERVAL
FROM
 DBA_MVIEWS MV,
 DBA_OBJECTS O,
 DBA_REFRESH R
WHERE
 O.OBJECT_TYPE = 'MATERIALIZED VIEW'
 AND MV.OWNER = O.OWNER
 AND MV.MVIEW_NAME = O.OBJECT_NAME
 AND O.STATUS = 'VALID'
 AND MV.OWNER = R.ROWNER(+)
 AND MV.MVIEW_NAME = R.RNAME(+) AND UPPER(MV.OWNER) = 'ORA_MTK' AND (UPPER(MV.MVIEW_NAME) = 'T3' OR UPPER(MV.MVIEW_NAME) = 'T2')
```

### Function

```sql
SELECT
 S.NAME,
 S.LINE,
 S.TEXT
FROM
 DBA_SOURCE S,
 DBA_OBJECTS O
WHERE
 1=1
 AND S.TYPE = 'FUNCTION'
 AND S.OWNER = O.OWNER
 AND S.TYPE = O.OBJECT_TYPE
 AND S.NAME = O.OBJECT_NAME
```

### Procedure

```sql
SELECT
 S.NAME,
 S.LINE,
 S.TEXT
FROM
 DBA_SOURCE S,
 DBA_OBJECTS O
WHERE
 1=1
 AND S.TYPE = 'PROCEDURE'
 AND S.OWNER = O.OWNER
 AND S.TYPE = O.OBJECT_TYPE
 AND S.NAME = O.OBJECT_NAME
```

### Package

```sql
SELECT
 S.NAME,
 S.TYPE,
 S.LINE,
 S.TEXT
FROM
 DBA_SOURCE S,
 DBA_OBJECTS O
WHERE
 1=1
 AND (S.TYPE = 'PACKAGE' OR S.TYPE = 'PACKAGE BODY')
 AND S.OWNER = O.OWNER
 AND S.TYPE = O.OBJECT_TYPE
 AND S.NAME = O.OBJECT_NAME
```

### Trigger

```sql
SELECT
 T.TRIGGER_NAME,
 T.OWNER,
 T.TABLE_OWNER,
 T.TRIGGER_TYPE,
 T.TRIGGERING_EVENT,
 T.TABLE_NAME,
 T.TRIGGER_BODY,
 T.WHEN_CLAUSE,
 T.DESCRIPTION,
 T.STATUS,
 T.BASE_OBJECT_TYPE,
 T.ACTION_TYPE
FROM
 DBA_TRIGGERS T,
 DBA_OBJECTS O
WHERE
 O.OBJECT_NAME = T.TRIGGER_NAME
 AND O.OWNER = T.TABLE_OWNER
 -- AND o.status = 'VALID'
 AND O.OBJECT_TYPE = 'TRIGGER'
```

### Synonym

```sql
SELECT OWNER,SYNONYM_NAME,TABLE_OWNER,TABLE_NAME FROM DBA_SYNONYMS WHERE DB_LINK IS NULL
```
