---
title: FAQs
summary: FAQs
author: Bin.Liu
date: 2021-09-14
---


# FAQs

## 支持字符集转换?

默认支持字符集转换。如源库GBK迁移到目标库UTF8.

在进行字符串转换时字段长度会进行变化.

Char类型数据需要注意. 如在Oracle里Char(1)迁移到openGauss/MogDB就会变成Char(2),业务查询会多个空格

## 存在字段长度变化?

会. 考虑到中文等其他非单字节字符串在数据库编码不一致下进行长度变化. 存在以下场景.

- 源库列类型是字符迁移到字节型. 字段长度会变成原来的2倍.

  - MySQL 数据库 迁移到 openGauss/MogDB 非PG/B兼容模式 会变成原来的3倍. 如varchar(10)变为varchar(30)

- 存在字符编码转换. 从GBK的数据库迁移到UTF-8的数据库长度会增加50%,向上取整.

|Source DB|Source Charset|Source Type|Target Type|Target DB| Target Charset|Target datCompatibility|
|---------|--------------|-----------|-----------|---------|---------------|-----------------------|
|Oracle   |ZHS16GBK  |VARCHAR2(100 BYTE)|VARCHAR(150)|openGauss|UTF8|A|
|Oracle   |ZHS16GBK  |VARCHAR2(100 CHAR)|VARCHAR(300)|openGauss|UTF8|A|
|Oracle   |ZHS16GBK  |VARCHAR2(100 BYTE)|VARCHAR(100)|openGauss|UTF8|PG/B|
|Oracle   |ZHS16GBK  |VARCHAR2(100 CHAR)|VARCHAR(100)|openGauss|UTF8|PG/B|
|Oracle   |ZHS16GBK  |VARCHAR2(100 BYTE)|VARCHAR(100)|openGauss|GBK |A|
|Oracle   |ZHS16GBK  |VARCHAR2(100 CHAR)|VARCHAR(200)|openGauss|GBK |A|
|Oracle   |ZHS16GBK  |VARCHAR2(100 BYTE)|VARCHAR(100)|openGauss|GBK |PG/B|
|Oracle   |ZHS16GBK  |VARCHAR2(100 CHAR)|VARCHAR(100)|openGauss|GBK |PG/B|
|Oracle   |AL32UTF8  |VARCHAR2(100 BYTE)|VARCHAR(100)|openGauss|UTF8|A|
|Oracle   |AL32UTF8  |VARCHAR2(100 CHAR)|VARCHAR(300)|openGauss|UTF8|A|
|Oracle   |AL32UTF8  |VARCHAR2(100 BYTE)|VARCHAR(100)|openGauss|UTF8|PG/B|
|Oracle   |AL32UTF8  |VARCHAR2(100 CHAR)|VARCHAR(100)|openGauss|UTF8|PG/B|
|oracle   |ZHS16GBK  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|A|
|oracle   |ZHS16GBK  |CHAR(2 )          |CHAR(3)|openGauss|UTF8|A|
|oracle   |ZHS16GBK  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|PG/B|
|oracle   |ZHS16GBK  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|PG/B|
|oracle   |AL32UTF8  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|A|
|oracle   |AL32UTF8  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|A|
|oracle   |AL32UTF8  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|PG/B|
|oracle   |AL32UTF8  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|PG/B|
|MySQL    |GBK       |VARCHAR(100 )     |VARCHAR(300)|openGauss|UTF8|A|
|MySQL    |GBK       |VARCHAR(100 )     |VARCHAR(300)|openGauss|GBK|A|
|MySQL    |UTF8      |VARCHAR(100 )     |VARCHAR(300)|openGauss|UTF8|A|
|MySQL    |GBK       |VARCHAR(100 )     |VARCHAR(100)|openGauss|UTF8|PG/B|
|MySQL    |GBK       |VARCHAR(100 )     |VARCHAR(100)|openGauss|GBK|PG/B|
|MySQL    |UTF8      |VARCHAR(100 )     |VARCHAR(100)|openGauss|UTF8|PG/B|
|DB2      |GBK       |VARCHAR(100 )     |VARCHAR(150)|openGauss|UTF8|A|
|DB2      |GBK       |VARCHAR(100 )     |VARCHAR(100)|openGauss|GBK|A|
|DB2      |UTF8      |VARCHAR(100 )     |VARCHAR(100)|openGauss|UTF8|A|
|DB2      |GBK       |VARCHAR(100 )     |VARCHAR(100)|openGauss|UTF8|PG/B|
|DB2      |GBK       |VARCHAR(100 )     |VARCHAR(100)|openGauss|GBK|PG/B|
|DB2      |UTF8      |VARCHAR(100 )     |VARCHAR(100)|openGauss|UTF8|PG/B|
|DB2      |ZHS16GBK  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|A|
|DB2      |ZHS16GBK  |CHAR(2 )          |CHAR(3)|openGauss|UTF8|A|
|DB2      |ZHS16GBK  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|PG/B|
|DB2      |ZHS16GBK  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|PG/B|
|DB2      |AL32UTF8  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|A|
|DB2      |AL32UTF8  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|A|
|DB2      |AL32UTF8  |CHAR(1 )          |CHAR(1)|openGauss|UTF8|PG/B|
|DB2      |AL32UTF8  |CHAR(2 )          |CHAR(2)|openGauss|UTF8|PG/B|

## 存在空字符串和NULL转换？

- Oracle `''` = `NULL`
- DB2  `''` != `NULL`
- MySQL  `''` != `NULL`
- PostgreSQL `''` != `NULL`
- openGauss
  - A 兼容模式下 `''` = `NULL`
  - PG 兼容模式下 `''` != `NULL`
  - B 兼容模式下 `''` != `NULL`
- Informix `''` != `NULL`

当目标数据库的处理方式和源数据库处理方式不一致时,MTK会根据列的非空属性进行数据转换.

如列存在非空约束`NOT NULL`,并且列数据库存在`''`,会把`''`时转为`" "`,允许为`NULL`时转为`NULL`

## 存在 Chr(0) 0x00 ?

在DB2/Oracle/MySQL中可以插入Chr(0)/0x00特殊字符.

但是在postgres类数据库中无法进行插入,迁移过程中Chr(0)会被替换成''.

## 存在时区转换 ?

- 迁移机器和应用服务器时区一致
- 数据库主机时区一致

## 迁移过程慢日志暴增 ?

在迁移到MySQL时,MySQL慢日志暴增. 迁移到MySQL使用Insert多条记录,可能会导致插入时间大于`long_query_time`值并记录到日志文件. 建议在迁移时关掉满日志记录或者把`long_query_time`调大.

## Oracle 的 Package 迁移处理方案 ?

参考 [convertPackageMethod](./../config/mtk-parameter.md#convertpackagemethod)

## Not define migrate object ?

没有配置迁移对象. 请查看[Objects](./../config/mtk-config.md#object)

## 性能

MTK进行并发任务进行迁移数据,并发数由`limit.parallel`进行控制. 并发颗粒度为

- 表(没有分区/没有自定义查询条件)
- 分区表单个分区/子分区
- 自定义查询条件(一个查询条件为一个并发任务)

并发总线程数计算方式为: `limit.parallel(worker工作者)+limit.parallel(查询线程)+limit.parallel*parallelInsert(写入线程)`. 在配置并发度的时候要根据运行MTK机器的CPU进行配置,配置为CPU总数的50%-70%之间.

- 为啥存在自定义查询条件?

单个查询性能的会有一定性能的限制,这个限制受限于数据库所在机器的配置和MTK所在机器的性能.这个限制可能在几M或者几十M,通过拆分为多个查询来提升整体迁移数据.

- 什么时候需要配置自定义查询条件?

  - 表大并且不是分区表
  - 表大是分区表但是数据分布不均匀,大量数据都在一个分区里.

- 怎么自定义查询条件?

    参考[tablesplit](../config/mtk-object.md#tablesplit)
