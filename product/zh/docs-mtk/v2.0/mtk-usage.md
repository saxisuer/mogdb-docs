---
title: 数据迁移工具 MTK 使用
summary: 数据迁移工具 MTK 使用
author: Liu Xu
date: 2021-03-04
---

# MTK使用

<div class="asciinema-player" data-cast="mtk"></div>

## 查看版本

```bash
./mtk version
```

## 申请License

```bash
./mtk license gen
```

## 迁移Oracle到MogDB

迁移Oracle到MogDB常见问题参考[Oracle To MogDB](./faq/mtk-oracle-to-openGauss.md)

### 安装[Oracle](./mtk-env.md#oracle)客户端

### 初始化项目

```bash
## 迁移Oracle到MogDB
./mtk init-project -s oracle -t mogdb -n ora2mg
# will build a migration directory
ora2mg
├── config
│   └── mtk.json
├── data
├── report
└── schema
```

### 编辑配置文件

具体查看[MTK配置说明](./config/mtk-config.md)章

```bash
vi ora2mg/config/mtk.json
```

1. 编辑`source`节点,定义源数据库连接信息. 配置参考[source](config/mtk-config.md#source)
2. 编辑`target`节点,定义目标数据库连接信息. 配置参考[target](config/mtk-config.md#target)

    > 修改connect和type信息,parameter根据运行在进行调整

3. 编辑`object`节点,定义迁移对象. 配置参考[object](config/mtk-object.md#object)

编辑完成后,运行[config-check](commands/mtk_config-check.md)检查配置文件是否正确

```shell
./mtk config-check -c my2mg/config/mtk.json
```

配置文件配置正常会输出类型信息

```shell
use config : ora2mg/config/mtk.json
There is no error in the configuration file
```

### 运行

```bash
./mtk -c ora2mg/config/mtk.json
# or debug
./mtk -c ora2mg/config/mtk.json --debug
# 只迁移Schema
./mtk -c ora2mg/config/mtk.json --schemaOnly
# 只迁移Data
./mtk -c ora2mg/config/mtk.json --dataOnly
```

### 查看报告

报告和运行日志会生成在`ora2og/report`目录下. 如下

```bash
[2022-01-20 11:29:25.800978]  INFO reportDir: ora2mg/report/report_20220120110125 function=GenReport line=412 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.426822]  INFO the text report : ora2mg/report/report_20220120110125.txt function=PrintReport line=270 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.429545]  INFO the warring report : ora2mg/report/report_20220120110125.warring function=PrintReport line=281 file=mtk/cmd/mtk/services/cmd.go
[2022-01-20 11:29:26.430118]  INFO the error report : ora2mg/report/report_20220120110125.err file=mtk/cmd/mtk/services/cmd.go function=PrintReport line=292
```

| 文件名                                      | 说明                     |
|---------------------------------------------|--------------------------|
| ora2mg/report/report_20220120110125         | html报告                 |
| ora2mg/report/report_20220120110125.txt     | 文本报告                 |
| ora2mg/report/report_20220120110125.warring | 只包含警告信息的文本报告 |
| ora2mg/report/report_20220120110125.err     | 只包含错误信息的文本报告 |

### 导出成文件

```bash
./mtk -c ora2mg/config/mtk.json --file
# 只迁移Schema
./mtk -c ora2mg/config/mtk.json --file --schemaOnly
# 只迁移Data
./mtk -c ora2mg/config/mtk.json --file --dataOnly
```

### 命令行指定迁移对象

```bash
# 指定表
./mtk -c ora2mg/config/mtk.json --tables schema1.table1,schema2.table2
# 指定Schema
./mtk -c ora2mg/config/mtk.json --schemas schema1,schema2
```

## MySQL to MogDB

迁移MySQL到MogDB常见问题参考[MySQL To MogDB](./faq/mtk-mysql-to-openGauss.md)

## 初始化项目

```shell
./mtk init-project -s mysql -t mogdb -n my2mg
```

## 编辑配置文件

```shell
vi my2mg/config/mtk.json
```

1. 编辑`source`节点,定义源数据库连接信息. 配置参考[source](config/mtk-config.md#source)
2. 编辑`target`节点,定义目标数据库连接信息. 配置参考[target](config/mtk-config.md#target)

    > 修改connect和type信息,parameter根据运行在进行调整

3. 编辑`object`节点,定义迁移对象. 配置参考[object](config/mtk-object.md#object)

编辑完成后,运行[config-check](commands/mtk_config-check.md)检查配置文件是否正确

```shell
./mtk config-check -c my2mg/config/mtk.json
```

配置文件配置正常会输出类型信息

```shell
use config : my2mg/config/mtk.json
There is no error in the configuration file
```

## 运行

```bash
./mtk -c my2mg/config/mtk.json
```

## 迁移 DB2 到 MogDB

迁移DB2到MogDB常见问题参考[DB2 To MogDB](./faq/mtk-db2-to-openGauss.md)

### 安装[db2](./mtk-env.md#db2)客户端

### 初始化项目

```shell
./mtk init-project -s db2 -t mogdb -n db22mg
```

### 编辑配置文件

```shell
vi db22mg/config/mtk.json
```

1. 编辑`source`节点,定义源数据库连接信息. 配置参考[source](config/mtk-config.md#source)
2. 编辑`target`节点,定义目标数据库连接信息. 配置参考[target](config/mtk-config.md#target)

    > 修改connect和type信息,parameter根据运行在进行调整

3. 编辑`object`节点,定义迁移对象. 配置参考[object](config/mtk-object.md#object)

编辑完成后,运行[config-check](commands/mtk_config-check.md)检查配置文件是否正确

```shell
./mtk config-check -c db22mg/config/mtk.json
```

配置文件配置正常会输出类型信息

```shell
use config : db22mg/config/mtk.json
There is no error in the configuration file
```

### 运行

```bash
./mtk -c db22mg/config/mtk.json
```

## 迁移 Informix to MogDB

迁移Informix到MogDB常见问题参考[Informix To MogDB](./faq/mtk-informix-to-openGauss.md)

### 安装[db2](./mtk-env.md#db2)客户端

### 初始化项目

```shell
./mtk init-project -s informix -t mogdb -n ifx2mg
```

### 编辑配置文件

```shell
vi ifx2mg/config/mtk.json
```

1. 编辑`source`节点,定义源数据库连接信息. 配置参考[source](config/mtk-config.md#source)
2. 编辑`target`节点,定义目标数据库连接信息. 配置参考[target](config/mtk-config.md#target)

    > 修改connect和type信息,parameter根据运行在进行调整

3. 编辑`object`节点,定义迁移对象. 配置参考[object](config/mtk-object.md#object)

编辑完成后,运行[config-check](commands/mtk_config-check.md)

```shell
./mtk config-check -c ifx2mg/config/mtk.json
```

配置文件配置正常会输出类型信息

```shell
use config : ifx2mg/config/mtk.json
There is no error in the configuration file
```

### 运行

```bash
./mtk -c ifx2mg/config/mtk.json
```

## 迁移方式

MTK 支持三种迁移方式.

### 一次性全部迁移

```bash
./mtk -c mtk.json
```

### 组合迁移

| 步骤                            | 命令                                           |
|---------------------------------|------------------------------------------------|
| ./mtk mig-tab-pre -c mtk.json   | [mig-tab-pre](commands/mtk_mig-tab-pre.md)     |
| ./mtk mig-tab-data -c mtk.json  | [mig-tab-data](commands/mtk_mig-tab-data.md)   |
| ./mtk mig-tab-post -c mtk.json  | [mig-tab-post](commands/mtk_mig-tab-post.md)   |
| ./mtk mig-tab-other -c mtk.json | [mig-tab-other](commands/mtk_mig-tab-other.md) |

### 分步迁移

用户自行控制迁移整体逻辑.

| 步骤                                       | 命令                                                                     | 描述                       |
|--------------------------------------------|--------------------------------------------------------------------------|----------------------------|
| ./mtk sync-schema -c mtk.json              | [mtk sync-schema](commands/mtk_sync-schema.md)                           | 迁移模式                   |
| ./mtk sync-sequence -c mtk.json            | [mtk sync-sequence](commands/mtk_sync-sequence.md)                       | 迁移序列                   |
| ./mtk sync-object-type -c mtk.json         | [mtk sync-object-type](commands/mtk_sync-object-type.md)                 | 迁移对象类型               |
| ./mtk sync-domain -c mtk.json              | [mtk sync-domain](commands/mtk_sync-domain.md)                           | 迁移域                     |
| ./mtk sync-wrapper -c mtk.json             | [mtk sync-wrapper](commands/mtk_sync-wrapper.md)                         | 迁移DB2 Wrapper            |
| ./mtk sync-server -c mtk.json              | [mtk sync-server](commands/mtk_sync-server.md)                           | 迁移DB2 Server             |
| ./mtk sync-user-mapping -c mtk.json        | [mtk sync-user-mapping](commands/mtk_sync-user-mapping.md)               | 迁移DB2 User Mapping       |
| ./mtk sync-queue -c mtk.json               | [mtk sync-queue](commands/mtk_sync-queue.md)                             | 迁移队列                   |
| ./mtk sync-table -c mtk.json               | [mtk sync-table](commands/mtk_sync-table.md)                             | 迁移表                     |
| ./mtk sync-nickname -c mtk.json            | [mtk sync-nickname](commands/mtk_sync-nickname.md)                       | 迁移DB2 Nickname           |
| ./mtk sync-rule -c mtk.json                | [mtk sync-rule](commands/mtk_sync-rule.md)                               | 迁移规则                   |
| ./mtk sync-table-data -c mtk.json          | [mtk sync-table-data](commands/mtk_sync-table-data.md)                   | 迁移表数据                 |
| ./mtk sync-table-data-estimate -c mtk.json | [mtk sync-table-data-estimate](commands/mtk_sync-table-data-estimate.md) | 预估表数据迁移时间         |
| ./mtk sync-index -c mtk.json               | [mtk sync-index](commands/mtk_sync-index.md)                             | 迁移索引                   |
| ./mtk sync-constraint -c mtk.json          | [mtk sync-constraint](commands/mtk_sync-constraint.md)                   | 迁移约束                   |
| ./mtk sync-db-link -c mtk.json             | [mtk sync-db-link](commands/mtk_sync-db-link.md)                         | 迁移数据库链接             |
| ./mtk sync-view -c mtk.json                | [mtk sync-view](commands/mtk_sync-view.md)                               | 迁移视图                   |
| ./mtk sync-mview -c mtk.json               | [mtk sync-mview](commands/mtk_sync-mview.md)                             | 迁移物化视图               |
| ./mtk sync-function -c mtk.json            | [mtk sync-function](commands/mtk_sync-function.md)                       | 迁移函数                   |
| ./mtk sync-procedure -c mtk.json           | [mtk sync-procedure](commands/mtk_sync-procedure.md)                     | 迁移过程                   |
| ./mtk sync-package -c mtk.json             | [mtk sync-package](commands/mtk_sync-package.md)                         | 迁移包                     |
| ./mtk sync-trigger -c mtk.json             | [mtk sync-trigger](commands/mtk_sync-trigger.md)                         | 迁移触发器                 |
| ./mtk sync-synonym -c mtk.json             | [mtk sync-synonym](commands/mtk_sync-synonym.md)                         | 迁移同义词                 |
| ./mtk sync-table-data-com -c mtk.json      | [mtk sync-table-data-com](commands/mtk_sync-table-data-com.md)           | 表行计数比较               |
| ./mtk sync-alter-sequence -c mtk.json      | [mtk sync-alter-sequence](commands/mtk_sync-alter-sequence.md)           | 修改序列起始值             |
| ./mtk sync-coll-statistics -c mtk.json     | [mtk sync-coll-statistics](commands/mtk_sync-coll-statistics.md)         | 收集表统计信息             |
| ./mtk check-table-data -c mtk.json         | [mtk check-table-data](commands/mtk_check-table-data.md)                 | 检查表数据是否存在异常数据 |

其他使用方式见[命令](commands/mtk.md)
