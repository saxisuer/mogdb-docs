---
title: MTK Parameter
summary: MTK Parameter
author: mtk
date: 2022-11-28 09:22:56
---

# MTK Parameter

## Parameter

Parameter 参数配置

| Field | Type | Group |Description |
|-------|------|-------|-------------|
| [enableTableParallelQuery](#enabletableparallelquery) |int | |查询表信息的方式. |
| [parallelInsert](#parallelinsert) |int | |并行插入并行度. |
| [dropSchema](#dropschema) |bool |object |删除存在的schema. |
| [dropExistingObject](#dropexistingobject) |bool |object |若对象已存在于目标数据库中，则删除此对象. schema/user/database除外 |
| [truncTable](#trunctable) |bool |object |当只迁移数据时，防止数据冲突使用. |
| [colKeyWords](#colkeywords) |map[string]int |object |列名关键字。 |
| [objKeyWords](#objkeywords) |map[string]int |object |预留对象关键字。 |
| [caseSensitive](#casesensitive) |int |object |针对生成的对象名进行大小写转换. |
| [quoteMark](#quotemark) |bool |object |针对生成的对象名是否进行双引号或者反引号包含. |
| [ignoreTableDDLCompErr](#ignoretableddlcomperr) |bool |object |是否忽略表结构对比错误 |
| [parallelIndex](#parallelindex) |int |object |针对openGauss/MogDB启用并行创建索引功能, 单个索引并行度最大为32. |
| [path](#path) |string |file |数据导出为文件时，指定导出目录. |
| [schemaPath](#schemapath) |string |file |sql脚本目录 |
| [dataPath](#datapath) |string |file |data数据目录 |
| [errDataPath](#errdatapath) |string |file |错误数据路径 |
| [fileType](#filetype) |string |file |数据导出为文件时，文件类型 |
| [fileSize](#filesize) |string |file |数据导出为文件时，单个文件大小。不指定则不限制. |
| [sqlDBType](#sqldbtype) |string |file |数据导出为文件时，指定生成那种数据库的语法. |
| [csvHeader](#csvheader) |bool |file |导出文件为csv时，指定是否包含csv文件头。默认不导出。 |
| [csvNullValue](#csvnullvalue) |string |file |csv空值展示 |
| [csvFieldDelimiter](#csvfielddelimiter) |string |file |csv默认间隔符。 |
| [csvOptionallyEnclosed](#csvoptionallyenclosed) |string |file |数据包裹符。 |
| [timeFormat](#timeformat) |string |file |定义时间格式 |
| [dateFormat](#dateformat) |string |file |定义日期格式 |
| [dateTimeFormat](#datetimeformat) |string |file |定义时间完整格式 |
| [excludeSysTable](#excludesystable) |[]string |table |排除系统对象定义。不配置则使用默认值 |
| [enableSyncCompTabPro](#enablesynccomptabpro) |bool |table |是否配置生成DDL包含表压缩属性语法。 默认不生成。 |
| [tableOptions](#tableoptions) |map[string]string |table |配置在创建表的时候添加表属性选项.暂时只支持openGauss/MogDB. |
| [indexOptions](#indexoptions) |map[string]string |table |和tableOptions选项配置一样,只针对索引生效. 未完成功能开发. |
| [remapSchema](#remapschema) |map[string]string |remap |迁移过程中是否对数据库schema进行改名操作。 |
| [remapTable](#remaptable) |map[string]string |remap |迁移过程中是否对表名进行改名操作 |
| [remapTablespace](#remaptablespace) |map[string]string |remap |迁移过程中是否对表空间名进行改名操作。 |
| [enableSyncTabTbsPro](#enablesynctabtbspro) |bool |remap |是否配置生成DDL包含表空间语法。 默认不生成 |
| [noSupportPartTabToNormalTab](#nosupportparttabtonormaltab) |bool |partition |把目标端不支持的分区表转换普通表 |
| [ignoreDB2PartInclusive](#ignoredb2partinclusive) |bool |partition |是否忽略DB2分区键值ENDING的包含属性 |
| [igNotSupportIntervalPart](#ignotsupportintervalpart) |bool |partition |支持忽略部分不支持的`interval分`区属性 |
| [ignoreTabPartition](#ignoretabpartition) |bool |partition |支持迁移到目标端数据库忽略分区语法, 现在只支持迁移到MySQL. |
| [autoAddMaxvaluePart](#autoaddmaxvaluepart) |bool |partition |参数`autoAddMaxvaluePart`允许不存在`maxvalue`分区的分区表自动添加`maxvalue`分区. |
| [igErrorData](#igerrordata) |bool |data |忽略插入失败的数据并记录到错误文件. |
| [enableBatchCommit](#enablebatchcommit) |bool |data |启用分批次Commit |
| [mySQLSkipErrorDateTimeData](#mysqlskiperrordatetimedata) |bool |data |自动跳过mysql错误时间。 |
| [autoAddMySQLAutoIncr](#autoaddmysqlautoincr) |bool |column |迁移到MySQL自动添加MySQL `AUTO_INCREMENT` 列 |
| [autoAddMySQLAutoIncrTabList](#autoaddmysqlautoincrtablist) |[]string |column |配合`autoAddMySQLAutoIncr`参数使用. |
| [ignoreNotSupportDefault](#ignorenotsupportdefault) |bool |column |支持忽略部分不支持列默认值.如Oracle的`sys_guid`。MTK会内嵌白名单进行忽略 |
| [replaceZeroDate](#replacezerodate) |string |column |openGauss/MogDB/PostgreSQL 时间格式不支持 `0000-00-00`. 配置此参数会进行对应替换. 替换的范围如下: |
| [virtualColToNormalCol](#virtualcoltonormalcol) |bool |column |是否把源库虚拟列转成目标库的正常列。 |
| [virtualColConv](#virtualcolconv) |map[string]string |column |虚拟列表达转换功能 |
| [mySQLToOgCharExpression](#mysqltoogcharexpression) |string |column |mysql 迁移到 openGauss 针对 char/varchar列进行计算列长表达式. 默认不计算 如 `* 3` 表示原来到长度`*3` |
| [convertOracleIntegerToNumeric](#convertoracleintegertonumeric) |bool |column |转换Oracle的Integer类型为NUMERIC. 支持openGauss/MogDB/PostgreSQL |
| [enableOgBlobClob](#enableogblobclob) |bool |column |迁移其他数据库的Blob、Clob为openGauss的Blob、Clob. |
| [skipColumnType](#skipcolumntype) |map[string]int |column |迁移时跳过某个数据类型列 |
| [skipColumnName](#skipcolumnname) |map[string]int |column |迁移时跳过某个列 |
| [charLengthChangeExclude](#charlengthchangeexclude) |[]string |column |跨字符集迁移时,针对CHAR类型指定表达式,对列名进行匹配并不进行长度扩充. |
| [enableCharTrimRightSpace](#enablechartrimrightspace) |bool |column |针对char迁移为varchar允许截取`char`类型右侧空格. 只支持目标端为openGauss/MogDB |
| [convertPackageMethod](#convertpackagemethod) |string |plsql |迁移 Oracle Package 到 openGauss/MogDB 的方式 |
| [enableConvertSrid](#enableconvertsrid) |bool |gis |Enable Convert Srid |
| [defaultSrid](#defaultsrid) |string |gis |PostGis Default Srid |
| [seqLastNumAddNum](#seqlastnumaddnum) |int |sequence |同步序录最后值时增加多少. |
| [templateSeqName](#templateseqname) |string |sequence |MySQL 自增列转为序列,序列名的模版 |
| [charAppendEmptyString](#charappendemptystring) |bool |select |针对ORA-29275: partial multibyte character 错误. 在oracle查询是拼接字符串 |
| [charsetTranscode](#charsettranscode) |bool |charset |针对错误的编码进行转码. |

Appears in:

- [`Option`](./mtk-config.md#option).parameter

**示例**:

```json
{
  "parallelInsert": 1,
  "dropSchema": false,
  "dropExistingObject": false,
  "truncTable": false,
  "colKeyWords": {},
  "objKeyWords": {},
  "caseSensitive": 0,
  "quoteMark": false,
  "path": "./data",
  "schemaPath": "",
  "dataPath": "",
  "errDataPath": "",
  "fileType": "",
  "fileSize": "",
  "csvHeader": false,
  "csvNullValue": "",
  "csvFieldDelimiter": ",",
  "csvOptionallyEnclosed": "\"",
  "excludeSysTable": [],
  "remapSchema": {},
  "remapTable": {},
  "remapTablespace": {},
  "enableSyncTabTbsPro": false,
  "enableSyncCompTabPro": false,
  "timeFormat": "HH:MI:SS",
  "dateFormat": "YYYY-MM-DD",
  "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
  "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
  "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
  "noSupportPartTabToNormalTab": false,
  "ignoreDB2PartInclusive": false,
  "igNotSupportIntervalPart": false,
  "igErrorData": false,
  "enableBatchCommit": false,
  "ignoreTabPartition": false,
  "autoAddMaxvaluePart": false,
  "autoAddMySQLAutoIncr": false,
  "autoAddMySQLAutoIncrTabList": [],
  "ignoreNotSupportDefault": false,
  "replaceZeroDate": "",
  "virtualColToNormalCol": false,
  "virtualColConv": {},
  "convertPackageMethod": "",
  "convertOracleIntegerToNumeric": false,
  "enableOgBlobClob": false,
  "enableConvertSrid": false,
  "defaultSrid": "4326",
  "seqLastNumAddNum": 0,
  "skipColumnType": {},
  "skipColumnName": {},
  "templateSeqName": "",
  "charAppendEmptyString": false,
  "tableOptions": {},
  "indexOptions": {}
}
```

### enableTableParallelQuery

**类型**: int

**描述**: 查询表信息的方式.

测试特性. 无需修改

**可选值**:

- 0 一次性查询出来,然后并发创建
- 1 并行查询,先查询表的基本信息,然后并发查询+目标端创建

### parallelInsert

**类型**: int

**描述**: 并行插入并行度.

测试特性. 无需修改

**默认值**: `false`

### dropSchema

**类型**: bool

**描述**: 删除存在的schema.

**默认值**: `false`

**可选值**:

- true
- false

**新增于**: v2.7.0

### dropExistingObject

**类型**: bool

**描述**: 若对象已存在于目标数据库中，则删除此对象. schema/user/database除外

一般在数据重新初始化时使用. 请谨慎慎用.
2.7.0 之后不在支持删除schema

**默认值**: `false`

**可选值**:

- true
- false

### truncTable

**类型**: bool

**描述**: 当只迁移数据时，防止数据冲突使用.

**默认值**: `false`

**可选值**:

- true
- false

### colKeyWords

**类型**: map[string]int

**描述**: 列名关键字。

在目标端创建时会自动增加""或并将关键字进行大小写转换

**可选值**:

- 1 代表小写
- 2 代表大写
- 其他为保持不变

**示例**:

Column KeyWords example

```json
{
  "colKeyWords": {
    "STREAM": 1,
    "TID": 1
  }
}
```

### objKeyWords

**类型**: map[string]int

**描述**: 预留对象关键字。

在目标端创建时会自动增加""或并将关键字进行大小写转换

**可选值**:

- 1 代表小写
- 2 代表大写
- 其他为保持不变

**示例**:

Column KeyWords example

```json
{
  "objKeyWords": {
    "STREAM": 1,
    "TID": 1
  }
}
```

### caseSensitive

**类型**: int

**描述**: 针对生成的对象名进行大小写转换.

**默认值**: 0

**可选值**:

- 0 即不进行处理
- 1 代表小写
- 2 代表大写
- 其他为保持不变

### quoteMark

**类型**: bool

**描述**: 针对生成的对象名是否进行双引号或者反引号包含.

```sql
-- MySQl
`a1`
-- Oracle/PostgreSQL/openGauss/MogDB
"a1"
```

```sql
-- quoteMark: false
CREATE TABLE mtk.t_pri (
  id DECIMAL(38) NOT NULL,
  id2 DECIMAL(38) NOT NULL,
  id3 DECIMAL(38),
  id4 DECIMAL(38),
  id5 DECIMAL(38)
);

-- quoteMark: true
CREATE TABLE "mtk"."t_pri" (
  "id" DECIMAL(38) NOT NULL,
  "id2" DECIMAL(38) NOT NULL,
  "id3" DECIMAL(38),
  "id4" DECIMAL(38),
  "id5" DECIMAL(38)
);
```

可与参数[caseSensitive](#casesensitive)一起使用,对数据库对象名进行大小写转换.
特殊关键字除外,特殊关键字使用[objKeyWords](#objkeywords)和[colKeyWords](#colkeywords)

|quoteMark|caseSensitive|描述|
|---------|-------------|---------------------------------------|
|false|0|创建语句不带双引号或反引号,<br/>对象名大小写由数据库默认配置决定|
|false|1|创建语句不带双引号或反引号,<br/>对象名大小写由数据库默认配置决定|
|false|2|创建语句不带双引号或反引号,<br/>对象名大小写由数据库默认配置决定|
|true |0|创建语句带双引号或反引号,<br/>对象名和原来保持一致|
|true |1|创建语句带双引号或反引号,<br/>对象名全部转为小写|
|true |2|创建语句带双引号或反引号,<br/>对象名全部转为大写|

**默认值**: `false`

**可选值**:

- true
- false

### ignoreTableDDLCompErr

**类型**: bool

**描述**: 是否忽略表结构对比错误

**默认值**: `false`

**Deprecated**: 功能废弃

**可选值**:

- true
- false

### parallelIndex

**类型**: int

**描述**: 针对openGauss/MogDB启用并行创建索引功能, 单个索引并行度最大为32.
总并行度 `limit.parallel * parallelIndex` 要小于 `max_connections*1/4`. 如数据库最大连接数为400,数据库层面最大提供给创建索引度线程为 `400*1/4`,即`100`
如果`limit.parallel * parallelIndex`大于100个后数据库会自动转为串行创建.
即配置`parallelIndex=8` 则`limit.parallel`最大可为`12`

**默认值**: 0

**新增于**: v2.7.1

### path

**类型**: string

**描述**: 数据导出为文件时，指定导出目录.

在进行数据迁移时,遇到错误数据会记录到这个目录下到`err_data_<YYYYDDMMHH24MISS>`目录下

**默认值**: `./data`

### schemaPath

**类型**: string

**描述**: sql脚本目录

### dataPath

**类型**: string

**描述**: data数据目录

### errDataPath

**类型**: string

**描述**: 错误数据路径

### fileType

**类型**: string

**描述**: 数据导出为文件时，文件类型

**可选值**:

- csv
- sql

### fileSize

**类型**: string

**描述**: 数据导出为文件时，单个文件大小。不指定则不限制.

支持以下格式：

- KiB
- MiB
- GiB
- TiB
- PiB
- KB
- MB
- GB
- TB
- PB

KB == 1000

KiB == 1024

**示例**:

FileSize Example

```json
{
  "fileSize": "2048MiB"
}
```

### sqlDBType

**类型**: string

**描述**: 数据导出为文件时，指定生成那种数据库的语法.

如从Oracle迁移到MySQL，默认生成MySQL的插入数据语法

### csvHeader

**类型**: bool

**描述**: 导出文件为csv时，指定是否包含csv文件头。默认不导出。

**默认值**: `false`

### csvNullValue

**类型**: string

**描述**: csv空值展示

### csvFieldDelimiter

**类型**: string

**描述**: csv默认间隔符。

**默认值**: `,`

### csvOptionallyEnclosed

**类型**: string

**描述**: 数据包裹符。

**默认值**: `"`

### timeFormat

**类型**: string

**描述**: 定义时间格式

**默认值**: `HH:MI:SS`

### dateFormat

**类型**: string

**描述**: 定义日期格式

**默认值**: `YYYY-MM-DD`

### dateTimeFormat

**类型**: string

**描述**: 定义时间完整格式

**默认值**: `YYYY-MM-DD HH24:MI:SS`

### excludeSysTable

**类型**: []string

**描述**: 排除系统对象定义。不配置则使用默认值

**示例**:

DB2 Exclude System Table example

```json
{
  "excludeSysTable": [
    "EXPLAIN_ACTUALS",
    "ADVISE_TABLE",
    "ADVISE_PARTITION",
    "ADVISE_MQT",
    "ADVISE_WORKLOAD",
    "ADVISE_INDEX",
    "ADVISE_INSTANCE",
    "OBJECT_METRICS",
    "EXPLAIN_DIAGNOSTIC_DATA",
    "EXPLAIN_DIAGNOSTIC",
    "EXPLAIN_STREAM",
    "EXPLAIN_PREDICATE",
    "EXPLAIN_OPERATOR",
    "EXPLAIN_OBJECT",
    "EXPLAIN_ARGUMENT",
    "EXPLAIN_STATEMENT",
    "EXPLAIN_INSTANCE"
  ]
}
```

### enableSyncCompTabPro

**类型**: bool

**描述**: 是否配置生成DDL包含表压缩属性语法。 默认不生成。

**默认值**: `false`

### tableOptions

**类型**: map[string]string

**描述**: 配置在创建表的时候添加表属性选项.暂时只支持openGauss/MogDB.

- openGauss/MogDB `create table xxx() with (xx)`;

参数不区分大小写

- "*"              迁移对象中所有表
- "Schema1.*"       迁移对象中Schema1下所有表
- "Schema1.TABLE_1"   迁移对象中Schema1下table表

选项特殊值 remove

如 `compression=remove` 会移除压缩属性

**默认值**: 空

**示例**:

Table Options Example

```json
{
  "tableOptions": {
    "*": "orientation=row, compression=no",
    "schema1.*": "orientation=row, compression=no",
    "schema1.table_1": "orientation=row, compression=no"
  }
}
```

**新增于**: v2.3.2

### indexOptions

**类型**: map[string]string

**描述**: 和tableOptions选项配置一样,只针对索引生效. 未完成功能开发.

**默认值**: 空

**新增于**: v2.3.2

### remapSchema

**类型**: map[string]string

**描述**: 迁移过程中是否对数据库schema进行改名操作。

格式为 原方案名:新方案名

如 MTK1更名为 MTK1_NEW, SOE更名为 SOE_NEW

**示例**:

Remap Schema Example

```json
{
  "remapSchema": {
    "DB_APP_01": "APP_01",
    "DB_APP_02": "APP_02"
  }
}
```

### remapTable

**类型**: map[string]string

**描述**: 迁移过程中是否对表名进行改名操作

**Deprecated**: 已不再支持

### remapTablespace

**类型**: map[string]string

**描述**: 迁移过程中是否对表空间名进行改名操作。

配合[enableSyncTabTbsPro](#enablesynctabtbspro)使用

如 USERSPACE1更名为 TBS01

**示例**:

Remap Tablespace Example

```json
{
  "remapTablespace": {
    "USERSPACE1": "TBS01"
  }
}
```

### enableSyncTabTbsPro

**类型**: bool

**描述**: 是否配置生成DDL包含表空间语法。 默认不生成

**默认值**: `false`

### noSupportPartTabToNormalTab

**类型**: bool

**描述**: 把目标端不支持的分区表转换普通表

把目标端不支持的子分区表属性自动移除

Oracle的List分区和Hash分区表在MogDB里为普通表。

Oracle的Range List复合分区表在MogDB里为Range分区表，子分区表自动移除

### ignoreDB2PartInclusive

**类型**: bool

**描述**: 是否忽略DB2分区键值ENDING的包含属性

如下面DB2建表语句分区语法中`ENDING`包含属性`INCLUSIVE`定义则此分区包含`20180101`数据

但是在openGauss数据库中范围分区不支持包含属性只能`less than`

这种情况下会遇到报错 `the part DATAMIN is included high value for db2 inclusive option` 可以配置此参数忽略上面错误

```sql
CREATE TABLE MTK1.TABLE_TEST_HAOTD
(
    DATADATE VARCHAR(8) NOT NULL,
    DATA1    VARCHAR(10),
    DATA2    VARCHAR(10)
) PARTITION BY RANGE(DATADATE) (
  PART "DATAMIN" STARTING(MINVALUE) ENDING('20180101') INCLUSIVE ,
  PART "P20180101" STARTING('20180101') ENDING('20180102') INCLUSIVE
)
```

**默认值**: `false`

### igNotSupportIntervalPart

**类型**: bool

**描述**: 支持忽略部分不支持的`interval分`区属性

**默认值**: `false`

### ignoreTabPartition

**类型**: bool

**描述**: 支持迁移到目标端数据库忽略分区语法, 现在只支持迁移到MySQL.

如Oracle分区表迁移到MySQL变成非分区表

Oracle

```sql
CREATE TABLE "MTK"."TAB_PART_LIST" (
  "DEPTNO" NUMBER(10,0) NOT NULL,
  "DEPTNAME" VARCHAR2(20 BYTE),
  "QUARTERLY_SALES" NUMBER(10,2),
  "STATE" VARCHAR2(2 BYTE)
) PARTITION BY LIST ("STATE")
(
  PARTITION "Q1_NORTHWEST" VALUES ('OR', 'WA') TABLESPACE "USERS",
  PARTITION "Q1_SOUTHWEST" VALUES ('AZ', 'CA', 'NM') TABLESPACE "USERS",
  PARTITION "Q1_NORTHEAST" VALUES ('NY', 'VT', 'NJ') TABLESPACE "USERS",
  PARTITION "Q1_SOUTHEAST" VALUES ('FL', 'GA') TABLESPACE "USERS",
  PARTITION "Q1_NORTHCENT" VALUES ('MN', 'WI') TABLESPACE "USERS",
  PARTITION "Q1_SOUTHCENT" VALUES ('OK', 'TX') TABLESPACE "USERS"
)
```

MySQL

```sql
CREATE TABLE mtk.tab_part_list (
  deptno BIGINT NOT NULL,
  deptname VARCHAR(20),
  quarterly_sales DECIMAL(10,2),
  state VARCHAR(2)
)
```

**默认值**: `false`

### autoAddMaxvaluePart

**类型**: bool

**描述**: 参数`autoAddMaxvaluePart`允许不存在`maxvalue`分区的分区表自动添加`maxvalue`分区.

如 DB2分区表迁移到openGauss

DB2允许定义`minvalue`分区,openGauss不支持.

DB2在定义`minvalue`分区未定义`maxvalue`分区时null值可以插入到`minvalue`分区,openGauss会报错.

但是openGauss分区允许null插入到`maxvalue`分区. 可通过此参数开启自动添加`maxvalue`分区

DB2未定义`maxvalue`分区

```sql
CREATE TABLE MTK1.PART_TAB_TEST02 (
 ID INTEGER NOT NULL,
 SALES_PERSON VARCHAR(50),
 REGION VARCHAR(50),
 SALES_DATE DATE
)
PARTITION BY RANGE(SALES_DATE)
(
 PART PJAN STARTING('2017-01-01') ENDING('2017-03-31') INCLUSIVE IN USERSPACE1,
 PART PFEB STARTING('2017-04-01') ENDING('2017-07-31') INCLUSIVE IN USERSPACE1,
 PART PMAR STARTING('2017-08-01') ENDING('2017-12-31') INCLUSIVE IN USERSPACE1,
 PART PAPR STARTING('2018-01-01') ENDING('2018-12-31') INCLUSIVE IN USERSPACE1
) ORGANIZE BY ROW
```

openGauss

```sql
CREATE TABLE DB2_MTK.PART_TAB_TEST02 (
 ID INTEGER NOT NULL,
 SALES_PERSON VARCHAR(50),
 REGION VARCHAR(50),
 SALES_DATE DATE
) PARTITION BY RANGE (SALES_DATE)
(
    PARTITION PJAN VALUES LESS THAN('2017-03-31'),
    PARTITION PFEB VALUES LESS THAN('2017-07-31'),
    PARTITION PMAR VALUES LESS THAN('2017-12-31'),
    PARTITION PAPR VALUES LESS THAN('2018-12-31'),
    PARTITION PART_MAXVALUE VALUES LESS THAN(MAXVALUE)
)
```

**新增于**: v0.0.36

### igErrorData

**类型**: bool

**描述**: 忽略插入失败的数据并记录到错误文件.

只支持 openGauss/MogDB/PostgreSQL/MySQL

- openGauss/MogDB/PostgreSQL

    | batchCommit | igErrorData | copy Savepoint | 描述         |
    |:------------|:------------|:---------------|:-----------|
    | false       | false       | false          | 全部rollback |
    | false       | false       | true           | 全部rollback |
    | false       | true        | true           | 失败数据未迁移    |
    | true        | false       | false          | 保留成功Commit数据   |
    | true        | false       | true           | 保留成功Commit数据   |
    | true        | true        | true           | 失败数据未迁移    |

- MySQL

    | batchCommit | igErrorData | 描述         |
    |:------------|:------------|:-----------|
    | false       | false       | 全部rollback |
    | false       | true        | 失败数据未迁移    |
    | true        | false       | 保留成功Commit数据   |
    | true        | true        | 失败数据未迁移    |

**默认值**: `false`

**新增于**: v2.2.3

### enableBatchCommit

**类型**: bool

**描述**: 启用分批次Commit

**默认值**: `false`

**可选值**:

- true
- false

### mySQLSkipErrorDateTimeData

**类型**: bool

**描述**: 自动跳过mysql错误时间。

如 `datetime` 类型里 `0000-00-00 13:14:15`

**默认值**: `false`

**Deprecated**: 从2.3.4以后版本已不再支持.

**可选值**:

- true
- false

### autoAddMySQLAutoIncr

**类型**: bool

**描述**: 迁移到MySQL自动添加MySQL `AUTO_INCREMENT` 列

参数为`true`,当原表不存在自增列,自动增加`AUTO_INCREMENT`列,并把新增列设置为主键,原来主键改为唯一键.

```sql
PK_ID BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL [INVISIBLE]
```

- MySQl 8.0.23 之前为 `visible`
- MySQl 8.0.23 为 `invisible column`

> MySQL supports invisible columns as of MySQL 8.0.23.<br/>
> An invisible column is normally hidden to queries, but can be accessed if explicitly referenced.<br/>
> Prior to MySQL 8.0.23, all columns are visible.

配合[ignoreTabPartition](#ignoretabpartition)参数使用.

- DB2迁移到MySQL

  - DB2

    ```sql
    CREATE TABLE MTK1.TAB_PK_TEST (
        COL1 VARCHAR(10) NOT NULL,
        ID BIGINT NOT NULL,
        COL2 VARCHAR(10),
        COL3 VARCHAR(10)
    ) IN USERSPACE1 ORGANIZE BY ROW
    ```

  - MySQL

    ```sql
    CREATE TABLE db2_mtk.tab_pk_test (
        -- 自动增加主键列
        pk_id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL INVISIBLE,
        col1 VARCHAR(10) NOT NULL,
        id BIGINT NOT NULL,
        col2 VARCHAR(10),
        col3 VARCHAR(10),
        -- 原主键变为唯一键
        UNIQUE KEY ( col1 )
    )
    ```

**默认值**: `false`

**新增于**: v2.1.1

### autoAddMySQLAutoIncrTabList

**类型**: []string

**描述**: 配合`autoAddMySQLAutoIncr`参数使用.

配置哪些表自动增加`auto_incr`列. 默认为空代表为全部表.

参数不区分大小写

- "TABLE_1"          迁移对象中所有表名为TABLE_1的表
- "Schema1.TABLE_1"  迁移对象中Schema1下的TABLE_1的表

**默认值**: 空

**示例**:

autoAddMySQLAutoIncrTabList Example

```json
{
  "autoAddMySQLAutoIncrTabList": [
    "TABLE_1",
    "SCHEMA1.TABLE_1"
  ]
}
```

**新增于**: v2.2.1

### ignoreNotSupportDefault

**类型**: bool

**描述**: 支持忽略部分不支持列默认值.如Oracle的`sys_guid`。MTK会内嵌白名单进行忽略

**默认值**: `false`

### replaceZeroDate

**类型**: string

**描述**: openGauss/MogDB/PostgreSQL 时间格式不支持 `0000-00-00`. 配置此参数会进行对应替换. 替换的范围如下:

- 列默认值
- 存储过程/函数中的SQL语句

> When find a "zero" date: `0000-00-00 00:00:00` it is replaced by a NULL.
>
> This could be a problem if your column is defined with NOT NULL constraint.
> If you can not remove the constraint,
>
> use this directive to set an arbitral date that will be used instead.
>
> You can also use -INFINITY if you don't want to use a fake date.

### virtualColToNormalCol

**类型**: bool

**描述**: 是否把源库虚拟列转成目标库的正常列。

**默认值**: `false`

**可选值**:

- true
- false

### virtualColConv

**类型**: map[string]string

**描述**: 虚拟列表达转换功能

**示例**:

virtualColConv example

```json
{
  "virtualColConv": {
    "LEFT(HOST,POSSTR(HOST,':')-1)": "SPLIT_PART(HOST,':',1)",
    "TRUNC_TIMESTAMP(SNAPTIME,'HH')+ (MINUTE(SNAPTIME)/10*10 +10) MINUTES": "date_trunc('hour',snaptime) + (date_part('minute',snaptime) / 10 +1)::int * interval '10 min'"
  }
}
```

### mySQLToOgCharExpression

**类型**: string

**描述**: mysql 迁移到 openGauss 针对 char/varchar列进行计算列长表达式. 默认不计算 如 `* 3` 表示原来到长度`*3`

**Deprecated**: mtk scales up the length based on the db type,column type, db compatibility mode

### convertOracleIntegerToNumeric

**类型**: bool

**描述**: 转换Oracle的Integer类型为NUMERIC. 支持openGauss/MogDB/PostgreSQL

**默认值**: `false`

**可选值**:

- true
- false

### enableOgBlobClob

**类型**: bool

**描述**: 迁移其他数据库的Blob、Clob为openGauss的Blob、Clob.

**默认值**: `false`

**可选值**:

- true
- false

### skipColumnType

**类型**: map[string]int

**描述**: 迁移时跳过某个数据类型列

格式为: "列类型": int

**默认值**: `0`

**可选值**:

- 1 不创建列并不迁移数据
- 2 创建列但不迁移数据

**示例**:

Skip ColumnType Example

```json
{
  "skipColumnType": {
    "COL_TYPE_1": 1,
    "COL_TYPE_2": 2
  }
}
```

### skipColumnName

**类型**: map[string]int

**描述**: 迁移时跳过某个列

优先级高于 `skipColumnType`

格式为 "schema.table_name.column": int

**默认值**: `0`

**可选值**:

- 1 不创建列并不迁移数据
- 2 创建列但不迁移数据

**示例**:

Skip ColumnName Example

```json
{
  "skipColumnName": {
    "SCHEMA1.TAB_01.COL1": 1,
    "SCHEMA1.TAB_01.COL2": 2
  }
}
```

### charLengthChangeExclude

**类型**: []string

**描述**: 跨字符集迁移时,针对CHAR类型指定表达式,对列名进行匹配并不进行长度扩充.
注意忽略大小写问题.

- `^update_time*`   忽略列名开头为 `update_time` 的列
- `^update_time`  忽略列名为 `update_time` 的列
- `(?i)^update_time$`  忽略列名大小写的情况下 `update_time` 的列

**示例**:

Char Length Change Exclude Example

```json
{
  "charLengthChangeExclude": [
    "^update_time*",
    "^visit_date$",
    "(?i)^visit_date$"
  ]
}
```

**新增于**: v2.5.2

### enableCharTrimRightSpace

**类型**: bool

**描述**: 针对char迁移为varchar允许截取`char`类型右侧空格. 只支持目标端为openGauss/MogDB

**默认值**: `false`

**可选值**:

- true
- false

**新增于**: v2.7.1

### convertPackageMethod

**类型**: string

**描述**: 迁移 Oracle Package 到 openGauss/MogDB 的方式

openGauss数据库版本大于等于3.0.0且datCompatibility为A默认为 `package`

openGauss数据库版本大于等于2.1.0可配置为 `package` 或 `schema`

openGauss数据库小于2.1.0 只能配置为 `schema`

**默认值**: `schema`

**可选值**:

- schema
- package

### enableConvertSrid

**类型**: bool

**描述**: Enable Convert Srid

**默认值**: `false`

**可选值**:

- true
- false

### defaultSrid

**类型**: string

**描述**: PostGis Default Srid

**默认值**: `4326`

### seqLastNumAddNum

**类型**: int

**描述**: 同步序录最后值时增加多少.

仅支持openGauss/MogDB 目标端

**默认值**: `0`

### templateSeqName

**类型**: string

**描述**: MySQL 自增列转为序列,序列名的模版

{{.TabName}}_{{.ColName}}_SEQ

仅支持以下两个变量

- {{.TabName}} 表名
- {{.ColName}} 列名

**默认值**: SEQ_{{.TabName}}_{{.ColName}}

**示例**:

Template SeqName Example

```json
{
  "templateSeqName": "{{.TabName}}_{{.ColName}}_SEQ"
}
```

**新增于**: v2.1.6

### charAppendEmptyString

**类型**: bool

**描述**: 针对ORA-29275: partial multibyte character 错误. 在oracle查询是拼接字符串

主要针对以下列类型

- Char
- Character
- NChar
- Varchar
- NVarchar
- Varchar2
- NVarchar2

```sql
select chr(195) from dual;         -- 报错
select chr(195)||'' from dual;     -- 不报错

-- 中文GBK编码 证券投
select utl_raw.cast_to_varchar2(hextoraw('D6A4C8AFCDB6D6')) from dual;          -- 报错
select utl_raw.cast_to_varchar2(hextoraw('D6A4C8AFCDB6D6'))||'' from dual;      -- 不报错
```

**默认值**: `false`

**可选值**:

- true
- false

### charsetTranscode

**类型**: bool

**描述**: 针对错误的编码进行转码.

- 表里异常数据. UTF8数据库存在GBK编码
- 存储过程中文乱码  -- 暂时未支持

**默认值**: false

**新增于**: v2.5.0
