---
title: MTK Configuration File Description
summary: Configuration File Description
author: mtk
date: 2022-09-26 11:15:35
---

# MTK Configuration File Description

## Config

Config MTK迁移配置文件.

| Field | Type | Description |
|-------|------|-------------|
| [source](#option) |Option |源数据库配置信息. |
| [target](#option) |Option |目标数据库配置信息. |
| [limit](./mtk-limit.md#limit) |Limit |迁移并发配置. |
| [object](./mtk-object.md#object) |Object |迁移对象配置. |
| [dataOnly](#dataonly) |bool |是否只迁移数据. |
| [schemaOnly](#schemaonly) |bool |是否只迁移数据结构. |
| [disableTableDataComp](#disabletabledatacomp) |bool |当数据迁移完成后，MTK会统计两边的行数进行对比. |
| [disableCollStatistics](#disablecollstatistics) |bool |当数据迁移完成后，MTK会收集目标端统计信息. |
| [reportFile](#reportfile) |string |迁移报告目录. |
| [debug](#debug) |bool |是否开启日志debug模式. |
| [preRun](#prerun) |bool |预运行. |
| [test](#test) |bool |试迁移. |
| [disableIgnoreCase](#disableignorecase) |bool |关闭在源库查询忽略大小写功能. |
| [disableSelectPart](#disableselectpart) |bool |关闭分区查询功能. |
| [disableFKCons](#disablefkcons) |bool |禁用外键同步. |
| [disableSyncIdxAfterData](#disablesyncidxafterdata) |bool |在一次性迁移模式下禁用在同步完单个表数据后立即创建这个表的索引. 改为全部完全部迁移表数据后,在进行同步索引 |
| [disablePrintMigDataProgress](#disableprintmigdataprogress) |bool |关闭打印迁移表数据进度功能.打印进度影响迁移性能 |

**示例**:

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "127.0.0.1",
      "user": "system",
      "port": 1521,
      "password": "******",
      "dbName": "orcl"
    }
  },
  "target": {
    "type": "opengauss",
    "connect": {
      "version": "3.0.0",
      "host": "127.0.0.1",
      "user": "gaussdb",
      "port": 26000,
      "password": "******",
      "dbName": "postgres"
    },
    "parameter": {
      "parallelInsert": 1,
      "dropExistingObject": false,
      "truncTable": false,
      "colKeyWords": {},
      "objKeyWords": {},
      "caseSensitive": 0,
      "quoteMark": false,
      "path": "./data",
      "schemaPath": "",
      "dataPath": "",
      "errDataPath": "",
      "fileType": "",
      "fileSize": "",
      "csvHeader": false,
      "csvNullValue": "",
      "csvFieldDelimiter": ",",
      "csvOptionallyEnclosed": "\"",
      "excludeSysTable": [],
      "remapSchema": {},
      "remapTable": {},
      "remapTablespace": {},
      "enableSyncTabTbsPro": false,
      "enableSyncCompTabPro": false,
      "timeFormat": "HH:MI:SS",
      "dateFormat": "YYYY-MM-DD",
      "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
      "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
      "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
      "noSupportPartTabToNormalTab": false,
      "ignoreDB2PartInclusive": false,
      "igNotSupportIntervalPart": false,
      "igErrorData": false,
      "enableBatchCommit": false,
      "ignoreTabPartition": false,
      "autoAddMaxvaluePart": false,
      "autoAddMySQLAutoIncr": false,
      "autoAddMySQLAutoIncrTabList": [],
      "ignoreNotSupportDefault": false,
      "replaceZeroDate": "",
      "virtualColToNormalCol": false,
      "virtualColConv": {},
      "mySQLSkipErrorDateTimeData": false,
      "ignoreTableDDLCompErr": false,
      "convertPackageMethod": "",
      "convertOracleIntegerToNumeric": false,
      "enableOgBlobClob": false,
      "enableConvertSrid": false,
      "defaultSrid": "4326",
      "seqLastNumAddNum": 0,
      "skipColumnType": {},
      "skipColumnName": {},
      "templateSeqName": "",
      "charAppendEmptyString": false,
      "tableOptions": {},
      "indexOptions": {}
    }
  },
  "limit": {
    "parallel": 2,
    "fetchSize": 1000,
    "batchSize": 1000,
    "bufferSize": 8,
    "cpBufferSize": 8,
    "oracleSelectParallel": 2,
    "channelCacheNum": 100000,
    "limit": 0
  },
  "object": {
    "tables": [
      "MTK.TAB1",
      "MTK.TAB111%",
      "MTK1.*"
    ],
    "schemas": [
      "SCHEMA1",
      "SCHEMA2"
    ],
    "excludeTable": {
      "SCHEMA1": [
        "TABLE_SKIP1",
        "TABLE_SKIP1",
        "TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG.*$"
      ],
      "SCHEMA2": [
        "TABLE_SKIP1",
        "TABLE_SKIP1"
      ]
    },
    "tableSplit": {
      "SCHEMA1": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      },
      "SCHEMA2": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      }
    }
  },
  "dataOnly": false,
  "schemaOnly": false,
  "disableTableDataComp": false,
  "disableCollStatistics": false,
  "reportFile": "./report/",
  "debug": false,
  "preRun": false,
  "test": false,
  "disableIgnoreCase": false,
  "disableSelectPart": false,
  "disableFKCons": false,
  "disableSyncIdxAfterData": false,
  "disablePrintMigDataProgress": false
}
```

### source

**类型**: [`Option`](#option)

**描述**: 源数据库配置信息.

**示例**:

Source Database Config Example

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "127.0.0.1",
      "user": "system",
      "port": 1521,
      "password": "******",
      "dbName": "orcl"
    }
  }
}
```

### target

**类型**: [`Option`](#option)

**描述**: 目标数据库配置信息.

**示例**:

Target Database Config Example

```json
{
  "target": {
    "type": "opengauss",
    "connect": {
      "version": "3.0.0",
      "host": "127.0.0.1",
      "user": "gaussdb",
      "port": 26000,
      "password": "******",
      "dbName": "postgres"
    },
    "parameter": {
      "parallelInsert": 1,
      "dropExistingObject": false,
      "truncTable": false,
      "colKeyWords": {},
      "objKeyWords": {},
      "caseSensitive": 0,
      "quoteMark": false,
      "path": "./data",
      "schemaPath": "",
      "dataPath": "",
      "errDataPath": "",
      "fileType": "",
      "fileSize": "",
      "csvHeader": false,
      "csvNullValue": "",
      "csvFieldDelimiter": ",",
      "csvOptionallyEnclosed": "\"",
      "excludeSysTable": [],
      "remapSchema": {},
      "remapTable": {},
      "remapTablespace": {},
      "enableSyncTabTbsPro": false,
      "enableSyncCompTabPro": false,
      "timeFormat": "HH:MI:SS",
      "dateFormat": "YYYY-MM-DD",
      "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
      "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
      "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
      "noSupportPartTabToNormalTab": false,
      "ignoreDB2PartInclusive": false,
      "igNotSupportIntervalPart": false,
      "igErrorData": false,
      "enableBatchCommit": false,
      "ignoreTabPartition": false,
      "autoAddMaxvaluePart": false,
      "autoAddMySQLAutoIncr": false,
      "autoAddMySQLAutoIncrTabList": [],
      "ignoreNotSupportDefault": false,
      "replaceZeroDate": "",
      "virtualColToNormalCol": false,
      "virtualColConv": {},
      "mySQLSkipErrorDateTimeData": false,
      "ignoreTableDDLCompErr": false,
      "convertPackageMethod": "",
      "convertOracleIntegerToNumeric": false,
      "enableOgBlobClob": false,
      "enableConvertSrid": false,
      "defaultSrid": "4326",
      "seqLastNumAddNum": 0,
      "skipColumnType": {},
      "skipColumnName": {},
      "templateSeqName": "",
      "charAppendEmptyString": false,
      "tableOptions": {},
      "indexOptions": {}
    }
  }
}
```

### limit

**类型**: [`Limit`](./mtk-limit.md#limit)

**描述**: 迁移并发配置.

**示例**:

Limit Example

```json
{
  "limit": {
    "parallel": 2,
    "fetchSize": 1000,
    "batchSize": 1000,
    "bufferSize": 8,
    "cpBufferSize": 8,
    "oracleSelectParallel": 2,
    "channelCacheNum": 100000,
    "limit": 0
  }
}
```

### object

**类型**: [`Object`](./mtk-object.md#object)

**描述**: 迁移对象配置.

**示例**:

Object Example

```json
{
  "object": {
    "tables": [
      "MTK.TAB1",
      "MTK.TAB111%",
      "MTK1.*"
    ],
    "schemas": [
      "SCHEMA1",
      "SCHEMA2"
    ],
    "excludeTable": {
      "SCHEMA1": [
        "TABLE_SKIP1",
        "TABLE_SKIP1",
        "TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG.*$"
      ],
      "SCHEMA2": [
        "TABLE_SKIP1",
        "TABLE_SKIP1"
      ]
    },
    "tableSplit": {
      "SCHEMA1": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      },
      "SCHEMA2": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      }
    }
  }
}
```

### dataOnly

**类型**: bool

**描述**: 是否只迁移数据.

**默认值**: `false`

**可选值**:

- true
- false

### schemaOnly

**类型**: bool

**描述**: 是否只迁移数据结构.

**默认值**: `false`

**可选值**:

- true
- false

### disableTableDataComp

**类型**: bool

**描述**: 当数据迁移完成后，MTK会统计两边的行数进行对比.
此参数可以禁用表数据select对比

**默认值**: `false`

**可选值**:

- true
- false

### disableCollStatistics

**类型**: bool

**描述**: 当数据迁移完成后，MTK会收集目标端统计信息.
此参数可以禁用收集统计信息

**默认值**: `false`

**可选值**:

- true
- false

### reportFile

**类型**: string

**描述**: 迁移报告目录.
v0.0.18之前为单个html报告
v0.0.18后是一个目录

### debug

**类型**: bool

**描述**: 是否开启日志debug模式.

**默认值**: `false`

**可选值**:

- true
- false

### preRun

**类型**: bool

**描述**: 预运行.

**默认值**: `false`

**可选值**:

- true
- false

### test

**类型**: bool

**描述**: 试迁移.
迁移参数`limit.limit`行数据. 此模式下数据不就行提交,会自动回退数据.
只是测试是否正常插入数据

**默认值**: `false`

**可选值**:

- true
- false

### disableIgnoreCase

**类型**: bool

**描述**: 关闭在源库查询忽略大小写功能.

**默认值**: `false`

**可选值**:

- true
- false

### disableSelectPart

**类型**: bool

**描述**: 关闭分区查询功能.

**默认值**: `false`

**可选值**:

- true
- false

### disableFKCons

**类型**: bool

**描述**: 禁用外键同步.

**默认值**: `false`

**可选值**:

- true
- false

### disableSyncIdxAfterData

**类型**: bool

**描述**: 在一次性迁移模式下禁用在同步完单个表数据后立即创建这个表的索引. 改为全部完全部迁移表数据后,在进行同步索引

**默认值**: `false`

**可选值**:

- true
- false

### disablePrintMigDataProgress

**类型**: bool

**描述**: 关闭打印迁移表数据进度功能.打印进度影响迁移性能

**默认值**: `false`

**Deprecated**: 更换了打印进度方式,不在异步打印进度

**可选值**:

- true
- false

## Connect

Connect 数据库连接信息

| Field | Type | Description |
|-------|------|-------------|
| [version](#version) |string |数据库版本. |
| [vendor](#vendor) |string |数据库发行厂家. |
| [host](#host) |string |数据库主机 |
| [user](#user) |string |数据库用户 |
| [port](#port) |int |数据库端口 |
| [password](#password) |Password |数据库用户密码 |
| [dbName](#dbname) |string |数据库名称 |
| [dsn](#dsn) |string |用户指定连接字符串. |
| [timeout](#timeout) |Duration |连接超时时间. |
| [charset](#charset) |string |数据库字符集. |
| [datCompatibility](#datcompatibility) |string |针对openGauss 数据库兼容模式. |
| [sqlMode](#sqlmode) |string |针对 MySQL 数据库 `sql_mode`. |
| [clientCharset](#clientcharset) |string |针对数据库编码转换场景使用,一般情况下不需要设置. |

Appears in:

- [`Option`](#option).connect

### version

**类型**: string

**描述**: 数据库版本.

无需指定. 连接数据库时自动查询

迁移成文件是需要手工指定版本

**示例**:

Version Example

```json
{
  "version": "2.1.0"
}
```

### vendor

**类型**: string

**描述**: 数据库发行厂家.

无需指定. 连接数据库时自动查询

**示例**:

Vendor Example

```json
{
  "vendor": "MySQL"
}
```

### host

**类型**: string

**描述**: 数据库主机

**可选值**:

- ip
- 域名

**示例**:

Host Example

```json
{
  "host": "127.0.0.1"
}
```

### user

**类型**: string

**描述**: 数据库用户

**示例**:

User Example

```json
{
  "user": "system"
}
```

### port

**类型**: int

**描述**: 数据库端口

**示例**:

Port Example

```json
{
  "port": 1521
}
```

### password

**类型**: Password

**描述**: 数据库用户密码

### dbName

**类型**: string

**描述**: 数据库名称

**示例**:

DBName Example

```json
{
  "dbName": "orcl"
}
```

### dsn

**类型**: string

**描述**: 用户指定连接字符串.

默认情况下无需指定.针对特殊场景预留功能

### timeout

**类型**: Duration

**描述**: 连接超时时间.

无需配置

**默认值**: 30s

### charset

**类型**: string

**描述**: 数据库字符集.

默认不用配置,连接数据库查询

**可选值**:

- gbk
- utf8

**示例**:

Charset Example

```json
{
  "charset": "gbk"
}
```

### datCompatibility

**类型**: string

**描述**: 针对openGauss 数据库兼容模式.

默认不用配置,连接数据库查询.

迁移成文件需要配置

**可选值**:

- A
- B
- PG

**示例**:

DatCompatibility Example

```json
{
  "datCompatibility": "A"
}
```

### sqlMode

**类型**: string

**描述**: 针对 MySQL 数据库 `sql_mode`.

默认不用配置,连接数据库查询.

迁移成文件需要配置

### clientCharset

**类型**: string

**描述**: 针对数据库编码转换场景使用,一般情况下不需要设置.

如Oracle ZHS16GBK编码迁移到openGauss UTF8. 遇到`ORA-29275: partial multibyte character`.

## Option

Option 数据库配置

| Field | Type | Description |
|-------|------|-------------|
| [type](#type) |string |数据库类型，不区分大小写 |
| [connect](#connect) |Connect |数据库连接信息 |
| [parameter](./mtk-parameter.md#parameter) |Parameter |参数配置 |

Appears in:

- [`Config`](#config).source
- [`Config`](#config).target

**示例**:

Source Database Config Example

```json
{
  "type": "oracle",
  "connect": {
    "version": "",
    "host": "127.0.0.1",
    "user": "system",
    "port": 1521,
    "password": "******",
    "dbName": "orcl"
  }
}
```

Target Database Config Example

```json
{
  "type": "opengauss",
  "connect": {
    "version": "3.0.0",
    "host": "127.0.0.1",
    "user": "gaussdb",
    "port": 26000,
    "password": "******",
    "dbName": "postgres"
  },
  "parameter": {
    "parallelInsert": 1,
    "dropExistingObject": false,
    "truncTable": false,
    "colKeyWords": {},
    "objKeyWords": {},
    "caseSensitive": 0,
    "quoteMark": false,
    "path": "./data",
    "schemaPath": "",
    "dataPath": "",
    "errDataPath": "",
    "fileType": "",
    "fileSize": "",
    "csvHeader": false,
    "csvNullValue": "",
    "csvFieldDelimiter": ",",
    "csvOptionallyEnclosed": "\"",
    "excludeSysTable": [],
    "remapSchema": {},
    "remapTable": {},
    "remapTablespace": {},
    "enableSyncTabTbsPro": false,
    "enableSyncCompTabPro": false,
    "timeFormat": "HH:MI:SS",
    "dateFormat": "YYYY-MM-DD",
    "dateTimeFormat": "YYYY-MM-DD HH24:MI:SS",
    "timeStampFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF",
    "timeStampZoneFormat": "YYYY-MM-DD HH24:MI:SSXFFFFFF TZR",
    "noSupportPartTabToNormalTab": false,
    "ignoreDB2PartInclusive": false,
    "igNotSupportIntervalPart": false,
    "igErrorData": false,
    "enableBatchCommit": false,
    "ignoreTabPartition": false,
    "autoAddMaxvaluePart": false,
    "autoAddMySQLAutoIncr": false,
    "autoAddMySQLAutoIncrTabList": [],
    "ignoreNotSupportDefault": false,
    "replaceZeroDate": "",
    "virtualColToNormalCol": false,
    "virtualColConv": {},
    "mySQLSkipErrorDateTimeData": false,
    "ignoreTableDDLCompErr": false,
    "convertPackageMethod": "",
    "convertOracleIntegerToNumeric": false,
    "enableOgBlobClob": false,
    "enableConvertSrid": false,
    "defaultSrid": "4326",
    "seqLastNumAddNum": 0,
    "skipColumnType": {},
    "skipColumnName": {},
    "templateSeqName": "",
    "charAppendEmptyString": false,
    "tableOptions": {},
    "indexOptions": {}
  }
}
```

### type

**类型**: string

**描述**: 数据库类型，不区分大小写

**可选值**:

- MySQL
- Oracle
- Postgres/PostgreSQL
- openGauss
- MogDB
- DB2
- sqlServer
- file
- informix

**示例**:

Type Example

```json
{
  "type": "Oracle"
}
```

### connect

**类型**: [`Connect`](#connect)

**描述**: 数据库连接信息

### parameter

**类型**: [`Parameter`](./mtk-parameter.md#parameter)

**描述**: 参数配置
