---
title: 数据迁移工具 MTK DSN说明
summary: 数据迁移工具 MTK DSN说明
author: Liu Xu
date: 2021-03-04
---

# 数据源(DSN)说明

## Oracle

通常使用中，MTK配置文件中无需配置dsn键值，而是直接配置host,port,user,password键值即可。

但是如果需要从Oracle云上数据库（Oracle Cloud Autonomous Database）中进行数据迁移，也就是源数据库是位于[Oracle Cloud](https://www.oracle.com/)中的Autonomous Database实例，则需要配置dsn，而其它参数可以留空。

具体操作参看Oracle官方文档
[Connecting to an Autonomous Database](https://docs.cloud.oracle.com/en-us/iaas/Content/Database/Tasks/adbconnecting.htm)

配置示例如下：

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "",
      "user": "",
      "port": 0,
      "password": "",
      "dbName": "",
      "dsn": "connectString=db202009151510_high user=ADMIN password=your_admin_password"
    }
  }
}
```

or

```json
{
  "source": {
    "type": "oracle",
    "connect": {
      "version": "",
      "host": "",
      "user": "ADMIN",
      "port": 0,
      "password": "your_admin_password",
      "dbName": "",
      "dsn": "connectString=db202009151510_high"
    }
  }
}
```

其中connectString指定的是 tnsnames.ora 文件中的 Oracle Net Service Name.

该 tnsnames.ora 文件需要按照Oracle Cloud的连接文档从Oracle Autonomous Database 实例相关页面中下载。

其它可参考文档：

[Go DRiver for Oracle User Guide](https://godror.github.io/godror/doc/connection.html)

[Oracle Database 19c Easy Connect Plus Configurable Database Connection Syntax](https://download.oracle.com/ocomdocs/global/Oracle-Net-19c-Easy-Connect-Plus.pdf)

```json
{
  "dsn": "user=system password=pwd connectString=\"(DESCRIPTION=(CONNECT_TIMEOUT = 5)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=mtk)))\" configDir= connectionClass=godror enableEvents=0 externalAuth=0 heterogeneousPool=0 libDir= newPassword= noTimezoneCheck=0 poolIncrement=0 poolMaxSessions=100 poolMinSessions=0 poolSessionMaxLifetime=0s poolSessionTimeout=0s poolWaitTimeout=0s prelim=0 standaloneConnection=1 sysasm=0 sysdba=0 sysoper=0 timezone="
}
```

## openGauss/MogDB/postgres

```json
{
  "dsn": "application_name=mtk_postgres connect_timeout=5 dbname=postgres host=127.0.0.1 min_read_buffer_size=8388608 password=pwd port=5434 sslmode=disable user=gaussdb"
}
```

## MySQL

```json
{
  "dsn": "root:pwd@tcp(127.0.0.1:3306)/mysql?loc=Local&multiStatements=true&timeout=30s&maxAllowedPacket=1073741824&charset=utf8mb4"
}
```

## DB2

```json
{
  "dsn": "ClientApplName=MTK;ConnectTimeout=5;DATABASE=testdb;HOSTNAME=127.0.0.1;PORT=50000;PWD=pwd;ProgramName=MTK;UID=db2inst1;XMLDeclaration=0"
}
```

## SQLServer

```json
{
  "dsn": "sqlserver://sa:pwd@127.0.0.1:1433?app+name=mtk&connection+timeout=0&database=testdb"
}
```
