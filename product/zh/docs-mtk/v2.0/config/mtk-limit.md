---
title: MTK Limit
summary: MTK Limit
author: mtk
date: 2022-09-26 11:15:35
---

# MTK Limit

## Limit

Limit 并发配置

| Field | Type | Description |
|-------|------|-------------|
| [parallel](#parallel) |int |并行度. |
| [fetchSize](#fetchsize) |int |指定一次行提取行数大小. |
| [batchSize](#batchsize) |int |指定批量插入或Copy条数大小并进行Commit. |
| [bufferSize](#buffersize) |int |指定查询或者批量插入时缓存大小(以MB为单位). |
| [cpBufferSize](#cpbuffersize) |int |定义Copy命令中使用的缓存大小(以MB为单位). |
| [oracleSelectParallel](#oracleselectparallel) |int |Oracle查询语句中增加hint `/* +parallel(t,n) */` |
| [limit](#limit) |int64 |定义每张表迁移多少行. |

Appears in:

- <code><a href="./mtk-config.md#config">Config</a>.limit</code>

**Example**:

Limit example

```json
{
  "parallel": 2,
  "fetchSize": 1000,
  "batchSize": 1000,
  "bufferSize": 8,
  "cpBufferSize": 8,
  "oracleSelectParallel": 2,
  "channelCacheNum": 10000,
  "limit": 0
}
```

### parallel

**类型**: int

**描述**: 并行度.

迁移数据时执行数据加载的作业(线程)数目.

**默认值**: 1

### fetchSize

**类型**: int

**描述**: 指定一次行提取行数大小.

有效值为 1-50000

支持 Oracle.

**默认值**: 1000

### batchSize

**类型**: int

**描述**: 指定批量插入或Copy条数大小并进行Commit.

2.3.4 之后版本开始支持批量Commit. 之前版本只是批量与数据交互不进行提交

如果提交失败程序会推出并记录错误数据到错误文件.

如果配置[igErrorData](./mtk-parameter.md#igerrordata)程序不会退出会记录错误数据到错误文件并继续处理数据.

有效值为 1-50000

支持Oracle、PostgreSQL、openGauss、MySQL.

批量提交支持 PostgreSQL/MogDB,MySQL

**默认值**: 1000

### bufferSize

**类型**: int

**描述**: 指定查询或者批量插入时缓存大小(以MB为单位).

有效值为1-1024

支持PostgreSQL、openGauss、MySQL.

**默认值**: 8 MB

### cpBufferSize

**类型**: int

**描述**: 定义Copy命令中使用的缓存大小(以MB为单位).

有效值为1-1024

支持 PostgreSQL、openGauss、MySQL

**默认值**: 8 MB

### oracleSelectParallel

**类型**: int

**描述**: Oracle查询语句中增加hint `/* +parallel(t,n) */`

### limit

**类型**: int64

**描述**: 定义每张表迁移多少行.
定义此参数不再进行表并发迁移.
