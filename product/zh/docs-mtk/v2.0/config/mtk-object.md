---
title: MTK Object
summary: MTK Object
author: mtk
date: 2022-09-26 11:15:35
---

# MTK Object

## Object

Object 迁移对象
迁移对象定义

- 注意区分大小写
- 迁移用户、database、schema,
- schema = mysql database
- schema = oracle user
- schema = postgres schema
- schema = db2 schema

> schemas和tables参数互斥
> 如果两个参数都填入了值，在执行MTK时会遇到“mtk-1002 schema and table cannot exist together”错误
>
> 没有定义迁移对象,则会报错 "not define Migrate object"

| Field | Type | Description |
|-------|------|-------------|
| [tables](#tables) |[]string |定义要迁移的表、视图、函数、存储过程、包等对象名. |
| [schemas](#schemas) |[]string | |
| [excludeTable](#excludetable) |map[string][]string |定义排除哪些表. |
| [tableSplit](#tablesplit) |map[string]map[string][]string |定义表粒度并行. |

Appears in:

- <code><a href="./mtk-config.md#config">Config</a>.object</code>

**Example**:

Object Example

```json
{
  "object": {
    "tables": [
      "MTK.TAB1",
      "MTK.TAB111%",
      "MTK1.*"
    ],
    "schemas": [
      "SCHEMA1",
      "SCHEMA2"
    ],
    "excludeTable": {
      "SCHEMA1": [
        "TABLE_SKIP1",
        "TABLE_SKIP1",
        "TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG*",
        "^TABLE_DUTY_LOG.*$"
      ],
      "SCHEMA2": [
        "TABLE_SKIP1",
        "TABLE_SKIP1"
      ]
    },
    "tableSplit": {
      "SCHEMA1": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      },
      "SCHEMA2": {
        "TAB_1": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ],
        "TAB_2": [
          "ID \u003c 10000",
          "ID \u003c 20000 AND ID \u003e=10000",
          "ID \u003e= 90000"
        ]
      }
    }
  }
}
```

### tables

**类型**: []string

**描述**: 定义要迁移的表、视图、函数、存储过程、包等对象名.

当运行子命令迁移时,如 sync-function 并且配置了 tables( `MTK.XXX` ) 就是迁移 Schema MTK 下的 XXX 函数. 序列除外.

|                 |Desc                 |
|-----------------|---------------------|
|schema_name.*             |is equivalent to configuring schema under [schemas](#schemas)|
|schema_name.table_name    ||
|schema_name.object_name%  ||
|schema_name.view_name     |./mtk sync-view   Migrates the view with the specified name under the schema|
|schema_name.function_name |./mtk sync-function  Migrates the function with the specified name under the schema|
|schema_name.procedure_name|./mtk sync-procedure  Migrates the stored procedure with the specified name under the schema|

**示例**:

Tables Example

```json
{
  "tables": [
    "MTK.TAB1",
    "MTK.TAB111%",
    "MTK1.*"
  ]
}
```

### schemas

**类型**: []string

**描述**:

**示例**:

Schemas Example

```json
{
  "schemas": [
    "SCHEMA1",
    "SCHEMA2"
  ]
}
```

### excludeTable

**类型**: map[string][]string

**描述**: 定义排除哪些表.

**示例**:

Exclude Table Example

```json
{
  "excludeTable": {
    "SCHEMA1": [
      "TABLE_SKIP1",
      "TABLE_SKIP1",
      "TABLE_DUTY_LOG*",
      "^TABLE_DUTY_LOG*",
      "^TABLE_DUTY_LOG.*$"
    ],
    "SCHEMA2": [
      "TABLE_SKIP1",
      "TABLE_SKIP1"
    ]
  }
}
```

### tableSplit

**类型**: map[string]map[string][]string

**描述**: 定义表粒度并行.
可使用[mtk show-table-split](../commands/mtk_show-table-split.md)自动生成
定义表迁移查询条件

**示例**:

Table Split Example

```json
{
  "tableSplit": {
    "SCHEMA1": {
      "TAB_1": [
        "ID \u003c 10000",
        "ID \u003c 20000 AND ID \u003e=10000",
        "ID \u003e= 90000"
      ],
      "TAB_2": [
        "ID \u003c 10000",
        "ID \u003c 20000 AND ID \u003e=10000",
        "ID \u003e= 90000"
      ]
    },
    "SCHEMA2": {
      "TAB_1": [
        "ID \u003c 10000",
        "ID \u003c 20000 AND ID \u003e=10000",
        "ID \u003e= 90000"
      ],
      "TAB_2": [
        "ID \u003c 10000",
        "ID \u003c 20000 AND ID \u003e=10000",
        "ID \u003e= 90000"
      ]
    }
  }
}
```
