---
title: 数据迁移工具 MTK 环境依赖
summary: 数据迁移工具 MTK 环境依赖
author: Liu Xu
date: 2021-03-04
---

# MTK环境依赖

迁移不同的数据库需要安装必要的客户端软件。

## Oracle

1. 进入[Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html)页面选择对应的平台

    + [Instant Client for Linux x86-64](https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html)
    + [Instant Client for Linux ARM (aarch64)](https://www.oracle.com/database/technologies/instant-client/linux-arm-aarch64-downloads.html)
    + [Instant Client for macOS (Intel x86)](https://www.oracle.com/database/technologies/instant-client/macos-intel-x86-downloads.html)

2. 选择相应的版本如`Version 19.10.0.0.0`

3. 下载`Basic Package (ZIP)`包,上传到服务器,执行解压命令

    ```bash
    # 此文件为arm64平台
    unzip instantclient-basic-linux.arm64-19.10.0.0.0dbru.zip
    ```

    会解压出来类似`instantclient_19_10`目录.

4. 配置环境变量

    ```bash
    Linux:
    export LD_LIBRARY_PATH=<对应目录>/instantclient_19_10:$LD_LIBRARY_PATH
    Mac:
    export DYLD_LIBRARY_PATH=<对应目录>/instantclient_19_10$DYLD_LIBRARY_PATH
    ```

<br/>

## DB2

1. 进入[DB2 odbc_cli](http://public.dhe.ibm.com/ibmdl/export/pub/software/data/db2/drivers/odbc_cli/)

2. 选择相应平台的版本下载,上传到服务器,执行解压并配置环境变量.

    ```bash
    tar -zxvf linuxx64_odbc_cli.tar.gz -C /db2client
    ```

3. 配置环境变量

    ```bash
    export DB2HOME=/db2client/clidriver
    export CGO_CFLAGS=-I$DB2HOME/include
    export CGO_LDFLAGS=-L$DB2HOME/lib
    export PATH=$DB2HOME/bin:$PATH
    Linux:
    export LD_LIBRARY_PATH=$DB2HOME/lib:$LD_LIBRARY_PATH
    Mac:
    export DYLD_LIBRARY_PATH=$DB2HOME/lib:$DYLD_LIBRARY_PATH
    ```

4. 测试

    ```bash
    db2cli --version
    ```

如果没有安装 db2 odbc cli 会遇到如下报错

```bash
mtk: error while loading shared libraries: libdb2.so.1: cannot open shared object file: No such file or directory
mtk: error while loading shared libraries: libdb2.so.1: cannot open shared object file: No such file or directory
```

<br/>

## MySQL

无需安装额外客户端

<br/>

## openGauss

无需安装额外客户端

## Informix

参考[DB2](#db2)
