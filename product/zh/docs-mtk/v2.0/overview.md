---
title: MTK 简介
summary: MTK 简介
author: Liu Xu
date: 2021-03-04
---

# MTK 简介

MTK全称为 Database Migration Toolkit，是一个可以将Oracle/DB2/MySQL/openGauss/SqlServer/Informix数据库的数据结构，全量数据高速导入到MogDB的工具。
最新版本同时支持对于Oracle/MySQL/DB2数据库中存储过程，函数，触发器等程序段的MogDB兼容性改写和导入。

## 多数据库类型支持

- 支持 Oracle,DB2,openGauss,SqlServer,MySQL,Informix 等数据库之间的互相迁移 (互为源和目标)。
- 支持将数据库内容导出成可执行的 SQL 脚本 （源数据库内容迁移到文本）。

## 迁移性能调整

- 支持调整数据迁移过程中的批量查询、批量插入大小等细粒度参数，来调整数据迁移的性能。
- 支持数据迁移时的多并发，并行和数据分片。

## 结构和数据分离

- 支持迁移对象结构和数据；也支持仅迁移结构或者仅迁移数据（在结构已经迁移完之后）。
- 支持表级和 Schema 级的迁移范围限定，允许指定schema下全部对象或者某些对象进行迁移 。
- 支持迁移过程中的 Schema 重映射，也就是支持将对象从源Schema迁移到目标端的不同名Schema下 。

## 程序迁移（支持Oracle/MySQL/DB2为源,openGauss为目标）

- 支持Oracle/MySQL/DB2->openGauss的存储过程，函数，触发器，包迁移。

自动根据openGauss的语法规则，对Oracle/MySQL/DB2的程序进行改写，之后再在目标端openGauss数据库中创建。

## 迁移场景

| 源数据库     | 目标数据库   |
|:-----------|:-----------|
| Oracle     | openGauss  |
| Oracle     | MySQL      |
| Oracle     | PostgreSQL |
| MySQL      | openGauss  |
| MySQL      | MySQL      |
| MySQL      | PostgreSQL |
| DB2        | openGauss  |
| DB2        | MySQL      |
| DB2        | PostgreSQL |
| DB2        | DB2        |
| SqlServer  | openGauss  |
| SqlServer  | MySQL      |
| SqlServer  | PostgreSQL |
| openGauss  | openGauss  |
| openGauss  | PostgreSQL |
| PostgreSQL | openGauss  |
| Informix   | openGauss  |

<br/>

## 支持数据库对象

具体支持情况,请使用[show-type](commands/mtk_show-type.md)命令查看

| 对象名称       | 是否支持 | 说明                                                                                    |
|----------------|----------|-----------------------------------------------------------------------------------------|
| Schema         | 支持     | 方案 <br />Oracle Users <br />MySQL Database <br />DB2 Schema <br />SqlServer Database Schema |
| Sequence       | 支持     | 序列                                                                                    |
| ObjectType     | 支持     | 类型                                                                                    |
| Domain         | 不支持   | 域                                                                                      |
| Wrapper        | 支持     | Only DB2                                                                                |
| Server         | 支持     | Only DB2                                                                                |
| User-mapping   | 支持     | Only DB2                                                                                |
| Queue          | 不支持   | 队列                                                                                    |
| Table          | 支持     | 不包含OBJECT TYPE TABLE                                                                 |
| NickName       | 支持     | Only DB2                                                                                |
| Rule           | 支持     | 规则                                                                                    |
| TableData      | 支持.    | 表数据                                                                                  |
| Index          | 支持     |                                                                                         |
| Constraint     | 支持     |                                                                                         |
| DBLink         | 支持     | 数据库连接. 仅支持查询个数，不提供语法转换                                              |
| View           | 支持     |                                                                                         |
| MView          | 支持     | 物化视图                                                                                |
| Function       | 支持     |                                                                                         |
| Procedure      | 支持     |                                                                                         |
| Package        | 支持     |                                                                                         |
| Trigger        | 支持     |                                                                                         |
| Synonym        | 支持     |                                                                                         |
| TableDataCom   | 支持     | 表行数对比                                                                              |
| AlterSequence  | 支持     | 迁移序列最后值                                                                          |
| CollStatistics | 支持     | 收集统计信息                                                                            |

## 存储过程转换

MTK支持以下存储过程转换：

- 入参和出参类型转换

- 函数重载

- FROM dual 移除

- 语法转换

  | 转换前                        | 转换后                 |
  | ----------------------------- | ---------------------- |
  | connect by                    | cte改写                |
  | EXECUTE IMMEDIATE             | EXECUTE                |
  | EXIT WHEN NOT FOUND部分语法   |                        |
  | interval语法                  |                        |
  | mod 语法 counter mod 1000 = 0 | mod(counter,4) == 0    |
  | null 相关语法 !=&#124;<> null | is not null            |
  | PIPE ROW                      | RETURN NEXT            |
  | select 1,2 into               | select 1,2 into STRICT |
  | select unique                 | SELECT DISTINCT        |
  | sequence nextval/currval      | nextval(xxx)           |
  | SQLCODE                       | SQLSTATE               |
  | truncate table 部分语法       |                        |

* 函数转换

  | 转换前                        | 转换后                                     |
  | ----------------------------- | ------------------------------------------ |
  | ADD_YEARS                     |                                            |
  | DBMS_OUTPUT.PUT_LINE          | RAISE NOTICE                               |
  | DBMS_LOB.GETLENGTH            | octet_length                               |
  | DBMS_LOB.SUBSTR               | substr                                     |
  | DBMS_STANDARD.RAISE EXCEPTION |                                            |
  | decode                        | case when                                  |
  | empty_blob/empty_clob         | null                                       |
  | from_tz                       |                                            |
  | listagg                       | string_agg                                 |
  | months_between                |                                            |
  | NUMTODSINTERVAL               |                                            |
  | NUMTOYMINTERVAL               |                                            |
  | nvl2                          | case when                                  |
  | raise_application_error       | RAISE EXCEPTION                            |
  | SYS_CONTEXT                   |                                            |
  | SYSTIMESTAMP                  | CURRENT_TIMESTAMP                          |
  | TO_CHAR                       | ::varchar                                  |
  | to_char(l_curr_time, 'TZH')   | EXTRACT( timezone_hour from l_curr_time)   |
  | to_char(l_curr_time, 'TZM')   | EXTRACT( timezone_minute from l_curr_time) |
  | TO_CLOB                       | ""                                         |
  | TO_TIMESTAMP_TZ               |                                            |
  | trunc                         | date_trunc                                 |
  | UTL_ROW.CAST_TO_RAW           | encode                                     |
