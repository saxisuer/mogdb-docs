---
title: Release 2.1
summary: Release 2.1
author: Bin.Liu
date: 2022-07-29
---

# Release 2.1 Notes

## v2.1.6

2021-12-28

- [mtk_2.1.6_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_amd64.tar.gz)
- [mtk_2.1.6_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_amd64_db2.tar.gz)
- [mtk_2.1.6_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_darwin_arm64.tar.gz)
- [mtk_2.1.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_amd64.tar.gz)
- [mtk_2.1.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_amd64_db2.tar.gz)
- [mtk_2.1.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_linux_arm64.tar.gz)
- [mtk_2.1.6_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_windows_amd64.tar.gz)
- [mtk_2.1.6_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_2.1.6_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.6/mtk_checksums.txt)

### Feat

- 添加参数 [disableSelectPart](../config/mtk-config.md#disableselectpart) 禁用分区表按照分区并行迁移功能
- 添加参数 [templateSeqName](../config/mtk-parameter.md#templateseqname) 支持MySQL到openGauss 序列命名模版
- 迁移MySQL到openGauss增加`show create table`查询
- 支持 openGauss 分区索引
- 添加 `show-drop-index` `show-drop-cons` `mig-select` 子命令

### Fix

- 禁用查询MySQL `INNODB_TABLESTATS` 视图
- 命令行并行和配置文件并行冲突问题
- 迁移 openGauss 查询分区和约束问题
- 迁移 DB2 to MySQL `alter_sequence` 问题
- DB2 timestamp 精度问题
- DB2 驱动数组溢出问题
- 迁移 openGauss index add opClass
- 命令 `mig-tab-post` 没收集统计信息问题
- 迁移 DB2 到 openGauss 语法转换问题

## v2.1.5

2021-12-11

- [mtk_2.1.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_amd64.tar.gz)
- [mtk_2.1.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_amd64_db2.tar.gz)
- [mtk_2.1.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_darwin_arm64.tar.gz)
- [mtk_2.1.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_amd64.tar.gz)
- [mtk_2.1.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_amd64_db2.tar.gz)
- [mtk_2.1.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_linux_arm64.tar.gz)
- [mtk_2.1.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_windows_amd64.tar.gz)
- [mtk_2.1.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_2.1.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.5/mtk_checksums.txt)

### Fix

- 预运行模式下Crash问题
- 迁移 DB2 索引列顺序问题
- 迁移 openGauss 约束问题

## v2.1.4

2021-12-10

- [mtk_2.1.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_amd64.tar.gz)
- [mtk_2.1.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_amd64_db2.tar.gz)
- [mtk_2.1.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_darwin_arm64.tar.gz)
- [mtk_2.1.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_amd64.tar.gz)
- [mtk_2.1.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_amd64_db2.tar.gz)
- [mtk_2.1.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_linux_arm64.tar.gz)
- [mtk_2.1.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_windows_amd64.tar.gz)
- [mtk_2.1.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_2.1.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.4/mtk_checksums.txt)

### Feat

- 支持跳过表某种类型的列
- 支持跳过具体的列
- 支持参数`seqLastNumAddNum`,设置序列最后值增加多少

### Fix

- 解决迁移MySQL 自增列大小查询问题
- 解决迁移Oracle 列默认值问题

## v2.1.3

2021-12-08

- [mtk_2.1.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_amd64.tar.gz)
- [mtk_2.1.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_amd64_db2.tar.gz)
- [mtk_2.1.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_darwin_arm64.tar.gz)
- [mtk_2.1.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_amd64.tar.gz)
- [mtk_2.1.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_amd64_db2.tar.gz)
- [mtk_2.1.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_linux_arm64.tar.gz)
- [mtk_2.1.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_windows_amd64.tar.gz)
- [mtk_2.1.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_2.1.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.3/mtk_checksums.txt)

### Fix

- 迁移MySQL视图Crash问题

## v2.1.2

2021-12-06

- [mtk_2.1.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_amd64.tar.gz)
- [mtk_2.1.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_amd64_db2.tar.gz)
- [mtk_2.1.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_darwin_arm64.tar.gz)
- [mtk_2.1.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_amd64.tar.gz)
- [mtk_2.1.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_amd64_db2.tar.gz)
- [mtk_2.1.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_linux_arm64.tar.gz)
- [mtk_2.1.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_windows_amd64.tar.gz)
- [mtk_2.1.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_2.1.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.2/mtk_checksums.txt)

### Feat

- 支持 MySQL Limit语法转换
- 支持 MySQL 5.1.73

### Fix

- 解决迁移 **date/datetime(n)/timestamp(n)** to openGauss **timestamp(n)** 精度问题
- 解决迁移 MySQL 分区带 `UNIX_TIMESTAMP`函数问题
- 解决迁移 MySQL **timestampdiff/isnull/str_to_date** 函数语法转换问题
- 解决迁移 DB2 函数缺失问题
- 解决迁移 MySQL 列默认值为空字符串问题
- 解决迁移 MySQL `Text`字段空字符串问题
- 解决迁移 MySQL 存储过程hang问题

## v2.1.1

2021-11-29

- [mtk_2.1.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_amd64.tar.gz)
- [mtk_2.1.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_amd64_db2.tar.gz)
- [mtk_2.1.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_darwin_arm64.tar.gz)
- [mtk_2.1.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_amd64.tar.gz)
- [mtk_2.1.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_amd64_db2.tar.gz)
- [mtk_2.1.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_linux_arm64.tar.gz)
- [mtk_2.1.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_windows_amd64.tar.gz)
- [mtk_2.1.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_2.1.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.1/mtk_checksums.txt)

### Feat

- 支持 MySQL 隐藏列(`invisible column`)
- 支持 Oracle `identity column`
- 支持 `XML` 到 MogDB `XML`类型
- 支持 DB2 9.7
- 添加 `init-project`子命令
- 添加参数 [autoAddMySQLAutoIncr](../config/mtk-parameter.md#autoaddmysqlautoincr)

### Fix

- 解决 Windows 下运行MTK问题
- 解决迁移 DB2 存储过程到MogDB部分语法转换问题
- 解决迁移 openGauss `blob` 到 `openGauss` `blob` 问题
- 解决迁移 Postgres 部分问题
- 解决迁移 DB2 `vargraphic` 到 MySQL `varchar`
- 解决迁移Oracle分区表表,分区值包含 `timestamp` 语法问题

## v2.1.0

2021-11-23

- [mtk_2.1.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_amd64.tar.gz)
- [mtk_2.1.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_amd64_db2.tar.gz)
- [mtk_2.1.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_darwin_arm64.tar.gz)
- [mtk_2.1.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_amd64.tar.gz)
- [mtk_2.1.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_amd64_db2.tar.gz)
- [mtk_2.1.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_linux_arm64.tar.gz)
- [mtk_2.1.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_windows_amd64.tar.gz)
- [mtk_2.1.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_2.1.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.1.0/mtk_checksums.txt)

### Feat

- 支持迁移Oracle Spatial to Postgis. See [Spatial](../faq/mtk-oracle-to-openGauss.md#spatial)
- 更新 openGauss 保留关键字列表
- 添加参数[enableOgBlobClob](../config/mtk-parameter.md#enableogblobclob)控制迁移Oracle/DB2/MySQL blob/clob数据到openGauss blob/clob
- 优化终端展示报告

### Fix

- 解决迁移DB2函数`STATIC DISPATCH`语法
- 解决迁移DB2视图`with check option`和`with row movement`语法
- 解决迁移表、列注释包含0x00问题
- 解决查询Oracle包语句问题
