---
title: Release 2.2
summary: Release 2.2
author: Bin.Liu
date: 2022-07-29
---

# Release 2.2 Notes

## v2.2.5

2022-04-10

### MTK

- [mtk_2.2.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_amd64.tar.gz)
- [mtk_2.2.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_amd64_db2.tar.gz)
- [mtk_2.2.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_darwin_arm64.tar.gz)
- [mtk_2.2.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_amd64.tar.gz)
- [mtk_2.2.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_amd64_db2.tar.gz)
- [mtk_2.2.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_linux_arm64.tar.gz)
- [mtk_2.2.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_windows_amd64.tar.gz)
- [mtk_2.2.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_2.2.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.2.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_amd64.tar.gz)
- [mtkd_2.2.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_amd64_db2.tar.gz)
- [mtkd_2.2.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.5/mtkd_2.2.5_linux_arm64.tar.gz)

### Bug Fixes

- **MariaDB:** 删除参数 `explicit_defaults_for_timestamp` 设置
- **MariaDB:** 10.3.9 默认值问题
- **MariaDB:** 查询索引问题
- **Oracle:** 迁移转化 `to_clob` 语法问题
- **Oracle:** 针对Char(1)不进行列长度转换
- **Oracle:** 序列名包含$符号问题
- **Postgres:** 迁移GIS数据问题
- **Postgres:** 查询函数SQL语句问题
- **Postgres:** 升级驱动版本

### Features

- **MTK:** 添加 `SetConnMaxLifetime` 设置并且升级到Go 1.17
- **MTKD:** 添加 `SetConnMaxLifetime` 设置
- **MTKD:** 重构MTKD程序
- **Oracle:** 添加参数 `convertPackageMethod` 支持迁移 Oracle 包到 openGauss 包
- **Oracle:** 转换 `binary_integer` to `integer`
- **Oracle:** 转换 `pls_integer` to `integer`
- **openGauss:** 连接字符串添加参数 `check_function_bodies=off`

### Performance Improvements

- **MTK:** 优化内存使用

## v2.2.4

2022-03-25

- [mtk_2.2.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_amd64.tar.gz)
- [mtk_2.2.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_amd64_db2.tar.gz)
- [mtk_2.2.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_darwin_arm64.tar.gz)
- [mtk_2.2.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_amd64.tar.gz)
- [mtk_2.2.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_amd64_db2.tar.gz)
- [mtk_2.2.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_linux_arm64.tar.gz)
- [mtk_2.2.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_windows_amd64.tar.gz)
- [mtk_2.2.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_2.2.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.4/mtk_checksums.txt)

### Bug Fixes

- **Oracle:** 查询表大小语句缺失查询条件
- **Oracle:** 收集统计信息语法问题
- **openGauss:** 2.1.0 COPY does not support sub transactions or exceptions

### Code Refactoring

- **Table:** 创建表失败错误信息中增加SQL语句

### Features

- **openGauss:** datCompatibility add ora/mysql
- **DB2:** support TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
- **Oracle:** 针对 package 没有 body 进行判断返回错误
- **Oracle:** 移除 procedure `authid current_user` 语法
- **Postgres:** 支持迁移 postgis 到 openGauss

## v2.2.3

2022-03-15

- [mtk_2.2.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_amd64.tar.gz)
- [mtk_2.2.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_amd64_db2.tar.gz)
- [mtk_2.2.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_darwin_arm64.tar.gz)
- [mtk_2.2.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_amd64.tar.gz)
- [mtk_2.2.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_amd64_db2.tar.gz)
- [mtk_2.2.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_linux_arm64.tar.gz)
- [mtk_2.2.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_windows_amd64.tar.gz)
- [mtk_2.2.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_2.2.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.3/mtk_checksums.txt)

### Feat

- 子命令 `show-table-split` 针对 Oracle 数据库添加 `ora_hash`方法
- 添加参数 [charAppendEmptyString](../config/mtk-parameter.md#charappendemptystring) 解决 ORA-29275 错误
- 添加参数 [clientCharset](../config/mtk-config.md#clientcharset)
- 添加参数 [IgErrorData](../config/mtk-parameter.md#igerrordata)

### Fix

- 解决转化 Oracle package 到 openGauss 语法包名没有删除问题

## v2.2.2

2022-03-01

- [mtk_2.2.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_amd64.tar.gz)
- [mtk_2.2.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_amd64_db2.tar.gz)
- [mtk_2.2.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_darwin_arm64.tar.gz)
- [mtk_2.2.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_amd64.tar.gz)
- [mtk_2.2.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_amd64_db2.tar.gz)
- [mtk_2.2.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_linux_arm64.tar.gz)
- [mtk_2.2.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_windows_amd64.tar.gz)
- [mtk_2.2.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_2.2.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.2/mtk_checksums.txt)

### Feat

- 增加检查orafce插件是否安装并跳过部分语法转换
- 针对MogDB 2.1.0 禁用`connect by`语法转换
- 优化迁移Oracle到openGauss/MogDB 约束改名逻辑
- 增加Oracle表数据字符集检查

### Fix

- 代码异味和安全问题
- 迁移 Oracle 到 MySQL 部分问题
- 迁移 Oracle to MogDB 2.1.0 部分语法转换问题
- DB2 clob 长度问题

## v2.2.1

2022-01-20

- [mtk_2.2.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_amd64.tar.gz)
- [mtk_2.2.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_amd64_db2.tar.gz)
- [mtk_2.2.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_darwin_arm64.tar.gz)
- [mtk_2.2.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_amd64.tar.gz)
- [mtk_2.2.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_amd64_db2.tar.gz)
- [mtk_2.2.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_linux_arm64.tar.gz)
- [mtk_2.2.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_windows_amd64.tar.gz)
- [mtk_2.2.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_2.2.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.1/mtk_checksums.txt)

### Feat

- 添加命令行参数`disableFKCons`支持不迁移外键约束
- 添加参数[autoAddMySQLAutoIncrTabList](../config/mtk-parameter.md#autoaddmysqlautoincrtablist)

### Fix

- MySQL `show create table` 语法问题
- MySQL 连接字符串问题
- 迁移到 Oracle 遇到 `ORA-00911` 错误
- 迁移到 Oracle 对象名长度大于30问题
- 迁移到 Oracle 序列 cache 必须大于1的问题
- 参数 `quoteMark` 不生效问题
- 迁移 DB2 到 MySQL 约束问题
- 迁移 DB2 到 MySQL 索引名和约束名长度大于64问题
- 迁移 DB2 `GENERATED IDENTITY` 列到 MySQL `auto_incr` 主键列顺序问题
- 迁移 SqlServer 到 MySQL 列类型转换问题

## v2.2.0

2022-01-11

- [mtk_2.2.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_amd64.tar.gz)
- [mtk_2.2.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_amd64_db2.tar.gz)
- [mtk_2.2.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_darwin_arm64.tar.gz)
- [mtk_2.2.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_amd64.tar.gz)
- [mtk_2.2.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_amd64_db2.tar.gz)
- [mtk_2.2.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_linux_arm64.tar.gz)
- [mtk_2.2.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_windows_amd64.tar.gz)
- [mtk_2.2.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_2.2.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.2.0/mtk_checksums.txt)

### Feat

- 增加表数据检查功能 [check-table-data](../commands/mtk_check-table-data.md)
- 支持MogDB 子分区

### Fix

- 迁移 openGauss to openGauss 列默认空问题
- 迁移 DB2 to openGauss 视图语法转换问题
- 迁移 DB2 to openGauss 存储过程转换 `end p1` 问题
- 迁移 DB2 to openGauss 存储过程转换缺少`package`属性问题
- 迁移 MySQL to openGauss `information_schema.tables` 字段存在null的问题
