---
title: Release 2.4
summary: Release 2.4
author: Bin.Liu
date: 2022-07-29
---

# Release 2.4 Notes

## v2.4.4

2022-08-01

### MTK

- [mtk_2.4.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_amd64.tar.gz)
- [mtk_2.4.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_amd64_db2.tar.gz)
- [mtk_2.4.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_darwin_arm64.tar.gz)
- [mtk_2.4.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_amd64.tar.gz)
- [mtk_2.4.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_amd64_db2.tar.gz)
- [mtk_2.4.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_linux_arm64.tar.gz)
- [mtk_2.4.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_windows_amd64.tar.gz)
- [mtk_2.4.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_2.4.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_amd64.tar.gz)
- [mtkd_2.4.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_amd64_db2.tar.gz)
- [mtkd_2.4.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.4/mtkd_2.4.4_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** 生成DDL脚本问题
- **MySQL:** 迁移 MySQL double 到 openGauss 问题
- **config:** 移除配置文件默认转为大写逻辑
- **openGauss:** 创建视图没有提交问题

### Code Refactoring

- **MySQL:** 优化DB2 自增列迁移到 MySQL自增列逻辑
- **Oracle:** 优化查询自定义类型SQL语句

### Features

- **MySQL:** 支持MySQL Bit类型迁移到MySQL bit类型
- **MySQL:** 支持生成MySQL外键指向索引语句
- **Oracle:** 支持 Oracle `type body` 改写
- **mtkd:** 增加 preCheck API接口
- **mtkd:** 表`task_run`增加错误原因字段
- **openGauss:** 支持自增列增加 `ALTER  SEQUENCE seq_name OWNED BY table_name.column_name` 语句

## v2.4.3

2022-07-18

### MTK

- [mtk_2.4.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_amd64.tar.gz)
- [mtk_2.4.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_amd64_db2.tar.gz)
- [mtk_2.4.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_darwin_arm64.tar.gz)
- [mtk_2.4.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_amd64.tar.gz)
- [mtk_2.4.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_amd64_db2.tar.gz)
- [mtk_2.4.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_linux_arm64.tar.gz)
- [mtk_2.4.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_windows_amd64.tar.gz)
- [mtk_2.4.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_2.4.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_amd64.tar.gz)
- [mtkd_2.4.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_amd64_db2.tar.gz)
- [mtkd_2.4.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.3/mtkd_2.4.3_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** 改写 Oracle Package 中序列 `.nextval/currval` 语法错误问题
- **Oracle:** 改写 Oracle type 到 openGauss `varchar2 (4000)` 问题
- **Oracle:** 改写 Oracle `decode` 包含注释语法错误问题
- **Oracle:** 改写 Oracle 触发器 `into:new.id` to openGauss 语法错误问题
- **Oracle:** 转换 Oracle `number(*,2)` to openGauss `NUMERIC(2,2)` 错误问题
- **openGauss:** Create view 没有设置 `search_path` 问题
- **openGauss:** 迁移Package自动生成同义词问题,忽略迁移此类同义词
- **openGauss:** 表列重复问题

### Code Refactoring

- **Oracle:** Rename postgres to postgresql
- **file:** 生成SQL脚本
- **openGauss:** 解析数据库版本
- **plsql:** 转换Type语法问题

### Features

- **MySQL:** 支持转换 MySQL 双引号常量 到 openGauss 单引号常量
- **MySQL:** 支持转换 MySQL `on update current_timestamp` 到 MogDB 3.0 B兼容模式
- **Oracle:** openGauss/MogDB 3.0.0 之后不在转换 Oracle `decode` 函数
- **Oracle:** 支持 Oracle 函数 out和return 共存改写
- **Oracle:** 支持改写部分 存储过程/包/函数里 `new type` 语法
- **Oracle:** 改写 Oracle `table(` to openGauss `UNNEST(` 语法
- **Oracle:** openGauss/MogDB 3.0.0 之后不在转换 包头里 `:=` to `default`
- **Oracle:** 改写 Oracle `extend` function 为注释
- **Oracle:** 添加迁移用户权限检查
- **mtk:** 增加源无效对象失败统计
- **mtk:** 支持加密密码并提供生成加密密码子命令[encrypt](../commands/mtk_encrypt.md)
- **openGauss:** 修改 openGauss 默认版本为 3.0.0
- **openGauss:** 支持迁移 MogDB  列属性 `on update current_timestamp`
- **openGauss:** 支持迁移 MogDB/openGauss `create type as table of`
- **openGauss:** 添加关键字 `VERIFY`

## v2.4.2

2022-07-04

### MTK

- [mtk_2.4.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_amd64.tar.gz)
- [mtk_2.4.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_amd64_db2.tar.gz)
- [mtk_2.4.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_darwin_arm64.tar.gz)
- [mtk_2.4.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_amd64.tar.gz)
- [mtk_2.4.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_amd64_db2.tar.gz)
- [mtk_2.4.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_linux_arm64.tar.gz)
- [mtk_2.4.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_windows_amd64.tar.gz)
- [mtk_2.4.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_2.4.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_amd64.tar.gz)
- [mtkd_2.4.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_amd64_db2.tar.gz)
- [mtkd_2.4.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.2/mtkd_2.4.2_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** 分批次提交相关问题
- **Oracle:** 改写float(64)语法问题
- **Oracle:** 改写 Oracle Package 到 openGauss package 一些问题
- **Oracle:** 改写 Oracle Type 到 openGauss Type 一些问题
- **Oracle:** 改写 Oracle Package 没有包体限制

### Features

- **MySQL:** add long_query_time default connect parameter. default 36000
- **Oracle:** support oracle column udt type
- **Oracle:** support table of type
- **Oracle:** rewrite Oracle `PIPELINED` to PostgreSQL `setof`

## v2.4.1

2022-06-24

### MTK

- [mtk_2.4.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_amd64.tar.gz)
- [mtk_2.4.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_amd64_db2.tar.gz)
- [mtk_2.4.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_darwin_arm64.tar.gz)
- [mtk_2.4.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_amd64.tar.gz)
- [mtk_2.4.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_amd64_db2.tar.gz)
- [mtk_2.4.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_linux_arm64.tar.gz)
- [mtk_2.4.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_windows_amd64.tar.gz)
- [mtk_2.4.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_2.4.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.4.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_amd64.tar.gz)
- [mtkd_2.4.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_amd64_db2.tar.gz)
- [mtkd_2.4.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.4.1/mtkd_2.4.1_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** 查询丢失主键列信息
- **db2:** 约束重复问题
- **mtkd:** task_result_details sequence issue

### Code Refactoring

- **db2:** 查询索引方式

### Features

- **MTK:** 支持Informix迁移到openGauss/MogDB
- **db2:** 增加对象ID查询
- **mtk:** 支持 MySQL/openGasuss/MogDB/PostgreSQL 分批次提交

### Performance Improvements

- **db2:** Optimize Convert float64 to string
- **db2:** Optimize Convert TimeDuration To OraInterval
