---
title: 发布记录
summary: 发布记录
author: Zhang Cuiping
date: 2021-09-17
---

# Release 2.5 Notes

- mtk  命令行工具
- mtkd 封装mtk作为后端服务程序,针对特定场景

## v2.5.2

2022-08-30

### MTK

- [mtk_2.5.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_windows_amd64.tar.gz)
- [mtk_2.5.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_arm64.tar.gz)
- [mtk_2.5.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_amd64_db2.tar.gz)
- [mtk_2.5.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_amd64_db2.tar.gz)
- [mtk_2.5.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_windows_amd64_db2.tar.gz)
- [mtk_2.5.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_arm64.tar.gz)
- [mtk_2.5.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_darwin_amd64.tar.gz)
- [mtk_2.5.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_2.5.2_linux_amd64.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_amd64_db2.tar.gz)
- [mtkd_2.5.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_arm64.tar.gz)
- [mtkd_2.5.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.2/mtkd_2.5.2_linux_amd64.tar.gz)

2022-08-30

### Bug Fixes

- **DB2:** `syscat.Columns` 视图中带表名后缀带空格问题
- **MySQL:** 移除 MySQL 作为源库时连接参数 `explicit_defaults_for_timestamp`
- **MySQL:** 迁移 `id BIGINT(20) unsigned AUTO_INCREMENT` 为 序列问题
- **MySQL:** 迁移 `id INT(10) unsigned AUTO_INCREMENT` 为 序列问题
- **MySQL:** 查询约束SQL添加 `binary` 关键字
- **MySQL:** 迁移 MySQL `Time` 类型列默认值问题
- **openGauss:** 当分区名为数字时查询分区语法问题

### Code Refactoring

- **MTKD:** 更新 `supportOperate` API 返回 数据结构
- **MTKD:** 更新 `convertSQLSupportType` API

### Features

- **MTK:** 添加参数`charLengthChangeExclude`
- **MTK:** 查询Oracle NLS相关参数并保存到`otherParams`
- **MTK:** 添加 mig-selects 子命令
- **MTKD:** 添加任务 `getSchemas` API
- **MTKD:**  添加 operate API
- **MTKD:** 添加合并任务运行记录功能
- **SQLServer:** 支持部分函数/过程语法转换为openGauss（测试版）
- **SQLServer:** 支持查询函数/过程
- **SQLServer:** 支持查询触发器

## v2.5.1

2022-08-16

### MTK

- [mtk_2.5.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_amd64.tar.gz)
- [mtk_2.5.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_amd64_db2.tar.gz)
- [mtk_2.5.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_darwin_arm64.tar.gz)
- [mtk_2.5.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_amd64.tar.gz)
- [mtk_2.5.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_amd64_db2.tar.gz)
- [mtk_2.5.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_linux_arm64.tar.gz)
- [mtk_2.5.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_windows_amd64.tar.gz)
- [mtk_2.5.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_2.5.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_amd64.tar.gz)
- [mtkd_2.5.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_amd64_db2.tar.gz)
- [mtkd_2.5.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.1/mtkd_2.5.1_linux_arm64.tar.gz)

2022-08-16

### Bug Fixes

- **DB2:** 改写 `insert into table_name values` 错误问题
- **MTKD:** typo SUCCESS -> Succeed
- **Oracle:** 查询 Oracle Package 问题

### Code Refactoring

- **MTKD:** 优化 DB2 查询表信息语句

### Features

- **MTK:** 添加 `convertOracleIntegerToNumeric` 参数
- **MTK:** `excludeTable` 增加正则表达式支持
- **MTK:** `mig-tab-data`/`sync-table-data` 添加 `tableSplit` 参数
- **MTK:** 添加 `convert-plsql`/`report-to-sql` 自命令
- **MTKD:** 添加 `errorDataFile` api

## v2.5.0

2022-08-14

### MTK

- [mtk_2.5.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_amd64.tar.gz)
- [mtk_2.5.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_amd64_db2.tar.gz)
- [mtk_2.5.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_darwin_arm64.tar.gz)
- [mtk_2.5.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_amd64.tar.gz)
- [mtk_2.5.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_amd64_db2.tar.gz)
- [mtk_2.5.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_linux_arm64.tar.gz)
- [mtk_2.5.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_windows_amd64.tar.gz)
- [mtk_2.5.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_2.5.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.5.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_amd64.tar.gz)
- [mtkd_2.5.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_amd64_db2.tar.gz)
- [mtkd_2.5.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.5.0/mtkd_2.5.0_linux_arm64.tar.gz)

### Bug Fixes

- **DB2:** 迁移 DB2 视图语法转换相关问题
- **MTK:** 解决 ColumnType 空指针问题
- **MTK:** 无效对象增加无效信息提示
- **MySQL:** `DEFAULT_GENERATED ON UPDATE CURRENT_TIMESTAMP` 语法问题
- **Oracle:** 忽略 `IOT_OVERFLOW` 表
- **Oracle:** 语法转换 `AQ$_CBSD_CHREXX` 问题
- **Oracle:** VARRAY Type and type varchar2(10 char)
- **Oracle:** 改成 `PIPE ROW` to `return next ();`
- **Oracle:** 改成 Oracle `SYS_EXTRACT_UTC` 问题
- **openGauss:** left join 别名问题 and 判断存储过程包含存储过程错误问题

### Code Refactoring

- **MTK:** 增加字符集定义
- **Oracle:** `authid current_user` 语法适配
- **RDBMS:** SetConnMaxLifetime to SetConnMaxIdleTime

### Features

- **MTK:** 添加Excel报告
- **MTK:** 增加 `convert-plsql` 子命令
- **MTK:** `report-to-sql` 子命令增加转换HTML报告为Excel报告功能
- **MTKD:** 添加 报告和日志下载、转换SQL、系统信息API
- **Oracle:** 查询 view/function/procedure 对象ID
- **Oracle:** 改写 Oracle `commit work` 为 openGauss `commit`
- **openGauss:** 默认迁移 Oracle Package to openGauss 3.0 Package

### Performance Improvements

- **Oracle:** 优化查询视图性能
