---
title: Release 2.0
summary: Release 2.0
author: Bin.Liu
date: 2022-07-29
---

# Release 2.0 Notes

## v2.0.0

2021-11-15

- [mtk_2.0.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_amd64.tar.gz)
- [mtk_2.0.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_amd64_db2.tar.gz)
- [mtk_2.0.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_darwin_arm64.tar.gz)
- [mtk_2.0.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_amd64.tar.gz)
- [mtk_2.0.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_amd64_db2.tar.gz)
- [mtk_2.0.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_linux_arm64.tar.gz)
- [mtk_2.0.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_windows_amd64.tar.gz)
- [mtk_2.0.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_2.0.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.0.0/mtk_checksums.txt)

### Feat

- 支持DB2迁移到DB2

### Fix

- 解决迁移DB2 `timestamp`列默认值到openGauss 格式问题
- 解决迁移Oracle 函数索引到 openGauss 问题
- 解决迁移MySQL `date_format`函数迁移到openGauss格式转换问题
