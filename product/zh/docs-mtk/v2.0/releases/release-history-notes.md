---
title: 历史发布记录
summary: 历史发布记录
author: Zhang Cuiping
date: 2021-09-17
---

# 历史发布记录

- mtk  命令行工具
- mtkd 封装mtk作为后端服务程序,针对特定场景

## v0.0.41

2021-11-06

### Fix

- 解决迁移MySQL`datatime`类型并且数据中存在`0000-00-00 00:00:00`报告没有提示错误问题

## v0.0.40

2021-11-05

### Feat

- 支持查询迁移DB2 `server`类型
- 支持查询迁移DB2 `wrapper`类型
- 支持查询迁移Oracle物化视图
- 支持忽略大小写查询迁移对象
- 配置文件参数`remapSchema`、`remapTable`、`remapTablespace`支持忽略大小写
- 添加测试迁移功能. 查看参数`test`和`limit`

### Fix

- 解决迁移Oracle表没有忽略物化视图问题
- 解决迁移DB2指定表时没有迁移序列问题
- 解决迁移MySQL视图语句包含IF表达式程序无法结束问题
- 解决导入数据时没有异常处理导致程序崩溃问题

## v0.0.39

2021-10-25

### Feat

- 支持DB2压缩表估算真实大小
- 支持文本报告
- 支持自动转换分区表`minvalue`分区为`maxvalue`
- 支持openGauss package 迁移

### Fix

- 解决迁移Oracle `varchar(10 char)` 长度问题
- 解决迁移Oracle 视图 `with read only` 语法问题
- 解决db2查看表大小慢问题.
- 解决db2字符集乱码问题,增加 `DB2CODEPAGE` 检查
- 解决索引约束并发创建问题
- 解决迁移db2函数存储过程 `truncate`和`update set` 语法问题
- 解决迁移openGauss 函数存储过程一些问题

## v0.0.38

2021-10-08

### Feat

- 导出成文件增加文件序号
- 导出成文件生成MySQL语句`LOAD DATA INFILE XX INTO TABLE`
- 支持DB2存储过程迁移到openGauss. (beta版本)
- 支持DB2函数迁移到openGauss. (beta版本)
- 更新授权许可机制
- 支持openGauss虚拟列
- 支持迁移postgres/openGauss `rule` 到 openGauss/postgres
- 支持迁移postgres/openGauss `base type` 到 openGauss/postgres

### Fix

- 迁移 db2 `interval表达式` 到 openGauss 问题
- 迁移 db2 `values表达式` 到 openGauss 问题. ignore insert into xx values xx
- 迁移 mysql 自增列到 openGauss 序列+1问题
- 迁移 openGauss 约束问题
- 迁移 openGauss 到 openGauss 部分问题
- 迁移 openGauss 查询序列慢问题
- 导出为csv时格式问题
- 迁移 db2 `varchar`/`timestamp` 范围分区到 mysql 问题

## v0.0.37

2021-09-24

### Feat

- 支持迁移PostgreSQL、db2、openGauss物化视图到openGauss
- 支持DB2查看存储过程、函数、存储语法
- 支持迁移MySQL范围分区包含 `to_days`/`year`/`unix_timestamp` 到 openGauss
- 支持PostgreSQL/openGauss `type`迁移到openGauss
- 支持PostgreSQL/openGauss `domain` 迁移到openGauss

### Fix

- 解决迁移到openGauss数据库兼容模式B没有判断
- 解决DB2自动虚拟列永远为0的场景，改写为`default 0`
- 解决序列最小值和最大值相等问题
- 解决迁移DB2到openGauss分区相关问题
- 解决MySQL查询语句大小写问题
- 解决迁移PostgreSQL到openGauss部分问题

### Perf

- 优化DB2查询`ADMINTABINFO`巨慢问题

## v0.0.36

2021-09-17

### Feat

- 支持多语言帮助说明
- 支持openGauss函数、视图、存储过程、触发器迁移
- 支持错误数据记录
- 添加参数`autoAddMaxvaluePart`，支持迁移分区表到`openGauss`时自动添加`maxvalue`分区
- 命令行添加参数支持导出成文件
- 支持迁移到PostgreSQL

### Fix

- 解决MySQL 8.0.23 查询视图问题
- 解决判断单表并行完成问题
- 解决迁移DB2到openGauss约束丢失问题
- 解决迁移DB2到openGauss列默认值丢失问题

## v0.0.35

2021-09-13

### Fix

- 增加参数`ignoreTabPartition`，支持迁移到目标端数据库忽略分区语法，现在只支持迁移到MySQL
- 迁移DB2、Oracle列类型`timestamp`到MySQL `datetime(6)`

## v0.0.34

2021-09-03

### Feat

- 解决索引、约束名超过64位
- 优化迁移报告
- 适配 MariaDB 5.5.62

### Fix

- 解决部分迁移Oracle 视图、函数到openGauss语法问题

- 解决`remapSchema`失效问题

## v0.0.33

2021-08-30

### Feat

- 迁移表数据增加表存在检查
- 增加参数`excludeSysTable`支持忽略系统表. 用户可自定义参数,不配置默认使用系统配置
- 支持迁移完数据后立即创建索引和约束
- 增加收集表统计信息
- 支持MySQL列类型`set` `enum`迁移到opengauss
- 增加参数`EnableSyncTabTbsPro` `RemapTbsSpace`支持表空间名转换
- DB2表创建语法增加 `compression` and `organize by`语法
- DB2连接字符串增加 `ClientApplName`,`ProgramName`
- MySQL查询database忽略大小写

### Fix

- 解决表名、列名特殊字符匹配
- 迁移到openGauss替换`dual` 为 `sys_dummy`
- 解决中文字符乱码问题
- 解决Oracle建表语句没有双引号问题
- 解决迁移到openGauss创建view/function/procedure/trigger `search_path`不对问题
- 解决表名、约束名、索引名一致问题
- 解决表已存在没有警告信息问题
- 解决MySQL 8.0视图`information_schema.TABLES` 列`auto-increment`数据不准问题关联视图`information_schema.INNODB_TABLESTATS`查询

## v0.0.32

2021-08-25

### Fix

- eslint安全问题

## v0.0.31

2021-08-25

### Feat

- 支持迁移DB2虚拟自增列到mysql自增列
- Oracle自动查询字符集并设置NLS_LANG环境变量
- 日志文件优先使用logfile参数,如果没有使用reportFile参数

### Fix

- 表数据迁移预估时间问题
- DB2列类型VarGraphic问题
- 升级axios
- 内存泄漏问题
- html报告样式
- 迁移db2到文件og格式问题.
- 安全问题

## v0.0.30

2021-08-18

### Fix

- 约束同名问题
- DB2 timestamp列默认直格式问题. `2019-01-10-10.52.08.554035`
- 迁移Oracle到Mysql一些问题
- 迁移Oracle到Mysql索引带排序问题.

## v0.0.29

2021-08-13

### Feat

- 支持迁移Oracle包到openGauss. (alpha beta version)
- 支持 mig-tab-pre/data/post/other 子命令
- 支持迁移Oracle触发器到 openGauss

### Fix

- 转换 Oracle trim 到 openGauss trim(both xx)
- 转换 Oracle dbms_lock.sleep 到 openGauss pg_sleep
- 迁移 DB2到MySQL insert语法问题
- 迁移 DB2分区表到MySQL问题
- MySQL创建约束不支持Using index,改成正常语法
- 迁移DB2序列到MySQL状态问题
- 生成report慢问题
- 解决转换存储过程一些问题

## v0.0.28

2021-08-09

### Feat

- 支持转换DB2一部分时间函数到openGauss
- 添加 gen auto complete 子命令
- 支持 connect by 重写为CTE语法 (alpha beta version)
- 支持 DB2 迁移到 MySQL
- copy 支持 batchSize

### Fix

- split table add abs function because mod function will appear negative.
- 解决函数和存储过程迁移到openGauss缺失重载参数package
- MySQL自增列迁移到openGauss序列, 序列Schema没有进行remap
- MySQL自增列迁移到openGauss序列cache改成1
- MySQL设置参数放在timestamp列自动生成 on update 语法
- 解决MySQL连接字符串时区问题. 默认本机时区

## v0.0.27

2021-08-02

### Feat

- 支持 Oracle同义词、 DB2 alias to openGauss 同义词
- 自动跳过DB2 nickname view
- 支持改写MySQL游标语法 `xxx cursor for`到 openGauss `cursor xxx is`
- 支持改写MySQL `join` 语法
- 支持改写Oracle `NLSSORT` 到 openGauss `collate`
- 支持改写Oracle `months_between`函数
- 支持Oracle/MySQL函数迁移到openGauss
- 支持改写Oracle `rownum` 到 openGauss `limit`
- 支持改写Oracle外连接到正常语法
- 支持迁移Oracle存储过程到openGauss. (alpha beta version)

### Fix

- 解决改写Oracle `add_years` 函数问题
- 自动跳过DB2函数索引创建到视图
- MySQL增加查询 `sql_mode` 参数
- 增加 golang panic 捕获
- 增加缺失到UTF8字符串定义
- 解决Oracle函数到openGauss一些问题
- 解决MySQL 8.0 视图查询问题
- 解决MySQL 列默认值`000-00-00 00:00:00`迁移到 openGauss`1970-01-01`
- 忽略MySQL 列前缀索引语法. (custom_condition(100)
- 改写 MySQL JOIN with WHERE clause instead of ON
- 解决DB2创建主键唯一键约束, 约束名和索引名不一致问题

## v0.0.26

2021-07-23

### Feat

- 支持Oracle函数迁移到openGauss. (alpha beta version)
- 支持Oracle Type 迁移到openGauss
- 移除DB2 列默认值 `"SYSIBM"."BLOB"('')`
- 支持MySQL `bit` 列类型

### Fix

- 解决配置文件错误但是没有提示信息问题
- 解决迁移MySQL约束同名问题
- 解决迁移MySQL text 字段 遇到 pq: invalid byte sequence for encoding "UTF8": 0x00 问题
- 解决参数 virtualColConv 支持忽略大小写
- 解决MySQL bigint unsigned auto incr issue
- 解决MySQL 8.0 函数存储过程查询问题
- 解决表名带空格问题
- 解决迁移DB2到openGauss约束问题,先创建唯一索引在创建唯一约束
- 解决MySQL索引列`custom_condition(1000)` to openGauss `substring(custom_condition,0,1000)`
- 解决DB2函数索引问题
- 解决MySQL `int unsigned` to bigint
- 解决MySQL 8.0 虚拟列问题

## v0.0.25

2021-07-19

### Feat

- add remove/restore comment code
- add interval type regular matching
- 增加参数`virtualColConv`允许用户自定义虚拟列表达式转换
- 添加修改序列功能,迁移序列最后值到目标库
- 自动跳过DB2虚拟列check约束
- 添加`gen-config`子命令
- 自动跳过DB2 nickname table index
- 报告增加开始时间、结束时间排序问题

### Fix

- 解决代码异味问题
- 解决copy语法缺失DB2自增ID虚拟列问题
- 解决Oracle 11g deferred_segment_creation. Change the show table size to left join

## v0.0.24

2021-07-16

### Feat

- 支持MySQL/DB2表拆分功能
- 支持openGauss虚拟列语法
- 支持迁移sqlServer视图到openGauss/MogDB
- 支持迁移sqlServer索引到openGauss/MogDB
- 支持迁移sqlServer约束到openGauss/MogDB
- 支持迁移sqlServer schema  tableData 到openGauss/MogDB
- 支持PostgreSQL二级分区创建语法
- 支持Oracle select hint parallel语法
- 命令`show-schema`增加展示`schema`大小
- 添加命令`check-config` 检查配置文件是否正常
- 添加命令,允许用户自己控制迁移类型
- 添加命令`showSchema`,`showTopTableSize`,`showTopTableSplit`

### Fix

- 解决迁移到PostgreSQL多列分区列问题
- 解决列名数字开头问题

## v0.0.23

2021-07-07

### Feat

- 支持迁移sqlserver identity column
- 支持迁移sqlserver table query
- 支持迁移sqlserver sequence 到 openGauss/mogdb

### Fix

- 解决DB2查询分区排序问题

## v0.0.22

2021-07-02

### Fix

- 解决MySQL列到openGauss列长度不够问题
- 解决程序没有正常退出问题
- 解决导出成文件列长没有自动增加问题
- 解决windows平台下报告没有生成问题
- 解决select语法没有增加双引号

## v0.0.21

2021-06-30

### Feat

- Oracle增加触发器、函数、存储过程、包创建语法

### Fix

- Oracle移除视图过滤条件

### Perf

- Optimize concurrent lock
- Optimize the performance of table synchronization data

## v0.0.20

2021-06-22

### Fix

- 解决DB2`xml`列类型包含XML版本问题,连接字符串添加`XMLDeclaration=1`
- 解决DB2`dbClob`列类型迁移到openGauss text列类型

## v0.0.19

2021-06-18

### Fix

- 解决Oracle范围分区包含多列问题
- 解决Oracle/DB2/MySQL 空字符串和null问题.

## v0.0.18

2021-06-16

### Feat

- 添加license功能
- 支持非utf8字符集迁移到openGauss/MogDB UTF8 字符集列长自动增加
- 支持预估Oracle/MySQL迁移时间
- 支持迁移达梦到openGauss
- 添加`colKeyWords`、`objKeyWords` 配置参数
- 添加 DB2 trigger/function/procedure查询统计
- 添加 Oracle Procedure/Function/Trigger/Type查询统计
- 添加 Oracle Procedure/Function/Package/Synonym/DBLink/Queue Type查询统计
- 支持迁移到openGauss 2.0.0
- openGauss添加获取数据库兼容性功能

### Fix

- DB2 GBK CHARSET Database char include chinese error slice bounds out of range
- Oracle '' = null to migrate openGauss is not null issue
- MySQL time issue. MySQL year to openGauss int
- Oracle/MySQL 0x00 issue
- DB2/MySQL blob/clob issue
- Oracle clob blob data issue
- Modify query Oracle dblink view. replace all_db_links to dba_db_links
- add Oracle long raw column type
- openGauss add Col Key Word stream. convert to "STREAM"
- missing column length from query openGauss/PostgreSQL database.
- migrate Oracle missing table
- table ddl comp CHAR = CHARACTER/VARCHAR = CHARACTER VARYING
