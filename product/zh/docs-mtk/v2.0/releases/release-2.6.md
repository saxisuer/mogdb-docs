---
title: 发布记录
summary: 发布记录
author: Zhang Cuiping
date: 2021-09-17
---

# Release 2.6 Notes

- mtk  命令行工具
- mtkd 封装mtk作为后端服务程序,针对特定场景

## v2.6.6

2022-10-20

### MTK

- [mtk_2.6.6_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_windows_amd64.tar.gz)
- [mtk_2.6.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_arm64.tar.gz)
- [mtk_2.6.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_amd64.tar.gz)
- [mtk_2.6.6_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_amd64.tar.gz)
- [mtk_2.6.6_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_arm64.tar.gz)
- [mtk_2.6.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_linux_amd64_db2.tar.gz)
- [mtk_2.6.6_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_darwin_amd64_db2.tar.gz)
- [mtk_2.6.6_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_2.6.6_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.6_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_amd64.tar.gz)
- [mtkd_2.6.6_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_amd64_db2.tar.gz)
- [mtkd_2.6.6_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.6/mtkd_2.6.6_linux_arm64.tar.gz)

### Bug Fixes

- **Oracle:** Disable the self-calculating lob field fetchSize
- **Oracle:** clob/blob use godror driver

## v2.6.5

2022-10-17

### MTK

- [mtk_2.6.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_windows_amd64.tar.gz)
- [mtk_2.6.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_arm64.tar.gz)
- [mtk_2.6.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_amd64.tar.gz)
- [mtk_2.6.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_amd64.tar.gz)
- [mtk_2.6.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_arm64.tar.gz)
- [mtk_2.6.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_linux_amd64_db2.tar.gz)
- [mtk_2.6.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_darwin_amd64_db2.tar.gz)
- [mtk_2.6.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_2.6.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_amd64.tar.gz)
- [mtkd_2.6.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_amd64_db2.tar.gz)
- [mtkd_2.6.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.5/mtkd_2.6.5_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** 表注释语句失败修改表状态为警告
- **MTK:** 修复导出成文件时部分场景未退出问题
- **MTKD:** 完成时间为负数问题
- **MTKD:** 数据库丢失数据问题
- **MTKD:** 返回查询Schema错误信息不准确问题
- **Oracle:** 改写包时包头结束不存在包名问题

### Features

- **MTK:** 子命令添加分组
- **MTK:** 自动确定字符集并设置`clientCharset`参数
- **Oracle:** 支持Public同义词
- **openGauss:** 禁用自动创建`orafce`插件

### Performance Improvements

- 优化字符串家处理性能
- **openGauss:** 优化插入性能

## v2.6.4

2022-10-05

### MTK

- [mtk_2.6.4_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_windows_amd64.tar.gz)
- [mtk_2.6.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_arm64.tar.gz)
- [mtk_2.6.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_amd64.tar.gz)
- [mtk_2.6.4_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_amd64.tar.gz)
- [mtk_2.6.4_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_arm64.tar.gz)
- [mtk_2.6.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_linux_amd64_db2.tar.gz)
- [mtk_2.6.4_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_darwin_amd64_db2.tar.gz)
- [mtk_2.6.4_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_2.6.4_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.4_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_amd64.tar.gz)
- [mtkd_2.6.4_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_amd64_db2.tar.gz)
- [mtkd_2.6.4_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.4/mtkd_2.6.4_linux_arm64.tar.gz)

### Bug Fixes

- **PostgreSQL:** bool type issue
- **PostgreSQL:** Mig Oracle `minus` to `EXCEPT`
- **PostgreSQL:** Mig Oracle function cursor issue

### Features

- **MTK:** Update the license version and be compatible with the old version
- **MTKD:** add tableOption `COLLATE=xxx_bin` for MySQL database
- **MTKD:** swagger add https schema
- **Oracle:** Support Oracle Flashback Queries.

## v2.6.3

2022-09-22

### MTK

- [mtk_2.6.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_windows_amd64.tar.gz)
- [mtk_2.6.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_arm64.tar.gz)
- [mtk_2.6.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_amd64.tar.gz)
- [mtk_2.6.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_amd64.tar.gz)
- [mtk_2.6.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_arm64.tar.gz)
- [mtk_2.6.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_linux_amd64_db2.tar.gz)
- [mtk_2.6.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_darwin_amd64_db2.tar.gz)
- [mtk_2.6.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_2.6.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_amd64.tar.gz)
- [mtkd_2.6.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_amd64_db2.tar.gz)
- [mtkd_2.6.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.3/mtkd_2.6.3_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** 导出成文件错误未退出问题
- **MTK:** 创建外键约束问题
- **Oracle:** 迁移 Spatial数据到文件问题
- **Oracle:** 改写Oracle 触发器 `after statement` 问题

### Performance Improvements

- **MTK:** 优化迁移到MySQL性能

## v2.6.2

2022-09-20

### MTK

- [mtk_2.6.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_windows_amd64.tar.gz)
- [mtk_2.6.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_arm64.tar.gz)
- [mtk_2.6.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_amd64.tar.gz)
- [mtk_2.6.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_amd64.tar.gz)
- [mtk_2.6.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_arm64.tar.gz)
- [mtk_2.6.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_linux_amd64_db2.tar.gz)
- [mtk_2.6.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_darwin_amd64_db2.tar.gz)
- [mtk_2.6.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_2.6.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_amd64.tar.gz)
- [mtkd_2.6.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_amd64_db2.tar.gz)
- [mtkd_2.6.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.2/mtkd_2.6.2_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** 导出文件时配置文件大小生成文件大小不匹配问题
- **MySQL:** 迁移 MySQL Varchar 空值问题
- **Oracle:** 迁移 Oracle 触发器 `IF:NEW.ID` 到 openGauss 问题
- **Oracle:** 改写函数参数存在注释问题
- **Oracle:** Spatial Rectangle issue
- **Oracle:** 迁移 Oracle `BINARY_DOUBLE` and `UDT` 到 openGauss 问题

### Performance Improvements

- **MTK:** Optimized record structure

## 2.6.1

2022-09-19

### MTK

- [mtk_2.6.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_windows_amd64.tar.gz)
- [mtk_2.6.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_arm64.tar.gz)
- [mtk_2.6.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_amd64.tar.gz)
- [mtk_2.6.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_amd64.tar.gz)
- [mtk_2.6.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_arm64.tar.gz)
- [mtk_2.6.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_linux_amd64_db2.tar.gz)
- [mtk_2.6.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_darwin_amd64_db2.tar.gz)
- [mtk_2.6.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_2.6.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_amd64.tar.gz)
- [mtkd_2.6.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_amd64_db2.tar.gz)
- [mtkd_2.6.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.1/mtkd_2.6.1_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** Mig MySQL Blob Issue
- **Oracle:** Mig MySQL Blob Issue
- **MTK** `mig-select` create table failed exit issue

### Performance Improvements

- Optimize Performance

## 2.6.0

2022-09-13

### MTK

- [mtk_2.6.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_windows_amd64.tar.gz)
- [mtk_2.6.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_arm64.tar.gz)
- [mtk_2.6.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_amd64.tar.gz)
- [mtk_2.6.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_amd64.tar.gz)
- [mtk_2.6.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_arm64.tar.gz)
- [mtk_2.6.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_linux_amd64_db2.tar.gz)
- [mtk_2.6.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_darwin_amd64_db2.tar.gz)
- [mtk_2.6.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_2.6.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.6.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_amd64.tar.gz)
- [mtkd_2.6.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_amd64_db2.tar.gz)
- [mtkd_2.6.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.6.0/mtkd_2.6.0_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** 解决部分空指针问题
- **MTK:** 命令行参数优先级问题
- **MTKD:** 命令行转换SQL语句问题
- **MySQL:** MySQL查询视图定义问题
- **MySQL:** 迁移 MySQL `set` 类型 为 PostgreSQL/openGauss/MogDB `Varchar` 类型
- **Oracle:** 迁移 Oracle `Spatial` 类型 To PostgreSQL/openGauss/MogDB 问题
- **PostgreSQL:** 导出 Oracle 到 PostgreSQL/openGauss/MogDB CSV 文件问题

### Code Refactoring

- **MTK:** Optimize the openGauss/PostgreSQL driver
- **PostgreSQL:** 入库相关逻辑

### Features

- **MTK:** `show-table` 添加 `detail` 参数
- **MTK:** 添加 `self upgrade` 命令
- **MTK:** 更新示例配置文件
- **MTK:** `convert-plsql` 添加 `output` 参数
- **MTKD:** 添加 `self upgrade` 命令
- **MySQL:** 支持 MySQL Spatial Data Types
- **MySQL:** Add `srs_id` for MySQL 8.0 column information
- **Oracle:** Support Pure Golang Oracle Driver Read Data
- **Oracle:** Support Pure go oracle client `go-ora`
- **openGauss:** Support Local Socket Connect

### Performance Improvements

- **MTK:** Optimize Performance
- **MTK:** Optimize Export file performance
