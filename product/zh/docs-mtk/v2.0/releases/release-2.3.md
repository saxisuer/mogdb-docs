---
title: Release 2.3
summary: Release 2.3
author: Bin.Liu
date: 2022-07-29
---

# Release 2.3 Notes

## v2.3.5

### MTK

- [mtk_2.3.5_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_amd64.tar.gz)
- [mtk_2.3.5_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_amd64_db2.tar.gz)
- [mtk_2.3.5_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_darwin_arm64.tar.gz)
- [mtk_2.3.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_amd64.tar.gz)
- [mtk_2.3.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_amd64_db2.tar.gz)
- [mtk_2.3.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_linux_arm64.tar.gz)
- [mtk_2.3.5_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_windows_amd64.tar.gz)
- [mtk_2.3.5_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_2.3.5_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.5_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_amd64.tar.gz)
- [mtkd_2.3.5_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_amd64_db2.tar.gz)
- [mtkd_2.3.5_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.5/mtkd_2.3.5_linux_arm64.tar.gz)

### Bug Fixes

- **mtk:** 迁移数据hang住

### Performance Improvements

- **mtk:** Optimize processing time data performance

## v2.3.4

2022-06-19

### Bug Fixes

- **MySQL:** 迁移 MySQL 到 openGauss 改写 `if then` 语法问题
- **MySQL:** 迁移 MySQL 到 openGauss B 兼容模式,`NULL`和空字符串、字段长度问题
- **Oracle:**  迁移 Oracle 到 openGauss 改写 `mod` 函数问题
- **Oracle:** 迁移 Oracle 到 openGauss 移除多余括号问题
- **Oracle:** `dataOnly` 模式下没有查询表分区信息
- **Oracle:** 对象名带$问题,默认不需要增加双引号
- **mtk:** 排除表问题
- **openGauss:** 忽略创建插件错误

### Features

- **Oracle:** 声明不支持函数里包含函数场景下语法转换
- **mtk:** 添加参数 [disablePrintMigDataProgress](../config/mtk-config.md#disableprintmigdataprogress) 禁用打印迁移数据进度
- **mtk:** 添加参数 [disableSyncIdxAfterData](../config/mtk-config.md#disablesyncidxafterdata) 禁用同步完表的数据立即创建此表索引
- **mtk:** 报告添加迁移数据查询SQL
- **openGauss:** 支持分批次Commit. 任务重试需要用户配置 [truncTable](../config/mtk-parameter.md#trunctable)

### Performance Improvements

- **mtk:** 优化迁移数据到openGauss性能. 提升2-3倍

## v2.3.3

2022-06-08

### MTK

- [mtk_2.3.3_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_amd64.tar.gz)
- [mtk_2.3.3_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_amd64_db2.tar.gz)
- [mtk_2.3.3_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_darwin_arm64.tar.gz)
- [mtk_2.3.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_amd64.tar.gz)
- [mtk_2.3.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_amd64_db2.tar.gz)
- [mtk_2.3.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_linux_arm64.tar.gz)
- [mtk_2.3.3_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_windows_amd64.tar.gz)
- [mtk_2.3.3_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_2.3.3_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.3_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_amd64.tar.gz)
- [mtkd_2.3.3_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_amd64_db2.tar.gz)
- [mtkd_2.3.3_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.3/mtkd_2.3.3_linux_arm64.tar.gz)

### Features

- **Oracle:** 改写 Oracle 函数声明 `PARALLEL_ENABLE` 为 空
- **Oracle:** 改写 Oracle 函数声明 `deterministic` 为 空 openGauss `IMMUTABLE`
- **mtkd:** 增加连接池参数
- **openGauss:** 添加 `timestamp` 精度警告
- **openGauss:** 支持 Oracle `ST_GEOMETRY` 为 openGauss postgis `geometry`
- **openGauss:** 改写 Oracle `wmsys.wm_concat` 为 `wm_concat`
- **openGauss:** 改写 Oracle  `! =` 为 openGauss `!=`

### Bug Fixes

- **report:** 优化结果展示，解决sql复制bug
- **MySQL:** MySQL ignores case query constraints where conditional issues
- **Oracle:** 迁移 Oracle `raw` to openGauss `raw` issue
- **mtkd:** 创建/修改/运行任务增加配置检查

## v2.3.2

2022-05-16

### MTK

- [mtk_2.3.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_amd64.tar.gz)
- [mtk_2.3.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_amd64_db2.tar.gz)
- [mtk_2.3.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_darwin_arm64.tar.gz)
- [mtk_2.3.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_amd64.tar.gz)
- [mtk_2.3.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_amd64_db2.tar.gz)
- [mtk_2.3.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_linux_arm64.tar.gz)
- [mtk_2.3.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_windows_amd64.tar.gz)
- [mtk_2.3.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_2.3.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_amd64.tar.gz)
- [mtkd_2.3.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_amd64_db2.tar.gz)
- [mtkd_2.3.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.2/mtkd_2.3.2_linux_arm64.tar.gz)

### Bug Fixes

- **MySQL:** 建表语句主键语法部分没有进行关键字处理问题
- **openGauss:** 转换 DB2 `current user` 为 openGauss `session_user`
- **Oracle:** 增强Number类型转换
  - number ->numeric
  - integer(number(*,0)) ->bigint
  - number(10,2) -> numeric(10,2)

### Features

- **openGauss:** 添加参数[indexOptions](../config/mtk-parameter.md#indexoptions)支持迁移到openGauss添加/修改/删除索引创建选项
- **openGauss:** 添加参数[tableOptions](../config/mtk-parameter.md#tableoptions)支持迁移到openGauss添加/修改/删除表创建选项

## v2.3.1

2022-05-09

### Bug Fixes

- **DB2:** 迁移到MySQL自增列不是主键第一列问题
- **MTKD:** 日志默认级别问题
- **MySQL:** 迁移Oracle Number(*,0)丢失具体精度问题
- **MySQL:** 迁移MySQL连接数占用过多问题
- **openGauss:** 迁移Oracle Number(*,0)丢失具体精度问题
- **openGauss:** 查询分区SQL语句问题

### Features

- **MySQL:** 支持迁移触发器到 openGauss/MogDB
- **Postgre:** 支持`GENERATED ALWAYS AS IDENTITY`
- **openGauss:** 支持openGauss 3.0 large sequence

## v2.3.0

2022-04-19

### MTK

- [mtk_2.3.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_amd64.tar.gz)
- [mtk_2.3.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_amd64_db2.tar.gz)
- [mtk_2.3.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_darwin_arm64.tar.gz)
- [mtk_2.3.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_amd64.tar.gz)
- [mtk_2.3.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_amd64_db2.tar.gz)
- [mtk_2.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_linux_arm64.tar.gz)
- [mtk_2.3.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_windows_amd64.tar.gz)
- [mtk_2.3.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_2.3.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.3.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_amd64.tar.gz)
- [mtkd_2.3.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_amd64_db2.tar.gz)
- [mtkd_2.3.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.3.0/mtkd_2.3.0_linux_arm64.tar.gz)

### Bug Fixes

- **DB2:** 迁移到 MySQL 对象名称右侧包含空格问题,MTK自动截取右侧空格
- **DB2:** 迁移到 MySQL BLOB, TEXT, GEOMETRY or JSON column 不能包含默认值
- **DB2:** 移除 DB2CODEPAGE 环境变量强制检查
- **MTKD:** API 接口返回 400 问题
- **MTKD:** 修改 `deleteUserId` 为 `deletedUserId`, `updateUserId` 为 `updatedUserId`
- **MySQL:** update 语法错误问题
- **MySQL:** 转换 `interval` 语法问题
- **MySQL:** 中文转换问题
- **MySQL:** 函数没有`begin/end`语法问题
- **MySQL:** 转换`join`语法问题
- **MySQL:** 转换 `convert` to `cast` 语法问题
- **Oracle:** 转换 package to og package 注释缺失问题
- **Oracle:** 迁移到 MySQL 对象名称右侧包含空格问题,MTK自动截取右侧空格
- **Oracle:** 转换insert语句包含return关键字问题
- **Oracle:** 转换包里存储过程end没有存储过程名称问题
- **Oracle:** 参数 `colKeyWords` 优先级问题
- **Oracle:** 存储过程参数带注释问题

### Code Refactoring

- **MTK:** 生存对象名称方法

### Features

- **DB2:** 支持 DB2 `date/time` column 默认值 `current date/time` to MySQL `current_date/time`
- **MTKD:** 密码加密存储
- **MySQL:** 转换注释 `#` to `--`
- **Oracle:** 替换查询视图`all_`为`dba_`视图
- **Oracle:** 支持转换Insert语句带别名
- **Oracle:** 支持转换中文符号 `（），`为英文
- **openGauss:** 移除参数`check_function_bodies=off`
