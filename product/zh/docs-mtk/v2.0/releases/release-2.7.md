---
title: 发布记录
summary: 发布记录
author: Zhang Cuiping
date: 2021-09-17
---

# Release 2.7 Notes

- mtk  命令行工具
- mtkd 封装mtk作为后端服务程序,针对特定场景

## v2.7.2

2022-11-29

### MTK

- [mtk_2.7.2_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_windows_amd64.tar.gz)
- [mtk_2.7.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_arm64.tar.gz)
- [mtk_2.7.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_amd64.tar.gz)
- [mtk_2.7.2_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_amd64.tar.gz)
- [mtk_2.7.2_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_arm64.tar.gz)
- [mtk_2.7.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_linux_amd64_db2.tar.gz)
- [mtk_2.7.2_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_darwin_amd64_db2.tar.gz)
- [mtk_2.7.2_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_2.7.2_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.2_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_amd64.tar.gz)
- [mtkd_2.7.2_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_amd64_db2.tar.gz)
- [mtkd_2.7.2_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.2/mtkd_2.7.2_linux_arm64.tar.gz)

### Bug Fixes

- **MTK:** 表数据对比没用记录SQL问题
- **openGauss:** 迁移 BLOB 数据到 MogDB/openGauss Bytea 数据问题
- **openGauss:** 生成分区语句时判断 MogDB 四位版本号问题

## v2.7.1

2022-11-28

### MTK

- [mtk_2.7.1_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_windows_amd64.tar.gz)
- [mtk_2.7.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_arm64.tar.gz)
- [mtk_2.7.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_amd64.tar.gz)
- [mtk_2.7.1_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_amd64.tar.gz)
- [mtk_2.7.1_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_arm64.tar.gz)
- [mtk_2.7.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_linux_amd64_db2.tar.gz)
- [mtk_2.7.1_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_darwin_amd64_db2.tar.gz)
- [mtk_2.7.1_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_2.7.1_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.1_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_amd64.tar.gz)
- [mtkd_2.7.1_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_amd64_db2.tar.gz)
- [mtkd_2.7.1_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.1/mtkd_2.7.1_linux_arm64.tar.gz)

### Bug Fixes

- **Informix:** 迁移`interval`数据问题
- **MTK:** 移除增加别名功能
- **MTK:** 导出文件时用户配置字符集未忽略大小写
- **MTK:** 自动设置 `clientCharset` 问题
- **MTKD:** 下载配置文件未指定文件名问题
- **MogDB:** 迁移 Informix 到MogDB列默认值问题
- **MySQL:** 改写 MySQL `GROUP_CONCAT` 到 openGauss 语法问题
- **Oracle:** 迁移 Oracle `VARRAY Type Column` 数据问题
- **Oracle:** 改写 Oracle Type `VARRAY OF PLS_INTEGER` 问题
- **Oracle:** 改写 Oracle Column Type `Char(10 Char)` issue
- **openGauss:** 改写 Mariadb/MySQL `current_timestamp()` issue
- **openGauss:** 创建触发器语法丢失 `;`
- **openGauss:** 迁移 Oracle Clob 没有替换 0x00

### Code Refactoring

- **MTK:** 替换 MySQL `0000-00-00`

### Features

- **MTK:** 适配 SQLServer 2012
- **MTK:** Oracle外部表不支持歉意
- **MTK:** 更新离线报告
- **MTKD:** 任务接口增加模糊查找功能
- **MTKD:** 添加生成Linux service配置文件子命令
- **MTKD:** 修改任务接口增加修改数据库类型检查
- **openGauss:** 添加参数 [enableParallelIndex](../config/mtk-parameter.md#enableparallelindex) 控制MogDB/openGauss并行创建索引
- **openGauss:** 改写 Oracle VARRAY Type to openGauss domain
- **openGauss:** 添加参数 [enableCharTrimRightSpace](../config/mtk-parameter.md#enablechartrimrightspace)
- **openGauss:** 升级驱动支持返回copy错误数据
- **openGauss:** 改写 Oracle `DBMS_UTILITY.format_error_backtrace` to openGauss `PG_EXCEPTION_CONTEXT`

## v2.7.0

2022-11-09

### MTK

- [mtk_2.7.0_windows_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_windows_amd64.tar.gz)
- [mtk_2.7.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_arm64.tar.gz)
- [mtk_2.7.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_amd64.tar.gz)
- [mtk_2.7.0_darwin_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_amd64.tar.gz)
- [mtk_2.7.0_darwin_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_arm64.tar.gz)
- [mtk_2.7.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_linux_amd64_db2.tar.gz)
- [mtk_2.7.0_darwin_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_darwin_amd64_db2.tar.gz)
- [mtk_2.7.0_windows_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_2.7.0_windows_amd64_db2.tar.gz)
- [mtk_checksums.txt](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtk_checksums.txt)

### MTKD

- [mtkd_2.7.0_linux_amd64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_amd64.tar.gz)
- [mtkd_2.7.0_linux_amd64_db2.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_amd64_db2.tar.gz)
- [mtkd_2.7.0_linux_arm64.tar.gz](https://cdn-mogdb.enmotech.com/mtk/v2.7.0/mtkd_2.7.0_linux_arm64.tar.gz)

### Bug Fixes

- 在配置`igErrorData` 插入重复数据问题
- 自升级相关问题
- **DB2:** 生成 nvarchar 语法问题
- **DB2:** 迁移 MySQL Blob 到 DB2 问题
- **MTK:** 配置 `remapSchema`, `xxx.xxx not existed` 提示schema不对问题
- **MTKD:** 解决程序异常时返回500错误
- **MySQL:** 迁移 Oracle Nvarchar to MySQL Nvarchar 问题
- **MySQL:** 迁移 openGauss to MySQL 一些问题
- **Oracle:** 通过 `ora_hash` 生成并行语句问题
- **Oracle:** 迁移 Oracle Nvarchar to openGauss/MogDB 问题
- **Oracle:** 迁移 Oracle type to openGauss to 未移除 `authid current_user` 语法问题
- **SQLServer:** 迁移 Sequence 问题
- **SQLServer:** 迁移 SQLServer To MogDB 一些问题
- **openGauss:** 查询列信息丢失timestamp精度问题
- **openGauss:** 导出文件时去掉数据检查

### Features

- **MTK:** Support GaussDB for openGauss Primary/Standby version
- **MTK:** 添加参数 `dropSchema`. `dropExistingObject=true` 不在支持删除schema功能
- **MySQL:** 迁移 MySQL Key Partition to MogDB Hash Partition
- **Oracle:** 自动设置环境变量 `ORA_OCI_NO_OPTIMIZED_FETCH=1`
