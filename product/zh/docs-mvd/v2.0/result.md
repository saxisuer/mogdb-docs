---
title: MVD 结果说明
summary: MVD 结果说明
author: hongyedba
date: 2021-09-02
---

# MVD 结果说明

MVD 结果默认以可读文本的方式在终端直接输出，用户可以输出为 json 和 plain 格式，且可以将最终结果输出到指定的结果文件中。以下以文本格式，介绍 MVD 结果各个部分的具体含义。

## 特征值对比

以下结果来源于如下命令：

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*'
```

对比结果：

```
>>>> =============================================
>>>> 1. Environments:
>>>> =============================================
 OPTION               | VALUE
 ---------------------+--------------------------------------------------------
 SOURCE_DB            | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']
 TARGET_DB            | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']
 WORKERS              | 8
 INCLUDE              | [['%', 'mtk', '%']]
 RESULT_FORMAT        | json
 FUNCTION_DIMENSION   | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}}

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
 OBJECT_TYPE               | EXTRA_TYPE      | DIFFERENCE
 --------------------------+-----------------+------------------------------------------------------------------
 TABLE                     | SOURCE_EXTRA    | ['MTK.TAB_PART_RANGE_HASH', 'MTK.TEST_PK_UK', 'MTK.TAB_PART_KEY_2', 'MTK.TAB_PART_HASH', 'MTK.TAB_PART_RANGE']
 PROCEDURE                 | SOURCE_EXTRA    | ['MTK.TEST_MTK_DATA', 'MTK.TEST_MTK_TEST']
 TRIGGER                   | SOURCE_EXTRA    | ['MTK.USER_LOG']
 SEQUENCE                  | TARGET_EXTRA    | ['MTK.LOGS_ID_SEQ', 'MTK.USERS_ID_SEQ']

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
 OWNER | TABLE_NAME         | TYPE             | NAME                      | DIFFERENCE
 ------+--------------------+------------------+---------------------------+------------------------------------------------------------------
 MTK   | TAB_PART_KEY_1     | CONSTRAINT       | PRIMARY                   | COMMON: []
 MTK   | TAB_PART_KEY_1     |                  |                           | SOURCE_EXTRA: []
 MTK   | TAB_PART_KEY_1     |                  |                           | TARGET_EXTRA: ['ID']

>>>> =============================================
>>>> 4. Table Data Differences (Statistics Analysis):
>>>> =============================================
 OWNER | TABLE_NAME | COLUMN_NAME  | DIMENSION |      SOURCE_VALUE |       TARGET_VALUE
 ------+------------+--------------+-----------+-------------------+-------------------
 MTK   | TABLE_NULL | CNT          | ROW       |                 3 |                  0
 MTK   | MTK_TEST_2 | COL_DOUBLE_2 | AVG       | 501.2637835427314 | 501.26378354273165
 MTK   | MTK_TEST_2 | COL_DOUBLE   | AVG       | 499.1979752563913 |  499.1979752563916
 MTK   | MTK_TEST_1 | COL_DOUBLE   | AVG       | 498.8954055279028 |  498.8954055279032
 MTK   | MTK_TEST_4 | COL_DOUBLE_2 | AVG       | 501.5950313959741 |  501.5950313959745
 MTK   | MTK_TEST_4 | COL_DOUBLE   | AVG       | 503.2131632758452 |  503.2131632758458

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## MD5 逐行对比

以下结果来源于如下命令:

```shell
./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306::root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*' -R './diff'
```

对比结果

```
>>>> =============================================
>>>> 1. Environments:
>>>> =============================================
 OPTION               | VALUE
 ---------------------+--------------------------------------------------------
 SOURCE_DB            | ['MYSQL', '121.36.15.2', '55444', '', 'root', '******']
 TARGET_DB            | ['MOGDB', '121.36.15.2', '55432', 'mysql_mtk', 'hongye', '******']
 WORKERS              | 8
 INCLUDE              | [['%', 'mtk', '%']]
 RESULT_FORMAT        | json
 ROW_DIR              | /Users/hongyedba/Desktop/mysql
 FUNCTION_DIMENSION   | {'AVG': {'TYPE': 'A', 'FUNCTIONS': {'*': 'AVG'}}, 'MIN': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MIN'}}, 'MAX': {'TYPE': 'NP', 'FUNCTIONS': {'*': 'MAX'}}}

>>>> =============================================
>>>> 2. Object Differences:
>>>> =============================================
 OBJECT_TYPE               | EXTRA_TYPE      | DIFFERENCE
 --------------------------+-----------------+------------------------------------------------------------------
 PROCEDURE                 | SOURCE_EXTRA    | ['MTK.TEST_MTK_DATA', 'MTK.TEST_MTK_TEST']
 TRIGGER                   | SOURCE_EXTRA    | ['MTK.USER_LOG']
 TABLE                     | SOURCE_EXTRA    | ['MTK.TAB_PART_RANGE', 'MTK.TAB_PART_RANGE_HASH', 'MTK.TAB_PART_KEY_2', 'MTK.TEST_PK_UK', 'MTK.TAB_PART_HASH']
 SEQUENCE                  | TARGET_EXTRA    | ['MTK.LOGS_ID_SEQ', 'MTK.USERS_ID_SEQ']

>>>> =============================================
>>>> 3. Table Structure Differences:
>>>> =============================================
 OWNER | TABLE_NAME         | TYPE             | NAME                      | DIFFERENCE
 ------+--------------------+------------------+---------------------------+------------------------------------------------------------------
 MTK   | TAB_PART_KEY_1     | CONSTRAINT       | PRIMARY                   | COMMON: []
 MTK   | TAB_PART_KEY_1     |                  |                           | SOURCE_EXTRA: []
 MTK   | TAB_PART_KEY_1     |                  |                           | TARGET_EXTRA: ['ID']

>>>> =============================================
>>>> 4. Table Data Differences (MD5 ROW-BY-ROW):
>>>> =============================================
 OWNER | TABLE_NAME | SOURCE_EXTRA | TARGET_EXTRA
 ------+------------+--------------+-------------
 MTK   | TABLE_NULL |       3 of 3 |       0 of 0
 MTK   | MTK_TEST_3 | 158 of 20000 | 158 of 20000
 MTK   | MTK_TEST_1 | 153 of 20000 | 153 of 20000
 MTK   | MTK_TEST_4 | 165 of 20000 | 165 of 20000
 MTK   | MTK_TEST_2 | 166 of 20000 | 166 of 20000

>>>> =============================================
>>>> 5. Completed
>>>> =============================================
```

## 环境

环境参数与变量信息，展示当前运行过程中的一些程序参数与变量。

## 对象差异

对象差异，按类别汇总源库和目标库的对象差异，该结果分 3 列展示：

- **OBJECT_TYPE**: 差异对象的类别
- **EXTRA_TYPE**: 差异类型，分为两种类型，`TARGET_EXTRA` 类型为目标库中多出来的对象，`SOURCE_EXTRA` 类型为源库中多出来的对象
- **DIFFERENCE**: 具体差异的对象列表，列表中的元素格式为 &lt;SCHEMA&gt;.&lt;OBJECT_NAME&gt;

## 表结构差异

表结构差异，展示每一张差异表的具体差异项，该结果分为 5 列展示：

- **OWNER**: 表的 Schema 信息
- **TABLE_NAME**: 表名
- **TYPE**: 差异类别，主要有： COLUMN_COUNT, COLUMN, COLUMN_POSITION, COLUMN_DATATYPE, COLUMN_NULLABLE, CONSTRAINT, INDEX 等
- **NAME**: 差异名称，不同差异类别中，差异名称有不同的含义
- **DIFFERENCE**: 具体的差异项，不同差异类别中，差异项的格式也有一定区别

各差异类别下，差异名称与差异项含义说明如下：

| 差异类别        | 差异名称 | 差异项                                                       |
| --------------- | -------- | ------------------------------------------------------------ |
| COLUMN_COUNT    | 无       | 格式： SOURCE: {n}, TARGET: {m}<br />其中： n, m 分别表示源库和目标库中字段的数量 |
| COLUMN          | 无       | 格式：COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />其中：x 表示源库和目标库中都存在的字段，y 表示源库多余的字段，z 表示目标库多余的字段 |
| COLUMN_POSITION | 字段名   | 格式： SOURCE: {n}, TARGET: {m}<br />其中： n, m 分别表示对应的字段在源库和目标库中的位置信息 |
| COLUMN_DATATYPE | 字段名   | 格式： SOURCE: {n}, TARGET: {m}<br />其中： n, m 分别表示对应的字段在源库和目标库中的类型信息 |
| COLUMN_NULLABLE | 字段名   | 格式： SOURCE: {n}, TARGET: {m}<br />其中： n, m 分别表示对应的字段在源库和目标库中的是否为空信息 |
| CONSTRAINT      | 约束类型 | 格式：COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />其中：x 表示源库和目标库中都存在的约束，y 表示源库多余的约束，z 表示目标库多余的约束 |
| INDEX           | 无       | 格式：COMMON: {x} SOURCE_EXTRA: {y} TARGET_EXTRA: {z}<br />其中：x 表示源库和目标库中都存在的索引，y 表示源库多余的索引，z 表示目标库多余的索引 |

**注意事项**：

- 对于约束和索引，名称不同不认为存在差异，只要约束和索引对应的 “字段名称，位置，顺序，是否使用函数” 等信息一致，即认为双方是一致的。即如下两个索引认为是相同的：

```sql
-- Oracle
create index idx_test_pk_oracle on hongye.test(id);
-- MogDB
create index idx_test_pk_mogdb on hongye.test(id);
```

## 表数据差异

数据差异，展示表中的数据差异，无差异的表表不展示。数据差异按照对比方式的不同分为两种：

- 特征值对比（Statistics Analysis）

- 逐行对比（MD5 ROW-BY-ROW）

特征值对比结果分为 6 列展示：

- **OWNER**: 表的 Schema 信息
- **TABLE_NAME**: 表名
- **COLUMN_NAME**: 字段名称
- **DIMENSION**: 统计维度，默认情况下，对于主键表，统计维度为：COUNT + AVG + CORR，对于非主键表，统计维度为： COUNT + AVG + MIN + MAX + MEDIAN
- **SOURCE_VALUE**: 源库统计值
- **TARGET_VALUE**: 目标库统计值

逐行对比结果分为 4 列展示：

- **OWNER**: 表的 Schema 信息
- **TABLE_NAME**: 表名
- **SOURCE_EXTRA**: 源端独有的行数，以及源端的总行数
- **TARGET_EXTRA**: 目标端独有的行数，以及目标端的总行数
