---
title: MVD 使用说明
summary: MVD 使用说明
author: hongyedba
date: 2021-09-02
---

# MVD 使用说明

MVD 为纯命令行工具，只能在 Shell 界面中执行。可通过 -h 选项查看命令帮助信息，如下：

```shell
Introduction:
    MVD is a data verification tool for Heterogeneous Databases.

Options:
    -h, --help           : Show help message
    -v, --version        : Show tool version [3.0.0]
    -x, --debug          : Run in debug mode, means more output logs
        --debug-md5      : Debug for print data before calculator MD5
    -c, --config-file    : Using a config file with format json
        --mtk-config     : Using a config file from MTK tool with format json
    -C, --catagory       : Compare catagory: A=All, M=Metadata, D=Data
    -d, --func-dimension : [Advanced Option] Functions used in data comparison
                           Default: avg:a,min:np,max:np,median:np
                           Format: <name>:<primary_type>    -- Same function name among all database
                                   <primary_type> := a|p|np, a = all, p = primary table, np = not [p]
                                                     can be ignored, then use default [a]
                           Format: <name>:<function_list>:<primary_type>
                                   <function_list> := <db_type> = <function_name> | <db_type> = <function_name>
                                   <db_type> := ORACLE|DB2|MYSQL|POSTGRESQL|MOGDB|OPENGAUSS|SQLSERVER|INFORMIX
                           Example 'testmin:oracle=min|mogdb=min|mysql=min|db2=min:p'
    -s, --source-db      : Source database to be verified
                           Format: <db_type>:<ip>:<port>:<name>:<user>:<password>
                                   <db_type> := ORACLE|DB2|MYSQL|POSTGRESQL|MOGDB|OPENGAUSS|SQLSERVER|INFORMIX
    -t, --target-db      : Target database to be verified, format see also '-s'
    -w, --workers        : Parallel workers (1~32), default: 8
    -T, --table          : Check a single table (source table info)
                           Format: <owner>.<table_name>
                             Tips: 1. if this option was specified, then '-i' and '-e' will be ignored
                                   2. Name with mixed-case means case sensitive
    -i, --include        : Source White list, patterns used for object filter, all patterns combined with comma
                           Format: <type>:<owner>.<object_name>,...
                                   <type>:<object_name>
                                   <owner>.<object_name>
                                   <object_name>
                             Tips: 1. <type> can use: */%/TABLE/VIEW/SEQUENCE/PROCEDURE/FUNCTION/OTHERS
                                   2. Can use */% in <owner> and <object_name> field, means to match all
                                   3. Name with mixed-case means case sensitive
    -e, --exclude        : Source Black list, patterns used for object filter, format see also '-i'
    -r, --remap-schema   : Schema transformation in comparision
                           Format: <source_schema>:<target_schema>,<source_schema>:<target_schema>...
        --column-list    : Set valid column list for data comparion, combined with comma
                           Format: <colume_1>,<colume_2>,<colume_3>,...
                             Tips: name with mixed-case means case sensitive
    -f, --result-file    : Result file, used to save result, default to print result to screen
    -F, --result-format  : Result file data format: json (default), plain
    -R, --row-dir        : Row directory for differences data (MD5 & KEY)
        --row-feedback   : Query row data when differences found, otherwise just key condition listed
        --ignore-float   : Ignore float data type in comparison
        --float-prec     : Float precision in data comparison, Range: 0-6, Default: 4
        --double-prec    : Double precision in data comparison, Range: 0-12, Default: 10
        --fraction-prec  : Fraction precision in data comparison, Range: 0-6, Default: 6 (Informix is 5)
    -z, --zero-char      : Specify a char for chr(0) in comparason, Default is empty char
    -Z, --time-zone      : Specify timezone for DB client, set empty use local, default is UTC(+00:00)
    -l, --logfile        : Write output information to logfile
    -L, --license        : Specify license file, default is: ./license.json
        --apply-license  : Apply for a new license from server

Usage:
    1. Verify a single table  (Using Statistics Analysis)
       ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -T 'HONGYE.TEST'
    2. Verify a single schema  (Using MD5 ROW-BY-ROW)
       ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -i 'HONGYE.*' -R './diff'
```

## 命令选项说明

| 命令选项             | 选项说明                                                     |
| -------------------- | ------------------------------------------------------------ |
| -h, --help           | 显示工具的帮助信息                                           |
| -v, --version        | 显示工具的当前版本信息                                       |
| -x, --debug          | 开启 DEBUG 模式，输出更详细的日志内容，仅在出现问题后的调试过程中使用 |
| --debug-md5          | 专用于 Python MD5 计算前值输出的 debug 选项，该选项会打开 DB2/Oracle 表中的所有数据，占用大量的磁盘日志空间，请谨慎使用 |
| -c, --config-file    | 指定程序运行时的配置参数（json格式），使用配置参数代替具体命令行选项 |
| --mtk-config         | 指定 MTK 运行时的配置文件（json格式），校验 MTK 同步过程中涉及到的对象和表的数据 |
| -C, --catagory       | 指定对比类别： A = 对比所有， M = 仅对比对象结构， D = 仅对比数据 |
| -d, --func-dimension | 高级选项，不是特别清晰该选项含义时，请勿使用。<br />指定数据对比过程中的统计函数维度，输入格式为： <br />   1. &lt;name&gt;:&lt;primary_type&gt;<br />   2. &lt;name&gt;:&lt;function_list&gt;:&lt;primary_type&gt;<br />其中： <br />   primary_type 可选值为 a = 所有表, p = 主键表, np = 非主键表<br />   name 函数名称，若函数名称即为实际数据库中的函数，那么无需指定 function_list，否则需要指定改函数在各个数据库中的实际表现<br />   function_list 指定该函数在各个数据库中的不同表现形式，格式为： &lt;db_type&gt;=&lt;function_name&gt;<br />其中 db_type 可选值如下： ORACLE, DB2, MYSQL, POSTGRESQL, MOGDB, OPENGAUSS, INFORMIX, SQLSERVER |
| -s, --source-db      | 指定对比的源端数据库，其输入格式为： &lt;db_type&gt;:&lt;ip&gt;:&lt;port&gt;:&lt;name&gt;:&lt;user&gt;:&lt;password&gt;<br />其中 db_type 可选值如下： ORACLE, DB2, MYSQL, POSTGRESQL, MOGDB, OPENGAUSS, INFORMIX, SQLSERVER |
| -t, --target-db      | 指定对比的目标端数据库，其输入格式与 `-s` 一样。             |
| -w, --workers        | 指定数据对比过程中的并发进程数量，范围是 1~32，默认是 8 个并发 |
| -T, --table          | 仅仅只针对单张表进行对比，输入格式为： &lt;owner&gt;.&lt;table_name&gt;，不可使用通配符，会与 `-i` 和 `-e` 选项冲突。 |
| -i, --include        | 需要包含进行对比的对象列表，可指定多个匹配模式，使用逗号分隔。<br />匹配格式包括：&lt;type&gt;:&lt;owner&gt;.&lt;object_name&gt;, &lt;owner&gt;.&lt;object_name&gt;, &lt;object_name&gt;, ...<br />可以在 OWNER, OBJECT_NAME 中使用 `*` 或 `%` 来标识通配符<br />TYPE 类型包括： */%/TABLE/VIEW/SEQUENCE/PROCEDURE/FUNCTION/OTHERS |
| -e, --exclude        | 需要排除的对象列表，其格式与 `-i` 一样。                     |
| -r, --remap-schema   | 指定对比时的源端与目标端的 Schema 映射关系，默认源端与目标端 Schema 同名无映射 |
| --column-list        | 指定只对比本选项列出的字段数据                               |
| -f, --result-file    | 对比结果的输出文件，默认无输出文件，直接在当前执行的命令行窗口展示结果 |
| -F, --result-format  | 对比结果的格式，支持 json, plain 两种，默认 plain 方式，即便于人工阅读的文本格式 |
| -R, --row-dir        | 执行逐行 MD5 对比模式，指定行差异结果的输出文件夹，每张有差异的表，都会在该目录中创建一个差异文件 |
| --row-feedback       | 是否回显差异行的完整数据，默认只显示差异行的 KEY （Oracle 中的 ROWID, PG 中的 CTID 等） |
| --ignore-float       | 是否在数据对比过程中，忽略浮点类型 （float, double, real 等非精确类型），默认不忽略 |
| -z, --zero-char      | 指定 chr(0) 字符在对比中的替代字符，默认为空字符，即移除 chr(0) 不可见字符 |
| -Z, --time-zone      | 指定客户端数据查询的时区，设置空字符串则使用本地操作系统时区，不设置则使用 UTC (+00:00) 时区 |
| -l, --logfile        | 指定工具运行的日志文件                                       |
| -L, --license        | 指定 License 文件的位置，若 license.json 不在当前目录则需要手动通过本选项指定 |
| -l, --apply-license  | 申请 License                                                 |

## 常用命令示例

以下为一些常用场景下的命令示例（以 2.0 MacOS 版本程序为例）。

注意： 特征值统计对比方式可能效率较慢，推荐使用 MD5 逐行对比模式。

0. 申请 License

   ```shell
   ./mvd_macos_x86_64 --apply-license
   ```

   需要在执行过程中输入接收 license 的邮箱地址

1. 对比 Oracle 到 MogDB 的结构和数据差异：

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:hongye:pwd' -t 'MOGDB:127.0.0.1:5432:omm:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

2. 对比 DB2 到 MogDB 的结构和数据差异：

   ```shell
   ./mvd_macos_x86_64 -s 'DB2:127.0.0.1:50000:HONGYE:db2inst1:pwd' -t 'MOGDB:127.0.0.1:5432:db2_mtk1:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

3. 对比 MySQL 到 MogDB 的结构和数据差异：

   ```shell
   ./mvd_macos_x86_64 -s 'MYSQL:127.0.0.1:3306:hongye:root:pwd' -t 'MOGDB:127.0.0.1:5432:mysql_mtk:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

4. 只对某一张表执行数据对比（特征值对比）：

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -T 'HONGYE.TEST'
   ```

5. 对比数据和结构，并精确识别差异行：

   ```shell
   ./mvd_macos_x86_64 -s 'ORACLE:127.0.0.1:1521:orcl:scott:tiger' -t 'MOGDB:127.0.0.1:5432:postgres:hongye:pwd' -i 'mtk.*' -R './diff'
   ```

6. 根据 MTK 的配置文件，进行迁移后的数据校验：

   ```shell
   ./mvd_macos_x86_64 --mtk-config oracle2opengauss.json
   ```
