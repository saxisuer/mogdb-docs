---
title: Release Notes
summary: Release Notes
author: Zhang Cuiping
date: 2021-08-30
---

# 发布记录

## v3.3.1 (2022-11-25)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.1/mvd_macos_x86_64)

### 新增功能

- 支持设置浮点数精度，毫秒精度，Informix 库毫秒精度最大设置到 5

### 功能优化

- 优化 MySQL 连接方式： 采取两次尝试，分别采用默认认证插件与mysql_native_password认证插件
- 更新打包脚本和逻辑，减少外部 Lib 文件，缩减可执行程序大小
- 列统计值对比也考虑浮点数精度问题

### 问题修复

- 修复 Informix 库主键表对比时，md5 计算时少算一列导致的数据对比异常
- 修复 Informix 库中 Money 数据对比时带符号的问题
- 修复 MySQL 数据对比时，无法识别 varchar 和 tinyint 字段类型数据的问题
- 修复 openGauss 在数据库为 B 兼容模式时，concat null 值返回 null 的问题
- 修复 openGauss 在数据库为 B 兼容模式时，coalesce(null::int, '') 返回 0 的问题

## v3.3.0 (2022-11-21)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.3.0/mvd_macos_x86_64)

### 新增功能

- 支持 Informix 库的基础对象对比，以及表数据的逐行 md5 对比

### 功能优化

- openGauss 库数据对比时，增加对 boolean 字段支持

### 问题修复

- 修复 openGauss 数据对比时，对 row 数据赋值导致的报错
- 修复OG库数据对比不识别text字段的问题

## v3.2.0 (2022-11-03)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.2.0/mvd_macos_x86_64)

### 新增功能

- 支持 SQL SERVER 库的基础对象对比，以及表数据的逐行 md5 对比
- 支持通过 callback 回调接收进程信息与结果

### 功能优化

- 优化 macOS 系统中 Oracle Lib 的初始化逻辑
- 表结构对比过程中，考虑到字段映射的自定义设定
- 支持 openGauss 中 Large Sequence 对象的识别

### 问题修复

- 修复 openGauss 中 money 类型数据处理函数的不兼容报错
- 修复 column-list 选项未设置时，列统计对比报错的问题
- 修复 MySQL 驱动报错： ImportError: No localization support for language 'eng'

## v3.1.1 (2022-10-12)

- [mvd_linux_arm64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_linux_arm64)
- [mvd_linux_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_linux_x86_64)
- [mvd_macos_x86_64](https://cdn-mogdb.enmotech.com/mvd/MVD_v3.1.1/mvd_macos_x86_64)

### 问题修复

- 修复 MogDB/openGauss/MySQL 库中由于 || 导致的数据对比异常，测试兼容 mariadb

## v3.1.0 (2022-10-10)

### 功能优化

- License 认证版本升级为 v31, 不再兼容旧版 License 文件

### 问题修复

- 修复 MogDB/openGauss 库中主键表对比时 || 拼接空串导致的数据对比异常

## v3.0.1 (2022-09-23)

### 新增功能

- 增加 "--upgrade" 选项，支持脚本自升级
- 支持 PostgreSQL 数据库的对比
- 支持单表模式 "-T" 下对 schema 名称与表名的重命名对比
- 支持单表模式 "-T" 下对字段名称重命名后的过滤与对比

### 功能优化

- Oracle 无主键表数据查询效率优化
- 数据查询排序规则调整（有主键按主键排序，无主键按 MD5 值排序）

### 问题修复

- 修复部分场景下，Oracle 字段中多字节字符乱码导致的查询报错

## v3.0.0 (2022-06-09)

### 新增功能

- 增加 "-L", "--license", "--apply-licnese" 选项，用于 License 申请和验证
- 增加 "--column-list" 参数，支持只对比指定字段的数据

### 功能优化

- 使用 "--debug-md5" 选项可输出源库和目标库字段列表与主键列表，便于问题分析定位
- 统计指标对比时，将字段过滤内置到查询之前，部分代码风格调整
- 调整对象名称中有空字符的显示方式（使用双引号包裹）
- 支持 MySQL 对象名称不带空格与其他库带空格的对比
- 添加 OG 执行 SQL 时的 Debug 信息

### 问题修复

- 修复 MogDB 主键数据对比，返回值为 NULL 导致的 Key 值对比失败
- 将 public 移除出系统用户列表

## v2.4.8 (2022-04-22)

### 新增功能

- 对象对比时，MogDB 库增加 Package 对象(2.1及以后版本)，以及 SYNONYM 对象的对比
- Oracle/DB2 数据库对比时，增加对 OS 本地变量的识别（TMPDIR, TMP），用于存储程序运行时的临时数据
- 增加 -Z 选项，可在运行时手动指定不同数据库中数据对比所使用的时区

### 功能优化

- 最终结果展示微调，可读性更优
- 优化 --debug-md5 选项在数据对比过程中的输出，能输出更加细致的双边数据对比过程
- 优化 -T 模式，使得单表对比模式下也支持 -r 指定 Schema 映射

### 问题修复

- 修复 openGauss/MogDB 库中的主键表，查询时排序规则不同，导致的对比结果异常的问题
- 修复非主键表对比过程中，主键标识识别失败，导致的程序报错问题
- 修复不同数据库中，主键字段名称大小写混合，导致排序数据不一致的问题 ("TID")
- 修复单边库中数据不一致时，无法正确识别的问题
- 修复主键字段尾部空格，导致主键表对比结果不正确的问题
- 修复 Oracle/DB2 中查询字段多次出现（主键表），导致查询报错的问题
- 修复 MySQL 库中 char 类型尾部空格导致的数据对比不正确的问题

## v2.4.0 (2021-12-20)

### 新增功能

- 增加对比类别选项 (-C/--catagory)： 提供单独对比对象结构，以及单独对比数据的功能
- 增加调试选项 (--debug-md5)： 提供对数据对比时，Python MD5计算结果的原值输出
- 增加Schema重映射选项 (--remap-schema)： 支持源库与目标库 Schema 名称发生变化情况下的数据对比

### 功能优化

- 优化数据对比逻辑： 对有主键的表，采取按主键对比的方案，并在结果中输出主键值与源和目标的MD5计算结果
- 输出结果优化： 重新改写对象结构对比，以及数据对比的结果展示，更加便于阅读和理解
- DB2 连接方式优化： 在DB2 数据库连接时，尝试更多的认证方式 (SERVER_ENCRYPT, SERVER, CLIENT, 默认)

### 问题修复

- 修复不可见字符 chr(0) 导致的数据对比失败，并添加对应的 -z/--zero-char 选项，可指定 chr(0) 在迁移中的转换字符
- 修复 DB2 数据库中，数字取模操作符 (%) 在部分版本中不可用的问题

## v2.3.2 (2021-11-08)

### 问题修复

- 修复低版本 MySQL(&lt;8.0) 时表约束对比错误，支持 MySQL 5.5, 5.7, 8.0
- 修复 MySQL 不同版本字段类型返回值在 Python 驱动中的数据类型不同的问题

## v2.3.1

### 问题修复

- 修复部分版本下 MySQL 查询字段类型，驱动返回值是 str/byte 导致的程序异常
- 修复 MySQL 驱动在 SUSE 系统中获取操作系统版本时，lsb_release -a 命令返回值解析报错的问题

## v2.3.0

### Feature

- 支持 Linux 平台下 openGauss/MogDB 中的 sha256 加密认证方式

## v2.2.14
