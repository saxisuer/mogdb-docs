MergeTocToolScript := $(CURDIR)/scripts/merge_by_toc.py
GeneratePdfToolScript := $(CURDIR)/scripts/generate_pdf.sh
GeneratePdfToolScriptV2 := $(CURDIR)/scripts/generate_pdf_v2.sh
GenerateDocsToolScriptV2 := $(CURDIR)/scripts/generate_docx.sh
PdftemplateV2File := $(CURDIR)/templates/eisvogel.tex
PdftemplateFile := $(CURDIR)/templates/template.tex
DocxTemplateDocxFile:= $(CURDIR)/templates/rmd-minion-reference.docx
BuildFileDir := $(CURDIR)/product
V             = 0
Q             = $(if $(filter 1,$V),,@)
M             = $(shell printf "\033[34;1m▶\033[0m")

all: help


get-install:
	npm install

# build: get-deps

# test: build
#   npm run test:cov
#   # go test -short -coverprofile=bin/cov.out `go list ./... | grep -v vendor/`
#   # go tool cover -func=bin/cov.out

export templateFile=${PdftemplateFile}
export templateV2File=${PdftemplateV2File}
export templateDocxFile=${DocxTemplateDocxFile}

.PHONY: lint
lint: ; $(info $(M) running markdownlint) @ ## Run markdownlint


.PHONY: help
help: ; $(info) @ ## Print help info
	@grep -E '^[ a-zA-Z1-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
      awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: install
install: ; $(info $(M) running npm install) @ ## Run Npm Install deps
	npm install

.PHONY: build
build: ; $(info $(M) running npm build) @ ## Run Npm build
	npm run build

genLanage=$(word 2,$(subst /, ,$1))
genVersion=$(word 4,$(subst /, ,$1))

define genName
    @echo "my name is $(0)"
endef

pdf_all:
	scripts/build_pdf.sh ${WITH_UPLOAD}
clean:
	rm -rf dist

.PHONY: docker
docker:
	docker build -t mogdb-docs .

lychee:
	lychee -E --exclude-mail --format markdown -- product/*/docs-mtk/
