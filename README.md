
# MogDB 文档网站 v2.1

## 🧐 参考资料

- [GatsbyJS参考文档](https://www.gatsbyjs.cn/tutorial/part-six/#transformer-plugins)

## 🚀 快速开始

node 版本 v15.14.0

1. **安装依赖包**

    ```shell
    npm i
    ```

2. **本地开发**

    ```shell
    npm run develop
    ```

    预览本地开发效果：<http://localhost:8000/>
    查看GraphiQL：<http://localhost:8000/___graphql>

3. **线上部署**

    ```bash
    npm run build
    # 使用 rsync 同步到 nginx 部署目录
    rsync -rltgoD --delete public/ /data/mogdb-docs
    ```

## 📁 目录结构

```bash
.
├── product
├── node_modules
├── public
├── scripts
├── src
├── static
├── templates
├── .gitignore
├── .prettierignore
├── .prettierrc
├── gatsby-browser.js
├── gatsby-config.js
├── gatsby-node.js
├── gatsby-ssr.js
├── LICENSE
├── package-lock.json
├── package.json
└── README.md
```

1. **`./product`**:

    文档目录。该目录下存放的文件为MogDB的全部文档，文档按照中英文、版本进行分类。

    注意每一个版本内部均配备一个目录**toc.md**文件。每一个文件均需要说明文件信息.

    必须写在第一行. 不能存在空换行

    ```md
    ---
    title: MogDB 简介
    summary: MogDB设计初衷，背景与设计理念。
    author: someone
    date: 2020-08-10
    ---
    ```

2. **`./scripts`**: 编译pdf等文件相关脚本。
3. **`./src`**: 核心开发目录。
4. **`./static`**: 静态资源目录。
5. **`./templates`**: 编译pdf等文件相关模版。
6. **`gatsby-browser.js`**: Gatsby API配置文件。[Gatsby Browser APIs](https://www.gatsbyjs.com/docs/browser-apis/)
7. **`gatsby-config.js`**: Gatsby 核心配置文件。[Gatsby Config docs](https://www.gatsbyjs.com/docs/gatsby-config/)
8. **`gatsby-node.js`**: Gatsby Node API配置文件。 [Gatsby Node APIs](https://www.gatsbyjs.com/docs/node-apis/)
9. **`gatsby-ssr.js`**: [Gatsby server-side rendering APIs](https://www.gatsbyjs.com/docs/ssr-apis/)

## 🧐 文档与版本配置说明

1. 文档内容放在docs目录下，按照中英文、版本进行划分。注意每个版本下必须设置目录文件(toc.md).
2. 为了更好的支持SEO，每一个文档内容在顶部最好设置标题与描述。
3. 文档的版本目前在代码内部写死，存放在src/utils/config.js。

## 编写注意事项

- 不在代码块里的`<`和`>`使用 "&lt;" 代替 `<` , 使用 "&gt;" 代替 `>`
- 列表后面跟列表关键符号需要增加转义

    ```md
    - +  --> 需要转义为 - \+
    - *  --> 需要转义为 - \*
    - -  --> 需要转义为 - \-
    + -  --> 需要转义为 + \+
    + +  --> 需要转义为 + \+
    + *  --> 需要转义为 + \*
    * *  --> 需要转义为 * \*
    * +  --> 需要转义为 * \+
    * -  --> 需要转义为 * \-
    ```

- 表格内的|需要转义

    ```md
    | A | B | C |
    | - | - | - |
    | \|| - | - |
    ```

- 字符符号连续出现,部分需要转义

  - `*****` 错误 -> `**\***` 正确
  - `*****` 错误 -> `**\***` 正确
  - `| node_id | integer  | Node ID (**node_id** in **pgxc_node**)|` 错误
    - `| node_id | integer  | Node ID (**node\_id** in **pgxc_node**)|`
    连续连个同时出现,存在误识别问题. 主要在表格中数据存在
  - 不要出现单个字体符号

- 字体符号不能出现空格

  - the `_SQL Coverage and Limitation _section` 错误
    `_SQL Coverage and Limitation_ section` 正确
  - `**粗体 **` 错误
    `**粗体**`  正确

    ```
    - **粗体** __粗体__
    - *斜体* _斜体_
    - ~下标~
    - ***粗斜体*** ___粗斜体___
    - ~~删除线文本~~
    ```

- 单独URL需要增加<>
    ```<http://example.com/>```
- 特殊符号. 加上反斜杠来帮助插入普通的符号

    ```md
    \   反斜线
    `   反引号 "\`"
    *   星号
    _   底线
    {}  花括号
    []  方括号
    ()  括弧
    #   井字号
    +   加号
    -   减号
    .   英文句点
    !   惊叹号
    $   惊叹号 "\$"
    ```

    > 以上符号在单独出现会影响PDF构建,需要进行专业. 如
    >
    > “|”, “;”，“&”，“$”，“<”，“>”，“`”，“\”，“!”
    >
    > 需要转义为
    >
    > “\\|”, “;”，“&”，“\\$”，“<”，“>”，“\\`”，“\\\\”，“!”
