# Contribute

git remote add upstream git@gitee.com:enmotech/mogdb-docs.git

## 修改 Bug

1. 个人仓库fork
2. 同步代码

    ```bash
    git clone <个人仓库地址>
    git remote add upstream git@gitee.com:enmotech/mogdb-docs.git
    git fetch upstream
    git checkout -b <branch_name> upstream/master # 从最新的master分支创建新分支
    ```

3. 修改代码
4. 提交 个人仓库 `branch_name` 分支 到 源仓库`master` 分支的PR.
5. 代码 review.

    > Jenkins会进行代码语法校验
    >
    > ```bash
    > markdownlint product
    > ```

6. 预览效果.
    PR会自动构建生成临时网站并评论到PR内

## 增加文档版本

1. 仓库管理员创建对应版本分支. 如 `mogdb_3.0`
2. 个人分支拉取代码

    ```bash
    git fetch upstream
    git checkout -b mogdb_3.0 upstream/master
    ```

3. 修改代码
4. 提交pr到`mogdb_3.0` 分支,预览效果
5. 全部编写完成后. 提交 `mogdb_3.0` 到 `master` 分支pr
6. 删除 `mogdb_3.0` 分支

## 文章链接

- 文档内连接写相对路径.

目录

```text
.
├── commands
│   ├── mtk.md
│   ├── mtk_check-table-data.md
├── example.md
├── faqs.md
├── mtk-config.md
```

```markdown
[mtk-config](mtk-config.md)
[test1](mtk-config.md#test1)
[mtk](commands/mtk.md)  链接到子目录的文档
[test2](commands/mtk.md#test2)  链接到子目录的文档的自章节
[test5](../aaaa/mtk-config.md#test2)  链接到父子目录的文档
```

## Commit Msg

<Type>(Scope): Short Message

- Type

    |Type|描述|
    |---|---|
    |feat|新增文章/新特性|
    |add|新增文章/新特性|
    |fix|修复/更新文章|
    |update|修复/更新文章|
    |style|样式修改|

- Scope

  范围
  - mogdb
  - mtk
  - ptk
  - sca
  - mdb
  - mvd

example:

- feat(mtk): release v2.3.4
- add(mogdb): add guc params xxx
- update(mogdb): update guc params xxx
- style: code highlight issue
