#!/bin/bash

set -e
# test passed in pandoc 1.19.1

MAINFONT="WenQuanYi Micro Hei"
MONOFONT="WenQuanYi Micro Hei Mono"

DocFile=${1}
DocTitle=${2}
DocOutPutFile=${3}
if [ ! -n "$DocFile" ]; then
    DocFile="doc.md"
fi
if [ ! -n "$DocTitle" ]; then
    DocTitle="MogDB"
fi
if [ ! -n "$DocOutPutFile" ]; then
    DocOutPutFile="output.pdf"
fi

# MAINFONT="Tsentsiu Sans HG"
# MONOFONT="Tsentsiu Sans Console HG"

#_version_tag="$(date '+%Y%m%d').$(git rev-parse --short HEAD)"
_version_tag="$(date '+%Y%m%d')"

# default version: `pandoc --latex-engine=xelatex doc.md -s -o output2.pdf`
# used to debug template setting error

# add docs versions
# generate PDF for dev version

pandoc -N --toc --smart --latex-engine=xelatex \
    --template=${templateFile} \
    --listings \
    --columns=80 \
    -V title="${DocTitle}" \
    -V author="Enmotech Inc." \
    -V date="${_version_tag}" \
    -V CJKmainfont="${MAINFONT}" \
    -V mainfont="${MAINFONT}" \
    -V sansfont="${MAINFONT}" \
    -V monofont="${MONOFONT}" \
    -V geometry:margin=1in \
    -V include-after="\\input{templates/copyright.tex}" \
    "${DocFile}" -s -o ${DocOutPutFile}
