#!/bin/bash

set -e
# test passed in pandoc 1.19.1

MAINFONT="WenQuanYi Micro Hei"
MONOFONT="WenQuanYi Micro Hei Mono"

DocFile=${1}
DocTitle=${2}
DocOutPutFile=${3}
if [ ! -n "$DocFile" ]; then
    DocFile="doc.md"
fi
if [ ! -n "$DocTitle" ]; then
    DocTitle="Mogdb"
fi
if [ ! -n "$DocOutPutFile" ]; then
    DocOutPutFile="output.pdf"
fi

# MAINFONT="Tsentsiu Sans HG"
# MONOFONT="Tsentsiu Sans Console HG"

#_version_tag="$(date '+%Y%m%d').$(git rev-parse --short HEAD)"
_version_tag="$(date '+%Y%m%d')"

# default version: `pandoc --latex-engine=xelatex doc.md -s -o output2.pdf`
# used to debug template setting error

# add docs versions
# generate PDF for dev version

pandoc -N --toc --pdf-engine=xelatex \
    -f markdown+smart+markdown_in_html_blocks+grid_tables+escaped_line_breaks \
    --template=${templateV2File} \
    --listings \
    --toc-depth=4 \
    -V title="${DocTitle}" \
    -V author="Enmotech Inc." \
    -V date="${_version_tag}" \
    -V CJKmainfont="${MAINFONT}" \
    -V mainfont="${MAINFONT}" \
    -V sansfont="${MAINFONT}" \
    -V monofont="${MONOFONT}" \
    -V include-after="\\input{templates/copyright.tex}" \
    --variable geometry:top=2.0cm \
    --variable geometry:bottom=2.0cm \
    --variable geometry:left=1.5cm \
    --variable geometry:right=1.5cm \
    --variable title-meta="${DocTitle}" \
    --variable author-meta="Enmotech Inc." \
    --variable creator-meta="Enmotech Inc." \
    --variable listings-disable-line-numbers="true" \
    --variable page-background="media/mogdb-logo.png" \
    --variable toc-own-page="true" \
    --variable titlepage="true" \
    --variable table-use-row-colors="true" \
    "${DocFile}" -s -o ${DocOutPutFile}
