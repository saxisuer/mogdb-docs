require("dotenv").config()
module.exports = {
  siteMetadata: {
    title: `MogDB Docs`,
    description: `MogDB Docs`,
    author: `@Mogdb`,
    siteUrl: `https://docs.mogdb.io`
  },
  plugins: [
    "gatsby-plugin-sass",
    {
      resolve: 'gatsby-plugin-import',
      options: {
        libraryName: "antd",
        style: 'css',
      }
    },
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        "trackingId": "G-XVG62KBB3X",
        "head": true
      }
    },
    "gatsby-plugin-react-helmet",
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-autolink-headers-j`,
            options: {
              offsetY: 84
            }
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              showLineNumbers: false
            }
          },
        ]
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        "name": "docs",
        "path": `${__dirname}/product`,
        "ignore": [`**/toc_*`]
      },
      __key: "pages"
    },
    "gatsby-plugin-sitemap",
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: `https://docs.mogdb.io`,
        sitemap: 'https://docs.mogdb.io/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }],
      },
    },
    `gatsby-plugin-meta-redirect`
  ]
};