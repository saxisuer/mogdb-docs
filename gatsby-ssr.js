const React = require("react")

export const onRenderBody = ({ setPostBodyComponents }) => {
  setPostBodyComponents([
    <script src='/asciinema-player.min.js'></script>
  ])
}