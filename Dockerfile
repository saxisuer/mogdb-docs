FROM node:14
WORKDIR /mogdb-docs
COPY . .
RUN rm -rf package-lock.json && \
    npm config set registry http://registry.npm.taobao.org/ && \
    npm i

EXPOSE 8000

CMD ["sh", "script/run.sh"]
